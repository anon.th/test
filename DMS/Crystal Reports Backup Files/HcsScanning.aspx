<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="HcsScanning.aspx.cs" AutoEventWireup="false" Inherits="com.ties.HcsScanning" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>HcsScanning</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet">
		<LINK media="all" href="css/jsDatePick_ltr.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/jquery.1.4.2.js"></script>
		<script language="javascript" src="Scripts/jsDatePick.min.1.3.js"></script>
		<script src="Scripts/jquery.timers.js" type="text/javascript"></script>
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<style type="text/css">A.button { DISPLAY: block; BACKGROUND-IMAGE: url(BUTTON SPRITES IMAGE); WIDTH: 86px; TEXT-INDENT: -9999px; HEIGHT: 130px }
	A.micbutton:hover { BACKGROUND-POSITION: 0px -130px }
	A.micbutton:active { BACKGROUND-POSITION: 0px -260px }
		</style>
		<script type="text/javascript">	  	
			var mode;
			var txtBarCode;
				  
			$(function () {
				var mode = document.getElementById("txtMode");
				var gridMode = document.getElementById("txtGridMode");				
								
		        // -------------------- Timer 1500 -------------------
		        //=========== SET FOCUS
		        $(".myFocus").everyTime(1500, function () {
						OnFocus();											
				});
				
				// ------------ Send action from Bar Code ------------ 
				// Check tab character or enter character
				$('#txtBarCode').keydown(function (event) 
				{
					if (event.keyCode == '9' || event.keyCode == '13' ) 
					{
						event.preventDefault();											
						addItem();
						
						OnFocus();
					}
				});										
			});			
			
			
			// --------------------------------- OnFocus ---------------------------------	
			function OnFocus()
		    {
				var formObject = document.HcsScanning;
				var _mode = document.getElementById("txtMode");
				var _gridMode = document.getElementById("txtGridMode");
				
				if (_mode.value == 'EDIT' && _gridMode.value == 'SCAN')
					formObject.txtBarCode.focus();
		    }
		</script>
		<script language="javascript" type="text/javascript">
		var browserType;
		
		if (document.layers) {browserType = "nn4"}
		if (document.all) {browserType = "ie"}
		if (window.navigator.userAgent.toLowerCase().match("gecko")) {
			browserType= "gecko"
		}
		

///================================== LoadJsDatePick ==================================///
function LoadJsDatePick()
{	
	g_globalObject = new JsDatePick({
		useMode:1,
		isStripped:true,
		target:"divCalendar"			
		/*selectedDate:{				This is an example of what the full configuration offers.
			day:5,						For full documentation about these settings please see the full version of the code.
			month:9,
			year:2006
		},
		yearsRange:[1978,2020],
		limitToToday:false,
		cellColorScheme:"beige",
		dateFormat:"%m-%d-%Y",
		imgPath:"images/",
		weekStartDay:1*/
		
		
	});
		
	g_globalObject.setOnSelectedDelegate(function(){
		var obj = g_globalObject.getSelectedDay();
		
		//alert("a date was just selected and the date is : " + obj.day + "/" + obj.month + "/" + obj.year);
		
		var sTmp = "";
		var sDay = "";
		var sMonth = "";
		var sYear = "";
		
		sYear = obj.year;
		
		sTmp = "0" + obj.month;
		sMonth = sTmp.substring(sTmp.length - 2);
		
		sTmp = "0" + obj.day;
		sDay = sTmp.substring(sTmp.length - 2);
		
		var txtViewDate = document.getElementById("txtViewDate");
		txtViewDate.value = sDay + "/" + sMonth + "/" + sYear;
		
		callServer();
	});
};

	
///================================== addItem ==================================///
function addItem() 
{
    var formObject = document.HcsScanning
    var now = new Date();
    var lblErrorMessage = document.getElementById("lblErrorMessage");

	lblErrorMessage.innerHTML = "";
	
    if (formObject.txtBarCode.value != "") 
    {
    	var txtFirstDate = document.getElementById("txtDate");
    	var txtHcsNumber = document.getElementById("txtHcsNumber");
    	//var lblHcsNumber = document.getElementById("lblHcsNumber");
    	    	
    	if (lblHcsNumber.innerHTML == '' || lblHcsNumber.innerHTML == ' ')
    	{  
    		formObject.txtBarCode.value = "";    		
    		alert("Error; Not Found Hcs Number.");
    	}
    	else
    	{    					
			if(!VerifyScannedBarCode(formObject.txtBarCode.value))
			{			
				formObject.txtBarCode.value = "";
				formObject.txtScanerID.value = "";
				txtFirstDate.value = "";
				formObject.txtBarCode.focus();
			}
			else
			{	
				if(formObject.txtScanerID.value == "")
				{
					formObject.txtScanerID.value= document.getElementById("lblUserID").innerHTML;
				}
				var txtItem = formObject.txtBarCode.value + JsGenSpace(32-formObject.txtBarCode.value.length);
				txtItem += formatDate(new Date(), "dd/mm/yyyy hh:nn:ss")+JsGenSpace(22-formatDate(new Date(), "dd/mm/yyyy hh:nn:ss").length);
				txtItem += formObject.txtScanerID.value;
					
					
				var txtVal = formObject.txtBarCode.value + '%' + formatDate(new Date(), "dd/mm/yyyy hh:nn:ss") + '%' + formObject.txtScanerID.value;
									
				txtFirstDate.value = formatDate(new Date(), "dd/mm/yyyy hh:nn:ss");
			
				if(txtHcsNumber.value == "")
				{
					txtHcsNumber.value = document.getElementById("lblHcsNumber").innerHTML
				}
										
				addOption(formObject.lbScanningPackages,txtItem,txtVal);
					
				doCallAjax('ADD');
					
				formObject.txtBarCode.value = "";
				formObject.txtScanerID.value = "";
				txtFirstDate.value = "";
				formObject.txtBarCode.focus();
			}
		} 
	}
} 


///================================== addOption ==================================///
function addOption(selectObject,optionText,optionValue) 
{
    var optionObject = new Option(optionText,optionValue)
    var optionRank = selectObject.options.length
    selectObject.options[optionRank]=optionObject
}


///================================== stopRKey ==================================///
function stopRKey(evt) 
{
	var evt = (evt) ? evt : ((event) ? event : null);
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
}

 
///================================== VerifyScannedBarCode ==================================///
function VerifyScannedBarCode(strVerify)
{
	for(i=0;i<strVerify.length;i++)		////loop for check each char not include "0-9,-,_,space and non english char"
	{
		var charCode = strVerify.charCodeAt(i);

		if(charCode > 31&&(charCode < 48 || charCode > 57)&&(charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122) && (charCode!=32)&& (charCode!=45)&& (charCode!=95)&& (charCode!=47))
		{
			alert("Consignment# error.");
			return false;
		}
	}
			
	return true;
}


///================================== MoveItems ==================================///
/*
	- Function MoveItems()
	- Direction: Specifies direction i.e. L = Left, R = Right
	- All: Move All items 1 = All, 0 = Selected
*/
function MoveItems(Direction,All,appendtxt)
{
	var LstLeft;
	var LstRight;
	var Removed = '';
           
    if (Direction == 'R')
    {
        var LstLeft = document.getElementById("lbScanningPackages");
    }
    
    Removed = Removed + LstLeft.options[0].value + ',';
                    
	LstLeft.options[0]=null;

	doCallAjax('ADD');
    return false;           
}
   

////================================== CallMoveItems ==================================////
function CallMoveItems(appendtxt)
{
    return MoveItems('R',1,appendtxt);
}


////============================= XmlHttpRequest - Start =============================////
var HttPRequest = false;

function doCallAjax() 
{
	var lbox = document.getElementById("lbScanningPackages");
	var Mode = 'ADD';
			
	if(lbox.length > 0)
	{	 
		HttPRequest = false;
		      
		if (window.XMLHttpRequest) {				// Mozilla, Safari,...
			HttPRequest = new XMLHttpRequest();
			if (HttPRequest.overrideMimeType) {
				HttPRequest.overrideMimeType('text/html');
			}
		} else if (window.ActiveXObject) {			// IE
			try 
			{
				HttPRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} 
			catch (e) 
			{
				try 
				{
					HttPRequest = new ActiveXObject("Microsoft.XMLHTTP");
				} 
				catch (e) 
				{}
			}
		} 
    		  
		if (!HttPRequest) {
			alert('Cannot create XMLHTTP instance');
			return false;
		}
		else
		{
			//alert('Create XMLHTTP instance');
		}
    	      
    	///-- Call Background HcsScanProcess.aspx
		var url = 'HcsScanProcess.aspx';
		var txtparam = lbox.options[0].value.split("%");
		var formObject = document.HcsScanning;				
  
		var appId = formObject.hidAppID.value;	
		var enterpriseId = formObject.hidEnterpriseID.value;
		var barCode = txtparam[0];
		var scanDateTime =txtparam[1];
		var scanUserId = txtparam[2];
		var hcsNo = document.getElementById("lblHcsNumber").innerHTML;
		var statusCode = document.getElementById("lblStatusCode").innerHTML;
		var userId = document.getElementById("lblUserID").innerHTML;
		var location =document.getElementById("lblLocation").innerHTML;
		      
		var pmeters = "tAppId=" + encodeURI(appId) +
					  "&tEntId=" + encodeURI(enterpriseId) +
					  "&tBarCode=" + encodeURI(barCode) +
		              "&tScanDateTime=" + encodeURI(scanDateTime) +
		              "&tHcsNo=" + encodeURI(hcsNo) +
		              "&tScanUserId=" + encodeURI(scanUserId) +
		              "&tStatusCode=" + encodeURI(statusCode) +
		              "&tLocation=" + encodeURI(location);
			
		HttPRequest.open('POST', url, true);

		HttPRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		HttPRequest.setRequestHeader("Content-length", pmeters.length);
		HttPRequest.send(pmeters);

		HttPRequest.onreadystatechange = function()		
		{		
			if(HttPRequest.readyState == 3)		///-- Loading Request
			{
				//document.getElementById("mySpan").innerHTML = "Now is Loading...";
			}

			if(HttPRequest.readyState == 4)		///-- Return Request
			{
				//RemoveFinnishRecord(document.getElementById("lbScanningPackages"), lbox.options[0].value,lbox.options[0].text);
							
				var result = HttPRequest.responseText;
				
				//alert(result);
				//alert(HttPRequest.responseText.length);
				
				//result =result.substring(0,1);

				if(HttPRequest.responseText.length==0)	///-- Lost network connection
				{
					alert("Lost network connection");

					//doCallAjax('ADD');
				}
				else if (result=='E')	///-- Case Return "E" that mean not insert to DB so Loop Insert First rows until can insert.
				{
					alert("Case Return [E] that mean not insert to DB so Loop Insert First rows until can insert");
					doCallAjax('ADD');
				}
				else
				{
					CallMoveItems(result);	
					
					callServer();
				}	
			}
		}
	}
}
//// ---- XmlHttpRequest - End ---- ///////////


////================================== JsGenSpace ==================================////
function JsGenSpace(cnt)
{
	var space='';
	var i=0;
	for (i=0;i<=cnt;i++)
	{
		space+=' ';
	}
	return space;
}


////============================= getMovedataInterval =============================////
function getMovedataInterval()
{
    return 600000;
}


////=================================== refresh ===================================////
function  refresh()
{
	var Mode='refresh';
	{
		HttPRequest = false;
		if (window.XMLHttpRequest) {			///-- Mozilla, Safari,...
			HttPRequest = new XMLHttpRequest();
			if (HttPRequest.overrideMimeType) {
			HttPRequest.overrideMimeType('text/html');
			}
		} else if (window.ActiveXObject) {		///-- IE
			try {
			HttPRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
			try {
				HttPRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
			}
		} 
    		  
		if (!HttPRequest) {
			alert('Cannot create XMLHTTP instance');
			return false;
		}
    	      
		var url = 'HcsScanProcess.aspx';
		var formObject = document.HcsScanning		  
		
		var pmeters = "tMode=" + Mode;

		HttPRequest.open('POST',url,true);

		HttPRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		HttPRequest.setRequestHeader("Content-length", pmeters.length);
		HttPRequest.send(pmeters);
    	
		HttPRequest.onreadystatechange = function()
		{			
			if(HttPRequest.readyState == 3)  ///-- Loading Request
			{
				//document.getElementById("mySpan").innerHTML = "Now is Loading...";
			}

			if(HttPRequest.readyState == 4) ///-- Return Request
			{
			
    		}
		}
	}
}

////======================================================================////
document.onkeypress = stopRKey; 
setInterval('refresh()',getMovedataInterval());
////======================================================================////


		
////============================= formatDate =============================////
var formatDate = function (formatDate, formatString) {
	if(formatDate instanceof Date) {
		var months = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
		var yyyy = formatDate.getFullYear();
		var yy = yyyy.toString().substring(2);
		var m = formatDate.getMonth()+1;
		var mm = m < 10 ? "0" + m : m;
		var mmm = months[m-1];
		var d = formatDate.getDate();
		var dd = d < 10 ? "0" + d : d;
		
		var h = formatDate.getHours();
		var hh = h < 10 ? "0" + h : h;
		var n = formatDate.getMinutes();
		var nn = n < 10 ? "0" + n : n;
		var s = formatDate.getSeconds();
		var ss = s < 10 ? "0" + s : s;

		formatString = formatString.replace(/yyyy/i, yyyy);
		formatString = formatString.replace(/yy/i, yy);
		formatString = formatString.replace(/mmm/i, mmm);
		formatString = formatString.replace(/mm/i, mm);
		formatString = formatString.replace(/m/i, m);
		formatString = formatString.replace(/dd/i, dd);
		formatString = formatString.replace(/d/i, d);
		formatString = formatString.replace(/hh/i, hh);
		formatString = formatString.replace(/h/i, h);
		formatString = formatString.replace(/nn/i, nn);
		formatString = formatString.replace(/n/i, n);
		formatString = formatString.replace(/ss/i, ss);
		formatString = formatString.replace(/s/i, s);

		return formatString;
	} else {
		return "";
	}
}


////============================= callServer =============================////
function callServer()
{
	var btn = document.getElementById('btnCallServer');

	if(btn == null)
		alert('btnCallServer button not found!');
	else
		btn.click();
}

		</script>
	</HEAD>
	<BODY onclick="GetScrollPosition();" onload="{SetScrollPosition();LoadJsDatePick();}"
		onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<FORM id="HcsScanning" method="post" runat="server">
			<p></p>
			<P class="mainTitleSize">&nbsp;HCS&nbsp;Scanning</P>
			<DIV><asp:label id="lblErrorMessage" CssClass="errorMsgColor" Width="800px" Runat="server"></asp:label><asp:textbox id="hidAppID" runat="server" Width="0px" Font-Size="XX-Small"></asp:textbox><asp:textbox id="hidEnterpriseID" runat="server" Width="0px" Font-Size="XX-Small"></asp:textbox><INPUT type="hidden" name="ScrollPosition">
				<asp:button id="btnCallServer" style="DISPLAY: none" runat="server"></asp:button></DIV>
			<TABLE width="1000" border="1">
				<TR id="trRow01">
					<TD style="VERTICAL-ALIGN: middle; TEXT-ALIGN: left"><label style="FONT-WEIGHT: bolder; FONT-SIZE: 15px; FONT-FAMILY: Verdana, Arial, Helveti">&nbsp;MODE:&nbsp;</label><asp:textbox id="txtMode" onblur="OnFocus();" runat="server" Width="60px" Height="23px" ReadOnly="True"
							Font-Bold="True" BackColor="DimGray" ForeColor="White">CLEAR</asp:textbox>
						<asp:textbox id="txtGridMode" runat="server" Width="60px" Height="23px" ReadOnly="True" Font-Bold="True"
							BackColor="DimGray" ForeColor="White" AutoPostBack="True"></asp:textbox></TD">
					</TD>
				<TR id="trRow02" style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">
					<TD id="tdRow02">
						<DIV>
							<FIELDSET style="WIDTH: 330px"><LEGEND class="tableHeadingFieldset">&nbsp;Action&nbsp;</LEGEND>
								<TABLE style="WIDTH: 100%">
									<TR>
										<TD style="HEIGHT: 18px" colSpan="3"><asp:button id="btnClear" tabIndex="20" runat="server" Width="130" Height="28" Text="Clear"></asp:button><asp:label id="lblStatusCode" style="DISPLAY: none" runat="server" CssClass="tableLabel">HCS</asp:label>&nbsp;&nbsp;
											<asp:textbox id="txtHcsNumber" runat="server" Width="0px" Font-Size="XX-Small"></asp:textbox></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 18px">&nbsp;</TD>
										<TD class="tableLabel" style="WIDTH: 110px"><asp:button onkeypress="LoadJsDatePick();" id="btnEdit" tabIndex="21" runat="server" Width="103"
												Height="28" Text="Edit"></asp:button></TD>
										<TD class="tableLabel"><asp:button id="btnNew" tabIndex="24" runat="server" Width="130" Height="22" Text="New HCS"></asp:button><br>
											<asp:button id="btnDelete" tabIndex="25" runat="server" Width="130" Height="22" Text="Delete HCS"></asp:button><br>
											<asp:button id="btnClose" tabIndex="26" runat="server" Width="130" Height="22" Text="Close HCS"></asp:button></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 18px">&nbsp;</TD>
										<TD class="tableLabel" style="WIDTH: 110px"><asp:button id="btnView" tabIndex="22" runat="server" Width="103" Height="28" Text="View"></asp:button></TD>
										<TD class="tableLabel"><asp:button id="btnPrint" tabIndex="27" runat="server" Width="130" Height="22" Text="Print PDF"></asp:button><br>
											<asp:button id="btnExport" tabIndex="28" runat="server" Width="130" Height="22" Text="Export XLS"></asp:button><br>
											<asp:button id="btnCancel" tabIndex="29" runat="server" Width="130" Height="22" Text="Cancel Closed HCS"></asp:button></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 18px">&nbsp;</TD>
										<TD class="tableLabel" style="WIDTH: 110px"><asp:button id="btnSearch" tabIndex="23" runat="server" Width="103" Height="28" Text="Search Cons #"></asp:button><br>
											&nbsp;</TD>
										<TD class="tableLabel">&nbsp;</TD>
									</TR>
									<TR>
										<TD>&nbsp;</TD>
										<TD class="tableLabel" style="WIDTH: 110px">HCS&nbsp;Number:</TD>
										<TD><asp:label id="lblHcsNumber" style="FONT-SIZE: 14px" runat="server" CssClass="tableLabel" Font-Bold="True"
												ForeColor="Red"></asp:label></TD>
									</TR>
									<TR>
										<TD>&nbsp;</TD>
										<TD class="tableLabel" style="WIDTH: 110px">HCS&nbsp;Date:</TD>
										<TD><asp:label id="lblHcsDate" style="FONT-SIZE: 14px" runat="server" CssClass="tableLabel" Font-Bold="True"
												ForeColor="Red"></asp:label></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 18px">&nbsp;</TD>
										<TD class="tableLabel" style="WIDTH: 110px">User&nbsp;ID:</TD>
										<TD><asp:label id="lblUserID" runat="server" CssClass="tableLabel" Font-Bold="True" ForeColor="Red"></asp:label></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 18px">&nbsp;</TD>
										<TD class="tableLabel" style="WIDTH: 110px">Location:</TD>
										<TD><asp:label id="lblLocation" runat="server" CssClass="tableLabel" Font-Bold="True" ForeColor="Red"></asp:label><asp:label id="lblSubDc" runat="server" CssClass="tableLabel" Font-Bold="True" ForeColor="Red"></asp:label></TD>
									</TR>
								</TABLE>
							</FIELDSET>
						</DIV>
						<DIV id="divEdit" style="Z-INDEX: 101; LEFT: 345px; POSITION: absolute; TOP: 97px" runat="server">
							<table style="TEXT-ALIGN: left; VERTICLE-ALIGN: top">
								<tr>
									<td>
										<FIELDSET style="WIDTH: 175px; HEIGHT: 132px"><LEGEND class="tableHeadingFieldset">&nbsp;Select&nbsp;Edit&nbsp;Data</LEGEND>
											<table style="WIDTH: 100%">
												<tr>
													<td style="WIDTH: 5px">&nbsp;</td>
													<td class="tableLabel"><asp:listbox id="lbEditData" runat="server" Width="145px" Height="110px" AutoPostBack="True"
															Rows="5"></asp:listbox></td>
												</tr>
											</table>
										</FIELDSET>
									</td>
									<td>
										<FIELDSET style="WIDTH: 329px; HEIGHT: 132px"><LEGEND class="tableHeadingFieldset">&nbsp;Scan&nbsp;Data&nbsp;</LEGEND>
											<table style="WIDTH: 100%">
												<tr style="HEIGHT: 30px">
													<td style="WIDTH: 5px">&nbsp;</td>
													<td style="TEXT-ALIGN: left"><asp:button id="btnScan" CssClass="queryButton" style="WIDTH: 50px; HEIGHT: 32px" runat="server" Text="Scan"></asp:button>&nbsp;<asp:button id="btnStop" CssClass="queryButton" style="WIDTH: 50px; HEIGHT: 32px" runat="server" Text="Stop"></asp:button></td>
												</tr>
												<tr style="HEIGHT: 30px">
													<td style="WIDTH: 5px">&nbsp;</td>
													<td class="tableLabel"><strong>Consignment Barcode:</strong></td>
												</tr>
												<tr style="HEIGHT: 30px">
													<td style="WIDTH: 5px">&nbsp;</td>
													<td class="tableLabel"><asp:textbox id="txtBarCode" runat="server" Width="260" Font-Size="Large" Height="32" MaxLength="30"></asp:textbox><asp:textbox id="txtScanerID" runat="server" Width="0px"></asp:textbox><cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="0px" ReadOnly="True" MaxLength="10"
															TextMaskType="msDate" TextMaskString="99/99/9999 99:99:99"></cc1:mstextbox>&nbsp;<INPUT class="queryButton" id="btnAdd" style="WIDTH: 35px; HEIGHT: 32px" onclick="addItem();"
															type="button" value="Add" name="btnAdd">
													</td>
												</tr>
											</table>
										</FIELDSET>
									</td>
								</tr>
								<tr>
									<td colSpan="2">
										<FIELDSET style="WIDTH: 510px; HEIGHT: 178px"><LEGEND class="tableHeadingFieldset">&nbsp;Status&nbsp;Scan&nbsp;</LEGEND>
											<table style="WIDTH: 100%">
												<tr>
													<td style="WIDTH: 5px">&nbsp;</td>
													<td class="tableLabel">&nbsp;Package&nbsp;Scan&nbsp;</td>
													<td class="tableLabel">&nbsp;Scan&nbsp;D/T&nbsp;</td>
													<td class="tableLabel">&nbsp;ID&nbsp;</td>
												</tr>
												<tr>
													<td style="WIDTH: 5px">&nbsp;</td>
													<td colSpan="3"><asp:listbox id="lbScanningPackages" style="FONT-FAMILY: 'Courier New', Courier, monospace" runat="server"
															Width="478px" Font-Size="8pt" Height="89px"></asp:listbox></td>
												</tr>
												<tr>
													<td style="WIDTH: 5px">&nbsp;</td>
													<td colSpan="3"><INPUT class="queryButton" id="btnResume" onclick="doCallAjax('ADD');return false;" type="button"
															value="Resume" name="btnResume"></td>
												</tr>
											</table>
											<br>
										</FIELDSET>
									</td>
								</tr>
							</table>
						</DIV>
						<DIV id="divView" style="Z-INDEX: 102; LEFT: 345px; POSITION: absolute; TOP: 98px" runat="server">
							<FIELDSET style="WIDTH: 510px; HEIGHT: 319px"><LEGEND class="tableHeadingFieldset">&nbsp;View&nbsp;Data&nbsp;</LEGEND><br>
								<table>
									<tr>
										<td style="WIDTH: 5px">&nbsp;</td>
										<td>
											<div id="divCalendar" style="BORDER-RIGHT: buttonshadow 1px outset; BORDER-TOP: buttonshadow 1px outset; BORDER-LEFT: buttonshadow 1px outset; WIDTH: 205px; BORDER-BOTTOM: buttonshadow 1px outset; HEIGHT: 230px; BACKGROUND-COLOR: white"
												align="center"></div>
										</td>
										<td style="WIDTH: 20px">&nbsp;</td>
										<td class="tableLabel" style="WIDTH: 40%" vAlign="top" align="left"><strong>Selected 
												Date:</strong><br>
											<asp:textbox id="txtViewDate" runat="server" Width="145px" Font-Size="Large" Height="32" ReadOnly="True"
												BackColor="Silver" AutoPostBack="True"></asp:textbox><br>
											<br>
											<strong>Select View Data:</strong><br>
											<asp:listbox id="lbViewData" runat="server" Width="145px" Height="110px" AutoPostBack="True"
												Rows="5"></asp:listbox></td>
									</tr>
								</table>
								<br>
								&nbsp;
							</FIELDSET>
						</DIV>
						<DIV id="divSearch" style="Z-INDEX: 103; LEFT: 345px; POSITION: absolute; TOP: 98px; TEXT-ALIGN: center"
							runat="server">
							<FIELDSET style="WIDTH: 510px; HEIGHT: 319px"><LEGEND class="tableHeadingFieldset">&nbsp;Search&nbsp;Data&nbsp;</LEGEND><br>
								<table style="WIDTH: 96%" border="1">
									<tr>
										<td class="tableLabel" style="WIDTH: 35%" vAlign="top" align="left" rowSpan="6">
											<div align="right"><strong>Entry&nbsp;Consignment No.:&nbsp;</strong><br>
												<asp:textbox id="txtSearchCons" runat="server" Width="155px" MaxLength="30"></asp:textbox>&nbsp;<br>
												<asp:button id="btnSearchCons" runat="server" CssClass="queryButton" Width="60" Height="30"
													Text="Search"></asp:button>&nbsp;<br>
											</div>
											<br>
										</td>
										<td class="tableLabel" style="HEIGHT: 37px" colSpan="2">&nbsp;<asp:label id="lblSResult" style="FONT-SIZE: 14px" runat="server" CssClass="tableLabel" Font-Bold="True"
												ForeColor="Orange">lblSResult</asp:label></td>
									</tr>
									<tr>
										<td class="tableLabel" style="WIDTH: 20%; HEIGHT: 37px">&nbsp;HCS No.:&nbsp;</td>
										<td class="tableLabel">&nbsp;<asp:label id="lblSHcsNo" style="FONT-SIZE: 14px" runat="server" CssClass="tableLabel" Font-Bold="True"
												ForeColor="Orange">lblSHcsNo</asp:label></td>
									</tr>
									<tr>
										<td class="tableLabel" style="WIDTH: 20%; HEIGHT: 37px">&nbsp;HCS Date:&nbsp;</td>
										<td class="tableLabel">&nbsp;<asp:label id="lblSHcsDate" style="FONT-SIZE: 14px" runat="server" CssClass="tableLabel" Font-Bold="True"
												ForeColor="Orange">lblSHcsDate</asp:label></td>
									</tr>
									<tr>
										<td class="tableLabel" style="WIDTH: 20%; HEIGHT: 37px">&nbsp;HCS Status:&nbsp;</td>
										<td class="tableLabel">&nbsp;<asp:label id="lblSHcsStatus" style="FONT-SIZE: 14px" runat="server" CssClass="tableLabel"
												Font-Bold="True" ForeColor="Orange">lblSHcsStatus</asp:label></td>
									</tr>
									<tr>
										<td class="tableLabel" style="WIDTH: 20%; HEIGHT: 37px">&nbsp;Created By:&nbsp;</td>
										<td class="tableLabel">&nbsp;<asp:label id="lblSCreatedBy" style="FONT-SIZE: 14px" runat="server" CssClass="tableLabel"
												Font-Bold="True" ForeColor="Orange">lblSCreatedBy</asp:label></td>
									</tr>
									<tr>
										<td class="tableLabel" style="WIDTH: 20%; HEIGHT: 37px">&nbsp;Created Date:&nbsp;</td>
										<td class="tableLabel">&nbsp;<asp:label id="lblSCreatedDate" style="FONT-SIZE: 14px" runat="server" CssClass="tableLabel"
												Font-Bold="True" ForeColor="Orange">lblSCreatedDate</asp:label></td>
									</tr>
								</table>
								<br>
								&nbsp;
							</FIELDSET>
						</DIV>
					</TD>
				</TR>
				<TR id="trRow03" style="WIDTH: 100%; TEXT-ALIGN: center" vAlign="top" align="center">
					<TD id="tdRow03">
						<DIV>
							<FIELDSET style="WIDTH: 100%"><LEGEND class="tableHeadingFieldset">&nbsp;Processed&nbsp;to&nbsp;DMS&nbsp;</LEGEND>
								<TABLE style="WIDTH: 100%; TEXT-ALIGN: center; verticle-align: top">
									<TR>
										<TD style="WIDTH: 60%" vAlign="top" align="center"><label class="maintitleSize" style="WIDTH: 100%; TEXT-ALIGN: center"><u>D&nbsp;M&nbsp;S</u></label>
											<br>
											<asp:datagrid id="dgHcs" runat="server" PageSize="100" OnEditCommand="dgHcs_Edit" OnUpdateCommand="dgHcs_Update"
												OnCancelCommand="dgHcs_Cancel" OnItemDataBound="dgHcs_ItemDataBound" AllowCustomPaging="True"
												AutoGenerateColumns="False" OnDeleteCommand="dgHcs_Delete">
												<ItemStyle Height="20px" CssClass="gridfield"></ItemStyle>
												<Columns>
													<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
														CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
														<HeaderStyle CssClass="gridHeading"></HeaderStyle>
													</asp:EditCommandColumn>
													<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
														<HeaderStyle CssClass="gridHeading"></HeaderStyle>
													</asp:ButtonColumn>
													<asp:TemplateColumn Visible="False" HeaderText="HCS&#160;No.">
														<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
														<ItemStyle CssClass="gridField"></ItemStyle>
														<ItemTemplate>
															<label class="gridLabel" style="width:50px">
																<%#DataBinder.Eval(Container.DataItem,"hcs_no")%>
															</label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Consignment&#160;No.">
														<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
														<ItemStyle CssClass="gridField"></ItemStyle>
														<ItemTemplate>
															<label class="gridLabel" style="width:70px">
																<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>
															</label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Scan&#160;Date">
														<HeaderStyle Font-Bold="True" CssClass="gridheading"></HeaderStyle>
														<ItemStyle CssClass="gridField"></ItemStyle>
														<ItemTemplate>
															<label class="gridLabel" style="width:120px">
																<%#DataBinder.Eval(Container.DataItem,"scandatetime", "{0:dd/MM/yy HH:mm:sss}")%>
															</label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn Visible="False" HeaderText="Scan&#160;User">
														<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
														<ItemStyle CssClass="gridField"></ItemStyle>
														<ItemTemplate>
															<label class="gridLabel" style="width:60px">
																<%#DataBinder.Eval(Container.DataItem, "scanuserid", "{0}")%>
															</label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Invr">
														<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
														<ItemStyle CssClass="gridField"></ItemStyle>
														<ItemTemplate>
															<asp:checkbox id="chkIsInvr" Enabled=False runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"isinvr","{0}") == "Y"? true : false %>'>
															</asp:checkbox>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Remark">
														<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
														<ItemStyle CssClass="gridField"></ItemStyle>
														<ItemTemplate>
															<label class="gridLabel" style="Width:250px">
																<%#DataBinder.Eval(Container.DataItem, "invrremark")%>
															</label>
														</ItemTemplate>
														<EditItemTemplate>
															<asp:TextBox id=txtInvrRemark runat="server" Width="250px" MaxLength="50" CssClass="gridTextbox" Text='<%#DataBinder.Eval(Container.DataItem,"invrremark")%>' Enabled='<%# DataBinder.Eval(Container.DataItem,"isinvr","{0}") == "Y"? true : false %>'>
															</asp:TextBox>
														</EditItemTemplate>
													</asp:TemplateColumn>
												</Columns>
												<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
											</asp:datagrid><br>
										</TD>
										<TD>&nbsp;</TD>
										<TD style="WIDTH: 40%" vAlign="top" align="center"><label class="maintitleSize" style="WIDTH: 100%; TEXT-ALIGN: center"><u>Data&nbsp;Missing</u></label>
											<br>
											<asp:datagrid id="dgError" runat="server" PageSize="100" OnItemDataBound="dgError_ItemDataBound"
												AllowCustomPaging="True" AutoGenerateColumns="False">
												<ItemStyle Height="20px" CssClass="gridfield"></ItemStyle>
												<Columns>
													<asp:TemplateColumn HeaderText="Consignment&#160;No.">
														<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
														<ItemStyle CssClass="gridField"></ItemStyle>
														<ItemTemplate>
															<label class="gridLabel" style="width:70px">
																<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>
															</label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Scan&#160;Date">
														<HeaderStyle Font-Bold="True" CssClass="gridheading"></HeaderStyle>
														<ItemStyle CssClass="gridField"></ItemStyle>
														<ItemTemplate>
															<label class="gridLabel" style="width:120px">
																<%#DataBinder.Eval(Container.DataItem,"scandatetime", "{0:dd/MM/yy HH:mm:sss}")%>
															</label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn Visible="False" HeaderText="Scan&#160;User">
														<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
														<ItemStyle CssClass="gridField"></ItemStyle>
														<ItemTemplate>
															<label class="gridLabel" style="width:60px">
																<%#DataBinder.Eval(Container.DataItem, "scanuserid", "{0}")%>
															</label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Dup">
														<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
														<ItemStyle CssClass="gridField"></ItemStyle>
														<ItemTemplate>
															<asp:checkbox id="chkIsDup" Enabled=False runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"isdup","{0}") == "Y"? true : false %>'>
															</asp:checkbox>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="No&#160;POD">
														<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
														<ItemStyle CssClass="gridField"></ItemStyle>
														<ItemTemplate>
															<asp:checkbox id="chkIsNoPod" Enabled=False runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"isnopod","{0}") == "Y"? true : false %>'>
															</asp:checkbox>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="HCR">
														<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
														<ItemStyle CssClass="gridField"></ItemStyle>
														<ItemTemplate>
															<asp:checkbox id="chkIsHcr" Enabled=False runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"ishcr","{0}") == "Y"? true : false %>'>
															</asp:checkbox>
														</ItemTemplate>
													</asp:TemplateColumn>
												</Columns>
												<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
											</asp:datagrid><br>
										</TD>
									</TR>
								</TABLE>
							</FIELDSET>
						</DIV>
					</TD>
				</TR>
			</TABLE>
			<br>
			<DIV id="divSubDc" style="Z-INDEX: 104; LEFT: 285px; VISIBILITY: hidden; POSITION: absolute; TOP: 150px; BACKGROUND-COLOR: silver; TEXT-ALIGN: center"
				runat="server">
				<table border="2">
					<tr>
						<td class="tableLabel" background="Silver"><strong>Select Sub DC</strong></td>
					</tr>
					<tr>
						<td><asp:listbox id="lbSubDc" runat="server" Width="110px" Height="100px" AutoPostBack="True" Rows="5"></asp:listbox></td>
					</tr>
					<tr>
						<td><input onclick="document.all['divSubDc'].style.visibility = 'hidden';" type="button" value="closed"></td>
					</tr>
				</table>
			</DIV>
			<DIV class="myFocus">&nbsp;</DIV>
			<DIV id="divTab" runat="server"></DIV>
		</FORM>
	</BODY>
</HTML>
