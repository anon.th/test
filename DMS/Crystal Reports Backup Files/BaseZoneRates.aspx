<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="BaseZoneRates.aspx.cs" AutoEventWireup="false" Inherits="com.ties.BaseZoneRates" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>BaseZoneRates</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script runat="server">
		</script>
		<script language="javascript">
//by X oct 03 08
		function setValue()
		{
			var s = document.all['inFile'];
			document.all['txtFilePath'].innerText = s.value;
			if(s.value != '') 
				document.all['btnImport'].disabled = false;
			else
				document.all['btnImport'].disabled = true;
			
		}
		
		function upBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-up.GIF)';
		}
		
		function downBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-down.GIF)';
		}		
		
		function clearValue()
		{
			if(document.all['txtFilePath'].innerText == '') {
				document.location.reload();
			}
		}
		
		function setOnOff()
		{
			//if(document.all['btnImport'].visible==false)
			var btnImp = document.getElementById("<%= btnImport_Show.ClientID %>");
			if(btnImp.visible == true)
			document.all['divBrowse'].style.visibility = 'visible';
			else
			document.all['divBrowse'].style.visibility = 'hidden';
		}
		
		function isNotSpecialCharacter() 
		{
			
			var returnValue = false;
			var keyCode = (window.event.which) ? window.event.which : window.event.keyCode;
			if ((keyCode == 37) || (keyCode == 42) ||  (keyCode == 91) ||  (keyCode == 93) ||  (keyCode == 38) ||  (keyCode == 33) ||  (keyCode == 64) ||  (keyCode == 35) ||  (keyCode == 36) ||  (keyCode == 94) ||  (keyCode == 40) ||  (keyCode == 41) ||  (keyCode == 95) ||  (keyCode == 43) || (keyCode == 34) || (keyCode == 58) || (keyCode == 59) || (keyCode == 39))
			{
				returnValue = true;
			}

			if ( window.event.returnValue )
			window.event.returnValue = returnValue;
			return returnValue;
		   
		}
		
		function testShowKeyCode()
		{
			var keyCode = (window.event.which) ? window.event.which : window.event.keyCode;
			alert(keyCode);
		}
	

		
		</script>
	</HEAD>
	<body onload="setOnOff();" onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="BaseZoneRates" method="post" encType="multipart/form-data" runat="server">
			<table id="xxx" style="Z-INDEX: 104; LEFT: 38px; WIDTH: 686px; POSITION: absolute; TOP: 14px; HEIGHT: 80px" width="686" border="0" runat="server">
				<tr>
					<td><asp:label id="lblTitle" runat="server" CssClass="mainTitleSize" Height="26px" Width="477px">Base Rates - By Weight</asp:label></td>
				</tr>
				<tr id="trQuery" runat="server">
					<td><asp:button id="btnQuery" runat="server" CssClass="queryButton" Width="60px" Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExecuteQuery" runat="server" CssClass="queryButton" Width="139px" Text="Execute Query" CausesValidation="False"></asp:button><asp:button id="btnInsert" runat="server" CssClass="queryButton" Width="68px" Text="Insert" CausesValidation="False"></asp:button>&nbsp;<asp:button id="btnImport_Show" runat="server" CssClass="queryButton" Width="140px" Text="Import Base Rates" CausesValidation="False"></asp:button>
					</td>
				</tr>
				<tr id="trImport" runat="server">
					<td><asp:button id="btnImport_Cancel" runat="server" CssClass="queryButton" Width="62px" Text="Cancel"></asp:button>&nbsp;
						<div onmouseup="upBrowse();" onmousedown="downBrowse();" id="divBrowse" style="DISPLAY: inline; BACKGROUND-IMAGE: url(images/btn-browse-up.GIF); WIDTH: 70px; HEIGHT: 21px"><INPUT id="inFile" onblur="setValue();" style="BORDER-RIGHT: #88a0c8 1px outset; BORDER-TOP: #88a0c8 1px outset; FONT-SIZE: 11px; FILTER: alpha(opacity: 0); BORDER-LEFT: #88a0c8 1px outset; WIDTH: 0px; COLOR: #003068; BORDER-BOTTOM: #88a0c8 1px outset; FONT-FAMILY: Arial; BACKGROUND-COLOR: #e9edf0; TEXT-DECORATION: none" onfocus="setValue();" type="file" name="inFile" runat="server"></div>
						<asp:button id="btnImport" runat="server" CssClass="queryButton" Width="62px" Text="Import" CausesValidation="False" Enabled="False"></asp:button><asp:button id="btnImport_Save" runat="server" CssClass="queryButton" Width="62px" Text="Save" Enabled="False"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:textbox id="txtFilePath" runat="server" CssClass="textField" Width="240px"></asp:textbox></td>
				</tr>
			</table>
			<asp:datagrid id="dgBZRates" style="Z-INDEX: 101; LEFT: 33px; POSITION: absolute; TOP: 134px" runat="server" Width="751px" AllowPaging="True" OnEditCommand="dgBZRates_Edit" OnCancelCommand="dgBZRates_Cancel" OnUpdateCommand="dgBZRates_Update" OnDeleteCommand="dgBZRates_Delete" OnItemDataBound="dgBZRates_Bound" OnPageIndexChanged="dgBZRates_PageChange" OnItemCommand="dgBZRates_Button" AutoGenerateColumns="False" ItemStyle-Height="20" PageSize="300">
				<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:TemplateColumn>
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							&nbsp;&nbsp;
							<asp:ImageButton CommandName="Select" id="imgSelect" runat="server" ImageUrl="images/butt-select.gif" Enabled="False"></asp:ImageButton>&nbsp;&nbsp;
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Origin Zone Code">
						<HeaderStyle Font-Bold="True" Width="90px" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblOZCode" Text='<%#DataBinder.Eval(Container.DataItem,"origin_zone_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtOZCode" Text='<%#DataBinder.Eval(Container.DataItem,"origin_zone_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" onclick="return false;" onpaste="return false;" onkeydown="return false;" onkeypress="return false;">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="ozcValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtOZCode" ErrorMessage="Origin Zone Code "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="OriginSearch">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Destination Zone Code">
						<HeaderStyle Font-Bold="True" Width="90px" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblDZCode" Text='<%#DataBinder.Eval(Container.DataItem,"destination_zone_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtDZCode" Text='<%#DataBinder.Eval(Container.DataItem,"destination_zone_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" onclick="return false;" onpaste="return false;" onkeydown="return false;" onkeypress="return false;">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="odcValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtDZCode" ErrorMessage="Destination Zone Code "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="DestinationSearch">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Service Type">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblServiceCode" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtServiceCode" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" onclick="return false;" onpaste="return false;" onkeydown="return false;" onkeypress="return false;">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="ScValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtServiceCode" ErrorMessage="Service Type "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="ServiceCodeSearch">
						<HeaderStyle CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Start WT">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblStartWt" Text='<%#DataBinder.Eval(Container.DataItem,"start_wt","{0:#0}")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtStartWt" Text='<%#DataBinder.Eval(Container.DataItem,"start_wt")%>' Runat="server" MaxLength="6" TextMaskType="msNumeric" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="5" NumberScale="1" onpaste="return false;">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="rfTxtStartWt" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtStartWt" ErrorMessage="Start WT "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="End WT">
						<HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblEndWt" Text='<%#DataBinder.Eval(Container.DataItem,"end_wt","{0:#0}")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtEndWt" Text='<%#DataBinder.Eval(Container.DataItem,"end_wt")%>' Runat="server" MaxLength="6" TextMaskType="msNumeric" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="5" NumberScale="1" onpaste="return false;">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="rfTxtEndWt" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtEndWt" ErrorMessage="End WT "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Start Price">
						<HeaderStyle Font-Bold="True" Width="90px" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblSPrice" Text='<%#DataBinder.Eval(Container.DataItem,"start_price","{0:n}")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtSPrice" Text='<%#DataBinder.Eval(Container.DataItem,"start_price","{0:n}")%>' Runat="server" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2" onpaste="return false;">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="rfTxtSPrice" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtSPrice" ErrorMessage="Start Price "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Increment Price">
						<HeaderStyle Font-Bold="True" Width="90px" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblIPrice" Text='<%#DataBinder.Eval(Container.DataItem,"increment_price","{0:n}")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtIPrice" Text='<%#DataBinder.Eval(Container.DataItem,"increment_price","{0:n}")%>' Runat="server" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2" onpaste="return false;">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="rfTxtIPrice" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtIPrice" ErrorMessage="Increment Price "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Effective Date">
						<HeaderStyle Font-Bold="True" Width="90px" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField" HorizontalAlign="Right"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblEffectivedate" Text='<%#DataBinder.Eval(Container.DataItem,"effective_date","{0:dd/MM/yyyy}")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox ID="txtEffectiveDate" MaxLength="10" TextMaskString="99/99/9999" TextMaskType="msDate" Text='<%#DataBinder.Eval(Container.DataItem,"effective_date","{0:dd/MM/yyyy}")%>' Runat="server">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="rfTxtEffectiveDate" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtEffectiveDate" ErrorMessage="Effective Date "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle VerticalAlign="Top" NextPageText="Next" Height="40px" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:label id="lblErrorMessage" style="Z-INDEX: 102; LEFT: 37px; POSITION: absolute; TOP: 97px" runat="server" CssClass="errorMsgColor" Height="35px" Width="540px">Place holder for err msg</asp:label><asp:validationsummary id="ValidationSummary1" style="Z-INDEX: 103; LEFT: 72px; POSITION: absolute; TOP: 103px" Height="36px" Width="346px" ShowSummary="False" DisplayMode="BulletList" ShowMessageBox="True" HeaderText="The following field(s) are required:" Runat="server"></asp:validationsummary></form>
	</body>
</HTML>
