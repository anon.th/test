<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="SearchJobsChequeRequisition.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.SearchJobsChequeRequisition" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Search Customs Jobs to Add to a Cheque Requisition</title>
		<meta content="True" name="vs_snapToGrid">
		<meta content="False" name="vs_showGrid">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script>function funcDisBack()
{history.go(+1);}
		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 1020px; HEIGHT: 800px" id="divMain"
				MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; LEFT: 0px" id="MainTable" border="0" width="100%"
					runat="server">
					<TBODY>
						<tr>
							<td colSpan="2" align="left"><asp:label id="Label1" runat="server" Width="600px" Height="27px" CssClass="mainTitleSize">
							Search Customs Jobs to Add to a Cheque Requisition</asp:label></td>
						</tr>
						<tr>
							<td style="HEIGHT: 26px" vAlign="top" colSpan="2" align="left">
								<asp:button id="btnQry" runat="server" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button>
								<label style="WIDTH:5px"></label>
								<asp:button id="btnExecQry" runat="server" CssClass="queryButton" Text="Execute Query" CausesValidation="False"></asp:button>
								<label style="WIDTH:5px"></label><label style="WIDTH:5px"></label><label style="WIDTH:5px">
								</label><label style="WIDTH:5px"></label>
							</td>
						</tr>
						<tr>
							<td vAlign="top" colSpan="2" align="left">
								<TABLE style="Z-INDEX: 112; WIDTH: 1000px" id="Table1" border="0" width="730" runat="server">
									<TR width="100%">
										<TD width="100%">
											<table border="0" cellSpacing="1" cellPadding="1" width="800">
												<TBODY>
													<TR>
														<TD style="HEIGHT: 19px" colSpan="6"><asp:label id="lblError" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red"></asp:label></TD>
													</TR>
													<TR>
														<TD colSpan="6">
															<fieldset style="WIDTH: 423px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="20px" Font-Bold="True">Dates</asp:label></legend>
																<TABLE id="tblDates" style="WIDTH: 656px; HEIGHT: 113px" cellSpacing="0" cellPadding="0"
																	align="left" border="0" runat="server">
																	<TR>
																		<td></td>
																		<TD colSpan="3">&nbsp;
																			<asp:radiobutton id="rbBookingDate" runat="server" CssClass="tableRadioButton" Text="Info Received"
																				Width="102px" Checked="True" GroupName="QueryByDate" AutoPostBack="True" Height="22px"></asp:radiobutton><asp:radiobutton id="rbEstDate" runat="server" CssClass="tableRadioButton" Text="Entry Compiled"
																				Width="111px" GroupName="QueryByDate" AutoPostBack="True" Height="22px"></asp:radiobutton>
																			<asp:radiobutton id="rbActDate" runat="server" CssClass="tableRadioButton" Text="Expected Arrival"
																				Width="123px" GroupName="QueryByDate" AutoPostBack="True" Height="22px"></asp:radiobutton>
																			<asp:radiobutton id="Radiobutton1" runat="server" CssClass="tableRadioButton" Text="Invoiced" GroupName="QueryByDate"
																				AutoPostBack="True" Height="22px" Width="75px"></asp:radiobutton>
																			<asp:radiobutton id="Radiobutton2" runat="server" CssClass="tableRadioButton" Text="Manifested" GroupName="QueryByDate"
																				AutoPostBack="True" Height="22px" Width="85px"></asp:radiobutton>
																			<asp:radiobutton id="Radiobutton3" runat="server" CssClass="tableRadioButton" Text="Delivered" GroupName="QueryByDate"
																				AutoPostBack="True" Height="22px" Width="75px"></asp:radiobutton>
																		</TD>
																	</TR>
																	<TR>
																		<td></td>
																		<TD colSpan="3">&nbsp;
																			<asp:radiobutton id="rbMonth" runat="server" CssClass="tableRadioButton" Text="Month" Width="73px"
																				Checked="True" GroupName="EnterDate" AutoPostBack="True" Height="21px"></asp:radiobutton>&nbsp;
																			<asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Width="88px" Height="19px"></asp:dropdownlist>&nbsp;
																			<asp:label id="lblYear" runat="server" CssClass="tableLabel" Width="30px" Height="10px">Year</asp:label>&nbsp;
																			<cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" NumberPrecision="4"
																				NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></TD>
																	</TR>
																	<TR>
																		<td style="HEIGHT: 20px"></td>
																		<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
																			<asp:radiobutton id="rbPeriod" runat="server" CssClass="tableRadioButton" Text="Period" Width="62px"
																				GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
																			<cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" TextMaskType="msDate"
																				MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
																			&nbsp;
																			<cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" TextMaskType="msDate"
																				MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
																	</TR>
																	<TR>
																		<td></td>
																		<TD colSpan="3">&nbsp;
																			<asp:radiobutton id="rbDate" runat="server" CssClass="tableRadioButton" Text="Date" Width="74px"
																				GroupName="EnterDate" AutoPostBack="True" Height="22px"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="82" TextMaskType="msDate"
																				MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
																	</TR>
																</TABLE>
															</fieldset>
														</TD>
													</TR>
													<tr>
														<td style="WIDTH: 250px"><asp:label id="Label4" runat="server" CssClass="tableLabel"> Job Entry Number:</asp:label></td>
														<td style="WIDTH: 200px">
															<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtAWBNumber" tabIndex="15" runat="server"
																CssClass="textField" Width="150px" MaxLength="100"></asp:textbox></td>
														<td><asp:label id="Label5" runat="server" CssClass="tableLabel"> House AWB Number:</asp:label></td>
														<td colSpan="3"><FONT face="Tahoma">
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Textbox1" tabIndex="15" runat="server"
																	CssClass="textField" Width="150px" MaxLength="100"></asp:textbox></FONT></td>
													</tr>
													<tr>
														<td>
															<asp:label style="Z-INDEX: 0" id="Label6" runat="server" CssClass="tableLabel">MAWB Number:</asp:label></td>
														<td colspan="5"><FONT face="Tahoma">
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Textbox2" tabIndex="15" runat="server"
																	CssClass="textField" Width="150px" MaxLength="100"></asp:textbox>&nbsp;
																<asp:CheckBox style="Z-INDEX: 0; MARGIN-LEFT: 1px" id="CheckBox1" runat="server" CssClass="tableLabel"
																	Text="Search MAWB in Job Entry" Width="188px"></asp:CheckBox></FONT></td>
													</tr>
													<tr>
														<td><asp:label style="Z-INDEX: 0" id="Label31" runat="server" CssClass="tableLabel"> Customer ID:</asp:label></td>
														<td style="WIDTH: 235px">
															<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Textbox3" tabIndex="15" runat="server"
																CssClass="textField" Width="150px" MaxLength="100"></asp:textbox></td>
														<td>
															<asp:label style="Z-INDEX: 0" id="Label11" runat="server" CssClass="tableLabel">Customer Group:</asp:label></td>
														<td><FONT face="Tahoma">
																<asp:dropdownlist style="Z-INDEX: 0" id="Dropdownlist3" tabIndex="31" runat="server" Width="150px"></asp:dropdownlist></FONT></td>
													</tr>
													<TR>
														<TD>
															<asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel">Customs Warehouse:</asp:label></TD>
														<TD style="WIDTH: 235px">
															<asp:dropdownlist style="Z-INDEX: 0" id="Dropdownlist2" tabIndex="31" runat="server" Width="150px"></asp:dropdownlist></TD>
														<TD>
															<asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel">Discharge Port:</asp:label></TD>
														<TD>
															<asp:dropdownlist style="Z-INDEX: 0" id="Dropdownlist1" tabIndex="31" runat="server" Width="150px"></asp:dropdownlist></TD>
													</TR>
													<tr>
														<td><FONT face="Tahoma">
																<asp:label style="Z-INDEX: 0" id="lblHCSupportedbyEnterprise" runat="server" CssClass="tableLabel"> Job Status: </asp:label></FONT></td>
														<td colspan="5"><FONT face="Tahoma">
																<asp:dropdownlist style="Z-INDEX: 0" id="Dropdownlist4" tabIndex="31" runat="server" Width="150px"></asp:dropdownlist>
																<asp:CheckBox style="Z-INDEX: 0; MARGIN-LEFT: 6px" id="CheckBox2" runat="server" CssClass="tableLabel"
																	Text="Search on Status not Achieved" Width="200px"></asp:CheckBox></FONT></td>
													</tr>
													<TR>
														<TD style="HEIGHT: 19px">
															<asp:label style="Z-INDEX: 0" id="Label7" runat="server" CssClass="tableLabel">Entry Type:</asp:label></TD>
														<TD style="HEIGHT: 19px" colSpan="5">
															<asp:dropdownlist style="Z-INDEX: 0" id="Dropdownlist5" tabIndex="31" runat="server" Width="150px"></asp:dropdownlist></TD>
													</TR>
													<tr>
														<td><asp:label style="Z-INDEX: 0" id="Label32" runat="server" CssClass="tableLabel"> Ship/Flight No:</asp:label></td>
														<td style="WIDTH: 235px"><FONT face="Tahoma">
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Textbox4" tabIndex="15" runat="server"
																	CssClass="textField" Width="150px" MaxLength="100"></asp:textbox></FONT></td>
														<td><FONT face="Tahoma">
																<asp:label style="Z-INDEX: 0" id="Label8" runat="server" CssClass="tableLabel">Exporter Name:</asp:label></FONT></td>
														<td colspan="3" vAlign="middle"><FONT face="Tahoma">
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Textbox7" tabIndex="15" runat="server"
																	CssClass="textField" Width="150px" MaxLength="100"></asp:textbox></FONT></td>
													</tr>
													<tr>
														<td><asp:label style="Z-INDEX: 0" id="Label33" runat="server" CssClass="tableLabel">Folio Number:</asp:label></td>
														<td style="WIDTH: 235px">
															<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Textbox5" tabIndex="15" runat="server"
																CssClass="textField" Width="150px" MaxLength="100"></asp:textbox></td>
														<td><FONT face="Tahoma">
																<asp:label style="Z-INDEX: 0" id="Label12" runat="server" CssClass="tableLabel">Cheque Req. No: </asp:label></FONT></td>
														<td colspan="3" vAlign="middle"><FONT face="Tahoma">
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Textbox8" tabIndex="15" runat="server"
																	CssClass="textField" Width="150px" MaxLength="100"></asp:textbox></FONT></td>
													</tr>
													<tr>
														<td><asp:label style="Z-INDEX: 0" id="Label34" runat="server" CssClass="tableLabel"> Customs Profile:</asp:label></td>
														<td style="WIDTH: 235px">
															<asp:dropdownlist style="Z-INDEX: 0" id="Dropdownlist6" tabIndex="31" runat="server" Width="150px"></asp:dropdownlist></td>
														<td><FONT face="Tahoma">
																<asp:label style="Z-INDEX: 0" id="Label13" runat="server" CssClass="tableLabel"> Customs Receipt No: </asp:label></FONT></td>
														<td colspan="3" vAlign="middle"><FONT face="Tahoma">
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Textbox9" tabIndex="15" runat="server"
																	CssClass="textField" Width="150px" MaxLength="100"></asp:textbox></FONT></td>
													</tr>
													<TR>
														<TD>
															<asp:label style="Z-INDEX: 0" id="Label9" runat="server" CssClass="tableLabel">Customs Lodgment (Declaration) No:</asp:label></TD>
														<TD style="WIDTH: 235px">
															<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Textbox6" tabIndex="15" runat="server"
																CssClass="textField" Width="150px" MaxLength="100"></asp:textbox></TD>
														<TD></TD>
														<TD vAlign="middle" colSpan="3"></TD>
													</TR>
													<TR>
														<TD></TD>
														<TD style="WIDTH: 235px"><FONT face="Tahoma"></FONT></TD>
														<TD></TD>
														<TD vAlign="middle" colSpan="3"></TD>
													</TR>
													<TR>
														<TD vAlign="middle" colSpan="6">
															<asp:button style="Z-INDEX: 0" id="Button1" runat="server" CssClass="queryButton" CausesValidation="False"
																Text="Add to Cheque Req"></asp:button>
														</TD>
													</TR>
													<tr>
														<td colSpan="6"><FONT face="Tahoma"></FONT><br>
														</td>
													</tr>
													<tr class="gridHeading">
														<td colSpan="6"><STRONG><FONT size="2">Search Results</FONT></STRONG></td>
													</tr>
													<tr>
														<td colSpan="6">
															<asp:datagrid style="Z-INDEX: 0" id="PackageDetails" tabIndex="42" runat="server" Width="1000px"
																FooterStyle-CssClass="gridHeading" BorderWidth="0px" CellSpacing="1" CellPadding="0" HeaderStyle-CssClass="gridHeading"
																ItemStyle-CssClass="gridField" AlternatingItemStyle-CssClass="gridField" 
																AutoGenerateColumns="False" AllowPaging="True" PageSize="6">
																<FooterStyle CssClass="gridHeading"></FooterStyle>
																<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
																<ItemStyle CssClass="gridField"></ItemStyle>
																<HeaderStyle CssClass="gridHeading"></HeaderStyle>
																<Columns>
																	<asp:TemplateColumn HeaderText="Select">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:CheckBox Runat="server" ID="chk"></asp:CheckBox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Invoice No.">
																		<HeaderStyle HorizontalAlign="Center" Width="150px"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem,"Invoice") %>
																		</ItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Client">
																		<HeaderStyle HorizontalAlign="Center" Width="200px"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem,"Client") %>
																		</ItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Duty/Tax">
																		<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem,"DutyTax") %>
																		</ItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="EPF">
																		<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem,"EPF") %>
																		</ItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Total">
																		<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem,"Total") %>
																		</ItemTemplate>
																		<FooterStyle HorizontalAlign="Center"></FooterStyle>
																	</asp:TemplateColumn>
																</Columns>
																<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
															</asp:datagrid></td>
													</tr>
												</TBODY>
											</table>
										</TD>
									</TR>
								</TABLE>
								<DIV></DIV>
							</td>
						</tr>
					</TBODY>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
