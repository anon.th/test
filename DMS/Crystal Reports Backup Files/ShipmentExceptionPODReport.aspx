<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="ShipmentExceptionPODReport.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.ShipmentExceptionPODReport" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentExceptionPODReport</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="ShipmentExceptionPODReport" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 100; LEFT: 19px; POSITION: absolute; TOP: 49px" runat="server" Width="64px" Text="Query"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 20px; POSITION: absolute; TOP: 80px" runat="server" Width="556px" Height="30px" CssClass="errorMsgColor">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnGenerate" style="Z-INDEX: 101; LEFT: 83px; POSITION: absolute; TOP: 49px" runat="server" Width="82px" Text="Generate"></asp:button>
			<TABLE id="tblShipmentExcepPODRepQry" style="Z-INDEX: 104; LEFT: 15px; WIDTH: 484px; POSITION: absolute; TOP: 115px; HEIGHT: 456px" width="484" border="0" runat="server">
				<TBODY>
					<TR width="100%">
						<TD width="100%" vAlign="top">
							<TABLE id="tblDates" style="LEFT: 1px; TOP: 2px" width="100%" align="left" border="0">
								<TR height="14">
									<TD class="tableHeading" colSpan="20">Retrieval Basis</TD>
								</TR>
								<TR height="14">
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
									<TD width="5%"></TD>
								</TR>
								<TR height="27">
									<TD colSpan="4"><asp:radiobutton id="rbMonth" runat="server" Width="100%" Text="Monthly" Height="22px" CssClass="tableRadioButton" Checked="True" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton></TD>
									<TD colSpan="4"><asp:dropdownlist id="ddMonth" runat="server" Width="100%" CssClass="gridDropDown"></asp:dropdownlist></TD>
									<TD align="right" colSpan="3"><asp:label id="lblYear" runat="server" Width="100%" Height="22px" CssClass="tableLabel">Year&nbsp;&nbsp;</asp:label></TD>
									<TD colSpan="4"><cc1:mstextbox id="txtYear" runat="server" Width="100%" CssClass="textField" NumberPrecision="4" NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></TD>
									<TD colSpan="5"></TD>
								</TR>
								<TR height="27">
									<TD colSpan="4"><asp:radiobutton id="rbPeriod" runat="server" Width="100%" Text="Period" Height="22px" CssClass="tableRadioButton" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton></TD>
									<TD colSpan="4"><cc1:mstextbox id="txtPeriod" runat="server" Width="100%" CssClass="textField" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD align="right" colSpan="3"><asp:label id="lblTo" runat="server" Width="100%" Height="22px" CssClass="tableLabel">To&nbsp;&nbsp;</asp:label></TD>
									<TD colSpan="4"><cc1:mstextbox id="txtTo" runat="server" Width="100%" CssClass="textField" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD colSpan="5"></TD>
								</TR>
								<TR height="27">
									<TD colSpan="4"><asp:radiobutton id="rbDate" runat="server" Width="100%" Text="Daily" Height="22px" CssClass="tableRadioButton" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton></TD>
									<TD colSpan="4"><cc1:mstextbox id="txtDate" runat="server" Width="100%" CssClass="textField" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD colSpan="12"></TD>
								</TR>
								<TR height="8">
									<TD colSpan="20"></TD>
								</TR>
								<TR height="14">
									<TD class="tableHeading" colSpan="20">Payer Details</TD>
								</TR>
								<TR height="14">
									<TD colSpan="20"></TD>
								</TR>
								<TR height="27">
									<TD colSpan="4"><asp:label id="lblPayer" runat="server" Width="100%" Height="22px" CssClass="tableLabel">Payer</asp:label></TD>
									<TD colSpan="4"><cc1:mstextbox id="txtPayer" runat="server" Width="100%" Height="22px" CssClass="textField" AutoPostBack="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12"></cc1:mstextbox></TD>
									<TD colSpan="5" align="right"><asp:label id="lblPayerAc" runat="server" Width="100%" Height="22px" CssClass="tableLabel">Payer Account&nbsp;&nbsp;</asp:label></TD>
									<TD colSpan="5"><cc1:mstextbox id="txtPayerAc" runat="server" Width="100%" Height="22px" CssClass="textField" AutoPostBack="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12"></cc1:mstextbox></TD>
									<TD colSpan="2"></TD>
								</TR>
								<TR height="8">
									<TD colSpan="20"></TD>
								</TR>
								<TR height="14">
									<TD class="tableHeading" colSpan="20">Origin/Destination</TD>
								</TR>
								<TR height="14">
									<TD colSpan="20"></TD>
								</TR>
								<TR height="27">
									<TD colSpan="4"><asp:label id="lblOrigin" runat="server" Width="100%" Height="22px" CssClass="tableLabel">Origin</asp:label></TD>
									<TD colSpan="4"><cc1:mstextbox id="txtOrigin" runat="server" Width="100%" Height="22px" CssClass="textField" AutoPostBack="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="30"></cc1:mstextbox></TD>
									<TD colSpan="5" align="right"><asp:label id="lblDestination" runat="server" Width="100%" Height="22px" CssClass="tableLabel">Destination&nbsp;&nbsp;</asp:label></TD>
									<TD colSpan="5"><cc1:mstextbox id="txtDestination" runat="server" Width="100%" Height="22px" CssClass="textField" AutoPostBack="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="30"></cc1:mstextbox></TD>
									<TD colSpan="2"></TD>
								</TR>
								<TR height="8">
									<TD colSpan="20"></TD>
								</TR>
								<TR height="14">
									<TD class="tableHeading" colSpan="20">Sender/Recipient</TD>
								</TR>
								<TR height="14">
									<TD colSpan="20"></TD>
								</TR>
								<TR height="27">
									<TD colSpan="4"><asp:radiobutton id="rbSender" runat="server" Width="100%" Text="Sender" Height="22px" CssClass="tableRadioButton" Checked="True" GroupName="QueryByType" AutoPostBack="True"></asp:radiobutton></TD>
									<TD colSpan="4"><asp:radiobutton id="rbRecipent" runat="server" Width="100%" Text="Recipent" Height="22px" CssClass="tableRadioButton" GroupName="QueryByType" AutoPostBack="True"></asp:radiobutton></TD>
									<TD colSpan="12"></TD>
								</TR>
								<TR height="27">
									<TD colSpan="4" align="right"><asp:label id="lblArea" runat="server" Width="100%" Height="22px" CssClass="tableLabel">Area&nbsp;&nbsp;</asp:label></TD>
									<TD colSpan="4"><cc1:mstextbox id="txtArea" runat="server" Width="100%" Height="22px" CssClass="textField" AutoPostBack="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="30"></cc1:mstextbox></TD>
									<TD colSpan="4" align="right"><asp:label id="lblRegion" runat="server" Width="100%" Height="22px" CssClass="tableLabel">Region&nbsp;&nbsp;</asp:label></TD>
									<TD colSpan="4"><cc1:mstextbox id="txtRegion" runat="server" Width="100%" Height="22px" CssClass="textField" AutoPostBack="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="30"></cc1:mstextbox></TD>
									<TD colSpan="4"></TD>
								</TR>
								<TR height="14">
									<TD colSpan="20"></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</TBODY>
			</TABLE>
			<asp:label id="lblTitle" style="Z-INDEX: 103; LEFT: 20px; POSITION: absolute; TOP: 9px" runat="server" Width="558px" Height="32px" CssClass="maintitleSize">Shipment Exception Upon POD Report</asp:label></form>
	</body>
</HTML>
