<%@ Page language="c#" Codebehind="CustomsMasterAWBXMLVerification.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsMasterAWBXMLVerification" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CustomsMasterAWBXMLVerification</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
		function funcDisBack(){
			history.go(+1);
		}

        function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function RemoveBadMasterAWBNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^-_/_a-zA-Z0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteMasterAWBNumber(obj) {
            setTimeout(function () {
                RemoveBadMasterAWBNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function validate(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		function ValidateMasterAWBNumber(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[-_/_a-zA-Z0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		function callback()
        {
				var btn = document.getElementById('btnClientEvent');
				if(btn != null)
				{					
					btn.click();
				}else{
					alert('error call code behind');
				}				        
			
        }
        
        function WeightKeyPress()
        {
			var TotalWeight = document.getElementById('TotalWeight');        
			var txtTotalWeightDesc = document.getElementById('txtTotalWeightDesc');
			txtTotalWeightDesc.value = TotalWeight.value;
        }
        
		function AWBsKeyPress()
        {
			var TotalAWBs = document.getElementById('TotalAWBs');        
			var txtTotalAWBsDesc = document.getElementById('txtTotalAWBsDesc');
			txtTotalAWBsDesc.value = TotalAWBs.value;        
        }
		</script>
		<style type="text/css">
			TABLE.GrdAWBXMLVerification TH { PADDING-LEFT: 5px }
		</style>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onclick="GetScrollPosition();" onload="SetScrollPosition();">
		<form id="CustomsMasterAWBXMLVerification" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 1523px; HEIGHT: 1543px; TOP: 34px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tbody>
						<tr>
							<td colSpan="2" align="left"><asp:label id="Label1" runat="server" Width="421px" Height="27px" CssClass="mainTitleSize">
								Master AWB XML Verification</asp:label></td>
						</tr>
						<tr>
							<td colspan="2">
								<asp:label style="Z-INDEX: 0" id="Label37" runat="server" CssClass="tableLabel">Master AWB:</asp:label><asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea; MARGIN-LEFT: 50px" id="txtMasterAWBNo"
									tabIndex="14" runat="server" Width="114px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td vAlign="top" colSpan="2" align="left">
								<asp:button id="btnAccept" runat="server" CssClass="queryButton" Text="Accept" CausesValidation="False"></asp:button>&nbsp;<asp:button id="btnClose" runat="server" CssClass="queryButton" Text="Close" CausesValidation="False"></asp:button>
							</td>
						</tr>
						<tr>
							<td colSpan="2" style="HEIGHT: 21px">
								<asp:label id="lblError" runat="server" ForeColor="Red" Font-Size="X-Small" Font-Bold="True"></asp:label>
							</td>
						</tr>
						<tr class="gridHeading">
							<td colSpan="2"><STRONG><FONT size="2">Validation Details</FONT></STRONG></td>
						</tr>
						<tr>
							<td colSpan="2">
								<asp:datagrid style="Z-INDEX: 0" id="GrdAWBXMLVerification" tabIndex="16" runat="server" Width="100%"
									AutoGenerateColumns="False" ShowFooter="True" DataKeyField="HouseAWBNo" AlternatingItemStyle-CssClass="gridField"
									ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridHeading" CellPadding="0" CellSpacing="1"
									BorderWidth="0px" FooterStyle-CssClass="gridHeading">
									<FooterStyle CssClass="gridHeading"></FooterStyle>
									<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn HeaderText="Source">
											<HeaderStyle HorizontalAlign="Center" Width="105px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center" Height="19px"></ItemStyle>
											<ItemTemplate>
												<asp:label id="lblSource" Text='<%# DataBinder.Eval(Container.DataItem,"Source") %>' runat="server">
												</asp:label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="House AWB Number">
											<HeaderStyle HorizontalAlign="Center" Width="105px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:label id="lblHouseAWBNo" runat="server">
													<%# DataBinder.Eval(Container.DataItem,"HouseAWBNo") %>
												</asp:label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Job Entry Number">
											<HeaderStyle HorizontalAlign="Center" Width="105px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:label id="lblJobEntryNo" runat="server">
													<%# DataBinder.Eval(Container.DataItem,"JobEntryNo") %>
												</asp:label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Ship/Flight Number">
											<HeaderStyle HorizontalAlign="Center" Width="105px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:label id="lblShipFlightNo" runat="server">
													<%# DataBinder.Eval(Container.DataItem,"ShipFlightNo") %>
												</asp:label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Departure City">
											<HeaderStyle HorizontalAlign="Center" Width="53px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:label id="lblDepartureCity" runat="server">
													<%# DataBinder.Eval(Container.DataItem,"DepartureCity") %>
												</asp:label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Departure Country">
											<HeaderStyle HorizontalAlign="Center" Width="105px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:label id="lblDepartureCountry" runat="server">
													<%# DataBinder.Eval(Container.DataItem,"DepartureCountry") %>
												</asp:label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Arrival Date">
											<HeaderStyle HorizontalAlign="Center" Width="105px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:label id="lblArrivalDate" runat="server"></asp:label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Transport Mode">
											<HeaderStyle HorizontalAlign="Center" Width="105px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:label id="lblTransportMode" runat="server">
													<%# DataBinder.Eval(Container.DataItem,"TransportMode") %>
												</asp:label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Customer ID">
											<HeaderStyle HorizontalAlign="Center" Width="105px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:label id="lblCustomerID" runat="server">
													<%# DataBinder.Eval(Container.DataItem,"CustomerID") %>
												</asp:label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Exporter Name">
											<HeaderStyle HorizontalAlign="Left" Width="105px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<asp:label id="lblExporterName" runat="server">
													<%# DataBinder.Eval(Container.DataItem,"ExporterName") %>
												</asp:label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Exporter Address">
											<HeaderStyle HorizontalAlign="Left" Width="105px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<asp:label id="lblExporterAddress1" runat="server">
													<%# DataBinder.Eval(Container.DataItem,"ExporterAddress1") %>
												</asp:label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Exporter Address 2">
											<HeaderStyle HorizontalAlign="Left" Width="105px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<asp:label id="lblExporterAddress2" runat="server">
													<%# DataBinder.Eval(Container.DataItem,"ExporterAddress2") %>
												</asp:label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Exporter Country">
											<HeaderStyle HorizontalAlign="Center" Width="105px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:label id="lblExporterCountry" runat="server">
													<%# DataBinder.Eval(Container.DataItem,"ExporterCountry") %>
												</asp:label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Goods Description">
											<HeaderStyle HorizontalAlign="Left" Width="105px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<asp:label id="lblGoodsDescription" runat="server">
													<%# DataBinder.Eval(Container.DataItem,"GoodsDescription") %>
												</asp:label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Package Quantity">
											<HeaderStyle HorizontalAlign="Right" Width="53px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<asp:label id="lblPkgQty" runat="server">
													<%# DataBinder.Eval(Container.DataItem,"PkgQty") %>
												</asp:label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Weight">
											<HeaderStyle HorizontalAlign="Right" Width="53px"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<asp:label id="lblWeight" runat="server">
													<%# DataBinder.Eval(Container.DataItem,"Weight") %>
												</asp:label>
												<asp:Label ID="lblShipFlightNo_Color" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem,"ShipFlightNo_Color") %>'>
												</asp:Label>
												<asp:Label ID="lblDepartureCity_Color" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem,"DepartureCity_Color") %>'>
												</asp:Label>
												<asp:Label ID="lblDepartureCountry_Color" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem,"DepartureCountry_Color") %>'>
												</asp:Label>
												<asp:Label ID="lblArrivalDate_Color" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem,"ArrivalDate_Color") %>'>
												</asp:Label>
												<asp:Label ID="lblTransportMode_Color" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem,"TransportMode_Color") %>'>
												</asp:Label>
												<asp:Label ID="lblCustomerID_Color" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem,"CustomerID_Color") %>'>
												</asp:Label>
												<asp:Label ID="lblExporterName_Color" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem,"ExporterName_Color") %>'>
												</asp:Label>
												<asp:Label ID="lblExporterAddress1_Color" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem,"ExporterAddress1_Color") %>'>
												</asp:Label>
												<asp:Label ID="lblExporterAddress2_Color" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem,"ExporterAddress2_Color") %>'>
												</asp:Label>
												<asp:Label ID="lblExporterCountry_Color" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem,"ExporterCountry_Color") %>'>
												</asp:Label>
												<asp:Label ID="lblGoodsDescription_Color" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem,"GoodsDescription_Color") %>'>
												</asp:Label>
												<asp:Label ID="lblPkgQty_Color" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem,"PkgQty_Color") %>'>
												</asp:Label>
												<asp:Label ID="lblWeight_Color" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem,"Weight_Color") %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
								</asp:datagrid>
							</td>
						</tr>
					</tbody>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
