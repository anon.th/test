<%@ Page language="c#" Codebehind="AgentConveyance.aspx.cs" AutoEventWireup="false" Inherits="com.ties.AgentConveyance" smartNavigation="false" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AgentConveyance</title>
		<LINK rev="stylesheet" href="css/styles.css" rel="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="AgentConveyance" method="post" runat="server">
			<asp:label id="lblTitle" style="Z-INDEX: 104; LEFT: 24px; POSITION: absolute; TOP: 8px" runat="server"
				CssClass="mainTitleSize" Height="32px" Width="336px">Agent Conveyance</asp:label><dbcombo:dbcombo id=dbCmbAgentId style="Z-INDEX: 115; LEFT: 112px; POSITION: absolute; TOP: 112px" tabIndex=1 runat="server" Height="17px" Width="140px" RegistrationKey='<%#System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"]%>' ReQueryOnLoad="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentIdServerMethod" TextBoxColumns="20" AutoPostBack="True"></dbcombo:dbcombo><dbcombo:dbcombo id=dbCmbAgentName style="Z-INDEX: 114; LEFT: 488px; POSITION: absolute; TOP: 112px" tabIndex=2 runat="server" Height="17px" Width="176px" RegistrationKey='<%#System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"]%>' ReQueryOnLoad="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentNameServerMethod" TextBoxColumns="25" AutoPostBack="True"></dbcombo:dbcombo><asp:button id="btnGoToFirstPage" style="Z-INDEX: 113; LEFT: 608px; POSITION: absolute; TOP: 41px"
				tabIndex="5" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text="|<"></asp:button><asp:button id="btnPreviousPage" style="Z-INDEX: 106; LEFT: 632px; POSITION: absolute; TOP: 41px"
				tabIndex="6" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text="<"></asp:button><asp:textbox id="txtCountRec" style="Z-INDEX: 110; LEFT: 656px; POSITION: absolute; TOP: 41px"
				tabIndex="7" runat="server" Height="19px" Width="24px"></asp:textbox><asp:button id="btnNextPage" style="Z-INDEX: 107; LEFT: 680px; POSITION: absolute; TOP: 41px"
				tabIndex="8" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text=">"></asp:button><asp:button id="btnGoToLastPage" style="Z-INDEX: 109; LEFT: 704px; POSITION: absolute; TOP: 41px"
				tabIndex="9" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text=">|"></asp:button><asp:label id="lblAgentName" style="Z-INDEX: 111; LEFT: 384px; POSITION: absolute; TOP: 112px"
				runat="server" CssClass="tableLabel" Width="100px">Agent Name</asp:label><asp:label id="lblAgentCode" style="Z-INDEX: 108; LEFT: 16px; POSITION: absolute; TOP: 112px"
				runat="server" CssClass="tableLabel" Width="100px">Agent Code</asp:label><asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 22px; POSITION: absolute; TOP: 41px" runat="server"
				CssClass="queryButton" Width="62px" CausesValidation="False" Text="Query"></asp:button><asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 84px; POSITION: absolute; TOP: 41px"
				runat="server" CssClass="queryButton" Width="130px" CausesValidation="False" Text="Execute Query"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 103; LEFT: 24px; POSITION: absolute; TOP: 72px"
				runat="server" CssClass="errorMsgColor" Width="720px">Error Message</asp:label><asp:requiredfieldvalidator id="reqAgentId" ErrorMessage="Agent Id " ControlToValidate="dbCmbAgentId" Display="None"
				BorderWidth="0" Runat="server"></asp:requiredfieldvalidator><asp:validationsummary id="PageValidationSummary" style="Z-INDEX: 105; LEFT: 606px; POSITION: absolute; TOP: 5px"
				runat="server" HeaderText="Please enter the following mandatory field(s):<br><br>" DisplayMode="BulletList" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary>
			<table id="tblMain" style="Z-INDEX: 112; LEFT: 16px; WIDTH: 720px; POSITION: absolute; TOP: 144px; HEIGHT: 301px"
				width="720" runat="server">
				<TR>
					<TD style="HEIGHT: 297px" vAlign="top" colSpan="19"><asp:datagrid id="dgConveyance" runat="server" Width="704px" OnPageIndexChanged="dgConveyance_PageChange"
							OnUpdateCommand="dgConveyance_Update" OnDeleteCommand="dgConveyance_Delete" OnItemDataBound="dgConveyance_ItemDataBound" OnCancelCommand="dgConveyance_Cancel"
							OnEditCommand="dgConveyance_Edit" HorizontalAlign="Left" AutoGenerateColumns="False" AllowPaging="True" AllowCustomPaging="True" OnItemCommand="dgConvey_Button">
							<ItemStyle HorizontalAlign="Left" CssClass="gridfield"></ItemStyle>
							<Columns>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
									CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
									<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:EditCommandColumn>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
									<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Conveyance Code">
									<HeaderStyle Font-Bold="True" HorizontalAlign="Left" Width="20%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label ID="lblConveyance" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"conveyance_code")%>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBox" ID="txtConveyance" Text='<%#DataBinder.Eval(Container.DataItem,"conveyance_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" >
										</cc1:mstextbox>
										<asp:RequiredFieldValidator ID="cConveyance" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtConveyance"
											ErrorMessage="Conveyance Code "></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton"
									CommandName="ConveyanceSearch" Visible="true">
									<HeaderStyle CssClass="gridHeading" Width="5%"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Conveyance Description">
									<HeaderStyle Width="20%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label ID="lblCCDescp" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"conveyance_description")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox ID="txtCCDescp" CssClass="gridTextBox" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"conveyance_description")%>' MaxLength="200">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Length(cm)">
									<HeaderStyle HorizontalAlign="Left" Width="10%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label ID="lblLength" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Length","{0:#0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBox" ID="txtLength" Text='<%#DataBinder.Eval(Container.DataItem,"Length")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" >
										</cc1:mstextbox>
										<asp:RequiredFieldValidator ID="reqLength" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtLength"
											ErrorMessage="Length "></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Breadth(cm)">
									<HeaderStyle HorizontalAlign="Left" Width="10%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label ID="lblBreadth" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Breadth","{0:#0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBox" ID="txtBreadth" Text='<%#DataBinder.Eval(Container.DataItem,"Breadth")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" >
										</cc1:mstextbox>
										<asp:RequiredFieldValidator ID="reqBreadth" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtBreadth"
											ErrorMessage="Breadth "></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Height(cm)">
									<HeaderStyle HorizontalAlign="Left" Width="10%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label ID="lblHeight" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Height","{0:#0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBox" ID="txtHeight" Text='<%#DataBinder.Eval(Container.DataItem,"Height")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" >
										</cc1:mstextbox>
										<asp:RequiredFieldValidator ID="reqHeight" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtHeight"
											ErrorMessage="Height "></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Max Weight(Kg)">
									<HeaderStyle HorizontalAlign="Left" Width="10%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label ID="lblWeight" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"max_wt","{0:#0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBox" ID="txtWeight" Text='<%#DataBinder.Eval(Container.DataItem,"max_wt")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" >
										</cc1:mstextbox>
										<asp:RequiredFieldValidator ID="reqWeight" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtWeight"
											ErrorMessage="Weight "></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
						</asp:datagrid></TD>
					<td style="HEIGHT: 297px" vAlign="top" align="left" colSpan="1"></td>
				</TR>
			</table>
			<INPUT type=hidden value="<%=strScrollPosition%>" name="ScrollPosition"><asp:button id="btnInsert" style="Z-INDEX: 116; LEFT: 215px; POSITION: absolute; TOP: 41px"
				runat="server" CssClass="queryButton" Width="80px" CausesValidation="False" Text="Insert"></asp:button>
		</form>
	</body>
</HTML>
