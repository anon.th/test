<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="ServiceZipcodeExcluded.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ServiceZipcodeExcluded" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ServiceZipcodeExcluded</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<FORM id="ServiceZipcodeExcluded" method="post" runat="server">
			<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 8px" id="Table1" border="0"
				cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD></TD>
				</TR>
				<TR>
					<TD><FONT face="Tahoma"><asp:label id="lblTitle" runat="server" Height="26px" CssClass="mainTitleSize" Width="447px">Service</asp:label></FONT></TD>
				</TR>
				<TR>
					<TD>
						<asp:button id="btnDelete" runat="server" Height="20" CssClass="queryButton" Width="62px" Visible="False"
							CausesValidation="False" Text="Delete"></asp:button>
						<asp:button id="btnQry" runat="server" Height="20" CssClass="queryButton" Width="62px" CausesValidation="False"
							Text="Query"></asp:button>
						<asp:button id="btnExecQry" runat="server" Height="20" CssClass="queryButton" Width="130px"
							CausesValidation="False" Text="Execute Query" Enabled="False"></asp:button>
						<asp:button id="btnInsert" runat="server" Height="20" CssClass="queryButton" Width="62px" CausesValidation="False"
							Text="Insert"></asp:button>
						<asp:button id="btnSave" runat="server" Height="20px" CssClass="queryButton" Width="66px" Visible="False"
							CausesValidation="True" Text="save"></asp:button>
					</TD>
				</TR>
				<TR>
					<TD><asp:label id="lblSCMessage" runat="server" Height="28px" CssClass="errorMsgColor" Width="624px"></asp:label></TD>
				</TR>
				<TR>
					<TD><FONT face="Tahoma">
							<asp:datagrid id="dgServiceCodes" runat="server" Width="888px" ItemStyle-Height="30px" AutoGenerateColumns="False"
								OnPageIndexChanged="dgServiceCodes_PageChange" OnItemDataBound="dgServiceCodes_Bound" OnEditCommand="dgServiceCodes_Edit"
								OnCancelCommand="dgServiceCodes_Cancel" OnUpdateCommand="dgServiceCodes_Update" OnSelectedIndexChanged="dgServiceCodes_SelectedIndexChanged"
								AllowPaging="True" PageSize="5" AllowCustomPaging="True" OnDeleteCommand="dgServiceCodes_Delete"
								SelectedItemStyle-CssClass="gridFieldSelected">
								<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
								<ItemStyle Height="25px" CssClass="gridField"></ItemStyle>
								<Columns>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;" CommandName="Select">
										<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:ButtonColumn>
									<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
										CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
										<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left"></ItemStyle>
									</asp:EditCommandColumn>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
										<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:ButtonColumn>
									<asp:TemplateColumn HeaderText="Service Code">
										<HeaderStyle Font-Bold="True" Width="19%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label id=lblServiceCode Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' CssClass="gridLabel" Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox id=txtServiceCode Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' CssClass="gridTextBox" Enabled="True" Runat="server" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
											</cc1:mstextbox>
											<asp:RequiredFieldValidator id="scValidator" Runat="server" ErrorMessage="Service Code" ControlToValidate="txtServiceCode"
												Display="None" BorderWidth="0"></asp:RequiredFieldValidator>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Service Description">
										<HeaderStyle Width="23%" CssClass="gridHeading"></HeaderStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblServiceDescription" Text='<%#DataBinder.Eval(Container.DataItem,"service_description")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" ID="txtServiceDescription" Text='<%#DataBinder.Eval(Container.DataItem,"service_description")%>' Runat="server" MaxLength="200">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Order By">
										<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label id=Label1 CssClass="gridLabelNumber" Text='<%#DataBinder.Eval(Container.DataItem,"service_charge_amt","{0:#0;-#0}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox id=txtServiceAmt CssClass="gridTextBoxNumber" Text='<%#DataBinder.Eval(Container.DataItem,"service_charge_amt","{0:#0;-#0}")%>' Enabled="True" Runat="server">
											</asp:TextBox>
											<asp:RangeValidator id="Rangevalidator1" Runat="server" Display="None" ControlToValidate="txtServiceAmt"
												ErrorMessage="Pls enter valid  value eg 10000 or -10000" Type="Double" MinimumValue="-10000" MaximumValue="10000"
												BorderWidth="0"></asp:RangeValidator>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Commit Time">
										<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="lblCommitTime" Text='<%#DataBinder.Eval(Container.DataItem,"commit_time","{0:HH:mm}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtCommitTime" Text='<%#DataBinder.Eval(Container.DataItem,"commit_time","{0:HH:mm}")%>' Runat="server" Enabled="True" TextMaskType="msTime" TextMaskString="99:99">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Transit Day">
										<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="lblTransitDay" Text='<%#DataBinder.Eval(Container.DataItem,"transit_day","{0:#0}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtTransitDay" Text='<%#DataBinder.Eval(Container.DataItem,"transit_day")%>' Runat="server" Enabled="True" MaxLength="12" TextMaskType="msNumeric" NumberMaxValue="60" NumberMinValue="0" NumberPrecision="2" NumberScale="0">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Transit Hour">
										<HeaderStyle Width="23%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label id=lblTransitHour Width="30px" CssClass="gridLabelNumber" Text='<%#DataBinder.Eval(Container.DataItem,"transit_hour","{0:#0}")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox id=txtTransitHour Enabled="True" CssClass="gridTextBoxNumber" Text='<%#DataBinder.Eval(Container.DataItem,"transit_hour")%>' Runat="server" TextMaskType="msNumeric" MaxLength="12" NumberScale="0" NumberPrecision="1" NumberMinValue="0" NumberMaxValue="9">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Auto Manifest">
										<HeaderStyle Width="23%" CssClass="gridHeading"></HeaderStyle>
										<ItemTemplate>
											<asp:DropDownList id="ddlAutoManifest" runat="server" CssClass="gridTextBox"></asp:DropDownList>
											<asp:RequiredFieldValidator id="reqAutoMnifest" runat="server" ErrorMessage="AutoManifest" ControlToValidate="ddlAutoManifest"
												Display="None"></asp:RequiredFieldValidator>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Tax Incl.&#160; in Rates">
										<HeaderStyle Width="60px" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False" Width="60px"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList id="ddlTaxInRates" runat="server" Width="100%" CssClass="gridTextBox"></asp:DropDownList>
											<asp:RequiredFieldValidator id="reqTaxInRates" runat="server" Display="None" ControlToValidate="ddlTaxInRates"
												ErrorMessage="Tax Incl. in Rates"></asp:RequiredFieldValidator>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Prepaid&#160;">
										<HeaderStyle Width="23%" CssClass="gridHeading"></HeaderStyle>
										<ItemTemplate>
											<asp:DropDownList id="ddlServiceIsPrepaid" runat="server" CssClass="gridTextBox"></asp:DropDownList>
											<asp:RequiredFieldValidator id="reqServiceIsPrepaid" runat="server" ErrorMessage="Prepaid" ControlToValidate="ddlServiceIsPrepaid"
												Display="None"></asp:RequiredFieldValidator>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
							</asp:datagrid></FONT></TD>
				</TR>
				<TR>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:button id="btnZSEInsert" runat="server" Height="20" CssClass="queryButton" Width="62px"
							CausesValidation="False" Text="Insert" Enabled="False"></asp:button><asp:button id="btnZSEInsertMultiple" runat="server" Height="20" CssClass="queryButton" Width="96px"
							Visible="False" CausesValidation="False" Text="Insert Multiple" Enabled="False"></asp:button></TD>
				</TR>
				<TR>
					<TD><FONT face="Tahoma"><asp:label id="lblZSCEMessage" runat="server" Height="31px" CssClass="errorMsgColor" Width="609px"></asp:label></FONT></TD>
				</TR>
				<TR>
					<TD>
						<asp:Panel ID="pnlSecondaryGrid" runat="server">
							<FIELDSET style="WIDTH: 720px; HEIGHT: 178px"><LEGEND>
									<asp:label id="Label2" runat="server" CssClass="tableField">Service Unavailable Postal Codes</asp:label></LEGEND>
								<asp:datagrid id="dgZipcodeServiceExcluded" runat="server" Width="711px" OnDeleteCommand="dgZipcodeServiceExcluded_Delete"
									AllowCustomPaging="True" PageSize="5" AllowPaging="True" OnUpdateCommand="dgZipcodeServiceExcluded_Update"
									OnCancelCommand="dgZipcodeServiceExcluded_Cancel" OnEditCommand="dgZipcodeServiceExcluded_Edit"
									OnItemDataBound="dgZipcodeServiceExcluded_Bound" OnPageIndexChanged="dgZipcodeServiceExcluded_PageChange"
									AutoGenerateColumns="False" ItemStyle-Height="20" OnItemCommand="dgZipcodeServiceExcluded_Button">
									<ItemStyle Height="20px"></ItemStyle>
									<Columns>
										<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
											CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;"
											Visible="False">
											<HeaderStyle Width="4%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
										</asp:EditCommandColumn>
										<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
											<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
										</asp:ButtonColumn>
										<asp:TemplateColumn HeaderText="Postal Code">
											<HeaderStyle Font-Bold="True" Width="30%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabel" ID="lblZipcode" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Runat="server">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<cc1:mstextbox CssClass="gridTextBox" ID="txtZipcode" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msUpperAlfaNumericWithUnderscore">
												</cc1:mstextbox>
												<asp:RequiredFieldValidator ID="exValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtZipcode"
													ErrorMessage="Zipcode is required field"></asp:RequiredFieldValidator>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="search"
											Visible="False">
											<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
										</asp:ButtonColumn>
										<asp:TemplateColumn HeaderText="State">
											<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabel" ID="lblState" Text='<%#DataBinder.Eval(Container.DataItem,"state_name")%>' Runat="server">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:TextBox CssClass="gridTextBox" ID="txtState" Text='<%#DataBinder.Eval(Container.DataItem,"state_name")%>' Runat="server" ReadOnly="true">
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Country">
											<HeaderStyle Width="20%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabel" ID="lblCountry" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:TextBox CssClass="gridTextBox" ID="txtCountry" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server" ReadOnly="true">
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
								</asp:datagrid></FIELDSET>
						</asp:Panel>
					</TD>
				</TR>
			</TABLE>
			<asp:validationsummary id="ValidationSummary1" Height="33px" Width="364px" HeaderText="Please enter the missing fields."
				Runat="server" ShowMessageBox="True" ShowSummary="False"></asp:validationsummary></FORM>
	</body>
</HTML>
