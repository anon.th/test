<%@ Page language="c#" Codebehind="RevenueBySalesIDRptQuery.aspx.cs" AutoEventWireup="false" Inherits="com.ties.RevenueBySalesIDRptQuery" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Revenue By SalesmanID Report</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="RevenueBySalesIDRptQuery" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 100; LEFT: 20px; POSITION: absolute; TOP: 40px" runat="server"
				Text="Query" CssClass="queryButton" Width="64px"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 24px; POSITION: absolute; TOP: 69px"
				runat="server" CssClass="errorMsgColor" Width="700px" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 101; LEFT: 90px; POSITION: absolute; TOP: 40px"
				runat="server" Text="Execute Query" CssClass="queryButton" Width="123px"></asp:button>
			<TABLE id="tblInternal" style="Z-INDEX: 104; LEFT: 21px; WIDTH: 800px; POSITION: absolute; TOP: 90px"
				width="800" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 401px; HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 390px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="20px" Font-Bold="True">Dates</asp:label></legend>
							<TABLE id="tblDates" style="WIDTH: 360px; HEIGHT: 91px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<td style="HEIGHT: 22px"></td>
									<TD style="HEIGHT: 22px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbBookingDate" runat="server" Text="Booking Date" CssClass="tableRadioButton"
											Width="115px" Height="22px" Checked="True" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbPickUpDate" runat="server" Text="Actual Pickup Date" CssClass="tableRadioButton"
											Width="180px" Height="22px" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton></TD>
									<td style="HEIGHT: 22px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Text="Month" CssClass="tableRadioButton" Width="73px"
											Height="21px" Checked="True" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Width="88px" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" CssClass="tableLabel" Width="30px" Height="22px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" NumberPrecision="4"
											NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></TD>
									<td></td>
								</TR>
								<TR>
									<td style="HEIGHT: 20px"></td>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Text="Period" CssClass="tableRadioButton" Width="62px"
											GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="HEIGHT: 20px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Text="Date" CssClass="tableRadioButton" Width="74px"
											Height="22px" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="82" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
						<FIELDSET style="WIDTH: 390px; HEIGHT: 81px"><LEGEND><asp:label id="Label15" runat="server" CssClass="tableHeadingFieldset" Width="48px" Font-Bold="True">Pickup</asp:label></LEGEND>
							<TABLE id="Table3" style="WIDTH: 360px; HEIGHT: 95px" cellSpacing="0" cellPadding="0" align="left"
								border="0" runat="server">
								<TR>
									<TD class="tableLabel" align="left" width="20%">&nbsp;
										<asp:label id="Label11" runat="server" CssClass="tableLabel" Width="111px" Height="22px">Route Code</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="dbComboRoutePath" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											TextBoxColumns="18" TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboPickupPathCodeSelect"
											Runat="server" RegistrationKey=" "></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel" align="left">&nbsp;
										<asp:label id="Label10" runat="server" CssClass="tableLabel" Width="84px" Height="22px">Postal Code</asp:label></TD>
									<TD><cc1:mstextbox id="txtRouteZipcode" runat="server" CssClass="textField" Width="139px" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="10"></cc1:mstextbox>&nbsp;
										<asp:button id="btnRouteZipCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
								</TR>
								<TR>
									<TD class="tableLabel" align="left">&nbsp;<asp:label id="Label16" runat="server" CssClass="tableLabel" Width="100px" Height="22px">State / Province</asp:label></TD>
									<TD><cc1:mstextbox id="txtRouteStateCode" runat="server" CssClass="textField" Width="139" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="10"></cc1:mstextbox>&nbsp;
										<asp:button id="btnRouteStateCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
					<TD vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 196px"><legend><asp:label id="lblPayerType" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Payer Type</asp:label></legend>
							<TABLE id="tblPayerType" style="WIDTH: 300px; HEIGHT: 73px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<tr>
									<td></td>
									<td colSpan="2" height="1">&nbsp;</td>
									<td></td>
									<td></td>
								</tr>
								<TR>
									<td></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="Label4" runat="server" CssClass="tableLabel" Width="79px" Height="22px">Payer Type</asp:label></TD>
									<td><asp:listbox id="lsbCustType" runat="server" Width="137px" Height="61px" AutoPostBack="True"
											SelectionMode="Multiple"></asp:listbox></td>
									<td></td>
								</TR>
								<TR height="33">
									<td bgColor="blue"></td>
									<TD class="tableLabel" vAlign="middle" colSpan="2">&nbsp;
										<asp:label id="lblPayerCode" runat="server" CssClass="tableLabel" Width="79px" Height="22px">Payer Code</asp:label>&nbsp;&nbsp;</TD>
									<td><asp:textbox id="txtPayerCode" runat="server" CssClass="textField" Width="148px"></asp:textbox>&nbsp;&nbsp;
										<asp:button id="btnPayerCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></td>
									<td></td>
								</TR>
								<TR height="33">
									<td bgColor="blue"></td>
									<TD class="tableLabel" vAlign="middle" colSpan="2">&nbsp;
										<asp:label id="Label7" runat="server" CssClass="tableLabel" Width="86px" Height="22px">Discount Band</asp:label>&nbsp;&nbsp;</TD>
									<td><dbcombo:dbcombo id="DbComboDiscountBand" tabIndex="1" runat="server" Width="80px" AutoPostBack="True"
											TextBoxColumns="4" TextUpLevelSearchButton="F" ShowDbComboLink="False" ServerMethod="DiscountBandServerMethod"
											Debug="false"></dbcombo:dbcombo></td>
									<td></td>
								</TR>
								<TR>
									<TD vAlign="middle" bgColor="#0000ff"></TD>
									<TD class="tableLabel" vAlign="top" colSpan="2"><FONT face="Tahoma">&nbsp;
											<asp:label id="Label9" runat="server" CssClass="tableLabel" Width="95px" Height="22px">Master Account</asp:label></FONT></TD>
									<TD><dbcombo:dbcombo id="DbComboMasterAccount" tabIndex="1" runat="server" Width="80px" AutoPostBack="True"
											TextBoxColumns="4" TextUpLevelSearchButton="F" ShowDbComboLink="False" ServerMethod="MasterAccountServerMethod"
											Debug="false"></dbcombo:dbcombo></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</fieldset>
						<FIELDSET style="WIDTH: 320px; HEIGHT: 50px"><LEGEND>
								<asp:label id="Label13" runat="server" Width="111px" CssClass="tableHeadingFieldset" Font-Bold="True">Report Type</asp:label></LEGEND>
							<TABLE id="Table4" style="WIDTH: 312px" cellSpacing="0" cellPadding="0" align="left" border="0"
								runat="server">
								<TR>
									<TD>&nbsp;
										<asp:label id="Label12" runat="server" Width="100px" CssClass="tableLabel">Type</asp:label></TD>
									<TD class="tableLabel" style="WIDTH: 197px" colSpan="3">
										<asp:RadioButtonList id="rbRptType" runat="server" Width="176px" Font-Size="X-Small" RepeatColumns="2">
											<asp:ListItem Value="Summary" Selected="True">Summary</asp:ListItem>
											<asp:ListItem Value="Detail">Detail</asp:ListItem>
										</asp:RadioButtonList></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 401px" vAlign="top">
						<fieldset style="WIDTH: 390px; HEIGHT: 120px"><legend><asp:label id="lblRouteType" runat="server" CssClass="tableHeadingFieldset" Width="145px" Font-Bold="True">Route / DC Selection</asp:label></legend>
							<TABLE id="tblRouteType" style="WIDTH: 360px; HEIGHT: 47px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<TD class="tableLabel" colSpan="2">&nbsp;
										<asp:radiobutton id="rbLongRoute" runat="server" Text="Linehaul" CssClass="tableRadioButton" Width="106px"
											Height="22px" Checked="True" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbShortRoute" runat="server" Text="Delivery Route" CssClass="tableRadioButton"
											Width="122px" Height="22px" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbAirRoute" runat="server" Text="Air Route" CssClass="tableRadioButton" Width="120px"
											Height="22px" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton></TD>
								</TR>
								<tr>
									<TD class="tableLabel" width="20%">&nbsp;<asp:label id="lblRouteCode" runat="server" CssClass="tableLabel" Width="111px" Height="22px">Route Code</asp:label></TD>
									<td><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											TextBoxColumns="18" TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboPathCodeSelect"
											Runat="server" RegistrationKey=" "></DBCOMBO:DBCOMBO></td>
								</tr>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label2" runat="server" CssClass="tableLabel" Width="154px" Height="22px">Origin Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											TextBoxColumns="18" TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelect"
											Runat="server" RegistrationKey=" "></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label3" runat="server" CssClass="tableLabel" Width="181px" Height="22px">Destination Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											TextBoxColumns="18" TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelect"
											Runat="server" RegistrationKey=" "></DBCOMBO:DBCOMBO></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 121px"><legend><asp:label id="lblDestination" runat="server" CssClass="tableHeadingFieldset" Width="79px"
									Font-Bold="True">Destination</asp:label></legend>
							<TABLE id="tblDestination" style="WIDTH: 300px; HEIGHT: 57px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<tr height="37">
									<td style="HEIGHT: 33px"></td>
									<TD class="tableLabel" style="WIDTH: 360px; HEIGHT: 33px" colSpan="3">&nbsp;
										<asp:label id="lblZipCode" runat="server" CssClass="tableLabel" Width="84px" Height="22px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCode" runat="server" CssClass="textField" Width="139px" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td style="HEIGHT: 33px"></td>
								</tr>
								<tr>
									<td style="HEIGHT: 33px"></td>
									<TD class="tableLabel" style="WIDTH: 360px; HEIGHT: 33px" colSpan="3">&nbsp;
										<asp:label id="lblStateCode" runat="server" CssClass="tableLabel" Width="100px" Height="22px">State / Province</asp:label><cc1:mstextbox id="txtStateCode" runat="server" CssClass="textField" Width="139" Height="22" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td style="HEIGHT: 33px"></td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 401px" vAlign="top">
						<fieldset style="WIDTH: 390px; HEIGHT: 50px"><legend><asp:label id="Label5" runat="server" CssClass="tableHeadingFieldset" Width="111px" Font-Bold="True">Salesman ID</asp:label></legend>
							<TABLE id="Table1" style="WIDTH: 347px" cellSpacing="0" cellPadding="0" align="left" border="0"
								runat="server">
								<tr>
									<td>&nbsp;
										<asp:label id="Label6" runat="server" CssClass="tableLabel" Width="163px">Salesman ID</asp:label></td>
									<TD class="tableLabel" style="WIDTH: 197px" colSpan="3">&nbsp;&nbsp;
										<DBCOMBO:DBCOMBO id="DbSalesman" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											TextBoxColumns="18" TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="SalesmanQuery"
											Runat="server" RegistrationKey=" "></DBCOMBO:DBCOMBO></TD>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="WIDTH: 400px" vAlign="top">
						<FIELDSET style="WIDTH: 320px; HEIGHT: 50px"><LEGEND><asp:label id="Label8" runat="server" CssClass="tableHeadingFieldset" Width="111px" Font-Bold="True">Service Type</asp:label></LEGEND>
							<TABLE id="Table2" style="WIDTH: 312px" cellSpacing="0" cellPadding="0" align="left" border="0"
								runat="server">
								<TR>
									<TD>&nbsp;
										<asp:label id="lblServiceCode" runat="server" CssClass="tableLabel" Width="100px">Service Code</asp:label></TD>
									<TD class="tableLabel" style="WIDTH: 197px" colSpan="3"><dbcombo:dbcombo id="dbCmbServiceCode" tabIndex="1" runat="server" Width="80px" AutoPostBack="True"
											TextBoxColumns="4" TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="ServiceCodeServerMethod"></dbcombo:dbcombo></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
			</TABLE>
			<asp:label id="Label1" style="Z-INDEX: 102; LEFT: 20px; POSITION: absolute; TOP: 4px" runat="server"
				CssClass="maintitleSize" Width="365px" Height="27px">Revenue By Salesman ID Report</asp:label></TR></TABLE></TR></TABLE><input 
style="Z-INDEX: 106; LEFT: 222px; POSITION: absolute; TOP: 35px" type=hidden 
value="<%=strScrollPosition%>" name=ScrollPosition>
		</form>
	</body>
</HTML>
