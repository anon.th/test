<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="ServiceFailure.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ServiceFailure" SmartNavigation="False" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ServiceFailure</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
				<script language="javascript">
			function makeUppercase(objId)
			{
				document.getElementById(objId).value = document.getElementById(objId).value.toUpperCase();
			}
		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="ServiceFailure" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 102; LEFT: 16px; POSITION: absolute; TOP: 53px" tabIndex="100" runat="server" Text="Query" Width="64px" CssClass="queryButton"></asp:button>&nbsp;&nbsp;
			<asp:label id="lblErrorMessage" style="Z-INDEX: 104; LEFT: 16px; POSITION: absolute; TOP: 80px" runat="server" Width="616px" CssClass="errorMsgColor" Height="30px">Place holder for err msg</asp:label><asp:label id="lblTitle" style="Z-INDEX: 103; LEFT: 15px; POSITION: absolute; TOP: 16px" runat="server" Width="558px" CssClass="maintitleSize" Height="32px">Service Exception Upon POD Report</asp:label><asp:button id="btnGenerate" style="Z-INDEX: 101; LEFT: 81px; POSITION: absolute; TOP: 53px" runat="server" Text="Generate" Width="82px" CssClass="queryButton"></asp:button>
			<TABLE id="tblShipmentTracking" style="Z-INDEX: 104; LEFT: 13px; WIDTH: 883px; POSITION: absolute; TOP: 102px; HEIGHT: 337px" width="883" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 464px; HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 450px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" Width="178px" CssClass="tableHeadingFieldset" Font-Bold="True">Estimated (Delivery) Date</asp:label></legend>
							<TABLE id="tblDates" style="WIDTH: 360px; HEIGHT: 91px" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Text="Month" Width="73px" CssClass="tableRadioButton" Height="21px" AutoPostBack="True" GroupName="EnterDate" Checked="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" Width="88px" CssClass="textField" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" Width="30px" CssClass="tableLabel" Height="22px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" Width="83" CssClass="textField" MaxLength="5" TextMaskType="msNumeric" NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
									<td></td>
								</TR>
								<TR>
									<td style="HEIGHT: 20px"></td>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Text="Period" Width="62px" CssClass="tableRadioButton" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" Width="83" CssClass="textField" MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="HEIGHT: 20px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Text="Date" Width="74px" CssClass="tableRadioButton" Height="22px" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 105px"><legend><asp:label id="lblPayerType" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Payer Type</asp:label></legend>
							<TABLE id="tblPayerType" style="WIDTH: 300px; HEIGHT: 73px" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<tr>
									<td></td>
									<td colSpan="2" height="1">&nbsp;</td>
									<td></td>
									<td></td>
								</tr>
								<TR>
									<td></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="Label4" runat="server" Width="87px" CssClass="tableLabel" Height="22px">Payer Type</asp:label></TD>
									<td><asp:listbox id="lsbCustType" runat="server" Width="137px" Height="61px" SelectionMode="Multiple"></asp:listbox></td>
									<td></td>
								</TR>
								<TR height="33">
									<td bgColor="blue"></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="lblPayerCode" runat="server" Width="79px" CssClass="tableLabel" Height="22px">Payer Code</asp:label>&nbsp;&nbsp;</TD>
									<td><asp:textbox id="txtPayerCode" runat="server" Width="148px" CssClass="textField"></asp:textbox>&nbsp;&nbsp;
										<asp:button id="btnPayerCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></td>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 464px; HEIGHT: 134px" vAlign="top">
						<fieldset style="WIDTH: 449px; HEIGHT: 127px"><legend><asp:label id="lblRouteType" runat="server" Width="145px" CssClass="tableHeadingFieldset" Font-Bold="True">Route / DC Selection</asp:label></legend>
							<TABLE id="tblRouteType" style="WIDTH: 439px; HEIGHT: 113px" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<TR>
									<TD class="tableLabel" style="WIDTH: 541px" colSpan="2">&nbsp;
										<asp:radiobutton id="rbLongRoute" runat="server" Text="Linehaul" Width="138px" CssClass="tableRadioButton" Height="22px" AutoPostBack="True" GroupName="QueryByRoute" Checked="True"></asp:radiobutton><asp:radiobutton id="rbShortRoute" runat="server" Text="Delivery Route" Width="131px" CssClass="tableRadioButton" Height="22px" AutoPostBack="True" GroupName="QueryByRoute"></asp:radiobutton><asp:radiobutton id="rbAirRoute" runat="server" Text="Air Route" Width="152px" CssClass="tableRadioButton" Height="22px" AutoPostBack="True" GroupName="QueryByRoute"></asp:radiobutton></TD>
								</TR>
								<tr>
									<TD class="tableLabel" width="20%">&nbsp;<asp:label id="lblRouteCode" runat="server" Width="111px" CssClass="tableLabel" Height="22px">Route Code</asp:label></TD>
									<td style="WIDTH: 354px"><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True" Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></td>
								</tr>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label2" runat="server" Width="154px" CssClass="tableLabel" Height="22px">Origin Distribution Center</asp:label></TD>
									<TD style="WIDTH: 354px"><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True" Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelect"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label3" runat="server" Width="181px" CssClass="tableLabel" Height="22px">Destination Distribution Center</asp:label></TD>
									<TD style="WIDTH: 354px"><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" Width="25px" CssClass="tableLabel" AutoPostBack="True" Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelect"></DBCOMBO:DBCOMBO></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 134px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 89px"><legend><asp:label id="lblDestination" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Destination</asp:label></legend>
							<TABLE id="tblDestination" style="WIDTH: 300px; HEIGHT: 89px" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<tr height="37">
									<td></td>
									<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
										<asp:label id="lblZipCode" runat="server" Width="84px" CssClass="tableLabel" Height="22px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCode" runat="server" Width="139px" CssClass="textField" MaxLength="10" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
										<asp:label id="lblStateCode" runat="server" Width="100px" CssClass="tableLabel" Height="22px">State / Province</asp:label><cc1:mstextbox id="txtStateCode" runat="server" Width="139" CssClass="textField" Height="22" MaxLength="10" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" colSpan="2">
						<fieldset style="WIDTH: 725px; HEIGHT: 50px"><legend><asp:label id="Label1" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Shipment</asp:label></legend>
							<TABLE id="Table1" style="WIDTH: 700px" cellSpacing="0" cellPadding="0" border="0">
								<TR>
									<TD>&nbsp;<asp:label id="lblConsigNo" runat="server" CssClass="tableLabel">Consignment No</asp:label><FONT face="Tahoma">&nbsp;
										</FONT>
										<cc1:mstextbox id="txtConsigNo" tabIndex="14" runat="server" Width="108px" CssClass="textField"></cc1:mstextbox></TD>
									<TD><asp:label id="lblServiceCode" runat="server" CssClass="tableLabel">Service Code</asp:label><FONT face="Tahoma">&nbsp;
										</FONT>
										<dbcombo:dbcombo id="DbCmbServiceCode" tabIndex="15" runat="server" Width="54px" Height="24px" AutoPostBack="True" TextBoxColumns="14" TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="ServiceCodeServerMethod"></dbcombo:dbcombo></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
