<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="ManualRatingOverride.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ManualRatingOverride" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PostDeliveryCOD</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" type="text/javascript">
			function ExecuteQueryConNo()
			{
				var btn = document.getElementById('btnExecQry');
				if(btn != null)
				{
					btn.disabled = true;
					__doPostBack('ExecuteQuery','');
				}
			}
		</script>
	</HEAD>
	<BODY onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<FORM id="PostDeliveryCOD" method="post" runat="server">
			<INPUT style="Z-INDEX: 108; POSITION: absolute; DISPLAY: none; TOP: 48px; LEFT: 144px"
				id="ExecuteQuery" type="button" name="hddCon" runat="server" CausesValidation="False">
			<P><asp:label style="Z-INDEX: 102; POSITION: absolute; TOP: 8px; LEFT: 7px" id="lblMainTitle"
					runat="server" Width="477px" Height="26px" CssClass="mainTitleSize"> Manual Rating Override</asp:label></P>
			<DIV style="Z-INDEX: 101; POSITION: relative; WIDTH: 838px; HEIGHT: 668px; TOP: 46px; LEFT: 0px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; WIDTH: 726px; TOP: 3px; LEFT: 0px" id="MainTable"
					border="0" width="726" runat="server">
					<TR vAlign="top">
						<TD vAlign="top" width="738"><asp:button id="btnQry" tabIndex="1" runat="server" CausesValidation="False" Width="61px" CssClass="queryButton"
								Text="Query"></asp:button><INPUT style="WIDTH: 128px; HEIGHT: 20px" id="btnExecQry" class="queryButton" onclick="ExecuteQueryConNo();"
								value="Execute Query" type="button" name="ExecQury" runat="server" CausesValidation="False">
							<%--<asp:button id="btnExecQry" tabIndex="2" runat="server" CssClass="queryButton" Width="130px"
								CausesValidation="False" Text="Execute Query"></asp:button>--%>
							<asp:button id="btnSave" tabIndex="3" runat="server" Width="66px" CssClass="queryButton" Text="Save"
								Enabled="False"></asp:button><asp:button id="btnClearManual" tabIndex="4" runat="server" Width="155px" CssClass="queryButton"
								Text="Clear Manual Override"></asp:button>
							<DIV style="WIDTH: 166px; DISPLAY: inline; HEIGHT: 19px" ms_positioning="FlowLayout"></DIV>
							<asp:button id="btnGoToFirstPage" tabIndex="5" runat="server" CausesValidation="False" Width="24px"
								CssClass="queryButton" Text="|<"></asp:button><asp:button id="btnPreviousPage" tabIndex="6" runat="server" CausesValidation="False" Width="24px"
								CssClass="queryButton" Text="<"></asp:button><cc1:mstextbox id="txtGoToRec" tabIndex="7" runat="server" Width="28px" CssClass="textFieldRightAlign"
								Enabled="True" Wrap="False" AutoPostBack="True" NumberMaxValue="999999" NumberMinValue="1" NumberPrecision="8" NumberScale="0" TextMaskType="msNumeric"
								MaxLength="5"></cc1:mstextbox><asp:button id="btnNextPage" tabIndex="8" runat="server" CausesValidation="False" Width="24px"
								CssClass="queryButton" Text=">"></asp:button><asp:button id="btnGoToLastPage" tabIndex="9" runat="server" CausesValidation="False" Width="24px"
								CssClass="queryButton" Text=">|"></asp:button><asp:label id="lblErrorMsg" runat="server" Width="537px" Height="19px" CssClass="errorMsgColor"></asp:label><asp:label id="lblNumRec" runat="server" Width="187px" Height="19px" CssClass="RecMsg"></asp:label><asp:validationsummary id="PageValidationSummary" runat="server" Width="346px" Height="39px" ShowMessageBox="True"
								ShowSummary="False" Visible="True"></asp:validationsummary></TD>
					</TR>
					<TR>
						<TD width="726">
							<FIELDSET><LEGEND><asp:label id="Label7" CssClass="tableHeadingFieldset" Font-Size="11px" Runat="server">Consignment Query Criteria</asp:label></LEGEND>
								<TABLE id="TblBookDtl" border="0" width="737" runat="server">
									<TR height="25">
										<TD width="3%"></TD>
										<TD style="WIDTH: 120px" width="120"><asp:label id="lblConsgmtNo" runat="server" Width="113px" Height="15px" CssClass="tableLabel">Consignment No</asp:label></TD>
										<TD style="WIDTH: 156px" width="156" colSpan="2"><asp:textbox id="txtConsigNo" tabIndex="10" runat="server" Width="141px" CssClass="textField"
												MaxLength="20"></asp:textbox></TD>
										<TD style="WIDTH: 191px" width="191"><asp:label id="lblRefNo" runat="server" Width="147px" Height="15px" CssClass="tableLabel">Ref No</asp:label></TD>
										<TD style="WIDTH: 146px" width="146"><asp:textbox id="txtRefNo" tabIndex="11" runat="server" Width="173px" CssClass="textField" MaxLength="20"></asp:textbox></TD>
									</TR>
									<TR>
										<TD width="3%"></TD>
										<TD style="WIDTH: 120px" width="120"><asp:label style="Z-INDEX: 0" id="Label1" runat="server" Width="113px" Height="15px" CssClass="tableLabel">Consignment Type</asp:label></TD>
										<TD style="WIDTH: 156px" width="156" colSpan="2"><asp:dropdownlist style="Z-INDEX: 0" id="ddlConType" tabIndex="14" runat="server" Width="154px" Height="69px"
												AutoPostBack="True" DataValueField="custid" DataTextField="custid">
												<asp:ListItem Value="DOMESTIC">DOMESTIC</asp:ListItem>
												<asp:ListItem Value="EXPORT">EXPORT</asp:ListItem>
												<asp:ListItem Value="IMPORT">IMPORT</asp:ListItem>
											</asp:dropdownlist></TD>
										<TD style="WIDTH: 191px" width="191"><asp:label style="Z-INDEX: 0" id="lblRouteCode" runat="server" Height="15px" CssClass="tableLabel">Destination DC</asp:label></TD>
										<TD style="WIDTH: 146px" width="146"><DBCOMBO:DBCOMBO style="Z-INDEX: 0" id="DbComboDestinationDC" tabIndex="12" Width="25px" CssClass="tableLabel"
												AutoPostBack="True" Runat="server" ServerMethod="DbComboDistributionCenterSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v"
												TextBoxColumns="18"></DBCOMBO:DBCOMBO></TD>
									</TR>
									<TR>
										<TD width="3%"></TD>
										<TD style="WIDTH: 120px" width="120"><asp:label style="Z-INDEX: 0" id="Label2" runat="server" Width="113px" Height="15px" CssClass="tableLabel">MAWB Number</asp:label></TD>
										<TD style="WIDTH: 156px" width="156" colSpan="2"><asp:textbox style="Z-INDEX: 0" id="txtMAWBNo" tabIndex="10" runat="server" Width="141px" CssClass="textField"
												MaxLength="30"></asp:textbox></TD>
										<TD style="WIDTH: 191px" width="191"><asp:label style="Z-INDEX: 0" id="lblDelManifest" runat="server" Width="130px" Height="15px"
												CssClass="tableLabel">Delivery Route Code</asp:label></TD>
										<TD style="WIDTH: 146px" width="146"><DBCOMBO:DBCOMBO style="Z-INDEX: 0" id="DbComboPathCode" tabIndex="13" Width="25px" CssClass="tableLabel"
												AutoPostBack="True" Runat="server" ServerMethod="DbComboPathCodeSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v" TextBoxColumns="18"></DBCOMBO:DBCOMBO></TD>
									</TR>
									<TR>
										<TD width="3%"></TD>
										<TD style="WIDTH: 120px" width="120"><asp:label style="Z-INDEX: 0" id="lblCustID" runat="server" Width="95px" Height="15px" CssClass="tableLabel">Customer ID</asp:label></TD>
										<TD style="WIDTH: 156px" width="156" colSpan="2"><asp:dropdownlist style="Z-INDEX: 0" id="ddbCustomer" tabIndex="14" runat="server" Width="154px" Height="69px"
												AutoPostBack="True" DataValueField="custid" DataTextField="custid"></asp:dropdownlist></TD>
										<TD style="WIDTH: 191px" width="191" colSpan="2"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtCustomer" tabIndex="15" runat="server"
												Width="350px" CssClass="textField" Enabled="False" MaxLength="20"></asp:textbox></TD>
									</TR>
									<TR height="25">
										<TD style="HEIGHT: 25px" width="3%"></TD>
										<TD style="WIDTH: 120px; HEIGHT: 25px" width="120"><asp:label style="Z-INDEX: 0" id="Label3" runat="server" Width="128px" Height="15px" CssClass="tableLabel">Manifested Date   From</asp:label></TD>
										<TD style="WIDTH: 156px; HEIGHT: 25px" width="156" colSpan="2"><cc1:mstextbox style="Z-INDEX: 0" id="txtManifestDateFrom" tabIndex="16" runat="server" Width="141px"
												CssClass="textField" TextMaskType="msDate" MaxLength="16" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
										<TD style="WIDTH: 191px; HEIGHT: 25px" width="191"><asp:label style="Z-INDEX: 0" id="lblStatusCode" runat="server" Width="136px" Height="15px"
												CssClass="tableLabel">Actual POD Date From</asp:label></TD>
										<TD style="HEIGHT: 25px" width="55%" colSpan="2"><cc1:mstextbox style="Z-INDEX: 0" id="txtAct_pod_From" tabIndex="16" runat="server" Width="60%"
												CssClass="textField" TextMaskType="msDate" MaxLength="16" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" width="3%"></TD>
										<TD style="WIDTH: 140px; HEIGHT: 25px" width="120" align="right"><asp:label style="Z-INDEX: 0" id="Label4" runat="server" Width="14px" Height="15px" CssClass="tableLabel">To</asp:label></TD>
										<TD style="WIDTH: 156px; HEIGHT: 25px" width="156" colSpan="2"><cc1:mstextbox style="Z-INDEX: 0" id="txtManifestDateTo" tabIndex="16" runat="server" Width="141px"
												CssClass="textField" TextMaskType="msDate" MaxLength="16" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
										<TD style="WIDTH: 191px; HEIGHT: 25px" width="191" align="right"><asp:label style="Z-INDEX: 0" id="lblExcepCode" runat="server" Width="14px" Height="15px" CssClass="tableLabel">To</asp:label></TD>
										<TD style="HEIGHT: 25px" width="55%" colSpan="2"><cc1:mstextbox style="Z-INDEX: 0" id="txtAct_Pod_to" tabIndex="17" runat="server" Width="60%" CssClass="textField"
												TextMaskType="msDate" MaxLength="16" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" width="3%"></TD>
										<TD style="WIDTH: 120px; HEIGHT: 25px" width="121"></TD>
										<TD style="WIDTH: 156px; HEIGHT: 25px" width="156" colSpan="2"></TD>
										<TD style="WIDTH: 191px; HEIGHT: 25px" width="191" colSpan="3"><asp:label style="Z-INDEX: 0" id="lblActualPodDate" runat="server" Height="15px" CssClass="tableLabel"></asp:label></TD>
									</TR>
								</TABLE>
							</FIELDSET>
							<TABLE style="Z-INDEX: 0; WIDTH: 744px; HEIGHT: 206px" id="Table7" border="0" width="744"
								align="left" runat="server">
								<TR>
									<TD style="WIDTH: 132px" width="132" colSpan="2" noWrap></TD>
									<TD style="WIDTH: 156px" width="156" noWrap align="right"><asp:label style="Z-INDEX: 0" id="Label59" runat="server" Width="144px" Height="15px" CssClass="tableLabelRight"
											Font-Size="11px" Font-Bold="True">Rated Amounts</asp:label></TD>
									<TD style="WIDTH: 176px" width="176" noWrap align="right"><asp:label style="Z-INDEX: 0" id="Label15" runat="server" Height="15px" CssClass="tableLabelRight"
											Font-Size="11px" Font-Bold="True">Override Amounts</asp:label></TD>
									<TD style="WIDTH: 36px" width="36" noWrap align="right"></TD>
									<TD style="WIDTH: 135px" width="135" noWrap align="left"></TD>
									<TD width="50" noWrap align="left"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 132px" width="132" colSpan="2" noWrap><asp:label style="Z-INDEX: 0" id="Label5" runat="server" Height="15px" CssClass="tableLabel"
											Font-Size="11px">Basic Charge</asp:label></TD>
									<TD style="WIDTH: 156px" width="156" noWrap align="left"><cc1:mstextbox style="Z-INDEX: 0" id="txtBasicCharge" tabIndex="18" runat="server" Width="100%"
											CssClass="textFieldRightAlign" Enabled="False" AutoPostBack="True" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2"
											TextMaskType="msNumeric" MaxLength="9"></cc1:mstextbox></TD>
									<TD style="WIDTH: 176px" width="176" noWrap align="right"><cc1:mstextbox style="Z-INDEX: 0" id="txtOverBasicCharge" tabIndex="18" runat="server" Width="144px"
											CssClass="textFieldRightAlign" Enabled="True" AutoPostBack="True" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2"
											TextMaskType="msNumeric" MaxLength="9"></cc1:mstextbox></TD>
									<TD style="WIDTH: 36px" width="36" noWrap align="right"></TD>
									<TD style="WIDTH: 135px" width="135" noWrap align="left"><asp:label style="Z-INDEX: 0" id="LastUserUpdateLabel" runat="server" Height="15px" CssClass="tableLabel"
											Font-Size="11px" Font-Bold="True">Last User Updated</asp:label></TD>
									<TD width="50" noWrap align="left"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 132px" width="132" colSpan="2" noWrap><asp:label style="Z-INDEX: 0" id="FreightChargeLabel" runat="server" Height="15px" CssClass="tableLabel"
											Font-Size="11px">Freight Charge</asp:label></TD>
									<TD style="WIDTH: 156px" width="156" noWrap align="left"><cc1:mstextbox style="Z-INDEX: 0" id="txtFreightChargeDis" tabIndex="18" runat="server" Width="100%"
											CssClass="textFieldRightAlign" Enabled="False" AutoPostBack="True" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2"
											TextMaskType="msNumeric" MaxLength="9"></cc1:mstextbox></TD>
									<TD style="WIDTH: 176px" width="176" noWrap align="right"><cc1:mstextbox style="Z-INDEX: 0" id="txtFreightCharge" tabIndex="18" runat="server" Width="144px"
											CssClass="textFieldRightAlign" Enabled="True" AutoPostBack="True" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2"
											TextMaskType="msNumeric" MaxLength="9"></cc1:mstextbox></TD>
									<TD style="WIDTH: 36px" width="36" noWrap align="right"></TD>
									<TD style="WIDTH: 135px" width="135" noWrap align="left"><cc1:mstextbox style="Z-INDEX: 0" id="txtLastUserUpd" tabIndex="18" runat="server" Width="100%"
											CssClass="textFieldRightAlign" Enabled="False" AutoPostBack="True" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2"
											TextMaskType="msNumeric" MaxLength="9"></cc1:mstextbox></TD>
									<TD width="50" noWrap align="left"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 132px" width="132" colSpan="2" noWrap><asp:label style="Z-INDEX: 0" id="InsruranceSurcLabel" runat="server" Height="15px" CssClass="tableLabel"
											Font-Size="11px">Insurance Surcharge</asp:label></TD>
									<TD style="WIDTH: 156px" width="156" noWrap align="left"><cc1:mstextbox style="Z-INDEX: 0" id="txtInsrSurcDis" tabIndex="18" runat="server" Width="100%"
											CssClass="textFieldRightAlign" Enabled="False" AutoPostBack="True" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2"
											TextMaskType="msNumeric" MaxLength="9"></cc1:mstextbox></TD>
									<TD style="WIDTH: 176px" width="176" noWrap align="right"><cc1:mstextbox style="Z-INDEX: 0" id="txtInsrSurc" tabIndex="19" runat="server" Width="144px" CssClass="textFieldRightAlign"
											Enabled="True" AutoPostBack="True" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2" TextMaskType="msNumeric" MaxLength="9"></cc1:mstextbox></TD>
									<TD style="WIDTH: 36px" width="36" noWrap align="right"></TD>
									<TD style="WIDTH: 135px" width="135" noWrap align="left"><asp:label style="Z-INDEX: 0" id="Label53" runat="server" CssClass="tableLabelRight" Font-Size="11px"
											Font-Bold="True">Last Updated D/T</asp:label></TD>
									<TD width="50" noWrap align="left"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 132px; HEIGHT: 22px" width="132" colSpan="2" noWrap><asp:label style="Z-INDEX: 0" id="OtherSurchargeLabel" runat="server" Height="15px" CssClass="tableLabel"
											Font-Size="11px">Non-VAS Surcharge</asp:label></TD>
									<TD style="WIDTH: 156px; HEIGHT: 22px" width="156" noWrap align="left"><cc1:mstextbox style="Z-INDEX: 0" id="lblOtherSurc" tabIndex="18" runat="server" Width="100%" CssClass="textFieldRightAlign"
											Enabled="False" AutoPostBack="True" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2" TextMaskType="msNumeric" MaxLength="9"></cc1:mstextbox></TD>
									<TD style="WIDTH: 176px; HEIGHT: 22px" width="176" noWrap align="right"><cc1:mstextbox style="Z-INDEX: 0" id="txtOtherSurc" tabIndex="20" runat="server" Width="144px"
											CssClass="textFieldRightAlign" Enabled="True" AutoPostBack="True" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2" TextMaskType="msNumeric"
											MaxLength="9"></cc1:mstextbox></TD>
									<TD style="WIDTH: 36px; HEIGHT: 22px" width="36" noWrap align="right"></TD>
									<TD style="WIDTH: 135px; HEIGHT: 22px" width="135" noWrap align="left"><cc1:mstextbox style="Z-INDEX: 0" id="txtLastUpdateDT" tabIndex="18" runat="server" Width="100%"
											CssClass="textFieldRightAlign" Enabled="False" AutoPostBack="True" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2" TextMaskType="msNumeric"
											MaxLength="9"></cc1:mstextbox></TD>
									<TD style="HEIGHT: 22px" width="50" noWrap align="left"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 132px" width="132" colSpan="2" noWrap><asp:label style="Z-INDEX: 0" id="TotalVASSurcLabel" runat="server" Height="15px" CssClass="tableLabel"
											Font-Size="11px">VAS Surcharge</asp:label></TD>
									<TD style="WIDTH: 156px" width="156" noWrap align="left"><cc1:mstextbox style="Z-INDEX: 0" id="txtTotVASSurc" tabIndex="18" runat="server" Width="100%"
											CssClass="textFieldRightAlign" Enabled="False" AutoPostBack="True" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2"
											TextMaskType="msNumeric" MaxLength="9"></cc1:mstextbox></TD>
									<TD style="WIDTH: 176px" width="176" noWrap align="right"><cc1:mstextbox style="Z-INDEX: 0" id="lblTotVASSurcOverride" tabIndex="19" runat="server" Width="144px"
											CssClass="textFieldRightAlign" Enabled="True" AutoPostBack="True" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2"
											TextMaskType="msNumeric" MaxLength="9"></cc1:mstextbox></TD>
									<TD style="WIDTH: 36px" width="36" noWrap align="right"></TD>
									<TD style="WIDTH: 135px" width="135" noWrap align="left"></TD>
									<TD width="50" noWrap align="left"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 132px" width="132" colSpan="2" noWrap><asp:label style="Z-INDEX: 0" id="ESASurcLabel" runat="server" Height="15px" CssClass="tableLabel"
											Font-Size="11px">ESA Surcharge</asp:label></TD>
									<TD style="WIDTH: 156px" width="156" noWrap align="left"><cc1:mstextbox style="Z-INDEX: 0" id="lblESASurc" tabIndex="18" runat="server" Width="100%" CssClass="textFieldRightAlign"
											Enabled="False" AutoPostBack="True" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2" TextMaskType="msNumeric" MaxLength="9"></cc1:mstextbox></TD>
									<TD style="WIDTH: 176px" width="176" noWrap align="right"><cc1:mstextbox style="Z-INDEX: 0" id="txtESASurc" tabIndex="21" runat="server" Width="144px" CssClass="textFieldRightAlign"
											Enabled="True" AutoPostBack="True" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2" TextMaskType="msNumeric" MaxLength="9"></cc1:mstextbox></TD>
									<TD style="WIDTH: 36px" width="36" noWrap align="right"></TD>
									<TD style="WIDTH: 135px" width="135" noWrap align="left"></TD>
									<TD width="50" noWrap align="left"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 132px; HEIGHT: 33px" width="132" colSpan="2" noWrap><asp:label style="Z-INDEX: 0" id="TotalAmountLabel" runat="server" Height="15px" CssClass="tableLabel"
											Font-Size="11px">Total Amount</asp:label></TD>
									<TD style="WIDTH: 156px; HEIGHT: 33px" width="156" noWrap align="left"><cc1:mstextbox style="Z-INDEX: 0" id="lblTotAmt" tabIndex="18" runat="server" Width="100%" CssClass="textFieldRightAlign"
											Enabled="False" AutoPostBack="True" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2" TextMaskType="msNumeric" MaxLength="9"></cc1:mstextbox></TD>
									<TD style="WIDTH: 176px; HEIGHT: 33px" width="176" noWrap align="right"><cc1:mstextbox style="Z-INDEX: 0" id="lblTotAmtOverride" tabIndex="21" runat="server" Width="144px"
											CssClass="textFieldRightAlign" Enabled="False" AutoPostBack="True" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2" TextMaskType="msNumeric"
											MaxLength="9"></cc1:mstextbox></TD>
									<TD style="WIDTH: 36px; HEIGHT: 33px" width="36" noWrap align="right"></TD>
									<TD style="WIDTH: 135px; HEIGHT: 33px" width="135" noWrap align="left"></TD>
									<TD style="HEIGHT: 33px" width="50" noWrap align="left"></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
			</DIV>
		</FORM>
	</BODY>
</HTML>
