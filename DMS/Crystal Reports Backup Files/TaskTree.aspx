<%@ Register TagPrefix="iewc" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls, Version=1.0.2.226, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Page language="c#" Codebehind="TaskTree.aspx.cs" AutoEventWireup="false" Inherits="com.ties.TaskTree" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>TaskTree</title>
		<meta content="True" name="vs_showGrid">
		<LINK href="<%=strStyleSheet%>" type=text/css rel=stylesheet 
name="style1">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript">
	
			var x = 250;
			var y = 30;
			function closeframe()
			{
				if (x > 28)
				{
					x -= 5;
					parent.fr_nav_content.cols = x + ",100%";
					setTimeout("closeframe()",1);
				}
				else
				{
					x = 250;
				}
			}
				
			function openframe()
			{
				if (y < 250)
				{
					y += 5;
					parent.fr_nav_content.cols = y + ",100%";
					setTimeout("openframe()",1);
				}
				else
				{
					y = 30;
				}
			}
				
		</script>
	</HEAD>
	<body background="<%=strNaviBgImage%>" ms_positioning="GridLayout">
		<form id="Form1" method="post" runat="server">
			<INPUT onclick="closeframe()" style="Z-INDEX: 101; POSITION: absolute; FONT-SIZE: 10px; TOP: -1px; LEFT: 0px"
				type="button" value="<"> <INPUT onclick="openframe()" style="Z-INDEX: 102; POSITION: absolute; FONT-SIZE: 10px; TOP: -1px; LEFT: 13px"
				type="button" value=">">
			<iewc:treeview id="TreeView1" runat="server" target="displayContent" Indent="0" ImageUrl="images/icons/folder.gif"
				CssClass=".IMGs" BackColor="Transparent" style="Z-INDEX: 100; POSITION: absolute; TOP: 19px; LEFT: 0px"
				SelectExpands="True" ExpandLevel="1" SELECTEDSTYLE="font-name:Verdana" DEFUALTSTYLE="font-name:Verdana"></iewc:treeview>&nbsp;
		</form>
	</body>
</HTML>
