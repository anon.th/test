<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<%@ Page language="c#" Codebehind="ImportConsignments.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ImportConsignments" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ImportConsignments</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/WorkProgress.js"></script>
		<script language="javascript">
		<!--
		
		function setValue()
		{
			var s = document.all['inFile'];
			document.all['txtFilePath'].innerText = s.value;
			if(s.value != '') 
			{//debugger;
				document.all['btnImport'].disabled = false;
				//document.all['btnHiddenPostbackBrowse'].click();
				//__doPostBack(btnHiddenPostbackBrowse,'');
			}
			else
				document.all['btnImport'].disabled = true;
			
		}
		
		function upBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-up.GIF)';
		}
		
		function downBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-down.GIF)';
		}
		
		function clearValue()
		{
			if(document.all['txtFilePath'].innerText == '') {
				document.location.reload();
			}
		}
		
		function callCodeBehindSvcCode()
		{
			
		}
		
		
		//-->
		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0); HideWait();" MS_POSITIONING="FlowLayout">
		<form id="ImportConsignments" name="ImportConsignments" method="post" encType="multipart/form-data"
			runat="server">
			<input id="btnHiddenPostbackBrowse" style="DISPLAY: none" type="button" runat="server">
				<div id="CSSDiv"><FONT face="Tahoma"></FONT></div>
				<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
				<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
				<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
				<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
				<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
				<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
				<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
				<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
				<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
				<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
				<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
				<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
				<table id="Table2" style="WIDTH: 3182px; HEIGHT: 563px" width="3182" align="center" border="0">
					<TBODY>
						<tr>
							<td><asp:label id="lblMainTitle" runat="server" CssClass="mainTitleSize" Width="477px" Height="26px">Import Consignments</asp:label></td>
						</tr>
						<tr>
							<td>
								<table id="Table3" width="100%" border="0">
									<TBODY>
										<tr>
											<td width="17%">
												<div onmouseup="upBrowse();" onmousedown="downBrowse();" id="divBrowse" style="BACKGROUND-IMAGE: url(images/btn-browse-up.GIF); WIDTH: 70px; DISPLAY: inline; HEIGHT: 20px"><INPUT id="inFile" onblur="setValue();" style="BORDER-BOTTOM: #88a0c8 1px outset; FILTER: alpha(opacity: 0); BORDER-LEFT: #88a0c8 1px outset; BACKGROUND-COLOR: #e9edf0; WIDTH: 0px; FONT-FAMILY: Arial; COLOR: #003068; FONT-SIZE: 11px; BORDER-TOP: #88a0c8 1px outset; BORDER-RIGHT: #88a0c8 1px outset; TEXT-DECORATION: none"
														onfocus="setValue();" type="file" name="inFile" runat="server" enableViewState="True"></div>
			</input>
			<asp:button id="btnImport" runat="server" CssClass="queryButton" Width="62px" Text="Import"
				Enabled="False"></asp:button><asp:button id="btnSave" runat="server" CssClass="queryButton" Width="66px" Text="Save" Enabled="False"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:textbox id="txtFilePath" runat="server" CssClass="textField" Width="240px"></asp:textbox>
			</TD>
			<td><asp:label id="lblErrorMsg" runat="server" CssClass="errorMsgColor" Width="566px" Height="19px"></asp:label><BR>
				<asp:label id="lblTotalSavedCon" runat="server" CssClass="errorMsgColor" Width="566px" Height="19px"></asp:label></td>
			</TR></TBODY></TABLE></TD></TR>
			<TR>
				<td><asp:datagrid id="dgConsignment" tabIndex="115" runat="server" Width="98%" OnSelectedIndexChanged="dgConsignment_SelectedIndexChanged"
						HeaderStyle-Height="20px" ItemStyle-Height="20px" PageSize="4" OnUpdateCommand="dgConsignment_Update"
						OnDeleteCommand="dgConsignment_Delete" OnCancelCommand="dgConsignment_Cancel" OnPageIndexChanged="dgConsignment_PageChange"
						AllowPaging="True" OnEditCommand="dgConsignment_Edit" AutoGenerateColumns="False" OnItemCommand="dgConsignment_Button"
						OnItemDataBound="dgConsignment_Bound" SelectedItemStyle-CssClass="gridFieldSelected" style="Z-INDEX: 0">
						<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
						<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
						<HeaderStyle Height="20px"></HeaderStyle>
						<Columns>
							<asp:TemplateColumn Visible="False" HeaderText="Record ID">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblConRecordid" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"recordid")%>'>
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn>
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									&nbsp;&nbsp;
									<asp:ImageButton CommandName="Select" id="imgSelect" runat="server" ImageUrl="images/butt-select.gif"></asp:ImageButton>&nbsp;&nbsp;
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0  title='Update' &gt;"
								CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel'&gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0  title='Edit' &gt;">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
							</asp:EditCommandColumn>
							<asp:TemplateColumn>
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									&nbsp;&nbsp;
									<asp:ImageButton CommandName="Delete" id="imgDelete" runat="server" ImageUrl="images/butt-delete.gif"></asp:ImageButton>&nbsp;&nbsp;
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Consignment No">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblconsignment_no" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtconsignment_no" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Booking No">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblbooking_no" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"booking_no")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<cc1:mstextbox id="txtbooking_no" tabIndex="1" runat="server" CssClass="gridTextBox" Text='<%#DataBinder.Eval(Container.DataItem,"booking_no")%>' NumberPrecision="10" NumberMaxValue="2147483647" TextMaskType="msNumeric" MaxLength="10" AutoPostBack="False">
									</cc1:mstextbox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Ref No">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblref_no" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ref_no")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtref_no" Text='<%#DataBinder.Eval(Container.DataItem,"ref_no")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Service Type">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblservice_code" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtservice_code" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>'>
									</asp:TextBox>
									<asp:RequiredFieldValidator ID="validservice_code" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtservice_code"
										ErrorMessage="The service code is required"></asp:RequiredFieldValidator>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="ServiceCodeSearch">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
							</asp:ButtonColumn>
							<asp:TemplateColumn HeaderText="Customer">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblpayerid" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"payerid")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtpayerid" Enabled="false" Text='<%#DataBinder.Eval(Container.DataItem,"payerid")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="PayerIDSearch">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
							</asp:ButtonColumn>
							<asp:TemplateColumn HeaderText="Name">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblpayer_name" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"payer_name")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtpayer_name" Text='<%#DataBinder.Eval(Container.DataItem,"payer_name")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Address">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblpayer_address1" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"payer_address1")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtpayer_address1" Text='<%#DataBinder.Eval(Container.DataItem,"payer_address1")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Address 2">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblpayer_address2" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"payer_address2")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtpayer_address2" Text='<%#DataBinder.Eval(Container.DataItem,"payer_address2")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Postal Code">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblpayer_zipcode" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"payer_zipcode")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtpayer_zipcode" MaxLength="12" Text='<%#DataBinder.Eval(Container.DataItem,"payer_zipcode")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="PayerPostalCodeSearch">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
							</asp:ButtonColumn>
							<asp:TemplateColumn HeaderText="Telephone">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lbltelephone" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"payer_telephone")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtpayer_telephone" Text='<%#DataBinder.Eval(Container.DataItem,"payer_telephone")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Fax">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblpayer_fax" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"payer_fax")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtpayer_fax" Text='<%#DataBinder.Eval(Container.DataItem,"payer_fax")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Cash / Credit">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblpayment_mode" CssClass="gridLabel" runat="server"></asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:DropDownList id="ddlpayment_mode" runat="server" CssClass="gridTextBox" Width="80"></asp:DropDownList>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Sender Name">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblsender_name" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"sender_name")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtsender_name" Text='<%#DataBinder.Eval(Container.DataItem,"sender_name")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="SenderNameSearch">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
							</asp:ButtonColumn>
							<asp:TemplateColumn HeaderText="Sender Address">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblsender_address1" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"sender_address1")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtsender_address1" Text='<%#DataBinder.Eval(Container.DataItem,"sender_address1")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Sender Address2">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblsender_address2" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"sender_address2")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtsender_address2" Text='<%#DataBinder.Eval(Container.DataItem,"sender_address2")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Postal Code">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblsender_zipcode" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"sender_zipcode")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtsender_zipcode" Text='<%#DataBinder.Eval(Container.DataItem,"sender_zipcode")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="SenderPostalCodeSearch">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
							</asp:ButtonColumn>
							<asp:TemplateColumn HeaderText="Telephone">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblsender_telephone" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"sender_telephone")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtsender_telephone" Text='<%#DataBinder.Eval(Container.DataItem,"sender_telephone")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Fax">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblsender_fax" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"sender_fax")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtsender_fax" Text='<%#DataBinder.Eval(Container.DataItem,"sender_fax")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Contact Person">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblsender_contact_person" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"sender_contact_person")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtsender_contact_person" Text='<%#DataBinder.Eval(Container.DataItem,"sender_contact_person")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Recipient Name">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblrecipient_name" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_name")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtrecipient_name" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_name")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="RecipientNameSearch">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
							</asp:ButtonColumn>
							<asp:TemplateColumn HeaderText="Recipient Address1">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblrecipient_address1" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_address1")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtrecipient_address1" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_address1")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Recipient Address2">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblrecipient_address2" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_address2")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtrecipient_address2" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_address2")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Postal Code">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblrecipient_zipcode" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_zipcode")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtrecipient_zipcode" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_zipcode")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="RecipientPostalCodeSearch">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
							</asp:ButtonColumn>
							<asp:TemplateColumn HeaderText="Telephone">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblrecipient_telephone" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_telephone")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtrecipient_telephone" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_telephone")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="RecipientTelephoneSearch">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
							</asp:ButtonColumn>
							<asp:TemplateColumn HeaderText="Fax">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblrecipient_fax" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_fax")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtrecipient_fax" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_fax")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Contact Person">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblrecipient_contact_person" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_contact_person")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtrecipient_contact_person" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_contact_person")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Declare Value">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lbldeclare_value" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"declare_value")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtdeclare_value" Text='<%#DataBinder.Eval(Container.DataItem,"declare_value")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="COD Amount">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblcod_amount" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cod_amount")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtcod_amount" Text='<%#DataBinder.Eval(Container.DataItem,"cod_amount")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Payment Type">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblpayment_type" CssClass="gridLabel" runat="server"></asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:DropDownList id="ddlpayment_type" runat="server" CssClass="gridTextBox" Width="80"></asp:DropDownList>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Commodity Code">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblcommodity_code" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"commodity_code")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtcommodity_code" Text='<%#DataBinder.Eval(Container.DataItem,"commodity_code")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="CommodityCodeSearch">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
							</asp:ButtonColumn>
							<asp:TemplateColumn HeaderText="Return POD HC">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblReturnPODHC" CssClass="gridLabel" runat="server"></asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:DropDownList id="ddlReturnPODHC" runat="server" CssClass="gridTextBox" Width="120"></asp:DropDownList>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Return Invoice HC">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblReturnInvHC" CssClass="gridLabel" runat="server"></asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:DropDownList id="ddlReturnInvHC" runat="server" CssClass="gridTextBox" Width="120"></asp:DropDownList>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Remark">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblremark" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"remark")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtremark" Text='<%#DataBinder.Eval(Container.DataItem,"remark")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Booking_Datetime">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="Label1" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"booking_datetime")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="Textbox1" Text='<%#DataBinder.Eval(Container.DataItem,"booking_datetime")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
						</Columns>
						<PagerStyle NextPageText="Next" Height="20px" Font-Size="Smaller" PrevPageText="Previous" HorizontalAlign="Left"></PagerStyle>
					</asp:datagrid></td>
			</TR>
			<tr id="trMain" runat="server">
				<td>&nbsp;
					<asp:datagrid id="dgPackages" tabIndex="115" runat="server" OnSelectedIndexChanged="dgPackages_SelectedIndexChanged"
						HeaderStyle-Height="20px" ItemStyle-Height="20px" PageSize="4" OnUpdateCommand="dgPackages_Update"
						OnDeleteCommand="dgPackages_Delete" OnCancelCommand="dgPackages_Cancel" OnPageIndexChanged="dgPackages_PageChange"
						AllowPaging="True" OnEditCommand="dgPackages_Edit" AutoGenerateColumns="False" OnItemCommand="dgPackages_Button"
						OnItemDataBound="dgPackages_Bound" SelectedItemStyle-CssClass="gridFieldSelected" width="700px">
						<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
						<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
						<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
						<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
						<Columns>
							<asp:TemplateColumn HeaderText="Record ID" Visible="False">
								<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblPkgRecordid" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"recordid")%>'>
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn>
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									&nbsp;&nbsp;
									<asp:ImageButton CommandName="Select" id="imgSelectPKG" runat="server" ImageUrl="images/butt-select.gif"></asp:ImageButton>&nbsp;&nbsp;
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0  title='Update' &gt;"
								CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel'&gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0  title='Edit' &gt;">
								<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
							</asp:EditCommandColumn>
							<asp:TemplateColumn>
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									&nbsp;&nbsp;
									<asp:ImageButton CommandName="Delete" id="imgDeletePKG" runat="server" ImageUrl="images/butt-delete.gif"></asp:ImageButton>&nbsp;&nbsp;
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Consignment No">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblpkg_consignment_no" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txt_pkg_consignment_no" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="MPS No">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblmps_code" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"mps_code")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txt_mps_code" Text='<%#DataBinder.Eval(Container.DataItem,"mps_code")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Length">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblpkg_length" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_length")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txt_pkg_length" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_length")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Breadth">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblpkg_breadth" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_breadth")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txt_pkg_breadth" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_breadth")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Height">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblpkg_height" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_height")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txt_pkg_height" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_height")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Weight">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblpkg_weight" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_wt")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txt_pkg_weight" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_wt")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Quantity">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemTemplate>
									<asp:Label ID="lblpkg_qty" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_qty")%>'>
									</asp:Label>
								</ItemTemplate>
								<EditItemTemplate>
									<asp:TextBox CssClass="gridTextBox" runat="server" ID="txt_pkg_qty" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_qty")%>'>
									</asp:TextBox>
								</EditItemTemplate>
							</asp:TemplateColumn>
						</Columns>
						<PagerStyle NextPageText="Next" Font-Size="Smaller" PrevPageText="Previous" HorizontalAlign="left"
							Height="20"></PagerStyle>
					</asp:datagrid></td>
			</tr>
			<TR>
				<TD>&nbsp;&nbsp;<asp:datagrid id="dgError" tabIndex="115" runat="server" Width="600px" HeaderStyle-Height="20px"
						ItemStyle-Height="20px" PageSize="4" OnPageIndexChanged="dgError_PageChange" AllowPaging="True" AutoGenerateColumns="False">
						<Columns>
							<asp:TemplateColumn HeaderText="Error Code">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemStyle CssClass="gridField"></ItemStyle>
								<ItemTemplate>
									<asp:Label ID="lblerror_code" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"error_code")%>'>
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Associated Field">
								<HeaderStyle CssClass="gridHeading"></HeaderStyle>
								<ItemStyle CssClass="gridField"></ItemStyle>
								<ItemTemplate>
									<asp:Label ID="lblassociated_text" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"associated_text")%>'>
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
							<asp:TemplateColumn HeaderText="Error Message">
								<HeaderStyle Width="60%" CssClass="gridHeading"></HeaderStyle>
								<ItemStyle CssClass="gridField"></ItemStyle>
								<ItemTemplate>
									<asp:Label ID="lblerror_message" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"error_message")%>'>
									</asp:Label>
								</ItemTemplate>
							</asp:TemplateColumn>
						</Columns>
						<PagerStyle NextPageText="Next" Font-Size="Smaller" PrevPageText="Previous" HorizontalAlign="left"
							Height="20"></PagerStyle>
					</asp:datagrid></TD>
			</TR>
			</TBODY></TABLE></form>
	</body>
</HTML>
