<%@ Page language="c#" Codebehind="CustomsReport_ExportInvoices_Cheques.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsReport_ExportInvoices_Cheques" %>
<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomsReport_ExportInvoices_Cheques</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="vs_snapToGrid" content="true">
		<meta name="vs_showGrid" content="true">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
		
		function funcDisBack(){
			history.go(+1);
		}

        function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function validate(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventdefault) theEvent.preventdefault();
			}
		}
		
		function ValidateInvoiceList(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[,_0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		function RemoveBadInvoiceList(strTemp, obj) {
            strTemp = strTemp.replace(/[^,_0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteInvoiceList(obj) {
            setTimeout(function () {
                RemoveBadInvoiceList(obj.value, obj);
            }, 1); //or 4
        }
		
		function CheckAll(chk)
		{
			all = document.getElementsByTagName("input");
			for(i=0;i<all.length;i++)
			{
				if(all[i].type=="checkbox" && all[i].id.indexOf("gridInvoice") > -1)
				{
					all[i].checked = chk.checked;
				}
			}
			//b = document.getElementById('btnExportSelected');
			b2 = document.getElementById('ExportSelected');
			if(chk.checked==true)
			{				
				//b.disabled=false;
				b2.disabled=false;
			}
			else
			{
				//b.disabled=true;
				b2.disabled=true;
			}
		}
		
		function CheckAll2(chk)
		{
			all = document.getElementsByTagName("input");
			for(i=0;i<all.length;i++)
			{
				if(all[i].type=="checkbox" && all[i].id.indexOf("gridCheques") > -1)
				{
					all[i].checked = chk.checked;
				}
			}
			//b = document.getElementById('btnExportSelected');	
			b2 = document.getElementById('ExportSelected');
			if(chk.checked==true)
			{				
				//b.disabled=false;
				b2.disabled=false;
			}
			else
			{
				//b.disabled=true;
				b2.disabled=false;
			}
		}
		
		function Check(chk)
		{
			//b = document.getElementById('btnExportSelected');
			b2 = document.getElementById('ExportSelected');
			all = document.getElementsByTagName("input");
			for(i=0;i<all.length;i++)
			{
				if(all[i].type=="checkbox" && all[i].id.indexOf("gridInvoice") > -1)
				{
					if (all[i].checked) 
					{ 
						//b.disabled = false;
						b2.disabled = false;
						return;
					}
				}
			}
			//b.disabled = true;
			b2.disabled = true;
		}
		
		function Check2(chk)
		{
			//b = document.getElementById('btnExportSelected');
			b2 = document.getElementById('ExportSelected');
			all = document.getElementsByTagName("input");
			for(i=0;i<all.length;i++)
			{
				if(all[i].type=="checkbox" && all[i].id.indexOf("gridCheques") > -1)
				{
					if (all[i].checked) 
					{ 
						//b.disabled = false;
						b2.disabled = false;
						return;
					}
				}
			}
			//b.disabled = true;
			b2.disabled = true;
		}
		
		function DoAction(serveraction){
			var confirmed = true;
			var isSubmitted = true;
			var URL = "";
			var ErrMsg = "Error export csv file";
			switch (serveraction){
				case "EXPORTDATA":
					document.getElementById('ExportSelected').disabled = true;
					document.forms[0].elements("ServerAction").value = serveraction;
					document.forms[0].submit();
					break;
			}
			/*
			if (isSubmitted && confirmed){
				if (ErrMsg != ""){
					alert(ErrMsg);
				} else {
					document.forms[0].elements("ServerAction").value = serveraction;
					document.forms[0].submit();
										
					var dialog = new ActiveXObject('MSComDlg.CommonDialog');
					dialog.Filter = 'CSV files (*.csv)|*.csv| ';
					dialog.MaxFileSize = 260;
					dialog.DialogTitle = 'Lagre rapport';
					dialog.DefaultExt = '.csv';
					
					var defaultPath = document.getElementById('txtDefaultPath').value;
					if(defaultPath != '')
						dialog.InitDir = defaultPath;
					else
						dialog.InitDir = 'c:\\';
					
					dialog.ShowSave();
					if(dialog.FileName != '')
					{
						//document.forms[0].elements("ServerAction").value = serveraction + "|" + dialog.FileName;
						//document.forms[0].elements("ServerAction").value = serveraction + "|" + "xx";
						//document.forms[0].submit();
					}
					
				}  
			}
			*/
		}		
		
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="" onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="CustomsReport_ExportInvoices_Cheques" method="post" runat="server">
			<INPUT id="ServerAction" type="hidden" name="ServerAction">
			<asp:TextBox style="Z-INDEX: 103; POSITION: absolute; DISPLAY: none; TOP: 16px; LEFT: 168px"
				id="txtDefaultPath" runat="server"></asp:TextBox>
			<div style="Z-INDEX: 101; POSITION: relative; WIDTH: 1000px; HEIGHT: 1543px; TOP: 34px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tbody>
						<tr>
							<td colSpan="4" align="left"><asp:label id="Label1" runat="server" Width="421px" Height="27px" CssClass="mainTitleSize">
								Export Invoices/Cheques</asp:label></td>
						</tr>
						<tr>
							<td style="HEIGHT: 21px" colSpan="4"><asp:button id="btnQuery" runat="server" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExecuteQuery" runat="server" CssClass="queryButton" Text="Execute Query"
									CausesValidation="False"></asp:button>
								<INPUT id="ExportSelected" class="queryButton" disabled value="Export Selected" type="button"
									onclick="javascript:DoAction('EXPORTDATA');">
							</td>
						</tr>
						<tr>
							<td style="HEIGHT: 19px" colSpan="4"><asp:label id="lbError" runat="server" ForeColor="Red" Font-Size="X-Small" Font-Bold="true"></asp:label></td>
						</tr>
						<tr>
							<td colSpan="4">
								<fieldset style="WIDTH: 22%"><legend style="COLOR: black">Export</legend>
									<table style="MARGIN: 5px" id="table_Export">
										<tbody>
											<tr>
												<td><asp:radiobutton id="rbInvoices" runat="server" Width="93px" Height="22px" CssClass="tableRadioButton"
														Text="Invoices" Checked="True" GroupName="QueryByDate" AutoPostBack="true"></asp:radiobutton><asp:radiobutton id="rbCheques" runat="server" Width="73px" Height="22px" CssClass="tableRadioButton"
														Text="Cheques" GroupName="QueryByDate" AutoPostBack="true"></asp:radiobutton></td>
											</tr>
										</tbody>
									</table>
								</fieldset>
							</td>
						</tr>
						<tr>
							<td width="15%"><asp:label style="Z-INDEX: 0; MARGIN-LEFT: 4px" id="Label2" runat="server" CssClass="tableLabel">Number Range</asp:label></td>
							<td width="13%"><asp:textbox style="Z-INDEX: 107" id="txtForNumberRange" tabIndex="0" onkeypress="validate(event)"
									onpaste="AfterPasteNonNumber(this)" runat="server" Width="136px" CssClass="textField" MaxLength="8"></asp:textbox></td>
							<td width="3%"><asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel">To</asp:label></td>
							<td><asp:textbox style="Z-INDEX: 107; MARGIN-LEFT: 4px" id="txtToNumberRange" tabIndex="0" onkeypress="validate(event)"
									onpaste="AfterPasteNonNumber(this)" runat="server" Width="136px" CssClass="textField" MaxLength="8"></asp:textbox></td>
						</tr>
						<TR height="60" id="trInvoiceList" runat="server">
							<TD width="15%" valign="top">
								<asp:label style="Z-INDEX: 0; MARGIN-LEFT: 4px" id="Label4" runat="server" CssClass="tableLabel"
									Width="98%">Or (Comma Separated) <br />Numbers</asp:label></TD>
							<TD colspan="3" valign="top">
								<asp:textbox style="Z-INDEX: 0" id="txtInvoicesList" runat="server" Height="55px" Width="500px"
									TextMode="MultiLine" onkeypress="ValidateInvoiceList(event)" onpaste="AfterPasteInvoiceList(this)"></asp:textbox></TD>
						</TR>
						<tr>
							<td colSpan="4"><asp:checkbox id="chkShowOnlyNotExported" runat="server" CssClass="tableLabel" Text="Show only not exported"
									AutoPostBack="True"></asp:checkbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 90%" class="gridHeading" colSpan="4"><asp:label id="lbInvoicesHeader" runat="server" Visible="True" Font-Size="13px" Font-Bold="True">Invoices Matching Selection Criteria</asp:label><asp:label id="lbCheques" runat="server" Visible="False" Font-Size="13px" Font-Bold="True">Cheques Matching Selection Criteria</asp:label></td>
						</tr>
						<tr>
							<td colSpan="4"><asp:datagrid style="Z-INDEX: 0" id="gridInvoice" tabIndex="42" runat="server" Width="100%" AllowPaging="True"
									AutoGenerateColumns="False" DataKeyField="Invoice_No" AlternatingItemStyle-CssClass="gridField" ItemStyle-CssClass="gridField"
									HeaderStyle-CssClass="gridHeading" CellPadding="0" CellSpacing="1" BorderWidth="0px" FooterStyle-CssClass="gridHeading"
									PageSize="200">
									<FooterStyle CssClass="gridHeading"></FooterStyle>
									<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn>
											<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<HeaderTemplate>
												<asp:checkbox id="chkHeader" CssClass="tableLabel" onclick="javascript:CheckAll(this);" runat="server"
													Text=""></asp:checkbox>
											</HeaderTemplate>
											<ItemTemplate>
												<asp:checkbox id="chkItem" CssClass="tableLabel" runat="server" Text="" onclick="javascript:Check(this);"></asp:checkbox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Invoice No">
											<HeaderStyle HorizontalAlign="Left" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<%--#DataBinder.Eval(Container.DataItem, "Invoice_No")--%>
												<asp:Label ID="lbInvoiceNo" Visible="True" Runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Customer">
											<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<%#DataBinder.Eval(Container.DataItem, "payerid")%>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Customer Name">
											<HeaderStyle HorizontalAlign="Left" Width="30%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<%#DataBinder.Eval(Container.DataItem, "cust_name")%>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Printed Date">
											<HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<%-- #DateTime.Parse(DataBinder.Eval(Container.DataItem, "PrintedDT").ToString()).ToString("dd/MM/yyyy") --%>
												<asp:Label ID="lbPrintedDT" Visible="True" Runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Last Modified">
											<HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<%-- #DateTime.Parse(DataBinder.Eval(Container.DataItem, "LastModifiedDT").ToString()).ToString("dd/MM/yyyy") --%>
												<asp:Label ID="lbLastModifiedDT" Visible="True" Runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Exported Date">
											<HeaderStyle HorizontalAlign="Center" Width="13%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<%-- #DateTime.Parse(DataBinder.Eval(Container.DataItem, "ExportedDT").ToString()).ToString("dd/MM/yyyy") --%>
												<asp:Label ID="lbExportedDT" Visible="True" Runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Amount">
											<HeaderStyle HorizontalAlign="Right" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<%--#DataBinder.Eval(Container.DataItem, "Amount")--%>
												<asp:Label ID="lbAmount" Visible="True" Runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
								</asp:datagrid></td>
						</tr>
						<tr>
							<td colSpan="4">
								<asp:datagrid style="Z-INDEX: 0" id="gridCheques" tabIndex="42" runat="server" Width="100%" AllowPaging="True"
									AutoGenerateColumns="False" AlternatingItemStyle-CssClass="gridField" ItemStyle-CssClass="gridField"
									HeaderStyle-CssClass="gridHeading" CellPadding="0" CellSpacing="1" BorderWidth="0px" FooterStyle-CssClass="gridHeading"
									PageSize="200" Visible="False">
									<FooterStyle CssClass="gridHeading"></FooterStyle>
									<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn>
											<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<HeaderTemplate>
												<asp:checkbox id="chkHeader2" CssClass="tableLabel" onclick="javascript:CheckAll2(this);" runat="server"
													Text=""></asp:checkbox>
											</HeaderTemplate>
											<ItemTemplate>
												<asp:checkbox id="chkItem2" CssClass="tableLabel" runat="server" Text="" onclick="javascript:Check2(this);"></asp:checkbox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Cheque No.">
											<HeaderStyle HorizontalAlign="Left" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lbChequeNo" Visible="True" Runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Requisition No.">
											<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<%#DataBinder.Eval(Container.DataItem, "requisition_no")%>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Payee Name">
											<HeaderStyle HorizontalAlign="Left" Width="30%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<%#DataBinder.Eval(Container.DataItem, "payee")%>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Approved Date">
											<HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lbApprovedDT" Visible="True" Runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Exported Date">
											<HeaderStyle HorizontalAlign="Center" Width="13%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lbChequeExportedDT" Visible="True" Runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Amount">
											<HeaderStyle HorizontalAlign="Right" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lbChequeAmount" Visible="True" Runat="server"></asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
								</asp:datagrid></td>
						</tr>
					</tbody>
				</TABLE>
			</div>
			<INPUT 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition> <INPUT type="hidden" name="hdnRefresh">
		</form>
	</body>
</HTML>
