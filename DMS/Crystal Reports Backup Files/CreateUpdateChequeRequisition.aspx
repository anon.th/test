<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="CreateUpdateChequeRequisition.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CreateUpdateChequeRequisition" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Create/Update a Cheque Requisition</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script type="text/javascript">
<!--
function onButtonClick()
{
 window.close();
}
// -->
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:label style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 8px" id="Label38" runat="server"
				ForeColor="Red" CssClass="tableLabel">*</asp:label>
			<div style="POSITION: relative; WIDTH: 813px; HEIGHT: 693px" id="divMain" MS_POSITIONING="GridLayout"
				runat="server">
				<TABLE style="POSITION: absolute" id="MainTable" border="0" width="100%" runat="server">
					<TBODY>
						<tr>
							<td colSpan="2" align="left"><asp:label id="Label1" runat="server" CssClass="mainTitleSize" Height="27px" Width="421px">
							Create/Update a Cheque Requisition</asp:label></td>
						</tr>
						<tr>
							<td style="HEIGHT: 26px" vAlign="top" colSpan="2" align="left"><asp:button id="btnQry" runat="server" CssClass="queryButton" CausesValidation="False" Text="Query"></asp:button><asp:button id="btnExecQry" runat="server" CssClass="queryButton" CausesValidation="False" Text="Execute Query"></asp:button><asp:button id="btnPrintConsNote" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Insert"></asp:button><asp:button id="btnSave" runat="server" CssClass="queryButton" CausesValidation="False" Text="Save"
									Enabled="False"></asp:button><asp:button id="btnPrintCons" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Print Cheque Req" Enabled="False"></asp:button><asp:button id="btnCustomsJobEntry" runat="server" CssClass="queryButton" Width="150px" CausesValidation="False"
									Text="Enter Receipt No." Enabled="False"></asp:button></td>
						</tr>
						<tr>
							<td vAlign="top" colSpan="2" align="left">
								<table border="0" cellSpacing="1" cellPadding="1" width="800">
									<TR>
										<TD style="HEIGHT: 19px" colSpan="6"><asp:label id="lblError" runat="server" ForeColor="Red" Font-Bold="True" Font-Size="X-Small"></asp:label></TD>
									</TR>
									<tr>
										<td><asp:label id="Label4" runat="server" CssClass="tableLabel"> Cheque Reqisition Number:</asp:label><asp:label style="Z-INDEX: 0" id="Label26" runat="server" ForeColor="Red" CssClass="tableLabel">*</asp:label></td>
										<td style="WIDTH: 237px"><FONT face="Tahoma"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtJobEntryID" tabIndex="14" runat="server"
													CssClass="textField" Width="200px" ReadOnly="True" MaxLength="100"></asp:textbox></FONT></td>
										<td><asp:label id="Label5" runat="server" CssClass="tableLabel"> Payee:</asp:label><asp:label id="Label22" runat="server" ForeColor="Red" CssClass="tableLabel">*</asp:label></td>
										<td colSpan="3"><asp:textbox style="Z-INDEX: 0" id="Textbox1" tabIndex="15" runat="server" CssClass="textField"
												Width="100px" MaxLength="100"></asp:textbox></td>
									</tr>
									<TR>
										<TD><FONT face="Tahoma"><asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel"> Status:</asp:label><asp:label style="Z-INDEX: 0" id="Label23" runat="server" ForeColor="Red" CssClass="tableLabel">*</asp:label></FONT></TD>
										<TD style="WIDTH: 237px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtHouseAWBNumber" tabIndex="15"
												runat="server" CssClass="textField" Width="200px" MaxLength="100">543232993</asp:textbox></TD>
										<TD><FONT face="Tahoma"></FONT></TD>
										<TD colSpan="3"><FONT face="Tahoma"></FONT></TD>
									</TR>
									<TR>
										<TD><FONT face="Tahoma"><asp:label style="Z-INDEX: 0" id="Label8" runat="server" CssClass="tableLabel">To Approve, enter</asp:label></FONT></TD>
										<TD style="WIDTH: 237px"></TD>
										<TD></TD>
										<TD colSpan="3"></TD>
									</TR>
									<TR>
										<TD><asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel">Cheque Number: </asp:label><asp:label style="Z-INDEX: 0" id="Label31" runat="server" ForeColor="Red" CssClass="tableLabel">*</asp:label></TD>
										<TD style="WIDTH: 237px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Textbox2" tabIndex="14" runat="server"
												CssClass="textField" Width="200px" Enabled="False" ReadOnly="True" MaxLength="100">144840</asp:textbox></TD>
										<TD><asp:label style="Z-INDEX: 0" id="Label9" runat="server" CssClass="tableLabel">Date Issued:</asp:label><asp:label style="Z-INDEX: 0" id="Label24" runat="server" ForeColor="Red" CssClass="tableLabel">*</asp:label></TD>
										<TD colSpan="3"><FONT face="Tahoma"><cc1:mstextbox style="Z-INDEX: 0" id="Mstextbox1" tabIndex="21" runat="server" CssClass="textField"
													Width="90px" MaxLength="12" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></FONT></TD>
									</TR>
									<TR>
										<TD><FONT face="Tahoma"><asp:label style="Z-INDEX: 0" id="Label6" runat="server" CssClass="tableLabel">After Disbursement to Customs:</asp:label></FONT></TD>
										<TD style="WIDTH: 237px"><FONT face="Tahoma"></FONT></TD>
										<TD><asp:label style="Z-INDEX: 0" id="Label7" runat="server" CssClass="tableLabel">Total Amount:</asp:label></TD>
										<TD colSpan="3"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox3"
												tabIndex="14" runat="server" CssClass="textField" Width="140px" Enabled="False" ReadOnly="True" MaxLength="100"></asp:textbox></TD>
									</TR>
									<TR>
										<TD><asp:label style="Z-INDEX: 0" id="Label11" runat="server" CssClass="tableLabel">Receipt Number:</asp:label></TD>
										<TD style="WIDTH: 237px"><FONT face="Tahoma"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox4"
													tabIndex="14" runat="server" CssClass="textField" Width="140px" Enabled="False" ReadOnly="True" MaxLength="100"></asp:textbox></FONT></TD>
										<TD><asp:label style="Z-INDEX: 0" id="Label10" runat="server" CssClass="tableLabel">Maximum invoices:</asp:label></TD>
										<TD colSpan="3"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox6"
												tabIndex="14" runat="server" CssClass="textField" Width="140px" Enabled="False" ReadOnly="True" MaxLength="100"></asp:textbox></TD>
									</TR>
									<TR>
										<TD><asp:label style="Z-INDEX: 0" id="Label12" runat="server" CssClass="tableLabel">Date Paid:</asp:label></TD>
										<TD style="WIDTH: 237px"><cc1:mstextbox style="Z-INDEX: 0" id="Mstextbox2" tabIndex="21" runat="server" CssClass="textField"
												Width="90px" MaxLength="12" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></TD>
										<TD><asp:label style="Z-INDEX: 0" id="Label13" runat="server" CssClass="tableLabel">Available Invoices:</asp:label></TD>
										<TD colSpan="3"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox5"
												tabIndex="14" runat="server" CssClass="textField" Width="140px" Enabled="False" ReadOnly="True" MaxLength="100"></asp:textbox></TD>
									</TR>
									<TR>
										<TD><asp:label style="Z-INDEX: 0" id="Label14" runat="server" CssClass="tableLabel">History</asp:label></TD>
										<TD style="WIDTH: 237px"><FONT face="Tahoma"></FONT></TD>
										<TD></TD>
										<TD colSpan="3"></TD>
									</TR>
									<TR>
										<TD><asp:label style="Z-INDEX: 0" id="Label15" runat="server" CssClass="tableLabel">Requested by:</asp:label></TD>
										<TD style="WIDTH: 237px"><FONT face="Tahoma"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox7"
													tabIndex="14" runat="server" CssClass="textField" Width="80px" Enabled="False" ReadOnly="True" MaxLength="100"></asp:textbox><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox8"
													tabIndex="14" runat="server" CssClass="textField" Width="150px" Enabled="False" ReadOnly="True" MaxLength="100"></asp:textbox></FONT></TD>
										<TD><asp:label style="Z-INDEX: 0" id="Label17" runat="server" CssClass="tableLabel">Modified by:</asp:label></TD>
										<TD colSpan="3"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox11"
												tabIndex="14" runat="server" CssClass="textField" Width="80px" Enabled="False" ReadOnly="True" MaxLength="100"></asp:textbox><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox12"
												tabIndex="14" runat="server" CssClass="textField" Width="150px" Enabled="False" ReadOnly="True" MaxLength="100"></asp:textbox></TD>
									</TR>
									<TR>
										<TD><asp:label style="Z-INDEX: 0" id="Label16" runat="server" CssClass="tableLabel">Approved by:</asp:label></TD>
										<TD style="WIDTH: 237px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox9"
												tabIndex="14" runat="server" CssClass="textField" Width="80px" Enabled="False" ReadOnly="True" MaxLength="100"></asp:textbox><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox10"
												tabIndex="14" runat="server" CssClass="textField" Width="150px" Enabled="False" ReadOnly="True" MaxLength="100"></asp:textbox></TD>
										<TD><asp:label style="Z-INDEX: 0" id="Label18" runat="server" CssClass="tableLabel">Paid by:</asp:label></TD>
										<TD colSpan="3"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox13"
												tabIndex="14" runat="server" CssClass="textField" Width="80px" Enabled="False" ReadOnly="True" MaxLength="100"></asp:textbox><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Textbox14"
												tabIndex="14" runat="server" CssClass="textField" Width="150px" Enabled="False" ReadOnly="True" MaxLength="100"></asp:textbox></TD>
									</TR>
									<TR>
										<TD><FONT face="Tahoma"></FONT></TD>
										<TD style="WIDTH: 237px"><FONT face="Tahoma"></FONT></TD>
										<TD></TD>
										<TD colSpan="3"></TD>
									</TR>
									<TR>
										<TD colSpan="6"><asp:button style="Z-INDEX: 0" id="Button1" runat="server" CssClass="queryButton" CausesValidation="False"
												Text="Search Jobs"></asp:button></TD>
									</TR>
									<TR>
										<TD height="15" colSpan="6"><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT></TD>
									</TR>
									<tr>
										<td colSpan="6" align="left">
											<table border="0" cellSpacing="0" cellPadding="0" width="800">
												<tr>
													<td class="gridHeading"><STRONG><FONT size="2">Cheque Requisition Details</FONT></STRONG>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<TR>
										<td colSpan="6"><asp:datagrid style="Z-INDEX: 0" id="PackageDetails" tabIndex="42" runat="server" Width="800px"
												AutoGenerateColumns="False" ShowFooter="True" DataKeyField="Seq" AlternatingItemStyle-CssClass="gridField"
												ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridHeading" CellPadding="0" CellSpacing="1" BorderWidth="0px"
												FooterStyle-CssClass="gridHeading" AllowPaging="True">
												<FooterStyle CssClass="gridHeading"></FooterStyle>
												<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
												<ItemStyle CssClass="gridField"></ItemStyle>
												<HeaderStyle CssClass="gridHeading"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn HeaderText="Seq. No.">
														<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<%# DataBinder.Eval(Container.DataItem,"Seq") %>
														</ItemTemplate>
														<FooterStyle HorizontalAlign="Center"></FooterStyle>
														<FooterTemplate>
															<asp:LinkButton id="btnAddItem" Runat="server" Text="<img src='images/butt-add.gif' alt='add' border=0>"
																CommandName="ADD_ITEM" />
														</FooterTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Invoice No.">
														<HeaderStyle HorizontalAlign="Center" Width="150px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<%# DataBinder.Eval(Container.DataItem,"Invoice") %>
														</ItemTemplate>
														<FooterStyle HorizontalAlign="Center"></FooterStyle>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Client">
														<HeaderStyle HorizontalAlign="Center" Width="200px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<%# DataBinder.Eval(Container.DataItem,"Client") %>
														</ItemTemplate>
														<FooterStyle HorizontalAlign="Center"></FooterStyle>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Duty/Tax">
														<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<%# DataBinder.Eval(Container.DataItem,"DutyTax") %>
														</ItemTemplate>
														<FooterStyle HorizontalAlign="Center"></FooterStyle>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="EPF">
														<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<%# DataBinder.Eval(Container.DataItem,"EPF") %>
														</ItemTemplate>
														<FooterStyle HorizontalAlign="Center"></FooterStyle>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Total">
														<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<%# DataBinder.Eval(Container.DataItem,"Total") %>
														</ItemTemplate>
														<FooterStyle HorizontalAlign="Center"></FooterStyle>
													</asp:TemplateColumn>
												</Columns>
												<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
											</asp:datagrid></td>
									</TR>
								</table>
								<DIV></DIV>
							</td>
						</tr>
					</TBODY>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
