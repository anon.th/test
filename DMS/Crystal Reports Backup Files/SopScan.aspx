<%@ Page language="c#" Codebehind="SopScan.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.SopScan" %>
<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>SOP Scanning</title>
		<meta content="False" name="vs_snapToGrid">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script src="Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
		<script src="Scripts/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
		<script src="Scripts/jquery.json-2.2.js" type="text/javascript"></script>
		<script src="Scripts/jstorage.js" type="text/javascript"></script>
		<script src="Scripts/jquery.timers.js" type="text/javascript"></script>
		<script src="Scripts/SopScan.js" type="text/javascript"></script>
		<script type="text/javascript">
		  var waitService = false;
		  var w; 
		     $(function () {

		        $(".myActive").everyTime(2500, function () {
		           // alert("Hello");
		            if (waitService != true) {
		                waitService = !waitService;
		        		TestService();
		            }
		        });
		        $(".myActiveData").everyTime(1500, function () {
		           // alert("Hello");
		            viewdata();
		            
		        });
		        
		        $(".myFocus").everyTime(2000, function () {
					if(w=="" || (w!="txtVehicle" && w!="txtDriverName" &&  w!="txtScanDate" && w!="txtScanTo"))
						onfocus();
				});



		    });
		    
		    
		    function onfocus() {
		        document.getElementById('val').focus();
		    }
		    
        function TestService() {
		        var index = $.jStorage.index();
		        if (index.length > 0) {
		            var userID = document.getElementById('lbUserID').innerHTML;
		            
		            document.getElementById("txtVehicle").value
		            var truckNumber = document.getElementById('txtVehicle').value;
		            var RountOfTruck = document.getElementById('txtDriverName').value;
		            var ShipDate = document.getElementById('lbShipDate').innerHTML;
		            
		            var strVal = $.jStorage.get(index[0]);
		            
						$.ajax({
							url: serviceURL_Load,
							type: "POST",
							dataType: "json",
							data: "{Barcode:'" + strVal + "',UserID:'" + userID + "',TruckNumber:'" 
							+ truckNumber + "',DriverName:'" + RountOfTruck + "'}",
							contentType: "application/json; charset=utf-8",
							success: function (msg) {
			                    
								if (msg["d"]["ReturnVal"]) {
								    //alert(msg["d"]["ReturnVal"]);
									$.jStorage.deleteKey(index[0]);
									reDraw();
								} else {
									//alert(msg["d"]["ReturnVal"]);
									document.getElementById("lberrmsg").innerHTML = "Duplicate scanning data is not allowed!";
									$.jStorage.deleteKey(index[0]);
									reDraw();
								}
							},
							error: function (e) {
							}
						});

		        } else {
		            //document.getElementById("lberrmsg").innerHTML = "";
		        }

		        waitService = !waitService;
		    }			
		    
		     function viewdata() {
			//return;
			//alert("Hello");
				var fromdate = document.getElementById('txtScanDate').value;
				var d = fromdate.substring(0,2);
				var m = fromdate.substring(3,5);
				var y = fromdate.substring(6,10);
				//alert(y + "-" + m + "-" + d);
				fromdate = y + "-" + m + "-" + d;
				fromdate = fromdate.replace("/","-");
				var todate = document.getElementById('txtScanTo').value;
				 d = todate.substring(0,2);
				 m = todate.substring(3,5);
				 y = todate.substring(6,10);
				 todate = y + "-" + m + "-" + d;
				 
				var route ="";
	           
                $.ajax({
                    url: serviceURL_LoadData,
                    type: "POST",
                    dataType: "json",
                    data: "{fromdate:'"+fromdate+"',todate:'"+todate+"',Route:'"+route+"'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {					
                        $(".viewdata").replaceWith(msg["d"]["ResponseData"]);
                    },
                    error: function (e) {
                    }
                });
                
            }
            
            function initialonload(){
                //Set focus on scan val  
                document.getElementById("val").focus();
                //Set current date
                var vNow = null;
                var strDay = "";
                var strMonth = "";
                var strYear = "";
                vNow = new Date();
                strDay = '0' + vNow.getDate();
                strDay = strDay.substring(strDay.length - 2);
                
                strMonth = '0' + (vNow.getMonth() + 1);
                strMonth = strMonth.substring(strMonth.length - 2);
                
                strYear  = vNow.getFullYear();
                document.getElementById('txtScanDate').value = strDay + '/' + strMonth + '/' + strYear;
                document.getElementById('txtScanTo').value = strDay + '/' + strMonth + '/' + strYear;
                         
				var el = document.forms[0].elements;
				for(var i=0;i<el.length;i++){
					el[i].onfocus=function(){
						w=this.name
						}
					el[i].onblur=function(){
						w="";
						}
				}
				      
           
                SetScrollPosition();
                
            }   
            
		</script>
		<STYLE type="text/css">.MissCon THEAD {
	COLOR: red
}
.MissCon TBODY {
	COLOR: red
}
		</STYLE>
		<!---->
	</HEAD>
	<body onclick="GetScrollPosition();" onload="initialonload();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="ShipmentPendingCOD" method="post" runat="server">
			<asp:label id="Label1" style="Z-INDEX: 101; LEFT: 20px; POSITION: absolute; TOP: 30px" runat="server"
				Height="27px" CssClass="maintitleSize" Width="365px"> SOP Scaning</asp:label><asp:label id="lblErrorMessage" style="Z-INDEX: 103; LEFT: 16px; POSITION: absolute; TOP: 40px"
				runat="server" Height="17px" CssClass="errorMsgColor" Width="680px"></asp:label>&nbsp;
			<label id="lberrmsg" style="FONT-SIZE: 20px; Z-INDEX: 103; LEFT: 200px; COLOR: red; POSITION: absolute; TOP: 35px">
			</label>&nbsp;
			<TABLE id="tblShipmentTracking" style="Z-INDEX: 102; LEFT: 16px; WIDTH: 688px; POSITION: absolute; TOP: 72px"
				width="688" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 100%" vAlign="top">
						<FIELDSET style="WIDTH: 100%"><LEGEND><asp:label id="Label6" runat="server" CssClass="maintitleSize" Width="104px" Font-Bold="True">Scan Data</asp:label></LEGEND>
							<TABLE id="Table1" style="WIDTH: 600px" cellSpacing="0" cellPadding="0" align="left" border="0"
								runat="server">
								<TR>
									<TD style="WIDTH: 188px"><asp:label id="Label5" runat="server" Height="8px" CssClass="tableLabel" Width="156px" Font-Bold="True"
											Font-Size="10pt">&nbsp;&nbsp; Vehicle#:</asp:label></TD>
									<td><asp:textbox id="txtVehicle" runat="server" Width="189px" Font-Size="10pt"></asp:textbox><asp:label id="lbShipDate" style="DISPLAY: none" runat="server">ShipDate</asp:label></td>
								</TR>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<TR>
									<TD style="WIDTH: 188px"><asp:label id="Label4" runat="server" Height="12px" CssClass="tableLabel" Width="176px" Font-Bold="True"
											Font-Size="10pt">&nbsp;&nbsp;Driver Name:</asp:label></TD>
									<td><asp:textbox id="txtDriverName" runat="server" Width="189px" Font-Size="10pt"></asp:textbox></td>
								</TR>
								<tr>
									<td>&nbsp;</td>
								</tr>
							</TABLE>
							<asp:label id="lbUserID" style="DISPLAY: none" runat="server" CssClass="tableLabel">UserID</asp:label></FIELDSET>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 100%" vAlign="top">
						<TABLE style="WIDTH: 95%" cellSpacing="0" cellPadding="0" align="center">
							<THEAD>
								<tr>
									<td style="WIDTH: 733px" align="center"><asp:label id="Label2" runat="server" CssClass="maintitleSize" Width="160px" Font-Bold="True"
											ForeColor="Black"> SOP Barcode</asp:label></td>
									<td style="WIDTH: 49px" width="49">&nbsp;</td>
								</tr>
							</THEAD>
							<tbody id="tulemused">
							</tbody>
							<tfoot>
								<tr align="center">
									<td style="WIDTH: 733px" width="733"></INPUT><asp:textbox id="val" runat="server" Width="558px" Font-Size="15pt"></asp:textbox></td>
									<td style="WIDTH: 49px" align="right"><input class="searchButton" id="btnAdd" style="WIDTH: 75px; HEIGHT: 29px" onclick="insert_value()"
											type="button" value="Add">
									</td>
								</tr>
							</tfoot>
						</TABLE>
						<script>reDraw()</script>
						<script src="Scripts/events.js" type="text/javascript"></script>
						<script>
						$('#val').keydown(function (event) {

							if (event.keyCode == '9' || event.keyCode == '13' ) {
								event.preventDefault();
								insert_value();
							}

						});
						
						
						</script>
					</TD>
				</TR>
				<tr>
					<td>
						<FIELDSET style="WIDTH: 100%"><LEGEND><asp:label id="Label10" runat="server" CssClass="maintitleSize" Width="168px" Font-Bold="True">SOP Missing</asp:label></LEGEND>
							<TABLE id="Table2" style="WIDTH: 600px" cellSpacing="0" cellPadding="0" align="left" border="0"
								runat="server">
								<TR>
									<TD style="WIDTH: 188px"><asp:label id="Label9" runat="server" Height="8px" CssClass="tableLabel" Width="156px" Font-Bold="True"
											Font-Size="10pt"> &nbsp;&nbsp;Scan Date:</asp:label></TD>
									<TD><asp:label id="Label8" style="DISPLAY: none" runat="server">ShipDate</asp:label><cc1:mstextbox id="txtScanDate" runat="server" CssClass="textField" Width="155px" Font-Size="10pt"
											MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;
										<asp:label id="Label11" runat="server" Height="8px" CssClass="tableLabel" Width="35px" Font-Bold="True"
											Font-Size="12px">to</asp:label><cc1:mstextbox id="txtScanTo" runat="server" CssClass="textField" Width="155px" Font-Size="10pt"
											MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
								</TR>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<TR style="DISPLAY: none">
									<TD style="WIDTH: 188px"><asp:label id="Label7" runat="server" Height="12px" CssClass="tableLabel" Width="176px" Font-Bold="True"
											Font-Size="10pt"> &nbsp;&nbsp;Route Code:</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" Height="2px" CssClass="tableLabel" Width="112px"
											ServerMethod="DbComboPathCodeSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" "
											TextBoxColumns="18" Runat="server" AutoPostBack="false"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<tr>
									<td>&nbsp;</td>
								</tr>
							</TABLE>
							<asp:label id="Label3" style="DISPLAY: none" runat="server" CssClass="tableLabel">UserID</asp:label></FIELDSET>
					</td>
				</tr>
				<tr>
					<td>
						<table class="MissCon" style="FONT-SIZE: 15pt; WIDTH: 100%" align="center" border="0">
							<thead>
								<TR>
									<td align="center"><STRONG>Consignment#</STRONG></td>
									<td align="center"><STRONG>Delivery Route</STRONG></td>
									<td align="center"><STRONG>Total Pkg.</STRONG></td>
									<td align="center"><STRONG>Missing Pkg.</STRONG></td>
								</TR>
							</thead>
							<TBODY class="viewdata">
								
							</TBODY>
						</table>
					</td>
				</tr>
			</TABLE>
			<DIV></DIV>
			<DIV class="myActive"></DIV>
			<DIV class="myFocus"></DIV>
			<DIV class="myActiveData"><FONT face="Tahoma"></FONT></DIV>
		</form>
	</body>
</HTML>
