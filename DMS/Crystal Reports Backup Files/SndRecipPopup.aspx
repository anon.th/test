<%@ Page language="c#" Codebehind="SndRecipPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.SndRecipPopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Sender/RecipientPopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="SndRecipPopup" method="post" runat="server">
			<asp:datagrid id="dgSndRecpMaster" style="Z-INDEX: 100; POSITION: absolute; TOP: 96px; LEFT: 45px"
				runat="server" AllowCustomPaging="True" CssClass="gridHeading" BorderStyle="None" BorderWidth="1px"
				CellPadding="3" BackColor="White" BorderColor="#CCCCCC" AutoGenerateColumns="False" Width="600px"
				AllowPaging="True" OnSelectedIndexChanged="dgSndRecpMaster_SelectedIndexChanged" OnPageIndexChanged="Paging">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" CssClass="drilldownFieldSelected"></SelectedItemStyle>
				<ItemStyle CssClass="drilldownField"></ItemStyle>
				<HeaderStyle Width="10%" CssClass="drilldownHeading"></HeaderStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="snd_rec_name" HeaderText="Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="address1" HeaderText="Address 1" ItemStyle-Wrap="True"></asp:BoundColumn>
					<asp:BoundColumn DataField="address2" HeaderText="Address 2" ItemStyle-Wrap="True"></asp:BoundColumn>
					<asp:BoundColumn DataField="country" HeaderText="Country"></asp:BoundColumn>
					<asp:BoundColumn DataField="zipcode" HeaderText="Postal Code" HeaderStyle-Width="100px"></asp:BoundColumn>
					<asp:BoundColumn DataField="telephone" HeaderText="telephone" Visible="False"></asp:BoundColumn>
					<asp:BoundColumn DataField="fax" HeaderText="fax" Visible="False"></asp:BoundColumn>
					<asp:BoundColumn DataField="contact_person" HeaderText="contact_person" Visible="False"></asp:BoundColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select"></asp:ButtonColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" ForeColor="#000066"
					BackColor="White" CssClass="normalText"></PagerStyle>
			</asp:datagrid>
			<asp:button id="btnClose" style="Z-INDEX: 107; POSITION: absolute; TOP: 63px; LEFT: 377px" runat="server"
				Width="78" CssClass="buttonProp" Height="21" Font-Bold="True" CausesValidation="False" Text="Close"></asp:button><asp:textbox id="txtName" style="Z-INDEX: 106; POSITION: absolute; TOP: 36px; LEFT: 167px" runat="server"
				CssClass="textField" Width="116px" Height="19px"></asp:textbox><asp:label id="lblZipCode" style="Z-INDEX: 105; POSITION: absolute; TOP: 34px; LEFT: 56px"
				runat="server" CssClass="tablelabel">Name</asp:label><asp:textbox id="txtZipCode" style="Z-INDEX: 103; POSITION: absolute; TOP: 62px; LEFT: 167px"
				runat="server" CssClass="textField" Width="116px" Height="19px"></asp:textbox><asp:label id="Label1" style="Z-INDEX: 104; POSITION: absolute; TOP: 62px; LEFT: 54px" runat="server"
				CssClass="tablelabel">Postal Code</asp:label>
			<asp:button id="btnSearch" style="Z-INDEX: 102; POSITION: absolute; TOP: 63px; LEFT: 290px"
				runat="server" Width="78" CssClass="buttonProp" Height="21" Text="Search" CausesValidation="False"
				Font-Bold="True"></asp:button></form>
	</body>
</HTML>
