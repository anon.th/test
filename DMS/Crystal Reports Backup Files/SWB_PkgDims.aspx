<%@ Page language="c#" Codebehind="SWB_PkgDims.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.SWB_PkgDims" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Scanning Workbench Emulator</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/Styles.css" type="text/css" rel="stylesheet">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="../Scripts/JScript_TextControl.js"></script>
		<script language="javascript">	
			function KeyDownHandler()
			{
			// process only the Enter key
				if (event.keyCode == 13)
				{
					event.returnValue=false;
					event.cancel = true;
					// submit the form by programmatically clicking the specified button
					if(document.getElementById("btnInsert").disabled==false)
					{
						//btn = "btnInsert";
						document.getElementById("btnInsert").click();
						//document.getElementById("txtConNo").focus();
					}
					else if(document.getElementById("btnUpdate").disabled==false)
					{
						//btn = "btnUpdate";
						document.getElementById("btnUpdate").click();
						//document.getElementById("txtConNo").focus();
					}
					
				}
			}
			function KeyDownExe()
			{
				if (event.keyCode == 9 && event.keyCode == 13)
				{
					document.getElementById("btnExcuteQuery").click();
					//document.getElementById("txtConNo").focus();
				}
			}
			
function  suppressNonEng(e)
 {
   var key;
   if(window.event)  key = window.event.keyCode;     //IE
   else  key = e.which;     //firefox

   if(key >128)  return false;
   else  return true;
 }		
 function suppressOnBluer(e)
 {//debugger;
	//e.value = Check2FontChar2LastChar(e);
     var s= e.value;
     e.value = '';
     for(i=0;i<s.length;i++)
          if (s.charCodeAt(i)<=128)
          {

           e.value += s.charAt(i);
             }
             if(s.length!=e.value.length)
             {
             //alert('Other than english character are not supported')
             return false;
             }
	__doPostBack('hddConNo','');
 }	
		
		</script>
	</HEAD>
	<BODY onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<FORM id="SWB_PkgDims" method="post" runat="server">
			<SCRIPT language="javascript" src="CSS_files/WebUIValidation.js" type="text/javascript"></SCRIPT>
			<input id="hddConNo" style="DISPLAY: none" type="button" name="hddCon" runat="server">
			<P><FONT face="Tahoma"></FONT>&nbsp;</P>
			<P><asp:label id="Header1" style="Z-INDEX: 101; POSITION: absolute; TOP: 23px; LEFT: 14px" Width="578px"
					CssClass="mainTitleSize" Runat="server">Scanning Workbench Emulator - Package Weights and Dims</asp:label></P>
			<P></P>
			<P><FONT face="Tahoma"></FONT>&nbsp;</P>
			<P></P>
			<asp:label id="lblErrrMsg" style="Z-INDEX: 102; POSITION: absolute; TOP: 88px; LEFT: 24px"
				Width="720px" CssClass="errorMsgColor" Runat="server" HEIGHT="2px"></asp:label><INPUT style="Z-INDEX: 103; POSITION: absolute; TOP: 560px; LEFT: 232px" type="hidden"
				name="ScrollPosition">
			<FIELDSET style="Z-INDEX: 107; POSITION: absolute; WIDTH: 815px; HEIGHT: 263px; TOP: 120px; LEFT: 8px"><LEGEND><asp:label id="lblPackageWeightsAndDims" runat="server" Width="155px" CssClass="tableHeadingFieldset"
						Height="18px">Package 
								Weights and Dims</asp:label></LEGEND>
				<table style="WIDTH: 100%" cellSpacing="0" cellPadding="0">
					<tr>
						<td style="WIDTH: 496px; HEIGHT: 106px" vAlign="top">
							<TABLE id="Table10">
								<TR>
									<TD>&nbsp;</TD>
									<TD style="WIDTH: 110px" vAlign="bottom"><asp:label id="lblConNo" runat="server" Width="136px" CssClass="tableLabel">Consignment Number:</asp:label></TD>
									<TD style="WIDTH: 203px" vAlign="bottom"><asp:textbox onkeypress="return suppressNonEng(event)" id="txtConNo" onkeydown="return (event.keyCode!=13);"
											onblur="suppressOnBluer(this)" style="TEXT-TRANSFORM: uppercase" runat="server" Width="172px" CssClass="tableTextbox" AutoPostBack="false"
											MaxLength="22"></asp:textbox></TD>
									<TD vAlign="bottom"><asp:label id="lblUser" runat="server" CssClass="tableLabel">User:</asp:label></TD>
									<TD vAlign="bottom"><asp:label id="lblUserDisplay" runat="server" Width="40px" CssClass="tableLabel"></asp:label></TD>
								</TR>
								<TR>
									<TD>&nbsp;</TD>
									<TD style="WIDTH: 110px" vAlign="bottom">&nbsp;</TD>
									<TD style="WIDTH: 203px" vAlign="bottom">&nbsp;</TD>
									<TD vAlign="bottom"><asp:label id="lblLocation" runat="server" CssClass="tableLabel">Location:</asp:label></TD>
									<TD vAlign="bottom"><asp:label id="lblLocationDisplay" runat="server" Width="56px" CssClass="tableLabel"></asp:label></TD>
								</TR>
								<tr>
									<TD>&nbsp;</TD>
									<TD style="WIDTH: 110px" vAlign="bottom"><asp:label id="lblConsignmentCount" runat="server" Width="128px" CssClass="tableLabel">Consignment Count:</asp:label></TD>
									<TD vAlign="bottom" colSpan="3"><asp:label id="lblConsignmentCountDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
								</tr>
								<tr>
									<TD style="HEIGHT: 20px">&nbsp;</TD>
									<TD style="WIDTH: 110px; HEIGHT: 20px" vAlign="bottom"><asp:label id="lblPackageDetailsCount" runat="server" Width="136px" CssClass="tableLabel">Package Details Count:</asp:label></TD>
									<TD style="HEIGHT: 20px" vAlign="bottom" colSpan="3"><asp:label id="lblPackageDetailsCountDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
								</tr>
								<tr>
									<TD>&nbsp;</TD>
									<TD style="WIDTH: 110px" vAlign="bottom"><asp:label id="lblCurrentDateTime" runat="server" Width="120px" CssClass="tableLabel">Current Date/Time:</asp:label></TD>
									<TD vAlign="bottom" colSpan="3"><asp:label id="lblCurrentDateTimeDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
								</tr>
							</TABLE>
						</td>
						<td style="WIDTH: 350px; HEIGHT: 106px" vAlign="top">
							<TABLE id="Table11" style="WIDTH: 449px; HEIGHT: 88px">
								<TR>
									<TD style="WIDTH: 100px" vAlign="bottom"><asp:label id="lblMPSNum" runat="server" CssClass="tableLabel">MPS Number:</asp:label></TD>
									<TD style="WIDTH: 75px" vAlign="bottom"><cc1:mstextbox id="txtMPSNo" tabIndex="1" runat="server" Width="60px" CssClass="tableTextbox" NumberPrecision="3"
											NumberMaxValue="999" TextMaskType="msNumeric"></cc1:mstextbox></TD>
									<TD vAlign="bottom"><asp:label id="lblLength" runat="server" CssClass="tableLabel">Length:</asp:label></TD>
									<TD style="WIDTH: 75px" vAlign="bottom"><cc1:mstextbox id="txtLength" tabIndex="4" runat="server" Width="60px" CssClass="tableTextbox"
											NumberPrecision="6" TextMaskType="msNumericCOD" NumberScale="2" NumberMaxValueCOD="9999.99"></cc1:mstextbox></TD>
									<TD vAlign="bottom"><asp:button id="btnInsert" tabIndex="7" runat="server" Width="94px" CssClass="queryButton" CausesValidation="True"
											Text="Insert"></asp:button></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 100px; HEIGHT: 27px" vAlign="bottom"><asp:label id="lblQuantity" runat="server" CssClass="tableLabel">Quantity:</asp:label></TD>
									<TD style="WIDTH: 75px; HEIGHT: 27px" vAlign="bottom"><cc1:mstextbox id="txtQuantity" tabIndex="2" runat="server" Width="60px" CssClass="tableTextbox"
											AutoPostBack="True" NumberPrecision="3" NumberMaxValue="999" TextMaskType="msNumeric"></cc1:mstextbox></TD>
									<TD style="HEIGHT: 27px" vAlign="bottom"><asp:label id="lblBreadth" runat="server" CssClass="tableLabel">Breadth:</asp:label></TD>
									<TD style="WIDTH: 75px; HEIGHT: 27px" vAlign="bottom"><cc1:mstextbox id="txtBreadth" tabIndex="5" runat="server" Width="60px" CssClass="tableTextbox"
											NumberPrecision="6" TextMaskType="msNumericCOD" NumberScale="2" NumberMaxValueCOD="9999.99"></cc1:mstextbox></TD>
									<TD style="HEIGHT: 27px" vAlign="bottom"><asp:button id="btnUpdate" tabIndex="8" runat="server" Width="94px" CssClass="queryButton" CausesValidation="False"
											Text="Update"></asp:button></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 100px" vAlign="bottom"><asp:label id="lblWeight" runat="server" CssClass="tableLabel">Weight:</asp:label></TD>
									<TD style="WIDTH: 75px" vAlign="bottom">
										<P><FONT face="Tahoma"><cc1:mstextbox id="txtWeight" tabIndex="3" runat="server" Width="60px" CssClass="tableTextbox"
													NumberPrecision="6" TextMaskType="msNumericCOD" NumberMinValue="0" Enabled="True" NumberMaxValueCOD="9999.99"
													NumberScale="2"></cc1:mstextbox></FONT></P>
									</TD>
									<TD vAlign="bottom"><asp:label id="lblHeight" runat="server" CssClass="tableLabel">Height:</asp:label></TD>
									<TD style="WIDTH: 75px" vAlign="bottom"><cc1:mstextbox id="txtHeight" tabIndex="6" runat="server" Width="60px" CssClass="tableTextbox"
											NumberPrecision="6" TextMaskType="msNumericCOD" NumberScale="2" NumberMaxValueCOD="9999.99"></cc1:mstextbox></TD>
									<TD vAlign="bottom"><asp:button id="btnDelete" tabIndex="9" runat="server" Width="94px" CssClass="queryButton" CausesValidation="False"
											Text="Delete"></asp:button></TD>
								</TR>
								<tr>
									<td colSpan="5"><asp:checkbox id="chkCWE" CssClass="tableLabel" Runat="server" AutoPostBack="True" Text="Consignment Weight Entered"
											Visible="False"></asp:checkbox></td>
								</tr>
							</TABLE>
						</td>
					</tr>
					<tr>
						<td style="WIDTH: 496px" vAlign="top">
							<TABLE id="Table16" style="WIDTH: 480px; HEIGHT: 159px">
								<TBODY class="tableLabel">
									<TR>
										<TD style="WIDTH: 45px; HEIGHT: 1px"><FONT face="Tahoma"></FONT></TD>
										<TD style="WIDTH: 183px; HEIGHT: 1px"><asp:label id="lblColConsignment" runat="server" CssClass="tableLabel">Consignment</asp:label></TD>
										<TD style="WIDTH: 53px; HEIGHT: 1px" align="left"><FONT face="Tahoma"></FONT><asp:label id="lblColServiceType" runat="server" CssClass="tableLabel">Service<br>Type</asp:label></TD>
										<TD style="WIDTH: 80px; HEIGHT: 1px"><asp:label id="lblColDestPostCode" runat="server" CssClass="tableLabel">Destination<br>Postal Code</asp:label></TD>
										<TD style="WIDTH: 40px; HEIGHT: 1px" align="left"><asp:label id="lblColPkgs" runat="server" CssClass="tableLabel">Pkgs.</asp:label></TD>
										<TD style="WIDTH: 134px; HEIGHT: 1px" align="left"><asp:label id="lblColSIPDateTime" runat="server" CssClass="tableLabel">SIP Date/ Time</asp:label></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 45px; HEIGHT: 100px"><FONT face="Tahoma"></FONT></TD>
										<TD class="style1" style="HEIGHT: 100px" vAlign="top" colSpan="5">
											<DIV style="WIDTH: 463px; HEIGHT: 110px; OVERFLOW: auto" align="left"><asp:listbox id="lbSIPCon" runat="server" Width="456px" Height="105px" AutoPostBack="True" Font-Names="Courier New"></asp:listbox><br>
											</DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
						</td>
						<td vAlign="top">
							<TABLE id="Table13" style="WIDTH: 448px; HEIGHT: 159px">
								<TBODY class="tableLabel">
									<TR>
										<TD style="WIDTH: 812px; HEIGHT: 1px"><asp:label id="lblColConsignment02" runat="server" CssClass="tableLabel">Consignment</asp:label></TD>
										<TD style="WIDTH: 107px; HEIGHT: 1px" align="center"><asp:label id="lblColMPSNo" runat="server" CssClass="tableLabel">MPS<br>No.</asp:label></TD>
										<TD style="WIDTH: 85px; HEIGHT: 1px" align="center"><asp:label id="lblColQty" runat="server" CssClass="tableLabel">Qty</asp:label></TD>
										<TD style="WIDTH: 49px; HEIGHT: 1px"><asp:label id="lblColWeight" runat="server" CssClass="tableLabel">Weight</asp:label></TD>
										<TD style="WIDTH: 108px; HEIGHT: 1px" align="right"><asp:label id="lblColLength" runat="server" CssClass="tableLabel">Length</asp:label></TD>
										<TD style="WIDTH: 101px; HEIGHT: 1px" align="right"><asp:label id="lblColBreadth" runat="server" CssClass="tableLabel">Breadth</asp:label></TD>
										<TD style="WIDTH: 134px; HEIGHT: 1px" align="center"><asp:label id="lblColHeight" runat="server" CssClass="tableLabel">Height</asp:label></TD>
									</TR>
									<TR>
										<TD class="style1" vAlign="top" colSpan="7">
											<DIV style="WIDTH: 440px; HEIGHT: 110px; OVERFLOW: auto"><asp:listbox id="lbSIPConDetail" runat="server" Width="440px" Height="105px" AutoPostBack="True"
													Font-Names="Courier New"></asp:listbox><br>
											</DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
						</td>
					</tr>
				</table>
			</FIELDSET>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<asp:validationsummary id="reqSummary" style="Z-INDEX: 106; POSITION: absolute; TOP: 560px; LEFT: 24px"
				Runat="server" ShowSummary="False" HeaderText="Please enter the missing  fields." ShowMessageBox="True"
				DisplayMode="BulletList"></asp:validationsummary><asp:button id="btnQuery" style="Z-INDEX: 104; POSITION: absolute; TOP: 56px; LEFT: 16px" runat="server"
				Width="104px" CssClass="queryButton" CausesValidation="False" Text="Query"></asp:button><asp:button id="btnExcuteQuery" style="Z-INDEX: 105; POSITION: absolute; TOP: 56px; LEFT: 128px"
				runat="server" Width="122px" CssClass="queryButton" Text="Execute Query"></asp:button></FORM>
	</BODY>
</HTML>
