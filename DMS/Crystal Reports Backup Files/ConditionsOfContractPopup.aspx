<%@ Page language="c#" Codebehind="ConditionsOfContractPopup.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ZipcodePopup" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ZipcodePopup</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript">
			function UpperLetterOnlyMask( toField )
			{
				var lcNewChar = String.fromCharCode(window.event.keyCode);
				var llRetVal = ((lcNewChar >= 'A' && lcNewChar <= 'Z') || (lcNewChar >= 'a' && lcNewChar <= 'z'));
				window.event.keyCode = lcNewChar.toUpperCase().charCodeAt(0);
				return llRetVal;
			}
			
			function UpperMaskSpecialWithHyphen(toField)
			{
				var lcNewChar = String.fromCharCode(window.event.keyCode);
				var llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9')||(lcNewChar >= 'A' && lcNewChar <= 'Z') || (lcNewChar >= 'a' && lcNewChar <= 'z') || (lcNewChar == '-'));
				window.event.keyCode = lcNewChar.toUpperCase().charCodeAt(0);
				return llRetVal;
			}

		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="ZipcodePopup" method="post" runat="server">
			<asp:datagrid style="Z-INDEX: 101; POSITION: absolute; TOP: 110px; LEFT: 45px" id="dgZipcode"
				runat="server" OnPageIndexChanged="Paging" AllowPaging="True" Width="522px" AutoGenerateColumns="False"
				BorderColor="#CCCCCC" BackColor="White" CellPadding="3" BorderWidth="1px" BorderStyle="None"
				CssClass="gridHeading" AllowCustomPaging="True">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
				<ItemStyle CssClass="drilldownField"></ItemStyle>
				<HeaderStyle Width="10%" CssClass="drilldownHeading"></HeaderStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="zipcode" HeaderText="Postal Code" ItemStyle-Width="90"></asp:BoundColumn>
					<asp:BoundColumn DataField="state_name" HeaderText="State" ItemStyle-Width="250"></asp:BoundColumn>
					<asp:BoundColumn DataField="country" HeaderText="Country" ItemStyle-Width="140"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="esa_surcharge" HeaderText="esa_surcharge"></asp:BoundColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select" ItemStyle-Width="40"></asp:ButtonColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" ForeColor="#000066"
					BackColor="White"></PagerStyle>
			</asp:datagrid><asp:label style="Z-INDEX: 106; POSITION: absolute; TOP: 59px; LEFT: 50px" id="lblVASCode"
				runat="server" CssClass="tablelabel">Postal Code</asp:label>
			<asp:button style="Z-INDEX: 102; POSITION: absolute; TOP: 83px; LEFT: 308px" id="btnSearch"
				runat="server" Width="84px" CssClass="buttonProp" Height="21px" Text="Search" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 100; POSITION: absolute; TOP: 83px; LEFT: 404px" id="btnOk" runat="server"
				Width="84px" CssClass="buttonProp" Height="21px" Text="Close" CausesValidation="False"></asp:button><asp:label style="Z-INDEX: 103; POSITION: absolute; TOP: 82px; LEFT: 51px" id="lblCountry"
				runat="server" Width="94px" CssClass="tableLabel">Country</asp:label>
			<asp:TextBox id="txtCountry" style="Z-INDEX: 105; POSITION: absolute; TOP: 83px; LEFT: 156px"
				runat="server" Width="139px" CssClass="textField" Height="19px" tabIndex="1"></asp:TextBox>
			<asp:TextBox id="txtZipcode" style="Z-INDEX: 103; POSITION: absolute; TOP: 59px; LEFT: 156px"
				runat="server" CssClass="textField" Width="139px" MaxLength="10"></asp:TextBox>
		</form>
	</body>
</HTML>
