<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="CostCode.aspx.cs" AutoEventWireup="false" Inherits="com.ties.CostCode" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CostCode</title>
		<link href="css/styles.css" rel="stylesheet" rev="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="CostCode" method="post" runat="server">
			<asp:Label id="Label1" style="Z-INDEX: 101; LEFT: 20px; POSITION: absolute; TOP: 6px" runat="server" Width="330px" Height="32px" CssClass="mainTitleSize">Cost Code</asp:Label>
			<asp:Label id="lblErrorMessage" style="Z-INDEX: 107; LEFT: 24px; POSITION: absolute; TOP: 71px" runat="server" Width="656px" CssClass="errorMsgColor">Error Message</asp:Label>
			<asp:validationsummary id="PageValidationSummary" style="Z-INDEX: 105; LEFT: 728px; POSITION: absolute; TOP: 152px" runat="server" ShowMessageBox="True" ShowSummary="False" DisplayMode="BulletList" HeaderText="Please enter the missing  fields."></asp:validationsummary>
			<asp:Button id="btnInsert" style="Z-INDEX: 104; LEFT: 213px; POSITION: absolute; TOP: 41px" runat="server" Text="Insert" CausesValidation="False" CssClass="queryButton"></asp:Button>
			<asp:Button id="btnExecuteQuery" style="Z-INDEX: 103; LEFT: 84px; POSITION: absolute; TOP: 41px" runat="server" Text="Execute Query" CausesValidation="False" Width="130px" CssClass="queryButton"></asp:Button>
			<asp:Button id="btnQuery" style="Z-INDEX: 102; LEFT: 22px; POSITION: absolute; TOP: 41px" runat="server" Text="Query" CausesValidation="False" Width="63px" CssClass="queryButton"></asp:Button>
			<asp:DataGrid id="dgCostCode" runat="server" AllowCustomPaging="True" AllowPaging="True" AutoGenerateColumns="False" style="Z-INDEX: 105; LEFT: 21px; POSITION: absolute; TOP: 98px" HorizontalAlign="Left" Width="525px" OnEditCommand="dgCostCode_Edit" OnCancelCommand="dgCostCode_Cancel" OnItemDataBound="dgCostCode_ItemDataBound" OnDeleteCommand="dgCostCode_Delete" OnUpdateCommand="dgCostCode_Update" OnPageIndexChanged="dgCostCode_PageChange">
				<ItemStyle HorizontalAlign="Left" CssClass="gridfield"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="0.5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
						<HeaderStyle Width="0.5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Cost Code" HeaderStyle-Font-Bold="true">
						<HeaderStyle HorizontalAlign="Left" Width="2%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblCostCode" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cost_code")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtCostCode" Text='<%#DataBinder.Eval(Container.DataItem,"cost_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumeric" MaxLength="12">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="cCostCode" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtCostCode" ErrorMessage="Cost Code "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Cost Code Description">
						<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblCCDescp" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"cost_code_description")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtCCDescp" CssClass="gridTextBox" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cost_code_description")%>' MaxLength="200">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Cost Type">
						<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblCostType" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"cost_type")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtCostType" CssClass="gridTextBox" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"cost_type")%>' MaxLength="100">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" CssClass="normalText" Position="Bottom" HorizontalAlign="Right"></PagerStyle>
			</asp:DataGrid>
		</form>
	</body>
</HTML>
