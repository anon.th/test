<%@ Page language="c#" Codebehind="AgentCustomableCharge.aspx.cs" AutoEventWireup="false" Inherits="com.ties.AgentCustomableCharge" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AgentCustomableCharge</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"--><LINK rev="stylesheet" href="css/styles.css" rel="stylesheet">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="AgentCustomableCharge" method="post" runat="server">
			<asp:label id="lblAgentCode" style="Z-INDEX: 113; LEFT: 27px; POSITION: absolute; TOP: 108px" runat="server" CssClass="tableLabel" Width="100px">Agent Code</asp:label><asp:label id="lblServiceMessage" style="Z-INDEX: 115; LEFT: 26px; POSITION: absolute; TOP: 86px" runat="server" ForeColor="Red"></asp:label><asp:datagrid id="dgCustomizedCode" style="Z-INDEX: 114; LEFT: 25px; POSITION: absolute; TOP: 142px" runat="server" Width="707px" OnUpdateCommand="dgCustomizedCode_Update" OnCancelCommand="dgCustomizedCode_Cancel" OnItemCommand="dgCustomizedCode_Button" AutoGenerateColumns="False" OnDeleteCommand="dgCustomizedCode_Delete" OnPageIndexChanged="dgCustomizedCode_PageChange" ItemStyle-Height="20" AllowPaging="True" PageSize="5" AllowCustomPaging="True" OnItemDataBound="dgCustomizedCode_Bound" OnSelectedIndexChanged="dgCustomizedCode_SelectedIndexChanged" SelectedItemStyle-CssClass="gridFieldSelected">
				<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
				<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Description">
						<HeaderStyle Font-Bold="True" Width="19%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblCustDescription" Text='<%#DataBinder.Eval(Container.DataItem,"code_text")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtCustDescription" Text='<%#DataBinder.Eval(Container.DataItem,"code_text")%>' Runat="server" Enabled="True" Readonly="True" TextMaskType="msUpperAlfaNumericWithUnderscore">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="" HeaderStyle-Width="0%">
						<HeaderStyle Font-Bold="True" Width="0%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label Width="0%" CssClass="gridLabel" ID="lblCustomizedCode" Runat="server"></asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox Width="0%" CssClass="gridTextBox" ID="txtCustomizedCode" Text='<%#DataBinder.Eval(Container.DataItem,"customized_assembly_code")%>' Readonly="True" Runat="server" Enabled="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="search">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Assembly Code">
						<HeaderStyle Width="23%" CssClass="gridHeading"></HeaderStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblAssemblyid" Text='<%#DataBinder.Eval(Container.DataItem,"assemblyid")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtAssemblyid" Text='<%#DataBinder.Eval(Container.DataItem,"assemblyid")%>' Runat="server" MaxLength="200" ReadOnly="True">
							</asp:TextBox>
							<asp:RequiredFieldValidator id="RequiredFieldValidator1" ControlToValidate="txtAssemblyid" style="Z-INDEX: 116; LEFT: 47px; POSITION: absolute; TOP: 326px" runat="server" ErrorMessage="assembly code is required" Display="None"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Description">
						<HeaderStyle Width="20%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblDescription" CssClass="gridlabel" Text='<%#DataBinder.Eval(Container.DataItem,"Assembly_description")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtDescription" Text='<%#DataBinder.Eval(Container.DataItem,"Assembly_description")%>' Runat="server" Enabled="True">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><dbcombo:dbcombo id="dbCmbAgentId" style="Z-INDEX: 112; LEFT: 138px; POSITION: absolute; TOP: 102px" tabIndex="1" runat="server" Width="140px" Height="17px" ReQueryOnLoad="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentIdServerMethod" TextBoxColumns="20" AutoPostBack="True"></dbcombo:dbcombo><dbcombo:dbcombo id="dbCmbAgentName" style="Z-INDEX: 111; LEFT: 490px; POSITION: absolute; TOP: 102px" tabIndex="2" runat="server" Width="182px" Height="17px" ReQueryOnLoad="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentNameServerMethod" TextBoxColumns="25" AutoPostBack="True"></dbcombo:dbcombo><asp:label id="lblAgentName" style="Z-INDEX: 108; LEFT: 386px; POSITION: absolute; TOP: 110px" runat="server" CssClass="tableLabel" Width="100px">Agent Name</asp:label><asp:button id="btnGoToFirstPage" style="Z-INDEX: 109; LEFT: 608px; POSITION: absolute; TOP: 48px" tabIndex="5" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text="|<"></asp:button><asp:button id="btnPreviousPage" style="Z-INDEX: 104; LEFT: 632px; POSITION: absolute; TOP: 48px" tabIndex="6" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text="<"></asp:button><asp:textbox id="txtCountRec" style="Z-INDEX: 107; LEFT: 656px; POSITION: absolute; TOP: 48px" tabIndex="7" runat="server" Width="24px" Height="19px"></asp:textbox><asp:button id="btnNextPage" style="Z-INDEX: 105; LEFT: 680px; POSITION: absolute; TOP: 48px" tabIndex="8" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text=">"></asp:button><asp:button id="btnGoToLastPage" style="Z-INDEX: 106; LEFT: 704px; POSITION: absolute; TOP: 48px" tabIndex="9" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text=">|"></asp:button><asp:button id="btnInsertVAS" style="Z-INDEX: 110; LEFT: 214px; POSITION: absolute; TOP: 48px" runat="server" CssClass="queryButton" Width="57px" CausesValidation="False" Text="Insert"></asp:button><asp:button id="btnExecQry" style="Z-INDEX: 102; LEFT: 84px; POSITION: absolute; TOP: 48px" runat="server" CssClass="queryButton" Width="130px" CausesValidation="False" Text="Execute Query"></asp:button><asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 22px; POSITION: absolute; TOP: 48px" runat="server" CssClass="queryButton" Width="62px" CausesValidation="False" Text="Query"></asp:button><asp:label id="lblTitle" style="Z-INDEX: 103; LEFT: 24px; POSITION: absolute; TOP: 8px" runat="server" CssClass="mainTitleSize" Width="336px" Height="32px">Agent Customize Functions</asp:label>
			<asp:ValidationSummary id="ValidationSummary1" style="Z-INDEX: 116; LEFT: 41px; POSITION: absolute; TOP: 322px" runat="server" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary></form>
	</body>
</HTML>
