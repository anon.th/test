<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="StatusException.aspx.cs" AutoEventWireup="false" Inherits="com.ties.StatusException" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>StatusException</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<FORM id="StatusException" method="post" runat="server">
			<asp:button id="btnExecQry" style="Z-INDEX: 106; POSITION: absolute; TOP: 45px; LEFT: 81px"
				runat="server" CausesValidation="False" Text="Execute Query" CssClass="queryButton" Enabled="False"
				Width="130px"></asp:button><asp:label id="lblMainTitle" style="Z-INDEX: 112; POSITION: absolute; TOP: 8px; LEFT: 21px"
				runat="server" CssClass="mainTitleSize" Width="477px" Height="26px">Status & Exception</asp:label><asp:button id="btnInsert" style="Z-INDEX: 103; POSITION: absolute; TOP: 45px; LEFT: 210px"
				runat="server" CausesValidation="False" Text="Insert" CssClass="queryButton" Width="62px"></asp:button><asp:button id="btnSave" style="Z-INDEX: 104; POSITION: absolute; TOP: 45px; LEFT: 272px" runat="server"
				CausesValidation="True" Text="save" CssClass="queryButton" Width="66px" Visible="False"></asp:button><asp:button id="btnDelete" style="Z-INDEX: 105; POSITION: absolute; TOP: 45px; LEFT: 338px"
				runat="server" CausesValidation="False" Text="Delete" CssClass="queryButton" Width="62px" Visible="False"></asp:button><asp:button id="btnQry" style="Z-INDEX: 107; POSITION: absolute; TOP: 45px; LEFT: 21px" runat="server"
				CausesValidation="False" Text="Query" CssClass="queryButton" Width="62px"></asp:button><asp:label id="lblSCMessage" style="Z-INDEX: 110; POSITION: absolute; TOP: 90px; LEFT: 22px"
				runat="server" CssClass="errorMsgColor" Width="721px" Height="25px"></asp:label><asp:validationsummary id="ValidationSummary1" style="Z-INDEX: 101; POSITION: absolute; TOP: 71px; LEFT: 22px"
				Width="520px" Height="36px" Runat="server" ShowMessageBox="True" DisplayMode="SingleParagraph" ShowSummary="False"></asp:validationsummary>
			<TABLE id="Table1" style="Z-INDEX: 113; POSITION: absolute; WIDTH: 892px; HEIGHT: 135px; TOP: 111px; LEFT: 12px"
				cellSpacing="1" cols="1" cellPadding="1" width="892" border="0">
				<TR>
					<TD style="HEIGHT: 82px" vAlign="top"><FONT face="Tahoma"><asp:datagrid id="dgStatusCodes" runat="server" Width="900px" Height="95px" SelectedItemStyle-CssClass="gridFieldSelected"
								OnDeleteCommand="dgStatusCodes_Delete" AllowCustomPaging="True" AllowPaging="True" OnSelectedIndexChanged="dgStatusCodes_SelectedIndexChanged"
								OnUpdateCommand="dgStatusCodes_Update" OnCancelCommand="dgStatusCodes_Cancel" OnEditCommand="dgStatusCodes_Edit" OnItemDataBound="dgStatusCodes_Bound"
								OnPageIndexChanged="dgStatusCodes_PageChange" AutoGenerateColumns="False" ItemStyle-Height="20">
								<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
								<ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
								<Columns>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;" CommandName="Select">
										<HeaderStyle HorizontalAlign="Center" Width="1%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:ButtonColumn>
									<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
										CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
										<HeaderStyle Width="1%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left"></ItemStyle>
									</asp:EditCommandColumn>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
										<HeaderStyle HorizontalAlign="Center" Width="1%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:ButtonColumn>
									<asp:TemplateColumn HeaderText="Status Code">
										<HeaderStyle Font-Bold="True" Width="40px" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblStatusCode" Text='<%#DataBinder.Eval(Container.DataItem,"status_code")%>' Runat="server">
											</asp:Label>
											<asp:Label ID="lblSCSystemCode" Text='<%#DataBinder.Eval(Container.DataItem,"system_code")%>' Runat="server" Visible="False">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" ID="txtStatusCode" Text='<%#DataBinder.Eval(Container.DataItem,"status_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
											</cc1:mstextbox>
											<asp:RequiredFieldValidator ID="scValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtStatusCode"
												ErrorMessage="Status Code is required field"></asp:RequiredFieldValidator>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Status Description">
										<HeaderStyle Width="120px" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Width="120px"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblStatusDescription" Text='<%#DataBinder.Eval(Container.DataItem,"status_description")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" ID="txtStatusDescription" Text='<%#DataBinder.Eval(Container.DataItem,"status_description")%>' Runat="server" MaxLength="200">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Status Type">
										<HeaderStyle Width="11%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList CssClass="gridDropDown" ID="ddlSCShipmentUpdate" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Close Status N/A">
										<HeaderStyle Width="55px" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList Width="55px" CssClass="gridDropDown" ID="ddlSCCloseStatus" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Invoiceable">
										<HeaderStyle Width="15px" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList CssClass="gridDropDown" ID="ddlSCInvoiceable" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Viewable Status for T&amp;T">
										<HeaderStyle Width="45px" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList CssClass="gridDropDown" ID="ddlVSTT" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Viewable Remark for T&amp;T">
										<HeaderStyle Width="45px" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList CssClass="gridDropDown" ID="ddlVRTT" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Auto Create Process No.">
										<HeaderStyle Width="35px" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" Width="35px" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label Width="100%" CssClass="tableLabelRight" ID="lblAutoCPN" Text='<%#DataBinder.Eval(Container.DataItem,"auto_process")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="textFieldRightAlign" ID="txtAutoCPN" Text='<%#DataBinder.Eval(Container.DataItem,"auto_process")%>' Runat="server" Enabled="True" TextMaskType="msNumeric" NumberMaxValue="99999" NumberMinValue="0" NumberPrecision="10" NumberScale="0">
											</cc1:mstextbox>
											<asp:RequiredFieldValidator ID="Requiredfieldvalidator1" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtAutoCPN"
												ErrorMessage="Auto Create Process No. is required field"></asp:RequiredFieldValidator>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Auto Populate Loc. on Create">
										<HeaderStyle Width="45px" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList CssClass="gridDropDown" ID="ddlAutoPLC" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Auto Create Process Remark">
										<HeaderStyle Width="18%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblAutoCPR" Text='<%#DataBinder.Eval(Container.DataItem,"auto_remark")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" ID="txtAutoCPR" Text='<%#DataBinder.Eval(Container.DataItem,"auto_remark")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="199">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Viewable Location for T&amp;T">
										<HeaderStyle Width="50px" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList CssClass="gridDropDown" ID="ddlVLTT" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Scanning&lt;BR&gt;Type">
										<HeaderStyle Width="50px" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList CssClass="gridDropDown" ID="ddlScanningType" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Allowed Roles">
										<HeaderStyle Width="150px" CssClass="gridHeading"></HeaderStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" Width ="150px" ID="Label2" Text='<%#DataBinder.Eval(Container.DataItem,"AllowRoles")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" Width ="150px" ID="txtAllowRoles" Text='<%#DataBinder.Eval(Container.DataItem,"AllowRoles")%>' Runat="server" MaxLength="200">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Allow in SWB Emulator">
										<HeaderStyle Width="50px" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList CssClass="gridDropDown" ID="ddlAllowInSWBEmu" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Allow Update By Batch">
										<HeaderStyle Width="60px" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList id="ddlAllowUPDByBatch" CssClass="gridDropDown" Width="50px" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Allow for Mobile App">
										<HeaderStyle Width="60px" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList id="ddlAllowMobile" CssClass="gridDropDown" Width="50px" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
							</asp:datagrid></FONT></TD>
				</TR>
				<TR>
					<TD vAlign="top"><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT></TD>
				</TR>
				<TR>
					<TD vAlign="top"><FONT face="Tahoma"><asp:button id="btnExceptionCodeInsert" runat="server" CausesValidation="False" Text="Insert"
								CssClass="queryButton" Enabled="False" Width="62px"></asp:button><BR>
							<asp:label id="lblEXMessage" runat="server" CssClass="errorMsgColor" Width="726px" Height="29px"></asp:label><asp:datagrid id="dgExceptionCodes" runat="server" Width="682px" OnDeleteCommand="dgExceptionCodes_Delete"
								AllowCustomPaging="True" AllowPaging="True" OnUpdateCommand="dgExceptionCodes_Update" OnCancelCommand="dgExceptionCodes_Cancel" OnEditCommand="dgExceptionCodes_Edit" OnItemDataBound="dgExceptionCodes_Bound"
								OnPageIndexChanged="dgExceptionCodes_PageChange" AutoGenerateColumns="False" ItemStyle-Height="20" PageSize="5">
								<ItemStyle Height="20px"></ItemStyle>
								<Columns>
									<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
										HeaderText="" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
										<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
									</asp:EditCommandColumn>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton"
										HeaderText="" CommandName="Delete">
										<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
									</asp:ButtonColumn>
									<asp:TemplateColumn HeaderText="Exception Code">
										<HeaderStyle Width="25%" Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblExceptionCode" Text='<%#DataBinder.Eval(Container.DataItem,"exception_code")%>' Runat="server">
											</asp:Label>
											<asp:Label ID="lblExSystemCode" Text='<%#DataBinder.Eval(Container.DataItem,"system_code")%>' Runat="server" Visible="False">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" ID="txtExceptionCode" Text='<%#DataBinder.Eval(Container.DataItem,"exception_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
											</cc1:mstextbox>
											<asp:RequiredFieldValidator ID="exValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtExceptionCode"
												ErrorMessage="Exception Code is required field"></asp:RequiredFieldValidator>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Exception Description">
										<HeaderStyle Width="35%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblExceptionDescription" Text='<%#DataBinder.Eval(Container.DataItem,"exception_description")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" ID="txtExceptionDescription" Text='<%#DataBinder.Eval(Container.DataItem,"exception_description")%>' Runat="server" MaxLength="200">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="MBG">
										<HeaderStyle Width="13%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList CssClass="gridDropDown" ID="ddlExMBG" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Close Status">
										<HeaderStyle Width="20%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList CssClass="gridDropDown" ID="ddlExCloseStatus" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Invoiceable">
										<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList CssClass="gridDropDown" ID="ddlExInvoiceable" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
							</asp:datagrid></FONT></TD>
				</TR>
			</TABLE>
		</FORM>
	</body>
</HTML>
