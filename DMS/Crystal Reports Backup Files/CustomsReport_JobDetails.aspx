<%@ Page language="c#" Codebehind="CustomsReport_JobDetails.aspx.cs" AutoEventWireup="false" Inherits="TIES.CustomsReport_JobDetails" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomsReport_JobDetails</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="vs_snapToGrid" content="true">
		<meta name="vs_showGrid" content="true">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
		function funcDisBack(){
			history.go(+1);
		}

        function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }
        function validate(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventdefault) theEvent.preventdefault();
			}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onunload="window.top.displayBanner.fnCloseAll(0);">
		<form id="CustomsReport_JobDetails" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 900px; HEIGHT: 1543px; TOP: 34px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tbody>
						<tr>
							<td colSpan="5" align="left"><asp:label id="Label1" runat="server" CssClass="mainTitleSize" Height="27px" Width="421px">
								Customs Jobs Listings</asp:label></td>
						</tr>
						<tr>
							<td colspan="5">
								<asp:button id="btnQry" runat="server" CssClass="queryButton" CausesValidation="False" Text="Query"></asp:button><asp:button id="btnGenerateReport" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Generate Report"></asp:button>
							</td>
						</tr>
						<tr>
							<td style="HEIGHT: 19px" colSpan="5"><asp:label id="Label71" runat="server" Font-Bold="true" Font-Size="X-Small" ForeColor="Red"></asp:label></td>
						</tr>
						<tr>
							<td colSpan="5">
								<fieldset style="WIDTH:674px"><legend style="COLOR: black">Dates</legend>
									<table style="MARGIN: 5px" id="table_fieldset_Dates">
										<tbody>
											<tr>
												<td colSpan="5"><asp:radiobutton id="rbInfoReceived" runat="server" CssClass="tableRadioButton" Height="22px" Width="110px"
														Text="Info Received" AutoPostBack="true" GroupName="QueryByDate" Checked="true"></asp:radiobutton><asp:radiobutton id="rbEntryCompiled" runat="server" CssClass="tableRadioButton" Height="22px" Width="121px"
														Text="Entry Compiled" AutoPostBack="true" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbExpectedArrival" runat="server" CssClass="tableRadioButton" Height="22px"
														Width="127px" Text="Expected Arrival" AutoPostBack="true" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbInvoiced" runat="server" CssClass="tableRadioButton" Height="22px" Width="82px"
														Text="Invoiced" AutoPostBack="true" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbManifested" runat="server" CssClass="tableRadioButton" Height="22px" Width="93px"
														Text="Manifested" AutoPostBack="true" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbDelivered" runat="server" CssClass="tableRadioButton" Height="22px" Width="116px"
														Text="Delivered" AutoPostBack="true" GroupName="QueryByDate"></asp:radiobutton></td>
											</tr>
											<tr>
												<td colSpan="5"><asp:radiobutton id="rbMonth" runat="server" CssClass="tableRadioButton" Height="21px" Width="73px"
														Text="Month" AutoPostBack="true" GroupName="EnterDate" Checked="true"></asp:radiobutton>&nbsp;
													<asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Height="19px" Width="88px"></asp:dropdownlist>&nbsp;
													<asp:label id="lblYear" runat="server" CssClass="tableLabel" Height="15px" Width="30px">Year</asp:label>&nbsp;
													<cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" MaxLength="5" TextMaskType="msNumeric"
														NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></td>
											</tr>
											<tr>
												<td style="HEIGHT: 20px" colSpan="5"><asp:radiobutton id="rbPeriod" runat="server" CssClass="tableRadioButton" Width="62px" Text="Period"
														AutoPostBack="true" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
													<cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
														TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
													&nbsp;
													<cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" MaxLength="10" TextMaskType="msDate"
														TextMaskString="99/99/9999"></cc1:mstextbox></td>
											</tr>
											<tr>
												<td colSpan="5">
													<asp:radiobutton id="rbDate" runat="server" Width="74px" CssClass="tableRadioButton" Text="Date"
														Height="22px" GroupName="EnterDate" AutoPostBack="true"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtdate" runat="server" Width="82" CssClass="textField" TextMaskType="msDate"
														MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>
												</td>
											</tr>
										</tbody>
									</table>
								</fieldset>
							</td>
						</tr>
						<tr>
							<td width="31%">
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label2">Job Entry Number</asp:label>
							</td>
							<td width="18%">
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="txtInvoiceNumberFrom"
									Width="145px"></asp:textbox>
							</td>
							<td width="21%">
								<asp:Label runat="server" id="Label3_empty">&nbsp;</asp:Label>
							</td>
							<td width="16%">
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label3">House AWB Number</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="txtHouseAwbNumber"
									Width="145px"></asp:textbox>
							</td>
						</tr>
						<tr>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label4_">MAWB Number</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="txtMawbNumber"
									Width="145px"></asp:textbox>
							</td>
							<td colspan="3">
								<asp:checkbox id="chkSearchMawbInJobEntry_" CssClass="tableLabel" runat="server" Text="Search MAWB in Job Entry"
									AutoPostBack="True" style="MARGIN-LEFT:5px"></asp:checkbox>
							</td>
						</tr>
						<tr>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label5">Customer ID</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="txtCustomerID"
									Width="145px"></asp:textbox>
							</td>
							<td>
								<asp:Label runat="server" ID="Label6">&nbsp;</asp:Label>
							</td>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label7">Customer Group</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="txtCustomerGroup"
									Width="145px"></asp:textbox>
							</td>
						</tr>
						<tr>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label8">Bonded Warehouse</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="txtBondedWarehouse"
									Width="145px"></asp:textbox>
							</td>
							<td>
								<asp:Label runat="server" ID="Label9" NAME="Label6">&nbsp;</asp:Label>
							</td>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label10">Discharge Port</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="txtDischargePort"
									Width="145px"></asp:textbox>
							</td>
						</tr>
						<tr>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label11">Job Status</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="txtJobStatus"
									Width="145px"></asp:textbox>
							</td>
							<td colspan="3">
								<asp:checkbox id="chkSearchOnStatusNotAchieved" CssClass="tableLabel" runat="server" Text="Search on Status not Achieved"
									AutoPostBack="True" style="MARGIN-LEFT:5px"></asp:checkbox>
							</td>
						</tr>
						<tr>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label12">Entry Type</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="txtEntryType"
									Width="145px"></asp:textbox>
							</td>
							<td colspan="3">
								<asp:Label Runat="server" id="Label13_empty">&nbsp;</asp:Label>
							</td>
						</tr>
						<tr>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label13">Ship/Flight No.</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="txtShipFlightNo"
									Width="145px"></asp:textbox>
							</td>
							<td>
								<asp:Label runat="server" ID="Label14">&nbsp;</asp:Label>
							</td>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label15">Exporter Name</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="txtExporterName"
									Width="145px"></asp:textbox>
							</td>
						</tr>
						<tr>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label16">Folio Number</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="FolioNumber"
									Width="145px"></asp:textbox>
							</td>
							<td>
								<asp:Label runat="server" ID="Label17">&nbsp;</asp:Label>
							</td>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label18">Cheque Req. No</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="txtChequeReqNo"
									Width="145px"></asp:textbox>
							</td>
						</tr>
						<tr>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label19">Customs Profile</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="txtCustomsProfile"
									Width="145px"></asp:textbox>
							</td>
							<td>
								<asp:Label runat="server" ID="Label20" NAME="Label6">&nbsp;</asp:Label>
							</td>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label21">Customs Receipt No</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="txtCustomsReceiptNo"
									Width="145px"></asp:textbox>
							</td>
						</tr>
						<tr>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label22">Customs Lodgment(Declaration) No</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="txtCustomsLodgmentNo"
									Width="145px"></asp:textbox>
							</td>
							<td colspan="3">
								<asp:Label runat="server" ID="Label23">&nbsp;</asp:Label>
							</td>
						</tr>
						<tr>
							<td colSpan="5">
								<!-- Gridview Area -->
							</td>
						</tr>
					</tbody>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
