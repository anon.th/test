<%@ Page language="c#" Codebehind="RetrievePickUpPkgsPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.RetrievePickUpPkgs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>RetrievePickUpPkgs</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="RetrievePickUpPkgs" method="post" runat="server">
			<asp:button id="btnCancel" style="Z-INDEX: 101; LEFT: 267px; POSITION: absolute; TOP: 180px" runat="server" Width="62px" Height="23px" CssClass="queryButton" Text="Cancel" CausesValidation="False"></asp:button><asp:datagrid id="dgShipUpdate" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" AllowCustomPaging="True" OnItemDataBound="dgShipUpdate_Bound" SelectedItemStyle-CssClass="gridFieldSelected" Width="598px">
				<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
				<ItemStyle CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:TemplateColumn HeaderText="Serial No">
						<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblSerialNo" Text='<%#DataBinder.Eval(Container.DataItem,"serial_no")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtSerialNo" Text='<%#DataBinder.Eval(Container.DataItem,"serial_no")%>' Runat="server" Enabled="True" MaxLength="12">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Recipient Postal Code">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblRecipientZip" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_zipcode")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtRecipientZip" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_zipcode")%>' Runat="server">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Service Code">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle VerticalAlign="Middle"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblServiceCode" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtServiceCode" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' Runat="server">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Consignment No">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle VerticalAlign="Middle"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblConsgmntNo" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtConsgmntNo" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' Runat="server">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="Select" HeaderText="Select" CommandName="Select">
						<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn Visible="False" HeaderText="IsCreated">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle VerticalAlign="Middle"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblIsCreated" Text='<%#DataBinder.Eval(Container.DataItem,"isCreated")%>' Runat="server" Enabled="True" Visible=False>
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid></form>
	</body>
</HTML>
