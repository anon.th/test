<%@ Page language="c#" Codebehind="CustomsJobStatus.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsJobStatus" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Customs Job Status</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="False">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
		function funcDisBack(){
			history.go(+1);
		}

		function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function RemoveBadMasterAWBNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^-_/_a-zA-Z0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteMasterAWBNumber(obj) {
            setTimeout(function () {
                RemoveBadMasterAWBNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function validate(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		function ValidateMasterAWBNumber(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[-_/_a-zA-Z0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="CustomsJobStatus" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 1175px; HEIGHT: 1248px; TOP: 32px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<TBODY>
						<tr>
							<td colSpan="2" align="left"><asp:label id="lblHeader" runat="server" Width="800px" Height="27px" CssClass="mainTitleSize">
							Customs Job Status</asp:label></td>
						</tr>
						<tr>
							<td style="HEIGHT: 26px" vAlign="top" colSpan="2" align="left"><asp:button id="btnQry" runat="server" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><label style="WIDTH: 5px"></label><asp:button id="btnExecQry" runat="server" CssClass="queryButton" Text="Execute Query" CausesValidation="False"></asp:button><asp:button id="btnGenerateReport" runat="server" CssClass="queryButton" Text="Generate Report"
									CausesValidation="False"></asp:button><label style="WIDTH: 5px"></label><label style="WIDTH: 5px"></label><label style="WIDTH: 5px"></label><label style="WIDTH: 5px"></label></td>
						</tr>
						<tr>
							<td vAlign="top" colSpan="2" align="left">
								<TABLE style="Z-INDEX: 112; WIDTH: 1100px" id="Table1" border="0" width="730" runat="server">
									<TR width="100%">
										<TD width="100%">
											<table border="0" cellSpacing="1" cellPadding="1" width="900">
												<TBODY>
													<TR>
														<TD style="HEIGHT: 19px" colSpan="6"><asp:label id="lblError" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red"></asp:label></TD>
														<TD style="HEIGHT: 19px"></TD>
													</TR>
													<TR>
														<TD colSpan="6">
															<fieldset style="WIDTH: 850px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" Width="20px" CssClass="tableHeadingFieldset" Font-Bold="True">Dates</asp:label></legend>
																<TABLE style="WIDTH: 850px; HEIGHT: 113px" id="tblDates" border="0" cellSpacing="0" cellPadding="0"
																	align="left" runat="server">
																	<TR>
																		<td></td>
																		<TD colSpan="3">&nbsp;
																			<asp:radiobutton id="rbinfoReceived" tabIndex="1" runat="server" Width="102px" Height="22px" CssClass="tableRadioButton"
																				Text="Info Received" Checked="True" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbEntryCompiled" tabIndex="2" runat="server" Width="111px" Height="22px" CssClass="tableRadioButton"
																				Text="Entry Compiled" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbExpectedArrival" tabIndex="3" runat="server" Width="123px" Height="22px" CssClass="tableRadioButton"
																				Text="Expected Arrival" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbInvoiced" tabIndex="4" runat="server" Width="75px" Height="22px" CssClass="tableRadioButton"
																				Text="Invoiced" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbManifested" tabIndex="5" runat="server" Width="150px" Height="22px" CssClass="tableRadioButton"
																				Text="Manifest Data Entered" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbDelivered" tabIndex="6" runat="server" Width="75px" Height="22px" CssClass="tableRadioButton"
																				Text="Delivered" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbStationOutProceessed" tabIndex="5" runat="server" Width="150px" Height="22px"
																				CssClass="tableRadioButton" Text="Station Out Processed" GroupName="QueryByDate"></asp:radiobutton></TD>
																	</TR>
																	<TR>
																		<td></td>
																		<TD colSpan="3">&nbsp;
																			<asp:radiobutton id="rbMonth" tabIndex="7" runat="server" Width="73px" Height="21px" CssClass="tableRadioButton"
																				Text="Month" Checked="True" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;
																			<asp:dropdownlist id="ddMonth" tabIndex="8" runat="server" Width="88px" Height="19px" CssClass="textField"></asp:dropdownlist>&nbsp;
																			<asp:label id="lblYear" runat="server" Width="30px" Height="10px" CssClass="tableLabel">Year</asp:label>&nbsp;
																			<cc1:mstextbox id="txtYear" tabIndex="9" runat="server" Width="83" CssClass="textField" NumberPrecision="4"
																				NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></TD>
																	</TR>
																	<TR>
																		<td style="HEIGHT: 20px"></td>
																		<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
																			<asp:radiobutton id="rbPeriod" tabIndex="10" runat="server" Width="62px" CssClass="tableRadioButton"
																				Text="Period" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
																			<cc1:mstextbox id="txtPeriod" tabIndex="11" runat="server" Width="82" CssClass="textField" TextMaskType="msDate"
																				MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
																			&nbsp;
																			<cc1:mstextbox id="txtTo" tabIndex="12" runat="server" Width="83" CssClass="textField" TextMaskType="msDate"
																				MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
																	</TR>
																	<TR>
																		<td></td>
																		<TD colSpan="3">&nbsp;
																			<asp:radiobutton id="rbDate" tabIndex="13" runat="server" Width="74px" Height="22px" CssClass="tableRadioButton"
																				Text="Date" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" tabIndex="14" runat="server" Width="82" CssClass="textField" TextMaskType="msDate"
																				MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
																	</TR>
																</TABLE>
															</fieldset>
														</TD>
														<TD></TD>
													</TR>
													<tr>
														<td style="WIDTH: 250px"><asp:label id="Label4" runat="server" CssClass="tableLabel"> Job Entry Number:</asp:label></td>
														<td style="WIDTH: 200px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="JobEntryNo" tabIndex="15" onkeypress="validate(event)"
																onpaste="AfterPasteNonNumber(this)" runat="server" Width="150px" CssClass="textField" MaxLength="10"></asp:textbox></td>
														<td><asp:label id="Label5" runat="server" CssClass="tableLabel"> House AWB Number:</asp:label></td>
														<td colSpan="3"><FONT face="Tahoma"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="HouseAWBNo" tabIndex="16" onkeypress="ValidateMasterAWBNumber(event)"
																	onpaste="AfterPasteMasterAWBNumber(this)" runat="server" Width="150px" CssClass="textField" MaxLength="30"></asp:textbox></FONT></td>
														<TD></TD>
													</tr>
													<tr>
														<td><asp:label style="Z-INDEX: 0" id="Label6" runat="server" CssClass="tableLabel">MAWB Number:</asp:label></td>
														<td colSpan="5"><FONT face="Tahoma"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="MasterAWBNo" tabIndex="17" onkeypress="ValidateMasterAWBNumber(event)"
																	onpaste="AfterPasteMasterAWBNumber(this)" runat="server" Width="150px" CssClass="textField" MaxLength="30"></asp:textbox><asp:checkbox style="Z-INDEX: 0; MARGIN-LEFT: 1px" id="SearchMAWBinJobEntry" tabIndex="18" runat="server"
																	Width="188px" CssClass="tableLabel" Text="Search MAWB in Job Entry"></asp:checkbox></FONT></td>
														<TD></TD>
													</tr>
													<tr>
														<td><asp:label style="Z-INDEX: 0" id="Label31" runat="server" CssClass="tableLabel"> Customer ID:</asp:label></td>
														<td style="WIDTH: 235px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="CustomerID" tabIndex="19" runat="server"
																Width="150px" CssClass="textField" MaxLength="100"></asp:textbox><asp:button style="Z-INDEX: 0" id="btnCustomerPopup" tabIndex="11" runat="server" Width="21px"
																Height="19px" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></td>
														<td><asp:label style="Z-INDEX: 0" id="Label11" runat="server" CssClass="tableLabel">Customer Group:</asp:label></td>
														<td><FONT face="Tahoma"><asp:dropdownlist style="Z-INDEX: 0" id="CustomerGroup" tabIndex="20" runat="server" Width="150px"></asp:dropdownlist></FONT></td>
														<TD></TD>
													</tr>
													<TR>
														<TD><asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel">Bonded Warehouse:</asp:label></TD>
														<TD style="WIDTH: 235px"><asp:dropdownlist style="Z-INDEX: 0" id="BondedWarehouse" tabIndex="21" runat="server" Width="150px"></asp:dropdownlist></TD>
														<TD><asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel">Discharge Port:</asp:label></TD>
														<TD><asp:dropdownlist style="Z-INDEX: 0" id="DischargePort" tabIndex="22" runat="server" Width="150px"></asp:dropdownlist></TD>
														<TD></TD>
													</TR>
													<tr>
														<td><FONT face="Tahoma"><asp:label style="Z-INDEX: 0" id="lblHCSupportedbyEnterprise" runat="server" CssClass="tableLabel"> Job Status: </asp:label></FONT></td>
														<td colSpan="5"><asp:dropdownlist style="Z-INDEX: 0" id="JobStatus" tabIndex="23" runat="server" Width="150px"></asp:dropdownlist><asp:checkbox id="SearchStatusNotAchieved" tabIndex="24" runat="server" Width="200px" CssClass="tableLabel "
																Text="Search on Status not Achieved"></asp:checkbox></td>
														<TD></TD>
													</tr>
													<TR>
														<TD style="HEIGHT: 19px"><asp:label style="Z-INDEX: 0" id="Label7" runat="server" CssClass="tableLabel">Entry Type:</asp:label></TD>
														<TD style="HEIGHT: 19px" colSpan="5"><asp:dropdownlist style="Z-INDEX: 0" id="EntryType" tabIndex="25" runat="server" Width="150px"></asp:dropdownlist></TD>
														<TD style="HEIGHT: 19px"></TD>
													</TR>
													<tr>
														<td><asp:label style="Z-INDEX: 0" id="Label32" runat="server" CssClass="tableLabel"> Ship/Flight No:</asp:label></td>
														<td style="WIDTH: 235px"><FONT face="Tahoma"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="ShipFlightNo" tabIndex="26" runat="server"
																	Width="150px" CssClass="textField" MaxLength="40" Columns="30"></asp:textbox></FONT></td>
														<td><FONT face="Tahoma"><asp:label style="Z-INDEX: 0" id="Label8" runat="server" CssClass="tableLabel">Exporter Name:</asp:label></FONT></td>
														<td vAlign="middle" colSpan="3"><FONT face="Tahoma"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="ExporterName" tabIndex="27" runat="server"
																	Width="150px" CssClass="textField" MaxLength="100"></asp:textbox></FONT></td>
														<TD vAlign="middle"></TD>
													</tr>
													<tr>
														<td><asp:label style="Z-INDEX: 0" id="Label33" runat="server" CssClass="tableLabel">Folio Number:</asp:label></td>
														<td style="WIDTH: 235px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="FolioNumber" tabIndex="28" onkeypress="validate(event)"
																onpaste="AfterPasteNonNumber(this)" runat="server" Width="150px" CssClass="textField" MaxLength="20"></asp:textbox></td>
														<td><FONT face="Tahoma"><asp:label style="Z-INDEX: 0" id="Label12" runat="server" CssClass="tableLabel">Cheque Req. No: </asp:label></FONT></td>
														<td vAlign="middle" colSpan="3"><FONT face="Tahoma"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="ChequeReqNo" tabIndex="29" onkeypress="validate(event)"
																	onpaste="AfterPasteNonNumber(this)" runat="server" Width="150px" CssClass="textField" MaxLength="10"></asp:textbox></FONT></td>
														<TD vAlign="middle"></TD>
													</tr>
													<tr>
														<td><asp:label style="Z-INDEX: 0" id="Label34" runat="server" CssClass="tableLabel"> Customs Profile:</asp:label></td>
														<td style="WIDTH: 235px"><asp:dropdownlist style="Z-INDEX: 0" id="AssignedProfile" tabIndex="30" runat="server" Width="150px"></asp:dropdownlist></td>
														<td><FONT face="Tahoma"><asp:label style="Z-INDEX: 0" id="Label13" runat="server" CssClass="tableLabel"> Customs Receipt No: </asp:label></FONT></td>
														<td vAlign="middle" colSpan="3"><FONT face="Tahoma"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="ReceiptNumber" tabIndex="31" runat="server"
																	Width="150px" CssClass="textField" MaxLength="40"></asp:textbox></FONT></td>
														<TD vAlign="middle"></TD>
													</tr>
													<TR>
														<TD style="HEIGHT: 22px"><asp:label style="Z-INDEX: 0" id="Label9" runat="server" CssClass="tableLabel">Customs Lodgment (Declaration) No:</asp:label></TD>
														<TD colSpan="5"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="CustomsDeclarationNo" tabIndex="32"
																runat="server" Width="150px" CssClass="textField" MaxLength="20"></asp:textbox><asp:checkbox id="DutyTaxNotOnChequeReq" tabIndex="33" runat="server" Width="404px" CssClass="tableLabel"
																Text="Show Customs Jobs only if Duty/Tax not on a Cheque Requisition"></asp:checkbox></TD>
													</TR>
													<tr>
														<td><asp:label style="Z-INDEX: 0" id="Label14" runat="server" CssClass="tableLabel">Customs Job Compiler:</asp:label></td>
														<td style="WIDTH: 235px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Compiler" tabIndex="34" runat="server"
																Width="150px" CssClass="textField" MaxLength="20"></asp:textbox></td>
														<td><asp:label style="Z-INDEX: 2" id="Label15" runat="server" CssClass="tableLabel">Customs Job Registrar: </asp:label></td>
														<td vAlign="middle" colSpan="3"><FONT face="Tahoma"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Registrar" tabIndex="35" runat="server"
																	Width="150px" CssClass="textField" MaxLength="10"></asp:textbox></FONT></td>
														<TD vAlign="middle"></TD>
													</tr>
													<tr>
														<td><FONT face="Tahoma"><asp:label style="Z-INDEX: 0" id="Label16" runat="server" CssClass="tableLabel"> Destination DC: </asp:label></FONT></td>
														<td colSpan="6"><asp:dropdownlist style="Z-INDEX: 0" id="DestinationDC" tabIndex="36" runat="server" Width="150px"></asp:dropdownlist><asp:checkbox id="NotDestinationDC" tabIndex="37" runat="server" Width="300px" CssClass="tableLabel "
																Text="Show jobs only not going to this DC"></asp:checkbox></td>
													</tr>
													<tr>
														<td colSpan="6"><asp:checkbox style="Z-INDEX: 1" id="NoSOPorPOD" tabIndex="38" runat="server" Width="500px" CssClass="tableLabel "
																Text="Show jobs only not delivered and not station-out-processed"></asp:checkbox></td>
														<TD></TD>
													</tr>
													<TR id="TrReportFormat" runat="server">
														<TD><FONT face="Tahoma"><asp:label style="Z-INDEX: 0" id="Label17" runat="server" CssClass="tableLabel"> Report Format: </asp:label></FONT></TD>
														<TD colSpan="6">
															<asp:dropdownlist style="Z-INDEX: 0" id="ReportFormat" tabIndex="36" runat="server" Width="150px">
																<asp:ListItem Value="CustomsJobArrivals" Selected="True">ARRIVALS</asp:ListItem>
																<asp:ListItem Value="CustomsJobDetail1">JOB DETAILS 1</asp:ListItem>
																<asp:ListItem Value="CustomsJobDetail2">JOB DETAILS 2</asp:ListItem>
															</asp:dropdownlist></TD>
													</TR>
													<tr>
														<td colSpan="6"><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><br>
														</td>
														<TD></TD>
													</tr>
													<tr id="trGrdJobStatus" class="gridHeading" runat="server">
														<td colSpan="6"><STRONG><FONT size="2">Customs Job Status</FONT></STRONG></td>
													</tr>
													<tr id="trSearchJobStatus" runat="server">
														<td colSpan="7">
															<table border="0" cellSpacing="0" cellPadding="0" width="100%">
																<tr>
																	<td width="20%">&nbsp;
																		<asp:button style="Z-INDEX: 0" id="btnAddChequeReq" runat="server" CssClass="queryButton" Text="Add to Cheque Req"
																			CausesValidation="False"></asp:button></td>
																	<td width="80%" align="left">
																		<fieldset style="WIDTH: 423px"><legend><asp:label id="Label1" runat="server" Width="140px" CssClass="tableHeadingFieldset" Font-Bold="True">These Disbursements</asp:label></legend>
																			<TABLE style="WIDTH: 656px; HEIGHT: 40px" id="Table2" border="0" cellSpacing="0" cellPadding="0"
																				align="left" runat="server">
																				<TR>
																					<td><asp:radiobutton style="Z-INDEX: 0" id="rdoDuty" runat="server" CssClass="tableRadioButton" Text="Duty+Tax+EPF"
																							Checked="True" GroupName="Disbursements"></asp:radiobutton></td>
																					<td><asp:radiobutton style="Z-INDEX: 0" id="rdoWharfage" runat="server" CssClass="tableRadioButton" Text="Wharfage &amp; Handling"
																							GroupName="Disbursements"></asp:radiobutton></td>
																					<td><asp:radiobutton style="Z-INDEX: 0" id="rdoFreight" runat="server" CssClass="tableRadioButton" Text="Freight Collect"
																							GroupName="Disbursements"></asp:radiobutton></td>
																					<td><asp:radiobutton style="Z-INDEX: 0" id="rdoDO" runat="server" CssClass="tableRadioButton" Text="DO Fee"
																							GroupName="Disbursements"></asp:radiobutton></td>
																					<td><asp:radiobutton style="Z-INDEX: 0" id="rdoStorage" runat="server" CssClass="tableRadioButton" Text="Storage"
																							GroupName="Disbursements"></asp:radiobutton></td>
																					<td><asp:radiobutton style="Z-INDEX: 0" id="rdoOther" runat="server" CssClass="tableRadioButton" Text="Other"
																							GroupName="Disbursements"></asp:radiobutton></td>
																				</TR>
																			</TABLE>
																		</fieldset>
																	</td>
																</tr>
																<TR>
																	<TD colspan="2" width="100%">
																		<asp:label style="Z-INDEX: 0" id="lblErrorAddChequeReq" runat="server" Font-Size="X-Small"
																			Font-Bold="True" ForeColor="Red"></asp:label></TD>
																</TR>
															</table>
														</td>
													</tr>
													<tr>
														<td colSpan="6"><asp:datagrid style="Z-INDEX: 0" id="grdJobStatus" runat="server" Width="1150px" FooterStyle-CssClass="gridHeading"
																BorderWidth="0px" CellSpacing="1" CellPadding="0" HeaderStyle-CssClass="gridHeading" ItemStyle-CssClass="gridField"
																AlternatingItemStyle-CssClass="gridField" DataKeyField="JobEntryNo" AutoGenerateColumns="False" AllowPaging="True"
																PageSize="6">
																<FooterStyle CssClass="gridHeading"></FooterStyle>
																<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
																<ItemStyle CssClass="gridField"></ItemStyle>
																<HeaderStyle CssClass="gridHeading"></HeaderStyle>
																<Columns>
																	<asp:TemplateColumn HeaderText="Job Number">
																		<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
																		<ItemTemplate>
																			<asp:LinkButton ID="lnkJobEntryNo" runat="server" Visible="True" CausesValidation="false" 
 Text='<%# DataBinder.Eval(Container.DataItem,"JobEntryNo") %>' 
 CommandName="EntryCompilingDate">
																			</asp:LinkButton>
																			<asp:label id="lblJobEntryNo" Visible="False" runat="server" Text ='<%# DataBinder.Eval(Container.DataItem,"JobEntryNo") %>'>
																			</asp:label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Registrar / Compiler">
																		<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem,"RegistrarCompiler") %>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Customer ID / Name">
																		<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center" Width="400px"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem,"CustomerName") %>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="House AWB Number">
																		<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
																		<ItemTemplate>
																			<asp:LinkButton ID="lnkHouseAWBNumber" runat="server" Visible="True" CausesValidation="false" CommandName="HouseAWBNumber" Text='<%#DataBinder.Eval(Container.DataItem,"HouseAWBNumber")%>'>
																			</asp:LinkButton>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Created Date">
																		<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem,"CreatedDate","{0:dd/MM/yyyy HH:mm}") %>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="MAWB Number">
																		<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem,"MasterAWBNumber") %>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Entry Compiling&lt;br/&gt;Date">
																		<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
																		<ItemTemplate>
																			<asp:label id="lblEntryCompilingDate" runat="server" Text=''></asp:label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Freight Collect&lt;br/&gt;Entry Date">
																		<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
																		<ItemTemplate>
																			<asp:LinkButton ID="lnkFreightCollectDate" runat="server" Visible="True" CausesValidation="false"
																				CommandName="FreightCollectDate"></asp:LinkButton>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Invoice Printed Date">
																		<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
																		<ItemTemplate>
																			<asp:LinkButton ID="lnkInvoicedDate" runat="server" Visible="True" CausesValidation="false" CommandName="InvoicedDate"></asp:LinkButton>
																			<asp:label id="lblInvoicedDate" runat="server" Visible=False Text='<%# DataBinder.Eval(Container.DataItem,"InvoiceStatus") %>'>
																			</asp:label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Invoice Received&lt;br/&gt;by Ops">
																		<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem,"InvoiceReceivedDate","{0:dd/MM/yyyy HH:mm}") %>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Manifest Data&lt;br/&gt;Entry Date">
																		<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
																		<ItemTemplate>
																			<asp:LinkButton ID="lbkManifestDataEntryDate" runat="server" Visible="True" CausesValidation="false" CommandName="ManifestDataEntryDate" Text = '<%# DataBinder.Eval(Container.DataItem,"ManifestDataEntryDate","{0:dd/MM/yyyy HH:mm}") %>'>
																			</asp:LinkButton>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Cheque Requisition&lt;br/&gt;Date">
																		<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
																		<ItemTemplate>
																			<asp:label id="lblChequeRequisitionDate" runat="server"></asp:label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Duty Paid Date">
																		<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
																		<ItemTemplate>
																			<asp:label id="lblDutyPaidDate" runat="server"></asp:label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Delivery Date">
																		<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem,"DeliveryDate","{0:dd/MM/yyyy HH:mm}") %>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																</Columns>
																<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
															</asp:datagrid></td>
													</tr>
													<tr id="trSearchJobStatusResults" class="gridHeading" runat="server">
														<td colSpan="6"><STRONG><FONT size="2">Search Results</FONT></STRONG></td>
													</tr>
													<tr>
														<td colSpan="6"><asp:datagrid style="Z-INDEX: 0" id="grdSearchJobStatus" tabIndex="42" runat="server" Width="1000px"
																FooterStyle-CssClass="gridHeading" BorderWidth="0px" CellSpacing="1" CellPadding="0" HeaderStyle-CssClass="gridHeading"
																ItemStyle-CssClass="gridField" AlternatingItemStyle-CssClass="gridField" AutoGenerateColumns="False" AllowPaging="True"
																PageSize="6">
																<FooterStyle CssClass="gridHeading"></FooterStyle>
																<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
																<ItemStyle CssClass="gridField"></ItemStyle>
																<HeaderStyle CssClass="gridHeading"></HeaderStyle>
																<Columns>
																	<asp:TemplateColumn HeaderText="Select">
																		<HeaderStyle HorizontalAlign="Center" Width="7%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:CheckBox Runat="server" ID="cbkConsignment_no" CrsID='<%#DataBinder.Eval(Container.DataItem,"Invoice_No")%>' Checked='<%# DataBinder.Eval(Container.DataItem,"Check").ToString().ToLower().Equals("true") %>'>
																			</asp:CheckBox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Invoice No.">
																		<HeaderStyle HorizontalAlign="Center" Width="8%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:label id="Label10" runat="server" Text ='<%# DataBinder.Eval(Container.DataItem,"Invoice_No") %>'>
																			</asp:label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Client">
																		<HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Left"></ItemStyle>
																		<ItemTemplate>
																			<%# DataBinder.Eval(Container.DataItem,"CustomerName") %>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Duty + Tax">
																		<HeaderStyle HorizontalAlign="Center" Width="8%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:label id="lblDutyTax" runat="server" Text ='<%# DataBinder.Eval(Container.DataItem,"DutyTax", "{0:#,##0.00}") %>'>
																			</asp:label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="EPF">
																		<HeaderStyle HorizontalAlign="Center" Width="7%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:label id="lblEPF" runat="server" Text ='<%# DataBinder.Eval(Container.DataItem,"EPF", "{0:#,##0.00}") %>'>
																			</asp:label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Wharfage & <br/>Handling">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:label id="lblWharfageHandling" runat="server" Text ='<%# DataBinder.Eval(Container.DataItem,"WharfageHandling", "{0:#,##0.00}") %>'>
																			</asp:label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Freight&lt;br/&gt;Collect Entry">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:label id="lblFreightCollect" runat="server" Text ='<%# DataBinder.Eval(Container.DataItem,"FreightCollect", "{0:#,##0.00}") %>'>
																			</asp:label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Delivery&lt;br/&gt;Order Fee">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:label id="lblDeliveryOrderFee" runat="server" Text ='<%# DataBinder.Eval(Container.DataItem,"DeliveryOrderFee", "{0:#,##0.00}") %>'>
																			</asp:label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Storage">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:label id="lblStorage" runat="server" Text ='<%# DataBinder.Eval(Container.DataItem,"Storage", "{0:#,##0.00}") %>'>
																			</asp:label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Other">
																		<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
																		<ItemStyle HorizontalAlign="Center"></ItemStyle>
																		<ItemTemplate>
																			<asp:label id="lblOther" runat="server" Text ='<%# DataBinder.Eval(Container.DataItem,"Other", "{0:#,##0.00}") %>'>
																			</asp:label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																</Columns>
																<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
															</asp:datagrid></td>
													</tr>
												</TBODY>
											</table>
										</TD>
									</TR>
								</TABLE>
								<DIV></DIV>
							</td>
						</tr>
					</TBODY>
				</TABLE>
			</div>
			<INPUT 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition> <INPUT type="hidden" name="hdnRefresh">
		</form>
	</body>
</HTML>
