<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="Shipment_Update_ByBatch.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.Shipment_Update_ByBatch" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PopUp_ViewVariances</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../css/Styles.css">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript">
		function Count(text,long)
		{
		    var maxlength = new Number(long); // Change number to your max length.
			if(document.getElementById('<%=txtRemark.ClientID%>').value.length > maxlength)
			{     text.value = text.value.substring(0,maxlength);}
		}
		
		function KeyDownHandler()
		{
			// process only the Enter key
			if (event.keyCode == 13 || event.keyCode == 9)
			{
				
				document.getElementById("btnExcuteQuery").click();
				document.getElementById("ddlStatus").focus();

			}
		}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:label style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 8px" id="Header1" Runat="server"
				Width="490" CssClass="mainTitleSize">Shipment Update By Batch</asp:label><asp:button style="Z-INDEX: 105; POSITION: absolute; TOP: 40px; LEFT: 256px" id="btnSave" runat="server"
				Width="103px" CssClass="queryButton" Enabled="False" Text="Save"></asp:button><asp:button style="Z-INDEX: 102; POSITION: absolute; TOP: 40px; LEFT: 112px" id="btnExcuteQuery"
				runat="server" Width="140px" CssClass="queryButton" Text="Execute Query"></asp:button><FONT face="Tahoma"><asp:button style="Z-INDEX: 103; POSITION: absolute; TOP: 40px; LEFT: 8px" id="btnQuery" runat="server"
					Width="101px" CssClass="queryButton" Text="Query"></asp:button></FONT><asp:label style="Z-INDEX: 104; POSITION: absolute; TOP: 64px; LEFT: 8px" id="lblError" runat="server"
				Width="504px" CssClass="errorMsgColor"></asp:label>&nbsp;
			<FIELDSET style="Z-INDEX: 106; POSITION: absolute; WIDTH: 573px; HEIGHT: 208px; TOP: 88px; LEFT: 296px">
				<P><LEGEND><asp:label id="lblApplyStatus" Runat="server" Width="106px" CssClass="tableHeadingFieldset"
							HEIGHT="16px"> Apply Status Code</asp:label></LEGEND></P>
				<FONT face="Tahoma">
					<table style="WIDTH: 568px; HEIGHT: 25px">
						<tr>
							<td style="WIDTH: 115px"><asp:label style="Z-INDEX: 0" id="Label1" runat="server" CssClass="tableLabel"> Status:</asp:label></td>
							<td><asp:dropdownlist style="Z-INDEX: 0" id="ddlStatus" tabIndex="1" runat="server" Width="144px" CssClass="textField"
									AutoPostBack="True"></asp:dropdownlist></td>
							<td><asp:label style="Z-INDEX: 0" id="lblStatusDateDis" runat="server" CssClass="tableLabel">Status Date/Time:</asp:label></td>
							<td><cc1:mstextbox style="Z-INDEX: 0" id="txtStatusDateTime" tabIndex="2" runat="server" Width="144px"
									CssClass="textField" TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99" MaxLength="16"></cc1:mstextbox></td>
						</tr>
						<TR>
							<TD style="WIDTH: 115px"><asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel">Person In Charge:</asp:label></TD>
							<TD><asp:textbox style="Z-INDEX: 0" id="txtPersonChange" tabIndex="3" runat="server" Width="141px"
									CssClass="textField" MaxLength="20"></asp:textbox></TD>
							<TD><asp:label style="Z-INDEX: 0" id="lblConsignee" runat="server" CssClass="tableLabel" Visible="False">Consignee Name:</asp:label></TD>
							<TD><asp:textbox style="Z-INDEX: 0" id="txtConsignee" tabIndex="4" runat="server" Width="144px" CssClass="textField"
									MaxLength="20" Visible="False"></asp:textbox></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 115px"><asp:label style="Z-INDEX: 0" id="lblRemark" runat="server" CssClass="tableLabel">Remarks:</asp:label></TD>
							<TD></TD>
							<TD></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD colSpan="4"><asp:textbox style="Z-INDEX: 0" id="txtRemark" tabIndex="5" onkeyup="javascript:Count(this,200);"
									onpaste="javascript:Count(this,180);" runat="server" Width="400px" CssClass="textField" MaxLength="200"
									onChange="javascript:Count(this,200);" Height="96px" TextMode="MultiLine"></asp:textbox></TD>
						</TR>
					</table>
			</FIELDSET>
			<TABLE style="Z-INDEX: 107; POSITION: absolute; WIDTH: 272px; HEIGHT: 164px; TOP: 96px; LEFT: 8px"
				id="Table1" border="0" cellSpacing="1" cellPadding="1" width="272">
				<TR>
					<TD style="WIDTH: 128px; HEIGHT: 19px"><asp:label id="lblBatchNumber" runat="server" CssClass="tableLabel">Enter Batch Number:</asp:label></TD>
					<TD style="HEIGHT: 19px"><FONT face="Tahoma"><cc1:mstextbox style="Z-INDEX: 0" id="txtBatchNo" runat="server" Width="106px" CssClass="tableTextbox"
								TextMaskType="msNumericCOD" NumberMaxValueCOD="9999999999" NumberPrecision="10"></cc1:mstextbox></FONT></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 128px; HEIGHT: 15px"></TD>
					<TD style="HEIGHT: 15px"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 128px; HEIGHT: 24px"><asp:label id="lblBatchDate" runat="server" CssClass="tableLabel">Batch Date:</asp:label></TD>
					<TD style="HEIGHT: 24px"><FONT face="Tahoma"><asp:label id="lblBatchDateDisplay" runat="server" CssClass="tableLabel"></asp:label></FONT></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 128px; HEIGHT: 23px"><asp:label id="lblTruckID" runat="server" CssClass="tableLabel">Truck ID:</asp:label></TD>
					<TD style="HEIGHT: 23px"><asp:label id="lblTruckIDDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 128px; HEIGHT: 24px"><asp:label id="lblBatchID" runat="server" CssClass="tableLabel">Batch ID:</asp:label></TD>
					<TD style="HEIGHT: 24px"><asp:label id="lblBatchIDDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 128px; HEIGHT: 24px"><asp:label id="lblBatchType" runat="server" CssClass="tableLabel">Batch Type:</asp:label></TD>
					<TD style="HEIGHT: 24px"><asp:label id="lblBatchTypeDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 128px; HEIGHT: 12px"><FONT face="Tahoma"></FONT></TD>
					<TD style="HEIGHT: 12px"><FONT face="Tahoma"></FONT></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 128px; HEIGHT: 22px"><asp:label id="lblUserDis" runat="server" CssClass="tableLabel">User:</asp:label></TD>
					<TD style="HEIGHT: 22px"><asp:label style="Z-INDEX: 0" id="lblUser" runat="server" CssClass="tableLabel"></asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 128px"><asp:label style="Z-INDEX: 0" id="lblLocationDis" runat="server" CssClass="tableLabel">Location:</asp:label></TD>
					<TD>
						<asp:dropdownlist style="Z-INDEX: 0" id="ddlCurrentLocation" runat="server" CssClass="textField" Width="106px"></asp:dropdownlist></TD>
				</TR>
			</TABLE>
			<asp:button style="DISPLAY:none" id="btnSubmitHidden" runat="server" Width="103px" CssClass="queryButton"
				Text="Test"></asp:button>
		</form>
		</FONT>
	</body>
</HTML>
