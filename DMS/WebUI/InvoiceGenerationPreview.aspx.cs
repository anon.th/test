using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.ties.BAL;
using com.ties.classes;
using com.common.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for InvoiceGenerationPreview.
	/// </summary>
	public class InvoiceGenerationPreview : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.WebControls.Label lblNumRec;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected com.common.util.msTextBox txtGoToRec;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Button btnSelectAllHC;
		protected System.Web.UI.WebControls.Button btnGenerate;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnSelectAll;
		protected System.Web.UI.WebControls.Label Label24;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.WebControls.Label lblSI_ServiceType;
		protected System.Web.UI.WebControls.Label lblSI_ActualPOD;
		protected System.Web.UI.WebControls.Label lblSI_DelivRoute;
		protected System.Web.UI.WebControls.Label lblSI_PickupDT;
		protected System.Web.UI.WebControls.Label lblSI_TotPackage;
		protected System.Web.UI.WebControls.Label lblSI_BookingDT;
		protected System.Web.UI.WebControls.Label lblSI_CusRef;
		protected System.Web.UI.WebControls.Label lblSI_consigNo;
		protected System.Web.UI.HtmlControls.HtmlTable Table10;
		protected System.Web.UI.WebControls.Label Label45;
		protected System.Web.UI.WebControls.Label Label48;
		protected System.Web.UI.WebControls.Label Label50;
		protected System.Web.UI.WebControls.Label Label46;
		protected System.Web.UI.WebControls.Label Label47;
		protected System.Web.UI.WebControls.Label Label49;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label lblInvoiceType;
		protected System.Web.UI.WebControls.Label lblCustID;
		protected System.Web.UI.WebControls.Label lblProvince;
		protected System.Web.UI.WebControls.Label lblCustName;
		protected System.Web.UI.WebControls.Label lblTelephone;
		protected System.Web.UI.WebControls.Label lblAddress1;
		protected System.Web.UI.WebControls.Label lblFax;
		protected System.Web.UI.WebControls.Label lblAddress2;
		protected System.Web.UI.WebControls.Label lblPostalCode;
		protected System.Web.UI.WebControls.Label lblHC_POD_Required;
		protected System.Web.UI.WebControls.DataGrid dgPreview;
		protected System.Web.UI.WebControls.Label lblMainTitle;

		private DataSet dsInvoiceHeader,dsInvoiceResult;
		private DataSet dsInvoiceQuery;
		private string m_strAppID, m_strEnterpriseID;
		private Utility ut;
		static private int m_iSetSize = 4;
		private SessionDS m_sdsInvoiceHeader = null;
		protected System.Web.UI.WebControls.Label lblHC_POD;
		string strInvType;


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			ut = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			
			m_strAppID = ut.GetAppID();
            m_strEnterpriseID = ut.GetEnterpriseID();

			if(!Page.IsPostBack)
			{
				//ViewState["QUERY_DS"] = m_sdsShipment.ds;

				ViewState["CPMode"] = ScreenMode.ExecuteQuery;
				ViewState["CPOperation"] = Operation.None;
				ViewState["IsTextChanged"] = false;
				ViewState["currentPage"] = 0;
				ViewState["currentSet"] = 0;
				this.GetRecSet();
				DisplayCurrentPage();
			}
			else
			{
					m_sdsInvoiceHeader = (SessionDS)Session["SESSION_DS1"];
					dsInvoiceQuery = (DataSet)Session["dsPreviewQUERY"];
			}

		}

		private void GetRecSet()
		{
			String tmpStr = ViewState["currentSet"].ToString().Trim();
			int iStartIndex = 0;
			int intIndexOf = tmpStr.IndexOf(".");
	
			if (intIndexOf > 0)
			{
				iStartIndex = Convert.ToInt32(tmpStr.Substring(0, intIndexOf)) * m_iSetSize;
			}
			else
			{
				iStartIndex = Convert.ToInt32(tmpStr) * m_iSetSize;
			}

			dsInvoiceQuery = (DataSet)Session["dsPreviewQUERY"];
			m_sdsInvoiceHeader  = InvoiceGenerationMgrDAL.GetInvoiceableShipmentsHeader(m_strAppID, m_strEnterpriseID,iStartIndex,m_iSetSize, dsInvoiceQuery);
			//m_sdsInvoiceHeader = TIESDAL.PostDeliveryCOD.GetPostDeliveryCODDS(utility.GetAppID(),utility.GetEnterpriseID(),iStartIndex,m_iSetSize,dsQuery);
			
			Session["SESSION_DS1"] = m_sdsInvoiceHeader;
			decimal pgCnt = (m_sdsInvoiceHeader.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < Convert.ToInt32(ViewState["currentSet"]))
			{
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
			}

			if(m_sdsInvoiceHeader.QueryResultMaxSize == 0 || m_sdsInvoiceHeader.QueryResultMaxSize == 1)
				EnableNavigationButtons(false,false,false,false);

		}


		public void DisplayCurrentPage()
		{
			
			//DataRow drCurrent = m_sdsInvoiceHeader.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];


			strInvType = (string)Session["Invoice_Type"];

			BindHeader(strInvType);
			DisplayRecNum();

		}

		public void BindHeader(string strInvType)
		{

			if(m_sdsInvoiceHeader.ds.Tables[0].Rows.Count <= 0) 
			{
				return;
			}

			DataRow drCurrent = m_sdsInvoiceHeader.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];

			//DataRow drCurrent = dsInvoiceResult.Tables[0].Rows[0];

//CLEAR ALL HEADER
			this.lblInvoiceType.Text = "";
			this.lblCustID.Text = "";
			this.lblCustName.Text = "";
			this.lblAddress1.Text = "";
			this.lblAddress2.Text = "";
			this.lblPostalCode.Text = "";
			this.lblProvince.Text = "";
			this.lblTelephone.Text = "";
			this.lblFax.Text = "";
			this.lblHC_POD_Required.Text = "";
//END OF CLEAR ALL HEADER
			
			
			if(strInvType=="CASH")
				this.lblInvoiceType.Text = "CASH";
			else
				this.lblInvoiceType.Text = "CREDIT";



//IN CASE OF INVOICE TYPE = CREDIT
			if(strInvType=="CREDIT")
			{
				if(drCurrent["payerid"]!=null&&(!drCurrent["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					this.lblCustID.Text = drCurrent["payerid"].ToString();
					ViewState["payerID"] = drCurrent["payerid"].ToString();
				}

				if(drCurrent["payer_name"]!=null&&(!drCurrent["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblCustName.Text = drCurrent["payer_name"].ToString();

				if(drCurrent["payer_address1"]!=null&&(!drCurrent["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblAddress1.Text = drCurrent["payer_address1"].ToString();

				if(drCurrent["payer_address2"]!=null&&(!drCurrent["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblAddress2.Text = drCurrent["payer_address2"].ToString();

				if(drCurrent["payer_zipcode"]!=null&&(!drCurrent["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					string strZipCode = drCurrent["payer_zipcode"].ToString();

					this.lblPostalCode.Text = strZipCode;

					Zipcode zipCode = new Zipcode();
					zipCode.Populate(this.m_strAppID,this.m_strEnterpriseID,strZipCode);
					String strStateCode = zipCode.StateCode;

					this.lblProvince.Text = strStateCode;
				}

				if(drCurrent["payer_telephone"]!=null&&(!drCurrent["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblTelephone.Text = drCurrent["payer_telephone"].ToString();

				if(drCurrent["payer_fax"]!=null&&(!drCurrent["payer_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblFax.Text = drCurrent["payer_fax"].ToString();

				if(drCurrent["pod_slip_required"]!=null&&(!drCurrent["pod_slip_required"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
						if(drCurrent["pod_slip_required"].ToString()=="N")
							this.lblHC_POD_Required.Text = "NO";
						else
							this.lblHC_POD_Required.Text = "YES";
					//drCurrent["pod_slip_required"].ToString();
				}
			}
//END OF INVOICE TYPE = CREDIT

//IN CASE OF INVOICE TYPE == "CASH"
			if(strInvType=="CASH") //&&drCurrent["CPayment_mode"].ToString()=="R")
			{
				if(drCurrent["payerid"]!=null&&(!drCurrent["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					this.lblCustID.Text = drCurrent["payerid"].ToString();
					ViewState["payerID"] = drCurrent["payerid"].ToString();
				}

				if(drCurrent["sender_name"]!=null&&(!drCurrent["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblCustName.Text = drCurrent["sender_name"].ToString();

				if(drCurrent["sender_address1"]!=null&&(!drCurrent["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblAddress1.Text = drCurrent["sender_address1"].ToString();

				if(drCurrent["sender_address2"]!=null&&(!drCurrent["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblAddress2.Text = drCurrent["sender_address2"].ToString();

				if(drCurrent["sender_zipcode"]!=null&&(!drCurrent["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					string strZipCode = drCurrent["sender_zipcode"].ToString();

					this.lblPostalCode.Text = strZipCode;

					Zipcode zipCode = new Zipcode();
					zipCode.Populate(this.m_strAppID,this.m_strEnterpriseID,strZipCode);
					String strStateCode = zipCode.StateCode;

					this.lblProvince.Text = strStateCode;
				}

				if(drCurrent["sender_telephone"]!=null&&(!drCurrent["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblTelephone.Text = drCurrent["sender_telephone"].ToString();

				if(drCurrent["sender_fax"]!=null&&(!drCurrent["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblFax.Text = drCurrent["sender_fax"].ToString();

					this.lblHC_POD_Required.Text = "NO";
			}
//END OF INVOICE TYPE = CASH
				
			dsInvoiceResult = InvoiceGenerationMgrDAL.GetInvoiceableShipmentsList(m_strAppID,m_strEnterpriseID,dsInvoiceQuery,this.lblCustID.Text,this.lblCustName.Text);

			BindGrid(dsInvoiceResult);
		
		
		}

		public void BindGrid(DataSet dsResult)
		{
				this.dgPreview.DataSource = dsResult;
				this.dgPreview.DataBind(); 

			DataSet dsInvoiceQuery = (DataSet)Session["dsPreviewQUERY"];
			string strPayerID = ViewState["payerID"].ToString();
//���������� DDLSC ���
			DataSet dsDDLSC = InvoiceGenerationMgrDAL.GetInvoiceableShipmentsCustomer(m_strAppID, m_strEnterpriseID,dsInvoiceQuery,this.lblCustID.Text,this.lblCustName.Text);
			
			//BIND DDL & SAVE BUTTON
			if(strInvType=="CREDIT")
			{//IF EQUALS TO CREDIT , DISABLED DDL & SAVE BUTTON - ELSE ENABLED ALL
				for(int i = 0; i <= dgPreview.Items.Count - 1; i++)
				{
					DropDownList ddlSCShipmentUpdate = (DropDownList)this.dgPreview.Items[i].FindControl("ddlSCShipmentUpdate");
					ddlSCShipmentUpdate.DataSource = dsDDLSC;
					ddlSCShipmentUpdate.DataTextField = "sender_name";
					ddlSCShipmentUpdate.DataValueField = "sender_name";
					ddlSCShipmentUpdate.DataBind();
					ddlSCShipmentUpdate.Enabled=false;

					ImageButton imgSave = (ImageButton)this.dgPreview.Items[i].FindControl("imgSave");
					imgSave.Visible = false;

					if(this.lblHC_POD_Required.Text=="NO")
					{
						CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");
						chkSelect.Checked = true;
					}
				}
			}
			if(strInvType=="CASH")
			{
				for(int i = 0; i <= dgPreview.Items.Count - 1; i++)
				{
					DropDownList ddlSCShipmentUpdate = (DropDownList)this.dgPreview.Items[i].FindControl("ddlSCShipmentUpdate");
					ddlSCShipmentUpdate.DataSource = dsDDLSC;
					ddlSCShipmentUpdate.DataTextField = "sender_name";
					ddlSCShipmentUpdate.DataValueField = "sender_name";
					ddlSCShipmentUpdate.DataBind();
					ddlSCShipmentUpdate.Enabled=true;

					ImageButton imgSave = (ImageButton)this.dgPreview.Items[i].FindControl("imgSave");
					imgSave.Visible = true;

					//IF HC POD REQUIRED = NO, CHECK ALL
					if(this.lblHC_POD_Required.Text=="NO")
					{
						CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");
						chkSelect.Checked = true;
					}
				}
			}

		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Response.Redirect("InvoiceGeneration.aspx");
		}


		private void btnNextPage_Click(object sender, System.EventArgs e)
		{
			EnableNavigationButtons(true,true,true,true);
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  < (Convert.ToInt32(m_sdsInvoiceHeader.DataSetRecSize) - 1) )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) + 1;
				DisplayCurrentPage();
			}
			else
			{
				decimal iTotalRec = (Convert.ToInt32(ViewState["currentRefSet"]) * m_iSetSize) + Convert.ToInt32(m_sdsInvoiceHeader.DataSetRecSize);
				if( iTotalRec ==  m_sdsInvoiceHeader.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) + 1;	
				GetRecSet();
				ViewState["currentPage"] = 0;
				DisplayCurrentPage();
			} 
			DisplayRecNum();
		}

		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = (int)Math.Floor((double)(m_sdsInvoiceHeader.QueryResultMaxSize - 1)/m_iSetSize);//(m_sdsInvoiceHeader.QueryResultMaxSize - 1)/m_iSetSize;	
			GetRecSet();
			ViewState["currentPage"] = (int)Math.Abs(Math.IEEERemainder((double)(m_sdsInvoiceHeader.QueryResultMaxSize - 1),(double)m_iSetSize)); //m_sdsInvoiceHeader.DataSetRecSize - 1;
			DisplayCurrentPage();	
			DisplayRecNum();
			EnableNavigationButtons(true,true,false,false);
		}

		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  > 0 )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) - 1;
				DisplayCurrentPage();
			}
			else
			{
				if( (Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) - 1;	
				GetRecSet();
				System.Threading.Thread.Sleep(3500);

				ViewState["currentPage"] = m_sdsInvoiceHeader.DataSetRecSize - 1;
				DisplayCurrentPage();
			}
			DisplayRecNum();
			EnableNavigationButtons(true,true,true,true);

		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = 0;	
			ViewState["currentPage"] = 0;
			GetRecSet();
			DisplayCurrentPage();
			DisplayRecNum();
			EnableNavigationButtons(false,false,true,true);
		}


		private void DisplayRecNum()
		{
			int iCurrentRec = (Convert.ToInt32(ViewState["currentPage"]) + 1) + (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) ;

			if(!ut.GetUserCulture().ToUpper().Equals("EN-US"))
			{
				lblNumRec.Text = ""+ iCurrentRec + " " + Utility.GetLanguageText(ResourceType.ScreenLabel, "of", ut.GetUserCulture()) + " " + m_sdsInvoiceHeader.QueryResultMaxSize + " " + Utility.GetLanguageText(ResourceType.ScreenLabel, "record(s)", ut.GetUserCulture());
			}
			else
			{
				lblNumRec.Text = ""+ iCurrentRec + " of " + m_sdsInvoiceHeader.QueryResultMaxSize + " record(s)";
			}

			//ADDED BY X FEB 22 08
			if(iCurrentRec>=m_sdsInvoiceHeader.QueryResultMaxSize)
			{
				EnableNavigationButtons(true,true,false,false);
				return;
			}
			//END ADDED BY X FEB 22 08
		}

		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}

		private void btnSelectAll_Click(object sender, System.EventArgs e)
		{
			for(int i = 0; i <= dgPreview.Items.Count - 1; i++)
			{

				CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");
				if(!chkSelect.Checked)
				{
					chkSelect.Checked = true;
				}
				else
				{
					chkSelect.Checked = false;
				}


				if(!this.btnSelectAll.TabIndex.Equals("1"))
				{
					this.btnSelectAll.TabIndex = 1;
					this.btnSelectAll.Text="Deselect All";
				}
				else
				{
					this.btnSelectAll.TabIndex = 2;
					this.btnSelectAll.Text="Select All";
				}

			}
		}





	}
}
