<%@ Page language="c#" Codebehind="PublicHolidays.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.PublicHolidays" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Public Holidays</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript">
		
		function CheckFromDate(aText)
		{		
			if (aText.value !="")
			{
				var myDays=["Sun","Mon","Tue","Wed","Thur","Fri","Sat","Sun"];
				var dt=aText.value;							
				var d=dt.substring(3,5)+'/'+dt.substring(0,2)+'/'+dt.substring(6,10);
				
				myDate=new Date(eval('"'+d+'"'));		
				var day =myDays[myDate.getDay()];		
				var Ctlname=aText.name;		
				var arr=Ctlname.split(':')
				//console.log(myDate);
				//console.log(day);

				//console.log(Ctlname);
				//console.log(arr);
				var actualCtl=eval(arr[0]+'_'+arr[1]+'_lblDayFrom');		
				//console.log(actualCtl);

				actualCtl.innerHTML=day;
			}
			else
			{
				var Ctlname=aText.name;		
				var arr=Ctlname.split(':')				
				var actualCtl=eval(arr[0]+'_'+arr[1]+'_lblDayFrom');		
				actualCtl.innerHTML="";
			}
		}
		
		function CheckToDate(aText)
		{	
			if (aText.value !="")
			{	
				var myDays=["Sun","Mon","Tue","Wed","Thur","Fri","Sat","Sun"];
				var dt=aText.value;							
				var d=dt.substring(3,5)+'/'+dt.substring(0,2)+'/'+dt.substring(6,10);
				//alert(d);
				myDate=new Date(eval('"'+d+'"'));		
				var day =myDays[myDate.getDay()];		
				var Ctlname=aText.name;		
				var arr=Ctlname.split(':')		
				var actualCtl=eval(arr[0]+'_'+arr[1]+'_lblDayTo');		
				actualCtl.innerHTML=day;
			}
			else
			{
				var Ctlname=aText.name;		
				var arr=Ctlname.split(':')		
				var actualCtl=eval(arr[0]+'_'+arr[1]+'_lblDayTo');		
				actualCtl.innerHTML="";
			}
		}
		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="PublicHoliday" method="post" runat="server">
			<asp:validationsummary id="PageValidationSummary" runat="server" HeaderText="Please check the following Invalid/Mandatory field(s):"
				DisplayMode="BulletList" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary>
			<asp:CheckBox id="chkApplyHolAll" style="Z-INDEX: 107; POSITION: absolute; TOP: 64px; LEFT: 400px"
				runat="server" Width="192px" Text="Apply holiday to all states" Enabled="False"></asp:CheckBox>
			<asp:button id="btnQuery" style="Z-INDEX: 101; POSITION: absolute; TOP: 63px; LEFT: 18px" runat="server"
				CssClass="queryButton" Text="Query" Width="100px" CausesValidation="False"></asp:button><asp:button id="btnExecuteQuery" style="Z-INDEX: 103; POSITION: absolute; TOP: 63px; LEFT: 119px"
				runat="server" CssClass="queryButton" Text="Execute Query" Width="100px" Enabled="False" CausesValidation="False"></asp:button><asp:button id="btnInsert" style="Z-INDEX: 102; POSITION: absolute; TOP: 63px; LEFT: 220px"
				runat="server" CssClass="queryButton" Text="Insert" Width="100px" CausesValidation="False"></asp:button>&nbsp;
			<asp:datagrid id="dgPublicHoliday" PageSize="50" style="Z-INDEX: 104; POSITION: absolute; TOP: 125px; LEFT: 18px"
				runat="server" Width="650px" OnItemDataBound="OnItemBound_PublicHoliday" AllowCustomPaging="True"
				AllowPaging="True" OnPageIndexChanged="OnPublicHoliday_PageChange" OnEditCommand="OnEdit_PublicHoliday"
				OnUpdateCommand="OnUpdate_PublicHoliday" OnCancelCommand="OnCancel_PublicHoliday" AutoGenerateColumns="False"
				OnDeleteCommand="OnDelete_PublicHoliday" OnItemCommand="dgPublicHoliday_Button" ItemStyle-Height="20">
				<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
				<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
						CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="6%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton"
						CommandName="Delete">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="State Code" HeaderStyle-Font-Bold="true">
						<HeaderStyle Width="13%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblCode" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"State_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtCode" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"State_code")%>' Runat="server" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore" >
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="rcStateCode" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtCode"
								ErrorMessage="State Code "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton"
						CommandName="StateSearch" Visible="true">
						<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Country" HeaderStyle-Font-Bold="true">
						<HeaderStyle Width="13%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblCountry" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"Country")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtCountry" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"Country")%>' Runat="server" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="rcCountry" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtCountry"
								ErrorMessage="Country "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Date From" HeaderStyle-Font-Bold="true">
						<HeaderStyle Width="12%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblDateFrom" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"DateFrom","{0:dd/MM/yyyy}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtDateFrom" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"DateFrom","{0:dd/MM/yyyy}")%>' Runat="server" MaxLength="10" TextMaskString="99/99/9999" TextMaskType="msDate" onblur="return CheckFromDate(this)">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="rcDateFrom" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtDateFrom"
								ErrorMessage="Date From "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="From">
						<HeaderStyle Width="7%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblDayFrom" Visible="True" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"DateFrom","{0:ddd}")%>'>
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Date To" HeaderStyle-Font-Bold="true">
						<HeaderStyle Width="12%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblDateTo" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"DateTo","{0:dd/MM/yyyy}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtDateTo" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"DateTo","{0:dd/MM/yyyy}")%>' Runat="server" MaxLength="10" TextMaskString="99/99/9999" TextMaskType="msDate" onblur="return CheckToDate(this)">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="rcDateTo" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtDateTo"
								ErrorMessage="Date To "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="To">
						<HeaderStyle Width="7%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblDayTo" Visible="True" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"DateTo","{0:ddd}")%>'>
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Holiday Description">
						<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblDescription" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"Holiday_description")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtDescription" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"Holiday_description")%>' Runat="server" MaxLength="200">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:label id="lblPHMessage" style="Z-INDEX: 105; POSITION: absolute; TOP: 94px; LEFT: 22px"
				runat="server" CssClass="errorMsgColor" Width="669px"></asp:label><asp:label id="Label1" style="Z-INDEX: 106; POSITION: absolute; TOP: 16px; LEFT: 27px" runat="server"
				Width="399px" Height="37px" CssClass="mainTitleSize">Public Holiday</asp:label></form>
	</body>
</HTML>
