using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using com.common.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;
using com.ties.DAL;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CustomerDetails.
	/// </summary>
	public class CreditStatusTermPopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.TextBox txtCustName;
		protected System.Web.UI.WebControls.Button btnRefresh;
		private String appID = null;
		private String enterpriseID = null;
		SessionDS m_sdsCustomer =null;
		private String CustID = "";
		private String FormID = "";
		string mode="";
		//Utility utility = null;
		
	

		//private String hdCreditLimit_CID = "";

		
		protected System.Web.UI.WebControls.Label CustIDNameErr;
		protected System.Web.UI.WebControls.Label lblCustIDName;
		protected System.Web.UI.WebControls.CustomValidator rdo;
		protected System.Web.UI.WebControls.Label lblReqPaymentMode;
		protected System.Web.UI.WebControls.Label lblPaymentMode;
		protected System.Web.UI.WebControls.Label lblCreditLimit;
		protected System.Web.UI.WebControls.Label lblCreditTerm;
		protected System.Web.UI.WebControls.Label lblCreditStatus;
		protected System.Web.UI.WebControls.Label lblReasonCode;
		protected System.Web.UI.WebControls.Label lblCreditUsed;
		protected System.Web.UI.WebControls.Label lblCreditAvailable;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.DropDownList ddlCreditStatus;
		protected System.Web.UI.WebControls.DropDownList ddlReason;
		protected System.Web.UI.WebControls.TextBox txtMasterAccountID;
		protected System.Web.UI.WebControls.Button btncancel;
		protected System.Web.UI.WebControls.Button btnOk;
		protected System.Web.UI.WebControls.RadioButton radPaymentCash;
		protected System.Web.UI.WebControls.RadioButton radCredit;
		protected System.Web.UI.WebControls.DropDownList ddlCreditTerms;
		protected System.Web.UI.WebControls.TextBox txtReason;
		protected System.Web.UI.WebControls.DataGrid dgdata;
		protected com.common.util.msTextBox txtCreditLimit;
		protected System.Web.UI.WebControls.TextBox txtCustID;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.CheckBox chkcreditLimitUsed;
		protected System.Web.UI.HtmlControls.HtmlGenericControl FONT1;
		protected System.Web.UI.HtmlControls.HtmlTableRow trcustomerID;
		protected System.Web.UI.HtmlControls.HtmlTableRow trMasterAccount;
		protected System.Web.UI.HtmlControls.HtmlTableRow trMALCU;
		protected System.Web.UI.WebControls.TextBox txtMasterAccount;
		protected System.Web.UI.HtmlControls.HtmlTableRow trPayment;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdmode;
		protected com.common.util.msTextBox txtcreditAva;
		protected com.common.util.msTextBox txtcreditused;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label lblrestatus;
		protected System.Web.UI.WebControls.Label lblreReason;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdpage;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Label lblreCreditTerms;
		protected com.common.util.msTextBox txtCreditThred;
	

		private void Page_Load(object sender, System.EventArgs e)
		{
			
			// Put user code to initialize the page here
		
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
				
			mode = Request.Params["mode"];
			FormID = Request.Params["FormID"];
			hdmode.Value = Request.Params["mode"];
			hdpage.Value = FormID;

			if (!IsPostBack)
			{
			//	m_sdsCustomer = GetEmptyCustomer(0); 
			//	txtCustID.Text = Request.Params["CUSTID_TEXT"];
				DataSet profileDS = SysDataManager1.GetEnterpriseProfile(utility.GetAppID(),utility.GetEnterpriseID());

				//Call function for rounding in TIESUtility  (by Sittichai 08/01/2008)
				if(profileDS.Tables[0].Rows.Count > 0)
				{
					int currency_decimal = (int) profileDS.Tables[0].Rows[0]["currency_decimal"];
					ViewState["m_format"] = "{0:F" + currency_decimal.ToString() + "}";
					Session["m_format"] = "{0:F" + currency_decimal.ToString() + "}";

					if  ( profileDS.Tables[0].Rows[0]["wt_rounding_method"] != System.DBNull.Value)
					{
						
						ViewState["wt_rounding_method"] = Convert.ToInt32(profileDS.Tables[0].Rows[0]["wt_rounding_method"]);
					}

					if  ( profileDS.Tables[0].Rows[0]["wt_increment_amt"] != System.DBNull.Value)
					{
						ViewState["wt_increment_amt"] = Convert.ToDecimal(profileDS.Tables[0].Rows[0]["wt_increment_amt"]);
					}
				}	



				this.RegisterStartupScript("pageload","<script language=javascript>document.getElementById('txtCreditLimit').select();</script>");
				btnOk.Attributes.Add("onclick","return checkbeforesave();");
				
				if(FormID.ToUpper().Equals("MA"))
				{
					trcustomerID.Visible = false;
					trPayment.Visible = false;

					trMALCU.Visible = true;
					trMasterAccount.Visible = true;
					txtMasterAccount.Text = Request.Params["CUSTID"];

					txtcreditused.Text = String.Format((String)Session["m_format"],Convert.ToDouble(getCreditused(txtMasterAccount.Text).ToString()));
					txtcreditAva.Text = String.Format((String)Session["m_format"],Convert.ToDouble(getMasterCreditLimit(txtMasterAccount.Text).ToString())-Convert.ToDouble(txtcreditused.Text));


					
				
					if(mode.Equals("A"))
					{
						
					//	ddlCreditStatus.Enabled = false;
						ddlReason.Enabled = false;
						lblreReason.Visible = false;
						lblreCreditTerms.Visible = true;						
						//lblrestatus.Visible = false;
					}
					else
					{
						lblrestatus.Visible = true;
						lblreReason.Visible = true;
						lblreCreditTerms.Visible=true;
						txtcreditAva.Enabled = false;
						txtcreditused.Enabled = false;
						//checkCustomerCreditusedcheck();
					}

				}
				else
				{

					this.dgdata.Columns[9].Visible = false;
					if(Request.Params["type"].ToString().ToUpper().Equals("R"))
					{
						radCredit.Checked=true;
						radPaymentCash.Checked=false;
					}
					else
					{
						radCredit.Checked=false;
						radPaymentCash.Checked=true;
					}
					if(mode.Equals("A"))
					{
						
						//	ddlCreditStatus.Enabled = false;
						ddlReason.Enabled = false;
						lblreReason.Visible = false;
						lblreCreditTerms.Visible = true;					
						//lblrestatus.Visible = false;
					}
					else
					{
						lblrestatus.Visible = true;
						lblreReason.Visible = true;
						txtcreditAva.Enabled = false;
						txtcreditused.Enabled = false;
						//checkCustomerCreditusedcheck();
						CustomerCreditused cs = new CustomerCreditused();					

						//Comment by Chai 03/02/2012
						//txtcreditused.Text = String.Format((String)Session["m_format"],Convert.ToDouble(cs.getTotalCreditUsed(this.appID,this.enterpriseID,Request.Params["CUSTID"].ToString()).ToString()));
						//txtcreditAva.Text = String.Format((String)Session["m_format"],Convert.ToDouble(cs.getTotalCreditLimit(this.appID,this.enterpriseID,Request.Params["CUSTID"].ToString()).ToString())-Convert.ToDouble(txtcreditused.Text));
						if(Request.QueryString["creditused"] != null && Request.QueryString["creditused"].ToString().Length>0)
						{
							txtcreditused.Text = String.Format((String)Session["m_format"],Convert.ToDouble(Request.QueryString["creditused"].ToString()));
						}
						if(Request.QueryString["creditavailable"] != null && Request.QueryString["creditavailable"].ToString().Length>0)
						{
							txtcreditAva.Text = String.Format((String)Session["m_format"],Convert.ToDouble(Request.QueryString["creditavailable"].ToString()));
						}
					}

					trcustomerID.Visible = true;
					trPayment.Visible = true;

					trMALCU.Visible = false;
					trMasterAccount.Visible = false;
					txtCustID.Text = Request.Params["CUSTID"];

				}
				
				CustID = Request.Params["CUSTID"];
			
				txtCustName.Text = Request.Params["CUSTNAME"];

		
			//	BindGrid();
				//checkCustomerCreditusedcheck();
				Read();
			}
			else
			{
				//m_sdsCustomer = (SessionDS)ViewState["CUST_DS"];
			}
			checkcredittocash(Request.Params["FormID"]);

		}


		private void Add()
		{

		}

		private void checkCustomerCreditusedcheck()
		{
			if(!FormID.Equals("MA"))
			{
				CustomerCreditused cs = new CustomerCreditused();
				txtcreditused.Text = String.Format((String)Session["m_format"],Convert.ToDouble(cs.getTotalCreditUsed(appID ,enterpriseID ,Request.Params["CUSTID"].ToString())));
				txtcreditAva.Text = String.Format((String)Session["m_format"],Convert.ToDouble(cs.getTotalCreditLimit(appID ,enterpriseID ,Request.Params["CUSTID"].ToString()) - double.Parse(txtcreditused.Text)));
			}
			else
			{
				if(chkcreditLimitUsed.Checked)
				{
					//txtCreditLimit.Enabled = false;
					//txtCreditThred.Enabled = false;
				
					txtCreditLimit.Text = getCreditLimitall(txtMasterAccount.Text).ToString();
					txtcreditused.Text = String.Format((String)Session["m_format"],Convert.ToDouble(getCreditused(txtMasterAccount.Text).ToString()));
					txtCreditThred.Text = "90%";
					double totalava = double.Parse(txtCreditLimit.Text) - double.Parse(txtcreditused.Text);
					txtcreditAva.Text =  String.Format((String)Session["m_format"],Convert.ToDouble(totalava.ToString()));
					if(totalava > 0)
					{
						ddlCreditStatus.SelectedValue = "A";
					}
				
				}
				else
				{
					txtMasterAccount.Enabled = false;
					txtCreditLimit.Text ="";
					CustomerCreditused cs = new CustomerCreditused();

					txtcreditused.Text = String.Format((String)Session["m_format"],Convert.ToDouble(getCreditused(txtMasterAccount.Text).ToString()));
					txtcreditAva.Text = String.Format((String)Session["m_format"],Convert.ToDouble(getMasterCreditLimit(txtMasterAccount.Text).ToString())-Convert.ToDouble(txtcreditused.Text));

					txtCreditThred.Text = "";
					ddlCreditStatus.SelectedValue = "";

					txtCreditLimit.Enabled = true;
					txtCreditThred.Enabled = true;

				}
			}

		}

		private void Read()
		{
			
			//string 	strcustomercreditid = "0";
			DataTable dt;
			FormID = Request.Params["FormID"];
			if(!FormID.Equals("MA"))
			{
				 dt = getCustomerCrditData(txtCustID.Text);
			}
			else
			{
				dt = getCustomerCrditData(txtMasterAccount.Text);
			}
			dgdata.DataSource = dt;
			dgdata.DataBind();

			DataTable dtStatusCodes = new DataTable();
			dtStatusCodes.Columns.Add(new DataColumn("code_text", typeof(string)));
			dtStatusCodes.Columns.Add(new DataColumn("code_str_value", typeof(string)));
		
			DataRow drNilRow = dtStatusCodes.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtStatusCodes.Rows.Add(drNilRow);

			ArrayList listStatus = Utility.GetCodeValues(this.appID,utility.GetUserCulture(), "cust_status_reason", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listStatus)
			{
				DataRow drEach = dtStatusCodes.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtStatusCodes.Rows.Add(drEach);
			}
			DataView dvCodes = new DataView(dtStatusCodes);

			ddlReason.DataSource = dvCodes;
			ddlReason.DataTextField = "code_text";
			ddlReason.DataValueField = "code_str_value";
			ddlReason.DataBind();

			dtStatusCodes.Rows.Clear();

			drNilRow = dtStatusCodes.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtStatusCodes.Rows.Add(drNilRow);

			ArrayList listStatusCreditTerm = Utility.GetCodeValues(this.appID,utility.GetUserCulture(), "cust_credit_term", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listStatusCreditTerm)
			{
				DataRow drEach = dtStatusCodes.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtStatusCodes.Rows.Add(drEach);
			}
			DataView dvCreditTerm = new DataView(dtStatusCodes);

			ddlCreditTerms.DataSource = dvCreditTerm;
			ddlCreditTerms.DataTextField = "code_text";
			ddlCreditTerms.DataValueField = "code_str_value";
			ddlCreditTerms.DataBind();

			
			//
//			dt.Dispose();
//			dt = new DataTable();
//			dt = SysDataMgrDAL.GetCodeValues(appID,"cust_cr_status",enterpriseID);
//			ddlCreditStatus.DataSource = dt;
//			ddlCreditStatus.DataTextField = "code_text";
//			ddlCreditStatus.DataValueField = "code_str_value";
//			ddlCreditStatus.DataBind();
			ddlCreditStatus.Items.Add(new ListItem("",""));
			ddlCreditStatus.Items.Add(new ListItem("AVAILABLE","A"));
			ddlCreditStatus.Items.Add(new ListItem("NOT AVAILABLE","N"));

		





		}

		// ��ͧ�������� class

		private double getCreditLimitall(string custid)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = "";

			FormID = Request.Params["FormID"];
			
			strQry = " select isnull(sum(credit_limit),0) as creditlimit from customer ";
			strQry += " where master_account = '" + custid + "'";
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			DataSet  roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			//cnt = roleData.Tables[0].Rows.Count;
			if(roleData.Tables[0].Rows.Count > 0)
			{
				return double.Parse(roleData.Tables[0].Rows[0]["creditlimit"].ToString());
			}
			else
			{
				return 0;
			}
		}
		private double getMasterCreditLimit(string masterCustid)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = "";

			FormID = Request.Params["FormID"];
			
			strQry = " select top 1 creditlimit from customer_credit ";
			strQry += " where isMasterAccount = 'Y' and custid = '" + masterCustid + "' order by updatedDate desc";
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			DataSet  roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			//cnt = roleData.Tables[0].Rows.Count;
			if(roleData.Tables[0].Rows.Count > 0)
			{
				return double.Parse(roleData.Tables[0].Rows[0]["creditlimit"].ToString());
			}
			else
			{
				return 0;
			}
		}

		private double getsumsumcompany(string custid,string appid,string enpid,string masteraccont)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = "";

			FormID = Request.Params["FormID"];
			
			DataSet  roleData;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			//			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			//			DataSet  roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			//cnt = roleData.Tables[0].Rows.Count;
			if(masteraccont.Length > 0)
			{
				//return double.Parse(roleData.Tables[0].Rows[0]["creditlimit"].ToString());
				//string macc = roleData.Tables[0].Rows[0]["master_account"].ToString();
				dbCmd = null;

				strQry = " select  sum(credit_limit) as credit_limit  from customer ";
				strQry += " where master_account = '" + masteraccont + "' ";
				strQry += " and applicationid = '" + appid  + "' ";
				strQry += " and enterpriseid = '" + enpid + "' ";
				strQry += " and custid <> '" + custid + "' ";
				


				dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
				roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if(roleData.Tables[0].Rows.Count > 0)
				{
					if(roleData.Tables[0].Rows[0]["credit_limit"].ToString().Length > 0)
					{
						double creditlimit = double.Parse(roleData.Tables[0].Rows[0]["credit_limit"].ToString());
						return creditlimit;
					}
					
				}
				else
				{
					return 0;
				}

					
			}
		
				return 0;
		
		}
		private bool validlimitmasterAccount(string custid,string appid,string enpid,string masteraccont,double newCreditlimit)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = "";

			FormID = Request.Params["FormID"];
			
			DataSet  roleData;
		
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
//			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
//			DataSet  roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			//cnt = roleData.Tables[0].Rows.Count;
			if(masteraccont.Length > 0)
			{
				double summasaccount = 0;
				//return double.Parse(roleData.Tables[0].Rows[0]["creditlimit"].ToString());
				//string macc = roleData.Tables[0].Rows[0]["master_account"].ToString();
				 dbCmd = null;

				strQry = " select top 1 creditlimit from customer_credit ";
				strQry += " where custid = '" + masteraccont + "' ";
				strQry += " and applicationid = '" + appid  + "' ";
				strQry += " and enterpriseid = '" + enpid + "' ";
				strQry += " and isMasterAccount = 'Y' ";
				strQry += " order by updateddate desc ";


				 dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
				roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if(roleData.Tables[0].Rows.Count > 0)
				{
					summasaccount = getsumsumcompany(custid,appid,enpid,masteraccont);
					CustIDNameErr.Text = "";
					double creditlimit = double.Parse(roleData.Tables[0].Rows[0]["creditlimit"].ToString());
					double total = (creditlimit - (summasaccount + newCreditlimit));
					if(total < 0)
					{
						CustIDNameErr.Text = " Credit Limit out of range ";
						return false;
					}
					
				}
				else
				{
					CustIDNameErr.Text = " Please set Credit Limit of " + masteraccont ;
					return false;
				}
				
			}
			else
			{
				return true;
			}

			return true;
		}

		private double getCreditused(string custid)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = "";

			FormID = Request.Params["FormID"];
			
			strQry = " select isnull(sum(credit_used),0) as creditused from customer ";
			strQry += " where master_account = '" + custid + "'";
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			DataSet  roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			//cnt = roleData.Tables[0].Rows.Count;
			if(roleData.Tables[0].Rows.Count > 0)
			{
				return double.Parse(roleData.Tables[0].Rows[0]["creditused"].ToString());
			}
			else
			{
				return 0; 
			}
		}
		private double getCreditusedByCustProfile(string masterCustid)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = "";

			FormID = Request.Params["FormID"];

			strQry = " select isnull(sum(credit_used),0) as creditused from customer ";
			strQry += " where master_account = '" + masterCustid + "'";
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			DataSet  roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			//cnt = roleData.Tables[0].Rows.Count;
			if(roleData.Tables[0].Rows.Count > 0)
			{
				return double.Parse(roleData.Tables[0].Rows[0]["creditused"].ToString());
			}
			else
			{
				return 0; 
			}
		}
		private DataTable getCustomerCrditData(string strcustid)
		{
	
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = "";

			FormID = Request.Params["FormID"];
			if(!FormID.Equals("MA"))
			{
				strQry = " select customer_Credit.*,Customer.cust_name from customer_credit ";
				strQry +=  " inner join Customer on Customer.custid  = customer_credit.custid ";
				strQry += " where customer_credit.custid  = '" + strcustid + "'";
				strQry += " and customer_credit.isMasterAccount  <> 'Y'";
				strQry += " order by updateddate desc ";
			}
			else
			{
				strQry = " select customer_Credit.* from customer_credit   ";
				strQry +=  " inner join Core_System_Code on Core_System_Code.code_text  = customer_credit.custid   ";
				strQry += " where customer_credit.custid  = '" + txtMasterAccount.Text + "' and customer_credit.isMasterAccount  = 'Y'";
				strQry += " and Core_System_Code.codeid = 'master_account' ";
				strQry += " order by updateddate desc ";

			}
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			DataSet  roleData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			//cnt = roleData.Tables[0].Rows.Count;
			
			return roleData.Tables[0];
			
		}


		public static SessionDS GetEmptyCustomer(int intRows)
		{
			DataTable dtCustomer = new DataTable();
			dtCustomer.Columns.Add(new DataColumn("custid", typeof(string)));
			dtCustomer.Columns.Add(new DataColumn("cust_name", typeof(string)));
			dtCustomer.Columns.Add(new DataColumn("address1", typeof(string)));
			dtCustomer.Columns.Add(new DataColumn("address2", typeof(string)));
			dtCustomer.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtCustomer.Columns.Add(new DataColumn("apply_esa_surcharge",typeof(string)));    
			for(int i=0; i < intRows; i++)
			{
				DataRow drEach = dtCustomer.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				dtCustomer.Rows.Add(drEach);
			}
			DataSet dsCustIdFields = new DataSet();
			dsCustIdFields.Tables.Add(dtCustomer);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsCustIdFields;
			sessionDS.DataSetRecSize = dsCustIdFields.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return sessionDS;
		}

	
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.chkcreditLimitUsed.CheckedChanged += new System.EventHandler(this.chkcreditLimitUsed_CheckedChanged);
			this.radPaymentCash.CheckedChanged += new System.EventHandler(this.radPaymentCash_CheckedChanged);
			this.radCredit.CheckedChanged += new System.EventHandler(this.radCredit_CheckedChanged);
			this.ddlReason.SelectedIndexChanged += new System.EventHandler(this.ddlReason_SelectedIndexChanged);
			this.dgdata.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgdata_ItemDataBound);
			this.ID = "CreditStatusTermPopup";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	
//		private void btnSearch_Click(object sender, System.EventArgs e)
//		{
//			StringBuilder strQry = new StringBuilder();
//			strQry.Append("select * from customer where applicationid='");
//			strQry.Append(appID);
//			strQry.Append("' and enterpriseid = '");
//			strQry.Append(enterpriseID);
//			strQry.Append("'");
//			if (txtCustID.Text.ToString() != null && txtCustID.Text.ToString() != "")
//			{
//				strQry.Append(" and custid like '%");
//				strQry.Append( Utility.ReplaceSingleQuote(txtCustID.Text.ToString()));
//				strQry.Append("%'");
//			}
//			if (txtCustName.Text.ToString() != null && txtCustName.Text.ToString() != "")
//			{
//				strQry.Append(" and cust_name like N'%");
//				strQry.Append(Utility.ReplaceSingleQuote(txtCustName.Text.ToString()));
//				strQry.Append("%'");
//			}
//			if (strCustType != null && strCustType.ToString() != "")
//			{
//				String tmpStrCustType = Utility.ReplaceSingleQuote(strCustType.ToString().Trim());
//				String newStrCustType = "";
//
//				if(tmpStrCustType.Length == 1)
//				{
//					newStrCustType = "'" + tmpStrCustType + "'";
//				}
//				else
//				{
//					String[] arrOfStr = tmpStrCustType.Split(',');
//
//					foreach(String str in arrOfStr)
//					{
//						newStrCustType += "'" + str + "',";
//					}
//
//					newStrCustType = newStrCustType.Substring(0, newStrCustType.Length - 1);
//				}
//				
//				strQry.Append(" and payer_type in (");
//				strQry.Append(newStrCustType);
//				strQry.Append(")");
//			}
//
//			strQry.Append(" and status_active = 'Y'");
//
//			if (strSalesID != null && strSalesID.ToString() != "")
//			{
//				strQry.Append(" and salesmanid = '");
//				strQry.Append(Utility.ReplaceSingleQuote(strSalesID));
//				strQry.Append("'");
//			}
//
//			String strSQLQuery = strQry.ToString();
//			ViewState["SQL_QUERY"] = strSQLQuery;
//			dgdata.CurrentPageIndex = 0;
//			ShowCurrentPage();
//		}

		private void BindGrid()
		{
			dgdata.VirtualItemCount = System.Convert.ToInt32(m_sdsCustomer.QueryResultMaxSize);
			dgdata.DataSource = m_sdsCustomer.ds;
			dgdata.DataBind();
			ViewState["CUST_DS"] = m_sdsCustomer;
		}

	
		private SessionDS GetCUSTDDS(int iCurrent, int iDSRecSize, String strQuery)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerPopup.aspx.cs","GetCUSTDDS","ERR001","DbConnection object is null!!");
				return sessionDS;
			}
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"ZoneCode");
			return  sessionDS;
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
		//	string strFormID = "CustomerProfile";
			String sScript = "";
			sScript += "<script language=javascript>";
			FormID = Request.Params["FormID"];
			string mas = Request.Params["masteraccont"];
			string creditStatus;
	
			if(!FormID.Equals("MA"))
			{
//				if(radCredit.Checked)
//				{	txtCreditLimit.Text = txtCreditLimit.Text.Length > 0 ? txtCreditLimit.Text : "0";
//					if(!validlimitmasterAccount(txtCustID.Text,appID,enterpriseID,mas,double.Parse(txtCreditLimit.Text)))
//					{
//						return;
//					}
//				}
				string paymentmode;
				if(radPaymentCash.Checked)
				{
					paymentmode = "C";
					txtCreditLimit.Text="0";
					ddlCreditTerms.SelectedIndex=0;
					ddlCreditStatus.SelectedIndex=0;
					txtCreditThred.Text="0";					
				}
				else
				{
					paymentmode = "R";
				}
				

				Hashtable ht = new Hashtable(8);	
				ht.Add("CreditLimit",txtCreditLimit.Text);
				ht.Add("PaymentMode",paymentmode);
				ht.Add("CreditTerm",ddlCreditTerms.SelectedValue);
				if(ddlCreditStatus.SelectedIndex!=0)
				{
					ht.Add("CreditStatus",ddlCreditStatus.SelectedValue);
					creditStatus = ddlCreditStatus.SelectedItem.Text;
				}
				else
				{
					ht.Add("CreditStatus","NOT AVAILABLE");
					creditStatus = "NOT AVAILABLE";
				}
				if(ddlReason.SelectedValue == "")
				{
					ht.Add("ReasonCode","");
				}
				else
				{
					ht.Add("ReasonCode",ddlReason.SelectedItem.Text);
				}
				ht.Add("ReasonDesc",txtReason.Text);
				ht.Add("CreditThreshold",txtCreditThred.Text);
				ht.Add("CreditUsed",txtcreditused.Text);
				Session["CustCreditInfo"] = ht;



				sScript += "  window.opener.CustomerProfile.txtCreditLimit.value = '" + txtCreditLimit.Text + "';";
				if(radPaymentCash.Checked)
				{
					sScript += "  window.opener.CustomerProfile.txtCreditUsed.value = '" + txtcreditused.Text + "';";
					sScript += "  window.opener.CustomerProfile.txtCreditAvailable.value = '" + txtcreditAva.Text + "';";
				}
				sScript += "  window.opener.CustomerProfile.txtCreditTerm.value = '" + (ddlCreditTerms.SelectedValue.Length>0? ddlCreditTerms.SelectedValue.ToString():"0")+ "';";
				sScript += "  window.opener.CustomerProfile.txtCreditStatus.value = '" + creditStatus + "';";
				sScript += "  window.opener.CustomerProfile.txtCreditThredholds.value = '" + txtCreditThred.Text + "';";
				if(radPaymentCash.Checked)
				{
					sScript += "  window.opener.CustomerProfile.radioBtnCash.checked = true;";
					sScript += "  window.opener.CustomerProfile.radioBtnCredit.checked = false;";
				
				}
				else
				{
					sScript += "  window.opener.CustomerProfile.radioBtnCash.checked = false;";
					sScript += "  window.opener.CustomerProfile.radioBtnCredit.checked = true;";
				}

				sScript += " window.close();";
				sScript += "</script>";
				Response.Write(sScript);


			}
			else
			{
			
				string strMACC_limits_credit_used;

				if(chkcreditLimitUsed.Checked)
				{
					strMACC_limits_credit_used = "Y";
				}
				else
				{
					strMACC_limits_credit_used = "N";
				}

				Hashtable ht = new Hashtable(9);
				ht.Add("CustID",txtMasterAccount.Text);
				ht.Add("CreditLimit",txtCreditLimit.Text);
				ht.Add("PaymentMode","N");
				ht.Add("CreditTerm",ddlCreditTerms.SelectedValue);
				if(ddlCreditStatus.SelectedIndex!=0)
				{
					ht.Add("CreditStatus",ddlCreditStatus.SelectedValue);
				}
				else
				{
					ht.Add("CreditStatus","");
				}
				if(ddlReason.SelectedValue == "")
				{
					ht.Add("ReasonCode","");
				}
				else
				{
					ht.Add("ReasonCode",ddlReason.SelectedItem.Text);
				}
				ht.Add("ReasonDesc",txtReason.Text);
				ht.Add("CreditThreshold",txtCreditThred.Text);
				ht.Add("MACC_limits_credit_used",strMACC_limits_credit_used);
				ht.Add("isMasterAccount","Y");

				Session["MasterCreditInfo"] = ht;

		
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			

			}
		
		
		}

		private void btncancel_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void chkcreditLimitUsed_CheckedChanged(object sender, System.EventArgs e)
		{
			checkCustomerCreditusedcheck();
		}

		private void ddlReason_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			txtReason.Text = ddlReason.SelectedItem.Value;
		}

		private void radPaymentCash_CheckedChanged(object sender, System.EventArgs e)
		{

			checkcredittocash(Request.Params["FormID"]);

		
		}

		private void checkcredittocash(string formid)
		{
			
			if(radPaymentCash.Checked)
			{
				txtCreditLimit.Text = "";
				txtCreditThred.Text = "";
				txtCreditLimit.ReadOnly=true;
				txtCreditThred.ReadOnly=true;
				ddlCreditStatus.Enabled=false;
				ddlCreditTerms.Enabled = false;
				ddlReason.Enabled = true;
				ddlCreditTerms.SelectedIndex = 0;
				ddlCreditStatus.SelectedIndex = 0;
			}
			else
			{
				//ddlCreditStatus.Enabled = true;
				txtCreditLimit.ReadOnly=false;
				txtCreditThred.ReadOnly=false;
				ddlCreditStatus.Enabled=true;
				ddlCreditTerms.Enabled = true;
			}
			if(mode.Equals("A"))
				ddlReason.Enabled = false;
			else
				ddlReason.Enabled = true;

		}

		private void dgdata_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				DataRowView dv =  (DataRowView) e.Item.DataItem;

				if(dv["creditlimit"].ToString().Length <= 0)
				{
					e.Item.Cells[0].Text = "N/A";
				}
				else
				{
					e.Item.Cells[0].Text = String.Format((String)Session["m_format"],Convert.ToDouble(dv["creditlimit"]));
				}
				if(dv["creditstatus"].ToString().Length <= 0)
				{
					e.Item.Cells[3].Text = "N/A";
				}
				else
				{
					if(e.Item.Cells[3].Text == "A")
					{
						e.Item.Cells[3].Text = "AVAILABLE";
					}
					else
					{
						e.Item.Cells[3].Text = "NOT AVAILABLE";
					}
				}

				if(dv["paymentmode"].ToString().Length <= 0)
				{
					e.Item.Cells[2].Text = "";
				}
				else
				{
					if(e.Item.Cells[2].Text == "C")
					{
						e.Item.Cells[2].Text = "Cash";
					}
					else if(e.Item.Cells[2].Text == "R")
					{
						e.Item.Cells[2].Text = "Credit";
					}
					else
					{
						e.Item.Cells[2].Text = "";
					}
				}


				if(dv["creditthreshold"].ToString().Length <= 0)
				{
					e.Item.Cells[4].Text = "N/A";
				}

				if(dv["creditterms"].ToString() == "")
				{
					e.Item.Cells[1].Text = "N/A";
				}

				CheckBox ch = (CheckBox)e.Item.FindControl("chklimitcreditused");
				ch.Checked = (dv["MACC_limits_credit_used"].ToString().Length > 0 && dv["MACC_limits_credit_used"].ToString().Equals("Y"));
			}
		}

		private void radCredit_CheckedChanged(object sender, System.EventArgs e)
		{
			checkcredittocash(Request.Form["FormID"]);
		}




//		private void dgdata_SelectedIndexChanged(object sender, System.EventArgs e)
//		{
//			if(dgAssignedConsignment.EditItemIndex != -1)
//			{
//				BindConsGrid();
//				return;
//			}
//			dgAssignedConsignment.CurrentPageIndex = e.NewPageIndex;			
//			BindConsGrid();
//			DisplayCurrentPage();
//		}	
	}
}