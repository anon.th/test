using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.applicationpages;
using TIESClasses;
using TIESDAL;
using com.ties;
using com.ties.DAL;
using com.ties.classes;
using com.common.util;
using System.Text;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CreateUpdateConsignment_Customs.
	/// </summary>
	public class CreateUpdateConsignment_Customs : BasePage
	{
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnPrintConsNote;
		protected System.Web.UI.WebControls.Label lblStatus;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label lblRecipPhoneNo;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label lblRecipPostCode;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label24;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label25;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label26;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.Label Label27;
		protected System.Web.UI.WebControls.Label Label31;
		protected System.Web.UI.WebControls.Label Label28;
		protected System.Web.UI.WebControls.Label Label32;
		protected System.Web.UI.WebControls.Label Label29;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.Label Label33;
		protected System.Web.UI.WebControls.Label Label34;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.Label Label30;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.WebControls.Label lblConsNo;
		protected System.Web.UI.WebControls.Label Label35;
		protected System.Web.UI.WebControls.Label Label36;
		protected System.Web.UI.WebControls.Button btnDisplayCustDtls;
		protected System.Web.UI.WebControls.Button Button2;
		protected System.Web.UI.WebControls.ValidationSummary Validationsummary1;
	
		private string appID = null;
		private string enterpriseID = null;
		protected System.Web.UI.WebControls.TextBox txtCustID;
		protected System.Web.UI.WebControls.TextBox txtCustName;
		protected System.Web.UI.WebControls.TextBox consignment_no;
		protected System.Web.UI.WebControls.DropDownList service_code;
		protected System.Web.UI.WebControls.TextBox ref_no;
		protected System.Web.UI.WebControls.DropDownList sender_name;
		protected System.Web.UI.WebControls.TextBox sender_address1;
		protected System.Web.UI.WebControls.TextBox sender_address2;
		protected System.Web.UI.WebControls.TextBox sender_zipcode;
		protected System.Web.UI.WebControls.TextBox sender_telephone;
		protected System.Web.UI.WebControls.TextBox sender_fax;
		protected System.Web.UI.WebControls.TextBox sender_email;
		protected System.Web.UI.WebControls.TextBox sender_contact_person;
		protected System.Web.UI.WebControls.TextBox recipient_telephone;
		protected System.Web.UI.WebControls.TextBox recipient_name;
		protected System.Web.UI.WebControls.TextBox recipient_address1;
		protected System.Web.UI.WebControls.TextBox recipient_address2;
		protected System.Web.UI.WebControls.TextBox recipient_zipcode;
		protected System.Web.UI.WebControls.TextBox recipient_fax;
		protected System.Web.UI.WebControls.TextBox recipient_contact_person;
		protected com.common.util.msTextBox declare_value;
		protected com.common.util.msTextBox cod_amount;
		protected System.Web.UI.WebControls.TextBox remark;
		protected System.Web.UI.WebControls.CheckBox return_pod_slip;
		protected System.Web.UI.WebControls.CheckBox return_invoice_hc;
		protected System.Web.UI.WebControls.DataGrid PackageDetails;
		protected System.Web.UI.WebControls.Label lblDangerous;
		protected System.Web.UI.WebControls.Label lblCODSupportedbyEnterprise;
		protected System.Web.UI.WebControls.Label lblInsSupportedbyEnterprise;
		protected System.Web.UI.WebControls.Label lblHCSupportedbyEnterprise;
		protected System.Web.UI.WebControls.TextBox sender_state_name;
		protected System.Web.UI.WebControls.TextBox recipient_state_name;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.WebControls.Label lblCustIdCngMsg;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divContractService;
		protected System.Web.UI.WebControls.TextBox txtContractService;
		protected System.Web.UI.WebControls.Button btnAcceptContract;
		protected System.Web.UI.WebControls.Button btnDeclineContrac;
		protected System.Web.UI.WebControls.TextBox txtCreated_By;
		protected System.Web.UI.WebControls.TextBox txtCreated_DT;
		protected System.Web.UI.WebControls.TextBox txtUpdated_By;
		protected System.Web.UI.WebControls.TextBox txtUpdated_DT;
		protected System.Web.UI.WebControls.TextBox txtlast_status;
		protected System.Web.UI.WebControls.TextBox txtlast_status_DT;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Label lblDelOperation;
		protected System.Web.UI.WebControls.Button btnDelOperationYes;
		protected System.Web.UI.WebControls.Button btnDelOperationNo;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divDelOperation;
		protected System.Web.UI.HtmlControls.HtmlTable Table3;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Button btnDangerousYes;
		protected System.Web.UI.WebControls.Button btnDangerousNo;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divDangerous;
		protected System.Web.UI.HtmlControls.HtmlTable Table4;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGrd;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox GoodsDescription;
		protected System.Web.UI.WebControls.Button btnPrintCons;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.TextBox ShippingList_No;
		protected System.Web.UI.WebControls.Label lblGoodsDescriptionRequired;
		protected System.Web.UI.WebControls.Button btnCustomsJobEntry;
		protected System.Web.UI.WebControls.DropDownList ddlSenderName;
		private string userID = null;
		
		public int DSMode
		{
			get
			{
				if(ViewState["iDSMode"]==null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["iDSMode"];
				}
			}
			set
			{
				ViewState["iDSMode"]=value;
			}
		}

		public int DSOperation
		{
			get
			{
				if(ViewState["iDSOperation"]==null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["iDSOperation"];
				}
			}
			set
			{
				ViewState["iDSOperation"]=value;
			}
		}


		public string OldServiceCode
		{
			get
			{
				if(ViewState["OldServiceCode"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["OldServiceCode"];
				}
			}
			set
			{
				ViewState["OldServiceCode"]=value;
			}
		}

		public bool ShowDangerousGoods
		{
			get
			{
				if(ViewState["ShowDangerousGoods"] == null)
				{
					return false;
				}
				else
				{
					return (bool)ViewState["ShowDangerousGoods"];
				}
			}
			set
			{
				ViewState["ShowDangerousGoods"]=value;
			}
		}


		public bool ShowContractAccepted
		{
			get
			{
				if(ViewState["ShowContractAccepted"] == null)
				{
					return false;
				}
				else
				{
					return (bool)ViewState["ShowContractAccepted"];
				}
			}
			set
			{
				ViewState["ShowContractAccepted"]=value;
			}
		}


		public int DangerousGoods
		{
			get
			{
				if(ViewState["DangerousGoods"] == null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["DangerousGoods"];
				}
			}
			set
			{
				ViewState["DangerousGoods"]=value;
			}
		}


		public int ContractAccepted
		{
			get
			{
				if(ViewState["ContractAccepted"] == null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["ContractAccepted"];
				}
			}
			set
			{
				ViewState["ContractAccepted"]=value;
			}
		}

		public bool IsUserCUSTOMS
		{
			get
			{
				if(ViewState["IsUserCUSTOMS"] == null)
				{
					return false;
				}
				else
				{
					return (bool)ViewState["IsUserCUSTOMS"];
				}
			}
			set
			{
				ViewState["IsUserCUSTOMS"]=value;
			}
		}


		public DataTable PkgData
		{
			get
			{
				if(ViewState["PkgData"]==null)
				{
					DataTable dt = new DataTable();			
					dt.Columns.Add("No",typeof(decimal));
					dt.Columns.Add("Act Wt",typeof(decimal));
					dt.Columns.Add("Qty",typeof(int));
					dt.Columns.Add("Total Kg",typeof(decimal));
					dt.Columns.Add("Width",typeof(decimal));
					dt.Columns.Add("Breadth",typeof(decimal));
					dt.Columns.Add("Height",typeof(decimal));
					ViewState["PkgData"]=dt;
					return dt;
				}
				else
				{
					return (DataTable)ViewState["PkgData"];
				}
			}
			set
			{
				ViewState["PkgData"]=value;
			}
		}


		public CustomerAccount UserCustomerAccount
		{
			get
			{
				if(ViewState["UserCustomerAccount"]==null)
				{
					return null;
				}
				else
				{
					return (CustomerAccount)ViewState["UserCustomerAccount"];
				}
			}
			set
			{
				ViewState["UserCustomerAccount"]=value;
			}
		}

		public com.ties.classes.CreateUpdateConsignmentConfigurations EnterpriseConfig
		{
			get
			{
				if(ViewState["EnterpriseConfigurations"]==null)
				{
					ViewState["EnterpriseConfigurations"]=new com.ties.classes.CreateUpdateConsignmentConfigurations();
					return null;
				}
				return (com.ties.classes.CreateUpdateConsignmentConfigurations)ViewState["EnterpriseConfigurations"];
			}
			set
			{
				ViewState["EnterpriseConfigurations"]=value;
				if(ViewState["EnterpriseConfigurations"] != null)
				{
					lblGoodsDescriptionRequired.Visible = ((com.ties.classes.CreateUpdateConsignmentConfigurations)ViewState["EnterpriseConfigurations"]).GoodsDescriptionRequired;
				}
				else
				{
					lblGoodsDescriptionRequired.Visible =false;
				}
				
			}
		}

		public com.ties.classes.PackageDetailsLimitsConfigurations PackageDetailsLimitsConfig
		{
			get
			{
				if(ViewState["PackageDetailsLimitsConfig"]==null)
				{
					ViewState["PackageDetailsLimitsConfig"]=new com.ties.classes.PackageDetailsLimitsConfigurations();
					return null;
				}
				return (com.ties.classes.PackageDetailsLimitsConfigurations)ViewState["PackageDetailsLimitsConfig"];
			}
			set
			{
				ViewState["PackageDetailsLimitsConfig"]=value;
			}
		}


		private void Page_Load(object sender, System.EventArgs e)
		{		
			//			// Put user code to initialize the page here
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();
			
			if(this.IsPostBack==false)
			{
				IsUserCUSTOMS = SysDataMgrDAL.IsUserAssignedRoles(appID,enterpriseID,userID,"CUSTOMS");
				clearscreen();			
				BindControl();

				if(UserCustomerAccount.IsEnterpriseUser)
				{					
					btnPrintConsNote.Enabled = false;
					btnSave.Enabled = false;
					btnDelete.Enabled = false;
					btnPrintCons.Enabled = false;
				}

				if(Request.QueryString["consignment_no"] != null && Request.QueryString["consignment_no"].ToString().Trim() != "")
				{
					consignment_no.Text=Request.QueryString["consignment_no"].ToString().Trim();
					ExecQry();
				}
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnPrintConsNote.Click += new System.EventHandler(this.btnPrintConsNote_Click);
			this.btnSave.Click += new System.EventHandler(this.btnPrintShipList_Click);
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			this.btnPrintCons.Click += new System.EventHandler(this.btnPrintCons_Click);
			this.btnCustomsJobEntry.Click += new System.EventHandler(this.btnCustomsJobEntry_Click);
			this.sender_name.SelectedIndexChanged += new System.EventHandler(this.ddlSender_SelectedIndexChanged);
			this.sender_zipcode.TextChanged += new System.EventHandler(this.sender_zipcode_TextChanged);
			this.btnDisplayCustDtls.Click += new System.EventHandler(this.btnDisplayCustDtls_Click);
			this.recipient_telephone.TextChanged += new System.EventHandler(this.recipient_telephone_TextChanged);
			this.recipient_zipcode.TextChanged += new System.EventHandler(this.recipient_zipcode_TextChanged);
			this.Button2.Click += new System.EventHandler(this.Button2_Click);
			this.PackageDetails.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.PackageDetails_ItemCommand);
			this.PackageDetails.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.PackageDetails_ItemDataBound);
			this.btnAcceptContract.Click += new System.EventHandler(this.btnAcceptContract_Click);
			this.btnDeclineContrac.Click += new System.EventHandler(this.btnDeclineContrac_Click);
			this.btnDelOperationYes.Click += new System.EventHandler(this.btnDelOperationYes_Click);
			this.btnDelOperationNo.Click += new System.EventHandler(this.btnDelOperationNo_Click);
			this.btnDangerousYes.Click += new System.EventHandler(this.btnDangerousYes_Click);
			this.btnDangerousNo.Click += new System.EventHandler(this.btnDangerousNo_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void BindControl()
		{
			this.txtCustID.Text="";
			this.txtCustName.Text="";
			this.sender_name.Items.Clear();

			sender_address1.Text="";
			sender_address2.Text="";
			sender_telephone.Text="";
			sender_fax.Text="";
			sender_contact_person.Text="";
			sender_email.Text="";
			sender_zipcode.Text="";
			sender_state_name.Text="";

			sender_name.Enabled=true;
			sender_address1.Enabled=true;
			sender_address2.Enabled=true;
			sender_telephone.Enabled=true;
			sender_fax.Enabled=true;
			sender_contact_person.Enabled=true;
			sender_email.Enabled=true;
			sender_zipcode.Enabled=true;

			remark.Text = "";


			DataTable data = new DataTable();

			btnSave.Enabled = false;
			btnExecQry.Enabled = true;
			btnPrintConsNote.Enabled = true;

			UserCustomerAccount = CustomerConsignmentDAL.GetCustomerAccount(appID,enterpriseID,userID);
			this.txtCustID.Text = UserCustomerAccount.Payerid;
			this.txtCustName.Text = UserCustomerAccount.Cust_name;
			DataSet dsRef = CustomerConsignmentDAL.GetReferenceDS(appID,enterpriseID, this.userID,UserCustomerAccount.Payerid).ds;
			DataSet dsRefDefault = CustomerConsignmentDAL.GetReferenceDefaultDS(appID,enterpriseID,UserCustomerAccount.Payerid,UserCustomerAccount.DefaultSender, 0,0).ds;
	
			if(this.IsUserCUSTOMS)
			{
				txtCustID.Enabled=true;
			}

			if(dsRef != null && dsRef.Tables["ReferenceTable"] != null)
			{
				this.sender_name.DataSource=dsRef.Tables["ReferenceTable"];
				this.sender_name.DataTextField="snd_rec_name";
				this.sender_name.DataValueField="snd_rec_name";
				this.sender_name.DataBind();											
			}

			if(UserCustomerAccount.IsMasterCustomerUser==false && UserCustomerAccount.IsCustomerUser==true)
			{
				sender_name.Enabled=false;
				if(UserCustomerAccount.DefaultSender != "")
				{
					try
					{
						this.sender_name.SelectedValue=UserCustomerAccount.DefaultSender;
					}
					catch
					{

					}
				}					
			}
			DSMode = (int)ScreenMode.None;
			DSOperation = (int)Operation.None;
		}


		private void btnQry_Click(object sender, System.EventArgs e)
		{
			clearscreen();
			BindControl();
	
			if(UserCustomerAccount.IsEnterpriseUser)
			{
				btnPrintConsNote.Enabled = false;
				btnSave.Enabled = false;
				btnDelete.Enabled = false;
				btnPrintCons.Enabled = false;
			}

			// Customs module
			txtCustID.Enabled = true;
			txtCustID.ReadOnly = false;
			btnPrintConsNote.Enabled = true;
			// *************
		}


		private void btnExecQry_Click(object sender, System.EventArgs e)
		{			
			ExecQry();
			SetInitialFocus(recipient_telephone);
		}


		private void ExecQry()
		{
			DataTable dtParams = CustomerConsignmentDAL.ConsignmentParameter();
			System.Data.DataRow drNew = dtParams.NewRow();
			drNew["action"] ="0";
			drNew["enterpriseid"] =enterpriseID;
			drNew["userloggedin"] =userID;
			drNew["consignment_no"] =this.consignment_no.Text.Trim();
			dtParams.Rows.Add(drNew);
			DataSet ds = CustomerConsignmentDAL.CSS_Consignment(appID,enterpriseID,userID,dtParams);
			System.Data.DataTable dtStatus = ds.Tables[0];
			System.Data.DataTable dtSender= ds.Tables[1];
			System.Data.DataTable dtPkg= ds.Tables[2];

			if(dtStatus.Rows.Count>0)
			{
				this.EnterpriseConfig = EnterpriseConfigMgrDAL.GetCreateUpdateConsignmentConfigurations(appID,enterpriseID,UserCustomerAccount.Payerid);
				lblError.Text="";
				if(Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
				{
					this.BindControl();
					this.BindService();

					int status = CustomerConsignmentDAL.CustomerConsProcessStep(appID,enterpriseID,userID,null,this.consignment_no.Text.Trim());
					btnSave.Enabled=false;
					btnDelete.Enabled=false;
					btnPrintCons.Enabled=false;
					if(status==1)
					{
						btnSave.Enabled=true;
						btnDelete.Enabled=true;
						btnPrintCons.Enabled=true;
					}
					else if(status==2)
					{
						btnSave.Enabled=false;
						btnDelete.Enabled=true;
						btnPrintCons.Enabled=false;
					}
					else if(status==3)
					{
						btnSave.Enabled=false;
						btnDelete.Enabled=false;
						btnPrintCons.Enabled=false;
					}
					else if(status==4)
					{
						btnSave.Enabled=false;
						btnDelete.Enabled=false;
						btnPrintCons.Enabled=false;
					}					

					consignment_no.Enabled = false;
					consignment_no.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
					DSMode = (int)ScreenMode.ExecuteQuery;
					DSOperation = (int)Operation.None;

					BindSendRecipientInfo(dtSender,dtPkg);					
				}
				else
				{
					lblError.Text = dtStatus.Rows[0]["ErrorMessage"].ToString();
				}
			}		
			if(UserCustomerAccount.IsEnterpriseUser)
			{
				btnPrintConsNote.Enabled = false;
				btnSave.Enabled = false;
				btnDelete.Enabled = false;
				btnPrintCons.Enabled = false;
			}
		}


		private void BindDGText(bool isDGDeclaration)
		{
			this.lblDangerous.Visible=true;
			if (isDGDeclaration == true)
			{
				this.lblDangerous.Text="Dangerous Goods Declaration: Shipper has declared that this consignment contains dangerous goods.";
			}
			else
			{
				this.lblDangerous.Text="Dangerous Goods Declaration: Shipper has declared that this consignment does not contain dangerous goods.";
			}			
		}
		

		private void BindDGText()
		{
			if(this.ShowDangerousGoods == true && EnterpriseConfigMgrDAL.ShowLabelDangerousGoods(this.appID,this.enterpriseID))
			{
				this.lblDangerous.Text="Dangerous Goods Declaration: Shipper has declared that this consignment contains dangerous goods.";
				this.lblDangerous.Visible=true;
			}
			else
			{
				this.lblDangerous.Text="Dangerous Goods Declaration: Shipper has declared that this consignment does not contain dangerous goods.";
				this.lblDangerous.Visible=false;
			}
		}
		

		private void btnPrintConsNote_Click(object sender, System.EventArgs e)
		{
			System.Data.DataSet ds = CustomerConsignmentDAL.CSS_Consignment(appID,enterpriseID,userID,1);
			System.Data.DataTable dtStatus = ds.Tables[0];
			System.Data.DataTable dtSender= ds.Tables[1];
			System.Data.DataTable dtPkg= ds.Tables[2];
			
			if(dtStatus.Rows.Count> 0)
			{
				clearscreen();
				this.BindControl();
				this.BindService();
				BindSender();
				OldServiceCode="";
				if(Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
				{
					if(dtSender != null && dtSender.Rows.Count>0)
					{
						if(this.EnterpriseConfig.DangerousGoods)
						{
							this.lblDangerous.Text="";
							this.lblDangerous.Visible=false;

							if(dtSender.Rows.Count> 0 && (dtSender.Rows[0]["DangerousGoods"] ==DBNull.Value || dtSender.Rows[0]["DangerousGoods"].ToString() =="1"))
							{
								this.ShowDangerousGoods=true;
								this.DangerousGoods = -1;
							}
							else
							{
								this.ShowDangerousGoods=false;
							}					
						}

						if(dtSender.Rows[0]["ContractAccepted"] == null || dtSender.Rows[0]["ContractAccepted"].ToString() =="0")
						{
							ShowContractAccepted=true;
							string payerId ="";			
							if(dtSender.Rows[0]["payerid"] != null)
							{
								payerId=dtSender.Rows[0]["payerid"].ToString();
							}
						}
						try
						{
							service_code.SelectedValue=dtSender.Rows[0]["service_code"].ToString();
						}
						catch
						{

						}
					}


					btnSave.Enabled = true;
					btnExecQry.Enabled = false;
					btnPrintConsNote.Enabled = false;

					consignment_no.Enabled = false;
					consignment_no.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");

					this.PkgData=null;
					BindPkg(dtPkg);
					DSMode = (int)ScreenMode.Insert;
					DSOperation = (int)Operation.Insert;
					SetInitialFocus(recipient_telephone);
				}
				else
				{
					lblError.Text = dtStatus.Rows[0]["ErrorMessage"].ToString();
				}
			}


			// Changes for making prototypes of Customs module
			consignment_no.Enabled = true;
			consignment_no.BackColor = System.Drawing.ColorTranslator.FromHtml("white");
			txtCustID.Text = "144840";
			txtCustName.Text = "NEC PNG";
			sender_address1.Text = "Jacksons Parade";
			sender_address2.Text = "Port Moresby";
			sender_telephone.Text = "3252411";
			sender_fax.Text = "3254768";
			sender_contact_person.Text = "Customer Service";
			sender_email.Text = "customs@pngaf.com.pg";
			
			recipient_telephone.Text = "6753000300";

			// below is a prototype for Save button 
			txtCustID.Enabled = false;
			txtCustID.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
			consignment_no.Enabled = false;
			consignment_no.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
			txtlast_status_DT.Text = "23/05/2014 15:46:02";
			txtCreated_DT.Text = "23/05/2014 15:46:02";
			txtlast_status.Text = "DATA ENTERED";
			txtCreated_By.Text = "DCAMPS";
			lblError.Text = "Consignment saved";
			
			consignment_no.Text = "543232993";
			lblDangerous.Visible = true;
			lblDangerous.Text = "Dangerous Goods Declaration: Shipper has declared that this consignment does not contain dangerous goods.";
			GoodsDescription.Text = "Customs Example";

			btnQry.Enabled = true;
			btnExecQry.Enabled = false;
			btnPrintConsNote.Enabled = true;
			btnSave.Enabled = true;
			btnDelete.Enabled = true;
			btnPrintCons.Enabled = true;
			btnCustomsJobEntry.Enabled = true;
			
			//
		}


		private void Textbox7_TextChanged(object sender, System.EventArgs e)
		{

		}


		private void btnPrintShipList_Click(object sender, System.EventArgs e)
		{
			this.EnterpriseConfig = EnterpriseConfigMgrDAL.GetCreateUpdateConsignmentConfigurations(appID,enterpriseID,UserCustomerAccount.Payerid);
			
			if(ValidateData())
			{				
				if(OldServiceCode != "")
				{
					if(OldServiceCode == this.EnterpriseConfig.DangerousGoodsService)
					{
						this.ShowDangerousGoods=false;
						this.DangerousGoods = -1;

						if(service_code.SelectedValue != this.EnterpriseConfig.DangerousGoodsService)
						{
							this.ShowDangerousGoods=true;
							this.DangerousGoods = -1;
						}
					}

				}
				else if(service_code.SelectedValue == this.EnterpriseConfig.DangerousGoodsService)
				{
					this.ShowDangerousGoods=true;
					this.DangerousGoods = -1;
				}


				DataSet dsContract = CustomerConsignmentDAL.EnterpriseContract(this.appID,this.enterpriseID,this.userID,this.txtCustID.Text);
				this.txtContractService.Text="";
				if(dsContract.Tables.Count>0 && dsContract.Tables[0].Rows.Count>0 
					&& dsContract.Tables[0].Rows[0]["AcceptedCurrentContract"] != DBNull.Value
					&& dsContract.Tables[0].Rows[0]["AcceptedCurrentContract"].ToString() =="1")
				{
					SaveData();
				}
				else
				{
					this.txtContractService.Text=dsContract.Tables[0].Rows[0]["ContractText"].ToString();
					this.divMain.Visible=false;
					this.divDangerous.Visible=false;
					this.divDelOperation.Visible=false;
					this.divContractService.Visible=true;
				}
			}			
		}


		private bool ValidateData()
		{
			if(service_code.Items.Count<=0)
			{
				this.lblError.Text="Service code is required";
				SetInitialFocus(service_code);
				return false;
			}

			if(this.service_code.SelectedValue =="")
			{
				this.lblError.Text="Service code is required";
				SetInitialFocus(service_code);
				return false;
			}
			else
			{
				if(this.service_code.SelectedValue=="DG")
				{
					this.ShowDangerousGoods=true;
				}
			}

			if(sender_name.Items.Count<=0)
			{
				this.lblError.Text="Sender name is required";
				SetInitialFocus(sender_name);
				return false;
			}

			if(this.sender_name.SelectedValue =="")
			{
				this.lblError.Text="Sender name is required";
				SetInitialFocus(sender_name);
				return false;
			}

			if(sender_address1.Text.Trim() =="")
			{
				this.lblError.Text="Sender address is required";
				SetInitialFocus(sender_address1);
				return false;
			}

			if(sender_zipcode.Text.Trim()=="")
			{
				this.lblError.Text="Sender zipcode is required";
				SetInitialFocus(sender_zipcode);
				return false;
			}

			if(sender_telephone.Text.Trim()=="")
			{
				this.lblError.Text="Sender telephone is required";
				SetInitialFocus(sender_telephone);
				return false;
			}

			if(sender_contact_person.Text.Trim()=="")
			{
				this.lblError.Text="Sender contact name is required";
				SetInitialFocus(sender_contact_person);
				return false;
			}

			if(recipient_telephone.Text.Trim() =="")
			{
				this.lblError.Text="Recipient telephone is required";
				SetInitialFocus(recipient_telephone);
				return false;
			}

			if(recipient_name.Text.Trim() =="")
			{
				this.lblError.Text="Recipient name is required";
				SetInitialFocus(recipient_name);
				return false;
			}

			if(recipient_address1.Text.Trim() =="")
			{
				this.lblError.Text="Recipient address is required";
				SetInitialFocus(recipient_address1);
				return false;
			}

			if(recipient_zipcode.Text.Trim() =="")
			{
				this.lblError.Text="Recipient postal code is required";
				SetInitialFocus(recipient_zipcode);
				return false;
			}

			if(recipient_zipcode.Text.Trim() =="")
			{
				this.lblError.Text="Recipient postal code is required";
				SetInitialFocus(recipient_zipcode);
				return false;
			}

			if(cod_amount.Visible == true && cod_amount.Text.Trim() !="")
			{
				try
				{
					decimal cod = Convert.ToDecimal(cod_amount.Text.Trim());
					if(cod>EnterpriseConfig.MaxCODAmount)
					{
						this.lblError.Text="COD amount exceeds the maximum ("+EnterpriseConfig.MaxCODAmount.ToString("N2")+")";
						SetInitialFocus(cod_amount);
						return false;
					}
				}
				catch
				{

				}
			}

			if(declare_value.Visible == true && declare_value.Text.Trim() !="")
			{
				try
				{
					decimal declare = Convert.ToDecimal(declare_value.Text.Trim());
					if(declare>EnterpriseConfig.MaxDeclaredValue)
					{
						this.lblError.Text="Declared Value PGK exceeds the maximum ("+EnterpriseConfig.MaxDeclaredValue.ToString("N2")+")";
						SetInitialFocus(declare_value);
						return false;
					}
				}
				catch
				{

				}
			}

			if(this.PkgData==null || this.PkgData.Rows.Count<=0)
			{
				this.lblError.Text="Package details are required";
				return false;
			}

			if(this.PackageDetails.EditItemIndex != -1)
			{
				this.lblError.Text="Please save package details";
				return false;
			}

			return true;
		}


		private void SaveData()
		{
			if(this.ShowDangerousGoods==true && this.DangerousGoods == -1)
			{
				this.divMain.Visible=false;
				this.divContractService.Visible=false;
				this.divDangerous.Visible=true;
				return;
			}
			divDangerous.Visible=false;
			divContractService.Visible=false;

			DataTable dtParams = CustomerConsignmentDAL.ConsignmentParameter();
			System.Data.DataRow drNew = dtParams.NewRow();
			foreach(System.Data.DataColumn col in dtParams.Columns)
			{
				string colName = col.ColumnName;
				System.Web.UI.Control ctrol = this.Page.FindControl(colName);
				if(ctrol != null)
				{
					if(ctrol.GetType()==typeof(TextBox))
					{
						TextBox txt = (TextBox)ctrol;
						if(txt.Text.Trim() != "")
						{
							if(txt.ID=="sender_zipcode" || txt.ID=="recipient_zipcode")
							{
								drNew[colName]=txt.Text.ToUpper().Trim();
							}
							else
							{
								drNew[colName]=txt.Text.Trim();
							}							
						}						
					}
					else if(ctrol.GetType()==typeof(com.common.util.msTextBox))
					{
						com.common.util.msTextBox txt = (com.common.util.msTextBox)ctrol;
						drNew[colName]=txt.Text;
					}
					else if(ctrol.GetType()==typeof(DropDownList))
					{
						DropDownList ddl = (DropDownList)ctrol;
						drNew[colName]=ddl.SelectedValue;
					}
					else if(ctrol.GetType()==typeof(CheckBox))
					{
						CheckBox cb = (CheckBox)ctrol;
						string val = "N";
						if(cb.Checked==true)
						{
							val="Y";
						}
						drNew[colName]=val;
					}
					else if(ctrol.GetType()==typeof(DataGrid))
					{
						DataTable dtPkg = this.PkgData;
						string pkg="";
						foreach(System.Data.DataRow dr in dtPkg.Rows)
						{
							string line ="";
							line = "";	

							if(dr["Qty"] != DBNull.Value && dr["Qty"].ToString() != "")
							{
								line+=""+int.Parse(dr["Qty"].ToString()).ToString();	
							}
							else
							{
								line+="0";	
							}


							if(dr["Act Wt"] != DBNull.Value && dr["Act Wt"].ToString() != "")
							{
								line+=","+dr["Act Wt"].ToString();	
							}
							else
							{
								line+=",0";	
							}

							if(dr["Width"] != DBNull.Value && dr["Width"].ToString() != "")
							{
								line+=","+dr["Width"].ToString();	
							}							


							if(dr["Breadth"] != DBNull.Value && dr["Breadth"].ToString() != "")
							{
								line+=","+dr["Breadth"].ToString();	
							}							


							if(dr["Height"] != DBNull.Value && dr["Height"].ToString() != "")
							{
								line+=","+dr["Height"].ToString();	
							}							


							if(pkg.Length>0)
							{
								pkg=pkg+";";
							}
							pkg+=line;
						}	
						drNew[colName]=pkg;
					}
				}
			}
			drNew["action"] ="2";
			drNew["enterpriseid"] =enterpriseID;
			drNew["userloggedin"] =userID;
			if(this.ShowDangerousGoods)
			{
				drNew["DangerousGoods"] =DangerousGoods.ToString();
			}
			else
			{
				if(OldServiceCode==this.EnterpriseConfig.DangerousGoodsService)
				{
					drNew["DangerousGoods"] ="1";
				}
				else
				{
					drNew["DangerousGoods"] ="0";
				}				
			}

			if(this.ShowContractAccepted)
			{
				drNew["ContractAccepted"] =ContractAccepted.ToString();
			}
			else
			{
				drNew["ContractAccepted"]=DBNull.Value;
			}
			
			dtParams.Rows.Add(drNew);
			DataSet ds = CustomerConsignmentDAL.CSS_Consignment(appID,enterpriseID,userID,dtParams);
			DataTable dt = ds.Tables[0];
			if(dt != null && dt.Rows.Count>0)
			{
				lblError.Text = dt.Rows[0]["ErrorMessage"].ToString();
				if(Convert.ToInt32(dt.Rows[0]["ErrorCode"].ToString()) == 0)
				{
					// After Consignment Saved
					btnPrintConsNote.Enabled = true;
					btnSave.Enabled = true;
					btnDelete.Enabled = true;
					this.btnPrintCons.Enabled = true;

					lblError.Text = "Consignment saved";	
				
					System.Data.DataTable dtSender= ds.Tables[1];
					System.Data.DataTable dtPkg= ds.Tables[2];

					this.lblDangerous.Text="";
					this.lblDangerous.Visible=false;

					this.ShowDangerousGoods=false;
					this.DangerousGoods=-1;
								
					BindSendRecipientInfo(dtSender,dtPkg);
				}
				else
				{
					this.DangerousGoods=-1;
				}
			}										

		}


		private void BindSendRecipientInfo(System.Data.DataTable dtSender, System.Data.DataTable dtPkg)
		{			
			System.Data.DataRow dr = dtSender.Rows[0];
			consignment_no.Text=dr["consignment_no"].ToString();
			ref_no.Text=dr["ref_no"].ToString();
			remark.Text=dr["remark"].ToString();

			txtlast_status_DT.Text=(dr["last_status_DT"] != DBNull.Value ? DateTime.Parse(dr["last_status_DT"].ToString()).ToString("dd/MM/yyyy HH:mm:ss") :"");
			txtlast_status.Text=dr["last_status"].ToString();
			txtCreated_DT.Text=(dr["Created_DT"] != DBNull.Value ? DateTime.Parse(dr["Created_DT"].ToString()).ToString("dd/MM/yyyy HH:mm:ss") :""); 
			txtCreated_By.Text=dr["Created_By"].ToString();
			txtUpdated_DT.Text=(dr["Updated_DT"] != DBNull.Value ? DateTime.Parse(dr["Updated_DT"].ToString()).ToString("dd/MM/yyyy HH:mm:ss") :""); 
			txtUpdated_By.Text=dr["Updated_By"].ToString();

			try
			{
				if(sender_name.Items.FindByValue(dr["sender_name"].ToString())!=null)
				{
					sender_name.SelectedValue=dr["sender_name"].ToString();
				}
				else
				{
					sender_name.Items.Insert(0,new ListItem(dr["sender_name"].ToString(),dr["sender_name"].ToString()));
				}				
			}
			catch
			{

			}
			sender_address1.Text=dr["sender_address1"].ToString();
			sender_address2.Text=dr["sender_address2"].ToString();
			sender_telephone.Text=dr["sender_telephone"].ToString();
			sender_fax.Text=dr["sender_fax"].ToString();
			sender_contact_person.Text=dr["sender_contact_person"].ToString();
			sender_email.Text=dr["sender_email"].ToString();
			sender_zipcode.Text=dr["sender_zipcode"].ToString();
			sender_state_name.Text=dr["sender_state_name"].ToString();	
					
			recipient_telephone.Text=(dr["recipient_telephone"] ==null ? "" : dr["recipient_telephone"].ToString());
			recipient_name.Text=(dr["recipient_name"] ==null ? "" : dr["recipient_name"].ToString());
			recipient_address1.Text=(dr["recipient_address1"] ==null ? "" : dr["recipient_address1"].ToString());
			recipient_address2.Text=(dr["recipient_address2"] ==null ? "" : dr["recipient_address2"].ToString());
			recipient_zipcode.Text=(dr["recipient_zipcode"] ==null ? "" : dr["recipient_zipcode"].ToString());
			recipient_fax.Text=(dr["recipient_fax"] ==null ? "" : dr["recipient_fax"].ToString());
			recipient_state_name.Text=(dr["recipient_state_name"] ==null? "" : dr["recipient_state_name"].ToString());
			recipient_contact_person.Text=(dr["recipient_contact_person"] ==null ? "" : dr["recipient_contact_person"].ToString());
			declare_value.Text=(dr["declare_value"] ==DBNull.Value  ? "" : Convert.ToDecimal(dr["declare_value"].ToString()).ToString("N2"));
			cod_amount.Text=(dr["cod_amount"] ==DBNull.Value  ? "" : Convert.ToDecimal(dr["cod_amount"].ToString()).ToString("N2"));
			remark.Text=(dr["remark"] ==null ? "" : dr["remark"].ToString());
			GoodsDescription.Text=(dr["GoodsDescription"] ==null ? "" : dr["GoodsDescription"].ToString());
			ShippingList_No.Text=(dr["ShippingList_No"] == DBNull.Value ? "" : dr["ShippingList_No"].ToString());

			return_pod_slip.Checked=(dr["return_pod_slip"] ==null ? false : (dr["return_pod_slip"].ToString() =="N" ?false:true));
			return_invoice_hc.Checked=(dr["return_invoice_hc"] ==null ? false : (dr["return_invoice_hc"].ToString() =="N" ?false:true));

			OldServiceCode="";
			this.ShowDangerousGoods=false;
			try
			{
				service_code.SelectedValue=dr["service_code"].ToString().Trim();
				if(service_code.SelectedValue==this.EnterpriseConfig.DangerousGoodsService)
				{
					OldServiceCode=service_code.SelectedValue;
				}
			}
			catch
			{
				OldServiceCode="";
			}

			this.lblDangerous.Text="";
			this.lblDangerous.Visible=false;
			if(this.EnterpriseConfig.DangerousGoods)
			{
				if(dtSender.Rows.Count> 0 && dtSender.Rows[0]["DangerousGoods"].ToString() =="1")
				{
					BindDGText(true);
				}
				else
				{
					BindDGText(false);
				}
				
	
			}

			if(dtSender.Rows[0]["ContractAccepted"] == null || dtSender.Rows[0]["ContractAccepted"].ToString() =="0")
			{
				ShowContractAccepted=true;
				string payerId ="";			
				if(dtSender.Rows[0]["payerid"] != null)
				{
					payerId=dtSender.Rows[0]["payerid"].ToString();
				}
			}
			else
			{
				ShowContractAccepted=false;
			}

			//BindDGText();
			
			this.PkgData=null;
			BindPkg(dtPkg);
		}


		private void ddlSender_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			BindSender();
			SetInitialFocus(recipient_telephone);
		}


		private void BindSender()
		{
			DataSet dsRefDefault = CustomerConsignmentDAL.GetReferenceDefaultDS(appID,enterpriseID,UserCustomerAccount.Payerid,sender_name.SelectedValue, 0,0).ds;
			if(dsRefDefault !=null  && dsRefDefault.Tables["ReferenceTable"] != null && dsRefDefault.Tables["ReferenceTable"].Rows.Count>0)
			{
				System.Data.DataRow dr = dsRefDefault.Tables["ReferenceTable"].Rows[0];

				sender_address1.Text=dr["address1"].ToString();
				sender_address2.Text=dr["address2"].ToString();
				sender_telephone.Text=dr["telephone"].ToString();
				sender_fax.Text=dr["fax"].ToString();
				sender_contact_person.Text=dr["contact_person"].ToString();
				sender_email.Text=dr["email"].ToString();
				sender_zipcode.Text=dr["zipcode"].ToString();
				sender_state_name.Text=GetStateName(sender_zipcode.Text);			
			}
			else
			{
				sender_address1.Text="";
				sender_address2.Text="";
				sender_telephone.Text="";
				sender_fax.Text="";
				sender_contact_person.Text="";
				sender_email.Text="";
				sender_zipcode.Text="";
				sender_state_name.Text="";
			}
		}


		private void BindPkg(DataTable dtPkg)
		{
			DataTable dt = this.PkgData;

			if(dtPkg != null && dtPkg.Rows.Count>0)
			{					
				for(int i=0;i<dtPkg.Rows.Count;i++)
				{
					DataRow dr = dt.NewRow();
					if(dtPkg.Rows[i]["mps_code"] != null  && dtPkg.Rows[i]["mps_code"] != DBNull.Value)
					{
						dr["No"]=decimal.Parse(dtPkg.Rows[i]["mps_code"].ToString());
					}

					if(dtPkg.Rows[i]["pkg_wt"] != null && dtPkg.Rows[i]["pkg_wt"] != DBNull.Value)
					{
						dr["Act Wt"]=decimal.Parse(dtPkg.Rows[i]["pkg_wt"].ToString());
					}
				
					if(dtPkg.Rows[i]["pkg_qty"] != null && dtPkg.Rows[i]["pkg_qty"] != DBNull.Value)
					{
						dr["Qty"]=int.Parse(dtPkg.Rows[i]["pkg_qty"].ToString());
					}

					if(dtPkg.Rows[i]["tot_wt"] != null  && dtPkg.Rows[i]["tot_wt"] != DBNull.Value)
					{
						dr["Total Kg"]=decimal.Parse(dtPkg.Rows[i]["tot_wt"].ToString());
					}

					if(dtPkg.Rows[i]["pkg_length"] != null  && dtPkg.Rows[i]["pkg_length"] != DBNull.Value)
					{
						dr["Width"]=decimal.Parse(dtPkg.Rows[i]["pkg_length"].ToString());
					}

					if(dtPkg.Rows[i]["pkg_breadth"] != null && dtPkg.Rows[i]["pkg_breadth"] != DBNull.Value)
					{
						dr["Breadth"]=decimal.Parse(dtPkg.Rows[i]["pkg_breadth"].ToString());
					}

					if(dtPkg.Rows[i]["pkg_height"] != null && dtPkg.Rows[i]["pkg_height"] != DBNull.Value)
					{
						dr["Height"]=decimal.Parse(dtPkg.Rows[i]["pkg_height"].ToString());
					}

					dt.Rows.Add(dr);
				}

			}
			PackageDetails.DataSource = dt;							
			PackageDetails.DataBind();
		}


		private void clearscreen()
		{
			lblError.Text="";
			divMain.Visible=true;
			divContractService.Visible=false;
			divDelOperation.Visible=false;
			divDangerous.Visible=false;

			this.txtCustID.Text="";
			this.txtCustName.Text="";
			this.sender_name.Items.Clear();
			this.service_code.Items.Clear();
			
			txtlast_status_DT.Text="";
			txtCreated_DT.Text="";
			txtUpdated_DT.Text="";
			txtlast_status.Text="";
			txtCreated_By.Text="";
			txtUpdated_By.Text="";
			consignment_no.Text="";
			consignment_no.BackColor=System.Drawing.Color.White;
			consignment_no.Enabled=true;

			UserCustomerAccount = CustomerConsignmentDAL.GetCustomerAccount(appID,enterpriseID,userID);
			this.txtCustID.Text = UserCustomerAccount.Payerid;
			this.txtCustName.Text = UserCustomerAccount.Cust_name;
			
			this.EnterpriseConfig = EnterpriseConfigMgrDAL.GetCreateUpdateConsignmentConfigurations(appID,enterpriseID,UserCustomerAccount.Payerid);
			this.PackageDetailsLimitsConfig = EnterpriseConfigMgrDAL.GetPackageDetailsLimitsConfigurations(appID,enterpriseID,UserCustomerAccount.Payerid);	

			lblCODSupportedbyEnterprise.Visible=this.EnterpriseConfig.CODSupportedbyEnterprise;
			cod_amount.Visible = this.EnterpriseConfig.CODSupportedbyEnterprise;

			lblInsSupportedbyEnterprise.Visible=this.EnterpriseConfig.InsSupportedbyEnterprise;
			declare_value.Visible=this.EnterpriseConfig.InsSupportedbyEnterprise;

			lblHCSupportedbyEnterprise.Visible=this.EnterpriseConfig.HCSupportedbyEnterprise;
			return_pod_slip.Visible = this.EnterpriseConfig.HCSupportedbyEnterprise;
			return_invoice_hc.Visible=this.EnterpriseConfig.HCSupportedbyEnterprise;

			sender_address1.Text="";
			sender_address2.Text="";
			sender_telephone.Text="";
			sender_fax.Text="";
			sender_contact_person.Text="";
			sender_email.Text="";
			sender_zipcode.Text="";
			sender_state_name.Text="";
			remark.Text="";

			sender_name.Enabled=true;
			sender_address1.Enabled=true;
			sender_address2.Enabled=true;
			sender_telephone.Enabled=true;
			sender_fax.Enabled=true;
			sender_contact_person.Enabled=true;
			sender_email.Enabled=true;
			sender_zipcode.Enabled=true;

			ref_no.Text="";
			recipient_telephone.Text="";
			recipient_name.Text="";
			recipient_address1.Text="";
			recipient_address2.Text="";
			recipient_zipcode.Text="";
			recipient_state_name.Text="";
			recipient_fax.Text="";
			recipient_contact_person.Text="";
			declare_value.Text="";
			cod_amount.Text="";
			recipient_telephone.Text="";
			recipient_telephone.Text="";
			GoodsDescription.Text="";
			ShippingList_No.Text="";

			return_pod_slip.Checked=false;
			return_invoice_hc.Checked=false;
			OldServiceCode="";
			this.lblDangerous.Text="";
			lblDangerous.Visible=false;

			ShowDangerousGoods=false;
			ShowContractAccepted=false;

			
			DangerousGoods=-1;

			btnExecQry.Enabled=true;
			btnPrintConsNote.Enabled = false;
			btnSave.Enabled = false;
			btnDelete.Enabled = false;
			btnPrintCons.Enabled = false;
			
			this.PkgData=null;
			BindPkg(null);

			DSMode = (int)ScreenMode.None;
			DSOperation = (int)Operation.None;
		}


		private void recipient_telephone_TextChanged(object sender, System.EventArgs e)
		{
			if(recipient_telephone.Text.Trim() != "")
			{
				recipient_name.Text= "";
				recipient_address1.Text= "";
				recipient_address2.Text= "";
				recipient_zipcode.Text= "";
				recipient_fax.Text= "";
				recipient_contact_person.Text= "";
				recipient_state_name.Text="";				
				DataSet dsReferences = DomesticShipmentMgrDAL.getReferencesByTelephoneNumber(appID,enterpriseID,
					0, 0, recipient_telephone.Text.TrimStart().TrimEnd()).ds;
				if (dsReferences.Tables[0].Rows.Count > 0)
				{
					DataRow dr = dsReferences.Tables[0].Rows[0];
					recipient_name.Text= dr["reference_name"].ToString();
					recipient_address1.Text= dr["address1"].ToString();
					recipient_address2.Text= dr["address2"].ToString();
					recipient_zipcode.Text= dr["zipcode"].ToString();
					recipient_fax.Text= dr["fax"].ToString();
					recipient_contact_person.Text= dr["contactperson"].ToString();
					recipient_state_name.Text=GetStateName(recipient_zipcode.Text);

					SetInitialFocus(service_code);
				}
				else
				{
					SetInitialFocus(recipient_name);
				}
			}
		}


		private string GetStateName(string zipcode)
		{
			string state_name="";
			System.Data.DataSet ds = CustomerConsignmentDAL.GetZipcode(this.appID,this.enterpriseID,zipcode);
			if(ds.Tables["Zipcode"].Rows.Count>0 && ds.Tables["Zipcode"].Rows[0]["state_name"] != DBNull.Value)
			{
				state_name = ds.Tables["Zipcode"].Rows[0]["state_name"].ToString();
			}
			
			return state_name;
		}


		private void btnDisplayCustDtls_Click(object sender, System.EventArgs e)
		{
			string strZipcodeClientID=null;
			string strZipcode=null;
			string strStateClientID=null;
			string strCustId=null;
			
			strCustId = this.userID;			
							
			strZipcodeClientID = sender_zipcode.ClientID;
			if (sender_zipcode.Text != null)
			{

				strZipcode = sender_zipcode.Text;
			}
			
			strStateClientID = sender_state_name.ClientID;				
						
			string sUrl = "ZipcodePopup.aspx?FORMID=CreateUpdateConsignment&ZIPCODE="+strZipcode+"&ZIPCODE_CID=";
			sUrl += strZipcodeClientID+"&STATE_CID="+strStateClientID;
			sUrl += "&SENDZIPCODE="+strZipcode+"&CUSTID="+strCustId;
			sUrl += "&CUSTTYPE="+sender_telephone.ClientID;
			
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		private void sender_zipcode_TextChanged(object sender, System.EventArgs e)
		{
			if(sender_zipcode.Text.Trim() != "")
			{
				sender_state_name.Text=GetStateName(sender_zipcode.Text);
				SetInitialFocus(sender_telephone);
			}
			else
			{
				sender_state_name.Text="";
				SetInitialFocus(sender_telephone);
				
			}
			
		}


		private void recipient_zipcode_TextChanged(object sender, System.EventArgs e)
		{
			if( recipient_zipcode.Text.Trim() != "")
			{
				recipient_state_name.Text=GetStateName(recipient_zipcode.Text);
				SetInitialFocus(recipient_fax);
			}
			else
			{
				recipient_state_name.Text="";
				SetInitialFocus(recipient_fax);
			}
		}


		private void BindService()
		{
			DataSet ds = SysDataMgrDAL.GetServiceCodeDataSet(this.appID,this.enterpriseID);
			service_code.DataSource=ds;
			service_code.DataTextField="service_code";
			service_code.DataValueField="service_code";
			service_code.DataBind();
		}


		private void btnAcceptContract_Click(object sender, System.EventArgs e)
		{
			ContractAccepted=1;
			this.divMain.Visible=true;
			this.divContractService.Visible=false;
			SaveData();
		}


		private void btnDeclineContrac_Click(object sender, System.EventArgs e)
		{
			ContractAccepted=0;
			this.divMain.Visible=true;
			this.divContractService.Visible=false;
			SaveData();
		}


		private void PackageDetails_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			string err_pkg="";
			this.lblError.Text="";
			string cmd = e.CommandName;
			if(cmd=="ADD_ITEM")
			{
				DataTable dtPkg = this.PkgData;
				System.Data.DataRow drNew = dtPkg.NewRow();
				com.common.util.msTextBox txtFooterWeight =  (com.common.util.msTextBox)e.Item.FindControl("txtFooterWeight");
				com.common.util.msTextBox txtFooterQty =  (com.common.util.msTextBox)e.Item.FindControl("txtFooterQty");
				com.common.util.msTextBox txtFooterLength =  (com.common.util.msTextBox)e.Item.FindControl("txtFooterLength");
				com.common.util.msTextBox txtFooterBreadth =  (com.common.util.msTextBox)e.Item.FindControl("txtFooterBreadth");
				com.common.util.msTextBox txtFooterHeight =  (com.common.util.msTextBox)e.Item.FindControl("txtFooterHeight");

				drNew["No"]= dtPkg.Rows.Count+1;
				decimal wt=1;
				decimal qty=1;
				decimal length=1;
				decimal breadth=1;
				decimal height=1;
				
				if(txtFooterWeight !=null && txtFooterWeight.Text.Trim() != "" && decimal.Parse(txtFooterWeight.Text) > 0)
				{					
					wt=decimal.Parse(txtFooterWeight.Text);
					if(wt>this.PackageDetailsLimitsConfig.PkgWtLimit)
					{
						err_pkg="Individual Package Weight exceeds the Enterprise limit.";
					}
					else
					{
						drNew["Act Wt"] =decimal.Parse(txtFooterWeight.Text);
					}
				}
				else
				{
					lblError.Text="Act Wt is required";
					SetInitialFocus(txtFooterWeight);
					return;
					//drNew["Act Wt"] =DBNull.Value;
				}

				if(txtFooterQty !=null && txtFooterQty.Text.Trim() != "" && decimal.Parse(txtFooterQty.Text)>0)
				{					
					qty = decimal.Parse(txtFooterQty.Text);
					if(qty>this.PackageDetailsLimitsConfig.PkgQtyLimit)
					{
						if(err_pkg.Length>0)
						{
							err_pkg+="<br/>";
						}
						err_pkg +="Total number of packages exceeds the Enterprise Limit.";
					}
					else
					{
						drNew["Qty"] =int.Parse(txtFooterQty.Text);
					}
				}
				else
				{
					lblError.Text="Qty is required";
					SetInitialFocus(txtFooterQty);
					return;
				}
				
				if((wt*qty)>this.PackageDetailsLimitsConfig.PkgRowWtLimit)
				{
					if(err_pkg.Length>0)
					{
						err_pkg+="<br/>";
					}
					err_pkg +="Package Weight x Quantity exceeds the limit that can be saved.";
				}
				else
				{
					drNew["Total Kg"]=wt*qty;
				}

				if(txtFooterLength !=null && txtFooterLength.Text.Trim() != "" && decimal.Parse(txtFooterLength.Text) >0)
				{
					drNew["Width"] =decimal.Parse(txtFooterLength.Text);	
					length=decimal.Parse(txtFooterLength.Text);
				}				
				else
				{
					drNew["Width"]  =DBNull.Value;
				}

				if(txtFooterBreadth !=null && txtFooterBreadth.Text.Trim() != "" && decimal.Parse(txtFooterBreadth.Text)>0)
				{
					drNew["Breadth"] =decimal.Parse(txtFooterBreadth.Text);
					breadth=decimal.Parse(txtFooterBreadth.Text);
				}				
				else
				{
					drNew["Breadth"] =DBNull.Value;
				}

				if(txtFooterHeight !=null && txtFooterHeight.Text.Trim() != "" && decimal.Parse(txtFooterHeight.Text)>0)
				{
					drNew["Height"] =decimal.Parse(txtFooterHeight.Text);
					height=decimal.Parse(txtFooterHeight.Text);
				}				
				else
				{
					drNew["Height"]  =DBNull.Value;
				}

				if(((length * breadth * height) * qty)>this.PackageDetailsLimitsConfig.VolumeLimit)
				{
					if(err_pkg.Length>0)
					{
						err_pkg+="<br/>";
					}
					err_pkg +="Volume of package exceeds the Enterprise Limit.";
				}

				if((((length * breadth * height) * qty) / this.PackageDetailsLimitsConfig.DensityFactor)>this.PackageDetailsLimitsConfig.PkgRowWtLimit)
				{
					if(err_pkg.Length>0)
					{
						err_pkg+="<br/>";
					}
					err_pkg +="Package Weight x Quantity exceeds the limit that can be saved.";
				}

				if(err_pkg.Trim() =="")
				{
					dtPkg.Rows.Add(drNew);
					this.PkgData=dtPkg;
					BindPkg(null);
				}
				else
				{
					lblError.Text=err_pkg;
				}

			}
			else if(cmd=="EDIT_ITEM")
			{
				PackageDetails.EditItemIndex=e.Item.ItemIndex;
				BindPkg(null);
			}
			else if(cmd=="DELETE_ITEM")
			{
				DataTable dtPkg = this.PkgData;
				dtPkg.Rows.RemoveAt(e.Item.ItemIndex);
				this.PkgData=dtPkg;
				PackageDetails.EditItemIndex = -1;
				BindPkg(null);
			}
			else if(cmd=="SAVE_ITEM")
			{
				DataTable dtPkg = this.PkgData;
				int k = e.Item.ItemIndex;
				decimal wt=decimal.Parse(dtPkg.Rows[k]["Act Wt"].ToString());
				decimal qty=decimal.Parse(dtPkg.Rows[k]["Qty"].ToString());
				decimal length=1;
				decimal breadth=1;
				decimal height=1;

				com.common.util.msTextBox txtWeight =  (com.common.util.msTextBox)e.Item.FindControl("txtWeight");
				com.common.util.msTextBox txtQty =  (com.common.util.msTextBox)e.Item.FindControl("txtQty");
				com.common.util.msTextBox txtLength =  (com.common.util.msTextBox)e.Item.FindControl("txtLength");
				com.common.util.msTextBox txtBreadth =  (com.common.util.msTextBox)e.Item.FindControl("txtBreadth");
				com.common.util.msTextBox txtHeight =  (com.common.util.msTextBox)e.Item.FindControl("txtHeight");

				if(txtWeight !=null && txtWeight.Text.Trim() != "" && decimal.Parse(txtWeight.Text)>0)
				{					
					wt=decimal.Parse(txtWeight.Text);
					if(wt>this.PackageDetailsLimitsConfig.PkgWtLimit)
					{
						err_pkg="Individual Package Weight exceeds the Enterprise limit.";
					}
				}				
				else
				{
					lblError.Text="Act Wt is required";
					SetInitialFocus(txtWeight);
					return;
				}
				if(txtQty !=null && txtQty.Text.Trim() != "" && decimal.Parse(txtQty.Text)>0)
				{					
					qty=decimal.Parse(txtQty.Text);
					if(qty>this.PackageDetailsLimitsConfig.PkgQtyLimit)
					{
						if(err_pkg.Length>0)
						{
							err_pkg+="<br/>";
						}
						err_pkg +="Total number of packages exceeds the Enterprise Limit.";
					}
				}				
				else
				{
					lblError.Text="Qty is required";
					SetInitialFocus(txtQty);
					return;
				}
												
				if((wt*qty)>this.PackageDetailsLimitsConfig.PkgRowWtLimit)
				{
					if(err_pkg.Length>0)
					{
						err_pkg+="<br/>";
					}
					err_pkg +="Package Weight x Quantity exceeds the limit that can be saved.";
				}

				if(txtLength !=null && txtLength.Text.Trim() != "" && decimal.Parse(txtLength.Text)>0)
				{					
					length=decimal.Parse(txtLength.Text);
				}

				if(txtBreadth !=null && txtBreadth.Text.Trim() != "" && decimal.Parse(txtBreadth.Text)>0)
				{					
					breadth=decimal.Parse(txtBreadth.Text);	
				}

				if(txtHeight !=null && txtHeight.Text.Trim() != "" && decimal.Parse(txtHeight.Text)>0)
				{					
					height=decimal.Parse(txtHeight.Text);
				}				

				if(((length * breadth * height) * qty)>this.PackageDetailsLimitsConfig.VolumeLimit)
				{
					if(err_pkg.Length>0)
					{
						err_pkg+="<br/>";
					}
					err_pkg +="Volume of package exceeds the Enterprise Limit.";
				}

				if((((length * breadth * height) * qty) / this.PackageDetailsLimitsConfig.DensityFactor)>this.PackageDetailsLimitsConfig.PkgRowWtLimit)
				{
					if(err_pkg.Length>0)
					{
						err_pkg+="<br/>";
					}
					err_pkg +="Package Weight x Quantity exceeds the limit that can be saved.";
				}

				if(err_pkg.Trim() =="")
				{
					dtPkg.Rows[k]["Act Wt"] =decimal.Parse(txtWeight.Text);
					dtPkg.Rows[k]["Qty"] =int.Parse(txtQty.Text);
					dtPkg.Rows[k]["Total Kg"]=wt*qty;
					if(txtLength !=null && txtLength.Text.Trim() != "" && decimal.Parse(txtLength.Text)>0)
					{
						dtPkg.Rows[k]["Width"] =decimal.Parse(txtLength.Text);
					}
					else
					{
						dtPkg.Rows[k]["Width"] =DBNull.Value;
					}
					
					if(txtBreadth !=null && txtBreadth.Text.Trim() != "" && decimal.Parse(txtBreadth.Text)>0)
					{
						dtPkg.Rows[k]["Breadth"] =decimal.Parse(txtBreadth.Text);
					}
					else
					{
						dtPkg.Rows[k]["Breadth"] =DBNull.Value;
					}
					
					if(txtHeight !=null && txtHeight.Text.Trim() != "" && decimal.Parse(txtHeight.Text)>0)
					{
						dtPkg.Rows[k]["Height"] =decimal.Parse(txtHeight.Text);
					}
					else
					{
						dtPkg.Rows[k]["Height"] =DBNull.Value;
					}
										
					this.PkgData=dtPkg;
					PackageDetails.EditItemIndex=-1;
					BindPkg(null);
				}
				else
				{
					lblError.Text=err_pkg;
				}

			}
			else if(cmd=="CANCEL_ITEM")
			{
				PackageDetails.EditItemIndex = -1;
				BindPkg(null);
			}


		}

		
		private void SetGridFocus()
		{		
			//			Control p = btnReprintConsNote.Parent; 
			//			while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
			//				p = p.Parent; 
			//
			//
			//			StringBuilder s = new StringBuilder(); 
			//			s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
			//			s.Append("<!--\n"); 
			//			s.Append("function SetInitialFocus()\n"); 
			//			s.Append("{\n"); 
			//			s.Append("   document."); 
			//
			//			s.Append(p.ClientID); 
			//
			//			s.Append("['"); 
			//			s.Append(btnReprintConsNote.ClientID); 
			//			s.Append("'].focus();\n"); 
			//			s.Append("}\n"); 
			//			s.Append("// -->\n"); 
			//			s.Append("window.onload = SetInitialFocus;\n"); 
			//			s.Append("</SCRIPT>"); 
			//
			//			string script = s.ToString();
			//			// Register Client Script 
			//			btnReprintConsNote.Page.RegisterClientScriptBlock("InitialFocus"+DateTime.Now.ToString("ddMMyyyyhhmmss"), script); 
		}


		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			divMain.Visible=false;			
			divContractService.Visible=false;
			divDelOperation.Visible=true;
		}


		private void btnPrintCons_Click(object sender, System.EventArgs e)
		{
			// Data
			String strConsignmentNo = consignment_no.Text;
			DataSet dsConsignments = com.ties.DAL.ConsignmentNoteDAL.PrePrintConsNote(appID,enterpriseID,userID,strConsignmentNo);
			if(dsConsignments !=null && dsConsignments.Tables[0].Rows.Count>0)
			{
				if(Convert.ToInt32( dsConsignments.Tables[0].Rows[0]["ErrorCode"]) > 0)
				{
					lblError.Text = dsConsignments.Tables[0].Rows[0]["ErrorMessage"].ToString();
				}		
				else if(dsConsignments.Tables[0].Rows[0]["ReportTemplate"].ToString() =="")
				{
					lblError.Text =Utility.GetLanguageText(ResourceType.UserMessage,"TEMPLATE_NOT_FOUND",utility.GetUserCulture());;
				}
				else
				{
					lblError.Text ="";
				}
			}
			if(lblError.Text =="")
			{


				String strUrl = null;
				strUrl = "ReportViewer1.aspx";
				String reportTemplate = dsConsignments.Tables[0].Rows[0]["ReportTemplate"].ToString() ;
				Session["REPORT_TEMPLATE"] = reportTemplate;
				Session["FORMID"] = "PrintConsNotes";
				Session["SESSION_DS_PRINTCONSNOTES"] = dsConsignments;
				Session["FormatType"] = ".pdf";
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindowReport.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);		
		
				ExecQry();
			}
		}


		private void btnDelOperationYes_Click(object sender, System.EventArgs e)
		{
			DataTable dtParams = CustomerConsignmentDAL.ConsignmentParameter();
			System.Data.DataRow drNew = dtParams.NewRow();
			drNew["action"] ="3";
			drNew["enterpriseid"] =enterpriseID;
			drNew["userloggedin"] =userID;
			drNew["consignment_no"] =this.consignment_no.Text.Trim();
			dtParams.Rows.Add(drNew);
			DataSet ds = CustomerConsignmentDAL.CSS_Consignment(appID,enterpriseID,userID,dtParams);
			System.Data.DataTable dtStatus = ds.Tables[0];
			System.Data.DataTable dtSender= ds.Tables[1];
			System.Data.DataTable dtPkg= ds.Tables[2];
			
			if(Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
			{
				clearscreen();
				lblError.Text = "Consignment deleted";	
			}
			else
			{
				lblError.Text = dtStatus.Rows[0]["ErrorMessage"].ToString();
			}
			
			divMain.Visible=true;			
			divDelOperation.Visible=false;
		}


		private void btnDelOperationNo_Click(object sender, System.EventArgs e)
		{
			divMain.Visible=true;			
			divDelOperation.Visible=false;
		}


		private void btnDangerousYes_Click(object sender, System.EventArgs e)
		{
			divMain.Visible=true;			
			divDangerous.Visible=false;
			DangerousGoods=1;
			SaveData();
		}


		private void btnDangerousNo_Click(object sender, System.EventArgs e)
		{
			divMain.Visible=true;			
			divDangerous.Visible=false;
			DangerousGoods=0;
			SaveData();
		}


		private void Button2_Click(object sender, System.EventArgs e)
		{
			string strZipcodeClientID=null;
			string strZipcode=null;
			string strStateClientID=null;
			string strCustId=null;
			
			strCustId = this.userID;						
			strZipcodeClientID = recipient_zipcode.ClientID;
			if (recipient_zipcode.Text != null)
			{
				strZipcode = recipient_zipcode.Text;
			}
				
			strStateClientID = recipient_state_name.ClientID;		
						
			string sUrl = "ZipcodePopup.aspx?FORMID=CreateUpdateConsignment&ZIPCODE="+strZipcode+"&ZIPCODE_CID=";
			sUrl += strZipcodeClientID+"&STATE_CID="+strStateClientID;
			sUrl += "&SENDZIPCODE="+strZipcode+"&CUSTID="+strCustId;
			sUrl += "&CUSTTYPE="+recipient_fax.ClientID;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}


		private void PackageDetails_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if((e.Item.ItemType == ListItemType.Item) || 
				(e.Item.ItemType == ListItemType.AlternatingItem))
			{
				LinkButton btnEditItem = (LinkButton)e.Item.FindControl("btnEditItem");
				LinkButton btnDeleteItem = (LinkButton)e.Item.FindControl("btnDeleteItem");
				if(btnEditItem != null)
				{
					btnEditItem.Enabled= btnSave.Enabled;
				}
				if(btnDeleteItem != null)
				{
					btnDeleteItem.Enabled= btnSave.Enabled;
				}
			}

			if((e.Item.ItemType == ListItemType.Footer))
			{
				LinkButton btnAddItem = (LinkButton)e.Item.FindControl("btnAddItem");
				if(btnAddItem != null)
				{
					btnAddItem.Enabled= btnSave.Enabled;
				}
			}
		}

		private void btnCustomsJobEntry_Click(object sender, System.EventArgs e)
		{
			String sUrl = "CreateCusotmersJobEntry.aspx";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScriptPopupSetSize("openLargeWindowParam.js",paramList,550,820);
			Utility.RegisterScriptString(sScript,"ShowPopupScript"+DateTime.Now.ToString("ddMMyyyyHHmmss"),this.Page);	
		}
	}
}
