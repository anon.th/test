using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.ties.classes;
using com.common.applicationpages;


namespace com.ties
{
	/// <summary>
	/// Summary description for AgentStatusCost.
	/// </summary>
	public class AgentStatusCost : BasePage
	{
		SessionDS m_sdsStatusCode = null;
		SessionDS m_sdsAgent = null;
		SessionDS m_sdsExceptionCode = null;
		DataView m_dvMBGOptions = null;		
		private static int m_iSetSize=4;
		String appID = null;
		String enterpriseID = null;
		

		protected System.Web.UI.WebControls.Label lblEXMessage;
		protected System.Web.UI.WebControls.Button btnInsertException;
		protected System.Web.UI.WebControls.Button btnInsertStatus;
		protected System.Web.UI.WebControls.DataGrid dgStatus;
		protected System.Web.UI.WebControls.DataGrid dgException;
		protected System.Web.UI.WebControls.Label lblSCMessage;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblAgentCode;
		protected System.Web.UI.WebControls.Label lblAgentName;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.TextBox txtCountRec;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentId;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentName;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqAgentId;
		String userID = null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			
			dbCmbAgentId.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbAgentName.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			ShowButtonColumns(true); //????????????
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();
			if(!Page.IsPostBack)
			{
				Session["SESSION_DS2"] = m_sdsExceptionCode;
				ViewState["SCOperation"] = Operation.None;
				ViewState["EXMode"] = ScreenMode.None;
				ViewState["EXOperation"] = Operation.None;
				ResetScreenForQuery();
			}
			else
			{
				if (Session["SESSION_DS1"]!= null)
					m_sdsStatusCode = (SessionDS)Session["SESSION_DS1"];
				if (Session["SESSION_DS2"] != null)
					m_sdsExceptionCode = (SessionDS)Session["SESSION_DS2"];
				if (Session["SESSION_DS3"] != null)
					m_sdsAgent = (SessionDS)Session["SESSION_DS3"];

				int iMode = (int) ViewState["SCMode"];
				switch(iMode)
				{
					case (int) ScreenMode.Query:
						LoadComboLists(true);
						break;
					default:
						LoadComboLists(false);
						break;

				}
				lblSCMessage.Text = "";
			}
			SetAgentIDServerStates();
			SetAgentNameServerStates();
			PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnInsertStatus.Click += new System.EventHandler(this.btnInsertStatus_Click);
			this.btnInsertException.Click += new System.EventHandler(this.btnInsertException_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			ResetScreenForQuery();
		}

		private void ResetScreenForQuery()
		{			
			LoadComboLists(true);
			m_sdsStatusCode = SysDataMgrDAL.GetEmptyAgentStatusDS(1);			
			ViewState["SCMode"] = ScreenMode.Query;
			Session["SESSION_DS1"] = m_sdsStatusCode;
			dbCmbAgentId.Value="";dbCmbAgentName.Value="";
			dbCmbAgentId.Text="";dbCmbAgentName.Text="";

			dgStatus.EditItemIndex = 0;
			dgStatus.CurrentPageIndex = 0;
			BindSCGrid();
			btnExecuteQuery.Enabled = true;
			Utility.EnableButton(ref btnInsertStatus, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			lblSCMessage.Text = "";

			ShowButtonColumns(false);			
			ResetDetailsGrid();
			dbCmbAgentId.Enabled=true;
			dbCmbAgentName.Enabled=true;
		}
	
		private void InsertAgent(int iRowIndex)
		{
			m_sdsAgent = AgentProfileMgrDAL.GetEmptyAgentDS(1);		
			DataRow drEach =  m_sdsAgent.ds.Tables[0].Rows[0];	
			drEach["agentid"]	= dbCmbAgentId.Value; 
			drEach["agent_name"]= dbCmbAgentName.Value;
			Session["SESSION_DS3"]= m_sdsAgent;
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			InsertAgent(0);
			ViewState["QUERY_DS"] = m_sdsAgent.ds;
			ViewState["currentPage"] = 0;
			ViewState["currentSet"] = 0;
			GetAgentRecSet();			
			if ((decimal)ViewState["QryRes"]!=0)
			{
				DisplayCurrentPage();
				ResetDetailsGrid();
				FillSCDataRow(dgStatus.Items[0],0);
				ViewState["QUERY_DSS"] = m_sdsStatusCode.ds;
				dgStatus.CurrentPageIndex = 0;
				ShowCurrentSCPage();
				btnExecuteQuery.Enabled = false;
				//		Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				lblSCMessage.Text = "";
				ViewState["SCMode"] = ScreenMode.ExecuteQuery;
				ViewState["AgentMode"] = ScreenMode.ExecuteQuery;
				EnableNavigationButtons(true,true,true,true);
				dbCmbAgentId.Enabled=false;
				dbCmbAgentName.Enabled=false;
			}
			else
			{
				lblSCMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			}
		}

		private void btnInsertStatus_Click(object sender, System.EventArgs e)
		{
			if((int)ViewState["SCMode"] != (int)ScreenMode.Insert || m_sdsStatusCode.ds.Tables[0].Rows.Count >= dgStatus.PageSize)
			{
				m_sdsStatusCode = SysDataMgrDAL.GetEmptyAgentStatusDS(0);
			}

			ShowButtonColumns(true); 
			AddRowInSCGrid();	
			dgStatus.EditItemIndex = m_sdsStatusCode.ds.Tables[0].Rows.Count - 1;
			dgStatus.CurrentPageIndex = 0;			
			LoadComboLists(false);
			BindSCGrid();
			ViewState["SCMode"] = ScreenMode.Insert;
			ViewState["SCOperation"] = Operation.Insert;
			Utility.EnableButton(ref btnInsertStatus, com.common.util.ButtonType.Insert,false,m_moduleAccessRights);
			btnExecuteQuery.Enabled = false;
			lblSCMessage.Text = "";
			ResetDetailsGrid();
			getPageControls(Page);
		}

		
		#region Status Grid Methods
		public void dgStatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Label lblStatus = (Label)dgStatus.SelectedItem.FindControl("lblStatus");
			msTextBox txtStatus = (msTextBox)dgStatus.SelectedItem.FindControl("txtStatus");
			String strStatus = null;

			if(lblStatus != null)
			{
				strStatus = lblStatus.Text;
			}

			if(txtStatus != null)
			{
				strStatus = txtStatus.Text;
			}

			ViewState["CurrentSC"] = strStatus;
			dgException.CurrentPageIndex = 0;

			
			ShowCurrentEXPage();

			ViewState["EXMode"] = ScreenMode.ExecuteQuery;

			if((int)ViewState["SCMode"] != (int)ScreenMode.Insert && (int)ViewState["SCOperation"] != (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsertStatus, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			}

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && dgStatus.EditItemIndex == dgStatus.SelectedIndex && (int)ViewState["SCOperation"] == (int)Operation.Insert)
			{
				btnInsertException.Enabled = false;
			}
			else
			{
				btnInsertException.Enabled = true;
			}
			lblSCMessage.Text = "";

		}

		protected void dgStatus_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && (int) ViewState["SCOperation"] == (int)Operation.Insert && dgStatus.EditItemIndex > 0)
			{
				m_sdsStatusCode.ds.Tables[0].Rows.RemoveAt(dgStatus.EditItemIndex);
				m_sdsStatusCode.QueryResultMaxSize--;
				dgStatus.CurrentPageIndex = 0;
			}

			dgStatus.EditItemIndex = e.Item.ItemIndex;
			ViewState["SCOperation"] = Operation.Update;
			BindSCGrid();
			lblSCMessage.Text = "";
			btnInsertException.Enabled=false;
			
		}
		
		public void dgStatus_Update(object sender, DataGridCommandEventArgs e)
		{
			//String strAgentId=(String)ViewState["AgentID"];
			String strAgentId = dbCmbAgentId.Value;
			if ((strAgentId=="") || (strAgentId == null))
			{
				//lblSCMessage.Text="Agent Id cannot be null";
				lblSCMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_AGENTID", utility.GetUserCulture());
				return;
			}
			
			FillSCDataRow(e.Item,e.Item.ItemIndex);

			int iOperation = (int)ViewState["SCOperation"];
			switch(iOperation)
			{

				case (int)Operation.Update:

					DataSet dsToUpdate = m_sdsStatusCode.ds.GetChanges();
					SysDataMgrDAL.ModifyStatusCodeDS(appID,enterpriseID,strAgentId,dsToUpdate);
					m_sdsStatusCode.ds.AcceptChanges();
					lblSCMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
					Utility.EnableButton(ref btnInsertStatus, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
					break;

				case (int)Operation.Insert:

					DataSet dsToInsert = m_sdsStatusCode.ds.GetChanges();
					try
					{
						SysDataMgrDAL.AddStatusCodeDS(appID,enterpriseID,strAgentId,dsToInsert);
						m_sdsStatusCode.ds.AcceptChanges();
						lblSCMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						Utility.EnableButton(ref btnInsertStatus, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblSCMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"DUPLICATE_STATUS_CODE",utility.GetUserCulture());				
							return;
						}
						if(strMsg.IndexOf("unique constraint") != -1 )
						{
							lblSCMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"DUPLICATE_STATUS_CODE",utility.GetUserCulture());				
							return;
						}	
						if(strMsg.IndexOf("FOREIGN KEY") != -1 )
						{
							lblSCMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"MASTER_NOT_FOUND",utility.GetUserCulture());				
							return;
						}	
						else
						{
							lblSCMessage.Text =  strMsg;
							return;
						}	
						
					}
					
					break;

			}
			
			dgStatus.EditItemIndex = -1;
			ViewState["SCOperation"] = Operation.None;
			lblEXMessage.Text="";
			btnInsertException.Enabled=true;
			BindSCGrid();
		}

		protected void dgStatus_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgStatus.EditItemIndex = -1;

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && (int)ViewState["SCOperation"] == (int)Operation.Insert)
			{
				m_sdsStatusCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsStatusCode.QueryResultMaxSize--;
				dgStatus.CurrentPageIndex = 0;
			}
			ViewState["SCOperation"] = Operation.None;
			BindSCGrid();
			Utility.EnableButton(ref btnInsertStatus, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			lblSCMessage.Text = "";
			btnInsertException.Enabled=true;
			
		}

		public void dgStatus_Delete(object sender, DataGridCommandEventArgs e)
		{
			String strAgentId=(String)ViewState["AgentID"];
			msTextBox txtStatus = (msTextBox)e.Item.FindControl("txtStatus");
			Label lblStatus = (Label)e.Item.FindControl("lblStatus");
			String strStatus = null;
			if(e.Item.Cells[2].Enabled ==false)
			{
				lblSCMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"NO_DEL_POSSIBLE",utility.GetUserCulture());
				return;
			}
			if(txtStatus != null)
			{
				strStatus = txtStatus.Text;
			}

			if(lblStatus != null)
			{
				strStatus = lblStatus.Text;
			}
			try
			{
				SysDataMgrDAL.DeleteStatusCodeDS(appID,enterpriseID,strAgentId, strStatus);
				lblSCMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblSCMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_EXP_TRANS",utility.GetUserCulture()); 										
				}
				return;
			}

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
			{
				m_sdsStatusCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsStatusCode.QueryResultMaxSize--;
				dgStatus.CurrentPageIndex = 0;

				if((int) ViewState["SCOperation"] == (int)Operation.Insert && dgStatus.EditItemIndex > 0)
				{
					m_sdsStatusCode.ds.Tables[0].Rows.RemoveAt(dgStatus.EditItemIndex-1);
					m_sdsStatusCode.QueryResultMaxSize--;
				}
				dgStatus.EditItemIndex = -1;
				dgStatus.SelectedIndex = -1;
				BindSCGrid();
				ResetDetailsGrid();
				
			}
			else
			{
				ShowCurrentSCPage();
			}

			
			ViewState["SCOperation"] = Operation.None;
			Utility.EnableButton(ref btnInsertStatus, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			lblEXMessage.Text="";
			btnInsertException.Enabled=true;
		}

		private void BindSCGrid()
		{
			dgStatus.VirtualItemCount = System.Convert.ToInt32(m_sdsStatusCode.QueryResultMaxSize);
			dgStatus.DataSource = m_sdsStatusCode.ds;
			dgStatus.DataBind();
			Session["SESSION_DS1"] = m_sdsStatusCode;
		}

		private void AddRowInSCGrid()
		{
			SysDataMgrDAL.AddRowAgentStatusDS(m_sdsStatusCode);
			Session["SESSION_DS1"] = m_sdsStatusCode;
		}

		protected void dgStatus_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsStatusCode.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			msTextBox txtStatus = (msTextBox)e.Item.FindControl("txtStatus");
			int iOperation = (int) ViewState["SCOperation"];
			if(txtStatus != null && iOperation == (int)Operation.Update)
			{
				txtStatus.Enabled = false;
			}

			SetAgentIDServerStates();
			SetAgentNameServerStates();
		}

		protected void dgStatus_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgStatus.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentSCPage();
			
		}

		public void dgStatus_Button(object sender, DataGridCommandEventArgs e)
		{
			int iOperation = (int)ViewState["SCOperation"];
			if (btnExecuteQuery.Enabled==true)
				return;

			if((iOperation==(int)Operation.None))
				return;
					
			if ((iOperation == (int)Operation.Update))
				return;


			String strCmdNm = e.CommandName;
			String strStatus = null;			
			
//			ShowButtonColumns(false);

			if(strCmdNm.Equals("StatusSearch"))
			{
				msTextBox txtStatus = (msTextBox)e.Item.FindControl("txtStatus");
				TextBox txtStatusDescp = (TextBox)e.Item.FindControl("txtStatusDescp");

				String strStatusClientID = null;
				String strSCDescpClientID=null;
				if(txtStatus != null)
				{
					strStatusClientID = txtStatus.ClientID;
					strStatus = txtStatus.Text;
				}
				if(txtStatusDescp != null)
				{
					strSCDescpClientID = txtStatusDescp.ClientID;					
				}
				
				String sUrl = "StatusCodePopup1.aspx?FORMID=AgentStatusCost&STATUSCODE="+strStatus+"&STATUSCODE_CID="+strStatusClientID+"&SCDESCRIPTION_CID="+strSCDescpClientID;
				
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				Logger.LogDebugInfo("StatusCode","dgStatus_Button","","Data Grid Status Code Search button clicked");
			}
		
		}

		private void ShowCurrentSCPage()
		{
			String strAgentId=(String)ViewState["AgentID"];
			int iStartIndex = dgStatus.CurrentPageIndex * dgStatus.PageSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DSS"];
						
			m_sdsStatusCode = SysDataMgrDAL.GetStatusCodeDS(appID,enterpriseID,strAgentId,iStartIndex,dgStatus.PageSize,dsQuery);
			decimal pgCnt = Convert.ToInt32((m_sdsStatusCode.QueryResultMaxSize - 1))/dgStatus.PageSize;
			if(pgCnt < dgStatus.CurrentPageIndex)
			{
				dgStatus.CurrentPageIndex = System.Convert.ToInt32(pgCnt);				
			}
			dgStatus.SelectedIndex = -1;
			dgStatus.EditItemIndex = -1;
			lblSCMessage.Text = "";
			lblEXMessage.Text="";

			BindSCGrid();
			ResetDetailsGrid();
		}

		private void FillSCDataRow(DataGridItem item, int drIndex)
		{

			DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
			msTextBox txtStatus = (msTextBox)item.FindControl("txtStatus");
			TextBox txtStatusDescp = (TextBox)item.FindControl("txtStatusDescp");
			TextBox txtStatusSurcharge = (TextBox)item.FindControl("txtStatusSurcharge");

			if(txtStatus != null)
				drCurrent["status_code"] = txtStatus.Text;			
			if(txtStatusDescp != null)
				drCurrent["status_description"] = txtStatusDescp.Text;						
			if(txtStatusSurcharge != null && txtStatusSurcharge.Text !="0")
				drCurrent["surcharge"] = txtStatusSurcharge.Text;
		}
		#endregion
	
		private void btnInsertException_Click(object sender, System.EventArgs e)
		{
			//If coming to any other mode to Insert Mode then create the empty data set.
			lblEXMessage.Text ="";
			if((int)ViewState["EXMode"] != (int)ScreenMode.Insert || m_sdsExceptionCode.ds.Tables[0].Rows.Count >= dgException.PageSize)
			{
				m_sdsExceptionCode = SysDataMgrDAL.GetEmptyAgentExcepDS(0);
			}

			AddRowInExGrid();	
			dgException.EditItemIndex = m_sdsExceptionCode.ds.Tables[0].Rows.Count - 1;
			dgException.CurrentPageIndex = 0;
			
			LoadComboLists(false);
			BindExGrid();
			ViewState["EXMode"] = ScreenMode.Insert;
			ViewState["EXOperation"] = Operation.Insert;
			btnInsertException.Enabled = false;
			getPageControls(Page);
		}

		
		#region Exception Grid Methods
		protected void dgException_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int)ViewState["EXMode"] == (int)ScreenMode.Insert && (int) ViewState["EXOperation"] == (int)Operation.Insert && dgException.EditItemIndex > 0)
			{
				m_sdsExceptionCode.ds.Tables[0].Rows.RemoveAt(dgException.EditItemIndex);
				m_sdsExceptionCode.QueryResultMaxSize--;
				dgException.CurrentPageIndex = 0;
			}

			dgException.EditItemIndex = e.Item.ItemIndex;
			ViewState["EXOperation"] = Operation.Update;
			BindExGrid();
			lblEXMessage.Text = "";
			lblSCMessage.Text="";
			
		}

		public void dgException_Update(object sender, DataGridCommandEventArgs e)
		{
			//String strAgentId=(String)ViewState["AgentID"];
			String strAgentId = dbCmbAgentId.Value;
			if ((strAgentId=="") || (strAgentId == null))
			{
				//lblSCMessage.Text="Agent Id cannot be null";
				lblSCMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_AGENTID", utility.GetUserCulture());
				return;
			}
			try
			{
				FillEXDataRow(e.Item,e.Item.ItemIndex);
			}
			catch(Exception ex)
			{
				String msg = ex.ToString();
				if (msg.IndexOf("unique") != -1)
				{
					lblEXMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage, "DUP_KEY_FOUND_EXP_CODE_NO", utility.GetUserCulture());
				}
				return;
			}
			int iOperation = (int)ViewState["EXOperation"];
			String strStatus = (String)ViewState["CurrentSC"];
			switch(iOperation)
			{
				case (int)Operation.Update:
					DataSet dsToUpdate = m_sdsExceptionCode.ds.GetChanges();
					SysDataMgrDAL.ModifyExceptionCodeDS(appID,enterpriseID,strAgentId, strStatus,dsToUpdate);					
					m_sdsExceptionCode.ds.AcceptChanges();
					lblEXMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
					btnInsertException.Enabled = true;
					break;

				case (int)Operation.Insert:
					DataSet dsToInsert = m_sdsExceptionCode.ds.GetChanges();
					try
					{
						SysDataMgrDAL.AddExceptionCodeDS(appID,enterpriseID,strAgentId,strStatus,dsToInsert);
						m_sdsExceptionCode.ds.AcceptChanges();
						lblEXMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						btnInsertException.Enabled = true;
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblEXMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_EXP_CODE_NO",utility.GetUserCulture());
						}
						else if(strMsg.IndexOf("unique") != -1 )
						{	//unique constraint violated //Duplicate key
							lblEXMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_EXP_CODE_NO",utility.GetUserCulture());
						}
						else if(strMsg.IndexOf("FOREIGN KEY") != -1 )
						{	//FOREIGN KEY
							lblEXMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MASTER_NOT_FOUND",utility.GetUserCulture());
						}
						else
						{
							lblEXMessage.Text = strMsg;
						}
						return;						
					}
			
					break;
			}
			
			dgException.EditItemIndex = -1;
			ViewState["EXOperation"] = Operation.None;
			BindExGrid();	
			lblSCMessage.Text="";
			
		}

		protected void dgException_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgException.EditItemIndex = -1;
			if((int)ViewState["EXMode"] == (int)ScreenMode.Insert && (int)ViewState["EXOperation"] == (int)Operation.Insert)
			{
				m_sdsExceptionCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsExceptionCode.QueryResultMaxSize--;
				dgException.CurrentPageIndex = 0;
			}
			btnInsertException.Enabled = true;
			lblEXMessage.Text = "";

			ViewState["EXOperation"] = Operation.None;
			BindExGrid();
			
		}

		protected void dgException_Delete(object sender, DataGridCommandEventArgs e)
		{
			msTextBox txtException = (msTextBox)e.Item.FindControl("txtException");
			Label lblException = (Label)e.Item.FindControl("lblException");
			String strException = null;
			if(txtException != null)
			{
				strException = txtException.Text;
			}

			if(lblException != null)
			{
				strException = lblException.Text;
			}

			String strStatus = (String)ViewState["CurrentSC"];
			String strAgentId=(String)ViewState["AgentID"];
			try
			{
				SysDataMgrDAL.DeleteExceptionCodeDS(appID,enterpriseID,strStatus,strException, strAgentId);
				lblEXMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblEXMessage.Text = "Error Deleting Exception code. Exception code being used in some transaction(s)";					
				}
				return;
			}

			if((int)ViewState["EXMode"] == (int)ScreenMode.Insert)
			{
				m_sdsExceptionCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsExceptionCode.QueryResultMaxSize--;
				dgException.CurrentPageIndex = 0;

				if((int) ViewState["EXOperation"] == (int)Operation.Insert && dgException.EditItemIndex > 0)
				{
					m_sdsExceptionCode.ds.Tables[0].Rows.RemoveAt(dgException.EditItemIndex-1);
					m_sdsExceptionCode.QueryResultMaxSize--;
				}
				dgException.EditItemIndex = -1;
				BindExGrid();
			}
			else
			{
				ShowCurrentEXPage();
			}
			

			ViewState["EXOperation"] = Operation.None;
			btnInsertException.Enabled = true;
			lblSCMessage.Text="";
		}

		protected void dgException_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsExceptionCode.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			msTextBox txtException = (msTextBox)e.Item.FindControl("txtException");
			int iOperation = (int) ViewState["EXOperation"];
			if(txtException != null && iOperation == (int)Operation.Update)
			{
				txtException.Enabled = false;
			}

			DropDownList ddlExMBG = (DropDownList)e.Item.FindControl("ddlExMBG");			
			if(ddlExMBG != null)
			{
				ddlExMBG.DataSource = LoadMBGOptions();
				ddlExMBG.DataTextField = "Text";
				ddlExMBG.DataValueField = "StringValue";
				ddlExMBG.DataBind();
				if((drSelected["mbg"]!= null) && (!drSelected["mbg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strMBG = (String)drSelected["mbg"];
					ddlExMBG.SelectedIndex = ddlExMBG.Items.IndexOf(ddlExMBG.Items.FindByValue(strMBG));//.FindByValue("N"));
					
				}				
			}
						
		}

		protected void dgException_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgException.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentEXPage();
			
		}

		public void dgException_Button(object sender, DataGridCommandEventArgs e)
		{
			int iOperation = (int)ViewState["EXOperation"];
			if((iOperation==(int)Operation.None))
				return;
					
			if ((iOperation == (int)Operation.Update))
				return;

			String strCmdNm = e.CommandName;
			String strException = null;			
			String strStatus = (String)ViewState["CurrentSC"];
			if(strCmdNm.Equals("ExceptionSearch"))
			{
				msTextBox txtException = (msTextBox)e.Item.FindControl("txtException");
				TextBox txtExceptionDescp = (TextBox)e.Item.FindControl("txtExceptionDescp");

				String strExceptionClientID = null;
				String strEXDescpClientID=null;
				if(txtException != null)
				{
					strExceptionClientID = txtException.ClientID;
					strException = txtException.Text;
				}
				if(txtExceptionDescp != null)
				{
					strEXDescpClientID = txtExceptionDescp.ClientID;					
				}
				
				String sUrl = "ExceptionCodePopup.aspx?FORMID=AgentStatusCost&STATUSCODE="+strStatus+"&EXCEPTIONCODE="+strException+"&EXCEPTIONCODE_CID="+strExceptionClientID+"&EXDESCRIPTION_CID="+strEXDescpClientID;
				
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				Logger.LogDebugInfo("StatusCode","dgStatus_Button","","Data Grid Status Code Search button clicked");
			}
		
		}

		#endregion

		private void BindExGrid()
		{
			dgException.VirtualItemCount =System.Convert.ToInt32(m_sdsExceptionCode.QueryResultMaxSize);
			dgException.DataSource = m_sdsExceptionCode.ds;
			dgException.DataBind();
			Session["SESSION_DS2"] = m_sdsExceptionCode;
		}

		private void AddRowInExGrid()
		{
			SysDataMgrDAL.AddRowAgentExcepDS(m_sdsExceptionCode);
			Session["SESSION_DS2"] = m_sdsExceptionCode;
		}

		private void ShowCurrentEXPage()
		{
			int iStartIndex = dgException.CurrentPageIndex * dgException.PageSize;
			String strStatus = (String)ViewState["CurrentSC"];
			String strAgentId=(String)ViewState["AgentID"];

			m_sdsExceptionCode = SysDataMgrDAL.GetExceptionCodeDS(appID,enterpriseID,strAgentId,strStatus,iStartIndex,dgException.PageSize);

			decimal pgCnt = Convert.ToInt32((m_sdsExceptionCode.QueryResultMaxSize - 1))/dgException.PageSize;
			if(pgCnt < dgException.CurrentPageIndex)
			{
				dgException.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgException.EditItemIndex = -1;
			BindExGrid();
			lblEXMessage.Text = "";
		}

		private void FillEXDataRow(DataGridItem item, int drIndex)
		{
			
			DataRow drCurrent = m_sdsExceptionCode.ds.Tables[0].Rows[drIndex];
			msTextBox txtException = (msTextBox)item.FindControl("txtException");
			TextBox txtExceptionDescp = (TextBox)item.FindControl("txtExceptionDescp");
			DropDownList ddlExMBG = (DropDownList)item.FindControl("ddlExMBG");
			msTextBox txtExcepSurcharge = (msTextBox)item.FindControl("txtExcepSurcharge");
			if(txtException != null)
				drCurrent["exception_code"] = txtException.Text;		
	
			if(txtExceptionDescp != null)
				drCurrent["exception_description"] = txtExceptionDescp.Text;			
			if(ddlExMBG != null)
				drCurrent["mbg"] = ddlExMBG.SelectedItem.Value;			
			if(txtExcepSurcharge != null && txtExcepSurcharge.Text !="0")
				drCurrent["Surcharge"] = Convert.ToDecimal(txtExcepSurcharge.Text);
			
		}


		/// <summary>
		/// Other methods..
		/// </summary>
		/// <param name="bNilValue"></param>
		protected ICollection LoadMBGOptions()		
		{			
			return m_dvMBGOptions;
		}

		private DataView GetMBGOptions(bool showNilOption) 
		{
			DataTable dtMBGOptions = new DataTable();
 
			dtMBGOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMBGOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList mbgOptionArray = Utility.GetCodeValues(appID,utility.GetUserCulture(),"mbg",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtMBGOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtMBGOptions.Rows.Add(drNilRow);
			}
			foreach(SystemCode mbgSysCode in mbgOptionArray)
			{
				DataRow drEach = dtMBGOptions.NewRow();
				drEach[0] = mbgSysCode.Text;
				drEach[1] = mbgSysCode.StringValue;
				dtMBGOptions.Rows.Add(drEach);
			}

			DataView dvMBGOptions = new DataView(dtMBGOptions);
			return dvMBGOptions;
		}

		private void LoadComboLists(bool bNilValue)
		{
			m_dvMBGOptions = GetMBGOptions(bNilValue);
		}

		private void ShowButtonColumns(bool bShow)
		{
			dgStatus.Columns[0].Visible = bShow;
			dgStatus.Columns[1].Visible = bShow;
			dgStatus.Columns[2].Visible = bShow;
		}

		private void ResetDetailsGrid()
		{
			dgException.CurrentPageIndex = 0;
			btnInsertException.Enabled = false;			
			m_sdsExceptionCode = SysDataMgrDAL.GetEmptyAgentExcepDS(0);
			Session["SESSION_DS2"] = m_sdsExceptionCode;
			lblEXMessage.Text = "";
			BindExGrid();
		}

		private void GetAgentRecSet()
		{
			int iStartIndex = Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsAgent = AgentProfileMgrDAL.GetAgentDS(appID,enterpriseID,iStartIndex,m_iSetSize,dsQuery);
			Session["SESSION_DS3"] = m_sdsAgent;
			ViewState["QryRes"] =  m_sdsAgent.QueryResultMaxSize; 			
			decimal pgCnt = (m_sdsAgent.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < Convert.ToDecimal(ViewState["currentSet"]))
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
		}


		private void btnNextPage_Click(object sender, System.EventArgs e)
		{			
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);
			if(Convert.ToInt32(ViewState["currentPage"])  < (m_sdsAgent.DataSetRecSize - 1) )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) + 1;
				DisplayCurrentPage();
				ShowCurrentSCPage();
			}
			else
			{
				decimal iTotalRec = (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) + m_sdsAgent.DataSetRecSize;
				if( iTotalRec ==  m_sdsAgent.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) + 1;	
				ViewState["currentPage"] = 0;
				DisplayCurrentPage();
				ShowCurrentSCPage();
			}
			EnableNavigationButtons(true,true,true,true);
			Utility.EnableButton(ref btnInsertStatus, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
		}
		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = Convert.ToInt32((m_sdsAgent.QueryResultMaxSize - 1))/m_iSetSize;	
			GetAgentRecSet();
			ViewState["currentPage"] = m_sdsAgent.DataSetRecSize - 1;
			DisplayCurrentPage();
			ShowCurrentSCPage();
			EnableNavigationButtons(true,true,false,false);
			Utility.EnableButton(ref btnInsertStatus, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
		}

		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);
			if(Convert.ToInt32(ViewState["currentPage"])  > 0 )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) - 1;
				DisplayCurrentPage();	
				ShowCurrentSCPage();
			}
			else
			{
				if( (Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) - 1;	
				GetAgentRecSet();
				ViewState["currentPage"] = m_sdsAgent.DataSetRecSize - 1;
				DisplayCurrentPage();
				ShowCurrentSCPage();
			}
			EnableNavigationButtons(true,true,true,true);
			Utility.EnableButton(ref btnInsertStatus, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = 0;	
			GetAgentRecSet();
			ViewState["currentPage"] = 0;
			DisplayCurrentPage();
			ShowCurrentSCPage();
			EnableNavigationButtons(false,false,true,true);		
			Utility.EnableButton(ref btnInsertStatus, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
		}
		
		private void DisplayCurrentPage()
		{
			DataRow drCurrent = m_sdsAgent.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
						
			if(!drCurrent["agentid"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dbCmbAgentId.Text		= drCurrent["agentid"].ToString();
				dbCmbAgentId.Value		= drCurrent["agentid"].ToString();
				ViewState["AgentID"]	= drCurrent["agentid"].ToString();
			}
			if(!drCurrent["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dbCmbAgentName.Text		= drCurrent["agent_name"].ToString();
				dbCmbAgentName.Value	= drCurrent["agent_name"].ToString();
			}
		}

		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}


		private void btnAgentCode_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("AgentPopup.aspx?FORMID=AgentStatusCost&COUNTRY_CID=txtCountry&STATE_CID=txtState&ZIPCODE_CID=txtZipCode&COUNTRY_TEXT=");
		}

		/// <summary>
		/// To open another window
		/// </summary>
		/// <param name="strUrl">an url to specify where the address is</param>
		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);	
		}


		
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]		
		public static object AgentIdServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
						
			String strAgentName="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{			
				if(args.ServerState["strAgentName"] != null && args.ServerState["strAgentName"].ToString().Length > 0)
				{
					strAgentName = args.ServerState["strAgentName"].ToString();					
				}	
			}
			
			DataSet dataset = DbComboDAL.PayerIDQuery(strAppID,strEnterpriseID,args, "A", strAgentName);	
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethod(true)]
		public static object AgentNameServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
	
			String strAgentID= "";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{				
				if (args.ServerState["strAgentID"] != null && args.ServerState["strAgentID"].ToString().Length > 0 )
				{
					strAgentID = (args.ServerState["strAgentID"].ToString());
				}
			}	

			DataSet dataset = DbComboDAL.PayerNameQuery(strAppID,strEnterpriseID,args, "A", strAgentID);	
			return dataset;                
		}
		private void SetAgentNameServerStates()
		{						
			String strAgentID=dbCmbAgentId.Value;			
			Hashtable hash = new Hashtable();		
			if (strAgentID !="")
			{
				hash.Add("strAgentID", strAgentID);
			}			
			dbCmbAgentName.ServerState = hash;
			dbCmbAgentName.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}

		private void SetAgentIDServerStates()
		{			
			String strAgentName=dbCmbAgentName.Value;			
			Hashtable hash = new Hashtable();			
			if (strAgentName !="")
			{
				hash.Add("strAgentName", strAgentName);
			}						

			dbCmbAgentId.ServerState = hash;
			dbCmbAgentId.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";			
		}


	}
}
