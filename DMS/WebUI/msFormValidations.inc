<SCRIPT LANGUAGE="JavaScript">
/*
IMPORTANT NOTES
- If there is any changes on the exising functions below, PLEASE search into the solution that it is IMPACT by the other forms or not.
*/
<!-- Begin
// isDigit
//    c 01....9 ise true, yoksa false doner.
function isDigit(myInput){
	var test = '' + myInput;
	if (isNaN(parseInt(test)))
		{
		return false;
		}
	return true;
}

function isFloat(myInput){
	var test = '' + myInput;
	if (isNaN(parseFloat(test)))
		{
		return false;
		}
	return true;
}


function isValidNumber(myInput)
{
	if (isNaN(myInput))
		{
			return false;
		}
	return true;

}
// trim fonksiyonlari
// ltrim: left trim
// rtrim: right trim
// ntrim: her iki taraftan trim
function lTrim(myInput2){
 while(myInput2.indexOf(' ',0)==0){
  myInput2 = myInput2.substr(1);
 }
 return myInput2;
}
function rTrim(myInput1){
 if (myInput1.length==0)
  return '';
 while(myInput1.lastIndexOf(' ')==(myInput1.length-1)){
  myInput1 = myInput1.substr(0,myInput1.length-1);
 }
 return myInput1;
}
function nTrim(myInput3){
 myInput3 = rTrim(lTrim(myInput3));
 return myInput3;
}

function leapYear(Year){
	if(((Year % 4)==0) && ((Year % 100)!=0) || ((Year % 400)==0))
		return (1);
	else
		return (0);
}

function getDaysInMonth(month, year) {
	var days = 100;
	if(month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12)
		days=31;
	else if (month==4 || month==6 || month==9 || month==11)
		days=30;
	else if (month==2){
		if (leapYear (year)==1)
			days=29;
		else
			days=28;
	}
	//alert(days);
	return (days);
}

function setDate(myInput){
	if (myInput.value.length>0){
		myInput.value = nTrim(myInput.value);
		//alert(myInput.value);
		var error=0;
		var myDate='';
		var myMonth='';
		var myYear='';
		
		for(i=0; i<2 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myDate=myDate + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}

		for(i=3; i<5 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myMonth=myMonth + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}

		for(i=6; i<10; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myYear=myYear + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}

		if(error==0)
		{
			if(parseInt(myDate) <= getDaysInMonth(myMonth, myYear))
			{
				if(parseInt(myMonth) <= 12)
				{
					if((parseInt(myYear) < 2100) && (parseInt(myYear) > 1999))
					{
					}
					else
					{
						error=4;
					}
				}
				else
				{
					error=3;
				}
			}
			else
			{
				error=2;
			}
		
			switch(error)
			{
				case 2:
					alert('Date format should be dd/mm/yyyy.  Day is Incorrect:  '+myDate);
					myInput.value='';
					myInput.focus();
					return false;
				case 3:
					alert('Date format should be dd/mm/yyyy.  Month is Incorrect: '+myMonth+'. Enter month between 1 and 12.');
					myInput.value='';
					myInput.focus();
					return false;
				case 4:
					alert('Date format should be dd/mm/yyyy.  Year is incorrect: '+myYear+'. Enter year between 2000 to 2099');
					myInput.value='';
					myInput.focus();
					return false;
				case 0:
					return true;
				default:
					return false;
			}
		}	
		else
		{
			alert('Incorrect date. Please enter correct date in format dd/mm/yyyy.');
			myInput.value='';
			myInput.focus();
			return false;
		}
	}
}

function setDateExportPOD(myInput){
	if (myInput.value.length>0){
		myInput.value = nTrim(myInput.value);
		//alert(myInput.value);
		var error=0;
		var myDate='';
		var myMonth='';
		var myYear='';
		
		var date = new Date();
		var dateNow = date.getDate();
		var monthNow = date.getMonth()+1;
		var yearNow = date.getYear();
		
		if(monthNow<10){monthNow = '0'+ monthNow };
		
		for(i=0; i<2 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myDate=myDate + myInput.value.substr(i,1)
				
			}
			else
			{
				error=1;
			}
		}

		for(i=3; i<5 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myMonth=myMonth + myInput.value.substr(i,1)
				
			}
			else
			{
				error=1;
			}
		}

		for(i=6; i<10; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myYear=myYear + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}

		if(error==0)
		{
			if(parseInt(myDate) <= getDaysInMonth(myMonth, myYear))
			{
				if(parseInt(myYear) >= parseInt(yearNow)) // If year more than now or this year
				{
					if(parseInt(myYear) > parseInt(yearNow))
					{
						error=5;
					}
					else if(myMonth > monthNow)
					{
						if(myMonth > 12)
						{
							error=3;
						}
						else
						{
							error=5;
						}
					}	
					else if(myDate > dateNow)
					{
						if(myMonth >= monthNow)
						{
							error=5;
						}
						else
						{
							error=0;
						}
					}
					else if(myMonth < 1) 
					{
						error=3;
					}
					else if(myDate < 1)
					{
						error=2;
					}
				}
				else if(myMonth > monthNow) // If year less than now 
				{	
					if(myMonth > monthNow)
					{
						if(myMonth > 12)
						{
							error=3;
						}
						if(myDate < 1)
						{
							error=2;
						}
						else
						{
							error=0;
						}
					}
				}
				else if(myMonth < 1)
				{
					error=3;
				}
				else if(myDate > dateNow)
				{
					error=0;
				}
				else if(myDate < 1)
				{
					error=2;
				}
				else if(parseInt(myMonth) <= 12)
				{
					if((parseInt(myYear) < 2100) && (parseInt(myYear) > 1999))
					{
					}
					else
					{
						error=4;
					}
				}
				else
				{
					error=3;
				}
			}
			else
			{
				error=2;
			}
		
			switch(error)
			{
				case 2:
					alert('Date format should be dd/mm/yyyy.  Day is Incorrect:  '+myDate);
					myInput.value='';
					myInput.focus();
					return false;
				case 3:
					alert('Date format should be dd/mm/yyyy.  Month is Incorrect: '+myMonth+'. Enter month between 1 and 12.');
					myInput.value='';
					myInput.focus();
					return false;
				case 4:
					alert('Date format should be dd/mm/yyyy.  Year is incorrect: '+myYear+'. Enter year between 2000 to 2099');
					myInput.value='';
					myInput.focus();
					return false;
				case 5:
					alert('Date must be less than or equal to today.');
					myInput.value='';
					myInput.focus();
					return false;
				case 0:
					return true;
				default:
					return false;
			}
		}	
		else
		{
			alert('Incorrect date. Please enter correct date in format dd/mm/yyyy.');
			myInput.value='';
			myInput.focus();
			return false;
		}
	}
}

function setDateTime(myInput)
{
	if (myInput.value.length>0)
	{
		myInput.value = nTrim(myInput.value);
		//alert(myInput.value);
		var error=0;
		var myDate='';
		var myMonth='';
		var myYear='';
		var myHour='';
		var myMin='';
		
		for(i=0; i<2 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myDate=myDate + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}

		for(i=3; i<5 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myMonth=myMonth + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}

		for(i=6; i<10; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myYear=myYear + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		for(i=11; i<13; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myHour=myHour + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		for(i=14; i<16; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myMin=myMin + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		

		if(error==0)
		{
			if(parseInt(myDate) <= getDaysInMonth(myMonth, myYear))
			{
				if(parseInt(myMonth) <= 12)
				{
					if((parseInt(myYear) < 2100) && (parseInt(myYear) > 1999))
					{
						if(parseInt(myHour) <= 23)
						{
							if(parseInt(myMin) <= 59)
							{
							
							}
							else
							{
								error=6;
							}
						}
						else
						{
							error=5;
						}
					}
					else
					{
						error=4;
					}
				}
				else
				{
					error=3;
				}
			}
			else
			{
				error=2;
			}
		
			switch(error)
			{
				case 2:
					alert('Incorrect date in dd/mm/yyyy : '+myDate);
					myInput.value='';
					myInput.focus();
					return false;
					
				case 3:
					alert('Enter month between 1 to 12');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 4:
					alert('Enter year between 2000 to 2099');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 5:
					alert('Enter hour between 0 to 23');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 6:
					alert('Enter minute between 0 to 59');
					myInput.value='';
					myInput.focus();
					return false;
					

				case 0:
					return true;
				default:
					return false;
			}
		}	
		else
		{
			alert('Incorrect date. Please enter correct date.');
			myInput.value='';
			myInput.focus();
			return false;
		}
	}

}

function setDateOrTime(myInput)
{
	if (myInput.value.length>0)
	{
		if(setDateTimeNoAction(myInput)==false)
		{
			if(setDate(myInput)==false)
			{
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
	return true;
}

function setDateTimeNoAction(myInput)
{
	if (myInput.value.length>0)
	{
		myInput.value = nTrim(myInput.value);
		//alert(myInput.value);
		var error=0;
		var myDate='';
		var myMonth='';
		var myYear='';
		var myHour='';
		var myMin='';
		
		for(i=0; i<2 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myDate=myDate + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}

		for(i=3; i<5 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myMonth=myMonth + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}

		for(i=6; i<10; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myYear=myYear + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		for(i=11; i<13; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myHour=myHour + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		for(i=14; i<16; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myMin=myMin + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		

		if(error==0)
		{
			if(parseInt(myDate) <= getDaysInMonth(myMonth, myYear))
			{
				if(parseInt(myMonth) <= 12)
				{
					if((parseInt(myYear) < 2100) && (parseInt(myYear) > 1999))
					{
						if(parseInt(myHour) <= 23)
						{
							if(parseInt(myMin) <= 59)
							{
							
							}
							else
							{
								error=6;
							}
						}
						else
						{
							error=5;
						}
					}
					else
					{
						error=4;
					}
				}
				else
				{
					error=3;
				}
			}
			else
			{
				error=2;
			}
		
			switch(error)
			{
				case 2:
					alert('Incorrect date in dd/mm/yyyy : '+myDate);
					myInput.value='';
					myInput.focus();
					return false;
					
				case 3:
					alert('Enter month between 1 to 12');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 4:
					alert('Enter year between 2000 to 2099');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 5:
					alert('Enter hour between 0 to 23');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 6:
					alert('Enter minute between 0 to 59');
					myInput.value='';
					myInput.focus();
					return false;
					

				case 0:
					return true;
				default:
					return false;
			}
		}	
		else
		{
			return false;
		}
	}

}

function setElasticDateTime(myInput)
{
	if (myInput.value.length>0)
	{
		myInput.value = nTrim(myInput.value);
		//alert(myInput.value);
		var error=0;
		var myDate='';
		var myMonth='';
		var myYear='';
		var myHour='';
		var myMin='';
		
		for(i=0; i<2 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myDate=myDate + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}

		for(i=3; i<5 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myMonth=myMonth + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}

		for(i=6; i<10; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myYear=myYear + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		
		if(myInput.value.length > 10)
		{
			for(i=11; i<13; i++)
			{
				if(isDigit(myInput.value.substr(i,1)))
				{
					myHour=myHour + myInput.value.substr(i,1);
				}
				else
				{
					error=1;
				}
			}
			for(i=14; i<16; i++)
			{
				if(isDigit(myInput.value.substr(i,1)))
				{
					myMin=myMin + myInput.value.substr(i,1);
				}
				else
				{
					error=1;
				}
			}
		}
		
		if(error==0)
		{
			if(parseInt(myDate) <= getDaysInMonth(myMonth, myYear))
			{
				if(parseInt(myMonth) <= 12)
				{
					if((parseInt(myYear) < 2100) && (parseInt(myYear) > 1999))
					{
						if(myInput.value.length > 10)
						{
							if(parseInt(myHour) <= 23)
							{
								if(parseInt(myMin) <= 59)
								{
								
								}
								else
								{
									error=6;
								}
							}
							else
							{
								error=5;
							}
						}
					}
					else
					{
						error=4;
					}
				}
				else
				{
					error=3;
				}
			}
			else
			{
				error=2;
			}
		
			switch(error)
			{
				case 2:
					alert('Incorrect date in dd/mm/yyyy : '+myDate);
					myInput.value='';
					myInput.focus();
					return false;
					
				case 3:
					alert('Enter month between 1 to 12');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 4:
					alert('Enter year between 2000 to 2099');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 5:
					alert('Enter hour between 0 to 23');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 6:
					alert('Enter minute between 0 to 59');
					myInput.value='';
					myInput.focus();
					return false;
					

				case 0:
					return true;
				default:
					return false;
			}
		}	
		else
		{
			alert('Incorrect date. Please enter correct date.');
			myInput.value='';
			myInput.focus();
			return false;
		}
	}

}

function setTime(myInput)
{
	if (myInput.value.length>0)
	{
		myInput.value = nTrim(myInput.value);
		//alert(myInput.value);
		var error=0;
		var myHour='';
		var myMin='';
		
		for(i=0; i<2; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myHour=myHour + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		for(i=3; i<5; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myMin=myMin + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
		

		if(error==0)
		{
			if(parseInt(myHour) <= 23)
			{
				if(parseInt(myMin) <= 59)
				{
				
				}
				else
				{
					error=6;
				}
			}
			else
			{
				error=5;
			}

			switch(error)
			{
				case 5:
					alert('Enter hour between 0 to 23');
					myInput.value='';
					myInput.focus();
					return false;
				case 6:
					alert('Enter minute between 0 to 59');
					myInput.value='';
					myInput.focus();
					return false;

				case 0:
					return true;
				default:
					return false;
			}
		}	
		else
		{
			alert('Incorrect time. Please enter correct time.');
			myInput.value='';
			myInput.focus();
			return false;			
		}
	}

}

// Gordion mod�lleri
function CheckMask( toField, tcMask )
{
	var i;
	var lcVal = toField.value;
	var llValOK = true;
	
	if (tcMask.length == 0)
		return true;
		
	for (i=0; i<lcVal.length && llValOK; i++)
	{
		lcMaskChar = tcMask.charAt(i);
		lcValChar  = lcVal.charAt(i);
		switch (lcMaskChar)
		{
			case '9':  // Say�sal
				llValOK = (lcValChar >= '0' && lcValChar <= '9') ;
				break;			
			case 'X':  // Herhangi
				llValOK = true;
				break;			
			case '!':  // B�y�k harf
				llValOK = (lcValChar >= 'A' && lcValChar <= 'Z') || (lcValChar >= '0' && lcValChar <= '9') || (lcValChar = '%' ) || (lcValChar = '*' );
				break;
			default:   // masktaki harf
				llValOK = lcValChar == lcMaskChar;
		}
	}
	
	if (!llValOK)
		toField.value = "";
	
	return llValOK;
}

function InputMask( toField, tcMask )
{
	var lcMaskChar, lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = true;
	
	if (tcMask.length == 0)
		return true;
		
	if (toField.value.length >= tcMask.length)  // InputMask tamam. Art�k kabul etme.
	{
		llRetVal = false;
	}
	else                                        // Mask� yeni gelen harfe g�re parse et
	{
		lcMaskChar = tcMask.charAt(toField.value.length);
		switch (lcMaskChar)
		{
			case '9':  // Say�sal
				llRetVal = (lcNewChar >= '0' && lcNewChar <= '9') ;
				break;
			case 'X':  // Herhangi
				break;
			case '!':  // B�y�k harf
				window.event.keyCode = lcNewChar.toUpperCase().charCodeAt(0);
				break;			         
			default:   // masktaki harf
				toField.value += lcMaskChar;
				llRetVal = InputMask(toField, tcMask);
		}
	}
	
	return llRetVal;
}

function formatInt ( ctrl )
{
	var separator = ",";
	var intm = ctrl.value.replace ( new RegExp ( separator, "g" ), "" );
	var regexp = new RegExp ( "\\B(\\d{3})(" + separator + "|$)" );
	do
	{
		intm = intm.replace ( regexp, separator + "$1" );
	}
	while ( intm.search ( regexp ) >= 0 )
	ctrl.value = intm;
}

function PriceMask( toField )
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = false;
	
	llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9') || (lcNewChar <='.') || (lcNewChar <=',') ) ;
	
	return llRetVal;
}

function NumberMask(toField,precision,scale,minValue,maxValue)
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = false;
	var lmRetVal = false;
	//alert("value is : "+toField.value+" precision " + precision +" scale is : "+scale+" minValue : "+minValue+" maxValue : "+maxValue);
	
	var isDotPresent = false;
	
	var currentNum = toField.value;
	
	var currentScale = 0;
	
	for(i = 0; i < currentNum.length; i++)
	{
		if(currentNum.charAt(i) == '.')
		{
			isDotPresent = true;
		}
		if(isDotPresent)
		{
			currentScale++;
		}
	}
	
	//alert("currentScale : "+currentScale+" scale is : "+scale);
	
	if((minValue < 0 && toField.value.length ==0 && lcNewChar =='-')||(minValue < 0 && toField.value.length > 0 && lcNewChar =='-'))
	{
		if(toField.value.length > 0)
		{
			llRetVal=true;
			if(llRetVal)
			{
				var newNum =lcNewChar + toField.value;
				//alert(newNum);
				var newFloatNum = parseFloat(newNum);
				//alert("New Num : "+ newNum+"New Float Num : "+newFloatNum);
				if(newFloatNum <= maxValue && newFloatNum >= minValue)
				{
					llRetVal = true;
				}
				else
				{
					llRetVal = false;
				}
				
			}
		}
		else
			llRetVal=true;
	}
	else
	{
			if(isDotPresent)
			{
				llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9') && (toField.value.length <= precision) && (currentScale <= scale)) ;
			}
			else
			{
				llRetVal = (((lcNewChar >= '0' && lcNewChar <= '9') || ((lcNewChar == '.') && (scale > 0))) && (toField.value.length < precision)) ;	
			}
			
			
			if(llRetVal)
			{
				var newNum = ''+ toField.value + lcNewChar;
				//alert(newNum);
				var newFloatNum = parseFloat(newNum);
				//alert("New Num : "+ newNum+"New Float Num : "+newFloatNum);
				if(newFloatNum <= maxValue && newFloatNum >= minValue)
				{
					llRetVal = true;
				}
				else
				{
					llRetVal = false;
				}
				
			}
	}
	
	return llRetVal;
}


function NumberMaskCust(toField,precision,scale,minValue,maxValue)
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = false;
	var lmRetVal = false;
	//alert("value is : "+toField.value+" enterd:"+lcNewChar+" precision " + precision +" scale is : "+scale+" minValue : "+minValue+" maxValue : "+maxValue);
	
	if(toField.value=='0'&&toField.value+lcNewChar!='0.')
		return false;
		
	var isDotPresent = false;
	
	var currentNum = toField.value;
	
	var currentScale = 0;
	
	for(i = 0; i < currentNum.length; i++)
	{
		if(currentNum.charAt(i) == '.')
		{
			isDotPresent = true;
		}
		if(isDotPresent)
		{
			currentScale++;	
			if(isDotPresent&&(parseFloat(toField.value+lcNewChar)< parseFloat(minValue))&&currentScale>=scale)
				return false;		
		}
	}
	
	//alert("currentScale : "+currentScale+" scale is : "+scale);
	
	if((parseFloat(minValue) < parseFloat(0.00) && toField.value.length ==0 && lcNewChar =='-')||(parseFloat(minValue) < parseFloat(0.00) && toField.value.length > 0 && lcNewChar =='-'))
	{
		if(toField.value.length > 0)
		{
			llRetVal=true;
			if(llRetVal)
			{
				var newNum =lcNewChar + toField.value;
				//alert(newNum);
				var newFloatNum = parseFloat(newNum);
				//alert("New Num : "+ newNum+"New Float Num : "+newFloatNum);
				if(newFloatNum <= maxValue && newFloatNum >= minValue)
				{
					llRetVal = true;
				}
				else
				{
					llRetVal = false;
				}
				
			}
		}
		else
			llRetVal=true;
	}
	else
	{
			if(isDotPresent)
			{
				llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9') && (toField.value.length <= precision) && (currentScale <= scale)) ;
			}
			else//IF it has no dot but 0 leads
			{
				llRetVal = (((lcNewChar >= '0' && lcNewChar <= '9') || ((lcNewChar == '.') && (scale >0 ))) && (toField.value.length < precision)) ;	
			}
			
			
			if(llRetVal)
			{
				var newNum = ''+ toField.value + lcNewChar;
				//alert(newNum);
				var newFloatNum = parseFloat(newNum);
				//alert("New Num : "+ newNum+"New Float Num : "+newFloatNum);
				if(newFloatNum <= maxValue && newFloatNum >= 0) // minValue)
				{
					llRetVal = true;
				}
				else
				{
					llRetVal = false;
				}
				
			}
	}
	
	return llRetVal;
}

function TrimNumberCust(toField,scale)
{
	var newNum = ''+ toField.value;
	var newFloatNum = parseFloat(newNum);
	//alert("New Number : "+newNum+" New Float Number : "+newFloatNum);
	if(!isNaN(newFloatNum))
	{
		toField.value = newFloatNum;
		
		if(scale > 0)
		{
			var currentNum = toField.value;
			var isDotPresent = false;

			for(i = 0; i < currentNum.length; i++)
			{
				if(currentNum.charAt(i) == '.')
				{
					isDotPresent = true;
					break;
				}
			}
			
			if(isDotPresent == false)
			{
				toField.value = toField.value + ".";
				
				for(i = 0; i < scale; i++)
				{
					toField.value = toField.value + '0';
				}
			}
		
		}
		
	}
	
}

function TrimNumber(toField,scale)
{
	var newNum = ''+ toField.value;
	var newFloatNum = parseFloat(newNum);
	//alert("New Number : "+newNum+" New Float Number : "+newFloatNum);

	if(!isNaN(newFloatNum))
	{
		toField.value = newFloatNum;
		
		if(scale > 0)
		{
			var currentNum = toField.value;
			var isDotPresent = false;

			for(i = 0; i < currentNum.length; i++)
			{
				if(currentNum.charAt(i) == '.')
				{
					isDotPresent = true;
					break;
				}
			}
			
			if(isDotPresent == false)
			{
				toField.value = toField.value + ".";
				
				for(i = 0; i < scale; i++)
				{
					toField.value = toField.value + '0';
				}
			}
		
		}
		
	}
	
}



function UpperMask( toField )
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = true;
	window.event.keyCode = lcNewChar.toUpperCase().charCodeAt(0);
	return llRetVal;
}

function UpperLetterOnlyMask( toField )
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = ((lcNewChar >= 'A' && lcNewChar <= 'Z'));
	window.event.keyCode = lcNewChar.toUpperCase().charCodeAt(0);
	return llRetVal;
}

function UpperMaskSpecial( toField )
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9')||(lcNewChar >= 'A' && lcNewChar <= 'Z') || (lcNewChar >= 'a' && lcNewChar <= 'z') || (lcNewChar == '_') || (lcNewChar == '%'));
	window.event.keyCode = lcNewChar.toUpperCase().charCodeAt(0);
	return llRetVal;
}

function LowerMask( toField )
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = true;
	window.event.keyCode = lcNewChar.toLowerCase().charCodeAt(0);
	return llRetVal;
}

function LowerMaskSpecial( toField )
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9')||(lcNewChar >= 'A' && lcNewChar <= 'Z') || (lcNewChar >= 'a' && lcNewChar <= 'z') || (lcNewChar == '_') || (lcNewChar == '%'));
	window.event.keyCode = lcNewChar.toLowerCase().charCodeAt(0);
	return llRetVal;
}

function AlfaNumericMaskSpecial(toField)
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9')||(lcNewChar >= 'A' && lcNewChar <= 'Z') || (lcNewChar >= 'a' && lcNewChar <= 'z') || (lcNewChar == '_') || (lcNewChar == '%'));
	return llRetVal;
}

function AlfaNumericSpaceMaskSpecial(toField)
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9')||(lcNewChar >= 'A' && lcNewChar <= 'Z') || (lcNewChar >= 'a' && lcNewChar <= 'z') || (lcNewChar == ' ') || (lcNewChar == '%'));
	return llRetVal;
}

function UpperMaskSpecialWithHyphen(toField)
{
	var lcNewChar = String.fromCharCode(window.event.keyCode);
	var llRetVal = ((lcNewChar >= '0' && lcNewChar <= '9')||(lcNewChar >= 'A' && lcNewChar <= 'Z') || (lcNewChar >= 'a' && lcNewChar <= 'z') || (lcNewChar == '-'));
	window.event.keyCode = lcNewChar.toUpperCase().charCodeAt(0);
	return llRetVal;
}

function round (theControl, howManyDigits) 
	{
		var n=theControl.value;
		n = n - 0;
		howManyDigits = howManyDigits || 2;
		var f = Math.pow(10, howManyDigits);
		n = Math.round(n * f) / f;
		n += Math.pow(10, - (howManyDigits + 1));
		n += '';
		if (howManyDigits == 0)
		{
			theControl.value=n.substring(0, n.indexOf('.')) 
		}
		else
		{
			theControl.value=n.substring(0, n.indexOf('.') + howManyDigits + 1);
		}
}

//  End -->
</SCRIPT>