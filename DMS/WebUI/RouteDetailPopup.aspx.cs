using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for RouteDetailPopup.
	/// </summary>
	public class RouteDetailPopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected System.Web.UI.WebControls.Label lblDeprtDate;
		protected com.common.util.msTextBox txtRouteCode;
		protected com.common.util.msTextBox txtDeprtDate;
		protected System.Web.UI.HtmlControls.HtmlTable ShipmentUpdMainTable;
		protected System.Web.UI.WebControls.DataGrid dgRoute;
		String strRouteType = null;
		DataSet m_dsShpmntDtlRoute = null;
		String strPathCode = null;
		String sFlightVehicleID=null;
	//	Utility utility = null;
		String appID = null;
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.WebControls.Button btnFind;
		String enterpriseID = null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			strRouteType = Request.Params["ROUTETYPE"];
			strPathCode = Request.Params["PATHCODE"];
			sFlightVehicleID=Request.Params["FLIGHT_VEHICLE_ID"];

		//	utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();

			if(!Page.IsPostBack)
			{
				ResetScreen();
			}
			else
			{
				m_dsShpmntDtlRoute = (DataSet)Session["SESSION_DS2"];
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		public void BindGrid()
		{
			dgRoute.DataSource = m_dsShpmntDtlRoute;
			dgRoute.DataBind();
			Session["SESSION_DS2"] = m_dsShpmntDtlRoute;
		}
		private void ResetScreen()
		{
			txtDeprtDate.Text = "";
			txtRouteCode.Text = "";
			if((strRouteType != null) && strRouteType.Equals("Air"))
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					dgRoute.Columns[3].HeaderText = Utility.GetLanguageText(ResourceType.ScreenLabel, "Flight No", utility.GetUserCulture());
					dgRoute.Columns[4].HeaderText = Utility.GetLanguageText(ResourceType.ScreenLabel, "Air way Bill No", utility.GetUserCulture());
					lblRouteCode.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Flight No", utility.GetUserCulture());					
				}
				else
				{
					dgRoute.Columns[3].HeaderText = "Flight No";
					dgRoute.Columns[4].HeaderText = "Air way Bill No";
					lblRouteCode.Text = "Flight No";
				}
			}
			else if((strRouteType != null) && (!strRouteType.Equals("Air")))
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					dgRoute.Columns[3].HeaderText = Utility.GetLanguageText(ResourceType.ScreenLabel, "Vehicle No", utility.GetUserCulture());
					dgRoute.Columns[4].HeaderText = Utility.GetLanguageText(ResourceType.ScreenLabel, "Driver Name", utility.GetUserCulture());
					lblRouteCode.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Route Code", utility.GetUserCulture());					
				}
				else
				{
					dgRoute.Columns[3].HeaderText = "Vehicle No";
					dgRoute.Columns[4].HeaderText = "Driver Name";
					lblRouteCode.Text = "Route Code";
				}
			}
			
			m_dsShpmntDtlRoute = ShipmentUpdateMgrDAL.GetEmptyRouteDetailDS();
			
			BindGrid();
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnFind_Click(object sender, System.EventArgs e)
		{
			DateTime dtDeparture =  new DateTime(1111,11,11);
			
			if(txtDeprtDate.Text.Trim().Length > 0)
				dtDeparture = System.DateTime.ParseExact(txtDeprtDate.Text.Trim(),"dd/MM/yyyy",null);
			
			DataSet dsDetail = null;
			try
			{
				 dsDetail = ShipmentUpdateMgrDAL.GetDlvryManifstDtl(appID,enterpriseID,dtDeparture,txtRouteCode.Text.Trim(),strPathCode);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("duplicate key") != -1 )
				{
					strMsg = "Duplicate Key cannot save the record";
				}
				else if(strMsg.IndexOf("FK") != -1 )
				{
					strMsg = "Foreign Key Error cannot save the record";
				}
				lblErrorMsg.Text = strMsg;
				
			}

			m_dsShpmntDtlRoute = ShipmentUpdateMgrDAL.GetEmptyRouteDetailDS();
			if(dsDetail != null)
			{
				int iTot = dsDetail.Tables[0].Rows.Count;
				for(int i = 0; i < iTot; i++)
				{
					ShipmentUpdateMgrDAL.AddNewRowInRouteDetailDS(m_dsShpmntDtlRoute);
					
					DataRow drDetail = dsDetail.Tables[0].Rows[i];
					DataRow drShpmntDtlRoute = m_dsShpmntDtlRoute.Tables[0].Rows[i];
					
					drShpmntDtlRoute["route_code"] = drDetail["route_code"];
					drShpmntDtlRoute["departure_date"] =  drDetail["delivery_manifest_datetime"];
					drShpmntDtlRoute["flight_vehicle_no"] = drDetail["flight_vehicle_no"];
					drShpmntDtlRoute["route_description"] = drDetail["route_description"];
					drShpmntDtlRoute["awb_driver_name"] = drDetail["awb_driver_name"];
					drShpmntDtlRoute["origin_state_code"] = drDetail["origin_state_code"];
					drShpmntDtlRoute["destination_state_code"] = drDetail["destination_state_code"];
				}
				BindGrid();
			}
			getPageControls(Page);
		}

		public void dgShipTrack_OnItemCommand(object sender, System.Web.UI.WebControls.DataGridCommandEventArgs e) 
		{
			String strRouteCode = "";
			String strRouteDesc = "";
			String strDepartDate = "";
			String strVehicleno = "";
			String strDriverName = "";
			String strOriginDesc = "";
			String strDestDesc = "";

			LinkButton lblDGRouteCode = (LinkButton)e.Item.FindControl("lblDGRouteCode");
			if(lblDGRouteCode != null)
			{
				strRouteCode = lblDGRouteCode.Text;
			}

			Label lblDescription = (Label)e.Item.FindControl("lblDescription");
			if(lblDescription != null)
			{
				strRouteDesc = lblDescription.Text;
			}

			Label lblDepartDate = (Label)e.Item.FindControl("lblDepartDate");
			if(lblDepartDate != null)
			{
				strDepartDate = lblDepartDate.Text;
			}

			Label lblVehicleNo = (Label)e.Item.FindControl("lblVehicleNo");
			if(lblVehicleNo != null)
			{
				strVehicleno = lblVehicleNo.Text;
			}

			Label lblDriverName = (Label)e.Item.FindControl("lblDriverName");
			if(lblDriverName != null)
			{
				strDriverName = lblDriverName.Text;
			}

			
			Label lblOriginStateCode = (Label)e.Item.FindControl("lblOriginStateCode");
			if(lblOriginStateCode != null)
			{
				strOriginDesc =  TIESUtility.GetStateDesc(appID,enterpriseID,lblOriginStateCode.Text);
			}

			Label lblDestinationStateCode = (Label)e.Item.FindControl("lblDestinationStateCode");
			if(lblDestinationStateCode != null)
			{
				strDestDesc =  TIESUtility.GetStateDesc(appID,enterpriseID,lblDestinationStateCode.Text);
			}

			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.opener.ShipmentUpdateRoute.txtRouteCode"+".value = \""+strRouteCode+"\";";
			sScript += "  window.opener.ShipmentUpdateRoute.txtRouteDesc"+".value = \""+strRouteDesc+"\";";
			sScript += "  window.opener.ShipmentUpdateRoute.txtDeptrDtTime"+".value = \""+strDepartDate+"\";";
			sScript += "  window.opener.ShipmentUpdateRoute."+sFlightVehicleID+".value = \""+strVehicleno+"\";";
			sScript += "  window.opener.ShipmentUpdateRoute.txtDriverName"+".value = \""+strDriverName+"\";";
			sScript += "  window.opener.ShipmentUpdateRoute.txtOrigin"+".value = \""+strOriginDesc+"\";";
			sScript += "  window.opener.ShipmentUpdateRoute.txtDestination"+".value = \""+strDestDesc+"\";";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);

		}
	}
}
