using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using TIESDAL;

namespace TIES.WebUI
{
	public class CustomsReport_BondStoreAging : BasePage
	{
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.DropDownList ddlBondStore;
		protected com.common.util.msTextBox txtAsOfDate;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.WebControls.Label lbError;
		protected System.Web.UI.WebControls.Label Label5;

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion	
		
		private string appID = string.Empty;
		private string enterpriseID = string.Empty;
		private string userLoggin = string.Empty;

		private void DisplayddlExchangeRateType()
		{
			ReportBondStoreAgingDAL db = new ReportBondStoreAgingDAL(appID, enterpriseID);
			
			ddlBondStore.DataSource = db.GetBondedWarehouses(this.enterpriseID).Tables[0];
			ddlBondStore.DataTextField = "DropDownListDisplayValue";
			ddlBondStore.DataValueField = "CodedValue";
			ddlBondStore.DataBind();
		}
		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				System.Text.StringBuilder s = new System.Text.StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings, Page.Session);
				appID = utility.GetAppID();
				enterpriseID = utility.GetEnterpriseID();
				userLoggin = utility.GetUserID();

				if(!IsPostBack)
				{
					DisplayddlExchangeRateType();
					txtAsOfDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
					SetInitialFocus(txtAsOfDate);
				}
			}
			catch(Exception ex)
			{
				this.lbError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lbError.Text = string.Empty;
			string asOfDate = string.Empty;

			if(!txtAsOfDate.Text.Equals(""))
				asOfDate = DateTime.ParseExact(txtAsOfDate.Text.Trim(), "dd/MM/yyyy", null).ToString("yyyy-MM-dd");
			else
			{
				this.lbError.Text = "As of Date and Bond Store are required";
				return;
			}

			DataTable dtParam = new DataTable();
			DataRow row = dtParam.NewRow();

			dtParam.Columns.Add("param1");
			dtParam.Columns.Add("param2");
			dtParam.Columns.Add("param3");

			row["param1"] = this.enterpriseID;
			row["param2"] = asOfDate;
			row["param3"] = ddlBondStore.SelectedValue;

			dtParam.Rows.Add(row);
			
			//Set and call report
			String strUrl = "ReportViewer.aspx";
			Session["FORMID"] = "BondStoreAging";
			Session["RptParam"] = dtParam;
				
			ArrayList paramList = new ArrayList();
				
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindowReport.js", paramList);
			//String sScript = "<script language=javascript>openPopup('" + strUrl + "');</script>"; 
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			this.lbError.Text = string.Empty;
			this.txtAsOfDate.Text = string.Empty;
			this.ddlBondStore.SelectedIndex = 0;
			txtAsOfDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
			SetInitialFocus(txtAsOfDate);
		}

		private void Button1_Click(object sender, System.EventArgs e)
		{
		
		}
	}
}
