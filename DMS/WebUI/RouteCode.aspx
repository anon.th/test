<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Page language="c#" Debug="true" Codebehind="RouteCode.aspx.cs" AutoEventWireup="false" Inherits="com.ties.RouteCode" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>RouteCode</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="RouteCode" method="post" runat="server">
			<asp:label id="lblHeading" style="Z-INDEX: 101; LEFT: 54px; POSITION: absolute; TOP: 15px" runat="server" CssClass="maintitleSize" Width="168px">Route Code</asp:label>
			<asp:ValidationSummary id="Pagevalid" style="Z-INDEX: 106; LEFT: 359px; POSITION: absolute; TOP: 4px" runat="server" ShowMessageBox="True" ShowSummary="False" HeaderText="Please enter the missing fields."></asp:ValidationSummary><asp:button id="btnInsertRow" style="Z-INDEX: 106; LEFT: 262px; POSITION: absolute; TOP: 53px" runat="server" Height="20px" Text="Insert" CausesValidation="False" CssClass="queryButton" Width="52px"></asp:button><asp:button id="btnExecuteQry" style="Z-INDEX: 103; LEFT: 132px; POSITION: absolute; TOP: 53px" runat="server" Text="ExecuteQuery" CausesValidation="False" CssClass="queryButton" Height="20"></asp:button><asp:button id="btnQuery" style="Z-INDEX: 102; LEFT: 54px; POSITION: absolute; TOP: 53px" runat="server" Text="Query" CausesValidation="False" CssClass="queryButton" Height="20" Width="77px"></asp:button><asp:label id="lblErrormsg" style="Z-INDEX: 104; LEFT: 55px; POSITION: absolute; TOP: 77px" runat="server" CssClass="errorMsgColor" Width="435px">Errormessage</asp:label>
			<asp:datagrid id="dgRouteCode" AllowCustomPaging="True" AllowPaging="True" AutoGenerateColumns="False" style="Z-INDEX: 105; LEFT: 56px; POSITION: absolute; TOP: 137px" runat="server" Width="517px" OnItemDataBound="dgRouteCode_ItemDataBound" OnUpdateCommand="dgRouteCode_Update" OnCancelCommand="dgRouteCode_Cancel" OnEditCommand="dgRouteCode_Edit" OnPageIndexChanged="dgRouteCode_PageChange" OnDeleteCommand="dgRouteCode_Delete">
				<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
				<ItemStyle CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle HorizontalAlign="Center" Width="0.5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
						<HeaderStyle HorizontalAlign="Center" Width="1%" CssClass="gridheading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Path Code" HeaderStyle-Font-Bold="True">
						<HeaderStyle HorizontalAlign="Center" Width="2%" CssClass="gridheading"></HeaderStyle>
						<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
						<ItemTemplate>
							<asp:Label Width="130px" CssClass="gridLabel" ID="lblPathCode" Text='<%#DataBinder.Eval(Container.DataItem,"path_code")%>' Runat ="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:DropDownList Width="130px" ID="ddPathCode" CssClass="gridTextBox" Runat="server"></asp:DropDownList>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Route Code" HeaderStyle-Font-Bold="True">
						<HeaderStyle Wrap="False" HorizontalAlign="Left" Width="2%" CssClass="gridheading"></HeaderStyle>
						<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
						<ItemTemplate>
							<asp:Label Width="130px" CssClass="gridLabel" ID="lblRouteCode" Runat ="server" Text='<%#DataBinder.Eval(Container.DataItem,"route_code")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox Width="130px" CssClass="gridTextBox" ID="txtRouteCode" Runat ="server" Text='<%#DataBinder.Eval(Container.DataItem,"route_code")%>' MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="rcValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtRouteCode" ErrorMessage="Route Code "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Route Description">
						<HeaderStyle Wrap="False" HorizontalAlign="Left" Width="5%" CssClass="gridheading"></HeaderStyle>
						<ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
						<ItemTemplate>
							<asp:Label Width="250px" ID="lblRouteDescp" CssClass="gridlabel" Runat ="server" Text='<%#DataBinder.Eval(Container.DataItem,"route_description")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox Width="250px" CssClass="gridTextBox" ID="txtRouteDescp" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"route_description")%>' MaxLength="200">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid></form>
	</body>
</HTML>