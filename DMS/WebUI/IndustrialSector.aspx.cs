using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.common.applicationpages;
namespace com.ties
{
	/// <summary>
	/// Summary description for IndustrialSector.
	/// </summary>
	public class IndustrialSector : BasePage
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		
		protected System.Web.UI.WebControls.DataGrid dgISector;
		private SessionDS m_sdsISector;
		private String m_strAppID;
		protected System.Web.UI.WebControls.Label lblValISector;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		private String m_strEnterpriseID;
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		//private bool toRemoveBlankRow = false;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();

			if(!Page.IsPostBack)
			{
				ViewState["m_strCode"] = "";
				ViewState["index"] = 0;
				ViewState["prevRow"] = 0;

				ViewState["SCMode"] = ScreenMode.None;
				ViewState["SCOperation"] = Operation.None;
				
				dgISector.EditItemIndex = -1;
				m_sdsISector = SysDataManager1.GetEmptyISector(1);
				enableEditColumn(false);
				BindISector();
				

				//-----START QUERYING------
				lblValISector.Text = "";
				m_sdsISector.ds.Tables[0].Rows.Clear();
				dgISector.CurrentPageIndex = 0;
				btnExecuteQuery.Enabled = true;
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				enableEditColumn(false);
				ViewState["SCMode"] = ScreenMode.Query;
				ViewState["SCOperation"] = Operation.None;
				ViewState["m_strCode"] = "";
				AddRow();
				ViewState["prevRow"]=0;
				ViewState["index"]=0;
				dgISector.EditItemIndex = 0;
				BindISector();

				Session["SESSION_DS1"] = m_sdsISector;
				Session["QUERY_DS"] = m_sdsISector;
			}
			else
			{
				m_sdsISector = (SessionDS)Session["SESSION_DS1"];
			}
			PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}
		/// <summary>
		/// To bind data to datagrid
		/// </summary>
		private void BindISector()
		{
			//lblValISector.Text ="";
			dgISector.VirtualItemCount = System.Convert.ToInt32(m_sdsISector.QueryResultMaxSize);
			dgISector.DataSource = m_sdsISector.ds;
			dgISector.DataBind();
			Session["SESSION_DS1"] = m_sdsISector;
		}
		/// <summary>
		/// On editing allows text input
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnEdit_ISector(object sender, DataGridCommandEventArgs e)
		{		
			lblValISector.Text ="";
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			int rowIndex = e.Item.ItemIndex;
			ViewState["index"] = rowIndex;
			Label lblCode = (Label)dgISector.Items[rowIndex].FindControl("lblCode");
			ViewState["m_strCode"] = lblCode.Text;
			if((int)ViewState["SCOperation"] == (int)Operation.Insert)
			{
				ViewState["SCMode"]= ScreenMode.Insert;
			}
			else
			{
				ViewState["SCMode"] = ScreenMode.None;

			}
			if(dgISector.EditItemIndex >0)
			{
				msTextBox txtCode = (msTextBox)dgISector.Items[dgISector.EditItemIndex].FindControl("txtCode");
				
				if(txtCode.Text!=null && txtCode.Text=="")	
				{
					ViewState["m_strCode"] = txtCode.Text;
					m_sdsISector.ds.Tables[0].Rows.RemoveAt(dgISector.EditItemIndex);
				}
			}
			dgISector.EditItemIndex = rowIndex;
			BindISector();
	
		}
		/// <summary>
		/// To return to viewing mode
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnCancel_ISector(object sender, DataGridCommandEventArgs e)
		{
			lblValISector.Text ="";
			dgISector.EditItemIndex = -1;
			m_sdsISector = (SessionDS)Session["SESSION_DS1"];
			int rowIndex = e.Item.ItemIndex;

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
			{
				m_sdsISector.ds.Tables[0].Rows.RemoveAt(rowIndex);
			}
			enableDeleteColumn(true);
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			BindISector();

		}
		/// <summary>
		/// To update where new fields entered and store to database
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnUpdate_ISector(object sender, DataGridCommandEventArgs e)
		{
			int rowIndex = e.Item.ItemIndex;
			updateLatestISector(rowIndex);

			m_sdsISector = (SessionDS)Session["SESSION_DS1"];
			DataSet dsISector = m_sdsISector.ds.GetChanges();
			SessionDS sdsISector = new SessionDS();
			sdsISector.ds = dsISector;

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
			{
				
				if( !isCodeEmpty( rowIndex ) )
				{
					try
					{
						SysDataManager1.InsertISector(sdsISector,m_strAppID, m_strEnterpriseID);
						m_sdsISector.ds.Tables[0].Rows[rowIndex].AcceptChanges();
						lblValISector.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());						
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;

						if(strMsg.IndexOf("duplicate key") != -1  || strMsg.IndexOf("unique") != -1)
						{
							lblValISector.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
						} 
						return;				
					}
					catch(Exception err)
					{
						String msg = err.ToString();
						lblValISector.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
					}					
				}	
			}
			else
			{
				try
				{
					SysDataManager1.UpdateISector(m_sdsISector,rowIndex,m_strAppID, m_strEnterpriseID);
					m_sdsISector.ds.Tables[0].Rows[rowIndex].AcceptChanges();
					lblValISector.Text = Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());					
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					strMsg = appException.InnerException.Message;

					if(strMsg.IndexOf("duplicate key") != -1  || strMsg.IndexOf("unique") != -1)
					{
						lblValISector.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
					} 
					return;				
				}
				catch(Exception err)
				{
					String msg = err.ToString();
					lblValISector.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
				}				
			}
			
			ViewState["SCOperation"] = Operation.None;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			dgISector.EditItemIndex = -1;
			BindISector();
		}
		/// <summary>
		/// to delete certain row
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnDelete_ISector(object sender, DataGridCommandEventArgs e)
		{
			//*Raj*//
			if (dgISector.EditItemIndex == e.Item.ItemIndex)
			{
				return;
			}
			//*Raj*//
			ViewState["SCOperation"] = Operation.Delete;
			int rowNum = e.Item.ItemIndex;
			if(checkEmptyFields(rowNum))
			{
				if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
				{
					DeleteISector(rowNum);
				}
				else
				{
					DeleteISector(rowNum);
					showCurrentPage();
				}
				if(dgISector.EditItemIndex <-1)
				{
					//-original m_sdsISector.ds.Tables[0].Rows.RemoveAt(dgISector.EditItemIndex-1);
					//*Raj*//
					m_sdsISector.ds.Tables[0].Rows.RemoveAt(dgISector.EditItemIndex);
					//*Raj*//
				}	
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				BindISector();
			}
			
		}
		/// <summary>
		/// to delete row
		/// </summary>
		/// <param name="rowIndex">to delete at this row index</param>
		private void DeleteISector(int rowIndex)
		{
			lblValISector.Text = "";
			//m_sdsISector = (SessionDS)Session["SESSION_DS1"];
			DataRow dr = m_sdsISector.ds.Tables[0].Rows[rowIndex];
			String strCode = (String)dr[0];
			try
			{
				SysDataManager1.DeleteISector(m_strAppID,m_strEnterpriseID,strCode);
				m_sdsISector.ds.Tables[0].Rows.RemoveAt(rowIndex);
				lblValISector.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1 || strMsg.IndexOf("REFERENCE") != -1)
				{
					lblValISector.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());
				}
				if(strMsg.IndexOf("child record found") != -1)
				{
					lblValISector.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());
				}				

				Logger.LogTraceError("ZipCode.aspx.cs","dgZipCode_Delete","RBAC003",appException.Message.ToString());
				//lblErrorMessage.Text = appException.Message.ToString();
				return;				
			}
		}
		/// <summary>
		/// to show code column or not
		/// </summary>
		/// <param name="toEnable">true to enable , false to disable</param>
		private void enableCodeColumn(bool toEnable)
		{
			dgISector.Columns[2].Visible=toEnable;//Code column
		}
		/// <summary>
		/// to show delete column or not
		/// </summary>
		/// <param name="toEnable">true to enable, false to disable</param>
		private void enableDeleteColumn(bool toEnable)
		{
			dgISector.Columns[1].Visible=toEnable;
		}
		/// <summary>
		/// to show edit column or not
		/// </summary>
		/// <param name="toEnable">true to enable, false to disable</param>
		private void enableEditColumn(bool toEnable)
		{
			dgISector.Columns[0].Visible=toEnable;
			dgISector.Columns[1].Visible=toEnable;
		}
		
		private void AddRow()
		{
			SysDataManager1.AddNewRowInIndSectorDS(ref m_sdsISector);
			BindISector();
		}

		/// <summary>
		/// To add a new row
		/// </summary>
//		private void AddRow()
//		{
//			m_sdsISector = (SessionDS)Session["SESSION_DS1"];
//			DataRow drNew = m_sdsISector.ds.Tables[0].NewRow();
//
//			drNew[0] = "";
//			drNew[1] = "";
//			
//			m_sdsISector.ds.Tables[0].Rows.Add(drNew);
//			
//		}
		/// <summary>
		/// to get text out from textboxes and store it to a dataset
		/// </summary>
		/// <param name="rowIndex">to get at this row index</param>
		private void updateLatestISector(int rowIndex)
		{
			msTextBox txtCode = (msTextBox)dgISector.Items[rowIndex].FindControl("txtCode");
			TextBox txtDescription = (TextBox)dgISector.Items[rowIndex].FindControl("txtDescription");
			DataRow dr = m_sdsISector.ds.Tables[0].Rows[rowIndex];

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
			{
				dr[0] = txtCode.Text;
			}
			else
			{
				dr[0] = (String)ViewState["m_strCode"];
			}
			String strDesc = txtDescription.Text;
			dr[1] = strDesc;
			
			Session["SESSION_DS1"] = m_sdsISector;
		}
		/// <summary>
		/// to check if fields are empty
		/// </summary>
		/// <param name="rowIndex">to check at this row index</param>
		/// <returns>return true or false</returns>
		private bool checkEmptyFields(int rowIndex)
		{
			msTextBox txtCode = (msTextBox)dgISector.Items[rowIndex].FindControl("txtCode");
			TextBox txtDescription = (TextBox)dgISector.Items[rowIndex].FindControl("txtDescription");
			if(txtCode!=null && txtDescription!=null)
			{
				DataRow dr = m_sdsISector.ds.Tables[0].Rows[rowIndex];
				if((int)ViewState["SCMode"] == (int)ScreenMode.Query)
				
					dr[0] = txtCode.Text;
				else
					dr[0] = (String)ViewState["m_strCode"];

				dr[1] = txtDescription.Text;

				Session["SESSION_DS1"] = m_sdsISector;
				Session["QUERY_DS"] = m_sdsISector;
				return false;
			}
			return true;
		}
		/// <summary>
		/// To query from databse and display results
		/// </summary>
		private void QueryISector()
		{
			checkEmptyFields((int)ViewState["index"]);

			m_sdsISector = SysDataManager1.QueryISector(m_sdsISector,m_strAppID,m_strEnterpriseID, 0 ,dgISector.PageSize);		
			dgISector.VirtualItemCount = System.Convert.ToInt32(m_sdsISector.QueryResultMaxSize);
			if(m_sdsISector.QueryResultMaxSize<1)
			{
				lblValISector.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			}
			Session["SESSION_DS1"] = m_sdsISector;

		}
		/// <summary>
		/// To check if code text is empty and do validation
		/// </summary>
		/// <param name="rowIndex"></param>
		/// <returns></returns>
		private bool isCodeEmpty(int rowIndex)
		{
			msTextBox txtCode = (msTextBox)dgISector.Items[rowIndex].FindControl("txtCode");
			if(txtCode!=null)
			{
				DataRow dr = m_sdsISector.ds.Tables[0].Rows[rowIndex];
				if(txtCode.Text!="")
				{
					if(txtCode.Text.IndexOf(" ",0)>0)
					{
						lblValISector.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CDE_NO_SPACE",utility.GetUserCulture());
						return true;
					}
					return false;
				}
			}
			lblValISector.Text =Utility.GetLanguageText(ResourceType.UserMessage,"CDE_COLUMN_REQ",utility.GetUserCulture());
			ViewState["SCOperation"] = Operation.Insert;
			return true;
		}
		/// <summary>
		/// Insert button event handler, where it enables and disables 
		/// certain wweb form components
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			lblValISector.Text = "";
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,false,m_moduleAccessRights);
			btnExecuteQuery.Enabled = false;
			dgISector.CurrentPageIndex = 0;
			if((int)ViewState["SCMode"] != (int)ScreenMode.Insert || m_sdsISector.ds.Tables[0].Rows.Count >= dgISector.PageSize)
			{
				m_sdsISector = SysDataManager1.GetEmptyISector(1);
				dgISector.EditItemIndex = m_sdsISector.ds.Tables[0].Rows.Count-1;
			}
			else
			{
				AddRow();
				dgISector.EditItemIndex = m_sdsISector.ds.Tables[0].Rows.Count-1;
			}
			//dgISector.VirtualItemCount = m_sdsISector.ds.Tables[0].Rows.Count;

			ViewState["SCMode"] = ScreenMode.Insert;
			enableEditColumn(true);		
			BindISector();
			getPageControls(Page);
		}

		/// <summary>
		/// Query event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblValISector.Text = "";
			m_sdsISector.ds.Tables[0].Rows.Clear();
			dgISector.CurrentPageIndex = 0;
			btnExecuteQuery.Enabled = true;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			enableEditColumn(false);
			ViewState["SCMode"] = ScreenMode.Query;
			ViewState["SCOperation"] = Operation.None;
			ViewState["m_strCode"] = "";
			AddRow();
			ViewState["prevRow"]=0;
			ViewState["index"]=0;
			dgISector.EditItemIndex = 0;
			BindISector();
		}

		/// <summary>
		/// Execute event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			dgISector.EditItemIndex = -1;
			QueryISector();
			ViewState["toQuery"] = false;
			btnExecuteQuery.Enabled = false;
			enableEditColumn(true);
			enableDeleteColumn(true);
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			BindISector();
		}
		/// <summary>
		/// on page change event handler where data is refresh
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnISector_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgISector.CurrentPageIndex = e.NewPageIndex;
			showCurrentPage();
			dgISector.SelectedIndex = -1;
			dgISector.EditItemIndex = -1;
			BindISector();
		}
		/// <summary>
		/// to refresh data
		/// </summary>
		private void showCurrentPage()
		{
			SessionDS m_sdsQueryISector = (SessionDS)Session["QUERY_DS"];
			int iStartIndex = dgISector.CurrentPageIndex * dgISector.PageSize;
			
			m_sdsISector = SysDataManager1.QueryISector(m_sdsQueryISector,m_strAppID,m_strEnterpriseID, iStartIndex,dgISector.PageSize);
			int pgCnt = (Convert.ToInt32( m_sdsISector.QueryResultMaxSize - 1))/dgISector.PageSize;
			if(pgCnt < dgISector.CurrentPageIndex)
			{
				dgISector.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			Session["SESSION_DS1"] = m_sdsISector;
			
		}
		/// <summary>
		/// on item bound event handler where is checks if code column should
		/// be enables or not
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnItemBound_ISector(object sender, DataGridItemEventArgs e)
		{
			msTextBox txtCode = (msTextBox)e.Item.FindControl("txtCode");
			if(txtCode!=null && ( (int)ViewState["SCMode"] != (int)ScreenMode.Insert && (int)ViewState["SCMode"] != (int)ScreenMode.Query) )
			{
				txtCode.Enabled = false;
			}

		}
		
	}
}
