using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using System.Text;
using System.Configuration;
using System.Data.OleDb;

namespace com.ties
{
	/// <summary>
	/// Summary description for ESA_Sectors.
	/// </summary>
	public class ESA_Sectors : com.common.applicationpages.BasePage
	{
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.DataGrid dgESA_Sectors;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.HtmlControls.HtmlTable xxx;

		
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if(!Page.IsPostBack)
			{
				this.lblErrorMessage.Text="";
				dsESA = SysDataMgrDAL.GetEmptyESASectors(1);
				Session["dsESA"] = dsESA;
				this.dgESA_Sectors.EditItemIndex = 0;
				BindGrid();
				dgESA_Sectors.Columns[0].Visible = false;  
				dgESA_Sectors.Columns[1].Visible = false;  

			}
		}
		public bool ckEffectiveDate(DateTime effDate)
		{
			DateTime effStart = System.DateTime.Now;
			ViewState["effStart"] = effStart;
			DateTime effEnd = System.DateTime.Now;
			bool ckDate = false;

			DataSet effDS = new DataSet();
			effDS = SysDataMgrDAL.GetEndRoundEffectiveDate(utility.GetAppID(),utility.GetEnterpriseID(),effStart.ToString("dd/MM/yyyy"));
			DataTable effDT = new DataTable();
			effDT = effDS.Tables[0];
			foreach(DataRow dr in effDT.Rows)
			{
				effEnd = Convert.ToDateTime(dr["endEffDate"].ToString());
				ViewState["effEnd"] = effEnd;
			}

			string getdate = "";
			if(effDate.Day.ToString().Length == 1)
				getdate = "0"+effDate.Day.ToString();
			else
				getdate = effDate.Day.ToString();

			if(effDate.Month.ToString().Length == 1)
				getdate += "/0"+effDate.Month.ToString()+"/"+effDate.Year.ToString();
			else
				getdate += "/"+effDate.Month.ToString()+"/"+effDate.Year.ToString();

			//DateTime dt = DateTime.ParseExact(getdate,"dd/MM/yyyy",null);
			DateTime dt = Convert.ToDateTime(getdate);
//			Response.Write(dt+","+effStart+","+effEnd);

			if(dt >= effStart && dt <= effEnd)
			{
				ckDate = true;
			}
			else
			{
				ckDate = false;
			}

			return ckDate;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		DataSet dsESA = null;

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			this.lblErrorMessage.Text = "";
			dsESA = SysDataMgrDAL.GetEmptyESASectors(1);
			Session["dsESA"] = dsESA;
			this.dgESA_Sectors.EditItemIndex = 0;
			BindGrid();

			this.btnExecuteQuery.Enabled = true;
			this.btnInsert.Enabled = true;

			dgESA_Sectors.Columns[0].Visible = false;  
			dgESA_Sectors.Columns[1].Visible = false;  
		}
		
		private void BindGrid()
		{
			dsESA = (DataSet)Session["dsESA"];
			dgESA_Sectors.DataSource = dsESA;
			dgESA_Sectors.DataBind(); 
		}

		public void dgESA_Sectors_Update(object sender, DataGridCommandEventArgs e)
		{
			String sZipCode = "";
			String sZoneCode = "";
			String sEffectiveDate = "";

			if(Session["sZipcode"] != null)
			{
				sZipCode = (String)Session["sZipcode"];
			}
			if(Session["sZone"] != null)
			{
				sZoneCode = (String)Session["sZone"];
			}
			if(Session["sEffectiveDate"] != null)
			{
				sEffectiveDate = (String)Session["sEffectiveDate"];
			}
			
			lblErrorMessage.Text = "";
			int iSelIndex = e.Item.ItemIndex;
			DataGridItem dgRow = dgESA_Sectors.Items[iSelIndex];

			msTextBox txtzipcode = (msTextBox)dgRow.Cells[2].FindControl("txtzipcode");
			msTextBox txtzone = (msTextBox)dgRow.Cells[4].FindControl("txtzone");
			msTextBox txtEffectiveDate = (msTextBox)dgRow.Cells[6].FindControl("txtEffectiveDate");

			//Add by Panas
			//Check ZipCode (in Zipcode Table)
			DataSet dsZipcode = new DataSet();
			dsZipcode = SysDataMgrDAL.Get_CheckZipCode(utility.GetAppID(), utility.GetEnterpriseID(),txtzipcode.Text);

			if(dsZipcode.Tables[0].Rows.Count <= 0)
			{
				this.lblErrorMessage.Text = "Incorrect ZipCode.";
				return;
			}
			//CheckZoneCode

			DataSet zoneCount = new DataSet();
			//zoneCount = SysDataMgrDAL.Get_CheckCustZoneCode(utility.GetAppID(), utility.GetEnterpriseID(),txtzone.Text);
			zoneCount = SysDataMgrDAL.Get_CheckESA_ZoneCode(utility.GetAppID(), utility.GetEnterpriseID(),txtzone.Text);
				
			if(zoneCount.Tables[0].Rows.Count<=0)
			{
				this.lblErrorMessage.Text = "Incorrect ZoneCode.";
				return;
			}
	
			//Assign Effective_Date (in case not assigned)
			if(txtEffectiveDate.Text=="")
			{
				this.lblErrorMessage.Text = "Incorrect Date.";
				return;
			}
			
			//end by Panas

			String effective_date = "";
			object objEffectiveDate = System.DBNull.Value;
			object percentEAS = System.DBNull.Value;
			DataSet dsESA = new DataSet();
			if(txtzipcode != null)
			{
				if(txtzipcode.Text==""||txtzone.Text==""||txtEffectiveDate.Text=="")
					return;

					try
					{
						if(txtEffectiveDate.Text.ToString().Trim() != "")
						{
							effective_date = txtEffectiveDate.Text.ToString().Trim();
							objEffectiveDate = DateTime.ParseExact(txtEffectiveDate.Text.ToString().Trim(),"dd/MM/yyyy",null);
						}
	
//						if(SysDataMgrDAL.IsESASectorExist(utility.GetAppID(),utility.GetEnterpriseID(), 
//							txtzipcode.Text.ToString(),
//							txtzone.Text.ToString(),txtEffectiveDate.Text.ToString()))
						DateTime effStart = System.DateTime.Now;
						DateTime effEnd = System.DateTime.Now.AddYears(1);
						effEnd = effEnd.AddDays(-1);
						DataSet effDS = new DataSet();
						effDS = SysDataMgrDAL.GetEndRoundEffectiveDate(utility.GetAppID(),utility.GetEnterpriseID(),effStart.ToString("dd/MM/yyyy"));
						DataTable effDT = new DataTable();
						effDT = effDS.Tables[0];

//						foreach(DataRow dr in effDT.Rows)
//						{
//							//effEnd = Convert.ToDateTime(dr["endEffDate"].ToString());
//						}

						//Response.Write(effStart.Date+","+Convert.ToDateTime(objEffectiveDate).Date+","+effEnd.Date);
						if(Convert.ToDateTime(objEffectiveDate).Date < effStart.Date || Convert.ToDateTime(objEffectiveDate).Date > effEnd.Date)
						{
							this.lblErrorMessage.Text = "Effective date must between "+effStart.ToString("dd/MM/yyyy")+" and "+effEnd.ToString("dd/MM/yyyy")+"";
						}
						else
						{
							if(!sZipCode.Equals("") && !sZoneCode.Equals("") && !sEffectiveDate.Equals(""))
							{	

								if(SysDataMgrDAL.ckDupUpdate_ESA_Sector(utility.GetAppID(), utility.GetEnterpriseID(), 
									txtzipcode.Text,txtzone.Text,txtEffectiveDate.Text,
									sZipCode,sZoneCode,sEffectiveDate))
								{
									this.lblErrorMessage.Text = "Error. Duplicate Data.";
									return;
								}
								else
								{
									SysDataMgrDAL.ModifyESASector(utility.GetAppID(),utility.GetEnterpriseID(), 
										txtzipcode.Text.ToString(),
										txtzone.Text.ToString(),effective_date,
										sZipCode,sZoneCode,sEffectiveDate);
									dsESA = SysDataMgrDAL.GetESASectors(utility.GetAppID(), utility.GetEnterpriseID(), "", "", "");
									Session["dsESA"] = dsESA;

									dgESA_Sectors.EditItemIndex = -1;
									BindGrid();

									this.btnInsert.Enabled = true;
									return;
								}
							}
							else
							{
								if(SysDataMgrDAL.IsESASectorExist(utility.GetAppID(),utility.GetEnterpriseID(), 
									txtzipcode.Text.ToString(),
									txtzone.Text.ToString(),txtEffectiveDate.Text.ToString()))
								{
									this.lblErrorMessage.Text = "Error. Duplicate Data.";
									return;
								}
								else
								{
									SysDataMgrDAL.InsertESASector(utility.GetAppID(),utility.GetEnterpriseID(), 
										txtzipcode.Text.ToString(),
										txtzone.Text.ToString(),
										txtEffectiveDate.Text.ToString());

									dsESA = SysDataMgrDAL.GetESASectors(utility.GetAppID(), utility.GetEnterpriseID(), "", "", "");
									Session["dsESA"] = dsESA;

									dgESA_Sectors.EditItemIndex = -1;
									BindGrid();

									this.btnInsert.Enabled = true;
									this.lblErrorMessage.Text = "1 row(s) inserted.";
								}
							}	
						}
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
//						strMsg = appException.InnerException.Message;
//						strMsg = appException.InnerException.InnerException.Message;
//		
						if(strMsg.IndexOf("FOREIGN KEY") != -1 )
						{
							lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"NO_ZONE_POSTAL",utility.GetUserCulture());
						}			
						return;
					}
			}
		}


		public void dgESA_Sectors_Delete(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			int iSelIndex = e.Item.ItemIndex;
			DataGridItem dgRow = dgESA_Sectors.Items[iSelIndex];
			Label lblzipcode = (Label)dgRow.Cells[2].FindControl("lblzipcode");
			Label lblzone = (Label)dgRow.Cells[4].FindControl("lblzone");
			Label lblEffectiveDate = (Label)dgRow.Cells[6].FindControl("lblEffectiveDate");
			msTextBox txtzipcode = (msTextBox)dgRow.Cells[2].FindControl("txtzipcode");
			msTextBox txtzone = (msTextBox)dgRow.Cells[4].FindControl("txtzone");
			msTextBox txtEffectiveDate = (msTextBox)dgRow.Cells[6].FindControl("txtEffectiveDate");

			String strzipcode = "";
			String strzone = "";
			String strEffective_date = "";

			if(lblzipcode == null)
				strzipcode = txtzipcode.Text;
			else
				strzipcode = lblzipcode.Text;

			if(lblzone == null)
				strzone = txtzone.Text;
			else
				strzone = lblzone.Text;

			if(lblEffectiveDate == null)
				strEffective_date = txtEffectiveDate.Text;
			else
				strEffective_date = lblEffectiveDate.Text;


			try
			{
				int iRowsDeleted = 0;

				iRowsDeleted = SysDataMgrDAL.DeleteESASector(utility.GetAppID(),utility.GetEnterpriseID(), 
					strzipcode,
					strzone,
					strEffective_date);

				lblErrorMessage.Text = iRowsDeleted + " row(s) deleted.";
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_FR",utility.GetUserCulture());
				}
				if(strMsg.ToLower().IndexOf("child record found") != -1 )
				{
					lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_FR",utility.GetUserCulture());	
				}			
				else
				{
					lblErrorMessage.Text = strMsg;
				}

				return;
			}

			dsESA = SysDataMgrDAL.GetESASectors(utility.GetAppID(), utility.GetEnterpriseID(), "", "", "");
			Session["dsESA"] = dsESA;
			dgESA_Sectors.EditItemIndex = -1;
			BindGrid();
		}


		public void dgESA_Sectors_Button(object sender, DataGridCommandEventArgs e)
		{
			String strCmdNm = e.CommandName;

			if(dgESA_Sectors.EditItemIndex != -1)
			{
				if(strCmdNm.Equals("zoneSearch"))
				{
					msTextBox txtzone = (msTextBox)e.Item.FindControl("txtzone");
					String strZoneCodeClientID = null;
					String strZoneCode = null;

					if(txtzone != null)
					{
						strZoneCodeClientID = txtzone.ClientID;
						strZoneCode = txtzone.Text;
					}

					String sUrl = "ZonePopup.aspx?FORMID=ESA_Sectors&ZONECODE_TEXT="+strZoneCode+
						"&ZONECODE="+strZoneCodeClientID;

					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				
				}
				else if(strCmdNm.Equals("zipcodeSearch"))
				{
					msTextBox txtzipcode	= (msTextBox)e.Item.FindControl("txtzipcode");
					String strZipcodeClientID = null;
					String strZipcode = null;
				
					if(txtzipcode != null)
					{
						strZipcodeClientID = txtzipcode.ClientID;
						strZipcode = txtzipcode.Text;
					}

					String sUrl = "ZipcodePopup.aspx?FORMID=ESA_Sectors&ZIPCODE="+strZipcode+
						"&ZIPCODE_CID="+strZipcodeClientID +
						"&ISFR=N";
	
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
			}
		}


		protected void dgESA_Sectors_PageChange(object sender, DataGridPageChangedEventArgs e)
		{

			dgESA_Sectors.CurrentPageIndex = e.NewPageIndex;
			dgESA_Sectors.EditItemIndex = -1;
			BindGrid();
		}


		protected void dgESA_Sectors_Cancel(object sender, DataGridCommandEventArgs e)
		{
				dgESA_Sectors.EditItemIndex = -1;
				dsESA = (DataSet)Session["dsESA"];
			
//				for(int i = 0; i <= dsESA.Tables[0].Rows.Count - 1; i++)
//				{
//					if(((dsESA.Tables[0].Rows[i]["postal_code"] == null) || 
//						(dsESA.Tables[0].Rows[i]["postal_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) ||
//						(dsESA.Tables[0].Rows[i]["postal_code"].ToString() == ""))
//						&&
//						((dsESA.Tables[0].Rows[i]["ESA_Sector"] == null) || 
//						(dsESA.Tables[0].Rows[i]["ESA_Sector"].GetType().Equals(System.Type.GetType("System.DBNull"))) ||
//						(dsESA.Tables[0].Rows[i]["ESA_Sector"].ToString() == "")))
//					{
//						dsESA.Tables[0].Rows[i].Delete();
//						dsESA.Tables[0].AcceptChanges();
//						i = 0;
//					}
//				}

				Session["dsESA"] = dsESA;
				BindGrid();
				this.lblErrorMessage.Text = "";

//				foreach(DataGridItem dgItem in dgESA_Sectors.Items)
//				{
//					DataSet dsZone = new DataSet();
//						
//					ImageButton imgSelect = (ImageButton)dgItem.Cells[0].Controls[1];
//					Label lblzipcode = (Label)dgItem.FindControl("lblzipcode");
//					Label lblzone = (Label)dgItem.FindControl("lblzone");
//					Label lblEffectiveDate = (Label)dgItem.FindControl("lblEffectiveDate");
//
//					dsZone = SysDataMgrDAL.GetESASectors(utility.GetAppID(), utility.GetEnterpriseID(),lblzipcode.Text, lblzone.Text, lblEffectiveDate.Text);
//
//					if(dsZone.Tables[0].Rows.Count > 0)
//					{
//						//imgSelect.ImageUrl  = "images/butt-red.gif";
//					}
//					else
//					{
//						//imgSelect.ImageUrl  = "images/butt-select.gif";
//					}
//				}
			
//			dgESA_Sectors.Columns[0].Visible = true;
//			
//			this.lblErrorMessage.Text="";
//			this.btnInsert.Enabled = false;
//			this.btnExecuteQuery.Enabled = false;
//			dgESA_Sectors.CurrentPageIndex = 0;
//
//			DataSet tmpDsESA = SysDataMgrDAL.GetEmptyESASectors(1);
//
//			dgESA_Sectors.EditItemIndex = 0;
//			dgESA_Sectors.DataSource = tmpDsESA;
//			dgESA_Sectors.DataBind();
//
//			dgESA_Sectors.Columns[0].Visible = true;  
//			dgESA_Sectors.Columns[1].Visible = true;  
//			dgESA_Sectors.Columns[2].Visible = true; 
		}


		protected void dgESA_Sectors_Edit(object sender, DataGridCommandEventArgs e)
		{

			dgESA_Sectors.EditItemIndex = e.Item.ItemIndex;
			BindGrid();

			msTextBox txtzipcode = (msTextBox)dgESA_Sectors.Items[e.Item.ItemIndex].Cells[2].FindControl("txtzipcode");
			if(txtzipcode != null)
			{	
				//txtzipcode.Enabled = false;
				Session["sZipcode"] = txtzipcode.Text;
				txtzipcode.Attributes.Add("onclick","return false;");
				txtzipcode.Attributes.Add("onpaste","return false;");
				txtzipcode.Attributes.Add("onkeypress","return false;");
				txtzipcode.Attributes.Add("onkeydown","return false;");
			}

			msTextBox txtzone = (msTextBox)dgESA_Sectors.Items[e.Item.ItemIndex].Cells[4].FindControl("txtzone");
			if(txtzone != null)
			{
				//txtzone.Enabled = false;
				Session["sZone"] = txtzone.Text;
				txtzone.Attributes.Add("onclick","return false;");
				txtzone.Attributes.Add("onpaste","return false;");
				txtzone.Attributes.Add("onkeypress","return false;");
				txtzone.Attributes.Add("onkeydown","return false;");
			}

			msTextBox txtEffectiveDate = (msTextBox)dgESA_Sectors.Items[e.Item.ItemIndex].Cells[6].FindControl("txtEffectiveDate");
			if(txtEffectiveDate != null)
			{
				Session["sEffectiveDate"] = txtEffectiveDate.Text;
			}

			dgESA_Sectors.Columns[0].Visible = true;
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			if(dgESA_Sectors.EditItemIndex==-1)
				return;
			DataGridItem dgRow = dgESA_Sectors.Items[dgESA_Sectors.EditItemIndex];
			String strZipcode = "";
			String strZone = "";
			String strEffective_date = "";

			msTextBox txtzipcode = (msTextBox)dgRow.Cells[2].FindControl("txtzipcode");
			if(txtzipcode != null)
			{
				strZipcode = txtzipcode.Text.Trim();
			}

			msTextBox txtzone = (msTextBox)dgRow.Cells[4].FindControl("txtzone");
			if(txtzone != null)
			{
				strZone = txtzone.Text.Trim();
			}

			msTextBox txtEffectivedate = (msTextBox)dgRow.Cells[6].FindControl("txtEffectivedate");
			if(txtEffectivedate != null)
			{
				if(txtEffectivedate.Text.Trim() != "")
					strEffective_date = Convert.ToString(txtEffectivedate.Text.Trim());
				else
					strEffective_date = "";
			}
			dsESA = SysDataMgrDAL.GetESASectors(utility.GetAppID(), utility.GetEnterpriseID(),
				strZipcode, strZone, strEffective_date);
			Session["dsESA"] = dsESA;
			dgESA_Sectors.EditItemIndex = -1;
			BindGrid();
			this.btnExecuteQuery.Enabled = false;

			dgESA_Sectors.Columns[0].Visible = true;  
			dgESA_Sectors.Columns[1].Visible = true;  
			dgESA_Sectors.Columns[2].Visible = true; 

		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			this.lblErrorMessage.Text = "";
			this.btnInsert.Enabled = false;
			this.btnExecuteQuery.Enabled = false;
			dgESA_Sectors.CurrentPageIndex = 0;

			DataSet tmpDsESA = SysDataMgrDAL.GetEmptyESASectors(1);

			dgESA_Sectors.EditItemIndex = 0;
			dgESA_Sectors.DataSource = tmpDsESA;
			dgESA_Sectors.DataBind();

			dgESA_Sectors.Columns[0].Visible = true;  
			dgESA_Sectors.Columns[1].Visible = true;  
			dgESA_Sectors.Columns[2].Visible = true; 

		}

	}
}
