<%@ Page language="c#" Codebehind="DispatchRecordDeletion.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.DispatchRecordDeletion" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DispatchRecordDeletion</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onunload="window.opener.opener.top.displayBanner.fnCloseAll(2);">
		<form id="DispatchRecordDeletion" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 185px" cellSpacing="1"
				cellPadding="1" width="300" border="0">
				<TR>
					<TD>
						<asp:datagrid id="m_dgDispatchDelete" runat="server" OnItemDataBound="OnDataBound_DispatchDeletion"
							AutoGenerateColumns="False" Width="768px" OnDeleteCommand="OnDelete_DispatchHistory" AllowCustomPaging="True"
							AllowPaging="True" OnPageIndexChanged="OnDispatchDelete_PageChange" PageSize="5">
							<Columns>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton"
									CommandName="Delete">
									<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Delete">
									<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:CheckBox ID="chkboxDeleted" Enabled="False" Runat="server"></asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Status Date/Time">
									<HeaderStyle Width="20%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblStatusDateTime" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"tracking_datetime","{0:dd/MM/yyyy HH:mm}")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Status Code">
									<HeaderStyle Width="8%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblStatus" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"status_code")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Exception Code">
									<HeaderStyle Width="8%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblException" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"exception_code")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Person In-Charge">
									<HeaderStyle Width="8%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblPerson" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"person_incharge")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Location">
									<HeaderStyle Width="8%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblLocation" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"location")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Remarks">
									<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblRemarks" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"remark")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Remarks Delete">
									<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblDeleteRemarks" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"delete_remark")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Last userid">
									<HeaderStyle Width="7%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblLastUserID" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"last_userid")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD align="middle"><asp:button id="btnClose" runat="server" Width="70px" Text="Close" CssClass="queryButton"></asp:button></TD>
				</TR>
			</TABLE>
			<TABLE id="Table2" style="Z-INDEX: 102; LEFT: 12px; WIDTH: 656px; POSITION: absolute; TOP: 13px; HEIGHT: 137px"
				cellSpacing="1" cellPadding="1" width="656" border="0">
				<TR>
					<TD style="WIDTH: 211px"><asp:label id="lblBookNo" runat="server" Width="142px" CssClass="tableLabel">Booking No</asp:label></TD>
					<TD><asp:label id="lblDisBookNo" runat="server" Width="243px" CssClass="tableField"></asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 211px"><asp:label id="lblBookDateTime" runat="server" Width="143px" CssClass="tableLabel">Booking Date/time</asp:label></TD>
					<TD><asp:label id="lblDisBookDateTime" runat="server" Width="240px" CssClass="tableField"></asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 211px"><asp:label id="lblSenderName" runat="server" Width="145px" CssClass="tableLabel">Sender Name</asp:label></TD>
					<TD><asp:label id="lblDisSenderName" runat="server" Width="240px" CssClass="tableField"></asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 211px"><asp:label id="lblPickupDateTime" runat="server" Width="205px" CssClass="tableLabel">Requested Pickup Date/Time</asp:label></TD>
					<TD><asp:label id="lblDisReqPickupDateTime" runat="server" Width="236px" CssClass="tableField"></asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 211px">
						<asp:Label id="lblActPickupDateTime" runat="server" CssClass="tableLabel">Actual Pickup Date/time</asp:Label></TD>
					<TD>
						<asp:Label id="lblDisActPickupDateTime" runat="server" CssClass="tableField"></asp:Label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 211px"><FONT face="Tahoma">
							<asp:Label id="lblOriginDC" runat="server" CssClass="tableLabel">Origin DC</asp:Label></FONT></TD>
					<TD><FONT face="Tahoma">
							<asp:Label id="lblDisOriginDC" runat="server" CssClass="tableField"></asp:Label></FONT></TD>
				</TR>
			</TABLE>
			<table>
				<tr>
					<td><asp:Label ID="lblErrorMsg" Runat="server" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 165px"
							Width="464px" ForeColor="Red"></asp:Label></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
