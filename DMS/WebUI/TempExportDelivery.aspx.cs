using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for TempExportDelivery.
	/// </summary>
	public class TempExportDelivery : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblError;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			
			// Valiable
			string XMLText = Session["XMLText"].ToString();
			string XMLFilename = Session["XMLFilename"].ToString();
			
			// Split File
			string[] tpFilename = Regex.Split(XMLFilename,"YYYY");
			string[] strline = XMLText.Split('\n');		

			string path = Server.MapPath("~")+"/WebUI/Export/";
			string filename = tpFilename[0]+DateTime.Now.ToString("yyyyMMddHHmmss")+".txt";

			// Write File
			StreamWriter file = new StreamWriter(path+filename);
			foreach(string x in strline)
			{
				if(x.Trim() != "")
				{
					file.WriteLine(x);
				}
			}
					
			file.Close();
			System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
			response.ClearContent();
			response.Clear();
			response.ContentType = "text/plain";
			response.AddHeader("Content-Disposition", 
				"attachment; filename=" + filename + ";");
			response.TransmitFile(path+filename);
			response.Flush();
			response.End();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
