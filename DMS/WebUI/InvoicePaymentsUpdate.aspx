<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="InvoicePaymentsUpdate.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.InvoicePaymentsUpdate" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomerPopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/msFormValidation.js"></script>
		<script language="javascript">
			var currentAmntPaid=0;
			var currentBalanceDue=0;
			var digit=0;
			function validateInput(){
			var frm = document.forms[0];		
				if(frm.txtAmntPaid.value!=""){
					var today = new Date();
					var day = today.getDate();
					var month = today.getMonth()+1;
					var year = today.getFullYear();
					var hours = today.getHours();
					if(hours<10){
						hours = "0"+hours;
					}
					var min = today.getMinutes();
					if(min<10){
						min = "0"+min;
					}
					var sec = today.getSeconds();
					if(sec<10){
						sec = "0"+sec;
					}
					var time = hours+":"+min+":"+sec;
					if(month<10){
						month = '0'+month;
					}
					frm.txtPaidDate.value =  day+"/"+month+"/"+year;
					var username = document.getElementById("txtUserName").value;//frm.txtUsername.value;					
					document.getElementById("lblDispPaymentUpdate").innerHTML = username+" "+day+"/"+month+"/"+year+" "+time;
					document.getElementById("lblDispPaymentUpdate").style.display="block";
					document.getElementById("lblPaymentUpd").style.display="block";
					var amntPaid,totalAmnt=0;
					//if(currentAmntPaid=="0"){
					//currentAmntPaid=frm.txtCurrAmntPaid.value;	
					digit = (currentAmntPaid.length-1)-currentAmntPaid.lastIndexOf('.');					
					//}					
					amntPaid=frm.txtAmntPaid.value;
					totalAmnt = parseFloat(currentAmntPaid.replace(/,/i,''))+parseFloat(amntPaid);
					frm.txtCurrAmntPaid.value = totalAmnt;
					round(frm.txtCurrAmntPaid,digit);
					frm.txtCurrAmntPaid.value=addCommas(frm.txtCurrAmntPaid.value);
					if(currentBalanceDue=="0"){
						currentBalanceDue=frm.txtBalanceDue.value;	
					}
					totalAmnt = parseFloat(currentBalanceDue.replace(/,/i,''))-parseFloat(amntPaid);
					frm.txtBalanceDue.value = totalAmnt;
					round(frm.txtBalanceDue,digit);
					frm.txtBalanceDue.value=addCommas(frm.txtBalanceDue.value);
					//document.getElementById("lblApplyAmt").style.display="block";
					//document.getElementById("txtApplyAmt").style.display="block";
					//document.getElementById("ddlLinkToCDN").disabled=false;
					
				}else{
					frm.txtPaidDate.value="";
					frm.txtPaidDate.innerHTML="";
					document.getElementById("lblDispPaymentUpdate").innerHTML="";
					document.getElementById("lblDispPaymentUpdate").style.display="none";
					document.getElementById("lblPaymentUpd").style.display="none";
					//document.getElementById("lblApplyAmt").style.display="none";
					//document.getElementById("txtApplyAmt").style.display="none";
					//document.getElementById("ddlLinkToCDN").disabled=true;
				}			
			}
			function addCommas(nStr)
			{			
				nStr += '';
				x = nStr.split('.');				
				x1 = x[0];
				
				//x2 = x.length &gt; 1 ? '.' + x[1] : '';
				var x2
				if(x.length=2){
					x2="."+x[1];
				}
				var rgx = /(\d+)(\d{3})/;
				while (rgx.test(x1)) {
					x1 = x1.replace(rgx, '$1' + ',' + '$2');
				}
				return x1 + x2;
			}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="CustomerDetails" method="post" runat="server">
			<table id="table0" cellSpacing="2" cellPadding="0" width="100%" border="0">
				<tr>
					<td><asp:label id="LblHeading" runat="server" Width="442px" CssClass="mainTitleSize" Height="30px">Invoice Payments Update</asp:label></td>
					<td><asp:button id="btnOK" runat="server" Width="85" CssClass="buttonProp" Height="21" Text="OK"></asp:button>&nbsp;
						<INPUT class="buttonProp" id="btnCancel" style="WIDTH: 85px; HEIGHT: 21px" onclick="self.close();"
							type="button" value="Cancel" height="21" width="58"></td>
				</tr>
			</table>
			<table id="table1" cellSpacing="2" cellPadding="0" width="100%" border="0">
				<tr>
					<td><asp:label id="ErrMsg" runat="server" Width="100%" CssClass="errorMsgColor" Height="16px"></asp:label></td>
				</tr>
			</table>
			<table id="table2" cellSpacing="2" cellPadding="0" width="100%" border="0">
				<tr>
					<td width="20%"><asp:label id="lblCustID" runat="server" Width="100%" CssClass="tableLabel" Height="16">Customer ID</asp:label></td>
					<td width="20%"><asp:textbox id="txtCustID" runat="server" Width="116px" CssClass="textField" ReadOnly="True"></asp:textbox></td>
					<td width="20%"><asp:label id="lblInvoiceStatus" runat="server" Width="100%" CssClass="tableLabel" Height="16">Invoice Status</asp:label></td>
					<td width="40%"><FONT face="Tahoma"><asp:label id="lblInvDispStat" runat="server" Width="100px" CssClass="tableLabel" Height="16">Status</asp:label></FONT></td>
				</tr>
				<tr>
					<td><asp:label id="lblCustName" runat="server" Width="100%" CssClass="tableLabel" Height="16">Customer Name</asp:label></td>
					<td colSpan="3"><asp:label id="lblCustDispName" runat="server" Width="100%" CssClass="tableLabel" Height="16"
							Font-Bold="True">Cust Name</asp:label></td>
				</tr>
				<tr>
					<td><FONT face="Tahoma"><asp:label id="lblInvNo" runat="server" Width="100%" CssClass="tableLabel" Height="16">Invoice Number</asp:label></FONT></td>
					<td><FONT face="Tahoma"><asp:textbox id="txtInvNo" runat="server" Width="116px" CssClass="textField" ReadOnly="True"></asp:textbox></FONT></td>
					<td><asp:label id="lblLastUpdate" runat="server" Width="100%" CssClass="tableLabel" Height="16"
							Font-Bold="True"> Last Updated By/Date</asp:label></td>
					<td><asp:label id="lblDispUpdate" runat="server" Width="100%" CssClass="tableLabel" Height="16"
							Font-Bold="True">Updated By / Date</asp:label></td>
				</tr>
				<tr>
					<td><asp:label id="lblPrintedDueDate" runat="server" Width="100%" CssClass="tableLabel" Height="16"
							Font-Bold="True">Printed Due Date</asp:label></td>
					<td><cc1:mstextbox id="txtPrintedDueDate" tabIndex="3" runat="server" Width="116px" CssClass="textField"
							ReadOnly="True" TextMaskType="msDate" TextMaskString="99/99/9999" AutoPostBack="True" MaxLength="16"></cc1:mstextbox></td>
					<td><asp:label id="lblApproveDate" runat="server" Width="100%" CssClass="tableLabel" Height="16"
							Font-Bold="True"> Approved By/Date</asp:label></td>
					<td><asp:label id="lblDispApprove" runat="server" Width="100%" CssClass="tableLabel" Height="16"
							Font-Bold="True">Approved By / Date</asp:label></td>
				</tr>
				<tr>
					<td><asp:label id="lblInternalDueDate" runat="server" Width="100%" CssClass="tableLabel" Height="16"
							Font-Bold="True">Internal Due Date</asp:label></td>
					<td><cc1:mstextbox id="txtInternalDueDate" tabIndex="3" runat="server" Width="116px" CssClass="textField"
							ReadOnly="True" TextMaskType="msDate" TextMaskString="99/99/9999" AutoPostBack="True" MaxLength="16"></cc1:mstextbox></td>
					<td><FONT face="Tahoma"></FONT></td>
					<td></td>
				</tr>
				<tr>
					<td><asp:label id="lblActualBPD" runat="server" Width="100%" CssClass="tableLabel" Height="16">Actual BPD</asp:label></td>
					<td><cc1:mstextbox id="txtActualBPD" tabIndex="3" runat="server" Width="116px" CssClass="textField"
							ReadOnly="True" TextMaskType="msDate" TextMaskString="99/99/9999" AutoPostBack="True" MaxLength="16"></cc1:mstextbox></td>
					<td><asp:label id="lblBatchNo" runat="server" Width="100%" CssClass="tableLabel" Height="16">Batch Number</asp:label></td>
					<td><asp:textbox id="txtBatchNo" runat="server" Width="116px" CssClass="textField" ReadOnly="True"></asp:textbox></td>
				</tr>
				<tr>
					<td><asp:label id="lblPPD" runat="server" Width="100%" CssClass="tableLabel" Height="16">Promised Payment Date</asp:label></td>
					<td><cc1:mstextbox id="txtPromisedPaymentDate" tabIndex="3" runat="server" Width="116px" CssClass="textField"
							ReadOnly="True" TextMaskType="msDate" TextMaskString="99/99/9999" AutoPostBack="True" MaxLength="16"></cc1:mstextbox></td>
					<td><asp:label id="lblTotalInvAmnt" runat="server" Width="100%" CssClass="tableLabel" Height="16">Total Invoice Amount</asp:label></td>
					<td><cc1:mstextbox id="txtTotalInvAmnt" CssClass="textField" Height="21px" Text="" width="116px" ReadOnly="True"
							TextMaskType="msNumeric" TextMaskString="#.00" AutoPostBack="false" MaxLength="11" NumberScale="2"
							NumberPrecision="10" NumberMinValue="0" NumberMaxValue="99999999" Runat="server"></cc1:mstextbox></td>
				</tr>
				<tr>
					<td><asp:label id="lblAmntDate" runat="server" Width="100%" CssClass="tableLabel" Height="16">Amount Paid</asp:label></td>
					<td><cc1:mstextbox id="txtAmntPaid" CssClass="textField" Text="" width="116px" ReadOnly="True" TextMaskType="msNumeric"
							TextMaskString="#.00" AutoPostBack="false" MaxLength="11" NumberScale="2" NumberPrecision="10"
							NumberMinValue="-99999999" NumberMaxValue="99999999" Runat="server" NumberMaxValueCOD="99999999"
							NumberMinValueCustVAS="-99999999"></cc1:mstextbox></td>
					<td><asp:label id="lblCurrentAmntPaid" runat="server" Width="100%" CssClass="tableLabel" Height="16">Current Amount Paid</asp:label></td>
					<td><cc1:mstextbox id="txtCurrAmntPaid" CssClass="textField" Height="21px" Text="" width="116px" ReadOnly="True"
							TextMaskType="msNumeric" TextMaskString="#.00" AutoPostBack="false" MaxLength="11" NumberScale="2"
							NumberPrecision="10" NumberMinValue="0" NumberMaxValue="99999999" Runat="server"></cc1:mstextbox></td>
				</tr>
				<tr>
					<td><FONT face="Tahoma"><asp:label id="lblPaidDate" runat="server" Width="100%" CssClass="tableLabel" Height="16">Paid Date</asp:label></FONT></td>
					<td><cc1:mstextbox id="txtPaidDate" tabIndex="3" runat="server" Width="116px" CssClass="textField"
							ReadOnly="True" TextMaskType="msDate" TextMaskString="99/99/9999" AutoPostBack="True" MaxLength="16"></cc1:mstextbox></td>
					<td><asp:label id="lblBalanceDue" runat="server" Width="100%" CssClass="tableLabel" Height="16">Balance Due</asp:label></td>
					<td><cc1:mstextbox id="txtBalanceDue" CssClass="textField" Text="" width="116px" ReadOnly="True" TextMaskType="msNumeric"
							TextMaskString="#.00" AutoPostBack="false" MaxLength="11" NumberScale="2" NumberPrecision="10"
							NumberMinValue="0" NumberMaxValue="99999999" Runat="server"></cc1:mstextbox></td>
				</tr>
				<TR>
					<TD><asp:label id="lblPaymentUpd" style="DISPLAY: none" runat="server" Width="100%" CssClass="tableLabel"
							Height="16" Font-Bold="True">Payment Updated By/Date</asp:label></TD>
					<TD colSpan="3"><asp:label id="lblDispPaymentUpdate" style="DISPLAY: none" runat="server" Width="100%" CssClass="tableLabel"
							Height="16" Font-Bold="True">Payment By / Date</asp:label></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label2" runat="server" Width="100%" CssClass="tableLabel" Height="16" Visible="False">Link to Credit Note</asp:label></TD>
					<TD><asp:dropdownlist id="ddlLinkToCDN" tabIndex="13" runat="server" Width="116px" CssClass="textField"
							Visible="False" Enabled="False"></asp:dropdownlist></TD>
					<TD><asp:label id="lblApplyAmt" style="DISPLAY: none" runat="server" Width="100%" CssClass="tableLabel"
							Height="16">Applied Amount</asp:label></TD>
					<TD><cc1:mstextbox id="txtApplyAmt" style="DISPLAY: none" CssClass="textField" Text="" width="116px"
							ReadOnly="False" TextMaskType="msNumeric" TextMaskString="#.00" AutoPostBack="false" MaxLength="11"
							NumberScale="2" NumberPrecision="10" NumberMaxValue="99999999" Runat="server" NumberMaxValueCOD="99999999"
							NumberMinValueCustVAS="0"></cc1:mstextbox></TD>
				</TR>
			</table>
			<table id="table3" cellSpacing="2" cellPadding="0" width="100%" border="0">
				<tr>
					<td><asp:label id="Label1" runat="server" Width="442px" CssClass="mainTitleSize" Height="30px">Associated Credit and Debit Notes</asp:label></td>
				</tr>
				<TR>
					<TD><asp:datagrid id="dgAssociatedCNDN" Width="100%" Runat="server" AllowPaging="True" ItemStyle-Height="20"
							AutoGenerateColumns="False">
							<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
							<Columns>
								<asp:BoundColumn DataField="CNDN" HeaderText="CR/DB">
									<HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="CNDN_NO" HeaderText="Note ID">
									<HeaderStyle HorizontalAlign="Center" Width="40%" CssClass="gridHeading"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="STAT_TEXT" HeaderText="Status">
									<HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="gridHeading"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="total_amnt" HeaderText="Total Amount">
									<HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="gridHeading"></HeaderStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
						</asp:datagrid><asp:textbox id="txtUserName" style="DISPLAY: none" runat="server" ReadOnly="True"></asp:textbox></TD>
				</TR>
			</table>
		</form>
	</body>
</HTML>
