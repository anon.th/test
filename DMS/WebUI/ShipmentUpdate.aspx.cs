using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.classes;
using com.ties.DAL;
using com.common.applicationpages;
using com.common.RBAC;
using com.common.classes;
using System.Text;
using System.IO;

namespace com.ties
{
	/// <summary>
	/// Summary description for ShipmentUpdateaspx.
	/// </summary>
	public class ShipmentUpdate : BasePage
	{
		protected System.Web.UI.WebControls.RadioButton rbtnPickUp;
		protected System.Web.UI.WebControls.Label lblStatusCode;
		protected com.common.util.msTextBox txtStatusCode;
		protected System.Web.UI.WebControls.Label lblExceptionCode;
		protected System.Web.UI.WebControls.Button btnStatusCodeSearch;
		protected System.Web.UI.WebControls.TextBox txtStatusDesc;
		protected com.common.util.msTextBox txtExceptioncode;
		protected System.Web.UI.WebControls.Button btnExcepDescSrch;
		protected System.Web.UI.WebControls.TextBox txtExceptionDesc;
		protected System.Web.UI.WebControls.Label lblScanDate;
		//protected com.common.util.msTextBox txtScanDate;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.HtmlControls.HtmlTable ShipmentUpdMainTable;
		protected System.Web.UI.WebControls.RadioButton rbtnDom;
		DataSet m_dsShpmntUpdt = null;
		DataTable dtTemp =null;
		//Utility utility = null;
		String appID = null;
		String enterpriseID = null;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		String userID = null;
		protected System.Web.UI.WebControls.Button btnGridInsertRow;
		protected System.Web.UI.WebControls.RequiredFieldValidator validStatusCode;
		protected System.Web.UI.WebControls.RequiredFieldValidator validExcepCode;
		protected System.Web.UI.WebControls.RequiredFieldValidator validScanDate;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.WebControls.Label TITLE;
		protected System.Web.UI.WebControls.DataGrid dgShipUpdate;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentUpdate;
		protected System.Web.UI.WebControls.Label lblShipment;
		protected String strDmstPickup = null;
		protected String strConsgBkNo = null ;
		protected String strStatusDT = null ;
		protected String strPackage = null ;

		protected System.Web.UI.WebControls.Label lblLocationTop;
		protected System.Web.UI.WebControls.DropDownList ddlLocation;
		protected String strConsgName = null ;
		protected System.Web.UI.WebControls.Label lblBatchNo;
		protected System.Web.UI.WebControls.Button btnSubmitHidden;
		protected System.Web.UI.WebControls.Button btnClearValue;
		protected System.Web.UI.WebControls.Button btnClearHide;
		protected System.Web.UI.WebControls.Button btnScanComplete;
		protected System.Web.UI.WebControls.Label lblBatch;
		protected System.Web.UI.WebControls.Button btnRegen;
		protected System.Web.UI.WebControls.Button btnPkgConfirm;
		protected System.Web.UI.WebControls.Button btnFakeSave;
		protected System.Web.UI.WebControls.Label lblImportSIPs;
		protected System.Web.UI.HtmlControls.HtmlGenericControl fsImport;
		protected System.Web.UI.WebControls.TextBox txtFilePath;
		protected System.Web.UI.WebControls.Button btnImport;
		protected System.Web.UI.HtmlControls.HtmlInputFile inFile;
		protected System.Web.UI.WebControls.DataGrid dgImport;
		protected System.Web.UI.WebControls.Panel pnImport;
		protected System.Web.UI.HtmlControls.HtmlInputButton hddConNo;
		protected System.Web.UI.WebControls.TextBox Txt_hddConNo;
		protected System.Web.UI.WebControls.TextBox Txt_hddID;
		protected System.Web.UI.WebControls.TextBox TxtStatusDateTime;
		protected ArrayList userRoleArray;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();

			strConsgBkNo  = Utility.GetLanguageText(ResourceType.ScreenLabel,"Consignment No",utility.GetUserCulture());
			strStatusDT = Utility.GetLanguageText(ResourceType.ScreenLabel,"Status Date/Time",utility.GetUserCulture());
			strConsgName = Utility.GetLanguageText(ResourceType.ScreenLabel,"Consignee Name",utility.GetUserCulture());
			strPackage = Utility.GetLanguageText(ResourceType.ScreenLabel,"Package#",utility.GetUserCulture());
			this.btnGridInsertRow.Attributes.Add("onclick","javascript:document.getElementById('"+ this.btnGridInsertRow.ClientID + "').disabled=true;" + this.GetPostBackEventReference(this.btnGridInsertRow));
			this.btnSave.Attributes.Add("onclick","javascript:document.getElementById('"+ this.btnSave.ClientID + "').disabled=true;document.getElementById('"+ this.btnFakeSave.ClientID + "').click();" + GetPostBackEventReference(this.btnSave));
			
			if(!Page.IsPostBack)
			{
				TxtStatusDateTime.Attributes.Remove("onKeyPress");
				TxtStatusDateTime.Attributes.Remove("onChange");

				TxtStatusDateTime.Attributes.Add("onKeyPress", "return InputMask(this,'99/99/9999 99:99')");
				TxtStatusDateTime.Attributes.Add("onChange", "CheckMask(this,'99/99/9999 99:99');return setDateAndTime(this);");

				ViewState["headText"] = Utility.GetLanguageText(ResourceType.ScreenLabel,"Consignment No",utility.GetUserCulture());

				ResetScreen();
				//validExcepCode.ErrorMessage = "Exception code ";
				validScanDate.ErrorMessage = "Status date ";
				validStatusCode.ErrorMessage = "Status Code ";
				DateTime dtScanDt = DateTime.Now;
				TxtStatusDateTime.Text = dtScanDt.ToString("dd/MM/yyyy HH:mm");
				GetLocation();
				if (getRole()=="OPSSU")
				{
					ddlLocation.Enabled = true; 
				}
				else
				{
					ddlLocation.Enabled = false; 
				}
				Session["AllowInSWB"] =true;
				btnGridInsertRow.Enabled=false;

				ViewState["TargerSQLServerFolder"] = TIESDAL.EnterpriseConfigMgrDAL.TargerSQLServerFolder(appID,enterpriseID);

				Utility ut = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				ut.SetInitialFocus(txtStatusCode);
				//SetInitialFocus(txtStatusCode);
				
			}
			else
			{
				m_dsShpmntUpdt = (DataSet)Session["SESSION_DS1"];
			}
			PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
			getPageControls(Page);
		}
		private string getRole()
		{
			User user = new User();
			user.UserID = userID;
			userRoleArray = com.common.RBAC.RBACManager.GetAllRoles(appID, enterpriseID, user);
			Role role;

			for(int i=0;i<userRoleArray.Count;i++)
			{
				role = (Role)userRoleArray[i];				
				if (role.RoleName.ToUpper().Trim() == "OPSSU") return "OPSSU";
			}
			return " ";
		}
		public void BindGrid()
		{			
			dgShipUpdate.DataSource = m_dsShpmntUpdt;
			dgShipUpdate.DataBind();
			Session["SESSION_DS1"] = m_dsShpmntUpdt;
		}
    



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.hddConNo.ServerClick += new System.EventHandler(this.hddConNo_ServerClick);
			this.btnRegen.Click += new System.EventHandler(this.btnRegen_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.rbtnDom.CheckedChanged += new System.EventHandler(this.rbtnDom_CheckedChanged);
			this.rbtnPickUp.CheckedChanged += new System.EventHandler(this.rbtnPickUp_CheckedChanged);
			this.txtStatusCode.TextChanged += new System.EventHandler(this.txtStatusCode_TextChanged);
			this.btnStatusCodeSearch.Click += new System.EventHandler(this.btnStatusCodeSearch_Click);
			this.txtExceptioncode.TextChanged += new System.EventHandler(this.txtExceptioncode_TextChanged);
			this.btnExcepDescSrch.Click += new System.EventHandler(this.btnExcepDescSrch_Click);
			this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
			this.btnScanComplete.Click += new System.EventHandler(this.btnScanComplete_Click);
			this.btnGridInsertRow.Click += new System.EventHandler(this.btnGridInsertRow_Click);
			this.btnClearHide.Click += new System.EventHandler(this.btnClearHide_Click);
			this.dgShipUpdate.ItemCreated += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgShipUpdate_ItemCreated);
			this.dgShipUpdate.SelectedIndexChanged += new System.EventHandler(this.dgShipUpdate_SelectedIndexChanged);
			this.btnPkgConfirm.Click += new System.EventHandler(this.btnPkgConfirm_Click);
			this.btnSubmitHidden.Click += new System.EventHandler(this.btnSubmitHidden_Click);
			this.ID = "ShipmentUpdate";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		
		protected void dgShipUpdate_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgShipUpdate.CurrentPageIndex = e.NewPageIndex;
			BindGrid();
		}

		public void dgShipUpdate_Update(object sender, DataGridCommandEventArgs e)
		{
			//			if(m_dsShpmntUpdt.Tables[0].Rows.Count>0 && ViewState["AllowInSWB"]!=null)
			//
			//			{
			//				if(Convert.ToBoolean(ViewState["AllowInSWB"])!= Convert.ToBoolean(Session["AllowInSWB"]))
			//
			//				return;
			//			}
			lblErrorMsg.Text = "";
			m_dsShpmntUpdt.EnforceConstraints = false;

			bool isExists = false;
			bool dupState = false;
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeSUState();
				ViewState["isTextChanged"] = true;
			}
			
			
			TextBox txtCnsgBkNo = (TextBox)e.Item.FindControl("txtCnsgBkNo");
			TextBox txtPkgNo = (TextBox)e.Item.FindControl("txtPackage");
			TextBox txtStatusDtTimec = (TextBox)e.Item.FindControl("txtStatusDtTime");
			TextBox txtPackage = (TextBox)e.Item.FindControl("txtPackage");
			TextBox txtStatusDtTime = (TextBox)e.Item.FindControl("txtStatusDtTime");
			TextBox txtPersonInchrg = (TextBox)e.Item.FindControl("txtPersonInchrg");
			TextBox txtRemarks = (TextBox)e.Item.FindControl("txtRemarks");

			if(txtCnsgBkNo != null)
			{
				DataSet dsLastestDt = null;
				String strLastStatusCode="";
				int iBkNo  = 0;
				//Call the method and check whether the consigment/booking no exists in table.
				
				if(rbtnDom.Checked == true)
				{
					isExists = DomesticShipmentMgrDAL.IsConsgmentExist(appID,enterpriseID,txtCnsgBkNo.Text.Trim());
					
					if(isExists)
					{
						if(txtPkgNo.Text=="")
						{
							txtPkgNo.Text="0";
						}
						int ipkgNo =int.Parse(txtPkgNo.Text);
						if(ipkgNo>DomesticShipmentMgrDAL.getPkgfromConNo_ConsignDetail(appID,enterpriseID,txtCnsgBkNo.Text.Trim()))
						{
							if(txtStatusCode.Text.Trim()=="SOP" || txtStatusCode.Text.Trim()=="SIP" || txtStatusCode.Text.Trim()=="CLS" || txtStatusCode.Text.Trim()=="UNSOP" || txtStatusCode.Text.Trim()=="UNSIP" || txtStatusCode.Text.Trim()=="UNCLS")
							{
								int counter=0;
								bool dupStateCase=false;
								foreach (DataRow dr in m_dsShpmntUpdt.Tables[0].Rows)
								{
									if (counter != e.Item.ItemIndex)
									{
										if(Convert.ToString(dr["CnsgBkgNo"]).Trim() ==  txtCnsgBkNo.Text.Trim())
										{
											if(Convert.ToString(dr["pkg_no"]).Trim() ==  txtPackage.Text.Trim())
												dupStateCase = true;
										}
									}
									counter++;
								}

								if(dupStateCase)
								{
									lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());  
									this.btnGridInsertRow.Enabled=false;
									return;
								}

								if(lblBatchNo.Text!="")
								{
									int iState = BatchManifest.GetDupinstatging(appID,enterpriseID,lblBatchNo.Text.Trim(),ddlLocation.SelectedValue.ToString().Trim(),txtCnsgBkNo.Text.Trim(),txtPkgNo.Text.Trim(),txtStatusCode.Text.Trim());
									if(iState==2)
									{
										lblErrorMsg.Text =  "Duplicate status code - this scan has already been recorded.";  
										//lblErrorMsg.Text =  "Duplicate record." + " Consignment# " + txtCnsgBkNo.Text.Trim() + " Pkg# " + txtPkgNo.Text.Trim() + " Status code: "+ txtStatusCode.Text.Trim() +" on Batch# " + lblBatchNo.Text.Trim();  
										return;
									}
									if(iState==3)
									{
										lblErrorMsg.Text =  "Invalid status because no corresponding package has scanned.";  
										//lblErrorMsg.Text =  "Invalid status." + " Consignment# " + txtCnsgBkNo.Text.Trim() + " Pkg# " + txtPkgNo.Text.Trim() + " Status code: "+ txtStatusCode.Text.Trim() +" on Batch# " + lblBatchNo.Text.Trim();  
										return;
									}
								}

								ViewState["Eindex"]=e.Item.ItemIndex;
								string script="<script langauge='javacript'>";
								script+= "confirmNewPkg('"+ txtPkgNo.ClientID+ "');";
								script+= "</script>";
								Page.RegisterStartupScript("confirmPkg",script);
							}
							else
							{
								lblErrorMsg.Text = "Package number not valid for this consignment.";
								this.btnGridInsertRow.Enabled=false;
							}
							return;
						}
						//get booking no for consignment & booking date/time
						if(txtStatusCode != null && txtStatusCode.Text.ToUpper().Equals("PODEX"))
						{
							if(!ShipmentUpdateMgrDAL.CheckPODStatus(appID,enterpriseID,txtCnsgBkNo.Text))
							{
								lblErrorMsg.Text = "Consignment# "+txtCnsgBkNo.Text+" must have POD status";
								this.btnGridInsertRow.Enabled=false;
								return;
							}
							if(!ShipmentUpdateMgrDAL.IsDupStatusPODEX(appID,enterpriseID,txtCnsgBkNo.Text, txtStatusCode.Text.ToUpper()))
							{
								lblErrorMsg.Text = txtStatusCode.Text.ToUpper() + " status has been assigned to Consignment# " +txtCnsgBkNo.Text.ToUpper()+".";
								this.btnGridInsertRow.Enabled=false;
								return;
							}
							//Compare Est DeliveryDate
							////							DateTime dtEstDelivery  = ShipmentUpdateMgrDAL.GetShipmentEstDeliveryDate(appID,enterpriseID,txtCnsgBkNo.Text.Trim());
							////							int i = dtEstDelivery.CompareTo(DateTime.ParseExact(txtStatusDtTimec.Text,"dd/MM/yyyy HH:mm",null));
							////							if(dtEstDelivery.CompareTo(DateTime.ParseExact(txtStatusDtTimec.Text,"dd/MM/yyyy HH:mm",null))< 0)
							////							{
							////								
							////								lblErrorMsg.Text ="xxxx";
							////								this.btnGridInsertRow.Enabled=false;
							////								return;
							////							}
							
							
						}
						iBkNo  = ShipmentUpdateMgrDAL.GetShipmentBookingNo(appID,enterpriseID,txtCnsgBkNo.Text.Trim());
						DateTime dtBkg  = ShipmentUpdateMgrDAL.GetShipmentBookingDateTime(appID,enterpriseID,txtCnsgBkNo.Text.Trim());
						if(dtBkg.CompareTo(DateTime.ParseExact(txtStatusDtTimec.Text,"dd/MM/yyyy HH:mm",null))> 0)
						{
							lblErrorMsg.Text =   Utility.GetLanguageText(ResourceType.UserMessage,"STATUS_DT_LESS_BKG_DT",utility.GetUserCulture());
							this.btnGridInsertRow.Enabled=false;
							return;
						}
						dsLastestDt = ShipmentTrackingMgrDAL.GetLatestDateFromShipment(appID,enterpriseID,iBkNo);

						//BY X FEB 12 09
						//Both domestic or pickup, if there is any Consignment with last_status_code = INVA
						//Do not Update - reject this operation with Error Message of INVA Consignment Nos.
						strLastStatusCode = ShipmentUpdateMgrDAL.GetShipmentLatest_Status_Code(appID,enterpriseID,txtCnsgBkNo.Text.Trim());
						if(strLastStatusCode!=null&&strLastStatusCode!=""&&strLastStatusCode=="INVA")
						{
							lblErrorMsg.Text = "Consignment No. " + txtCnsgBkNo.Text + " has INVA status, cannot update.";
							this.btnGridInsertRow.Enabled=false;
							return;
						}
						//END BY X FEB 12 09
					}
					//BY X JUL 10 08
					if(txtStatusCode.Text!="SIP" && txtStatusCode.Text!="UNSIP" && txtStatusCode.Text!="SOP" && txtStatusCode.Text!="UNSOP"&& txtStatusCode.Text!="CLS"&& txtStatusCode.Text!="UNCLS")
					{
						if(ShipmentUpdateMgrDAL.IsDupTrackDT_SHP_AndStaging(this.appID.ToString(),this.enterpriseID.ToString(),DateTime.ParseExact(txtStatusDtTimec.Text,"dd/MM/yyyy HH:mm",null),txtCnsgBkNo.Text.Trim().ToString(),null))
						{
							//dupState=true;
							lblErrorMsg.Text = "Prior consignment status recorded for the same date/time so this status cannot be saved.";  
							this.btnGridInsertRow.Enabled=false;
							return;
						}
						else
						{dupState=false;}
					}

					bool isMDE = false;
					isMDE = TIESUtility.IsStatusMDE(appID,enterpriseID,txtCnsgBkNo.Text.Trim());
					if(txtStatusCode.Text.Trim() == "POD") 
					{
						if(!isMDE)
						{
							lblErrorMsg.Text = "Consignment# "+txtCnsgBkNo.Text+" must have MDE status";
							this.btnGridInsertRow.Enabled=false;
							return;
						}
						//add by Hong 22-03-2011
						bool isDest = TIESUtility.isDestinationStation(appID,enterpriseID,txtCnsgBkNo.Text.Trim(),ddlLocation.SelectedItem.Text.Trim());
						if(!isDest)
						{
							lblErrorMsg.Text = "POD can be entered only by destination DC.";
							this.btnGridInsertRow.Enabled=false;
							return;
						
						}
						
						

						// end add by Hong
					}
				}
				else if(rbtnPickUp.Checked == true)
				{
					iBkNo = Convert.ToInt32(txtCnsgBkNo.Text.Trim());
					isExists = TIESUtility.IsPickUpBookingExist(appID,enterpriseID,iBkNo);
					if(isExists)
					{
						DateTime dtBkg =  DispatchTrackingMgrDAL.PickupBkgDateTime(appID,enterpriseID,Convert.ToInt32(txtCnsgBkNo.Text.Trim()));
						if(dtBkg.CompareTo(DateTime.ParseExact(txtStatusDtTimec.Text,"dd/MM/yyyy HH:mm",null))> 0)
						{
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"STATUS_DT_LESS_BKG_DT",utility.GetUserCulture());  
							this.btnGridInsertRow.Enabled=false;
							return;
						}
						dsLastestDt =  DispatchTrackingMgrDAL.GetLatestDateFromDispatch(appID,enterpriseID,Convert.ToInt32(txtCnsgBkNo.Text.Trim()));
					}
					//BY GwanG JUL 11 08
					if(ShipmentUpdateMgrDAL.IsDupTrackDT_SHP_AndStaging(this.appID.ToString(),this.enterpriseID.ToString(),DateTime.ParseExact(txtStatusDtTimec.Text,"dd/MM/yyyy HH:mm",null),null,iBkNo.ToString()))
					{dupState=true;}
					else
					{dupState=false;}
				}


			
				if(isExists == false)
				{
					lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"NO_BKG_CONSGMT",utility.GetUserCulture());  
					this.btnGridInsertRow.Enabled=false;
					if(rbtnDom.Checked==true )
					{
						if(txtStatusCode.Text.Trim()=="SOP" || txtStatusCode.Text.Trim()=="SIP" || txtStatusCode.Text.Trim()=="CLS")
						{
							DataTable dtTemp = m_dsShpmntUpdt.Tables[0].Clone();
							dtTemp.Columns["consignment_no"].Unique = false; 
							dtTemp.Columns["consignment_no"].AllowDBNull = true;
							dtTemp.Columns["pkg_no"].AllowDBNull = true;
							for(int i = 0; i < m_dsShpmntUpdt.Tables[0].Rows.Count; i++)
							{
								dtTemp.ImportRow(m_dsShpmntUpdt.Tables[0].Rows[i]);
							}

							dtTemp.Rows[e.Item.ItemIndex]["CnsgBkgNo"]=txtCnsgBkNo.Text.Trim();
							dtTemp.Rows[e.Item.ItemIndex]["tracking_datetime"]=DateTime.ParseExact(txtStatusDtTime.Text,"dd/MM/yyyy HH:mm",null);
							dtTemp.Rows[e.Item.ItemIndex]["person_incharge"]=txtPersonInchrg.Text.Trim();
							dtTemp.Rows[e.Item.ItemIndex]["remarks"]=txtRemarks.Text.Trim();
							dtTemp.Rows[e.Item.ItemIndex]["pkg_no"]=txtPkgNo.Text.Trim();
							dtTemp.Rows[e.Item.ItemIndex].AcceptChanges();

							Session["dtTemp"]=dtTemp;

							string script="<script langauge='javacript'>";
							script+= "Pop('"+ txtCnsgBkNo.Text.Trim()+ "','"+txtStatusCode.Text.Trim()+"','"+ txtCnsgBkNo.ClientID +"');";
							script+= "</script>";
							Page.RegisterStartupScript("pop",script);
						}
					}
					return;


				}
				/*else if(rbtnDom.Checked == true)
				{	//BY X JUL 09 08 
		
//					if(TIESUtility.IsInvoiced(appID,enterpriseID,iBkNo,txtCnsgBkNo.Text.Trim()) == true)
//					{
//						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVOICE_SHIPMT_PRESENT",utility.GetUserCulture());  
//						return;
//					}
				}*/
			
				//Update the dataset

				int count = 0;

				foreach (DataRow dr in m_dsShpmntUpdt.Tables[0].Rows)
				{
					if (count != e.Item.ItemIndex)
					{
						if(Convert.ToString(dr["CnsgBkgNo"]).Trim() ==  txtCnsgBkNo.Text.Trim())
						{
							if(this.rbtnDom.Checked)
							{
								//TextBox txtPackage = (TextBox)e.Item.FindControl("txtPackage");
								if(Convert.ToString(dr["pkg_no"]).Trim() ==  txtPackage.Text.Trim())
									dupState = true;
							}
							else
								dupState = true;
						}
					}
					count++;
				}


				if(dupState)
				{
					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());  
					this.btnGridInsertRow.Enabled=false;
					return;
				}
				else
				{
					
					if(rbtnDom.Checked == true)
					{
						if(lblBatchNo.Text!="")
						{
							int iState = BatchManifest.GetDupinstatging(appID,enterpriseID,lblBatchNo.Text.Trim(),ddlLocation.SelectedValue.ToString().Trim(),txtCnsgBkNo.Text.Trim(),txtPkgNo.Text.Trim(),txtStatusCode.Text.Trim());
							if(iState==2)
							{
								lblErrorMsg.Text =  "Duplicate status code - this scan has already been recorded.";  
								//lblErrorMsg.Text =  "Duplicate record." + " Consignment# " + txtCnsgBkNo.Text.Trim() + " Pkg# " + txtPkgNo.Text.Trim() + " Status code: "+ txtStatusCode.Text.Trim() +" on Batch# " + lblBatchNo.Text.Trim();  
								return;
							}
							if(iState==3)
							{
								lblErrorMsg.Text =  "Invalid status because no corresponding package has scanned.";  
								//lblErrorMsg.Text =  "Invalid status." + " Consignment# " + txtCnsgBkNo.Text.Trim() + " Pkg# " + txtPkgNo.Text.Trim() + " Status code: "+ txtStatusCode.Text.Trim() +" on Batch# " + lblBatchNo.Text.Trim();  
								return;
							}
						}
					}
					DataRow drEach = m_dsShpmntUpdt.Tables[0].Rows[e.Item.ItemIndex];
					drEach["CnsgBkgNo"] = txtCnsgBkNo.Text.Trim();


					if(txtStatusDtTime != null)
					{
						DataRow drCurrent = m_dsShpmntUpdt.Tables[0].Rows[e.Item.ItemIndex];
						drCurrent["tracking_datetime"] =  DateTime.ParseExact(txtStatusDtTime.Text,"dd/MM/yyyy HH:mm",null);
					}
				
					if(txtPersonInchrg != null)
					{
						DataRow drCurrent = m_dsShpmntUpdt.Tables[0].Rows[e.Item.ItemIndex];
						drCurrent["person_incharge"] = txtPersonInchrg.Text;
					}

					if(txtRemarks != null)
					{
						DataRow drCurrent = m_dsShpmntUpdt.Tables[0].Rows[e.Item.ItemIndex];
						drCurrent["remarks"] = txtRemarks.Text;
					}
					// TU 16/02/2011
					
					if(txtPackage != null)
					{
						DataRow drCurrent = m_dsShpmntUpdt.Tables[0].Rows[e.Item.ItemIndex];
						drCurrent["pkg_no"] = txtPackage.Text;
					}

					if (rbtnPickUp.Checked==true)
					{
						//BY X JAN 21 08
						TextBox txtLocation = (TextBox)e.Item.FindControl("txtLocation");
						if(txtLocation != null)
						{
							bool boolLocationOK=false;
						
							if(txtLocation.Text.Trim()!="") //�����प�������ҧ
							{
								DataSet dataset1 = com.ties.classes.DbComboDAL.UserLocationQuery(this.appID,this.enterpriseID);
								if(dataset1.Tables[0].Rows.Count > 0) //����մի���ʡ�������硡дիա�͹�Ш��
								{
									foreach(DataRow tmpDr in dataset1.Tables[0].Rows)
									{
										if(tmpDr["origin_code"].ToString().ToUpper()==txtLocation.Text.Trim().ToUpper())
											boolLocationOK = true; //��ҵç�ѹ �����
									}
						
									if(boolLocationOK)//�����प�� �ç�дի� ��ѹ�֡�Ŵ���
									{
										DataRow drCurrent = m_dsShpmntUpdt.Tables[0].Rows[e.Item.ItemIndex];
										drCurrent["location"] = txtLocation.Text;
									}
									else //������ç�ѹ �����ͧ��͹ �������͡ ���ѹ�֡��¨��
									{
										lblErrorMsg.Text =  "The Location must be a Distribution Center.";//Utility.GetLanguageText(ResourceType.UserMessage,"Consignee Name is a required entry for a POD status.",utility.GetUserCulture());  
										return;
									}
								}
								else//�óմի� �ѹ��������������� ���ͧ�������µ�����
								{
									DataRow drCurrent = m_dsShpmntUpdt.Tables[0].Rows[e.Item.ItemIndex];
									drCurrent["location"] = txtLocation.Text;
								}
							}
							else //�����प����ҧ������� �ѹ�֡����
							{
								DataRow drCurrent = m_dsShpmntUpdt.Tables[0].Rows[e.Item.ItemIndex];
								drCurrent["location"] = txtLocation.Text;
							}
						
						}
					}


					//					TextBox txtStatusCode = (TextBox)e.Item.FindControl("txtStatusCode");
					
					TextBox txtConsignee_Name = (TextBox)e.Item.FindControl("txtConsignee_Name");
					if(txtConsignee_Name != null)
					{
						if(txtConsignee_Name.Text.Trim()==""&&txtStatusCode.Text=="POD")
						{
							lblErrorMsg.Text =  "Consignee Name is a required entry for a POD status.";//Utility.GetLanguageText(ResourceType.UserMessage,"Consignee Name is a required entry for a POD status.",utility.GetUserCulture());  
							this.btnGridInsertRow.Enabled=false;
							return;
						}
						else
						{
							DataRow drCurrent = m_dsShpmntUpdt.Tables[0].Rows[e.Item.ItemIndex];
							drCurrent["Consignee_Name"] = txtConsignee_Name.Text;
						}
					}


					//END BY X JAN 21 08

					m_dsShpmntUpdt.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					dgShipUpdate.EditItemIndex = -1;
					btnSave.Enabled=true;
					btnGridInsertRow.Enabled = true;
					BindGrid();
				}	
			}
		}

		public void dgShipUpdate_Cancel(object sender, DataGridCommandEventArgs e)
		{
			TextBox txtCnsgBkNo = (TextBox)e.Item.FindControl("txtCnsgBkNo");
			TextBox txtPersonInchrg = (TextBox)e.Item.FindControl("txtPersonInchrg");
			TextBox txtRemarks = (TextBox)e.Item.FindControl("txtRemarks");
			if(((txtCnsgBkNo != null) && (txtCnsgBkNo.Text.Length == 0)&& (txtPersonInchrg != null)) || ((txtPersonInchrg.Text.Length == 0) && (txtRemarks != null)) || ((txtRemarks.Text.Length == 0)))
			{
				m_dsShpmntUpdt.Tables[0].Rows.RemoveAt(dgShipUpdate.EditItemIndex);
			}
			dgShipUpdate.EditItemIndex = -1;
			btnGridInsertRow.Enabled = true;
			btnSave.Enabled=true;
			BindGrid();
		}

		public void dgShipUpdate_Edit(object sender, DataGridCommandEventArgs e)
		{
			dgShipUpdate.EditItemIndex = e.Item.ItemIndex;
			btnSave.Enabled=false;
			BindGrid();
			RequiredFieldValidator validatePgk = (RequiredFieldValidator)e.Item.FindControl("validatePgk");
			if(Session["AllowInSWB"]!=null && validatePgk!=null )
			{
				validatePgk.Enabled = Convert.ToBoolean(Session["AllowInSWB"]);
				if(!validatePgk.Enabled)
				{
					dgShipUpdate.Columns[3].Visible= false;
				}
				else
				{
					dgShipUpdate.Columns[3].Visible= true;

				}
			}
			TextBox txtCnsgBkNo = (TextBox)dgShipUpdate.Items[dgShipUpdate.EditItemIndex].FindControl("txtCnsgBkNo");
			if(txtCnsgBkNo != null)
			{
				string script="<script langauge='javacript'>";
				script+= "document.getElementById('" + txtCnsgBkNo.ClientID + "').focus();";
				script+= "</script>";
				Page.RegisterStartupScript("focus"+DateTime.Now.ToString("ddMMyyhhmmss"),script);
			}
		}

		public void dgShipUpdate_Delete(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text = "";
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeSUState();
				ViewState["isTextChanged"] = true;
			}
			
			try
			{
				DataRow drCurrent = m_dsShpmntUpdt.Tables[0].Rows[e.Item.ItemIndex];
				drCurrent.Delete();
				m_dsShpmntUpdt.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
			}
			catch(System.IndexOutOfRangeException)
			{
				//If the first row is deleted without clicking update in grid
			}

			if(btnGridInsertRow.Enabled == false)
			{
				dgShipUpdate.EditItemIndex = m_dsShpmntUpdt.Tables[0].Rows.Count - 1;
			}
			//btnGridInsertRow.Enabled = true;
			//btnSave.Enabled = true;
			BindGrid();
		}

		private void dgShipUpdate_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}
		private void Save()
		{
			dtTemp = m_dsShpmntUpdt.Tables[0].Clone();
			dtTemp.Columns["consignment_no"].Unique = false; 
			dtTemp.Columns["consignment_no"].AllowDBNull = true;
			for(int i = 0; i < m_dsShpmntUpdt.Tables[0].Rows.Count; i++)
			{
				dtTemp.ImportRow(m_dsShpmntUpdt.Tables[0].Rows[i]);
			}
			lblErrorMsg.Text = "";
			//HC Return Task
			bool PODDupFlag = false;
			bool OtherStatusFlag = false;
			//HC Return Task

			if((int)ViewState["SUOperation"] == (int)Operation.Saved)
			{
				if((m_dsShpmntUpdt == null) || (m_dsShpmntUpdt.Tables[0].Rows.Count == 0))
				{
					lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DETAIL",utility.GetUserCulture());  
					return;
				}

				//Modified by GwanG on 12Feb08
				if(txtStatusCode.Text.Length > 0)
				{
					bool isExists = false;
					isExists = TIESUtility.IsStatusCodeExist(appID,enterpriseID,txtStatusCode.Text.Trim(),userID);
					if(isExists)
					{				
						String strType = null;
						if(rbtnDom.Checked == true)
						{
							strType = "D";
						}
						else if(rbtnPickUp.Checked == true)
						{
							strType = "P";
						}
				
						StatusCodeDesc statusCodeDesc =  TIESUtility.GetStatusCodeDesc(appID,enterpriseID,txtStatusCode.Text.Trim(),strType);
						String strStatusCd = statusCodeDesc.strStatusCode;
						String strStatusDes = statusCodeDesc.strStatusDesc;
						if(strStatusCd == null)
						{
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"STATUS_PRESENT",utility.GetUserCulture());  						
							//txtStatusCode.Text = "";
							return;
						}
						else
						{
							txtStatusDesc.Text = strStatusDes;
						}
					}
					else 
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"STATUS_PRESENT",utility.GetUserCulture());
						txtStatusDesc.Text = "";
						return;
					}
				}
				else
				{
					txtStatusDesc.Text="";
				}
				

				try
				{
					if(rbtnPickUp.Checked == true)
						strDmstPickup = "Pickup";
					else if(rbtnDom.Checked == true)
						strDmstPickup = "Domestic";
					if(strDmstPickup.Equals("Domestic"))
					{
						for(int i = 0; i < m_dsShpmntUpdt.Tables[0].Rows.Count; i++)
						{
							DataRow drEach = m_dsShpmntUpdt.Tables[0].Rows[i];
							if((drEach["CnsgBkgNo"] != null) && (!drEach["CnsgBkgNo"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["CnsgBkgNo"].ToString().Trim() != ""))
							{
								String strConsgnment = drEach["CnsgBkgNo"].ToString();
								if(lblBatchNo.Text!="")
								{
									int iState = BatchManifest.GetDupinstatging(appID,enterpriseID,lblBatchNo.Text.Trim(),ddlLocation.SelectedValue.ToString().Trim(),strConsgnment,drEach["pkg_no"].ToString(),txtStatusCode.Text.Trim());
									//									if(iState!=0)
									//									{
									if(iState==2)
									{
										lblErrorMsg.Text =  "Duplicate record." + " Consignment# " + strConsgnment + " Pkg# " + drEach["pkg_no"].ToString() + " Status code: "+ txtStatusCode.Text.Trim() +" on Batch# " + lblBatchNo.Text.Trim();  
										return;
									}
									if(iState==3)
									{
										lblErrorMsg.Text =  "Invalid status." + " Consignment# " + strConsgnment + " Pkg# " + drEach["pkg_no"].ToString() + " Status code: "+ txtStatusCode.Text.Trim() +" on Batch# " + lblBatchNo.Text.Trim();  
										return;
									}
									//									}
								}

								
								int iBookingNo = ShipmentUpdateMgrDAL.GetShipmentBookingNo(appID,enterpriseID,strConsgnment);
								drEach["booking_no"] = iBookingNo;

								//HC Return Task
								DataSet tmpDSZipCode = ShipmentUpdateMgrDAL.getZipCodeByCon(appID,enterpriseID,strConsgnment, Convert.ToString(iBookingNo));
								///////// by Tumz
								if(tmpDSZipCode.Tables[0].Rows.Count > 0)
									drEach["ZipCode"] = tmpDSZipCode.Tables[0].Rows[0]["recipient_zipcode"].ToString();
								//HC Return Task
							}
							else
							{
								lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"PLS_PRSS_SAVE",utility.GetUserCulture());  
								return;
							}
						}
					}
					String strExcepCode = null;
					if(txtExceptioncode.Text.Length > 0)
						strExcepCode = txtExceptioncode.Text;

					//HC Return Task
					DataSet m_dsShpmntUpdt_Invalid = m_dsShpmntUpdt.Clone();
					string updatingStatus = txtStatusCode.Text.Trim();			
					for(int rowIndex = 0; rowIndex <= m_dsShpmntUpdt.Tables[0].Rows.Count - 1;)					
					{
						DataRow row = m_dsShpmntUpdt.Tables[0].Rows[rowIndex];

						if(row["booking_no"] != DBNull.Value)
						{
							if(row["CnsgBkgNo"] != DBNull.Value)
							{
								DataSet tmpTrackDs = ShipmentUpdateMgrDAL.getTrackingRecords(appID, enterpriseID, 
									Convert.ToInt32(row["booking_no"].ToString().Trim()), row["CnsgBkgNo"].ToString());

								#region POD case
								if (updatingStatus == "POD")
								{
									bool delFlag = false;

									foreach (DataRow drLocal in tmpTrackDs.Tables[0].Rows)
									{
										if(drLocal["status_code"] != DBNull.Value)
										{
											string tmpStatusCode = drLocal["status_code"].ToString();
											string tmpDeleted = "";

											if(drLocal["deleted"] != DBNull.Value)
											{
												tmpDeleted = Convert.ToString(drLocal["deleted"]);
											}

											if (tmpStatusCode.Trim() == "POD" && tmpDeleted.Trim() != "Y")
											{
												m_dsShpmntUpdt_Invalid.Tables[0].ImportRow(row);
												m_dsShpmntUpdt_Invalid.Tables[0].AcceptChanges();

												row.Delete();
												m_dsShpmntUpdt.Tables[0].AcceptChanges();
												rowIndex = 0;
												delFlag = true;

												PODDupFlag = true;
											}
										}
									}
									if( !PODDupFlag && !delFlag && ShipmentUpdateMgrDAL.MF_Shipment_CheckDupPOD(appID, enterpriseID,row["CnsgBkgNo"].ToString())==-1)
									{
										m_dsShpmntUpdt_Invalid.Tables[0].ImportRow(row);
										m_dsShpmntUpdt_Invalid.Tables[0].AcceptChanges();

										row.Delete();
										m_dsShpmntUpdt.Tables[0].AcceptChanges();
										rowIndex = 0;
										delFlag = true;

										PODDupFlag = true;
									}

									if(!delFlag) rowIndex++;
								}
									#endregion

									#region HCR case
								else if (updatingStatus == "HCR")
								{
									bool isNotdeleted = true;
									bool isNotHavePOD = true;
									bool delFlag = false;
									String return_pod_slipFlag = "N";
									String return_invoice_hcFlag = "N";

									DataSet tmpHCRDs;
									tmpHCRDs = ShipmentUpdateMgrDAL.getHCFromShipment(appID, enterpriseID, 
										Convert.ToInt32(row["booking_no"].ToString().Trim()), row["CnsgBkgNo"].ToString());
									DataRow rowHCRDs = tmpHCRDs.Tables[0].Rows[0];

									if( (rowHCRDs["return_pod_slip"] == null) || (rowHCRDs["return_pod_slip"]==DBNull.Value) || 
										(rowHCRDs["return_pod_slip"].ToString() == ""))
										return_pod_slipFlag = "N";
									else
										return_pod_slipFlag = rowHCRDs["return_pod_slip"].ToString();

									if( (rowHCRDs["return_invoice_hc"] == null) ||  (rowHCRDs["return_invoice_hc"]==DBNull.Value) || 
										(rowHCRDs["return_invoice_hc"].ToString() == ""))
										return_invoice_hcFlag = "N";
									else
										return_invoice_hcFlag = tmpHCRDs.Tables[0].Rows[0]["return_invoice_hc"].ToString();

									if (return_pod_slipFlag == "N" && return_invoice_hcFlag == "N") 
									{
										m_dsShpmntUpdt_Invalid.Tables[0].ImportRow(row);
										m_dsShpmntUpdt_Invalid.Tables[0].AcceptChanges();

										row.Delete();
										m_dsShpmntUpdt.Tables[0].AcceptChanges();
										rowIndex = 0;
										delFlag = true;

										OtherStatusFlag = true;
									}
									else
									{
										foreach (DataRow drLocal in tmpTrackDs.Tables[0].Rows)
										{
											if(drLocal["status_code"] != DBNull.Value)
											{
												string tmpStatusCode = drLocal["status_code"].ToString();
												string tmpDeleted = "";

												if(drLocal["deleted"] != DBNull.Value)
												{
													tmpDeleted = Convert.ToString(drLocal["deleted"]);
												}

												if (tmpStatusCode.Trim() == "HCR" && tmpDeleted.Trim() != "Y")
												{
													m_dsShpmntUpdt_Invalid.Tables[0].ImportRow(row);
													m_dsShpmntUpdt_Invalid.Tables[0].AcceptChanges();

													row.Delete();
													m_dsShpmntUpdt.Tables[0].AcceptChanges();
													rowIndex = 0;
													delFlag = true;

													isNotdeleted = false;

													OtherStatusFlag = true;
												}

												if (tmpStatusCode.Trim() == "POD" && tmpDeleted.Trim() != "Y")
												{
													isNotHavePOD = false;
												}
											}
										}

										if (isNotdeleted && isNotHavePOD)
										{
											m_dsShpmntUpdt_Invalid.Tables[0].ImportRow(row);
											m_dsShpmntUpdt_Invalid.Tables[0].AcceptChanges();

											row.Delete();
											m_dsShpmntUpdt.Tables[0].AcceptChanges();
											rowIndex = 0;
											delFlag = true;

											OtherStatusFlag = true;
										}
									}

									if(!delFlag)
										rowIndex++;
								}
									#endregion

									#region INVR case
								else if (updatingStatus == "INVR")
								{
									bool isNotdeleted = true;
									bool isNotHaveHCR = true;
									bool delFlag = false;

									foreach (DataRow drLocal in tmpTrackDs.Tables[0].Rows)
									{
										if(drLocal["status_code"] != DBNull.Value)
										{
											string tmpStatusCode = drLocal["status_code"].ToString();
											string tmpDeleted = "";

											if(drLocal["deleted"] != DBNull.Value)
											{
												tmpDeleted = Convert.ToString(drLocal["deleted"]);
											}

											if (tmpStatusCode.Trim() == "INVR" && tmpDeleted.Trim() != "Y")
											{
												m_dsShpmntUpdt_Invalid.Tables[0].ImportRow(row);
												m_dsShpmntUpdt_Invalid.Tables[0].AcceptChanges();

												row.Delete();
												m_dsShpmntUpdt.Tables[0].AcceptChanges();
												rowIndex = 0;
												delFlag = true;

												isNotdeleted = false;

												OtherStatusFlag = true;
											}

											if (tmpStatusCode.Trim() == "HCRHQ" && tmpDeleted.Trim() != "Y")
											{
												m_dsShpmntUpdt_Invalid.Tables[0].ImportRow(row);
												m_dsShpmntUpdt_Invalid.Tables[0].AcceptChanges();

												row.Delete();
												m_dsShpmntUpdt.Tables[0].AcceptChanges();
												rowIndex = 0;
												delFlag = true;

												isNotdeleted = false;

												OtherStatusFlag = true;
											}

											if (tmpStatusCode.Trim() == "HCR" && tmpDeleted.Trim() != "Y")
											{
												isNotHaveHCR = false;
											}
										}
									}

									if (isNotdeleted && isNotHaveHCR)
									{
										m_dsShpmntUpdt_Invalid.Tables[0].ImportRow(row);
										m_dsShpmntUpdt_Invalid.Tables[0].AcceptChanges();

										row.Delete();
										m_dsShpmntUpdt.Tables[0].AcceptChanges();
										rowIndex = 0;
										delFlag = true;

										OtherStatusFlag = true;
									}

									if(!delFlag)
										rowIndex++;
								}
									#endregion
								
									#region HCRHQ case
								else if (updatingStatus == "HCRHQ")
								{
									bool isNotdeleted = true;
									bool isNotHaveHCR = true;
									bool delFlag = false;

									foreach (DataRow drLocal in tmpTrackDs.Tables[0].Rows)
									{
										if(drLocal["status_code"] != DBNull.Value)
										{
											string tmpStatusCode = drLocal["status_code"].ToString();
											string tmpDeleted = "";

											if(drLocal["deleted"] != DBNull.Value)
											{
												tmpDeleted = Convert.ToString(drLocal["deleted"]);
											}

											if (tmpStatusCode.Trim() == "INVR" && tmpDeleted.Trim() != "Y")
											{
												m_dsShpmntUpdt_Invalid.Tables[0].ImportRow(row);
												m_dsShpmntUpdt_Invalid.Tables[0].AcceptChanges();

												row.Delete();
												m_dsShpmntUpdt.Tables[0].AcceptChanges();
												rowIndex = 0;
												delFlag = true;

												isNotdeleted = false;

												OtherStatusFlag = true;
											}

											if (tmpStatusCode.Trim() == "HCRHQ" && tmpDeleted.Trim() != "Y")
											{
												m_dsShpmntUpdt_Invalid.Tables[0].ImportRow(row);
												m_dsShpmntUpdt_Invalid.Tables[0].AcceptChanges();

												row.Delete();
												m_dsShpmntUpdt.Tables[0].AcceptChanges();
												rowIndex = 0;
												delFlag = true;

												isNotdeleted = false;

												OtherStatusFlag = true;
											}

											if (tmpStatusCode.Trim() == "HCR" && tmpDeleted.Trim() != "Y")
											{
												isNotHaveHCR = false;
											}
										}
									}

									if (isNotdeleted && isNotHaveHCR)
									{
										m_dsShpmntUpdt_Invalid.Tables[0].ImportRow(row);
										m_dsShpmntUpdt_Invalid.Tables[0].AcceptChanges();

										row.Delete();
										m_dsShpmntUpdt.Tables[0].AcceptChanges();
										rowIndex = 0;
										delFlag = true;

										OtherStatusFlag = true;
									}
									if(!delFlag)
										rowIndex++;
								}
									#endregion

								else
								{
									rowIndex++;
								}
							}
						}
					}
					

					if(PODDupFlag)
						lblErrorMsg.Text = "POD Status already exists for consignment(s)";

					if(OtherStatusFlag)
						lblErrorMsg.Text = updatingStatus + " is invalid for this consignment(s)";
					//HC Return Task

					if(rbtnDom.Checked==true)
					{
						for (int i =0;i<m_dsShpmntUpdt.Tables[0].Rows.Count;i++)
						{
							m_dsShpmntUpdt.Tables[0].Rows[i]["location"]=ddlLocation.SelectedValue.ToString().Trim();
						}
					}

					ShipmentUpdateMgrDAL.AddRecordShipmntTracking(appID,enterpriseID,m_dsShpmntUpdt,txtStatusCode.Text,txtExceptioncode.Text,strDmstPickup,userID,lblBatchNo.Text.Trim());
					if(lblBatchNo.Text.Trim()!="")
					{
						Boolean First=false;
						DataSet dest =BatchManifest.GetDestinationAllType(appID,enterpriseID,(string)ViewState["BatchID"]);
						//,(string)ViewState["batch_type"]
						if(dest.Tables[0].Rows.Count>0)
						{
							string strOrigin = dest.Tables[0].Rows[0]["origin_station"].ToString(); 
					
							if(ddlLocation.SelectedValue.ToString().Trim()==strOrigin)
							{
								SWB_Emulator.SWB_Batch_Manifest_Update(appID,enterpriseID,lblBatchNo.Text.Trim(),userID,"0");
								First=true;
							}
						}
						if(First==false)
						{
							DataSet Arr_DC =BatchManifest.GetBatchDetail_ShippingTerminationLoad(appID,enterpriseID,lblBatchNo.Text.Trim(),ddlLocation.SelectedValue.ToString().Trim());
							if (Arr_DC.Tables[0].Rows.Count > 0 )
							{
								if(Arr_DC.Tables[0].Rows[0]["Arr_DT"]!=System.DBNull.Value)
								{
									SWB_Emulator.SWB_Batch_Manifest_Update(appID,enterpriseID,lblBatchNo.Text.Trim(),userID,"0");
								}
							}
						}
					}
					//HC Return Task
					m_dsShpmntUpdt = m_dsShpmntUpdt_Invalid;
					//HC Return Task
				
					ChangeSUState();

				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					if(strMsg.IndexOf("duplicate key") != -1 )
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());  
					}
					else if(strMsg.IndexOf("FK") != -1 )
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());  
					}
					else if(strMsg.IndexOf("PK") != -1 )
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());  
					} 
					else if(strMsg.IndexOf("unique constraint") != -1)
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());  
					}
					else
					{
						if(strMsg.ToLower().Equals("error error in nonquery : incorrect syntax near the keyword 'and'."))
							strMsg="Update Booking/Consignment No.";
						lblErrorMsg.Text = strMsg;
					}
					return;
				}

				//HC Return Task
				if(!PODDupFlag && !OtherStatusFlag)
				{
					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture()) + ". Please wait 5 - 10 minutes to view shipment tracking history.";
				}

				if(strDmstPickup.Equals("Domestic"))
				{
					if (txtStatusCode.Text.Trim()=="SIP"||txtStatusCode.Text.Trim()=="SOP" || txtStatusCode.Text.Trim()=="UNSIP" || txtStatusCode.Text.Trim()=="UNSOP"|| txtStatusCode.Text.Trim()=="CLS"||txtStatusCode.Text.Trim()=="UNCLS")
					{
						DataTable dtt = DTSelectDistinct(dtTemp,"CnsgBkgNo");
						string Msg = "Status code applied to "+ dtt.Rows.Count + " consignments.";
						if(lblBatchNo.Text!="")
						{
							Msg += " On batch no. "+lblBatchNo.Text+".";
						}
						lblErrorMsg.Text=Msg + " Please wait 5 - 10 minutes to view shipment tracking history.";
					}
				}
				
				//HC Return Task
				
				Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,false,m_moduleAccessRights);
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			}

			//HC Return Task
			if(PODDupFlag || OtherStatusFlag)
			{
				ResetScreenFromSave();
			}
			else
			{
				ResetScreen();
			}
			//HC Return Task
		}


		private bool ColumnEqual(object A, object B)
		{
			if (A == DBNull.Value && B == DBNull.Value) // both are DBNull.Value
 
				return true;
 
			if (A == DBNull.Value || B == DBNull.Value) // only one is DBNull.Value
 
				return false;
 
			return (A.Equals(B)); // value type standard comparison
 
		}
 
		public DataTable DTSelectDistinct(DataTable SourceTable, string FieldName)
		{
			DataTable dt = new DataTable();
			dt.Columns.Add(FieldName, SourceTable.Columns[FieldName].DataType);
			object LastValue = null;
			foreach (DataRow dr in SourceTable.Select("", FieldName))
			{
				if (LastValue == null || !(ColumnEqual(LastValue, dr[FieldName])))
				{
					LastValue = dr[FieldName];
					dt.Rows.Add(new object[] { LastValue });
				}
			}
			return dt;
		}

		//		public DataTable DTSelectDistinct(DataTable dt , String keyfield )
		//		{
		//
		//
		//			DataTable newDt ;
		//			newDt = dt.Clone();
		//			newDt.Columns["consignment_no"].Unique = false; 
		//			newDt.Columns["consignment_no"].AllowDBNull = true;
		//			ArrayList list = new ArrayList();
		//			if (dt.Rows.Count > 0 )
		//			{
		//				foreach (DataRow dr in dt.Rows)
		//				{
		//					if (list.Count > 0 && !list.Contains(dr[keyfield].ToString().Trim()))
		//					{
		//						newDt.ImportRow(dr);
		//					}
		//					else if(list.Count == 0)
		//					{
		//						list.Add(dr[keyfield].ToString().Trim());
		//						newDt.ImportRow(dr);
		//					}
		//				}
		//			}
		//			return newDt;
		//		}

		private void GetLocation()
		{
			DataSet ds = DbComboDAL.UserLocationQuery(utility.GetAppID(),utility.GetEnterpriseID());
			if (ds.Tables[0].Rows.Count > 0 )
			{
				ddlLocation.DataSource=ds.Tables[0];
				ddlLocation.DataTextField="origin_code";
				ddlLocation.DataValueField="origin_code";
				ddlLocation.DataBind();
				string loc = com.common.RBAC.RBACManager.GetUser(utility.GetAppID(),utility.GetEnterpriseID(), userID).UserLocation.ToString();
				ddlLocation.SelectedValue = loc; 
			}
		}


		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void ChangeSUState()
		{
			if(((int)ViewState["SUMode"] == (int)ScreenMode.Insert) && ((int)ViewState["SUOperation"] == (int)Operation.None))
			{
				ViewState["SUOperation"] = Operation.Insert;
			}
			else if(((int)ViewState["SUMode"] == (int)ScreenMode.Insert) && ((int)ViewState["SUOperation"] == (int)Operation.Insert))
			{
				//During saving
				ViewState["SUOperation"] = Operation.Saved;
			}
			else if(((int)ViewState["SUMode"] == (int)ScreenMode.Insert) && ((int)ViewState["SUOperation"] == (int)Operation.Saved))
			{
				ViewState["SUOperation"] = Operation.Update;
			}
			else if(((int)ViewState["SUMode"] == (int)ScreenMode.Insert) && ((int)ViewState["SUOperation"] == (int)Operation.Update))
			{
				ViewState["SUOperation"] = Operation.Saved;
			}
			else if(((int)ViewState["SUMode"] == (int)ScreenMode.ExecuteQuery) && ((int)ViewState["SUOperation"] == (int)Operation.None))
			{
				ViewState["SUOperation"] = Operation.Update;
			}
			else if(((int)ViewState["SUMode"] == (int)ScreenMode.ExecuteQuery) && ((int)ViewState["SUOperation"] == (int)Operation.Update))
			{
				ViewState["SUOperation"] = Operation.None;
			}
		}
		
		private void txtStatusCode_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeSUState();
				ViewState["isTextChanged"] = true;
			}

			if(txtStatusCode.Text.Length > 0)
			{
				//Modify By GwanG on 04-Feb-08
				
				clearGridStatus();
				txtExceptioncode.Text="";
				txtExceptionDesc.Text="";
				btnGridInsertRow.Enabled=false;
				btnScanComplete.Enabled=false;
				bool isExists = false;
				isExists = TIESUtility.IsStatusCodeExist(appID,enterpriseID,txtStatusCode.Text.Trim(),userID);
				if(isExists)
				{				
					String strType = null;
					if(rbtnDom.Checked == true)
					{
						strType = "D";
					}
					else if(rbtnPickUp.Checked == true)
					{
						strType = "P";
					}
				
					StatusCodeDesc statusCodeDesc =  TIESUtility.GetStatusCodeDesc(appID,enterpriseID,txtStatusCode.Text.Trim(),strType);
					String strStatusCd = statusCodeDesc.strStatusCode;
					String strStatusDes = statusCodeDesc.strStatusDesc;
					if(strStatusCd == null)
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"STATUS_PRESENT",utility.GetUserCulture());  						
						//txtStatusCode.Text = "";
						return;
					}
					else
					{
						txtStatusDesc.Text = strStatusDes;
					}
					DataSet dsStatusCode = new DataSet();
					dsStatusCode= TIESUtility.GetStatusAllowInSWB(appID,enterpriseID);
					string strStatusInSWB ="";
					if(dsStatusCode.Tables[0].Rows.Count>0)
					{
						foreach(DataRow dr in dsStatusCode.Tables[0].Rows)
						{
							strStatusInSWB+= dr["status_code"] +"/ ";
						}
						strStatusInSWB =strStatusInSWB.Substring(0,strStatusInSWB.LastIndexOf('/'));
					}

					if(statusCodeDesc.Allow_in_SWB=="Y")
					{
						//�ͧ�����Allow_in_SWB= N ����¹�� Allow_in_SWB= Y
						if(m_dsShpmntUpdt.Tables[0].Rows.Count>0 && !Convert.ToBoolean(Session["AllowInSWB"]))
						{
							//lblErrorMsg.Text = "You may not change to " + strStatusInSWB;
							String sScript = "";
							sScript += "<script language=javascript>";
							sScript += "  alert('You may not change to "+ strStatusInSWB + "');";
							sScript += "</script>";
							Response.Write(sScript);
							ViewState["AllowInSWB"]=true;
							if(ViewState["status_code"]!=null)
							{
								txtStatusCode.Text= ViewState["status_code"].ToString();
							}
							return;
						}
						ViewState["AllowInSWB"]=true;
						Session["AllowInSWB"] =true;

					}
					else
					{
						//�ͧ�����Allow_in_SWB= Y ����¹�� Allow_in_SWB= N
						if(m_dsShpmntUpdt.Tables[0].Rows.Count>0 && Convert.ToBoolean(Session["AllowInSWB"]))
						{
							//lblErrorMsg.Text = "You may only change to " +strStatusInSWB;
							String sScript = "";
							sScript += "<script language=javascript>";
							sScript += "  alert('You may only change to "+ strStatusInSWB + "');";
							sScript += "</script>";
							Response.Write(sScript);
							ViewState["AllowInSWB"]=false;
							if(ViewState["status_code"]!=null)
							{
								txtStatusCode.Text= ViewState["status_code"].ToString();
							}
							return;
						}
						ViewState["AllowInSWB"]=false;
						Session["AllowInSWB"] =false;
					}
					ViewState["status_code"] = txtStatusCode.Text;
					if(rbtnDom.Checked==true)
					{
						if (txtStatusCode.Text.Trim()=="SIP"||txtStatusCode.Text.Trim()=="SOP" || txtStatusCode.Text.Trim()=="UNSIP" || txtStatusCode.Text.Trim()=="UNSOP"|| txtStatusCode.Text.Trim()=="CLS"||txtStatusCode.Text.Trim()=="UNCLS")
						{
							btnScanComplete.Enabled=true;
						}
						else
						{
							btnGridInsertRow.Enabled=true;
						}
					}
					if(rbtnPickUp.Checked==true)
					{
						btnGridInsertRow.Enabled=true;
						btnScanComplete.Enabled=false;
					}
					
				}
				else 
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"STATUS_PRESENT",utility.GetUserCulture());
					txtStatusDesc.Text = "";
					return;
				}
			}
			else
			{
				btnGridInsertRow.Enabled=false;
				btnScanComplete.Enabled=false;
				txtStatusDesc.Text="";
			}
		
		}

		private void txtExceptioncode_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeSUState();
				ViewState["isTextChanged"] = true;
			}
			if(txtExceptioncode.Text.Length > 0)
			{
				//Modify by Gwang on 4-Feb-08
				if(txtStatusCode.Text.Length == 0)
				{
					//lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_STATUS",utility.GetUserCulture());  
					//return;					
					DataSet tmpStatusCode = TIESUtility.GetStatusCodeByExceptCode(appID,enterpriseID,txtExceptioncode.Text.Trim());
					if (tmpStatusCode.Tables[0].Rows.Count < 1 )
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_STATUS_CODE",utility.GetUserCulture());  
						txtExceptioncode.Text = "";
						txtExceptionDesc.Text = "";
						return;
					}
					DataRow tmpDr = tmpStatusCode.Tables[0].Rows[0];
					txtStatusCode.Text = (String)tmpDr["status_code"];

					//Get StatusCode Description
					String strType = null;
					if(rbtnDom.Checked == true)
					{
						strType = "D";
					}
					else if(rbtnPickUp.Checked == true)
					{
						strType = "P";
					}
					//enabled button 
					if(rbtnDom.Checked==true)
					{
						if (txtStatusCode.Text.Trim()=="SIP"||txtStatusCode.Text.Trim()=="SOP" || txtStatusCode.Text.Trim()=="UNSIP" || txtStatusCode.Text.Trim()=="UNSOP"|| txtStatusCode.Text.Trim()=="CLS"||txtStatusCode.Text.Trim()=="UNCLS")
						{
							btnScanComplete.Enabled=true;
						}
						else
						{
							btnGridInsertRow.Enabled=true;
						}
					}
					if(rbtnPickUp.Checked==true)
					{
						btnGridInsertRow.Enabled=true;
						btnScanComplete.Enabled=false;
					}
					
				
					StatusCodeDesc statusCodeDesc =  TIESUtility.GetStatusCodeDesc(appID,enterpriseID,txtStatusCode.Text.Trim(),strType);
					String strStatusCd = statusCodeDesc.strStatusCode;
					String strStatusDes = statusCodeDesc.strStatusDesc;
					if(strStatusCd == null)
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"STATUS_PRESENT",utility.GetUserCulture());  						
						//txtStatusCode.Text = "";
						return;
					}
					else
					{
						txtStatusDesc.Text = strStatusDes;
					}
				}

				ExceptionCodeDesc excepCodeDesc =  TIESUtility.GetExceptionCodeDesc(appID,enterpriseID,txtExceptioncode.Text.Trim(),txtStatusCode.Text.Trim());
				String strExcepCd = excepCodeDesc.strExceptionCode;
				String strExcepDes = excepCodeDesc.strExceptionDesc;
				if(strExcepCd == null)
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_STATUS_CODE",utility.GetUserCulture());  
					txtExceptioncode.Text = "";
					txtExceptionDesc.Text = "";
					return;
				}
				else
				{
					txtExceptionDesc.Text = strExcepDes;


				}
			}
		}

		private void btnStatusCodeSearch_Click(object sender, System.EventArgs e)
		{
			String status_type =null;
			if (rbtnDom.Checked==true)
			{
				status_type="D";
			}
			else if (rbtnPickUp.Checked==true)
			{
				status_type="P";
			}
			OpenWindowpage("StatusCodePopup.aspx?FORMID=ShipmentUpdate&STATUSCODE="+txtStatusCode.Text+"&EXCEPTCODE="+txtExceptioncode.Text+"&STATUSTYPE="+status_type+" ");
		}

		private void btnExcepDescSrch_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("ExceptionCodePopup.aspx?FORMID=ShipmentUpdate&EXCEPTIONCODE="+txtExceptioncode.Text+"&STATUSCODE="+txtStatusCode.Text);
		}

		private void rbtnPickUp_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			txtExceptioncode.Text = "";
			txtExceptionDesc.Text = "";
			txtStatusCode.Text = "";
			txtStatusDesc.Text = "";
			

			if(rbtnPickUp.Checked == true)
			{
				lblErrorMsg.Text="";
				pnImport.Visible=false;
				dgImport.DataSource=null;
				dgImport.DataBind();
				dgImport.Visible=false;
				dgShipUpdate.Visible=true;

				dgShipUpdate.Columns[2].HeaderText = Utility.GetLanguageText(ResourceType.ScreenLabel,"Booking No",utility.GetUserCulture());
				strDmstPickup = "Pickup";
				ViewState["headText"] = Utility.GetLanguageText(ResourceType.ScreenLabel,"Booking No",utility.GetUserCulture());
				dgShipUpdate.Columns[3].Visible=false;
				dgShipUpdate.Columns[5].Visible=true;
				lblLocationTop.Visible=false;
				ddlLocation.Visible=false;
			}
			else if(rbtnPickUp.Checked == false)
			{
				lblErrorMsg.Text="";
				pnImport.Visible=true;
				dgImport.DataSource=null;
				dgImport.DataBind();
				dgImport.Visible=false;
				dgShipUpdate.Visible=true;

				dgShipUpdate.Columns[2].HeaderText = Utility.GetLanguageText(ResourceType.ScreenLabel,"Consignment No",utility.GetUserCulture());
				strDmstPickup = "Domestic";
				ViewState["headText"] = Utility.GetLanguageText(ResourceType.ScreenLabel,"Consignment No",utility.GetUserCulture());
			}
			//Get the empty dataset.
			m_dsShpmntUpdt = ShipmentUpdateMgrDAL.GetEmptyshipupdDS();
			BindGrid();
			btnGridInsertRow.Enabled=false;
			btnScanComplete.Enabled=false;
			lblBatch.Text="";
			lblBatchNo.Text="";
		}

		private void rbtnDom_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			txtExceptioncode.Text = "";
			txtExceptionDesc.Text = "";
			txtStatusCode.Text = "";
			txtStatusDesc.Text = "";

			if(rbtnDom.Checked == false)
			{
				lblErrorMsg.Text="";
				pnImport.Visible=false;
				dgImport.DataSource=null;
				dgImport.DataBind();
				dgImport.Visible=false;
				dgShipUpdate.Visible=true;

				dgShipUpdate.Columns[2].HeaderText = Utility.GetLanguageText(ResourceType.ScreenLabel,"Booking No",utility.GetUserCulture());
				strDmstPickup = "Pickup";
				ViewState["headText"] = Utility.GetLanguageText(ResourceType.ScreenLabel,"Booking No",utility.GetUserCulture());
			}
			else if(rbtnDom.Checked == true)
			{
				lblErrorMsg.Text="";
				pnImport.Visible=true;
				dgImport.DataSource=null;
				dgImport.DataBind();
				dgImport.Visible=false;
				dgShipUpdate.Visible=true;

				dgShipUpdate.Columns[2].HeaderText = Utility.GetLanguageText(ResourceType.ScreenLabel,"Consignment No",utility.GetUserCulture());
				strDmstPickup = "Domestic";
				ViewState["headText"] = Utility.GetLanguageText(ResourceType.ScreenLabel,"Consignment No",utility.GetUserCulture());
				lblLocationTop.Visible=true;
				ddlLocation.Visible=true;
				dgShipUpdate.Columns[3].Visible=true;
				dgShipUpdate.Columns[5].Visible=false;
			}

			//Get the empty dataset.
			m_dsShpmntUpdt = ShipmentUpdateMgrDAL.GetEmptyshipupdDS();
			BindGrid();
			btnGridInsertRow.Enabled=false;
			btnScanComplete.Enabled=false;

			

		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			//			if(m_dsShpmntUpdt.Tables[0].Rows.Count>0 && ViewState["AllowInSWB"]!=null)
			//
			//			{
			//				if(Convert.ToBoolean(ViewState["AllowInSWB"])!= Convert.ToBoolean(Session["AllowInSWB"]))
			//				return;
			//			}
			//			Save();




			lblErrorMsg.Text="";
			if(txtExceptioncode.Text != "PODEX34")
			{ 
				if(rbtnDom.Checked==true)
				{
					//if (txtStatusCode.Text.Trim()=="SIP"||txtStatusCode.Text.Trim()=="SOP" || txtStatusCode.Text.Trim()=="UNSIP" || txtStatusCode.Text.Trim()=="UNSOP"|| txtStatusCode.Text.Trim()=="CLS"||txtStatusCode.Text.Trim()=="UNCLS")
					//{
					//	DataTable dt ;
					//	if(m_dsShpmntUpdt==null)
					//	{
					//lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DETAIL",utility.GetUserCulture());  
					//		return;
					//	}
					//						if (dt.Rows.Count>0)
					//						{
					//							string statusCode = txtStatusCode.Text.Trim();
					//							String sUrl = "Manifest/PopUp_CreateUpdateBatch.aspx?strFormID=ShipmentUpdate";
					//							sUrl +="&status="+ statusCode;
					//							sUrl +="&BATCH_NO_CID="+ lblBatchNo.ClientID;
					//							sUrl +="&usrloc="+ ddlLocation.SelectedItem.Text;
					//							ArrayList paramList = new ArrayList();
					//							paramList.Add(sUrl);
					//							String sScript = Utility.GetScriptPopupSetSize("OpenSetSize.js",paramList,400,500);
					//							Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
					//						}
					//						else
					//						{
					//							lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DETAIL",utility.GetUserCulture());  
					//							return;
					//						}
					//	}
					//else
					//{
					Save();
					ResetScreen();	
					Utility ut = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
					ut.SetInitialFocus(txtStatusCode);
					//}
				}
				else
				{
					Save();
					ResetScreen();	
					Utility ut = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
					ut.SetInitialFocus(txtStatusCode);
				}

			}
			else
			{
				txtExceptioncode.Text = "";
				lblErrorMsg.Text = "Can't Update Exception Code PODEX34";
			}   
		}

		private void btnGridInsertRow_Click(object sender, System.EventArgs e)
		{	
			lblErrorMsg.Text = "";

			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);

			if(TxtStatusDateTime.Text.Length <=0)
			{
				DateTime dtScanDt = DateTime.Now;
				TxtStatusDateTime.Text = dtScanDt.ToString("dd/MM/yyyy HH:mm");				
			}
			com.common.classes.User user = com.common.RBAC.RBACManager.GetUser(utility.GetAppID(), utility.GetEnterpriseID(), utility.GetUserID());
	
			if(m_dsShpmntUpdt == null)
			{
				m_dsShpmntUpdt = ShipmentUpdateMgrDAL.GetEmptyshipupdDS();
			}

			//�ó� Status Code �� Auto LOCATION on Create = Y ���Ѻ��प���Ѵ�����¨�� by X FEB 06 08
			if(txtStatusCode.Text.Trim().Length==0)
			{
				btnGridInsertRow.Enabled=false;
				return;
			}
			//status code ����� Exception code ��� require exception code ����
			if(txtStatusCode.Text.Trim().Length>0)
			{
				DataSet dsExceptionCode =SysDataMgrDAL.GetExceptionCodeDS(appID,enterpriseID ,txtStatusCode.Text.Trim());
				if(dsExceptionCode.Tables[0].Rows.Count>0&&txtExceptioncode.Text.Trim().Length==0)
				{
					lblErrorMsg.Text = "Exception code is required for this status code";
					//btnGridInsertRow.Enabled=false;
					return;
				}
			}
			if(txtStatusCode.Text.Length > 0)
			{
				String strType = null;
				if (rbtnDom.Checked==true)
				{
					strType="D";
				}
				else if (rbtnPickUp.Checked==true)
				{
					strType="P";
				}
				StatusCodeDesc statusCodeDesc =  TIESUtility.GetStatusCodeDesc(appID,enterpriseID,txtStatusCode.Text.Trim(),strType);
				String strStatusCd = statusCodeDesc.strStatusCode;
				String strStatusDes = statusCodeDesc.strStatusDesc;
				String strAuto_Location = statusCodeDesc.strAuto_Location;

				if(strStatusCd == null)
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"STATUS_PRESENT",utility.GetUserCulture());  
					//txtStatusCode.Text = "";
					return;
				}
				else
				{
					txtStatusDesc.Text = strStatusDes;
					//BY X FEB 06 08 ����ҡ�൵���鴹�� �ѹ����� AUTO PLC = N �������� User's Location �Ш��
					//					StatusCodeDesc statusCodeDesc1 =  TIESUtility.GetStatusCodeInfo(appID,enterpriseID,txtStatusCode.Text.Trim());
					//					String strAuto_Location = statusCodeDesc1.strAuto_Location;


					if(strAuto_Location!=null)
					{
						if(strAuto_Location.Equals("Y"))
						{
							if(TxtStatusDateTime.Text.Length > 0)
							{
								m_dsShpmntUpdt.EnforceConstraints = false;
								ShipmentUpdateMgrDAL.AddNewRowInShpmntUpdt(ref m_dsShpmntUpdt,TxtStatusDateTime.Text,user.UserLocation);
							}
							else
							{
								DateTime dtScanDt = DateTime.Now;
								ShipmentUpdateMgrDAL.AddNewRowInShpmntUpdt(ref m_dsShpmntUpdt,dtScanDt.ToString("dd/MM/yyyy hh:mm"),user.UserLocation);
								TxtStatusDateTime.Text = dtScanDt.ToString("dd/MM/yyyy HH:mm");
							}						
						}
						else
						{
							if(TxtStatusDateTime.Text.Length > 0)
							{
								m_dsShpmntUpdt.EnforceConstraints = false;
								ShipmentUpdateMgrDAL.AddNewRowInShpmntUpdt(ref m_dsShpmntUpdt,TxtStatusDateTime.Text,"");
							}
							else
							{
								DateTime dtScanDt = DateTime.Now;
								ShipmentUpdateMgrDAL.AddNewRowInShpmntUpdt(ref m_dsShpmntUpdt,dtScanDt.ToString("dd/MM/yyyy hh:mm"),"");
								TxtStatusDateTime.Text = dtScanDt.ToString("dd/MM/yyyy HH:mm");
							}
						}
					}
					else	
					{
						if(TxtStatusDateTime.Text.Length > 0)
						{
							m_dsShpmntUpdt.EnforceConstraints = false;
							ShipmentUpdateMgrDAL.AddNewRowInShpmntUpdt(ref m_dsShpmntUpdt,TxtStatusDateTime.Text,"");
						}
						else
						{
							DateTime dtScanDt = DateTime.Now;
							ShipmentUpdateMgrDAL.AddNewRowInShpmntUpdt(ref m_dsShpmntUpdt,dtScanDt.ToString("dd/MM/yyyy hh:mm"),"");
							TxtStatusDateTime.Text = dtScanDt.ToString("dd/MM/yyyy HH:mm");
						}
					}

				}
			}
			else if(TxtStatusDateTime.Text.Length > 0)
			{
				m_dsShpmntUpdt.EnforceConstraints = false;
				ShipmentUpdateMgrDAL.AddNewRowInShpmntUpdt(ref m_dsShpmntUpdt,TxtStatusDateTime.Text,user.UserLocation);
			}
			else
			{
				DateTime dtScanDt = DateTime.Now;
				TxtStatusDateTime.Text = dtScanDt.ToString("dd/MM/yyyy HH:mm");				
				ShipmentUpdateMgrDAL.AddNewRowInShpmntUpdt(ref m_dsShpmntUpdt,TxtStatusDateTime.Text,user.UserLocation);
				
			}
			//���óը��

			dgShipUpdate.EditItemIndex = m_dsShpmntUpdt.Tables[0].Rows.Count - 1;
			btnSave.Enabled=false;
			BindGrid();
			RequiredFieldValidator validatePgk = (RequiredFieldValidator)dgShipUpdate.Items[ m_dsShpmntUpdt.Tables[0].Rows.Count - 1].FindControl("validatePgk");	
			validatePgk.Enabled	 = Convert.ToBoolean(Session["AllowInSWB"]);

			if(txtStatusCode.Text.Trim()=="POD")
			{
				dgShipUpdate.Columns[6].Visible=true;
				RequiredFieldValidator reqConsnee = (RequiredFieldValidator)dgShipUpdate.Items[ m_dsShpmntUpdt.Tables[0].Rows.Count - 1].FindControl("reqConsnee");	
				reqConsnee.Enabled=true;
			}
			else
			{
				dgShipUpdate.Columns[6].Visible=false;
				RequiredFieldValidator reqConsnee = (RequiredFieldValidator)dgShipUpdate.Items[ m_dsShpmntUpdt.Tables[0].Rows.Count - 1].FindControl("reqConsnee");	
				reqConsnee.Enabled=false;
			}


			if(!validatePgk.Enabled)
			{
				dgShipUpdate.Columns[3].Visible= false;
			}
			else
			{
				dgShipUpdate.Columns[3].Visible= true;

			}
			btnGridInsertRow.Enabled = false;
			getPageControls(Page);

			TextBox txtCons=(TextBox)dgShipUpdate.Items[ m_dsShpmntUpdt.Tables[0].Rows.Count - 1].FindControl("txtCnsgBkNo");
			setFocusCtrl(txtCons.ClientID);

		}

		private void setFocusCtrl(string ctrlName)
		{
			string script="<script langauge='javacript'>";
			script+= "document.all['"+ ctrlName +"'].focus();";
			script+= "</script>";
			Page.RegisterStartupScript("setFocus",script);
		
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			ResetScreen();	
		}

		private void ResetScreen()
		{
			GetLocation();
			txtStatusCode.Text = "";
			txtStatusDesc.Text = "";
			txtExceptioncode.Text = "";
			txtExceptionDesc.Text = "";
			TxtStatusDateTime.Text = "";
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,false,m_moduleAccessRights);
			Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,true,m_moduleAccessRights);
			ViewState["SUMode"] = ScreenMode.Insert;
			ViewState["SUOperation"] = Operation.Insert;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,false,m_moduleAccessRights);
			rbtnDom.Checked = true;
			strDmstPickup = "Domestic";
			dgShipUpdate.Columns[2].HeaderText = Utility.GetLanguageText(ResourceType.ScreenLabel,"Consignment No",utility.GetUserCulture());
			ViewState["isTextChanged"] = false;
			DateTime dtScanDt = DateTime.Now;
			TxtStatusDateTime.Text = dtScanDt.ToString("dd/MM/yyyy HH:mm");
			m_dsShpmntUpdt = ShipmentUpdateMgrDAL.GetEmptyshipupdDS();
			//			ShipmentUpdateMgrDAL.AddNewRowInShpmntUpdt(m_dsShpmntUpdt);
			//			dgShipUpdate.EditItemIndex = m_dsShpmntUpdt.Tables[0].Rows.Count - 1;
			btnGridInsertRow.Enabled = false;
			btnScanComplete.Enabled = false;

			BindGrid();
			lblBatchNo.Text="";
			lblBatch.Text="";
			dgShipUpdate.Visible=true;
			dgImport.Visible=false;
		}

		private void TxtStatusDateTime_TextChanged(object sender, System.EventArgs e)
		{
		
		}

		//HC Return Task
		private void ResetScreenFromSave()
		{
			GetLocation();
			txtStatusCode.Text = "";
			txtStatusDesc.Text = "";
			txtExceptioncode.Text = "";
			txtExceptionDesc.Text = "";
			TxtStatusDateTime.Text = "";
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,false,m_moduleAccessRights);
			Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,true,m_moduleAccessRights);
			ViewState["SUMode"] = ScreenMode.Insert;
			ViewState["SUOperation"] = Operation.Insert;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,false,m_moduleAccessRights);
			rbtnDom.Checked = true;
			strDmstPickup = "Domestic";
			dgShipUpdate.Columns[2].HeaderText = Utility.GetLanguageText(ResourceType.ScreenLabel,"Consignment No",utility.GetUserCulture());
			ViewState["isTextChanged"] = false;
			DateTime dtScanDt = DateTime.Now;
			TxtStatusDateTime.Text = dtScanDt.ToString("dd/MM/yyyy HH:mm");
			btnGridInsertRow.Enabled = false;
			BindGrid();
			lblBatchNo.Text="";
			lblBatch.Text="";
		}

		private void dgShipUpdate_ItemCreated(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Header)
			{
				if(rbtnDom.Checked == false)
				{
					e.Item.Cells[2].Text  = (string)ViewState["headText"]; //"Booking No";//Utility.GetLanguageText(ResourceType.ScreenLabel,"Booking No",utility.GetUserCulture());
				}
				else if(rbtnDom.Checked == true)
				{
					e.Item.Cells[2].Text  = (string)ViewState["headText"]; //"Consignment No";//Utility.GetLanguageText(ResourceType.ScreenLabel,"Consignment No",utility.GetUserCulture());
				}
			}
		}

		private void btnSubmitHidden_Click(object sender, System.EventArgs e)
		{
			if (Session["BatchNo"]!=null)
			{
				lblBatchNo.Text=Session["BatchNo"].ToString();
				lblBatch.Text = "Batch: " + Session["BatchNo"].ToString()+" selected";
				btnScanComplete.Enabled=false;
				btnGridInsertRow.Enabled=true;
				ViewState["BatchID"]=Session["BatchID"];
				Session.Remove("BatchID");
				Session.Remove("BatchNo");
				Session.Remove("CreateBatchState");
				//Save();
			}
		}

		private void clearGridStatus()
		{
			m_dsShpmntUpdt.Tables[0].Rows.Clear();
			m_dsShpmntUpdt.AcceptChanges();
			dgShipUpdate.EditItemIndex = -1;
			//			btnGridInsertRow.Enabled=false;
			//			btnScanComplete.Enabled=false;
			lblBatch.Text="";
			lblBatchNo.Text="";
			//btnGridInsertRow.Enabled = true;
			BindGrid();
			if(txtStatusCode.Text.Trim()=="POD")
			{
				dgShipUpdate.Columns[6].Visible=true;
				//					RequiredFieldValidator reqConsnee = (RequiredFieldValidator)dgShipUpdate.Items[ m_dsShpmntUpdt.Tables[0].Rows.Count - 1].FindControl("reqConsnee");	
				//					reqConsnee.Enabled=true;
			}
			else
			{
				dgShipUpdate.Columns[6].Visible=false;
				//					RequiredFieldValidator reqConsnee = (RequiredFieldValidator)dgShipUpdate.Items[ m_dsShpmntUpdt.Tables[0].Rows.Count - 1].FindControl("reqConsnee");	
				//					reqConsnee.Enabled=false;
			}
		}

		
		private void btnClearHide_Click(object sender, System.EventArgs e)
		{
			clearGridStatus();
		}

		private void btnScanComplete_Click(object sender, System.EventArgs e)
		{
			if(TxtStatusDateTime.Text.Length <=0)
			{
				DateTime dtScanDt = DateTime.Now;
				TxtStatusDateTime.Text = dtScanDt.ToString("dd/MM/yyyy HH:mm");				
			}

			string statusCode = txtStatusCode.Text.Trim();
			String sUrl = "Manifest/PopUp_CreateUpdateBatch.aspx?strFormID=ShipmentUpdate";
			sUrl +="&status="+ statusCode;
			sUrl +="&usrloc="+ ddlLocation.SelectedItem.Text;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScriptPopupSetSize("OpenSetSize.js",paramList,400,500);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnRegen_Click(object sender, System.EventArgs e)
		{
			if(Session["dtTemp"]!=null)
			{
				DataTable dt = (DataTable)Session["dtTemp"];
				m_dsShpmntUpdt.Tables.RemoveAt(0);
				m_dsShpmntUpdt.AcceptChanges();
				m_dsShpmntUpdt.Tables.Add(dt);
				m_dsShpmntUpdt.AcceptChanges();
				dgShipUpdate.EditItemIndex = -1;
				btnSave.Enabled=true;
				BindGrid();
				btnScanComplete.Enabled=false;
				btnGridInsertRow.Enabled=true;
				lblErrorMsg.Text="";
				Session.Remove("dtTemp");
			}
		}

		private void btnPkgConfirm_Click(object sender, System.EventArgs e)
		{
			if(ViewState["Eindex"]!=null)
			{
				int Gridindex=(int)ViewState["Eindex"];
				TextBox txtCnsgBkNo = (TextBox)dgShipUpdate.Items[Gridindex].FindControl("txtCnsgBkNo");
				TextBox txtPkgNo = (TextBox)dgShipUpdate.Items[Gridindex].FindControl("txtPackage");
				TextBox txtPackage = (TextBox)dgShipUpdate.Items[Gridindex].FindControl("txtPackage");
				TextBox txtStatusDtTime = (TextBox)dgShipUpdate.Items[Gridindex].FindControl("txtStatusDtTime");
				TextBox txtPersonInchrg = (TextBox)dgShipUpdate.Items[Gridindex].FindControl("txtPersonInchrg");
				TextBox txtRemarks = (TextBox)dgShipUpdate.Items[Gridindex].FindControl("txtRemarks");
				DataRow drEach = m_dsShpmntUpdt.Tables[0].Rows[Gridindex];
				drEach["CnsgBkgNo"] = txtCnsgBkNo.Text.Trim();
				if(txtStatusDtTime != null)
				{
					DataRow drCurrent = m_dsShpmntUpdt.Tables[0].Rows[Gridindex];
					drCurrent["tracking_datetime"] =  DateTime.ParseExact(txtStatusDtTime.Text,"dd/MM/yyyy HH:mm",null);
				}
				
				if(txtPersonInchrg != null)
				{
					DataRow drCurrent = m_dsShpmntUpdt.Tables[0].Rows[Gridindex];
					drCurrent["person_incharge"] = txtPersonInchrg.Text;
				}

				if(txtRemarks != null)
				{
					DataRow drCurrent = m_dsShpmntUpdt.Tables[0].Rows[Gridindex];
					drCurrent["remarks"] = txtRemarks.Text;
				}
				if(txtPackage != null)
				{
					DataRow drCurrent = m_dsShpmntUpdt.Tables[0].Rows[Gridindex];
					drCurrent["pkg_no"] = txtPackage.Text;
				}
				m_dsShpmntUpdt.Tables[0].Rows[Gridindex].AcceptChanges();
				dgShipUpdate.EditItemIndex = -1;
				btnSave.Enabled=true;
				btnGridInsertRow.Enabled=true;
				BindGrid();
			}
		}

		private void btnImport_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			if(uploadFiletoServer() && ViewState["ImpStatusFileName"] != null)
			{
				Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);								
				string pathFn = ViewState["ImpStatusFileName"].ToString();
				if(ViewState["TargerSQLServerFolder"] != null && ViewState["TargerSQLServerFolder"].ToString() != "")
				{
					FileStream fs = new FileStream(pathFn, FileMode.Open, FileAccess.Read);
					BinaryReader br = new BinaryReader(fs);
					Byte[] bytes = br.ReadBytes((Int32)fs.Length);
					br.Close();
					fs.Close();

					pathFn = ShipmentUpdateMgrDAL.CopyVARBINARY2SQLServerFolder(utility.GetAppID(), utility.GetEnterpriseID(),ViewState["ImpFileName"].ToString(),ViewState["TargerSQLServerFolder"].ToString(),bytes);
				}				
				
				DataSet result = ShipmentUpdateMgrDAL.InsertStaging(utility.GetAppID(), utility.GetEnterpriseID(),utility.GetUserID(),pathFn,ddlLocation.SelectedValue.Trim());

				int tbResult=-1;
				int tbCount = -1;
				for(int i=0;i < result.Tables.Count;i++)
				{
					if(result.Tables[i].Columns["booking_no"] != null)
					{
						tbResult = i;
					}
					if(result.Tables[i].Columns["Status_Inserted_Count"] != null)
					{
						tbCount=i;
					}
				}
				if(tbResult!=-1)
				{
					dgImport.DataSource = result.Tables[tbResult];
				}
				else
				{
					dgImport.DataSource = null;
				}				
				dgImport.DataBind();
				dgImport.Visible=true;
				dgShipUpdate.Visible=false;
				int inserted=0;
				int status_inserted=0;
				if(tbCount != -1 && tbResult != -1)
				{
					if(result.Tables[tbCount].Rows.Count>0 && result.Tables[tbCount].Rows[0]["Data_Count"] != null 
						&& result.Tables[tbCount].Rows[0]["Data_Count"].ToString().Trim() != "")
					{
						inserted=int.Parse(result.Tables[tbCount].Rows[0]["Data_Count"].ToString());
						inserted=inserted-result.Tables[tbResult].Rows.Count;					
					}
				
					if(result.Tables[tbCount].Rows.Count>0 && result.Tables[tbCount].Rows[0]["Status_Inserted_Count"] != null 
						&& result.Tables[tbCount].Rows[0]["Status_Inserted_Count"].ToString().Trim() != "")
					{
						status_inserted=int.Parse(result.Tables[tbCount].Rows[0]["Status_Inserted_Count"].ToString());		
					}
				}

				lblErrorMsg.Text=status_inserted.ToString() + " status codes inserted for " + inserted + " consignments";
			}
		}

		private bool uploadFiletoServer()
		{
			bool status = false;
			ViewState["ImpStatusFileName"] = null;
			ViewState["ImpFileName"] = null;
			if((inFile.PostedFile != null ) && (inFile.PostedFile.ContentLength > 0))
			{
				string extension = System.IO.Path.GetExtension(inFile.PostedFile.FileName);
				if(extension.ToUpper() == ".XLS" || extension.ToUpper() == ".XLSX")
				{
					ViewState["ImpFileName"]=System.IO.Path.GetFileName(inFile.PostedFile.FileName);
					string fn = DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + System.IO.Path.GetFileName(inFile.PostedFile.FileName);
					string path = Server.MapPath("Excel") + "\\";
					string pathFn = path + fn;		
					string pathShare=(string)System.Configuration.ConfigurationSettings.AppSettings["ImportStatusSharedPath"];
					string mappedPath = System.IO.Path.Combine(pathShare,fn);
					try
					{																		
						if (System.IO.File.Exists(mappedPath)) 						
							System.IO.File.Delete(mappedPath);
						inFile.PostedFile.SaveAs(mappedPath);	
						ViewState["ImpStatusFileName"] = mappedPath;
						lblErrorMsg.Text = "";
						txtFilePath.Text="";
						status = true;


					}
					catch(Exception ex)
					{
						this.lblErrorMsg.Text = ex.Message;
						status = false;
						throw ex;						
					}					
				}
				else
				{
					this.lblErrorMsg.Text = "Not a valid Excel Workbook.";
					txtFilePath.Text = "";
					status = false;
				}
			}
			else
			{
				lblErrorMsg.Text = "Please select a file to upload.";
				status = false;
			}
			return status;
		}

	
				
		private void hddConNo_ServerClick(object sender, System.EventArgs e)
		{
			string ConNo="";
			
			if(Txt_hddConNo.Text.Trim().Length > 0 &&Utility.ValidateConsignmentNo(Txt_hddConNo.Text.Trim(), ref ConNo)==false)
			{
				//				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSNO_INVALID",utility.GetUserCulture()); 
				//				SetInitialFocus(txtConsigNo);
				return;
			}
			else
			{
				msTextBox txtPackage = (msTextBox)dgShipUpdate.Items[dgShipUpdate.EditItemIndex].FindControl("txtPackage");
				lblErrorMsg.Text="";
				Txt_hddConNo.Text=ConNo;
				string script="<script langauge='javacript'>";
				script+= "ReturnValidateConNo('"+ ConNo + "');";
				if(txtPackage != null)
				{
					script+= "document.getElementById('" + txtPackage.ClientID + "').focus();";
				}
				script+= "</script>";
				Page.RegisterStartupScript("Return"+DateTime.Now.ToString("ddMMyyhhmmss"),script);
			}
		}	
		//HC Return Task
	}	
}
