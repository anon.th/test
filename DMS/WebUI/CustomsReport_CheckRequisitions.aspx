<%@ Page language="c#" Codebehind="CustomsReport_CheckRequisitions.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsReport_CheckRequisitions1" %>
<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomsReport_CheckRequisitions</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="vs_snapToGrid" content="true">
		<meta name="vs_showGrid" content="true">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
		function SetRadioFocus()
		{
			var radioButtons=document.getElementsByName('OptionalReport');
	        
			for (var x = 0; x < radioButtons.length; x ++) 
			{
				alert(radioButtons[x].id);
				if (radioButtons[x].checked) 
					{
						alert("You checked " + radioButtons[x].id);
					}
			}
			
		}
		
		function getCheckedRadio() {

			var radioButtons = document.getElementsById('rbtNoDetail');
			alert(radioButtons.id);
		}
		
		
		
		function funcDisBack(){
			history.go(+1);
		}

        function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }
        function validate(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventdefault) theEvent.preventdefault();
			}
		}
		
		function ValidateConsignmentList(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[-/,_a-zA-Z0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		function RemoveBadConsignmentList(strTemp, obj) {
            strTemp = strTemp.replace(/[^-/,_a-zA-Z0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteConsignmentList(obj) {
            setTimeout(function () {
                RemoveBadConsignmentList(obj.value, obj);
            }, 1); //or 4
        }
        
        function ValidateDecNoList(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[,_0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		function RemoveBadDecNoList(strTemp, obj) {
            strTemp = strTemp.replace(/[^,_0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteDecNoList(obj) {
            setTimeout(function () {
                RemoveBadDecNoList(obj.value, obj);
            }, 1); //or 4
        }
        
        function ValidateMAWBList(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[-,_0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		function RemoveBadMAWBList(strTemp, obj) {
            strTemp = strTemp.replace(/[^-,_0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteMAWBList(obj) {
            setTimeout(function () {
                RemoveBadMAWBList(obj.value, obj);
            }, 1); //or 4
        }
        
        
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="CustomsReport_CheckRequisitions" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 900px; HEIGHT: 1543px; TOP: 34px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tbody>
						<tr>
							<td colSpan="4" align="left"><asp:label id="Label1" runat="server" CssClass="mainTitleSize" Height="27px" Width="421px">
								Cheque Requisition Listings</asp:label></td>
						</tr>
						<tr>
							<td colspan="4">
								<asp:button id="btnQry" runat="server" CssClass="queryButton" CausesValidation="False" Text="Query"></asp:button><asp:button id="btnGenerateReport" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Generate Report"></asp:button>
							</td>
						</tr>
						<tr>
							<td style="HEIGHT: 19px" colSpan="4"><asp:label id="lbError" runat="server" Font-Bold="true" Font-Size="X-Small" ForeColor="Red"></asp:label></td>
						</tr>
						<tr>
							<td colspan="4" style="HEIGHT: 145px">
								<fieldset style="WIDTH:43%">
									<legend style="COLOR: black">
										Dates</legend>
									<table style="MARGIN: 5px" id="table_fieldset_Dates">
										<tbody>
											<tr>
												<td colSpan="3"><asp:radiobutton id="rbRequested" runat="server" CssClass="tableRadioButton" Height="22px" Width="100px"
														Text="Requested" AutoPostBack="true" GroupName="QueryByDate" Checked="true"></asp:radiobutton>&nbsp;<asp:radiobutton id="rbApproved" runat="server" CssClass="tableRadioButton" Height="22px" Width="117px"
														Text="Approved" AutoPostBack="true" GroupName="QueryByDate"></asp:radiobutton>&nbsp;<asp:radiobutton id="rbPaid" runat="server" CssClass="tableRadioButton" Height="22px" Width="127px"
														Text="Paid" AutoPostBack="true" GroupName="QueryByDate"></asp:radiobutton></td>
											</tr>
											<tr>
												<td colSpan="3"><asp:radiobutton id="rbMonth" runat="server" CssClass="tableRadioButton" Height="21px" Width="73px"
														Text="Month" AutoPostBack="true" GroupName="EnterDate" Checked="true"></asp:radiobutton>&nbsp;
													<asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Height="19px" Width="82px"></asp:dropdownlist>&nbsp;
													<asp:label id="lblYear" runat="server" CssClass="tableLabel" Height="15px" Width="30px">Year</asp:label>&nbsp;&nbsp;
													<cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" MaxLength="5" TextMaskType="msNumeric"
														NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></td>
											</tr>
											<tr>
												<td style="HEIGHT: 20px" colSpan="3"><asp:radiobutton id="rbPeriod" runat="server" CssClass="tableRadioButton" Width="62px" Text="Period"
														AutoPostBack="true" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
													<cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
														TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
													&nbsp;
													<cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" MaxLength="10" TextMaskType="msDate"
														TextMaskString="99/99/9999"></cc1:mstextbox></td>
											</tr>
											<tr>
												<td colSpan="3">
													<asp:radiobutton id="rbDate" runat="server" Width="74px" CssClass="tableRadioButton" Text="Date"
														Height="22px" GroupName="EnterDate" AutoPostBack="true"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtdate" runat="server" Width="82" CssClass="textField" TextMaskType="msDate"
														MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>
												</td>
											</tr>
										</tbody>
									</table>
								</fieldset>
							</td>
						</tr>
						<tr>
							<td width="15%">
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label2">Cheque Number</asp:label>
							</td>
							<td width="12%">
								<asp:textbox style="Z-INDEX: 0" tabIndex="17" runat="server" CssClass="textField" ID="txtChequeNumberFrom"
									Width="84px" onpaste="AfterPasteNonNumber(this)" onkeypress="validate(event)" MaxLength="15"></asp:textbox>
							</td>
							<td width="3%">
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label3">To</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="18" runat="server" CssClass="textField" ID="txtChequeNumberTo"
									Width="110" onpaste="AfterPasteNonNumber(this)" onkeypress="validate(event)" MaxLength="15"></asp:textbox>
							</td>
						</tr>
						<tr>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label4">Requisition Number</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="19" runat="server" CssClass="textField" ID="txtRequisitionNumberFrom"
									Width="84px" onpaste="AfterPasteNonNumber(this)" onkeypress="validate(event)" MaxLength="10"></asp:textbox>
							</td>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label5">To</asp:label>
							</td>
							<td>
								<asp:textbox style="Z-INDEX: 0" tabIndex="20" runat="server" CssClass="textField" ID="txtRequisitionTo"
									Width="110" onpaste="AfterPasteNonNumber(this)" onkeypress="validate(event)" MaxLength="10"></asp:textbox>
							</td>
						</tr>
						<tr>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label6">Requisition Type</asp:label>
							</td>
							<td colspan="3">
								<asp:dropdownlist style="Z-INDEX: 0" tabIndex="21" runat="server" AutoPostBack="True" ID="ddlRequisitionType"
									Width="84px"></asp:dropdownlist>
							</td>
						</tr>
						<tr>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label7">Payee Name</asp:label>
							</td>
							<td colspan="3">
								<asp:textbox style="Z-INDEX: 0" tabIndex="22" runat="server" CssClass="textField" ID="txtPayeeName"
									Width="128"></asp:textbox>
							</td>
						</tr>
						<tr>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label8">Receipt Number</asp:label>
							</td>
							<td colspan="3">
								<asp:textbox style="Z-INDEX: 0" tabIndex="23" runat="server" CssClass="textField" ID="txtReceiptNumber"
									Width="84"></asp:textbox>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<fieldset style="WIDTH:43%; HEIGHT:2px" tabIndex="0">
									<legend style="COLOR: black">
										Show on report only if Cheque Requisition has</legend>
									<table style="MARGIN: 5px" id="table_fieldset_ShowReport">
										<tr>
											<td colSpan="3"><asp:radiobutton id="rbtNoDetail" runat="server" CssClass="tableRadioButton" Height="22px" Width="100px"
													Text="No details" GroupName="OptionalReport" tabIndex="25"></asp:radiobutton><asp:radiobutton id="rbtSomeDetail" runat="server" CssClass="tableRadioButton" Height="22px" Width="117px"
													Text="Some details" GroupName="OptionalReport" tabIndex="25"></asp:radiobutton><asp:radiobutton id="rbtIgnoreThis" runat="server" CssClass="tableRadioButton" Height="22px" Width="127px"
													Text="Ignore this" GroupName="OptionalReport" Checked="True" tabIndex="25"></asp:radiobutton></td>
										</tr>
									</table>
								</fieldset>
							</td>
						</tr>
						<TR>
							<TD colSpan="4">
								<table id="tbSearchDetail" runat="server" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td>
											<FIELDSET style="Z-INDEX: 0; WIDTH: 620px; HEIGHT: 2px" tabIndex="0"><LEGEND style="COLOR: black">Search 
													details for</LEGEND>
												<TABLE style="MARGIN: 5px;WIDTH: 600px" id="Table1">
													<TR>
														<TD>
															<asp:RadioButtonList id="rdoSearchDetail" runat="server" CssClass="tableRadioButton" RepeatDirection="Horizontal"
																style="Z-INDEX: 0" tabIndex="26" AutoPostBack="True"></asp:RadioButtonList></TD>
													</TR>
													<tr>
														<td>
															<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label9">Enter a comma separated list of numbers:</asp:label>
														</td>
													</tr>
													<tr>
														<td>
															<asp:textbox style="Z-INDEX: 0" id="txtInvoicesList" onkeypress="ValidateInvoiceList(event)"
																onpaste="AfterPasteInvoiceList(this)" runat="server" Width="550px" Height="100px" TextMode="MultiLine"
																tabIndex="27"></asp:textbox>
														</td>
													</tr>
													<TR>
														<TD>
															<asp:label style="Z-INDEX: 0" id="lblListNumberDetail" runat="server" CssClass="tableLabel">Note: Assessment numbers should be entered without prefix.</asp:label></TD>
													</TR>
												</TABLE>
											</FIELDSET>
										</td>
									</tr>
								</table>
							</TD>
						</TR>
					</tbody>
				</TABLE>
			</div>
			<INPUT value="<%=strScrollPosition%>" type=hidden name=ScrollPosition> <INPUT type="hidden" name="hdnRefresh">
		</form>
	</body>
</HTML>
