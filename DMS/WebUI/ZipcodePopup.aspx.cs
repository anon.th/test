using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using com.ties.classes;   
using System.Text;
using com.common.applicationpages;
namespace com.ties
{
	/// <summary>
	/// Summary description for ZipcodePopup.
	/// </summary>
	public class ZipcodePopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Button btnOk;
		//Utility utility = null;
		private String strZipcodeClientID = null;
		private String strStateClientID = null;
		private String strCountryClientID = null;

		String sESASurchargeClientID=null;
		String sESAClientID=null;
		String sSendCutOffTime_CID=null;
		protected System.Web.UI.WebControls.DataGrid dgZipcode;

		SessionDS m_sdsZipcode = null;
		String strAppID = "";
		String strEnterpriseID = "";
		String strFormID = "";
		String strSendZipCode=null;
		String strCustType=null;
		String strCustId=null;
		String strEAS=null;
		String strESAApplied=null;
		String strESASurcharge="0";
		String strZipcode = null;
		String strState = null;
		String strCountry = null;
		String strAgentZipcodes =null;
		String strStateCode=null;
		string strFocus=null;
		protected System.Web.UI.WebControls.Label lblCountry;
		protected System.Web.UI.WebControls.Label lblVASCode;
		protected System.Web.UI.WebControls.TextBox txtCountry;
		protected System.Web.UI.WebControls.TextBox txtZipcode;
		String strEASSurcharge = null;
		

		private void Page_Load(object sender, System.EventArgs e)
		{			
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();

			strZipcodeClientID = Request.Params["ZIPCODE_CID"];
			strStateClientID = Request.Params["STATE_CID"];
			strCountryClientID = Request.Params["COUNTRY_CID"];
			strFormID = Request.Params["FORMID"];
			strSendZipCode = Request.Params["SENDZIPCODE"]; 
			strCustType= Request.Params["CUSTTYPE"];  
			strCustId=Request.Params["CUSTID"];  
			strAgentZipcodes = Request.Params["AGENTZIPCODE"]; 
			
			//PickupRequestBooking
			sESASurchargeClientID=Request.Params["ESASurcharge_CID"];
			sESAClientID=Request.Params["ESA_CID"];
			sSendCutOffTime_CID=Request.Params["SendCutOffTime_CID"];

			this.txtZipcode.Attributes.Add("onKeyPress", "return UpperMaskSpecialWithHyphen(this)");
			this.txtCountry.Attributes.Add("onKeyPress", "return UpperLetterOnlyMask(this)");
			SetFocus(txtZipcode.ClientID.ToString()); // Thosapol.y	Add Code (26/06/2013)
			if(!Page.IsPostBack)
			{
				txtZipcode.Text = Request.Params["ZIPCODE"];
				if (strFormID=="CreateUpdateConsignment")
				{
					txtCountry.Enabled=false;
					txtCountry.Text=com.ties.DAL.SysDataMgrDAL.GetCountryDS(strAppID,strEnterpriseID);
				}
				else
				{
					txtCountry.Text = Request.Params["COUNTRY_TEXT"];
				}

				strStateCode= Request.Params["STATECODE"];
				m_sdsZipcode = GetEmptyZipcodeDS(0);
				BindGrid();
			}
			else
			{
				m_sdsZipcode = (SessionDS)ViewState["ZIPCODE_DS"];
			}

			// Put user code to initialize the page here

			//			txtZipcode.Text = Request.Params["ZipCode"];
		}

		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			dgZipcode.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentPage();
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgZipcode.SelectedIndexChanged += new System.EventHandler(this.dgZipcode_SelectedIndexChanged);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public static SessionDS GetEmptyZipcodeDS(int iNumRows)
		{
			DataTable dtZipcode = new DataTable();
			dtZipcode.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtZipcode.Columns.Add(new DataColumn("state_name", typeof(string)));
			dtZipcode.Columns.Add(new DataColumn("country", typeof(string)));
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtZipcode.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";

				dtZipcode.Rows.Add(drEach);
			}

			DataSet dsZipcode = new DataSet();
			dsZipcode.Tables.Add(dtZipcode);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsZipcode;
			sessionDS.DataSetRecSize = dsZipcode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;


			return  sessionDS;
	
		}

		private SessionDS GetZipcodeDS(int iCurrent, int iDSRecSize, String strQuery)
		{

			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("ZipcodePopup.aspx.cs","GetZipcodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"ZipcodeStateCountry");

			return  sessionDS;
			
		}

		private void BindGrid()
		{
			dgZipcode.VirtualItemCount = System.Convert.ToInt32(m_sdsZipcode.QueryResultMaxSize);
			dgZipcode.DataSource = m_sdsZipcode.ds;
			dgZipcode.DataBind();
			ViewState["ZIPCODE_DS"] = m_sdsZipcode;
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgZipcode.CurrentPageIndex * dgZipcode.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsZipcode = GetZipcodeDS(iStartIndex,dgZipcode.PageSize,sqlQuery);
			decimal pgCnt = (m_sdsZipcode.QueryResultMaxSize - 1)/dgZipcode.PageSize;
			if(pgCnt < dgZipcode.CurrentPageIndex)
			{
				dgZipcode.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgZipcode.SelectedIndex = -1;
			dgZipcode.EditItemIndex = -1;

			BindGrid();
			getPageControls(Page);
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{

			String strZipCode = txtZipcode.Text;
			String strCountry = txtCountry.Text;
			StringBuilder strQry = new StringBuilder();
			if (strFormID=="CreateUpdateConsignment")
			{
				strQry.Append("select z.zipcode as zipcode,s.state_name as state_name,z.country as country,z.esa_surcharge as esa_surcharge ");
				strQry.Append("from dbo.Zipcode z inner join dbo.Enterprise b ");
				strQry.Append("on z.applicationid = b.applicationid AND z.enterpriseid = b.enterpriseid AND z.Country = b.Country ");
				strQry.Append("join State s on z.applicationid = s.applicationid AND z.enterpriseid = s.enterpriseid AND z.Country = s.Country AND z.state_code = s.state_code ");
				strQry.Append("where z.applicationid='");
				strQry.Append(strAppID);
				strQry.Append("' and z.enterpriseid = '");
				strQry.Append(strEnterpriseID);
				strQry.Append("' ");

				if(strZipCode != null && strZipCode != "")
				{
					strQry.Append(" and z.zipcode like '%");
					strQry.Append(Utility.ReplaceSingleQuote(strZipCode));
					strQry.Append("%' ");
				}
				if(strCountry != null && strCountry != "")
				{
					strQry.Append(" and z.country like '%");
					strQry.Append(Utility.ReplaceSingleQuote(strCountry));
					strQry.Append("%' ");
				}				
			}
			else
			{
				strQry.Append("select z.zipcode as zipcode,s.state_name as state_name,z.country as country,z.esa_surcharge as esa_surcharge from Zipcode z, State s where z.applicationid='");
				strQry.Append(strAppID);
				strQry.Append("' and z.enterpriseid = '");
				strQry.Append(strEnterpriseID);
				strQry.Append("' and z.applicationid = s.applicationid and z.enterpriseid = s.enterpriseid and z.country = s.country and z.state_code = s.state_code ");

				if(strZipCode != null && strZipCode != "")
				{
					strQry.Append(" and z.zipcode like '%");
					strQry.Append(Utility.ReplaceSingleQuote(strZipCode));
					strQry.Append("%' ");
				}
				if(strCountry != null && strCountry != "")
				{
					strQry.Append(" and z.country like '%");
					strQry.Append(Utility.ReplaceSingleQuote(strCountry));
					strQry.Append("%' ");
				}
			}

			
			String strSQLQuery = strQry.ToString();

			ViewState["SQL_QUERY"] = strSQLQuery;
			dgZipcode.CurrentPageIndex = 0;

			ShowCurrentPage();

		}
		private  void CallESA()
		{
			

			if (strCustType=="C")
			{
				Customer customer = new Customer();
				customer.Populate(strAppID,strEnterpriseID,strCustId);
				strESAApplied =customer.ESASurcharge ;
			}
			else
			{
				Agent agent = new Agent();
				agent.Populate(strAppID,strEnterpriseID,strCustId);
				strESAApplied=agent.ESASurcharge; 

			}
				Zipcode zipCode = new Zipcode();
				zipCode.Populate(strAppID,strEnterpriseID,strCountry,strZipcode); 
				strESASurcharge	= zipCode.EASSurcharge.ToString()  ;
			
			if ((strESAApplied=="Y")&&(strESASurcharge!="0")&&(strESASurcharge!=null))
			{
				strEAS = "YES";
				strEASSurcharge=strESASurcharge; 
			}
			else 
			{
				strEAS="NO";
				strEASSurcharge="";
			}
			
			if((strCustType.Equals("NS")&&(strESASurcharge!="0")&&(strESASurcharge!=null)))
			{
				strEAS = "YES";
				strEASSurcharge= strESASurcharge;
			}
		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void dgZipcode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			
			int iSelIndex = dgZipcode.SelectedIndex;
			DataGridItem dgRow = dgZipcode.Items[iSelIndex];
			strZipcode = dgRow.Cells[0].Text;
			strState = dgRow.Cells[1].Text;
			strCountry = dgRow.Cells[2].Text;

//			if(strFormID=="EnterpriseProfile")
//			{
//				String sScript = "";
//				sScript += "<script language=javascript>";
//				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = '"+strZipcode+"';" ;
//				sScript += "  window.opener."+strFormID+"."+strStateClientID+".value = '"+strState+"';" ;
//				sScript += "  window.opener."+strFormID+"."+strCountryClientID+".value = '"+strCountry+"';" ;
//				sScript += "  window.close();";
//				sScript += "</script>";
//				Response.Write(sScript);
//			}
//			else 
			if (strFormID=="PickupRequestBooking")

			{

				strEASSurcharge = dgRow.Cells[3].Text;		
				Zipcode zipCode = new Zipcode();
				zipCode.Populate(strAppID,strEnterpriseID,strZipcode);
				strCountry = zipCode.Country;
				zipCode.Populate(strAppID,strEnterpriseID,strCountry,strZipcode); 
				strState=zipCode.StateName; 
				string strCutofftime = zipCode.CuttOffTime.ToString("HH:mm");   
				string strRouteCode = zipCode.PickUpRoute;
			
				if (strSendZipCode==",Sender")
				{
					CallESA();
					
				}

				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.opener."+strFormID+"."+strStateClientID+".value = \""+strState+"\";";
				sScript += "  window.opener."+strFormID+"."+strCountryClientID+".value = \""+strCountry+"\";";
				sScript += "  window.opener."+strFormID+"."+"txtRouteCode.value = \""+strRouteCode+"\";";
				
				if (strSendZipCode!=null)
				{
					sScript += "  window.opener."+strFormID+"."+sESASurchargeClientID+".value = \""+strEASSurcharge+"\";";
					sScript += "  window.opener."+strFormID+"."+sESAClientID+".value = \""+strEAS+"\";";
					sScript += "  window.opener."+strFormID+"."+sSendCutOffTime_CID+".value = \""+strCutofftime+"\";";
				}
				sScript += "  window.opener."+strFormID+".submit();";
				sScript += "  window.close();window.opener.callCodeBehind();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="ShipmentTrackingQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="ShipmentTrackingQuery_New")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="ShipmentTrackingQueryRpt")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="ShipmentExceptionReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="ShipmentPendingPODReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="ServiceFailure")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="RevenueCostReportQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="PODReportQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="ServiceQualityIndicatorQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			//Jeab 14 Jun 2011
			else if (strFormID=="PODEXRptQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			//Jeab 14 Jun 2011  =========> End
			//Jeab 17 Jun 2011
			else if (strFormID=="RevenueBySalesIDRptQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			//Jeab 17 Jun 2011  =========> End

			else if (strFormID=="SalesTerritoryReportQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="AverageDeliveryTimeReportQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			// by Ching Nov 30,2007
			else if (strFormID=="ShipmentPendingCOD")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="CODAmountCollectedReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="CODRemittanceReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			// End
			//HC Return Task
			else if (strFormID=="PendingHCReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			//HC Return Task
			//HC Return Task
			else if (strFormID=="ServiceExceptionHCReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			//HC Return Task
			else if (strFormID=="MarginAnalysisReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="RevenueByStateReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="TopNReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="AvgDeliveryTimeReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="ESA_Sectors")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="AgentProfile")
			{
				if (strAgentZipcodes == "AgentZipCodes" )
				{
					String sScript = "";
					sScript += "<script language=javascript>";
					sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
					sScript += "  window.close();";
					sScript += "</script>";
					Response.Write(sScript);
				}
				else
				{
					String sScript = "";
					sScript += "<script language=javascript>";
					sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
					sScript += "  window.opener."+strFormID+"."+strStateClientID+".value =\""+strState+"\";";
					sScript += "  window.opener."+strFormID+"."+strCountryClientID+".value = \""+strCountry+"\";";
					sScript += "  window.close();";
					sScript += "</script>";
					Response.Write(sScript);
				}
			}
			else if(strFormID=="AgentZipCodePopup.aspx")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.opener."+strFormID+"."+strStateClientID+".value =\""+strState+"\";";
				sScript += "  window.opener."+strFormID+"."+strCountryClientID+".value = \""+strCountry+"\";";
				sScript += "  window.close();";

				sScript += "</script>";
				Response.Write(sScript);
				//txtZipCode
			}
			else if (strFormID.Equals("ShipmentDetails"))
			{
				StringBuilder strBuilder = new StringBuilder("<script language=javascript>\n");
				strBuilder.Append(" window.opener.");
				strBuilder.Append(strFormID);
				strBuilder.Append(".txtRecipZip.value=\"");
				strBuilder.Append(strZipcode);
				strBuilder.Append("\";\n");
				strBuilder.Append(" window.close();\n");
				strBuilder.Append("</script>");
				Response.Write(strBuilder.ToString());
			}
			else if(strFormID=="ImportConsignments")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";

				sScript += "</script>";
				Response.Write(sScript);
				//txtZipCode
			}
			else if(strFormID=="CustomerProfile")
			{
				String strFR = Request.Params["ISFR"];

				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				if (strFR.Trim() != "Y")
				{
					sScript += "  window.opener."+strFormID+"."+strStateClientID+".value =\""+strState+"\";";
					sScript += "  window.opener."+strFormID+"."+strCountryClientID+".value = \""+strCountry+"\";";
				}
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			//PSA# 2009-014
			else if (strFormID=="TotalAverageDeliveryTimeReportQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if (strFormID=="CreateUpdateConsignment")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.opener."+strFormID+"."+strStateClientID+".value =\""+strState+"\";";
				sScript += "  window.opener."+strFormID+"."+strCustType+".focus();";
				sScript += "  window.close();";

				sScript += "</script>";
				Response.Write(sScript);
			}
			//PSA# 2009-014
			else
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strZipcodeClientID+".value = \""+strZipcode+"\";";
				sScript += "  window.opener."+strFormID+"."+strStateClientID+".value =\""+strState+"\";";
				sScript += "  window.opener."+strFormID+"."+strCountryClientID+".value = \""+strCountry+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}

		}

		
		// Thosapol.y Add Sub (26/06/2013)
		private void SetFocus(string idControl)
		{
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("<script language = 'javascript'>");
			strBuilder.Append("document.forms.ZipcodePopup." + idControl + ".focus();");
			strBuilder.Append("document.forms.ZipcodePopup." + idControl + ".select();");

			strBuilder.Append("</script>");
			RegisterStartupScript("Focus",strBuilder.ToString());
		}
	}
}
