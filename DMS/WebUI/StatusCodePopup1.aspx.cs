using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for StatusCodePopup1.
	/// </summary>
	public class StatusCodePopup1 : BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnSearch;
		
		protected msTextBox txtStatusSrch;
		protected System.Web.UI.WebControls.TextBox txtDescSrch;
		protected System.Web.UI.WebControls.DataGrid m_dgStatusCode;
		
		//Utility utility = null;
		
		private String appID = null;
		private String enterpriseID = null;
		private String strStatusCode = null;
		private String strStatusDescription = null;
		protected System.Web.UI.WebControls.Label lblStatusCode;
		protected System.Web.UI.WebControls.Label lblStatusDescription;
		protected System.Web.UI.WebControls.Label lblStatusClose;
		protected System.Web.UI.WebControls.Label lblInvoiceable;
		protected System.Web.UI.WebControls.Label lblMainTitle;

		protected System.Web.UI.WebControls.DropDownList ddlCloseSrch;
		protected System.Web.UI.WebControls.DropDownList ddlInvoiceSrch;
		private SessionDS m_sdsStatusCode = null;

		private DataView m_dvStatusCloseOptions = null;
		protected System.Web.UI.WebControls.Button btnClose;
		private DataView m_dvInvoiceableOptions = null;


		
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);

			if(!Page.IsPostBack)
			{
				txtStatusSrch.Text = Request.Params["STATUSCODE"];
				LoadComboLists();
				BindComboLists();
				RefreshData();
				BindStatusCode();
				ViewState["STATUS_DS"] = m_sdsStatusCode;
			}
			else
			{
				m_sdsStatusCode = (SessionDS)ViewState["STATUS_DS"];
			}
			getPageControls(Page);	
		}
		private void RefreshData()
		{
			DbConnection dbConApp = null;
			//IDbCommand dbCmd = null;
			//DataSet dsStatus = null;
			
			appID			= utility.GetAppID();
			enterpriseID	= utility.GetEnterpriseID();

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("StatusCodePopup1.aspx.cs","RefreshData","StatusCodePopup1l001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select status_code, status_description, status_close, invoiceable from Status_Code where applicationid ='");
			strQry.Append(appID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("' ");

			if(txtStatusSrch.Text.ToString()!="")
			{
				strQry.Append(" and status_code like '%");
				strQry.Append(Utility.ReplaceSingleQuote(txtStatusSrch.Text.ToString())+"%'");
			}
			if(txtDescSrch.Text.ToString()!="")
			{
				strQry.Append(" and status_description like N'%");
				strQry.Append(Utility.ReplaceSingleQuote(txtDescSrch.Text.ToString())+"%'");
			}
			if(ddlCloseSrch.SelectedItem.Value != "")
			{
				strQry.Append(" and status_close like '%");
				strQry.Append(ddlCloseSrch.SelectedItem.Value+"%'");
			}
			if(ddlInvoiceSrch.SelectedItem.Value != "")
			{
				strQry.Append(" and invoiceable like '%");
				strQry.Append(ddlInvoiceSrch.SelectedItem.Value + "%'");
			}
			//strQry.Append("'");
			String strQuery = strQry.ToString();

			IDbCommand idbCom = dbConApp.CreateCommand(strQuery, CommandType.Text);

			try
			{
				int iStartIndex = m_dgStatusCode.CurrentPageIndex * m_dgStatusCode.PageSize;
				m_sdsStatusCode = dbConApp.ExecuteQuery(idbCom, iStartIndex,m_dgStatusCode.PageSize,"Status_Code");
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("StatusCodePopup1.aspx.cs","RefreshData","Statel002","Error in the query String");
				throw appExpection;
			}

			ViewState["STATUS_DS"] = m_sdsStatusCode;

		}

		private void BindStatusCode()
		{
			m_dgStatusCode.VirtualItemCount = System.Convert.ToInt32(m_sdsStatusCode.QueryResultMaxSize);
			m_dgStatusCode.DataSource = m_sdsStatusCode.ds;
			m_dgStatusCode.DataBind();
			ViewState["STATUS_DS"] = m_sdsStatusCode;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.m_dgStatusCode.SelectedIndexChanged += new System.EventHandler(this.m_dgStatusCode_SelectedIndexChanged);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		public void OnPaging_Dispatch(Object sender, DataGridPageChangedEventArgs e)
		{
			m_dgStatusCode.CurrentPageIndex = e.NewPageIndex;
			RefreshData();
			BindStatusCode();
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			RefreshData();
			BindStatusCode();
			getPageControls(Page);
		}

		private void m_dgStatusCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			String strFormID = Request.Params["FORMID"];
			String strStatusCodeClientID = Request.Params["STATUSCODE_CID"];
			String strStatusDescriptionClientID = Request.Params["SCDESCRIPTION_CID"];


			int iSelIndex = m_dgStatusCode.SelectedIndex;
			DataGridItem dgRow = m_dgStatusCode.Items[iSelIndex];
			strStatusCode = dgRow.Cells[0].Text;
			strStatusDescription = dgRow.Cells[1].Text;


			String sScript = "";
			sScript += "<script language=javascript>";
			if(strFormID.Equals("AgentStatusCost"))
			{
				sScript += "  window.opener.AgentStatusCost."+strStatusCodeClientID+".value = \""+strStatusCode+"\";";
				sScript += "  window.opener.AgentStatusCost."+strStatusDescriptionClientID+".value = \""+strStatusDescription+"\";";
			}
			else
			{
				sScript += "  window.opener."+strFormID+"."+strStatusCodeClientID+".value = \""+strStatusCode+"\";";
				sScript += "  window.opener."+strFormID+"."+strStatusDescriptionClientID+".value = \""+strStatusDescription+"\";";
			}
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);

			
		}



		private DataView GetStatusCloseOptions(bool showNilOption)
		{
			DataTable dtStatusCloseOptions = new DataTable();
			
			dtStatusCloseOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtStatusCloseOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList statusCloseOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"close_status",CodeValueType.StringValue);


			if(showNilOption)
			{
				DataRow drNilRow = dtStatusCloseOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtStatusCloseOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode statusCloseSysCode in statusCloseOptionArray)
			{
				DataRow drEach = dtStatusCloseOptions.NewRow();
				drEach[0] = statusCloseSysCode.Text;
				drEach[1] = statusCloseSysCode.StringValue;
				dtStatusCloseOptions.Rows.Add(drEach);
			}

			DataView dvStatusCloseOptions = new DataView(dtStatusCloseOptions);
			return dvStatusCloseOptions;

		}

		private DataView GetInvoiceableOptions(bool showNilOption)
		{
			DataTable dtInvoiceableOptions = new DataTable();
			
			dtInvoiceableOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtInvoiceableOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList invoiceableOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"invoiceable",CodeValueType.StringValue);


			if(showNilOption)
			{
				DataRow drNilRow = dtInvoiceableOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtInvoiceableOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode invoiceableSysCode in invoiceableOptionArray)
			{
				DataRow drEach = dtInvoiceableOptions.NewRow();
				drEach[0] = invoiceableSysCode.Text;
				drEach[1] = invoiceableSysCode.StringValue;
				dtInvoiceableOptions.Rows.Add(drEach);
			}

			DataView dvInvoiceableOptions = new DataView(dtInvoiceableOptions);
			return dvInvoiceableOptions;

		}

		private void LoadComboLists()
		{
			m_dvStatusCloseOptions = GetStatusCloseOptions(true);
			m_dvInvoiceableOptions = GetInvoiceableOptions(true);
		}

		private void BindComboLists()
		{
			ddlCloseSrch.DataSource = (ICollection)m_dvStatusCloseOptions;
			ddlCloseSrch.DataTextField = "Text";
			ddlCloseSrch.DataValueField = "StringValue";
			ddlCloseSrch.DataBind();

			ddlInvoiceSrch.DataSource = (ICollection)m_dvInvoiceableOptions;
			ddlInvoiceSrch.DataTextField = "Text";
			ddlInvoiceSrch.DataValueField = "StringValue";
			ddlInvoiceSrch.DataBind();


		
		
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
	}
}
