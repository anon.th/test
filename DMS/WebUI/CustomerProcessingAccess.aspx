<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="CustomerProcessingAccess.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomerProcessingAccess" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<TITLE>CustomerProcessingAccess</TITLE>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
					function funcDisBack()
					{
						history.go(+1);
					}

					function RemoveBadPaseNumber(strTemp, obj) {
						strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-integer
						obj.value = strTemp.replace(/\s/g, ''); // replace space
					}
			
					function AfterPasteNumber(obj) {
						setTimeout(function () {
							RemoveBadPaseNumber(obj.value, obj);
						}, 1); //or 4
					}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="CustomerProcessingAccess" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 1008px; HEIGHT: 1543px; TOP: 34px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tbody>
						<tr>
							<td colSpan="2"><asp:label id="Label1" runat="server" CssClass="mainTitleSize" Height="27px" Width="421px">Customer Processing Access</asp:label></td>
						</tr>
						<tr>
							<td colSpan="2"><asp:button id="btnQuery" runat="server" CssClass="queryButton" CausesValidation="False" Text="Query"></asp:button><asp:button id="btnExecuteQuery" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Execute Query" Enabled="true"></asp:button></td>
						</tr>
						<tr>
							<td style="HEIGHT: 21px" colSpan="6"><asp:label id="lbError" runat="server" ForeColor="Red" Font-Bold="True" Font-Size="X-Small"></asp:label></td>
						</tr>
						<tr>
							<td><asp:label style="Z-INDEX: 0" id="lbUserID" runat="server" CssClass="tableLabel" Width="70px">User ID</asp:label>&nbsp;
								<cc1:mstextbox id="txtUserID" tabIndex="1" CssClass="textField" Width="153" MaxLength="40" Runat="server"
									TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>
								<asp:label style="Z-INDEX: 0" id="lbBack" runat="server" CssClass="tableLabel" Width="20px"></asp:label>
								<asp:label style="Z-INDEX: 0" id="lbCustomerID" runat="server" CssClass="tableLabel" Width="80px">Customer ID</asp:label>
								<cc1:mstextbox id="txtCustomerID" tabIndex="2" CssClass="textField" Width="135px" MaxLength="40"
									Runat="server" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td colSpan="2"><asp:datagrid style="Z-INDEX: 0" id="gridCusProAcc" tabIndex="42" runat="server" Width="100%"
									FooterStyle-CssClass="gridHeading" BorderWidth="0px" CellSpacing="1" CellPadding="0" HeaderStyle-CssClass="gridHeading"
									ItemStyle-CssClass="gridField" AlternatingItemStyle-CssClass="gridField" ShowFooter="True" AutoGenerateColumns="False">
									<FooterStyle CssClass="gridHeading"></FooterStyle>
									<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn>
											<HeaderStyle Width="5%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:LinkButton id="btnDeleteItem" Runat="server" Text="<img src='images/butt-delete.gif' alt='delete' border=0>"
													CommandName="DELETE_ITEM" />
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Center"></FooterStyle>
											<FooterTemplate>
												<asp:LinkButton id="btnAddItem" Runat="server" Text="<img src='images/butt-add.gif' alt='add' border=0>"
													CommandName="ADD_ITEM" />
											</FooterTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="User ID">
											<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lbItemUserID" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "userid") %>'>
												</asp:Label>
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Center"></FooterStyle>
											<FooterTemplate>
												<cc1:mstextbox id="txtAddUserId" Width="100%" tabIndex="3" CssClass="textField" MaxLength="40"
													Runat="server" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>
											</FooterTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="User Name">
											<HeaderStyle HorizontalAlign="Left" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem, "user_name") %>
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Center"></FooterStyle>
											<FooterTemplate>
											</FooterTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Customer ID">
											<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lbItemCustID" Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "custid") %>'>
												</asp:Label>
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Center"></FooterStyle>
											<FooterTemplate>
												<cc1:mstextbox id="txtAddCustID" tabIndex="4" CssClass="textField" Width="100%" MaxLength="40"
													Runat="server" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>
											</FooterTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Customer Name">
											<HeaderStyle HorizontalAlign="Left" Width="20%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem, "cust_name") %>
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Center"></FooterStyle>
											<FooterTemplate>
											</FooterTemplate>
										</asp:TemplateColumn>
									</Columns>
								</asp:datagrid></td>
						</tr>
					</tbody>
				</TABLE>
			</div>
		</form>
		</SCRIPT>
	</body>
</HTML>
