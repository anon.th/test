using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using com.ties.classes;   
using System.Text;
using TIESDAL;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for ZipcodePopup.
	/// </summary>
	public class RecipientCostCentrePopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Button btnOk;
		//Utility utility = null;
		private String strZipcodeClientID = null;
		private String strStateClientID = null;
		private String strCountryClientID = null;

		private String strTelephoneID = null;
		private String strCostCentreID = null;
		private String strNameID = null;
		private String strAddress1ID = null;
		private String strAddress2ID = null;
		private String strZipcodeID = null;
		private String strFaxID = null;
		private String strContactPersonID = null;
		private String strStateNameID = null;

		String sESASurchargeClientID=null;
		String sESAClientID=null;
		String sSendCutOffTime_CID=null;
		protected System.Web.UI.WebControls.DataGrid dgZipcode;

		SessionDS m_sdsZipcode = null;
		String strAppID = "";
		String strEnterpriseID = "";
		String strFormID = "";
		String strSendZipCode=null;
		String strCustType=null;
		String strCustId=null;
		String strEAS=null;
		String strESAApplied=null;
		String strESASurcharge="0";
		String strZipcode = null;
		String strState = null;
		String strCountry = null;
		String strAgentZipcodes =null;
		String strStateCode=null;
		string strFocus=null;
		protected System.Web.UI.WebControls.Label lblCountry;
		protected System.Web.UI.WebControls.Label lblVASCode;
		protected System.Web.UI.WebControls.TextBox txtCountry;
		protected System.Web.UI.WebControls.TextBox Textbox1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox txtTelephone;
		protected System.Web.UI.WebControls.TextBox txtCostCentre;
		protected System.Web.UI.WebControls.TextBox txtName;
		protected System.Web.UI.WebControls.Label lblWarning;
		String strEASSurcharge = null;
		
		private void Page_Load(object sender, System.EventArgs e)
		{			
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();

			strTelephoneID = Request.Params["TELEPHONE_CID"];
			strCostCentreID = Request.Params["COST_CENTRE_ID"];
			strNameID = Request.Params["NAME_ID"];
			strAddress1ID = Request.Params["ADDRESS1_ID"];
			strAddress2ID = Request.Params["ADDRESS2_ID"];
			strZipcodeID = Request.Params["ZIPCODE_ID"];
			strFaxID = Request.Params["FAX_ID"];
			strContactPersonID = Request.Params["CONTACT_PERSON_ID"];
			strStateNameID = Request.Params["STATE_NAME_ID"];

			strZipcodeClientID = Request.Params["ZIPCODE_CID"];
			strStateClientID = Request.Params["STATE_CID"];
			strCountryClientID = Request.Params["COUNTRY_CID"];
			strFormID = Request.Params["FORMID"];
			strSendZipCode = Request.Params["SENDZIPCODE"]; 
			strCustType= Request.Params["CUSTTYPE"];  
			strCustId=Request.Params["CUSTID"];  
			strAgentZipcodes = Request.Params["AGENTZIPCODE"]; 
			
			//PickupRequestBooking
			sESASurchargeClientID=Request.Params["ESASurcharge_CID"];
			sESAClientID=Request.Params["ESA_CID"];
			sSendCutOffTime_CID=Request.Params["SendCutOffTime_CID"];

//			this.txtTelephone.Attributes.Add("onKeyPress", "return UpperMaskSpecialWithHyphen(this)");
//			this.txtCostCentre.Attributes.Add("onKeyPress", "return UpperMaskSpecialWithHyphen(this)");
//			this.txtName.Attributes.Add("onKeyPress", "return UpperMaskSpecialWithHyphen(this)");
			SetFocus(txtCostCentre.ClientID.ToString()); // Thosapol.y	Add Code (26/06/2013)

			if(!Page.IsPostBack)
			{
				lblWarning.Visible = false;
				txtCostCentre.Text = Request.Params["COST_CENTRE"];
				strStateCode= Request.Params["STATECODE"];
				m_sdsZipcode = GetEmptyZipcodeDS(0);
				BindGrid();
			}
			else
			{
				m_sdsZipcode = (SessionDS)ViewState["ZIPCODE_DS"];
			}

			// Put user code to initialize the page here

			//			txtZipcode.Text = Request.Params["ZipCode"];
		}

		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			dgZipcode.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentPage();
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgZipcode.SelectedIndexChanged += new System.EventHandler(this.dgZipcode_SelectedIndexChanged);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public static SessionDS GetEmptyZipcodeDS(int iNumRows)
		{
			DataTable dtZipcode = new DataTable();			
			dtZipcode.Columns.Add(new DataColumn("telephone", typeof(string)));
			dtZipcode.Columns.Add(new DataColumn("cost_centre", typeof(string)));
			dtZipcode.Columns.Add(new DataColumn("reference_name", typeof(string)));
			dtZipcode.Columns.Add(new DataColumn("address1", typeof(string)));
			dtZipcode.Columns.Add(new DataColumn("address2", typeof(string)));
			dtZipcode.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtZipcode.Columns.Add(new DataColumn("fax", typeof(string)));
			dtZipcode.Columns.Add(new DataColumn("contactperson", typeof(string)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtZipcode.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";

				dtZipcode.Rows.Add(drEach);
			}

			DataSet dsZipcode = new DataSet();
			dsZipcode.Tables.Add(dtZipcode);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsZipcode;
			sessionDS.DataSetRecSize = dsZipcode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;


			return  sessionDS;
	
		}

		private SessionDS GetZipcodeDS(int iCurrent, int iDSRecSize, String strQuery)
		{

			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("ZipcodePopup.aspx.cs","GetZipcodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"ZipcodeStateCountry");

			return  sessionDS;
			
		}

		private void BindGrid()
		{
			dgZipcode.VirtualItemCount = System.Convert.ToInt32(m_sdsZipcode.QueryResultMaxSize);
			dgZipcode.DataSource = m_sdsZipcode.ds;
			dgZipcode.DataBind();

			ViewState["ZIPCODE_DS"] = m_sdsZipcode;
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgZipcode.CurrentPageIndex * dgZipcode.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsZipcode = GetZipcodeDS(iStartIndex,dgZipcode.PageSize,sqlQuery);
			decimal pgCnt = (m_sdsZipcode.QueryResultMaxSize - 1)/dgZipcode.PageSize;
			if(pgCnt < dgZipcode.CurrentPageIndex)
			{
				dgZipcode.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgZipcode.SelectedIndex = -1;
			dgZipcode.EditItemIndex = -1;

			BindGrid();
			getPageControls(Page);
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			String strTelephone = txtTelephone.Text.Trim();
			String strCostCentre = txtCostCentre.Text.Trim();
			String strName = txtName.Text.Trim();
			StringBuilder strQry = new StringBuilder();

			if ((strTelephone == null || strTelephone == string.Empty) &&
				(strCostCentre == null || strCostCentre == string.Empty) &&
				(strName == null || strName == string.Empty) )
			{
				lblWarning.Visible = true;
				return;
			}
			else
			{
				lblWarning.Visible = false;
			}

			if (strFormID=="CreateUpdateConsignment")
			{     
				strQry.Append("SELECT ISNULL(telephone,'') AS telephone,  ");
				strQry.Append("ISNULL(cost_centre,'') AS cost_centre, ");
				strQry.Append("ISNULL(reference_name,'') AS reference_name, ");
				strQry.Append("ISNULL(address1,'') AS address1, ");
				strQry.Append("ISNULL(address2,'') AS address2, ");
				strQry.Append("ISNULL(zipcode,'') AS zipcode, ");
				strQry.Append("ISNULL(fax,'') AS fax, ");
				strQry.Append("ISNULL(contactperson,'') AS contactperson ");
				strQry.Append("FROM v_References ");
				strQry.Append("WHERE applicationid='");
				strQry.Append(strAppID);
				strQry.Append("' and enterpriseid = '");
				strQry.Append(strEnterpriseID);
				strQry.Append("' ");

				if(strTelephone != null && strTelephone != "")
				{
					strQry.Append(" and telephone like '%");
					strQry.Append(Utility.ReplaceSingleQuote(strTelephone));
					strQry.Append("%' ");
				}
				if(strCostCentre != null && strCostCentre != "")
				{
					strQry.Append(" and cost_centre like '%");
					strQry.Append(Utility.ReplaceSingleQuote(strCostCentre));
					strQry.Append("%' ");
				}
				if(strName != null && strName != "")
				{
					strQry.Append(" and reference_name like '%");
					strQry.Append(Utility.ReplaceSingleQuote(strName));
					strQry.Append("%' ");
				}
				strQry.Append(" Order by telephone, cost_centre, reference_name, address1, address2, zipcode, fax, contactperson ASC ");
			}
			else
			{
				strQry.Append("SELECT telephone, cost_centre, reference_name, address1, address2, zipcode, fax, contactperson  ");
				strQry.Append("FROM v_References ");
				strQry.Append("WHERE applicationid='");
				strQry.Append(strAppID);
				strQry.Append("' and enterpriseid = '");
				strQry.Append(strEnterpriseID);
				strQry.Append("' ");

				if(strTelephone != null && strTelephone != "")
				{
					strQry.Append(" and telephone like '%");
					strQry.Append(Utility.ReplaceSingleQuote(strTelephone));
					strQry.Append("%' ");
				}
				if(strCostCentre != null && strCostCentre != "")
				{
					strQry.Append(" and cost_centre like '%");
					strQry.Append(Utility.ReplaceSingleQuote(strCostCentre));
					strQry.Append("%' ");
				}
				if(strName != null && strName != "")
				{
					strQry.Append(" and reference_name like '%");
					strQry.Append(Utility.ReplaceSingleQuote(strName));
					strQry.Append("%' ");
				}
				strQry.Append(" Order by telephone, cost_centre, reference_name, address1, address2, zipcode, fax, contactperson ASC ");
			}

			String strSQLQuery = strQry.ToString();

			ViewState["SQL_QUERY"] = strSQLQuery;
			dgZipcode.CurrentPageIndex = 0;

			ShowCurrentPage();

		}
		private  void CallESA()
		{
			

			if (strCustType=="C")
			{
				Customer customer = new Customer();
				customer.Populate(strAppID,strEnterpriseID,strCustId);
				strESAApplied =customer.ESASurcharge ;
			}
			else
			{
				Agent agent = new Agent();
				agent.Populate(strAppID,strEnterpriseID,strCustId);
				strESAApplied=agent.ESASurcharge; 

			}
				Zipcode zipCode = new Zipcode();
				zipCode.Populate(strAppID,strEnterpriseID,strCountry,strZipcode); 
				strESASurcharge	= zipCode.EASSurcharge.ToString()  ;
			
			if ((strESAApplied=="Y")&&(strESASurcharge!="0")&&(strESASurcharge!=null))
			{
				strEAS = "YES";
				strEASSurcharge=strESASurcharge; 
			}
			else 
			{
				strEAS="NO";
				strEASSurcharge="";
			}
			
			if((strCustType.Equals("NS")&&(strESASurcharge!="0")&&(strESASurcharge!=null)))
			{
				strEAS = "YES";
				strEASSurcharge= strESASurcharge;
			}
		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void dgZipcode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			
			int iSelIndex = dgZipcode.SelectedIndex;
			DataGridItem dgRow = dgZipcode.Items[iSelIndex];

			strZipcode = Server.HtmlDecode(dgRow.Cells[0].Text);
			strState = Server.HtmlDecode(dgRow.Cells[1].Text);
			strCountry = Server.HtmlDecode(dgRow.Cells[2].Text);

			string strTelephone = Server.HtmlDecode(dgRow.Cells[0].Text);
			string strCostCentre = Server.HtmlDecode(dgRow.Cells[1].Text);
			string strName = Server.HtmlDecode(dgRow.Cells[2].Text);
			string strAddress1 = Server.HtmlDecode(dgRow.Cells[3].Text);
			string strAddress2 = Server.HtmlDecode(dgRow.Cells[4].Text);
			string strZipcodeVal = Server.HtmlDecode(dgRow.Cells[5].Text);
			string strFax = Server.HtmlDecode(dgRow.Cells[6].Text);
			string strContactPerson = Server.HtmlDecode(dgRow.Cells[7].Text);
			string strStateName = GetStateName(strZipcodeVal);

			if (strFormID=="CreateUpdateConsignment")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strTelephoneID+".value = \""+strTelephone+"\";";
				sScript += "  window.opener."+strFormID+"."+strCostCentreID+".value = \""+strCostCentre+"\";";
				sScript += "  window.opener."+strFormID+"."+strNameID+".value = \""+strName+"\";";
				sScript += "  window.opener."+strFormID+"."+strAddress1ID+".value = \""+strAddress1+"\";";
				sScript += "  window.opener."+strFormID+"."+strAddress2ID+".value = \""+strAddress2+"\";";
				sScript += "  window.opener."+strFormID+"."+strZipcodeID+".value = \""+strZipcodeVal+"\";";
				sScript += "  window.opener."+strFormID+"."+strFaxID+".value = \""+strFax+"\";";
				sScript += "  window.opener."+strFormID+"."+strContactPersonID+".value = \""+strContactPerson+"\";";
				sScript += "  window.opener."+strFormID+"."+strStateNameID+".value = \""+strStateName+"\";";
				sScript += "  window.opener."+strFormID+"."+strNameID+".focus();";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strTelephoneID+".value = \""+strZipcode+"\";";
				sScript += "  window.opener."+strFormID+"."+strCostCentreID+".value = \""+strZipcode+"\";";
				sScript += "  window.opener."+strFormID+"."+strNameID+".value = \""+strZipcode+"\";";
				sScript += "  window.opener."+strFormID+"."+strAddress1ID+".value = \""+strZipcode+"\";";
				sScript += "  window.opener."+strFormID+"."+strAddress2ID+".value = \""+strZipcode+"\";";
				sScript += "  window.opener."+strFormID+"."+strZipcodeID+".value = \""+strZipcode+"\";";
				sScript += "  window.opener."+strFormID+"."+strFaxID+".value = \""+strZipcode+"\";";
				sScript += "  window.opener."+strFormID+"."+strContactPersonID+".value = \""+strZipcode+"\";";
				sScript += "  window.opener."+strFormID+"."+strStateNameID+".value = \""+strZipcode+"\";";
				sScript += "  window.opener."+strFormID+"."+strNameID+".focus();";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}

		}

		private void SetFocus(string idControl)
		{
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("<script language = 'javascript'>");
			strBuilder.Append("document.forms.ZipcodePopup." + idControl + ".focus();");
			strBuilder.Append("document.forms.ZipcodePopup." + idControl + ".select();");

			strBuilder.Append("</script>");
			RegisterStartupScript("Focus",strBuilder.ToString());
		}

		private string GetStateName(string zipcode)
		{
			string state_name="";
			System.Data.DataSet ds = CustomerConsignmentDAL.GetZipcode(strAppID,strEnterpriseID,zipcode);
			if(ds.Tables["Zipcode"].Rows.Count>0 && ds.Tables["Zipcode"].Rows[0]["state_name"] != DBNull.Value)
			{
				state_name = ds.Tables["Zipcode"].Rows[0]["state_name"].ToString();
			}
			
			return state_name;
		}


	}
}
