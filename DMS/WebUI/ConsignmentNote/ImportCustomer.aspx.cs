using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;  
using System.Drawing;
using System.Text;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;  
using System.Globalization; 
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using com.ties.BAL;  
using Cambro.Web.DbCombo;
using TIES;
using TIESDAL;
using System.Data.OleDb;
using System.IO;

namespace com.ties
{
	/// <summary>
	/// Summary description for ImportCustomer.
	/// </summary>
	public class ImportCustomer : BasePage
	{
		protected System.Web.UI.HtmlControls.HtmlInputFile file_upload;
		protected System.Web.UI.WebControls.Label ErrorMsg;
		protected System.Web.UI.HtmlControls.HtmlInputButton btnImport;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Image imgCheck1;
		protected System.Web.UI.WebControls.Image imgCheck2;
		protected System.Web.UI.WebControls.Label lblCheck1;
		protected System.Web.UI.WebControls.Image imgCheck3;
		protected System.Web.UI.WebControls.Image imgCheck4;
		protected System.Web.UI.WebControls.Label lblCheck2;
		protected System.Web.UI.WebControls.Image imgCheck5;
		protected System.Web.UI.WebControls.Image imgCheck6;
		protected System.Web.UI.WebControls.Label lblCheck3;
		protected System.Web.UI.WebControls.Image imgCheck7;
		protected System.Web.UI.WebControls.Image imgCheck8;
		protected System.Web.UI.WebControls.Label lblCheck4;
		protected System.Web.UI.WebControls.Image imgCheck9;
		protected System.Web.UI.WebControls.Image imgCheck10;
		protected System.Web.UI.WebControls.Label lblCheck5;
		protected System.Web.UI.WebControls.Image imgCheck11;
		protected System.Web.UI.WebControls.Image imgCheck12;
		protected System.Web.UI.WebControls.Label lblCheck6;
		protected System.Web.UI.WebControls.Panel pnlCheckStep;
		protected System.Web.UI.WebControls.Panel pnlDescription;
		protected System.Web.UI.WebControls.TextBox txtException;
		protected System.Web.UI.WebControls.Panel pnlException;
		
		Utility utility = null;
		String strAppID = null;
		String strEnterpriseID = null;
		String PayerID = null;
		String userID = null;
		public String strMsg = "";
		public int ErrorNumber = 0;
		private string FileName = "";
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			String ServerAction = "";
	

			try
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				strAppID = Session["applicationID"] + ""; 
				strEnterpriseID = Session["enterpriseID"] + ""; 
				userID = Session["userID"] + ""; 
				PayerID = Session["PayerID"] + "";


				if (!Page.IsPostBack)
				{
					ServerAction = Request.QueryString["ServerAction"] + "";
				}
				else
				{
					ServerAction = Request.Form["ServerAction"] + ""; 
				}

				switch (ServerAction.ToUpper())
				{
					case "IMPORTDATA":
						ImportData();
						break;
					case "CLEAR":
						ClearStep();
						break;
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void ImportData()
		{
			try
			{
				ClearStep();
				//Check Excel File Format
				Step1(); 
				
				//step2
				if (txtException.Text == "" && strMsg == "")  
				{ 
					//do step2 : Import Data from Excel File
					imgCheck1.Visible = true;
					Step2(); 
				}
				else
				{
					//show error step1
					imgCheck2.Visible = true;
					imgCheck4.Visible = true;
					imgCheck6.Visible = true;
					imgCheck8.Visible = true;
					imgCheck10.Visible = true;
					imgCheck12.Visible = true;
				}

				//step3 
				if (txtException.Text == "" && strMsg == "")  
				{ 
					//do step 3 : Check Required Data
					imgCheck1.Visible = true;
					imgCheck3.Visible = true;
					Step3(); 
				}
				else
				{
					//show error step2
					imgCheck4.Visible = true;
					imgCheck6.Visible = true;
					imgCheck8.Visible = true;
					imgCheck10.Visible = true;
					imgCheck12.Visible = true;
				}

				//step4
				if (txtException.Text == "" && strMsg == "")  
				{ 
					//do step 4 : Check Required Data
					imgCheck1.Visible = true;
					imgCheck3.Visible = true;
					imgCheck5.Visible = true;
					Step4(); 
				}
				else
				{
					//show error step3
					imgCheck6.Visible = true;
					imgCheck8.Visible = true;
					imgCheck10.Visible = true;
					imgCheck12.Visible = true;
				}

				//step5
				if (txtException.Text == "" && strMsg == "")  
				{ 
					//do step 5 : Save Data to Database
					imgCheck1.Visible = true;
					imgCheck3.Visible = true;
					imgCheck5.Visible = true;
					imgCheck7.Visible = true;
					Step5(); 
				}
				else
				{
					//show error step4
					imgCheck8.Visible = true;
					imgCheck10.Visible = true;
					imgCheck12.Visible = true;
				}

				//step6
				if (txtException.Text == "" && strMsg == "")  
				{ 
					//do step 6 : Import Data Complete 
					imgCheck1.Visible = true;
					imgCheck3.Visible = true;
					imgCheck5.Visible = true;
					imgCheck7.Visible = true;
					imgCheck9.Visible = true;
					imgCheck11.Visible = true;
				}
				else
				{
					//show error step5
					imgCheck10.Visible = true;
					imgCheck12.Visible = true;
				}
			}
			catch(Exception ex)
			{
				ErrorMsg.Text = ex.Message.ToString() ;
			}
		}

		private void ClearStep()
		{
			strMsg = "";
			ErrorMsg.Text  = "";
			//lblDescription.Text = "";
			txtException.Text = "";			
			imgCheck1.Visible = false;
			imgCheck2.Visible = false;
			imgCheck3.Visible = false;
			imgCheck4.Visible = false;
			imgCheck5.Visible = false;
			imgCheck6.Visible = false;
			imgCheck7.Visible = false;
			imgCheck8.Visible = false;
			imgCheck9.Visible = false;
			imgCheck10.Visible = false;
			imgCheck11.Visible = false;
			imgCheck12.Visible = false;
		}

		private bool uploadFiletoServer()
		{
			bool status = true;

			if((file_upload.PostedFile != null ) && (file_upload.PostedFile.ContentLength > 0))
			{
				string extension = System.IO.Path.GetExtension(file_upload.PostedFile.FileName);

				if(extension.ToUpper() == ".XLS")
				{
					string fn = System.IO.Path.GetFileName(file_upload.PostedFile.FileName);
					string path = Server.MapPath("..\\Excel") + "\\";
					string pathFn = Server.MapPath("..\\Excel") + "\\" + userID + "_" + fn;

					ViewState["FileName"] = pathFn;

					try
					{
						if(System.IO.Directory.Exists(path) == false)
							System.IO.Directory.CreateDirectory(path);

						if (System.IO.File.Exists(pathFn)) 
							System.IO.File.Delete(pathFn);

						file_upload.PostedFile.SaveAs(pathFn);

					}
					catch(Exception ex)
					{
						throw new ApplicationException("Error during upload file to the server", null);
					}
				}
				else
				{
					status = false;
				}
			}
			else
			{
				status = false;
			}

			return status;
		}
		//Verify step 1 : Check Excel File Format
		private void Step1() 
		{
			string extension = "";
			strMsg = "";

			try
			{
				if ((file_upload.PostedFile != null) && (file_upload.PostedFile.ContentLength > 0))
				{
					extension = System.IO.Path.GetExtension(file_upload.PostedFile.FileName);
					if(extension.ToUpper() != ".XLS")
					{
						strMsg += "Selected file was not XLS file. (Excel File Format) Please check!\r\n";
					}
					else
					{
						imgCheck1.Visible = true;
						if(!uploadFiletoServer())
						{
							strMsg += "Cannot upload file to server. \r\n";
						}
					}
				}

				if (strMsg != "")
				{
					ErrorNumber = ErrorNumber + 1;
					txtException.Text += ErrorNumber.ToString() + ". " + strMsg;
				}
			}
			catch(Exception ex)
			{
				ErrorNumber = ErrorNumber + 1;
				txtException.Text += ErrorNumber.ToString() + ". Invalid data format in Excel file. \r\n";
			}
		}


		//Verify step 2 : Import Data from Excel File
		private void Step2()
		{
			DataTable DT = new DataTable(); 
			int iRowsAffected = 0;
			strMsg = "";

			try
			{
//				FileName = file_upload.Value + "";
//				FileName = FileName.Replace("\'", "\\\'");
//				String connString = ConfigurationSettings.AppSettings["ExcelConn"];
//				connString = connString.Replace("(DestinationFile)", FileName);
				String connString = ConfigurationSettings.AppSettings["ExcelConn"];
				connString = connString.Replace("(DestinationFile)", (String)ViewState["FileName"]);

				//insert consignment sheet into tmp_Consignment
				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append("SELECT * FROM [Sheet1$]");
				OleDbDataAdapter daExcel = new OleDbDataAdapter(strBuilder.ToString(), connString);

				DataSet dsExcel = new DataSet();
				daExcel.Fill(dsExcel);
				if ((dsExcel != null) && (dsExcel.Tables.Count > 0))
				{
					DT = dsExcel.Tables[0];
					if ((DT != null) && (DT.Rows.Count > 0))
					{
						iRowsAffected = ConsignmentNoteDAL.InsertTmpReference2(strAppID, strEnterpriseID, PayerID,  DT);
					}
				}
				if (iRowsAffected < 0)
				{
					strMsg += "Error import Customer data! Please check data in excel file.\r\n";
				}



				if (strMsg != "")
				{
					ErrorNumber = ErrorNumber + 1;
					txtException.Text += ErrorNumber.ToString() + ". " + strMsg;
				}
			}
			catch(Exception ex)
			{
				ErrorNumber = ErrorNumber + 1;
				txtException.Text += strMsg + ErrorNumber.ToString() + ". Can not connect to Excel File! Please contact Administrator!\r\n";
			}
			finally
			{
				DT.Clear();
				DT.Dispose();
			}
		}


		//Verify step 3 : Check Required Data
		private void Step3()
		{
			DataSet DS = new DataSet(); 
			DataTable DT = new DataTable();
			DataTable DTT = new DataTable();
			string CustomerCode, Reference_Name, Addr1, Addr2, ZipCode, Telephone, Contact_Person = "";
			string Msg = "";
			string Msg1 = "";
			strMsg  = "";

			try
			{
				//Check tmpConsignment
				DS = ConsignmentNoteDAL.GetTmpReference2(strAppID, strEnterpriseID, PayerID);
				if ((DS != null) && DS.Tables.Count > 0)
				{
					Session["TMP_REFERENCE2"] = DS.Tables[0];  //declare for use next step
					DT = DS.Tables[0];
					DTT = DS.Tables[0];
					DataSet dsZipcode = new DataSet();
					if ((DT != null) && (DT.Rows.Count > 0))
					{
						Zipcode zipcode = new Zipcode();

						//create array list from excel file - start
						int cnt = 0;
						string[] arrCustCd = new string[DT.Rows.Count];
						foreach (DataRow DRR in DTT.Rows)
						{
							arrCustCd[cnt] = DRR["CustomerCode"].ToString();
							cnt++;
						}

						foreach (DataRow DR in DT.Rows)
						{
							int intDupExcel = 0;

							CustomerCode = "";
							Msg1 = "";
								
							CustomerCode = DR["CustomerCode"].ToString();
							Reference_Name = DR["reference_name"].ToString();
							Addr1 = DR["address1"].ToString();
							Addr2 = DR["address2"].ToString();
							ZipCode = DR["zipcode"].ToString();
							Telephone = DR["telephone"].ToString();
							Contact_Person = DR["contactperson"].ToString();								

							if (CustomerCode == "") Msg += " Customer Code, ";
							if (Reference_Name == "") Msg += " Customer Name, ";
							if (Addr1 == "") Msg += " Address 1, ";
							if (Addr2 == "") Msg += " Address 2, ";
							if (ZipCode == "") Msg += " Zipcode, ";
							if (Telephone == "") Msg += " Telephone, ";
							if (Contact_Person == "") Msg += " Contact Person, ";
							if (Msg != "")	break;

							//Check duplicate in Excel file
							intDupExcel = CheckDupExcel(arrCustCd, CustomerCode);

							if (intDupExcel > 1)
							{
								strMsg += "Customer Code : '" + CustomerCode.ToString() + "' duplicate in excel file\r\n";	
								break;
							}

							if (CustomerCode != "")
							{
								//if (DR["CustomerCode"].ToString().Trim() == "") { Msg += "CustomerCode, "; }
								if (DR["zipcode"].ToString().Trim() == "") { Msg1 += "Zipcode, "; }
								dsZipcode = zipcode.ckZipcodeInSystem(strAppID,strEnterpriseID,DR["zipcode"].ToString().Trim());
								if (dsZipcode.Tables[0].Rows.Count < 1)
								{
									Msg1 += " Zipcode, ";
								}
								if (DR["telephone"].ToString().Trim() == "") { Msg1 += "TelNum, "; }
								if (Msg1 != "")
								{
									Msg1 = Msg1.Substring(0, Msg1.Length - 2);
									strMsg += "CustomerCode " + CustomerCode + " Require data in column " + Msg1 + "\r\n";
								}
							}

							///check Length for each field - start
							if (Reference_Name.Length > 100)
								strMsg += "Customer Code: " + CustomerCode + " -  Length of Customer Name greater than 100 characters. \r\n";

							if (Addr1.Length > 100)
								strMsg += "Customer Code: " + CustomerCode + " -  Length of Address1 greater than 100 characters. \r\n";
							
							if (Addr2.Length > 100)
								strMsg += "Customer Code: " + CustomerCode + " -  Length of Address2 greater than 100 characters. \r\n";
							
							if (Telephone.Length > 20)
								strMsg += "Customer Code: " + CustomerCode + " -  Length of Telephone greater than 20 characters. \r\n";
							
							if (Contact_Person.Length > 100)
								strMsg += "Customer Code: " + CustomerCode + " -  Length of Contact Person greater than 100 characters. \r\n";
							///check Length for each field - end
					
						}//end loop
						if (Msg != "")
						{
							strMsg += "System require " + Msg + " data.\r\n";
						}
						
					} //end check DT != null
				} //end check DS != null
				else
				{
					strMsg += "Error tmp_References2 Table Technical.! Please contact Administrator!\r\n";
				}

				if (strMsg != "")
				{
					ErrorNumber = ErrorNumber + 1;
					txtException.Text +=  ErrorNumber.ToString() + ". System Require Data.! Please Check data in excel file!\r\n" + strMsg;
				}
			}
			catch(Exception ex)
			{
				ErrorNumber = ErrorNumber + 1;
				txtException.Text  += ErrorNumber.ToString() + ". Error Technical.! Please contact Administrator!\r\n";
			}
			finally
			{
				DS.Dispose();
				DT.Dispose();
				DTT.Dispose();
			}
		}
	


		//Verify step 4 : Check Format Data
		private void Step4()
		{
			DataTable dtReference = new DataTable(); 
			DataSet DS1 = new DataSet(); 
			DataSet DS2 = new DataSet(); 
			DataTable DT1 = new DataTable(); 
			DataTable DT2 = new DataTable(); 
			string Msg = "";
			string Msg1 = "";
			string Msg2 = "";
			string Msg3 = "";
			strMsg  = "";

			try
			{
				dtReference = (DataTable)Session["TMP_REFERENCE2"];
				if ((dtReference != null) && (dtReference.Rows.Count > 0))
				{
					foreach (DataRow drReference in dtReference.Rows)
					{
						Msg2 = ""; Msg3 = "";
						DS1 = null; DS2 = null;
						DT1 = null; DT2 = null;
						if(drReference["CustomerCode"].ToString().Trim() != "")
						{
							//check dupicate CustomerCode
							DS1 = ConsignmentNoteDAL.SearchReference2(strAppID, strEnterpriseID, PayerID, "", drReference["CustomerCode"].ToString().Trim());
							if ((DS1 != null) && (DS1.Tables.Count > 0) && (DS1.Tables[0] != null))
							{
								DT1 = DS1.Tables[0];
								if ((DT1 != null) && (DT1.Rows.Count > 0)) {Msg1 += drReference["CustomerCode"].ToString().Trim() + ", "; }
							}
							else
							{
								Msg3 += "CustomerCode, "; 
							}
						
							//check format zipcode
							DS2 = ConsignmentNoteDAL.SearchZipcode(strAppID, strEnterpriseID, drReference["zipcode"].ToString().Trim());
							if ((DS2 != null) && (DS2.Tables.Count > 0) && (DS2.Tables[0] != null))
							{
								DT2 = DS2.Tables[0];
								if ((DT2 != null) && (DT2.Rows.Count != 1)) {Msg2 += "zipcode, "; }
							}
							else
							{
								Msg3 += "zipcode, "; 
							}

							if (Msg2 != "")
							{
								Msg2 = Msg2.Substring(0, Msg2.Length - 2);
								Msg += "CustomerCode " + drReference["CustomerCode"].ToString().Trim() + " Format not correct in column " + Msg2 + "\r\n";
							}
							if (Msg3 != "")
							{
								Msg3 = Msg3.Substring(0, Msg3.Length - 2);
								Msg += "CustomerCode " + drReference["CustomerCode"].ToString().Trim() + " Find error in column " + Msg3 + "\r\n";
							}
						}
					}//end loop
				
					if (Msg1 != "")
					{
						Msg1 = Msg1.Substring(0, Msg1.Length - 2);
						strMsg += "Find CustomerCode dupicate " + Msg1 + "\r\n";
					}
					
					if (Msg != "")
					{
						strMsg += "Data not complete! Please check data format in excel file!\r\n" + Msg;
					}
				}
				else
				{
					strMsg += "Error Technical no data in tmp_References2 Table.! Please contact Administrator!\r\n";
				}

				if (strMsg != "")
				{
					ErrorNumber = ErrorNumber + 1;
					txtException.Text  += ErrorNumber.ToString() + ". " +  strMsg;
				}
			}
			catch(Exception ex)
			{
				ErrorNumber = ErrorNumber + 1;
				txtException.Text  += ErrorNumber.ToString() + ". Error Technical.! Please contact Administrator!\r\n";
			}
			finally
			{
				dtReference.Dispose();
				DS1.Dispose();
				DS2.Dispose();
				DT1.Dispose();
				DT2.Dispose();
			}
		}
		

		
		//Verify step 5 : Save Data to Database
		private void Step5()
		{
			DataTable dtReference = new DataTable();
			string Msg = "";
			strMsg  = "";

			try
			{
				dtReference = (DataTable)Session["TMP_REFERENCE2"];
				if ((dtReference != null) && (dtReference.Rows.Count > 0))
				{
					//Insert data from tmp_References2 to References2
					Msg = ConsignmentNoteDAL.InsertTmpReferences2ToTable(strAppID, strEnterpriseID, PayerID, dtReference);
					if (Msg != "")  { strMsg += "Data not complete! Please check Customer data!\r\n" + Msg; }
				}
				else
				{
					strMsg += "Error Technical no data in tmp_Consignment, tmp_Package Table.! Please contact Administrator!\r\n";
				}
			}
			catch(Exception ex)
			{
				ErrorNumber = ErrorNumber + 1;
				txtException.Text  += ErrorNumber.ToString() + ". Error Technical.! Please contact Administrator!\r\n";
			}
			finally
			{
				dtReference.Dispose();
			}
		}
		


		private int CheckDupExcel(string[] arrList, string strCustCd)
		{
			int count = 0;

			foreach (string itm in arrList)
			{
				if (itm.Equals(strCustCd))
				{
					count++;
				}
			}

			return count;
		}

	}
}
