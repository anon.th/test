using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;  
using System.Drawing;
using System.Text;
using System.Configuration;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;  
using System.Globalization; 
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using com.ties.BAL;  
using Cambro.Web.DbCombo;
using TIES;
using TIESDAL;
using System.IO;
using GenCode128;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


namespace com.ties
{
	/// <summary>
	/// Summary description for ConsignmentNotePrint.
	/// </summary>
	public class ConsignmentNotePrint : BasePage
	{
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.DataGrid dgResult;
		protected System.Web.UI.WebControls.Label lblNotFound;
		protected System.Web.UI.WebControls.Label ErrorMsg;
		protected System.Web.UI.WebControls.TextBox txtConsignmentNo;
		protected System.Web.UI.WebControls.TextBox txtCustomerCode;

		protected CrystalDecisions.Web.CrystalReportViewer CrystalReportViewer1;

		Utility utility = null;
		String appID = null;
		String enterpriseID = null;
		String userID = null;
		String PayerID = null;
		public int max = 0;
		public String strMsg = null;
		protected System.Web.UI.WebControls.DataGrid DataGrid1;
		public String BarcodeTagPath = "";
		private string FileName = "";
	
		public string LicensePlateTemplateReport
		{
			get
			{
				if(ViewState["LicensePlateTemplateReport"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["LicensePlateTemplateReport"];
				}
			}
			set
			{
				ViewState["LicensePlateTemplateReport"]=value;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			String ServerAction = "";
			ErrorMsg.Text = "";

			try
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				appID = utility.GetAppID();
				enterpriseID = utility.GetEnterpriseID();
				userID = utility.GetUserID();
				PayerID = Session["PayerID"] + "";
				BarcodeTagPath = (string)System.Configuration.ConfigurationSettings.AppSettings["BarcodeTagPath"];

				if (!Page.IsPostBack)
				{
					LicensePlateTemplateReport = EnterpriseConfigMgrDAL.LicensePlateTemplate(appID,enterpriseID);
					ServerAction = Request.QueryString["ServerAction"] + "";
					LoadData();
				}
				else
				{
					ServerAction = Request.Form["ServerAction"] + ""; 
				}
				max = Convert.ToInt32(ViewState["Max"]);
				switch (ServerAction.ToUpper())
				{
					case "CLEAR":
						ClearData();
						break;
					case "SEARCH":
						LoadData();
						break;
					case "PRINT":
						PrintData("ReportViewer");
						break;
					case "PRINTPDF":
						PrintData("PDF");
						break;
					case "PRINTLP": 
						PrintLicensePlate();
						break;
					case "EXPORTLP": 
						ExportLicensePlate();
						break;
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();

				ErrorMsg.Text = appException.Message.ToString();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgResult.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgResult_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void LoadData()
		{
			DataSet DS = new DataSet();
			ErrorMsg.Text = "";

			try
			{
				Session["SEARCH_CONSIGNMENT_PRINT"] = null;
				DS = ConsignmentNoteDAL.SearchConsignmentNote(appID, enterpriseID, PayerID, txtConsignmentNo.Text.Trim(), txtCustomerCode.Text.Trim());
				if (DS != null && DS.Tables.Count > 0)
				{
					Session["SEARCH_CONSIGNMENT_PRINT"] = DS.Tables[0];
					BindDG();
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString();
				ErrorMsg.Text = strMsg;
			} 
		}

		private void BindDG()
		{
			DataTable DT = new DataTable();
			ErrorMsg.Text = "";

			try
			{
				DT = (DataTable)Session["SEARCH_CONSIGNMENT_PRINT"];
				if (DT != null && DT.Rows.Count > 0)
				{
					//					max = DT.Rows.Count; 
					ViewState["Max"] = DT.Rows.Count;
					lblNotFound.Visible = false;
					dgResult.Visible = true;
  
					dgResult.DataSource = DT;
					dgResult.DataBind();  
				}
				else
				{
					lblNotFound.Visible  = true;
					dgResult.Visible =false;  
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString() ;
				ErrorMsg.Text = strMsg;
			}
		}

		private void ClearData()
		{
			try
			{
				txtConsignmentNo.Text = "";
				txtCustomerCode.Text = "";  
				LoadData(); 
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString() ;
				ErrorMsg.Text = strMsg;
			}
		}

		private void PrintData(string strFileType)
		{
			DataTable DT = new DataTable();
			DataRow DR = null;
			DataSet dsReport = new DataSet();
			DataTable dtReport = new DataTable();
			DataSet dsReport1 = new DataSet();
			DataTable dtReport1 = new DataTable();
			DataRow drReport1 = null;
			DataSet dsPage = new DataSet();
			DataTable dtPage = new DataTable(); 
			DataSet dsQty = new DataSet();
			DataTable dtQty = new DataTable();
			DataRow drQty = null;
			int i = 0;
			int j = 0;
			string ItemSelected = "";
			string tmpConsignmentNo = "";
			string tmpCopyPage = "";
			string ConsignmentNo = "";
			int jRowsAffected = 0;
			int TotalQty = 0;
			double TotalKilo = 0;
			Random rand = new Random();
			int RandNo = 0;
			string strUrl = "";
			ErrorMsg.Text = "";
			string strQty = "";

			try
			{
				DT = (DataTable)Session["SEARCH_CONSIGNMENT_PRINT"];
				if (DT != null && DT.Rows.Count > 0)
				{
					while (i < DT.Rows.Count) 
					{
						DR = DT.Rows[i];
						ItemSelected = Request.Form["chkITEM_" + i] + "";
						if (ItemSelected == "Y")
						{
							if (!DR["CONSIGNMENT_NO"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								tmpConsignmentNo += "'" + DR["CONSIGNMENT_NO"] + "',";
							}
						}
						i++;
					}

					if (tmpConsignmentNo.Length > 0) 
					{
						tmpConsignmentNo = tmpConsignmentNo.Substring(0, tmpConsignmentNo.Length - 1); 
					
						//Clone DataTable Report to sent into Crystal Report
						dsReport = ConsignmentNoteDAL.GetPrintReport(appID, enterpriseID, PayerID, tmpConsignmentNo);
						if (dsReport != null && dsReport.Tables.Count > 0 && dsReport.Tables[0] != null && dsReport.Tables[0].Rows.Count > 0)
						{
							dtReport = dsReport.Tables[0];
							dtReport1 = dtReport.Clone(); 
							dtReport1.Clear();

							//gen Random number for each reported
							RandNo = rand.Next();
							j = 1;

							////// add new column //////
							//Random No
							DataColumn RandomNo = new DataColumn("RandomNo", typeof(Int32));
							dtReport1.Columns.Add(RandomNo); 

							//running number
							DataColumn ID = new DataColumn("ID", typeof(Int32));
							dtReport1.Columns.Add(ID); 

							//Page Copy
							DataColumn Copy = new DataColumn("Copy", typeof(String));
							dtReport1.Columns.Add(Copy);
						
							//TotalQty
							DataColumn TotalCountQTY = new DataColumn("TotalCountQTY", typeof(String));
							dtReport1.Columns.Add(TotalCountQTY);

							//TotalKilo
							DataColumn TotalCountKilo = new DataColumn("TotalCountKilo", typeof(String));
							dtReport1.Columns.Add(TotalCountKilo);

							//ConsignmentNo Barcode
							DataColumn ConsignmentNoBarcode = new DataColumn("ConsignmentNoBarcode", typeof(String));
							dtReport1.Columns.Add(ConsignmentNoBarcode);

							//ServiceType Barcode
							DataColumn ServiceTypeBarcode = new DataColumn("ServiceTypeBarcode", typeof(String));
							dtReport1.Columns.Add(ServiceTypeBarcode);
						
							//Recipient ID Barcode
							DataColumn RecipientCustRefZipBarcode = new DataColumn("RecipientCustRefZipBarcode", typeof(String));
							dtReport1.Columns.Add(RecipientCustRefZipBarcode);
						
							//Total Quantity Barcode
							DataColumn TotalQTYBarcode = new DataColumn("TotalQTYBarcode", typeof(String));
							dtReport1.Columns.Add(TotalQTYBarcode);

							//Get print page copy
							dsPage = ConsignmentNoteDAL.GetParameterConsignment(appID, enterpriseID, PayerID);
							if (dsPage != null && dsPage.Tables.Count > 0 && dsPage.Tables[0] != null && dsPage.Tables[0].Rows.Count > 0)
							{
								dtPage = dsPage.Tables[0];
								foreach (DataRow drPage in dtPage.Rows)
								{
									tmpCopyPage = "";
									if ((drPage["ParamName"] + "" == "P1_Shipper") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "1. SHIPPER";}
									else if ((drPage["ParamName"] + "" == "P2_DataEntry") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "2. DATA ENTRY";}
									//else if ((drPage["ParamName"] + "" == "P3_HCR1") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "3. HCR-1";}
									else if ((drPage["ParamName"] + "" == "P3_HCR1") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "3. RTN TO ORI.";}
									else if ((drPage["ParamName"] + "" == "P4_HCR2") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "4. HCR-2";}
									else if ((drPage["ParamName"] + "" == "P5_Destination") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "5. DESTINATION";}
									else if ((drPage["ParamName"] + "" == "P6_Consignee") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "6. CONSIGNEE";}

									//check copy page
									if (tmpCopyPage != "")
									{
										//add data from dtReport to dtReport1
										foreach (DataRow drReport in dtReport.Rows)
										{

											ConsignmentNo = "";
											TotalQty = 0;
											TotalKilo = 0;

											ConsignmentNo = drReport["Consignment_No"] + "";

											//sum Total Qty
											dsQty = ConsignmentNoteDAL.SumQtyByConsignment(appID, enterpriseID, PayerID, ConsignmentNo);
											if (dsQty != null && dsQty.Tables.Count > 0 && dsQty.Tables[0] != null && dsQty.Tables[0].Rows.Count > 0)
											{
												drQty = dsQty.Tables[0].Rows[0];
												if (!drQty["TotalQty"].GetType().Equals(System.Type.GetType("System.DBNull"))) { TotalQty = (int)drQty["TotalQty"]; }
												if (!drQty["TotalKilo"].GetType().Equals(System.Type.GetType("System.DBNull"))) { TotalKilo = (double)drQty["TotalKilo"]; }
											}										

											//add data
											drReport1 = dtReport1.NewRow(); 
											drReport1["RandomNo"] = RandNo;
											drReport1["ID"] = j;
											drReport1["Copy"] = tmpCopyPage;
											drReport1["TotalCountQTY"] = TotalQty;
											drReport1["TotalCountKilo"] = TotalKilo;
											drReport1["consignment_No"] = ConsignmentNo;
											drReport1["PayerID"] = drReport["PayerID"] + "";
											drReport1["Telephone"] = drReport["Telephone"] + "";
											drReport1["LastUserID"] = drReport["LastUserID"] + "";
											drReport1["HCReturn"] = drReport["HCReturn"];
											drReport1["InvReturn"] = drReport["InvReturn"];
											drReport1["codamount"] = drReport["codamount"];
											drReport1["LastUpdateDate"] = drReport["LastUpdateDate"];
											drReport1["ConsignmentDate"] = drReport["ConsignmentDate"];
											drReport1["ServiceType"] = drReport["ServiceType"] + "";
											drReport1["address1"] = drReport["address1"] + "";
											drReport1["address2"] = drReport["address2"] + "";
											drReport1["zipcode"] = drReport["zipcode"] + "";
											drReport1["payer_name"] = drReport["payer_name"] + "";
											drReport1["cust_address1"] = drReport["cust_address1"] + "";
											drReport1["cust_address2"] = drReport["cust_address2"] + "";
											drReport1["Cust_State_Code"] = drReport["Cust_State_Code"] + "";
											drReport1["Cust_zipcode"] = drReport["Cust_zipcode"] + "";
											drReport1["state_name"] = drReport["state_name"] + "";
											drReport1["ref_code"] = drReport["ref_code"] + "";
											drReport1["company_address1"] = drReport["company_address1"] + "";
											drReport1["company_address2"] = drReport["company_address2"] + "";
											drReport1["company_zipcode"] = drReport["company_zipcode"] + "";
											drReport1["company_state_name"] = drReport["company_state_name"] + "";
											drReport1["sender_Contact_person"] = drReport["sender_Contact_person"] + "";
											drReport1["Recipion_state_name"] = drReport["Recipion_state_name"] + "";
											drReport1["sender_telephone"] = drReport["sender_telephone"] + "";
											drReport1["reference_name"] = drReport["reference_name"] + "";
											drReport1["DeclareValue"] = drReport["DeclareValue"];
											drReport1["cust_name"] = drReport["cust_name"] + "";
											drReport1["sender_zipcode"] = drReport["sender_zipcode"] + "";
											drReport1["cust_ref"] = drReport["cust_ref"] + "";
											drReport1["Special_Instruction"] = drReport["Special_Instruction"] + "";
											drReport1["recipion_contact_person"] = drReport["recipion_contact_person"] + "";

											//											if (ConsignmentNo != "") { drReport1["ConsignmentNoBarcode"] = Bar128AB(ConsignmentNo, 0).Replace("'","''"); }	
											if (ConsignmentNo != "") { drReport1["ConsignmentNoBarcode"] = "*"+ConsignmentNo+"*"; }
											//											if (drReport["ServiceType"] + "" != "") { drReport1["ServiceTypeBarcode"] = Bar128AB(drReport["ServiceType"] + "", 0).Replace("'","''"); }
											if (drReport["ServiceType"] + "" != "") { drReport1["ServiceTypeBarcode"] = "*"+(drReport["ServiceType"] + "")+"*"; }
											//											if (drReport["zipcode"] + "" != "") { drReport1["RecipientCustRefZipBarcode"] = Bar128AB(drReport["zipcode"] + "", 0).Replace("'","''"); }
											if (drReport["zipcode"] + "" != "") { drReport1["RecipientCustRefZipBarcode"] = "*"+(drReport["zipcode"] + "")+"*"; }
											//											if (TotalQty.ToString() != "") { drReport1["TotalQTYBarcode"] = Bar128AB(TotalQty.ToString(), 0).Replace("'","''"); }
											if (TotalQty < 10)
											{
												strQty = "0"+TotalQty.ToString();
											}
											else
											{
												strQty = TotalQty.ToString();
											}
											if (strQty != "") { drReport1["TotalQTYBarcode"] = "*"+(strQty)+"*"; }
										
											dtReport1.Rows.Add(drReport1);
											dtReport1.AcceptChanges();
											j++;
										} //end of loop Report									
									}//end check copy page
								} //end of loop page

								//insert report data into tempTable
								if (dtReport1 != null && dtReport1.Rows.Count > 0)
								{
									jRowsAffected = ConsignmentNoteDAL.InsertConsignmentNoteDetailReport(appID, enterpriseID, PayerID, RandNo, dtReport1);
									if (jRowsAffected > 0)
									{
										if (strFileType == "PDF")
										{//- For Pdf Report
											PrintConsPDF();											
										}
										else
										{//- For ReportViewer

											//gen report .rpt
											dsReport1.Tables.Add(dtReport1);
											if (dsReport1 != null && dsReport1.Tables.Count > 0)
											{
												strUrl = "../ReportViewer.aspx";
												Session["FORMID"] = "ConsignmentNotePrint";
												//Session["SESSION_DS1"] = dsReport1;
												ArrayList paramList = new ArrayList();
												paramList.Add(strUrl);
												String sScript = Utility.GetScript("openParentWindow.js",paramList);
												Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
											}
										}
									}
								}
							}
							else
							{
								ErrorMsg.Text = "Please set print ConsignmentNote.";
							}						
						}
						//}
					}
					else
					{
						ErrorMsg.Text = "Please select ConsignmentNo.";
					}
				}
			}
			catch(Exception ex)
			{
				strMsg = ex.Message.ToString();  
				ErrorMsg.Text = strMsg;
			}
			finally 
			{
				DT.Dispose(); 
				dsReport.Dispose();
				dtReport.Dispose();
				dsReport1.Dispose();
				dtReport1.Dispose();
				dsPage.Dispose();
				dtPage.Dispose();
				dsQty.Dispose();
				dtQty.Dispose();
			}
		}

		#region tmp PrintData() comment at 18/6/53 have column about tb_ConsignmentNoteDetail
		/*
		 private void PrintData()
		{
			DataTable DT = new DataTable();
			DataRow DR = null;
			DataSet dsReport = new DataSet();
			DataTable dtReport = new DataTable();
			DataSet dsReport1 = new DataSet();
			DataTable dtReport1 = new DataTable();
			DataRow drReport1 = null;
			DataSet dsPage = new DataSet();
			DataTable dtPage = new DataTable(); 
			DataSet dsQty = new DataSet();
			DataTable dtQty = new DataTable();
			DataRow drQty = null;
			int i = 0;
			int j = 0;
			string ItemSelected = "";
			string tmpConsignmentNo = "";
			string tmpCopyPage = "";
			string ConsignmentNo = "";
			int jRowsAffected = 0;
			int TotalQty = 0;
			Random rand = new Random();
			int RandNo = 0;
			string strUrl = "";
			ErrorMsg.Text = "";

			try
			{
				DT = (DataTable)Session["SEARCH_CONSIGNMENT_PRINT"];
				if (DT != null && DT.Rows.Count > 0)
				{
					while (i < DT.Rows.Count) 
					{
						DR = DT.Rows[i];
						ItemSelected = Request.Form["chkITEM_" + i] + "";
						if (ItemSelected == "Y")
						{
							if (!DR["CONSIGNMENT_NO"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								tmpConsignmentNo += "'" + DR["CONSIGNMENT_NO"] + "',";
							}
						}
						i++;
					}

					if (tmpConsignmentNo.Length > 0) 
					{
						tmpConsignmentNo = tmpConsignmentNo.Substring(0, tmpConsignmentNo.Length - 1); 
					
						//Clone DataTable Report to sent into Crystal Report
						dsReport = ConsignmentNoteDAL.GetPrintReport(appID, enterpriseID, PayerID, tmpConsignmentNo);
						if (dsReport != null && dsReport.Tables.Count > 0 && dsReport.Tables[0] != null && dsReport.Tables[0].Rows.Count > 0)
						{
							dtReport = dsReport.Tables[0];
							dtReport1 = dtReport.Clone(); 
							dtReport1.Clear();

							//gen Random number for each reported
							RandNo = rand.Next();
							j = 1;

							////// add new column //////
							//Random No
							DataColumn RandomNo = new DataColumn("RandomNo", typeof(Int32));
							dtReport1.Columns.Add(RandomNo); 

							//running number
							DataColumn ID = new DataColumn("ID", typeof(Int32));
							dtReport1.Columns.Add(ID); 

							//Page Copy
							DataColumn Copy = new DataColumn("Copy", typeof(String));
							dtReport1.Columns.Add(Copy);
						
							//TotalQty
							DataColumn TotalCountQTY = new DataColumn("TotalCountQTY", typeof(String));
							dtReport1.Columns.Add(TotalCountQTY);

							//ConsignmentNo Barcode
							DataColumn ConsignmentNoBarcode = new DataColumn("ConsignmentNoBarcode", typeof(String));
							dtReport1.Columns.Add(ConsignmentNoBarcode);

							//ServiceType Barcode
							DataColumn ServiceTypeBarcode = new DataColumn("ServiceTypeBarcode", typeof(String));
							dtReport1.Columns.Add(ServiceTypeBarcode);
						
							//Recipient ID Barcode
							DataColumn RecipientCustRefZipBarcode = new DataColumn("RecipientCustRefZipBarcode", typeof(String));
							dtReport1.Columns.Add(RecipientCustRefZipBarcode);
						
							//Total Quantity Barcode
							DataColumn TotalQTYBarcode = new DataColumn("TotalQTYBarcode", typeof(String));
							dtReport1.Columns.Add(TotalQTYBarcode);

							//Get print page copy
							dsPage = ConsignmentNoteDAL.GetParameterConsignment(appID, enterpriseID, PayerID);
							if (dsPage != null && dsPage.Tables.Count > 0 && dsPage.Tables[0] != null && dsPage.Tables[0].Rows.Count > 0)
							{
								dtPage = dsPage.Tables[0];
								foreach (DataRow drPage in dtPage.Rows)
								{
									tmpCopyPage = "";
									if ((drPage["ParamName"] + "" == "P1_Shipper") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "1. SHIPPER";}
									else if ((drPage["ParamName"] + "" == "P2_DataEntry") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "2. DATA ENTRY";}
									else if ((drPage["ParamName"] + "" == "P3_HCR1") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "3. HCR-1";}
									else if ((drPage["ParamName"] + "" == "P4_HCR2") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "4. HCR-2";}
									else if ((drPage["ParamName"] + "" == "P5_Destination") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "5. DESTINATION";}
									else if ((drPage["ParamName"] + "" == "P6_Consignee") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "6. CONSIGNEE";}

									//check copy page
									if (tmpCopyPage != "")
									{
										//add data from dtReport to dtReport1
										foreach (DataRow drReport in dtReport.Rows)
										{

											ConsignmentNo = "";

											ConsignmentNo = drReport["Consignment_No"] + "";

											//sum Total Qty
											dsQty = ConsignmentNoteDAL.SumQtyByConsignment(appID, enterpriseID, PayerID, ConsignmentNo);
											if (dsQty != null && dsQty.Tables.Count > 0 && dsQty.Tables[0] != null && dsQty.Tables[0].Rows.Count > 0)
											{
												drQty = dsQty.Tables[0].Rows[0];
												if (!drQty["TotalQty"].GetType().Equals(System.Type.GetType("System.DBNull"))) { TotalQty = (int)drQty["TotalQty"]; }
											}
										

											//add data
											drReport1 = dtReport1.NewRow(); 
											drReport1["RandomNo"] = RandNo;
											drReport1["ID"] = j;
											drReport1["Copy"] = tmpCopyPage;
											drReport1["TotalCountQTY"] = TotalQty;
											drReport1["seq_no"] = drReport["seq_no"];
											drReport1["consignment_No"] = ConsignmentNo;
											drReport1["PayerID"] = drReport["PayerID"] + "";
											drReport1["Telephone"] = drReport["Telephone"] + "";
											drReport1["LastUserID"] = drReport["LastUserID"] + "";
											drReport1["HCReturn"] = drReport1["HCReturn"];
											drReport1["InvReturn"] = drReport1["InvReturn"];
											drReport1["codamount"] = drReport["codamount"];
											drReport1["LastUpdateDate"] = drReport["LastUpdateDate"];
											drReport1["ConsignmentDate"] = drReport["ConsignmentDate"];
											drReport1["ServiceType"] = drReport["ServiceType"] + "";
											drReport1["Boxsize"] = drReport["Boxsize"] + "";
											drReport1["kilo"] = drReport["kilo"];
											drReport1["Qty"] = drReport["Qty"];
											drReport1["box_desc"] = drReport["box_desc"] + "";
											drReport1["charge_rate"] = drReport["charge_rate"];
											drReport1["address1"] = drReport["address1"] + "";
											drReport1["address2"] = drReport["address2"] + "";
											drReport1["zipcode"] = drReport["zipcode"] + "";
											drReport1["payer_name"] = drReport["payer_name"] + "";
											drReport1["cust_address1"] = drReport["cust_address1"] + "";
											drReport1["cust_address2"] = drReport["cust_address2"] + "";
											drReport1["Cust_State_Code"] = drReport["Cust_State_Code"] + "";
											drReport1["Cust_zipcode"] = drReport["Cust_zipcode"] + "";
											drReport1["state_name"] = drReport["state_name"] + "";
											drReport1["ref_code"] = drReport["ref_code"] + "";
											drReport1["company_address1"] = drReport["company_address1"] + "";
											drReport1["company_address2"] = drReport["company_address2"] + "";
											drReport1["company_zipcode"] = drReport["company_zipcode"] + "";
											drReport1["company_state_name"] = drReport["company_state_name"] + "";
											drReport1["sender_Contact_person"] = drReport["sender_Contact_person"] + "";
											drReport1["Recipion_state_name"] = drReport["Recipion_state_name"] + "";
											drReport1["sender_telephone"] = drReport["sender_telephone"] + "";
											drReport1["reference_name"] = drReport["reference_name"] + "";
											drReport1["DeclareValue"] = drReport["DeclareValue"];
											drReport1["cust_name"] = drReport["cust_name"] + "";
											drReport1["sender_zipcode"] = drReport["sender_zipcode"] + "";
											drReport1["cust_ref"] = drReport["cust_ref"] + "";
											drReport1["Special_Instruction"] = drReport["Special_Instruction"] + "";
											drReport1["recipion_contact_person"] = drReport["recipion_contact_person"] + "";

											if (ConsignmentNo != "") { drReport1["ConsignmentNoBarcode"] = Bar128AB(ConsignmentNo, 0).Replace("'","''"); }		
											if (drReport["ServiceType"] + "" != "") { drReport1["ServiceTypeBarcode"] = Bar128AB(drReport["ServiceType"] + "", 0).Replace("'","''"); }
											if (drReport["zipcode"] + "" != "") { drReport1["RecipientCustRefZipBarcode"] = Bar128AB(drReport["zipcode"] + "", 0).Replace("'","''"); }
											if (TotalQty.ToString() != "") { drReport1["TotalQTYBarcode"] = Bar128AB(TotalQty.ToString(), 0).Replace("'","''"); }
										
											dtReport1.Rows.Add(drReport1);
											dtReport1.AcceptChanges();
											j++;
										} //end of loop Report
									
									}//end check copy page
								} //end of loop page

								//insert report data into tempTable
								if (dtReport1 != null && dtReport1.Rows.Count > 0)
								{
									jRowsAffected = ConsignmentNoteDAL.InsertConsignmentNoteDetailReport(appID, enterpriseID, PayerID, RandNo, dtReport1);
									if (jRowsAffected > 0)
									{
										//gen report .rpt
										dsReport1.Tables.Add(dtReport1);
										if (dsReport1 != null && dsReport1.Tables.Count > 0)
										{
											strUrl = "../ReportViewer.aspx";
											Session["FORMID"] = "ConsignmentNotePrint";
											//Session["SESSION_DS1"] = dsReport1;
											ArrayList paramList = new ArrayList();
											paramList.Add(strUrl);
											String sScript = Utility.GetScript("openParentWindow.js",paramList);
											Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
										}
									}
								}
							}
							else
							{
								ErrorMsg.Text = "Please set print ConsignmentNote.";
							}

						
						}
						//}
					}
					else
					{
						ErrorMsg.Text = "Please select ConsignmentNo.";
					}
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message.ToString() ;
				ErrorMsg.Text = strMsg;
			}
			finally 
			{
				dsPage.Dispose(); 
			}
		} 
		 */
		#endregion

#region PrintPDF
		private void PrintConsPDF()
		{
			string strConsNo = "";
			DataTable dtReport, dtReportDt = new DataTable();
			dtReport = GetConsPdfDS(ref strConsNo);
			dtReportDt = GetConsPdfDetailDS(strConsNo);

			try
			{
				///Print to PDF ----- start ---------------------------------------------------------------------
				///
				////Define Crystal Reports variables
				ReportDocument crReportDocument = new TIES.WebUI.ConsignmentNote.PreprintConsPdf();

				ExportOptions crExportOptions  = new ExportOptions();
				DiskFileDestinationOptions crDiskFileDestinationOptions  = new DiskFileDestinationOptions();
 
				String Fname = "";

				crReportDocument.SetDataSource(dtReport);
				crReportDocument.OpenSubreport("PreprintConsPdfDetail.rpt").SetDataSource(dtReportDt);

				Fname = Server.MapPath("..\\Export") + "\\tmpConsPDF.pdf";
				
				crDiskFileDestinationOptions = new DiskFileDestinationOptions();
				crDiskFileDestinationOptions.DiskFileName = Fname;
				crExportOptions = crReportDocument.ExportOptions;

				crExportOptions.DestinationOptions = crDiskFileDestinationOptions;
				crExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
				crExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;

				crExportOptions.FormatOptions = new PdfRtfWordFormatOptions();
				crReportDocument.Export();

				////The following code writes the pdf file to the Client�s browser.
				Response.ClearContent();
				Response.ClearHeaders();
				Response.ContentType = "application/pdf";
				Response.WriteFile(Fname);
				Response.Flush();
				//Response.Close();

				////delete the exported file from disk
				System.IO.File.Delete(Fname);
				///
				///Print to PDF ----- end -----------------------------------------------------------------------
			}
			catch (Exception err)
			{
				ErrorMsg.Text = err.Message.ToString();
			}
			finally 
			{
				dtReport.Dispose();
				dtReportDt.Dispose();
			}
		}

		
		private DataTable GetConsPdfDS(ref String strConsignNo ) 
		{
			DataSet dsReport = new DataSet();
			DataTable dtReport = new DataTable();						
			ErrorMsg.Text =	"";
			DataTable dtCons = new DataTable();
			
			dtCons.Columns.Add("RandomNo"); 
			dtCons.Columns.Add("ID");
			dtCons.Columns.Add("Copy");
			dtCons.Columns.Add("Consignment_no");
			dtCons.Columns.Add("TotalCountQTY");
			dtCons.Columns.Add("TotalCountKilo");
			dtCons.Columns.Add("PayerID");							
			dtCons.Columns.Add("Telephone");
			dtCons.Columns.Add("LastUserID");
			dtCons.Columns.Add("HCReturn");
			dtCons.Columns.Add("InvReturn");					
			dtCons.Columns.Add("CODamount");
			dtCons.Columns.Add("LastUpdateDate");
			dtCons.Columns.Add("ConsignmentDate");							
			dtCons.Columns.Add("ServiceType");							
			dtCons.Columns.Add("Address1");
			dtCons.Columns.Add("Address2");
			dtCons.Columns.Add("Zipcode");							
			dtCons.Columns.Add("Payer_Name");
			dtCons.Columns.Add("Cust_Address1");
			dtCons.Columns.Add("Cust_Address2");
			dtCons.Columns.Add("Cust_State_Code");
			dtCons.Columns.Add("Cust_Zipcode");							
			dtCons.Columns.Add("State_Name");
			dtCons.Columns.Add("Ref_Code");
			dtCons.Columns.Add("Company_Address1");
			dtCons.Columns.Add("Company_Address2");
			dtCons.Columns.Add("Company_Zipcode");
			dtCons.Columns.Add("Company_State_Name");							
			dtCons.Columns.Add("Sender_Contact_Person");							
			dtCons.Columns.Add("Recipion_State_Name");
			dtCons.Columns.Add("Sender_Telephone");
			dtCons.Columns.Add("Reference_Name");
			dtCons.Columns.Add("DeclareValue");
			dtCons.Columns.Add("Cust_Name");
			dtCons.Columns.Add("Sender_Zipcode");
			dtCons.Columns.Add("Cust_Ref");
			dtCons.Columns.Add("Special_Instruction");							
			dtCons.Columns.Add("Recipion_Contact_Person");
			dtCons.Columns.Add("ConsignmentNoBarcode");
			dtCons.Columns.Add("ServiceTypeBarcode");
			dtCons.Columns.Add("RecipientCustRefBarcode");
			dtCons.Columns.Add("TotalQTYBarcode");
			dtCons.Columns.Add("UserID");			

			try 
			{
				//// Get DataTable Consignment
				dsReport = ConsignmentNoteDAL.GetConsReport(appID, enterpriseID);

				if (dsReport != null && dsReport.Tables.Count > 0 && dsReport.Tables[0] != null && dsReport.Tables[0].Rows.Count > 0)
				{
					dtReport = dsReport.Tables[0];

					strConsignNo = "";

					for (int i = 0; i < dtReport.Rows.Count; i++) 
					{
						DataRow drReport = null;
						drReport = dtReport.Rows[i];

						DataRow dr;
						dr = dtCons.NewRow();

						dr["RandomNo"] = drReport["RandomNo"].ToString();
						dr["ID"] = Convert.ToInt32(drReport["ID"].ToString());
						dr["Copy"] = drReport["Copy"].ToString();
						dr["Consignment_no"] = drReport["Consignment_no"].ToString();

						strConsignNo += "'" + drReport["Consignment_no"].ToString() + "',";

						dr["TotalCountQTY"] = Convert.ToInt32(drReport["TotalCountQTY"].ToString());
						dr["TotalCountKilo"] = String.Format("{0:0.00}", Convert.ToDouble(drReport["TotalCountKilo"].ToString()));
						dr["PayerID"] = drReport["PayerID"].ToString();
						dr["Telephone"] = drReport["Telephone"].ToString();
						dr["LastUserID"] = drReport["LastUserID"].ToString();				
						dr["HCReturn"] = (Convert.ToBoolean(drReport["HCReturn"].ToString()) == true)? 1 : 0;
						dr["InvReturn"] = (Convert.ToBoolean(drReport["InvReturn"].ToString()) == true)? 1 : 0;
						dr["CODamount"] = String.Format("{0:0.00}", Convert.ToDouble(drReport["CODamount"].ToString()));
						dr["LastUpdateDate"] = drReport["LastUpdateDate"].ToString();
						dr["ConsignmentDate"] = Convert.ToDouble(drReport["ConsignmentDate"].ToString());	
						dr["ServiceType"] = drReport["ServiceType"].ToString();
						dr["Address1"] = drReport["Address1"].ToString();
						dr["Address2"] = drReport["Address2"].ToString();
						dr["Zipcode"] = drReport["Zipcode"].ToString();
						dr["Payer_Name"] = drReport["Payer_Name"].ToString();
						dr["Cust_Address1"] = drReport["Cust_Address1"].ToString();
						dr["Cust_Address2"] = drReport["Cust_Address2"].ToString();
						dr["Cust_State_Code"] = drReport["Cust_State_Code"].ToString();
						dr["Cust_Zipcode"] = drReport["Cust_Zipcode"].ToString();	
						dr["State_Name"] = drReport["State_Name"].ToString();
						dr["Ref_Code"] = drReport["Ref_Code"].ToString();
						dr["Company_Address1"] = drReport["Company_Address1"].ToString();
						dr["Company_Address2"] = drReport["Company_Address2"].ToString();
						dr["Company_Zipcode"] = drReport["Company_Zipcode"].ToString();
						dr["Company_State_Name"] = drReport["Company_State_Name"].ToString();
						dr["Sender_Contact_Person"] = drReport["Sender_Contact_Person"].ToString();
						dr["Recipion_State_Name"] = drReport["Recipion_State_Name"].ToString();
						dr["Sender_Telephone"] = drReport["Sender_Telephone"].ToString();
						dr["Reference_Name"] = drReport["Reference_Name"].ToString();
						dr["DeclareValue"] = String.Format("{0:0.00}", Convert.ToDouble(drReport["DeclareValue"].ToString()));
						dr["Cust_Name"] = drReport["Cust_Name"].ToString();
						dr["Sender_Zipcode"] = drReport["Sender_Zipcode"].ToString();
						dr["Cust_Ref"] = drReport["Cust_Ref"].ToString();
						dr["Special_Instruction"] = drReport["Special_Instruction"].ToString();
						dr["Recipion_Contact_Person"] = drReport["Recipion_Contact_Person"].ToString();
						dr["ConsignmentNoBarcode"] = drReport["ConsignmentNoBarcode"].ToString();
						dr["ServiceTypeBarcode"] = drReport["ServiceTypeBarcode"].ToString();
						dr["RecipientCustRefBarcode"] = drReport["RecipientCustRefZipBarcode"].ToString();
						dr["TotalQTYBarcode"] = drReport["TotalQTYBarcode"].ToString();
						dr["UserID"] = utility.GetUserID().ToUpper();
					
						dtCons.Rows.Add(dr);
					}
					strConsignNo = strConsignNo.Substring(0, strConsignNo.Length - 1);
				}
				return dtCons;
			}
			catch(Exception err)
			{
				ErrorMsg.Text = err.Message.ToString();

				dtCons = null;
				return dtCons;
			}
			finally
			{
				dsReport.Dispose();
				dtReport.Dispose();
				dtCons.Dispose();
			}
		}

		
		private DataTable GetConsPdfDetailDS(string consNo) 
		{
			DataSet dsReport = new DataSet();
			DataTable dtReport = new DataTable();						
			ErrorMsg.Text =	"";
			DataTable dtConsDt = new DataTable();

			dtConsDt.Columns.Add("Consignment_No");
			dtConsDt.Columns.Add("Seq_No");
			dtConsDt.Columns.Add("Description");
			dtConsDt.Columns.Add("Boxsize");							
			dtConsDt.Columns.Add("kilo");
			dtConsDt.Columns.Add("Qty");
			dtConsDt.Columns.Add("Flag_Export");

			try
			{
				//// Get DataTable Consignment
				dsReport = ConsignmentNoteDAL.GetConsDetailReport(appID, enterpriseID, consNo);

				if (dsReport != null && dsReport.Tables.Count > 0 && dsReport.Tables[0] != null && dsReport.Tables[0].Rows.Count > 0)
				{
					dtReport = dsReport.Tables[0];

					for (int i = 0; i < dtReport.Rows.Count; i++) 
					{
						DataRow drReport = null;
						drReport = dtReport.Rows[i];

						DataRow dr;
						dr = dtConsDt.NewRow();

						dr["Consignment_no"] = drReport["Consignment_no"].ToString();	
						dr["Seq_No"] = Convert.ToInt32(drReport["Seq_No"].ToString());
						dr["Description"] = drReport["Description"].ToString();	
						dr["Boxsize"] = ((drReport["Boxsize"].ToString()=="")) ? "Cus." : drReport["Boxsize"].ToString();
						dr["kilo"] = String.Format("{0:0.00}", Convert.ToDouble(drReport["kilo"].ToString()));
						dr["Qty"] = Convert.ToInt32(drReport["Qty"].ToString());
						dr["Flag_Export"] = drReport["Flag_Export"].ToString();
					
						dtConsDt.Rows.Add(dr);
					}
				}
				return dtConsDt;
			}
			catch(Exception err)
			{
				ErrorMsg.Text = err.Message.ToString();

				dtConsDt = null;
				return dtConsDt;
			}
			finally
			{
				dsReport.Dispose();
				dtReport.Dispose();
				dtConsDt.Dispose();
			}
		}

#endregion PrintPDF

#region PrintLicensePlate
		
		//start boon - 2010/10/11 -- 2
		private void PrintLicensePlate()
		{
			DataTable dt = new DataTable();
			dt = GetPrintLicensePlate();

			if (dt.Rows.Count > 0)
			{
				DataTable dtPrint = new DataTable();
				dtPrint.Columns.Add("consignment");
				dtPrint.Columns.Add("reference_name");
				dtPrint.Columns.Add("telephone");
				dtPrint.Columns.Add("address");
				dtPrint.Columns.Add("contactperson");
				dtPrint.Columns.Add("zip");
				dtPrint.Columns.Add("route1");
				dtPrint.Columns.Add("route2");
				dtPrint.Columns.Add("service");
				dtPrint.Columns.Add("derv");
				dtPrint.Columns.Add("province");
				dtPrint.Columns.Add("sender_name");
				dtPrint.Columns.Add("sender_tel");
				dtPrint.Columns.Add("cust_ref");
				dtPrint.Columns.Add("uid");
				dtPrint.Columns.Add("swbid");
				dtPrint.Columns.Add("dclocation");
				dtPrint.Columns.Add("dateprint");
				dtPrint.Columns.Add("datescan");
				dtPrint.Columns.Add("package");
				dtPrint.Columns.Add("Label");

				foreach (DataRow dr2 in dt.Rows) 
				{
					string strPackNo = dr2["no_of_package"].ToString().Replace(" ", "").Substring(0, dr2["no_of_package"].ToString().Replace(" ", "").IndexOf("of"));
					string strPackAll = dr2["no_of_package"].ToString().Replace(" ", "").Substring(dr2["no_of_package"].ToString().Replace(" ", "").IndexOf("of") + 2);
					DataRow dr;
					dr = dtPrint.NewRow();
					dr["consignment"] = dr2["consignment"];
					dr["reference_name"] = dr2["reference_name"];
					dr["telephone"] = dr2["telephone"];
					dr["address"] = dr2["address"];
					dr["contactperson"] = dr2["contactperson"];
					dr["zip"] = dr2["zipcode"];
					dr["route1"] = dr2["linehaul1"];
					dr["route2"] = dr2["linehaul2"];
					dr["service"] = dr2["service_type"];
					dr["derv"] = dr2["delivery_route_code"];
					dr["province"] = dr2["province"];
					dr["sender_name"] = dr2["sender_name"];
					dr["sender_tel"] = dr2["sender_tel"];
					dr["cust_ref"] = dr2["cust_ref"];
					dr["uid"] = dr2["userid"];
					dr["swbid"] = dr2["swb_id"];
					dr["dclocation"] = dr2["dc_location"];
					dr["dateprint"] = dr2["printing_dt"];
					dr["datescan"] = dr2["scanning_dt"];
					dr["package"] = dr2["no_of_package"];
//					dr["Label"] = Bar128AB(dr2["consignment"].ToString() + " " + strPackNo + " " + strPackAll + " " + dr2["delivery_route_code"].ToString(), 1);
					////Code39 barcode, space is "="
					////Code39 barcode, start with "*" and end with "*"
					dr["Label"] = "*" + dr2["consignment"].ToString() + "=" + strPackNo + "=" + strPackAll + "=" + dr2["delivery_route_code"].ToString()+ "*";
					dtPrint.Rows.Add(dr);
				}

				try
				{
//					ReportDocument rpt = new TIES.WebUI.ConsignmentNote.LicensePlate();			
//					rpt.SetDataSource(dtPrint);
//					rpt.PrintOptions.PaperSource = CrystalDecisions.Shared.PaperSource.Lower;
//					rpt.PrintOptions.PrinterName = "Zebra TLP2844-Z";
//					rpt.PrintToPrinter(1, false, 1, dtPrint.Rows.Count); 


					///Print to PDF ----- start ---------------------------------------------------------------------
					///
					////Define Crystal Reports variables
					
					string reporttemplate = LicensePlateTemplateReport;
					Session["REPORT_TEMPLATE"]=reporttemplate;
					TIES.WebUI.ConsignmentNote.LicensePlate crReportDocument = new TIES.WebUI.ConsignmentNote.LicensePlate();
					crReportDocument.ResourceName=reporttemplate;

					ExportOptions crExportOptions  = new ExportOptions();
					DiskFileDestinationOptions crDiskFileDestinationOptions  = new DiskFileDestinationOptions();
 
					String Fname = "";

					crReportDocument.SetDataSource(dtPrint);

					Fname = Server.MapPath("..\\Export") + "\\tmpLicensPlate.pdf";
				
					crDiskFileDestinationOptions = new DiskFileDestinationOptions();
					crDiskFileDestinationOptions.DiskFileName = Fname;
					crExportOptions = crReportDocument.ExportOptions;

					crExportOptions.DestinationOptions = crDiskFileDestinationOptions;
					crExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
					crExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;

					crExportOptions.FormatOptions = new PdfRtfWordFormatOptions();
					crReportDocument.Export();

					////The following code writes the pdf file to the Client�s browser.
					Response.ClearContent();
					Response.ClearHeaders();
					Response.ContentType = "application/pdf";
					Response.WriteFile(Fname);
					Response.Flush();
					//Response.Close();

					////delete the exported file from disk
					System.IO.File.Delete(Fname);
					///
					///Print to PDF ----- end -----------------------------------------------------------------------
				}
				catch (Exception err)
				{
					ErrorMsg.Text = err.Message.ToString();
				}
				finally 
				{
					dt.Dispose(); 
					dtPrint.Dispose();
				}
			}
			else
			{
				ErrorMsg.Text = "Please select ConsignmentNo.";
			}
		}

		private DataTable GetPrintLicensePlate() 
		{
			DataTable DT = new DataTable();
			DataRow DR = null;
			DataSet dsReport = new DataSet();
			DataTable dtReport = new DataTable();
			int i = 0;
			string ItemSelected = "";
			string tmpConsignmentNo = "";
			ErrorMsg.Text = "";

			DataTable printingTb = new DataTable();
			printingTb.Columns.Add("consignment");
			printingTb.Columns.Add("reference_name");
			printingTb.Columns.Add("telephone");
			printingTb.Columns.Add("address");
			printingTb.Columns.Add("contactperson");
			printingTb.Columns.Add("zipcode");
			printingTb.Columns.Add("linehaul1");
			printingTb.Columns.Add("linehaul2");
			printingTb.Columns.Add("service_type");
			printingTb.Columns.Add("delivery_route_code");
			printingTb.Columns.Add("province");
			printingTb.Columns.Add("sender_name");
			printingTb.Columns.Add("sender_tel");
			printingTb.Columns.Add("cust_ref");
			printingTb.Columns.Add("userid");
			printingTb.Columns.Add("swb_id");
			printingTb.Columns.Add("dc_location");
			printingTb.Columns.Add("printing_dt");
			printingTb.Columns.Add("scanning_dt");
			printingTb.Columns.Add("no_of_package");

			try
			{
				//// Get DataTable Consignment by Session at LoadData method
				DT = (DataTable)Session["SEARCH_CONSIGNMENT_PRINT"];
			
				if (DT != null && DT.Rows.Count > 0)
				{
					while (i < DT.Rows.Count) 
					{
						DR = DT.Rows[i];
						ItemSelected = Request.Form["chkITEM_" + i] + "";
						if (ItemSelected == "Y")
						{
							if (!DR["CONSIGNMENT_NO"].GetType().Equals(System.Type.GetType("System.DBNull")))
							{
								tmpConsignmentNo += "'" + DR["CONSIGNMENT_NO"] + "',";
							}
						}
						i++;
					}

					if (tmpConsignmentNo.Length > 0) 
					{
						tmpConsignmentNo = tmpConsignmentNo.Substring(0, tmpConsignmentNo.Length - 1); 
					
						//Get DataTable Report to send into Crystal Report
						dsReport = ConsignmentNoteDAL.GetPrintReportLicensePlate(appID, enterpriseID, PayerID, tmpConsignmentNo);

						if (dsReport != null && dsReport.Tables.Count > 0 && dsReport.Tables[0] != null && dsReport.Tables[0].Rows.Count > 0)
						{
							dtReport = dsReport.Tables[0];

							for (int ii = 0; ii < dtReport.Rows.Count; ii++) 
							{						
								DataSet dsRouteInfo = new DataSet();
								DataSet dsState = new DataSet();
								DataRow drReport = null;
								drReport = dtReport.Rows[ii];

								dsRouteInfo = ConsignmentNoteDAL.GetSWBRouteInformation(appID, enterpriseID, drReport["ZIP_CODE"].ToString());

								dsState = ConsignmentNoteDAL.GetDMSState(appID, enterpriseID, dsRouteInfo.Tables[0].Rows[0]["dest_postal_code"].ToString());

								if (Convert.ToInt32(drReport["TOTAL_QTY"].ToString()) == 1)		////Qty = 1
								{
									DataRow dr;
									dr = printingTb.NewRow();
									dr["consignment"] = drReport["CONSIGNMENT_NO"].ToString();
									dr["reference_name"] = drReport["REFERENCE_NAME"].ToString();
									dr["telephone"] = drReport["TELEPHONE"].ToString();
									dr["address"] = drReport["ADDRESS"].ToString();
									dr["contactperson"] = drReport["CONTACTPERSON"].ToString();
									dr["zipcode"] = drReport["ZIP_CODE"].ToString();
									dr["linehaul1"] = dsRouteInfo.Tables[0].Rows[0]["line_haul1"].ToString();
									dr["linehaul2"] = dsRouteInfo.Tables[0].Rows[0]["line_haul2"].ToString();
									dr["service_type"] = drReport["SERVICE_TYPE"].ToString();
									dr["delivery_route_code"] = dsRouteInfo.Tables[0].Rows[0]["delivery_route_code"].ToString();
									dr["province"] = dsState.Tables[0].Rows[0]["state_name"].ToString();
									dr["sender_name"] = drReport["SENDER_NAME"].ToString();
									dr["sender_tel"] = drReport["SENDER_TEL"].ToString();
									dr["cust_ref"] = drReport["cust_ref"].ToString();
									dr["userid"] = userID.ToUpper();
									dr["swb_id"] = PayerID.ToUpper();
									dr["dc_location"] = dsRouteInfo.Tables[0].Rows[0]["destination_DC"].ToString();
									dr["printing_dt"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
									dr["scanning_dt"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
									dr["no_of_package"] = Convert.ToInt32(drReport["TOTAL_QTY"].ToString()) + " of " + Convert.ToInt32(drReport["TOTAL_QTY"].ToString());
									printingTb.Rows.Add(dr);
								} 
								else if (Convert.ToInt32(drReport["TOTAL_QTY"].ToString()) > 1)		////Qty >1
								{
									for (int j = 1; j <= Convert.ToInt32(drReport["TOTAL_QTY"].ToString()); j++) 
									{
										DataRow dr;
										dr = printingTb.NewRow();
										dr["consignment"] = drReport["CONSIGNMENT_NO"].ToString();
										dr["reference_name"] = drReport["REFERENCE_NAME"].ToString();
										dr["telephone"] = drReport["TELEPHONE"].ToString();
										dr["address"] = drReport["ADDRESS"].ToString();
										dr["contactperson"] = drReport["CONTACTPERSON"].ToString();
										dr["zipcode"] = drReport["ZIP_CODE"].ToString();
										dr["linehaul1"] = dsRouteInfo.Tables[0].Rows[0]["line_haul1"].ToString();
										dr["linehaul2"] = dsRouteInfo.Tables[0].Rows[0]["line_haul2"].ToString();
										dr["service_type"] = drReport["SERVICE_TYPE"].ToString();
										dr["delivery_route_code"] = dsRouteInfo.Tables[0].Rows[0]["delivery_route_code"].ToString();
										dr["province"] = dsState.Tables[0].Rows[0]["state_name"].ToString();
										dr["sender_name"] = drReport["SENDER_NAME"].ToString();
										dr["sender_tel"] = drReport["SENDER_TEL"].ToString();
										dr["cust_ref"] = drReport["cust_ref"].ToString();
										dr["userid"] = userID.ToUpper();
										dr["swb_id"] = PayerID.ToUpper();
										dr["dc_location"] = dsRouteInfo.Tables[0].Rows[0]["destination_DC"].ToString();
										dr["printing_dt"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
										dr["scanning_dt"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
										dr["no_of_package"] = j + " of " + Convert.ToInt32(drReport["TOTAL_QTY"].ToString());
										printingTb.Rows.Add(dr);
									}
								}
							}
						}
					}
				}
				return printingTb;
			}
			catch(Exception err)
			{
				ErrorMsg.Text = err.Message.ToString();

				printingTb = null;
				return printingTb;
			}
			finally
			{
				DT.Dispose();
				dsReport.Dispose();
				dtReport.Dispose();
			}
		}

		private string AddLength(string str) 
		{
			string strReturn = "";
			if (str.Length == 1)
				strReturn = "00" + str;
			else if (str.Length == 2)
				strReturn = "0" + str;
			else if (str.Length == 3)
				strReturn = str;

			return strReturn;
		}   
		//end boon - 2010/10/11 -- 2

#endregion PrintLicensePlate

#region ExportLicensePlate

        //start boon - 2010/10/18 -- 1
		private void ExportLicensePlate() 
		{   
			DataTable DT = new DataTable();
			DataRow DR = null;
			DataSet dsReport = new DataSet();
			DataTable dtReport = new DataTable();
			int i = 0;
			string strConsignmentNo = ""; 
      
			//// Get DataTable Consignment by Session at LoadData method
			DT = (DataTable)Session["SEARCH_CONSIGNMENT_PRINT"];
			
			if (DT != null && DT.Rows.Count > 0)
			{
				while (i < DT.Rows.Count) 
				{
					DR = DT.Rows[i];
					if (!DR["CONSIGNMENT_NO"].GetType().Equals(System.Type.GetType("System.DBNull")))
					{
						strConsignmentNo += "'" + DR["CONSIGNMENT_NO"] + "',";
					}
					i++;
				}
			}
			 
			if (strConsignmentNo.Length > 0) 
			{
				DataTable exportTb = new DataTable();

				exportTb.Columns.Add("Consignment_no");
				exportTb.Columns.Add("Service_Type");
				exportTb.Columns.Add("Dest_Postal_Code");
				exportTb.Columns.Add("Total_Pkg");
				exportTb.Columns.Add("CreatedBy");
				exportTb.Columns.Add("CreateDT");
				exportTb.Columns.Add("PayerID");

				strConsignmentNo = strConsignmentNo.Substring(0, strConsignmentNo.Length - 1); 

				try
				{
					//Get DataTable Report to send into Excel
					dsReport = ConsignmentNoteDAL.GetPrintReportLicensePlate(appID, enterpriseID, PayerID, strConsignmentNo);

					if (dsReport != null && dsReport.Tables.Count > 0 && dsReport.Tables[0] != null && dsReport.Tables[0].Rows.Count > 0)
					{
						dtReport = dsReport.Tables[0];

						for (int ii = 0; ii < dtReport.Rows.Count; ii++) 
						{
							DataRow drReport = null;
							drReport = dtReport.Rows[ii];
	
							DataRow dr;
							dr = exportTb.NewRow();

							dr["Consignment_no"] = drReport["CONSIGNMENT_NO"].ToString();
							dr["Service_Type"] = drReport["SERVICE_TYPE"].ToString();
							dr["Dest_Postal_Code"] = drReport["ZIP_CODE"].ToString();
							dr["Total_Pkg"] = Convert.ToInt32(drReport["TOTAL_QTY"].ToString());
							dr["CreatedBy"] = userID;
							//dr["CreateDT"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm");	//incorrect for SWB
							dr["CreateDT"] = DateTime.Now.ToString("MM/dd/yyyy HH:mm");
							dr["PayerID"] = PayerID;
						
							exportTb.Rows.Add(dr);
						}
					}

					//Create Excel 
					ExportData(exportTb);	
				}
				catch (Exception err)
				{
					ErrorMsg.Text = err.Message.ToString();
				}
				finally
				{
					DT.Dispose();
					dsReport.Dispose();
					dtReport.Dispose();
					exportTb.Dispose();
				}
			}
		}

		private void ExportData(DataTable dtSource)
		{
			DataTable dtCons = new DataTable();
			string Res = "";			
 
			try
			{
				dtCons = dtSource;

				if ((dtCons != null) && (dtCons.Columns.Count > 0) && (dtCons.Rows.Count > 0))
				{
					TIESClasses.CreateXls Xls = new TIESClasses.CreateXls();
					// Set type column
					string[] ColumnType = new string[dtCons.Columns.Count];
					ColumnType[1] = Xls.TypeText; 

					Res = Xls.createXlsConsign(dtCons, GetPathExport(), ColumnType);

					if (Res == "0")
					{	
						GetDownloadFile(Res);
				
						// Kill Process after save
						Xls.KillProcesses("EXCEL");
					} 
					else
					{
						ErrorMsg.Text = Res;
					}
				}
			}
			catch(Exception ex)
			{
				ErrorMsg.Text = ex.Message.ToString() ;
			}
			finally
			{
				dtCons.Dispose();
				dtSource.Dispose();
			}
		}

		private string  GetPathExport()
		{
			FileName = PayerID.ToUpper() + "_" + userID.ToUpper() + "_LicensePlate_" + String.Format("{0:yyyyMMdd}", DateTime.Now);
			string mapFile = Server.MapPath("..\\Export") + "\\" + FileName + ".xls";
			return mapFile;
		}

		private void GetDownloadFile(string Res)
		{
			if (Res == "0")
			{
				FileName = PayerID.ToUpper() + "_" + userID.ToUpper() + "_LicensePlate_" + String.Format("{0:yyyyMMdd}", DateTime.Now);
				Response.ContentType="application/ms-excel";
				Response.AddHeader( "content-disposition","attachment; filename=" + FileName + ".xls");
				FileStream fs = new FileStream(GetPathExport(),FileMode.Open);
				long FileSize;
				FileSize = fs.Length;
				byte[] getContent = new byte[(int)FileSize];
				fs.Read(getContent, 0, (int)fs.Length);
				fs.Close();

				Response.BinaryWrite(getContent);
			} 
		}
		//end boon - 2010/10/18 -- 1

#endregion ExporttLicensePlate

		public string Bar128AB(string BarText, int Subset)
		{
			string BarTextOut = "";
			BarText = BarText.Trim();
			long sum = 0;
			long charvalue = 0;
			string startchar = "";
			string thischar = "";
			string checksum = "";
			string barcodeout = "";
			int checksumvalue = 0;
			if (Subset == 0)
			{
				sum = 103;
				startchar = "{";
			}
			else
			{
				sum = 104;
				startchar = "|";
			}
			for (int i = 1; i <= BarText.Length; i++)
			{
				thischar = Convert.ToInt32(BarText[i - 1]).ToString();
				if (Convert.ToInt32(thischar) < 123)
					charvalue = Convert.ToInt32(thischar) - 32;
				else
					charvalue = Convert.ToInt32(thischar) - 70;
				sum = sum + (charvalue * i);

				if (BarText[i - 1].ToString() == " ")
					BarTextOut = BarTextOut + Convert.ToChar(174);
				else
					BarTextOut = BarTextOut + BarText[i - 1].ToString();
			}
			checksumvalue = Convert.ToInt32(sum) % 103;
			if (Convert.ToInt32(checksumvalue) > 90)
				checksum = Convert.ToChar(Convert.ToInt32(checksumvalue) + 70).ToString();
			else if (Convert.ToInt32(checksumvalue) > 0)
				checksum = Convert.ToChar(Convert.ToInt32(checksumvalue) + 32).ToString();
			else
				checksum = Convert.ToChar(174).ToString();
			barcodeout = startchar + BarTextOut + checksum + "~ ";
			return barcodeout;
		}
		
		private void dgResult_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				e.Item.Attributes.Add("onmouseover", "javascript:ShowBar(this);"); 
				e.Item.Attributes.Add("onmouseout", "javascript:HideBar(this);");
			}
		}

		#region Gen Image as Barcode
//			private void PrintData()
//			{
//				DataTable DT = new DataTable();
//				DataRow DR = null;
//				DataSet dsReport = new DataSet();
//				DataTable dtReport = new DataTable();
//				DataSet dsReport1 = new DataSet();
//				DataTable dtReport1 = new DataTable();
//				DataRow drReport1 = null;
//				DataSet dsPage = new DataSet();
//				DataTable dtPage = new DataTable(); 
//				DataSet dsQty = new DataSet();
//				DataTable dtQty = new DataTable();
//				DataRow drQty = null;
//				int i = 0;
//				int j = 0;
//				string ItemSelected = "";
//				string tmpConsignmentNo = "";
//				string tmpCopyPage = "";
//				string ConsignmentNo = "";
//				int jRowsAffected = 0;
//				int TotalQty = 0;
//				string strUrl = "";
//				ErrorMsg.Text = "";
//				
//				try
//				{
//					DT = (DataTable)Session["SEARCH_CONSIGNMENT_PRINT"];
//					if (DT != null && DT.Rows.Count > 0)
//					{
//						while (i < DT.Rows.Count) 
//						{
//							DR = DT.Rows[i];
//							ItemSelected = Request.Form["chkITEM_" + i] + "";
//							if (ItemSelected == "Y")
//							{
//								if (!DR["CONSIGNMENT_NO"].GetType().Equals(System.Type.GetType("System.DBNull")))
//								{
//									tmpConsignmentNo += "'" + DR["CONSIGNMENT_NO"] + "',";
//								}
//							}
//							i++;
//						}
//
//						if (tmpConsignmentNo.Length > 0) 
//						{
//							tmpConsignmentNo = tmpConsignmentNo.Substring(0, tmpConsignmentNo.Length - 1); 
//							//jRowsAffected = ConsignmentNoteDAL.InsertConsignmentNoteDetailReport(appID,  enterpriseID,  PayerID,tmpConsignmentNo);
//							//if (jRowsAffected > 0)
//							//{
//							
//							//Clone DataTable Report to sent into Crystal Report
//							dsReport = ConsignmentNoteDAL.GetPrintReport(appID, enterpriseID, PayerID, tmpConsignmentNo);
//							if (dsReport != null && dsReport.Tables.Count > 0 && dsReport.Tables[0] != null && dsReport.Tables[0].Rows.Count > 0)
//							{
//								dtReport = dsReport.Tables[0];
//								dtReport1 = dtReport.Clone(); 
//								dtReport1.Clear();
//
//								////// add new column //////
//								//running number
//								DataColumn Seq = new DataColumn("seq", typeof(Int32));
//								dtReport1.Columns.Add(Seq); 
//
//								//Page Copy
//								DataColumn Copy = new DataColumn("Copy", typeof(String));
//								dtReport1.Columns.Add(Copy);
//								
//								//TotalQty
//								DataColumn TotalCountQTY = new DataColumn("TotalCountQTY", typeof(String));
//								dtReport1.Columns.Add(TotalCountQTY);
//
//								//ConsignmentNo Barcode
//								DataColumn ConsignmentNoBarcode = new DataColumn("ConsignmentNoBarcode", typeof(String));
//								dtReport1.Columns.Add(ConsignmentNoBarcode);
//	  
//								//ServiceType Barcode
//								DataColumn ServiceTypeBarcode = new DataColumn("ServiceTypeBarcode", typeof(String));
//								dtReport1.Columns.Add(ServiceTypeBarcode);
//								
//								//Recipient ID Barcode
//								DataColumn RecipientCustrefBarcode = new DataColumn("RecipientCustrefBarcode", typeof(String));
//								dtReport1.Columns.Add(RecipientCustrefBarcode);
//								
//								//Total Quantity Barcode
//								DataColumn TotalQTYBarcode = new DataColumn("TotalQTYBarcode", typeof(String));
//								dtReport1.Columns.Add(TotalQTYBarcode);
//								
//								//Get print page copy
//								dsPage = ConsignmentNoteDAL.GetParameterConsignment(appID, enterpriseID, PayerID);
//								if (dsPage != null && dsPage.Tables.Count > 0 && dsPage.Tables[0] != null && dsPage.Tables[0].Rows.Count > 0)
//								{
//									dtPage = dsPage.Tables[0];
//									foreach (DataRow drPage in dtPage.Rows)
//									{
//										tmpCopyPage = "";
//										if ((drPage["ParamName"] + "" == "P1_Shipper") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "1. SHIPPER";}
//										else if ((drPage["ParamName"] + "" == "P2_DataEntry") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "2. DATA ENTRY";}
//										else if ((drPage["ParamName"] + "" == "P3_HCR1") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "3. HCR-1";}
//										else if ((drPage["ParamName"] + "" == "P4_HCR2") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "4. HCR-2";}
//										else if ((drPage["ParamName"] + "" == "P5_Destination") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "5. DESTINATION";}
//										else if ((drPage["ParamName"] + "" == "P6_Consignee") && (drPage["ParamValue"] + "" == "Y")) {tmpCopyPage = "6. CONSIGNEE";}
//
//										//check copy page
//										if (tmpCopyPage != "")
//										{
//											//add data from dtReport to dtReport1
//											foreach (DataRow drReport in dtReport.Rows)
//											{
//
//												ConsignmentNo = "";
//												ConsignmentNo = drReport["Consignment_No"] + "";
//
//												//sum Total Qty
//												dsQty = ConsignmentNoteDAL.SumQtyByConsignment(appID, enterpriseID, PayerID, ConsignmentNo);
//												if (dsQty != null && dsQty.Tables.Count > 0 && dsQty.Tables[0] != null && dsQty.Tables[0].Rows.Count > 0)
//												{
//													drQty = dsQty.Tables[0].Rows[0];
//													if (!drQty["TotalQty"].GetType().Equals(System.Type.GetType("System.DBNull"))) { TotalQty = (int)drQty["TotalQty"]; }
//												}
//
//												//add data
//												drReport1 = dtReport1.NewRow(); 
//												drReport1["Seq"] = j;
//												drReport1["Copy"] = tmpCopyPage;
//												drReport1["TotalCountQTY"] = TotalQty;
//												drReport1["seq_no"] = drReport["seq_no"];
//												drReport1["consignment_No"] = ConsignmentNo;
//												drReport1["PayerID"] = drReport["PayerID"];
//												drReport1["Telephone"] = drReport["Telephone"];
//												drReport1["LastUserID"] = drReport["LastUserID"];
//												drReport1["HCReturn"] = drReport["HCReturn"];
//												drReport1["InvReturn"] = drReport["InvReturn"];
//												drReport1["codamount"] = drReport["codamount"];
//												drReport1["LastUpdateDate"] = drReport["LastUpdateDate"];
//												drReport1["ConsignmentDate"] = drReport["ConsignmentDate"];
//												drReport1["ServiceType"] = drReport["ServiceType"];
//												drReport1["Boxsize"] = drReport["Boxsize"];
//												drReport1["kilo"] = drReport["kilo"];
//												drReport1["Qty"] = drReport["Qty"];
//												drReport1["box_desc"] = drReport["box_desc"];
//												drReport1["charge_rate"] = drReport["charge_rate"];
//												drReport1["address1"] = drReport["address1"];
//												drReport1["address2"] = drReport["address2"];
//												drReport1["zipcode"] = drReport["zipcode"];
//												drReport1["payer_name"] = drReport["payer_name"];
//												drReport1["cust_address1"] = drReport["cust_address1"];
//												drReport1["cust_address2"] = drReport["cust_address2"];
//												drReport1["state_code"] = drReport["state_code"];
//												drReport1["zipcode"] = drReport["zipcode"];
//												drReport1["state_name"] = drReport["state_name"];
//												drReport1["ref_code"] = drReport["ref_code"];
//												drReport1["company_address1"] = drReport["company_address1"];
//												drReport1["company_address2"] = drReport["company_address2"];
//												drReport1["company_zipcode"] = drReport["company_zipcode"];
//												drReport1["company_state_name"] = drReport["company_state_name"];
//												drReport1["sender_Contact_person"] = drReport["sender_Contact_person"];
//												drReport1["Recipion_state_name"] = drReport["Recipion_state_name"];
//												drReport1["sender_telephone"] = drReport["sender_telephone"];
//												drReport1["reference_name"] = drReport["reference_name"];
//												drReport1["DeclareValue"] = drReport["DeclareValue"];
//												drReport1["cust_name"] = drReport["cust_name"];
//												drReport1["sender_zipcode"] = drReport["sender_zipcode"];
//												drReport1["cust_ref"] = drReport["cust_ref"];
//												drReport1["Special_Instruction"] = drReport["Special_Instruction"];
//												drReport1["recipion_contact_person"] = drReport["recipion_contact_person"];
//												
//												
//												//Clear file and gen barcode
//												ClearTagBarcode();	
//
//												//gen ConsignmentNo barcode
//												if (ConsignmentNo != "")
//												{
//													System.Drawing.Image imgConsignmentNo = GenCode128.Code128Rendering.MakeBarcodeImage(ConsignmentNo, int.Parse("2"), true);
//													imgConsignmentNo.Save(Server.MapPath(BarcodeTagPath) + "Bar.tiff");
//													drReport1["ConsignmentNoBarcode"] = GetPhoto("Bar.tiff");
//												}
//
//												//gen ServiceType barcode
//												if (drReport["ServiceType"] + "" != "")
//												{
//													System.Drawing.Image imgServiceType = GenCode128.Code128Rendering.MakeBarcodeImage(drReport["ServiceType"] + "" , int.Parse("1"), true);
//													imgServiceType.Save(Server.MapPath(BarcodeTagPath) + "ServiceType.tiff");
//													drReport1["ServiceTypeBarcode"] = GetPhoto("ServiceType.tiff");
//												}
//												
//												//gen Recipient Zipcode barcode
//												if (drReport["zipcode"] + "" != "")
//												{
//													System.Drawing.Image imgRecipient = GenCode128.Code128Rendering.MakeBarcodeImage(drReport["zipcode"] + "", int.Parse("1"), true);
//													imgRecipient.Save(Server.MapPath(BarcodeTagPath) + "RecipientBar.tiff");
//													drReport1["RecipientCustrefBarcode"] = GetPhoto("RecipientBar.tiff");
//												}
//
//												//gen TotalQuantity barcode
//												if (TotalQty.ToString() != "")
//												{
//													System.Drawing.Image imgTotalQTY = GenCode128.Code128Rendering.MakeBarcodeImage(TotalQty.ToString() , int.Parse("1"), true);
//													imgTotalQTY.Save(Server.MapPath(BarcodeTagPath) + "TotalQTYBar.tiff");
//													drReport1["TotalQTYBarcode"] = GetPhoto("TotalQTYBar.tiff");
//												}
//												
//												/*System.Drawing.Image myimg = Code128Rendering.MakeBarcodeImage(txtConsignmentNo.Text.Trim(), int.Parse("2"), true);
//													byte[] content = ReadBitmap2ByteArray(myimg);*/
//												
//												dtReport1.Rows.Add(drReport1);
//												dtReport1.AcceptChanges();
//											} //end of loop Report
//											j++;
//										}//end check copy page
//									} //end of loop page
//
//									//gen report .rpt
//									dsReport1.Tables.Add(dtReport1);
//									if (dsReport1 != null && dsReport1.Tables.Count > 0)
//									{
//										/*strUrl = "../ReportViewerDataSet.aspx";
//										Session["FORMID"] = "ConsignmentNotePrint";
//										Session["SESSION_DS1"] = dsReport1;
//										ArrayList paramList = new ArrayList();
//										paramList.Add(strUrl);
//										String sScript = Utility.GetScript("openParentWindow.js",paramList);
//										Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);*/
//										
//										DataGrid1.DataSource = dsReport1.Tables[0];
//										DataGrid1.DataBind();  
//									}
//								}
//								else
//								{
//									ErrorMsg.Text = "Please set print ConsignmentNote.";
//								}
//
//								
//							}
//							//}
//						}
//						else
//						{
//							ErrorMsg.Text = "Please select ConsignmentNo.";
//						}
//					}
//				}
//				catch(ApplicationException appException)
//				{
//					strMsg = appException.Message.ToString() ;
//					ErrorMsg.Text = strMsg;
//				}
//				finally 
//				{
//					dsPage.Dispose(); 
//				}
//			}
//
//			private void ClearTagBarcode()
//			{
//
//				try
//				{
//					if (BarcodeTagPath != "")
//					{
//						//clear Bar.tiff
//						if (File.Exists(Server.MapPath(BarcodeTagPath) + "Bar.tiff")) 
//						{
//							File.Delete(Server.MapPath(BarcodeTagPath) + "Bar.tiff");
//						}
//
//						//clear ServiceType.tiff
//						if (File.Exists(Server.MapPath(BarcodeTagPath) + "ServiceType.tiff")) 
//						{
//							File.Delete(Server.MapPath(BarcodeTagPath) + "ServiceType.tiff");
//						}
//
//						//clear RecipientBar.tiff
//						if (File.Exists(Server.MapPath(BarcodeTagPath) + "RecipientBar.tiff")) 
//						{
//							File.Delete(Server.MapPath(BarcodeTagPath) + "RecipientBar.tiff");
//						}
//
//						//clear TotalQTYBar.tiff
//						if (File.Exists(Server.MapPath(BarcodeTagPath) + "TotalQTYBar.tiff")) 
//						{
//							File.Delete(Server.MapPath(BarcodeTagPath) + "TotalQTYBar.tiff");
//						}
//					}
//				}
//				catch(ApplicationException appException)
//				{
//					strMsg = appException.Message.ToString() ;
//					ErrorMsg.Text = strMsg;
//				}
//			}
//
//
//			private byte[] GetPhoto(String FullFileName)
//			{
//				byte[] photo = null;
//				
//				try
//				{
//					if (BarcodeTagPath != "" && FullFileName != "")
//					{
//						FileStream fs = new FileStream(Server.MapPath(BarcodeTagPath) + FullFileName, FileMode.Open, FileAccess.Read);
//						BinaryReader br = new BinaryReader(fs);
//
//						photo = br.ReadBytes((int)fs.Length);
//						br.Close();
//						fs.Close();
//					}
//				}
//				catch(ApplicationException appException)
//				{
//					strMsg = appException.Message.ToString() ;
//					ErrorMsg.Text = strMsg;
//				}
//				return photo;
//			}
		#endregion

	}
}
