<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="BoxSize.aspx.cs" AutoEventWireup="false" Inherits="com.ties.BoxSize" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Box Size</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../css/Styles.css" type="text/css" rel="Stylesheet">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript" type="text/javascript">
			function DoAction(serveraction){
				var confirmed = true;
				var isSubmitted = true;
				var URL = "";
				var ErrMsg = "";
				switch (serveraction){
					case "CLEAR":
						isSubmitted = true;
						break;
					case "SEARCH":
						isSubmitted = true;
						break;
					case "SAVE":
						isSubmitted = true;
						break;
					case "INSERT":
						isSubmitted = true;
						break;
					case "DELETE":
						confirmed = confirm("��سҡ� OK �����׹�ѹ���ź������");
						break;
				}
				if (URL != ""){
					window.location.href = URL; 
				} else {
					if (isSubmitted && confirmed){
						if (ErrMsg != ""){
							alert(ErrMsg);
						} else {
							document.forms[0].elements("ServerAction").value = serveraction;
							document.forms[0].submit(); 
						}  
					}
				}
			}
			 		 			
			var old_bgcolor;
			var old_class;
			function ShowBar(src) {
				if (!src.contains(event.fromElement)) {
					src.style.cursor = 'hand';
					old_class = src.className;
					src.className = 'tabletotal';
				}
			}
			function HideBar(src) {
				if (!src.contains(event.toElement)) {
					src.style.cursor = 'default';
					src.className = old_class;
				}
			}
		</script>
	</HEAD>
	<body onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();"
		onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="ViewLayout">
		<form id="frmBoxSize" method="post" runat="server">
			<INPUT id="ServerAction" type="hidden" name="ServerAction"> <INPUT id="ItemIndex" type="hidden" name="ItemIndex">
			<input id="BoxSizeID" type="hidden" value="<%=BoxSizeID %>" name="BoxSizeID">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td>
						<table class="TextBody" cellSpacing="0" cellPadding="3" width="100%" border="0">
							<tr>
								<td colspan="2"><asp:label id="lblMainTitle" runat="server" CssClass="mainTitleSize" Width="477px">Box Size</asp:label></td>
							</tr>
							<tr>
								<td align="right" width="35%">
									<a onclick="javascript:DoAction('CLEAR');"><input class="queryButton" id="btnQuery" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Query" name="btnQuery"></a> <a onclick="javascript:DoAction('SEARCH');">
										<input class="queryButton" id="btnExcute" style="WIDTH: 104px; HEIGHT: 20px" type="button"
											value="Execute Query" name="btnExcute"></a> <A onclick="javascript:DoAction('INSERT');">
										<input class="queryButton" id="btnInsert" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Insert" name="btnAdd"></A>
								</td>
								<td align="left" width="55%" vAlign="middle">
									<asp:Panel id="PanelButton" runat="server">
										<A onclick="javascript:DoAction('SAVE');"><INPUT class="queryButton" id="btnSave" style="WIDTH: 60px; HEIGHT: 20px" type="button"
												value="Save" name="btnSave"></A> <A onclick="javascript:DoAction('DELETE');">
											<INPUT class="queryButton" id="btnDelete" style="WIDTH: 60px; HEIGHT: 20px" type="button"
												value="Delete" name="btnDelete"></A>
									</asp:Panel>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<asp:label id="ErrorMsg" runat="server" Width="643px" CssClass="errorMsgColor" Height="3px"></asp:label><BR>
									<asp:label id="ErrorMsg1" runat="server" Width="643px" CssClass="errorMsgColor" Height="3px"></asp:label>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<asp:Panel id="PanelSearch" runat="server">
										<TABLE class="TextBody" cellSpacing="0" cellPadding="3" width="100%" border="0">
											<TR class="gridHeading">
												<TD><STRONG><FONT size="3">Search BoxSize</FONT></STRONG></TD>
											</TR>
											<TR>
												<TD align="center">BoxSize&nbsp;:&nbsp;
													<asp:DropDownList id="lstBoxSize" runat="server" Width="120px"></asp:DropDownList></TD>
											</TR>
										</TABLE>
									</asp:Panel>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<asp:Panel id="PanelData" runat="server">
										<TABLE class="TextBody" cellSpacing="0" cellPadding="3" width="100%" border="0">
											<TR class="tablepagingitem" style="HEIGHT: 5px">
												<TD style="WIDTH: 250px" align="right">&nbsp;</TD>
												<TD style="WIDTH: 150px" align="left">&nbsp;</TD>
												<TD style="WIDTH: 150px" align="right">&nbsp;</TD>
												<TD style="WIDTH: 450px" align="left">&nbsp;</TD>
											</TR>
											<TR class="gridHeading">
												<TD colSpan="4"><STRONG><FONT size="3">BoxSize Detail</FONT></STRONG></TD>
											</TR>
											<TR>
												<TD align="right">BoxSize ID&nbsp;:<FONT color="#ff0066">*</FONT></TD>
												<TD align="left">
													<asp:TextBox id="txtBoxSizeID" runat="server" MaxLength="10"></asp:TextBox></TD>
												<TD align="right"></TD>
												<TD align="left"></TD>
											</TR>
											<TR>
												<TD align="right">Width&nbsp;:<FONT color="#ff0066">*</FONT></TD>
												<TD align="left">
													<asp:TextBox id="txtWidth" runat="server" MaxLength="10"></asp:TextBox></TD>
												<TD align="right">Length&nbsp;:<FONT color="#ff0066">*</FONT></TD>
												<TD align="left">
													<asp:TextBox id="txtLength" runat="server" MaxLength="10"></asp:TextBox></TD>
											</TR>
											<TR>
												<TD align="right">Height&nbsp;:<FONT color="#ff0066">*</FONT></TD>
												<TD align="left">
													<asp:TextBox id="txtHeight" runat="server" MaxLength="10"></asp:TextBox></TD>
												<TD align="right">Max Weight&nbsp;:&nbsp;</TD>
												<TD align="left">
													<asp:TextBox id="txtMaxWeight" runat="server" MaxLength="10"></asp:TextBox></TD>
											</TR>
											<TR>
												<TD colSpan="4"></TD>
											</TR>
											<TR>
												<TD colSpan="4"></TD>
											</TR>
											<TR>
												<TD align="center" colSpan="4">
													<asp:datagrid id="dgChargeRate" runat="server" Width="500px" Visible="False" AutoGenerateColumns="False"
														ShowFooter="True" DataKeyField="RATEID" AlternatingItemStyle-CssClass="gridField" ItemStyle-CssClass="gridField"
														HeaderStyle-CssClass="gridHeading" CellPadding="0" CellSpacing="1" BorderWidth="0px" FooterStyle-CssClass="gridHeading"
														PageSize="100">
														<FooterStyle CssClass="gridHeading"></FooterStyle>
														<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
														<ItemStyle CssClass="gridField"></ItemStyle>
														<HeaderStyle CssClass="gridHeading"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn>
																<HeaderStyle Width="80px"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<asp:LinkButton id="btnEditItem" Runat="server" Text="<img src='../images/butt-edit.gif' alt='edit' border=0>"
																		CommandName="EDIT_ITEM" />
																	<asp:LinkButton id="btnDeleteItem" Runat="server" Text="<img src='../images/butt-delete.gif' alt='delete' border=0>"
																		CommandName="DELETE_ITEM" />
																</ItemTemplate>
																<FooterStyle HorizontalAlign="Center"></FooterStyle>
																<FooterTemplate>
																	<asp:LinkButton id="btnAddItem" Runat="server" Text="<img src='../images/butt-add.gif' alt='add' border=0>"
																		CommandName="ADD_ITEM" />
																</FooterTemplate>
																<EditItemTemplate>
																	<asp:LinkButton id="btnSaveItem" Runat="server" Text="<img src='../images/butt-update.gif' alt='save' border=0>"
																		CommandName="SAVE_ITEM" />
																	<asp:LinkButton id="btnCancelItem" Runat="server" Text="<img src='../images/butt-red.gif' alt='cancel' border=0>"
																		CommandName="CANCEL_ITEM" />
																</EditItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn Visible="False" DataField="RATEID" HeaderText="Consignment No"></asp:BoundColumn>
															<asp:TemplateColumn HeaderText="No.">
																<HeaderStyle Width="25px"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<%# Container.ItemIndex +1 %>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Service Type">
																<HeaderStyle Width="60px"></HeaderStyle>
																<ItemTemplate>
																	<%# DataBinder.Eval(Container.DataItem,"SERVICE_CODE") %>
																</ItemTemplate>
																<FooterTemplate>
																	<asp:DropDownList ID="lstServiceAdd" Runat="server"></asp:DropDownList>
																</FooterTemplate>
																<EditItemTemplate>
																	<asp:DropDownList ID="lstService" Runat="server"></asp:DropDownList>
																</EditItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Description">
																<HeaderStyle Width="100px"></HeaderStyle>
																<ItemTemplate>
																	<%# DataBinder.Eval(Container.DataItem,"SERVICE_DESCRIPTION") %>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Charge Rate (Baht)">
																<HeaderStyle Width="80px"></HeaderStyle>
																<ItemStyle HorizontalAlign="Right"></ItemStyle>
																<ItemTemplate>
																	<%# DataBinder.Eval(Container.DataItem,"CHARGE_RATE", "{0:#,##0.00}") %>
																</ItemTemplate>
																<FooterTemplate>
																	<input type="text" id="txtRateAdd" name="txtRateAdd" size="10">
																</FooterTemplate>
																<EditItemTemplate>
																	<%# DataBinder.Eval(Container.DataItem,"CHARGE_RATE", "<input type=text id=txtRate name=txtRate size=10 value='{0}'>") %>
																</EditItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
											<TR>
												<TD align="center" colSpan="4">
													<asp:label id="lblNotFound" runat="server" Width="643px" CssClass="errorMsgColor" Height="3px"></asp:label></TD>
											</TR>
										</TABLE>
									</asp:Panel>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
