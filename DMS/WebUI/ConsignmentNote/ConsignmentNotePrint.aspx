<%@ Page language="c#" Codebehind="ConsignmentNotePrint.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ConsignmentNotePrint" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ConsignmentNote Print</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/Styles.css" type="text/css" rel="Stylesheet">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript" type="text/javascript">
			function DoAction(serveraction) {
				frmPrintConsignment.elements("ServerAction").value = serveraction;
				frmPrintConsignment.submit();
			}
			
			function CheckAll(){
				var i, chkAll, chkTmp, max;
				max = <%=max%>;
			    
				chkAll = document.frmPrintConsignment.chkAllItem.checked;
				for (i=0; i<max; i++){
					chkTmp = document.forms[0].elements("chkITEM_" + i);
					if (chkTmp != null){
						chkTmp.checked = chkAll;
					} 
				}
			}
			
				
			var old_bgcolor;
			var old_class;
			function ShowBar(src) {
				if (!src.contains(event.fromElement)) {
					src.style.cursor = 'hand';
					old_class = src.className;
					src.className = 'tabletotal';
				}
			}
			function HideBar(src) {
				if (!src.contains(event.toElement)) {
					src.style.cursor = 'default';
					src.className = old_class;
				}
			}
		</script>
	</HEAD>
	<body onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();"
		onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="ViewLayout">
		<form id="frmPrintConsignment" method="post" runat="server">
			<input id="ServerAction" type="hidden" name="ServerAction">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td>
						<table class="TextBody" cellSpacing="0" cellPadding="3" border="0">
							<tr>
								<td colSpan="4"><asp:label id="lblMainTitle" runat="server" Width="477px" CssClass="mainTitleSize">Print Consignment Note</asp:label></td>
							</tr>
							<tr class="tablepagingitem" style="HEIGHT: 5px">
								<td style="WIDTH: 250px" align="right">&nbsp;</td>
								<td style="WIDTH: 250px" align="left">&nbsp;</td>
								<td style="WIDTH: 200px" align="right">&nbsp;</td>
								<td style="WIDTH: 350px" align="left">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" colSpan="4">
									<a onclick="javascript:DoAction('CLEAR');"><input class="queryButton" id="btnQuery" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Query" name="btnQuery"></a><a onclick="javascript:DoAction('SEARCH');">
										<input class="queryButton" id="btnExcute" style="WIDTH: 104px; HEIGHT: 20px" type="button"
											value="Execute Query" name="btnExcute"></a> 
									<!-- a onclick="javascript:DoAction('PRINT');">
										<input class="queryButton" id="btnPrint" style="WIDTH: 140px; HEIGHT: 20px" type="button"
											value="Print Consignment" name="btnPrint"></a -->
									<a onclick="javascript:DoAction('PRINTPDF');"><input class="queryButton" id="btnPrintPdf" style="WIDTH: 140px; HEIGHT: 20px" type="button"
											value="Print Consignment" name="btnPrintPdf"></a><a onclick="javascript:DoAction('PRINTLP');">
										<input class="queryButton" id="btnPrintLP" style="WIDTH: 140px; HEIGHT: 20px" type="button"
											value="Print License Plate" name="btnPrintLP"></a><a onclick="javascript:DoAction('EXPORTLP');">
										<input class="queryButton" id="btnExportLP" style="WIDTH: 150px; HEIGHT: 20px" type="button"
											value="Export License Plate" name="btnExportLP"></a>
								</td>
							</tr>
							<tr class="tablepagingitem" style="HEIGHT: 5px">
								<td colSpan="4">&nbsp;</td>
							</tr>
							<tr>
								<td colSpan="4"><asp:label id="ErrorMsg" runat="server" Width="643px" CssClass="errorMsgColor" Height="3px"></asp:label></td>
							</tr>
							<tr>
								<td colSpan="4">&nbsp;</td>
							</tr>
							<tr>
								<td align="right">Consignment No&nbsp;: &nbsp;</td>
								<td align="left"><asp:textbox id="txtConsignmentNo" runat="server" Width="200px"></asp:textbox></td>
								<td align="right">Customer Code&nbsp;: &nbsp;</td>
								<td align="left"><asp:textbox id="txtCustomerCode" runat="server" Width="200px"></asp:textbox></td>
							</tr>
							<tr>
								<td colSpan="4">&nbsp;</td>
							</tr>
							<tr>
								<td align="center" colSpan="4"><asp:datagrid id="dgResult" runat="server" PageSize="1000" AlternatingItemStyle-CssClass="gridField"
										ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridHeading" HorizontalAlign="Center" CellPadding="2" CellSpacing="1"
										BorderWidth="0px" AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="25px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
												<HeaderTemplate>
													<input type="checkbox" id="chkAllItem" name="chkAllItem" onclick="javascript:CheckAll();">
												</HeaderTemplate>
												<ItemTemplate>
													<%#"<input type=checkbox id=chkITEM_" + Container.ItemIndex + " name=chkITEM_" + Container.ItemIndex + " value='Y'>"%>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="CONSIGNMENT_NO" HeaderText="Consignment No">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="CUSTOMERCODE" HeaderText="Customer Code">
												<HeaderStyle Width="60px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="REFERENCE_NAME" HeaderText="Recipient Name">
												<HeaderStyle Width="200px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="ADDRESS" HeaderText="Recipient Address">
												<HeaderStyle Width="200px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="STATE" HeaderText="State">
												<HeaderStyle Width="100px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="ZIPCODE" HeaderText="Zipcode">
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="TELEPHONE" HeaderText="Telephone">
												<HeaderStyle Width="50px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="SENDER_NAME" HeaderText="Sender Name">
												<HeaderStyle Width="100px"></HeaderStyle>
												<ItemStyle HorizontalAlign="Left"></ItemStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle NextPageText="Next" Font-Size="Smaller" PrevPageText="Previous" HorizontalAlign="Right"></PagerStyle>
									</asp:datagrid><asp:label id="lblNotFound" runat="server" Visible="False" ForeColor="Red" Font-Bold="True">Data not found.</asp:label></td>
							</tr>
						</table>
						<asp:datagrid id="DataGrid1" runat="server"></asp:datagrid></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
