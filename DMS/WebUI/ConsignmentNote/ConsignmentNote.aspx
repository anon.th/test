<%@ Page language="c#" Codebehind="ConsignmentNote.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ConsignmentNote" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ConsignmentNote</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/Styles.css" type="text/css" rel="Stylesheet">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<!--script language="javascript" src="../Scripts/settingScrollPosition.js"></script-->
		<script language="javascript" type="text/javascript">
		function openBrWindow(theURL,winName,features) {
			window.open(theURL,winName,features);
		}
		
		function DoAction(serveraction){
			var confirmed = true;
            var isSubmitted = true;
            var URL = "";
            var ErrMsg = "";
            switch (serveraction){
                case "SAVE":
                   /* if (document.forms[0].elements("lstExpenseName").value == '')ErrMsg = "* Expense Name!\n";
                    if (isDate(document.forms[0].elements("txtPaymentDate").value) == false)ErrMsg += "* ��Ǩ�ͺ Payment Date!\n";
                    if (IsNumeric(document.forms[0].elements("txtAmount").value) == false)ErrMsg += "* ��Ǩ�ͺ Amount!\n";
                    
                    if (ErrMsg != ""){
                        confirmed = false;
                        alert("��س��кآ�����\n"+ErrMsg);
                    } else {
                        confirmed = confirm("��سҡ� OK �����׹�ѹ��úѹ�֡������");
                    }*/
                    //confirmed = confirm("��سҡ� OK �����׹�ѹ��úѹ�֡������");
                     isSubmitted = true;
                    break;
                case "INSERT":
                    isSubmitted = true;
                    URL = "ConsignmentNote.aspx?ServerAction=INSERT";
                    break;
                case "DELETE":
                    confirmed = confirm("��سҡ� OK �����׹�ѹ���ź������");
                    break;
            }
            if (URL != ""){
                window.location.href = URL; 
            } else {
                if (isSubmitted && confirmed){
                    if (ErrMsg != ""){
                        alert(ErrMsg);
                    } else {
                        document.forms[0].elements("ServerAction").value = serveraction;
                        document.forms[0].submit(); 
                    }  
                }
            }
		 }
		 		 
		function FindCompany(cid,cname,caddress,ctel,czipcode,ccontactperson){
			//alert("CODE : " + cid, + " NAME : " + cname, + " ADDRESS : " + caddress, + " TEL : " + ctel, + "ZONE CODE : " + czone);
			//alert(czipcode);
			document.forms[0].elements("CompanyCode").value = cid;
			document.forms[0].elements("txtCompanyName").value = cname;
			document.forms[0].elements("txtCompanyAddress").value = caddress;
			document.forms[0].elements("txtCompanyTel").value = ctel;
			document.forms[0].elements("Zipcode").value = czipcode;
			document.forms[0].elements("txtRecipientName").value = ccontactperson;
			document.forms[0].elements("ServerAction").value = "FINDCOMPANY";
			document.forms[0].submit(); 
		}
		
		var old_bgcolor;
		var old_class;
        function ShowBar(src) {
			if (!src.contains(event.fromElement)) {
				src.style.cursor = 'hand';
				old_class = src.className;
				src.className = 'tabletotal';
			}
		}
		function HideBar(src) {
			if (!src.contains(event.toElement)) {
				src.style.cursor = 'default';
				src.className = old_class;
			}
		}
		
		</script>
	</HEAD>
	<body onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();"
		onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="ViewLayout">
		<FORM id="frmConsignment" method="post" runat="server">
			<INPUT id="ServerAction" type="hidden" name="ServerAction"> <INPUT id="ItemIndex" type="hidden" name="ItemIndex">
			<input 
id=ConsignmentNo type=hidden value="<%=ConsignmentNo %>" name=ConsignmentNo> <input id=CompanyCode type=hidden value="<%=CompanyCode%>" 
name=CompanyCode> <input id=Zipcode type=hidden 
value="<%=Zipcode%>" name=Zipcode>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td><asp:label id="lblMainTitle" runat="server" CssClass="mainTitleSize">Consignment Note</asp:label></td>
				</tr>
				<tr>
					<td>
						<table class="TextBody" cellSpacing="0" cellPadding="2" width="100%" border="0">
							<TBODY>
								<TR class="tablepagingitem">
									<TD align="left" colSpan="3"><A onclick="javascript:DoAction('INSERT','');"><input class="queryButton" id="btnInsert" style="WIDTH: 60px; HEIGHT: 20px" type="button"
												value="Insert" name="btnAdd"></A> <a onclick="javascript:DoAction('SAVE','');">
											<input class="queryButton" id="btnSave" style="WIDTH: 60px; HEIGHT: 20px" type="button"
												value="Save" name="btnSave"></a> <a onclick="javascript:DoAction('DELETE','');">
											<input class="queryButton" id="btnDelete" style="WIDTH: 60px; HEIGHT: 20px" type="button"
												value="Delete" name="btnDelete"></a> <!--a onclick="javascript:DoAction('BACK','');">
										<input class="queryButton" id="btnBack" style="WIDTH: 60px; HEIGHT: 20px" type="button"
											value="Back" name="btnBack"></a--></TD>
								</TR>
								<tr>
									<td colSpan="4"><asp:label id="ErrorMsg" runat="server" CssClass="errorMsgColor" Height="3px" Width="643px"></asp:label><br>
										<asp:label id="ErrorMsg1" runat="server" CssClass="errorMsgColor" Height="3px" Width="643px"></asp:label></td>
								</tr>
								<tr>
									<td align="right">Consignment #&nbsp;:&nbsp;</td>
									<td align="left" colSpan="4"><asp:label id="lblConsignmentNo" runat="server" Font-Bold="True"></asp:label><FONT face="Tahoma">&nbsp;&nbsp;
										</FONT>
										<asp:label id="lblCompanyCode" runat="server" Font-Bold="True"></asp:label></td>
								</tr>
								<tr>
									<td align="right">Sender&nbsp;Name&nbsp;:<FONT color="#ff0066">*</FONT>&nbsp;</td>
									<td align="left"><asp:dropdownlist id="ddlSender" runat="server" Width="250px" AutoPostBack="True"></asp:dropdownlist></td>
									<td align="right">Sender Telephone&nbsp;:&nbsp;</td>
									<td align="left"><asp:textbox id="txtSenderTel" runat="server" CssClass="txtReadOnly" Width="160px" Enabled="False"></asp:textbox></td>
								</tr>
								<tr>
									<td align="right">Contact Person&nbsp;:&nbsp;</td>
									<td align="left"><asp:textbox id="txtContactPerson" runat="server" CssClass="txtReadOnly" Width="250px" Enabled="False"></asp:textbox></td>
									<td align="right">&nbsp;</td>
									<td align="left">&nbsp;</td>
								</tr>
								<tr>
									<td align="right">Address&nbsp;:&nbsp;</td>
									<td align="left"><asp:textbox id="txtAddr" runat="server" CssClass="txtReadOnly" Width="450px" Enabled="False"></asp:textbox></td>
									<td align="right">Postal Code/State&nbsp;:&nbsp;</td>
									<td align="left"><asp:textbox id="txtPostalCode" runat="server" CssClass="txtReadOnly" Width="80px" Enabled="False"></asp:textbox><asp:textbox id="txtState" runat="server" CssClass="txtReadOnly" Width="160px" Enabled="False"></asp:textbox></td>
								</tr>
								<tr class="gridHeading">
									<td colSpan="4"><STRONG><FONT size="2">Recipient Detail</FONT></STRONG></td>
								</tr>
								<tr>
									<td align="right">Company Name&nbsp;:&nbsp;</td>
									<td align="left"><asp:textbox id="txtCompanyName" runat="server" CssClass="txtReadOnly" Width="250px" ReadOnly="True"></asp:textbox><FONT face="Tahoma">&nbsp;<A onclick="openBrWindow('FindCompany.aspx','','scrollbars=yes,resizable=yes,width=950,height=600')"
												href="#"><IMG id="imgSearch" style="BORDER-BOTTOM-STYLE: none; BORDER-RIGHT-STYLE: none; MARGIN: 0px; BORDER-TOP-STYLE: none; BORDER-LEFT-STYLE: none"
													height="22" alt="search" src="../images/butt-search.gif" width="22" runat="server"></A></FONT>
									</td>
									<td align="right">Telephone No&nbsp;:<FONT color="#ff0066">*</FONT></td>
									<td align="left"><asp:textbox id="txtCompanyTel" runat="server" Width="160px" MaxLength="50" BorderStyle="Groove"></asp:textbox></td>
								</tr>
								<tr>
									<td vAlign="top" align="right">Address&nbsp;:&nbsp;</td>
									<td align="left"><asp:textbox id="txtCompanyAddress" runat="server" CssClass="txtReadOnly" Width="450px" ReadOnly="True"></asp:textbox></td>
									<td align="right">Cust. Ref. #&nbsp;:&nbsp;</td>
									<td align="left" colSpan="3"><asp:textbox id="txtCustomerRef" runat="server" Width="200px" MaxLength="30"></asp:textbox></td>
								</tr>
					&nbsp; 
					<!--<tr -->
					<!--<td align="right">Customer Zone&nbsp;:&nbsp;</td>
									<td align="left"><asp:textbox id="txtCustomerZone" runat="server" CssClass="txtReadOnly" Width="72px" ReadOnly="True"></asp:textbox></td>--></tr>
				<!-- tr -->
				<TR>
					<td align="right">Recipient Name&nbsp;:<FONT color="#ff0066">*</FONT></td>
					<td align="left"><asp:textbox id="txtRecipientName" runat="server" Width="250px" MaxLength="100"></asp:textbox></td>
					<td align="right">&nbsp;</td>
					<td align="left"><FONT face="Tahoma"></FONT></td>
				</TR>
				<tr>
					<td align="right">Service Type&nbsp;:<FONT color="#ff0066">*</FONT></td>
					<td align="left"><asp:dropdownlist id="lstServiceType" runat="server" Width="80px"></asp:dropdownlist></td>
					<td align="right">Hard Copy&nbsp;:&nbsp;</td>
					<td align="left"><asp:checkbox id="chkHC" runat="server" Text="HCR"></asp:checkbox>&nbsp;
						<asp:checkbox id="chkInvoice" runat="server" Text="INVR"></asp:checkbox></td>
				</tr>
				<tr>
					<td align="right">C.O.D. Amount&nbsp;:&nbsp;</td>
					<td align="left"><cc1:mstextbox id="txtCOD" tabIndex="0" runat="server" Height="22px" Width="120px" AutoPostBack="True"
							MaxLength="11" NumberScale="2" TextMaskType="msNumeric" NumberMaxValue="10000000" NumberPrecision="11"></cc1:mstextbox></td>
					<td align="right">Declared Value THB&nbsp;:&nbsp;</td>
					<td align="left"><cc1:mstextbox id="txtDeclared" tabIndex="0" runat="server" Height="22px" Width="120px" AutoPostBack="True"
							MaxLength="11" NumberScale="2" TextMaskType="msNumeric" NumberMaxValue="1000000" NumberPrecision="11"></cc1:mstextbox></td>
					</TD></tr>
				<tr>
					<td vAlign="top" align="right">Special Delivery&nbsp;&nbsp;&nbsp;<BR>
						Instructions&nbsp;:&nbsp;</td>
					<td align="left" colSpan="3"><asp:textbox id="txtInstruction" runat="server" Width="822px" MaxLength="200"></asp:textbox></td>
				</tr>
				<!-- <tr class="tablepagingitem" style="HEIGHT: 3px">
					<td colSpan="4">&nbsp;</td> --> 
				</TR>
				<tr class="gridHeading">
					<td colSpan="4"><STRONG><FONT size="2">Package Detail</FONT></STRONG></td>
				</tr>
				<tr>
					<td colSpan="4"><asp:datagrid id="dgPackage" runat="server" Width="900px" AutoGenerateColumns="False" ShowFooter="True"
							DataKeyField="SEQ_NO" AlternatingItemStyle-CssClass="gridField" ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridHeading"
							CellPadding="0" CellSpacing="1" BorderWidth="0px" FooterStyle-CssClass="gridHeading">
							<FooterStyle CssClass="gridHeading"></FooterStyle>
							<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
							<ItemStyle CssClass="gridField"></ItemStyle>
							<HeaderStyle CssClass="gridHeading"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle Width="11%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:LinkButton id="btnEditItem" Runat="server" Text="<img src='../images/butt-edit.gif' alt='edit' border=0>"
											CommandName="EDIT_ITEM" />
										<asp:LinkButton id="btnDeleteItem" Runat="server" Text="<img src='../images/butt-delete.gif' alt='delete' border=0>"
											CommandName="DELETE_ITEM" />
									</ItemTemplate>
									<FooterStyle HorizontalAlign="Center"></FooterStyle>
									<FooterTemplate>
										<asp:LinkButton id="btnAddItem" Runat="server" Text="<img src='../images/butt-add.gif' alt='add' border=0>"
											CommandName="ADD_ITEM" />
									</FooterTemplate>
									<EditItemTemplate>
										<asp:LinkButton id="btnSaveItem" Runat="server" Text="<img src='../images/butt-update.gif' alt='save' border=0>"
											CommandName="SAVE_ITEM" />
										<asp:LinkButton id="btnCancelItem" Runat="server" Text="<img src='../images/butt-red.gif' alt='cancel' border=0>"
											CommandName="CANCEL_ITEM" />
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="No.">
									<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<%# DataBinder.Eval(Container.DataItem,"SEQ_NO")%>
									</ItemTemplate>
									<FooterStyle HorizontalAlign="Center"></FooterStyle>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Box Size">
									<HeaderStyle HorizontalAlign="Center" Width="13%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
									<ItemTemplate>
										<%# DataBinder.Eval(Container.DataItem,"BOX_SIZE") %>
									</ItemTemplate>
									<FooterTemplate>
										<asp:DropDownList id="lstBoxSizeAdd" Runat="server"></asp:DropDownList>
									</FooterTemplate>
									<EditItemTemplate>
										<asp:DropDownList id="lstBoxSize" Runat="server"></asp:DropDownList>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Description">
									<HeaderStyle HorizontalAlign="Center" Width="11%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
									<ItemTemplate>
										<%# DataBinder.Eval(Container.DataItem,"BOX_DESC") %>
									</ItemTemplate>
									<FooterStyle HorizontalAlign="Left"></FooterStyle>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Act. Wt. (kg.)">
									<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
									<ItemTemplate>
										<%# DataBinder.Eval(Container.DataItem,"KILO", "{0:#,##0.00}") %>
									</ItemTemplate>
									<FooterStyle HorizontalAlign="Right"></FooterStyle>
									<FooterTemplate>
										<input type="text" id="txtActualWeightAdd" name="txtActualWeightAdd" size="5" style="TEXT-ALIGN: right"
											maxlength="6">
									</FooterTemplate>
									<EditItemTemplate>
										<%# DataBinder.Eval(Container.DataItem,"KILO", "<input type=text id=txtActualWeight name=txtActualWeight size=5 style='TEXT-ALIGN: right' value='{0}' maxlength=6>") %>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Qty.">
									<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
									<ItemTemplate>
										<%# DataBinder.Eval(Container.DataItem,"QTY", "{0:#,##0}") %>
									</ItemTemplate>
									<FooterStyle HorizontalAlign="Right"></FooterStyle>
									<FooterTemplate>
										<input type="text" id="txtQuantityAdd" name="txtQuantityAdd" size="5" style="TEXT-ALIGN: right"
											maxlength="3">
									</FooterTemplate>
									<EditItemTemplate>
										<%# DataBinder.Eval(Container.DataItem,"QTY", "<input type=text id=txtQuantity name=txtQuantity size=5 style='TEXT-ALIGN: right' value='{0}' maxlength=3>") %>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Total (kg.)">
									<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
									<ItemTemplate>
										<%# DataBinder.Eval(Container.DataItem,"TOTAL_KG", "{0:#,##0.00}") %>
									</ItemTemplate>
									<FooterStyle HorizontalAlign="Right"></FooterStyle>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Width (cm.)">
									<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
									<ItemTemplate>
										<%# DataBinder.Eval(Container.DataItem,"WIDTH", "{0:#,##0.00}") %>
									</ItemTemplate>
									<FooterStyle HorizontalAlign="Right"></FooterStyle>
									<FooterTemplate>
										<input type="text" id="txtWidthAdd" name="txtWidthAdd" size="5" style="TEXT-ALIGN: right"
											maxlength="3">
									</FooterTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Length (cm.)">
									<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
									<ItemTemplate>
										<%# DataBinder.Eval(Container.DataItem,"LENGTH", "{0:#,##0.00}") %>
									</ItemTemplate>
									<FooterStyle HorizontalAlign="Right"></FooterStyle>
									<FooterTemplate>
										<input type="text" id="txtLengthAdd" name="txtLengthAdd" size="5" style="TEXT-ALIGN: right"
											maxlength="3">
									</FooterTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Height (cm.)">
									<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
									<ItemTemplate>
										<%# DataBinder.Eval(Container.DataItem,"HEIGTH", "{0:#,##0.00}") %>
									</ItemTemplate>
									<FooterStyle HorizontalAlign="Right"></FooterStyle>
									<FooterTemplate>
										<INPUT id="txtHeightAdd" style="TEXT-ALIGN: right" type="text" size="5" name="txtHeightAdd"
											maxlength="3">
									</FooterTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td colSpan="4">&nbsp;</td>
				</tr>
			</table>
			</TD></TR></TBODY></TABLE></FORM>
	</body>
</HTML>
