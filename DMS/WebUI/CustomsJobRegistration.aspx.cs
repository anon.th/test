using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.applicationpages;
using TIESClasses;
using TIESDAL;
using com.ties;
using com.ties.DAL;
using com.ties.classes;
using com.common.util;
using System.Text;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CustomsJobRegistration.
	/// </summary>
	public class CustomsJobRegistration : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label Label65;
		protected System.Web.UI.WebControls.Button Button3;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.Label Label38;
		protected System.Web.UI.WebControls.Label Label39;
		protected System.Web.UI.WebControls.Label Label40;
		protected System.Web.UI.WebControls.Label Label41;
		protected System.Web.UI.WebControls.Label Label43;
		protected System.Web.UI.WebControls.TextBox txtModeOfTransport;
		protected System.Web.UI.WebControls.Label Label42;
		protected System.Web.UI.WebControls.Label Label44;
		protected System.Web.UI.WebControls.Label Label57;
		protected System.Web.UI.WebControls.Label Label58;
		protected System.Web.UI.WebControls.Label Label59;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divCustomsJobEntry;
		protected System.Web.UI.HtmlControls.HtmlTable Table5;
		protected System.Web.UI.WebControls.TextBox JobEntryNo;
		protected System.Web.UI.WebControls.TextBox HouseAWBNo;
		protected System.Web.UI.WebControls.TextBox CustomerID;
		protected System.Web.UI.WebControls.TextBox CustomerTaxCode;
		protected System.Web.UI.WebControls.DropDownList TransportMode;
		protected System.Web.UI.WebControls.TextBox ExporterName;
		protected System.Web.UI.WebControls.TextBox ExporterAddress1;
		protected System.Web.UI.WebControls.TextBox MasterAWBNumber;
		protected com.common.util.msTextBox InfoReceivedDate;
		protected System.Web.UI.WebControls.TextBox Consignee_name;
		protected System.Web.UI.WebControls.TextBox Consignee_address1;
		protected System.Web.UI.WebControls.TextBox Consignee_address2;
		protected System.Web.UI.WebControls.TextBox Consignee_zipcode;
		protected System.Web.UI.WebControls.TextBox Consignee_state;
	

		private string appID = null;
		private string enterpriseID = null;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label37;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox ExporterAddress2;
		protected System.Web.UI.WebControls.Label Label45;
		protected System.Web.UI.WebControls.TextBox txtCounty;
		protected System.Web.UI.WebControls.Label Label47;
		protected System.Web.UI.WebControls.TextBox LoadingPortCity;
		protected System.Web.UI.WebControls.Label Label48;
		protected System.Web.UI.WebControls.DropDownList LoadingPortCountry;
		protected System.Web.UI.WebControls.TextBox txtPortCountry;
		protected System.Web.UI.WebControls.DropDownList DischargePort;
		protected System.Web.UI.WebControls.Label Label50;
		protected System.Web.UI.WebControls.DropDownList CurrencyCode;
		protected System.Web.UI.WebControls.Label Label55;
		protected System.Web.UI.WebControls.TextBox exchange_rate;
		protected System.Web.UI.WebControls.TextBox Consignee_telephone;
		protected System.Web.UI.WebControls.CheckBox cbkDoorToDoor;
		protected System.Web.UI.WebControls.Label lblOriginCity;
		protected System.Web.UI.WebControls.DropDownList ddlOriginCity;
		protected System.Web.UI.WebControls.Label lblShipFlightNo;
		protected System.Web.UI.WebControls.TextBox ShipFlightNo;
		protected System.Web.UI.WebControls.Label Label62;
		protected com.common.util.msTextBox ExpectedArrivalDate;
		protected System.Web.UI.WebControls.Label Label64;
		protected com.common.util.msTextBox ActualArrivalDate;
		protected System.Web.UI.WebControls.DropDownList EntryType;
		protected System.Web.UI.WebControls.Label Label68;
		protected System.Web.UI.WebControls.DropDownList DeliveryTerms;
		protected System.Web.UI.WebControls.TextBox txtDeliveryTerms;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.DropDownList ExporterCountry;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label49;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label66;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.TextBox Total_pkgs;
		protected com.common.util.msTextBox Total_weight;
		protected System.Web.UI.WebControls.Label Label16;
		protected com.common.util.msTextBox ChargeableWeight;
		protected System.Web.UI.WebControls.CheckBox IgnoreMAWBValidation;
		private string userID = null;

		public string consignment_no
		{
			get
			{
				if(ViewState["consignment_no"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["consignment_no"];
				}
			}
			set
			{
				ViewState["consignment_no"]=value;
			}
		}


		private void Page_Load(object sender, System.EventArgs e)
		{
			appID = Request.QueryString["appID"].ToString();
			enterpriseID = Request.QueryString["enterpriseID"].ToString();
			userID = Request.QueryString["userID"].ToString();
			
			if(this.IsPostBack==false)
			{
				clearscreen();
				if(Request.QueryString["payerid"] != null)
				{
					CustomerID.Text=Request.QueryString["payerid"].ToString();
				}
				if(Request.QueryString["consignment_no"] != null)
				{
					consignment_no =Request.QueryString["consignment_no"].ToString();
				}
				ExecQry();
				this.btnSave.Attributes.Add("onclick","javascript:document.getElementById('"+ this.btnSave.ClientID + "').disabled=true;" + GetPostBackEventReference(this.btnSave));
				SetInitialFocus(InfoReceivedDate);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.Button3.Click += new System.EventHandler(this.Button3_Click);
			this.TransportMode.SelectedIndexChanged += new System.EventHandler(this.TransportMode_SelectedIndexChanged);
			this.ExporterCountry.SelectedIndexChanged += new System.EventHandler(this.ExporterCountry_SelectedIndexChanged);
			this.cbkDoorToDoor.CheckedChanged += new System.EventHandler(this.cbkDoorToDoor_CheckedChanged);
			this.LoadingPortCountry.SelectedIndexChanged += new System.EventHandler(this.LoadingPortCountry_SelectedIndexChanged);
			this.CurrencyCode.SelectedIndexChanged += new System.EventHandler(this.CurrencyCode_SelectedIndexChanged);
			this.DeliveryTerms.SelectedIndexChanged += new System.EventHandler(this.DeliveryTerms_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void clearscreen()
		{
			JobEntryNo.Text="";
			HouseAWBNo.Text="";
			CustomerID.Text="";
			CustomerTaxCode.Text="";
			TransportMode.Items.Clear();
			ExporterName.Text="";
			ExporterAddress1.Text="";
			ExporterAddress2.Text="";
			ExporterCountry.Items.Clear();
			txtCounty.Text="";
			LoadingPortCity.Text="";
			LoadingPortCountry.Items.Clear();
			txtPortCountry.Text="";
			DischargePort.Items.Clear();
			CurrencyCode.Items.Clear();
			InfoReceivedDate.Text="";
			MasterAWBNumber.Text="";
			ShipFlightNo.Text="";
			ExpectedArrivalDate.Text="";
			ActualArrivalDate.Text="";
			EntryType.Items.Clear();
			DeliveryTerms.Items.Clear();
			Consignee_name.Text="";
			Consignee_address1.Text="";
			Consignee_address2.Text="";
			Consignee_zipcode.Text="";
			Consignee_state.Text="";
			Consignee_telephone.Text="";
			exchange_rate.Text="";

			Total_pkgs.Text="";
			Total_weight.Text="";
			ChargeableWeight.Text="";

			lblOriginCity.Visible=false;
			//lblOriginCityReq.Visible=true;
			ddlOriginCity.Visible=false;
			IgnoreMAWBValidation.Checked=false;
			ddlOriginCity.SelectedIndex=0;
		}


		private void BindControl()
		{
			DataSet dsTransportMode = CustomJobCompilingDAL.GetDropDownListDefaultDisplayValue(appID,enterpriseID,"TransportMode");
			DataSet dsDischargePort = CustomJobCompilingDAL.GetDropDownListDefaultDisplayValue(appID,enterpriseID,"DischargePort");
			DataSet dsEntryType = CustomJobCompilingDAL.GetDropDownListDefaultDisplayValue(appID,enterpriseID,"EntryType");
			DataSet dsCurrencies = CustomJobCompilingDAL.GetCurrencies(appID,enterpriseID);
			DataSet dsExporterCountry = CustomJobCompilingDAL.GetCountry(appID,enterpriseID);
			DataSet dsLoadingPortCountry = CustomJobCompilingDAL.GetCountry(appID,enterpriseID);
			DataSet dsDeliveryTerms = CustomJobCompilingDAL.GetDropDownListDefaultDisplayValue(appID,enterpriseID,"Incoterms");
			DataSet dsExpressCities = CustomJobCompilingDAL.GetDropDownListDefaultDisplayValue(appID,enterpriseID,"ExpressCities");

			txtCounty.Text="";
			txtPortCountry.Text="";
			txtModeOfTransport.Text="";
			txtDeliveryTerms.Text="";
			TransportMode.DataSource=dsTransportMode;
			TransportMode.DataTextField="DropDownListDisplayValue";
			TransportMode.DataValueField="CodedValue";
			TransportMode.DataBind();
			if(dsTransportMode.Tables[0].Rows.Count>0)
			{
				txtModeOfTransport.Text=dsTransportMode.Tables[0].Rows[0]["Description"].ToString();
			}

			DischargePort.DataSource=dsDischargePort;
			DischargePort.DataTextField="DropDownListDisplayValue";
			DischargePort.DataValueField="CodedValue";
			DischargePort.DataBind();

			EntryType.DataSource=dsEntryType;
			EntryType.DataTextField="DropDownListDisplayValue";
			EntryType.DataValueField="CodedValue";
			EntryType.DataBind();

			CurrencyCode.DataSource=dsCurrencies;
			CurrencyCode.DataTextField="CurrencyCode";
			CurrencyCode.DataValueField="CurrencyCode";
			CurrencyCode.DataBind();

			ExporterCountry.DataSource=dsExporterCountry;
			ExporterCountry.DataTextField="CountryCode";
			ExporterCountry.DataValueField="CountryCode";
			ExporterCountry.DataBind();
			if(dsExporterCountry.Tables[0].Rows.Count>0)
			{
				txtCounty.Text=dsExporterCountry.Tables[0].Rows[0]["CountryName"].ToString();
			}

			LoadingPortCountry.DataSource=dsLoadingPortCountry;
			LoadingPortCountry.DataTextField="CountryCode";
			LoadingPortCountry.DataValueField="CountryCode";
			LoadingPortCountry.DataBind();
			if(dsLoadingPortCountry.Tables[0].Rows.Count>0)
			{
				txtPortCountry.Text=dsLoadingPortCountry.Tables[0].Rows[0]["CountryName"].ToString();
			}

			DeliveryTerms.DataSource=dsDeliveryTerms;
			DeliveryTerms.DataTextField="DropDownListDisplayValue";
			DeliveryTerms.DataValueField="CodedValue";
			DeliveryTerms.DataBind();
			if(dsDeliveryTerms.Tables[0].Rows.Count>0)
			{
				txtDeliveryTerms.Text=dsDeliveryTerms.Tables[0].Rows[0]["Description"].ToString();
			}

			ddlOriginCity.DataSource=dsExpressCities;
			ddlOriginCity.DataTextField="DropDownListDisplayValue";
			ddlOriginCity.DataValueField="CodedValue";
			ddlOriginCity.DataBind();

		}


		private void ExecQry()
		{
			BindControl();
			lblError.Text="";
			DataTable dtParams = CustomerConsignmentDAL.JobRegistrationParameter();
			System.Data.DataRow drNew = dtParams.NewRow();
			drNew["action"] ="0";
			drNew["enterpriseid"] =enterpriseID;
			drNew["userloggedin"] =userID;
			drNew["payerid"] =this.CustomerID.Text.Trim();
			drNew["consignment_no"] =this.consignment_no;			
			dtParams.Rows.Add(drNew);
			DataSet ds = CustomerConsignmentDAL.Customs_JobRegistration(appID,enterpriseID,userID,dtParams);
			System.Data.DataTable dtStatus = ds.Tables[0];
			System.Data.DataTable dtJob= ds.Tables[1];
			if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
			{
				BindJobData(dtJob);
			}
			else
			{
				lblError.Text=(dtStatus.Rows.Count>0?dtStatus.Rows[0]["ErrorMessage"].ToString():"");
			}
		}


		private void BindJobData(DataTable dtJob)
		{
			if(dtJob.Rows.Count<=0)
			{
				return;
			}
			DataRow dr = dtJob.Rows[0];
			JobEntryNo.Text=dr["JobEntryNo"].ToString();
			HouseAWBNo.Text=dr["HouseAWBNo"].ToString();
			CustomerID.Text=dr["CustomerID"].ToString();
			CustomerTaxCode.Text=dr["CustomerTaxCode"].ToString();
			try
			{
				TransportMode.SelectedValue=dr["TransportMode"].ToString();
			}
			catch
			{

			}
			
			DataSet dsTransportMode = CustomJobCompilingDAL.GetDropDownListDisplayValue(appID,enterpriseID,"TransportMode",TransportMode.SelectedValue);			
			txtModeOfTransport.Text="";			
			if(dsTransportMode.Tables[0].Rows.Count>0)
			{
				txtModeOfTransport.Text=dsTransportMode.Tables[0].Rows[0]["Description"].ToString();
			}

			ExporterName.Text=dr["ExporterName"].ToString();
			ExporterAddress1.Text=dr["ExporterAddress1"].ToString();
			ExporterAddress2.Text=dr["ExporterAddress2"].ToString();
			try
			{
				ExporterCountry.SelectedValue=dr["ExporterCountry"].ToString();
			}
			catch
			{

			}
			
			txtCounty.Text=CustomJobCompilingDAL.GetCountryName(appID,enterpriseID,ExporterCountry.SelectedValue);

			LoadingPortCity.Text=dr["LoadingPortCity"].ToString();
			if(dr["LoadingPortCountry"] != DBNull.Value && dr["LoadingPortCountry"].ToString().Trim() !="")
			{
				LoadingPortCountry.SelectedValue=dr["LoadingPortCountry"].ToString();
				txtPortCountry.Text=CustomJobCompilingDAL.GetCountryName(appID,enterpriseID,LoadingPortCountry.SelectedValue);
			}
			else
			{
				try
				{
					LoadingPortCountry.SelectedValue="AU";
					txtPortCountry.Text=CustomJobCompilingDAL.GetCountryName(appID,enterpriseID,LoadingPortCountry.SelectedValue);
				}
				catch
				{

				}
			}
			try
			{
				DischargePort.SelectedValue=dr["DischargePort"].ToString();
			}
			catch
			{

			}
			try
			{
				CurrencyCode.SelectedValue=dr["CurrencyCode"].ToString();
			}
			catch
			{

			}
			
			InfoReceivedDate.Text=(dr["InfoReceivedDate"] != DBNull.Value ? DateTime.Parse(dr["InfoReceivedDate"].ToString()).ToString("dd/MM/yyyy") :"");
			MasterAWBNumber.Text=dr["MasterAWBNumber"].ToString();
			ShipFlightNo.Text=dr["ShipFlightNo"].ToString();
			ExpectedArrivalDate.Text=(dr["ExpectedArrivalDate"] != DBNull.Value ? DateTime.Parse(dr["ExpectedArrivalDate"].ToString()).ToString("dd/MM/yyyy") :"");
			ActualArrivalDate.Text=(dr["ActualArrivalDate"] != DBNull.Value ? DateTime.Parse(dr["ActualArrivalDate"].ToString()).ToString("dd/MM/yyyy") :"");
			try
			{
				EntryType.SelectedValue=dr["EntryType"].ToString();
			}
			catch
			{

			}
			
			try
			{
				DeliveryTerms.SelectedValue=dr["DeliveryTerms"].ToString();
			}
			catch
			{

			}
			
			txtDeliveryTerms.Text=dr["DeliveryTerms_Desc"].ToString();

			Consignee_name.Text=dr["Consignee_name"].ToString();
			Consignee_address1.Text=dr["Consignee_address1"].ToString();
			Consignee_address2.Text=dr["Consignee_address2"].ToString();
			Consignee_zipcode.Text=dr["Consignee_zipcode"].ToString();
			Consignee_state.Text=dr["Consignee_state"].ToString();
			Consignee_telephone.Text=dr["Consignee_telephone"].ToString();
			exchange_rate.Text=dr["exchange_rate"].ToString();

			Total_pkgs.Text=(dr["Total_pkgs"] != DBNull.Value?Decimal.Parse(dr["Total_pkgs"].ToString()).ToString("N0"): "0");;
			Total_weight.Text=(dr["Total_weight"] != DBNull.Value?Decimal.Parse(dr["Total_weight"].ToString()).ToString("N1"): "");
			ChargeableWeight.Text=(dr["ChargeableWeight"] != DBNull.Value?Decimal.Parse(dr["ChargeableWeight"].ToString()).ToString("N1"): "");

			cbkDoorToDoor.Checked=false;
			lblOriginCity.Visible=false;
			//lblOriginCityReq.Visible=true;
			ddlOriginCity.Visible=false;

			if(dr["ExpressFlag"] != DBNull.Value && dr["ExpressFlag"].ToString().Trim() =="1")
			{
				cbkDoorToDoor.Checked=true;
				lblOriginCity.Visible=true;
				//lblOriginCityReq.Visible=true;
				ddlOriginCity.Visible=true;
				try
				{
					ddlOriginCity.SelectedValue=dr["ExpressCity"].ToString();
				}
				catch
				{
					ddlOriginCity.SelectedIndex=0;
				}
			}
			if(JobEntryNo.Text.Trim() != "")
			{
				EntryType.Enabled=false;
			}
			else
			{
				EntryType.Enabled=true;
			}

			IgnoreMAWBValidation.Checked=false;
			if(dr["IgnoreMAWBValidation"] != DBNull.Value && dr["IgnoreMAWBValidation"].ToString().Equals("1"))
			{
				IgnoreMAWBValidation.Checked=true;
			}
		}


		private void TransportMode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DataSet dsTransportMode = CustomJobCompilingDAL.GetDropDownListDisplayValue(appID,enterpriseID,"TransportMode",TransportMode.SelectedValue);			
			txtModeOfTransport.Text="";			
			if(dsTransportMode.Tables[0].Rows.Count>0)
			{
				txtModeOfTransport.Text=dsTransportMode.Tables[0].Rows[0]["Description"].ToString();
			}		
			SetInitialFocus(TransportMode);
		}


		private void ExporterCountry_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DataSet ds = CustomJobCompilingDAL.GetCountry(appID,enterpriseID,ExporterCountry.SelectedValue);
			txtCounty.Text="";
			
			try
			{
				if(ds.Tables[0].Rows.Count>0)
				{
					txtCounty.Text=ds.Tables[0].Rows[0]["CountryName"].ToString();
					CurrencyCode.SelectedValue=ds.Tables[0].Rows[0]["CurrencyCode"].ToString();

				}				
			}
			catch
			{

			}
			DateTime RateDate = DateTime.Now;
			if(InfoReceivedDate.Text.Trim()=="")
			{
				lblError.Text="Info received date is required";
				return;
			}
			try
			{
				RateDate= DateTime.ParseExact(InfoReceivedDate.Text.Trim(),"dd/MM/yyyy",null);
			}
			catch
			{

			}

			CountriesCurrencies currency = SysDataMgrDAL.GetCountriesCurrencies(appID,enterpriseID,null,CurrencyCode.SelectedValue,RateDate);			
			exchange_rate.Text = currency.Exchange_rate;

			SetInitialFocus(ExporterCountry);
		}


		private void LoadingPortCountry_SelectedIndexChanged(object sender, System.EventArgs e)
		{	
			txtPortCountry.Text=CustomJobCompilingDAL.GetCountryName(appID,enterpriseID,LoadingPortCountry.SelectedValue);
			//txtPortCountry.Text = LoadingPortCountry.SelectedItem.Text;
			SetInitialFocus(LoadingPortCountry);
		}


		private void DeliveryTerms_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DataSet dsDeliveryTerms = CustomJobCompilingDAL.GetDropDownListDisplayValue(appID,enterpriseID,"Incoterms",DeliveryTerms.SelectedValue);			
			txtDeliveryTerms.Text="";			
			if(dsDeliveryTerms.Tables[0].Rows.Count>0)
			{
				txtDeliveryTerms.Text=dsDeliveryTerms.Tables[0].Rows[0]["Description"].ToString();
			}
			SetInitialFocus(DeliveryTerms);
		}


		private void CurrencyCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DateTime RateDate = DateTime.Now;
			if(InfoReceivedDate.Text.Trim()=="")
			{
				lblError.Text="Info received date is required";
				return;
			}
			try
			{
				RateDate= DateTime.ParseExact(InfoReceivedDate.Text.Trim(),"dd/MM/yyyy",null);
			}
			catch
			{

			}

			CountriesCurrencies currency = SysDataMgrDAL.GetCountriesCurrencies(appID,enterpriseID,null,CurrencyCode.SelectedValue,RateDate);

			//ExchangeRate rate = SysDataMgrDAL.GetExchangeRate(appID,enterpriseID,CurrencyCode.SelectedValue);
			exchange_rate.Text = currency.Exchange_rate;

			SetInitialFocus(CurrencyCode);
		}


		private void Button3_Click(object sender, System.EventArgs e)
		{
			string sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.opener.callback();  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}


		private void btnSave_Click(object sender, System.EventArgs e)
		{
			if(Utility.GetConsNoRegex(MasterAWBNumber.Text.Trim())==false)
			{
				lblError.Text = "Master AWB Number is invalid.";
				SetInitialFocus(MasterAWBNumber);
				return;
			}

			if(InfoReceivedDate.Text.Trim()=="")
			{
				lblError.Text="Info received date is required";
				return;
			}

			try
			{
				DateTime.ParseExact(InfoReceivedDate.Text,"dd/MM/yyyy",null).ToString("yyyy-MM-dd");
			}
			catch
			{
				lblError.Text="Info received date is invalid";
				return;
			}

			if(MasterAWBNumber.Text.Trim()=="")
			{
				lblError.Text="Master AWB number is required";
				return;
			}

			if(TransportMode.SelectedValue=="")
			{
				lblError.Text="Transport mode is required";
				return;
			}

			if(ExporterName.Text=="")
			{
				lblError.Text="Exporter name is required";
				return;
			}

			if(ExporterCountry.SelectedValue=="")
			{
				lblError.Text="Exporter country is required";
				return;
			}

			if(LoadingPortCity.Text.Trim() =="")
			{
				lblError.Text="Loading port city is required";
				return;
			}

			if(LoadingPortCountry.SelectedValue=="")
			{
				lblError.Text="Loading port country is required";
				return;
			}

			if(DischargePort.SelectedValue=="")
			{
				lblError.Text="Discharge port is required";
				return;
			}

			if(CurrencyCode.SelectedValue=="")
			{
				lblError.Text="Currency code is required";
				return;
			}

			if(ShipFlightNo.Text.Trim() =="")
			{
				lblError.Text="Ship/flight number is required";
				return;
			}

			if(ExpectedArrivalDate.Text.Trim() =="")
			{
				lblError.Text="Expected arrival date is required";
				return;
			}

			try
			{
				DateTime.ParseExact(ExpectedArrivalDate.Text,"dd/MM/yyyy",null).ToString("yyyy-MM-dd");
			}
			catch
			{
				lblError.Text="Expected arrival date is invalid";
				return;
			}

			if(ActualArrivalDate.Text.Trim() !="")
			{
				try
				{
					DateTime.ParseExact(ActualArrivalDate.Text,"dd/MM/yyyy",null).ToString("yyyy-MM-dd");
				}
				catch
				{
					lblError.Text=" Actual goods arrival date is invalid";
					return;
				}
			}

			if(EntryType.SelectedValue=="")
			{
				lblError.Text="Entry type is required";
				return;
			}

			if(DeliveryTerms.SelectedValue=="")
			{
				lblError.Text="Delivery terms are required";
				return;
			}

			
			if(Total_pkgs.Text=="")
			{
				lblError.Text="Package quantity must be greater than zero";
				return;
			}

			
			if(Total_weight.Text=="")
			{
				lblError.Text="Total weight must be greater than zero";
				return;
			}

//			if(cbkDoorToDoor.Checked && ddlOriginCity.SelectedValue=="")
//			{
//				lblError.Text="Origin city is required for Express door-to-door customs jobs";
//				return;
//			}

			DataTable dtParams = CustomerConsignmentDAL.JobRegistrationParameter();
			System.Data.DataRow drNew = dtParams.NewRow();
			foreach(System.Data.DataColumn col in dtParams.Columns)
			{
				string colName = col.ColumnName;
				System.Web.UI.Control ctrol = this.Page.FindControl(colName);
				if(ctrol != null)
				{
					if(ctrol.GetType()==typeof(TextBox))
					{
						TextBox txt = (TextBox)ctrol;
						if(txt.Text.Trim() != "")
						{
							if(txt.ID=="sender_zipcode" || txt.ID=="recipient_zipcode")
							{
								drNew[colName]=txt.Text.ToUpper().Trim();
							}
							else
							{
								drNew[colName]=txt.Text.Trim();
							}							
						}						
					}
					else if(ctrol.GetType()==typeof(com.common.util.msTextBox))
					{
						com.common.util.msTextBox txt = (com.common.util.msTextBox)ctrol;
						if(txt.TextMaskType==MaskType.msDate)
						{
							drNew[colName]=DateTime.ParseExact(txt.Text,"dd/MM/yyyy",null).ToString("yyyy-MM-dd");
						}
						else
						{
							if(colName=="Total_weight" || colName=="ChargeableWeight" )
							{
								drNew[colName]=txt.Text.Trim().Replace(",","");
							}
							else
							{
								drNew[colName]=txt.Text.Trim();
							}
						}
						
					}
					else if(ctrol.GetType()==typeof(DropDownList))
					{
						DropDownList ddl = (DropDownList)ctrol;
						drNew[colName]=ddl.SelectedValue;
					}				
				}
			}
			drNew["action"] ="1";
			drNew["enterpriseid"] =enterpriseID;
			drNew["userloggedin"] =userID;
			drNew["payerid"] =this.CustomerID.Text.Trim();
			drNew["consignment_no"] =this.consignment_no;				
			if(cbkDoorToDoor.Checked)
			{
				drNew["ExpressDoorToDoor"] = 1;
				if(ddlOriginCity.SelectedValue  !="")
				{
					drNew["ExpressCity"] = ddlOriginCity.SelectedValue;
				}
			}
			else
			{
				drNew["ExpressDoorToDoor"] = 0;
				drNew["ExpressCity"] = DBNull.Value;
			}

			drNew["IgnoreMAWBValidation"] =(IgnoreMAWBValidation.Checked==true?1:0);
			dtParams.Rows.Add(drNew);

			DataSet ds = CustomerConsignmentDAL.Customs_JobRegistration(appID,enterpriseID,userID,dtParams);
			System.Data.DataTable dtStatus = ds.Tables[0];
			System.Data.DataTable dtJob = ds.Tables[1];
			if(dtStatus != null && dtStatus.Rows.Count>0)
			{
				lblError.Text = dtStatus.Rows[0]["ErrorMessage"].ToString();
				if(Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
				{
					BindJobData(dtJob);
				}
			}
			btnSave.Enabled=true;
		}

		private void cbkDoorToDoor_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbkDoorToDoor.Checked)
			{
				lblOriginCity.Visible=true;
				//lblOriginCityReq.Visible=true;
				ddlOriginCity.Visible=true;
				if(ddlOriginCity.Items.Count>0)
				{
					ddlOriginCity.SelectedIndex=0;
				}
			}
			else
			{
				lblOriginCity.Visible=false;
				//lblOriginCityReq.Visible=true;
				ddlOriginCity.Visible=false;
			}
			SetInitialFocus(cbkDoorToDoor);
		}
	}
}
