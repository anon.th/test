using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.RBAC;
using com.common.DAL;
using com.common.util;
using com.common.classes;
//using System.Web.UI.WebControls;
using com.common.applicationpages;
using com.ties.classes;
using com.ties.DAL;
using com.ties.BAL;
using com.ties;

namespace com.ties
{
	/// <summary>
	/// Summary description for InvoiceDetails.
	/// </summary>
	public class InvoiceDetails : BasePage
	{
		protected System.Web.UI.WebControls.Button btnCancelDtl;
		protected System.Web.UI.WebControls.Label lblErrorMsgDtl;
		protected System.Web.UI.WebControls.Label lblMainTitleDtl;
		protected System.Web.UI.WebControls.Label lblProvinceDtl;
		protected System.Web.UI.WebControls.Label Label_50;
		protected System.Web.UI.WebControls.Label lblCustIDDtl;
		protected System.Web.UI.WebControls.Label Label_49;
		protected System.Web.UI.WebControls.Label lblTelephoneDtl;
		protected System.Web.UI.WebControls.Label Label_48;
		protected System.Web.UI.WebControls.Label lblCustNameDtl;
		protected System.Web.UI.WebControls.Label Label_47;
		protected System.Web.UI.WebControls.Label lblFaxDtl;
		protected System.Web.UI.WebControls.Label Label_45;
		protected System.Web.UI.WebControls.Label lblAddress1Dtl;
		protected System.Web.UI.WebControls.Label Label_1;
		protected System.Web.UI.WebControls.Label lblAddress2Dtl;
		protected System.Web.UI.WebControls.Label Label_2;
		protected System.Web.UI.WebControls.Label lblPostalCodeDtl;
		protected System.Web.UI.WebControls.Label Label_46;
		protected System.Web.UI.WebControls.Label lblHC_POD_RequiredDtl;
		protected System.Web.UI.WebControls.Label Label_3;
		protected System.Web.UI.WebControls.Label Label_7;
		protected System.Web.UI.WebControls.Label Label_9;
		protected System.Web.UI.WebControls.Label Label_11;
		protected System.Web.UI.WebControls.Label Label_13;
		protected System.Web.UI.WebControls.Label Label_15;
		protected System.Web.UI.WebControls.Label Label_17;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.WebControls.Label lblInvNoDtl;
		protected System.Web.UI.WebControls.Label lblInvStatusDtl;
		protected System.Web.UI.WebControls.Label lblPaymentTermDtl;
		protected System.Web.UI.WebControls.Label lblInvDateDtl;
		protected System.Web.UI.WebControls.Label lblDueDateDtl;
		protected System.Web.UI.WebControls.Label lblTotalAmtDtl;
		protected System.Web.UI.WebControls.Label lblTotalAdjDtl;
		protected System.Web.UI.WebControls.Label Label_19;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.WebControls.Label lblContactDtl;
		protected System.Web.UI.WebControls.Label lblCancel_AppHeadDtl;
		protected System.Web.UI.WebControls.Label lblCancel_AppTextDtl;
		protected System.Web.UI.WebControls.DataGrid dgEditDtl;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummaryDtl;

		
		String appID = null;
		String enterpriseID = null;
		String m_strCulture = null;
		String m_strUserID = null;
		String strInvNo = null;
		String strRemark = null;
		String UserRole = "ACCT";

//		decimal temp_tot_freight;
//		decimal temp_insurance_amt;
//		decimal temp_other_surcharge;
//		decimal temp_esa_surcharge;
//		decimal temp_total_exception;
//		decimal temp_mbg_amount;
//		decimal temp_old_invoice_adj;
//		decimal temp_new_invoice_adj;
//		decimal temp_tot_vas_surcharge;
//		decimal temp_old_total;
		protected System.Web.UI.WebControls.DataGrid dgPreviewDtl;
		protected com.common.util.msTextBox Mstextbox1;
		protected System.Web.UI.WebControls.Label Label_6;
//		decimal temp_new_total;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			m_strUserID = utility.GetUserID();
			m_strCulture = utility.GetUserCulture();
		
//			
//			if(Request.Params["INVNO"] != null)
//			{
//				strInvNo = Request.Params["INVNO"];
//			}			
			DataSet profileDS = SysDataManager1.GetEnterpriseProfile(utility.GetAppID(),utility.GetEnterpriseID());

			//Call function for rounding in TIESUtility 
			if(profileDS.Tables[0].Rows.Count > 0)
			{ 
				int currency_decimal = (int) profileDS.Tables[0].Rows[0]["currency_decimal"];
				ViewState["m_format"] = "{0:F" + currency_decimal.ToString() + "}";

				if  ( profileDS.Tables[0].Rows[0]["wt_rounding_method"] != System.DBNull.Value)
				{
						
					ViewState["wt_rounding_method"] = Convert.ToInt32(profileDS.Tables[0].Rows[0]["wt_rounding_method"]);
				}

				if  ( profileDS.Tables[0].Rows[0]["wt_increment_amt"] != System.DBNull.Value)
				{
					ViewState["wt_increment_amt"] = Convert.ToDecimal(profileDS.Tables[0].Rows[0]["wt_increment_amt"]);
				}
			}		

			ViewState["strInvNo"] = "INV2775";

			BindingHeader();
			BindingDetail();
			this.dgEditDtl.Visible = false;
			this.lblErrorMsgDtl.Text = "";
			
			
		} 

		

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnCancelDtl.Click += new System.EventHandler(this.btnCancelDtl_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void BindingHeader()
		{
			DateTime dtFrom = DateTime.MinValue ;
			DateTime dtTo = DateTime.MaxValue ;
			strInvNo = (String)ViewState["strInvNo"];
			DataSet dsInv = InvoiceManagementMgrDAL.GetInvoiceByInvNo(appID,enterpriseID,strInvNo);//GetInvoice(appID,enterpriseID,dtFrom,dtTo,strInvNo,dtFrom,dtTo,null,null,null);
			Session["SESSION_DSINV"] = dsInv;

			DataRow drEach = dsInv.Tables[0].Rows[0];
			if ((drEach["payerid"] != null) && (drEach["payerid"].ToString() != ""))
			{
				if(drEach["payerid"].ToString().Trim() == "N/A")
				{
					this.lblCustIDDtl.Text = "CASH";
				}
				else
				{
					this.lblCustIDDtl.Text = drEach["payerid"].ToString();
				}
			}

			if ((drEach["payer_name"] != null) && (drEach["payer_name"].ToString() != ""))
			{
				this.lblCustNameDtl.Text = drEach["payer_name"].ToString();
			}

			if ((drEach["payer_address1"] != null) && (drEach["payer_address1"].ToString() != ""))
			{
				this.lblAddress1Dtl.Text = drEach["payer_address1"].ToString();
			}

			if ((drEach["payer_address2"] != null) && (drEach["payer_address2"].ToString() != ""))
			{
				this.lblAddress2Dtl.Text = drEach["payer_address2"].ToString();
			}

			if ((drEach["payer_zipcode"] != null) && (drEach["payer_zipcode"].ToString() != ""))
			{
				this.lblPostalCodeDtl.Text = drEach["payer_zipcode"].ToString();
			}

			if ((drEach["state_code"] != null) && (drEach["state_code"].ToString() != ""))
			{
				this.lblProvinceDtl.Text = drEach["state_code"].ToString();
			}

			if ((drEach["payer_telephone"] != null) && (drEach["payer_telephone"].ToString() != ""))
			{
				this.lblTelephoneDtl.Text = drEach["payer_telephone"].ToString();
			}
			
			if ((drEach["payer_fax"] != null) && (drEach["payer_fax"].ToString() != ""))
			{
				this.lblFaxDtl.Text = drEach["payer_fax"].ToString();
			}
			
			if ((drEach["contact_person"] != null) && (drEach["contact_person"].ToString() != ""))
			{
				this.lblContactDtl.Text = drEach["contact_person"].ToString();
			} 
			
			if ((drEach["pod_slip_required"] != null) && (drEach["pod_slip_required"].ToString() != ""))
			{
				String podStatus = drEach["pod_slip_required"].ToString();
				this.lblHC_POD_RequiredDtl.Text = InvoiceManagementMgrDAL.GetCodeTextByValue(appID, m_strCulture,"pod_slip_required", podStatus ,CodeValueType.StringValue);
			} 
			
			if ((drEach["invoice_no"] != null) && (drEach["invoice_no"].ToString() != ""))
			{
				this.lblInvNoDtl.Text = drEach["invoice_no"].ToString();
			}

			if ((drEach["invoice_status"] != null) && (drEach["invoice_status"].ToString() != ""))
			{
				String invStatus =  drEach["invoice_status"].ToString();
				ViewState["INVSTATE"] = invStatus;
				this.lblInvStatusDtl.Text = InvoiceManagementMgrDAL.GetCodeTextByValue(appID,m_strCulture,"invoice_status",invStatus,CodeValueType.StringValue);
				switch(invStatus)
				{
					case "A":
						this.lblCancel_AppHeadDtl.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"Approve Date",m_strCulture);
						if(!drEach["approved_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						{
							DateTime dtApprove = (DateTime)drEach["approved_date"];
							this.lblCancel_AppTextDtl.Text = dtApprove.ToString("dd/MM/yyyy ");
						}

						break;

					case "C":
						this.lblCancel_AppHeadDtl.Text = Utility.GetLanguageText(ResourceType.ScreenLabel,"Cancel Date",m_strCulture);
						if(!drEach["cancel_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						{
							DateTime dtCancel = (DateTime)drEach["cancel_date"];
							this.lblCancel_AppTextDtl.Text = dtCancel.ToString("dd/MM/yyyy ");
						}
						break;
				}
			}

			if ((drEach["payment_mode"] != null) && (drEach["payment_mode"].ToString() != ""))
			{
				String strPaymentMode = drEach["payment_mode"].ToString();
				switch(strPaymentMode)
				{
					case "C"://Cash
						this.lblPaymentTermDtl.Text = "NOT APPLICABLE";
						break;
					case "R"://Creadit
						if((drEach["credit_term"] != null) && drEach["credit_term"].ToString() != "")
						{
							int creditTerm = Convert.ToInt16(drEach["credit_term"]);
							if(creditTerm == 0)
							{
								this.lblPaymentTermDtl.Text = "IMMEDIATE";
							}
							else if(creditTerm > 0)
							{
								this.lblPaymentTermDtl.Text = "NET "+creditTerm.ToString()+" DAYS";
							}
							else 
							{
								this.lblPaymentTermDtl.Text = "";
							}

						}
						else
						{
							this.lblPaymentTermDtl.Text = "NONE SPECIFIED";
						}

						break;
				}

			}
			
			if ((drEach["invoice_date"] != null) && (drEach["invoice_date"].ToString() != ""))
			{
				DateTime dtInvoice = (DateTime)drEach["invoice_date"];
				this.lblInvDateDtl.Text = dtInvoice.ToString("dd/MM/yyyy ");
			}

			if ((drEach["due_date"] != null) && (drEach["due_date"].ToString() != ""))
			{
				DateTime dtDueDate= (DateTime)drEach["due_date"];
				this.lblDueDateDtl.Text = dtDueDate.ToString("dd/MM/yyyy ");
			}

			if ((drEach["invoice_amt"] != null) && (drEach["invoice_amt"].ToString() != ""))
			{
				this.lblTotalAmtDtl.Text = utility.encodeNegValue(drEach["invoice_amt"].ToString(),(String)ViewState["m_format"]);
			}

			if ((drEach["invoice_adj_amount"] != null) && (drEach["invoice_adj_amount"].ToString() != ""))
			{				
				this.lblTotalAdjDtl.Text = utility.encodeNegValue(drEach["invoice_adj_amount"].ToString(),(String)ViewState["m_format"]);
			}

		
		}


		private void BindingDetail()
		{
			strInvNo = (String)ViewState["strInvNo"];
			DataSet dsInvDtl = InvoiceManagementMgrDAL.GetInvoiceDetail(appID,enterpriseID,strInvNo);
			Session["SESSION_DSINVDTL"] = dsInvDtl;
			dgPreviewDtl.DataSource = dsInvDtl;
			dgPreviewDtl.DataBind();
		}


		public void dgPreviewDtl_dgEditDtl(object sender, DataGridCommandEventArgs e)
		{
			//gridFieldSelected
			int col = dgPreviewDtl.Columns.Count;
			for (int i=0;i<col;i++)
			{
				e.Item.CssClass = "gridFieldSelected";
				e.Item.Cells[i].BackColor = Color.FromName("#ffcc00");
				//e.Item.Style.Add("background-color","#ffcc00");
			}
		   
			//Consgn_No
			if(!e.Item.Cells[0].Text.GetType().Equals(System.Type.GetType("System.DBNull")) && e.Item.Cells[0].Text.Trim() != "" && e.Item.Cells[0].Text.Trim() != "&nbsp;" )
			{
				ViewState["strConsgnNo"] = e.Item.Cells[1].Text;
			}
			//insurance_surcharge,other_surch_amount,tot_vas_surcharge,esa_surcharge,total_exception,invoice_adj_amount,other_surcharge,mbg_amount,invoice_amt

			Label lblFreight = (Label)e.Item.FindControl("lblFreight");
			if((lblFreight != null) && (!lblFreight.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblFreight.Text.Trim() != ""))
			{
				ViewState["temp_tot_freight"] = Convert.ToDecimal(lblFreight.Text);
			}
			else
			{
				ViewState["temp_tot_freight"] = Convert.ToDecimal(0);
			}            
			
			//insruance_amt
			Label lblInsurance = (Label)e.Item.FindControl("lblInsurance");
			if((lblInsurance != null) && (!lblInsurance.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblInsurance.Text.Trim() != ""))
			{
				ViewState["temp_insurance_amt"] = Convert.ToDecimal(lblInsurance.Text);
			}
			else
			{
				ViewState["temp_insurance_amt"] = Convert.ToDecimal(0);
			}

			//tot_vas_surcharge
			Label lblVAS = (Label)e.Item.FindControl("lblVAS");
			if((lblVAS != null) && (!lblVAS.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblVAS.Text.Trim() != ""))
			{				
				ViewState["temp_tot_vas_surcharge"] = Convert.ToDecimal(lblVAS.Text);
			}
			else
			{
				ViewState["temp_tot_vas_surcharge"] = Convert.ToDecimal(0);
			}

			//esa_surcharge
			Label lblESA = (Label)e.Item.FindControl("lblESA");
			if((lblESA != null) && (!lblESA.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblESA.Text.Trim() != ""))
			{

				ViewState["temp_esa_surcharge"] = Convert.ToDecimal(lblESA.Text);
			}
			else
			{
				ViewState["temp_esa_surcharge"] = Convert.ToDecimal(0);
			}

			//total_exception
			Label lblException = (Label)e.Item.FindControl("lblException");
			if((lblException != null) && (!lblException.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblException.Text.Trim() != ""))
			{
				ViewState["temp_total_exception"] = Convert.ToDecimal(utility.decodeNegValue(lblException.Text));
			}
			else
			{
				ViewState["temp_total_exception"] = Convert.ToDecimal(0);
			}

			//invoice_adj_amount
			Label lblAdjustment = (Label)e.Item.FindControl("lblAdjustment");
			if((lblAdjustment != null) && (!lblAdjustment.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblAdjustment.Text.Trim() != ""))
			{
				ViewState["temp_old_invoice_adj"] = Convert.ToDecimal(utility.decodeNegValue(lblAdjustment.Text));
				ViewState["InvAdjExist"] = true;
			}
			else
			{
				ViewState["temp_old_invoice_adj"] = Convert.ToDecimal(0);	
				ViewState["InvAdjExist"] = false;
			}            

			//other_surcharge
			Label lblOther = (Label)e.Item.FindControl("lblOther");
			if((lblOther != null) && (!lblOther.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblOther.Text.Trim() != ""))
			{
				ViewState["temp_other_surcharge"] = Convert.ToDecimal(utility.decodeNegValue(lblOther.Text));
			}
			else
			{
				ViewState["temp_other_surcharge"] = Convert.ToDecimal(0);
			}

			//mbg_amount
			Label lblMBG = (Label)e.Item.FindControl("lblMBG");
			if((lblMBG != null) && (!lblMBG.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblMBG.Text.Trim() != ""))
			{
				ViewState["temp_mbg_amount"] = Convert.ToDecimal(lblMBG.Text);
			}
			else
			{
				ViewState["temp_mbg_amount"] = Convert.ToDecimal(0);
			}

			//temp_old_total 
			Label lblTOT = (Label)e.Item.FindControl("lblTOT");
			if((lblTOT != null) && (!lblTOT.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblTOT.Text.Trim() != ""))
			{
				ViewState["temp_old_total"] = Convert.ToDecimal(lblTOT.Text);
			}
			else
			{
				ViewState["temp_old_total"] = Convert.ToDecimal(0);
			}

			if(!e.Item.Cells[19].Text.GetType().Equals(System.Type.GetType("System.DBNull")) && e.Item.Cells[19].Text.Trim() != "" && e.Item.Cells[19].Text.Trim() != "&nbsp;")
			{
				ViewState["temp_adjusted_remark"] = e.Item.Cells[19].Text;
			}
			else
			{
				ViewState["temp_adjusted_remark"] = "";
			}

			this.dgEditDtl.Visible = true;
			//dgEditDtl.DataBind();
			int iSelIndex = e.Item.ItemIndex;
			DataGridItem dgRow = dgPreviewDtl.Items[iSelIndex];
			ViewState["strConsgnNo"] = dgRow.Cells[1].Text;

			DataSet ds = new DataSet();
			DataTable dt = new DataTable();
			DataRow dr = dt.NewRow();

			dt.Columns.Add(new DataColumn("consignment_no", typeof(string)));

			dr["consignment_no"] = ViewState["strConsgnNo"];
			dt.Rows.Add(dr);
			ds.Tables.Add(dt);

			dgEditDtl.DataSource = ds;
			dgEditDtl.DataBind();
		}


		public void dgEditDtl_dgUpdate(object sender, DataGridCommandEventArgs e)
		{
			decimal temp_new_invoice_adj = 0;
			decimal sumAmtInvDtl = 0;
			sumAmtInvDtl = SumAmountInvDtl();
			TextBox txtRemark = (TextBox)e.Item.FindControl("txtRemark");
			msTextBox txtAmount = (msTextBox)e.Item.FindControl("txtAmount");
			
			if(txtRemark.Text.Trim() == "" && txtAmount.Text.Trim() == "")
			{
				//UPDATE To Invoice_Detail Table
				ViewState["temp_new_total"] = sumAmtInvDtl;
				DataSet dsNewInvDtl = InvoiceManagementMgrDAL.GetEmptyInvoiceDetail();
				DataRow drRecShipment = dsNewInvDtl.Tables[0].Rows[0];
				drRecShipment["invoice_no"] = ViewState["strInvNo"];
				drRecShipment["consignment_no"] = ViewState["strConsgnNo"];
				drRecShipment["adjusted_by"] = m_strUserID;
				drRecShipment["adjusted_date"] = Utility.DateFormat(appID,enterpriseID,System.DateTime.Now,DTFormat.DateLongTime);
				drRecShipment["invoice_adj_amount"] = System.DBNull.Value;
				drRecShipment["invoice_amt"] = (decimal)ViewState["temp_new_total"];
				drRecShipment["adjusted_remark"] = strRemark;


				int iRow = InvoiceManagementMgrDAL.AdjustInvoice(appID,enterpriseID,dsNewInvDtl);
				lblErrorMsgDtl.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",m_strCulture);
				BindingHeader();
				BindingDetail();

				dgEditDtl.Visible = false;
				return;

			}
			else if(txtAmount.Text.Trim() == "0")
			{
				//UPDATE To Invoice_Detail Table
				ViewState["temp_new_total"] = sumAmtInvDtl;
				DataSet dsNewInvDtl = InvoiceManagementMgrDAL.GetEmptyInvoiceDetail();
				DataRow drRecShipment = dsNewInvDtl.Tables[0].Rows[0];
				drRecShipment["invoice_no"] = ViewState["strInvNo"];
				drRecShipment["consignment_no"] = ViewState["strConsgnNo"];
				drRecShipment["adjusted_by"] = m_strUserID;
				drRecShipment["adjusted_date"] = Utility.DateFormat(appID,enterpriseID,System.DateTime.Now,DTFormat.DateLongTime);
				drRecShipment["invoice_adj_amount"] = 0;
				drRecShipment["invoice_amt"] = (decimal)ViewState["temp_new_total"];
				drRecShipment["adjusted_remark"] = strRemark;


				int iRow = InvoiceManagementMgrDAL.AdjustInvoice(appID,enterpriseID,dsNewInvDtl);
				lblErrorMsgDtl.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",m_strCulture);
				BindingHeader();
				BindingDetail();

				dgEditDtl.Visible = false;
				return ;
			}
			else
			{
				if(txtRemark.Text.Trim() == "" || txtRemark.Text == null)
				{
					lblErrorMsgDtl.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVDTL_REMARK",m_strCulture);
					ViewState["ErrDtl"] = true;
					this.dgEditDtl.Visible = true;
					return;
				}
				else
				{
					strRemark = txtRemark.Text;
				}

				if((txtAmount.Text.Trim() == "") || (txtAmount.Text == null))
				{
					lblErrorMsgDtl.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVDTL_AMOUNT",m_strCulture);
					ViewState["ErrDtl"] = true;
					this.dgEditDtl.Visible = true;
					return;
				}
				else
				{			
	
					DropDownList ddlTempApplyAs = (DropDownList)e.Item.FindControl("ddlApplyAs");
					if(ddlTempApplyAs != null)
					{
						if(ddlTempApplyAs.SelectedItem.Value == "A")
						{
							temp_new_invoice_adj = Convert.ToDecimal(txtAmount.Text) ;
						}

						else if (ddlTempApplyAs.SelectedItem.Value == "P")
						{
							decimal tempval;
							//temp_new_invoice_adj =TIESUtility.EnterpriseRounding(Convert.ToDecimal(drCurrent["cod_surcharge_amt"]),(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]
							tempval = (Convert.ToDecimal(txtAmount.Text) * sumAmtInvDtl) / 100;					
							temp_new_invoice_adj = TIESUtility.EnterpriseRounding(tempval,(int)ViewState["wt_rounding_method"], (decimal)ViewState["wt_increment_amt"]);
						}
					}				

					DropDownList ddlTempAdjType = (DropDownList)e.Item.FindControl("ddlAdjType");
					if(ddlTempAdjType != null)
					{
						if(ddlTempAdjType.SelectedItem.Value == "C")
						{
							ViewState["temp_new_invoice_adj"] = -temp_new_invoice_adj ;
						}
						else if(ddlTempAdjType.SelectedItem.Value == "D")
						{
							ViewState["temp_new_invoice_adj"] = Convert.ToDecimal(temp_new_invoice_adj);
						}
					}

					if(ddlTempAdjType.SelectedItem.Value == "C")
					{
						//Check temp_new_invoice_adj have to less than or equal Amount
						if(temp_new_invoice_adj > sumAmtInvDtl)
						{
							lblErrorMsgDtl.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVDTL_AMTLESSZERO",m_strCulture);
							ViewState["ErrDtl"] = true;
							this.dgEditDtl.Visible = true;
							return;
						}
					}
				
					
				}

			
				//UPDATE To Invoice_Detail Table
				ViewState["temp_new_total"] = (decimal)ViewState["temp_new_invoice_adj"] + sumAmtInvDtl;
				DataSet dsNewInvDtl = InvoiceManagementMgrDAL.GetEmptyInvoiceDetail();
				DataRow drRecShipment = dsNewInvDtl.Tables[0].Rows[0];
				drRecShipment["invoice_no"] = ViewState["strInvNo"];
				drRecShipment["consignment_no"] = ViewState["strConsgnNo"];
				drRecShipment["adjusted_by"] = m_strUserID;
				drRecShipment["adjusted_date"] = Utility.DateFormat(appID,enterpriseID,System.DateTime.Now,DTFormat.DateLongTime);
				drRecShipment["invoice_adj_amount"] = (decimal)ViewState["temp_new_invoice_adj"];
				drRecShipment["invoice_amt"] = (decimal)ViewState["temp_new_total"];
				drRecShipment["adjusted_remark"] = strRemark;


				int iRow = InvoiceManagementMgrDAL.AdjustInvoice(appID,enterpriseID,dsNewInvDtl);
				lblErrorMsgDtl.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",m_strCulture);
				BindingHeader();
				BindingDetail();

				dgEditDtl.Visible = false;
			
			}
		}


		public void dgEditDtl_dgBound(object sender, DataGridItemEventArgs e)
		{
			
			if(e.Item.ItemIndex == -1)
			{
				return;
			}


			Label consignment_no = (Label) e.Item.FindControl("lblConsgnNo");
			if(consignment_no != null)
			{
				consignment_no.Text = (String)ViewState["strConsgnNo"];
			}

			com.common.util.msTextBox txtAmount = (com.common.util.msTextBox)e.Item.FindControl("txtAmount");
			if((bool)ViewState["InvAdjExist"])
			{
				txtAmount.Text = Convert.ToString((Math.Abs((decimal)ViewState["temp_old_invoice_adj"])));
			}


			TextBox txtRemark = (TextBox)e.Item.FindControl("txtRemark");
			if((bool)ViewState["InvAdjExist"])
			{
				txtRemark.Text = ViewState["temp_adjusted_remark"].ToString();
			}

			DropDownList ddlAdjType = (DropDownList) e.Item.FindControl("ddlAdjType");
			if(ddlAdjType != null)
			{
				DataTable dtAdjType = new DataTable();
				dtAdjType.Columns.Add(new DataColumn("Text", typeof(string)));
				dtAdjType.Columns.Add(new DataColumn("StringValue", typeof(string)));

				ArrayList listAdjType = Utility.GetCodeValues(utility.GetAppID(),m_strCulture,"adjustment_type",CodeValueType.StringValue);
				foreach(SystemCode typeSysCode in listAdjType)
				{
					DataRow drEach = dtAdjType.NewRow();
					drEach[0] = typeSysCode.Text;
					drEach[1] = typeSysCode.StringValue;
					dtAdjType.Rows.Add(drEach);
				}
				DataView dvAdjType = new DataView(dtAdjType);

				ddlAdjType.DataSource =  dvAdjType;
				ddlAdjType.DataTextField =  "Text";
				ddlAdjType.DataValueField =	 "StringValue";
				ddlAdjType.DataBind();
				if((bool)ViewState["InvAdjExist"])
				{
					if((decimal)ViewState["temp_old_invoice_adj"] < 0)
					{
						ddlAdjType.ClearSelection();
						ddlAdjType.Items.FindByValue("C").Selected = true;
					}
					else
					{
						ddlAdjType.ClearSelection();
						ddlAdjType.Items.FindByValue("D").Selected = true;
					}
				}
				else
				{
					ddlAdjType.Items.FindByValue("C").Selected = true;
				}
				
			}

			DropDownList ddlApplyAs = (DropDownList)e.Item.FindControl("ddlApplyAs");
			if(ddlApplyAs != null)
			{
				DataTable dtApplyAs = new DataTable();
				dtApplyAs.Columns.Add(new DataColumn("Text", typeof(string)));
				dtApplyAs.Columns.Add(new DataColumn("StringValue", typeof(string)));

				ArrayList listApplyAs = Utility.GetCodeValues(utility.GetAppID(),m_strCulture,"apply_as_type",CodeValueType.StringValue);
				foreach(SystemCode typeSysCode in listApplyAs)
				{
					DataRow drEach = dtApplyAs.NewRow();
					drEach[0] = typeSysCode.Text;
					drEach[1] = typeSysCode.StringValue;
					dtApplyAs.Rows.Add(drEach);
				}
				DataView dvApplyAs = new DataView(dtApplyAs);
				ddlApplyAs.DataSource =  dvApplyAs;
				ddlApplyAs.DataTextField =  "Text";
				ddlApplyAs.DataValueField =	 "StringValue";
				ddlApplyAs.DataBind();
				ddlApplyAs.ClearSelection();
				ddlApplyAs.Items.FindByValue("A").Selected = true;

			}

		}


		public void dgEditDtl_Button(object sender, DataGridCommandEventArgs e)
		{
			
		}


		public void dgPreviewDtl_Button(object sender, DataGridCommandEventArgs e)
		{

		}


		public void dgPreviewDtl_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			

			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			String invStatus = "";
			if(Utility.IsNotDBNull(ViewState["INVSTATE"]))
			{
				invStatus = (string)ViewState["INVSTATE"];
			}

//			if(this.UserRole == "ACCT" || invStatus == "N" )
//			{
//				dgPreviewDtl.Columns[0].Visible = true;
//			}
//
//			if(this.UserRole == "ACCTMGR" || invStatus != "N")
//			{
//				dgPreviewDtl.Columns[0].Visible = false;	
//			}

			if(invStatus == "N" )
			{
				dgPreviewDtl.Columns[0].Visible = true;
			}

			else if (invStatus != "N")
			{
				dgPreviewDtl.Columns[0].Visible = false;	
			}
			

		}


	

		private decimal SumAmountInvDtl()
		{ 
			decimal tempSum = Convert.ToDecimal(0);
			tempSum += Convert.ToDecimal(ViewState["temp_tot_freight"]);
			tempSum += Convert.ToDecimal(ViewState["temp_insurance_amt"]);
			tempSum += Convert.ToDecimal(ViewState["temp_other_surcharge"]);
			tempSum += Convert.ToDecimal(ViewState["temp_esa_surcharge"]);
			tempSum += Convert.ToDecimal(ViewState["temp_total_exception"]);
			tempSum += Convert.ToDecimal(ViewState["temp_mbg_amount"]);
			tempSum += Convert.ToDecimal(ViewState["temp_tot_vas_surcharge"]);

			return tempSum;
		
		}
		
	


	private void btnCancelDtl_Click(object sender, System.EventArgs e)
		{
		
		}



	
	}
}
