<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="DispatchTrackingQueryPage.aspx.cs" AutoEventWireup="false" Inherits="com.ties.DispatchTrackingQueryPage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DispatchTrackingQueryPage</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.opener.top.displayBanner.fnCloseAll(1);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="DispatchTrackingQueryPage" method="post" runat="server">
			<asp:datagrid style="Z-INDEX: 101; POSITION: absolute; TOP: 53px; LEFT: 6px" id="m_dgDispatch"
				runat="server" PageSize="25" HorizontalAlign="Center" OnPageIndexChanged="OnDispatchQuery_PageChange"
				OnItemDataBound="OnDataBound_DispatchQueryPage" AllowPaging="True" AllowCustomPaging="True"
				AutoGenerateColumns="False" OnUpdateCommand="OnUpdate_Dispatch" OnItemCommand="OnEdit_DispatchHistory"
				OnEditCommand="dgDispatch_Edit">
				<ItemStyle Height="20px" CssClass="gridFieldSmall"></ItemStyle>
				<Columns>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-update.gif' border=0  title='Update' &gt;" CommandName="DispatchStatusUpdate">
						<HeaderStyle Width="2%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Booking No">
						<HeaderStyle Width="6%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:LinkButton ID="lnkbtnBookNo" Text='<%#DataBinder.Eval(Container.DataItem,"booking_no")%>' Runat="server" CommandName="DispatchDeletePopUp">
							</asp:LinkButton>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Payer Id">
						<HeaderStyle Width="3%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle Width="3%" CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label Width="3%" CssClass="gridLabelSmall" ID="Label1" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"payerid")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Payment Mode">
						<HeaderStyle Width="3%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle Width="3%" CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label Width="3%" CssClass="gridLabelSmall" ID="Label2" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"payment_mode")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Booking Date/Time">
						<HeaderStyle Width="112px" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle Width="112px" CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label Width="112px" CssClass="gridLabelSmall" ID="lblBookDateTime" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"booking_datetime","{0:dd/MM/yyyy HH:mm}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Sender Name">
						<HeaderStyle Width="6%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblSender" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"sender_name")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Sender Address">
						<HeaderStyle Width="12%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblSenderAdd" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"sender_address")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Status Code">
						<HeaderStyle Width="4%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label id=lblStatus CssClass="gridLabelSmall" Text='<%#DataBinder.Eval(Container.DataItem,"latest_status_code")%>' Runat="server" Visible="True">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Exception Code">
						<HeaderStyle Width="4%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label id=lblException CssClass="gridLabelSmall" Text='<%#DataBinder.Eval(Container.DataItem,"latest_exception_code")%>' Runat="server" Visible="True">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Requested Pickup Date/Time">
						<HeaderStyle Width="112px" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle Width="112px" CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label Width="112px" CssClass="gridLabelSmall" ID="lblPickupDateTime" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"req_pickup_datetime","{0:dd/MM/yyyy HH:mm}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Cash Amount">
						<HeaderStyle Width="3%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblCashAmount" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"cash_amount","{0:n}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Cash Collected">
						<HeaderStyle Width="3%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblCashCollected" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"cash_collected")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Total Pkg">
						<HeaderStyle Width="3%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblTotalPkg" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"tot_pkg")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Total Wt">
						<HeaderStyle Width="3%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelSmall" ID="lblTotalWt" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"tot_wt")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Dispatch Type">
						<HeaderStyle Width="7%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label id=lblDispatchType CssClass="gridLabelSmall" Text='<%#DataBinder.Eval(Container.DataItem,"booking_type")%>' Runat="server" Visible="True">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Pickup Route" Visible="true">
						<HeaderStyle Width="7%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label id=lblPickupRoute CssClass="gridLabelSmall" Text='<%#DataBinder.Eval(Container.DataItem,"pickup_route")%>' Runat="server" Visible="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtPickup_Route" Text='<%#DataBinder.Eval(Container.DataItem,"pickup_route")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" onclick="return false;" onpaste="return false;" onkeydown="return false;" onkeypress="return false;">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Visible="true" Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;"
						HeaderText="Select Pickup Route" CommandName="Pickup_Route_Search">
						<HeaderStyle CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
					</asp:ButtonColumn>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
						HeaderText="Change Pickup Route" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;"
						EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left" CssClass="gridFieldSmall"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:TemplateColumn HeaderText="Remarks">
						<HeaderStyle Width="17%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label id=lblRemark CssClass="gridLabelSmall" Text='<%#DataBinder.Eval(Container.DataItem,"remark")%>' Runat="server" Visible="True">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Last  userid">
						<HeaderStyle Width="7%" CssClass="gridHeadingSmall"></HeaderStyle>
						<ItemStyle CssClass="gridFieldSmall"></ItemStyle>
						<ItemTemplate>
							<asp:Label id="lblLastUserID" CssClass="gridLabelSmall" Text='<%#DataBinder.Eval(Container.DataItem,"last_userid")%>' Runat="server" Visible="True">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:button style="Z-INDEX: 102; POSITION: absolute; TOP: 20px; LEFT: 25px" id="btnClose" runat="server"
				Text="Close" CssClass="queryButton"></asp:button><asp:label style="Z-INDEX: 103; POSITION: absolute; TOP: 12px; LEFT: 93px" id="lblErrorMsg"
				runat="server" CssClass="errorMsgColor" Height="31px" Width="855px"></asp:label><input 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition>
		</form>
	</body>
</HTML>
