<%@ Page language="c#" Codebehind="DispatchStatusUpdate.aspx.cs" AutoEventWireup="false" Inherits="com.ties.DispatchStatusUpdate" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>DispatchStatusUpdate</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
  </HEAD>
	<body MS_POSITIONING="GridLayout" onunload="window.opener.opener.top.displayBanner.fnCloseAll(2);">
		<form id="DispatchStatusUpdate" method="post" runat="server">
			<TABLE id="Table2" style="Z-INDEX: 101; LEFT: 8px; WIDTH: 659px; POSITION: absolute; TOP: 8px; HEIGHT: 113px" cellSpacing="1" cellPadding="1" width="659" border="0">
				<TR>
					<TD style="WIDTH: 249px">
						<asp:label id="lblBookNo" runat="server" CssClass="tableLabel" Width="142px">Booking No</asp:label></TD>
					<TD>
						<asp:label id="lblDisBookNo" runat="server" CssClass="tableField" Width="243px"></asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 249px">
						<asp:label id="lblBookDateTime" runat="server" CssClass="tableLabel" Width="143px">Booking Date/Time</asp:label></TD>
					<TD>
						<asp:label id="lblDisBookDateTime" runat="server" CssClass="tableField" Width="240px"></asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 249px">
						<asp:label id="lblSenderName" runat="server" CssClass="tableLabel" Width="145px">Sender Name</asp:label></TD>
					<TD>
						<asp:label id="lblDisSenderName" runat="server" CssClass="tableField" Width="240px"></asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 249px">
						<asp:label id="lblReqPickupDateTime" runat="server" CssClass="tableLabel" Width="237px">Requested Pickup Date/Time</asp:label></TD>
					<TD>
						<asp:label id="lblDisReqPickupDateTime" runat="server" CssClass="tableField" Width="236px"></asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 249px">
						<asp:Label id="lblActPickupDateTime" runat="server" CssClass="tableLabel">Actual Pickup Date/Time</asp:Label></TD>
					<TD>
						<asp:Label id="lblDisActPickupDateTime" runat="server" CssClass="tableField"></asp:Label></TD>
				</TR>
			</TABLE>
			<TABLE id="Table1" style="Z-INDEX: 102; LEFT: 9px; WIDTH: 695px; POSITION: absolute; TOP: 155px; HEIGHT: 191px" cellSpacing="1" cellPadding="1" width="695" border="0">
				<TR>
					<TD>
						<asp:Label id="lblStatus" CssClass="tableLabel" runat="server">Status Code</asp:Label></TD>
					<TD>
						<asp:TextBox id="txtStatus" runat="server" Width="162px" CssClass="textField"></asp:TextBox>
						<asp:Button id="btnStatusSrch" runat="server" Text="..." CssClass="searchButton"></asp:Button>
						<asp:Label id="lblAstStatusCode" runat="server" Width="18px" CssClass="asteriskProp"></asp:Label></TD>
					<TD>
						<asp:Label id="lblStatusDateTime" CssClass="tableLabel" runat="server" Width="112px">Status Date/Time</asp:Label></TD>
					<TD>
						<cc1:mstextbox id="txtStatusDateTime" runat="server" TextMaskType="msDateTime" MaxLength="16" TextMaskString="99/99/9999 99:99" CssClass="textField" Width="126px"></cc1:mstextbox>
						<asp:Label id="lblAstStatusDT" runat="server" Width="24px" CssClass="asteriskProp"></asp:Label></TD>
				</TR>
				<TR>
					<TD>
						<asp:Label id="lblException" CssClass="tableLabel" runat="server" Width="96px">Exception Code</asp:Label></TD>
					<TD>
						<asp:TextBox id="txtException" runat="server" Width="161px" CssClass="textField"></asp:TextBox>
						<asp:Button id="btnExceptionSrch" runat="server" Text="..." CssClass="searchButton"></asp:Button></TD>
					<TD>
						<asp:Label id="lblPersonInCharge" CssClass="tableLabel" runat="server" Width="104px">Person In-Charge</asp:Label></TD>
					<TD>
						<asp:TextBox id="txtPersonInCharge" runat="server" CssClass="textField" Width="126px"></asp:TextBox>
						<asp:Label id="lblAstPerson" runat="server" Width="18px" CssClass="asteriskProp" Height="6px"></asp:Label></TD>
				</TR>
				<TR>
					<TD vAlign="top" align="left">
						<asp:Label id="lblRemarks" CssClass="tableLabel" runat="server">Remarks</asp:Label></TD>
					<TD>
						<asp:TextBox id="txtRemarks" runat="server" Height="95px" TextMode="MultiLine" Width="181px" CssClass="textField"></asp:TextBox></TD>
					<TD></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD align="middle" colSpan="4">
						<asp:Button id="btnSave" runat="server" Width="70px" Text="Save" CssClass="queryButton"></asp:Button>
						<asp:Button id="btnCancel" runat="server" Width="70px" Text="Cancel" CssClass="queryButton"></asp:Button></TD>
				</TR>
			</TABLE>
			<asp:Label id="lblErrMsg" style="Z-INDEX: 103; LEFT: 16px; POSITION: absolute; TOP: 132px" runat="server" Width="639px" CssClass="errorMsgColor" Height="16px"></asp:Label>
		</form>
	</body>
</HTML>
