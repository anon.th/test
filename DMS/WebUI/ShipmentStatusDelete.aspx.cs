using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for ShipmentStatusDelete.
	/// </summary>
	public class ShipmentStatusDelete : BasePopupPage
	{
		private SessionDS m_sdsShipmentDelete = null;
		private String m_strAppID;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		private String m_strEnterpriseID;
		protected String strDelete;
		protected String strPerson;
		protected String strDT;
		protected String strStatus;
		protected String strStatusDSC;
		protected String strExcep;
		protected String strExcepDSC;
		protected String strRemarks;
		protected String strLocation;
		protected String strRemarksDel;
		protected String strPersonInchrage;
		protected String strUserUpdated;
		protected System.Web.UI.WebControls.DataGrid dgShipmentDelete;
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.WebControls.Label lblBookNo;
		protected System.Web.UI.WebControls.Label lblDisBookNo;
		protected System.Web.UI.WebControls.Label lblConsgNo;
		protected System.Web.UI.WebControls.Label lblDispConsgNo;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label lblDispRefNo;
		protected System.Web.UI.WebControls.Label lblBookDateTime;
		protected System.Web.UI.WebControls.Label lblDisBookDateTime;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblDisEstDateTime;
		protected System.Web.UI.WebControls.Label lblShipmentType;
		protected System.Web.UI.WebControls.Label lblDispShipmentType;
		protected System.Web.UI.WebControls.Label lblOrigin;
		protected System.Web.UI.WebControls.Label lblDispOrigin;
		protected System.Web.UI.WebControls.Label lblDestination;
		protected System.Web.UI.WebControls.Label lblDispDestination;
		protected System.Web.UI.WebControls.Label lblPayerCode;
		protected System.Web.UI.WebControls.Label lblDispPayercode;
		protected System.Web.UI.WebControls.Label lblPayerName;
		protected System.Web.UI.WebControls.Label lblDispPayerName;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.HtmlControls.HtmlTable Table3;
		protected String strUpdateDT;
		protected String m_strUserID;
		protected System.Web.UI.WebControls.Button btnMBG;
		protected System.Web.UI.WebControls.CheckBox chkExpend;
		protected String strUserRole;
	

		public String UserRole
		{
			get{return strUserRole;}
			set{strUserRole = value;}
		}
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
		//	string strPath = this.Request.Url.AbsoluteUri.Replace("ShipmentStatusDelete","MBGProcessingForm.aspx?MODID=05MBG&PARENTMODID=04CustomerService&PopupType=True");
			
		//	btnMBG.Attributes.Add("onclick", "javascript:window.open('" + strPath + "','','height=500,width=850,left=120,top=85,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=yes,toolbar=no')");

			

			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strUserID =  utility.GetUserID();
			int rtnPackage; 


			if(TIESDAL.PostDeliveryCOD.GetUserRole(m_strAppID,m_strEnterpriseID,m_strUserID,"DEOMANAGER")==1)
			{
				this.UserRole = "DEOMANAGER";
			}
			else
			{
				this.UserRole = "";
			}

			if(TIESDAL.PostDeliveryCOD.GetUserRole(m_strAppID,m_strEnterpriseID,m_strUserID,"CLAIMMGR")==1)
			{
				btnMBG.Enabled=true;
			}
			else
			{
				btnMBG.Enabled=false;
			}


			strDT = Utility.GetLanguageText(ResourceType.ScreenLabel,"Status Date/Time",utility.GetUserCulture());
			strDelete = Utility.GetLanguageText(ResourceType.ScreenLabel,"Delete ",utility.GetUserCulture());
			strStatus = Utility.GetLanguageText(ResourceType.ScreenLabel,"Status Code",utility.GetUserCulture());
			strStatusDSC = Utility.GetLanguageText(ResourceType.ScreenLabel,"Status Description",utility.GetUserCulture());
			strExcep = Utility.GetLanguageText(ResourceType.ScreenLabel,"Exception Code",utility.GetUserCulture());
			strExcepDSC = Utility.GetLanguageText(ResourceType.ScreenLabel,"Exception Description",utility.GetUserCulture());
			strRemarks = Utility.GetLanguageText(ResourceType.ScreenLabel,"Remarks",utility.GetUserCulture());
			strLocation = Utility.GetLanguageText(ResourceType.ScreenLabel,"Location",utility.GetUserCulture());
			strRemarksDel = Utility.GetLanguageText(ResourceType.ScreenLabel,"Remarks Delete",utility.GetUserCulture());
			strPersonInchrage = Utility.GetLanguageText(ResourceType.ScreenLabel,"Person Responsible",utility.GetUserCulture());
			strUpdateDT = Utility.GetLanguageText(ResourceType.ScreenLabel,"Updated D/T",utility.GetUserCulture());
			strUserUpdated = Utility.GetLanguageText(ResourceType.ScreenLabel,"User Updated",utility.GetUserCulture());



			if(!Page.IsPostBack)
			{
				if((bool)Session["toRefresh"])
				{
//					m_sdsShipmentDelete = (SessionDS)Session["SESSION_DS2"];
//					displayInfo();
//					BindShipment();
					//
					dgShipmentDelete.CurrentPageIndex = (int)Session["DELETE_PAGE_NO"];
					String strBookNo = (String)Session["FORMID"];
					try
					{
						int tmpBookNo = Convert.ToInt32(strBookNo);
					}
					catch
					{
						strBookNo=null;
					}
					String strConsgNo = Request.Params["CONSG_NO"];
					int iStartIndex = dgShipmentDelete.CurrentPageIndex * dgShipmentDelete.PageSize;
					//Modified by GwanG on 18Feb08
					//m_sdsShipmentDelete = ShipmentTrackingMgrDAL.ShipmentForDeletion(m_strAppID, m_strEnterpriseID, strBookNo, strConsgNo, iStartIndex, dgShipmentDelete.PageSize );
					rtnPackage = ShipmentTrackingMgrDAL.GetEnterpriseConfigurationsReturnPackages(m_strAppID, m_strEnterpriseID);
					m_sdsShipmentDelete = ShipmentTrackingMgrDAL.ShipmentForDeletion_New(m_strAppID, m_strEnterpriseID, strBookNo, strConsgNo,rtnPackage);
					displayInfo();
					BindShipment();
					RefreshQueryPage();
				}
				else
				{
					Session["DELETE_PAGE_NO"]=0;
					String strBookNo = (String)Session["FORMID"];
					String strConsgNo = Request.Params["CONSG_NO"];
					dgShipmentDelete.CurrentPageIndex = (int)Session["DELETE_PAGE_NO"];
					try
					{
						int tmpBookNo = Convert.ToInt32(strBookNo);
					}
					catch
					{
						strBookNo=null;
					}

					int iStartIndex = dgShipmentDelete.CurrentPageIndex * dgShipmentDelete.PageSize;
					//Modified by GwanG on 18Feb08
					//m_sdsShipmentDelete = ShipmentTrackingMgrDAL.ShipmentForDeletion(m_strAppID, m_strEnterpriseID, strBookNo, strConsgNo, iStartIndex, dgShipmentDelete.PageSize );
					rtnPackage = ShipmentTrackingMgrDAL.GetEnterpriseConfigurationsReturnPackages(m_strAppID, m_strEnterpriseID);				
					m_sdsShipmentDelete = ShipmentTrackingMgrDAL.ShipmentForDeletion_New(m_strAppID, m_strEnterpriseID, strBookNo, strConsgNo,rtnPackage);
					//m_sdsShipmentDelete = ShipmentTrackingMgrDAL.ShipmentForDeletion(m_strAppID, m_strEnterpriseID, strBookNo, strConsgNo);

					displayInfo();
					BindShipment();
				}
				if(Request.Params["FORMID"]!=null && Request.Params["FORMID"].ToString()=="ViewVariance")
				{
					dgShipmentDelete.Columns[0].Visible=false;
				}
			}
			else
			{
				m_sdsShipmentDelete = (SessionDS)Session["SESSION_DS2"];
			}
			lblErrorMsg.Text="";

			string [] a = this.Request.Url.AbsoluteUri.ToString().Split(new Char [] {'?'});
			

			string strPath = a[0].Replace("ShipmentStatusDelete.aspx","MBGProcessingForm.aspx?MODID=05MBG&PARENTMODID=04CustomerService&PopupType=True&Consignment=" +lblDispConsgNo.Text.Trim() +"");
			
			btnMBG.Attributes.Add("onclick", "javascript:window.open('" + strPath + "','','height=500,width=850,left=120,top=85,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=yes,toolbar=no')");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgShipmentDelete.SelectedIndexChanged += new System.EventHandler(this.dgShipmentDelete_SelectedIndexChanged);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.chkExpend.CheckedChanged += new System.EventHandler(this.chkExpend_CheckedChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openLastChildWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void displayInfo()
		{
			//int rowIndex = int.Parse(Request.Params["INFO_INDEX"]);
				try
				{
					DataRow dr=m_sdsShipmentDelete.ds.Tables[0].Rows[0];						
					
					lblDisBookNo.Text = dr["booking_no"].ToString();
					lblDispConsgNo.Text = dr["consignment_no"].ToString();
					if(dr["booking_datetime"]!=System.DBNull.Value && dr["booking_datetime"].ToString()!="")
					{
						DateTime dtBookDateTime = (DateTime)dr["booking_datetime"];
						//lblDisBookDateTime.Text = dr["booking_datetime"].ToString();
						lblDisBookDateTime.Text = dtBookDateTime.ToString("dd/MM/yyyy HH:mm");

					}
					if(dr["est_delivery_datetime"]!=System.DBNull.Value && dr["est_delivery_datetime"].ToString()!="")
					{
						DateTime dtEstDateTime = (DateTime)dr["est_delivery_datetime"];
						//lblDisBookDateTime.Text = dr["booking_datetime"].ToString();
						lblDisEstDateTime.Text = dtEstDateTime.ToString("dd/MM/yyyy HH:mm");

					}
					lblDispRefNo.Text = dr["ref_no"].ToString();
					lblDispPayercode.Text = dr["payerid"].ToString();
					lblDispShipmentType.Text = dr["service_code"].ToString();
					lblDispOrigin.Text = dr["origin_state_code"].ToString();
					lblDispDestination.Text = dr["destination_state_code"].ToString();
					lblDispPayerName.Text = dr["payer_name"].ToString();

					int rtnPackage = ShipmentTrackingMgrDAL.GetEnterpriseConfigurationsReturnPackages(m_strAppID, m_strEnterpriseID);				
					if(rtnPackage == 1)
					{
						chkExpend.Checked =true;
					}
					else 
					{
						chkExpend.Checked =false;
					}
				}
				catch(Exception ex)
				{
					Logger.LogTraceError("ShipmentStatusDeletion","Error01","displayInfo",ex.Message);
				}
		}
	
		private void BindShipment()
		{
			dgShipmentDelete.VirtualItemCount = System.Convert.ToInt32(m_sdsShipmentDelete.QueryResultMaxSize);
			dgShipmentDelete.DataSource = m_sdsShipmentDelete.ds.Tables[1];
			dgShipmentDelete.DataBind();
			Session["SESSION_DS2"] = m_sdsShipmentDelete;
		}

		protected bool isINVA()
		{
			bool blINVA = false;

			foreach(DataGridItem dgItem in this.dgShipmentDelete.Items)
			{
				Label lblStatusCode = (Label)dgItem.FindControl("lblStatusCode");

				if((lblStatusCode != null) && (!lblStatusCode.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblStatusCode.Text.Trim() != ""))
				{
					if (lblStatusCode.Text.Trim().Equals("INVA"))
					{
						blINVA = true;
					}
				}
			}
			return blINVA;
		}

		protected void OnDataBound_ShipmentDeletion(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}
			DataRow dr = m_sdsShipmentDelete.ds.Tables[1].Rows[e.Item.ItemIndex];
			
			LinkButton btnDelete = (LinkButton)e.Item.Cells[0].Controls[0];
			if(dr["tracking_datetime2"].ToString() == "")
			{
				btnDelete.Visible = false;
				//((System.Web.UI.WebControls.ButtonColumn)(e.Item.Cells[0].Controls[0] as ButtonColumn)).Visible = true;
			}
			else
			{
				btnDelete.Visible = true;
				//((System.Web.UI.WebControls.ButtonColumn)(e.Item.Cells[0].Controls[0] as ButtonColumn)).Visible = false;
			}


			String strDeleted = dr["deleted"].ToString();
			CheckBox chkboxDeleted = (CheckBox)e.Item.FindControl("chkboxDeleted");
			if(strDeleted=="y" || strDeleted=="Y" && chkboxDeleted!=null)
			{				
				chkboxDeleted.Checked = true;
				e.Item.Cells[0].Enabled = false;			
			}
			else
			{
				chkboxDeleted.Checked = false;
			}
			Label lblStatusCode = (Label)e.Item.FindControl("lblStatusCode");
			String strDefaultStatus = TIESUtility.getInitialShipmentStatusCode(m_strAppID, m_strEnterpriseID);
			if(lblStatusCode.Text == strDefaultStatus)
			{
				e.Item.Cells[0].Enabled = false;
			}
			String strStatusCode = dr["status_code"].ToString();
			
			Label lblStatusDesc = (Label)e.Item.FindControl("lblStatusDesc");			
			Label lblConsgnee = (Label)e.Item.FindControl("lblConsgnee");
			String strConsgnee = dr["consignee_name"].ToString();
			if(strStatusCode.Trim() == "")
			{
				lblStatusCode.Visible = false;
				lblStatusDesc.Visible = false;
			}
			else
			{
				if(strStatusCode == "POD" && (strConsgnee.Trim() != "" && strConsgnee != null) )
				{
					lblConsgnee.Visible = true;
				}
				else
				{
					lblConsgnee.Visible = false;
				}
				lblStatusCode.Visible = true;
				lblStatusDesc.Visible = true;
			}

			Label lblExcepCode = (Label)e.Item.FindControl("lblExcepCode");
			Label lblExcepDesc = (Label)e.Item.FindControl("lblExcepDesc");
			String strExcepCode = dr["exception_code"].ToString();
			if(strExcepCode.Trim() == "")
			{
				lblExcepCode.Visible = false;
				lblExcepDesc.Visible = false;
			}
			else
			{
				lblExcepCode.Visible = true;
				lblExcepDesc.Visible = true;
			}

			Label lblRemk = (Label)e.Item.FindControl("lblRemk");
			String strRemk = dr["remarks"].ToString();
			if(strRemk.Trim() == "")
			{
				lblRemk.Visible = false;
			}
			else
			{
				lblRemk.Visible = true;
			}

			Label lblRemkDel = (Label)e.Item.FindControl("lblRemkDel");
			String strRemkDel = dr["delete_remark"].ToString();
			if(strRemkDel.Trim() == "")
			{
				lblRemkDel.Visible = false;
			}
			else
			{
				lblRemkDel.Visible = true;
			}

			Label lblLocation = (Label)e.Item.FindControl("lblLocation");
			String strLoc = dr["location"].ToString();
			if(strLoc.Trim() == "")
			{
				lblLocation.Visible = false;
			}
			else
			{
				lblLocation.Visible = true;
			}
		}

		protected void OnDelete_ShipmentHistory(object sender, DataGridCommandEventArgs e)
		{
			
			if(e.Item.Cells[0].Enabled ==false)
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"NO_DEL_POSSIBLE",utility.GetUserCulture());
				return;
			}
			//m_sdsDispatchDelete = (SessionDS)Session["SESSION_DS2"];
			String strBookDateTime = Request.Params["BOOK_DATE"];
			//String strLastDateTime = Request.Params["LAST_DATE"];
			Label lblStatusDateTime = (Label)e.Item.FindControl("lblStatusDateTime");
			String strStatusDateTime = lblStatusDateTime.Text;
			Label lblStatusCode=(Label)e.Item.FindControl("lblStatusCode");
			String strStatusCode=lblStatusCode.Text.Trim();
			//Check POD STATUS
			if(ShipmentTrackingMgrDAL.chkPODStatusHasPODEX(m_strAppID,m_strEnterpriseID,lblDisBookNo.Text.Trim(),lblDispConsgNo.Text.Trim(),strStatusCode))
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage," ERR_DEL_SHP_TRCK",utility.GetUserCulture());
				return;
			}
			

			if(!UserRole.Equals("DEOMANAGER") && (isINVA() == true))
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"NO_DEL_POSSIBLE",utility.GetUserCulture());
				return;
			}
			else
			{
				int num = e.Item.ItemIndex - 1;
				OpenWindowpage("DeleteHistoryConfirmPopup.aspx?FORM_ID=ShipmentStatusDelete.aspx&BOOKDATE="+strBookDateTime+"&STATUSDATE="+strStatusDateTime+"&ROWNUM="+num+"&STATUSCODE="+strStatusCode+"&CONSIGNMENT="+lblDispConsgNo.Text.Trim());
			}
		}

		protected void OnShipmentDelete_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			
			dgShipmentDelete.CurrentPageIndex = e.NewPageIndex;
			String strBookNo = (String)Session["FORMID"];
			String strConsgNo = Request.Params["CONSG_NO"];
			int iStartIndex = dgShipmentDelete.CurrentPageIndex * dgShipmentDelete.PageSize;
			Session["DELETE_PAGE_NO"]=dgShipmentDelete.CurrentPageIndex;
			m_sdsShipmentDelete = ShipmentTrackingMgrDAL.ShipmentForDeletion(m_strAppID, m_strEnterpriseID, strBookNo, strConsgNo, iStartIndex, dgShipmentDelete.PageSize );
			BindShipment();
		}

//		private void OpenWindowpage(String strUrl)
//		{
//			ArrayList paramList = new ArrayList();
//			paramList.Add(strUrl);
//			String sScript = Utility.GetScript("openLastChildWindow.js",paramList);
//			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
//		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.opener;" ;
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void dgShipmentDelete_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}
		private void RefreshQueryPage()
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "var sURL = unescape(window.opener.location.pathname); ";
			sScript += "  window.opener.location.replace(sURL);" ;
			//sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void chkExpend_CheckedChanged(object sender, System.EventArgs e)
		{
			string strBookNo = (String)Session["FORMID"];
			string strConsgNo = Request.Params["CONSG_NO"];
			dgShipmentDelete.CurrentPageIndex = (int)Session["DELETE_PAGE_NO"];
			try
			{
				int tmpBookNo = Convert.ToInt32(strBookNo);
			}
			catch
			{
				strBookNo=null;
			}

			if(chkExpend.Checked == true)
			{
				m_sdsShipmentDelete = ShipmentTrackingMgrDAL.ShipmentForDeletion_New(m_strAppID, m_strEnterpriseID, strBookNo, strConsgNo,1);
				displayInfo();
				BindShipment();
				chkExpend.Checked = true;
			}
			else
			{
				m_sdsShipmentDelete = ShipmentTrackingMgrDAL.ShipmentForDeletion_New(m_strAppID, m_strEnterpriseID, strBookNo, strConsgNo,0);
				displayInfo();
				BindShipment();
				chkExpend.Checked = false;
			}
		}

//		private void btnMBG_Click(object sender, System.EventArgs e)
//		{
//			OpenWindowpage("MBGProcessingForm.aspx?PopupType=True");
//		}

		
	}
}
