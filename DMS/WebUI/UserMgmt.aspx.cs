using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.RBAC;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.common.applicationpages;

namespace TIES.WebUI
{
    /// <summary>
    /// Summary description for UserProfile.
    /// </summary>
    public class UserMgmt : BasePage
    {
        protected System.Web.UI.WebControls.RequiredFieldValidator txtRoleNameVal;
        protected System.Web.UI.WebControls.Button btnDelete;
        protected System.Web.UI.WebControls.Button btnInsert;
        protected System.Web.UI.WebControls.Button btnGoToFirstPage;
        protected System.Web.UI.WebControls.Button btnPreviousPage;
        protected System.Web.UI.WebControls.TextBox txtCountRec;
        protected System.Web.UI.WebControls.Button btnNextPage;
        protected System.Web.UI.WebControls.Button btnGoToLastPage;
        protected System.Web.UI.WebControls.Button btnSave;
        protected System.Web.UI.WebControls.Button btnQry;
        protected System.Web.UI.WebControls.Button btnExecQry;
        protected System.Web.UI.WebControls.CheckBox chkGrantsIns;
        protected System.Web.UI.WebControls.Label lblInsRec;
        protected System.Web.UI.WebControls.CheckBox chkGrantsUpd;
        protected System.Web.UI.WebControls.Label lblUpdRec;
        protected System.Web.UI.WebControls.CheckBox chkGrantsDel;
        protected System.Web.UI.WebControls.Label lblDelRec;
        protected System.Web.UI.WebControls.Label lblMessages;
        protected System.Web.UI.WebControls.TreeView funcTree;
        protected System.Web.UI.WebControls.Button btnRemove;
        protected System.Web.UI.WebControls.Button btnAdd;
        protected System.Web.UI.WebControls.Button btnRemoveAll;
        protected System.Web.UI.WebControls.Button btnAddAll;
        protected System.Web.UI.WebControls.RequiredFieldValidator txtUsrNmVal;
        private System.Web.UI.WebControls.TreeNode treeNode;
        private System.Web.UI.WebControls.TreeNode tiesNode;
        ArrayList childList = new ArrayList();
        private String appID = null;
        private String enterpriseID = null;
        //Utility   utility = null;
        ArrayList selItemArray = new ArrayList();
        private String strCreatedUserID = null;
        bool isChecked = false;
        private decimal selRoleID = 0;
        private String selUserID = null;
        protected System.Web.UI.HtmlControls.HtmlGenericControl UserProfilePanel;
        protected System.Web.UI.WebControls.Panel Panel1;
        protected System.Web.UI.WebControls.Button btnToCancel;
        protected System.Web.UI.WebControls.Button btnNotToSave;
        protected System.Web.UI.WebControls.Button btnToSaveChanges;
        protected System.Web.UI.WebControls.Label lblConfirmMsg;
        protected System.Web.UI.HtmlControls.HtmlTable tblPermissions;
        protected System.Web.UI.HtmlControls.HtmlTable tblAdditionalRoles;
        protected System.Web.UI.WebControls.Label Label1;
        protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
        protected System.Web.UI.WebControls.Label lblPerGrn;
        protected System.Web.UI.WebControls.Label Label3;
        protected System.Web.UI.WebControls.Label req;
        protected System.Web.UI.WebControls.Label lblUserID;
        protected com.common.util.msTextBox txtUserID;
        protected System.Web.UI.WebControls.Label reqPswd;
        protected System.Web.UI.WebControls.Label lblUserPswd;
        protected System.Web.UI.WebControls.Button btnResetPswd;
        protected System.Web.UI.WebControls.Label reqName;
        protected System.Web.UI.WebControls.Label lblUsrName;
        protected com.common.util.msTextBox txtUserName;
        protected System.Web.UI.WebControls.Label reqCul;
        protected System.Web.UI.WebControls.Label Label2;
        protected System.Web.UI.WebControls.DropDownList ddbCulture;
        protected System.Web.UI.WebControls.RegularExpressionValidator regEmailValidate;
        protected System.Web.UI.WebControls.Label lblEmail;
        protected System.Web.UI.WebControls.TextBox txtEmail;
        protected System.Web.UI.WebControls.Label lblDept;
        protected com.common.util.msTextBox txtDepartment;
        protected System.Web.UI.WebControls.Label lblUserType;
        protected System.Web.UI.WebControls.DropDownList ddbUserType;
        protected System.Web.UI.HtmlControls.HtmlTable tblRoleInfo;
        protected System.Web.UI.WebControls.Label Label4;
        protected System.Web.UI.WebControls.ListBox lbRoleMaster;
        protected System.Web.UI.WebControls.ListBox lbRolesAdded;
        protected System.Web.UI.HtmlControls.HtmlTable tblist;
        protected System.Web.UI.WebControls.Label lblconf;
        protected System.Web.UI.WebControls.Label lblUserLocation;
        protected System.Web.UI.WebControls.DropDownList ddbUserLocation;
        protected System.Web.UI.WebControls.Label lblCustomerAcc;
        protected System.Web.UI.WebControls.DropDownList ddbCustomerAcc;
        protected System.Web.UI.WebControls.Label Label5;
        ArrayList m_QryList = null;

        private void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            //utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
            appID = utility.GetAppID();
            enterpriseID = utility.GetEnterpriseID();
            String m_strUserID = utility.GetUserID();
            //			if (m_strUserID.ToUpper()=="ADMINISTRATOR")
            //			{
            //				txtUserName.Text=m_strEnterpriseID;
            //			}
            //			else
            //			{
            //				txtUserName.Text=m_strEnterpriseID;//get Username either from Config file or Database & Display
            //			}	

            if (!Page.IsPostBack)
            {
                ViewState["isTextChange"] = false;
                ViewState["iTotalUsers"] = 0;
                ViewState["iPageIndex"] = 0;
                ViewState["isRolesDeleted"] = false;
                ViewState["isQueryMode"] = false;
                ViewState["isDirty"] = false;
                ViewState["UMOperation"] = "None";
                ViewState["RemoveModules"] = false;
                ViewState["NoModifyIDU"] = false;
                Session["Password"] = null;

                LoadUserTypeList();
                LoadUserCultureList();
                LoadUserLocation(); //by sittichai
                LoadCustomerAccount(); //by sittichai
                txtUserID.Enabled = true;
                Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete, false, m_moduleAccessRights);
                btnExecQry.Enabled = true;
                btnGoToFirstPage.Enabled = false;
                btnGoToLastPage.Enabled = false;
                btnNextPage.Enabled = false;
                btnPreviousPage.Enabled = false;
                Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                btnQry.Enabled = true;
                Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, false, m_moduleAccessRights);
                btnResetPswd.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Reset Password", utility.GetUserCulture());
                btnResetPswd.Enabled = false;
                btnRemove.Enabled = false;
                btnAdd.Enabled = false;
                btnRemoveAll.Enabled = false;
                btnAddAll.Enabled = false;
                ddbCustomerAcc.Enabled = false;

                ViewState["NextOperation"] = "None";
                //txtUsrPswd.Enabled = true;
                funcTree.Enabled = false;
                chkGrantsDel.Checked = false;
                chkGrantsIns.Checked = false;
                chkGrantsUpd.Checked = false;
                chkGrantsDel.Enabled = false;
                chkGrantsIns.Enabled = false;
                chkGrantsUpd.Enabled = false;
                lblMessages.Text = "";
                UserProfilePanel.Visible = false;

                Module module = new Module();
                module.ModuleId = "root";

                treeNode = BuildModulesTree(module);
                tiesNode = CloneTreeNode(treeNode.ChildNodes[0]);
                funcTree.PathSeparator = '|';
                funcTree.Nodes.AddAt(0, tiesNode);
                //funcTree.CollapseAll();
                funcTree.Attributes.Add("onclick", "postBackByObject()");//addnew for treenode event

                GetDataIntoList();
                setFocusCtrl(txtUserID.ClientID);
            }
            else
            {
                tiesNode = funcTree.Nodes[0];
            }

        }
        public void LoadUserTypeList()
        {
            ListItem defItem = new ListItem();
            if (!utility.GetUserCulture().ToUpper().Equals("EN-US"))
            {
                defItem.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "User Type", utility.GetUserCulture());
            }
            else
            {
                defItem.Text = "User Type";
            }
            defItem.Value = "0";
            ddbUserType.Items.Add(defItem);
            ArrayList systemCodes = Utility.GetCodeValues(appID, utility.GetUserCulture(), "user_type", CodeValueType.StringValue);
            foreach (SystemCode sysCode in systemCodes)
            {
                ListItem lstItem = new ListItem();
                lstItem.Text = sysCode.Text;
                lstItem.Value = sysCode.StringValue;
                ddbUserType.Items.Add(lstItem);
            }
        }

        public void LoadUserCultureList()
        {
            ListItem defItem = new ListItem();
            if (!utility.GetUserCulture().ToUpper().Equals("EN-US"))
            {
                defItem.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "User Culture", utility.GetUserCulture());
            }
            else
            {
                defItem.Text = "User Culture";
            }
            defItem.Value = "";
            ddbCulture.Items.Add(defItem);
            ArrayList systemCodes = Utility.GetCodeValues(appID, utility.GetUserCulture(), "user_culture", CodeValueType.StringValue);
            foreach (SystemCode sysCode in systemCodes)
            {
                ListItem lstItem = new ListItem();
                lstItem.Text = sysCode.Text;
                lstItem.Value = sysCode.StringValue;
                ddbCulture.Items.Add(lstItem);
            }
            ddbCulture.SelectedIndex = 0;

        }

        //by sittichai
        public void LoadUserLocation()
        {
            Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings, HttpContext.Current.Session);
            String strAppID = util.GetAppID();
            String strEnterpriseID = util.GetEnterpriseID();

            DataSet dataset = com.ties.classes.DbComboDAL.UserLocationQuery(strAppID, strEnterpriseID);
            this.ddbUserLocation.DataSource = dataset;
            this.ddbUserLocation.DataBind();

            ListItem defItem = new ListItem();

            defItem.Text = "User Location";

            defItem.Value = "";
            ddbUserLocation.Items.Insert(0, (defItem));

        }

        //by sittichai
        public void LoadCustomerAccount()
        {
            Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings, HttpContext.Current.Session);
            String strAppID = util.GetAppID();
            String strEnterpriseID = util.GetEnterpriseID();

            DataSet dataset = com.ties.classes.DbComboDAL.CustomerAccountQuery(strAppID, strEnterpriseID);
            this.ddbCustomerAcc.DataSource = dataset;
            this.ddbCustomerAcc.DataBind();

            ListItem defItem = new ListItem();

            defItem.Text = "Customer Account";

            defItem.Value = "";
            ddbCustomerAcc.Items.Insert(0, (defItem));
        }

        public TreeNode BuildModulesTree(Module parentModule)
        {
            TreeNode node = new TreeNode();
            if (parentModule.ModuleName != null)
            {
                //node.Text = Utility.GetLanguageText(ResourceType.ModuleName,parentModule.ModuleName,utility.GetUserCulture());

                String strNodeText = "<font face=" + System.Configuration.ConfigurationSettings.AppSettings["treeFontName"] + " size=" + System.Configuration.ConfigurationSettings.AppSettings["treeFontSize"] + ">";
                strNodeText += Utility.GetLanguageText(ResourceType.ModuleName, parentModule.ModuleName, utility.GetUserCulture());
                strNodeText += "</font>";

                node.Text = strNodeText;
            }

            if (parentModule.ModuleIconImage != null)
            {
                node.ImageUrl = parentModule.ModuleIconImage;
            }

            node.ShowCheckBox = true;
            node.Value = "IDU" + ":" + parentModule.ModuleId;
            //node.Value = "IDU" + ":" + parentModule.ModuleId.Replace('/', ' ');
            String moduleID = parentModule.ModuleId;
            childList = (ArrayList)RBACManager.GetCoreModules(appID, moduleID);
            int cnt = childList.Count;

            foreach (Module child in childList)
            {

                TreeNode childNode = BuildModulesTree(child);
                node.SelectAction = TreeNodeSelectAction.Expand;
                node.ChildNodes.Add(childNode);
            }
            return node;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            base.OnInit(e);
            InitializeComponent();

        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
            this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
            this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
            this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
            this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
            this.btnResetPswd.Click += new System.EventHandler(this.btnResetPswd_Click);
            this.ddbCulture.SelectedIndexChanged += new System.EventHandler(this.ddbCulture_SelectedIndexChanged);
            this.txtEmail.TextChanged += new System.EventHandler(this.txtEmail_TextChanged);
            this.ddbUserType.SelectedIndexChanged += new System.EventHandler(this.ddbUserType_SelectedIndexChanged);
            this.chkGrantsIns.CheckedChanged += new System.EventHandler(this.chkGrantsIns_CheckedChanged);
            this.chkGrantsUpd.CheckedChanged += new System.EventHandler(this.chkGrantsUpd_CheckedChanged);
            this.chkGrantsDel.CheckedChanged += new System.EventHandler(this.chkGrantsDel_CheckedChanged);
            this.funcTree.SelectedNodeChanged += new EventHandler(this.funcTree_SelectedNodeChanged);
            this.funcTree.TreeNodeCheckChanged += new TreeNodeEventHandler(this.funcTree_TreeNodeCheckChanged);
            //this.funcTree.SelectedIndexChange += new System.Web.UI.WebControls.SelectEventHandler(this.funcTree_SelectedIndexChange);
            //this.funcTree.Check += new System.Web.UI.WebControls.ClickEventHandler(this.funcTree_SelectedCheckChange);
            this.lbRoleMaster.SelectedIndexChanged += new System.EventHandler(this.lbRoleMaster_SelectedIndexChanged);
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            this.btnAddAll.Click += new System.EventHandler(this.btnAddAll_Click);
            this.btnRemoveAll.Click += new System.EventHandler(this.btnRemoveAll_Click);
            this.btnToSaveChanges.Click += new System.EventHandler(this.btnToSaveChanges_Click);
            this.btnNotToSave.Click += new System.EventHandler(this.btnNotToSave_Click);
            this.btnToCancel.Click += new System.EventHandler(this.btnToCancel_Click);
            this.ID = "UserMgmt";
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        private void funcTree_SelectedNodeChanged(Object sender, EventArgs e)
        {
            String strParentNodeValue = null;
            String strSelectedTreeNode = null;
            String strIndex = funcTree.SelectedNode.ValuePath;

            TreeNodeCollection nodeCollection = funcTree.FindNode(strIndex).ChildNodes;
            int nodeCnt = nodeCollection.Count;

            if (nodeCnt > 0)
            {
                chkGrantsDel.Checked = false;
                chkGrantsIns.Checked = false;
                chkGrantsUpd.Checked = false;
                chkGrantsDel.Enabled = false;
                chkGrantsIns.Enabled = false;
                chkGrantsUpd.Enabled = false;
            }
            else if (nodeCnt == 0)
            {
                chkGrantsDel.Enabled = true;
                chkGrantsIns.Enabled = true;
                chkGrantsUpd.Enabled = true;
                strSelectedTreeNode = funcTree.FindNode(strIndex).Value;
                if (strIndex.Equals(funcTree.Nodes[0].ValuePath))
                {
                    strParentNodeValue = "root";
                }
                else
                {
                    TreeNode parentNode = funcTree.FindNode(strIndex).Parent;
                    strParentNodeValue = parentNode.Value;
                }

                String strModuleRights = strSelectedTreeNode.Substring(0, strSelectedTreeNode.IndexOf(":"));
                strModuleRights = strModuleRights.Substring(0, (strModuleRights.Length));

                try
                {
                    if ((strModuleRights.IndexOf("I")) >= 0)
                    {
                        chkGrantsIns.Checked = true;
                    }
                    else
                    {
                        chkGrantsIns.Checked = false;
                    }

                }
                catch (Exception indexOutOfBoundExp)
                {
                    String msg = indexOutOfBoundExp.ToString();
                    chkGrantsIns.Checked = false;
                }

                try
                {
                    if ((strModuleRights.IndexOf("U")) >= 0)
                    {
                        chkGrantsUpd.Checked = true;
                    }
                    else
                    {
                        chkGrantsUpd.Checked = false;
                    }

                }
                catch (Exception indexOutOfBoundUExp)
                {
                    String msg = indexOutOfBoundUExp.ToString();
                    chkGrantsUpd.Checked = false;
                }

                try
                {
                    if ((strModuleRights.IndexOf("D")) >= 0)
                    {
                        chkGrantsDel.Checked = true;
                    }
                    else
                    {
                        chkGrantsDel.Checked = false;
                    }

                }
                catch (Exception indexOutOfBoundDExp)
                {
                    String msg = indexOutOfBoundDExp.ToString();
                    chkGrantsDel.Checked = false;
                }
            }
        }

        private void funcTree_TreeNodeCheckChanged(System.Object sender, TreeNodeEventArgs e)
        {
            lblMessages.Text = "";
            String strIndex = e.Node.ValuePath;
            ViewState["isTextChange"] = true;
            funcTree.Nodes[0].Checked = false;

            //if ((!strIndex.Equals(funcTree.Nodes[0].ValuePath)) && (lbRolesAdded.Items.Count > 0))
            //{
            //    if (funcTree.FindNode(strIndex).Checked == true)
            //    {
            //        funcTree.FindNode(strIndex).Checked = false;
            //    }
            //    else
            //    {
            //        funcTree.FindNode(strIndex).Checked = true;
            //    }
            //    lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ADD_ROLES_MODULES", utility.GetUserCulture());
            //    ViewState["isRolesDeleted"] = true;
            //    UserProfilePanel.Visible = true;
            //    divMain.Visible = false;
            //    lbRoleMaster.Visible = false;
            //    lbRolesAdded.Visible = false;
            //    ddbUserType.Visible = false;
            //    ddbCulture.Visible = false;
            //}

            //if (strIndex.Equals(funcTree.Nodes[0].ValuePath))
            //{
            //    if ((funcTree.FindNode(strIndex).ChildNodes.Count > 0))
            //    {
            //        TreeNodeCollection childNodeCollection = funcTree.FindNode(strIndex).ChildNodes;
            //        foreach (TreeNode treeNode in childNodeCollection)
            //        {
            //            treeNode.Checked = funcTree.FindNode(strIndex).Checked;
            //        }
            //    }
            //    else if (funcTree.FindNode(strIndex).ChildNodes.Count <= 0)
            //    {
            //        TreeNode parentNode = (TreeNode)funcTree.FindNode(strIndex).Parent;
            //        TreeNodeCollection treeNodeCollection = parentNode.ChildNodes;
            //        parentNode.Checked = false;
            //        foreach (TreeNode treeNode in treeNodeCollection)
            //        {
            //            if (treeNode.Checked)
            //            {
            //                parentNode.Checked = true;
            //                break;
            //            }
            //        }
            //    }
            //}
            //funcTree.Nodes[0].Checked = false;

            if (funcTree.FindNode(strIndex).ValuePath != funcTree.Nodes[0].ValuePath)
            {
                if ((funcTree.FindNode(strIndex).ChildNodes.Count > 0))//check and uncheck childnode when check parent
                {
                    TreeNodeCollection childNodeCollection = funcTree.FindNode(strIndex).ChildNodes;
                    foreach (TreeNode treeNode in childNodeCollection)
                    {
                        treeNode.Checked = e.Node.Checked;
                    }
                }
                else if (funcTree.FindNode(strIndex).ChildNodes.Count <= 0)//check and uncheck parentnode when check child
                {
                    TreeNode parentNode = funcTree.FindNode(strIndex).Parent;
                    TreeNodeCollection treeNodeCollection = parentNode.ChildNodes;
                    parentNode.Checked = false;
                    foreach (TreeNode treeNode in treeNodeCollection)
                    {
                        if (treeNode.Checked)
                        {
                            parentNode.Checked = true;
                            break;
                        }
                    }
                }
            }

            funcTree.Nodes[0].Checked = false;
        }

        private void ButtonsEnabled(bool isEnable)
        {
            btnAdd.Enabled = isEnable;
            btnAddAll.Enabled = isEnable;
            Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete, isEnable, m_moduleAccessRights);
            btnExecQry.Enabled = isEnable;
            btnGoToFirstPage.Enabled = isEnable;
            btnGoToLastPage.Enabled = isEnable;
            Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, isEnable, m_moduleAccessRights);
            btnNextPage.Enabled = isEnable;
            btnPreviousPage.Enabled = isEnable;
            btnQry.Enabled = isEnable;
            btnRemove.Enabled = isEnable;
            Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, isEnable, m_moduleAccessRights);
        }
        private void GetDataIntoList()
        {
            Role role = new Role();
            role.RoleID = 0;
            ArrayList roleListArray = RBACManager.GetAllRoles(appID, enterpriseID, role);
            if ((roleListArray != null) && (roleListArray.Count > 0))
            {
                foreach (Role allRoles in roleListArray)
                {
                    ListItem listItemVar = new ListItem();

                    listItemVar.Text = allRoles.RoleName;
                    listItemVar.Value = allRoles.RoleID.ToString();
                    lbRoleMaster.Items.Add(listItemVar);
                }

            }

        }

        private void SelectAllNodes(TreeNode rootNode, bool btnFlag)
        {

            rootNode.Checked = btnFlag;
            TreeNodeCollection treeNodeCollection = rootNode.ChildNodes;
            foreach (TreeNode node in treeNodeCollection)
            {
                SelectAllNodes(node, btnFlag);
            }
        }
        private void btnInsert_Click(object sender, System.EventArgs e)
        {
            Insert_Click();
            setFocusCtrl(txtUserID.ClientID);
        }
        private void Insert_Click()
        {
            btnResetPswd.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Create Password", utility.GetUserCulture());
            Session["Password"] = null;

            btnResetPswd.Enabled = true;
            btnRemove.Enabled = true;
            btnRemoveAll.Enabled = true;
            btnAdd.Enabled = true;
            btnAddAll.Enabled = true;

            if ((bool)ViewState["isDirty"] == false)
            {
                if ((bool)ViewState["isTextChange"] == true)
                {
                    lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SAVE_PROMPT", utility.GetUserCulture());
                    UserProfilePanel.Visible = true;
                    divMain.Visible = false;
                    lbRoleMaster.Visible = false;
                    lbRolesAdded.Visible = false;
                    ddbUserType.Visible = false;
                    ddbCulture.Visible = false;
                    ViewState["NextOperation"] = "Insert";
                }
                else
                {
                    ViewState["UMOperation"] = "Insert";
                    ViewState["isQueryMode"] = false;
                    lblMessages.Text = "";
                    Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, true, m_moduleAccessRights);
                    btnExecQry.Enabled = false;
                    Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete, false, m_moduleAccessRights);
                    funcTree.Enabled = true;
                    SelectAllNodes(tiesNode, false);
                    txtUserID.Text = "";
                    txtUserID.Enabled = true;
                    txtDepartment.Text = "";
                    txtEmail.Text = "";
                    txtUserName.Text = "";
                    ddbUserType.SelectedItem.Selected = false;
                    ddbUserType.Items.FindByValue("0").Selected = true;
                    ddbCulture.SelectedItem.Selected = false;
                    ddbCulture.Items.FindByValue("").Selected = true;
                    ddbCustomerAcc.SelectedItem.Selected = false;           //by sittichai
                    ddbCustomerAcc.Items.FindByValue("").Selected = true;
                    ddbUserLocation.SelectedItem.Selected = false;          //by sittichai
                    ddbUserLocation.Items.FindByValue("").Selected = true;

                    chkGrantsDel.Checked = false;
                    chkGrantsIns.Checked = false;
                    chkGrantsUpd.Checked = false;
                    if (lbRolesAdded.Items.Count > 0)
                    {
                        RemoveAddedRoles();
                    }

                }
            }
            else
            {
                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "CLK_QUERY_INSERT_PROCEED", utility.GetUserCulture());
                Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                ViewState["isDirty"] = false;
            }
        }

        private void btnAdd_Click(object sender, System.EventArgs e)
        {
            lblMessages.Text = "";
            ViewState["isTextChange"] = true;
            ListItemCollection tmpItemCollection = new ListItemCollection();
            int i = 0;
            foreach (ListItem listItem in lbRoleMaster.Items)
            {
                if (listItem.Selected)
                {
                    ListItem lstItem = new ListItem();
                    lstItem.Text = listItem.Text;
                    lstItem.Value = listItem.Value;
                    lbRolesAdded.Items.Add(lstItem);
                    tmpItemCollection.Add(lstItem);
                    decimal iSelRoleID = Convert.ToDecimal(listItem.Value);
                    if ((i == 0) && (lbRolesAdded.Items.Count == 1) && (IsAnyNodeChecked(tiesNode) == true))
                    {
                        ListItemCollection tmpItemCol = new ListItemCollection();

                        foreach (ListItem addedItem in lbRolesAdded.Items)
                        {

                            ListItem addedLstItem = new ListItem();
                            addedLstItem.Text = addedItem.Text;
                            addedLstItem.Value = addedItem.Value;
                            lbRoleMaster.Items.Add(addedLstItem);
                            tmpItemCol.Add(addedLstItem);

                        }

                        foreach (ListItem tmpLstItem in tmpItemCol)
                        {
                            lbRolesAdded.Items.Remove(tmpLstItem);
                        }
                        lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ADD_MODULES_ROLES", utility.GetUserCulture());
                        ViewState["RemoveModules"] = true;
                        ViewState["isRolesDeleted"] = false;
                        UserProfilePanel.Visible = true;
                        divMain.Visible = false;
                        lbRoleMaster.Visible = false;
                        lbRolesAdded.Visible = false;
                        ddbUserType.Visible = false;
                        ddbCulture.Visible = false;
                        return;
                    }
                    SelectAllNodes(tiesNode, iSelRoleID);
                    i++;
                }
            }

            foreach (ListItem lstItem in tmpItemCollection)
            {
                lbRoleMaster.Items.Remove(lstItem);
            }
        }

        private void lbRoleMaster_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            String strRoleID = lbRoleMaster.SelectedItem.Value;
            selItemArray.Add(strRoleID);

        }

        private void UpdateAllNodeData(TreeNode rootNode)
        {
            TreeNodeCollection treeNodeCollection = rootNode.ChildNodes;
            foreach (TreeNode node in treeNodeCollection)
            {
                String strNodeData = node.Value;
                String strID = strNodeData.Substring((strNodeData.IndexOf(":") + 1));
                node.Value = "IDU:" + strID;
                UpdateAllNodeData(node);
            }
        }

        private void RemoveAddedRoles()
        {
            lblMessages.Text = "";
            ListItemCollection tmpItemCollection = new ListItemCollection();

            foreach (ListItem listItem in lbRolesAdded.Items)
            {
                ListItem lstItem = new ListItem();
                lstItem.Text = listItem.Text;
                lstItem.Value = listItem.Value;
                lbRoleMaster.Items.Add(lstItem);
                tmpItemCollection.Add(lstItem);
            }

            foreach (ListItem lstItem in tmpItemCollection)
            {
                lbRolesAdded.Items.Remove(lstItem);
            }

            SelectAllNodes(tiesNode, false);
            //Make all the tree nodedata's module rights to IDU
            UpdateAllNodeData(tiesNode);
            if (lbRolesAdded.Items.Count > 0)
            {
                foreach (ListItem listItem in lbRolesAdded.Items)
                {
                    decimal iSelRoleID = Convert.ToDecimal(listItem.Value);
                    SelectAllNodes(tiesNode, iSelRoleID);
                }
            }
        }

        private void btnRemove_Click(object sender, System.EventArgs e)
        {
            lblMessages.Text = "";
            ViewState["isTextChange"] = true;
            ListItemCollection tmpItemCollection = new ListItemCollection();

            foreach (ListItem listItem in lbRolesAdded.Items)
            {
                if (listItem.Selected)
                {
                    ListItem lstItem = new ListItem();
                    lstItem.Text = listItem.Text;
                    lstItem.Value = listItem.Value;
                    lbRoleMaster.Items.Add(lstItem);
                    tmpItemCollection.Add(lstItem);
                }
            }

            foreach (ListItem lstItem in tmpItemCollection)
            {
                lbRolesAdded.Items.Remove(lstItem);
            }

            //Unselect the whole tree & again check it depending on the roles which exists
            SelectAllNodes(tiesNode, false);
            //Make all the tree nodedata to IDU
            UpdateAllNodeData(tiesNode);
            if (lbRolesAdded.Items.Count > 0)
            {
                foreach (ListItem listItem in lbRolesAdded.Items)
                {
                    decimal iSelRoleID = Convert.ToDecimal(listItem.Value);
                    SelectAllNodes(tiesNode, iSelRoleID);
                }
            }
        }

        private void btnRemoveAll_Click(object sender, System.EventArgs e)
        {
            ViewState["isTextChange"] = true;
            lblMessages.Text = "";
            SelectAllNodes(tiesNode, false);
            chkGrantsDel.Checked = false;
            chkGrantsIns.Checked = false;
            chkGrantsUpd.Checked = false;

            foreach (ListItem listItem in lbRolesAdded.Items)
            {
                lbRoleMaster.Items.Add(listItem);
                //lbRoleMaster.Items.Add(listItem.Text);
            }
            lbRolesAdded.Items.Clear();

        }

        private void btnAddAll_Click(object sender, System.EventArgs e)
        {
            ViewState["isTextChange"] = true;
            lblMessages.Text = "";
            //			lbRolesAdded.Items.Clear();
            foreach (ListItem listItem in lbRoleMaster.Items)
            {
                //				lbRolesAdded.Items.Add(listItem.Value);
                ListItem lstItem = new ListItem();
                lstItem.Text = listItem.Text;
                lstItem.Value = listItem.Value;
                lbRolesAdded.Items.Add(lstItem);
            }
            lbRoleMaster.Items.Clear();
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            SaveUpdateRecord();
        }
        private void SaveUpdateRecord()
        {
            m_QryList = new ArrayList();
            ArrayList roleList = null;
            bool isCheck = IsAnyNodeChecked(tiesNode);
            lblMessages.Text = "";
            String strRootNode = null;
            String strUsrID = null;
            String strMsg = null;

            int iuserID = txtUserID.Text.Trim().Length;
            strUsrID = txtUserID.Text.Trim();
            com.common.classes.User userAlreadyExists = RBACManager.GetUser(appID, enterpriseID, strUsrID);
            regEmailValidate.Validate();
            if (!regEmailValidate.IsValid)
            {
                lblMessages.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "invalid email", utility.GetUserCulture());
                return;
            }

            // Added 22-Apr-2014
            if (ddbUserLocation.SelectedItem.Value == "")
            {
                lblMessages.Text = "Please select User Location.";
                return;
            }
            // ********

            if ((ddbUserType.SelectedItem.Value) == "0")
            {
                strMsg = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_USER_TYPE", utility.GetUserCulture());
            }
            else if ((ddbCulture.SelectedItem.Value) == "")
            {
                strMsg = Utility.GetLanguageText(ResourceType.UserMessage, "SEL_CULTURE", utility.GetUserCulture());
            }
            else if (!(iuserID > 0))
            {
                strMsg = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MARKED_FIELD", utility.GetUserCulture());
            }
            else if ((Session["Password"] == null) && ((String)ViewState["UMOperation"] == "Insert"))
            {
                strMsg = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_PWD", utility.GetUserCulture());
            }

            else if (txtUserName.Text == null || txtUserName.Text == "")
            {
                strMsg = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MARKED_FIELD", utility.GetUserCulture());
            }
            else if ((lbRolesAdded.Items.Count > 0) && ((ddbUserType.SelectedItem.Value) != "0") && ((ddbCulture.SelectedItem.Value) != "0"))
            {
                String strUserID = null;
                btnQry.Enabled = true;
                Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                funcTree.Enabled = true;
                roleList = new ArrayList();

                //return all the roles in a arraylist
                foreach (ListItem listItem in lbRolesAdded.Items)
                {
                    Role roleObj = new Role();
                    roleObj.RoleID = Convert.ToDecimal(listItem.Value);
                    roleList.Add(roleObj);
                }
                com.common.classes.User user = new com.common.classes.User();
                if (userAlreadyExists == null)
                {
                    user.UserName = txtUserName.Text.Trim();
                    if (Session["Password"] != null)
                    {
                        user.UserPswd = (String)Session["Password"];
                    }
                    user.UserCulture = ddbCulture.SelectedItem.Value.ToString();
                    user.UserID = txtUserID.Text.Trim();
                    user.UserEmail = txtEmail.Text.Trim();
                    user.UserDepartment = txtDepartment.Text.Trim();
                    user.UserType = ddbUserType.SelectedItem.Value.ToString();
                    user.UserLocation = ddbUserLocation.SelectedItem.Value.ToString();  //by sittichai
                    user.CustomerAccount = ddbCustomerAcc.SelectedItem.Value.ToString();  //by sittichai 
                    try
                    {
                        RBACManager.CreateUserNRoles(appID, enterpriseID, user, roleList);
                    }
                    catch (ApplicationException appException)
                    {
                        String strMessage = appException.Message;
                        if (strMessage.IndexOf("duplicate key") != -1)
                        {
                            lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "DUP_KEY_FOUND", utility.GetUserCulture());
                        }
                        else if (strMessage.IndexOf("FK") != -1)
                        {
                            lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "FOREIGN_KEY_FOUND", utility.GetUserCulture());
                        }
                        else
                        {
                            lblMessages.Text = strMsg;
                        }

                        return;
                    }
                    strMsg = Utility.GetLanguageText(ResourceType.UserMessage, "INS_SUCCESSFULLY", utility.GetUserCulture());
                }
                else if (userAlreadyExists != null)
                {
                    strUserID = txtUserID.Text.Trim();
                    com.common.classes.User userObj = new com.common.classes.User();
                    userObj.UserID = strUserID;
                    user.UserID = strUserID;
                    user.UserDepartment = txtDepartment.Text.Trim();
                    user.UserEmail = txtEmail.Text.Trim();
                    user.UserCulture = ddbCulture.SelectedItem.Value.ToString();
                    user.UserName = txtUserName.Text.Trim();
                    user.UserLocation = ddbUserLocation.SelectedItem.Value.ToString();  //by sittichai
                    user.CustomerAccount = ddbCustomerAcc.SelectedItem.Value.ToString();  //by sittichai
                    if (Session["Password"] != null)
                    {
                        user.UserPswd = (String)Session["Password"];
                    }
                    user.UserType = ddbUserType.SelectedItem.Value.ToString();
                    try
                    {
                        RBACManager.ModifyUserNRoles(appID, enterpriseID, strUserID, user, roleList);
                    }
                    catch (ApplicationException appException)
                    {
                        String strMessage = appException.Message;
                        if (strMessage.IndexOf("duplicate key") != -1)
                        {
                            lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "DUP_KEY_FOUND", utility.GetUserCulture());
                        }
                        else if (strMessage.IndexOf("FK") != -1)
                        {
                            lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "FOREIGN_KEY_FOUND", utility.GetUserCulture());
                        }
                        else
                        {
                            lblMessages.Text = strMessage;
                        }

                        return;
                    }

                    Object obj = Session["UserArray"];

                    if (obj != null)
                    {
                        ArrayList userArray = (ArrayList)Session["UserArray"];
                        foreach (com.common.classes.User sesUser in userArray)
                        {
                            if (sesUser.UserID == strUserID)
                            {
                                sesUser.UserID = txtUserID.Text.Trim();
                                sesUser.UserDepartment = txtDepartment.Text.Trim();
                                sesUser.UserEmail = txtEmail.Text.Trim();
                                sesUser.UserName = txtUserName.Text.Trim();
                                break;
                            }
                        }
                    }
                    strMsg = Utility.GetLanguageText(ResourceType.UserMessage, "UPD_SUCCESSFULLY", utility.GetUserCulture());
                }
            }
            else if ((lbRolesAdded.Items.Count == 0) && ((ddbUserType.SelectedItem.Value) != "0") && ((ddbCulture.SelectedItem.Value) != "0"))
            {
                //if (IsAnyNodeChecked(tiesNode))
                if(isCheck)
                {
                    //Modules are assigned to the user directly add record in module_tree_user_relation
                    com.common.classes.User user = new com.common.classes.User();
                    user.UserName = txtUserName.Text.Trim();
                    if (Session["Password"] != null)
                    {
                        user.UserPswd = (String)Session["Password"];
                    }
                    user.UserCulture = ddbCulture.SelectedItem.Value.ToString();
                    user.UserID = txtUserID.Text.Trim();
                    user.UserEmail = txtEmail.Text.Trim();
                    user.UserDepartment = txtDepartment.Text.Trim();
                    user.UserType = ddbUserType.SelectedItem.Value.ToString();
                    user.UserLocation = ddbUserLocation.SelectedItem.Value.ToString();  //by sittichai
                    user.CustomerAccount = ddbCustomerAcc.SelectedItem.Value.ToString();  //by sittichai
                    if (userAlreadyExists == null)
                    {
                        //Add a new user
                        try
                        {
                            strRootNode = tiesNode.Value;
                            strRootNode = strRootNode.Substring((strRootNode.IndexOf(":") + 1));
                            RBACManager.GetQryAddUserNModules(appID, enterpriseID, user, "root", strRootNode, "", ref m_QryList);

                        }
                        catch (ApplicationException appException)
                        {
                            String strMessage = appException.Message;
                            if (strMessage.IndexOf("duplicate key") != -1)
                            {
                                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "DUP_KEY_FOUND", utility.GetUserCulture());
                            }
                            else if (strMessage.IndexOf("FK") != -1)
                            {
                                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "FOREIGN_KEY_FOUND", utility.GetUserCulture());
                            }
                            else
                            {
                                lblMessages.Text = strMsg;
                            }

                            return;
                        }
                        strMsg = Utility.GetLanguageText(ResourceType.UserMessage, "UPD_SUCCESSFULLY", utility.GetUserCulture());
                        ViewState["UMOperation"] = "None";
                        Session["Password"] = null;
                    }
                    else if (userAlreadyExists != null)
                    {
                        try
                        {
                            strRootNode = tiesNode.Value;
                            strRootNode = strRootNode.Substring((strRootNode.IndexOf(":") + 1));
                            RBACManager.GetQryModifyUserNModules(appID, enterpriseID, user, txtUserID.Text.Trim(), "root", strRootNode, "", ref m_QryList);
                        }
                        catch (ApplicationException appException)
                        {
                            String strMessage = appException.Message;
                            if (strMessage.IndexOf("duplicate key") != -1)
                            {
                                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "DUP_KEY_FOUND", utility.GetUserCulture());
                            }
                            else if (strMessage.IndexOf("FK") != -1)
                            {
                                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "FOREIGN_KEY_FOUND", utility.GetUserCulture());
                            }
                            else
                            {
                                lblMessages.Text = strMsg;
                            }

                            return;
                        }
                        Object obj = Session["UserArray"];

                        if (obj != null)
                        {
                            ArrayList userArray = (ArrayList)Session["UserArray"];
                            foreach (com.common.classes.User sesUser in userArray)
                            {
                                if (sesUser.UserID == txtUserID.Text.Trim())
                                {
                                    sesUser.UserID = txtUserID.Text.Trim();
                                    sesUser.UserDepartment = txtDepartment.Text.Trim();
                                    sesUser.UserEmail = txtEmail.Text.Trim();
                                    sesUser.UserName = txtUserName.Text.Trim();
                                    break;
                                }
                            }
                        }
                        strMsg = Utility.GetLanguageText(ResourceType.UserMessage, "UPD_SUCCESSFULLY", utility.GetUserCulture());
                        Session["Password"] = null;
                    }

                    SelectAllNodes(tiesNode, txtUserID.Text.Trim());
                    RBACManager.SaveRecordsInBatch(appID, enterpriseID, ref m_QryList);
                }
                else
                {
                    strMsg = "Assign user role or directly assign module";
                }
            }
            ViewState["isTextChange"] = false;
            lblMessages.Text = strMsg;
        }
        private bool IsAnyNodeChecked(TreeNode rootNode)
        {
            bool isCheck = false;

            TreeNodeCollection treeNodeCollection = rootNode.ChildNodes;

            foreach (TreeNode node in treeNodeCollection)
            {
                isCheck = node.Checked;
                if (isCheck)
                {
                    break;
                }
                IsAnyNodeChecked(node);
            }

            return isCheck;
        }
        private void SelectAllNodes(TreeNode rootNode, String userID)
        {
            strCreatedUserID = userID;
            String strIndex = null;
            String treeNode = null;
            TreeNode parentNode = null;
            String strParentNodeValue = null;
            String strParentID = null, strModRight = null;
            String strChildID = null;
            User user = new User();
            user.UserID = strCreatedUserID;

            TreeNodeCollection treeNodeCollection = rootNode.ChildNodes;

            foreach (TreeNode node in treeNodeCollection)
            {
                isChecked = node.Checked;
                if (isChecked)
                {
                    strIndex = node.ValuePath;
                    treeNode = funcTree.FindNode(strIndex).Value;
                    parentNode = funcTree.FindNode(strIndex).Parent;
                    strParentNodeValue = parentNode.Value;
                    strChildID = treeNode.Substring((treeNode.IndexOf(":") + 1));
                    strModRight = treeNode.Substring(0, (treeNode.IndexOf(":")));
                    strParentID = strParentNodeValue.Substring((strParentNodeValue.IndexOf(":") + 1));
                    RBACManager.GetModulesUserInsertQry(appID, enterpriseID, user, strParentID, strChildID, strModRight, ref m_QryList);
                }
                SelectAllNodes(node, strCreatedUserID);
            }
        }
        private String UnionStrings(String treeRights, String modRights)
        {
            int iLength = treeRights.Length;
            int i = 0;
            for (i = 0; i <= iLength - 1; i++)
            {
                String strChar = treeRights.Substring(i, 1);
                int iPos = modRights.IndexOf(strChar);
                if (iPos == -1)
                {
                    modRights = modRights.Trim() + strChar;
                }
            }
            return modRights;
        }

        private void SelectAllNodes(TreeNode rootNode, decimal roleID)
        {
            selRoleID = roleID;
            String strIndex = null;
            String treeNode = null;
            TreeNode parentNode = null;
            String strParentNodeValue = null;
            String strParentID = null;
            String strModRight = null;
            String strChildID = null;

            TreeNodeCollection treeNodeCollection = rootNode.ChildNodes;

            foreach (TreeNode node in treeNodeCollection)
            {
                strIndex = node.ValuePath;
                treeNode = funcTree.FindNode(strIndex).Value;
                parentNode = funcTree.FindNode(strIndex).Parent;
                strParentNodeValue = parentNode.Value;
                strChildID = treeNode.Substring((treeNode.IndexOf(":") + 1));
                strParentID = strParentNodeValue.Substring((strParentNodeValue.IndexOf(":") + 1));
                ArrayList selRoleList = RBACManager.GetAllModules(appID, enterpriseID, strParentID, strChildID, selRoleID);
                if (selRoleList.Count > 0)
                {
                    String strNodeData = null;
                    Module module = (Module)selRoleList[0];
                    String strSelModRights = module.ModuleRights;
                    String strNode = treeNode.Substring((treeNode.IndexOf(":") + 1));
                    if (node.Checked == true)
                    {
                        strModRight = treeNode.Substring(0, (treeNode.IndexOf(":")));
                        String strRightsUnion = UnionStrings(strModRight, strSelModRights);
                        strNodeData = strRightsUnion.Trim() + ":" + strNode;
                    }
                    else
                    {
                        node.Checked = true;
                        strNodeData = strSelModRights.Trim() + ":" + strNode;
                    }
                    funcTree.FindNode(strIndex).Value = strNodeData;
                }
                SelectAllNodes(node, selRoleID);
            }
        }
        private void UpdateSelectedNodeData(String modRights)
        {
            String strIndex = funcTree.SelectedNode.ValuePath;
            String strSelectedNode = funcTree.SelectedNode.Text;
            String strNode = strSelectedNode.Substring((strSelectedNode.IndexOf(":") + 1));
            String strNodeData = modRights + ":" + strNode;
            funcTree.FindNode(strIndex).Value = strNodeData;
        }
        private void OnCheckedChange()
        {
            bool isUpd = chkGrantsUpd.Checked;
            bool isDel = chkGrantsDel.Checked;
            bool isIns = chkGrantsIns.Checked;

            String strFlag = "";
            if (isUpd)
                strFlag += "U";
            if (isDel)
                strFlag += "D";
            if (isIns)
                strFlag += "I";
            UpdateSelectedNodeData(strFlag);
        }
        private void chkGrantsIns_CheckedChanged(object sender, System.EventArgs e)
        {
            if (lbRolesAdded.Items.Count > 0)
            {
                lblConfirmMsg.Text = "Cannot modify the permission rights of roles. Please modify the permission from Role Management Module";
                ViewState["NoModifyIDU"] = true;
                UserProfilePanel.Visible = true;
                divMain.Visible = false;
                btnNotToSave.Enabled = false;
                btnToSaveChanges.Enabled = false;

                lbRoleMaster.Visible = false;
                lbRolesAdded.Visible = false;
                ddbUserType.Visible = false;
                ddbCulture.Visible = false;


            }
            if (((bool)ViewState["NoModifyIDU"] == true) && (chkGrantsIns.Checked == true))
            {
                chkGrantsIns.Checked = false;
            }
            else if (((bool)ViewState["NoModifyIDU"] == true) && (chkGrantsIns.Checked == false))
            {
                chkGrantsIns.Checked = true;
            }
            else
            {
                OnCheckedChange();
            }
            ViewState["isTextChange"] = true;
            ViewState["NoModifyIDU"] = false;
        }
        private void chkGrantsUpd_CheckedChanged(object sender, System.EventArgs e)
        {
            if (lbRolesAdded.Items.Count > 0)
            {
                lblConfirmMsg.Text = "Cannot modify the permission rights of roles. Please modify the permission from Role Management Module";
                ViewState["NoModifyIDU"] = true;
                UserProfilePanel.Visible = true;
                divMain.Visible = false;
                btnNotToSave.Enabled = false;
                btnToSaveChanges.Enabled = false;

                lbRoleMaster.Visible = false;
                lbRolesAdded.Visible = false;
                ddbUserType.Visible = false;
                ddbCulture.Visible = false;


            }
            if (((bool)ViewState["NoModifyIDU"] == true) && (chkGrantsUpd.Checked == true))
            {
                chkGrantsUpd.Checked = false;
            }
            else if (((bool)ViewState["NoModifyIDU"] == true) && (chkGrantsUpd.Checked == false))
            {
                chkGrantsUpd.Checked = true;
            }
            else
            {
                OnCheckedChange();
            }
            ViewState["isTextChange"] = true;
            ViewState["NoModifyIDU"] = false;
        }
        private void chkGrantsDel_CheckedChanged(object sender, System.EventArgs e)
        {
            if (lbRolesAdded.Items.Count > 0)
            {
                lblConfirmMsg.Text = "Cannot modify the permission rights of roles. Please modify the permission from Role Management Module";
                ViewState["NoModifyIDU"] = true;
                UserProfilePanel.Visible = true;
                divMain.Visible = false;
                btnNotToSave.Enabled = false;
                btnToSaveChanges.Enabled = false;

                lbRoleMaster.Visible = false;
                lbRolesAdded.Visible = false;
                ddbUserType.Visible = false;
                ddbCulture.Visible = false;


            }
            if (((bool)ViewState["NoModifyIDU"] == true) && (chkGrantsDel.Checked == true))
            {
                chkGrantsDel.Checked = false;
            }
            else if (((bool)ViewState["NoModifyIDU"] == true) && (chkGrantsDel.Checked == false))
            {
                chkGrantsDel.Checked = true;
            }
            else
            {
                OnCheckedChange();
            }
            ViewState["isTextChange"] = true;
            ViewState["NoModifyIDU"] = false;
        }
        private void btnQry_Click(object sender, System.EventArgs e)
        {
            Qry_Click();
        }
        private void Qry_Click()
        {
            btnResetPswd.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Reset Password", utility.GetUserCulture());
            Session["Password"] = null;
            btnResetPswd.Enabled = false;
            btnRemove.Enabled = false;
            btnAdd.Enabled = false;
            btnRemoveAll.Enabled = false;
            btnAddAll.Enabled = false;

            if ((bool)ViewState["isDirty"] == false)
            {
                if ((bool)ViewState["isTextChange"] == true)
                {
                    lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SAVE_PROMPT", utility.GetUserCulture());
                    UserProfilePanel.Visible = true;
                    divMain.Visible = false;
                    lbRoleMaster.Visible = false;
                    lbRolesAdded.Visible = false;
                    ddbUserType.Visible = false;
                    ddbCulture.Visible = false;
                    ViewState["NextOperation"] = "Query";
                }
                else
                {
                    ViewState["isQueryMode"] = true;
                    lblMessages.Text = "";
                    btnExecQry.Enabled = true;
                    Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, false, m_moduleAccessRights);
                    Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete, false, m_moduleAccessRights);
                    Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                    btnGoToFirstPage.Enabled = false;
                    btnGoToLastPage.Enabled = false;
                    btnNextPage.Enabled = false;
                    btnPreviousPage.Enabled = false;
                    funcTree.Enabled = false;
                    SelectAllNodes(tiesNode, false);
                    txtDepartment.Text = "";
                    txtEmail.Text = "";
                    txtUserID.Text = "";
                    txtUserName.Text = "";
                    btnResetPswd.Enabled = false;
                    txtUserID.Enabled = true;
                    chkGrantsDel.Checked = false;
                    chkGrantsIns.Checked = false;
                    chkGrantsUpd.Checked = false;
                    ddbUserType.SelectedItem.Selected = false;
                    ddbUserType.Items.FindByValue("0").Selected = true;
                    ddbCulture.SelectedItem.Selected = false;
                    ddbCulture.Items.FindByValue("").Selected = true;
                    ddbUserLocation.SelectedItem.Selected = false;          //by sittichai
                    ddbUserLocation.Items.FindByValue("").Selected = true;
                    ddbCustomerAcc.SelectedItem.Selected = false;           //by sittichai
                    ddbCustomerAcc.Items.FindByValue("").Selected = true;
                    ListItemCollection tmpItemCollection = new ListItemCollection();

                    foreach (ListItem listItem in lbRolesAdded.Items)
                    {
                        ListItem lstItem = new ListItem();
                        lstItem.Text = listItem.Text;
                        lstItem.Value = listItem.Value;
                        lbRoleMaster.Items.Add(lstItem);
                        tmpItemCollection.Add(lstItem);
                    }

                    foreach (ListItem lstItem in tmpItemCollection)
                    {
                        lbRolesAdded.Items.Remove(lstItem);
                    }
                }
            }
            else
            {
                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "CLK_QUERY_INSERT_PROCEED", utility.GetUserCulture());
                btnQry.Enabled = true;
                ViewState["isDirty"] = false;
            }
            setFocusCtrl(txtUserID.ClientID);
        }
        private void btnExecQry_Click(object sender, System.EventArgs e)
        {
            ExecQry_Click();

            // by sittichai
            if (this.ddbUserType.SelectedItem.Value == "C")
            {
                this.ddbCustomerAcc.Enabled = true;
                //this.ddbUserLocation.Enabled=false;
                //this.ddbUserLocation.SelectedIndex = 0;
            }
            else
            {
                this.ddbCustomerAcc.Enabled = false;
                this.ddbUserLocation.Enabled = true;
                this.ddbCustomerAcc.SelectedIndex = 0;
            }

            // Added on 22-Apr-2014
            ValidateCSS_Account();
            //***********************

            setFocusCtrl(txtUserID.ClientID);
        }
        private void ExecQry_Click()
        {
            ViewState["UMOperation"] = "None";
            if (((bool)ViewState["isTextChange"] == true) && ((bool)ViewState["isQueryMode"] == false))
            {
                lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SAVE_PROMPT", utility.GetUserCulture());
                UserProfilePanel.Visible = true;
                divMain.Visible = false;
                lbRoleMaster.Visible = false;
                lbRolesAdded.Visible = false;
                ddbUserType.Visible = false;
                ddbCulture.Visible = false;
                ViewState["NextOperation"] = "ExecuteQuery";
            }
            lblMessages.Text = "";
            btnExecQry.Enabled = false;
            btnQry.Enabled = true;
            Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, true, m_moduleAccessRights);
            Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete, true, m_moduleAccessRights);
            Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
            btnGoToFirstPage.Enabled = true;
            btnGoToLastPage.Enabled = true;
            btnNextPage.Enabled = true;
            btnPreviousPage.Enabled = true;
            txtUserID.Enabled = false;
            btnRemove.Enabled = true;
            btnAdd.Enabled = true;
            btnRemoveAll.Enabled = true;
            btnAddAll.Enabled = true;

            btnResetPswd.Enabled = true;
            funcTree.Enabled = true;
            com.common.classes.User user = new com.common.classes.User();

            user.UserID = txtUserID.Text.Trim();
            user.UserDepartment = txtDepartment.Text.Trim();
            user.UserEmail = txtEmail.Text.Trim();
            user.UserType = ddbUserType.SelectedItem.Value;
            user.UserCulture = ddbCulture.SelectedItem.Value;
            user.UserLocation = ddbUserLocation.SelectedItem.Value;
            user.CustomerAccount = ddbCustomerAcc.SelectedItem.Value;

            ArrayList userList = (ArrayList)RBACManager.GetAllUsers(appID, enterpriseID, user);
            if ((userList != null) && (userList.Count <= 0))
            {
                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "NO_RECORD_FOUND", utility.GetUserCulture());
                btnGoToFirstPage.Enabled = false;
                btnGoToLastPage.Enabled = false;
                btnNextPage.Enabled = false;
                btnPreviousPage.Enabled = false;
            }
            else if ((userList != null) && (userList.Count > 0))
            {
                lblMessages.Text = "";
                ViewState["iTotalUsers"] = userList.Count;
                Session.Add("UserArray", userList);
                DisplayUsers(0);
                ViewState["iPageIndex"] = 0;

            }
            ViewState["isTextChange"] = false;
        }
        private void DisplayUsers(int indexRole)
        {
            RemoveAddedRoles();
            ArrayList userArray = (ArrayList)Session["UserArray"];

            if (indexRole == -1)
            {
                return;
            }
            if ((userArray != null) && (indexRole <= userArray.Count))
            {
                SelectAllNodes(tiesNode, false);
                com.common.classes.User user = (com.common.classes.User)userArray[indexRole];
                txtUserID.Text = user.UserID;
                txtUserName.Text = user.UserName;
                btnResetPswd.Enabled = true;
                txtDepartment.Text = user.UserDepartment;
                txtEmail.Text = user.UserEmail;
                ddbUserType.SelectedItem.Selected = false;
                ddbUserType.Items.FindByValue(user.UserType).Selected = true;
                ddbCulture.SelectedItem.Selected = false;
                ddbCulture.Items.FindByValue(user.UserCulture).Selected = true;
                ddbUserLocation.SelectedItem.Selected = false;
                ddbUserLocation.Items.FindByValue(user.UserLocation).Selected = true;      //by sittichai
                ddbCustomerAcc.SelectedItem.Selected = false;
                ddbCustomerAcc.Items.FindByValue(user.CustomerAccount).Selected = true;  //by sittichai
                ArrayList userRoleArray = RBACManager.GetAllRoles(appID, enterpriseID, user);
                if ((userRoleArray != null) && (userRoleArray.Count > 0))
                {
                    lblMessages.Text = "";
                    ListItemCollection tmpItemCollection = new ListItemCollection();

                    foreach (Role roleObj in userRoleArray)
                    {
                        ListItem lstItem = new ListItem();
                        lstItem.Value = Convert.ToDecimal(roleObj.RoleID).ToString();
                        lstItem.Text = roleObj.RoleName;
                        lbRolesAdded.Items.Add(lstItem);
                        tmpItemCollection.Add(lstItem);
                        decimal iSelRoleID = Convert.ToDecimal(lstItem.Value);
                        SelectAllNodes(tiesNode, iSelRoleID);
                    }

                    foreach (ListItem lstItem in tmpItemCollection)
                    {
                        lbRoleMaster.Items.Remove(lstItem);
                    }
                }
                //get data from module_tree_user_relation
                SelDisplayedUserRights(tiesNode, txtUserID.Text.Trim());
            }
        }
        private void SelDisplayedUserRights(TreeNode rootNode, String userID)
        {
            selUserID = userID;
            String strIndex = null;
            String treeNode = null;
            TreeNode parentNode = null;
            String strParentNodeValue = null;
            String strParentID = null;
            String strModRight = null;
            String strChildID = null;
            com.common.classes.User user = new com.common.classes.User();
            user.UserID = selUserID;

            TreeNodeCollection treeNodeCollection = rootNode.ChildNodes;
            foreach (TreeNode node in treeNodeCollection)
            {
                strIndex = node.ValuePath;
                treeNode = funcTree.FindNode(strIndex).Value;
                parentNode = (TreeNode)funcTree.FindNode(strIndex).Parent;
                strParentNodeValue = parentNode.Value;
                strChildID = treeNode.Substring((treeNode.IndexOf(":") + 1));
                strParentID = strParentNodeValue.Substring((strParentNodeValue.IndexOf(":") + 1));
                ArrayList selRoleList = RBACManager.GetAllModules(appID, enterpriseID, strParentID, strChildID, selUserID);

                if (selRoleList.Count > 0)
                {
                    String strNodeData = null;
                    Module module = (Module)selRoleList[0];
                    String strSelModRights = module.ModuleRights;
                    String strNode = treeNode.Substring((treeNode.IndexOf(":") + 1));
                    if (node.Checked == true)
                    {
                        strModRight = treeNode.Substring(0, (treeNode.IndexOf(":")));
                        String strRightsUnion = UnionStrings(strModRight, strSelModRights);
                        strNodeData = strRightsUnion.Trim() + ":" + strNode;
                    }
                    else
                    {
                        node.Checked = true;
                        strNodeData = strSelModRights.Trim() + ":" + strNode;
                    }
                    funcTree.FindNode(strIndex).Value = strNodeData;
                }
                SelDisplayedUserRights(node, selUserID);
            }
        }

        private void btnNotToSave_Click(object sender, System.EventArgs e)
        {
            UserProfilePanel.Visible = false;
            divMain.Visible = true;
            lbRoleMaster.Visible = true;
            lbRolesAdded.Visible = true;
            ddbUserType.Visible = true;
            ddbCulture.Visible = true;
            ViewState["isTextChange"] = false;
            if ((String)ViewState["NextOperation"] == "Insert")
            {
                Insert_Click();
            }
            else if ((String)ViewState["NextOperation"] == "ExecuteQuery")
            {
                ExecQry_Click();
            }
            else if ((String)ViewState["NextOperation"] == "Query")
            {
                Qry_Click();
            }
            else if ((String)ViewState["NextOperation"] == "MoveFirst")
            {
                MoveFirstDS();
            }
            else if ((String)ViewState["NextOperation"] == "MoveNext")
            {
                MoveNextDS();
            }
            else if ((String)ViewState["NextOperation"] == "MoveLast")
            {
                MoveLastDS();
            }
            else if ((String)ViewState["NextOperation"] == "MovePrevious")
            {
                MovePreviousDS();
            }

        }

        private void btnToSaveChanges_Click(object sender, System.EventArgs e)
        {
            if ((bool)ViewState["isRolesDeleted"] == true)
            {
                RemoveAddedRoles();
                ViewState["isRolesDeleted"] = false;
            }
            else if ((bool)ViewState["RemoveModules"] == true)
            {
                //Uncheck all the tree nodes.
                //add the selected role to the list
                //check the tree accrding to the modules in the role.
                RemoveModulesAddRole();
                ViewState["RemoveModules"] = false;
            }
            else
            {
                SaveUpdateRecord();
            }
            UserProfilePanel.Visible = false;
            divMain.Visible = true;
            lbRoleMaster.Visible = true;
            lbRolesAdded.Visible = true;
            ddbUserType.Visible = true;
            ddbCulture.Visible = true;
        }
        private void RemoveModulesAddRole()
        {
            //Uncheck the tree nodes
            SelectAllNodes(tiesNode, false);
            //Make all the tree nodedata's module rights to IDU
            UpdateAllNodeData(tiesNode);

            //Add the selected role to the list.
            lblMessages.Text = "";
            ViewState["isTextChange"] = true;
            ListItemCollection tmpItemCollection = new ListItemCollection();
            foreach (ListItem listItem in lbRoleMaster.Items)
            {
                if (listItem.Selected)
                {
                    ListItem lstItem = new ListItem();
                    lstItem.Text = listItem.Text;
                    lstItem.Value = listItem.Value;
                    lbRolesAdded.Items.Add(lstItem);
                    tmpItemCollection.Add(lstItem);
                    decimal iSelRoleID = Convert.ToDecimal(listItem.Value);
                    //Update the nodes according to the roles
                    SelectAllNodes(tiesNode, iSelRoleID);

                }
            }
            foreach (ListItem lstItem in tmpItemCollection)
            {
                lbRoleMaster.Items.Remove(lstItem);
            }
        }
        private void btnToCancel_Click(object sender, System.EventArgs e)
        {
            UserProfilePanel.Visible = false;
            divMain.Visible = true;
            lbRoleMaster.Visible = true;
            lbRolesAdded.Visible = true;
            ddbUserType.Visible = true;
            ddbCulture.Visible = true;
            btnNotToSave.Enabled = true;
            btnToSaveChanges.Enabled = true;
        }

        private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
        {
            MoveFirstDS();
        }
        private void MoveFirstDS()
        {
            if ((bool)ViewState["isTextChange"] == true)
            {
                lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SAVE_PROMPT", utility.GetUserCulture());
                UserProfilePanel.Visible = true;
                divMain.Visible = false;
                lbRoleMaster.Visible = false;
                lbRolesAdded.Visible = false;
                ddbUserType.Visible = false;
                ddbCulture.Visible = false;
                ViewState["NextOperation"] = "MoveFirst";
            }
            RemoveAddedRoles();
            ViewState["iPageIndex"] = 0;
            if ((int)ViewState["iTotalUsers"] > 0)
            {
                DisplayUsers(0);
            }

            if (this.ddbUserType.SelectedItem.Value == "C")
            {
                this.ddbCustomerAcc.Enabled = true;
                //this.ddbUserLocation.Enabled=false;
                //this.ddbUserLocation.SelectedIndex = 0;
            }
            else
            {
                this.ddbCustomerAcc.Enabled = false;
                this.ddbUserLocation.Enabled = true;
                this.ddbCustomerAcc.SelectedIndex = 0;
            }
            ValidateCSS_Account();
        }

        private void btnGoToLastPage_Click(object sender, System.EventArgs e)
        {
            MoveLastDS();
        }
        private void MoveLastDS()
        {
            if ((bool)ViewState["isTextChange"] == true)
            {
                lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SAVE_PROMPT", utility.GetUserCulture());
                UserProfilePanel.Visible = true;
                divMain.Visible = false;
                lbRoleMaster.Visible = false;
                lbRolesAdded.Visible = false;
                ddbUserType.Visible = false;
                ddbCulture.Visible = false;
                ViewState["NextOperation"] = "MoveLast";
            }
            else
            {
                RemoveAddedRoles();
                if ((int)ViewState["iTotalUsers"] > 0)
                {
                    ViewState["iPageIndex"] = ((int)ViewState["iTotalUsers"] - 1);
                    DisplayUsers((int)ViewState["iTotalUsers"] - 1);
                }
            }

            if (this.ddbUserType.SelectedItem.Value == "C")
            {
                this.ddbCustomerAcc.Enabled = true;
                //this.ddbUserLocation.Enabled=false;
                //this.ddbUserLocation.SelectedIndex = 0;
            }
            else
            {
                this.ddbCustomerAcc.Enabled = false;
                this.ddbUserLocation.Enabled = true;
                this.ddbCustomerAcc.SelectedIndex = 0;
            }
            ValidateCSS_Account();
        }

        private void btnPreviousPage_Click(object sender, System.EventArgs e)
        {
            MovePreviousDS();
        }
        private void MovePreviousDS()
        {
            if ((bool)ViewState["isTextChange"] == true)
            {
                lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SAVE_PROMPT", utility.GetUserCulture());
                UserProfilePanel.Visible = true;
                divMain.Visible = false;
                lbRoleMaster.Visible = false;
                lbRolesAdded.Visible = false;
                ddbUserType.Visible = false;
                ddbCulture.Visible = false;
                ViewState["NextOperation"] = "MovePrevious";
            }
            else
            {
                RemoveAddedRoles();
                txtUserID.Enabled = false;
                ViewState["iPageIndex"] = (int)ViewState["iPageIndex"] - 1;
                if ((int)ViewState["iPageIndex"] >= 0)
                {
                    DisplayUsers((int)ViewState["iPageIndex"]);
                }
                else
                {
                    ViewState["iPageIndex"] = 0;
                    DisplayUsers((int)ViewState["iPageIndex"]);
                }
            }
            if (this.ddbUserType.SelectedItem.Value == "C")
            {
                this.ddbCustomerAcc.Enabled = true;
                //this.ddbUserLocation.Enabled=false;
                //this.ddbUserLocation.SelectedIndex = 0;
            }
            else
            {
                this.ddbCustomerAcc.Enabled = false;
                this.ddbUserLocation.Enabled = true;
                this.ddbCustomerAcc.SelectedIndex = 0;
            }
            ValidateCSS_Account();
        }
        private void btnNextPage_Click(object sender, System.EventArgs e)
        {
            MoveNextDS();
        }
        private void MoveNextDS()
        {

            if ((bool)ViewState["isTextChange"] == true)
            {
                lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SAVE_PROMPT", utility.GetUserCulture());
                UserProfilePanel.Visible = true;
                divMain.Visible = false;
                lbRoleMaster.Visible = false;
                lbRolesAdded.Visible = false;
                ddbUserType.Visible = false;
                ddbCulture.Visible = false;
                ViewState["NextOperation"] = "MoveNext";
            }
            else
            {
                RemoveAddedRoles();
                txtUserID.Enabled = false;
                ViewState["iPageIndex"] = (int)ViewState["iPageIndex"] + 1;
                if ((int)ViewState["iPageIndex"] == ((int)ViewState["iTotalUsers"]))
                {
                    ViewState["iPageIndex"] = (int)ViewState["iPageIndex"] - 1;
                    DisplayUsers((int)ViewState["iPageIndex"]);
                }
                else if ((int)ViewState["iPageIndex"] < (int)ViewState["iTotalUsers"])
                {
                    DisplayUsers((int)ViewState["iPageIndex"]);
                }
            }
            if (this.ddbUserType.SelectedItem.Value == "C")
            {
                this.ddbCustomerAcc.Enabled = true;
                //this.ddbUserLocation.Enabled=false;
                //this.ddbUserLocation.SelectedIndex = 0;
            }
            else
            {
                this.ddbCustomerAcc.Enabled = false;
                this.ddbUserLocation.Enabled = true;
                this.ddbCustomerAcc.SelectedIndex = 0;
            }

            ValidateCSS_Account();
        }

        private void ValidateCSS_Account()
        {
            if (IsCSS_Account(txtUserID.Text.Trim()))
            {
                this.ddbCustomerAcc.Enabled = false;
            }
        }

        private void btnDelete_Click(object sender, System.EventArgs e)
        {
            lblMessages.Text = "";

            btnResetPswd.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Reset Password", utility.GetUserCulture());
            btnResetPswd.Enabled = false;

            ViewState["isQueryMode"] = false;

            if (txtUserID.Text.Trim().Length > 0)
            {
                try
                {
                    RBACManager.DeleteUser(appID, enterpriseID, txtUserID.Text.Trim());
                }
                catch (ApplicationException appException)
                {
                    lblMessages.Text = appException.Message;
                    return;
                }

                ArrayList userArray = (ArrayList)Session["UserArray"];
                if ((userArray != null) && (userArray.Count > 0))
                {
                    foreach (com.common.classes.User sesUser in userArray)
                    {
                        if (sesUser.UserID == txtUserID.Text.Trim())
                        {
                            userArray.Remove(sesUser);
                            break;
                        }
                    }
                    Session.Add("UserArray", userArray);
                }

                ViewState["iTotalUsers"] = userArray.Count;
                ViewState["iPageIndex"] = (int)ViewState["iPageIndex"] + 1;
                if ((int)ViewState["iTotalUsers"] == (int)ViewState["iPageIndex"])
                {
                    ViewState["iPageIndex"] = (int)ViewState["iPageIndex"] - 1;
                    DisplayUsers((int)ViewState["iPageIndex"]);
                }
                else if ((int)ViewState["iTotalUsers"] > (int)ViewState["iPageIndex"])
                {
                    ViewState["iPageIndex"] = (int)ViewState["iPageIndex"] - 1;
                    DisplayUsers((int)ViewState["iPageIndex"]);
                }
                else if ((int)ViewState["iTotalUsers"] < (int)ViewState["iPageIndex"])
                {
                    ViewState["iPageIndex"] = (int)ViewState["iTotalUsers"] - 1;
                    DisplayUsers((int)ViewState["iPageIndex"]);
                }
                else
                {
                    txtUserID.Text = "";
                    txtUserName.Text = "";
                    btnResetPswd.Enabled = false;
                    txtDepartment.Text = "";
                    txtEmail.Text = "";
                    ddbUserType.SelectedItem.Selected = false;
                    ddbUserType.Items.FindByValue("0").Selected = true;
                    ddbCulture.SelectedItem.Selected = false;
                    ddbCulture.Items.FindByValue("").Selected = true;
                    ddbCustomerAcc.SelectedItem.Selected = false;
                    ddbCustomerAcc.Items.FindByValue("").Selected = true;
                    ddbUserLocation.SelectedItem.Selected = false;
                    ddbUserLocation.Items.FindByValue("").Selected = true;
                    SelectAllNodes(tiesNode, false);
                }
            }
            ViewState["isTextChange"] = false;
            lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "DELETED_SUCCESSFULLY", utility.GetUserCulture());
        }

        private void txtUserID_TextChanged(object sender, System.EventArgs e)
        {

            ViewState["isTextChange"] = true;
            if ((btnExecQry.Enabled == false) && (btnQry.Enabled == true) && (btnInsert.Enabled == true) && (btnSave.Enabled == false) && (btnDelete.Enabled == false))
            {
                ViewState["isTextChange"] = false;
                Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, false, m_moduleAccessRights);
                ViewState["isDirty"] = true;
            }
        }

        private void txtUserName_TextChanged(object sender, System.EventArgs e)
        {
            ViewState["isTextChange"] = true;
            if ((btnExecQry.Enabled == false) && (btnQry.Enabled == true) && (btnInsert.Enabled == true) && (btnSave.Enabled == false) && (btnDelete.Enabled == false))
            {

                ViewState["isTextChange"] = false;
                Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, false, m_moduleAccessRights);
                ViewState["isDirty"] = true;
            }
        }

        private void ddbCulture_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ViewState["isTextChange"] = true;
            if ((btnExecQry.Enabled == false) && (btnQry.Enabled == true) && (btnInsert.Enabled == true) && (btnSave.Enabled == false) && (btnDelete.Enabled == false))
            {
                ViewState["isTextChange"] = false;
                Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, false, m_moduleAccessRights);
                ViewState["isDirty"] = true;
            }
        }

        private void txtEmail_TextChanged(object sender, System.EventArgs e)
        {
            ViewState["isTextChange"] = true;
            if ((btnExecQry.Enabled == false) && (btnQry.Enabled == true) && (btnInsert.Enabled == true) && (btnSave.Enabled == false) && (btnDelete.Enabled == false))
            {
                ViewState["isTextChange"] = false;
                Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, false, m_moduleAccessRights);
                ViewState["isDirty"] = true;
            }
        }

        private void txtDepartment_TextChanged(object sender, System.EventArgs e)
        {
            ViewState["isTextChange"] = true;
            if ((btnExecQry.Enabled == false) && (btnQry.Enabled == true) && (btnInsert.Enabled == true) && (btnSave.Enabled == false) && (btnDelete.Enabled == false))
            {
                ViewState["isTextChange"] = false;
                Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, false, m_moduleAccessRights);
                ViewState["isDirty"] = true;
            }
        }

        private void ddbUserType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ViewState["isTextChange"] = true;

            if ((btnExecQry.Enabled == false) && (btnQry.Enabled == true) && (btnInsert.Enabled == true) && (btnSave.Enabled == false) && (btnDelete.Enabled == false))
            {
                ViewState["isTextChange"] = false;
                Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, false, m_moduleAccessRights);
                ViewState["isDirty"] = true;
            }
            //by sittichai
            if (this.ddbUserType.SelectedItem.Value == "C")
            {
                this.ddbCustomerAcc.Enabled = true;
                //this.ddbUserLocation.Enabled=false;
                //this.ddbUserLocation.SelectedIndex = 0;
                this.ddbUserLocation.Enabled = true;
            }
            else
            {
                this.ddbCustomerAcc.Enabled = false;
                this.ddbUserLocation.Enabled = true;
                this.ddbCustomerAcc.SelectedIndex = 0;
            }
        }

        private void btnResetPswd_Click(object sender, System.EventArgs e)
        {
            String sUrl = "PasswordPopup.aspx";
            ArrayList paramList = new ArrayList();
            paramList.Add(sUrl);
            String sScript = Utility.GetScript("openWindowParam.js", paramList);
            Utility.RegisterScriptString(sScript, "ShowPopupScript", this.Page);
        }

        private void setFocusCtrl(string ctrlName)
        {
            string script = "<script langauge='javacript'>";
            script += "document.all['" + ctrlName + "'].focus();";
            script += "</script>";
            Page.RegisterStartupScript("setFocus", script);

        }

        public bool IsCSS_Account(string userID)
        {
            /*
			 * The user should not be able to change a payer ID for an existing customer login account if there is an entry in the CSS_Accounts table for that userid. 
			 * Disable the dropdown listbox in this case.
			 */
            Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings, HttpContext.Current.Session);
            String strAppID = util.GetAppID();
            String strEnterpriseID = util.GetEnterpriseID();

            DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
            IDbConnection conApp = dbCon.GetConnection();

            string strsql = "SELECT * FROM dbo.CSS_Accounts WHERE userid='" + userID.Trim() + "' AND enterpriseid='" + strEnterpriseID + "' AND applicationid='" + strAppID + "'";

            IDbCommand dbCmdCh = null;
            dbCmdCh = dbCon.CreateCommand(strsql, CommandType.Text);
            DataSet dsUser = null;
            dsUser = (DataSet)dbCon.ExecuteQuery(dbCmdCh, com.common.DAL.ReturnType.DataSetType);

            if (dsUser.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
