<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="CustomsTariffs.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsTariffs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomsTariffs</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="False">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
			function funcDisBack(){
				history.go(+1);
			}

			function CheckTariffs(obj) {
				var output = obj.value.replace(/[^.0-9]/g, "");
				obj.value = output;
			}

			function RemoveBadNonNumber(strTemp, obj) {
				strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			function AfterPasteNonNumber(obj) {
				setTimeout(function () {
					RemoveBadNonNumber(obj.value, obj);
				}, 1); //or 4
			}
			
			function AfterPasteDateEdit(obj) {
				//var strDate = obj.value;
				//obj.value = strDate.toString().trim();
				//alert('xxxx');
			}
			
			function RemoveBadPaseNumberwhithDot(strTemp, obj) {
				strTemp = strTemp.replace(/[^.0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			
			function AfterPasteNumberwhithDot(obj) {
						setTimeout(function () {
							RemoveBadPaseNumberwhithDot(obj.value, obj);
						}, 1); //or 4
					}
			
			
			 function callback()
			{
					var btn = document.getElementById('btnClientEvent');
					if(btn != null)
					{					
						btn.click();
					}else{
						alert('error call code behind');
					}				        
				
			}
			function validate(evt) {
				var theEvent = evt || window.event;
				var key = theEvent.keyCode || theEvent.which;
				key = String.fromCharCode( key );
				var regex = /[.0-9]/;
				if( !regex.test(key) ) {
					theEvent.returnValue = false;
					if(theEvent.preventDefault) theEvent.preventDefault();
				}
			}
			function validateCode(key) { 
            var keycode = (key.which) ? key.which : key.keyCode; 
            if (!(keycode == 8 || keycode == 46) && (keycode < 48 || keycode > 57)) {
                return false;
            }
             
			}
			
			function isDecimalValidKey(textboxIn, evt)
			{
				var charCode = (evt.which) ? evt.which : event.keyCode;    

				if (charCode > 31 && (charCode < 45 || charCode == 47 || charCode > 57))
				{
				return false;
				}     

				if(charCode == 46 && textboxIn.value.indexOf(".") != -1)
				{
					return false;
				}   
				return true;
			} 
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 1008px; HEIGHT: 1250px; TOP: 34px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<table style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tbody>
						<tr>
							<td><asp:label style="Z-INDEX: 0" id="lbCustomsTariffs" runat="server" Width="200px" Height="27px"
									CssClass="mainTitleSize">Customs Tariffs</asp:label></td>
						</tr>
						<tr>
							<td><asp:button style="Z-INDEX: 0" id="btQuery" runat="server" CssClass="queryButton" Text="Query"
									CausesValidation="False"></asp:button><label style="WIDTH: 5px"></label><asp:button style="Z-INDEX: 0" id="btExecQuery" runat="server" CssClass="queryButton"
									Text="Execute Query" CausesValidation="False"></asp:button></td>
						</tr>
						<tr>
							<td style="HEIGHT: 19px" colSpan="6"><asp:label style="Z-INDEX: 0" id="lblError" runat="server" ForeColor="Red" Font-Bold="True"
									Font-Size="X-Small"></asp:label></td>
						</tr>
						<tr>
							<td><asp:label style="Z-INDEX: 0" id="lbTariffCode" runat="server" CssClass="tableLabel"> Tariff Code:</asp:label><LABEL style="Z-INDEX: 0; WIDTH: 20px"></LABEL>
								<asp:textbox id="txtTariffCode" onkeypress="validate(event)" runat="server" Width="150px" CssClass="textField"
									MaxLength="10" tabIndex="1"></asp:textbox></td>
						</tr>
						<TR>
							<TD style="HEIGHT: 19px" colSpan="6"></TD>
						</TR>
						<TR>
							<TD><asp:datagrid style="Z-INDEX: 0" id="gridTariffs" runat="server" Width="100%" DataKeyField="tariff_code"
									AllowPaging="True" FooterStyle-CssClass="gridHeading" BorderWidth="0px" CellSpacing="1" CellPadding="0"
									HeaderStyle-CssClass="gridHeading" ItemStyle-CssClass="gridField" AlternatingItemStyle-CssClass="gridField"
									ShowFooter="True" AutoGenerateColumns="False">
									<FooterStyle CssClass="gridHeading"></FooterStyle>
									<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn>
											<HeaderStyle Width="8%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:LinkButton id="btnEditItem" Runat="server" Text="<img src='images/butt-edit.gif' alt='edit' border=0>"
													CommandName="EDIT_ITEM" />
												<asp:LinkButton id="btnDeleteItem" Runat="server" Text="<img src='images/butt-delete.gif' alt='delete' border=0>"
													CommandName="DELETE_ITEM" />
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Center"></FooterStyle>
											<FooterTemplate>
												<asp:LinkButton id="btnAddItem" Runat="server" Text="<img src='images/butt-add.gif' alt='add' border=0>"
													CommandName="ADD_ITEM" tabIndex="9" />
											</FooterTemplate>
											<EditItemTemplate>
												<asp:LinkButton id="btnSaveItem" Runat="server" Text="<img src='images/butt-update.gif' alt='save' border=0>"
													CommandName="SAVE_ITEM" />
												<asp:LinkButton id="btnCancelItem" Runat="server" Text="<img src='images/butt-red.gif' alt='cancel' border=0>"
													CommandName="CANCEL_ITEM" />
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Tariff">
											<HeaderStyle HorizontalAlign="Left" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lbID" Runat="server" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem, "tariff_code") %>'>
												</asp:Label>
												<%# DataBinder.Eval(Container.DataItem, "tariff_code") %>
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Left"></FooterStyle>
											<FooterTemplate>
												<asp:textbox id="txtTariffCodeAdd" onkeypress="validate(event)" runat="server" Width="90px" CssClass="textField"
													MaxLength="10" tabIndex="2"></asp:textbox>
											</FooterTemplate>
											<EditItemTemplate>
												<cc1:mstextbox style="Z-INDEX: 0" id="txtTariffCodeEdit" runat="server" CssClass="textField" Width="90px" MaxLength="10" onkeypress="validate(event)" Text='<%#DataBinder.Eval(Container.DataItem, "tariff_code")%>' onpaste="AfterPasteNumberwhithDot(this)">
												</cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Description">
											<HeaderStyle HorizontalAlign="Left" Width="24%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lbdescription1" Visible="True" Runat="server" Width="300px" Style="word-wrap: normal; word-break: break-all;"></asp:Label>
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Left"></FooterStyle>
											<FooterTemplate>
												<asp:textbox id="txtDescAdd" runat="server" tabIndex="3" CssClass="textField" MaxLength="200" Width="300"></asp:textbox>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:textbox id="txtDescEdit" runat="server" CssClass="textField" MaxLength="200" Width="300" Text='<%#DataBinder.Eval(Container.DataItem, "description1")%>'>
												</asp:textbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Duty Rate">
											<HeaderStyle HorizontalAlign="Right" Width="7%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lbDutyRate" Visible="True" Runat="server"></asp:Label>
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Right"></FooterStyle>
											<FooterTemplate>
												<cc1:mstextbox id="txtDutyRateAdd" runat="server" CssClass="gridTextBoxNumber" NumberPrecision="14"
													TextMaskType="msNumericCOD" MaxLength="14" NumberScale="5" NumberMinValue="0" onpaste="AfterPasteNumberwhithDot(this)"
													NumberMaxValueCOD="100000000" tabIndex="4"></cc1:mstextbox>
											</FooterTemplate>
											<EditItemTemplate>
												<cc1:mstextbox id="txtDutyRateEdit" runat="server" CssClass="gridTextBoxNumber" NumberPrecision="14" TextMaskType="msNumericCOD" MaxLength="14" NumberScale="4" NumberMinValue="0" onpaste="AfterPasteNumberwhithDot(this)" NumberMaxValueCOD="100000000" Text='<%#DataBinder.Eval(Container.DataItem, "duty_rate", "{0:#####0.00}")%>'>
												</cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Duty Calc">
											<HeaderStyle HorizontalAlign="Center" Width="7%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lbDutyCalc" Visible="True" Runat="server"></asp:Label>
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Center"></FooterStyle>
											<FooterTemplate>
												<asp:DropDownList ID="ddlDutyCalcAdd" Runat="server" tabIndex="5"></asp:DropDownList>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:DropDownList ID="ddlDutyCalcEdit" Runat="server"></asp:DropDownList>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Excise Rate">
											<HeaderStyle HorizontalAlign="Right" Width="7%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lbExciseRate" Visible="True" Runat="server"></asp:Label>
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Right"></FooterStyle>
											<FooterTemplate>
												<cc1:mstextbox id="txtExciseRateAdd" runat="server" CssClass="gridTextBoxNumber" NumberPrecision="14"
													TextMaskType="msNumericCOD" MaxLength="14" NumberScale="5" NumberMinValue="0" onpaste="AfterPasteNumberwhithDot(this)"
													NumberMaxValueCOD="100000000" tabIndex="6"></cc1:mstextbox>
											</FooterTemplate>
											<EditItemTemplate>
												<cc1:mstextbox id="txtExciseRateEdit" runat="server" CssClass="gridTextBoxNumber" NumberPrecision="14" TextMaskType="msNumericCOD" MaxLength="14" NumberScale="5" NumberMinValue="0" onpaste="AfterPasteNumberwhithDot(this)" NumberMaxValueCOD="100000000" Text='<%#DataBinder.Eval(Container.DataItem, "excise_rate", "{0:#####0.00}")%>'>
												</cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Excise Calc">
											<HeaderStyle HorizontalAlign="Center" Width="7%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lbExciseCalc" Visible="True" Runat="server"></asp:Label>
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Center"></FooterStyle>
											<FooterTemplate>
												<asp:DropDownList ID="ddlExciseCalcAdd" Runat="server" tabIndex="7"></asp:DropDownList>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:DropDownList ID="ddlExciseCalcEdit" Runat="server"></asp:DropDownList>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Permit Req.">
											<HeaderStyle HorizontalAlign="Center" Width="7%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lbPermitReq" Visible="True" Runat="server"></asp:Label>
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Center"></FooterStyle>
											<FooterTemplate>
												<asp:DropDownList ID="ddlPermitReqAdd" Runat="server" tabIndex="8"></asp:DropDownList>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:DropDownList ID="ddlPermitReqEdit" Runat="server"></asp:DropDownList>
											</EditItemTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
								</asp:datagrid></TD>
						</TR>
					</tbody>
				</table>
			</div>
		</form>
	</body>
</HTML>
