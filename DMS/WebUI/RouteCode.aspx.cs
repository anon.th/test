using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.ties.classes;
using Cambro.Web.DbCombo;
using com.common.applicationpages;

namespace com.ties
{
    /// <summary>
    /// Summary description for RouteCode.
    /// </summary>
    public class RouteCode : BasePage
    {
        protected System.Web.UI.WebControls.DataGrid dgRouteCode;
        protected System.Web.UI.WebControls.Label lblHeading;
        protected System.Web.UI.WebControls.Button btnExecuteQry;
        protected System.Web.UI.WebControls.Button btnQuery;
        protected System.Web.UI.WebControls.Label lblErrormsg;
        protected System.Web.UI.WebControls.DropDownList ddPathCode;
        protected System.Web.UI.WebControls.Button btnInsertRow;
        protected System.Web.UI.WebControls.ValidationSummary PageVlid;
        protected System.Web.UI.WebControls.ValidationSummary Pagevalid;

        public string m_strAppID = null;
        public string m_strEnterpriseID = null;
        SessionDS m_dsRouteCode = null;


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            base.OnInit(e);
            InitializeComponent();




        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnInsertRow.Click += new System.EventHandler(this.btnInsertRow_Click);
            this.btnExecuteQry.Click += new System.EventHandler(this.btnExecuteQry_Click);
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        private void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            m_strAppID = utility.GetAppID();
            m_strEnterpriseID = utility.GetEnterpriseID();
            if (!IsPostBack)
            {
                QueryMode();
            }
            else
            {
                m_dsRouteCode = (SessionDS)Session["SESSION_DS1"];
            }
            Pagevalid.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
        }

        [Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
        public static object ddPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
        {
            Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings, HttpContext.Current.Session);
            String strAppID = util.GetAppID();
            String strEnterpriseID = util.GetEnterpriseID();

            DataSet dataset = DbComboDAL.PathCodeQuery(strAppID, strEnterpriseID, args, "");
            return dataset;
        }

        public static object SetDropdownPathCode()
        {
            Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings, HttpContext.Current.Session);
            String strAppID = util.GetAppID();
            String strEnterpriseID = util.GetEnterpriseID();
            DataSet dataset = DbComboDAL.PathCodeQuery(strAppID, strEnterpriseID, null, "");
            return dataset;
        }

        public void BindRouteGrid()
        {
            dgRouteCode.VirtualItemCount = System.Convert.ToInt32(m_dsRouteCode.QueryResultMaxSize);
            dgRouteCode.DataSource = m_dsRouteCode.ds;
            dgRouteCode.DataBind();
            Session["SESSION_DS1"] = m_dsRouteCode;
        }

        public void QueryMode()
        {
            ViewState["RCOperation"] = Operation.None;
            ViewState["RCMode"] = ScreenMode.Query;

            btnQuery.Enabled = true;
            btnExecuteQry.Enabled = true;
            Utility.EnableButton(ref btnInsertRow, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);

            m_dsRouteCode = SysDataMgrDAL.GetEmptyRouteCodeDS();

            m_dsRouteCode.DataSetRecSize = 1;
            AddRow();
            Session["SESSION_DS1"] = m_dsRouteCode;
            BindRouteGrid();
            lblErrormsg.Text = "";

            EditHRow(true);
            dgRouteCode.Columns[0].Visible = false;
            dgRouteCode.Columns[1].Visible = false;
        }

        private void AddRow()
        {
            SysDataMgrDAL.AddNewRowInRouteCode(ref m_dsRouteCode);
            BindRouteGrid();
        }

        private void EditHRow(bool bItemIndex)
        {
            foreach (DataGridItem item in dgRouteCode.Items)
            {
                if (bItemIndex)
                    dgRouteCode.EditItemIndex = item.ItemIndex;
                else
                    dgRouteCode.EditItemIndex = -1;
            }
            dgRouteCode.DataBind();
        }

        private void GetChangedRow(DataGridItem item, int rowIndex)
        {
            this.ddPathCode = (DropDownList)item.FindControl("ddPathCode");
            msTextBox txtRouteCode = (msTextBox)item.FindControl("txtRouteCode");
            TextBox txtRouteDescp = (TextBox)item.FindControl("txtRouteDescp");

            string strPathCode = "";
            string strRouteCode = "";
            string strRouteDescp = "";

            if (this.ddPathCode != null)
                strPathCode = this.ddPathCode.SelectedValue.ToString();

            if (txtRouteCode != null)
                strRouteCode = txtRouteCode.Text.ToString();

            if (txtRouteDescp != null)
                strRouteDescp = txtRouteDescp.Text.ToString();


            DataRow dr = m_dsRouteCode.ds.Tables[0].Rows[rowIndex];
            dr[0] = strPathCode;
            dr[1] = strRouteCode;
            dr[2] = strRouteDescp;
            Session["SESSION_DS1"] = m_dsRouteCode;

        }

        protected void dgRouteCode_Edit(object sender, DataGridCommandEventArgs e)
        {
            lblErrormsg.Text = "";
            if ((int)ViewState["RCMode"] == (int)ScreenMode.Insert && (int)ViewState["RCOperation"] == (int)Operation.Insert && dgRouteCode.EditItemIndex > 0)
            {
                m_dsRouteCode.ds.Tables[0].Rows.RemoveAt(dgRouteCode.EditItemIndex);
                dgRouteCode.CurrentPageIndex = 0;
            }
            dgRouteCode.EditItemIndex = e.Item.ItemIndex;
            ViewState["RCOperation"] = Operation.Update;
            BindRouteGrid();
        }
        /*---*/
        private void btnQuery_Click(object sender, System.EventArgs e)
        {
            lblErrormsg.Text = "";
            dgRouteCode.CurrentPageIndex = 0;
            QueryMode();
        }

        private void btnInsertRow_Click(object sender, System.EventArgs e)
        {
            lblErrormsg.Text = "";
            if (btnExecuteQry.Enabled == true)
                btnExecuteQry.Enabled = false;

            if ((int)ViewState["RCMode"] != (int)ScreenMode.Insert || m_dsRouteCode.ds.Tables[0].Rows.Count >= dgRouteCode.PageSize)
            {
                m_dsRouteCode = SysDataMgrDAL.GetEmptyRouteCodeDS();
                dgRouteCode.EditItemIndex = m_dsRouteCode.ds.Tables[0].Rows.Count - 1;
                dgRouteCode.CurrentPageIndex = 0;
                AddRow();
            }
            else
            {
                AddRow();
            }
            Utility.EnableButton(ref btnInsertRow, com.common.util.ButtonType.Insert, false, m_moduleAccessRights);
            EditHRow(false);
            ViewState["RCMode"] = ScreenMode.Insert;
            ViewState["RCOperation"] = Operation.Insert;
            dgRouteCode.Columns[0].Visible = true;
            dgRouteCode.Columns[1].Visible = true;
            dgRouteCode.EditItemIndex = m_dsRouteCode.ds.Tables[0].Rows.Count - 1;
            dgRouteCode.CurrentPageIndex = 0;
            BindRouteGrid();
            getPageControls(Page);
        }

        protected void dgRouteCode_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemIndex == -1)
                return;

            DataRow drSelected = m_dsRouteCode.ds.Tables[0].Rows[e.Item.ItemIndex];
            if (drSelected.RowState == DataRowState.Deleted)
                return;

            if ((int)ViewState["RCOperation"] != (int)Operation.Insert && (!btnExecuteQry.Enabled))
            {
                this.ddPathCode = (DropDownList)e.Item.FindControl("ddPathCode");
                if (this.ddPathCode != null)
                    this.ddPathCode.Enabled = true;//false

                msTextBox txtRouteCode = (msTextBox)e.Item.FindControl("txtRouteCode");
                if (txtRouteCode != null)
                    txtRouteCode.ReadOnly = true;
            }

            if ((int)ViewState["RCMode"] == (int)ScreenMode.Insert && (int)ViewState["RCOperation"] == (int)Operation.Insert)
                e.Item.Cells[1].Enabled = true;//false

            if ((int)ViewState["RCMode"] == (int)ScreenMode.Insert)
                e.Item.Cells[1].Enabled = true; //false
            else
                e.Item.Cells[1].Enabled = true;

            DropDownList ddPathCode = (DropDownList)e.Item.FindControl("ddPathCode");

            if (ddPathCode != null)
            {
                ddPathCode.DataSource = CreatePahtCodeOptions();
                ddPathCode.DataTextField = "Text";
                ddPathCode.DataValueField = "StringValue";
                ddPathCode.DataBind();
                //re-select ddl
                String strType = (String)drSelected["path_code"];
                ddPathCode.SelectedIndex = ddPathCode.Items.IndexOf(ddPathCode.Items.FindByValue(strType));
            }

        }

        /// <summary>
		/// To populate options type in the drop down list
		/// </summary>
		/// <param name="showNilOption">to show an empty option or not</param>
		/// <returns></returns>
		private DataView CreatePahtCodeOptions()
        {
            DataTable dtTypeOptions = new DataTable();
            ArrayList codeValues = null;
            dtTypeOptions.Columns.Add(new DataColumn("Text", typeof(string)));
            dtTypeOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

            DataSet typeOptionArray = TIESUtility.GetAllPathCode(m_strAppID, m_strEnterpriseID);

            int cnt = typeOptionArray.Tables[0].Rows.Count;

            if (cnt > 0)
            {
                codeValues = new ArrayList();

                for (int i = 0; i < cnt; i++)
                {
                    DataRow drEach = typeOptionArray.Tables[0].Rows[i];
                    SystemCode systemCode = new SystemCode();

                    if (!drEach["path_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
                    {
                        systemCode.Text = (String)drEach["path_code"];
                        systemCode.StringValue = (String)drEach["path_code"];
                    }

                    codeValues.Add(systemCode);
                }

                DataRow drNilRow = dtTypeOptions.NewRow();
                drNilRow[0] = "";
                drNilRow[1] = "";
                dtTypeOptions.Rows.Add(drNilRow);

                foreach (SystemCode typeSysCode in codeValues)
                {
                    DataRow drEach = dtTypeOptions.NewRow();
                    drEach[0] = typeSysCode.Text;
                    drEach[1] = typeSysCode.StringValue;
                    dtTypeOptions.Rows.Add(drEach);
                }
            }
            DataView dvTypeOptions = new DataView(dtTypeOptions);
            return dvTypeOptions;
        }

        protected void dgRouteCode_Cancel(object sender, DataGridCommandEventArgs e)
        {
            lblErrormsg.Text = "";
            int rowIndex = e.Item.ItemIndex;
            if ((int)ViewState["RCMode"] == (int)ScreenMode.Insert && (int)ViewState["RCOperation"] == (int)Operation.Insert)
            {
                Utility.EnableButton(ref btnInsertRow, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                m_dsRouteCode.ds.Tables[0].Rows.RemoveAt(rowIndex);
            }
            ViewState["RCOperation"] = Operation.None;
            dgRouteCode.EditItemIndex = -1;
            BindRouteGrid();
        }

        protected void dgRouteCode_PageChange(object sender, DataGridPageChangedEventArgs e)
        {
            lblErrormsg.Text = "";
            dgRouteCode.CurrentPageIndex = e.NewPageIndex;
            dgRouteCode.SelectedIndex = -1;
            dgRouteCode.EditItemIndex = -1;
            RetreiveSelectedPage();
        }

        private void RetreiveSelectedPage()
        {
            int iStartRow = dgRouteCode.CurrentPageIndex * dgRouteCode.PageSize;
            DataSet dsQuery = (DataSet)Session["QUERY_DS"];

            m_dsRouteCode = SysDataMgrDAL.GetRouteCodeDS(m_strAppID, m_strEnterpriseID, iStartRow, dgRouteCode.PageSize, dsQuery);
            int iPageCnt = Convert.ToInt32((m_dsRouteCode.QueryResultMaxSize - 1)) / dgRouteCode.PageSize;
            if (iPageCnt < dgRouteCode.CurrentPageIndex)
            {
                dgRouteCode.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
            }
            BindRouteGrid();
            Session["SESSION_DS1"] = m_dsRouteCode;
        }

        protected void dgRouteCode_Delete(object sender, DataGridCommandEventArgs e)
        {
            if (btnExecuteQry.Enabled)
                return;

            if ((int)ViewState["RCOperation"] == (int)Operation.Update)
            {
                dgRouteCode.EditItemIndex = -1;
            }
            BindRouteGrid();

            int rowIndex = e.Item.ItemIndex;
            m_dsRouteCode = (SessionDS)Session["SESSION_DS1"];
            DataRow dr = m_dsRouteCode.ds.Tables[0].Rows[rowIndex];
            String strPathCode = (string)dr[0];
            String strRouteCode = (string)dr[1];

            if (strRouteCode == null || strRouteCode == "")
                return;

            try
            {
                // delete from table
                int iRowsDeleted = SysDataMgrDAL.DeleteRouteCodeDS(m_strAppID, m_strEnterpriseID, strRouteCode, strPathCode);
                ArrayList paramValues = new ArrayList();
                paramValues.Add(iRowsDeleted.ToString());
                lblErrormsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "DLD_SUCCESSFULLY", utility.GetUserCulture(), paramValues);
            }
            catch (ApplicationException appException)
            {
                String strMsg = appException.Message;
                if (strMsg.IndexOf("FK") != -1)
                {
                    lblErrormsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "CHILD_FOUND_CANNOT_DEL", utility.GetUserCulture());
                }
                if (strMsg.ToLower().IndexOf("child record found") != -1)
                {
                    lblErrormsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "CHILD_FOUND_CANNOT_DEL", utility.GetUserCulture());
                }

                Logger.LogTraceError("RouteCode", "dgRouteCode_Delete", "RBAC003", appException.Message.ToString());
                return;
            }

            if ((int)ViewState["RCMode"] == (int)ScreenMode.Insert)
                m_dsRouteCode.ds.Tables[0].Rows.RemoveAt(rowIndex);
            else
            {
                RetreiveSelectedPage();
            }
            ViewState["RCOperation"] = Operation.None;
            //Make the row in non-edit Mode
            EditHRow(false);
            BindRouteGrid();
            Logger.LogDebugInfo("RouteCode", "dgRouteCode_Delete", "INF004", "updating data grid...");

        }

        private void btnExecuteQry_Click(object sender, System.EventArgs e)
        {
            lblErrormsg.Text = "";
            btnExecuteQry.Enabled = false;
            ViewState["RCMode"] = ScreenMode.ExecuteQuery;
            //Since Query mode is always one row in edit mode, the rowindex is set to zero
            int rowIndex = 0;
            GetChangedRow(dgRouteCode.Items[0], rowIndex);
            Session["QUERY_DS"] = m_dsRouteCode.ds;
            RetreiveSelectedPage();
            if (m_dsRouteCode.ds.Tables[0].Rows.Count == 0)
            {
                lblErrormsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "NO_RECORD_FOUND", utility.GetUserCulture());
                dgRouteCode.EditItemIndex = -1;
                return;
            }
            //set all records to non-edit mode after execute query
            EditHRow(false);
            dgRouteCode.Columns[0].Visible = true;
            dgRouteCode.Columns[1].Visible = true;
        }

        protected void dgRouteCode_Update(object sender, DataGridCommandEventArgs e)
        {
            lblErrormsg.Text = "";
            if (btnExecuteQry.Enabled)
            {
                return;
            }
            int iRowsAffected = 0;
            int rowIndex = e.Item.ItemIndex;
            GetChangedRow(e.Item, e.Item.ItemIndex);

            int iOperation = (int)ViewState["RCOperation"];
            try
            {
                if (iOperation == (int)Operation.Insert)
                {
                    iRowsAffected = SysDataMgrDAL.InsertRouteCode(m_strAppID, m_strEnterpriseID, rowIndex, m_dsRouteCode.ds);
                    ArrayList paramValues = new ArrayList();
                    paramValues.Add(iRowsAffected.ToString());
                    lblErrormsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ROWS_INS", utility.GetUserCulture(), paramValues); ;
                }
                else
                {
                    DataSet dsChangedRow = m_dsRouteCode.ds.GetChanges();
                    iRowsAffected = SysDataMgrDAL.UpdateRouteCode(m_strAppID, m_strEnterpriseID, dsChangedRow);
                    ArrayList paramValues = new ArrayList();
                    paramValues.Add(iRowsAffected.ToString());
                    lblErrormsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ROWS_UPD", utility.GetUserCulture(), paramValues); ;
                    m_dsRouteCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
                }
            }
            catch (ApplicationException appException)
            {
                String strMsg = appException.Message;
                strMsg = appException.InnerException.Message;
                strMsg = appException.InnerException.InnerException.Message;
                if (strMsg.IndexOf("PRIMARY KEY") != -1)
                    lblErrormsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "NO_DUPLICATE", utility.GetUserCulture());
                else if (strMsg.IndexOf("duplicate key") != -1)
                {
                    lblErrormsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "NO_DUPLICATE", utility.GetUserCulture());
                }

                Logger.LogTraceError("DeliveryPathCode.aspx.cs", "dgState_Update", "RBAC003", appException.Message.ToString());
                return;
            }

            if (iRowsAffected == 0)
            {
                return;
            }
            Utility.EnableButton(ref btnInsertRow, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
            ViewState["RCOperation"] = Operation.None;
            dgRouteCode.EditItemIndex = -1;
            BindRouteGrid();
            Logger.LogDebugInfo("State", "dgDelvPath_Update", "INF004", "updating data grid...");

        }


    }
}
