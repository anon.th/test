<%@ Page Language="c#" CodeBehind="DomesticShipment_New.aspx.cs" AutoEventWireup="false" Inherits="com.ties.DomesticShipment_New" SmartNavigation="False" %>

<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>DomesticShipment</title>
    <meta name="vs_snapToGrid" content="True">
    <meta name="vs_showGrid" content="True">
    <link rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
    <meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <!--#INCLUDE FILE="msFormValidations.inc"-->
    <script language="javascript" src="Scripts/settingScrollPosition.js"></script>
    <script language="javascript" src="Scripts/JScript_TextControl.js"></script>
    <script language="javascript" type="text/javascript">
        function callCodeBehind(cusid) {//debugger;
            var btn = document.getElementById('btnClientEvent');
            if (btn != null) {
                document.getElementById("txtCustID").value = cusid;
                btn.click();
            } else {
                alert('error call code behind');
            }
        }

        function callCodeBehindSvcCode() {		//debugger;									
            __doPostBack('<%= txtShipPckUpTime.ClientID  %>', '');
        }

        function CalculateTotalAmtFunction() {//debugger;
            //var btn = document.getElementById("btnCmd");
            //btn.click();
            var decInsuranceChrg = 0;
            var decFrghtChrg = 0;
            var decTotOtherChrg = 0;
            var decTotVASSurcharge = 0;
            var decESASSurcharge = 0;
            var decTotalAmt = 0;

            var InsChrgValue = document.getElementById("txtInsChrg").value;
            var FreightChrgValue = document.getElementById("txtFreightChrg").value;
            var OtherSurcharge2Value = document.getElementById("txtOtherSurcharge2").value;
            var TotVASSurChrgValue = document.getElementById("txtTotVASSurChrg").value;
            var ESASurchrgValue = document.getElementById("txtESASurchrg").value;
            var objInsChrg = document.getElementById("txtShpTotAmt");

            if (InsChrgValue.length > 0) {
                decInsuranceChrg = parseFloat(InsChrgValue);
            }
            if (FreightChrgValue.length > 0) {
                decFrghtChrg = parseFloat(FreightChrgValue);
            }
            if (OtherSurcharge2Value.length > 0) {
                decTotOtherChrg = parseFloat(OtherSurcharge2Value);
            }
            if (TotVASSurChrgValue.length > 0) {
                decTotVASSurcharge = parseFloat(TotVASSurChrgValue);
            }
            if (ESASurchrgValue.length > 0) {
                decESASSurcharge = parseFloat(ESASurchrgValue);
            }
            decTotalAmt = Math.round(decTotVASSurcharge + decFrghtChrg + decInsuranceChrg + decTotOtherChrg + decESASSurcharge);
            objInsChrg.value = parseFloat(decTotalAmt).toFixed(0);

        }
        function ValidateConNo(obj) {//debugger;
            var btn = document.getElementById('btnExecQry');
            if (btn != null && btn.disabled == true) {
                __doPostBack('hddConNo', '');
                return true;
            } else {
                return false;
            }
        }

        function ExecuteQueryConNo() {
            var btn = document.getElementById('btnExecQry');
            if (btn != null) {
                btn.disabled = true;
                __doPostBack('ExecuteQuery', '');
                //return true;
            }
            //else{
            //	return false;
            //}
        }

    </script>
</head>
<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
    onload="SetScrollPosition();" ms_positioning="GridLayout">
    <form id="DomesticShipment" method="post" name="DomesticShipment" runat="server">
        <input style="z-index: 108; position: absolute; display: none; top: 48px; left: 144px"
            id="ExecuteQuery" type="button" name="hddCon" runat="server" causesvalidation="False">
        <asp:TextBox Style="z-index: 0" ID="Textbox4" TabIndex="126" runat="server" Visible="False" ReadOnly="True"
            CssClass="textFieldRightAlign" Width="131px"></asp:TextBox><input style="display: none" id="hddConNo" type="button" name="hddCon" runat="server" causesvalidation="False">
        <p>
            <asp:Label Style="z-index: 107; position: absolute; top: 8px; left: 8px" ID="lblMainTitle"
                runat="server" CssClass="mainTitleSize" Width="477px">Domestic Shipment</asp:Label>
        </p>
        <div style="z-index: 102; position: relative; width: 1225px; height: 1602px; top: 16px; left: -1px"
            id="divMain" ms_positioning="GridLayout" runat="server">
            <table style="z-index: 101; position: absolute; width: 1064px; height: 1309px; top: 8px; left: 0px"
                id="MainTable" border="0" width="1064" runat="server">
                <tr>
                    <td style="height: 70px" width="738">
                        <table border="0" cellspacing="0" cellpadding="0" width="738">
                            <tr>
                                <td width="61">
                                    <asp:Button ID="btnQry" runat="server" CssClass="queryButton" Width="61px" CausesValidation="False"
                                        Text="Query"></asp:Button></td>
                                <td width="130">
                                    <%--<asp:button id="btnExecQry" runat="server" CssClass="queryButton" Width="130px" CausesValidation="False"
											Text="Execute Query"></asp:button>--%>
                                    <input style="z-index: 0" id="btnExecQry" type="button" name="ExecQury" onclick="ExecuteQueryConNo();"
                                        class="queryButton" runat="server" value="Execute Query" causesvalidation="False">
                                </td>
                                <td width="62">
                                    <asp:Button ID="btnInsert" runat="server" CssClass="queryButton" Width="62px" CausesValidation="False"
                                        Text="Insert"></asp:Button></td>
                                <td width="66">
                                    <asp:Button ID="btnSave" runat="server" CssClass="queryButton" Width="66px" Text="Save"></asp:Button></td>
                                <td>
                                    <asp:Button Style="display: none; visibility: hidden" ID="btnDelete" runat="server" CssClass="queryButton"
                                        Width="62px" CausesValidation="False" Text="Delete"></asp:Button><asp:Button Style="display: none" ID="btnClientEvent" runat="server" CausesValidation="False"
                                            Text="ClientEvent"></asp:Button><asp:Button Style="z-index: 0" ID="btnPrintConsNote" runat="server" CssClass="queryButton" Width="130px"
                                                CausesValidation="False" Text="Print Con Note"></asp:Button></td>
                                <td width="238"></td>
                                <td width="24">
                                    <asp:Button ID="btnGoToFirstPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False"
                                        Text="|<"></asp:Button></td>
                                <td width="24">
                                    <asp:Button ID="btnPreviousPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False"
                                        Text="<"></asp:Button></td>
                                <td width="23">
                                    <asp:TextBox ID="txtGoToRec" TabIndex="8" runat="server" Width="23px" Height="19px" Enabled="False"></asp:TextBox></td>
                                <td style="width: 27px" width="24">
                                    <asp:Button ID="btnNextPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False"
                                        Text=">"></asp:Button></td>
                                <td width="24">
                                    <asp:Button ID="btnGoToLastPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False"
                                        Text=">|"></asp:Button></td>
                            </tr>
                            <tr>
                                <td colspan="11" align="right">
                                    <asp:Label ID="lblNumRec" runat="server" CssClass="RecMsg" Width="274px" Height="19px"></asp:Label></td>
                            </tr>
                        </table>
                        <asp:Label ID="lblErrorMsg" runat="server" CssClass="errorMsgColor" Width="566px" Height="19px"></asp:Label><asp:RegularExpressionValidator ID="Regularpickupdate" runat="server" CssClass="errorMsgColor" Width="467px" ControlToValidate="txtShipPckUpTime"
                            Display="None" ValidationExpression="^((1[0-2]|(0[1-9])|[1][\d]|[2][\d]|[3][0]|[3][1])\/((0[1-9])|(1[0-2]))\/([\d][\d][\d][\d])[\s]((0[0-9])|[1][\d]|(2[0-4]))\:(0[0-9]|(1[\d]|2[\d]|3[\d]|4[\d]|5[\d]|6[0])))"></asp:RegularExpressionValidator><asp:RegularExpressionValidator ID="RegularEstDlvryDt" runat="server" CssClass="errorMsgColor" Width="467px" ControlToValidate="txtShipEstDlvryDt"
                                Display="None" ValidationExpression="^((1[0-2]|(0[1-9])|[1][\d]|[2][\d]|[3][0]|[3][1])\/((0[1-9])|(1[0-2]))\/([\d][\d][\d][\d])[\s]((0[0-9])|[1][\d]|(2[0-4]))\:(0[0-9]|(1[\d]|2[\d]|3[\d]|4[\d]|5[\d]|6[0])))"></asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator Style="z-index: 117; position: absolute; top: 80px; left: 0px" ID="RegularShpManfstDt"
                            runat="server" CssClass="errorMsgColor" Width="467px" ControlToValidate="txtShipManifestDt" Display="None" ValidationExpression="^((1[0-2]|(0[1-9])|[1][\d]|[2][\d]|[3][0]|[3][1])\/((0[1-9])|(1[0-2]))\/([\d][\d][\d][\d])[\s]((0[0-9])|[1][\d]|(2[0-4]))\:(0[0-9]|(1[\d]|2[\d]|3[\d]|4[\d]|5[\d]|6[0])))"></asp:RegularExpressionValidator>
                        <asp:ValidationSummary ID="PageValidationSummary" runat="server" Visible="True" Height="35px" ShowMessageBox="True"
                            ShowSummary="False"></asp:ValidationSummary>
                        <asp:RegularExpressionValidator Style="z-index: 117; position: absolute; top: 80px; left: 0px" ID="Regularexpressionvalidator1"
                            runat="server" CssClass="errorMsgColor" Width="467px" ControlToValidate="txtBookDate" Display="None" ValidationExpression="^((1[0-2]|(0[1-9])|[1][\d]|[2][\d]|[3][0]|[3][1])\/((0[1-9])|(1[0-2]))\/([\d][\d][\d][\d])[\s]((0[0-9])|[1][\d]|(2[0-4]))\:(0[0-9]|(1[\d]|2[\d]|3[\d]|4[\d]|5[\d]|6[0])))"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td style="height: 9px" valign="baseline" width="738">
                        <table style="width: 972px; height: 8px" id="Table6" border="0" cellspacing="1" cellpadding="1">
                            <tr>
                                <td style="width: 1px; height: 8px" height="8" width="1"><font face="Tahoma"></font></td>
                                <td style="width: 80px; height: 8px" valign="baseline" align="left">
                                    <asp:Label ID="lblBookingNo" runat="server" CssClass="tableLabel" Width="80px">Booking #</asp:Label>
                                </td>
                                <td style="width: 150px; height: 8px" valign="baseline" align="left">
                                    <cc1:msTextBox ID="txtBookingNo" TabIndex="1" runat="server" CssClass="textField" Width="167px"
                                        AutoPostBack="True" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="2147483647" NumberPrecision="10"></cc1:msTextBox>
                                </td>

                                <td style="padding-left: 10px; width: 80px; height: 8px" valign="baseline" align="left">
                                    <asp:Label ID="lblBookDate" runat="server" CssClass="tableLabel" Width="80px">Booking D/T</asp:Label>
                                </td>
                                <td style="width: 114px; height: 8px" valign="baseline" align="left">
                                    <cc1:msTextBox ID="txtBookDate" TabIndex="7" runat="server" CssClass="textField" Width="114px"
                                        MaxLength="16" TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99"></cc1:msTextBox>
                                </td>

                                <td style="padding-left: 10px; width: 86px; height: 8px" valign="baseline" align="left">
                                    <asp:Label ID="lblOrigin" runat="server" CssClass="tableLabel" Width="86px">Ori. Province</asp:Label>
                                </td>
                                <td style="width: 114px; height: 8px" valign="baseline" align="left">
                                    <asp:TextBox ID="txtOrigin" TabIndex="3" runat="server" CssClass="textField" Width="180px" Enabled="False"
                                        MaxLength="10"></asp:TextBox>
                                </td>

                                <td style="padding-left: 10px; width: 100px; height: 8px" valign="baseline" align="left">
                                    <asp:Label ID="lblStatusCode" runat="server" CssClass="tableLabel" Width="100px">LAST Status</asp:Label>
                                </td>
                                <td style="width: 114px; height: 8px" valign="baseline" align="left">
                                    <asp:TextBox ID="txtLatestStatusCode" TabIndex="8" runat="server" CssClass="textField" Width="100%"
                                        Enabled="False"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 1px; height: 21px"><font face="Tahoma">
                                    <asp:RequiredFieldValidator ID="validConsgNo" Width="3px" ControlToValidate="txtConsigNo" runat="server" ErrorMessage="Consignment No">*</asp:RequiredFieldValidator></font></td>
                                <td style="height: 8px" valign="baseline" align="left">
                                    <asp:Label ID="lblConsgmtNo" runat="server" CssClass="tableLabel" Width="80px">Con. #</asp:Label>
                                </td>
                                <td style="height: 8px" valign="baseline" align="left">
                                    <asp:TextBox onblur="return ValidateConNo(this);" Style="text-transform: uppercase" ID="txtConsigNo"
                                        TabIndex="4" runat="server" CssClass="textField" Width="167px" MaxLength="21"></asp:TextBox>
                                </td>

                                <td style="padding-left: 10px; height: 8px" valign="baseline" align="left">
                                    <asp:Label ID="lblShipManifestDt" runat="server" CssClass="tableLabel" Width="80px"> Manifest D/T</asp:Label>
                                </td>
                                <td style="height: 8px" valign="baseline" align="left">
                                    <cc1:msTextBox ID="txtShipManifestDt" TabIndex="7" runat="server" CssClass="textField" Width="114px"
                                        AutoPostBack="True" MaxLength="16" TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99"></cc1:msTextBox>
                                </td>

                                <td style="padding-left: 10px; height: 8px" valign="baseline" align="left">
                                    <asp:Label ID="lblDestination" runat="server" CssClass="tableLabel" Width="86px">Dest. Province</asp:Label>
                                </td>
                                <td style="height: 8px" valign="baseline" align="left">
                                    <asp:TextBox ID="txtDestination" TabIndex="6" runat="server" CssClass="textField" Width="180px"
                                        Enabled="False" MaxLength="10"></asp:TextBox>
                                </td>

                                <td style="padding-left: 10px; height: 8px" valign="baseline" align="left">
                                    <asp:Label ID="lblExcepCode" runat="server" CssClass="tableLabel" Width="94px">LAST Exception</asp:Label>
                                </td>
                                <td style="height: 8px" valign="baseline" align="left">
                                    <asp:TextBox ID="txtLatestExcepCode" TabIndex="11" runat="server" CssClass="textField" Width="100%"
                                        Enabled="False"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 1px; height: 21px"></td>
                                <td style="height: 8px" valign="baseline" align="left">
                                    <asp:Label ID="lblRefNo" runat="server" CssClass="tableLabel" Width="80px">Cust. Ref. #</asp:Label>
                                </td>
                                <td style="height: 8px" valign="baseline" align="left" colspan="3">
                                    <asp:TextBox ID="txtRefNo" TabIndex="5" runat="server" CssClass="textField" MaxLength="50" Width="100%"></asp:TextBox>
                                </td>

                                <td style="padding-left: 10px; height: 8px" valign="baseline" align="left">
                                    <asp:Label ID="lblRouteCode" runat="server" CssClass="tableLabel" Width="80px">Route Code</asp:Label>
                                </td>
                                <td style="height: 8px" valign="baseline" align="left">
                                    <asp:TextBox ID="txtRouteCode" runat="server" ReadOnly="True" CssClass="textField" Width="100%"
                                        Enabled="False" MaxLength="12"></asp:TextBox>
                                </td>

                                <td style="padding-left: 10px; height: 8px" valign="baseline" align="left"><font face="Tahoma">
                                    <asp:Label ID="Label3" runat="server" CssClass="tableLabel" Width="86px">Pickup Route</asp:Label></font></td>
                                <td style="height: 8px" valign="baseline" align="left">
                                    <font face="Tahoma">
                                        <asp:TextBox ID="txtPickupRoute" TabIndex="6" runat="server" CssClass="textField" Width="100%"
                                            Enabled="False" MaxLength="10"></asp:TextBox>
                                    </font>
                                </td>

                                <td style="height: 21px"><font face="Tahoma">
                                    <asp:Button ID="btnRouteCode" TabIndex="10" runat="server" Visible="False" CssClass="searchButton"
                                        Width="21px" CausesValidation="False" Text="..." Height="19px" Enabled="False"></asp:Button><asp:Label Style="display: none" ID="lblDelManifest" runat="server" CssClass="tableLabel" Width="80px">Auto Manifest</asp:Label><asp:TextBox Style="display: none" ID="txtDelManifest" TabIndex="12" runat="server" CssClass="textField"
                                            Width="40px" Enabled="False"></asp:TextBox></font></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="width: 738px; height: 176px" width="738" align="left">
                        <table style="width: 972px; height: 154px" id="Table8" border="0" cellspacing="1" cellpadding="1">
                            <tr>
                                <td style="height: 3px">
                                    <asp:Label ID="lblCustInfo" CssClass="tableHeadingFieldset" Width="130px" runat="server">CUSTOMER PROFILE</asp:Label>
                                </td>
                                <td style="height: 3px">
                                    <asp:RequiredFieldValidator ID="validCustID" Width="3px" ControlToValidate="txtCustID" runat="server">*</asp:RequiredFieldValidator>
                                </td>
                                <td style="height: 3px">
                                    <asp:Label ID="lblCustID" runat="server" CssClass="tableLabel" Width="16px"> ID</asp:Label>
                                </td>

                                <td style="height: 3px; width:350px" colspan="3">
                                    <cc1:msTextBox Style="z-index: 0; text-transform: uppercase" ID="txtCustID" TabIndex="15" runat="server"
                                        CssClass="textField" Width="68px" AutoPostBack="True" MaxLength="20" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:msTextBox>
                                    <asp:Button ID="btnDisplayCustDtls" runat="server" CssClass="searchButton" Width="21px" CausesValidation="False" Text="..." Height="19px"></asp:Button>
                                    <asp:DropDownList ID="ddbCustType" TabIndex="13" runat="server" Visible="False" Width="72px"></asp:DropDownList>
                                </td>

                                <td style="height: 3px" colspan="2">
                                    <asp:TextBox ID="txtCustAdd1" TabIndex="18" runat="server" Visible="False" CssClass="textField"
                                        Width="40px" AutoPostBack="True" MaxLength="100"></asp:TextBox>
                                    <asp:TextBox ID="txtCustFax" TabIndex="24" runat="server" Visible="False" CssClass="textField"
                                        Width="48px" AutoPostBack="True" MaxLength="20"></asp:TextBox>
                                    <asp:Label ID="lblCustType" runat="server" Visible="False" CssClass="tableLabel" Width="64px">Cust Type</asp:Label>
                                    <asp:RequiredFieldValidator ID="validCustType" Visible="False" Width="3px" ControlToValidate="ddbCustType" runat="server">*</asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 13px; height: 3px"></td>
                                <td style="width: 31px; height: 3px">
                                    <asp:RequiredFieldValidator ID="validCustAdd1" Visible="False" Width="3px" ControlToValidate="txtCustAdd1" runat="server"
                                        ErrorMessage="Customer Address">*</asp:RequiredFieldValidator></td>
                                <td style="height: 3px;" colspan="2">
                                    <asp:TextBox ID="txtCustAdd2" TabIndex="19" runat="server" Visible="False" CssClass="textField" Width="65px" AutoPostBack="True"></asp:TextBox>
                                    <asp:Label ID="lblTelphone" TabIndex="100" runat="server" Visible="False" CssClass="tableLabel" Width="58px">Telephone</asp:Label>
                                </td>
                                <td style="height: 3px"><font face="Tahoma">
                                    <asp:CheckBox ID="chkNewCust" TabIndex="14" runat="server" Visible="False" CssClass="tableLabel"
                                        Width="104px" Text="New Customer" Height="16px" AutoPostBack="True"></asp:CheckBox></font></td>
                            </tr>

                            <tr style="height: 17px">
                                <td><font face="Tahoma">&nbsp;<asp:Label ID="lblAddress" runat="server" Visible="False" CssClass="tableLabel" Width="48px">Address</asp:Label></font></td>
                                <td>
                                    <asp:RequiredFieldValidator ID="validCustName" Width="3px" ControlToValidate="txtCustName" runat="server">*</asp:RequiredFieldValidator></td>
                                <td>
                                    <asp:Label ID="lblName" runat="server" CssClass="tableLabel" Width="32px">Name</asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtCustName" runat="server" CssClass="textField" Width="214px" MaxLength="100"></asp:TextBox>
                                </td>
                                <td colspan="2">
                                    <asp:Label ID="lblPaymode" runat="server" CssClass="tableLabel" Width="64px"> Payment</asp:Label>
                                    <asp:RadioButton ID="rbCash" runat="server" Width="16px" Text="Cash" Enabled="False" Font-Size="Smaller" GroupName="PaymentType"></asp:RadioButton>
                                    <asp:RadioButton ID="rbCredit" runat="server" Width="16px" Text="Credit" Enabled="False" Font-Size="Smaller" GroupName="PaymentType"></asp:RadioButton>
                                </td>
                                <td style="height: 17px">
                                    <asp:RequiredFieldValidator ID="validCustZip" Visible="False" Width="3px" ControlToValidate="txtCustZipCode"
                                        runat="server" ErrorMessage="Customer Postal Code">*</asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 31px; height: 17px">
                                    <asp:Label ID="lblZip" runat="server" Visible="False" CssClass="tableLabel" Width="48px"> Zipcode</asp:Label>
                                </td>
                                <td colspan="2" align="left">
                                    <asp:TextBox ID="txtCustZipCode" TabIndex="20" runat="server" Visible="False" CssClass="textField"
                                        Width="45px" AutoPostBack="True" MaxLength="10"></asp:TextBox>
                                    <asp:TextBox ID="txtCustCity" runat="server" Visible="False" ReadOnly="True" CssClass="textField" Width="24px"></asp:TextBox>
                                    <asp:TextBox ID="txtCustTelephone" TabIndex="23" runat="server" Visible="False" CssClass="textField" Width="32px" AutoPostBack="True" MaxLength="20"></asp:TextBox>
                                    <asp:Label ID="lblFax" TabIndex="100" runat="server" Visible="False" CssClass="tableLabel" Width="7px">Fax</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustStateCode" runat="server" Visible="False" ReadOnly="True" CssClass="textField" Width="79px"></asp:TextBox>
                                </td>
                            </tr>

                            <tr style="height: 17px">
                                <td>&nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td colspan="3">
                                    &nbsp;</td>
                                <td colspan="4">
                                    <asp:RadioButton ID="rbChargetoSender" runat="server" Width="165px" Text="Charge to Sender" Font-Size="Smaller" GroupName="ChargeTo" Checked="True"></asp:RadioButton>
                                    <asp:RadioButton ID="rbChargetoReceiver" runat="server" Width="165px" Text="Charge to Receiver" Font-Size="Smaller" GroupName="ChargeTo"></asp:RadioButton>
                                </td>
                                <td colspan="2" align="left">
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>

                            <tr>
                                <td height="1" valign="top" colspan="13" style="padding: 5px 0 5px 0" nowrap>
                                    <hr style="width: 842.82%; height: 1px" size="1" width="842.82%">
                                </td>
                            </tr>

                            <tr style="height: 12px">
                                <td>
                                    <asp:Label ID="lblSendInfo" CssClass="tableHeadingFieldset" Width="40px" runat="server">FROM:</asp:Label>
                                    <asp:CheckBox ID="chkSendCustInfo" runat="server" CssClass="tableLabel" Width="57px" Text="Same" AutoPostBack="True"></asp:CheckBox>
                                </td>
                                <td style="width: 10px;">
                                    <asp:RequiredFieldValidator ID="validSendName" Width="3px" ControlToValidate="txtSendName" runat="server">*</asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 16px;">
                                    <asp:Label ID="lblSendNm" runat="server" CssClass="tableLabel" Width="34px">Name</asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtSendName" runat="server" CssClass="textField" Width="190px" AutoPostBack="True" MaxLength="100"></asp:TextBox>
                                    <asp:Button ID="btnSendCust" runat="server" CssClass="searchButton" CausesValidation="False" Text="..."></asp:Button>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="txtSendAddr1" runat="server" CssClass="textField" Width="200px" MaxLength="100"></asp:TextBox>
                                </td>
                                <td style="height: 12px"></td>
                                <td style="height: 12px"></td>
                                <td></td>
                                <td>
                                    <asp:TextBox ID="txtSendState" runat="server" Visible="False" ReadOnly="True" CssClass="textField" Width="79px"></asp:TextBox>
                                </td>
                            </tr>

                            <tr style="height: 24px">
                                <td></td>
                                <td style="width: 10px;">
                                    <asp:RequiredFieldValidator ID="validSenderAdd1" Visible="False" Width="3px" ControlToValidate="txtSendAddr1" runat="server">*</asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 16px;">
                                    <asp:Label ID="lblSendAddr1" runat="server" Visible="False" CssClass="tableLabel" Width="42px">Address</asp:Label>
                                </td>
                                <td colspan="3"></td>
                                <td>
                                    <asp:TextBox ID="txtSendAddr2" runat="server" CssClass="textField" Width="200px" MaxLength="100"></asp:TextBox>
                                </td>
                                <td style="width: 13px;">
                                    <asp:RequiredFieldValidator ID="validSendZip" Width="3px" ControlToValidate="txtSendZip" runat="server">*</asp:RequiredFieldValidator></td>
                                <td style="width: 31px;">
                                    <asp:Label ID="lblSendZip" runat="server" CssClass="tableLabel" Width="48px"> Zipcode</asp:Label>
                                </td>
                                <td width="300px">
                                    <asp:TextBox Style="text-transform: uppercase" ID="txtSendZip" runat="server" CssClass="textField" Width="65px" AutoPostBack="True" MaxLength="10"></asp:TextBox>
                                    <asp:TextBox ID="txtSendCity" runat="server" ReadOnly="True" CssClass="textField" Width="200px"></asp:TextBox>
                                </td>
                            </tr>

                            <tr style="height: 17px">
                                <td><font face="Tahoma"></font></td>
                                <td style="width: 10px;"></td>
                                <td style="width: 16px;">
                                    <asp:Label ID="lblSendConPer" runat="server" CssClass="tableLabel" Width="40px">Contact</asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="txtSendContPer" runat="server" CssClass="textField" Width="150px" MaxLength="100"></asp:TextBox>
                                </td>
                                <td style="width: 60px;">
                                    <asp:Label ID="lblSendTel" TabIndex="100" runat="server" CssClass="tableLabel" Width="52px">Telephone</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSendTel" runat="server" CssClass="textField" Width="200px" MaxLength="20"></asp:TextBox>
                                </td>
                                <td></td>
                                <td>
                                    <asp:Label ID="lblSendFax" TabIndex="100" runat="server" CssClass="tableLabel" Width="20px">Fax</asp:Label>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox ID="txtSendFax" runat="server" CssClass="textField" Width="173px" MaxLength="20"></asp:TextBox>
                                </td>
                                <td>
                                    <cc1:msTextBox ID="txtSendCuttOffTime" runat="server" Visible="False" ReadOnly="True" CssClass="textField"
                                        Width="62px" MaxLength="5" TextMaskType="msTime" TextMaskString="99:99"></cc1:msTextBox>
                                </td>
                            </tr>

                            <tr>
                                <td style="padding: 15px 0 15px 0;" align="top" colspan="13" nowrap>
                                    <hr style="width: 842.82%; height: 1px" size="1" width="842.82%">
                                </td>
                            </tr>

                            <tr style="height: 22px">
                                <td>
                                    <asp:Label ID="lblRecInfo" CssClass="tableHeadingFieldset" Width="40px" runat="server">TO:</asp:Label><asp:CheckBox ID="chkRecip" TabIndex="34" runat="server" CssClass="tableLabel" Width="57px" Text="Same"
                                        AutoPostBack="True"></asp:CheckBox></td>
                                <td></td>
                                <td style="width: 16px;">
                                    <asp:Label ID="lblRecipTelephone" TabIndex="83" runat="server" CssClass="tableLabel" Width="48px">Tel. 1</asp:Label></td>
                                <td style="width: 250px;">
                                    <asp:TextBox ID="txtRecipTel" TabIndex="35" runat="server" CssClass="textField" Width="114px" AutoPostBack="True" MaxLength="20"></asp:TextBox>
                                    <asp:Button ID="btnTelephonePopup" TabIndex="36" runat="server" CssClass="searchButton" CausesValidation="False" Text="..."></asp:Button>
                                </td>
                                <td style="width: 2px; height: 22px">
                                    <asp:RequiredFieldValidator ID="validRecipName" Width="1px" ControlToValidate="txtRecName" runat="server">*</asp:RequiredFieldValidator></td>
                                <td style="height: 22px">
                                    <asp:Label ID="lblRecipNm" TabIndex="100" runat="server" CssClass="tableLabel" Width="26px">Name</asp:Label></td>
                                <td colspan="2"><font face="Tahoma">
                                    <asp:TextBox ID="txtRecName" TabIndex="37" runat="server" CssClass="textField" Width="178px" AutoPostBack="True" MaxLength="100"></asp:TextBox>
                                    <asp:Button ID="btnRecipNm" TabIndex="38" runat="server" CssClass="searchButton" CausesValidation="False"
                                        Text="..."></asp:Button></font></td>
                                <td style="width: 13px;"></td>
                                <td style="width: 31px;"><font face="Tahoma"></font></td>
                                <td colspan="2"><font face="Tahoma"></font></td>
                                <td>
                                    <asp:Label ID="lblSendCuttOffTime" runat="server" Visible="False" CssClass="tableLabel" Width="43px">Cut-off</asp:Label>
                                </td>
                            </tr>

                            <tr style="height: 22px">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="width: 2px;">
                                    <asp:RequiredFieldValidator ID="validRecipAdd1" Width="3px" ControlToValidate="txtRecipAddr1" runat="server">*</asp:RequiredFieldValidator></td>
                                <td style="height: 22px">
                                    <asp:Label ID="lblRecipAddr1" TabIndex="100" runat="server" CssClass="tableLabel" Width="32px">Address</asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtRecipAddr1" TabIndex="39" runat="server" CssClass="textField" Width="200px" MaxLength="100"></asp:TextBox>
                                </td>
                                <td style="width: 13px; height: 22px"></td>
                                <td style="height: 22px" colspan="3">
                                    <asp:TextBox ID="txtRecipAddr2" TabIndex="40" runat="server" CssClass="textField" Width="225px" MaxLength="100"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox Style="display: none" ID="txtRecipState" runat="server" ReadOnly="True" CssClass="textField" Width="56px"></asp:TextBox>
                                    <asp:Button Style="z-index: 0" ID="btnBind" TabIndex="58" Visible="False" CssClass="queryButton"
                                        CausesValidation="False" Text="Refresh" runat="server"></asp:Button>
                                </td>
                            </tr>

                            <tr>
                                <td style="">
                                    <asp:Button ID="btnPopulateVAS" TabIndex="57" Visible="False" CssClass="queryButton" Width="112px"
                                        CausesValidation="False" Text="GetQuotationVAS" Enabled="False" runat="server"></asp:Button>
                                </td>
                                <td style="width: 10px"><font face="Tahoma"></font></td>
                                <td style="width: 16px">
                                    <asp:Label ID="lblRecpContPer" TabIndex="100" runat="server" CssClass="tableLabel" Width="40px">Contact</asp:Label></td>
                                <td style="" colspan="2">
                                    <asp:TextBox Style="z-index: 0" ID="txtRecpContPer" TabIndex="41" runat="server" CssClass="textField" Width="150px" MaxLength="100"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="lblRecipFax" TabIndex="85" runat="server" CssClass="tableLabel" Width="40px">Tel. 2</asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtRecipFax" TabIndex="42" runat="server" CssClass="textField" Width="200px"
                                        MaxLength="20"></asp:TextBox>
                                </td>
                                <td style="width: 13px">
                                    <asp:RequiredFieldValidator ID="validRecipZip" Width="3px" ControlToValidate="txtRecipZip" runat="server">*</asp:RequiredFieldValidator></td>
                                <td style="width: 31px"><font face="Tahoma">
                                    <asp:Label ID="lblRecipZip" TabIndex="100" runat="server" CssClass="tableLabel" Width="48px"> Zipcode</asp:Label></font></td>
                                <td>
                                    <asp:TextBox Style="text-transform: uppercase" ID="txtRecipZip" TabIndex="43" runat="server"
                                        CssClass="textField" Width="65px" AutoPostBack="True" MaxLength="10"></asp:TextBox>
                                    <asp:TextBox ID="txtRecipCity" runat="server" ReadOnly="True" CssClass="textField" Width="200px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr style="height: 10px;">
                    <td></td>
                </tr>

                <tr>
                    <td style="width: 738px; height: 18px" width="738" align="left">
                        <table style="width: 930px; height: 36px" id="Table5" border="0" cellspacing="1" cellpadding="1">
                            <tr>
                                <td style="width: 96px">
                                    <asp:Button ID="btnPkgDetails" TabIndex="46" runat="server" CssClass="queryButton" Width="112px"
                                        CausesValidation="False" Text="Package Detail"></asp:Button></td>
                                <td style="width: 90px">
                                    <asp:Label ID="lblPkgActualWt" runat="server" CssClass="tableLabel" Width="80px">Total Act. Wt.</asp:Label>
                                </td>
                                <td style="width: 90px">
                                    <asp:TextBox ID="txtActualWeight" runat="server" CssClass="textFieldRightAlign" Width="80px"></asp:TextBox>
                                </td>
                                <td style="width: 90px">
                                    <asp:Label ID="lblPkgTotPkgs" runat="server" CssClass="tableLabel" Width="88px">Total Pkgs.</asp:Label>
                                </td>
                                <td style="width: 90px">
                                    <asp:TextBox ID="txtPkgTotpkgs" runat="server" ReadOnly="False" CssClass="textFieldRightAlign"
                                        Width="80px"></asp:TextBox></td>
                                <td style="width: 90px">
                                    <asp:Label ID="lblPkgChargeWt" runat="server" CssClass="tableLabel" Width="88px">Total Chg.Wt.</asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtPkgChargeWt" runat="server" ReadOnly="False" CssClass="textFieldRightAlign" Width="80px"></asp:TextBox>
                                </td>
                                <td style="width: 110px">
                                    <asp:Label ID="lblPkgCommCode" runat="server" Visible="true" CssClass="tableLabel" Width="100px">Commodity Code</asp:Label></td>
                                <td><font face="Tahoma"></font>
                                    <asp:TextBox ID="txtPkgCommCode" TabIndex="44" runat="server" Visible="true" CssClass="textField"
                                        Width="100px" AutoPostBack="True" MaxLength="12"></asp:TextBox><asp:Button ID="btnPkgCommCode" TabIndex="45" runat="server" CssClass="searchButton" CausesValidation="False"
                                            Text="..."></asp:Button></td>
                            </tr>
                            <tr>
                                <td style="width: 96px">
                                    <asp:Button ID="btnViewOldPD" TabIndex="46" runat="server" Visible="False" CssClass="queryButton"
                                        Width="112px" CausesValidation="False" Text="View Old PD"></asp:Button><asp:TextBox ID="txtPkgCommDesc" runat="server" Visible="False" ReadOnly="False" CssClass="textField"
                                            Width="112px" Enabled="False"></asp:TextBox></td>
                                <td style="width: 100px">
                                    <asp:Label ID="Label6" runat="server" CssClass="tableLabel" Width="96px">Total Act. R Wt.</asp:Label></td>
                                <td style="width: 73px"><font face="Tahoma">
                                    <asp:TextBox ID="txtPkgActualWt" runat="server" ReadOnly="False" CssClass="textFieldRightAlign"
                                        Width="80px"></asp:TextBox></font></td>
                                <td style="width: 92px">
                                    <asp:Label ID="lblPkgDimWt" runat="server" CssClass="tableLabel" Width="86px">Total Dim. Wt.</asp:Label></td>
                                <td style="width: 69px">
                                    <asp:TextBox ID="txtPkgDimWt" runat="server" ReadOnly="False" CssClass="textFieldRightAlign"
                                        Width="80px"></asp:TextBox></td>
                                <td style="width: 69px"><font face="Tahoma">
                                    <asp:Label ID="lblTotalVol" runat="server" CssClass="tableLabel" Width="88px">Total Vol.</asp:Label></font></td>
                                <td>
                                    <asp:TextBox ID="txttotVol" runat="server" ReadOnly="False" CssClass="textFieldRightAlign" Width="80px"></asp:TextBox></td>
                                <td colspan="2"><font face="Tahoma">
                                    <asp:Label ID="lblPD_Replace_Date" runat="server" Visible="False" CssClass="tableLabel"></asp:Label></font></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="width: 738px; height: 52px" width="738" align="left">
                        <table style="width: 938px; height: 16px" id="tblShipService" border="0" width="938" runat="server">
                            <tr style="height: 25px">
                                <td style="width: 16px; height: 23px" width="16">
                                    <asp:RequiredFieldValidator ID="validSrvcCode" ControlToValidate="txtShpSvcCode" runat="server">*</asp:RequiredFieldValidator></td>
                                <td style="width: 140px; height: 23px" width="156">
                                    <asp:Label ID="lblShipSerCode" runat="server" CssClass="tableLabel" Width="100px">Service Type</asp:Label>
                                </td>
                                <td style="width: 46px; height: 23px" width="46">
                                    <cc1:msTextBox Style="text-transform: uppercase" ID="txtShpSvcCode" TabIndex="47" runat="server"
                                        CssClass="textField" Width="74px" AutoPostBack="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:msTextBox>
                                </td>
                                <td style="width: 3px; height: 23px" width="3">
                                    <asp:Button ID="btnShpSvcCode" TabIndex="48" runat="server" CssClass="searchButton" CausesValidation="False" Text="..."></asp:Button></td>
                                <td style="width: 100px; height: 23px" width="36" colspan="2">
                                    <asp:TextBox ID="txtShpSvcDesc" runat="server" Visible="False" ReadOnly="True" CssClass="textField"
                                        Width="202px"></asp:TextBox></td>
                                <td style="width: 130px">
                                    <asp:Label ID="lblShipPickUpTime" runat="server" CssClass="tableLabel" Width="108px">Actual Pickup D/T</asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtShipPckUpTime" runat="server" CssClass="textField" Width="114px" AutoPostBack="True"></asp:TextBox></td>
                                <td></td>
                                <td style="width:200px"></td>
                            </tr>


                            <tr style="height: 25px">
                                <td style="width: 16px; height: 15px" width="16"></td>
                                <td style="height: 15px">
                                    <asp:Label ID="lblGenDes" runat="server" CssClass="tableLabel" Width="100px">General Description</asp:Label>
                                </td>
                                <td colspan="4" rowspan="2">
                                    <asp:TextBox ID="txtGenDes" runat="server" Rows="5" Width="95%" TextMode="MultiLine"></asp:TextBox>
                                </td>
                                <td style="width: 126px; height: 15px" width="126">
                                    <asp:Label ID="lblShipEstDlvryDt" TabIndex="108" runat="server" CssClass="tableLabel" Width="107px">Est. Delivery D/T</asp:Label>
                                </td>
                                <td style="height: 15px; width: 120px">
                                    <asp:TextBox ID="txtShipEstDlvryDt" runat="server" CssClass="textField" Width="114px" AutoPostBack="True"></asp:TextBox>
                                </td>
                                <td style="width: 100px; height: 15px" width="101">
                                    <asp:Label ID="lblEstHCDt" TabIndex="108" runat="server" CssClass="tableLabel" Width="107px">Est. HCR D/T</asp:Label>
                                </td>
                                <td style="height: 15px">
                                    <asp:TextBox ID="txtEstHCDt" runat="server" ReadOnly="True" CssClass="textField" Width="114px"
                                        AutoPostBack="True"></asp:TextBox>
                                </td>
                            </tr>

                            <tr style="height: 25px">
                                <td style="width: 16px; height: 15px" width="16"></td>
                                <td style="width: 80px; height: 15px"></td>
                                <td style="width: 126px; height: 15px" width="126">
                                    <asp:Label ID="Label1" TabIndex="108" runat="server" CssClass="tableLabel" Width="112px">Actual Delivery D/T</asp:Label>
                                </td>
                                <td style="height: 15px; width: 120px">
                                    <asp:TextBox ID="txtActDelDt" runat="server" ReadOnly="True" CssClass="textField" Width="114px"
                                        AutoPostBack="True"></asp:TextBox>
                                </td>
                                <td style="width: 100px; height: 15px" width="101">
                                    <asp:Label ID="lblActHCDt" TabIndex="108" runat="server" CssClass="tableLabel" Width="96px">Actual HCR D/T</asp:Label>
                                </td>
                                <td style="height: 15px">
                                    <asp:TextBox ID="txtActHCDt" runat="server" ReadOnly="True" CssClass="textField" Width="114px"
                                        AutoPostBack="True"></asp:TextBox>
                                </td>
                            </tr>


                            <tr style="height: 25px">
                                <td style="width: 16px; height: 15px" width="16"></td>
                                <td style="height: 15px">
                                    <asp:Label ID="lblShipDclValue" runat="server" CssClass="tableLabel" Width="120px">Declared Value THB</asp:Label></td>
                                <td style="width: 46px; height: 15px" width="46">
                                    <asp:TextBox ID="txtShpDclrValue" TabIndex="49" runat="server" CssClass="textFieldRightAlign"
                                        Width="74px" AutoPostBack="True"></asp:TextBox></td>
                                <td style="width: 3px; height: 15px" width="3"></td>
                                <td style="width: 130px;"><font face="Tahoma">
                                    <asp:Label ID="lblShipMaxCovg" runat="server" CssClass="tableLabel" Width="120px">Maximum Coverage</asp:Label></font></td>
                                <td style="width: 80px; height: 15px">
                                    <asp:TextBox ID="txtShpMaxCvrg" runat="server" ReadOnly="True" CssClass="textFieldRightAlign" Width="74px"></asp:TextBox>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="height: 25px">
                                <td style="width: 16px; height: 23px" width="16"></td>
                                <td style="height: 23px">
                                    <asp:Label ID="lblShipAdpercDV" runat="server" CssClass="tableLabel" Width="125px">Insurance Sur. %</asp:Label></td>
                                <td style="width: 46px; height: 23px" width="46">
                                    <asp:TextBox ID="txtShpAddDV" runat="server" ReadOnly="True" CssClass="textFieldRightAlign" Width="74px"></asp:TextBox></td>
                                <td style="width: 3px; height: 23px" width="3"></td>
                                <td style="height: 23px">
                                    <asp:Label ID="lblShipInsSurchrg" runat="server" CssClass="tableLabel" Width="120px"> Insurance Surcharge</asp:Label></td>
                                <td style="width: 19px; height: 23px" width="19">
                                    <asp:TextBox ID="txtShpInsSurchrg" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
                                        Width="74px"></asp:TextBox></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr style="height: 25px">
                                <td style="width: 16px; height: 12px" width="16"></td>
                                <td style="height: 12px">
                                    <asp:Label ID="Label5" runat="server" Visible="False" CssClass="tableLabel" Width="120px"> Other Surcharge</asp:Label></td>
                                <td style="width: 46px; height: 12px" width="46">
                                    <asp:TextBox ID="txtOtherSurcharge" runat="server" Visible="False" CssClass="textFieldRightAlign"
                                        Width="74px"></asp:TextBox></td>
                                <td style="width: 3px; height: 12px" width="3"></td>
                                <td style="width: 77px; height: 12px" width="77">
                                    <asp:Label ID="lblCODAmount" runat="server" CssClass="tableLabel" Width="88px">C.O.D. Amount</asp:Label></td>
                                <td style="width: 19px; height: 12px" width="19">
                                    <cc1:msTextBox ID="txtCODAmount" TabIndex="52" runat="server" CssClass="textFieldRightAlign" Width="74px"
                                        AutoPostBack="True" MaxLength="11" TextMaskType="msNumericCOD" NumberPrecision="10" NumberMaxValueCOD="99999999.99" NumberMinValue="0" NumberScale="2"></cc1:msTextBox></td>
                                <td></td>
                                <td>
                                    <asp:CheckBox ID="chkshpRtnHrdCpy" runat="server" CssClass="tableLabel" Width="14px" Text="HCR" AutoPostBack="True"></asp:CheckBox>
                                    <asp:CheckBox ID="chkInvHCReturn" runat="server" CssClass="tableLabel" Width="38px" Text="INVR" AutoPostBack="True"></asp:CheckBox>
                                </td>
                                <td colspan="2">
                                    <asp:RadioButton ID="rbtnShipFrghtPre" runat="server" CssClass="tableLabel" Width="112px" Text="Freight Prepaid"
                                        Height="16px" GroupName="Freight"></asp:RadioButton>
                                    <asp:RadioButton ID="rbtnShipFrghtColl" TabIndex="55" runat="server" CssClass="tableLabel" Width="108px"
                                        Text="Freight Collect" Height="16px" GroupName="Freight"></asp:RadioButton>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 16px; height: 12px" width="16"></td>
                                <td style="height: 12px"></td>
                                <td style="width: 46px; height: 12px" width="46"></td>
                                <td style="width: 3px; height: 12px" width="3"></td>
                                <td style="width: 77px; height: 12px" width="77"></td>
                                <td style="width: 19px; height: 12px" width="19"></td>
                                <td style="width: 126px; height: 12px" width="126" align="right"></td>
                                <td style="height: 12px" align="right"></td>
                                <td style="width: 101px; height: 12px" width="101"></td>
                                <td style="height: 12px"></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="width: 16px; height: 12px" width="16"></td>
                                <td style="width: 130px; height: 12px" width="114"><font face="Tahoma">
                                    <asp:CheckBox ID="cbIntDoc" runat="server" CssClass="tableLabel" Width="120px" Text="Int'l Documents"
                                        Height="25px" AutoPostBack="True"></asp:CheckBox></font></td>
                                <td style="width: 74px; height: 12px" width="74"><font face="Tahoma">
                                    <asp:CheckBox ID="cbExport" runat="server" CssClass="tableLabel" Width="96px" Text="Export" Height="25px"
                                        AutoPostBack="True"></asp:CheckBox></font></td>
                                <td style="width: 81px; height: 12px" width="81">
                                    <asp:Label Style="z-index: 0" ID="Label4" runat="server" CssClass="tableLabel" Width="60px">MAWB No.</asp:Label></td>
                                <td style="width: 77px; height: 12px" width="77">
                                    <asp:TextBox Style="z-index: 0" ID="txtMAWBNo" runat="server" ReadOnly="True" CssClass="textField"
                                        Width="114px" AutoPostBack="True"></asp:TextBox></td>
                                <td colspan="2" style="width: 250px">
                                    <asp:Label Style="z-index: 0" ID="Label7" runat="server" CssClass="tableLabel" Width="232px">Out-forwarding Freight Charge Paid by</asp:Label></td>
                                <td style="width: 101px; height: 12px" width="101">
                                    <asp:TextBox Style="z-index: 0" ID="txtPayerid1" runat="server" ReadOnly="True" CssClass="textField" Width="114px" AutoPostBack="True"></asp:TextBox>
                                </td>
                                <td style="height: 12px">
                                    <asp:TextBox Style="z-index: 0" ID="txtPayerid2" runat="server" ReadOnly="True" CssClass="textField" Width="192px" AutoPostBack="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 16px; height: 12px" width="16"></td>
                                <td style="width: 114px; height: 12px" width="114"></td>
                                <td style="width: 74px; height: 12px" width="74" colspan="3"></td>
                                <td style="width: 81px; height: 12px" width="81"></td>
                                <td style="width: 77px; height: 12px" width="77"></td>
                                <td style="width: 19px; height: 12px" width="19"></td>
                                <td style="width: 126px; height: 12px" width="126"></td>
                                <td style="width: 101px; height: 12px" width="101"></td>
                                <td style="height: 12px"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%--  HC Return Task --%>                <%--  HC Return Task --%>                <%--  HC Return Task --%>
                <tr>
                    <td style="width: 738px; height: 32px" valign="baseline" width="738" align="left">
                        <table style="width: 947px; height: 4px" id="Table7" border="0" cellspacing="1" cellpadding="1"
                            width="0">
                            <tr>
                                <td style="width: 62px">
                                    <p>
                                        <asp:Label ID="lblSpeHanInstruction" runat="server" CssClass="tableLabel" Width="72px">Special Del. Instructions</asp:Label>
                                    </p>
                                </td>
                                <td><font face="Tahoma">
                                    <p>
                                        <asp:TextBox ID="txtSpeHanInstruction" TabIndex="56" runat="server" Width="864px" Height="36px"
                                            AutoPostBack="True" MaxLength="200" TextMode="MultiLine"></asp:TextBox>
                                    </p>
                                </font>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%--  HC Return Task --%>
                <tr valign="top">
                    <td style="width: 738px;" width="738" align="left">
                        <table>
                            <tr>
                                <td>
                                    <asp:DataGrid ID="dgVAS" TabIndex="115" runat="server" Width="722px" OnItemDataBound="dgVAS_ItemDataBound"
                                        OnItemCommand="dgVAS_Button" AutoGenerateColumns="False" OnEditCommand="dgVAS_Edit" AllowPaging="True" OnPageIndexChanged="dgVAS_PageChange" OnCancelCommand="dgVAS_Cancel"
                                        OnDeleteCommand="dgVAS_Delete" OnUpdateCommand="dgVAS_Update" PageSize="4" ItemStyle-Height="20px" HeaderStyle-Height="20px">
                                        <Columns>
                                            <asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton"
                                                CommandName="Delete">
                                                <HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
                                                <ItemStyle CssClass="gridField"></ItemStyle>
                                            </asp:ButtonColumn>
                                            <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0  title='Update' &gt;"
                                                CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel'&gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0  title='Edit' &gt;">
                                                <HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
                                                <ItemStyle CssClass="gridField"></ItemStyle>
                                            </asp:EditCommandColumn>
                                            <asp:TemplateColumn HeaderText="Surcharge Code">
                                                <HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
                                                <ItemStyle CssClass="gridField"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblVASCode" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <cc1:msTextBox CssClass="gridTextBox" runat="server" ID="txtVASCode" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>'>
                                                    </cc1:msTextBox>
                                                    <asp:RequiredFieldValidator ID="validVASCode" runat="server" BorderWidth="0" Display="None" ControlToValidate="txtVASCode"
                                                        ErrorMessage="The VAS Code is required"></asp:RequiredFieldValidator>
                                                </EditItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton"
                                                CommandName="Search">
                                                <HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
                                                <ItemStyle CssClass="gridField"></ItemStyle>
                                            </asp:ButtonColumn>
                                            <asp:TemplateColumn HeaderText="Description">
                                                <HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
                                                <ItemStyle CssClass="gridField"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" CssClass="gridLabel" ID="lblVASDesc" Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox CssClass="gridTextBox" runat="server" ID="txtVASDesc" Enabled="True" ReadOnly="True" EnableViewState="True" Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>'>
                                                    </asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Amount">
                                                <HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
                                                <ItemStyle CssClass="gridField"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" CssClass="gridLabelNumber" ID="lblSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge",(String)ViewState["m_format"])%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtSurcharge" CssClass="gridTextBoxNumber" runat="server" Enabled="False" EnableViewState="True" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge",(String)ViewState["m_format"])%>'>
                                                    </asp:TextBox>
                                                    <asp:TextBox ID="txtSurchargeHinden" Style="display: none" runat="server" Enabled="True" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge",(String)ViewState["m_format"])%>'>
                                                    </asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Remarks">
                                                <HeaderStyle Width="56%" CssClass="gridHeading"></HeaderStyle>
                                                <ItemStyle CssClass="gridField"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" CssClass="gridLabel" ID="lblRemarks" Text='<%#DataBinder.Eval(Container.DataItem,"remarks")%>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox CssClass="gridTextBox" runat="server" ID="txtRemarks" Text='<%#DataBinder.Eval(Container.DataItem,"remarks")%>'>
                                                    </asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <PagerStyle NextPageText="Next" Font-Size="Smaller" PrevPageText="Previous" HorizontalAlign="Right"
                                            Height="20"></PagerStyle>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td align="right">
                                    <asp:Button ID="btnDGInsert" TabIndex="59" runat="server" CssClass="queryButton" Width="69px"  CausesValidation="False" Text="Insert"></asp:Button></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr valign="top">
                    <td style="width: 738px" width="738" align="left">
                        <table style="z-index: 120; width: 737px; height: 158px" id="tblCharges" runat="server">
                            <tr style="height:40px;">
                                <td style="width: 220px"></td>
                                <td align="right">
                                    <asp:Label Style="z-index: 0" ID="Label8" TabIndex="121" runat="server" CssClass="tableLabel"
                                        Height="20px" Font-Bold="True">Final Ratings</asp:Label></td>
                                <td style="width:60px"></td>
                                <td align="right">
                                    <asp:Label Style="z-index: 0" ID="lbOverridden" TabIndex="121" runat="server" CssClass="tableLabel"
                                        Width="146px" Height="20px" Font-Bold="True">Overridden Ratings</asp:Label></td>
                                <td></td>
                                <td align="right">
                                    <asp:Label Style="z-index: 0" ID="Label10" TabIndex="121" runat="server" CssClass="tableLabel"
                                        Width="152px" Height="20px" Font-Bold="True">Post-Invoicing Charges</asp:Label></td>
                            </tr>
                            <tr>
                                <td style="width: 144px" width="144">
                                    <asp:Label Style="z-index: 0" ID="Label11" TabIndex="117" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px">Basic Charge</asp:Label></td>
                                <td>
                                    <asp:TextBox Style="z-index: 0" ID="txtBasicCharge" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
                                        Width="131px"></asp:TextBox></td>
                                <td></td>
                                <td align="right">
                                    <asp:TextBox Style="z-index: 0" ID="txtOvrBasicCharge" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
                                        Width="131px"></asp:TextBox></td>
                                <td>
                                    <asp:Label Style="z-index: 0" ID="lblPODEX" TabIndex="121" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px">PODEX Surcharges</asp:Label></td>
                                <td>
                                    <asp:TextBox Style="z-index: 0" ID="txtPODEX" TabIndex="122" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
                                        Width="131px"></asp:TextBox></td>
                            </tr>
                            <tr style="height:25px">
                                <td>
                                    <asp:Label ID="lblFreightChrg" TabIndex="117" runat="server" CssClass="tableLabel" Width="141px"
                                        Height="20px">Freight Charge</asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtFreightChrg" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
                                        Width="131px"></asp:TextBox></td>
                                <td></td>
                                <td align="right">
                                    <asp:TextBox ID="txtOriFreightChrg" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
                                        Width="131px"></asp:TextBox></td>
                                <td>
                                    <asp:Label Style="z-index: 0" ID="lblMBGAmt" TabIndex="123" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px">MBG Amount</asp:Label></td>
                                <td>
                                    <asp:TextBox Style="z-index: 0" ID="txtMBGAmt" TabIndex="124" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                            </tr>

                             <tr style="height:25px">
                                <td>
                                    <asp:Label ID="lblInsChrg" TabIndex="119" runat="server" CssClass="tableLabel" Width="141px"
                                        Height="20px">Insurance Surcharge</asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtInsChrg" TabIndex="120" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
                                        Width="131px"></asp:TextBox></td>
                                <td></td>
                                <td align="right">
                                    <asp:TextBox ID="txtOriInsChrg" TabIndex="120" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
                                        Width="131px"></asp:TextBox></td>
                                <td>
                                    <asp:Label Style="z-index: 0" ID="lblOtherInvC_D" TabIndex="125" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px">Other Invoice Credit/Debit</asp:Label></td>
                                <td>
                                    <asp:TextBox Style="z-index: 0" ID="txtOtherInvC_D" TabIndex="126" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                            </tr>
                            <%--By Aoo 22/02/2008--%>
                            <tr>
                                <td>
                                    <asp:Label Style="z-index: 0" ID="Label12" TabIndex="119" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px">Non-VAS Surcharges</asp:Label></td>
                                <td style="height: 22px">
                                    <asp:TextBox Style="z-index: 0" ID="txtNonVASSurcharges" TabIndex="120" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                                <td style="height: 22px"></td>
                                <td style="height: 22px" align="right">
                                    <asp:TextBox Style="z-index: 0" ID="txtOvrrNonVASSurcharges" TabIndex="120" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                                <td style="height: 22px">
                                    <asp:Label Style="z-index: 0" ID="lblTotalInvAmt" TabIndex="121" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px">Total Invoiced Amount</asp:Label></td>
                                <td style="height: 22px">
                                    <asp:TextBox Style="z-index: 0" ID="txtTotalInvAmt" TabIndex="122" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                            </tr>
                             <tr style="height:25px">
                                <td style="width: 144px" width="144">
                                    <asp:Label Style="z-index: 0" ID="lblTotVASSurChrg" TabIndex="121" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px"> VAS Surcharges</asp:Label></td>
                                <td>
                                    <asp:TextBox Style="z-index: 0" ID="txtTotVASSurChrg" TabIndex="122" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                                <td></td>
                                <td align="right">
                                    <asp:TextBox Style="z-index: 0" ID="txtOritotVas" TabIndex="124" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                                <td>
                                    <asp:Label Style="z-index: 0" ID="lblCreditNotes" TabIndex="123" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px">Credit Notes</asp:Label></td>
                                <td>
                                    <asp:TextBox Style="z-index: 0" ID="txtCreditNotes" TabIndex="124" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                            </tr>
                            <%--End By Aoo 22/02/2008--%>
                             <tr style="height:25px">
                                <td style="width: 144px" width="144">
                                    <asp:Label Style="z-index: 0" ID="lblESASurchrg" TabIndex="123" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px">ESA Surcharge</asp:Label></td>
                                <td>
                                    <asp:TextBox Style="z-index: 0" ID="txtESASurchrg" TabIndex="124" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                                <td></td>
                                <td align="right">
                                    <asp:TextBox Style="z-index: 0" ID="txtOriESASurchrg" TabIndex="124" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                                <td>
                                    <asp:Label Style="z-index: 0" ID="lblDebitNotes" TabIndex="125" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px">Debit Notes</asp:Label></td>
                                <td>
                                    <asp:TextBox Style="z-index: 0" ID="txtDebitNotes" TabIndex="126" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                            </tr>
                             <tr style="height:25px">
                                <td style="width: 144px" width="144">
                                    <asp:Label Style="z-index: 0" ID="lblShpTotAmt" TabIndex="125" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px">Total Rated Amount</asp:Label></td>
                                <td>
                                    <asp:TextBox Style="z-index: 0" ID="txtShpTotAmt" TabIndex="126" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                                <td></td>
                                <td align="center">
                                    <asp:Label Style="z-index: 0" ID="lblRateOverrideBy" TabIndex="121" runat="server" CssClass="tableLabel"
                                        Height="20px">Override Applied by User</asp:Label></td>
                                <td>
                                    <asp:Label Style="z-index: 0" ID="lblTotalConRevenue" TabIndex="121" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px">Total Consignment Revenue</asp:Label></td>
                                <td>
                                    <asp:TextBox Style="z-index: 0" ID="txtTotalConRevenue" TabIndex="122" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                            </tr>
                             <tr style="height:25px">
                                <td style="width: 144px" width="144">
                                    <asp:Label Style="z-index: 0" ID="Label13" TabIndex="125" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px">GST on Rated Amount</asp:Label></td>
                                <td>
                                    <asp:TextBox Style="z-index: 0" ID="txtGST" TabIndex="126" runat="server" ReadOnly="True" CssClass="textFieldRightAlign"
                                        Width="131px"></asp:TextBox></td>
                                <td></td>
                                <td align="right">
                                    <asp:TextBox Style="z-index: 0" ID="txtRateOverrideBy" TabIndex="124" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px" Enabled="False"></asp:TextBox></td>
                                <td>
                                    <asp:Label Style="z-index: 0" ID="lblPaidRevenue" TabIndex="121" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px">Paid Revenue</asp:Label></td>
                                <td>
                                    <asp:TextBox Style="z-index: 0" ID="txtPaidRevenue" TabIndex="122" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                            </tr>
                            <%--By Aoo 22/02/2008--%>
                             <tr style="height:25px">
                                <td style="width: 144px" width="144">
                                    <asp:Label Style="z-index: 0" ID="Label14" TabIndex="125" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px">Export (International) Freight Charge</asp:Label></td>
                                <td>
                                    <asp:TextBox Style="z-index: 0" ID="txtExportInterFreiChar" TabIndex="126" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                                <td></td>
                                <td align="center">
                                    <asp:Label Style="z-index: 0" ID="lblRateOverrideDate" TabIndex="121" runat="server" CssClass="tableLabel"
                                        Height="20px">Override Applied on D/T</asp:Label></td>
                                <td></td>
                                <td></td>
                            </tr>
                             <tr style="height:25px">
                                <td style="width: 144px" width="144">
                                    <asp:Label Style="z-index: 0" ID="Label15" TabIndex="119" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px">Customs Fees</asp:Label></td>
                                <td>
                                    <asp:TextBox Style="z-index: 0" ID="txtCustomsFees" TabIndex="126" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                                <td></td>
                                <td align="right">
                                    <asp:TextBox Style="z-index: 0" ID="txtRateOverrideDate" TabIndex="124" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px" Enabled="False"></asp:TextBox></td>
                                <td></td>
                                <td></td>
                            </tr>
                             <tr style="height:25px">
                                <td style="width: 144px" width="144">
                                    <asp:Label Style="z-index: 0" ID="Label16" TabIndex="119" runat="server" CssClass="tableLabel"
                                        Width="141px" Height="20px">Freight Collect Charges</asp:Label></td>
                                <td>
                                    <asp:TextBox Style="z-index: 0" ID="txtFreCollChar" TabIndex="126" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                                <td></td>

                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                             <tr style="height:25px">
                                <td style="width: 144px" width="144">
                                    <asp:Label Style="z-index: 0" ID="lblOtherSurcharge" TabIndex="119" runat="server" Visible="False"
                                        CssClass="tableLabel" Width="141px" Height="20px">Other Surcharge</asp:Label></td>
                                <td width="20%">
                                    <asp:TextBox Style="z-index: 0" ID="txtOtherSurcharge2" TabIndex="120" runat="server" Visible="False"
                                        ReadOnly="True" CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                             <tr style="height:25px">
                                <td style="width: 144px" width="144"></td>
                                <td></td>
                                <td></td>
                                <td align="right">
                                    <asp:Label Style="z-index: 0" ID="lblOritotVas" TabIndex="121" runat="server" Visible="False"
                                        CssClass="tableLabel" Height="12px"></asp:Label></td>
                                <td>
                                    <asp:TextBox Style="z-index: 0" ID="txtOriShpTotAmt" TabIndex="126" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                                <td></td>
                            </tr>
                             <tr style="height:25px">
                                <td style="width: 144px" width="144"></td>
                                <td></td>
                                <td></td>
                                <td align="right">
                                    <asp:TextBox Style="z-index: 0" ID="txtOriOthersurcharge2" TabIndex="120" runat="server" ReadOnly="True"
                                        CssClass="textFieldRightAlign" Width="131px"></asp:TextBox></td>
                                <td></td>
                                <td></td>
                            </tr>
                             <tr style="height:25px">
                                <td style="width: 144px" width="144"></td>
                                <td></td>
                                <td></td>
                                <td align="right"></td>
                                <td>
                                    <asp:Label Style="z-index: 0" ID="lblRateOverrideByValue" TabIndex="121" runat="server" CssClass="tableLabel"
                                        Height="20px" Font-Bold="True"></asp:Label></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="width: 144px" width="144"><font face="Tahoma"></font></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:Label Style="z-index: 0" ID="lblRateOverrideDateValue" TabIndex="121" runat="server" CssClass="tableLabel"
                                        Height="20px" Font-Bold="True"></asp:Label></td>
                                <td></td>
                            </tr>
                            <%--End By Aoo 22/02/2008--%>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        &nbsp;
			<div style="z-index: 101; position: relative; width: 444px; height: 224px; top: 46px; left: 70px; relative: absolute"
                id="DomstcShipPanel" ms_positioning="GridLayout" runat="server">
                <fieldset style="z-index: 80; position: relative; width: 374px; height: 178px; top: 46px; left: 70px; relative: absolute"
                    ms_positioning="GridLayout">
                    <legend>
                        <asp:Label ID="lblConf" CssClass="tableHeadingFieldset" runat="server">Confirmation</asp:Label></legend>
                    <table style="z-index: 75; position: absolute; width: 346px; height: 153px; top: 23px; left: 7px"
                        id="Table1" runat="server">
                        <tr>
                            <td>
                                <p align="center">
                                    <asp:Label ID="lblConfirmMsg" runat="server" Width="321px"></asp:Label>
                                </p>
                                <p>
                                <p align="center">
                                    <asp:Button ID="btnToSaveChanges" runat="server" CssClass="queryButton" Text="Yes"></asp:Button><asp:Button ID="btnNotToSave" runat="server" CssClass="queryButton" CausesValidation="False"
                                        Text="No"></asp:Button><asp:Button ID="btnToCancel" runat="server" CssClass="queryButton" CausesValidation="False"
                                            Text="Cancel"></asp:Button>
                                </p>
                                <p></p>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </div>
        <div style="z-index: 104; position: relative; width: 637px; height: 176px; top: 46px; left: 7px"
            id="divCustIdCng" ms_positioning="GridLayout" runat="server">
            <table style="z-index: 105; position: absolute; width: 530px; height: 129px; top: 16px; left: 43px"
                id="Table2" runat="server">
                <tr>
                    <td>
                        <p>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:Label ID="lblCustIdCngMsg" runat="server" Width="337px" Height="46px">All changes cannot be reverted back <br>Do you want to change to another customer ID ?</asp:Label>
                        </p>
                        <p>
                        <p>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:Button ID="btnCustIdChgYes" runat="server" CssClass="queryButton" CausesValidation="False"
                                    Text="Yes"></asp:Button>&nbsp;
								<asp:Button ID="btnCustIdChgNo" runat="server" CssClass="queryButton" CausesValidation="False"
                                    Text=" No "></asp:Button>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
        <div style="z-index: 106; position: relative; width: 625px; height: 217px; top: 46px; left: 22px"
            id="divDelOperation" ms_positioning="GridLayout" runat="server">
            <table style="z-index: 109; position: absolute; width: 545px; height: 107px; top: 20px; left: 21px"
                id="Table3" runat="server">
                <tr>
                    <td>
                        <p align="center">
                            <asp:Label ID="lblDelOperation" runat="server" Width="400px" Height="36px">Record Deleted cannot be reverted back. Confirm Deletion ?</asp:Label>
                        </p>
                        <p align="center">
                            <asp:Button ID="btnDelOperationYes" runat="server" CssClass="queryButton" CausesValidation="False"
                                Text="Yes"></asp:Button>&nbsp;
								<asp:Button ID="btnDelOperationNo" runat="server" CssClass="queryButton" CausesValidation="False"
                                    Text=" No "></asp:Button>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
        <div style="z-index: 105; position: relative; width: 625px; height: 217px; top: 46px; left: 22px"
            id="divUpdateAutoManifest" ms_positioning="GridLayout" runat="server">
            <table style="z-index: 109; position: absolute; width: 545px; height: 107px; top: 20px; left: 21px"
                id="Table4" runat="server">
                <tr>
                    <td>
                        <p align="center">
                            <asp:Label ID="Label2" runat="server" Width="400px" Height="36px">Shipment was originally manifested on a different route /
day. This shipment will be reassigned to a new route.
You must manually remove the shipment from prior
linehaul and / or delivery manifests.</asp:Label>
                        </p>
                        <p align="center">
                            <asp:Button ID="btnUpdateAutoManifest" runat="server" CssClass="queryButton" CausesValidation="False"
                                Text="OK"></asp:Button>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
        <div style="z-index: 103; position: relative; width: 439px; height: 218px; top: 40px; left: 46px"
            id="ExceedTimeState" ms_positioning="GridLayout" runat="server">
            <font face="Tahoma"></font><font face="Tahoma"></font>
            <br>
            <p align="center">
                <asp:Label Style="z-index: 110; position: absolute; top: 39px; left: 41px" ID="lbl_ExceedTimeState"
                    runat="server" Width="359px">The Estimated Pickup Date/Time exceeds the Cut-off time for Sender Postal code. To override the standard cut-off time and still schecule the pick up at the time you have entered press Cancel. To reschedule the pickup before the Cut-off time press OK.</asp:Label>
            </p>
            <p align="center">
                <asp:Button Style="z-index: 101; position: absolute; top: 153px; left: 220px" ID="btnExceedCancel"
                    runat="server" CssClass="queryButton" Width="65px" CausesValidation="False" Text="Cancel"></asp:Button>
                <asp:Button Style="z-index: 103; position: absolute; top: 153px; left: 169px" ID="btnExceedOK"
                    runat="server" CssClass="queryButton" Width="44px" CausesValidation="False" Text="OK"></asp:Button>
            </p>
        </div>
        <input
            value="<%=strScrollPosition%>" type="hidden" name="ScrollPosition">
        <input type="hidden" name="hdnRefresh">
    </form>
</body>
</html>
