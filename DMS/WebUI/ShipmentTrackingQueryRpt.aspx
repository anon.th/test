<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo"%>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<%@ Page language="c#" Codebehind="ShipmentTrackingQueryRpt.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ShipmentTrackingQueryRpt"  EnableEventValidation="False" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentTrackingQuery</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
				<script language="javascript">
			function makeUppercase(objId)
			{
				document.getElementById(objId).value = document.getElementById(objId).value.toUpperCase();
			}
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="ShipmentTrackingQueryRpt" method="post" runat="server">
			<asp:button style="Z-INDEX: 100; POSITION: absolute; TOP: 40px; LEFT: 20px" id="btnQuery" runat="server"
				Text="Query" CssClass="queryButton" Width="64px"></asp:button>
			<TABLE style="Z-INDEX: 106; POSITION: absolute; WIDTH: 797px; HEIGHT: 442px; TOP: 80px; LEFT: 16px"
				id="tblExternal" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 400px; HEIGHT: 137px" vAlign="top">
						<FIELDSET style="WIDTH: 390px; HEIGHT: 129px"><LEGEND><asp:label id="lblDateCust" runat="server" CssClass="tableHeadingFieldset" Width="30px" Font-Bold="True">Dates</asp:label></LEGEND>
							<TABLE style="WIDTH: 360px; HEIGHT: 91px" id="Table3" border="0" cellSpacing="0" cellPadding="0"
								align="left" runat="server">
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbBookingDateCust" runat="server" Text="Booking Date" CssClass="tableRadioButton"
											Width="115px" AutoPostBack="True" GroupName="QueryByDate" Checked="True" Height="22px"></asp:radiobutton><asp:radiobutton id="rbPickupDateCust" runat="server" Text="Pickup Date" CssClass="tableRadioButton"
											Width="113px" AutoPostBack="True" GroupName="QueryByDate" Height="22px"></asp:radiobutton><asp:radiobutton id="rbStatusDateCust" runat="server" Text="Status Date" CssClass="tableRadioButton"
											Width="94px" AutoPostBack="True" GroupName="QueryByDate" Height="22px"></asp:radiobutton></TD>
									<td></td>
								</TR>
								<TR>
									<TD></TD>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonthCust" runat="server" Text="Month" CssClass="tableRadioButton" Width="73px"
											AutoPostBack="True" GroupName="EnterDate" Height="21px"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonthCust" runat="server" CssClass="textField" Width="88px" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYearCust" runat="server" CssClass="tableLabel" Width="30px" Height="17px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYearCust" runat="server" CssClass="textField" Width="83" NumberPrecision="4"
											NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px"></TD>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriodCust" runat="server" Text="Period" CssClass="tableRadioButton" Width="62px"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriodCust" runat="server" CssClass="textField" Width="82" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtToCust" runat="server" CssClass="textField" Width="83" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD style="HEIGHT: 20px"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDateCust" runat="server" Text="Date" CssClass="tableRadioButton" Width="74px"
											AutoPostBack="True" GroupName="EnterDate" Height="11px"></asp:radiobutton>&nbsp;
										<cc1:mstextbox id="txtDateCust" runat="server" CssClass="textField" Width="80px" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
					<TD style="HEIGHT: 137px" vAlign="top">&nbsp;
						<FIELDSET style="WIDTH: 320px; HEIGHT: 89px" DESIGNTIMEDRAGDROP="281"><LEGEND><asp:label id="Label8" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Destination</asp:label></LEGEND>
							<TABLE style="WIDTH: 300px; HEIGHT: 89px" id="Table6" border="0" cellSpacing="0" cellPadding="0"
								align="left" runat="server">
								<TR height="37">
									<TD></TD>
									<TD style="WIDTH: 360px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblPostalCodeCust" runat="server" CssClass="tableLabel" Width="84px" Height="18px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCodeCust" runat="server" CssClass="textField" Width="139px" TextMaskType="msUpperAlfaNumericWithHyphen"
											MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCodeCust" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD style="WIDTH: 360px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblStateCust" runat="server" CssClass="tableLabel" Width="100px" Height="16px"> Province</asp:label><cc1:mstextbox id="txtStateCodeCust" runat="server" CssClass="textField" Width="139" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCodeCust" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
			</TABLE>
			<asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 71px; LEFT: 20px" id="lblErrorMessage"
				runat="server" CssClass="errorMsgColor" Width="700px" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 40px; LEFT: 84px" id="btnExecuteQuery"
				runat="server" Text="Execute Query" CssClass="queryButton" Width="123px"></asp:button><asp:button style="Z-INDEX: 102; POSITION: absolute; TOP: 40px; LEFT: 234px" id="btnCancelQry"
				runat="server" Text="Cancel Query" CssClass="queryButton" Visible="False" Enabled="False"></asp:button>
			<TABLE style="Z-INDEX: 104; POSITION: absolute; WIDTH: 800px; HEIGHT: 500px; TOP: 84px; LEFT: 14px"
				id="tblInternal" border="0" width="800" runat="server">
				<TR>
					<TD style="WIDTH: 468px; HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 456px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="20px" Font-Bold="True">Dates</asp:label></legend>
							<TABLE style="WIDTH: 360px; HEIGHT: 91px" id="tblDates" border="0" cellSpacing="0" cellPadding="0"
								align="left" runat="server">
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbBookingDate" runat="server" Text="Booking Date" CssClass="tableRadioButton"
											Width="115px" AutoPostBack="True" GroupName="QueryByDate" Checked="True" Height="22px"></asp:radiobutton><asp:radiobutton id="rbPkDate" runat="server" Text="Pickup Date" CssClass="tableRadioButton" Width="119px"
											AutoPostBack="True" GroupName="QueryByDate" Height="22px"></asp:radiobutton><asp:radiobutton id="rbStatusDate" runat="server" Text="Status Date" CssClass="tableRadioButton"
											Width="94px" AutoPostBack="True" GroupName="QueryByDate" Height="22px"></asp:radiobutton></TD>
									<td></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Text="Month" CssClass="tableRadioButton" Width="73px"
											AutoPostBack="True" GroupName="EnterDate" Checked="True" Height="21px"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Width="88px" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" CssClass="tableLabel" Width="30px" Height="22px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" NumberPrecision="4"
											NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></TD>
									<td></td>
								</TR>
								<TR>
									<td style="HEIGHT: 20px"></td>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Text="Period" CssClass="tableRadioButton" Width="62px"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="HEIGHT: 20px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Text="Date" CssClass="tableRadioButton" Width="74px"
											AutoPostBack="True" GroupName="EnterDate" Height="22px"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="82" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 105px"><legend><asp:label id="lblPayerType" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Payer Type</asp:label></legend>
							<TABLE style="WIDTH: 300px; HEIGHT: 73px" id="tblPayerType" border="0" cellSpacing="0"
								cellPadding="0" align="left" runat="server">
								<tr>
									<td></td>
									<td height="1" colSpan="2">&nbsp;</td>
									<td></td>
									<td></td>
								</tr>
								<TR>
									<td></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="Label4" runat="server" CssClass="tableLabel" Width="88px" Height="22px">Payer Type</asp:label></TD>
									<td><asp:listbox id="lsbCustType" runat="server" Width="137px" Height="61px" SelectionMode="Multiple"></asp:listbox></td>
									<td></td>
								</TR>
								<TR height="33">
									<td bgColor="blue"></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="lblPayerCode" runat="server" CssClass="tableLabel" Width="79px" Height="22px">Payer Code</asp:label>&nbsp;&nbsp;</TD>
									<td><asp:textbox id="txtPayerCode" runat="server" CssClass="textField" Width="148px"></asp:textbox>&nbsp;&nbsp;
										<asp:button id="btnPayerCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></td>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 468px; HEIGHT: 134px" vAlign="top">
						<fieldset style="WIDTH: 457px; HEIGHT: 127px"><legend><asp:label id="lblRouteType" runat="server" CssClass="tableHeadingFieldset" Width="145px" Font-Bold="True">Route / DC Selection</asp:label></legend>
							<TABLE style="WIDTH: 447px; HEIGHT: 113px" id="tblRouteType" border="0" cellSpacing="0"
								cellPadding="0" align="left" runat="server">
								<TR>
									<TD style="WIDTH: 447px" class="tableLabel" colSpan="2">&nbsp;
										<asp:radiobutton id="rbLongRoute" runat="server" Text="Linehaul" CssClass="tableRadioButton" Width="142px"
											AutoPostBack="True" GroupName="QueryByRoute" Checked="True" Height="22px"></asp:radiobutton><asp:radiobutton id="rbShortRoute" runat="server" Text="Delivery/CL" CssClass="tableRadioButton"
											Width="131px" AutoPostBack="True" GroupName="QueryByRoute" Height="22px"></asp:radiobutton><asp:radiobutton id="rbAirRoute" runat="server" Text="Air Route" CssClass="tableRadioButton" Width="152px"
											AutoPostBack="True" GroupName="QueryByRoute" Height="22px"></asp:radiobutton></TD>
								</TR>
								<tr>
									<TD class="tableLabel" width="20%">&nbsp;<asp:label id="lblRouteCode" runat="server" CssClass="tableLabel" Width="111px" Height="22px">Route Code</asp:label></TD>
									<td style="WIDTH: 260px"><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></td>
								</tr>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label2" runat="server" CssClass="tableLabel" Width="154px" Height="22px">Origin Distribution Center</asp:label></TD>
									<TD style="WIDTH: 260px"><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelectOrigin"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label3" runat="server" CssClass="tableLabel" Width="181px" Height="22px">Destination Distribution Center</asp:label></TD>
									<TD style="WIDTH: 260px"><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelectDestination"></DBCOMBO:DBCOMBO></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 134px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 89px"><legend><asp:label id="lblDestination" runat="server" CssClass="tableHeadingFieldset" Width="79px"
									Font-Bold="True">Destination</asp:label></legend>
							<TABLE style="WIDTH: 300px; HEIGHT: 89px" id="tblDestination" border="0" cellSpacing="0"
								cellPadding="0" align="left" runat="server">
								<tr height="37">
									<td></td>
									<TD style="WIDTH: 360px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblZipCode" runat="server" CssClass="tableLabel" Width="84px" Height="22px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCode" runat="server" CssClass="textField" Width="139px" TextMaskType="msUpperAlfaNumericWithHyphen"
											MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<TD style="WIDTH: 360px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblStateCode" runat="server" CssClass="tableLabel" Width="100px" Height="22px">State / Province</asp:label><cc1:mstextbox id="txtStateCode" runat="server" CssClass="textField" Width="139" Height="22" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" colSpan="2">
						<fieldset style="WIDTH: 692px; HEIGHT: 152px"><legend><asp:label id="lblShipment" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Shipment</asp:label></legend>
							<TABLE style="WIDTH: 680px; HEIGHT: 121px" id="tblShipment" border="0" cellSpacing="0"
								cellPadding="0" width="680" align="left" runat="server">
								<tr>
									<td style="HEIGHT: 26px"></td>
									<TD style="HEIGHT: 26px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblBookingType" runat="server" CssClass="tableLabel" Width="119px" Height="22px">Booking Type</asp:label><asp:dropdownlist id="Drp_BookingType" runat="server" CssClass="textField" Width="161px"></asp:dropdownlist>
									</TD>
									<td style="HEIGHT: 26px"></td>
								</tr>
								<tr>
									<td style="HEIGHT: 30px"></td>
									<TD style="HEIGHT: 30px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblConsignmentNo" runat="server" CssClass="tableLabel" Width="110px" Height="18px">Consignment No</asp:label>&nbsp;&nbsp;
										<asp:textbox id="txtConsignmentNo" runat="server" CssClass="textField" Width="161px"
											MaxLength="30"><%-- Pongsakorn.s delete AutoPostBack="True"--%></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:label id="lblBookingNo" runat="server" CssClass="tableLabel" Width="118px" Height="22px">Booking No</asp:label>&nbsp;<cc1:mstextbox id="txtBookingNo" runat="server" CssClass="textField" Width="134px"
											NumberPrecision="10" NumberMaxValue="2147483647" TextMaskType="msNumeric" MaxLength="10"><%-- Pongsakorn.s delete AutoPostBack="True"--%></cc1:mstextbox></TD>
									<td style="HEIGHT: 30px"></td>
								</tr>
								<tr>
									<td style="HEIGHT: 29px"></td>
									<TD style="HEIGHT: 29px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblStatusCode" runat="server" CssClass="tableLabel" Width="89px" Height="22px">Status Code</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtStatusCode" runat="server" CssClass="textField" Width="161px" AutoPostBack="True"
											TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="10"></cc1:mstextbox>&nbsp;
										<asp:button id="btnStatusCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;<asp:label id="lblExceptionCode" runat="server" CssClass="tableLabel" Width="89px" Height="22px">Exception Code</asp:label>&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;&nbsp;
										<cc1:mstextbox id="txtExceptionCode" runat="server" CssClass="textField" Width="134px" AutoPostBack="True"
											TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="10"></cc1:mstextbox><asp:button id="btnExceptionCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td style="HEIGHT: 29px"></td>
								</tr>
								<tr>
									<td style="HEIGHT: 30px"></td>
									<TD style="HEIGHT: 30px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblShipmentClosed" runat="server" CssClass="tableLabel" Width="120px" Height="17px">Shipment Closed</asp:label><asp:radiobutton id="rbShipmentYes" runat="server" Text="Yes" CssClass="tableRadioButton" Width="39px"
											AutoPostBack="True" GroupName="QueryByShipment" Height="22px" Enabled="False"></asp:radiobutton><asp:radiobutton id="rbShipmentNo" runat="server" Text="No" CssClass="tableRadioButton" Width="87px"
											AutoPostBack="True" GroupName="QueryByShipment" Height="22px" Enabled="False"></asp:radiobutton>
                                        <asp:label id="lblInvoiceIssued" runat="server" CssClass="tableLabel" Width="98px" Height="17px">Invoice Issued</asp:label>
                                        <asp:radiobutton id="rbInvoiceYes" runat="server" Text="Yes" CssClass="tableRadioButton" Width="58px"
											AutoPostBack="True" GroupName="QueryByInvoice" Height="22px"></asp:radiobutton><asp:radiobutton id="rbInvoiceNo" runat="server" Text="No" CssClass="tableRadioButton" Width="60px"
											AutoPostBack="True" GroupName="QueryByInvoice" Checked="True" Height="22px"></asp:radiobutton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</TD>
									<td style="HEIGHT: 30px"></td>
								</tr>
								<TR>
									<TD style="HEIGHT: 30px"></TD>
									<TD style="HEIGHT: 30px" class="tableLabel" colSpan="3"><FONT face="Tahoma">&nbsp;&nbsp;</FONT>
										<asp:label id="Label5" runat="server" CssClass="tableLabel" Width="320px" Height="17px"> Match status code Inside History:</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										
										<asp:radiobutton id="rbCheckAllYes" runat="server" Text="Yes" CssClass="tableRadioButton" Width="58px"
											AutoPostBack="True" GroupName="SearchAll" Height="22px"></asp:radiobutton><FONT face="Tahoma"></FONT><asp:radiobutton id="rbCheckAllNo" runat="server" Text="No" CssClass="tableRadioButton" Width="58px"
											AutoPostBack="True" GroupName="SearchAll" Checked="True" Height="22px"></asp:radiobutton></TD>
									<TD style="HEIGHT: 30px"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 30px"></TD>
									<TD style="HEIGHT: 30px" class="tableLabel" colSpan="3"><FONT face="Tahoma">&nbsp;&nbsp;</FONT>
										<asp:label id="Label6" runat="server" CssClass="tableLabel" Width="320px" Height="17px"> Show Package Detail</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:radiobutton id="rdoPkgYes" runat="server" Text="Yes" CssClass="tableRadioButton" Width="58px"
											AutoPostBack="True" GroupName="SearchPKG" Height="22px"></asp:radiobutton><FONT face="Tahoma"></FONT><asp:radiobutton id="rdoPkgNo" runat="server" Text="No" CssClass="tableRadioButton" Width="58px"
											AutoPostBack="True" GroupName="SearchPKG" Checked="True" Height="22px"></asp:radiobutton></TD>
									<TD style="HEIGHT: 30px"></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
			</TABLE>
			<asp:label style="Z-INDEX: 103; POSITION: absolute; TOP: 4px; LEFT: 20px" id="Label1" runat="server"
				CssClass="maintitleSize" Width="365px" Height="27px">Shipment Tracking</asp:label></TR></TABLE></TR></TABLE><input 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition>
		</form>
	</body>
</HTML>
