<%@ Page language="c#" Codebehind="DistributionCenter.aspx.cs" AutoEventWireup="false" Inherits="com.ties.DistributionCenter" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Distribution Centers</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="IndustrialSector" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; POSITION: absolute; TOP: 52px; LEFT: 23px" runat="server"
				Width="127px" Text="Query" CssClass="queryButton" CausesValidation="False"></asp:button><asp:button id="btnExecuteQuery" style="Z-INDEX: 103; POSITION: absolute; TOP: 52px; LEFT: 150px"
				runat="server" Width="127px" Text="Execute Query" CssClass="queryButton" CausesValidation="False" Enabled="False"></asp:button><asp:button id="btnInsert" style="Z-INDEX: 102; POSITION: absolute; TOP: 52px; LEFT: 276px"
				runat="server" Width="127px" Text="Insert" CssClass="queryButton" CausesValidation="False"></asp:button>&nbsp;
			<asp:datagrid id="dgDCenter" style="Z-INDEX: 104; POSITION: absolute; TOP: 126px; LEFT: 23px"
				runat="server" Width="98%" AllowPaging="True" AllowCustomPaging="True" OnPageIndexChanged="OnISector_PageChange"
				OnEditCommand="OnEdit_ISector" OnCancelCommand="OnCancel_ISector" OnDeleteCommand="OnDelete_ISector"
				OnUpdateCommand="OnUpdate_ISector" AutoGenerateColumns="False" HorizontalAlign="Center" OnItemDataBound="OnItemBound_ISector">
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
						CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="4%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle Width="4%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Code">
						<HeaderStyle Font-Bold="True" Width="8%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label id=lblCode Text='<%#DataBinder.Eval(Container.DataItem,"origin_code")%>' CssClass="gridLabel" Visible="True" Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id=txtCode Text='<%#DataBinder.Eval(Container.DataItem,"origin_code")%>' CssClass="gridTextBox" Visible="True" Runat="server" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator id="cCostCode" Runat="server" ErrorMessage="Code " ControlToValidate="txtCode" Display="None"
								BorderWidth="0"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Description">
						<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label id=Label2 Text='<%#DataBinder.Eval(Container.DataItem,"origin_name")%>' CssClass="gridLabel" Runat="server" Visible="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox id=txtDescription Text='<%#DataBinder.Eval(Container.DataItem,"origin_name")%>' CssClass="gridTextBox" Runat="server" Visible="True" MaxLength="200">
							</asp:TextBox>
							<asp:RequiredFieldValidator id="cOName" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtDescription"
								ErrorMessage="Description"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Hub">
						<HeaderStyle Width="8%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:DropDownList id="ddlHub" CssClass="gridDropDown" Runat="server"></asp:DropDownList>
							<asp:RequiredFieldValidator id="ReqHub" runat="server" ErrorMessage="Hub" ControlToValidate="ddlHub" Display="None"></asp:RequiredFieldValidator>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Scanning">
						<HeaderStyle Width="8%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:DropDownList id="ddlManifest" CssClass="gridDropDown" Runat="server"></asp:DropDownList>
							<asp:RequiredFieldValidator id="ReqManifest" runat="server" ErrorMessage="Scanning" ControlToValidate="ddlManifest"
								Display="None"></asp:RequiredFieldValidator>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Email Address">
						<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label id=Label3 Width ="200px" Text='<%#DataBinder.Eval(Container.DataItem,"EmailAddress")%>' CssClass="gridLabel" Visible="True" Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox id=txtEmail Width ="200px" Text='<%#DataBinder.Eval(Container.DataItem,"EmailAddress")%>' CssClass="gridTextBox" Visible="True" Runat="server" MaxLength="200">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Postal Code">
						<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label id="lblZipcode" Width ="80px" Text='<%#DataBinder.Eval(Container.DataItem,"Zipcode")%>' CssClass="gridLabel" Visible="True" Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox id="txtZipcode" MaxLength="12" Width ="80px" Text='<%#DataBinder.Eval(Container.DataItem,"Zipcode")%>' CssClass="gridTextBox" Visible="True" Runat="server">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Operated By">
						<HeaderStyle Width="8%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:DropDownList id="ddlOperated" CssClass="gridDropDown"									 								 							 
								Runat="server">
							</asp:DropDownList>
							<asp:RequiredFieldValidator id="Requiredfieldvalidator1" runat="server" ErrorMessage="Operated By" 
								ControlToValidate="ddlOperated"
								Display="None"></asp:RequiredFieldValidator>
						</ItemTemplate>						
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Turnaround Time (HH:MM)">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label id="lblTurnaroundTime" Width ="50px" Text='<%#DataBinder.Eval(Container.DataItem,"TurnaroundTime")%>' CssClass="gridLabel" Visible="True" Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>							
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtTurnaroundTime" Text='<%#DataBinder.Eval(Container.DataItem,"TurnaroundTime","{0:HH:mm}")%>' Runat="server" Enabled="True" TextMaskType="msTime" TextMaskString="99:99" MaxLength="5">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator id="Requiredfieldvalidator2" runat="server" ErrorMessage="Turnaround Time" 
								ControlToValidate="txtTurnaroundTime"
								Display="None"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Nearest Airport">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label id="lblNearestAirport" Width ="35px" Text='<%#DataBinder.Eval(Container.DataItem,"NearestAirport")%>' CssClass="gridLabel" Visible="True" Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id="txtNearestAirport"  runat="server" Width="155px" name="txtUserID" MaxLength="3" style="TEXT-TRANSFORM: uppercase"
										TextMaskType="msUpperAlfaNumericWithUnderscore"  Text='<%#DataBinder.Eval(Container.DataItem,"NearestAirport")%>' CssClass="gridTextBox"></cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Cost Center">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label id="Label4" Width ="35px" Text='<%#DataBinder.Eval(Container.DataItem,"CostCenter")%>' CssClass="gridLabel" Visible="True" Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id="msCostCenter"  runat="server" Width="95px" name="msCostCenter" MaxLength="12" style="TEXT-TRANSFORM: uppercase"
										TextMaskType="msNumeric"  Text='<%#DataBinder.Eval(Container.DataItem,"CostCenter")%>' 
										NumberPrecision="12" NumberMaxValue="2147483647" ssClass="gridTextBox"></cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:label id="lblValISector" style="Z-INDEX: 105; POSITION: absolute; TOP: 85px; LEFT: 26px"
				runat="server" Width="500px" CssClass="errorMsgColor"></asp:label><asp:label id="Label1" style="Z-INDEX: 107; POSITION: absolute; TOP: 14px; LEFT: 32px" runat="server"
				Width="321px" CssClass="mainTitleSize" Height="27px">Distribution Centers</asp:label><asp:validationsummary id="PageValidationSummary" style="Z-INDEX: 106; POSITION: absolute; TOP: 152px; LEFT: 728px"
				runat="server" ShowMessageBox="True" ShowSummary="False" DisplayMode="BulletList" HeaderText="Please enter the missing fields."></asp:validationsummary></form>
	</body>
</HTML>
