<%@ Page language="c#" Codebehind="HcrStatusByBatch.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.HcrStatusByBatch" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>HcrStatusByBatch</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<meta content="False" name="vs_snapToGrid">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<LINK media="all" href="css/jsDatePick_ltr.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script src="Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
		<script src="Scripts/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
		<script src="Scripts/jquery.json-2.2.js" type="text/javascript"></script>
		<script src="Scripts/jstorage.js" type="text/javascript"></script>
		<script src="Scripts/jquery.timers.js" type="text/javascript"></script>
		<script src="Scripts/HcrByBatch.js" type="text/javascript"></script>
		<script language="javascript" src="Scripts/jsDatePick.min.1.3.js"></script>
		<!-- ====================================================================== -->
		<script language="javascript" type="text/javascript">
			$(function () {				
				// -------------------- Timer 1000 - callService ---------------------------------------
				$(".myActive").everyTime(1000, function () {
					if (document.getElementById("txtMode").value == "PROCESSING...")
					{
						callService();
					}
				});						        	        
		        
		        // -------------------- Timer 2500 - viewdataUpdate & viewdataMissing ------------------
		        $(".myActiveData").everyTime(2500, function () {
					if (document.getElementById("txtMode").value == "PROCESSING...")
					{
						viewdataUpdate();
						viewdataMissing();
					}
		        });
			});
					    

			// -------------------------------  flushData -------------------------------
			function flushData(){
				$.jStorage.flush();
				reDraw();
			}
			
			
			// -------------------------------  callService -------------------------------
		    function callService()
			{							
		        var index = $.jStorage.index();
		        	        	                		        
		        if (index.length > 0)
		        {		        
		            var userID = document.getElementById('lblUserId').innerHTML;		            
		            var statusCodeAll = document.getElementById('txtUpdStatus').value;		            
		            var barcode = $.jStorage.get(index[0]); 
		            		            		            
		            if (barcode != '' && statusCodeAll != '' && userID != '')
		            {    
	            		///Call Store Procedure
						$.ajax({
							url: serviceURL_SaveHCR,
							type: "POST",
							dataType: "json",
							data: "{Barcode:'" + barcode + "',StatusCodeAll:'" + statusCodeAll + "',UpdUserID:'" + userID + "'}",
							contentType: "application/json; charset=utf-8",
							success: function (msg) {	                    
								if (msg["d"]["ReturnVal"]) {
									//alert(msg["d"]["ReturnVal"]);
									//document.getElementById("lberrmsg").innerHTML = "????????";								
									$.jStorage.deleteKey(index[0]);
									reDraw();
									//alert('success');
								} 							
							},
							//error: function (e) {},							
							error: function(msg){
								alert("Call Service error");
							}
						});
					}
		        }		        
		    }			

		<!-- -------------------------------------------------- -->
		<!-- script for button -- Add/Remove/Add All/Remove All -->
		<!-- -------------------------------------------------- -->
			$(document).ready(function() {
				///If you want to move selected item from fromListBox to toListBox
				$("#btnAdd_").click(function() {
					$("#lbFrom  option:selected").appendTo("#lbTo");
				});
	            
				///If you want to move all item from fromListBox to toListBox
				$("#btnAddAll_").click(function() {
					$("#lbFrom option").appendTo("#lbTo");
				});

				///If you want to remove selected item from toListBox to fromListBox
				$("#btnRemove_").click(function() {
					$("#lbTo option:selected").appendTo("#lbFrom");
				});

				///If you want to remove all items from toListBox to fromListBox
				$("#btnRemoveAll_").click(function() {
					$("#lbTo option").appendTo("#lbFrom");
				});
			});
		<!-- --------------------------------------------------------------------------------- -->
		var browserType;
		
		if (document.layers) {browserType = "nn4"}
		if (document.all) {browserType = "ie"}
		if (window.navigator.userAgent.toLowerCase().match("gecko")) {browserType= "gecko"}
		<!-- --------------------------------------------------------------------------------- -->
		///================================== getConSelected ==================================///		
		function getConSelected() 
		{
			var appid = document.getElementById("lblAppId").innerText;			
			var entid = document.getElementById("lblEntId").innerText; 
			var hcsno = document.getElementById("lblHcsNumber").innerText;	
			
			if (hcsno == '')
			{
				alert("Please select HCS No.");
			}
			else
			{			
				$.ajax({
					url: serviceURL_ConsNoLoad,
					type: "POST",
					dataType: "json",
					data: "{appid:'"+appid+"',entid:'"+entid+"',hcsno:'"+hcsno+"'}",
					contentType: "application/json; charset=utf-8",
					success: function (msg) {
						$(".viewdataConSelected").replaceWith(msg["d"]["ResponseData"]);	
					},
					//error: function (e) {},
					error: function(msg){
						alert("viewdataConSelected error.");
					}
				}); 			
			
				if (browserType == "gecko" )
					document.poppedLayer = eval('document.getElementById("divSelectCons")');
				else if (browserType == "ie")
					document.poppedLayer = eval('document.getElementById("divSelectCons")');
				else
					document.poppedLayer = eval('document.layers["divSelectCons"]');
				document.poppedLayer.style.visibility = "visible";	
			}						
			
									 					        
			
		}
		<!-- --------------------------------------------------------------------------------- -->
		///================================== LoadJsDatePick ==================================///
		function LoadJsDatePick()
		{	
			g_globalObject = new JsDatePick({
				useMode:1,
				isStripped:true,
				target:"divCalendar"
			});
				
			g_globalObject.setOnSelectedDelegate(function(){
				var obj = g_globalObject.getSelectedDay();
				
				//alert("a date was just selected and the date is : " + obj.day + "/" + obj.month + "/" + obj.year);
				
				var sTmp = "";
				var sDay = "";
				var sMonth = "";
				var sYear = "";
				
				sYear = obj.year;
				
				sTmp = "0" + obj.month;
				sMonth = sTmp.substring(sTmp.length - 2);
				
				sTmp = "0" + obj.day;
				sDay = sTmp.substring(sTmp.length - 2);
				
				var txtDate = document.getElementById("txtDate");
				txtDate.value = sDay + "/" + sMonth + "/" + sYear;
				
				var txtHcsNo = document.getElementById("txtHcsNo");
				txtHcsNo.value = " ";
								
				callServer();				
			});
		};
		
		/////============================= callServer =============================////
		function callServer()
		{
			var btn = document.getElementById('btnCallServer');

			if(btn == null)
				alert('btnCallServer button not found!');
			else
				btn.click();
		}
		
		
		/////============================= insertJStorage =============================/////
		function insertJStorage() 
		{
			var val = document.getElementById('txtConsVal').value;			

			var arr = val.split("|");
										
			if (val != '')
			{
				for(var i=0; i<arr.length; i++) {
					var value = arr[i];
					if (value != ''){
						$.jStorage.set("valCons_" + value, value);
					}
				}
	
				reDraw();
			}
		}
		
		
		
		

		/////====================================== SubmitCons ======================================/////
		function SubmitCons()
		{	
			if (browserType == "gecko" )
				document.poppedLayer = eval('document.getElementById("divBarcode")');
			else if (browserType == "ie")
				document.poppedLayer = eval('document.getElementById("divBarcode")');
			else
				document.poppedLayer = eval('document.layers["divBarcode"]');
			document.poppedLayer.style.visibility = "visible";


			if (browserType == "gecko" )
				document.poppedLayer = eval('document.getElementById("divSelectCons")');
			else if (browserType == "ie")
				document.poppedLayer = eval('document.getElementById("divSelectCons")');
			else
				document.poppedLayer = eval('document.layers["divSelectCons"]');
			document.poppedLayer.style.visibility = "hidden";
						
			
			var i=1;
			var str = "";
			for(i=1; i<=document.getElementById("hdnChkCount").value; i++)
			{
				if (document.getElementById("chk_"+i).checked == true)
				{
					str = str + document.getElementById("val_"+i).value + "|";
				}
			}
			var hcsno = document.getElementById("lblHcsNumber").innerText;
			var appid = document.getElementById("lblAppId").innerText;			
			var entid = document.getElementById("lblEntId").innerText;
			var strcons = "|" + str;
			var userid = document.getElementById('lblUserId').innerHTML;
						
			///-- Save Process
			$.ajax({
				url: serviceURL_HCRByBatchSelectedSave,
				type: "POST",
				dataType: "json",
				data: "{appid:'" + appid + "',entid:'" + entid + "',hcsno:'" + hcsno + "',strcons:'" + strcons + "',userid:'" + userid + "'}",
				contentType: "application/json; charset=utf-8",
				success: function (msg) {	                    
					if (msg["d"]["ReturnVal"]) {
						//alert(msg["d"]["ReturnVal"]);
						//document.getElementById("lberrmsg").innerHTML = "????????";								
					} 							
				},
				//error: function (e) {},							
				error: function(msg){
					alert("Call Service error");
				}
			});
			
			
			///-- Select from Hcr_bybatch_selected
			$.ajax({
				url: serviceURL_ConsNoSelectedLoad,
				type: "POST",
				dataType: "json",
				data: "{appid:'"+appid+"',entid:'"+entid+"',hcsno:'"+hcsno+"'}",
				contentType: "application/json; charset=utf-8",
				success: function (msg) {					
					$('#txtConsVal').val(msg["d"]["ResponseData"]);			
					
					insertJStorage();
					
					//alert(msg["d"]["ResponseData"]);
				},
				//error: function (e) {}
				error: function(msg){
					alert("txtConsVal error.");
				}
			}); 
		}
		
		
		
		
		
		
		</script>
		<!-- -------------------------------------------------- CSS -------------------------------------------------- -->
		<style type="text/css">
			.MissCon THEAD {
				COLOR: red
			}
			.MissCon TBODY {
				COLOR: red
			}
		</style>
		<!-- =========================================================== HTML =========================================================== -->
	</HEAD>
	<BODY onclick="GetScrollPosition();" onload="{SetScrollPosition();LoadJsDatePick();}"
		onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<FORM id="Form1" method="post" runat="server">
			<p class="maintitleSize">HCR Scanning By Batch</p>
			<div>&nbsp; <label class="lblErrorMsg" id="lblErrorMsg" style="FONT-SIZE: 20px; WIDTH: 1000px; COLOR: red"
					runat="server"></label>&nbsp; <input style="DISPLAY: none" type="hidden" name="ScrollPosition">
				&nbsp;
				<asp:button id="btnCallServer" style="DISPLAY: none" runat="server"></asp:button>&nbsp;
				<asp:textbox id="txtConsVal" style="DISPLAY: none" runat="server"></asp:textbox>&nbsp;
				<asp:label id="lblAppId" style="DISPLAY: none" runat="server" CssClass="tableLabel"></asp:label>&nbsp;
				<asp:label id="lblEntId" style="DISPLAY: none" runat="server" CssClass="tableLabel"></asp:label>&nbsp;
				<input id="txtUpdStatus" style="DISPLAY: none" readOnly type="text"> &nbsp;
			</div>
			<div>
				<TABLE width="1000" border="1">
					<TR style="VERTICAL-ALIGN: top; TEXT-ALIGN: left">
						<TD>
							<!-- ======================================================== Select Data ======================================================== -->
							<DIV id="divData">
								<FIELDSET style="WIDTH: 510px; HEIGHT: 300px"><LEGEND class="tableHeadingFieldset">&nbsp;Select&nbsp;Data&nbsp;</LEGEND><br>
									<table>
										<tr>
											<td style="WIDTH: 5px">&nbsp;</td>
											<td>
												<div id="divCalendar" style="BORDER-RIGHT: buttonshadow 1px outset; BORDER-TOP: buttonshadow 1px outset; BORDER-LEFT: buttonshadow 1px outset; WIDTH: 205px; BORDER-BOTTOM: buttonshadow 1px outset; HEIGHT: 230px; BACKGROUND-COLOR: white"
													align="center"></div>
											</td>
											<td style="WIDTH: 20px">&nbsp;</td>
											<td class="tableLabel" style="WIDTH: 40%" vAlign="top" align="left"><strong>Selected&nbsp;Date:</strong><br>
												<asp:textbox id="txtDate" runat="server" Font-Size="Large" AutoPostBack="True" Height="32" ReadOnly="True"
													BackColor="Silver" Width="145px"></asp:textbox><br>
												<br>
												<strong>Select HCS Number:</strong><br>
												<asp:listbox id="lbHcsData" runat="server" AutoPostBack="True" Height="161px" Width="145px" Rows="5"></asp:listbox></td>
											<td style="WIDTH: 20px">&nbsp;</td>
											<td class="tableLabel" style="WIDTH: 40%" vAlign="top" align="left"><strong>Selected&nbsp;HCS#:</strong><br>
												<asp:textbox id="txtHcsNo" runat="server" Font-Size="Large" Height="32" ReadOnly="True" BackColor="Silver"
													Width="218px"></asp:textbox><br>
												<input class="searchButton" id="btnGetCons" style="WIDTH: 130px; HEIGHT: 29px" onclick="getConSelected()"
													type="button" value="Get Consignment">
												<br>
											</td>
											<td style="WIDTH: 5px">&nbsp;</td>
										</tr>
									</table>
								</FIELDSET>
							</DIV>
							<!-- ======================================================== Info. ======================================================== -->
							<DIV style="Z-INDEX: 101; LEFT: 645px; VERTICAL-ALIGN: top; POSITION: absolute; TOP: 66px">
								<FIELDSET style="WIDTH: 340px"><LEGEND class="tableHeadingFieldset">Info.</LEGEND>
									<TABLE style="WIDTH: 100%">
										<TR>
											<TD style="HEIGHT: 18px">&nbsp;</TD>
											<TD class="tableLabel" style="WIDTH: 110px">HCS&nbsp;Number:</TD>
											<TD><asp:label id="lblHcsNumber" style="FONT-SIZE: 14px" runat="server" CssClass="tableLabel" Font-Bold="True"
													ForeColor="Red"></asp:label></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 18px">&nbsp;</TD>
											<TD class="tableLabel" style="WIDTH: 110px">HCS&nbsp;Date:</TD>
											<TD><asp:label id="lblHcsDate" style="FONT-SIZE: 14px" runat="server" CssClass="tableLabel" Font-Bold="True"
													ForeColor="Red"></asp:label></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 18px">&nbsp;</TD>
											<TD class="tableLabel" style="WIDTH: 110px">User&nbsp;ID:</TD>
											<TD><asp:label id="lblUserId" style="FONT-SIZE: 14px" runat="server" CssClass="tableLabel" Font-Bold="True"
													ForeColor="Red"></asp:label></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 18px">&nbsp;</TD>
											<TD class="tableLabel" style="WIDTH: 110px">Location:</TD>
											<TD><asp:label id="lblLocation" runat="server" CssClass="tableLabel" Font-Bold="True" ForeColor="Red"></asp:label></TD>
										</TR>
									</TABLE>
								</FIELDSET>
							</DIV>
							<!-- ======================================================== Status ======================================================== -->
							<DIV id="divStatus" style="Z-INDEX: 102; LEFT: 645px; VERTICAL-ALIGN: top; POSITION: absolute; TOP: 190px">
								<table id="tbSelectStatus" style="VERTICAL-ALIGN: top; TEXT-ALIGN: center;">
									<tr>
										<td style="TEXT-ALIGN: center" colSpan="4">
											<input id="txtMode" style="WIDTH: 100%" readOnly type="text" style="color: #FFFFFF; background-color:silver; font-weight: bold;">&nbsp;
										</td>
									</tr>
									<tr>
										<td style="FONT-SIZE: 10pt"><b>Status</b><br>
											<asp:listbox id="lbFrom" runat="server" Height="100px" Width="105px" SelectionMode="Multiple"></asp:listbox></td>
										<td>&nbsp;<br>
											<input id="btnAdd_" style="WIDTH: 35px" type="button" value=">"><br>
											<input id="btnRemove_" style="WIDTH: 35px" type="button" value="<"><br>
											<input id="btnAddAll_" style="WIDTH: 35px" type="button" value=">>"><br>
											<input id="btnRemoveAll_" style="WIDTH: 35px" type="button" value="<<">
										</td>
										<td style="FONT-SIZE: 10pt"><b>Update&nbsp;Status</b><br>
											<asp:listbox id="lbTo" runat="server" Height="100px" Width="105px" SelectionMode="Multiple"></asp:listbox></td>
										<td style="FONT-SIZE: 10pt; TEXT-ALIGN: center; veticle-align: bottom">&nbsp;<br>
											<br>
											<input class="queryButton" id="btnClear_" style="WIDTH: 80px; HEIGHT: 32px" onclick="{SetMode('');clearView();}"
												type="button" value="Clear"><br>
											<input class="queryButton" id="btnScan" style="WIDTH: 80px; HEIGHT: 32px" onclick="{SetMode('PROCESSING...');showScanData();}"
												type="button" value="Process"><br>
											<input class="queryButton" id="btnFlush" style="WIDTH: 80px; HEIGHT: 32px" onclick="{SetMode('');flushData();}"
												type="button" value="Flush">
											<br>
										</td>
									</tr>
								</table>
							</DIV>
						</TD>
					</TR>
					<TR>
						<TD>
							<!-- ======================================================== Database Result ======================================================== -->
							<div id="divBarcode" style="VISIBILITY: hidden">
								<table id="tblShipmentTracking" width="100%" border="0" runat="server">
									<TBODY>
										<TR>
											<TD style="WIDTH: 100%" vAlign="top"><br>
												<table style="WIDTH: 100%; TEXT-ALIGN: center">
													<tr>
														<!-- $jstorage MEMORY -->
														<td class="maintitleSize" style="WIDTH: 5%; COLOR: black" vAlign="top" align="center"><u><b>Local</b></u>
															<table class="GetCon" style="FONT-SIZE: 10pt; WIDTH: 100%" align="center" border="1">
																<thead>
																	<tr>
																		<td align="center"><STRONG>&nbsp;Consignment#&nbsp;</STRONG></td>
																	</tr>
																</thead>
																<tbody class="viewdataGetCon" id="disp_cons_local">
																</tbody>
																<tfoot>
																</tfoot>
															</table>
														</td>
														<!-- data COMPLETE -->
														<td class="maintitleSize" style="WIDTH: 36%; COLOR: black" vAlign="top" align="center"><u><b>D&nbsp;M&nbsp;S</b></u>
															<table class="CompleteCon" style="FONT-SIZE: 10pt; WIDTH: 100%" align="center" border="1">
																<thead>
																	<tr>
																		<td align="center"><STRONG>Consignment#</STRONG></td>
																		<td align="center"><STRONG>Scan Date</STRONG></td>
																		<td align="center"><STRONG>Status</STRONG></td>
																	</tr>
																</thead>
																<tbody class="viewdataUpdate">
																</tbody>
																<tfoot>
																</tfoot>
															</table>
														</td>
														<!-- data MISSING -->
														<td class="maintitleSize" style="WIDTH: 59%; COLOR: black" vAlign="top" align="center"><u><b>Data&nbsp;Missing</b></u>
															<table class="MissCon" style="FONT-SIZE: 10pt; WIDTH: 100%" align="center" border="1">
																<thead>
																	<TR>
																		<td align="center"><STRONG>Consignment#</STRONG></td>
																		<td align="center"><STRONG>Scan Date</STRONG></td>
																		<td align="center"><STRONG>Status</STRONG></td>
																		<td align="center"><STRONG>Remark</STRONG></td>
																	</TR>
																</thead>
																<tbody class="viewdataMissing">
																</tbody>
																<tfoot>
																</tfoot>
															</table>
														</td>
													</tr>
												</table>
												<!-- ------------------------------------------------------- -->
												<script>reDraw()</script>
												<!-- ------------------------------------------------------- -->
												<script src="Scripts/events.js" type="text/javascript"></script>
												<!-- ------------------------------------------------------- --> &nbsp;
											</TD>
										</TR>
									</TBODY>
								</table>
								<br>
								<br>
							</div>
						</TD>
					</TR>
				</TABLE>
			</div>
			<!-- ======================================================== ViewDataConSelected ======================================================== -->
			<div id="divSelectCons" style="Z-INDEX: 102; LEFT: 402px; VISIBILITY: hidden; VERTICAL-ALIGN: top; POSITION: absolute; TOP: 200px">
				<TABLE id="tabSelectCons" cellSpacing="2" cellPadding="2" border="2">
					<TR>
						<TD class="tableLabel" style="COLOR: black; HEIGHT: 30px; BACKGROUND-COLOR: silver; TEXT-ALIGN: center; VERTICLE-ALIGN: top"><STRONG>Select&nbsp;Consignment</STRONG>
						</TD>
					</TR>
					<TR>
						<TD style="BACKGROUND-COLOR: silver; TEXT-ALIGN: center; VERTICLE-ALIGN: top">
							<table class="GetCon" style="FONT-SIZE: 10pt" align="center" border="1">
								<thead>
									<tr>
										<td align="center"><input id="chkAll" onclick="ClickCheckAll(this);" type="checkbox" name="chkAll"></td>
										<td align="center"><STRONG>&nbsp;Consignment#&nbsp;</STRONG></td>
									</tr>
								</thead>
								<tbody class="viewdataConSelected">
								</tbody>
								<tfoot>
									<tr>
										<td align="center" colSpan="2">
											<input class="searchButton" id="btnSubmit" style="WIDTH: 65px; HEIGHT: 30px" onclick="SubmitCons();"
												type="button" value="Submit">&nbsp;
											<input class="searchButton" id="btnCancel" style="WIDTH: 65px; HEIGHT: 30px" onclick="CancelCons();"
												type="button" value="Cancel"></td>
									</tr>
								</tfoot>
							</table>
						</TD>
					</TR>
				</TABLE>
			</div>
			<!-- ================================================================================================================== -->
			<DIV class="myActive">&nbsp;</DIV>
			<DIV class="myActiveData">&nbsp;</DIV>
		</FORM>
	</BODY>
</HTML>
