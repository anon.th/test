﻿<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="cr" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>
<%@ Page language="c#" Codebehind="ReportViewer.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ReportViewer" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ReportViewer</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		
		<!--
		<script type="text/javascript">
			function closeWindow()
			{
				window.top.close();
				alert('Close OK !!');
			}
		</script>
		-->
		
		
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="ReportViewer" method="post" runat="server">
			<CR:CRYSTALREPORTVIEWER id="rptViewer" style="Z-INDEX: 100; POSITION: absolute; TOP: 50px; LEFT: 3px;" runat="server"
				BestFitPage="True" Height="100%" Width="100%" HasExportButton="False" HasPrintButton="False" DisplayToolbar="False"
                ToolPanelView="None" HasToggleGroupTreeButton="False" DisplayStatusbar="False" HasDrilldownTabs="False">

			</CR:CRYSTALREPORTVIEWER>
            
            <asp:label id="lblErrorMesssage" style="Z-INDEX: 114; POSITION: absolute; TOP: 32px; LEFT: 8px" runat="server" Width="824px" CssClass="errorMsgColor"></asp:label>
            <asp:button id="btnSearch" style="Z-INDEX: 111; POSITION: absolute; TOP: 7px; LEFT: 893px" runat="server" CssClass="queryButton" Text="Search">
            
                </asp:button><asp:dropdownlist id="ddbZoom" style="Z-INDEX: 110; POSITION: absolute; TOP: 9px; LEFT: 710px" runat="server"
				Height="24px" Width="67px" AutoPostBack="True">
				<asp:ListItem Value="25">25%</asp:ListItem>
				<asp:ListItem Value="50">50%</asp:ListItem>
				<asp:ListItem Value="75">75%</asp:ListItem>
				<asp:ListItem Value="100" Selected="True">100%</asp:ListItem>
				<asp:ListItem Value="125">125%</asp:ListItem>
				<asp:ListItem Value="150">150%</asp:ListItem>
				<asp:ListItem Value="175">175%</asp:ListItem>
				<asp:ListItem Value="200">200%</asp:ListItem>
			</asp:dropdownlist><asp:button id="btnShowGrpTree" style="Z-INDEX: 109; POSITION: absolute; TOP: 8px; LEFT: 6px" runat="server" Width="121px" CssClass="queryButton" Text="Show Group Tree"></asp:button>
            <asp:button id="btnMoveLast" style="Z-INDEX: 106; POSITION: absolute; TOP: 8px; LEFT: 269px" runat="server" Width="24px" CssClass="queryButton" Text=">|"></asp:button>
            <asp:button id="btnMoveNext" style="Z-INDEX: 105; POSITION: absolute; TOP: 8px; LEFT: 244px" runat="server" Width="25" CssClass="queryButton" Text=">"></asp:button>
            <asp:button id="btnExport" style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 466px" runat="server" CssClass="queryButton" Text="Export"></asp:button>
            <asp:button id="btnMoveFirst" style="Z-INDEX: 102; POSITION: absolute; TOP: 8px; LEFT: 130px" runat="server" Width="25px" CssClass="queryButton" Text="|<"></asp:button>
            <asp:button id="btnMovePrevious" style="Z-INDEX: 103; POSITION: absolute; TOP: 8px; LEFT: 155px" runat="server" Width="25" CssClass="queryButton" Text="<"></asp:button>
            <cc1:mstextbox id="txtGoTo" style="Z-INDEX: 107; POSITION: absolute; TOP: 8px; LEFT: 183px" runat="server" Width="56px" CssClass="textFieldRightAlign" TextMaskType="msNumeric" MaxLength="6" NumberMaxValue="999999" NumberPrecision="6" NumberScale="0"></cc1:mstextbox>
            <asp:dropdownlist id="ddbExport" style="Z-INDEX: 108; POSITION: absolute; TOP: 8px; LEFT: 294px" runat="server" Height="24px" Width="171px"></asp:dropdownlist>
            <asp:button id="btnExportExcel" style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 532px" runat="server" CssClass="queryButton" Text="Export Excel" Enabled="False"></asp:button>
            <asp:textbox id="txtTextToSearch" style="Z-INDEX: 112; POSITION: absolute; TOP: 8px; LEFT: 784px" runat="server" Width="96px" CssClass="textField"></asp:textbox>
            <asp:label id="lblZoom" style="Z-INDEX: 113; POSITION: absolute; TOP: 11px; LEFT: 664px" runat="server" Height="16px" Width="68px" CssClass="tableLabel" Font-Bold="True">Zoom</asp:label></form>
	</body>
</HTML>
