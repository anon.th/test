<%@ Page language="c#" Codebehind="CustomsInvoicing.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsInvoicing" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomsInvoicing</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
			function funcDisBack(){
				history.go(+1);
			}

			function RemoveBadNonNumber(strTemp, obj) {
				strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			function AfterPasteNonNumber(obj) {
				setTimeout(function () {
					RemoveBadNonNumber(obj.value, obj);
				}, 1); //or 4
			}
			function validate(evt) {
				var theEvent = evt || window.event;
				var key = theEvent.keyCode || theEvent.which;
				key = String.fromCharCode( key );
				var regex = /[0-9]/;
				if( !regex.test(key) ) {
					theEvent.returnValue = false;
					if(theEvent.preventDefault) theEvent.preventDefault();
				}
			}
			
		function RemoveBadNonNumberWeight(strTemp, obj) {
			strTemp = strTemp.replace(/[^._0-9]/g, ''); //replace non-numeric
            strTemp = strTemp.replace(/\s/g, ''); // replace space
            
			var res = strTemp.split(".");     			
			for (var i=0; i<res.length-1; i++) {
				if(i==0)
				{
					if (res[i].length > 6) 
					{
						strTemp=res[i].substring(res[i].length - 6);			
					}else{
						strTemp=res[i];
					}
				}
				if(i==1)
				{
					strTemp=strTemp+"."+res[i].substring(0,1);									
				}
			}	
			if(res.length == 1)
			{
				strTemp=strTemp.substring(strTemp.length - 6);				
			}			
			obj.value=strTemp;
        }
        
        function AfterPasteNonNumberWeight(obj) {
            setTimeout(function () {
                RemoveBadNonNumberWeight(obj.value, obj);
            }, 1); //or 4
        }
			
			function funcFocusCountrol(){
				//alert(document.getElementById('bFocus'));
				//document.getElementById('bFocus').focus();  
				var dimensions = getSize();
				document.forms[0].ScrollPosition.value = 500;
				//window.scroll(0, (dimensions.height)/2);
				//window.scrollTo(0,1000);
				alert(dimensions.height);
			}
			
			function getSize() {
				var myWidth = 0, myHeight = 0;
				if( typeof( window.innerWidth ) == 'number' ) {
					//Non-IE
					myWidth = window.innerWidth;
					myHeight = window.innerHeight;
				} else if( document.documentElement &&
					( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
					//IE 6+ in 'standards compliant mode'
					myWidth = document.documentElement.clientWidth;
					myHeight = document.documentElement.clientHeight;
				} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
					//IE 4 compatible
					myWidth = document.body.clientWidth;
					myHeight = document.body.clientHeight;
				}
				return {'height': myHeight, 'width': myWidth};
			}

		function focusElement(ele){
			ele.focus();
			var dimensions = getSize();
			window.scrollBy(0, (dimensions.height-ele.offsetHeight)/2);
		}
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="CustomsInvoicing" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 1000px; HEIGHT: 1543px; TOP: 34px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tbody>
						<tr>
							<td><asp:label id="Label1" runat="server" Width="421px" Height="27px" CssClass="mainTitleSize">Prepare/Update an Invoice</asp:label></td>
						</tr>
						<tr>
							<td style="HEIGHT: 22px"><asp:button id="btnQuery" runat="server" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExecuteQuery" runat="server" CssClass="queryButton" Text="Execute Query"
									CausesValidation="False" style="Z-INDEX: 0"></asp:button><asp:button id="btnSave" runat="server" CssClass="queryButton" Text="Save" CausesValidation="False"
									Enabled="False"></asp:button><asp:button id="btnPrintInvoice" runat="server" CssClass="queryButton" Text="Print Invoice"
									CausesValidation="False" Enabled="True"></asp:button><asp:button style="Z-INDEX: 0" id="btnUnfinalize" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Unfinalize Invoice" Enabled="True"></asp:button>
								<asp:button style="Z-INDEX: 0; DISPLAY: none" id="btnExecQryHidden" runat="server" Width="130px"
									Text="Execute Query" CausesValidation="False"></asp:button>
							</td>
						</tr>
						<tr>
							<td style="HEIGHT: 19px" colSpan="6"><asp:label id="lblError" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red"></asp:label></td>
						</tr>
						<tr>
							<td>
								<table id="table_content" width="80%">
									<tbody>
										<tr>
											<td colSpan="4">
												<table style="WIDTH: 100%; HEIGHT: 74px" id="table_top">
													<tbody>
														<tr>
															<td style="WIDTH: 135px"><asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel">Invoice Number</asp:label><asp:label id="Label10" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
															<td style="WIDTH: 207px"><asp:textbox style="Z-INDEX: 0" id="Invoice_No" runat="server" Width="120px" CssClass="textField"
																	MaxLength="100" tabIndex="1" onblur="if( this.value !=''){document.getElementById('btnExecQryHidden').click();}"
																	onkeypress="validate(event)" onpaste="AfterPasteNonNumber(this)"></asp:textbox></td>
															<td style="WIDTH: 129px"><asp:label style="Z-INDEX: 0" id="Label9" runat="server" CssClass="tableLabel">Invoice Date</asp:label><asp:label id="Label11" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
															<td><cc1:mstextbox style="Z-INDEX: 0" id="InvoiceDate" tabIndex="2" runat="server" Width="96px" CssClass="textField"
																	MaxLength="12" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></td>
														</tr>
														<tr>
															<td style="WIDTH: 135px"><asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel">House AWB Number</asp:label></td>
															<td style="WIDTH: 207px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="consignment_no"
																	tabIndex="1" runat="server" Width="140px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
															<td style="WIDTH: 129px"><asp:label style="Z-INDEX: 0" id="Label12" runat="server" CssClass="tableLabel">Master AWB Number</asp:label></td>
															<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="MasterAWBNumber"
																	tabIndex="4" runat="server" Width="140px" CssClass="textField" Enabled="False" MaxLength="100"
																	ReadOnly="True"></asp:textbox></td>
														</tr>
														<tr>
															<td style="WIDTH: 135px"><asp:label style="Z-INDEX: 0" id="Label4" runat="server" CssClass="tableLabel">Entry Type</asp:label></td>
															<td style="WIDTH: 207px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="EntryType"
																	tabIndex="2" runat="server" Width="72px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
										<tr id="row_billing_delivery">
											<td style="WIDTH: 351px" width="351" colSpan="2">
												<fieldset style="HEIGHT: 160px"><legend style="COLOR: black">Billing</legend>
													<table style="MARGIN: 5px" id="table_Billing">
														<tr>
															<td><asp:label style="Z-INDEX: 0" id="Label5" runat="server" CssClass="tableLabel">Customer - ID</asp:label>&nbsp;<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="CustomerID"
																	tabIndex="5" runat="server" Width="64px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>
															</td>
															<td><asp:label style="Z-INDEX: 0" id="Label6" runat="server" CssClass="tableLabel">Tax Code</asp:label>&nbsp;<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="CustomerTaxCode"
																	tabIndex="6" runat="server" Width="100px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>
															</td>
														</tr>
														<tr>
															<td colSpan="2"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="CustomerName"
																	tabIndex="7" runat="server" Width="320px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
														</tr>
														<tr>
															<td colSpan="2"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Customer_address1"
																	tabIndex="8" runat="server" Width="320px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
														</tr>
														<tr>
															<td colSpan="2"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Customer_address2"
																	tabIndex="9" runat="server" Width="320px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
														</tr>
														<tr>
															<td colSpan="2"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Customer_zipcode"
																	tabIndex="10" runat="server" Width="85px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>&nbsp;<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Customer_state"
																	tabIndex="11" runat="server" Width="230px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
														</tr>
														<tr>
															<td><asp:label style="Z-INDEX: 0" id="Label7" runat="server" CssClass="tableLabel">Phone</asp:label>&nbsp;<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Customer_telephone"
																	tabIndex="12" runat="server" Width="114px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>
															</td>
															<td>&nbsp;<asp:label style="Z-INDEX: 0" id="Label8" runat="server" CssClass="tableLabel">Fax</asp:label>&nbsp;<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Customer_fax"
																	tabIndex="13" runat="server" Width="130px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>
															</td>
														</tr>
													</table>
												</fieldset>
											</td>
											<td colSpan="2">
												<fieldset style="WIDTH: 77.72%; HEIGHT: 160px"><legend style="COLOR: black">Delivery</legend>
													<table style="MARGIN: 5px; WIDTH: 320px; HEIGHT: 146px" id="table_Delivery">
														<tbody>
															<tr>
																<td><asp:textbox style="Z-INDEX: 0" id="Delivery_Name" tabIndex="3" runat="server" Width="304px"
																		CssClass="textField"></asp:textbox></td>
															</tr>
															<tr>
																<td><asp:textbox style="Z-INDEX: 0" id="Delivery_address1" tabIndex="4" runat="server" Width="304px"
																		CssClass="textField"></asp:textbox></td>
															</tr>
															<tr>
																<td><asp:textbox style="Z-INDEX: 0" id="Delivery_address2" tabIndex="5" runat="server" Width="304px"
																		CssClass="textField"></asp:textbox></td>
															</tr>
															<tr>
																<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="Delivery_zipcode" tabIndex="6"
																		runat="server" Width="114px" CssClass="textField" MaxLength="100" ReadOnly="False" AutoPostBack="True"></asp:textbox>&nbsp;<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Delivery_state"
																		tabIndex="14" runat="server" Width="184px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>
																</td>
															</tr>
															<tr>
																<td><asp:label style="Z-INDEX: 0" id="Label13" runat="server" CssClass="tableLabel">Phone</asp:label>&nbsp;
																	<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Delivery_telephone"
																		tabIndex="14" runat="server" Width="96px" CssClass="textField" Enabled="False" MaxLength="100"
																		ReadOnly="True"></asp:textbox>&nbsp;
																	<asp:label style="Z-INDEX: 0" id="Label14" runat="server" CssClass="tableLabel">Fax</asp:label>&nbsp;
																	<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Delivery_fax"
																		tabIndex="14" runat="server" Width="117px" CssClass="textField" Enabled="False" MaxLength="100"
																		ReadOnly="True"></asp:textbox></td>
															</tr>
															<tr>
																<td><asp:label style="Z-INDEX: 0" id="Label15" runat="server" CssClass="tableLabel">Date of Delivery</asp:label>&nbsp;
																	<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="DeliveryDate"
																		tabIndex="14" runat="server" Width="136px" CssClass="textField" Enabled="False" MaxLength="100"
																		ReadOnly="True"></asp:textbox></td>
															</tr>
														</tbody>
													</table>
												</fieldset>
											</td>
										</tr>
										<TR>
											<TD colSpan="4">
												<fieldset style="Z-INDEX: 0; WIDTH: 690px"><LEGEND style="COLOR: black">Job Details</LEGEND>
													<TABLE style="MARGIN: 5px">
														<TR>
															<TD>
																<asp:label style="Z-INDEX: 0" id="Label28" runat="server" CssClass="tableLabel">Weight</asp:label></TD>
															<TD>
																<cc1:mstextbox style="Z-INDEX: 0" id="TotalWeight" tabIndex="6" onpaste="AfterPasteNonNumberWeight(this)"
																	runat="server" CssClass="textFieldRightAlign" Width="65px" MaxLength="14" TextMaskType="msNumericCOD"
																	NumberScale="1" NumberMaxValueCOD="999999.9" NumberMinValue="0" NumberPrecision="8"></cc1:mstextbox></TD>
															<TD>
																<asp:label style="Z-INDEX: 0" id="Label18" runat="server" CssClass="tableLabel" Width="80px">Ship/Flight No</asp:label></TD>
															<TD>
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="ShipFlightNo"
																	tabIndex="14" runat="server" CssClass="textField" Width="105px" Enabled="False" MaxLength="100"
																	ReadOnly="True"></asp:textbox></TD>
															<TD>
																<asp:label style="Z-INDEX: 0" id="Label19" runat="server" CssClass="tableLabel">Arrival Date</asp:label></TD>
															<TD>
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="ActualArrivalDate"
																	tabIndex="14" runat="server" CssClass="textField" Width="70px" Enabled="False" MaxLength="100"
																	ReadOnly="True"></asp:textbox></TD>
															<TD>
																<asp:label style="Z-INDEX: 0" id="Label20" runat="server" CssClass="tableLabel">From</asp:label></TD>
															<TD>
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="From_City"
																	tabIndex="14" runat="server" CssClass="textField" Width="30px" Enabled="False" MaxLength="100"
																	ReadOnly="True"></asp:textbox></TD>
															<TD>
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="From_CountryCode"
																	tabIndex="14" runat="server" CssClass="textField" Width="30px" Enabled="False" MaxLength="100"
																	ReadOnly="True"></asp:textbox></TD>
															<TD>
																<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="From_CountryName"
																	tabIndex="14" runat="server" CssClass="textField" Width="120px" Enabled="False" MaxLength="200"
																	ReadOnly="True"></asp:textbox></TD>
														</TR>
														<TR>
															<TD>
																<asp:label style="Z-INDEX: 0" id="Label32" runat="server" CssClass="tableLabel">Chg Wt</asp:label></TD>
															<TD><FONT face="Tahoma">
																	<cc1:mstextbox style="Z-INDEX: 0" id="ChargeableWeight" tabIndex="6" onpaste="AfterPasteNonNumberWeight(this)"
																		runat="server" CssClass="textFieldRightAlign" Width="65px" MaxLength="14" TextMaskType="msNumericCOD"
																		NumberScale="1" NumberMaxValueCOD="999999.9" NumberMinValue="0" NumberPrecision="8"></cc1:mstextbox></FONT></TD>
															<TD>
																<asp:label style="Z-INDEX: 0" id="Label33" runat="server" CssClass="tableLabel" Width="80px">Packages</asp:label>
                                                            </TD>
                                                            <TD>                                                            
                                                                <asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Packages"
																tabIndex="14" runat="server" CssClass="textField" Width="105px" Enabled="False" MaxLength="200"
																ReadOnly="True"></asp:textbox>															
															</TD>
															<TD><FONT face="Tahoma"></FONT></TD>
															<TD><FONT face="Tahoma"></FONT></TD>
															<TD><FONT face="Tahoma"></FONT></TD>
															<TD><FONT face="Tahoma"></FONT></TD>
															<TD><FONT face="Tahoma"></FONT></TD>
															<TD><FONT face="Tahoma"></FONT></TD>
														</TR>                                                       
                                                        <TR>
                                                            <TD>
																<asp:label style="Z-INDEX: 0" id="Label35" runat="server" CssClass="tableLabel">Description</asp:label>
                                                            </TD>
                                                            <TD colspan="9">                                                            
                                                                <asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Description1"
																tabIndex="14" runat="server" CssClass="textField" Width="260px" Enabled="False" MaxLength="200"
																ReadOnly="True"></asp:textbox>															    
															</TD>	
                                                        </TR>
                                                        <TR>
                                                            <TD><FONT face="Tahoma"></FONT></TD>
                                                            <TD colspan="9">
                                                                <FONT face="Tahoma">
                                                                    <asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Description2"
																	tabIndex="14" runat="server" CssClass="textField" Width="260px" Enabled="False" MaxLength="200"
																	ReadOnly="True"></asp:textbox>
															    </FONT>
															</TD>				
                                                        </TR>
                                                        <TR>
                                                            <TD><FONT face="Tahoma"></FONT></TD>
                                                            <TD colspan="9">
                                                                <FONT face="Tahoma">
                                                                    <asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Description3"
																	tabIndex="14" runat="server" CssClass="textField" Width="260px" Enabled="False" MaxLength="200"
																	ReadOnly="True"></asp:textbox>
															    </FONT>
															</TD>												
                                                        </TR>
                                                        <TR>
                                                            <TD><FONT face="Tahoma"></FONT></TD>
                                                            <TD colspan="9">
                                                                <FONT face="Tahoma">
                                                                    <asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Description4"
																	tabIndex="14" runat="server" CssClass="textField" Width="260px" Enabled="False" MaxLength="200"
																	ReadOnly="True"></asp:textbox>
															    </FONT>
															</TD>														
                                                        </TR>
                                                        <TR>
                                                            <TD><FONT face="Tahoma"></FONT></TD>	
                                                            <TD colspan="9">
                                                                <FONT face="Tahoma">
                                                                    <asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Description5"
																	tabIndex="14" runat="server" CssClass="textField" Width="260px" Enabled="False" MaxLength="200"
																	ReadOnly="True"></asp:textbox>
															    </FONT>
															</TD>			                                                            
                                                        </TR>
                                                        <TR>
                                                            <TD><FONT face="Tahoma"></FONT></TD>
                                                            <TD colspan="9">
                                                                <FONT face="Tahoma">
                                                                    <asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="Description6"
																	tabIndex="14" runat="server" CssClass="textField" Width="260px" Enabled="False" MaxLength="200"
																	ReadOnly="True"></asp:textbox>
															    </FONT>
															</TD>                                                           
                                                        </TR>
													</TABLE>
												</fieldset>
											</TD>
										</TR>
										<tr>
											<td style="WIDTH: 351px" colSpan="2">
												<table style="Z-INDEX: 0">
													<tr>
														<td colSpan="2">
															<fieldset style="WIDTH: 344px"><legend style="COLOR: black">Customs Declaration</legend>
																<table style="MARGIN: 5px">
																	<tr>
																		<td>
																			<asp:label style="Z-INDEX: 0;MARGIN-RIGHT: 5px" id="Label16" runat="server" CssClass="tableLabel">Folio No</asp:label>
																			<asp:textbox style="Z-INDEX: 0" id="FolioNumber" tabIndex="7" onkeypress="validate(event)" onpaste="AfterPasteNonNumber(this)"
																				runat="server" CssClass="textField" Width="75px" MaxLength="20"></asp:textbox>
																			<asp:label style="Z-INDEX: 0;MARGIN-LEFT: 5px" id="Label17" runat="server" CssClass="tableLabel">Declaration Form Pages</asp:label>
																			<asp:textbox style="Z-INDEX: 0" id="DeclarationFormPages" tabIndex="8" onkeypress="validate(event)"
																				onpaste="AfterPasteNonNumber(this)" runat="server" CssClass="textField" Width="40px" MaxLength="3"></asp:textbox>
																		</td>
																	</tr>
																	<tr>
																		<td><FONT face="Tahoma">
																				<asp:label style="Z-INDEX: 0" id="Label29" runat="server" CssClass="tableLabel" Width="120px">Assessment Number</asp:label>
																				<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="AssessmentNumber"
																					tabIndex="14" runat="server" CssClass="textField" Width="70px" Enabled="False" MaxLength="100"
																					ReadOnly="True"></asp:textbox>
																				<asp:label style="Z-INDEX: 0;MARGIN-LEFT: 12px" id="Label31" runat="server" CssClass="tableLabel">Date</asp:label>
																				<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="AssessmentDate"
																					tabIndex="14" runat="server" CssClass="textField" Width="75px" Enabled="False" MaxLength="100"
																					ReadOnly="True"></asp:textbox></FONT></td>
																	</tr>
																	<TR>
																		<TD>
																			<asp:label style="Z-INDEX: 0" id="Label30" runat="server" CssClass="tableLabel" Width="120px">Assessment Amount</asp:label>
																			<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="AssessmentAmount"
																				tabIndex="14" runat="server" CssClass="textField" Width="100px" Enabled="False" MaxLength="100"
																				ReadOnly="True"></asp:textbox></TD>
																	</TR>
																</table>
															</fieldset>
														</td>
													</tr>
												</table>
											</td>
											<td colSpan="2" valign="top">
												<fieldset style="WIDTH: 344px; HEIGHT: 106px"><legend style="COLOR: black">Description 
														of Other Charges</legend>
													<table style="MARGIN: 5px">
														<tbody>
															<tr>
																<td><asp:textbox style="Z-INDEX: 0" id="Desc_OthCharges1" tabIndex="9" runat="server" Width="304"
																		CssClass="textField"></asp:textbox></td>
															</tr>
															<tr>
																<td><asp:textbox style="Z-INDEX: 0" id="Desc_OthCharges2" tabIndex="10" runat="server" Width="304"
																		CssClass="textField"></asp:textbox></td>
															</tr>
															<tr>
																<td style="HEIGHT: 22px"><asp:textbox style="Z-INDEX: 0" id="Desc_OthCharges3" tabIndex="11" runat="server" Width="304"
																		CssClass="textField"></asp:textbox></td>
															</tr>
															<tr>
																<td><asp:textbox style="Z-INDEX: 0" id="Desc_OthCharges4" tabIndex="12" runat="server" Width="304"
																		CssClass="textField"></asp:textbox></td>
															</tr>
														</tbody>
													</table>
												</fieldset>
											</td>
										</tr>
										<tr>
											<td colSpan="4">
												<fieldset style="WIDTH: 680px; HEIGHT: 63px"><legend style="COLOR: black">Invoice 
														Totals</legend>
													<table style="MARGIN: 5px" width="100%">
														<tbody>
															<tr>
																<td style="WIDTH: 89px"><asp:label style="Z-INDEX: 0" id="Label21" runat="server" CssClass="tableLabel">Agency Fees</asp:label></td>
																<td style="WIDTH: 96px">
																	<asp:textbox style="Z-INDEX: 0; TEXT-ALIGN: right; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea"
																		id="AgencyFees" tabIndex="14" runat="server" Width="125px" CssClass="textField" Enabled="False"
																		MaxLength="100" ReadOnly="True"></asp:textbox>
																</td>
																<td style="WIDTH: 102px"><asp:label style="Z-INDEX: 0" id="Label22" runat="server" CssClass="tableLabel">Disbursements</asp:label></td>
																<td style="WIDTH: 188px">
																	<asp:textbox style="Z-INDEX: 0; TEXT-ALIGN: right; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea"
																		id="Disbursements" tabIndex="14" runat="server" Width="125px" CssClass="textField" Enabled="False"
																		MaxLength="100" ReadOnly="True"></asp:textbox></td>
																<td><asp:label style="Z-INDEX: 0" id="Label23" runat="server" CssClass="tableLabel">Total</asp:label></td>
																<td><asp:textbox style="Z-INDEX: 0; TEXT-ALIGN: right; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea"
																		id="TotalInvoice" tabIndex="14" runat="server" Width="125px" CssClass="textField" Enabled="False"
																		MaxLength="100" ReadOnly="True"></asp:textbox></td>
															</tr>
														</tbody>
													</table>
												</fieldset>
											</td>
										</tr>
										<tr>
											<td colSpan="4">
												<fieldset style="WIDTH: 690px; HEIGHT: 87px"><legend style="COLOR: black">History</legend>
													<table style="MARGIN: 5px">
														<tbody>
															<tr>
																<td style="WIDTH: 89px"><asp:label style="Z-INDEX: 0" id="Label24" runat="server" CssClass="tableLabel">Prepared by</asp:label></td>
																<td style="WIDTH: 254px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="PreparedBy"
																		tabIndex="14" runat="server" Width="91px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="PreparedDT"
																		tabIndex="14" runat="server" Width="137px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
																<td><asp:label style="Z-INDEX: 0" id="Label25" runat="server" CssClass="tableLabel">Modified by</asp:label></td>
																<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="ModifiedBy"
																		tabIndex="14" runat="server" Width="91px" CssClass="textField" Enabled="False" MaxLength="100"
																		ReadOnly="True"></asp:textbox><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="ModifiedDT"
																		tabIndex="14" runat="server" Width="137px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
															</tr>
															<tr>
																<td><asp:label style="Z-INDEX: 0" id="Label26" runat="server" CssClass="tableLabel">Printed by</asp:label></td>
																<td style="WIDTH: 254px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="PrintedBy"
																		tabIndex="14" runat="server" Width="91px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="PrintedDT"
																		tabIndex="14" runat="server" Width="137px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
																<td><asp:label style="Z-INDEX: 0" id="Label27" runat="server" CssClass="tableLabel">Exported by</asp:label></td>
																<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="ExportedBy"
																		tabIndex="14" runat="server" Width="91px" CssClass="textField" Enabled="False" MaxLength="100"
																		ReadOnly="True"></asp:textbox><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="ExportedDT"
																		tabIndex="14" runat="server" Width="137px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
															</tr>
														</tbody>
													</table>
												</fieldset>
											</td>
										</tr>
										<tr class="gridHeading">
											<td colSpan="4"><STRONG><FONT size="2"><label id="InvoiceDetails">Invoice Details</label></FONT></STRONG>&nbsp;
												<a id="aFocus" href="#"></a>
											</td>
										</tr>
										<tr>
											<td colSpan="4">
												<asp:button id="btnClientEvent" style="DISPLAY: none" runat="server" CausesValidation="False"
													Text="ClientEvent"></asp:button>
												<asp:datagrid style="Z-INDEX: 0" id="GrdInvoiceDetails" tabIndex="42" runat="server" Width="100%"
													AutoGenerateColumns="False" AlternatingItemStyle-CssClass="gridField" ItemStyle-CssClass="gridField"
													HeaderStyle-CssClass="gridHeading" CellPadding="0" CellSpacing="1" BorderWidth="0px" FooterStyle-CssClass="gridHeading">
													<FooterStyle CssClass="gridHeading"></FooterStyle>
													<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
													<ItemStyle CssClass="gridField" Height="25px"></ItemStyle>
													<HeaderStyle CssClass="gridHeading"></HeaderStyle>
													<Columns>
														<asp:TemplateColumn>
															<HeaderStyle Width="11%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:LinkButton id="btnEditItem" Runat="server" Text="<img src='images/butt-edit.gif' alt='edit' border=0>"
																	CommandName="EDIT_ITEM" />
															</ItemTemplate>
															<EditItemTemplate>
																<asp:LinkButton id="btnSaveItem" Runat="server" Text="<img src='images/butt-update.gif' alt='save' border=0>"
																	CommandName="SAVE_ITEM" />
																<asp:LinkButton id="btnCancelItem" Runat="server" Text="<img src='images/butt-red.gif' alt='cancel' border=0>"
																	CommandName="CANCEL_ITEM" />
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Code">
															<HeaderStyle HorizontalAlign="Center" Width="7%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:label id="lblCode" Text='<%# DataBinder.Eval(Container.DataItem,"Code") %>' runat="server">
																</asp:label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:label id="lblConsignment_no" Text='<%# DataBinder.Eval(Container.DataItem,"consignment_no") %>' runat="server" Visible="False">
																</asp:label>
																<asp:label id="lblPayerid" Text='<%# DataBinder.Eval(Container.DataItem,"payerid") %>' runat="server" Visible="False">
																</asp:label>
																<asp:label id="lblInvoice_No" Text='<%# DataBinder.Eval(Container.DataItem,"Invoice_No") %>' runat="server" Visible="False">
																</asp:label>
																<asp:textbox id="txtCode" Text='<%# DataBinder.Eval(Container.DataItem,"Code") %>' Enabled="False" runat="server" Width="60px" CssClass="gridTextBox">
																</asp:textbox>
															</EditItemTemplate>
															<FooterStyle HorizontalAlign="Left"></FooterStyle>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Fee/Disb">
															<HeaderStyle HorizontalAlign="Center" Width="9%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:label id="lblFeeDisb" runat="server">
																	<%# DataBinder.Eval(Container.DataItem,"FeeDisb") %>
																</asp:label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:textbox id="txtFeeDisb" Text='<%# DataBinder.Eval(Container.DataItem,"FeeDisb") %>' Enabled="False" runat="server" Width="60px" CssClass="gridTextBox">
																</asp:textbox>
															</EditItemTemplate>
															<FooterStyle HorizontalAlign="Left"></FooterStyle>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Description">
															<HeaderStyle HorizontalAlign="Center" Width="29%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Left"></ItemStyle>
															<ItemTemplate>
																<asp:label id="lblDescription" runat="server">
																	<%# DataBinder.Eval(Container.DataItem,"Description") %>
																</asp:label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:textbox id="txtDescription" Text='<%# DataBinder.Eval(Container.DataItem,"Description") %>' Enabled="False" runat="server" Width="200px" CssClass="gridTextBox">
																</asp:textbox>
															</EditItemTemplate>
															<FooterStyle HorizontalAlign="Left"></FooterStyle>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="GL Code">
															<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:label id="lblGLCode" runat="server">
																	<%# DataBinder.Eval(Container.DataItem,"GLCode") %>
																</asp:label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:textbox id="txtGLCode" Text='<%# DataBinder.Eval(Container.DataItem,"GLCode") %>' Enabled="False" runat="server" Width="60px" CssClass="gridTextBox">
																</asp:textbox>
															</EditItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Calculated">
															<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
															<ItemTemplate>
																<asp:label id="lblCalculated" runat="server"></asp:label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:label id="txtCalculatedAmount" runat="server"></asp:label>
															</EditItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Override">
															<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
															<ItemTemplate>
																<asp:label id="lblOverride" runat="server"></asp:label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtOverrideAmount" onpaste="AfterPasteNonNumber(this)"
																	Runat="server" Enabled="True" TextMaskType="msNumeric" MaxLength="14" NumberMaxValue="100000000"
																	NumberMinValue="0" NumberPrecision="14" NumberScale="5"></cc1:mstextbox>
															</EditItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Invoice Amount">
															<HeaderStyle HorizontalAlign="Center" Width="14%"></HeaderStyle>
															<ItemStyle HorizontalAlign="Right"></ItemStyle>
															<ItemTemplate>
																<asp:label id="lblInvoice" runat="server"></asp:label>
															</ItemTemplate>
															<FooterStyle HorizontalAlign="Center"></FooterStyle>
														</asp:TemplateColumn>
													</Columns>
												</asp:datagrid>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</TABLE>
			</div>
			<INPUT 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition> <INPUT type="hidden" name="hdnRefresh">
		</form>
	</body>
</HTML>
