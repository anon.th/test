using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TIESDAL;
using TIESClasses;

using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;

using com.common.applicationpages;
using com.ties;
using System.Text;
using System.Globalization;
using System.IO;


namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for RetrieveCustomerConsignments.
	/// </summary>
	public class RetrieveCustomerConsignments : BasePage
	{
		protected System.Web.UI.WebControls.ValidationSummary Validationsummary1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.DropDownList Drp_Location;
		protected System.Web.UI.WebControls.Button btnHidReport;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Panel Panel1;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Panel pnlImport;
		protected System.Web.UI.WebControls.Label lblImportSIPs;
		protected System.Web.UI.HtmlControls.HtmlGenericControl fsImport;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.TextBox txtShipListNo;
		protected com.common.util.msTextBox txtBookingNo;
		protected com.common.util.msTextBox txtActDate;
		protected System.Web.UI.WebControls.TextBox txtBookingDate;
		protected System.Web.UI.WebControls.TextBox txtCustID;
		protected System.Web.UI.WebControls.TextBox txtCustName;
		protected System.Web.UI.WebControls.TextBox txtBillingAddr1;
		protected System.Web.UI.WebControls.TextBox txtBillingAddr2;
		protected System.Web.UI.WebControls.TextBox txtZipCode;
		protected System.Web.UI.WebControls.TextBox txtStateName;
		protected System.Web.UI.WebControls.TextBox txtEstDate;
		protected System.Web.UI.WebControls.TextBox txtSenderName;
		protected System.Web.UI.WebControls.TextBox txtSenderAddr1;
		protected System.Web.UI.WebControls.TextBox txtSenderAddr2;
		protected System.Web.UI.WebControls.TextBox txtSenderZipcode;
		protected System.Web.UI.WebControls.TextBox txtSenderStateName;
		protected System.Web.UI.WebControls.TextBox txtSenderPhone;
		protected System.Web.UI.WebControls.TextBox txtSenderContactName;
		protected System.Web.UI.WebControls.DataGrid grdConList;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
	
		public int DSMode
		{
			get
			{
				if(ViewState["iDSMode"]==null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["iDSMode"];
				}
			}
			set
			{
				ViewState["iDSMode"]=value;
			}
		}

		public int DSOperation
		{
			get
			{
				if(ViewState["iDSOperation"]==null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["iDSOperation"];
				}
			}
			set
			{
				ViewState["iDSOperation"]=value;
			}
		}


		private string appID = null;
		private string enterpriseID = null;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Button btnRetrieveCons;
		protected System.Web.UI.WebControls.Button btnCustomsRetrieveCons;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.HtmlControls.HtmlGenericControl Fieldset1;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.HtmlControls.HtmlGenericControl Fieldset2;
		protected System.Web.UI.WebControls.TextBox Invoice_No;
		protected System.Web.UI.WebControls.TextBox txtConsNo;
		protected System.Web.UI.WebControls.Button btnExecQryHidden;
		private string userID = null;
		private bool IsAssigned
		{
			get
			{
				if(ViewState["IsAssigned"]==null)
				{
					ViewState["IsAssigned"]=false;
				}
				return (bool)ViewState["IsAssigned"];
			}
			set
			{
				ViewState["IsAssigned"]=value;
			}
		}
		public string ConsList
		{
			get
			{
				if(ViewState["ConsList"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["ConsList"];
				}
			}
			set
			{
				ViewState["ConsList"]=value;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();

			if(this.IsPostBack==false)
			{
				//txtShipListNo.Attributes.Add("onkeypress","if (window.event.keyCode >= 48 && window.event.keyCode <= 57) return ture; else return false;");
				txtShipListNo.Attributes.Add("onkeypress","if (window.event.keyCode == 13 ) { document.getElementById('txtBookingNo').focus(); return false;} else {return true;}");

				

				DSMode = (int)ScreenMode.None;
				DSOperation = (int)Operation.None;
				clearscreen();
				SetInitialFocus(txtShipListNo);
							
			}
			this.btnRetrieveCons.Attributes.Add("onclick", "javascript:document.getElementById('"+ this.btnRetrieveCons.ClientID + "').disabled=true;" + GetPostBackEventReference(this.btnRetrieveCons));
			this.btnCustomsRetrieveCons.Attributes.Add("onclick","javascript:document.getElementById('"+ this.btnCustomsRetrieveCons.ClientID + "').disabled=true;" + GetPostBackEventReference(this.btnCustomsRetrieveCons));	
			//this.btnCustomsRetrieveCons.Attributes.Add("onclick","javascript:document.getElementById('"+ this.btnCustomsRetrieveCons.ClientID + "').disabled=true;" + GetPostBackEventReference(this.btnCustomsRetrieveCons));
		}

		private void BindGrid()
		{
			string ShipListNo = null;
			int bookingNo=-1;
			if(this.txtShipListNo.Text.Trim() != "")
			{
				ShipListNo=this.txtShipListNo.Text.Trim();
			}
			if(this.txtBookingNo.Text.Trim() != "")
			{
				try
				{
					bookingNo = Convert.ToInt32(this.txtBookingNo.Text.Trim());
				}
				catch
				{
					bookingNo=-1;
				}
			}

			System.Data.DataSet ds = CustomerConsignmentDAL.CSS_RetrieveCustomerCons(this.appID,this.enterpriseID,ShipListNo,bookingNo,Invoice_No.Text.Trim(),txtConsNo.Text.Trim());
			DataTable dtStatus = ds.Tables[0];
			DataTable dtPickup = ds.Tables[1];
			DataTable dtCons = ds.Tables[2];
			lblErrorMsg.Text="";
			btnRetrieveCons.Enabled=false;
			btnCustomsRetrieveCons.Enabled=false;
			if(dtStatus.Rows.Count>0)
			{
				if(dtStatus.Rows[0]["ErrorCode"].ToString() != "0")
				{
					lblErrorMsg.Text =dtStatus.Rows[0]["ErrorMessage"].ToString();
				}
				else
				{
					if(dtStatus.Rows[0]["RetrieveConsEnabled"].ToString() == "1")
					{
						btnRetrieveCons.Enabled=true;
					}
					if(dtStatus.Rows[0]["InvoiceReceivedEnabled"].ToString() == "1")
					{
						btnCustomsRetrieveCons.Enabled=true;
					}
				}				
			}
			if(dtPickup.Rows.Count>0)
			{
				System.Data.DataRow dr = dtPickup.Rows[0];
				txtActDate.Text=(dr["act_pickup_datetime"] != DBNull.Value?Convert.ToDateTime(dr["act_pickup_datetime"]).ToString("dd/MM/yyyy HH:mm",null):"");
				txtBookingNo.Text=dr["booking_no"].ToString();
				txtBookingDate.Text=(dr["booking_datetime"] != DBNull.Value?Convert.ToDateTime(dr["booking_datetime"]).ToString("dd/MM/yyyy HH:mm",null):"");
				txtCustID.Text=dr["payerid"].ToString();
				txtCustName.Text=dr["payer_name"].ToString();
				txtBillingAddr1.Text=dr["payer_address1"].ToString();
				txtBillingAddr2.Text=dr["payer_address2"].ToString();
				txtZipCode.Text=dr["payer_zipcode"].ToString();
				txtStateName.Text=dr["payer_state_name"].ToString();
				txtEstDate.Text=(dr["est_pickup_DT"] != DBNull.Value?Convert.ToDateTime(dr["est_pickup_DT"]).ToString("dd/MM/yyyy HH:mm",null):"");
				txtSenderName.Text=dr["sender_name"].ToString();
				txtSenderAddr1.Text=dr["sender_address1"].ToString();
				txtSenderAddr2.Text=dr["sender_address2"].ToString();
				txtSenderZipcode.Text=dr["sender_zipcode"].ToString();
				txtSenderStateName.Text=dr["sender_state_name"].ToString();
				txtSenderPhone.Text=dr["sender_telephone"].ToString();
				txtSenderContactName.Text=dr["sender_contact_person"].ToString();
			}
			else
			{
				txtActDate.Text="";
				txtBookingNo.Text="";
				txtBookingDate.Text="";
				txtCustID.Text="";
				txtCustName.Text="";
				txtBillingAddr1.Text="";
				txtBillingAddr2.Text="";
				txtZipCode.Text="";
				txtStateName.Text="";
				txtEstDate.Text="";
				txtSenderName.Text="";
				txtSenderAddr1.Text="";
				txtSenderAddr2.Text="";
				txtSenderZipcode.Text="";
				txtSenderStateName.Text="";
				txtSenderPhone.Text="";
				txtSenderContactName.Text="";
			}

			if(ShipListNo != null && ShipListNo.Trim() != "")
			{
				this.IsAssigned=false;
				DataSet dsCSSU = SysDataMgrDAL.UserAssignedRoles(this.appID,this.enterpriseID,this.userID,"CSSU");
				if(dsCSSU.Tables.Count>0 && dsCSSU.Tables[0].Rows.Count>0 && dsCSSU.Tables[0].Rows[0]["IsAssigned"] != null && dsCSSU.Tables[0].Rows[0]["IsAssigned"] != DBNull.Value)
				{
					if(dsCSSU.Tables[0].Rows[0]["IsAssigned"].ToString() =="1")
					{
						this.IsAssigned=true;
					}
					else
					{
						this.IsAssigned=false;
					}			
				}			
			}
			else
			{
				this.IsAssigned=true;
			}

			
			if(DSMode==(int)ScreenMode.ExecuteQuery)
			{
				string strCons="";
				foreach(DataRow dr in dtCons.Rows)
				{
					if(strCons.Length>0)
					{
						strCons+=";";
					}
					strCons+=dr["consignment_no"].ToString();
				}
				this.ConsList=strCons;
				DSMode = (int)ScreenMode.None;
			}

			DataColumn colCheck = new DataColumn("Check",typeof(bool));
			colCheck.DefaultValue = true;
			dtCons.Columns.Add(colCheck);

			this.grdConList.DataSource=dtCons;
			this.grdConList.DataBind();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnRetrieveCons.Click += new System.EventHandler(this.btnRetrieveCons_Click);
			this.btnCustomsRetrieveCons.Click += new System.EventHandler(this.btnCustomsRetrieveCons_Click);
			this.btnExecQryHidden.Click += new System.EventHandler(this.btnExecQryHidden_Click);
			this.grdConList.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.grdConList_PageIndexChanged);
			this.grdConList.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdConList_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			DSMode = (int)ScreenMode.Query;
			DSOperation = (int)Operation.None;
			clearscreen();
			SetInitialFocus(txtShipListNo);
		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			if(txtShipListNo.Text.Trim() != "" && (Invoice_No.Text.Trim() != "" || txtConsNo.Text.Trim() != ""))
			{
				lblErrorMsg.Text="Enter only one of Shipping list or consigment number or invoice number to retrieve.";
				return;
			}

			if(txtBookingNo.Text.Trim() =="" && txtActDate.Text.Trim() =="")
			{
				if(txtShipListNo.Text.Trim() == "" && Invoice_No.Text.Trim() == "" && txtConsNo.Text.Trim() == "")
				{
					lblErrorMsg.Text="Enter only one of Shipping list or consigment number or invoice number to retrieve.";
					return;
				}
			}

			this.ConsList="";
			DSMode = (int)ScreenMode.ExecuteQuery;
			DSOperation = (int)Operation.None;
			BindGrid();
			if(lblErrorMsg.Text.Trim() =="")
			{
				txtShipListNo.Enabled=false;
				txtBookingNo.Enabled=false;
				txtActDate.Enabled=false;
				Invoice_No.Enabled=false;
				txtConsNo.Enabled=false;
			}
		}

		private void clearscreen()
		{
			lblErrorMsg.Text="";
			txtShipListNo.Text="";
			txtBookingNo.Text="";
			txtActDate.Text="";
			txtBookingDate.Text="";
			txtCustID.Text="";
			txtCustName.Text="";
			txtBillingAddr1.Text="";
			txtBillingAddr2.Text="";
			txtZipCode.Text="";
			txtStateName.Text="";
			txtEstDate.Text="";
			txtSenderName.Text="";
			txtSenderAddr1.Text="";
			txtSenderAddr2.Text="";
			txtSenderZipcode.Text="";
			txtSenderStateName.Text="";
			txtSenderPhone.Text="";
			txtSenderContactName.Text="";
			Invoice_No.Text="";
			txtConsNo.Text="";

			txtShipListNo.Enabled=true;
			txtBookingNo.Enabled=true;
			txtActDate.Enabled=true;
			Invoice_No.Enabled=true;
			txtConsNo.Enabled=true;

			btnExecQry.Enabled=true;
			btnRetrieveCons.Enabled=false;
			btnCustomsRetrieveCons.Enabled=false;

			ConsList="";

			IsAssigned=false;

			DataTable dtList = CustomerConsignmentDAL.ConsignmentParameter();
			dtList.Columns.Add("Check",typeof(bool));
			this.grdConList.CurrentPageIndex=0;
			this.grdConList.DataSource = dtList;
			this.grdConList.DataBind();
		}

		private void btnRetrieveCons_Click(object sender, System.EventArgs e)
		{
			this.lblErrorMsg.Text="";
			ConsList="";			
			string cons="";
			foreach(DataGridItem item in grdConList.Items)
			{
				CheckBox chkConNo = (CheckBox)item.FindControl("cbkConsignment_no");
				if(chkConNo != null && chkConNo.Checked==true)
				{
					string id = chkConNo.Attributes["CrsID"].ToString();
					if(cons.Length>0)
					{
						cons+=";";
					}
					cons+=id;
				}
			}
			ConsList=cons;

			if(this.ConsList.Length>0)
			{
				if(Invoice_No.Text.Trim() != "")
				{
					DataSet dsInvoice = CustomerConsignmentDAL.Customs_InvoiceReceivedByOps(appID,enterpriseID,userID,Invoice_No.Text.Trim());
					if(dsInvoice !=null && dsInvoice.Tables[0].Rows.Count>0)
					{
						if(Convert.ToInt32(dsInvoice.Tables[0].Rows[0]["ErrorCode"]) > 0)
						{
							this.lblErrorMsg.Text = dsInvoice.Tables[0].Rows[0]["ErrorMessage"].ToString();
							return;
						}
					}
				}

				int bookingNo=-1;
				if(this.txtBookingNo.Text.Trim() != "")
				{
					try
					{
						bookingNo = Convert.ToInt32(this.txtBookingNo.Text.Trim());
					}
					catch
					{
						bookingNo=-1;
					}
				}
				DateTime dPupDate =DateTime.MinValue;
				if(txtActDate.Text.Trim() != "")
				{
					try
					{
						IFormatProvider culture = new CultureInfo("en-GB", true);
						dPupDate = Convert.ToDateTime(txtActDate.Text,culture);

						//dPupDate =DateTime.ParseExact(txtActDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
					}
					catch(Exception ex)
					{
						dPupDate=DateTime.MinValue;
						throw ex;
					}
				}
				string strConsignmentNo = this.ConsList;
				DataSet dsShipping = CustomerConsignmentDAL.CSS_RetrieveConsignmentsToDMS(appID,enterpriseID,userID,strConsignmentNo,bookingNo,dPupDate);
				if(dsShipping !=null && dsShipping.Tables[0].Rows.Count>0)
				{
					if(Convert.ToInt32(dsShipping.Tables[0].Rows[0]["ErrorCode"]) > 0)
					{
						this.lblErrorMsg.Text = dsShipping.Tables[0].Rows[0]["ErrorMessage"].ToString();
					}
					else
					{

						BindGrid();
						btnExecQry.Enabled=false;
						btnRetrieveCons.Enabled=false;
						btnCustomsRetrieveCons.Enabled=false;
						if(txtConsNo.Text.Trim() != "")
						{
							DataTable dtList = CustomerConsignmentDAL.ConsignmentParameter();
							dtList.Columns.Add("Check",typeof(bool));
							this.grdConList.CurrentPageIndex=0;
							this.grdConList.DataSource = dtList;
							this.grdConList.DataBind();
						}
						DSMode = (int)ScreenMode.None;
						DSOperation = (int)Operation.None;
					}
					this.lblErrorMsg.Text = dsShipping.Tables[0].Rows[0]["ErrorMessage"].ToString();
				}
				
			}
			else
			{
				this.lblErrorMsg.Text = "No consignment selected.";
			}
		}
		

		public void changed(object sender, System.EventArgs e)
		{
			string id = string.Empty;
			string lst = ConsList;
			if (((CheckBox)sender).Checked)
			{
				id = ((CheckBox)sender).Attributes["CrsID"].ToString();
				if(lst != "")
				{
					lst+=";";
				}
				lst+=id;
				//arrCourse.Add(id);
			}
			else
			{
				id = ((CheckBox)sender).Attributes["CrsID"].ToString();
				//arrCourse.Remove(id);
				lst = lst.Replace(id,"");
				lst = lst.Replace(";;",";");
				if(lst.Length==1 && lst==";")
				{
					lst="";
				}
			}
			ConsList=lst;
		}

		private void grdConList_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			grdConList.CurrentPageIndex = e.NewPageIndex;
			grdConList.SelectedIndex = -1;
			grdConList.EditItemIndex = -1;
			BindGrid();	
		}

		private void grdConList_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if((e.Item.ItemType == ListItemType.Item) || 
				(e.Item.ItemType == ListItemType.AlternatingItem))
			{
				CheckBox cbkConsignment_no = (CheckBox)e.Item.FindControl("cbkConsignment_no");
				string lblConsignment_no = DataBinder.Eval(e.Item.DataItem,"consignment_no").ToString();
				if(cbkConsignment_no != null)
				{					
					string strCons =this.ConsList;
					if(strCons.IndexOf(lblConsignment_no.Trim())==-1)
					{
						cbkConsignment_no.Checked=false;
					}
					cbkConsignment_no.Enabled=this.IsAssigned;					
				}
			} 	
		}

		private void txtShipListNo_TextChanged(object sender, EventArgs e)
		{
			btnExecQry_Click(null,null);
		}

		private void btnCustomsRetrieveCons_Click(object sender, System.EventArgs e)
		{
			DataSet dsShipping = CustomerConsignmentDAL.Customs_InvoiceReceivedByOps(appID,enterpriseID,userID,Invoice_No.Text.Trim());
			if(dsShipping !=null && dsShipping.Tables[0].Rows.Count>0)
			{
				if(Convert.ToInt32( dsShipping.Tables[0].Rows[0]["ErrorCode"]) > 0)
				{
					this.lblErrorMsg.Text = dsShipping.Tables[0].Rows[0]["ErrorMessage"].ToString();
				}
				else
				{
					BindGrid();
					btnExecQry.Enabled=false;
					btnRetrieveCons.Enabled=false;
					btnCustomsRetrieveCons.Enabled=false;
					if(txtConsNo.Text.Trim() != "")
					{
						DataTable dtList = CustomerConsignmentDAL.ConsignmentParameter();
						dtList.Columns.Add("Check",typeof(bool));
						this.grdConList.CurrentPageIndex=0;
						this.grdConList.DataSource = dtList;
						this.grdConList.DataBind();
					}
					DSMode = (int)ScreenMode.None;
					DSOperation = (int)Operation.None;
				}
				this.lblErrorMsg.Text = dsShipping.Tables[0].Rows[0]["ErrorMessage"].ToString();
			}
		}

		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}

		private void btnExecQryHidden_Click(object sender, System.EventArgs e)
		{
			if(txtShipListNo.Text.Trim() != "" && (Invoice_No.Text.Trim() != "" || txtConsNo.Text.Trim() != ""))
			{
				lblErrorMsg.Text="Enter only one of Shipping list or consigment number or invoice number to retrieve.";
				return;
			}

			if(txtBookingNo.Text.Trim() =="" && txtActDate.Text.Trim() =="")
			{
				if(txtShipListNo.Text.Trim() == "" && Invoice_No.Text.Trim() == "" && txtConsNo.Text.Trim() == "")
				{
					lblErrorMsg.Text="Enter only one of Shipping list or consigment number or invoice number to retrieve.";
					return;
				}
			}

			this.ConsList="";
			DSMode = (int)ScreenMode.ExecuteQuery;
			DSOperation = (int)Operation.None;
			BindGrid();
			if(lblErrorMsg.Text.Trim() =="")
			{
				txtShipListNo.Enabled=false;
				txtBookingNo.Enabled=false;
				txtActDate.Enabled=false;
				Invoice_No.Enabled=false;
				txtConsNo.Enabled=false;
			}	
		}

	}
}
