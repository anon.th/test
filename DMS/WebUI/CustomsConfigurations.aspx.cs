using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.applicationpages;
using com.common.util;
using com.common.RBAC;
using TIESClasses;
using TIESDAL;

namespace TIES.WebUI
{
	public class CustomsConfigurations : BasePage
	{
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.WebControls.DataGrid gridConfiguration;
		protected System.Web.UI.WebControls.DropDownList dllCodeID;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
	
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.gridConfiguration.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.gridConfiguration_ItemCommand);
			this.gridConfiguration.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.gridConfiguration_PageIndexChanged);
			this.gridConfiguration.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.gridConfiguration_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private string appID = string.Empty;
		private string enterpriseID = string.Empty;
		private string userLoggin = string.Empty;

		public int countHeader
		{
			get
			{
				if(ViewState["countHeader"] == null)
					return 0;
				else
					return (int)ViewState["countHeader"];
			}
			set
			{
				ViewState["countHeader"] = value;
			}
		}
		public DataSet dsGridConfig
		{
			get
			{
				if(ViewState["dsGridConfig"] == null)
					return new DataSet();
				else
					return (DataSet)ViewState["dsGridConfig"];
			}
			set
			{
				ViewState["dsGridConfig"] = value;
			} 
		}
		public DataTable dtResult
		{
			get
			{
				if(ViewState["dtResult"] == null)
					return new DataTable();
				else
					return (DataTable)ViewState["dtResult"];
			}
			set
			{
				ViewState["dtResult"] = value;
			} 
		}
		public DataTable dtGrid
		{
			get
			{
				if(ViewState["dtGrid"] == null)
					return new DataTable();
				else
					return (DataTable)ViewState["dtGrid"];
			}
			set
			{
				ViewState["dtGrid"] = value;
			} 
		}
		

		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				System.Text.StringBuilder s = new System.Text.StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("SetScrollPosition();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}
		private void DisplayddlCodeID()
		{
			try
			{
				CustomsConfigurationsDAL db = new CustomsConfigurationsDAL(
					appID, 
					enterpriseID);
				TIESClasses.CustomsConfigurations objInfo = new TIESClasses.CustomsConfigurations(
					"0",
					enterpriseID);

				dllCodeID.DataSource = db.DropdownList(objInfo).Tables[1];
				dllCodeID.DataTextField = "DropDownListDisplayValue";
				dllCodeID.DataValueField = "CodedValue";
				dllCodeID.DataBind();
			}
			catch(Exception ex)
			{
				this.lblError.Text = ex.Message + "|" + ex.InnerException;
			}
		}

		private void DisplayGridView()
		{
			try
			{
				CustomsConfigurationsDAL db = new CustomsConfigurationsDAL(
					appID, 
					enterpriseID);
				TIESClasses.CustomsConfigurations objInfo = new TIESClasses.CustomsConfigurations(
					"0",
					enterpriseID,
					dllCodeID.SelectedValue);

				dsGridConfig = db.GridView(objInfo);

				if(dsGridConfig.Tables[0].Rows[0][0].ToString().Trim().Equals("0"))
				{
					this.lblError.Text = dsGridConfig.Tables[0].Rows[0][2].ToString();

					if(!dsGridConfig.Tables[1].Rows[3][1].ToString().Equals(""))
						gridConfiguration.PageSize = int.Parse(dsGridConfig.Tables[1].Rows[3][1].ToString());
					else
						gridConfiguration.PageSize = 25;

					dtResult = dsGridConfig.Tables[0];
					dtGrid = dsGridConfig.Tables[3];

					gridConfiguration.EditItemIndex = -1;

					gridConfiguration.DataSource = dtGrid;
					gridConfiguration.DataBind();
				}
				else
					this.lblError.Text = dsGridConfig.Tables[0].Rows[0][2].ToString();
			}
			catch(Exception ex)
			{
				this.lblError.Text = ex.Message + "|" + ex.InnerException;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings, Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userLoggin = utility.GetUserID();

			if(!IsPostBack)
				DisplayddlCodeID();
		}

		private void gridConfiguration_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				if((e.Item.ItemType == ListItemType.Header))
				{
					#region Header
					int iRow = 2;
					foreach(DataRow row in dsGridConfig.Tables[2].Rows)
					{
						if(!row["Title"].ToString().Equals(""))
						{
							countHeader += 1;
							e.Item.Cells[iRow].Text = row["Title"].ToString();
						}
						else
							e.Item.Cells[iRow].Visible = false;

						if(row["Title"].ToString().IndexOf("Description") > -1)
							e.Item.Cells[iRow].Width = new Unit("30%");
						else
							e.Item.Cells[iRow].Width = new Unit("10%");

						if(!dsGridConfig.Tables[1].Rows[0][1].ToString().Equals("0") || 
							!dsGridConfig.Tables[1].Rows[1][1].ToString().Equals("0") || 
							!dsGridConfig.Tables[1].Rows[2][1].ToString().Equals("0"))
						{
							e.Item.Cells[0].Visible = true;
							e.Item.Cells[0].Width = new Unit("10%");
						}
						else
							e.Item.Cells[0].Visible = false;

						if(row["Title"].ToString().IndexOf("Description") > -1)
							e.Item.Cells[iRow].HorizontalAlign =  HorizontalAlign.Left;

						iRow++;
					}
					#endregion
				}
				else if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
				{
					#region Item
					int iRow = 2;
					DataRowView drItem = (DataRowView)e.Item.DataItem;

					foreach(DataRow row in dsGridConfig.Tables[2].Rows)
					{
						if(row["Title"].ToString().Equals(""))
							e.Item.Cells[iRow].Visible = false;
						
						if(row["Title"].ToString().IndexOf("Description") > -1)
							e.Item.Cells[iRow].HorizontalAlign =  HorizontalAlign.Left;
						iRow++;
					}

					if(!dsGridConfig.Tables[1].Rows[0][1].ToString().Equals("0") || 
						!dsGridConfig.Tables[1].Rows[1][1].ToString().Equals("0") || 
						!dsGridConfig.Tables[1].Rows[2][1].ToString().Equals("0"))
					{
						e.Item.Cells[0].Visible = true;
						e.Item.Cells[0].Width = new Unit("10%");
					}
					else
						e.Item.Cells[0].Visible = false;

					//Can Delete
					if(dsGridConfig.Tables[1].Rows[1][1].ToString().Equals("0"))
					{
						LinkButton btnDeleteItem = (LinkButton)e.Item.FindControl("btnDeleteItem");
						btnDeleteItem.Visible = false;
					}
					//Can Edit
					if(dsGridConfig.Tables[1].Rows[2][1].ToString().Equals("0"))
					{
						LinkButton btnEditItem = (LinkButton)e.Item.FindControl("btnEditItem");
						btnEditItem.Visible = false;
					}
					#endregion
				}
				else if((e.Item.ItemType == ListItemType.Footer))
				{
					#region Footer
					int iRow = 2;
					foreach(DataRow row in dsGridConfig.Tables[2].Rows)
					{
						//Check visible
						if(row["Title"].ToString().Equals(""))
							e.Item.Cells[iRow].Visible = false;
						else
							e.Item.Cells[iRow].Visible = true;

						iRow++;
					}

					if(!dsGridConfig.Tables[1].Rows[0][1].ToString().Equals("0") || 
						!dsGridConfig.Tables[1].Rows[1][1].ToString().Equals("0") || 
						!dsGridConfig.Tables[1].Rows[2][1].ToString().Equals("0"))
					{
						e.Item.Cells[0].Visible = true;
						e.Item.Cells[0].Width = new Unit("10%");
					}
					else
						e.Item.Cells[0].Visible = false;

					//Can Add
					if(dsGridConfig.Tables[1].Rows[0][1].ToString().Equals("0"))
					{
						LinkButton btnAddItem = (LinkButton)e.Item.FindControl("btnAddItem");
						btnAddItem.Visible = false;

						for(int i = 0;i < e.Item.Cells.Count - 1;i++)
							e.Item.Cells[i].Visible = false;
					}
					#endregion
				}
				else if((e.Item.ItemType == ListItemType.EditItem))
				{
					#region EditItem
					
					msTextBox txtEditC1 = (msTextBox)e.Item.FindControl("txtEditC1");
					SetInitialFocus(txtEditC1);
					int iRow = 2;
					foreach(DataRow row in dsGridConfig.Tables[2].Rows)
					{
						//Check visible
						if(row["Title"].ToString().Equals(""))
							e.Item.Cells[iRow].Visible = false;
						else
							e.Item.Cells[iRow].Visible = true;
						iRow++;
					}
					#endregion
				}
			}
			catch(Exception ex)
			{
				this.lblError.Text = ex.Message + "|" + ex.InnerException;
			}
		}

		private void gridConfiguration_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			string command = e.CommandName;

			lblError.Text = string.Empty;

			if(command.Equals("ADD_ITEM"))
			{
				#region ADD_ITEM
				
				try
				{
					msTextBox txtAddC1 = (msTextBox)e.Item.FindControl("txtAddC1");
					if(txtAddC1.Text.Trim().Equals("") || txtAddC1.Text.Trim().Equals("0"))
					{
						this.lblError.Text = "Column: " + dsGridConfig.Tables[2].Rows[0][1].ToString() + " is required";
						return;
					}
					TextBox txtAddC2 = (TextBox)e.Item.FindControl("txtAddC2");
					if(txtAddC2.Text.Trim().Equals(""))
					{
						this.lblError.Text = "Column: " + dsGridConfig.Tables[2].Rows[1][1].ToString() + " is required";
						return;
					}
					TextBox txtAddC3 = (TextBox)e.Item.FindControl("txtAddC3");
					if(txtAddC3.Text.Trim().Equals(""))
					{
						this.lblError.Text = "Column: " + dsGridConfig.Tables[2].Rows[2][1].ToString() + " is required";
						return;
					}
					TextBox txtAddC4 = (TextBox)e.Item.FindControl("txtAddC4");
					TextBox txtAddC5 = (TextBox)e.Item.FindControl("txtAddC5");
					TextBox txtAddC6 = (TextBox)e.Item.FindControl("txtAddC6");
					TextBox txtAddC7 = (TextBox)e.Item.FindControl("txtAddC7");
					TextBox txtAddC8 = (TextBox)e.Item.FindControl("txtAddC8");
					TextBox txtAddC9 = (TextBox)e.Item.FindControl("txtAddC9");
					TextBox txtAddC10 = (TextBox)e.Item.FindControl("txtAddC10");

					TIESClasses.CustomsConfigurations objInfo = new TIESClasses.CustomsConfigurations();
					objInfo.Action = "2";
					objInfo.EnterpriseId = this.enterpriseID;
					objInfo.CodeID = dllCodeID.SelectedValue;
					objInfo.PK = dsGridConfig.Tables[0].Rows[0][3].ToString();
					objInfo.C1 = txtAddC1.Text.Trim();
					objInfo.C2 = txtAddC2.Text.Trim();
					objInfo.C3 = txtAddC3.Text.Trim();
					objInfo.C4 = txtAddC4.Text.Trim();
					objInfo.C5 = txtAddC5.Text.Trim();
					objInfo.C6 = txtAddC6.Text.Trim();
					objInfo.C7 = txtAddC7.Text.Trim();
					objInfo.C8 = txtAddC8.Text.Trim();
					objInfo.C9 = txtAddC9.Text.Trim();
					objInfo.C10 = txtAddC10.Text.Trim();

					DataSet dsResult = new DataSet();
					CustomsConfigurationsDAL db = new CustomsConfigurationsDAL(appID, enterpriseID);
					dsResult = db.ExecCustomsEditConfigurations(objInfo);

					dtResult = dsResult.Tables[0];
					dtGrid = dsResult.Tables[1];

					

					lblError.Text = dtResult.Rows[0][2].ToString();

					if(dtResult.Rows[0][0].ToString().Equals("0"))
					{
						dsGridConfig.Tables[0].Rows[0][3] = dtResult.Rows[0][3];
						dsGridConfig.Tables[0].AcceptChanges();

						gridConfiguration.CurrentPageIndex = 0;
						gridConfiguration.DataSource = dtGrid;
						gridConfiguration.DataBind();
					}
				}
				catch(Exception ex)
				{
					this.lblError.Text = ex.Message + "|" + ex.InnerException;
				}

				#endregion
			}
			else if(command.Equals("EDIT_ITEM"))
			{
				#region EDIT_ITEM
				gridConfiguration.EditItemIndex = e.Item.ItemIndex; 

				gridConfiguration.DataSource = dtGrid;
				gridConfiguration.DataBind();
				#endregion
			}
			else if(command.Equals("SAVE_ITEM"))
			{
				#region SAVE_ITEM
				
				try
				{
					Label lbPK = (Label)e.Item.FindControl("lbPK");
					msTextBox txtEditC1 = (msTextBox)e.Item.FindControl("txtEditC1");
					if(txtEditC1.Text.Trim().Equals("") || txtEditC1.Text.Trim().Equals("0"))
					{
						this.lblError.Text = "Column: " + dsGridConfig.Tables[2].Rows[0][1].ToString() + " is required";
						return;
					}
					TextBox txtEditC2 = (TextBox)e.Item.FindControl("txtEditC2");
					if(txtEditC2.Text.Trim().Equals(""))
					{
						this.lblError.Text = "Column: " + dsGridConfig.Tables[2].Rows[1][1].ToString() + " is required";
						return;
					}
					TextBox txtEditC3 = (TextBox)e.Item.FindControl("txtEditC3");
					if(txtEditC3.Text.Trim().Equals(""))
					{
						this.lblError.Text = "Column: " + dsGridConfig.Tables[2].Rows[2][1].ToString() + " is required";
						return;
					}
					TextBox txtEditC4 = (TextBox)e.Item.FindControl("txtEditC4");
					TextBox txtEditC5 = (TextBox)e.Item.FindControl("txtEditC5");
					TextBox txtEditC6 = (TextBox)e.Item.FindControl("txtEditC6");
					TextBox txtEditC7 = (TextBox)e.Item.FindControl("txtEditC7");
					TextBox txtEditC8 = (TextBox)e.Item.FindControl("txtEditC8");
					TextBox txtEditC9 = (TextBox)e.Item.FindControl("txtEditC9");
					TextBox txtEditC10 = (TextBox)e.Item.FindControl("txtEditC10");

					TIESClasses.CustomsConfigurations objInfo = new TIESClasses.CustomsConfigurations();
					objInfo.Action = "2";
					objInfo.EnterpriseId = this.enterpriseID;
					objInfo.CodeID = dllCodeID.SelectedValue;
					objInfo.PK = lbPK.Text.Trim();
					objInfo.C1 = txtEditC1.Text.Trim();
					objInfo.C2 = txtEditC2.Text.Trim();
					objInfo.C3 = txtEditC3.Text.Trim();
					objInfo.C4 = txtEditC4.Text.Trim();
					objInfo.C5 = txtEditC5.Text.Trim();
					objInfo.C6 = txtEditC6.Text.Trim();
					objInfo.C7 = txtEditC7.Text.Trim();
					objInfo.C8 = txtEditC8.Text.Trim();
					objInfo.C9 = txtEditC9.Text.Trim();
					objInfo.C10 = txtEditC10.Text.Trim();

					DataSet dsResult = new DataSet();
					CustomsConfigurationsDAL db = new CustomsConfigurationsDAL(appID, enterpriseID);
					dsResult = db.ExecCustomsEditConfigurations(objInfo);

					dtResult = dsResult.Tables[0];

					lblError.Text = dtResult.Rows[0][2].ToString();

					if(dtResult.Rows[0][0].ToString().Equals("0"))
					{
						dtGrid = dsResult.Tables[1];

						gridConfiguration.EditItemIndex = -1;
						gridConfiguration.CurrentPageIndex = 0;
						gridConfiguration.DataSource = dtGrid;
						gridConfiguration.DataBind();
					}
				}
				catch(Exception ex)
				{
					this.lblError.Text = ex.Message + "|" + ex.InnerException;
				}

				#endregion
			}
			else if(command.Equals("CANCEL_ITEM"))
			{
				#region CANCEL_ITEM
				gridConfiguration.EditItemIndex = -1;

				gridConfiguration.DataSource = dtGrid;
				gridConfiguration.DataBind();
				#endregion
			}
			else if(command.Equals("DELETE_ITEM"))
			{
				#region DELETE_ITEM

				try
				{
					Label lbPK = (Label)e.Item.FindControl("lbPK");

					TIESClasses.CustomsConfigurations objInfo = new TIESClasses.CustomsConfigurations();
					objInfo.Action = "3";
					objInfo.EnterpriseId = this.enterpriseID;
					objInfo.CodeID = dllCodeID.SelectedValue;
					objInfo.PK = lbPK.Text.Trim();

					DataSet dsResult = new DataSet();
					CustomsConfigurationsDAL db = new CustomsConfigurationsDAL(appID, enterpriseID);
					dsResult = db.ExecCustomsEditConfigurations(objInfo);

					dtResult = dsResult.Tables[0];
					dtGrid = dsResult.Tables[1];

					lblError.Text = dtResult.Rows[0][2].ToString();

					if(dtResult.Rows[0][0].ToString().Equals("0"))
					{
						gridConfiguration.EditItemIndex = -1;
						gridConfiguration.CurrentPageIndex = 0;
						gridConfiguration.DataSource = dtGrid;
						gridConfiguration.DataBind();
					}
				}
				catch(Exception ex)
				{
					this.lblError.Text = ex.Message + "|" + ex.InnerException;
				}

				#endregion
			}
		}

		private void gridConfiguration_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			gridConfiguration.CurrentPageIndex = e.NewPageIndex;
			gridConfiguration.DataSource = dtGrid;
			gridConfiguration.DataBind();
		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			try
			{
				countHeader = 0;
				DisplayGridView();
			}
			catch(Exception ex)
			{
				this.lblError.Text = ex.Message + "|" + ex.InnerException;
			}
		}
		private void btnQry_Click(object sender, System.EventArgs e)
		{
			lblError.Text = string.Empty;
			dllCodeID.SelectedIndex = 0;
	
			gridConfiguration.EditItemIndex = -1;
			gridConfiguration.DataSource = null;
			gridConfiguration.DataBind();
		}
	}
}
