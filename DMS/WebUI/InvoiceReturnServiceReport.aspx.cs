using System;
//using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for InvoiceReturnServiceReport.
	/// </summary>
	public class InvoiceReturnServiceReport : com.common.applicationpages.BasePage   
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnGenerate;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.RadioButton rbShipPendPOD;
		protected System.Web.UI.WebControls.RadioButton rbShipPendPODClosure;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected com.common.util.msTextBox txtPeriod;
		protected System.Web.UI.WebControls.Label lblTo;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.RadioButton rbPickupRouteCode;
		protected System.Web.UI.WebControls.RadioButton rbDeliveryRouteCode;
		protected System.Web.UI.WebControls.Label lblDelliveryType;
		protected System.Web.UI.WebControls.DropDownList ddDeliveryType;
		protected System.Web.UI.WebControls.Label lblDeliveryPathCode;
		protected Cambro.Web.DbCombo.DbCombo DbCmbPathCodeSelect;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected Cambro.Web.DbCombo.DbCombo dbCmbRouteCode;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.HtmlControls.HtmlTable table1;
		protected System.Web.UI.HtmlControls.HtmlTable table2;
		private DataView m_dvMonths;
		private String m_strAppID;
		protected System.Web.UI.WebControls.RadioButton rbInvoiceReturnPeding;
		protected System.Web.UI.WebControls.RadioButton rbInvoiceReturnException;
		private String m_strEnterpriseID;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			m_strAppID = System.Configuration.ConfigurationSettings.AppSettings["applicationID"];//utility.GetAppID();
			m_strEnterpriseID = System.Configuration.ConfigurationSettings.AppSettings["enterpriseID"];//utility.GetEnterpriseID();


    		if(!Page.IsPostBack)
			{
				DefaultScreen();
			}
//			SetComboRegistryKey();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
			this.rbInvoiceReturnPeding.CheckedChanged += new System.EventHandler(this.rbInvoiceReturnPeding_CheckedChanged);
			this.rbInvoiceReturnException.CheckedChanged += new System.EventHandler(this.rbInvoiceReturnException_CheckedChanged);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion	


		#region "Private Method"

		
		private void DefaultScreen()
		{
			rbInvoiceReturnPeding.Checked = true;
			rbInvoiceReturnException.Checked = false;
			rbMonth.Checked = true;
			rbPeriod.Checked = false;
			rbDate.Checked = false;
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
			lblErrorMessage.Text=""; 
			ddMonth.SelectedIndex =0;
//			ddDeliveryType.SelectedIndex =0;
			ddMonth.SelectedIndex=0; 
			LoadMonths();
//			LoadDeliveryType();
			ClearCombo();
			lblErrorMessage.Text = "";
		}

		private void LoadMonths()
		{
			ddMonth.Items.Clear();
			ListItem defItem = new ListItem();
			defItem.Text = "";
			defItem.Value = "0";
			ddMonth.Items.Add(defItem);
			ArrayList systemCodes = Utility.GetCodeValues(m_strAppID  ,"en-US","months",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				ddMonth.Items.Add(lstItem);
			}
				
		}

		private void ClearCombo()
		{		
//			dbCmbRouteCode.Text ="";dbCmbRouteCode.Value="";
//			DbCmbPathCodeSelect.Text=""; DbCmbPathCodeSelect.Value=""; 
		}


		
		private void setParamDataset()
		{
//			String strShpPending = null;
			String strDateType = null;
			String strStartDate = null;
//			String strSelectRouteCode = null;
//			String StrSelectSenRecp = null;
			DateTime dtStartDate = DateTime.Now;
			DateTime dtEndDate = DateTime.Now;
//			
//			//Report Type
//			if(rbShipPendPOD.Checked == true )
//				strShpPending = "N";
//			else if(rbShipPendPODClosure.Checked == true)
//				strShpPending = "Y";
//			
//
//			//Retrieval Basis
//			if(rbBookingDate.Checked == true)
//				strDateType = "B";
//			else if(rbDepartureDate.Checked == true)
//				strDateType = "D";
//			else if (rbEstDeliveryDate.Checked == true)
//				strDateType = "E";
//			
			if((ddMonth.SelectedIndex == 0) && (txtYear.Text == "")&&(txtPeriod.Text == "")&&(txtTo.Text == "")&&(txtDate.Text == ""))
			{
				lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVL",utility.GetUserCulture());
				return;
			}
			
			if (rbMonth.Checked == true)
			{					
				if(ddMonth.SelectedItem.Value != null && Convert.ToInt32(ddMonth.SelectedItem.Value) < 10)
					strStartDate="01"+"/0"+ddMonth.SelectedItem.Value+"/"+txtYear.Text.Trim();
				else
					strStartDate="01"+"/"+ddMonth.SelectedItem.Value+"/"+txtYear.Text.Trim();
				dtStartDate=System.DateTime.ParseExact(strStartDate,"dd/MM/yyyy",null);				
				dtEndDate=dtStartDate.AddMonths(1).AddDays(-1);				
			}
			if (rbPeriod.Checked == true)
			{	
				dtStartDate=System.DateTime.ParseExact(txtPeriod.Text.Trim(),"dd/MM/yyyy",null);									
				dtEndDate=System.DateTime.ParseExact(txtTo.Text.Trim(),"dd/MM/yyyy",null);
			}

			if (rbDate.Checked == true)
			{				
				dtStartDate=System.DateTime.ParseExact(txtDate.Text.Trim(),"dd/MM/yyyy",null);
				dtEndDate=dtStartDate.AddDays(1).AddMinutes(-1);				
			}
//			//Transfer Type
//			if(rbPickupRouteCode.Checked == true )
//				strSelectRouteCode = "P";
//			else if(rbDeliveryRouteCode.Checked == true)
//				strSelectRouteCode = "D";
//			else
//				strSelectRouteCode = "*"; 
//			
//			String strDlvType = ddDeliveryType.SelectedItem.Value;
//			String strPathCode = DbCmbPathCodeSelect.Value;
//			String strRouteCode = dbCmbRouteCode.Value;
//  
//			//Status Details
//
//			//	String strStatusCode = dbCmbStatusCode.Value;
//			//	String strExeceptionCode = dbCmbExceptionCode.Value;
// 
//			//Sender-Recipient Details
//			if (rbSender.Checked == true )
//				StrSelectSenRecp = "S";
//			else if(rbRecipent.Checked == true )
//				StrSelectSenRecp = "R";
//			else
//				StrSelectSenRecp ="*";
//
//			String strSenderName = dbCmbSenderName.Value ; 
//			String strRecpName = dbCmbRecpName.Value ; 
//
			//Creating  DataSet with columns..
			DataSet m_dsQuery =  new DataSet();
			DataTable dtParam = new DataTable();
//
			dtParam.Columns.Add("ApplicationId",typeof(string));
			dtParam.Columns.Add("EnterpriseId",typeof(string));
//			dtParam.Columns.Add("ShipType",typeof(string));
//			dtParam.Columns.Add("DateType",typeof(string));
			dtParam.Columns.Add("From_Date",typeof(DateTime));
			dtParam.Columns.Add("To_Date",typeof(DateTime));
//			dtParam.Columns.Add("TranfType",typeof(string));
//			dtParam.Columns.Add("DlvType",typeof(string));
//			dtParam.Columns.Add("PathCOde",typeof(string));
//			dtParam.Columns.Add("RouteCode",typeof(string));
//			dtParam.Columns.Add("StatusCode",typeof(string));
//			dtParam.Columns.Add("ExceptionCode",typeof(string));
//			dtParam.Columns.Add("SendRecpType",typeof(string));
//			dtParam.Columns.Add("SenderName",typeof(string));
//			dtParam.Columns.Add("RecpName",typeof(string));
//
			//Populating the DataSet with values..

			DataRow drEach = dtParam.NewRow();
//
			drEach["ApplicationId"] = utility.GetAppID();
			drEach["EnterpriseId"] = utility.GetEnterpriseID();
//			drEach["ShipType"] = strShpPending;
//			drEach["DateType"] = strDateType;
			drEach["From_Date"] = dtStartDate;
			drEach["To_Date"] = dtEndDate;
//			drEach["TranfType"] = strSelectRouteCode;
//			drEach["DlvType"] = strDlvType;
//			drEach["PathCOde"] = strPathCode;
//			drEach["RouteCode"] = strRouteCode;
//			//drEach["StatusCode"] = strStatusCode;
//			//drEach["ExceptionCode"] = strExeceptionCode;
//			drEach["SendRecpType"] = StrSelectSenRecp;
//			drEach["SenderName"] = strSenderName;
//			drEach["RecpName"] = strRecpName;
			m_dsQuery.Tables.Add(dtParam) ; 
			m_dsQuery.Tables[0].Rows.Add(drEach);
			Session["SESSION_DS1"] = m_dsQuery;
			 
		}

		
		private bool ValidateValues()
		{
			bool iCheck=false;
			if((ddMonth.SelectedIndex==0)&&(txtYear.Text=="")&&(txtPeriod.Text=="")&&(txtTo.Text=="")&&(txtDate.Text==""))
			{				
				lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVL",utility.GetUserCulture());
				return iCheck=true;
			
			}
			if(rbMonth.Checked == true)
			{
				if((ddMonth.SelectedIndex>0) &&(txtYear.Text==""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((ddMonth.SelectedIndex==0) &&(txtYear.Text!=""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbPeriod.Checked == true )
			{
				if((txtPeriod.Text!="")&&(txtTo.Text==""))
				{					
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_END_DT",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((txtPeriod.Text=="")&&(txtTo.Text!=""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_START_DT",utility.GetUserCulture());					
					return iCheck=true;			
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbDate.Checked == true )
			{
				if(txtDate.Text=="")
				{					
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT",utility.GetUserCulture());	
					return iCheck=true;				
				}
			}			

			return iCheck;
			
		}

		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
			

		#endregion

		#region "Controls Onclick"
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			
			DefaultScreen();

		}

		private void btnGenerate_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			bool icheck =  ValidateValues();
			if((rbInvoiceReturnPeding.Checked) && (!rbInvoiceReturnException.Checked) )
			{
				Session["FORMID"]="INVOICE RETURN PENDING"; 
			}
			else
			{
				Session["FORMID"]="INVOICE RETURN EXCEPTION"; 
			}
											  
			
			if(icheck==false)
			{
				setParamDataset();
				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				OpenWindowpage(strUrl);
			}
		}



		#endregion

		#region "Controls Onchange"

		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}

		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}

		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = true;
		}


		#endregion

		private void rbInvoiceReturnPeding_CheckedChanged(object sender, System.EventArgs e)
		{
				rbInvoiceReturnException.Checked= false;
		}

		private void rbInvoiceReturnException_CheckedChanged(object sender, System.EventArgs e)
		{
		
			rbInvoiceReturnPeding.Checked= false;
		}





	}
}
