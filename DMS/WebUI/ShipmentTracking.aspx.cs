using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for ShipmentTracking.
	/// </summary>
	public class ShipmentTracking : BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		private SessionDS m_sdsShipTrack = null;
		private SessionDS m_sdsShipDel = null;
		private String m_strAppID;
		protected System.Web.UI.WebControls.DataGrid dgShipTrack;
		private String m_strEnterpriseID;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			if(!Page.IsPostBack)
			{
				if((bool)Session["toRefresh"])
				{
					Session["toRefresh"]=false;
					//RetrieveSelectedPage();
//					m_sdsShipTrack = (SessionDS)Session["SESSION_DS1"];					
//					BindGrid();


					dgShipTrack.CurrentPageIndex = (int)Session["CURRENT_PAGE"];
					int iStartIndex = dgShipTrack.CurrentPageIndex * dgShipTrack.PageSize;
					//String strType = Request.Params["FORMID"];

					//DataSet dsShipTrack = null; 
					DataSet dsShipTrack = (DataSet)Session["QUERY_DS"];
					m_sdsShipTrack = ShipmentTrackingMgrDAL.QueryShipmentTracking_New(dsShipTrack, m_strAppID, m_strEnterpriseID, iStartIndex, dgShipTrack.PageSize);

					decimal iPageCnt = (m_sdsShipTrack.QueryResultMaxSize - 1)/dgShipTrack.PageSize;
					if(iPageCnt < dgShipTrack.CurrentPageIndex)
					{
						dgShipTrack.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
					}
					BindGrid();

					Session["SESSION_DS1"] = m_sdsShipTrack;
				}
				else
				{
					Session["toRefresh"]=false;
					Session["CURRENT_PAGE"]=0;
					m_sdsShipTrack = (SessionDS)Session["SESSION_DS1"];
					//RetrieveSelectedPage();
					BindGrid();
				}
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void BindGrid()
		{
			dgShipTrack.VirtualItemCount = System.Convert.ToInt32(m_sdsShipTrack.QueryResultMaxSize);
			dgShipTrack.DataSource =  m_sdsShipTrack.ds;
			dgShipTrack.DataBind();
		}

		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openLargerChildParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		public void dgShipTrack_OnItemCommand(object sender, System.Web.UI.WebControls.DataGridCommandEventArgs e) 
		{
			int rowNum = e.Item.ItemIndex;
			if(rowNum>=0)
			{
				//LinkButton lnkbtnConsignNo = (LinkButton)dgShipTrack.Items[rowNum].FindControl("lnkbtnConsignNo");
				Label lnkbtnBookNo = (Label)e.Item.FindControl("lnkbtnBookNo");
				LinkButton lnkbtnConsignNo = (LinkButton)e.Item.FindControl("lnkbtnConsignNo");
				//String strConsgNo = lnkbtnConsignNo.Text;
				String strBookNo = lnkbtnBookNo.Text;
				String strConsign = lnkbtnConsignNo.Text.Trim();
				Session["FORMID"] = strBookNo;
				Session["INFO_INDEX"] = e.Item.ItemIndex;

				try
				{
					if(e.CommandName != "Delete")
					{	
						//Session["SESSION_DS2"] = ShipmentTrackingMgrDAL.ShipmentForDeletion(m_strAppID, m_strEnterpriseID, strBookNo, strConsgNo, 0, 10 );
						//Label lblBookDateTime = (Label)e.Item.FindControl("lblBookDateTime");
						lblErrorMsg.Text = "";
						OpenWindowpage("DomesticShipmentDisplay.aspx?FORMID=ShipmentTracking.aspx&BOOK_NO="+strBookNo+"&CONSIGN="+strConsign);
					}
				}
				catch(Exception err)
				{
					lblErrorMsg.Text = err.ToString();
					lblErrorMsg.Text = "No status codes found in Shipment Tracking Table, check if Shipment Update has inserted a new status code";
				}
			
			}
		}

		public void dgShipTrack_OnDelete(object sender, System.Web.UI.WebControls.DataGridCommandEventArgs e) 
		{
			lblErrorMsg.Text = "";
			int rowNum = e.Item.ItemIndex;
			if (rowNum>=0)
			{
				Label lnkbtnBookNo = (Label)e.Item.FindControl("lnkbtnBookNo");
				LinkButton lnkbtnConsignNo = (LinkButton)dgShipTrack.Items[rowNum].FindControl("lnkbtnConsignNo");
				Label lblBookDate = (Label)e.Item.FindControl("lblBookDate");
				//Label lblLastDate = (Label)e.Item.FindControl("lblStatusDateTime");
				String strConsgNo = lnkbtnConsignNo.Text;
				String strBookNo = lnkbtnBookNo.Text;
				String strBookDate = lblBookDate.Text;
				//String strLastDate = lblLastDate.Text;
				OpenWindowpage("ShipmentStatusDelete.aspx?FORMID="+strBookNo+"&CONSG_NO="+strConsgNo+"&BOOK_DATE="+strBookDate+"&INFO_INDEX="+e.Item.ItemIndex+" ");
				/*
				m_sdsShipDel = ShipmentTrackingMgrDAL.ShipmentForDeletion(m_strAppID, m_strEnterpriseID, strBookNo, strConsgNo, 0, 5 );
				if (m_sdsShipDel.QueryResultMaxSize < 1)
				{
					lblErrorMsg.Text = "No status codes found in Shipment Tracking Table, check if Shipment Update has inserted a new status code";
				}
				else
				{
					Session["SESSION_DS2"] = m_sdsShipDel;
					OpenWindowpage("ShipmentStatusDelete.aspx?FORMID="+strBookNo+"&CONSG_NO="+strConsgNo+"&BOOK_DATE="+strBookDate+"&INFO_INDEX="+e.Item.ItemIndex+" ");
				}
				*/
			}
		}

		public void dgShipTrack_OnDataBound(object sender, DataGridItemEventArgs e) 
		{
		}

		public void dgShipTrack_OnPageChange(object sender, DataGridPageChangedEventArgs e) 
		{
			dgShipTrack.CurrentPageIndex = e.NewPageIndex;
			RetrieveSelectedPage();
			/*int iStartIndex = dgShipTrack.CurrentPageIndex * dgShipTrack.PageSize;
			//String strType = Request.Params["FORMID"];

			DataSet dsShipTrack = null; //(DataSet)Session["QUERY_DS"];
			m_sdsShipTrack = ShipmentTrackingMgrDAL.QueryShipmentTracking_New(null, dsShipTrack, m_strAppID, m_strEnterpriseID, iStartIndex, dgShipTrack.PageSize);

			int iPageCnt = (m_sdsShipTrack.QueryResultMaxSize - 1)/dgShipTrack.PageSize;
			if(iPageCnt < dgShipTrack.CurrentPageIndex)
			{
				dgShipTrack.CurrentPageIndex = iPageCnt;
			}
			BindGrid();

			Session["SESSION_DS1"] = m_sdsShipTrack;
			dgShipTrack.CurrentPageIndex = e.NewPageIndex;
			BindGrid();*/
		}

		public void RetrieveSelectedPage()
		{
			
			int iStartIndex = dgShipTrack.CurrentPageIndex * dgShipTrack.PageSize;
			//String strType = Request.Params["FORMID"];

			//DataSet dsShipTrack = null; 
			DataSet dsShipTrack = (DataSet)Session["QUERY_DS"];
			m_sdsShipTrack = ShipmentTrackingMgrDAL.QueryShipmentTracking_New(dsShipTrack, m_strAppID, m_strEnterpriseID, iStartIndex, dgShipTrack.PageSize);

			decimal iPageCnt = (m_sdsShipTrack.QueryResultMaxSize - 1)/dgShipTrack.PageSize;
			if(iPageCnt < dgShipTrack.CurrentPageIndex)
			{
				dgShipTrack.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			BindGrid();

			Session["SESSION_DS1"] = m_sdsShipTrack;
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.opener;" ;
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

	}
}
