<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<%@ Page language="c#" Codebehind="AccPacExportReconciliation.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.AccPacExportReconciliation" EnableEventValidation="False" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AccPac Export Reconciliation</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" type="text/javascript">
			function ExecuteQueryConNo()
			{
				var btn = document.getElementById('btnExecute');
				if(btn != null)
				{
					btn.disabled = true;
					__doPostBack('ExecuteQuery','');
				}
			}
		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="AccPacExport" method="post" runat="server">
			<asp:label style="Z-INDEX: 103; POSITION: absolute; TOP: 10px; LEFT: 20px" id="lblTitle" runat="server"
				CssClass="maintitleSize" Height="27px" Width="365px">AccPac Export Reconciliation</asp:label><INPUT style="Z-INDEX: 108; POSITION: absolute; DISPLAY: none; TOP: 8px; LEFT: 8px" id="ExecuteQuery"
				type="button" name="hddCon" runat="server" CausesValidation="False"><asp:button style="Z-INDEX: 100; POSITION: absolute; TOP: 54px; LEFT: 19px" id="btnQuery" runat="server"
				CssClass="queryButton" Height="20" Width="64px" Text="Query"></asp:button><asp:label style="Z-INDEX: 104; POSITION: absolute; TOP: 80px; LEFT: 20px" id="lblErrorMessage"
				runat="server" CssClass="errorMsgColor" Height="21px" Width="528px">Place holder for err msg</asp:label>&nbsp;
			<%--<asp:button style="Z-INDEX: 103; POSITION: absolute; TOP: 54px; LEFT: 84px" id="btnExecute"
				runat="server" CssClass="queryButton" Height="20" Width="120px" Text="Execute Query"></asp:button>--%>
			<INPUT style="Z-INDEX: 102; POSITION: absolute; WIDTH: 112px; HEIGHT: 20px; TOP: 54px; LEFT: 88px"
				id="btnExecute" class="queryButton" onclick="ExecuteQueryConNo();" value="Execute Query"
				type="button" name="ExecQury" runat="server" CausesValidation="False">
			<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 54px; LEFT: 205px" id="btnGenerateReport"
				runat="server" CssClass="queryButton" Height="20" Width="120px" Text="Generate Report" Enabled="False"></asp:button>
			<table style="Z-INDEX: 105; POSITION: absolute; WIDTH: 500px; TOP: 100px; LEFT: 16px" id="tblShipmentTracking"
				border="0" runat="server">
				<tr>
					<td>
						<fieldset><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="176px" Font-Bold="True">Export Retrieval Criteria</asp:label></legend>
							<table>
								<tr>
									<td><asp:label style="Z-INDEX: 0" id="lbPeriodFrom" runat="server" CssClass="tableLabel" Height="22px"
											Width="79px">Period From</asp:label></td>
									<TD><cc1:mstextbox id="txtDateStart" runat="server" CssClass="textField" Width="82" TextMaskString="99/99/9999"
											TextMaskType="msDate" MaxLength="10"></cc1:mstextbox></TD>
									<TD><asp:label style="Z-INDEX: 0" id="lbPeriodEnd" runat="server" CssClass="tableLabel" Height="22px"
											Width="24px">To</asp:label></TD>
									<TD><cc1:mstextbox style="Z-INDEX: 0" id="txtDateEnd" runat="server" CssClass="textField" Width="82"
											TextMaskString="99/99/9999" TextMaskType="msDate" MaxLength="10"></cc1:mstextbox></TD>
								</tr>
								<tr>
									<td><asp:label style="Z-INDEX: 0" id="lbExportedBy" runat="server" CssClass="tableLabel" Height="22px"
											Width="79px">Exported By</asp:label></td>
									<TD>
										<asp:TextBox style="Z-INDEX: 0" id="txtExportedBy" runat="server" Width="82px" CssClass="textField"
											MaxLength="10"></asp:TextBox></TD>
									<TD></TD>
									<TD></TD>
								</tr>
							</table>
						</fieldset>
					</td>
				</tr>
			</table>
			<table style="Z-INDEX: 107; POSITION: absolute; TOP: 208px; LEFT: 16px" width="750">
				<TR class="gridHeading">
					<TD colSpan="6"><STRONG><FONT size="2">Invoices Matching Selection Criteria</FONT></STRONG></TD>
				</TR>
			</table>
			<table style="Z-INDEX: 106; POSITION: absolute; TOP: 230px; LEFT: 16px" width="100%">
				<tr>
					<td><asp:datagrid style="Z-INDEX: 0" id="dgExportAccPac" runat="server" Width="750px" AllowPaging="True"
							HorizontalAlign="Left" AutoGenerateColumns="False" PageSize="20" SelectedItemStyle-CssClass="gridFieldSelected">
							<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
							<ItemStyle Height="15px" CssClass="gridField"></ItemStyle>
							<HeaderStyle Font-Bold="True" HorizontalAlign="Center" Height="40px" Width="2%" CssClass="gridHeading"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn>
									<ItemStyle HorizontalAlign="Center" Height="25px" Width="50px"></ItemStyle>
									<ItemTemplate>
										<asp:CheckBox style="Z-INDEX: 0" id="chkNo" Runat="server" AutoPostBack="True" OnCheckedChanged="getCheck_onChecked"></asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="100px"></HeaderStyle>
									<HeaderTemplate>
										<asp:Label ID="lblHeadNumber" Runat="server" Text="Exported Date/Time"></asp:Label>
									</HeaderTemplate>
									<ItemTemplate>
										<center>
											<asp:Label ID="lblConNo" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"exported_date","{0:dd/MM/yyyy HH:mm}")%>'>
											</asp:Label>
										</center>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="150px"></HeaderStyle>
									<HeaderTemplate>
										<asp:Label ID="lblHeadPOD" Runat="server" Text="Exported By"></asp:Label>
									</HeaderTemplate>
									<ItemTemplate>
										<center>
											<asp:Label ID="PODDate" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"exported_by")%>'>
											</asp:Label>
										</center>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle Width="50px"></HeaderStyle>
									<HeaderTemplate>
										<asp:Label ID="Label2" Runat="server" Text="No. of Cons"></asp:Label>
									</HeaderTemplate>
									<ItemTemplate>
										<center>
											<asp:Label ID="lblConsignee" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"NoOfCons")%>'>
											</asp:Label>
										</center>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle HorizontalAlign="Left" Width="100px"></HeaderStyle>
									<HeaderTemplate>
										<asp:Label ID="Label3" Runat="server" Text="Domestic Doc. Rev."></asp:Label>
									</HeaderTemplate>
									<ItemTemplate>
										<center>
											<asp:Label ID="lblRefNo" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"DomesticDocRev", "{0:#,##0.00}")%>'>
											</asp:Label>
										</center>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle HorizontalAlign="Left" Width="100px"></HeaderStyle>
									<HeaderTemplate>
										<asp:Label ID="Label4" Runat="server" Text="Domestic Freight Rev."></asp:Label>
									</HeaderTemplate>
									<ItemTemplate>
										<center>
											<asp:Label ID="lblDeliveryDC" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"DomesticFreightRev", "{0:#,##0.00}")%>'>
											</asp:Label>
										</center>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle HorizontalAlign="Left" Width="100px"></HeaderStyle>
									<HeaderTemplate>
										<asp:Label ID="Label5" Runat="server" Text="Export Doc. Rev."></asp:Label>
									</HeaderTemplate>
									<ItemTemplate>
										<center>
											<asp:Label ID="lblExportedDT" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ExportDocRev", "{0:#,##0.00}")%>'>
											</asp:Label>
										</center>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle HorizontalAlign="Left" Width="100px"></HeaderStyle>
									<HeaderTemplate>
										<asp:Label ID="Label6" Runat="server" Text="Export Freight Rev."></asp:Label>
									</HeaderTemplate>
									<ItemTemplate>
										<center>
											<asp:Label ID="lblMAWBNo" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ExportFreightRev", "{0:#,##0.00}")%>'>
											</asp:Label>
										</center>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
									<HeaderStyle HorizontalAlign="Left" Width="100px"></HeaderStyle>
									<HeaderTemplate>
										<asp:Label ID="Label1" Runat="server" Text="invoice_no"></asp:Label>
									</HeaderTemplate>
									<ItemTemplate>
										<center>
											<asp:Label ID="lbInvoiceNo" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"invoice_no")%>'>
											</asp:Label>
										</center>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
						</asp:datagrid></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
