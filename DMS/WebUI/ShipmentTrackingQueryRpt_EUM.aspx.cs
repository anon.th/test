using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;
using Cambro.Web.DbCombo;
using com.common.RBAC;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for ShipmentTrackingQueryRpt_EUM.
	/// </summary>
	public class ShipmentTrackingQueryRpt_EUM : BasePage
	{
		
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnCancelQry;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.RadioButton rbBookingDate;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbLongRoute;
		protected System.Web.UI.WebControls.RadioButton rbShortRoute;
		protected System.Web.UI.WebControls.RadioButton rbAirRoute;
		protected System.Web.UI.WebControls.Label lblConsignmentNo;
		protected System.Web.UI.WebControls.TextBox txtConsignmentNo;
		protected System.Web.UI.WebControls.Label lblStatusCode;
		protected com.common.util.msTextBox txtStatusCode;
		protected com.common.util.msTextBox txtExceptionCode;
		protected System.Web.UI.WebControls.Label lblShipmentClosed;
		protected System.Web.UI.WebControls.RadioButton rbShipmentYes;
		protected System.Web.UI.WebControls.RadioButton rbShipmentNo;
		protected System.Web.UI.WebControls.RadioButton rbInvoiceYes;
		protected System.Web.UI.WebControls.Label lblPayerCode;
		protected System.Web.UI.WebControls.TextBox txtPayerCode;
		protected System.Web.UI.WebControls.Button btnPayerCode;
		protected System.Web.UI.WebControls.Button btnStatusCode;
		protected System.Web.UI.WebControls.Button btnExceptionCode;
		protected System.Web.UI.WebControls.Label lblZipCode;
		protected com.common.util.msTextBox txtZipCode;
		protected System.Web.UI.WebControls.Button btnZipCode;
		protected System.Web.UI.WebControls.Label lblStateCode;
		protected com.common.util.msTextBox txtStateCode;
		protected System.Web.UI.WebControls.Button btnStateCode;
		protected System.Web.UI.WebControls.RadioButton rbInvoiceNo;
		protected System.Web.UI.WebControls.Label lblInvoiceIssued;
		protected System.Web.UI.WebControls.Label lblBookingType;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.TextBox txtPeriod2;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.HtmlControls.HtmlTable tblPayerType;
		protected System.Web.UI.HtmlControls.HtmlTable tblRouteType;
		protected System.Web.UI.HtmlControls.HtmlTable tblDestination;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipment;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.Label lblPayerType;
		protected System.Web.UI.WebControls.Label lblRouteType;
		protected System.Web.UI.WebControls.Label lblDestination;
		protected System.Web.UI.WebControls.Label lblShipment;
		protected com.common.util.msTextBox txtBookingNo;
		protected System.Web.UI.WebControls.Label lblExceptionCode;
		protected System.Web.UI.WebControls.Label lblBookingNo;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCode;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected Cambro.Web.DbCombo.DbCombo DbComboOriginDC;
		protected Cambro.Web.DbCombo.DbCombo DbComboDestinationDC;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.RadioButton rbPkDate;
		protected System.Web.UI.WebControls.DropDownList Drp_BookingType;
		protected System.Web.UI.WebControls.ListBox lsbCustType;
		protected System.Web.UI.HtmlControls.HtmlTable tblInternal;
		protected System.Web.UI.WebControls.Button btnStateCodeCust;
		protected com.common.util.msTextBox txtStateCodeCust;
		protected System.Web.UI.WebControls.Button btnZipCodeCust;
		protected com.common.util.msTextBox txtZipCodeCust;
		protected System.Web.UI.WebControls.Label Label8;
		protected com.common.util.msTextBox txtDateCust;
		protected System.Web.UI.WebControls.RadioButton rbDateCust;
		protected com.common.util.msTextBox txtToCust;
		protected com.common.util.msTextBox txtPeriodCust;
		protected System.Web.UI.WebControls.RadioButton rbPeriodCust;
		protected com.common.util.msTextBox txtYearCust;
		protected System.Web.UI.WebControls.DropDownList ddMonthCust;
		protected System.Web.UI.WebControls.RadioButton rbMonthCust;
		protected System.Web.UI.HtmlControls.HtmlTable tblExternal;
		protected System.Web.UI.HtmlControls.HtmlTable Table3;
		protected System.Web.UI.HtmlControls.HtmlTable Table6;
		protected System.Web.UI.WebControls.Label lblDateCust;
		protected System.Web.UI.WebControls.Label lblPostalCodeCust;
		protected System.Web.UI.WebControls.Label lblStateCust;
		protected System.Web.UI.WebControls.Label lblYearCust;
		protected System.Web.UI.WebControls.RadioButton rbBookingDateCust;
		protected System.Web.UI.WebControls.RadioButton rbPickupDateCust;
		protected System.Web.UI.WebControls.RadioButton rbStatusDateCust;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.RadioButton rbCheckAllYes;
		protected System.Web.UI.WebControls.RadioButton rbCheckAllNo;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.RadioButton rdoPkgYes;
		protected System.Web.UI.WebControls.RadioButton rdoPkgNo;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.DropDownList ddlTransporter;
		protected System.Web.UI.WebControls.RadioButton rbStatusDate;

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.rbMonthCust.CheckedChanged += new System.EventHandler(this.rbMonthCust_CheckedChanged);
			this.rbPeriodCust.CheckedChanged += new System.EventHandler(this.rbPeriodCust_CheckedChanged);
			this.rbDateCust.CheckedChanged += new System.EventHandler(this.rbDateCust_CheckedChanged);
			this.btnZipCodeCust.Click += new System.EventHandler(this.btnZipCodeCust_Click);
			this.btnStateCodeCust.Click += new System.EventHandler(this.btnStateCodeCust_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.btnPayerCode.Click += new System.EventHandler(this.btnPayerCode_Click);
			this.rbLongRoute.CheckedChanged += new System.EventHandler(this.rbLongRoute_CheckedChanged);
			this.rbShortRoute.CheckedChanged += new System.EventHandler(this.rbShortRoute_CheckedChanged);
			this.rbAirRoute.CheckedChanged += new System.EventHandler(this.rbAirRoute_CheckedChanged);
			this.btnZipCode.Click += new System.EventHandler(this.btnZipCode_Click);
			this.btnStateCode.Click += new System.EventHandler(this.btnStateCode_Click);
			this.btnStatusCode.Click += new System.EventHandler(this.btnStatusCode_Click);
			this.btnExceptionCode.Click += new System.EventHandler(this.btnExceptionCode_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		String m_strAppID=null;
		String m_strEnterpriseID=null;
		String m_strCulture=null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			DbComboOriginDC.ClientOnSelectFunction="makeUppercase('DbComboOriginDC:ulbTextBox');";
			DbComboDestinationDC.ClientOnSelectFunction="makeUppercase('DbComboDestinationDC:ulbTextBox');";

			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboOriginDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboDestinationDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			com.common.classes.User user = RBACManager.GetUser(m_strAppID, m_strEnterpriseID, (String)Session["userID"]);
			ViewState["usertype"] = user.UserType;
			ViewState["PayerID"] = user.PayerID;

			if ((String)ViewState["usertype"]  == "C")
			{
				tblInternal.Visible = false;
				tblExternal.Visible = true;
			}
			else
			{
				tblInternal.Visible = true;
				tblExternal.Visible = false;
			}

			if(!Page.IsPostBack)
			{
				DefaultScreen();
				ddlmonths();
				ddlmonthsCust();
				LoadCustomerTypeList();
				LoadBookingTypeList();
				Session["toRefresh"]=false;
			}

			SetDbComboServerStates();
		}


		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{

			//btnExecuteQuery.Enabled = false;
			lblErrorMessage.Text = "";

			/*
			int intChk = 0;
			intChk = ValidateData();
			if(intChk < 1)
			{
				return;
			}
			*/

			bool icheck =  ValidateValues();

			if(icheck == false)
			{
				String strModuleID = Request.Params["MODID"];
				DataSet dsShipment = GetShipmentQueryData();

				//				String strUrl = null;
				//				SessionDS sessionds = new SessionDS();
				//				sessionds = ShipmentTrackingMgrDAL.QueryShipmentTracking(dsShipment, m_strAppID, m_strEnterpriseID, 0, 10);
				//				strUrl = "ShipmentTracking.aspx?FORMID=TRUE_CHECK";
				//				//String strFeatures = "'height=500,width=900,left=30,top=30,location=no,menubar=no,scrollbars=yes,resizable=yes,status=yes,titlebar=no,toolbar=no'";
				//				Session["SESSION_DS1"] = sessionds;
				//				Session["QUERY_DS"] = dsShipment;
				//				ArrayList paramList = new ArrayList();
				//				paramList.Add(strUrl);
				//				String sScript = Utility.GetScript("openParentWindow.js",paramList);
				//				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

				

				String strUrl = null;
				//String sFeatures = "'height=700,width=900,left=20,top=5,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
				strUrl = "ReportViewer.aspx";
				//String strFeatures = "'height=700,width=900,left=20,top=5,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
				if(rbCheckAllNo.Checked)  //Jeab 13 Dec 10
					//Session["FORMID"] = "ShipmentTrackingQueryRpt";
					if(rdoPkgNo.Checked)
					{
						Session["FORMID"] = "ShipmentTrackingQueryRpt_NoDT";
					}
					else
					{
						Session["FORMID"] = "ShipmentTrackingQueryRpt";
					}
				else if (rbCheckAllYes.Checked)
				{
					if(txtStatusCode.Text.Trim() == "")
						//Session["FORMID"] = "ShipmentTrackingQueryRpt";
						if(rdoPkgNo.Checked)
						{
							Session["FORMID"] = "ShipmentTrackingQueryRpt_NoDT";
						}
						else
						{
							Session["FORMID"] = "ShipmentTrackingQueryRpt";
						}
					else
						//Session["FORMID"] = "ShipmentTrackingQueryRptAll";
						if(rdoPkgNo.Checked)
					{
						Session["FORMID"] = "ShipmentTrackingQueryRptAll_NoDT";
					}
					else
					{
						Session["FORMID"] = "ShipmentTrackingQueryRptAll";
					}   //Jeab 13 Dec 10  ========> End
				}
				Session["SESSION_DS1"] = dsShipment;
				//OpenWindowpage(strUrl, strFeatures);
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindow.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}

			/*---------------------------------------------------------------PAK 7 June 2013
			String strUrl = null;
			//String sFeatures = "'height=700,width=900,left=20,top=5,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
			strUrl = "ReportViewer.aspx";
			//String strFeatures = "'height=700,width=900,left=20,top=5,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
			if(rbCheckAllNo.Checked)  //Jeab 13 Dec 10
//				Session["FORMID"] = "ShipmentTrackingQueryRpt";
					if(rdoPkgNo.Checked)
					{
						Session["FORMID"] = "ShipmentTrackingQueryRpt_NoDT";
					}
					else
					{
						Session["FORMID"] = "ShipmentTrackingQueryRpt";
					}
			else if (rbCheckAllYes.Checked)
			{
				if(txtStatusCode.Text.Trim() == "")
//					Session["FORMID"] = "ShipmentTrackingQueryRpt";
						if(rdoPkgNo.Checked)
						{
							Session["FORMID"] = "ShipmentTrackingQueryRpt_NoDT";
						}
						else
						{
							Session["FORMID"] = "ShipmentTrackingQueryRpt";
						}
				else
//					Session["FORMID"] = "ShipmentTrackingQueryRptAll";
						if(rdoPkgNo.Checked)
						{
							Session["FORMID"] = "ShipmentTrackingQueryRptAll_NoDT";
						}
						else
						{
							Session["FORMID"] = "ShipmentTrackingQueryRptAll";
						}   //Jeab 13 Dec 10  ========> End
			}
			Session["SESSION_DS1"] = dsShipment;
			//OpenWindowpage(strUrl, strFeatures);
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			--------------------------------------------------------------------------------*/

			//check for module id before opening another web form
			//			if (strModuleID.Equals("03ShipmentTracking"))
			//			{
			//				String strUrl = null;
			//				SessionDS sessionds = new SessionDS();
			//				sessionds = ShipmentTrackingMgrDAL.QueryShipmentTracking(dsShipment, m_strAppID, m_strEnterpriseID, 0, 10);
			//				strUrl = "ShipmentTracking.aspx?FORMID=TRUE_CHECK";
			//				//String strFeatures = "'height=500,width=900,left=30,top=30,location=no,menubar=no,scrollbars=yes,resizable=yes,status=yes,titlebar=no,toolbar=no'";
			//				Session["SESSION_DS1"] = sessionds;
			//				Session["QUERY_DS"] = dsShipment;
			//				ArrayList paramList = new ArrayList();
			//				paramList.Add(strUrl);
			//				String sScript = Utility.GetScript("openParentWindow.js",paramList);
			//				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			//			}
			//			else
			//			{
			//				String strUrl = null;
			//				//String sFeatures = "'height=700,width=900,left=20,top=5,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
			//				strUrl = "ReportViewer.aspx";
			//				//String strFeatures = "'height=700,width=900,left=20,top=5,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
			//				Session["FORMID"] = "ShipmentTrackingQuery";
			//				Session["SESSION_DS1"] = dsShipment;
			//				//OpenWindowpage(strUrl, strFeatures);
			//				ArrayList paramList = new ArrayList();
			//				paramList.Add(strUrl);
			//				String sScript = Utility.GetScript("openParentWindow.js",paramList);
			//				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			//			}

		}


		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}


		#region "Page Functions"

		private DataTable CreateEmptyDataTable()
		{
			DataTable dtShipment = new DataTable();

			#region "Dates"
			dtShipment.Columns.Add(new DataColumn("tran_date", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
			dtShipment.Columns.Add(new DataColumn("end_date", typeof(DateTime)));
			#endregion
			
			#region "Payer Type"
			dtShipment.Columns.Add(new DataColumn("payer_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payer_code", typeof(string)));
			#endregion
		
			#region "Route / DC Selection"
			dtShipment.Columns.Add(new DataColumn("route_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("route_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("origin_dc", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("destination_dc", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("delPath_origin_dc", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("delPath_destination_dc", typeof(string)));
			#endregion

			#region "Destination"
			dtShipment.Columns.Add(new DataColumn("zip_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("state_code", typeof(string)));
			#endregion

			#region "Shipment"
			dtShipment.Columns.Add(new DataColumn("booking_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("consignment_no", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("booking_no", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("status_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("exception_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("shipment_closed", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("shipment_invoiced", typeof(string)));
			//Added By Tom 24/7/09
			dtShipment.Columns.Add(new DataColumn("track_all", typeof(string)));
			//End Added By Tom 24/7/09
			#endregion

			#region "Report To Show"
			dtShipment.Columns.Add(new DataColumn("RptType", typeof(string)));
			#endregion

			return dtShipment;
		}


		private DataSet GetShipmentQueryData()
		{
			DataSet dsShipment = new DataSet();
			DataTable dtShipment = CreateEmptyDataTable();
			DataRow dr = dtShipment.NewRow(); 
			
			#region "Dates"

			if ((String)ViewState["usertype"] != "C")
			{
				if (rbBookingDate.Checked) 
				{	// Booking Date Selected
					dr["tran_date"] = "B";
				}
				if (rbPkDate.Checked) 
				{	// Pickup Date Selected
					dr["tran_date"] = "P";
				}
				if (rbStatusDate.Checked) 
				{	// Status Date Selected
					dr["tran_date"] = "S";
				}
			
				string strMonth =null;

				if (rbMonth.Checked == true) 
				{	// Month Selected
					strMonth = ddMonth.SelectedItem.Value;
					if (strMonth != "" && txtYear.Text != "") 
					{
						if(strMonth.Length == 1)
							strMonth = "0" + strMonth;
						dr["start_date"] = DateTime.ParseExact ("01/"+strMonth.ToString()+"/"+txtYear.Text, "dd/MM/yyyy", null);
						int intLastDay = 0;
						intLastDay = LastDayOfMonth(strMonth, txtYear.Text);
						dr["end_date"] = DateTime.ParseExact (intLastDay.ToString()+"/"+strMonth.ToString()+"/"+txtYear.Text+" 23:59", "dd/MM/yyyy HH:mm", null);
					}
				}
				if (rbPeriod.Checked == true) 
				{	// Period Selected
					if (txtPeriod.Text != ""  && txtTo.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact(txtPeriod.Text ,"dd/MM/yyyy",null);
						dr["end_date"] = DateTime.ParseExact(txtTo.Text+" 23:59" ,"dd/MM/yyyy HH:mm",null);
					}
				}
				if (rbDate.Checked == true) 
				{	// Date Selected
					if (txtDate.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy",null);
						dr["end_date"] = DateTime.ParseExact(txtDate.Text+" 23:59", "dd/MM/yyyy HH:mm",null);
					}
				}
			}
			else
			{
				if (rbBookingDateCust.Checked) 
				{	// Booking Date Selected
					dr["tran_date"] = "B";
				}
				if (rbPickupDateCust.Checked) 
				{	// Pickup Date Selected
					dr["tran_date"] = "P";
				}
				if (rbStatusDateCust.Checked) 
				{	// Status Date Selected
					dr["tran_date"] = "S";
				}
			
				string strMonth =null;

				if (rbMonthCust.Checked == true) 
				{	// Month Selected
					strMonth = ddMonthCust.SelectedItem.Value;
					if (strMonth != "" && txtYearCust.Text != "") 
					{
						if(strMonth.Length == 1)
							strMonth = "0" + strMonth;

						dr["start_date"] = DateTime.ParseExact ("01/"+strMonth.ToString()+"/"+txtYearCust.Text, "dd/MM/yyyy", null);
						int intLastDay = 0;
						intLastDay = LastDayOfMonth(strMonth, txtYearCust.Text);
						dr["end_date"] = DateTime.ParseExact (intLastDay.ToString()+"/"+strMonth.ToString()+"/"+txtYearCust.Text+" 23:59", "dd/MM/yyyy HH:mm", null);
					}
				}
				if (rbPeriodCust.Checked == true) 
				{	// Period Selected
					if (txtPeriodCust.Text != ""  && txtToCust.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact(txtPeriodCust.Text ,"dd/MM/yyyy",null);
						dr["end_date"] = DateTime.ParseExact(txtToCust.Text+" 23:59" ,"dd/MM/yyyy HH:mm",null);
					}
				}
				if (rbDateCust.Checked == true) 
				{	// Date Selected
					if (txtDateCust.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact(txtDateCust.Text, "dd/MM/yyyy",null);
						dr["end_date"] = DateTime.ParseExact(txtDateCust.Text+" 23:59", "dd/MM/yyyy HH:mm",null);
					}
				}
			}
		
			#endregion

			#region "Payer Type"

			string strCustPayerType = "";

			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
			{
				if(lsbCustType.Items[i].Selected == true)
				{
					strCustPayerType += "\"" + lsbCustType.Items[i].Value + "\",";
				}
			}

			if (strCustPayerType != "") 
				dr["payer_type"] = "(" + strCustPayerType.Substring(0,strCustPayerType.Length - 1) + ")";
			else
				dr["payer_type"] = System.DBNull.Value;
			
			if ((String)ViewState["usertype"]  == "C")
				dr["payer_code"] = (String)ViewState["PayerID"];
			else
				dr["payer_code"] = txtPayerCode.Text.Trim();

			#endregion
			
			#region "Route / DC Selection"
			if (rbLongRoute.Checked) 
			{
				dr["route_type"] = "L";
			}
			if (rbShortRoute.Checked) 
			{
				dr["route_type"] = "S";
			}
			if (rbAirRoute.Checked) 
			{
				dr["route_type"] = "A";
			}

			dr["route_code"] = DbComboPathCode.Value;
			dr["origin_dc"] = DbComboOriginDC.Value.ToUpper();
			dr["destination_dc"] = DbComboDestinationDC.Value.ToUpper();
			
			if (rbLongRoute.Checked || rbAirRoute.Checked)
			{
				com.ties.classes.DeliveryPath delPath = new com.ties.classes.DeliveryPath();
				delPath.Populate(m_strAppID, m_strEnterpriseID, DbComboPathCode.Value);

				dr["delPath_origin_dc"] = delPath.OriginStation;
				dr["delPath_destination_dc"] = delPath.DestinationStation;
			}
			else
			{
				dr["delPath_origin_dc"] = "";
				dr["delPath_destination_dc"] = "";
			}
			#endregion

			#region "Destination"
			if((String)ViewState["usertype"] != "C")
			{
				dr["zip_code"] = txtZipCode.Text.Trim();
				dr["state_code"] = txtStateCode.Text.Trim();
			}
			else
			{
				dr["zip_code"] = txtZipCodeCust.Text.Trim();
				dr["state_code"] = txtStateCodeCust.Text.Trim();
			}
			#endregion

			#region "Shipment"
			dr["booking_type"] = Drp_BookingType.SelectedItem.Value;
			dr["consignment_no"] = txtConsignmentNo.Text;
			dr["booking_no"] = txtBookingNo.Text;
			dr["status_code"] = txtStatusCode.Text;
			dr["exception_code"] = txtExceptionCode.Text;

			if (rbShipmentYes.Checked)
				dr["shipment_closed"] = "Y";
			else
				dr["shipment_closed"] = "N";

			if (rbInvoiceYes.Checked)
				dr["shipment_invoiced"] = "Y";
			else
				dr["shipment_invoiced"] = "N";
			//Added By tom 24/7/09
			if (rbCheckAllYes.Checked)
			{
				if(txtStatusCode.Text == "")
					dr["track_all"] = "N";
				else
					dr["track_all"] = "Y";
			}
			else
				dr["track_all"] = "N";
			//End Added By Tom 24/7/09
			#endregion

			#region "Report To Show"

			if ((String)ViewState["usertype"] != "C")
			{
				dr["RptType"] = "N";
			}
			else
			{
				dr["RptType"] = "C";
			}

			#endregion

			dtShipment.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipment);
			return dsShipment;
		}


		private void DefaultScreen()
		{
			#region "Dates"

			rbBookingDate.Checked = true;
			rbPkDate.Checked = false;
			rbStatusDate.Checked = false;
			rbMonth.Checked = true;
			rbPeriod.Checked = false;
			rbDate.Checked = false;

			ddMonth.Enabled = true;
			ddMonth.SelectedIndex = -1;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;

			#endregion

			#region "Payer Type"

			lsbCustType.SelectedIndex = -1;
			txtPayerCode.Text = null;

			#endregion

			#region "Route / DC Selection"
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";

			DbComboOriginDC.Text="";
			DbComboOriginDC.Value="";

			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			if(util.CheckUserRoleLocation())
			{
				DbComboDestinationDC.Text="";
				DbComboDestinationDC.Value="";
				DbComboDestinationDC.Enabled=true;
			}
			else
			{
				string UserLocation = util.UserLocation();	
				DbComboDestinationDC.Text=UserLocation;
				DbComboDestinationDC.Value=UserLocation;
				DbComboDestinationDC.Enabled=false;
			}

			#endregion

			#region "Destination"

			txtZipCode.Text = null;
			txtStateCode.Text = null;

			#endregion

			#region "Shipment"

			txtConsignmentNo.Text = null;
			Drp_BookingType.SelectedIndex = -1;
			txtStatusCode.Text = null;
			txtExceptionCode.Text = null;
			rbInvoiceYes.Checked = false;
			rbInvoiceNo.Checked = true;
			//Added By Tom 23/7/09
			rbCheckAllYes.Checked = false;
			rbCheckAllNo.Checked = true;
			//End Added By Tom 23/7/09 
			
			rdoPkgYes.Checked = false;
			rdoPkgNo.Checked = true;
			#endregion

			//Cust LogOn
			#region "Date"

			rbBookingDateCust.Checked = true;
			rbPickupDateCust.Checked = false;
			rbStatusDateCust.Checked = false;
			rbMonthCust.Checked = true;
			rbPeriodCust.Checked = false;
			rbDateCust.Checked = false;

			ddMonthCust.Enabled = true;
			ddMonthCust.SelectedIndex = -1;
			txtYearCust.Text = null;
			txtYearCust.Enabled = true;
			txtPeriodCust.Text = null;
			txtPeriodCust.Enabled = false;
			txtToCust.Text = null;
			txtToCust.Enabled = false;
			txtDateCust.Text = null;
			txtDateCust.Enabled = false;

			#endregion

			#region "Destination"

			txtZipCodeCust.Text = null;
			txtStateCodeCust.Text = null;

			#endregion
			
			lblErrorMessage.Text = "";
		}


		private int ValidateData()
		{
			if ((String)ViewState["usertype"] != "C")
			{
				if(rbMonth.Checked == true && (ddMonth.SelectedIndex>0) &&(txtYear.Text==""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return 0;
				}
				else if (txtYear.Text != "" )
				{
					if (Convert.ToInt32(txtYear.Text) < 2000 || Convert.ToInt32(txtYear.Text) > 2099)
					{
						lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
						return -1;
					}
				}
			}
			else
			{
				if(rbMonthCust.Checked == true && (ddMonthCust.SelectedIndex>0) && (txtYearCust.Text==""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return 0;
				}
				else if (txtYearCust.Text != "" )
				{
					if (Convert.ToInt32(txtYearCust.Text) < 2000 || Convert.ToInt32(txtYearCust.Text) > 2099)
					{
						lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
						return -1;
					}
				}
			}
			return 1;
		}
		

		private int LastDayOfMonth(String strMonth, String strYear)
		{
			int intLastDay = 0;
			switch (strMonth)
			{
				case "01":
				case "03":
				case "05":
				case "07":
				case "08":
				case "10":
				case "12":
					intLastDay = 31;
					break;
				case "04":
				case "06":
				case "09":
				case "11":
					intLastDay = 30;
					break;
				case "02":
					//intLastDay = 28;
					//by ching 19/02/2008
					DateTime dt = System.DateTime.ParseExact("01/02/"+strYear,"dd/MM/yyyy",null);
					DateTime firstDayOfThisMonth = dt.Subtract(TimeSpan.FromDays(dt.Day - 1));
					DateTime firstDayOfNextMonth = firstDayOfThisMonth.AddMonths(1);
					DateTime lastDayOfThisMonth = firstDayOfNextMonth.Subtract(TimeSpan.FromDays(1)) ;

					intLastDay = Int32.Parse(lastDayOfThisMonth.Day.ToString());
					break;
			}
			return intLastDay;
		}


		private void OpenWindowpage(String strUrl, String strFeatures)
		{
			
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion


		#region "Dates Part : Controls"

		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			ddMonth.SelectedIndex = 0;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = true;
		}

		
		#endregion

		
		#region "Payer Type Part : Controls

		private void btnPayerCode_Click(object sender, System.EventArgs e)
		{
			string strCustPayerType = "";
			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
			{
				if(lsbCustType.Items[i].Selected == true)
				{
					strCustPayerType += lsbCustType.Items[i].Value + ",";
				}
			}
			if((strCustPayerType != null) && (strCustPayerType.Trim().Length > 0))
				strCustPayerType = strCustPayerType.Substring(0, strCustPayerType.Length - 1);

			String sUrl = "CustomerPopup.aspx?FORMID="+"ShipmentTrackingQueryRpt"+
				"&CustType="+strCustPayerType.ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion

		#region "Route / DC Selection : Controls"

		private void rbLongRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}


		private void rbShortRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";	
		}


		private void rbAirRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}


		#endregion

		#region "Destination : Controls"

		private void btnZipCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "ZipCodePopup.aspx?FORMID=ShipmentTrackingQueryRpt"+"&ZIPCODE_CID="+txtZipCode.ClientID+"&ZIPCODE="+txtZipCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openPostalWindow.js",paramList); //Thosapol.y  Modify (26/06/2013)
			//String sScript = Utility.GetScript("openParentWindow.js",paramList); //Thosapol.y  Comment (26/06/2013)
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		private void btnStateCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "StatePopup.aspx?FORMID=ShipmentTrackingQueryRpt"+"&STATECODE="+txtStateCode.ClientID+"&STATECODE_TEXT="+txtStateCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion

		#region "Shipment : Controls"

		private void btnStatusCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "StatusCodePopup.aspx?FORMID=ShipmentTrackingQueryRpt"+"&STATUSCODE="+txtStatusCode.Text+"&EXCEPTCODE="+txtExceptionCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		private void btnExceptionCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "ExceptionCodePopup.aspx?FORMID=ShipmentTrackingQueryRpt"+"&EXCEPTIONCODE="+txtExceptionCode.Text.Trim().ToString()+"&STATUSCODE="+txtStatusCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion


		#region "Dates Part : DropDownList"

		private void ddlmonths()
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);

			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}


		#endregion

		#region "Date Part : DropDownList - Cust"

		private void ddlmonthsCust()
		{
			DataTable dtMonthsCust = new DataTable();
			dtMonthsCust.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonthsCust.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonthsCust.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonthsCust.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonthsCust.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonthsCust.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonthsCust);

			ddMonthCust.DataSource = dvMonths;
			ddMonthCust.DataTextField = "Text";
			ddMonthCust.DataValueField = "StringValue";
			ddMonthCust.DataBind();	
		}


		#endregion

		#region "Payer Type Part : DropDownList"

		public void LoadCustomerTypeList()
		{
			ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"customer_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				lsbCustType.Items.Add(lstItem);
			}
		}



		#endregion

		#region "Route / DC Selection : DropDownList"

		private void SetDbComboServerStates()
		{
			String strDeliveryType=null;
			if (rbLongRoute.Checked==true)
				strDeliveryType="L";
			else if (rbShortRoute.Checked==true)
				strDeliveryType="S";
			else if (rbAirRoute.Checked==true)
				strDeliveryType="A";

			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
			DbComboPathCode.ServerState = hash;
			DbComboPathCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					if(strDelType == "S") 
					{
						strWhereClause=" and Delivery_Type in ('"+Utility.ReplaceSingleQuote(strDelType)+"','W') ";
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct delivery_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
					else
						strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"'";
				}				
			}
			
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]

		public static object DbComboDistributionCenterSelectOrigin(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			string strAppID = util.GetAppID();
			string strEnterpriseID = util.GetEnterpriseID();	

			DataSet dataset = new DataSet();
			dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,args);
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboDistributionCenterSelectDestination(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			string strAppID = util.GetAppID();
			string strEnterpriseID = util.GetEnterpriseID();
			string locId = util.UserLocation();		

			DataSet dataset = new DataSet();
			if(util.CheckUserRoleLocation())
			{
				dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,args);	
			}
			else
			{
				dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,locId,args);
			}
			
			
			return dataset;
		}


		#endregion

		#region "Shipment : DropDownList"

		public void LoadBookingTypeList()
		{
			ListItem defItem = new ListItem();
			defItem.Text = "";
			defItem.Value = "0";
			Drp_BookingType.Items.Add(defItem);
			ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"booking_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				Drp_BookingType.Items.Add(lstItem);
			}

			if (Drp_BookingType.Items.Count > 0)
				Drp_BookingType.SelectedIndex = 0;
		}

		#endregion

		private void btnZipCodeCust_Click(object sender, System.EventArgs e)
		{
			String sUrl = "ZipCodePopup.aspx?FORMID=ShipmentTrackingQueryRpt"+"&ZIPCODE_CID="+txtZipCodeCust.ClientID+"&ZIPCODE="+txtZipCodeCust.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnStateCodeCust_Click(object sender, System.EventArgs e)
		{
			String sUrl = "StatePopup.aspx?FORMID=ShipmentTrackingQueryRpt"+"&STATECODE="+txtStateCodeCust.ClientID+"&STATECODE_TEXT="+txtStateCodeCust.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void rbMonthCust_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonthCust.Enabled = true;
			txtYearCust.Text = null;
			txtYearCust.Enabled = true;
			txtPeriodCust.Text = null;
			txtPeriodCust.Enabled = false;
			txtToCust.Text = null;
			txtToCust.Enabled = false;
			txtDateCust.Text = null;
			txtDateCust.Enabled = false;
		}

		private void rbPeriodCust_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonthCust.Enabled = false;
			ddMonthCust.SelectedIndex = 0;
			rbMonthCust.Checked = false;
			txtYearCust.Text = null;
			txtYearCust.Enabled = false;
			txtPeriodCust.Text = null;
			txtPeriodCust.Enabled = true;
			txtToCust.Text = null;
			txtToCust.Enabled = true;
			txtDateCust.Text = null;
			txtDateCust.Enabled = false;
		}

		private void rbDateCust_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonthCust.Enabled = false;
			txtYearCust.Text = null;
			txtYearCust.Enabled = false;
			txtPeriodCust.Text = null;
			txtPeriodCust.Enabled = false;
			txtToCust.Text = null;
			txtToCust.Enabled = false;
			txtDateCust.Text = null;
			txtDateCust.Enabled = true;
		}
		
		private bool ValidateValues()
		{
			bool iCheck=false;
	
			if((String)ViewState["usertype"] != "C")
			{
				if((ddMonth.SelectedIndex==0)&&(txtYear.Text=="")&&(txtPeriod.Text=="")&&(txtTo.Text=="")&&(txtDate.Text==""))
				{				
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVL",utility.GetUserCulture());
					return iCheck=true;
			
				}
				if(rbMonth.Checked == true)
				{
					if((ddMonth.SelectedIndex>0) &&(txtYear.Text==""))
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
						return iCheck=true;
					}
					else if((ddMonth.SelectedIndex==0) &&(txtYear.Text!=""))
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
						return iCheck=true;
					}
					else
					{
						return iCheck=false;
					}
				}
				if(rbPeriod.Checked == true )
				{
					if((txtPeriod.Text!="")&&(txtTo.Text==""))
					{					
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_END_DT",utility.GetUserCulture());
						return iCheck=true;
					}
					else if((txtPeriod.Text=="")&&(txtTo.Text!=""))
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_START_DT",utility.GetUserCulture());					
						return iCheck=true;			
					}
					else
					{
						return iCheck=false;
					}
				}
				if(rbDate.Checked == true )
				{
					if(txtDate.Text=="")
					{					
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT",utility.GetUserCulture());	
						return iCheck=true;				
					}
				}
			}
			else
			{
				if((ddMonthCust.SelectedIndex==0)&&(txtYearCust.Text=="")&&(txtPeriodCust.Text=="")&&(txtToCust.Text=="")&&(txtDateCust.Text==""))
				{				
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVL",utility.GetUserCulture());
					return iCheck=true;
			
				}
				if(rbMonthCust.Checked == true)
				{
					if((ddMonthCust.SelectedIndex>0) &&(txtYearCust.Text==""))
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
						return iCheck=true;
					}
					else if((ddMonthCust.SelectedIndex==0) &&(txtYearCust.Text!=""))
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
						return iCheck=true;
					}
					else
					{
						return iCheck=false;
					}
				}
				if(rbPeriodCust.Checked == true )
				{
					if((txtPeriodCust.Text!="")&&(txtToCust.Text==""))
					{					
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_END_DT",utility.GetUserCulture());
						return iCheck=true;
					}
					else if((txtPeriodCust.Text=="")&&(txtToCust.Text!=""))
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_START_DT",utility.GetUserCulture());					
						return iCheck=true;			
					}
					else
					{
						return iCheck=false;
					}
				}
				if(rbDateCust.Checked == true )
				{
					if(txtDateCust.Text=="")
					{					
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT",utility.GetUserCulture());	
						return iCheck=true;				
					}
				}
			}			
			return iCheck;
		}

	}
}
