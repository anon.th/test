using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.ties.classes;
using com.common.applicationpages;
using Cambro.Web.DbCombo;

namespace com.ties
{
	/// <summary>
	/// Summary description for AgentZoneRates.
	/// </summary>
	public class AgentZoneRates : BasePage
	{
	
//		Utility utility = null;
		SessionDS m_sdsBZRates = null;
//		AccessRights m_moduleAccessRights = null;
		String m_strAppID;
		String m_strEnterpriseID;	
		SessionDS m_sdsAgent = null;
		static private int m_iSetSize = 4;
		private string sDbComboRegKey="";
		

		protected System.Web.UI.HtmlControls.HtmlTable tblMain;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.DataGrid dgBZRates;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblAgentCode;
		protected System.Web.UI.WebControls.Label lblAgentName;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.TextBox txtCountRec;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentId;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentName;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqAgentId;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.WebControls.TextBox txtOZCode;
		protected System.Web.UI.WebControls.TextBox txtDZCode;
		string strRetMsg = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);

			sDbComboRegKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"]; //"aeaaaaU99999baaaaaaaaaEbbaaaauxm6rdDVU1m6ndDVULm6fdDFn1yJ3gBFnx-FZGmfn0~RVxqXnwzSr1xd7w~RvL~NnwyYJ0~Sn1xqrxzDVeBIVLmUadm6fdDVU1m2yto6fdDVULm6fh~vZGmZq0yReHqyZcmdZsnfJwlUqdCIzZrVZWmcZsnczXc";//System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboRegKey();
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			if (!Page.IsPostBack)
			{
				QueryMode();
				ViewState["AgentOperation"] = Operation.None;
				ViewState["AgentMode"]= ScreenMode.None;
				ViewState["MovePrevious"]=false;
				ViewState["MoveNext"]=false;
				ViewState["MoveFirst"]=false;
				ViewState["MoveLast"]=false;  
			}
			else
			{
				if (Session["SESSION_DS1"] !=null)
				{
					m_sdsBZRates = (SessionDS) Session["SESSION_DS1"];
				}

				if (Session["SESSION_DS3"] !=null)
				{
					m_sdsAgent	= (SessionDS)Session["SESSION_DS3"];
				}
			}
			SetAgentNameServerStates();
			SetAgentIDServerStates();
			PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}

		private void SetAgentNameServerStates()
		{						
			String strAgentID=dbCmbAgentId.Value;			
			Hashtable hash = new Hashtable();		
			if (strAgentID !="")
			{
				hash.Add("strAgentID", strAgentID);
			}			
			dbCmbAgentName.ServerState = hash;			
			dbCmbAgentName.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";					
			
		}

		private void SetAgentIDServerStates()
		{			
			String strAgentName=dbCmbAgentName.Value;			
			Hashtable hash = new Hashtable();			
			if (strAgentName !="")
			{
				hash.Add("strAgentName", strAgentName);
			}
			dbCmbAgentId.ServerState = hash;
			dbCmbAgentId.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";						
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dbCmbAgentId.SelectedItemChanged += new System.EventHandler(this.dbCmbAgentId_SelectedItemChanged_1);
			this.dbCmbAgentName.SelectedItemChanged += new System.EventHandler(this.dbCmbAgentName_SelectedItemChanged_1);
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			dgBZRates.CurrentPageIndex = 0;
			QueryMode();
		}

		private void InsertAgent(int iRowIndex)
		{
			m_sdsAgent = AgentProfileMgrDAL.GetEmptyAgentDS(1);	
			DataRow drEach =  m_sdsAgent.ds.Tables[0].Rows[0];	
			drEach["agentid"]	= dbCmbAgentId.Value; 
			drEach["agent_name"]= dbCmbAgentName.Value;
			Session["SESSION_DS3"]= m_sdsAgent;
		}
		
		private void GetAgentRecSet()
		{
			int iStartIndex = Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsAgent = AgentProfileMgrDAL.GetAgentDS(m_strAppID,m_strEnterpriseID,iStartIndex,m_iSetSize,dsQuery);
			Session["SESSION_DS3"] = m_sdsAgent;
			ViewState["QryRes"] =  m_sdsAgent.QueryResultMaxSize; 			
			decimal pgCnt = (m_sdsAgent.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < Convert.ToDecimal(ViewState["currentSet"]))
			{
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
			}
		}

		private void DisplayCurrentPage()
		{
			DataRow drCurrent = m_sdsAgent.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
						
			if(Utility.IsNotDBNull(drCurrent["agentid"]))
			{
				dbCmbAgentId.Text		= drCurrent["agentid"].ToString();
				dbCmbAgentId.Value		= drCurrent["agentid"].ToString();
				ViewState["AgentID"]	= drCurrent["agentid"].ToString();
			}
			if(Utility.IsNotDBNull(drCurrent["agent_name"]))
			{
				dbCmbAgentName.Text		= drCurrent["agent_name"].ToString();
				dbCmbAgentName.Value	= dbCmbAgentName.Text;
			}
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			btnExecuteQuery.Enabled = false;
			InsertAgent(0);
			ViewState["QUERY_DS"] = m_sdsAgent.ds;
			ViewState["currentPage"] = 0;
			ViewState["currentSet"] = 0;
			GetAgentRecSet();	

			if ((decimal)ViewState["QryRes"]!=0)
			{
				DisplayCurrentPage();
				//Conveyance Rates				
				GetChangedRow(dgBZRates.Items[0],0);
				ViewState["QUERY_DSB"] = m_sdsBZRates.ds;
				dgBZRates.CurrentPageIndex = 0;
				RetreiveSelectedPage();	
				if(m_sdsBZRates.ds.Tables[0].Rows.Count==0)
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
					dgBZRates.EditItemIndex = -1; 
					return;
				}
				//set all records to non-edit mode after execute query
				EditRow(false);
				dgBZRates.Columns[0].Visible=true;
				dgBZRates.Columns[1].Visible=true;
				
				ViewState["ConvMode"] = ScreenMode.ExecuteQuery;
				ViewState["AgentMode"] = ScreenMode.ExecuteQuery;
				EnableNavigationButtons(true,true,true,true);
				dbCmbAgentId.Enabled=false;
				dbCmbAgentName.Enabled=false;
			}
			else
			{
				lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			}
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			//clear the rows from the datagrid
			if (btnExecuteQuery.Enabled == true)
			{
				btnExecuteQuery.Enabled = false;
			}
			if((int)ViewState["Mode"] != (int)ScreenMode.Insert || m_sdsBZRates.ds.Tables[0].Rows.Count >= dgBZRates.PageSize)
			{
				m_sdsBZRates = SysDataManager2.GetEmptyAgentBZRatesDS();
			}
			else
			{
				AddRow();
			}

			btnInsert.Enabled=false;
			EditRow(false);
			ViewState["Mode"] = ScreenMode.Insert;
			ViewState["Operation"] = Operation.Insert;
			dgBZRates.Columns[0].Visible=true;
			dgBZRates.Columns[1].Visible=true;
			//AddRow();
			dgBZRates.EditItemIndex = m_sdsBZRates.ds.Tables[0].Rows.Count - 1;
			dgBZRates.CurrentPageIndex = 0;
			BindGrid();
			Logger.LogDebugInfo("BaseZoneRates","btnInsert_Click","INF003","Data Grid Items count"+dgBZRates.Items.Count);
			getPageControls(Page);
		}


		#region BaseZone Rates Grid Methods
		public void dgBZRates_Button(object sender, DataGridCommandEventArgs e)
		{
			int iOperation = (int)ViewState["Operation"];
			int iMode =(int)ViewState["Mode"];
			String strCmdNm = e.CommandName;
			String strtxtZoneCode = null; 
			String strZoneCode = null;			
			String sUrl=null;
			String sScript=null;
			ArrayList paramList=null;
		
			if (strCmdNm.Equals("Update") || strCmdNm.Equals("Edit") || strCmdNm.Equals("Delete") || strCmdNm.Equals("Cancel"))
			{
				return;
			}
			if (iMode==(int)ScreenMode.ExecuteQuery)
			{
				return;
			}
			if (strCmdNm.Equals("OriginSearch") )
			{
				this.txtOZCode = (msTextBox)e.Item.FindControl("txtOZCode");
				if(this.txtOZCode != null)
				{
					strtxtZoneCode = this.txtOZCode.ClientID;
					strZoneCode = this.txtOZCode.Text;
				}
			}
			else if (strCmdNm.Equals("DestinationSearch") )
			{
				this.txtDZCode = (msTextBox)e.Item.FindControl("txtDZCode");
				if(txtDZCode != null)
				{
					strtxtZoneCode = this.txtDZCode.ClientID;
					strZoneCode = this.txtDZCode.Text;
				}
			}
			else
			{
				return; 
				//This Button Event fires for Page Navigation also,
				// So the Popup window must not be shown.
			}

			sUrl = "ZonePopup.aspx?FORMID=AgentZoneRates&ZONECODE_TEXT="+strZoneCode+"&ZONECODE="+strtxtZoneCode;
			paramList = new ArrayList();
			paramList.Add(sUrl);
			sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
						
		}

		protected void dgBZRates_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			String strAgentId=dbCmbAgentId.Value.Trim();
			if (strAgentId=="")
			{
				lblErrorMessage.Text="Agent Id cannot be null";
				return;
			}

			if (btnExecuteQuery.Enabled)
			{
				return;
			}

			int iRowsAffected = 0;
			int intChkRange = 1;
			int rowIndex = e.Item.ItemIndex;
			GetChangedRow(e.Item, e.Item.ItemIndex);
		
			int iOperation = (int)ViewState["Operation"];
			try
			{
				if (iOperation == (int)Operation.Insert)
				{
					bool bAlreadyExists=SysDataManager2.CheckBZRWeights(m_sdsBZRates.ds, rowIndex, m_strAppID,m_strEnterpriseID, strAgentId);
					if (bAlreadyExists==true)
					{
						lblErrorMessage.Text ="Weight Range is already existing. Please use another range.";
						return;
					}

					intChkRange = ChkWtRange();
					if (intChkRange > 0)
					{
						iRowsAffected = SysDataManager2.InsertAgentBZRates(m_sdsBZRates.ds, rowIndex, m_strAppID, m_strEnterpriseID, strAgentId);
						ArrayList paramValues = new ArrayList();
						paramValues.Add(""+iRowsAffected);						
						lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_INS",utility.GetUserCulture(),paramValues);
					}
					else
					{
						lblErrorMessage.Text = strRetMsg.ToString();
						return;
					}
				}
				else
				{ 
					DataSet dsChangedRow = m_sdsBZRates.ds.GetChanges();
					iRowsAffected = SysDataManager2.UpdateAgentBZRates(dsChangedRow, m_strAppID, m_strEnterpriseID, strAgentId);
					ArrayList paramValues = new ArrayList();
					paramValues.Add(""+iRowsAffected);						
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_UPD",utility.GetUserCulture(),paramValues);
					m_sdsBZRates.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
				}
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				strMsg = appException.InnerException.Message;
				strMsg = appException.InnerException.InnerException.Message;
				if(strMsg.IndexOf("duplicate") != -1 )
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());						
				}
				if((strMsg.IndexOf("unique") != -1) || (strMsg.IndexOf("PRIMARY KEY") != -1))
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());						
				}
				else if(strMsg.IndexOf("FOREIGN KEY") != -1 )
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MASTER_NOT_FOUND",utility.GetUserCulture());
				}				
				else if(strMsg.IndexOf("parent key") != -1 )
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_ZIP_MSTR",utility.GetUserCulture());
				}		
				else
				{
					lblErrorMessage.Text=strMsg;
				}

				Logger.LogTraceError("BaseZoneRates.aspx.cs","dgBZRates_Update","RBAC003",appException.Message.ToString());				
				return;
			}

			if (iRowsAffected == 0)
			{
				return;
			}
			btnInsert.Enabled=true;
			ViewState["Operation"] = Operation.None;

			if (intChkRange >= 0)
			{
				dgBZRates.EditItemIndex = -1;
			}

			BindGrid();
			Logger.LogDebugInfo("BaseZoneRates","dgBZRates_Update","INF004","updating data grid...");			
		
		}

		protected void dgBZRates_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			int rowIndex = e.Item.ItemIndex;
			if ((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int)ViewState["Operation"] == (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				m_sdsBZRates.ds.Tables[0].Rows.RemoveAt(rowIndex);
			}
			ViewState["Operation"] = Operation.None;
			dgBZRates.EditItemIndex = -1;
			BindGrid();
			Logger.LogDebugInfo("BaseZoneRates","dgBZRates_Edit","INF004","updating data grid...");
		}

		protected void dgBZRates_Edit(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text="";
			if((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int) ViewState["Operation"] == (int)Operation.Insert && dgBZRates.EditItemIndex > 0)
			{
				m_sdsBZRates.ds.Tables[0].Rows.RemoveAt(dgBZRates.EditItemIndex);
				m_sdsBZRates.QueryResultMaxSize--;
				dgBZRates.CurrentPageIndex = 0;
			}
			dgBZRates.EditItemIndex = e.Item.ItemIndex;
			ViewState["Operation"] = Operation.Update;
			BindGrid();
			Logger.LogDebugInfo("BaseZoneRates","dgBZRates_Edit","INF004","updating data grid...");
		}

		protected void dgBZRates_Delete(object sender, DataGridCommandEventArgs e)
		{
			String strAgentId=(String)ViewState["AgentID"];
			if (strAgentId=="")
			{
				lblErrorMessage.Text="Agent Id cannot be null";
				return;
			}
			if (btnExecuteQuery.Enabled)
				return;

			if ((int)ViewState["Operation"] == (int)Operation.Update)
			{
				dgBZRates.EditItemIndex = -1;
			}			
			
			int rowIndex = e.Item.ItemIndex;
			m_sdsBZRates = (SessionDS)Session["SESSION_DS1"];
			
			try
			{
				// delete from table
				int iRowsDeleted = SysDataManager2.DeleteAgentBZRates(m_sdsBZRates.ds, rowIndex, m_strAppID, m_strEnterpriseID, strAgentId);
				ArrayList paramValues = new ArrayList();
				paramValues.Add(""+iRowsDeleted);						
				lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_DLD",utility.GetUserCulture(),paramValues);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_ZONE_TRANS",utility.GetUserCulture());
				if(strMsg.IndexOf("child record") != -1)
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_ZONE_TRANS",utility.GetUserCulture());
				else
				{
					lblErrorMessage.Text = strMsg;
				}

				Logger.LogTraceError("BaseZoneRates.aspx.cs","dgBZRates_Delete","RBAC003",appException.Message.ToString());				
				return;
			}
			if((int)ViewState["Mode"] == (int)ScreenMode.Insert)
				m_sdsBZRates.ds.Tables[0].Rows.RemoveAt(rowIndex);				
			else
			{
				RetreiveSelectedPage();
			}

			ViewState["Operation"] = Operation.None;
			//Make the row in non-edit Mode
			EditRow(false);
			BindGrid();
			Logger.LogDebugInfo("BaseZoneRates","dgBZRates_Delete","INF004","updating data grid...");			
			
		}

		protected void dgBZRates_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsBZRates.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			if ((int)ViewState["Operation"] != (int)Operation.Insert && (!btnExecuteQuery.Enabled))
			{
				this.txtOZCode = (msTextBox)e.Item.FindControl("txtOZCode");
				this.txtDZCode = (msTextBox)e.Item.FindControl("txtDZCode");
				msTextBox txtStartWt = (msTextBox)e.Item.FindControl("txtStartWt");
				msTextBox txtEndWt = (msTextBox)e.Item.FindControl("txtEndWt");
				
				if(this.txtOZCode != null )
				{
					this.txtOZCode.Text=drSelected["Origin_Zone_Code"].ToString();
					this.txtOZCode.Enabled=false;
				}
				if(this.txtDZCode != null )
				{
					this.txtDZCode.Text=drSelected["Destination_Zone_Code"].ToString();
					this.txtDZCode.Enabled=false;
					
				}
				if (txtStartWt !=null)
					txtStartWt.Enabled = false;
				if (txtEndWt !=null)
					txtEndWt.Enabled = false;				
			}

			if ((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int)ViewState["Operation"] == (int)Operation.Insert)
			{
				e.Item.Cells[1].Enabled=false;
			}
			SetAgentIDServerStates();
			SetAgentNameServerStates();
		}

		protected void dgBZRates_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgBZRates.CurrentPageIndex = e.NewPageIndex;
			RetreiveSelectedPage();
			Logger.LogDebugInfo("BaseZoneRates","dgBZRates_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		#endregion

		private void BindGrid()
		{
			dgBZRates.VirtualItemCount = System.Convert.ToInt32(m_sdsBZRates.QueryResultMaxSize);
			dgBZRates.DataSource = m_sdsBZRates.ds;
			dgBZRates.DataBind();
			Session["SESSION_DS1"] = m_sdsBZRates;
		}

		private void EditRow(bool bItemIndex)
		{
			foreach (DataGridItem item in dgBZRates.Items)
			{ 
				if (bItemIndex) 
				{
					dgBZRates.EditItemIndex = item.ItemIndex; }
				else 
				{
					dgBZRates.EditItemIndex = -1; }
			}
			dgBZRates.DataBind();
		}

		private void QueryMode()
		{
			//Set Query Mode
			ViewState["Mode"] = ScreenMode.None;
			ViewState["Operation"] = Operation.None;
			ViewState["AgentOperation"] = Operation.None;
			ViewState["AgentMode"]		= ScreenMode.Query;
            			
			m_sdsAgent = AgentProfileMgrDAL.GetEmptyAgentDS(1);
			EnableNavigationButtons(false,false,false,false);

			dbCmbAgentId.Value="";dbCmbAgentName.Value="";
			dbCmbAgentId.Text="";dbCmbAgentName.Text="";

			ClearDbCombos();

			//Set Button Enable Properties
			btnQuery.Enabled=true;
			btnExecuteQuery.Enabled=true;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			//inset an empty row for query 
			m_sdsBZRates = SysDataManager2.GetEmptyAgentBZRatesDS();
			m_sdsBZRates.DataSetRecSize = 1;
			//AddRow(); 
			Session["SESSION_DS1"] = m_sdsBZRates;
			BindGrid();
			lblErrorMessage.Text="";
			//Make the row in Edit Mode
			EditRow(true);
			dgBZRates.Columns[0].Visible=false;
			dgBZRates.Columns[1].Visible=false;
			dbCmbAgentId.Enabled=true;
			dbCmbAgentName.Enabled=true;
		}

		private void AddRow()
		{
			SysDataManager2.AddRowAgentBZRatesDS(ref m_sdsBZRates);
			BindGrid();
		}

		private int ChkWtRange()
		{
			int intChkWtRet = 0;
			intChkWtRet = SysDataManager2.CheckWtInBZRatesDS (m_sdsBZRates.ds, m_strAppID, m_strEnterpriseID);
			switch (intChkWtRet)
			{
				case -1:
					//strRetMsg = "End Weight should be greater than Start Weight";
					strRetMsg = Utility.GetLanguageText(ResourceType.UserMessage, "EWT_GT_SWT", utility.GetUserCulture());
					break;
				case -2:
					//strRetMsg = "End Weight should be greater than Start Weight";
					strRetMsg = Utility.GetLanguageText(ResourceType.UserMessage, "EWT_GT_SWT", utility.GetUserCulture());
					break;
				default:
					strRetMsg = null;
					break;
			}
			return intChkWtRet;
		}

		private void GetChangedRow(DataGridItem item, int rowIndex)
		{
			this.txtOZCode = (msTextBox)item.FindControl("txtOZCode");
			this.txtDZCode = (msTextBox)item.FindControl("txtDZCode");
			msTextBox txtStartWt = (msTextBox)item.FindControl("txtStartWt");
			msTextBox txtEndWt = (msTextBox)item.FindControl("txtEndWt");
			msTextBox txtSPrice = (msTextBox)item.FindControl("txtSPrice");
			msTextBox txtIPrice = (msTextBox)item.FindControl("txtIPrice");
			
			String strOZCode="";
			String strDZCode="";
			decimal decStartWt = 0;
			decimal decEndWt = 0;
			decimal decSPrice = 0;
			decimal decIPrice = 0;

			if(this.txtDZCode !=null)
			{
				strDZCode = this.txtDZCode.Text;
			}
			if (this.txtOZCode !=null)
			{
				strOZCode = this.txtOZCode.Text;
			}
			if ((txtStartWt !=null) && (txtStartWt.Text != ""))
			{
				decStartWt = Convert.ToDecimal(txtStartWt.Text.ToString());
			}
			if ((txtEndWt !=null) && (txtEndWt.Text != ""))
			{
				decEndWt = Convert.ToDecimal(txtEndWt.Text.ToString());
			}
			if ((txtSPrice !=null) && (txtSPrice.Text != ""))
			{
				decSPrice = Convert.ToDecimal(txtSPrice.Text.ToString());
			}
			if ((txtIPrice !=null) && (txtIPrice.Text != ""))
			{
				decIPrice = Convert.ToDecimal(txtIPrice.Text.ToString());
			}

			try
			{
				DataRow dr = m_sdsBZRates.ds.Tables[0].Rows[rowIndex];
				dr[0] = strOZCode;
				dr[1] = strDZCode;
				dr[2] = decStartWt;
				dr[3] = decEndWt;
				dr[4] = decSPrice;
				dr[5] = decIPrice;
			}
			catch (Exception ex)
			{
				lblErrorMessage.Text=ex.Message;
			}
		}

		private void RetreiveSelectedPage()
		{
			String strAgentId="";
			strAgentId=dbCmbAgentId.Value; 
			if (strAgentId=="") 
				strAgentId=(String)ViewState["AgentID"];			

			int iStartRow = dgBZRates.CurrentPageIndex * dgBZRates.PageSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DSB"];	

			m_sdsBZRates = SysDataManager2.GetAgentBZRatesDS(m_strAppID, m_strEnterpriseID, strAgentId,  iStartRow, dgBZRates.PageSize, dsQuery);
			decimal iPageCnt =Convert.ToInt32((m_sdsBZRates.QueryResultMaxSize - 1))/dgBZRates.PageSize;
			if(iPageCnt < dgBZRates.CurrentPageIndex)
			{
				dgBZRates.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			dgBZRates.SelectedIndex = -1;
			dgBZRates.EditItemIndex = -1;
			lblErrorMessage.Text = "";
			BindGrid();
			Session["SESSION_DS1"] = m_sdsBZRates;
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object AgentIdServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
						
			String strAgentName="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{			
				if(args.ServerState["strAgentName"] != null && args.ServerState["strAgentName"].ToString().Length > 0)
				{
					strAgentName = args.ServerState["strAgentName"].ToString();					
				}	
			}
			
			DataSet dataset = DbComboDAL.PayerIDQuery(strAppID,strEnterpriseID,args, "A", strAgentName);	
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethod(true)]
		public static object AgentNameServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
	
			String strAgentID= "";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{				
				if (args.ServerState["strAgentID"] != null && args.ServerState["strAgentID"].ToString().Length > 0 )
				{
					strAgentID = (args.ServerState["strAgentID"].ToString());
				}
			}	

			DataSet dataset = DbComboDAL.PayerNameQuery(strAppID,strEnterpriseID,args, "A", strAgentID);	
			return dataset;                
		}


		[Cambro.Web.DbCombo.ResultsMethod(true)]
		public static object OriginZoneSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			String strAgentID= "";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{				
				if (args.ServerState["strAgentID"] != null && args.ServerState["strAgentID"].ToString().Length > 0 )
				{
					strAgentID = (args.ServerState["strAgentID"].ToString());
				}
			}	
			DataSet dataset = DbComboDAL.OriginZoneQuery(strAppID,strEnterpriseID,args, strAgentID);	
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethod(true)]
		public static object DestinationZoneSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			String strAgentID= "";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{				
				if (args.ServerState["strAgentID"] != null && args.ServerState["strAgentID"].ToString().Length > 0 )
				{
					strAgentID = (args.ServerState["strAgentID"].ToString());
				}
			}	
			DataSet dataset = DbComboDAL.DestinationZoneQuery(strAppID,strEnterpriseID,args, strAgentID);	
			return dataset;
		}

		private void dbCmbAgentId_SelectedItemChanged(object sender, System.EventArgs e)
		{
			SetAgentNameServerStates();			
		}

		private void ClearDbCombos()
		{
			if(dgBZRates.EditItemIndex == -1)
				return;

			if(dgBZRates.Items.Count==0)
				return;

			this.txtOZCode = (msTextBox)dgBZRates.Items[dgBZRates.EditItemIndex].FindControl("txtOZCode");
			this.txtDZCode = (msTextBox)dgBZRates.Items[dgBZRates.EditItemIndex].FindControl("txtDZCode");		

			if(this.txtOZCode != null && this.txtDZCode !=null)
			{
				this.txtOZCode.Text="";
				this.txtDZCode.Text="";				
			}			
		}

		private void dbCmbAgentName_SelectedItemChanged(object sender, System.EventArgs e)
		{
			SetAgentNameServerStates();
		}

		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = Convert.ToInt32((m_sdsAgent.QueryResultMaxSize - 1))/m_iSetSize;	
			GetAgentRecSet();
			ViewState["currentPage"] = m_sdsAgent.DataSetRecSize - 1;
			DisplayCurrentPage();
			RetreiveSelectedPage();
			EnableNavigationButtons(true,true,false,false);
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			SetAgentIDServerStates();
			SetAgentNameServerStates();

		}

		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  > 0 )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) - 1;
				DisplayCurrentPage();	
				RetreiveSelectedPage();
			}
			else
			{
				if( (Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) - 1;	
				GetAgentRecSet();
				ViewState["currentPage"] = m_sdsAgent.DataSetRecSize - 1;
				DisplayCurrentPage();
				RetreiveSelectedPage();
			}
			EnableNavigationButtons(true,true,true,true);
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			SetAgentIDServerStates();
			SetAgentNameServerStates();
		}

		private void btnNextPage_Click(object sender, System.EventArgs e)
		{			
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);
			if(Convert.ToInt32(ViewState["currentPage"])  < (m_sdsAgent.DataSetRecSize - 1) )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) + 1;
				DisplayCurrentPage();
				RetreiveSelectedPage();
			}
			else
			{
				decimal iTotalRec = (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) + m_sdsAgent.DataSetRecSize;
				if( iTotalRec ==  m_sdsAgent.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) + 1;	
				ViewState["currentPage"] = 0;
				DisplayCurrentPage();
				RetreiveSelectedPage();
			}
			EnableNavigationButtons(true,true,true,true);
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			SetAgentIDServerStates();
			SetAgentNameServerStates();
		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = 0;	
			GetAgentRecSet();
			ViewState["currentPage"] = 0;
			DisplayCurrentPage();
			RetreiveSelectedPage();
			EnableNavigationButtons(false,false,true,true);		
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			SetAgentIDServerStates();
			SetAgentNameServerStates();
		}
		
		
		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}

		private void dbCmbAgentId_SelectedItemChanged_1(object sender, System.EventArgs e)
		{
			DbComboRegKey();			
			SetAgentNameServerStates();
		}

		private void dbCmbAgentName_SelectedItemChanged_1(object sender, System.EventArgs e)
		{
			DbComboRegKey();
			SetAgentIDServerStates();			
		}
		private void DbComboRegKey()
		{
			dbCmbAgentId.RegistrationKey=sDbComboRegKey;//System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbAgentName.RegistrationKey=sDbComboRegKey;//System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		}


	}
}
