<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Page language="c#" Codebehind="ShipmentTrackingQuery.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ShipmentTrackingQuery" EnableEventValidation="False" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentTrackingQuery</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="ShipmentTrackingQuery" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 100; POSITION: absolute; TOP: 40px; LEFT: 20px" runat="server"
				Width="64px" CssClass="queryButton" Text="Query"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 105; POSITION: absolute; TOP: 71px; LEFT: 20px"
				runat="server" Width="700px" CssClass="errorMsgColor" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 101; POSITION: absolute; TOP: 40px; LEFT: 84px"
				runat="server" Width="123px" CssClass="queryButton" Text="Execute Query"></asp:button><asp:button id="btnCancelQry" style="Z-INDEX: 102; POSITION: absolute; TOP: 40px; LEFT: 234px"
				runat="server" CssClass="queryButton" Text="Cancel Query" Enabled="False" Visible="False"></asp:button>
			<TABLE id="tblShipmentTracking" style="Z-INDEX: 104; POSITION: absolute; WIDTH: 851px; HEIGHT: 500px; TOP: 80px; LEFT: 14px"
				width="851" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 464px">
						<fieldset style="WIDTH: 457px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" Width="20px" CssClass="tableHeadingFieldset" Font-Bold="True">Dates</asp:label></legend>
							<TABLE id="tblDates" style="WIDTH: 445px; HEIGHT: 113px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbBookingDate" runat="server" Width="126px" CssClass="tableRadioButton" Text="Booking Date"
											Height="22px" Checked="True" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbPickupDate" runat="server" Width="119px" CssClass="tableRadioButton" Text="Pickup Date"
											Height="22px" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbStatusDate" runat="server" Width="116px" CssClass="tableRadioButton" Text="Status Date"
											Height="22px" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton></TD>
									<td style="WIDTH: 1px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Width="73px" CssClass="tableRadioButton" Text="Month"
											Height="21px" Checked="True" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" Width="88px" CssClass="textField" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" Width="30px" CssClass="tableLabel" Height="15px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" Width="83" CssClass="textField" NumberPrecision="4"
											NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></TD>
									<td style="WIDTH: 1px"></td>
								</TR>
								<TR>
									<td style="HEIGHT: 20px"></td>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Width="62px" CssClass="tableRadioButton" Text="Period"
											GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" Width="82" CssClass="textField" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" Width="83" CssClass="textField" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="WIDTH: 1px; HEIGHT: 20px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Width="74px" CssClass="tableRadioButton" Text="Date"
											Height="22px" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" Width="82" CssClass="textField" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="WIDTH: 1px"></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD>
						<fieldset style="WIDTH: 357px; HEIGHT: 105px"><legend><asp:label id="lblPayerType" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Payer Type</asp:label></legend>
							<TABLE id="tblPayerType" style="WIDTH: 344px; HEIGHT: 73px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<tr>
									<td></td>
									<td colSpan="3" height="1">&nbsp;</td>
									<td></td>
								</tr>
								<TR>
									<td></td>
									<TD class="tableLabel" colSpan="3">&nbsp;
										<asp:radiobutton id="rbCustomer" runat="server" Width="75px" CssClass="tableRadioButton" Text="Customer"
											Height="22px" Checked="True" GroupName="QueryByPayer" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbAgent" runat="server" Width="146px" CssClass="tableRadioButton" Text="Agent"
											Height="22px" GroupName="QueryByPayer" AutoPostBack="True"></asp:radiobutton></TD>
									<td></td>
								</TR>
								<TR height="33">
									<td bgColor="blue"></td>
									<TD class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblPayerCode" runat="server" Width="79px" CssClass="tableLabel" Height="22px">Payer Code</asp:label><cc1:mstextbox id="txtPayerCode" runat="server" Width="148px" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="20"></cc1:mstextbox><asp:button id="btnPayerCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 464px">
						<fieldset style="WIDTH: 458px; HEIGHT: 86px"><legend><asp:label id="lblRouteType" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Route Type</asp:label></legend>
							<TABLE id="tblRouteType" style="WIDTH: 450px; HEIGHT: 69px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<td></td>
									<TD class="tableLabel" colSpan="3" style="WIDTH: 454px">&nbsp;
										<asp:radiobutton id="rbLongRoute" runat="server" Width="140px" CssClass="tableRadioButton" Text="Linehaul"
											Height="22px" GroupName="QueryByRoute" AutoPostBack="True" Checked="True"></asp:radiobutton><asp:radiobutton id="rbShortRoute" runat="server" Width="104px" CssClass="tableRadioButton" Text="Short Route"
											Height="22px" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbAirRoute" runat="server" Width="184px" CssClass="tableRadioButton" Text="Air Route"
											Height="22px" GroupName="QueryByRoute" AutoPostBack="True"></asp:radiobutton></TD>
									<td style="WIDTH: 2px"></td>
								</TR>
								<tr>
									<td></td>
									<TD class="tableLabel" style="WIDTH: 454px" colSpan="3">&nbsp;
										<asp:label id="lblRouteCode" runat="server" Width="111px" CssClass="tableLabel" Height="22px">Route Code</asp:label>
										<!--<cc1:mstextbox id="txtRouteCode" runat="server" Width="151px" CssClass="textField" AutoPostBack="True"
											TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12"></cc1:mstextbox>&nbsp;
										<asp:button id="btnRouteCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button>-->
										<DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
											ServerMethod="DbComboPathCodeSelect" ReQueryDisabled="True"></DBCOMBO:DBCOMBO>
									</TD>
									<td style="WIDTH: 2px"></td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
					<TD>
						<fieldset style="WIDTH: 359px; HEIGHT: 89px"><legend><asp:label id="lblDestination" runat="server" Width="79px" CssClass="tableHeadingFieldset"
									Font-Bold="True">Destination</asp:label></legend>
							<TABLE id="tblDestination" cellSpacing="0" cellPadding="0" align="left" border="0" runat="server">
								<tr height="37">
									<td></td>
									<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
										<asp:label id="lblZipCode" runat="server" Width="74px" CssClass="tableLabel" Height="22px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCode" runat="server" Width="139px" CssClass="textField" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithHyphen"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
										<asp:label id="lblStateCode" runat="server" Width="90px" CssClass="tableLabel" Height="22px">State Code</asp:label><cc1:mstextbox id="txtStateCode" runat="server" Width="139" CssClass="textField" Height="22" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD colSpan="2">
						<fieldset style="WIDTH: 828px; HEIGHT: 173px"><legend><asp:label id="lblShipment" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Shipment</asp:label></legend>
							<TABLE id="tblShipment" style="WIDTH: 809px; HEIGHT: 121px" cellSpacing="0" cellPadding="0"
								width="809" align="left" border="0" runat="server">
								<tr>
									<td></td>
									<TD class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblBookingType" runat="server" Width="115px" CssClass="tableLabel" Height="22px">Booking Type</asp:label>&nbsp;
										<asp:dropdownlist id="Drp_BookingType" runat="server" Width="161px" CssClass="textField"></asp:dropdownlist></TD>
								</tr>
								<tr>
									<td></td>
									<TD class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblConsignmentNo" runat="server" Width="110px" CssClass="tableLabel" Height="18px">Consignment No</asp:label>&nbsp;&nbsp;
										<asp:textbox id="txtConsignmentNo" runat="server" Width="161px" CssClass="textField" AutoPostBack="True"
											MaxLength="30"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										<!--#Start:PSA 2009_029 By Gwang on 13Mar09-->
										<asp:label id="lblRefNo" runat="server" CssClass="tableLabel" Width="110px" Height="18px">Customer Ref. #</asp:label>&nbsp;&nbsp;							<asp:textbox id="txtRefNo" tabIndex="5" runat="server" CssClass="textField" Width="99px" AutoPostBack="True"
											MaxLength="20"></asp:textbox>&nbsp; 
										<!--#End:PSA 2009_029 By Gwang on 13Mar09-->
										<asp:label id="lblBookingNo" runat="server" Width="80px" CssClass="tableLabel" Height="18px">Booking No</asp:label>&nbsp;<cc1:mstextbox id="txtBookingNo" runat="server" Width="134px" CssClass="textField" AutoPostBack="True"
											NumberPrecision="10" NumberMaxValue="2147483647" TextMaskType="msNumeric" MaxLength="10"></cc1:mstextbox></TD>
								</tr>
								<tr>
									<td></td>
									<TD class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblStatusCode" runat="server" Width="89px" CssClass="tableLabel" Height="22px">Status Code</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtStatusCode" runat="server" Width="161px" CssClass="textField" AutoPostBack="True"
											TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="10"></cc1:mstextbox>&nbsp;
										<asp:button id="btnStatusCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:label id="lblExceptionCode" runat="server" Width="89px" CssClass="tableLabel" Height="22px">Exception Code</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <cc1:mstextbox id="txtExceptionCode" runat="server" Width="134px" CssClass="textField" AutoPostBack="True"
											TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="10"></cc1:mstextbox><asp:button id="btnExceptionCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
								</tr>
								<tr>
									<td></td>
									<TD class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblShipmentClosed" runat="server" Width="120px" CssClass="tableLabel" Height="17px">Shipment Closed</asp:label><asp:radiobutton id="rbShipmentYes" runat="server" Width="39px" CssClass="tableRadioButton" Text="Yes"
											Height="22px" Enabled="False" GroupName="QueryByShipment" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbShipmentNo" runat="server" Width="87px" CssClass="tableRadioButton" Text="No"
											Height="22px" Enabled="False" GroupName="QueryByShipment" AutoPostBack="True"></asp:radiobutton><asp:label id="lblInvoiceIssued" runat="server" Width="98px" CssClass="tableLabel" Height="17px">Invoice Issued</asp:label><asp:radiobutton id="rbInvoiceYes" runat="server" Width="64px" CssClass="tableRadioButton" Text="Yes"
											Height="22px" GroupName="QueryByInvoice" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbInvoiceNo" runat="server" Width="60px" CssClass="tableRadioButton" Text="No"
											Height="22px" Checked="True" GroupName="QueryByInvoice" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									</TD>
								</tr>
								<tr>
									<td></td>
									<TD class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="Label2" runat="server" CssClass="tableLabel" Height="17px">Match status code Inside History</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:radiobutton id="rbCheckAllYes" runat="server" Width="56px" CssClass="tableRadioButton" Text="Yes"
											Height="22px" GroupName="SearchAll" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;<asp:radiobutton id="rbCheckAllNo" runat="server" Width="87px" CssClass="tableRadioButton" Text="No"
											Height="22px" GroupName="SearchAll" Checked="True" AutoPostBack="True"></asp:radiobutton>
									</TD>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
			</TABLE>
			<asp:label id="Label1" style="Z-INDEX: 103; POSITION: absolute; TOP: 4px; LEFT: 20px" runat="server"
				Width="365px" CssClass="maintitleSize" Height="27px">Shipment Tracking</asp:label></TR></TABLE></TR></TABLE><input 
type=hidden value="<%=strScrollPosition%>" name=ScrollPosition>
		</form>
	</body>
</HTML>
