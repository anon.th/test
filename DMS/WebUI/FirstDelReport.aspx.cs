using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;
using Cambro.Web.DbCombo;
using com.common.RBAC;
using com.ties.classes;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for FirstDelReport.
	/// </summary>
	public class FirstDelReport : BasePage
	{
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.RadioButton rbBookingDate;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.Label lblPayerType;
		protected System.Web.UI.WebControls.Label lblPayerCode;
		protected System.Web.UI.WebControls.TextBox txtPayerCode;
		protected System.Web.UI.WebControls.Button btnPayerCode;
		protected System.Web.UI.WebControls.Label lblRouteType;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCode;
		protected System.Web.UI.WebControls.Label Label3;
		protected Cambro.Web.DbCombo.DbCombo DbComboDestinationDC;
		protected System.Web.UI.WebControls.Label lblDestination;
		protected System.Web.UI.WebControls.Label lblZipCode;
		protected com.common.util.msTextBox txtZipCode;
		protected System.Web.UI.WebControls.Button btnZipCode;
		protected System.Web.UI.WebControls.Label lblStateCode;
		protected com.common.util.msTextBox txtStateCode;
		protected System.Web.UI.WebControls.Button btnStateCode;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.HtmlControls.HtmlTable tblPayerType;
		protected System.Web.UI.HtmlControls.HtmlTable tblRouteType;
		protected System.Web.UI.HtmlControls.HtmlTable tblInternal;
		protected System.Web.UI.WebControls.RadioButton rbShortRoute;
		protected System.Web.UI.WebControls.CheckBox chkShowDetail;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.ListBox lsbCustType;
		protected System.Web.UI.HtmlControls.HtmlTable tblDestination;

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnZipCode.Click += new System.EventHandler(this.btnZipCode_Click);
			this.btnStateCode.Click += new System.EventHandler(this.btnStateCode_Click);
			this.rbBookingDate.CheckedChanged += new System.EventHandler(this.rbBookingDate_CheckedChanged);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.ddMonth.SelectedIndexChanged += new System.EventHandler(this.ddMonth_SelectedIndexChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.btnPayerCode.Click += new System.EventHandler(this.btnPayerCode_Click);
			this.ID = "SalesTerritoryReportQuery";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		String m_strAppID=null;
		String m_strEnterpriseID=null;
		String m_strCulture=null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboDestinationDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			com.common.classes.User user = RBACManager.GetUser(m_strAppID, m_strEnterpriseID, (String)Session["userID"]);
			ViewState["usertype"] = user.UserType;
			ViewState["PayerID"] = user.PayerID;

			if ((String)ViewState["usertype"]  == "C")
			{
				tblInternal.Visible = false;
			}
			else
			{
				tblInternal.Visible = true;
			}

			if(!Page.IsPostBack)
			{
				DefaultScreen();
				ddlmonths();
				ddlmonthscust();
				LoadCustomerTypeList();
				Session["toRefresh"]=false;
			}

			SetDbComboServerStates();
		}


		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			bool icheck =  ValidateValues();
			if(icheck == false)
			{
				String strModuleID = Request.Params["MODID"];
				DataSet dsShipment = GetShipmentQueryData();

				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				if(chkShowDetail.Checked)
				{
					if(txtZipCode.Text == "" && txtStateCode.Text == "")
						Session["FORMID"] = "FirstDelDetailReport";
					else
						Session["FORMID"] = "FirstDelReportProv";
				}
				else
				{
					if(txtZipCode.Text == "" && txtStateCode.Text == "")
						Session["FORMID"] = "FirstDelReport";
					else
						Session["FORMID"] = "FirstDelReportProv";
				}
				
				Session["SESSION_DS1"] = dsShipment;
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindow.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
		}


		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}


		#region "Page Functions"

		private DataTable CreateEmptyDataTable()
		{
			DataTable dtShipment = new DataTable();

			#region "Dates"
			dtShipment.Columns.Add(new DataColumn("tran_date", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
			dtShipment.Columns.Add(new DataColumn("end_date", typeof(DateTime)));
			#endregion
			
			#region "Payer Type"
			dtShipment.Columns.Add(new DataColumn("payer_type", typeof(string)));	//boon
			dtShipment.Columns.Add(new DataColumn("payer_code", typeof(string)));
			#endregion
		
			#region "Route / DC Selection"
			dtShipment.Columns.Add(new DataColumn("route_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("destination_dc", typeof(string)));
			#endregion

			#region "Destination"
			dtShipment.Columns.Add(new DataColumn("zip_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("state_code", typeof(string)));
			#endregion

			#region "Report To Show"
			dtShipment.Columns.Add(new DataColumn("RptType", typeof(string)));
			#endregion

			#region "flag"
			dtShipment.Columns.Add(new DataColumn("flag", typeof(string)));
			#endregion

			return dtShipment;
		}


		private DataSet GetShipmentQueryData()
		{
			DataSet dsShipment = new DataSet();
			DataTable dtShipment = CreateEmptyDataTable();
			DataRow dr = dtShipment.NewRow(); 
			
			#region "Dates"
			
			//by sittichai  23/01/2008
			if((String)ViewState["usertype"] != "C")
			{
				if (rbBookingDate.Checked) 
				{	// Booking Date Selected
					dr["tran_date"] = "B";
				}
			
				string strMonth =null;
				if (rbMonth.Checked) 
				{	// Month Selected
					strMonth = ddMonth.SelectedItem.Value;
					if (strMonth != "" && txtYear.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact ("01/"+strMonth.ToString()+"/"+txtYear.Text+" 00:00:01", "dd/MM/yyyy HH:mm:ss", null);
						int intLastDay = 0;
						intLastDay = LastDayOfMonth(strMonth, txtYear.Text);
						dr["end_date"] = DateTime.ParseExact (intLastDay.ToString()+"/"+strMonth.ToString()+"/"+txtYear.Text+" 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
					}
				}
				if (rbPeriod.Checked) 
				{	// Period Selected
					if (txtPeriod.Text != ""  && txtTo.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact(txtPeriod.Text+" 00:00:01","dd/MM/yyyy HH:mm:ss",null);
						dr["end_date"] = DateTime.ParseExact(txtTo.Text+" 23:59:59" ,"dd/MM/yyyy HH:mm:ss",null);
					}
				}
				if (rbDate.Checked) 
				{	// Date Selected
					if (txtDate.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact(txtDate.Text+" 00:00:01", "dd/MM/yyyy HH:mm:ss",null);
						dr["end_date"] = DateTime.ParseExact(txtDate.Text+" 23:59:59", "dd/MM/yyyy HH:mm:ss",null);
					}
				}
			}
			
			#endregion

			#region "Payer Type"

			///------------boon start
			string strCustPayerType = "";

			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
			{
				if(lsbCustType.Items[i].Selected == true)
				{
					strCustPayerType += "\"" + lsbCustType.Items[i].Value + "\",";
				}
			}

			if (strCustPayerType != "") 
				dr["payer_type"] = "(" + strCustPayerType.Substring(0,strCustPayerType.Length - 1) + ")";
			else
				dr["payer_type"] = System.DBNull.Value;			
			///--------------boon end

			if ((String)ViewState["usertype"]  == "C")

				dr["payer_code"] = (String)ViewState["PayerID"];
			else
				dr["payer_code"] = txtPayerCode.Text.Trim();

			#endregion
			
			#region "Route / DC Selection"

			dr["route_code"] = DbComboPathCode.Value;
			dr["destination_dc"] = DbComboDestinationDC.Value;
			
			#endregion

			#region "Destination"
			//by sittichai  23/01/2008
			if((String)ViewState["usertype"] != "C")
			{
				dr["zip_code"] = txtZipCode.Text.Trim();
				dr["state_code"] = txtStateCode.Text.Trim();
			}
			#endregion

			#region "Report To Show"

			if ((String)ViewState["usertype"] != "C")
			{
				dr["RptType"] = "N";
			}
			else
			{
				dr["RptType"] = "C";
			}

			#endregion

			dtShipment.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipment);
			return dsShipment;
		}


		private void DefaultScreen()
		{
			#region "Dates"

			rbBookingDate.Checked = true;
			rbMonth.Checked = true;
			rbPeriod.Checked = false;
			rbDate.Checked = false;


			ddMonth.Enabled = true;
			ddMonth.SelectedIndex = -1;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;

			#endregion

			#region "Payer Type"

			txtPayerCode.Text = null;
			lsbCustType.SelectedIndex = -1;		//boon 2011/01/19

			#endregion

			#region "Route / DC Selection"

			DbComboPathCode.Text="";
			DbComboPathCode.Value="";

			DbComboDestinationDC.Text="";
			DbComboDestinationDC.Value="";

			#endregion

			#region "Destination"

			txtZipCode.Text = null;
			txtStateCode.Text = null;

			#endregion
			
			lblErrorMessage.Text = "";
		}


		private bool ValidateValues()
		{
			bool iCheck=false;

			if((String)ViewState["usertype"] != "C")
			{
	
				if((ddMonth.SelectedIndex==0)&&(txtYear.Text=="")&&(txtPeriod.Text=="")&&(txtTo.Text=="")&&(txtDate.Text==""))
				{				
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVL",utility.GetUserCulture());
					return iCheck=true;
			
				}
				if(rbMonth.Checked == true)
				{
					if((ddMonth.SelectedIndex>0) &&(txtYear.Text==""))
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
						return iCheck=true;
					}
					else if((ddMonth.SelectedIndex==0) &&(txtYear.Text!=""))
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
						return iCheck=true;
					}
					else
					{
						return iCheck=false;
					}
				}
				if(rbPeriod.Checked == true )
				{
					if((txtPeriod.Text!="")&&(txtTo.Text==""))
					{					
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_END_DT",utility.GetUserCulture());
						return iCheck=true;
					}
					else if((txtPeriod.Text=="")&&(txtTo.Text!=""))
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_START_DT",utility.GetUserCulture());					
						return iCheck=true;			
					}
					else
					{
						return iCheck=false;
					}
				}
				if(rbDate.Checked == true )
				{
					if(txtDate.Text=="")
					{					
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT",utility.GetUserCulture());	
						return iCheck=true;				
					}
				}			
			}
			return iCheck;	
		}

		private int ValidateData()
		{
			if (txtYear.Text != "" )
			{
				if (Convert.ToInt32(txtYear.Text) < 2000 || Convert.ToInt32(txtYear.Text) > 2099)
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
					return -1;
				}
			}
			return 1;
		}
		

		private int LastDayOfMonth(String strMonth, String strYear)
		{
			int intLastDay = 0;
			switch (strMonth)
			{
				case "01":
				case "03":
				case "05":
				case "07":
				case "08":
				case "10":
				case "12":
					intLastDay = 31;
					break;
				case "04":
				case "06":
				case "09":
				case "11":
					intLastDay = 30;
					break;
				case "02":
					//intLastDay = 28;
					//by sittichai 18/02/2008
					DateTime dt = System.DateTime.ParseExact("01/02/"+strYear,"dd/MM/yyyy",null);
					DateTime firstDayOfThisMonth = dt.Subtract(TimeSpan.FromDays(dt.Day - 1));
					DateTime firstDayOfNextMonth = firstDayOfThisMonth.AddMonths(1);
					DateTime lastDayOfThisMonth = firstDayOfNextMonth.Subtract(TimeSpan.FromDays(1)) ;

					intLastDay = Int32.Parse(lastDayOfThisMonth.Day.ToString());
					break;
			}
			return intLastDay;
		}


		private void OpenWindowpage(String strUrl, String strFeatures)
		{
			
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion

		#region "Dates Part : Controls"

		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = true;
		}

		#endregion

		#region "Payer Type Part : Controls

		private void btnPayerCode_Click(object sender, System.EventArgs e)
		{

			//-------boon start
			string strCustPayerType = "";
			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
			{
				if(lsbCustType.Items[i].Selected == true)
				{
					strCustPayerType += lsbCustType.Items[i].Value + ",";
				}
			}
			//-------boon end

			if((strCustPayerType != null) && (strCustPayerType.Trim().Length > 0))
				strCustPayerType = strCustPayerType.Substring(0, strCustPayerType.Length - 1);

			String sUrl = "CustomerPopup.aspx?FORMID="+"SalesTerritoryReportQuery"+
				"&CustType="+strCustPayerType.ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion

		#region "Route / DC Selection : Controls"

		private void rbLongRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}


		private void rbShortRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";	
		}


		private void rbAirRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}


		#endregion

		#region "Destination : Controls"

		private void btnZipCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "ZipCodePopup.aspx?FORMID=SalesTerritoryReportQuery"+"&ZIPCODE_CID="+txtZipCode.ClientID+"&ZIPCODE="+txtZipCode.Text.ToString().Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		private void btnStateCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "StatePopup.aspx?FORMID=SalesTerritoryReportQuery"+"&STATECODE="+txtStateCode.ClientID+"&STATECODE_TEXT="+txtStateCode.Text.ToString().Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		#endregion

		#region "Dates Part : DropDownList"

		private void ddlmonths()
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);

			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}

		private void ddlmonthscust()
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);
		}


		#endregion

		#region "Payer Type Part : DropDownList"

		public void LoadCustomerTypeList()
		{
			ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"customer_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				lsbCustType.Items.Add(lstItem);
			}
		}

		#endregion

		#region "Route / DC Selection : DropDownList"

		private void SetDbComboServerStates()
		{
			String strDeliveryType=null;
			if (rbShortRoute.Checked==true)
				strDeliveryType="S";

			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
			DbComboPathCode.ServerState = hash;
			DbComboPathCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"'";
					if(strDelType == "S") 
					{
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct delivery_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
				}				
			}
			
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboDistributionCenterSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,args);	
			return dataset;
		}


		#endregion

		#region "Sale : DropDownList"

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object SalesmanQuery(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.SalesmanQuery(strAppID,strEnterpriseID,args);	
			return dataset;
		}

		#endregion
		
		#region "Service Type : DropDownList"

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object ServiceCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strServiceCode="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strServiceCode"] != null && args.ServerState["strServiceCode"].ToString().Length > 0)
				{
					strServiceCode = args.ServerState["strServiceCode"].ToString();					
				}
			}	
			DataSet dataset = com.ties.classes.DbComboDAL.ServiceCodeQuery (strAppID,strEnterpriseID,args);	
			return dataset;                
		}

		#endregion	

		private void rbPickUpDate_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void rbBookingDate_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void ddMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

	}
}