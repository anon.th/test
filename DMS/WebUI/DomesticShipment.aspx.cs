using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.ties.BAL;
using com.common.applicationpages;
using System.Text;
using System.Globalization;
using TIESDAL;

namespace com.ties
{
	/// <summary>
	/// Summary description for DomesticShipment.
	/// </summary>
	public class DomesticShipment : BasePage
	{
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Label lblBookingNo;
		protected com.common.util.msTextBox txtBookingNo;
		protected System.Web.UI.WebControls.Label lblConsgmtNo;
		protected System.Web.UI.WebControls.Label lblBookDate;
		protected System.Web.UI.WebControls.Label lblRefNo;
		protected System.Web.UI.WebControls.Label lblOrigin;
		protected System.Web.UI.WebControls.Label lblDestination;
		protected System.Web.UI.WebControls.TextBox txtRefNo;
		protected System.Web.UI.WebControls.TextBox txtOrigin;
		protected System.Web.UI.WebControls.TextBox txtDestination;
		protected System.Web.UI.WebControls.Label lblShipManifestDt;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected com.common.util.msTextBox txtShipManifestDt;
		protected System.Web.UI.WebControls.TextBox txtRouteCode;
		protected System.Web.UI.WebControls.Label lblStatusCode;
		protected System.Web.UI.WebControls.Label lblExcepCode;
		protected System.Web.UI.WebControls.Label lblDelManifest;
		protected System.Web.UI.WebControls.Button btnRouteCode;
		protected System.Web.UI.WebControls.Label lblCustType;
		protected System.Web.UI.WebControls.Label lblCustID;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.DropDownList ddbCustType;
		protected com.common.util.msTextBox txtCustID;
		protected System.Web.UI.WebControls.TextBox txtCustName;
		protected System.Web.UI.WebControls.TextBox txtCustAdd1;
		protected System.Web.UI.WebControls.TextBox txtCustAdd2;
		protected System.Web.UI.WebControls.TextBox txtCity;
		protected System.Web.UI.WebControls.TextBox txtStateCode;
		protected System.Web.UI.HtmlControls.HtmlTable TblBookDtl;
		protected com.common.util.msTextBox txtBookDate;
		protected System.Web.UI.WebControls.TextBox txtDelManifest;
		protected System.Web.UI.WebControls.Label lblPaymode;
		protected System.Web.UI.WebControls.Label lblTelphone;
		protected System.Web.UI.WebControls.Label lblFax;
		protected System.Web.UI.WebControls.RadioButton rbCash;
		protected System.Web.UI.WebControls.RadioButton rbCredit;
		protected System.Web.UI.WebControls.TextBox txtCustTelephone;
		protected System.Web.UI.WebControls.TextBox txtCustFax;
		protected System.Web.UI.WebControls.Label lblZip;
		protected System.Web.UI.WebControls.CheckBox chkRecip;
		protected System.Web.UI.WebControls.Label lblRec;
		protected System.Web.UI.WebControls.TextBox txtRecName;
		protected System.Web.UI.WebControls.TextBox txtRecpContPer;
		protected System.Web.UI.WebControls.Label lblAddress;
		protected System.Web.UI.WebControls.Label lblRecipAddr1;
		protected System.Web.UI.WebControls.TextBox txtRecipAddr2;
		protected System.Web.UI.WebControls.Label lblRecipTelephone;
		protected System.Web.UI.WebControls.TextBox txtRecipTel;
		protected System.Web.UI.WebControls.Button btnRecipNm;
		protected System.Web.UI.WebControls.Label lblRecpContPer;
		protected System.Web.UI.WebControls.TextBox txtRecipAddr1;
		protected System.Web.UI.WebControls.Label lblRecipFax;
		protected System.Web.UI.WebControls.TextBox txtRecipFax;
		protected System.Web.UI.WebControls.Label lblRecipZip;
		protected System.Web.UI.WebControls.TextBox txtRecipZip;
		protected System.Web.UI.WebControls.Label lblRecipNm;
		protected System.Web.UI.WebControls.TextBox txtRecipCity;
		protected System.Web.UI.WebControls.TextBox txtRecipState;
		protected System.Web.UI.WebControls.Label lblSendCust;
		protected System.Web.UI.WebControls.Label lblSendNm;
		protected System.Web.UI.WebControls.TextBox txtSendName;
		protected System.Web.UI.WebControls.Button btnSendCust;
		protected System.Web.UI.WebControls.Label lblSendConPer;
		protected System.Web.UI.WebControls.TextBox txtSendContPer;
		protected System.Web.UI.WebControls.Label lblSendAddr1;
		protected System.Web.UI.WebControls.TextBox txtSendAddr1;
		protected System.Web.UI.WebControls.Label lblSendTel;
		protected System.Web.UI.WebControls.TextBox txtSendTel;
		protected System.Web.UI.WebControls.TextBox txtSendAddr2;
		protected System.Web.UI.WebControls.Label lblSendFax;
		protected System.Web.UI.WebControls.TextBox txtSendFax;
		protected System.Web.UI.WebControls.Label lblSendZip;
		protected System.Web.UI.WebControls.TextBox txtSendZip;
		protected System.Web.UI.WebControls.TextBox txtSendState;
		protected System.Web.UI.WebControls.Label lblSendCuttOffTime;
		protected com.common.util.msTextBox txtSendCuttOffTime;
		protected System.Web.UI.WebControls.Label lblPkgActualWt;
		protected System.Web.UI.WebControls.TextBox txtPkgActualWt;
		protected System.Web.UI.WebControls.Label lblPkgCommCode;
		protected System.Web.UI.WebControls.TextBox txtPkgCommCode;
		protected System.Web.UI.WebControls.Label lblPkgDimWt;
		protected System.Web.UI.WebControls.TextBox txtPkgDimWt;
		protected System.Web.UI.WebControls.TextBox txtPkgCommDesc;
		protected System.Web.UI.WebControls.Label lblPkgTotPkgs;
		protected System.Web.UI.WebControls.TextBox txtPkgTotpkgs;
		protected System.Web.UI.WebControls.Label lblPkgChargeWt;
		protected System.Web.UI.WebControls.TextBox txtPkgChargeWt;
		protected System.Web.UI.WebControls.Label lblShipSerCode;
		protected com.common.util.msTextBox txtShpSvcCode;
		protected System.Web.UI.WebControls.Button btnShpSvcCode;
		protected System.Web.UI.WebControls.TextBox txtShpSvcDesc;
		protected System.Web.UI.WebControls.Label lblShipPickUpTime;
		protected System.Web.UI.WebControls.TextBox txtShipPckUpTime;
		protected System.Web.UI.WebControls.Label lblShipDclValue;
		protected System.Web.UI.WebControls.TextBox txtShpDclrValue;
		protected System.Web.UI.WebControls.TextBox txtShipEstDlvryDt;
		protected System.Web.UI.WebControls.Label lblShipAdpercDV;
		protected System.Web.UI.WebControls.TextBox txtShpAddDV;
		protected System.Web.UI.WebControls.Label lblShipMaxCovg;
		protected System.Web.UI.WebControls.TextBox txtShpMaxCvrg;
		protected System.Web.UI.WebControls.Label lblShipInsSurchrg;
		protected System.Web.UI.WebControls.TextBox txtShpInsSurchrg;
		protected System.Web.UI.WebControls.CheckBox chkshpRtnHrdCpy;
		protected System.Web.UI.WebControls.RadioButton rbtnShipFrghtPre;
		protected System.Web.UI.WebControls.RadioButton rbtnShipFrghtColl;
		protected System.Web.UI.WebControls.DataGrid dgVAS;
		protected System.Web.UI.WebControls.Label lblFreightChrg;
		protected System.Web.UI.WebControls.TextBox txtFreightChrg;
		protected System.Web.UI.WebControls.Label lblInsChrg;
		protected System.Web.UI.WebControls.TextBox txtInsChrg;
		protected System.Web.UI.WebControls.Label lblTotVASSurChrg;
		protected System.Web.UI.WebControls.TextBox txtTotVASSurChrg;
		protected System.Web.UI.WebControls.Label lblShpTotAmt;
		protected System.Web.UI.WebControls.TextBox txtShpTotAmt;
		protected System.Web.UI.WebControls.CheckBox chkSendCustInfo;
		protected System.Web.UI.WebControls.Button btnPkgCommCode;
		protected System.Web.UI.WebControls.Button btnQry;
		//Utility utility = null;
		String appID = null;
		String enterpriseID = null;
		String userID = null;
		protected System.Web.UI.WebControls.TextBox txtCustZipCode;
		protected System.Web.UI.WebControls.Button btnGo;
		protected System.Web.UI.WebControls.TextBox txtLatestStatusCode;
		protected System.Web.UI.WebControls.TextBox txtLatestExcepCode;
		protected System.Web.UI.WebControls.Button btnPkgDetails;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.RequiredFieldValidator validCustID;
		protected System.Web.UI.WebControls.RequiredFieldValidator validCustZip;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.WebControls.RequiredFieldValidator validSendName;
		protected System.Web.UI.WebControls.RequiredFieldValidator validSendZip;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.WebControls.RequiredFieldValidator validRecipName;
		protected System.Web.UI.WebControls.RequiredFieldValidator validRecipZip;
		protected System.Web.UI.WebControls.Button btnDisplayCustDtls;
		protected System.Web.UI.WebControls.Button btnDGInsert;
		protected System.Web.UI.WebControls.TextBox txtCustStateCode;
		DataSet m_dsVAS = null;
		protected System.Web.UI.WebControls.RequiredFieldValidator validSrvcCode;
		protected System.Web.UI.WebControls.CheckBox chkNewCust;
		private String strErrorMsg = null;
		protected System.Web.UI.WebControls.RequiredFieldValidator validConsgNo;
		protected System.Web.UI.WebControls.RequiredFieldValidator validDclrValue;
		protected System.Web.UI.WebControls.Button btnToCancel;
		protected System.Web.UI.WebControls.Button btnNotToSave;
		protected System.Web.UI.WebControls.Button btnToSaveChanges;
		protected System.Web.UI.WebControls.Label lblConfirmMsg;
		protected System.Web.UI.WebControls.Button btnPopulateVAS;
		protected System.Web.UI.WebControls.Button btnBind;
		DataSet m_dsPkgDetails = null;
		protected System.Web.UI.WebControls.Label lblShipEstDlvryDt;
		DomesticShipmentMgrBAL domesticMgrBAL = null;
		SessionDS m_sdsDomesticShip = null;
		static int m_iSetSize = 10;
		protected System.Web.UI.WebControls.Label lblESASurchrg;
		protected System.Web.UI.WebControls.RequiredFieldValidator validCustType;
		protected System.Web.UI.WebControls.RequiredFieldValidator validSenderAdd1;
		protected System.Web.UI.WebControls.RequiredFieldValidator validRecipAdd1;
		protected System.Web.UI.WebControls.RequiredFieldValidator validCustName;
		protected System.Web.UI.WebControls.RequiredFieldValidator validCustAdd1;
		protected System.Web.UI.WebControls.RegularExpressionValidator Regularpickupdate;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularEstDlvryDt;
		protected System.Web.UI.WebControls.RegularExpressionValidator RegularShpManfstDt;
		protected System.Web.UI.WebControls.TextBox txtESASurchrg;
		protected String m_sScript = "";
		protected System.Web.UI.WebControls.TextBox txtActDelDt;
		protected System.Web.UI.WebControls.TextBox txtSpeHanInstruction;
		protected System.Web.UI.WebControls.Label lblSpeHanInstruction;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.Label lblCustIdCngMsg;
		protected System.Web.UI.WebControls.Button btnCustIdChgYes;
		protected System.Web.UI.WebControls.Button btnCustIdChgNo;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divCustIdCng;
		protected System.Web.UI.HtmlControls.HtmlGenericControl DomstcShipPanel;
		protected System.Web.UI.WebControls.Label lblDelOperation;
		protected System.Web.UI.WebControls.Button btnDelOperationYes;
		protected System.Web.UI.WebControls.Button btnDelOperationNo;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divDelOperation;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlTable tblCustInfo;
		protected System.Web.UI.HtmlControls.HtmlTable tblSenderInfo;
		protected System.Web.UI.HtmlControls.HtmlTable tblRecepInfo;
		protected System.Web.UI.HtmlControls.HtmlTable tblPkgInfo;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipService;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipHandling;
		protected System.Web.UI.HtmlControls.HtmlTable tblCharges;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.TextBox txtGoToRec;
		protected System.Web.UI.WebControls.Label lblCustInfo;
		protected System.Web.UI.WebControls.Label lblSendInfo;
		protected System.Web.UI.WebControls.Label lblRecInfo;
		protected System.Web.UI.WebControls.Label lblPkgInfo;
		protected System.Web.UI.WebControls.Label lblShpService;
		protected System.Web.UI.WebControls.Label shpHanChr;
		protected System.Web.UI.WebControls.Label lblConf;
		protected System.Web.UI.HtmlControls.HtmlTable Table3;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divUpdateAutoManifest;
		protected System.Web.UI.HtmlControls.HtmlTable Table4;
		protected System.Web.UI.WebControls.Button btnUpdateAutoManifest;
		protected System.Web.UI.WebControls.TextBox Textbox1;
		protected System.Web.UI.WebControls.Label lblCODAmount;
		protected com.common.util.msTextBox txtCODAmount;
		//protected com.common.util.msTextBox txtCODAmount;
		protected System.Web.UI.WebControls.RequiredFieldValidator Requiredfieldvalidator1;
		protected System.Web.UI.WebControls.TextBox txtConsigNo;
		protected System.Web.UI.WebControls.Button btnTelephonePopup;
		protected System.Web.UI.WebControls.CheckBox chkInvHCReturn;
		protected System.Web.UI.WebControls.TextBox Textbox2;
		protected System.Web.UI.WebControls.TextBox Textbox3;
		protected System.Web.UI.WebControls.TextBox txtEstHCDt;
		protected System.Web.UI.WebControls.TextBox txtActHCDt;
		//protected System.Web.UI.WebControls.RequiredFieldValidator validCODAmount;
		//public string strScrollPosition;
		protected System.Web.UI.WebControls.Button m_btnClicked = null;
		//private int m_StartIndex=0;
		//private int wt_rounding_method = 0;
		protected System.Web.UI.WebControls.Label Label5;
		//private decimal wt_increment_amt = 1;
		protected System.Web.UI.WebControls.TextBox txtOtherSurcharge;
		protected System.Web.UI.WebControls.Label lblOtherSurcharge;
		protected System.Web.UI.WebControls.TextBox txtOtherSurcharge2;
		protected System.Web.UI.WebControls.Label lblPODEX;
		protected System.Web.UI.WebControls.TextBox txtPODEX;
		protected System.Web.UI.WebControls.Label lblMBGAmt;
		protected System.Web.UI.WebControls.TextBox txtMBGAmt;
		protected System.Web.UI.WebControls.Label lblOtherInvC_D;
		protected System.Web.UI.WebControls.TextBox txtOtherInvC_D;
		protected System.Web.UI.WebControls.Label lblTotalInvAmt;
		protected System.Web.UI.WebControls.TextBox txtTotalInvAmt;
		protected System.Web.UI.WebControls.Label lblCreditNotes;
		protected System.Web.UI.WebControls.TextBox txtCreditNotes;
		protected System.Web.UI.WebControls.Label lblDebitNotes;
		protected System.Web.UI.WebControls.TextBox txtDebitNotes;
		protected System.Web.UI.WebControls.Label lblTotalConRevenue;
		protected System.Web.UI.WebControls.TextBox txtTotalConRevenue;
		protected System.Web.UI.WebControls.TextBox txtOriFreightChrg;
		protected System.Web.UI.WebControls.TextBox txtOriInsChrg;
		protected System.Web.UI.WebControls.TextBox txtOriESASurchrg;
		protected System.Web.UI.WebControls.TextBox txtOriShpTotAmt;
		protected System.Web.UI.WebControls.TextBox txtOriOthersurcharge2;
		protected System.Web.UI.WebControls.Label lblRateOverrideBy;
		protected System.Web.UI.WebControls.Label lblRateOverrideByValue;
		protected System.Web.UI.WebControls.Label lblRateOverrideDate;
		protected System.Web.UI.WebControls.Label lblRateOverrideDateValue;
		
		//by Aoo
		protected string strOtherSurcharge;
		
		protected string strCusPerSurcharge;
		protected string strCusFree_Coverage;
		protected string strCusMax_Coverage;
		protected string strCusOtherSurchargeAmt;

		protected string strEntPerSurcharge;
		protected string strEntFree_Coverage;
		protected string strEntMax_Coverage;
		int iDSMode = 0;
		int iDSOperation = 4;

		String strDt="";

		protected string strUsePerSurcharge;
		protected string strUseFree_Coverage;
		protected System.Web.UI.WebControls.Label lblOritotVas;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.TextBox txtActualWeight;
		protected System.Web.UI.WebControls.TextBox txtSendCity;
		protected System.Web.UI.WebControls.TextBox txtCustCity;
		protected string strUseMax_Coverage;
		//private SessionDS m_sdsDCenter;
		//private bool isInitialBooking;
		protected System.Web.UI.WebControls.Label lblTotalVol;
		protected System.Web.UI.WebControls.TextBox txttotVol;
		protected System.Web.UI.WebControls.TextBox txtPaidRevenue;
		protected System.Web.UI.WebControls.Label lblPaidRevenue;
		protected System.Web.UI.WebControls.Label lblPD_Replace_Date;
		protected System.Web.UI.WebControls.Button btnViewOldPD;
		protected System.Web.UI.WebControls.Button btnClientEvent;
		protected System.Web.UI.HtmlControls.HtmlInputButton hddConNo;
		protected System.Web.UI.WebControls.Label lbl_ExceedTimeState;
		protected System.Web.UI.WebControls.Button btnExceedCancel;
		protected System.Web.UI.WebControls.Button btnExceedOK;
		protected System.Web.UI.HtmlControls.HtmlGenericControl ExceedTimeState;
		protected System.Web.UI.WebControls.RegularExpressionValidator Regularexpressionvalidator1;
		protected System.Web.UI.WebControls.Label lblEstHCDt;
		protected System.Web.UI.WebControls.Label lblActHCDt;
		private bool fromInsertBtn = true;
		//End by Aoo


		private enum DeliveryDateType
		{
			Today,
			Tomorrow,
			Saturday,
			Sunday,
			Holiday
		}
		private DeliveryDateType EstimatedDeliveryDateType
		{
			get
			{
				if(ViewState["EstimatedDeliveryDateType"] ==null)
					return DeliveryDateType.Today;
				else
					return (DeliveryDateType)ViewState["EstimatedDeliveryDateType"];
			}
			set
			{
				ViewState["EstimatedDeliveryDateType"] = value;
			}
		}
		private DataTable EstimatedDeliveryDateData
		{
			get
			{
				if(ViewState["EstimatedDeliveryDateData"] ==null)
					return null;
				else
					return (DataTable)ViewState["EstimatedDeliveryDateData"];
			}
			set
			{
				ViewState["EstimatedDeliveryDateData"] = value;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();			

			rbtnShipFrghtPre.Checked = true; 
			rbtnShipFrghtColl.Enabled = false;

			#region Block Control By Script

			// JavaScript Block Text  By Aoo
			txtRecipTel.Attributes.Add("onkeypress","if (window.event.keyCode >= 48 && window.event.keyCode <= 57) return ture; else return false;");
			txtPkgActualWt.Attributes.Add("onkeypress","return false;");
			txtPkgTotpkgs.Attributes.Add("onkeypress","return false;");
			txtPkgDimWt.Attributes.Add("onkeypress","return false;");
			txtPkgChargeWt.Attributes.Add("onkeypress","return false;");
			txtPkgCommDesc.Attributes.Add("onkeypress","return false;");
			txtOtherSurcharge.Attributes.Add("onkeypress","return false;");
			txtOtherSurcharge2.Attributes.Add("onkeypress","return false;");
			txtActualWeight.Attributes.Add("onkeypress","return false;");	//TU on 17June08

			txtPkgActualWt.Attributes.Add("onkeydown","return false;");
			txtPkgTotpkgs.Attributes.Add("onkeydown","return false;");
			txtPkgDimWt.Attributes.Add("onkeydown","return false;");
			txtPkgChargeWt.Attributes.Add("onkeydown","return false;");
			txtPkgCommDesc.Attributes.Add("onkeydown","return false;");
			txtOtherSurcharge.Attributes.Add("onkeydown","return false;");
			txtOtherSurcharge2.Attributes.Add("onkeydown","return false;");
			txtActualWeight.Attributes.Add("onkeydown","return false;");	//TU on 17June08


			txtPkgActualWt.Attributes.Add("onkeyup","return false;");
			txtPkgTotpkgs.Attributes.Add("onkeyup","return false;");
			txtPkgDimWt.Attributes.Add("onkeyup","return false;");
			txtPkgChargeWt.Attributes.Add("onkeyup","return false;");
			txtPkgCommDesc.Attributes.Add("onkeyup","return false;");
			txtOtherSurcharge.Attributes.Add("onkeyup","return false;");
			txtOtherSurcharge2.Attributes.Add("onkeyup","return false;");
			txtActualWeight.Attributes.Add("onkeyup","return false;");	//TU on 17June08


			txtPkgActualWt.Attributes.Add("onpaste","return false;");
			txtPkgTotpkgs.Attributes.Add("onpaste","return false;");
			txtPkgDimWt.Attributes.Add("onpaste","return false;");
			txtPkgChargeWt.Attributes.Add("onpaste","return false;");
			txtPkgCommDesc.Attributes.Add("onpaste","return false;");
			txtOtherSurcharge.Attributes.Add("onpaste","return false;");
			txtOtherSurcharge2.Attributes.Add("onpaste","return false;");
			txtActualWeight.Attributes.Add("onpaste","return false;");	//TU on 17June08


			//End

			#endregion
		
			if (!IsPostBack)
			{
				DataSet dsEprise = SysDataManager1.GetEnterpriseProfile(appID,enterpriseID);
				if(dsEprise.Tables[0].Rows.Count > 0 && dsEprise.Tables[0].Rows[0]["currency"].ToString()!="")
				{
					lblShipDclValue.Text="Declared Value " + dsEprise.Tables[0].Rows[0]["currency"].ToString();				
				}
				//by Tumz
				//				ScreenMode eNumScreenModeCheckForCompare = ScreenMode.None;
				//				ViewState["eNumScreenModeCheckForCompare"] = eNumScreenModeCheckForCompare;

				ViewState["PartialCon"]=false;
				ViewState["PopUpRecipZip"]=false;
				//btnPkgCommCode.Attributes.Add("onclick","return CallCommodityWin()");
				//PageValidationSummary.HeaderText = "Pls enter the following mandatory field";
				PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
				validCustID.ErrorMessage = Utility.GetLanguageText(ResourceType.ScreenLabel, "Cust ID", utility.GetUserCulture());
				validCustZip.ErrorMessage = Utility.GetLanguageText(ResourceType.ScreenLabel, "Customer Postal Code", utility.GetUserCulture());
				validSendZip.ErrorMessage = Utility.GetLanguageText(ResourceType.ScreenLabel, "Sender Postal Code", utility.GetUserCulture());
				validSenderAdd1.ErrorMessage = Utility.GetLanguageText(ResourceType.ScreenLabel, "Sender Address", utility.GetUserCulture());
				validSendName.ErrorMessage = Utility.GetLanguageText(ResourceType.ScreenLabel, "Sender Name", utility.GetUserCulture());
				validRecipName.ErrorMessage = Utility.GetLanguageText(ResourceType.ScreenLabel, "Recipient Name", utility.GetUserCulture());
				validRecipZip.ErrorMessage = Utility.GetLanguageText(ResourceType.ScreenLabel, "Recipient Postal Code", utility.GetUserCulture());
				validRecipAdd1.ErrorMessage = Utility.GetLanguageText(ResourceType.ScreenLabel, "Recipient Address1", utility.GetUserCulture());
				//validConsgNo.ErrorMessage = Utility.GetLanguageText(ResourceType.ScreenLabel, "Consignment Number", utility.GetUserCulture());
				//validDclrValue.ErrorMessage = Utility.GetLanguageText(ResourceType.ScreenLabel, "Declare value", utility.GetUserCulture());
				//validCODAmount.ErrorMessage = Utility.GetLanguageText(ResourceType.ScreenLabel, "COD Amount", utility.GetUserCulture());
				validSrvcCode.ErrorMessage = Utility.GetLanguageText(ResourceType.ScreenLabel, "Service Type", utility.GetUserCulture());
				//validCommCode.ErrorMessage = Utility.GetLanguageText(ResourceType.ScreenLabel, "Commodity code", utility.GetUserCulture());
				validCustName.ErrorMessage = Utility.GetLanguageText(ResourceType.ScreenLabel, "Customer Name", utility.GetUserCulture());
				validCustAdd1.ErrorMessage = Utility.GetLanguageText(ResourceType.ScreenLabel, "Customer Address", utility.GetUserCulture());
				Regularpickupdate.ErrorMessage = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_PCKDF", utility.GetUserCulture());//"Enter the pickup date in format dd/MM/yyyy HH:mm";
				RegularEstDlvryDt.ErrorMessage = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_ESTDF", utility.GetUserCulture());//"Enter the estimated delivery date in format dd/MM/yyyy HH:mm";
				RegularShpManfstDt.ErrorMessage = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_SHMDF", utility.GetUserCulture());//"Enter the Shipment Manifest date in format dd/MM/yyyy HH:mm";

				m_dsVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
				Session["SESSION_DS1"] = m_dsVAS;
				BindVASGrid();
				m_dsPkgDetails=DomesticShipmentMgrDAL.GetEmptyPkgDtlDS();
				Session["SESSION_DS2"] = m_dsPkgDetails;
				BindPKGGrid();
				rbtnShipFrghtPre.Enabled = false;
				rbtnShipFrghtColl.Enabled = false;
				LoadCustomerTypeList();
				btnGoToFirstPage.Enabled = false;
				btnGoToLastPage.Enabled = false;
				btnNextPage.Enabled = false;
				btnPreviousPage.Enabled = false;
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				btnQry.Enabled = true;
				Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,false,m_moduleAccessRights);
				Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete,false,m_moduleAccessRights);
				btnExecQry.Enabled = true;
				chkNewCust.Checked = false;
				chkSendCustInfo.Checked = false;
				chkRecip.Checked = false;
				rbCredit.Checked = true;
				chkshpRtnHrdCpy.Checked = false;

				//HC Return Task
				chkInvHCReturn.Checked = false;
				//HC Return Task

				//Flag used for checking if it is holiday/weekend extra surcharge will be imposed.
				ViewState["AddSurcharge"] = "none";
				//Saving the VAS Code the holiday/weekend service
				ViewState["ServiceVASCode"] = "";
				ViewState["isHoliday"] = false;
				ViewState["isTextChanged"] = false;
				ViewState["DSMode"] = ScreenMode.None;
				ViewState["DSOperation"] = Operation.None;
				Session["SESSION_DS3"] = m_sdsDomesticShip;
				ViewState["CurrentSetSize"]=0;
				ViewState["Operations"] = Operation.None;
				ViewState["OperationsVAS"] = Operation.None;  //Jeab 09 Mar 11
				ViewState["Day"] = "";
				ViewState["ServiceExists"] = false;
				ViewState["NextOperation"] = "None";
				//On load default screen to Query Mode
				//ViewState["DSMode"] = ScreenMode.Query;
				btnPkgDetails.Enabled = false;
				ResetForQuery();
				//Flag for retrieval of packages from pickup.
				ViewState["PickupBooking"]="No";
				txtShpSvcCode.ReadOnly = false;
				txtRecipZip.ReadOnly = false;
				divCustIdCng.Visible=false;
				DomstcShipPanel.Visible=false;
				divDelOperation.Visible=false;
				divUpdateAutoManifest.Visible=false;
				ExceedTimeState.Visible=false;
				//########## Automatically Assing to Manifests (DMS Phase 1) ##########

				ViewState["IsNeedAutoManifest"] = false;

				//########## Automatically Assing to Manifests (DMS Phase 1) ##########

				DataSet profileDS = SysDataManager1.GetEnterpriseProfile(appID, enterpriseID);
				Session["DMSENTPROF"] = profileDS;
				if(profileDS.Tables[0].Rows.Count > 0)
				{
					int currency_decimal = (int) profileDS.Tables[0].Rows[0]["currency_decimal"];
					ViewState["m_format"] = "{0:F" + currency_decimal.ToString() + "}";
					
					if  ( profileDS.Tables[0].Rows[0]["wt_rounding_method"] != System.DBNull.Value)
					{
						
						ViewState["wt_rounding_method"] = Convert.ToInt32(profileDS.Tables[0].Rows[0]["wt_rounding_method"]);
					}

					if  ( profileDS.Tables[0].Rows[0]["wt_increment_amt"] != System.DBNull.Value)
					{
						ViewState["wt_increment_amt"] = Convert.ToDecimal(profileDS.Tables[0].Rows[0]["wt_increment_amt"]);
					}
				}

				//	btnDisplayCustDtls.Attributes.Add("onclick","ShowPupup()");
				//	Utility.RegisterScriptFile("DSCustomerAgent.js","CustomerAgentPopupDisplay",this.Page);

				SetOriginalCtrl(false); //add new 12:04 3/3/2551
				EnterpriseConfigurations();
			}
			else
			{
				if(Session["dtPRConsignmentVAS"]!=null)
				{
					DataTable dtPRConsignmentVAS = (DataTable)Session["dtPRConsignmentVAS"];
					if(dtPRConsignmentVAS.Rows.Count>0)
					{
						if(!dtPRConsignmentVAS.Columns.Contains("surcharge"))
							dtPRConsignmentVAS.Columns["vas_surcharge"].ColumnName = "surcharge";

						if(!dtPRConsignmentVAS.Columns.Contains("remarks"))
							dtPRConsignmentVAS.Columns.Add("remarks", typeof(bool));

						dtPRConsignmentVAS.TableName = "VAS";
						if(m_dsVAS==null)
						{
							m_dsVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
						}

						foreach(DataRow dr in dtPRConsignmentVAS.Rows)
						{
							m_dsVAS.Tables[0].ImportRow(dr);
						}

					}
					if(m_dsVAS==null)
					{
						m_dsVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
					}
					Session["SESSION_DS1"] = m_dsVAS;
					Session["dtPRConsignmentVAS"]=null;

				}

				m_dsVAS				= (DataSet)Session["SESSION_DS1"];
				m_dsPkgDetails		= (DataSet)Session["SESSION_DS2"];
				m_sdsDomesticShip	= (SessionDS)Session["SESSION_DS3"];	
			}
			string strQuery=null;
			if(Request.Params["hdnRefresh"] != null && Request.Params["hdnRefresh"] == "REFRESH")
			{
				strQuery=Request.Params["hdnRefresh"];
				if((bool)ViewState["isTextChanged"] == false)
				{
					ChangeDSState();
					ViewState["isTextChanged"] = true;
				}
			}
			if(strQuery != null)
			{
				refreshVAS();
			}

			//Removed by CRTS 954 Turn off Locking from over Credit Limit
			//			if(txtCustID.Text.Trim().Length>0)
			//			{
			//				if(CustomerProfileDAL.Check_IsCreditStatusNotAvail(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text.Trim()))
			//				{
			//					this.btnSave.Enabled=false;
			//					this.btnToSaveChanges.Enabled=false;
			//				}
			//			}
			//End by CRTS 954 Turn off Locking from over Credit Limit
			iDSMode = (int)ViewState["DSMode"];
			iDSOperation = (int)ViewState["DSOperation"];

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.hddConNo.ServerClick += new System.EventHandler(this.hddConNo_ServerClick);
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			this.btnClientEvent.Click += new System.EventHandler(this.btnClientEvent_Click);
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.txtBookingNo.TextChanged += new System.EventHandler(this.txtBookingNo_TextChanged);
			this.txtConsigNo.TextChanged += new System.EventHandler(this.txtConsigNo_TextChanged);
			this.txtRefNo.TextChanged += new System.EventHandler(this.txtRefNo_TextChanged);
			this.txtRouteCode.TextChanged += new System.EventHandler(this.txtRouteCode_TextChanged);
			this.btnRouteCode.Click += new System.EventHandler(this.btnRouteCode_Click);
			this.txtCustID.TextChanged += new System.EventHandler(this.txtCustID_TextChanged);
			this.btnDisplayCustDtls.Click += new System.EventHandler(this.btnDisplayCustDtls_Click);
			this.ddbCustType.SelectedIndexChanged += new System.EventHandler(this.ddbCustType_SelectedIndexChanged);
			this.txtCustAdd1.TextChanged += new System.EventHandler(this.txtCustAdd1_TextChanged);
			this.txtCustFax.TextChanged += new System.EventHandler(this.txtCustFax_TextChanged);
			this.txtCustAdd2.TextChanged += new System.EventHandler(this.txtCustAdd2_TextChanged);
			this.chkNewCust.CheckedChanged += new System.EventHandler(this.chkCustInfo_CheckedChanged);
			this.txtCustName.TextChanged += new System.EventHandler(this.txtCustName_TextChanged);
			this.rbCash.CheckedChanged += new System.EventHandler(this.rbCash_CheckedChanged);
			this.rbCredit.CheckedChanged += new System.EventHandler(this.rbCredit_CheckedChanged);
			this.txtCustZipCode.TextChanged += new System.EventHandler(this.txtCustZipCode_TextChanged);
			this.txtCustTelephone.TextChanged += new System.EventHandler(this.txtCustTelephone_TextChanged);
			this.chkSendCustInfo.CheckedChanged += new System.EventHandler(this.chkSendCustInfo_CheckedChanged);
			this.txtSendName.TextChanged += new System.EventHandler(this.txtSendName_TextChanged);
			this.btnSendCust.Click += new System.EventHandler(this.btnSendCust_Click);
			this.txtSendAddr1.TextChanged += new System.EventHandler(this.txtSendAddr1_TextChanged);
			this.txtSendAddr2.TextChanged += new System.EventHandler(this.txtSendAddr2_TextChanged);
			this.txtSendZip.TextChanged += new System.EventHandler(this.txtSendZip_TextChanged);
			this.txtSendContPer.TextChanged += new System.EventHandler(this.txtSendContPer_TextChanged);
			this.txtSendTel.TextChanged += new System.EventHandler(this.txtSendTel_TextChanged);
			this.txtSendFax.TextChanged += new System.EventHandler(this.txtSendFax_TextChanged);
			this.chkRecip.CheckedChanged += new System.EventHandler(this.chkRecip_CheckedChanged);
			this.txtRecipTel.TextChanged += new System.EventHandler(this.txtRecipTel_TextChanged);
			this.btnTelephonePopup.Click += new System.EventHandler(this.btnTelephonePopup_Click);
			this.txtRecName.TextChanged += new System.EventHandler(this.txtRecName_TextChanged);
			this.btnRecipNm.Click += new System.EventHandler(this.btnRecipNm_Click);
			this.txtRecipAddr1.TextChanged += new System.EventHandler(this.txtRecipAddr1_TextChanged);
			this.txtRecipAddr2.TextChanged += new System.EventHandler(this.txtRecipAddr2_TextChanged);
			this.btnPopulateVAS.Click += new System.EventHandler(this.btnPopulateVAS_Click);
			this.txtRecpContPer.TextChanged += new System.EventHandler(this.txtRecpContPer_TextChanged);
			this.txtRecipFax.TextChanged += new System.EventHandler(this.txtRecipFax_TextChanged);
			this.txtRecipZip.TextChanged += new System.EventHandler(this.txtRecipZip_TextChanged);
			this.btnPkgDetails.Click += new System.EventHandler(this.btnPkgDetails_Click);
			this.btnPkgCommCode.Click += new System.EventHandler(this.btnPkgCommCode_Click);
			this.btnViewOldPD.Click += new System.EventHandler(this.btnViewOldPD_Click);
			this.txtShpSvcCode.TextChanged += new System.EventHandler(this.txtShpSvcCode_TextChanged);
			this.btnShpSvcCode.Click += new System.EventHandler(this.btnShpSvcCode_Click);
			this.txtShipPckUpTime.TextChanged += new System.EventHandler(this.txtShipPckUpTime_TextChanged);
			this.txtShpDclrValue.TextChanged += new System.EventHandler(this.txtShpDclrValue_TextChanged);
			this.txtShipEstDlvryDt.TextChanged += new System.EventHandler(this.txtShipEstDlvryDt_TextChanged);
			this.txtCODAmount.TextChanged += new System.EventHandler(this.txtCODAmount_TextChanged);
			this.chkshpRtnHrdCpy.CheckedChanged += new System.EventHandler(this.chkshpRtnHrdCpy_CheckedChanged);
			this.chkInvHCReturn.CheckedChanged += new System.EventHandler(this.chkInvHCReturn_CheckedChanged);
			this.txtSpeHanInstruction.TextChanged += new System.EventHandler(this.txtSpeHanInstruction_TextChanged);
			this.btnDGInsert.Click += new System.EventHandler(this.btnDGInsert_Click);
			this.txtFreightChrg.TextChanged += new System.EventHandler(this.txtFreightChrg_TextChanged);
			this.txtESASurchrg.TextChanged += new System.EventHandler(this.txtESASurchrg_TextChanged);
			this.btnToSaveChanges.Click += new System.EventHandler(this.btnToSaveChanges_Click);
			this.btnNotToSave.Click += new System.EventHandler(this.btnNotToSave_Click);
			this.btnToCancel.Click += new System.EventHandler(this.btnToCancel_Click);
			this.btnDelOperationYes.Click += new System.EventHandler(this.btnDelOperationYes_Click);
			this.btnDelOperationNo.Click += new System.EventHandler(this.btnDelOperationNo_Click);
			this.btnUpdateAutoManifest.Click += new System.EventHandler(this.btnUpdateAutoManifest_Click);
			this.btnExceedCancel.Click += new System.EventHandler(this.btnExceedCancel_Click);
			this.btnExceedOK.Click += new System.EventHandler(this.btnExceedOK_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public void LoadCustomerTypeList()
		{
			ListItem defItem = new ListItem();
			defItem.Text = "";
			defItem.Value = "0";
			ddbCustType.Items.Add(defItem);
			ArrayList systemCodes = Utility.GetCodeValues(appID,utility.GetUserCulture(),"customer_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				ddbCustType.Items.Add(lstItem);
			}
		}

		private void GetValuesIntoDS(int index)
		{
			Zipcode zipCode = new Zipcode();
			strErrorMsg = "";

			DataRow drEach = m_sdsDomesticShip.ds.Tables[0].Rows[index];
			if(txtBookingNo.Text.Trim().Length > 0)
			{
				try
				{
					drEach["booking_no"] = Convert.ToInt64(txtBookingNo.Text.Trim());
				}
				catch(System.FormatException ex)
				{
					lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_BKG_INT",utility.GetUserCulture());
					Console.WriteLine(ex.Message);
					return;
				}
			}
			else
				drEach["booking_no"] = 0;

			if(txtConsigNo.Text.Trim() != "")
			{
				drEach["consignment_no"] = txtConsigNo.Text.Trim() ;
			}

			if(txtRefNo.Text.Trim() != "")
			{
				drEach["ref_no"] = txtRefNo.Text.Trim();
			}

			if(txtBookDate.Text.Trim().Length > 0)
			{
				drEach["booking_datetime"] = System.DateTime.ParseExact(txtBookDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			}

			if(txtCustID.Text.Trim() != "")
			{
				drEach["payerid"] = txtCustID.Text.Trim();
			}

			if(ddbCustType.SelectedItem.Text.ToString() != "")
			{
				drEach["payer_type"] = ddbCustType.SelectedItem.Value.ToString();
			}

			if(chkNewCust.Checked == true)
				drEach["new_account"] = "Y";
			else
				drEach["new_account"] = "N";

			if(txtCustName.Text.Trim() != "")
			{
				drEach["payer_name"] = txtCustName.Text.Trim();
			}

			if(txtCustAdd1.Text.Trim() != "")
			{
				drEach["payer_address1"] = txtCustAdd1.Text.Trim();
			}

			if(txtCustAdd2.Text.Trim() != "")
			{
				drEach["payer_address2"] = txtCustAdd2.Text.Trim();
			}

			if(txtCustZipCode.Text.Trim() != "")
			{
				drEach["payer_zipcode"] = txtCustZipCode.Text.Trim();
			}

			if(txtCustStateCode.Text.Trim() != "")
			{
				drEach["payer_country"] = txtCustStateCode.Text.Trim();
			}

			if(txtCustTelephone.Text.Trim() != "")
			{
				drEach["payer_telephone"] = txtCustTelephone.Text.Trim();
			}

			if(txtCustFax.Text.Trim() != "")
			{
				drEach["payer_fax"] = txtCustFax.Text.Trim();
			}

			if(rbCash.Checked == true)
				drEach["payment_mode"] = "C";
			else if(rbCredit.Checked == true)
				drEach["payment_mode"] = "R";
			
			if(txtSendName.Text.Trim() != "")
			{
				drEach["sender_name"] = txtSendName.Text.Trim();
			}

			if(txtSendAddr1.Text.Trim() != "")
			{
				drEach["sender_address1"] = txtSendAddr1.Text.Trim();
			}

			if(txtSendAddr2.Text.Trim() != "")
			{
				drEach["sender_address2"] = txtSendAddr2.Text.Trim();
			}

			//########## Automatically Assing to Manifests (DMS Phase 1) ##########
			if(txtSendZip.Text.Trim() != "")
			{
				if(((int)ViewState["DSOperation"] == (int)Operation.Update))
				{
					if(drEach["sender_zipcode"] != System.DBNull.Value)
					{
						String tmpNewSendZip = (String)txtSendZip.Text.ToString();
						String tmpOldSendZip = (String)drEach["sender_zipcode"];

						if (tmpNewSendZip != tmpOldSendZip)
							ViewState["IsNeedAutoManifest"] = true;
					}
					else
					{
						ViewState["IsNeedAutoManifest"] = true;
					}
				}

				drEach["sender_zipcode"] = txtSendZip.Text.Trim();
			}
			//########## Automatically Assing to Manifests (DMS Phase 1) ##########

			if(txtSendState.Text.Trim() != "")
			{
				drEach["sender_country"] = txtSendState.Text.Trim();
			}

			if(txtSendTel.Text.Trim() != "")
			{
				drEach["sender_telephone"] = txtSendTel.Text.Trim();
			}

			if(txtSendFax.Text.Trim() != "")
			{
				drEach["sender_fax"] = txtSendFax.Text.Trim();
			}

			if(txtSendContPer.Text.Trim() != "")
			{
				drEach["sender_contact_person"] = txtSendContPer.Text.Trim();
			}

			if(txtRecName.Text.Trim() != "")
			{
				drEach["recipient_name"] = txtRecName.Text.Trim();
			}

			if(txtRecipAddr1.Text.Trim() != "")
			{
				drEach["recipient_address1"] = txtRecipAddr1.Text.Trim();
			}

			if(txtRecipAddr2.Text.Trim() != "")
			{
				drEach["recipient_address2"] = txtRecipAddr2.Text.Trim();
			}

			//########## Automatically Assing to Manifests (DMS Phase 1) ##########
			if(txtRecipZip.Text.Trim() != "")
			{
				if(((int)ViewState["DSOperation"] == (int)Operation.Update))
				{
					if(drEach["recipient_zipcode"] != System.DBNull.Value)
					{
						String tmpNewRecZip = (String)txtRecipZip.Text.ToString();
						String tmpOldRecZip = (String)drEach["recipient_zipcode"];

						if (tmpNewRecZip != tmpOldRecZip)
							ViewState["IsNeedAutoManifest"] = true;
					}
					else
					{
						ViewState["IsNeedAutoManifest"] = true;
					}
				}

				drEach["recipient_zipcode"] = txtRecipZip.Text.Trim();
			}
			//########## Automatically Assing to Manifests (DMS Phase 1) ##########

			if(txtRecipState.Text.Trim() != "")
			{
				drEach["recipient_country"] = txtRecipState.Text.Trim();
			}

			if(txtPkgActualWt.Text.Trim().Length > 0)
			{
				drEach["tot_wt"] = Convert.ToDecimal(txtPkgActualWt.Text.Trim());
			}
			else
			{
				drEach["tot_wt"] = 0;
			}
			//TU on 17June08
			if(txtActualWeight.Text.Trim().Length > 0)
			{
				drEach["tot_act_wt"] = Convert.ToDecimal(txtActualWeight.Text.Trim());
			}
			else
			{
				drEach["tot_act_wt"] = 0;
			}

			if(txtRecipTel.Text.Trim() != "")
			{
				drEach["recipient_telephone"] = txtRecipTel.Text.Trim();
			}

			if(txtPkgDimWt.Text.Trim().Length > 0)
			{
				drEach["tot_dim_wt"] = Convert.ToDecimal(txtPkgDimWt.Text.Trim());
			}
			else
			{
				drEach["tot_dim_wt"] = 0;
			}
			if(txtRecipFax.Text.Trim() != "")
			{
				drEach["recipient_fax"] = txtRecipFax.Text.Trim();
			}
			if(txtPkgTotpkgs.Text.Trim().Length > 0)
			{
				drEach["tot_pkg"] = Convert.ToDecimal(txtPkgTotpkgs.Text.Trim());
			}
			else
			{
				drEach["tot_pkg"] = 0;
			}

			////////////////////////////// Set  to tbl Shipment (edit by Tumz.) /////////////////////////
			String m_strUserID=utility.GetUserID();
			if((ScreenMode)ViewState["DSMode"] != ScreenMode.Insert)
			{
				drEach["pkg_detail_replaced_by"] = m_strUserID;
				drEach["pkg_detail_replaced_datetime"] = DateTime.Now;
			}

			if(txtShpSvcCode.Text.Trim() != "")
			{
				drEach["service_code"] = txtShpSvcCode.Text.Trim();
			}

			if(txtRecpContPer.Text.Trim() != "")
			{
				drEach["recipient_contact_person"] = txtRecpContPer.Text.Trim();
			}

			if(txtShpDclrValue.Text.Trim().Length > 0)
			{
				drEach["declare_value"] = Convert.ToDecimal(txtShpDclrValue.Text.Trim());
			}
			else
			{
				drEach["declare_value"] = 0;
			}

			if(txtCODAmount.Text.Trim().Length > 0)
			{
				drEach["cod_amount"] = Convert.ToDecimal(txtCODAmount.Text.Trim());
			}
			else
			{
				drEach["cod_amount"] = 0;
			}

			if(txtPkgChargeWt.Text.Trim().Length > 0)
			{
				drEach["chargeable_wt"] = Convert.ToDecimal(txtPkgChargeWt.Text.Trim());
			}
			else
			{
				drEach["chargeable_wt"] = 0;
			}
			if(txtShpInsSurchrg.Text.Trim().Length > 0)
			{
				drEach["insurance_surcharge"] = Convert.ToDecimal(txtShpInsSurchrg.Text.Trim());
			}
			else
			{
				drEach["insurance_surcharge"] = 0;
			}
			if(txtShpMaxCvrg.Text.Trim().Length > 0)
			{
				drEach["max_insurance_cover"] = Convert.ToDecimal(txtShpMaxCvrg.Text.Trim());
			}
			else
			{
				drEach["max_insurance_cover"] = 0;
			}
			if(txtShipPckUpTime.Text.Trim().Length > 0)
			{
				drEach["act_pickup_datetime"] = System.DateTime.ParseExact(txtShipPckUpTime.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			}
			if(txtPkgCommCode.Text.Trim() != "")
			{
				drEach["commodity_code"] = txtPkgCommCode.Text.Trim();
			}
			else
			{
				drEach["commodity_code"] = System.DBNull.Value;
			}

			//########## Automatically Assing to Manifests (DMS Phase 1) ##########
			if (txtShipEstDlvryDt.Text.Trim().Length > 0)
			{
				if(((int)ViewState["DSOperation"] == (int)Operation.Update))
				{
					if(drEach["est_delivery_datetime"] != System.DBNull.Value)
					{
						DateTime tmpOldDate = (DateTime)drEach["est_delivery_datetime"];
						DateTime tmpNewDate = System.DateTime.ParseExact(txtShipEstDlvryDt.Text.Trim(),"dd/MM/yyyy HH:mm",null);
						System.TimeSpan ts = tmpOldDate.Subtract(tmpNewDate);

						if (Math.Abs(ts.TotalDays) >= 1)
							ViewState["IsNeedAutoManifest"] = true;
					}
					else
					{
						ViewState["IsNeedAutoManifest"] = true;
					}
				}

				drEach["est_delivery_datetime"] = System.DateTime.ParseExact(txtShipEstDlvryDt.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			}
			//########## Automatically Assing to Manifests (DMS Phase 1) ##########

			if(txtShpAddDV.Text.Trim().Length > 0)
			{
				drEach["percent_dv_additional"] = Convert.ToDecimal(txtShpAddDV.Text.Trim());
			}
			
			//drEach["act_delivery_date"] = DateTime.Now;

			if(rbtnShipFrghtPre.Checked == true)
				drEach["payment_type"] = "FP";
			else if (rbtnShipFrghtColl.Checked == true)
				drEach["payment_type"] = "FC";
			

			if(chkshpRtnHrdCpy.Checked == true)
				drEach["return_pod_slip"] = "Y";
			else if(chkshpRtnHrdCpy.Checked == false)
				drEach["return_pod_slip"] = "N";

			//HC Return Task
			if(chkInvHCReturn.Checked == true)
				drEach["return_invoice_hc"] = "Y";
			else if(chkInvHCReturn.Checked == false)
				drEach["return_invoice_hc"] = "N";

			if ((chkshpRtnHrdCpy.Checked == false) && (chkInvHCReturn.Checked == false))
			{
				drEach["est_hc_return_datetime"] = System.DBNull.Value;
				drEach["act_hc_return_datetime"] = System.DBNull.Value;
			}
			else
			{
				if((drEach["est_delivery_datetime"] != null) && 
					(!drEach["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
					(drEach["est_delivery_datetime"].ToString() != ""))
				{
					zipCode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
					String strStateCode = zipCode.StateCode;

					if (strStateCode.Trim() != "")
					{
						string strConNo = "";
						string strBookingNo = "";

						if (txtBookingNo.Text.Trim() != "" )
							strBookingNo = txtBookingNo.Text.Trim();
						if (txtConsigNo.Text.Trim() != "" )
							strConNo = txtConsigNo.Text.Trim();

						object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime((DateTime)drEach["est_delivery_datetime"], strStateCode,
							strConNo.Trim(), strBookingNo.Trim(), appID, enterpriseID);

						if((tmpReHCDateTime != null) && 
							(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							drEach["est_hc_return_datetime"] = (DateTime)tmpReHCDateTime;
						}
						else
						{
							drEach["est_hc_return_datetime"] = System.DBNull.Value;
							drEach["act_hc_return_datetime"] = System.DBNull.Value;
						}
					}
					else
					{
						drEach["est_hc_return_datetime"] = System.DBNull.Value;
						drEach["act_hc_return_datetime"] = System.DBNull.Value;
					}
				}	
				else
				{
					drEach["est_hc_return_datetime"] = System.DBNull.Value;
					drEach["act_hc_return_datetime"] = System.DBNull.Value;
				}
			}
			//HC Return Task
			
			drEach["shipment_type"]= "D";
			
			if(txtSendZip.Text.Trim() != "")
			{
				zipCode.Populate(appID,enterpriseID,txtSendZip.Text.Trim());
				String strStateCode = zipCode.StateCode;

				//************** origin_state_code ***********************//
				drEach["origin_state_code"] = strStateCode;

				//************** DC to origin_station ********************//
				DataSet tmpDC = DomesticShipmentMgrDAL.getDCToOrigStation(appID, enterpriseID,
					0, 0, txtSendZip.Text.Trim()).ds;

				drEach["origin_station"] = tmpDC.Tables[0].Rows[0]["origin_station"].ToString();
				ViewState["orig_station"] = tmpDC.Tables[0].Rows[0]["origin_station"].ToString();

				tmpDC.Dispose();
				tmpDC = null;
			}
			

			//drEach["invoice_no"] = "";
			

			if(txtRecipZip.Text.Trim() != "")
			{
				zipCode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
				String strStateCode = zipCode.StateCode;
				
				//************** destination_state_code *****************//
				drEach["destination_state_code"] = strStateCode;

				//************** DC to dest_station *********************//
				DataSet tmpDC = DomesticShipmentMgrDAL.getDCToDestStation(appID, enterpriseID,
					0, 0, txtRouteCode.Text.Trim()).ds;

				drEach["destination_station"] = tmpDC.Tables[0].Rows[0]["origin_station"].ToString();
				ViewState["dest_station"] = tmpDC.Tables[0].Rows[0]["origin_station"].ToString();
				ViewState["path_code"] = tmpDC.Tables[0].Rows[0]["path_code"].ToString();

				tmpDC.Dispose();
				tmpDC = null;
			}
			
			if(txtTotVASSurChrg.Text.Trim().Length > 0)
			{
				drEach["tot_vas_surcharge"] = Convert.ToDecimal(txtTotVASSurChrg.Text.Trim());
			}
			else
			{
				drEach["tot_vas_surcharge"] = 0;
			}

			if(txtLatestStatusCode.Text.Trim() != "")
			{
				drEach["last_status_code"] = txtLatestStatusCode.Text.Trim();
			}
			
			if(txtShipManifestDt.Text.Trim().Length > 0)
			{
				drEach["shpt_manifest_datetime"] = System.DateTime.ParseExact(txtShipManifestDt.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			}
			if(txtDelManifest.Text.Trim().Length > 0)
			{
				drEach["delivery_manifested"] =  txtDelManifest.Text.Trim().Substring(0,1);
			}
		
			//########## Automatically Assing to Manifests (DMS Phase 1) ##########
			if(txtRouteCode.Text.Trim() != "")
			{
				if(((int)ViewState["DSOperation"] == (int)Operation.Update))
				{
					if(drEach["route_code"] != System.DBNull.Value)
					{
						String tmpNewRouteZip = (String)txtRouteCode.Text.ToString();
						String tmpOldRouteZip = (String)drEach["route_code"];

						if (tmpNewRouteZip != tmpOldRouteZip)
							ViewState["IsNeedAutoManifest"] = true;
					}
					else
					{
						ViewState["IsNeedAutoManifest"] = true;
					}
				}

				drEach["route_code"] = txtRouteCode.Text.Trim();
				ViewState["route_code"] = txtRouteCode.Text.Trim();
			}
			//########## Automatically Assing to Manifests (DMS Phase 1) ##########
			
			//Get the quotation number & version 
			QuotationData custData;
			custData.iQuotationVersion = 0;
			custData.strQuotationNo= null;

			if(ddbCustType.SelectedItem.Value != "0")
			{
				try
				{
					custData = TIESUtility.CustomerQtnActive(appID,enterpriseID,txtCustID.Text.Trim());
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					strErrorMsg = strMsg;
					return;
				}
			
				drEach["quotation_no"] = custData.strQuotationNo;
				drEach["quotation_version"] = custData.iQuotationVersion.ToString();
			}

			if(txtPkgCommCode.Text.Trim() != "")
			{
				drEach["commodity_code"] = txtPkgCommCode.Text.Trim();
			}

			
			if(txtFreightChrg.Text.Trim().Length > 0)
			{
				drEach["tot_freight_charge"] = Convert.ToDecimal(txtFreightChrg.Text.Trim());
			}
			else
			{
				drEach["tot_freight_charge"] = 0;
			}

			if(txtESASurchrg.Text.Trim().Length > 0)
			{
				drEach["esa_surcharge"] = Convert.ToDecimal(txtESASurchrg.Text.Trim());
			}
			else
			{
				drEach["esa_surcharge"] = 0;
			}
			
			if(txtSpeHanInstruction.Text !="")
			{
				drEach["remark"] = txtSpeHanInstruction.Text.Trim();
			}

			drEach["last_status_datetime"] = System.DateTime.ParseExact(System.DateTime.Now.ToString("dd/MM/yyyy HH:mm"),"dd/MM/yyyy HH:mm",null);
			
			Customer customer = new Customer();
			customer.Populate(appID,enterpriseID, drEach["payerid"].ToString());

			if((customer.mbg != null) && 
				(!customer.mbg.GetType().Equals(System.Type.GetType("System.DBNull"))) && 
				(customer.mbg.ToString() != ""))
			{
				if(customer.mbg.ToString().Trim() == "Y")
					drEach["mbg"]= "Y";
				else
					drEach["mbg"]= "N";
			}
			else
			{
				drEach["mbg"]= "N";
			}
			
			//By Aoo 22/02/2008
			if(this.txtOtherSurcharge.Text.Trim().Length > 0)
			{
				drEach["other_surch_amount"] = Convert.ToDecimal(txtOtherSurcharge.Text.Trim());
			}
			else
			{
				drEach["other_surch_amount"] = 0;
			}
			//End By Aoo
			
			lblErrorMsg.Text = strErrorMsg;
		}

		private void ClearAllFields()
		{
			txtBookDate.Text = "";
			txtBookingNo.Text = "";
			txtOrigin.Text = "";
			txtDestination.Text = "";
			txtLatestStatusCode.Text = "";
			txtLatestExcepCode.Text = "";
			txtDelManifest.Text = "";
			ddbCustType.SelectedItem.Selected = false;
			ddbCustType.Items.FindByValue("0").Selected = true;
			txtConsigNo.Text = "";
			txtRefNo.Text = "";
			txtShipManifestDt.Text = "";
			txtRouteCode.Text = "";
			txtGoToRec.Text = "";
			txtCustID.Text = "";
			txtCustName.Text = "";
			txtCustAdd1.Text = "";
			txtCustAdd2.Text = "";
			txtCustZipCode.Text = "";
			txtCustCity.Text = "";
			txtCustStateCode.Text = "";
			txtCustTelephone.Text = "";
			txtCustFax.Text = "";
			txtActDelDt.Text="";
			txtSpeHanInstruction.Text="";

			//HC Return Task
			txtEstHCDt.Text="";
			txtActHCDt.Text="";
			//HC Return Task

			decimal tmpCod = 0;
			txtCODAmount.Text = String.Format("{0:F2}", tmpCod);
			
			chkNewCust.Checked = false;
			chkSendCustInfo.Checked = false;
			chkRecip.Checked = false;

			if((int)ViewState["DSMode"]!=(int)ScreenMode.Query)
			{
				rbCash.Checked = false;
				rbCredit.Checked = true;
			}

			txtSendAddr1.Text = "";
			txtSendAddr2.Text = "";
			txtSendName.Text = "";
			txtSendZip.Text = "";
			txtSendCity.Text = "";
			txtSendState.Text = "";
			txtSendCuttOffTime.Text = "";
			txtSendContPer.Text = "";
			txtSendTel.Text = "";
			txtSendFax.Text = "";
			txtRecName.Text = "";
			txtRecipAddr1.Text = "";
			txtRecipAddr2.Text = "";
			txtRecipZip.Text = "";
			txtRecipCity.Text = "";
			txtRecipState.Text = "";
			txtRecpContPer.Text = "";
			txtRecipTel.Text = "";
			txtRecipFax.Text = "";
			txtPkgActualWt.Text = "";			
			txtActualWeight.Text = "";//TU 17June08 
			txtPkgChargeWt.Text = "";
			txtPkgCommCode.Text = "";
			txtPkgDimWt.Text = "";
			txtPkgCommDesc.Text = "";
			txtPkgDimWt.Text = "";
			txtPkgTotpkgs.Text = "";
			txtShipEstDlvryDt.Text = "";
			txtShipManifestDt.Text = "";
			txtShipPckUpTime.Text = "";
			txtShpAddDV.Text = "";
			txttotVol.Text = "";  //Jeab 22 Feb 2011

			decimal tmpDclrValue = 0;
			txtShpDclrValue.Text = String.Format((String)ViewState["m_format"], tmpDclrValue);

			txtShpInsSurchrg.Text = "";
			txtShpMaxCvrg.Text = "";
			txtShpSvcCode.Text = "";
			txtShpSvcDesc.Text = "";
			txtShpDclrValue.Text = "";	//Declare Value
			txtCODAmount.Text = "";	//COD Amount
			txtShpTotAmt.Text = "";
			txtTotVASSurChrg.Text = "";
			txtFreightChrg.Text = "";
			txtInsChrg.Text = "";
			txtShpTotAmt.Text = "";
			txtESASurchrg.Text = "";
			txtOtherSurcharge.Text = txtOtherSurcharge2.Text = "";

			//Invoice Field
			txtPODEX.Text = "";
			txtMBGAmt.Text = "";
			txtOtherInvC_D.Text = "";
			txtTotalInvAmt.Text = "";
			txtCreditNotes.Text = "";
			txtDebitNotes.Text = "";
			txtTotalConRevenue.Text = "";
			txtPaidRevenue.Text ="";

			//UnLock Manual Override
			LockAllManualOverride(false);

			ViewState["DSPickupData"]=null;
		}

		private void Insert_Click()
		{	
			m_sdsDomesticShip = DomesticShipmentMgrDAL.GetDomShipSessionDS();
			Session["SESSION_DS3"] = m_sdsDomesticShip;
			btnPkgDetails.Enabled = true;
			if(btnSave.Enabled)
			{
				if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
				{
					lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
					if(lblErrorMsg.Text != "Con# " + txtConsigNo.Text + " Saved Successfully")
					{
						DomstcShipPanel.Visible = true;
						divMain.Visible=false;
					} 
					else
					{
						DomstcShipPanel.Visible = false;
						divMain.Visible=true;
					}
					divDelOperation.Visible=false;
					ddbCustType.Visible = false;
					//ViewState["MoveFirst"]=true;	
					ViewState["NextOperation"]="Insert";
					return;
				}
			}
			else
			{
				ViewState["isTextChanged"] = false;
				//ViewState["DSOperation"] =Operation.None;
			}
			
			ViewState["PickupBooking"] = "No";
			txtShpSvcCode.ReadOnly = false;
			txtRecipZip.ReadOnly = false;
			Session["SESSION_DS2"] = null;
			lblErrorMsg.Text = "";
			Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,true,m_moduleAccessRights);
			btnExecQry.Enabled = false;
			Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete,false,m_moduleAccessRights);
			//			rbtnShipFrghtColl.Enabled = true;
			rbtnShipFrghtPre.Enabled = true;
			rbtnShipFrghtColl.Checked = false;
			rbtnShipFrghtPre.Checked = true;
			rbCash.Checked = false;
			rbCredit.Checked = true;
			txtShipManifestDt.Enabled = false;
			chkNewCust.Enabled = true;
			if(fromInsertBtn)
				ClearAllFields();

			strDt = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
			if(txtBookDate.Text == "")
				txtBookDate.Text = strDt;

			
			txtShipManifestDt.Text = strDt;
			//
			txtSpeHanInstruction.Text="";
			//
			txtLatestStatusCode.Text = TIESUtility.getInitialShipmentStatusCode(appID,enterpriseID);
			txtDelManifest.Text = TIESUtility.getInitialDlvrymanifest(appID,enterpriseID);
			chkSendCustInfo.Checked = false;
			chkRecip.Checked = false;

			if(txtShipPckUpTime.Text == "")
				txtShipPckUpTime.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
			

			decimal tmpDclrValue = 0;
			txtShpDclrValue.Text = String.Format((String)ViewState["m_format"], tmpDclrValue);

			decimal tmpCod = 0;
			txtCODAmount.Text = String.Format("{0:F2}",tmpCod);
			
			m_dsVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
			Session["SESSION_DS1"] = m_dsVAS;
			BindVASGrid();

			ViewState["AddSurcharge"] = "none";
			ViewState["DSMode"]=ScreenMode.Insert;
			ViewState["DSOperation"] = Operation.Insert;
			ViewState["currentPage"] = 0;

			txtBookingNo.Enabled = true;
			txtBookingNo.AutoPostBack = true;
			txtBookDate.Enabled = false;
			txtConsigNo.Enabled = true;
			txtConsigNo.AutoPostBack= true;
			txtDelManifest.Enabled = false;
			txtShipManifestDt.Enabled = false;
			ViewState["isTextChanged"] = false;
			//opal 20121108
			this.txtCODAmount.Enabled = true;

			DataSet commodities = CommodityDAL.GetCommodities(appID, enterpriseID,0,0).ds;

			if (commodities.Tables[0].Rows.Count == 1)
			{
				if ((commodities.Tables[0].Rows[0]["commodity_code"] != null) && (commodities.Tables[0].Rows[0]["commodity_code"].ToString() != ""))
					txtPkgCommCode.Text = commodities.Tables[0].Rows[0]["commodity_code"].ToString();
				else
					txtPkgCommCode.Text = "";

				if ((commodities.Tables[0].Rows[0]["commodity_description"] != null) && (commodities.Tables[0].Rows[0]["commodity_description"].ToString() != ""))
					txtPkgCommDesc.Text = commodities.Tables[0].Rows[0]["commodity_description"].ToString();
				else
					txtPkgCommDesc.Text = "";
			}
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{			
			Insert_Click();
			SetInitialFocus(txtBookingNo);
			ViewState["PartialCon"]=false;
			ViewState["InitialBooking"] = true;

			btnViewOldPD.Visible = false;
			EnterpriseConfigurations();
		}

		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}

		private ServiceAvailable CheckDayServiceAvail()
		{
			bool isWeekHolidaySrvAvail = false;
			ServiceData serviceData;
			serviceData.isServiceAvail = false;
			serviceData.strVASCode = null;
			DateTime dtDlvryDt;
			String strWeekDay = "";
			String strDayType = "";
			String strVASCode = null;
			ServiceAvailable serviceAvailable = new ServiceAvailable();
			ViewState["Day"] = "";

			strErrorMsg = "";
			//Get the day from the delivery date.
			dtDlvryDt = System.DateTime.ParseExact(txtShipEstDlvryDt.Text.ToString(),"dd/MM/yyyy HH:mm",null);
			strWeekDay = dtDlvryDt.DayOfWeek.ToString();

			if ((strWeekDay.Equals("Saturday")) || (strWeekDay.Equals("Sunday")))
			{
				//Check whether the service is there for saturday or sunday
				strDayType = "W";

				ViewState["Day"] = "WeekEnd";

				try
				{
					serviceData = TIESUtility.IsServiceAvailable(appID,enterpriseID,txtSendZip.Text.Trim(),strWeekDay,strDayType);
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					strErrorMsg = strMsg;
					return serviceAvailable;
				}
				
				//Flag which shows service available or not
				isWeekHolidaySrvAvail = serviceData.isServiceAvail;

				ViewState["ServiceExists"] = isWeekHolidaySrvAvail;

				//VAS Code for the service
				strVASCode = serviceData.strVASCode;

			}

			//Check whether the delivery date is on holiday 
			bool isHoliday = false;
			try
			{
				isHoliday = TIESUtility.IsDayHoliday(appID,enterpriseID,System.DateTime.ParseExact(txtShipEstDlvryDt.Text.Trim().ToString(),"dd/MM/yyyy HH:mm",null));
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				strErrorMsg = strMsg;
				return serviceAvailable;
			}

			if(isHoliday == true)
			{
				ViewState["isHoliday"] = true;
				//check whether service is provided for holiday
				strDayType = "H";
				
				ViewState["Day"] = "Holiday";

				try
				{
					serviceData	= TIESUtility.IsServiceAvailable(appID,enterpriseID,txtSendZip.Text.Trim(),strWeekDay,strDayType);
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					strErrorMsg = strMsg;
					return serviceAvailable;
				}
				
				//Flag which shows service available or not
				isWeekHolidaySrvAvail = serviceData.isServiceAvail;
				
				ViewState["ServiceExists"] = isWeekHolidaySrvAvail;
				
				//VAS Code for the service
				strVASCode = serviceData.strVASCode; 
				
			}
			
			serviceAvailable.isServiceAvail = isWeekHolidaySrvAvail;
			serviceAvailable.strVASCode = strVASCode;
			
			return serviceAvailable;
		}

		private void CalcAdditionalSurchrg(String strVASCode)
		{
			//Added by GwanG on 08April08
			String strSatDel = (string)ViewState["StateSatDel"];
			String strSunDel = (string)ViewState["StateSunDel"];
			String strPubDel = (string)ViewState["StatePubDel"]; 
			ViewState["StateErr"] = "N";

			//The service is available
			QuotationData quotationData;
			quotationData.iQuotationVersion = 0;
			quotationData.strQuotationNo = null;
			int iQuotnVersion = 0;
			String strQuotnNo = null;

			//Get the quotation number & version 
			quotationData =  TIESUtility.CustomerQtnActive(appID,enterpriseID,txtCustID.Text.Trim());
			iQuotnVersion = quotationData.iQuotationVersion;
			strQuotnNo = quotationData.strQuotationNo;

			//Check whether the quotation is active.
			String strQuotVASCode = null;
			String strVasDesc = null;
			decimal decQuotSurchrg;
			VASSurcharge  vasSurcharge;
			DataSet vasSearchDS;
			

			if(strQuotnNo != null)
			{
				vasSurcharge = TIESUtility.GetSurchrgVASCode(appID,enterpriseID,"C",txtCustID.Text.Trim(),strQuotnNo,iQuotnVersion,strVASCode);
				decQuotSurchrg = vasSurcharge.decSurcharge;			
				strQuotVASCode = vasSurcharge.strVASCode;
				strVasDesc = vasSurcharge.strVASDesc;
			}
			else //There is no active quotation
			{
				//Get the Surcharge from the Vas base table
				VAS vas = new VAS();
				vas.Populate(appID,enterpriseID,strVASCode);
				decQuotSurchrg = vas.Surcharge;
				strQuotVASCode = vas.VASCode;
				strVasDesc = vas.VasDescription;
			}

			if(strQuotVASCode != null)
			{
				if (m_dsVAS.Tables[0].Select("vas_code = '" + strQuotVASCode + "'").Length == 0)
				{
					switch(strQuotVASCode)
					{
						case "SATDEL":			
							if(strSatDel == "Y")
							{
								//Get from Customer_VAS
								vasSearchDS = SysDataMgrDAL.GetVASCustDS(this.appID,this.enterpriseID,strQuotVASCode,txtCustID.Text);
								if( vasSearchDS.Tables[0].Rows.Count>0)
								{
									//Call function Addrow & Calculate
									//Function(DataSet vasSearchDS);
									AddNewRowVAS(vasSearchDS);					
								}							
							
								else if(Utility.IsNotDBNull(ViewState["StateSatDelCode"]))
								{
									String strSatDelCode = (string)ViewState["StateSatDelCode"];
									DataSet vasSearchDS1 = SysDataMgrDAL.GetCustDescSurch(appID,enterpriseID,strSatDelCode);
									if(vasSearchDS1.Tables[0].Rows.Count>0)
									{
										AddNewRowVAS1(vasSearchDS1);
									}

								}
							}
							else
							{
								ViewState["StateErr"] = "Y" ;
								ViewState["MsgErr"] =Utility.GetLanguageText(ResourceType.UserMessage,"DMS_ESTSAT",utility.GetUserCulture());
							}							
							break;
						case "SUNDEL":
							if(strSunDel == "Y")
							{
								vasSearchDS = SysDataMgrDAL.GetVASCustDS(this.appID,this.enterpriseID,strQuotVASCode,txtCustID.Text);
								if( vasSearchDS.Tables[0].Rows.Count>0)
								{
									//Call function Addrow & Calculate
									//Function(DataSet vasSearchDS);
									AddNewRowVAS(vasSearchDS);					
								}

								else if(Utility.IsNotDBNull(ViewState["StateSunDelCode"]))
								{
									String strSunDelCode = (string)ViewState["StateSunDelCode"];
									DataSet vasSearchDS1 = SysDataMgrDAL.GetCustDescSurch(appID,enterpriseID,strSunDelCode);
									if(vasSearchDS1.Tables[0].Rows.Count>0)
									{
										AddNewRowVAS1(vasSearchDS1);
									}

								}
							}
							else
							{
								ViewState["StateErr"] = "Y" ;
								ViewState["MsgErr"] =Utility.GetLanguageText(ResourceType.UserMessage,"DMS_ESTSUN",utility.GetUserCulture());
							}
							break;
						case "PUBDEL":
							if(strPubDel == "Y")
							{
								vasSearchDS = SysDataMgrDAL.GetVASCustDS(this.appID,this.enterpriseID,strQuotVASCode,txtCustID.Text);
								if( vasSearchDS.Tables[0].Rows.Count>0)
								{
									//Call function Addrow & Calculate
									//Function(DataSet vasSearchDS);
									AddNewRowVAS(vasSearchDS);					
								}

								else if(Utility.IsNotDBNull(ViewState["StatePubDelCode"]))
								{
									String strPubDelCode = (string)ViewState["StatePubDelCode"];
									DataSet vasSearchDS1 = SysDataMgrDAL.GetCustDescSurch(appID,enterpriseID,strPubDelCode);
									if(vasSearchDS1.Tables[0].Rows.Count>0)
									{
										AddNewRowVAS1(vasSearchDS1);
									}

								}
							}
							else
							{
								ViewState["StateErr"] = "Y" ;
								ViewState["MsgErr"] =Utility.GetLanguageText(ResourceType.UserMessage,"DMS_ESTHOLIDAY",utility.GetUserCulture());							
							}
							break;

						default :
							
							//Add the record to the VAS DataSet	& bind to the GRID
							AddRowInVASGrid();	
							dgVAS.EditItemIndex = m_dsVAS.Tables[0].Rows.Count - 1;

							DataRow drCurrent = m_dsVAS.Tables[0].Rows[dgVAS.EditItemIndex];
							drCurrent["surcharge"] = TIESUtility.EnterpriseRounding(decQuotSurchrg,(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]);
							drCurrent["vas_code"] = strQuotVASCode;
							drCurrent["vas_description"] = strVasDesc;
							//Display the Total VAS Surcharge
							if(txtTotVASSurChrg.Text.Length > 0)
							{
								txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], Convert.ToDecimal(txtTotVASSurChrg.Text)+decQuotSurchrg);
							}
							else
							{
								txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], decQuotSurchrg);
							}

							//Display the Total of all charges
							CalculateTotalAmt();
							dgVAS.EditItemIndex = -1;
							BindVASGrid();							
							break;

					}

				}
			}
		}

		private bool SaveUpdateRecord()
		{
			lblErrorMsg.Text = "";
			bool isError = false;
			if(dgVAS.EditItemIndex != -1)
			{
				m_dsVAS.Tables[0].Rows.RemoveAt(dgVAS.EditItemIndex);
			}
			if(ViewState["SeriviceNotAvail"] != null)
			{
				bool serAvail = (bool)ViewState["SeriviceNotAvail"];
				if(!serAvail) 
				{
					strErrorMsg = "Service type is not available";
					lblErrorMsg.Text = strErrorMsg;
					txtShpSvcCode.Text = "";
					txtShpSvcDesc.Text = "";
					return isError;
				}
			}

			else
			{
				bool IsServiceAvail = false;
				try
				{
					IsServiceAvail = Service.IsAvailable(appID,enterpriseID,txtShpSvcCode.Text.Trim(), txtSendZip.Text.Trim(), txtRecipZip.Text.Trim());
				}
				catch(ApplicationException appException)
				{
					lblErrorMsg.Text = appException.Message;
					isError = true;
					return isError;
				}
				if(!IsServiceAvail)
				{
					strErrorMsg = "Service type is not available";
					lblErrorMsg.Text = strErrorMsg;
					txtShpSvcCode.Text = "";
					txtShpSvcDesc.Text = "";
					return isError;
				}
			}
			
			if(txtShipEstDlvryDt.Text.ToString().Trim() == "")
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVLY",utility.GetUserCulture());
				isError = true;
				return isError;
			}

			ServiceAvailable serviceAvailable =  CheckDayServiceAvail();
			m_dsPkgDetails = (DataSet)Session["SESSION_DS2"];
			
//			if((DomesticShipmentMgrDAL.IsConsgmentPurged(appID,enterpriseID,txtConsigNo.Text.Trim()) == true) && ((int)ViewState["DSMode"] == (int)ScreenMode.Insert) && (((int)ViewState["DSOperation"] == (int)Operation.Saved) ))
//			{
//				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());
//				isError = true;
//				return isError;
//			}

			if((DomesticShipmentMgrDAL.IsConsgmentExist(appID,enterpriseID,txtConsigNo.Text.Trim()) == true) && ((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery) && (((int)ViewState["DSOperation"] == (int)Operation.Insert) ))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_BKG_CONSGMT",utility.GetUserCulture());
				isError = true;
				return isError;
			}
			if(txtBookingNo.Text.Trim()=="")
			{
				if(DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null).CompareTo(DateTime.ParseExact(txtShipManifestDt.Text,"dd/MM/yyyy HH:mm",null)) < 0)
				{
					lblErrorMsg.Text  = Utility.GetLanguageText(ResourceType.UserMessage,"DELETED_DT_LESS_SHPT_MFST_DT",utility.GetUserCulture());
					isError = true;
					return isError;
				}
			}
			else if((m_dsPkgDetails == null) && ((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_PKG_DETAILS",utility.GetUserCulture());
				isError = true;
				return isError;
			}
			else if(m_dsVAS == null)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_VAS_DETAILS",utility.GetUserCulture());
				isError = true;
				return isError;
			}

			//Added By GwanG on 28April08 >> Alert message when incorrected package detail
			if(m_dsPkgDetails == null)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_PKG_DETAILS",utility.GetUserCulture());
				isError = true;
				return isError;
			}
			if(m_dsPkgDetails.Tables[0].Rows.Count <= 0)		
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_PKG_DETAILS",utility.GetUserCulture());
				isError = true;
				return isError;
			}
			//End

			//Cal Total VAS Surcharge
			object sumSurcharge = m_dsVAS.Tables[0].Compute("sum(surcharge)",null);
			if(sumSurcharge!=null && sumSurcharge!=DBNull.Value) txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], (decimal)sumSurcharge);

			//calCulateESA();
			CalculateAllCharge();
			if(lblErrorMsg.Text != "Customer has no Box Rate defined.")
			{
				if(txtFreightChrg.Text.Trim() =="" && txtPkgChargeWt.Text.Length >0 && txtShpDclrValue.Text.Trim().Length >0)
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_RATES",utility.GetUserCulture());
					isError = true;
					return isError;
				}
			}
			else
			{
				isError = true;
				return isError;
			}

			//
			if(txtSpeHanInstruction.Text.Trim().Length>200)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_200CHRS_SPL",utility.GetUserCulture());
				isError = true;
				return isError;
			}
//			CHAI 03/04/2013  NOT CHECK VAS
//			else if((serviceAvailable.isServiceAvail == true) && ((String)ViewState["AddSurcharge"] == "none") && ((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery))
//			{
//				bool isVASExists = false;
//				ViewState["ServiceVASCode"] = serviceAvailable.strVASCode;
//				//Check whether the service is already there in the VAS grid's dataset.
//				for(int i=0;i<m_dsVAS.Tables[0].Rows.Count;i++)
//				{
//					DataRow drCurrent = m_dsVAS.Tables[0].Rows[i];
//					if((drCurrent["vas_code"] != null) && ((!drCurrent["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull")))) && ((String)drCurrent["vas_code"] == (String)ViewState["ServiceVASCode"]))
//					{
//						isVASExists = true;
//					}
//					
//				}
//				if(isVASExists == false)
//				{
//					//display the popup msg
//					/*lblConfirmMsg.Text = txtShipEstDlvryDt.Text.ToString()+ " Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge will be imposed. Do you want to continue?";
//					DomstcShipPanel.Visible = true;
//					divCustIdCng.Visible=false;
//					divMain.Visible=false;
//					divDelOperation.Visible=false;
//					ddbCustType.Visible = false;
//					ViewState["AddSurcharge"] = "asking";
//					isError = true;
//					return isError;*/
//				}
//			}
			else
			{
				//if no service available then get the next working day...
				/*
				if((bool)ViewState["ServiceExists"] == false && (((String)ViewState["Day"] == "WeekEnd" ||((String)ViewState["Day"] == "Holiday"))))
				{
					ReCalculateDlvryDt();
				}
				*/
				//########## Automatically Assing to Manifests (DMS Phase 1) ##########

				//4. After all audits are completed and passed and just before the Domestic Shipment record
				//is saved, assign the following two fields as shown:

				if(((txtBookingNo.Text.Length == 0) && ((int)ViewState["DSMode"] == (int)ScreenMode.Insert) && (((int)ViewState["DSOperation"] == (int)Operation.Insert)||((int)ViewState["DSOperation"] == (int)Operation.Saved))
					|| ((txtBookingNo.Text.Length > 0) && ((int)ViewState["DSMode"] == (int)ScreenMode.Insert) && ((int)ViewState["DSOperation"] == (int)Operation.Insert))||((int)ViewState["DSOperation"] == (int)Operation.Saved)))
				{
					DataRow drEach = m_sdsDomesticShip.ds.Tables[0].Rows[Convert.ToInt32((ViewState["currentPage"]))];
					
					if(txtSendZip.Text.Trim() != "")
					{
						DataSet tmpDC = DomesticShipmentMgrDAL.getDCToOrigStation(appID, enterpriseID,
							0, 0, txtSendZip.Text.Trim()).ds;
						ViewState["orig_station"] = tmpDC.Tables[0].Rows[0]["origin_station"].ToString();

						tmpDC.Dispose();
						tmpDC = null;
					}

					// Comment By TU  15/02/2011 Remove manifest
					if (DeliveryManifestMgrDAL.IsAutomanifest(appID,enterpriseID,txtShpSvcCode.Text.ToString().Trim()) == "Y")
					{
						//Shipment.shpt_manifest_datetime <-- current date / time
						String strDt = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
						txtShipManifestDt.Text = strDt;

						//Shipment.delivery_manifested <-- Y
						if(DistributionCenterDAL.IsDChasSWB(appID,enterpriseID,(String)ViewState["orig_station"]))
							txtDelManifest.Text = "No";    
						else
							txtDelManifest.Text = "Yes";
					}
					//
				}
		
				GetValuesIntoDS(Convert.ToInt32((ViewState["currentPage"])));
				//########## Automatically Assing to Manifests (DMS Phase 1) ##########

				Logger.LogDebugInfo("module1","DomesticShipment.cs","SaveUpdateRecord","INF001"," This is before add/update ");
				DataSet dsDomesticShipment = m_sdsDomesticShip.ds.GetChanges();
				
				//				if(((txtBookingNo.Text.Length == 0) && ((int)ViewState["DSOperation"] == (int)Operation.Insert))
				//					|| ((txtBookingNo.Text.Length > 0) && ((int)ViewState["DSOperation"] == (int)Operation.Insert)))
				
				int dsoperation = (int)ViewState["DSOperation"];
				if(((txtBookingNo.Text.Length == 0) && ((int)ViewState["DSMode"] == (int)ScreenMode.Insert) && (((int)ViewState["DSOperation"] == (int)Operation.Insert)||((int)ViewState["DSOperation"] == (int)Operation.Saved))
					|| ((txtBookingNo.Text.Length > 0) && ((int)ViewState["DSMode"] == (int)ScreenMode.Insert) && ((int)ViewState["DSOperation"] == (int)Operation.Insert))||((int)ViewState["DSOperation"] == (int)Operation.Saved)))
				{
					try
					{
						int iSerialNo = 0;
						if(Session["SerialNo"] != null)
						{
							String strSerialNo = (String)Session["SerialNo"];
							iSerialNo = Convert.ToInt32(strSerialNo);
						}
						txtBookingNo.Text = DomesticShipmentMgrDAL.AddDomesticShipment(appID,enterpriseID,dsDomesticShipment,m_dsVAS, null,m_dsPkgDetails,userID,txtRecipZip.Text.Trim(),1).ToString();
						Logger.LogDebugInfo("module1","DomesticShipment.cs","SaveUpdateRecord","INF001"," This is after  add ");
						
						DataRow drEach = m_sdsDomesticShip.ds.Tables[0].Rows[Convert.ToInt32((ViewState["currentPage"]))];

						// Comment By TU  15/02/2011 Remove manifest
						if (DeliveryManifestMgrDAL.IsAutomanifest(appID,enterpriseID,(string)drEach["service_code"]) == "Y")
						{
							if(SysDataManager1.ChkManifest((string)drEach["origin_state_code"],appID,enterpriseID))
							{
								DomesticShipmentMgrDAL.setAutomaticManifest(appID,enterpriseID,
									dsDomesticShipment, (String)ViewState["path_code"],Convert.ToInt64(txtBookingNo.Text.Trim()),
									(String)ViewState["orig_station"], (String)ViewState["dest_station"], (String)ViewState["route_code"], (string)drEach["service_code"],Int32.Parse(ViewState["ClickNoHoliday"].ToString()));
							}

						}
						else
						{
							//9. If the above query does not return a value then:
							//Call exit_w-o_manifesting (logic for this exit program is shown below)
							if(SysDataManager1.ChkManifest((string)drEach["origin_state_code"],appID,enterpriseID))
							{
								DomesticShipmentMgrDAL.UpdateShipmentOfAutoManifest(appID, enterpriseID,
									Convert.ToInt32(txtBookingNo.Text.TrimStart().TrimEnd()), (String)drEach["consignment_no"]);
							}

							txtShipManifestDt.Text = "";
							txtDelManifest.Text = "No";
						}
						//						
						ViewState["isTextChanged"] = false;
						ChangeDSState();
						m_sdsDomesticShip.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])].AcceptChanges();
					}

					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							strErrorMsg = "Duplicate Key cannot save the record";
						}
						else if(strMsg.IndexOf("FK") != -1 )
						{
							strErrorMsg = "Foreign Key Error cannot save the record";
						}
						else if(strMsg.IndexOf("Duplicate Consignment") != -1 )
						{
							strErrorMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());
						}
						else
						{
							strErrorMsg = strMsg;
						}
						lblErrorMsg.Text = strErrorMsg;
						isError = true;
						// Comment By TU  15/02/2011 Remove manifest
						if(strMsg.IndexOf("Record Inserted - Not Manifested.") != -1 )
						{
							try
							{
								DomesticShipmentMgrDAL.UpdateShipmentOfAutoManifest(appID, enterpriseID,
									Convert.ToInt32(txtBookingNo.Text.TrimStart().TrimEnd()), txtConsigNo.Text.TrimStart().TrimEnd());

								txtShipManifestDt.Text = "";
								txtDelManifest.Text = "No";
							}	
							catch(Exception ex)
							{
								ex = ex;
							}
						}
						//
						return isError;
					}
					
					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
					//Added By Tom Jan 19, 10
					if(lblErrorMsg.Text == "Record Saved Successfully")
						lblErrorMsg.Text = "Con# " + txtConsigNo.Text + " Saved Successfully";
					//End Added By Tom Jan 19, 10
					//ViewState["DSOperation"] = Operation.None;
				}
				else if((txtBookingNo.Text.Length > 0) && ((int)ViewState["DSOperation"] == (int)Operation.Update))
				{
					try
					{
						Logger.LogDebugInfo("module1","DomesticShipment.cs","SaveUpdateRecord","INF001"," This is before  update ");
						
						if((bool)ViewState["PartialCon"])
						{
							DomesticShipmentMgrDAL.UpdateDomesticShp_Partial(appID,enterpriseID,dsDomesticShipment,m_dsVAS,m_dsPkgDetails,this.userID, Convert.ToInt64(ViewState["preShipBkNo"]));
						}
						else
						{
							DomesticShipmentMgrDAL.UpdateDomesticShp(appID,enterpriseID,dsDomesticShipment,m_dsVAS,m_dsPkgDetails);
						}
						//txtBookingNo.Text = DomesticShipmentMgrDAL.AddDomesticShipment(appID,enterpriseID,dsDomesticShipment,m_dsVAS,m_dsPkgDetails,userID,txtRecipZip.Text.Trim(),2).ToString();

						// Comment By TU  15/02/2011 Remove manifest
						DomesticShipmentMgrDAL.DeleteAndAutoDMD(appID,enterpriseID,dsDomesticShipment, (String)ViewState["path_code"],
							Convert.ToInt64(txtBookingNo.Text.Trim()),(String)ViewState["orig_station"], (String)ViewState["dest_station"], 
							(String)ViewState["route_code"],Int32.Parse(ViewState["ClickNoHoliday"].ToString()));


						ViewState["PartialCon"]=false;
						ViewState["isTextChanged"] = false;
						Logger.LogDebugInfo("module1","DomesticShipment.cs","SaveUpdateRecord","INF001"," This is after  update ");
						ChangeDSState();
						m_sdsDomesticShip.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])].AcceptChanges();
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());
						}
						else if(strMsg.IndexOf("FK") != -1 )
						{
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CNSG_NO",utility.GetUserCulture());
						}
						else
						{
							lblErrorMsg.Text = appException.Message.ToString();
						}
						isError = false;
						return isError;
					}
					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
				}

				txtBookingNo.Enabled = false;
				txtConsigNo.Enabled = false;
			}
			
			//lblErrorMsg.Text = strErrorMsg;
			Logger.LogDebugInfo("module1","DomesticShipment.cs","SaveUpdateRecord","INF001"," Out of the saveupdate method ");
			ViewState["isTextChanged"] = false;
			return isError;
		}

		
		private void btnSave_Click(object sender, System.EventArgs e)
		{
			string ConNo="";
			if(txtConsigNo.Text.Trim().Length > 0 &&Utility.ValidateConsignmentNo(txtConsigNo.Text.Trim(),ref ConNo)==false)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSNO_INVALID",utility.GetUserCulture()); 
				SetInitialFocus(txtConsigNo);
				return;
			}
			else
			{
				txtConsigNo.Text=ConNo;
				lblErrorMsg.Text="";
			}

			ViewState["ClickNoHoliday"]=0;
			//SaveUpdateRecord();
			//ViewState["fromInsert"] = true;

			//Added By GwanG on 06May08
			ViewState["m_dsVAS_old"]  = m_dsVAS.Copy();

			//Added By GwanG on 1-Apr-08; remove SATPUP,SUNPUP,PUBPUP
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			Customer cs = new Customer();
			cs.Populate(appID, enterpriseID,txtCustID.Text.Trim());

			if (txtBookingNo.Text.Trim() == "" )
			{
				DataRow drEach = m_sdsDomesticShip.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
				txtBookingNo.Text = drEach["booking_no"].ToString();
			}

			DataRow drEachPreShip = m_sdsDomesticShip.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
			ViewState["preShipBkNo"] = drEachPreShip["booking_no"].ToString();
			drEachPreShip = null;
			 
			if(chkNewCust.Checked == false)
			{
				//if(txtCustID.Text.Length<0)
				if(cs.CustomerName == null)
				{
					this.lblErrorMsg.Text = "Customer ID does not exist.";
					return;
				}
			}

			bool IsServiceAvail = false;

			try
			{
				IsServiceAvail = Service.IsAvailable(appID,enterpriseID,txtShpSvcCode.Text.Trim(), txtSendZip.Text.Trim(), txtRecipZip.Text.Trim());
			}
			catch(ApplicationException appException)
			{
				IsServiceAvail = false;
			}

			if(!IsServiceAvail)
			{
				strErrorMsg = "Service type is not available";
				lblErrorMsg.Text = strErrorMsg;
				txtShpSvcCode.Text = "";
				txtShpSvcDesc.Text = "";
				return;
			}

			for(int i=0;i<m_dsVAS.Tables[0].Rows.Count;i++)
			{
				DataRow drCurrent = m_dsVAS.Tables[0].Rows[i];
				if(((String)drCurrent["vas_code"] == "SATPUP")|| ((String)drCurrent["vas_code"] == "SUNPUP")  || ((String)drCurrent["vas_code"] == "PUBPUP" ) || ((String)drCurrent["vas_code"] == "SATDEL")|| ((String)drCurrent["vas_code"] == "SUNDEL")  || ((String)drCurrent["vas_code"] == "PUBDEL") )
				{
					txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], Convert.ToDecimal(txtTotVASSurChrg.Text)- Convert.ToDecimal(drCurrent["surcharge"]) );
					m_dsVAS.Tables[0].Rows.RemoveAt(i);			
					i--;
				}			
					
			}



			m_dsVAS.Tables[0].AcceptChanges();
			Session["SESSION_DS1"] = m_dsVAS;


			//Get Enterprise Profile
			DataSet CustVASDS = new DataSet();

			if(Utility.IsNotDBNull(Session["SESSION_DSCUST"]))
			{
				CustVASDS = (DataSet)Session["SESSION_DSCUST"];
			}
			else
			{
				CustVASDS = SysDataMgrDAL.QueryVAS(this.appID,this.enterpriseID,txtCustID.Text);
			}

			Session["SESSION_DSCUST"] = CustVASDS;
			DataSet profileDS = (DataSet)Session["DMSENTPROF"];
			String strSatDel = null; //"sat_delivery_avail"
			String strSatDelCode = null; //"sat_delivery_vas_code"
			String strSunDel = null; //"sun_delivery_avail"
			String strSunDelCode = null; //"sun_delivery_vas_code"
			String strPubDel = null; //"pub_delivery_avail"
			String strPubDelCode = null; //"pub_delivery_vas_code"
			String strSatPick = null; //"sat_pickup_avail"
			String strSatPickCode = null; //"sat_pickup_vas_code"
			String strSunPick = null; //"sun_pickup_avail"
			String strSunPickCode = null; //"sun_pickup_vas_code"
			String strPubPick = null; //"pub_pickup_avail"		
			String strPubPickCode = null; //"pub_pickup_vas_code"		

			if(profileDS.Tables[0].Rows.Count > 0)
			{			
					
				if  ( profileDS.Tables[0].Rows[0]["sat_delivery_avail"] != System.DBNull.Value)
				{						
					strSatDel = profileDS.Tables[0].Rows[0]["sat_delivery_avail"].ToString();
					ViewState["StateSatDel"] = strSatDel;
					if  ( profileDS.Tables[0].Rows[0]["sat_delivery_vas_code"] != System.DBNull.Value)
					{						
						strSatDelCode = profileDS.Tables[0].Rows[0]["sat_delivery_vas_code"].ToString();
						ViewState["StateSatDelCode"] = strSatDelCode;
					}
				}

				if  ( profileDS.Tables[0].Rows[0]["sun_delivery_avail"] != System.DBNull.Value)
				{						
					strSunDel = profileDS.Tables[0].Rows[0]["sun_delivery_avail"].ToString();
					ViewState["StateSunDel"] = strSunDel;
					if  ( profileDS.Tables[0].Rows[0]["sun_delivery_vas_code"] != System.DBNull.Value)
					{						
						strSunDelCode = profileDS.Tables[0].Rows[0]["sun_delivery_vas_code"].ToString();
						ViewState["StateSunDelCode"] = strSunDelCode;
					}

				}

				if  ( profileDS.Tables[0].Rows[0]["pub_delivery_avail"] != System.DBNull.Value)
				{						
					strPubDel = profileDS.Tables[0].Rows[0]["pub_delivery_avail"].ToString();
					ViewState["StatePubDel"] = strPubDel;
					if  ( profileDS.Tables[0].Rows[0]["pub_delivery_vas_code"] != System.DBNull.Value)
					{						
						strPubDelCode = profileDS.Tables[0].Rows[0]["pub_delivery_vas_code"].ToString();
						ViewState["StatePubDelCode"] = strPubDelCode;
					}
				}

				if  ( profileDS.Tables[0].Rows[0]["sat_pickup_avail"] != System.DBNull.Value)
				{						
					strSatPick = profileDS.Tables[0].Rows[0]["sat_pickup_avail"].ToString();
					if  ( profileDS.Tables[0].Rows[0]["sat_pickup_vas_code"] != System.DBNull.Value)
					{						
						strSatPickCode = profileDS.Tables[0].Rows[0]["sat_pickup_vas_code"].ToString();
						ViewState["StateSatPupCode"] = strSatPickCode;
					}
				}

				if  ( profileDS.Tables[0].Rows[0]["sun_pickup_avail"] != System.DBNull.Value)
				{						 
					strSunPick = profileDS.Tables[0].Rows[0]["sun_pickup_avail"].ToString();
					if  ( profileDS.Tables[0].Rows[0]["sun_pickup_vas_code"] != System.DBNull.Value)
					{						
						strSunPickCode = profileDS.Tables[0].Rows[0]["sun_pickup_vas_code"].ToString();
						ViewState["StateSunPupCode"] = strSunPickCode;
					}
				}

				if  ( profileDS.Tables[0].Rows[0]["pub_pickup_avail"] != System.DBNull.Value)
				{						
					strPubPick = profileDS.Tables[0].Rows[0]["pub_pickup_avail"].ToString();
					if  ( profileDS.Tables[0].Rows[0]["pub_pickup_vas_code"] != System.DBNull.Value)
					{						
						strPubPickCode = profileDS.Tables[0].Rows[0]["pub_pickup_vas_code"].ToString();
						ViewState["StatePubPupCode"] = strPubPickCode;
					}
				}



			}

			DateTime TMPPickupDate = System.DateTime.ParseExact(txtShipPckUpTime.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			Zipcode tmpZip = new Zipcode();
			tmpZip.Populate(appID, enterpriseID, txtRecipZip.Text.Trim());

			// Saturday
			if(TMPPickupDate.DayOfWeek == System.DayOfWeek.Saturday)
			{						
				if(strSatPick == "Y")
				{
					//Get from Customer_VAS
					DataSet vasSearchDS = SysDataMgrDAL.GetVASCustDS(this.appID,this.enterpriseID,"SATPUP",txtCustID.Text);					
					if( vasSearchDS.Tables[0].Rows.Count>0)
					{
						//Call function Addrow & Calculate
						//Function(DataSet vasSearchDS);
						AddNewRowVAS(vasSearchDS);					
					}
						//Get from Enterprise
					else if(Utility.IsNotDBNull(strSatPickCode))
					{
						DataSet vasSearchDS1 = SysDataMgrDAL.GetCustDescSurch(appID,enterpriseID,strSatPickCode.ToString());
						if(vasSearchDS1.Tables[0].Rows.Count>0)
						{
							AddNewRowVAS1(vasSearchDS1);
						}

					}
				}
				else
				{
					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"DMS_PICKUPSAT",utility.GetUserCulture());
					return;
				}
			}

			// Sunday
			if(TMPPickupDate.DayOfWeek == System.DayOfWeek.Sunday)
			{
				DataSet vasSearchDS = SysDataMgrDAL.GetVASCustDS(this.appID,this.enterpriseID,"SUNPUP",txtCustID.Text);	

				if(strSunPick == "Y")
				{
					if( vasSearchDS.Tables[0].Rows.Count>0)
					{
						AddNewRowVAS(vasSearchDS);	
					}
						//Get from Enterprise
					else if(Utility.IsNotDBNull(strSunPickCode))
					{
						DataSet vasSearchDS1 = SysDataMgrDAL.GetCustDescSurch(appID,enterpriseID,strSunPickCode.ToString());
						if(vasSearchDS1.Tables[0].Rows.Count>0)
						{
							AddNewRowVAS1(vasSearchDS1);
						}

					}

				}
				else
				{
					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"DMS_PICKUPSUN",utility.GetUserCulture());
					return;
				}
			}

			// Monday - Friday
			if((TMPPickupDate.DayOfWeek != System.DayOfWeek.Saturday) && (TMPPickupDate.DayOfWeek != System.DayOfWeek.Sunday))
			{
				bool isPubDay = DomesticShipmentMgrDAL.IsEnterpriseHolidayOfAutoManifest(appID, enterpriseID, TMPPickupDate, tmpZip.StateCode);
				if(isPubDay)
				{
					DataSet vasSearchDS = SysDataMgrDAL.GetVASCustDS(this.appID,this.enterpriseID,"PUBPUP",txtCustID.Text);

					if(strPubPick == "Y")
					{
						if( vasSearchDS.Tables[0].Rows.Count>0)
						{
							AddNewRowVAS(vasSearchDS);
						}
							//Get from Enterprise
						else if(Utility.IsNotDBNull(strPubPickCode))
						{
							DataSet vasSearchDS1 = SysDataMgrDAL.GetCustDescSurch(appID,enterpriseID,strPubPickCode.ToString());
							if(vasSearchDS1.Tables[0].Rows.Count>0)
							{
								AddNewRowVAS1(vasSearchDS1);
							}

						}

					}
					else
					{
						lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"DMS_PICKUPHOLIDAY",utility.GetUserCulture());
						return;
					}
				}
			}

			//Delete && ADDED All VAS IN CUSTOMER 
			if(CustVASDS.Tables[0].Rows.Count > 0)
			{
				foreach(DataRow dr in CustVASDS.Tables[0].Rows)
				{
					if(((String)dr["vas_code"] != "SATPUP")&& ((String)dr["vas_code"] != "SUNPUP")  && ((String)dr["vas_code"] != "PUBPUP" ))
					{

						if(m_dsVAS.Tables[0].Select("vas_code = '"+ dr["vas_code"].ToString()+"'").Length > 0)
						{

							DataRow drCurrent = m_dsVAS.Tables[0].Select("vas_code = '"+ dr["vas_code"].ToString()+"'")[0];

							txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], Convert.ToDecimal(txtTotVASSurChrg.Text)- Convert.ToDecimal(drCurrent["surcharge"]));
							m_dsVAS.Tables[0].Select("vas_code = '"+ dr["vas_code"].ToString()+"'")[0].Delete();
							m_dsVAS.AcceptChanges();

						}
											
						if(((String)dr["vas_code"] != "SATDEL")&&((String)dr["vas_code"] != "SUNDEL")  &&((String)dr["vas_code"] != "PUBDEL"))
						{
							DataSet vasSearchDS = SysDataMgrDAL.GetVASCustDS(this.appID,this.enterpriseID,dr["vas_code"].ToString(),txtCustID.Text);	
							if( vasSearchDS.Tables[0].Rows.Count>0)
							{
								//Jeab 09 Mar 11
								if ( ViewState["OperationsVAS"].ToString() == "Insert" )
								{
									AddNewRowVAS(vasSearchDS);											
								}						
								//								AddNewRowVAS(vasSearchDS);	 
								//Jeab 09 Mar 11  =========> End
							}
						}
					}
					m_dsVAS.Tables[0].AcceptChanges();
					Session["SESSION_DS1"] = m_dsVAS;
				}
			}

		
			/////////////////////////////////////////End of Added//////////////////////////////////////////////

			//Comment By GwanG on 3-Apr-08
			//			for(int i=0;i<m_dsVAS.Tables[0].Rows.Count;i++)
			//			{
			//				DataRow drCurrent = m_dsVAS.Tables[0].Rows[i];
			//				if(((String)drCurrent["vas_code"] == "SATDEL")|| ((String)drCurrent["vas_code"] == "SUNDEL")  || ((String)drCurrent["vas_code"] == "PUBDEL")  )
			//				{
			//					txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], Convert.ToDecimal(txtTotVASSurChrg.Text)- Convert.ToDecimal(drCurrent["surcharge"]) );
			//					m_dsVAS.Tables[0].Rows.RemoveAt(i);			
			//					i--;
			//				}
			//					
			//					
			//			}
			//
			//			m_dsVAS.Tables[0].AcceptChanges();
			//			Session["SESSION_DS1"] = m_dsVAS;
			/////////////////////////////////////////End of Comment//////////////////////////////////////////////
			
			this.EstimatedDeliveryDateType =DeliveryDateType.Today;
			this.EstimatedDeliveryDateData = null;
			EstimatedDeliveryDate();
		}
		private void PrepareForNewInsert()
		{		
			Insert_Click();
			ViewState["PickupBooking"] = "No";
			txtShpSvcCode.ReadOnly = false;
			txtRecipZip.ReadOnly = false;
			Session["SESSION_DS2"] = null;
			if(lblErrorMsg.Text != "Con# " + txtConsigNo.Text + " Saved Successfully")
				lblErrorMsg.Text ="";
			Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,true,m_moduleAccessRights);
			btnExecQry.Enabled = false;
			Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete,false,m_moduleAccessRights);
			//			rbtnShipFrghtColl.Enabled = true;
			rbtnShipFrghtPre.Enabled = true;
			rbtnShipFrghtColl.Checked = false;
			rbtnShipFrghtPre.Checked = true;
			rbCash.Checked = false;
			rbCredit.Checked = true;
			txtShipManifestDt.Enabled = false;
			if(fromInsertBtn)
				ClearAllFields();

			strDt = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
			if(txtBookDate.Text == "")
				txtBookDate.Text = strDt;

			
			txtShipManifestDt.Text = strDt;
			//
			txtSpeHanInstruction.Text="";
			//
			txtLatestStatusCode.Text = TIESUtility.getInitialShipmentStatusCode(appID,enterpriseID);
			txtDelManifest.Text = TIESUtility.getInitialDlvrymanifest(appID,enterpriseID);
			chkSendCustInfo.Checked = false;
			chkRecip.Checked = false;

			if(txtShipPckUpTime.Text == "")
				txtShipPckUpTime.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
			

			decimal tmpDclrValue = 0;
			txtShpDclrValue.Text = String.Format((String)ViewState["m_format"], tmpDclrValue);

			decimal tmpCod = 0;
			txtCODAmount.Text = String.Format("{0:F2}",tmpCod);
			
			m_dsVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
			Session["SESSION_DS1"] = m_dsVAS;
			BindVASGrid();

			ViewState["AddSurcharge"] = "none";
			ViewState["DSMode"]=ScreenMode.Insert;
			ViewState["DSOperation"] = Operation.Insert;
			ViewState["currentPage"] = 0;

			txtBookingNo.Enabled = true;
			txtBookingNo.AutoPostBack = true;
			txtBookDate.Enabled = false;
			txtConsigNo.Enabled = true;
			txtConsigNo.AutoPostBack= true;
			txtDelManifest.Enabled = false;
			txtShipManifestDt.Enabled = false;
			ViewState["isTextChanged"] = false;

			DataSet commodities = CommodityDAL.GetCommodities(appID, enterpriseID,0,0).ds;

			if (commodities.Tables[0].Rows.Count == 1)
			{
				if ((commodities.Tables[0].Rows[0]["commodity_code"] != null) && (commodities.Tables[0].Rows[0]["commodity_code"].ToString() != ""))
					txtPkgCommCode.Text = commodities.Tables[0].Rows[0]["commodity_code"].ToString();
				else
					txtPkgCommCode.Text = "";

				if ((commodities.Tables[0].Rows[0]["commodity_description"] != null) && (commodities.Tables[0].Rows[0]["commodity_description"].ToString() != ""))
					txtPkgCommDesc.Text = commodities.Tables[0].Rows[0]["commodity_description"].ToString();
				else
					txtPkgCommDesc.Text = "";
			}
			SetInitialFocus(txtBookingNo);
			ViewState["PartialCon"]=false;
			ViewState["InitialBooking"] = true;
		}

		private void txtBookingNo_TextChanged(object sender, System.EventArgs e)
		{
			string ConNo="";
			if(txtConsigNo.Text.Trim().Length > 0 &&Utility.ValidateConsignmentNo(txtConsigNo.Text.Trim(),ref ConNo)==false)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSNO_INVALID",utility.GetUserCulture()); 
				SetInitialFocus(txtConsigNo);
				return;
			}
			else
			{
				txtConsigNo.Text=ConNo;
				lblErrorMsg.Text="";
			}

			if(txtConsigNo.Text.Trim() != "")
				Compare_PD_SPKG();
			
			//if ((txtBookingNo.Text.Trim().Length > 0) || ((int)ViewState["DSMode"] == (int)ScreenMode.Insert))
			if ((txtBookingNo.Text.Trim().Length > 0) && ((int)ViewState["DSMode"] == (int)ScreenMode.Insert))
			{
				ViewState["InitialBooking"] = null;
				//this.txtRecipZip.Text = "";
				BookingNoDetails();
				getCutomerData();
				ViewState["Operations"] = Operation.Insert;
			}
		}

		/// <BookingNoDetails>
		/// This method displays the details for the domestic shipment from the pickuprequest table.
		/// </BookingNoDetails>
		private void BookingNoDetails()
		{
			lblErrorMsg.Text = "";

			int iBookingNo = Convert.ToInt32(txtBookingNo.Text.Trim());
			string strConsignment = txtConsigNo.Text;

			//Clear all fields.
			if(this.txtRecipZip.Text.Length>0&&((int)ViewState["DSMode"] == (int)ScreenMode.Insert))
			{
				this.lblErrorMsg.Text="";
			}
			else
			{
				string bookingDate = "";
				string manefestDate = "";
				string actualDate = "";

				//// add by Tumz. ////
				actualDate = txtShipPckUpTime.Text;
				bookingDate = txtBookDate.Text;
				manefestDate = txtShipManifestDt.Text;

				if(ViewState["InitialBooking"] != null)
					ClearAllFields();
				txtConsigNo.Text = strConsignment;

				//// add by Tumz. ////
				txtShipPckUpTime.Text = actualDate;
				txtBookDate.Text = bookingDate;
				txtShipManifestDt.Text = manefestDate;
				txtShpDclrValue.Text = "0.00";
			}

			txtBookingNo.Text = iBookingNo.ToString();

			ViewState["DSMode"] = ScreenMode.Insert;
			
			strErrorMsg = "";
			Enterprise enterprise = null;
			Zipcode zipCode = null;
			
			DataSet dsPickUpData = DomesticShipmentMgrDAL.GetFromPickUp(appID,enterpriseID,iBookingNo);
			ViewState["DSPickupData"]  =dsPickUpData;
			int cnt = dsPickUpData.Tables[0].Rows.Count;
			if (cnt>0)
			{
				//call the method & check whether the booking_no exists in pickup_Shipment table.
				//				bool IsShipmentExist = TIESUtility.IsPickupShipmentExist(appID,enterpriseID,iBookingNo);
				//				if(!IsShipmentExist)
				//				{
				//					lblErrorMsg.Text = "Booking number does not exists";
				//					return;
				//				}
				//check whether all the serial no for the booking no. is saved in domestic shipment.
				bool IsCanCreateShpmnt = DomesticShipmentMgrDAL.IsDomesticShpCreate(appID,enterpriseID,iBookingNo);
	
				//				if(IsCanCreateShpmnt == false)
				//				{
				//					lblErrorMsg.Text = "Domestic shipment for this booking no is already created";
				//					return;
				//				}

				ViewState["PickupBooking"] = "Yes";
				txtShpSvcCode.ReadOnly = false;
				//	txtRecipZip.ReadOnly = true;

				DataRow drEach = dsPickUpData.Tables[0].Rows[0];
				DateTime dtBookingDateTime = DateTime.Now;
				if(!drEach["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					dtBookingDateTime = (DateTime)drEach["booking_datetime"];
					txtBookDate.Text = dtBookingDateTime.ToString("dd/MM/yyyy HH:mm");
				}

				String strPayerType = "";
				if(!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerType = (String)drEach["payer_type"];
					ddbCustType.SelectedItem.Selected = false;
					ddbCustType.Items.FindByValue(strPayerType).Selected = true;
				}
				
				String strPayerZip = "";
				if(!drEach["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerZip = (String)drEach["payer_zipcode"];
				}
				txtCustZipCode.Text = strPayerZip;

				String strPayerCountry = "";
				if(!drEach["payer_country"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerCountry = (String)drEach["payer_country"];
				}
				txtCustStateCode.Text = strPayerCountry;

				
				String strPayerName = "";
				if(!drEach["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerName = (String)drEach["payer_name"];
					txtCustName.Text = strPayerName;
				}
				else
				{
					txtCustName.Text = "";
				}
				
				String strPayerID = "";
				if(!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerID = (String)drEach["payerid"];
					txtCustID.Text = strPayerID;
					
					//Get the POD return 
					Customer customer = new Customer();
					customer.Populate(appID,enterpriseID,strPayerID);
					String strPOD = customer.PODSlipRequired;
					if(strPOD == "Y")
					{
						chkshpRtnHrdCpy.Checked = true;
					}
					else
					{
						chkshpRtnHrdCpy.Checked = false;
					}

					//HC Return Task
					//Get the Invoice return 
					String strInvoice = Convert.ToString(customer.hc_invoice_required);
					if(strInvoice == "Y")
					{
						chkInvHCReturn.Checked = true;
					}
					else
					{
						chkInvHCReturn.Checked = false;
					}
					//HC Return Task
				}
				else
				{
					txtCustID.Text = "";
				}
				
				String strPayerAdd1 = "";
				if(!drEach["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerAdd1 = (String)drEach["payer_address1"];
					txtCustAdd1.Text = strPayerAdd1;
				}
				else
				{
					txtCustAdd1.Text = "";
				}
				

				String strPayerAdd2 = "";
				if(!drEach["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerAdd2 = (String)drEach["payer_address2"];
					txtCustAdd2.Text = strPayerAdd2;
				}
				else
				{
					txtCustAdd2.Text = "";
				}
				

				String strPayerState = "";
				
				if(strPayerCountry.Equals(""))
				{
					enterprise = new Enterprise();
					enterprise.Populate(appID,enterpriseID);
					strPayerCountry = enterprise.Country;
				}
				zipCode = new Zipcode();
				zipCode.Populate(appID,enterpriseID,strPayerCountry,strPayerZip);
				strPayerState = zipCode.StateName;
				txtCustCity.Text = strPayerState;
				txtCustStateCode.Text = strPayerCountry;
				txtOrigin.Text = strPayerState;

				String strPayerTel = "";
				if(!drEach["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerTel = (String)drEach["payer_telephone"];
					txtCustTelephone.Text = strPayerTel;
				}
				else
				{
					txtCustTelephone.Text = "";
				}
				
				String strPayerFax = "";
				if(!drEach["payer_Fax"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strPayerFax = (String)drEach["payer_Fax"];
				}
				txtCustFax.Text = strPayerFax;

				String strPaymentMode = drEach["payment_mode"].ToString();
				if (strPaymentMode.Equals("C"))
				{
					rbCredit.Checked = false;
					rbCash.Checked = true;
				}
				else if (strPaymentMode.Equals("R"))
				{
					rbCash.Checked = false;
					rbCredit.Checked = true;
				}
				String strSenderNm = "";
				if(!drEach["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strSenderNm = (String)drEach["sender_name"];
				}
				txtSendName.Text = strSenderNm;

				String strSenderAdd1 = "";
				if(!drEach["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strSenderAdd1 = (String)drEach["sender_address1"];
				}
				txtSendAddr1.Text = strSenderAdd1;

				String strSenderAdd2 = "";
				if(!drEach["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strSenderAdd2 = (String)drEach["sender_address2"];
				}
				txtSendAddr2.Text = strSenderAdd2;
				
				String strSenderZip = "";
				if(!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strSenderZip = (String)drEach["sender_zipcode"];
				}
				txtSendZip.Text = strSenderZip;

				String strSendCountry = "";
				if(!drEach["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strSendCountry = (String)drEach["sender_country"];
				}
				txtSendState.Text = strSendCountry;
				
				String strSenderState = "";
				//call the method & get the state
				if(strSendCountry.Equals(""))
				{
					enterprise = new Enterprise();
					enterprise.Populate(appID,enterpriseID);
					strSendCountry = enterprise.Country;
				}
				zipCode = new Zipcode();
				zipCode.Populate(appID,enterpriseID,strSendCountry,strSenderZip);
				strSenderState = zipCode.StateName;
				txtSendCity.Text = strSenderState;
				txtSendState.Text = strSendCountry;

				txtDestination.Text = strSenderState;
				txtSendCuttOffTime.Text = zipCode.CuttOffTime.ToString("HH:mm");
				
				String strSendContctPerson = "";
				if(!drEach["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strSendContctPerson = (String)drEach["sender_contact_person"];
				}
				txtSendContPer.Text = strSendContctPerson;

				String strSenderTelephone = "";
				if(!drEach["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strSenderTelephone = (String)drEach["sender_telephone"];
				}
				txtSendTel.Text = strSenderTelephone;

				String strSenderFax = "";
				if(!drEach["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strSenderFax = (String)drEach["sender_fax"];
				}
				txtSendFax.Text = strSenderFax;
				
				if((drEach["act_pickup_datetime"] != null) && (!drEach["act_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtActPickup = (DateTime)drEach["act_pickup_datetime"];
					txtShipPckUpTime.Text = dtActPickup.ToString("dd/MM/yyyy HH:mm");	
				}
				else
				{
					txtShipPckUpTime.Text=System.DateTime.Now.ToString("dd/MM/yyyy HH:mm");
				}
				
				if(drEach["esa_pickup_surcharge"]!=DBNull.Value /*&& IsCalculate("original_rated_esa")*/)
				{
					decimal decESASurchrg = Convert.ToDecimal(drEach["esa_pickup_surcharge"]);
					txtESASurchrg.Text = String.Format((String)ViewState["m_format"], decESASurchrg);
				}
				String strDt = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
				//	txtBookDate.Text = strDt;
				txtShipManifestDt.Text = strDt;
				txtLatestStatusCode.Text = TIESUtility.getInitialShipmentStatusCode(appID,enterpriseID);
				txtDelManifest.Text = TIESUtility.getInitialDlvrymanifest(appID,enterpriseID);
				rbCash.Enabled = false;
				rbCredit.Enabled = false;
			}
			else
			{
				ViewState["PickupBooking"] = "No";
				txtShpSvcCode.ReadOnly = false;
				txtRecipZip.ReadOnly = false;
				//				rbCash.Enabled = true;
				//				rbCredit.Enabled = true;
				rbCash.Enabled = false;
				rbCredit.Enabled = false;
				if(!(bool)ViewState["PartialCon"])
					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_BKG_NO",utility.GetUserCulture());
				return;
			}
		}
		
		private void chkCustInfo_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkNewCust.Checked == true)
			{
				rbCredit.Checked = false;
				rbCash.Checked = true;
				txtCustID.Enabled = false;
				btnDisplayCustDtls.Enabled = false;
				txtCustID.Text = "NEW";
				txtCustName.Text = "";
				txtCustAdd1.Text = "";
				txtCustAdd2.Text = "";
				txtCustStateCode.Text = "";
				txtCustZipCode.Text = "";
				txtCustTelephone.Text = "";
				txtCustFax.Text = "";
				ddbCustType.SelectedItem.Selected = false;
				ddbCustType.Items.FindByValue("C").Selected = true;
			}
			else
			{
				rbCash.Checked = false;
				rbCredit.Checked = true;
				txtCustID.Enabled = true;
				btnDisplayCustDtls.Enabled = true;
			}
		}

		private void chkSendCustInfo_CheckedChanged(object sender, System.EventArgs e)
		{
			//ViewState["isTextChanged"]=true;
			CopyCustInfoToSender();	
			//CalculateAllCharge();
		}

		private void chkRecip_CheckedChanged(object sender, System.EventArgs e)
		{
			CopyCustInfoToRecip();
			//CalculateAllCharge();
		}

		/// <CopyCustInfoToSender>
		/// This method copies all the information from the Customer section to the Sender section
		/// </CopyCustInfoToSender>
		private void CopyCustInfoToSender()
		{
			if(chkSendCustInfo.Checked)
			{
				txtSendName.Text	= txtCustName.Text.Trim();
				txtSendAddr1.Text	= txtCustAdd1.Text.Trim();
				txtSendAddr2.Text	= txtCustAdd2.Text.Trim();
				txtSendZip.Text		= txtCustZipCode.Text.Trim();
				txtSendCity.Text	= txtCustCity.Text.Trim();
				txtSendState.Text	= txtCustStateCode.Text.Trim();

				Customer customer = new Customer();
				customer.Populate(appID,enterpriseID,txtCustID.Text.Trim());
				txtSendContPer.Text = customer.ContactPerson;
				//txtSendContPer.Text = txtCustID.Text.Trim();
				txtSendTel.Text		= txtCustTelephone.Text.Trim();
				txtSendFax.Text		= txtCustFax.Text.Trim();
				if((txtSendState.Text.Trim().Length>0) && (txtCustZipCode.Text.Trim().Length>0))
				{
					Zipcode zipCode = new Zipcode();
					zipCode.Populate(appID,enterpriseID,txtSendState.Text.Trim(),txtCustZipCode.Text.Trim());
					txtSendCity.Text = zipCode.StateName;
					txtSendCuttOffTime.Text = zipCode.CuttOffTime.ToString("HH:mm");
					txtOrigin.Text = txtSendCity.Text;
				}
			}
			else
			{
				txtSendName.Text	= "";
				txtSendAddr1.Text	= "";
				txtSendAddr2.Text	= "";
				txtSendZip.Text		= "";
				txtSendCity.Text	= "";
				txtSendState.Text	= "";
				txtSendContPer.Text = "";
				txtSendTel.Text		= "";
				txtSendFax.Text		= "";
				txtSendCuttOffTime.Text  = "";
				txtSendContPer.Text = "";
				txtOrigin.Text = "";
			}
		}

		/// <CopyCustInfoToRecip>
		/// This method copies all the information from the Customer section to the Recipient section
		/// </CopyCustInfoToRecip>
		private void CopyCustInfoToRecip()
		{
			if(chkRecip.Checked)
			{
				//txtRecName.Text		= txtCustName.Text.Trim();
				txtRecName.Text		= txtCustName.Text.Trim();
				txtRecipAddr1.Text	= txtCustAdd1.Text.Trim();
				txtRecipAddr2.Text	= txtCustAdd2.Text.Trim();
				txtRecipZip.Text	= txtCustZipCode.Text.Trim();
				txtRecipCity.Text	= txtCustCity.Text.Trim();
				txtRecipState.Text	= txtCustStateCode.Text.Trim();
				//txtRecpContPer.Text = txtCustID.Text.Trim();
				Customer customer = new Customer();
				customer.Populate(appID,enterpriseID,txtCustID.Text.Trim());
				txtSendContPer.Text = customer.ContactPerson;
				txtRecipTel.Text	= txtCustTelephone.Text.Trim();
				txtRecipFax.Text	= txtCustFax.Text.Trim();
				txtDestination.Text = txtRecipCity.Text;

				Enterprise enterprise = new Enterprise();
				enterprise.Populate(appID,enterpriseID);
				String strPayerCountry = enterprise.Country;

				Zipcode zipCode = new Zipcode();
				zipCode.Populate(appID,enterpriseID,strPayerCountry,txtRecipZip.Text.Trim());
				if(zipCode.StateName == null)
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECIP_ZIP",utility.GetUserCulture());
					txtRecipCity.Text = "";
					txtRecipState.Text = "";
					txtDestination.Text = "";
					txtRouteCode.Text = "";
					txtRecipZip.Text = "";
					return;
				}
				txtRouteCode.Text = zipCode.DeliveryRoute;
			}
			else
			{
				txtRecName.Text		= "";
				txtRecipAddr1.Text	= "";
				txtRecipAddr2.Text	= "";
				txtRecipZip.Text	= "";
				txtRecipCity.Text	= "";
				txtRecipState.Text	= "";
				txtRecpContPer.Text = "";
				txtRecipTel.Text	= "";
				txtRecipFax.Text	= "";
				txtDestination.Text = "";
				txtRecpContPer.Text = "";
			}
		}

		private void txtCustZipCode_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
			
			//if((txtCustZipCode.Text.Length > 0) && ((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSMode"]!=(int)ScreenMode.Query))
			//{
			lblErrorMsg.Text = "";

			//Get the State & Country
			Enterprise enterprise = new Enterprise();
			enterprise.Populate(appID,enterpriseID);
			String strPayerCountry = enterprise.Country;
		
			Zipcode zipCode = new Zipcode();
			zipCode.Populate(appID,enterpriseID,strPayerCountry,txtCustZipCode.Text.Trim());
			if(zipCode.StateName == null)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_CUST_ZIP",utility.GetUserCulture());
				txtCustCity.Text = "";
				txtCustStateCode.Text = "";
				return;
			}
			txtCustCity.Text = zipCode.StateName;
			txtCustStateCode.Text = strPayerCountry;
			//}
		}

		private void txtSendZip_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
			//if((txtSendZip.Text.Length > 0) && ((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSMode"]!=(int)ScreenMode.Query))
			//{
			//Get the State & Country
			Enterprise enterprise = new Enterprise();
			enterprise.Populate(appID,enterpriseID);
			String strPayerCountry = enterprise.Country;
		
			Zipcode zipCode = new Zipcode();
			zipCode.Populate(appID,enterpriseID,strPayerCountry,txtSendZip.Text.Trim());
			if(zipCode.StateName == null)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_SEND_ZIP",utility.GetUserCulture());
				txtSendCity.Text = "";
				txtSendState.Text = "";
				txtSendCuttOffTime.Text = "";
				txtOrigin.Text = "";
				txtSendZip.Text = "";
				return;
			}
			txtSendCity.Text = zipCode.StateName;
			txtSendState.Text = strPayerCountry;
			txtSendCuttOffTime.Text = zipCode.CuttOffTime.ToString("HH:mm");
			txtOrigin.Text = txtSendCity.Text;

			//calCulateESA(); 
			//calCulateESA_ServiceType();
			this.CalculateAllCharge();



		}
		public void getCutomerData()
		{
			String strCustPaymentType = null;
			String strPOD = null;
			String strInvoice = null;
			lblErrorMsg.Text = "";
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if (PRBMgrDAL.isActiveCustomer(appID,enterpriseID,txtCustID.Text.Trim()) == false)
			{
				lblErrorMsg.Text= "Inactive customer ID. AAAS";
	
				txtCustName.Text = "";
				txtCustAdd1.Text = "";
				txtCustAdd2.Text = "";
				txtCustFax.Text = "";
				txtCustTelephone.Text = "";
				txtCustZipCode.Text = "";
				txtCustCity.Text = "";
				txtCustStateCode.Text = "";
				//Added By Tom 22/7/09
				txtSpeHanInstruction.Text = "";
				//End Added By Tom 22/7/09
	
				return;
			}			
			
			Customer customer = new Customer();
			customer.Populate(appID,enterpriseID,txtCustID.Text.Trim());
			txtCustName.Text = customer.CustomerName;
			txtCustAdd1.Text = customer.CustomerAddress1;
			txtCustAdd2.Text = customer.CustomerAddress2;
			txtCustFax.Text = customer.CustomerFax;
			txtCustTelephone.Text = customer.CustomerTelephone;
			txtCustZipCode.Text = customer.ZipCode;
			//Added By Tom 22/7/09
			txtSpeHanInstruction.Text = customer.special_instruction;
			//End Added By Tom 22/7/09

			//By Aoo 15/02/2008
			if(customer.insurance_percent_surcharge != null)
			{
				strCusPerSurcharge = customer.insurance_percent_surcharge.ToString();
			}
			else
			{
				strCusPerSurcharge = "";
			}
			if(customer.insurance_maximum_amt != null)
			{
				strCusFree_Coverage = customer.free_insurance_amt.ToString();
			}
			else
			{
				strCusFree_Coverage = "";
			}
			if(customer.insurance_maximum_amt != null)
			{
				strCusMax_Coverage = customer.insurance_maximum_amt.ToString();
			}
			else
			{
				strCusMax_Coverage = "";
			}
			if(customer.OtherSurchargeAmount != null)
			{
				this.txtOtherSurcharge.Text = customer.OtherSurchargeAmount;
			}
			else
			{
				strCusOtherSurchargeAmt = "";
				this.txtOtherSurcharge.Text = "";
			}

			//call the method to get the country & state code
			Enterprise enterprise = new Enterprise();
			enterprise.Populate(appID,enterpriseID);
			String strCountry = enterprise.Country;

			Zipcode zipCode = new Zipcode();
			zipCode.Populate(appID,enterpriseID,strCountry,txtCustZipCode.Text);
			String strState = zipCode.StateName;
			txtCustCity.Text = strState;
			txtCustStateCode.Text = strCountry;
			
			if(enterprise.Insurance_Percent_Surcharge != null)
			{
				strEntPerSurcharge = enterprise.Insurance_Percent_Surcharge;
			}
			else
			{
				strEntPerSurcharge = "";
			}
			if(enterprise.Free_Insurance_Amt != null)
			{
				strEntFree_Coverage = enterprise.Free_Insurance_Amt;
			}
			else
			{
				strEntFree_Coverage = "";
			}
			if(enterprise.Max_Insurance_Amt != null )
			{
				strEntMax_Coverage = enterprise.Max_Insurance_Amt;
			}
			else
			{
				strEntMax_Coverage = "";
			}
			

			if(strCusPerSurcharge.Equals(""))
			{
				strUsePerSurcharge = strEntPerSurcharge;
				txtShpAddDV.Text = strUsePerSurcharge;
			}
			else
			{
				strUsePerSurcharge = strCusPerSurcharge;
				txtShpAddDV.Text = strUsePerSurcharge;
			}

			if(strCusFree_Coverage.Equals(""))
			{
				strUseFree_Coverage = strEntFree_Coverage;
				Session["Free_Coverage"] = strUseFree_Coverage;
			}
			else
			{
				strUseFree_Coverage = strCusFree_Coverage;
				Session["Free_Coverage"] = strUseFree_Coverage;
			}

			if(strCusMax_Coverage.Equals(""))
			{
				strUseMax_Coverage = strEntMax_Coverage;
				txtShpMaxCvrg.Text = strUseMax_Coverage;
			}
			else
			{
				strUseMax_Coverage = strCusMax_Coverage;
				txtShpMaxCvrg.Text = String.Format((String)ViewState["m_format"],Convert.ToDecimal(strUseMax_Coverage));
			}


			
			txtShpAddDV.Text = strUsePerSurcharge;
			txtShpMaxCvrg.Text = String.Format((String)ViewState["m_format"],Convert.ToDecimal(strUseMax_Coverage));
			//-----------

			SessionDS DSCust = SysDataMgrDAL.GetCustInforByCustID(appID,enterpriseID, 0, 0, txtCustID.Text.Trim());
			if (DSCust.ds.Tables[0].Rows.Count > 0)
			{
				// by sittichai 08/02/2008
				ddbCustType.SelectedIndex = ddbCustType.Items.IndexOf(ddbCustType.Items.FindByValue(customer.payer_type.ToString()));

			}
			//			strCustPaymentType = (String)customer.payer_type;
			strCustPaymentType = (String)customer.PaymentMode;
			//Response.Write("<script>alert('"+strCustPaymentType+"');</script>");
			if(ViewState["DSPickupData"]!=null)
			{
				DataSet dsPickupData = (DataSet)ViewState["DSPickupData"];
				if(dsPickupData.Tables[0].Rows.Count>0)
				{
					strCustPaymentType = dsPickupData.Tables[0].Rows[0]["Payment_mode"].ToString();
				}
				ViewState["DSPickupData"]=null;
			}
			if(strCustPaymentType == "C")
			{
				this.rbCash.Checked = true;
				this.rbCredit.Checked = false;
				this.rbCash.Enabled = false;
				this.rbCredit.Enabled = false;
			}
			else if(strCustPaymentType == "R")
			{
				this.rbCredit.Checked = true;
				this.rbCash.Checked = false;
				//				this.rbCash.Enabled = true;
				//				this.rbCredit.Enabled = true;
				rbCash.Enabled = false;
				rbCredit.Enabled = false;
			}
			if(txtBookingNo.Text.Trim()!="")
			{
				int iBookingNo = Convert.ToInt32(txtBookingNo.Text.Trim());
				DataSet dsPickUpData = DomesticShipmentMgrDAL.GetFromPickUp(appID,enterpriseID,iBookingNo);
				int cnt = dsPickUpData.Tables[0].Rows.Count;
				if(cnt<=0)
				{
					//					this.rbCash.Enabled = true;
					//					this.rbCredit.Enabled = true;
					rbCash.Enabled = false;
					rbCredit.Enabled = false;
				}
				else
				{
					this.rbCash.Enabled = false;
					this.rbCredit.Enabled = false;
				}
			}			
			//Get the POD return 
			
			
			strPOD = customer.PODSlipRequired;
			if(strPOD == "Y")
			{
				chkshpRtnHrdCpy.Checked = true;
			}
			else
			{
				chkshpRtnHrdCpy.Checked = false;
			}


			//HC Return Task
			//Get the Invoice return 
			strInvoice = Convert.ToString(customer.hc_invoice_required);
			if(strInvoice == "Y")
			{
				chkInvHCReturn.Checked = true;
			}
			else
			{
				chkInvHCReturn.Checked = false;
			}
			//HC Return Task

			//CalculateAllCharge();
		}

		/// <summary>
		/// ////////// Create by Tumz. /////////////////////////////////////////////////////
		/// </summary>
		private void Compare_PD_SPKG()
		{
			if ((((((ScreenMode)ViewState["DSMode"] == ScreenMode.Insert) && ((Operation)ViewState["DSOperation"] == Operation.Insert)) || ((ScreenMode)ViewState["DSMode"] == ScreenMode.Insert) && ((Operation)ViewState["DSOperation"] == Operation.Saved)) || (((ScreenMode)ViewState["DSMode"] == ScreenMode.Insert) && ((Operation)ViewState["DSOperation"] == Operation.Update))) || (((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery) && ((Operation)ViewState["DSOperation"] == Operation.None)))
			{
				int bookNo = 0;
				if(txtBookingNo.Text.Trim() != "")
					bookNo = Convert.ToInt32(txtBookingNo.Text.Trim());
				DataSet dsShipmentPKG = DomesticShipmentMgrDAL.GetShipmentPKGForCompare(appID,enterpriseID, bookNo, txtConsigNo.Text.Trim());
				DataSet dsPakageDetail = null;
				DataSet dsQryPkg = null;

				bool fNULL_dsPakageDetail = false;
				bool fNULL_dsShipmentPKG = false;

				if(dsShipmentPKG == null)
					fNULL_dsShipmentPKG = true;

				String custid ;
				try
				{
					dsPakageDetail = DomesticShipmentMgrDAL.GetEmptyPkgDtlDS();
					//call the method for getting the package details during query
					if(txtCustID.Text == "")
						custid = null;
					else
						custid = txtCustID.Text;
					dsQryPkg = DomesticShipmentMgrDAL.GetPakageDtlsSWB(appID ,enterpriseID ,txtConsigNo.Text ,custid);
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					lblErrorMsg.Text = strMsg;
					return;
				}
				if(dsQryPkg != null)
				{
					dsPakageDetail = dsQryPkg;
					Session["SESSION_DS2"] = dsPakageDetail;
					m_dsPkgDetails = dsPakageDetail;
				}
				else
					fNULL_dsPakageDetail = true;

				
				//select table for show total
				decimal tot_act_wt = 0;
				decimal tot_wt = 0;  //round
				decimal pkg_qty = 0;
				decimal tot_dim_wt = 0;
				decimal chargeable_wt = 0;
				decimal pkg_volume = 0;


				//ds from pakage detail
				if((fNULL_dsShipmentPKG == false && fNULL_dsPakageDetail == false) || (fNULL_dsShipmentPKG == true && fNULL_dsPakageDetail == false))
				{
					if((fNULL_dsShipmentPKG == false && fNULL_dsPakageDetail == false) && ((((ScreenMode)ViewState["DSMode"] == ScreenMode.Insert) && ((Operation)ViewState["DSOperation"] == Operation.Update)) || (((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery) && ((Operation)ViewState["DSOperation"] == Operation.None))))
					{
						if(dsShipmentPKG != null)
						{
							Session["SESSION_DS2"] = dsShipmentPKG;
							m_dsPkgDetails = dsShipmentPKG;

							GetGridValuesTotal(dsShipmentPKG);
							for(int i=0; i< dsShipmentPKG.Tables[0].Rows.Count; i++)
							{
								tot_act_wt += decimal.Parse(dsShipmentPKG.Tables[0].Rows[i]["tot_act_wt"].ToString());
								tot_wt += decimal.Parse(dsShipmentPKG.Tables[0].Rows[i]["tot_wt"].ToString());
								pkg_qty += decimal.Parse(dsShipmentPKG.Tables[0].Rows[i]["pkg_qty"].ToString());
								tot_dim_wt += decimal.Parse(dsShipmentPKG.Tables[0].Rows[i]["tot_dim_wt"].ToString());
								chargeable_wt += decimal.Parse(dsShipmentPKG.Tables[0].Rows[i]["chargeable_wt"].ToString());
								pkg_volume += decimal.Parse(dsShipmentPKG.Tables[0].Rows[i]["pkg_volume"].ToString());
							}

							txtActualWeight.Text = tot_act_wt.ToString("#,##0.00");
							txtPkgActualWt.Text = tot_wt.ToString("#,##0");
							txtPkgTotpkgs.Text = pkg_qty.ToString("#,##0");
							txtPkgDimWt.Text = tot_dim_wt.ToString("#,##0");
							txtPkgChargeWt.Text = chargeable_wt.ToString("#,##0");
							txttotVol.Text = pkg_volume.ToString("#,##0");
						}
						else
							ClearTotal();
					}
					else if(dsPakageDetail != null)
					{
						GetGridValuesTotal(dsPakageDetail);
						for(int i=0;i < dsPakageDetail.Tables[0].Rows.Count;i++)
						{
							tot_act_wt += decimal.Parse(dsPakageDetail.Tables[0].Rows[i]["tot_act_wt"].ToString());
							tot_wt += decimal.Parse(dsPakageDetail.Tables[0].Rows[i]["tot_wt"].ToString());
							pkg_qty += decimal.Parse(dsPakageDetail.Tables[0].Rows[i]["pkg_qty"].ToString());
							tot_dim_wt += decimal.Parse(dsPakageDetail.Tables[0].Rows[i]["tot_dim_wt"].ToString());
							chargeable_wt += decimal.Parse(dsPakageDetail.Tables[0].Rows[i]["chargeable_wt"].ToString());
							pkg_volume += decimal.Parse(dsPakageDetail.Tables[0].Rows[i]["pkg_volume"].ToString());
						}

						txtActualWeight.Text = tot_act_wt.ToString("#,##0.00");
						txtPkgActualWt.Text = tot_wt.ToString("#,##0");
						txtPkgTotpkgs.Text = pkg_qty.ToString("#,##0");
						txtPkgDimWt.Text = tot_dim_wt.ToString("#,##0");
						txtPkgChargeWt.Text = chargeable_wt.ToString("#,##0");
						txttotVol.Text = pkg_volume.ToString("#,##0");
					}
					else
						ClearTotal();
					
				}
				else if(fNULL_dsShipmentPKG == false)
				{
					if(dsShipmentPKG != null)
					{
						Session["SESSION_DS2"] = dsShipmentPKG;
						m_dsPkgDetails = dsShipmentPKG;

						GetGridValuesTotal(dsShipmentPKG);
						for(int i=0; i< dsShipmentPKG.Tables[0].Rows.Count; i++)
						{
							tot_act_wt += decimal.Parse(dsShipmentPKG.Tables[0].Rows[i]["tot_act_wt"].ToString());
							tot_wt += decimal.Parse(dsShipmentPKG.Tables[0].Rows[i]["tot_wt"].ToString());
							pkg_qty += decimal.Parse(dsShipmentPKG.Tables[0].Rows[i]["pkg_qty"].ToString());
							tot_dim_wt += decimal.Parse(dsShipmentPKG.Tables[0].Rows[i]["tot_dim_wt"].ToString());
							chargeable_wt += decimal.Parse(dsShipmentPKG.Tables[0].Rows[i]["chargeable_wt"].ToString());
							pkg_volume += decimal.Parse(dsShipmentPKG.Tables[0].Rows[i]["pkg_volume"].ToString());
						}

						txtActualWeight.Text = tot_act_wt.ToString("#,##0.00");
						txtPkgActualWt.Text = tot_wt.ToString("#,##0");
						txtPkgTotpkgs.Text = pkg_qty.ToString("#,##0");
						txtPkgDimWt.Text = tot_dim_wt.ToString("#,##0");
						txtPkgChargeWt.Text = chargeable_wt.ToString("#,##0");
						txttotVol.Text = pkg_volume.ToString("#,##0");
					}
					else
						ClearTotal();
					
				}
				else
					ClearTotal();
				
			}
		}

		private void GetGridValuesTotal(DataSet dsPkg)
		{
			Customer customer = new Customer();
			customer.Populate(appID,enterpriseID,txtCustID.Text);
			int cnt = dsPkg.Tables[0].Rows.Count;
			int i = 0;

			decimal decTotDimWt = 0;
			decimal decTotWt = 0;
			decimal  TOTVol = 0; 
			decimal decTot_act_Wt = 0;
			int iTotPkgs = 0;
			decimal decChrgWt = 0;


			//Jeab 28 Dec 10
			if (customer.Dim_By_tot =="Y")
			{
				decTotDimWt = 0;
				decTotWt = 0;
				for(i = 0;i<cnt;i++)
				{
					DataRow drEach = dsPkg.Tables[0].Rows[i];
					decTotDimWt += (decimal)drEach["tot_dim_wt"];
					decTotWt += (decimal)drEach["tot_wt"];
					if (drEach["tot_act_wt"]== System.DBNull.Value )
					{
						drEach["tot_act_wt"]= 0	;
					}
					decTot_act_Wt += (decimal)drEach["tot_act_wt"];
					iTotPkgs += (int)drEach["pkg_qty"];
					
					//					if (drEach["pkg_TOTvolume"] != null)
					//					{
					//						TOTVol += (decimal)drEach["pkg_TOTvolume"];
					//					}
					//					else
					//					{
					TOTVol += Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]); //Jeab 22 Feb 2011
					//					}
					//edit by Tumz.
					drEach["pkg_volume"] = Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]);;
				}

				if(decTotDimWt < decTotWt)  // �� decTotWt  ᷹ decTot_act_Wt (�������Ѻ������͹���)  Jeab 28 Feb 2011
				{
					decChrgWt = decTotWt;  // �� decTotWt  ᷹ decTot_act_Wt  (�������Ѻ������͹���)    Jeab 28 Feb 2011
					for(i = 0;i<cnt;i++)
					{
						DataRow drEach = dsPkg.Tables[0].Rows[i];
						drEach["chargeable_wt"] = drEach["tot_wt"];
					}
				}
				else
				{
					decChrgWt = decTotDimWt;
					for(i = 0;i<cnt;i++)
					{
						DataRow drEach = dsPkg.Tables[0].Rows[i];
						drEach["chargeable_wt"] = drEach["tot_dim_wt"];
					}
				}
			}
			else
			{
				for(i = 0;i<cnt;i++)
				{
					DataRow drEach = dsPkg.Tables[0].Rows[i];
					decTotDimWt += (decimal)drEach["tot_dim_wt"];
					decTotWt += (decimal)drEach["tot_wt"];
					//TU on 17June08
					if (drEach["tot_act_wt"]== System.DBNull.Value )
					{
						drEach["tot_act_wt"]= 0	;
					}
					decTot_act_Wt += (decimal)drEach["tot_act_wt"];
					TOTVol += Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]); //Jeab 22 Feb 2011

					//edit by Tumz.
					drEach["pkg_volume"] = Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]);

					iTotPkgs += (int)drEach["pkg_qty"];
					//decChrgWt += (decimal)drEach["chargeable_wt"];
					if(customer.ApplyDimWt == "Y" || customer.ApplyDimWt == "")
					{
						if((decimal)drEach["tot_wt"] < (decimal)drEach["tot_dim_wt"])
						{
							decChrgWt +=  (decimal)drEach["tot_dim_wt"];
							drEach["chargeable_wt"] = drEach["tot_dim_wt"];
						}
						else
						{
							decChrgWt += (decimal)drEach["tot_wt"];
							drEach["chargeable_wt"] = drEach["tot_wt"];
						}
					}
					else if(customer.ApplyDimWt == "N")
					{
						decChrgWt += (decimal)drEach["tot_wt"];
						drEach["chargeable_wt"] = drEach["tot_wt"];
					}
				}
			}
		}

		private void ClearTotal()
		{
			txtActualWeight.Text = "";
			txtPkgActualWt.Text = "";
			txtPkgTotpkgs.Text = "";
			txtPkgDimWt.Text = "";
			txtPkgChargeWt.Text = "";
			txttotVol.Text = "";
		}


		private void txtCustID_TextChanged(object sender, System.EventArgs e)
		{
			getCutomerData();
			if(txtCustID.Text == "CASH")
			{
				chkNewCust.Checked = false;
				chkNewCust.Enabled = false;
			}
			else
			{
				chkNewCust.Enabled = true;
			}

			Compare_PD_SPKG();
			CalculateInsuranceSurcharge();
			CalculateCODVAS();
			CalculateTotalAmt();
			//Removed by CRTS 954 Turn off Locking from over Credit Limit

			//			if(CustomerProfileDAL.Check_IsCreditStatusNotAvail(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text.Trim()))
			//			{		
			//			
			//						 this.btnSave.Enabled=false;
			//						 lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_PRB_CRSTATUS_NA", utility.GetUserCulture());
			//
			//			#region All Comment
			//			//			String strCustPaymentType = null;
			//			//			String strPOD = null;
			//			//			String strInvoice = null;
			//			//			lblErrorMsg.Text = "";
			//			//			if((bool)ViewState["isTextChanged"] == false)
			//			//			{
			//			//				ChangeDSState();
			//			//				ViewState["isTextChanged"] = true;
			//			//			}
			//			//			
			//			//			if (PRBMgrDAL.isActiveCustomer(appID,enterpriseID,txtCustID.Text.Trim()) == false)
			//			//			{
			//			//				lblErrorMsg.Text= "Inactive customer ID.";
			//			//	
			//			//				txtCustName.Text = "";
			//			//				txtCustAdd1.Text = "";
			//			//				txtCustAdd2.Text = "";
			//			//				txtCustFax.Text = "";
			//			//				txtCustTelephone.Text = "";
			//			//				txtCustZipCode.Text = "";
			//			//				txtCustCity.Text = "";
			//			//				txtCustStateCode.Text = "";
			//			//	
			//			//				return;
			//			//			}
			//			//
			//			//			string strCusPerSurcharge;
			//			//			string strCusFree_Coverage;
			//			//			string strCusMax_Coverage;
			//			//
			//			//			string strEntPerSurcharge;
			//			//			string strEntFree_Coverage;
			//			//			string strEntMax_Coverage;
			//			//
			//			//			string strUsePerSurcharge;
			//			//			string strUseFree_Coverage;
			//			//			string strUseMax_Coverage;
			//			//
			//			//			Customer customer = new Customer();
			//			//			customer.Populate(appID,enterpriseID,txtCustID.Text.Trim());
			//			//			txtCustName.Text = customer.CustomerName;
			//			//			txtCustAdd1.Text = customer.CustomerAddress1;
			//			//			txtCustAdd2.Text = customer.CustomerAddress2;
			//			//			txtCustFax.Text = customer.CustomerFax;
			//			//			txtCustTelephone.Text = customer.CustomerTelephone;
			//			//			txtCustZipCode.Text = customer.ZipCode;
			//			//
			//			//			//By Aoo 15/02/2008
			//			//
			//			//			strCusPerSurcharge = customer.insurance_percent_surcharge.ToString();
			//			//			strCusFree_Coverage = customer.free_insurance_amt.ToString();
			//			//			strCusMax_Coverage = customer.insurance_maximum_amt.ToString();
			//			////
			//			////			//End By Aoo
			//			////		
			//			////	//-----		//call the method to get the country & state code
			//			//			Enterprise enterprise = new Enterprise();
			//			//			enterprise.Populate(appID,enterpriseID);
			//			//			String strCountry = enterprise.Country;
			//			//
			//			//			Zipcode zipCode = new Zipcode();
			//			//			zipCode.Populate(appID,enterpriseID,strCountry,txtCustZipCode.Text);
			//			//			String strState = zipCode.StateName;
			//			//			txtCustCity.Text = strState;
			//			//			txtCustStateCode.Text = strCountry;
			//			//
			//			//
			//			//			
			//			//
			//			//			strEntPerSurcharge = enterprise.Insurance_Percent_Surcharge.ToString();
			//			//			strEntFree_Coverage = enterprise.Free_Insurance_Amt.ToString();
			//			//			strEntMax_Coverage = enterprise.Max_Insurance_Amt.ToString();
			//			//			
			//			//
			//			//			if(strCusPerSurcharge.Equals(""))
			//			//			{
			//			//				strUsePerSurcharge = strEntPerSurcharge;
			//			//				txtShpAddDV.Text = strUsePerSurcharge;
			//			//			}
			//			//			else
			//			//			{
			//			//				strUsePerSurcharge = strCusPerSurcharge;
			//			//				txtShpAddDV.Text = strUsePerSurcharge;
			//			//			}
			//			//
			//			//			if(strCusFree_Coverage.Equals(""))
			//			//			{
			//			//				strUseFree_Coverage = strEntFree_Coverage;
			//			//				Session["Free_Coverage"] = strUseFree_Coverage;
			//			//			}
			//			//			else
			//			//			{
			//			//				strUseFree_Coverage = strCusFree_Coverage;
			//			//				Session["Free_Coverage"] = strUseFree_Coverage;
			//			//			}
			//			//
			//			//			if(strCusMax_Coverage.Equals(""))
			//			//			{
			//			//				strUseMax_Coverage = strEntMax_Coverage;
			//			//				txtShpMaxCvrg.Text = strUseMax_Coverage;
			//			//			}
			//			//			else
			//			//			{
			//			//				strUseMax_Coverage = strCusMax_Coverage;
			//			//				txtShpMaxCvrg.Text = strUseMax_Coverage;
			//			//			}
			//			//
			//			////			// Test By Aoo
			//			//			Response.Write("<script>alert('Insurance Percentage from Customer : "+strCusPerSurcharge+"');</script>");
			//			//			Response.Write("<script>alert('Free Coverage from Customer : "+strCusFree_Coverage+"');</script>");
			//			//			Response.Write("<script>alert('Max Caverage from Customer : "+strCusMax_Coverage+"');</script>");
			//			//
			//			//			Response.Write("<script>alert('Insurance Percentage from Enterprise : "+strEntPerSurcharge+"');</script>");
			//			//			Response.Write("<script>alert('Free Coverage from Enterprise : "+strEntFree_Coverage+"');</script>");
			//			//			Response.Write("<script>alert('Max Caverage from Enterprise : "+strEntMax_Coverage+"');</script>");
			//			//			
			//			//			Response.Write("<script>alert('Insurance Percentage of final : "+strUsePerSurcharge+"');</script>");
			//			//			Response.Write("<script>alert('Free Coverage of final : "+strUseFree_Coverage+"');</script>");
			//			//			Response.Write("<script>alert('Max Caverage of final : "+strUseMax_Coverage+"');</script>");
			//			//			// End Test By Aoo.
			//			//			
			//			//			txtShpAddDV.Text = strUsePerSurcharge;
			//			//			txtShpMaxCvrg.Text = strUseMax_Coverage;
			//			//		//-----------
			//			//
			//			//			SessionDS DSCust = SysDataMgrDAL.GetCustInforByCustID(appID,enterpriseID, 0, 0, txtCustID.Text.Trim());
			//			//
			//			//			if (DSCust.ds.Tables[0].Rows.Count > 0)
			//			//			{
			//			//				// by sittichai 08/02/2008
			//			//				ddbCustType.SelectedIndex = ddbCustType.Items.IndexOf(ddbCustType.Items.FindByValue(customer.payer_type.ToString()));
			//			//
			//			//			}
			//			//			strCustPaymentType = (String)customer.payer_type;
			//			//			if(strCustPaymentType == "C")
			//			//			{
			//			//				this.rbCash.Checked = true;
			//			//				this.rbCredit.Checked = false;
			//			//				this.rbCash.Enabled = false;
			//			//				this.rbCredit.Enabled = false;
			//			//			}
			//			//			else
			//			//			{
			//			//				this.rbCredit.Checked = true;
			//			//				this.rbCash.Checked = false;
			//			//				this.rbCash.Enabled = true;
			//			//				this.rbCredit.Enabled = true;
			//			//			}
			//			//		
			//			//			//Get the POD return 
			//			//			
			//			//			
			//			//			strPOD = customer.PODSlipRequired;
			//			//			if(strPOD == "Y")
			//			//			{
			//			//				chkshpRtnHrdCpy.Checked = true;
			//			//			}
			//			//			else
			//			//			{
			//			//				chkshpRtnHrdCpy.Checked = false;
			//			//			}
			//			//
			//			//
			//			//			//HC Return Task
			//			//			//Get the Invoice return 
			//			//			strInvoice = Convert.ToString(customer.hc_invoice_required);
			//			//			if(strInvoice == "Y")
			//			//			{
			//			//				chkInvHCReturn.Checked = true;
			//			//			}
			//			//			else
			//			//			{
			//			//				chkInvHCReturn.Checked = false;
			//			//			}
			//			//			//HC Return Task
			//
			//			#endregion
			//		}
			//End CRTS 954 Turn off Locking from over Credit Limit

		}

		private void btnDisplayCustDtls_Click(object sender, System.EventArgs e)
		{
			strErrorMsg = "";			
			if(btnSave.Enabled)
			{
				if(ddbCustType.SelectedItem.Value == "0" && ddbCustType.Visible == true)
				{
					strErrorMsg = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_CUST_TYPE",utility.GetUserCulture());
					lblErrorMsg.Text=strErrorMsg;
				}
				else
				{
					poppulateCustomerAddress();
				}

				//				else if(txtBookingNo.Enabled==false)
				//				{
				//					divCustIdCng.Visible=true;
				//					DomstcShipPanel.Visible=false;
				//					divMain.Visible=false;
				//					divDelOperation.Visible=false;
				//				}
			}


		}

		private void btnPkgDetails_Click(object sender, System.EventArgs e)
		{
			string ConNo="";
			if(txtConsigNo.Text.Trim().Length > 0 &&Utility.ValidateConsignmentNo(txtConsigNo.Text.Trim(),ref ConNo)==false)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSNO_INVALID",utility.GetUserCulture()); 
				SetInitialFocus(txtConsigNo);
				return;
			}
			else
			{
				txtConsigNo.Text=ConNo;
				lblErrorMsg.Text="";
			}

			if(((int)ViewState["DSMode"] == (int)ScreenMode.ExecuteQuery) && (txtBookingNo.Text.Trim().Length > 0) /*&& ((int)ViewState["DSOperation"] != (int)Operation.Update)*/)
			{
				DataSet dsQryPkg = null;
				DataSet dsShipmentPKG = null;
				try
				{
					//call the method for getting the package details or package pkg during query
					if(((((ScreenMode)ViewState["DSMode"] == ScreenMode.Insert) && ((Operation)ViewState["DSOperation"] == Operation.Update)) || (((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery) && ((Operation)ViewState["DSOperation"] == Operation.None))) || (((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery) && ((Operation)ViewState["DSOperation"] == Operation.Update)))
					{
						if(Session["SESSION_DS2"] == null)
						{
							dsQryPkg = DomesticShipmentMgrDAL.GetPakageDtls(appID,enterpriseID,Convert.ToInt32(txtBookingNo.Text.Trim()), txtConsigNo.Text.Trim());
							Session["SESSION_DS2"] = dsQryPkg;
						}
					}	
					else
					{
						
					}
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					lblErrorMsg.Text = strMsg;
					return;
				}
				//				if((int)ViewState["DSOperation"] != (int)Operation.Update)
				//				{
				//					Session["SESSION_DS2"] = dsQryPkg;
				//				}
			}

			String strApplyDimWt = null;
			String strApplyESA = null;
			String strDim_By_tot = null;  //Jeab 28 Dec 10

			Customer customer = new Customer();
			customer.Populate(appID,enterpriseID,txtCustID.Text);
			strApplyDimWt = customer.ApplyDimWt;
			strApplyESA = customer.ESASurcharge;
			strDim_By_tot = customer.Dim_By_tot;  //Jeab 28 Dec 10
			
			DataRow drEach = m_sdsDomesticShip.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
			//calCulateESA();
			//calCulateESA_ServiceType();
			this.CalculateAllCharge();

			int iBookingNo = 0;
			if(txtBookingNo.Text.Length > 0)
				iBookingNo = Convert.ToInt32(txtBookingNo.Text);
			else
				iBookingNo = int.Parse(drEach["booking_no"].ToString());
	
			bool OnlyShow = !IsCalculate("original_rated_freight") || !IsCalculate("original_rated_other") || !IsCalculate("original_rated_esa");
			bool PartialCon = (bool)ViewState["PartialCon"];

			string flgAutoRetrieve = "f";
			if(txtActualWeight.Text != "")
				flgAutoRetrieve = "t";

			ViewState["PickupBooking"] = "yes";
			String sUrl = "PkgDetailsPopup.aspx?FormBtn=PkgDetails&ApplyDimWt="+strApplyDimWt+
				"&BTNACTIVE="+(String)ViewState["PickupBooking"]+
				"&BOOKINGNO="+iBookingNo+
				"&CUSTID="+txtCustID.Text+
				"&OnlyShow="+ OnlyShow+
				"&PartialCon="+ PartialCon+
				"&TotPackage="+ txtPkgTotpkgs.Text+
				"&ConsignmentNumber="+ txtConsigNo.Text+
				"&ActWeight="+ txtActualWeight.Text+
				"&CODAmount="+ txtCODAmount.Text+
				"&ShipDclrVal="+ txtShpDclrValue.Text+
				"&TotVASSurch="+ this.txtTotVASSurChrg.Text+
				"&ServiceCode="+ this.txtShpSvcCode.Text+
				"&ServiceDesc="+ this.txtShpSvcDesc.Text+
				"&RecipZip="+ this.txtRecipZip.Text+
				"&SendZip="+ this.txtSendZip.Text+
				"&DimWeight="+ txtPkgDimWt.Text+
				"&DimByTOT="+ strDim_By_tot+	 //Jeab 28 Dec 10
				"&flgAutoRetrieve="+ flgAutoRetrieve; //Tumz.

			if(((((ScreenMode)ViewState["DSMode"] == ScreenMode.Insert) && ((Operation)ViewState["DSOperation"] == Operation.Update)) || (((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery) && ((Operation)ViewState["DSOperation"] == Operation.None))) || (((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery) && ((Operation)ViewState["DSOperation"] == Operation.Update)))
			{
				sUrl = sUrl + "&status=u";
			}
				

			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			//Modified By Tom Jan 19, 10
			String sScript = Utility.GetScript("openPkg.js",paramList);
			//End Modified By tom Jan 19, 10
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void btnPkgCommCode_Click(object sender, System.EventArgs e)
		{
			/*
			String sUrl = "CommodityPopup.aspx?FORMID=DomesticShipment"+"&CUSTID_TEXT="+txtCustID.Text.Trim().ToString();
			String sFeatures = "'height=450,width=670,left=100,top=50,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=yes,toolbar=no'";
			String sScript ="";
			sScript += "<script language=javascript>";
			sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
			sScript += "</script>";
			Response.Write(sScript);
			*/
			String sUrl = "CommodityPopup.aspx?FORMID=DomesticShipment"+"&CUSTID_TEXT="+txtCustID.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);

			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private bool IsNumeric(string str) 
		{
			try 
			{
				Convert.ToDecimal(str);
				return true;
			}
			catch(FormatException) 
			{
				return false;
			}
		}

		public void CalculateInsuranceSurcharge()
		{
			if(m_dsPkgDetails!=null)
				//				return;
				if(txtShpDclrValue.Text.Trim() != "")
				{
					if(IsNumeric(txtShpDclrValue.Text) == false)
					{
						decimal tmpDclrValue = 0;
						txtShpDclrValue.Text = String.Format((String)ViewState["m_format"], tmpDclrValue);

						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_NOS",utility.GetUserCulture());
						return;
					}
				}
				else
				{
					decimal tmpDclrValue = 0;
					txtShpDclrValue.Text = String.Format((String)ViewState["m_format"], tmpDclrValue);

				}

			decimal tmpShpDclrValue = Convert.ToDecimal(txtShpDclrValue.Text.Trim());
			txtShpDclrValue.Text = String.Format((String)ViewState["m_format"], tmpShpDclrValue);

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			Enterprise enterprise = new Enterprise();
			enterprise.Populate(appID,enterpriseID);
			
			Customer customer = new Customer();
			customer.Populate(appID,enterpriseID, txtCustID.Text.Trim());

			if((customer.insurance_maximum_amt == null) || 
				(customer.insurance_maximum_amt.GetType().Equals(System.Type.GetType("System.DBNull"))))
				txtShpMaxCvrg.Text = String.Format((String)ViewState["m_format"], enterprise.MaxInsuranceAmt);
			else
				txtShpMaxCvrg.Text = String.Format((String)ViewState["m_format"], customer.insurance_maximum_amt);
			
			if((customer.insurance_percent_surcharge == null) || 
				(customer.insurance_percent_surcharge.GetType().Equals(System.Type.GetType("System.DBNull"))))
				txtShpAddDV.Text =  String.Format("{0:F2}", enterprise.InsPercSurchrg);
			else
				txtShpAddDV.Text =  String.Format("{0:F2}", customer.insurance_percent_surcharge);


			//Calculate Insurance Surcharge
			//if(IsCalculate("original_rated_ins"))
			//{			
			DomesticShipmentMgrBAL domesticBAL = new DomesticShipmentMgrBAL();
			decimal decInsSurchrg = domesticBAL.ComputeInsSurchargeByCust(appID, enterpriseID, Convert.ToDecimal(txtShpDclrValue.Text.Trim()), txtCustID.Text.Trim(), customer);
			txtShpInsSurchrg.Text = txtInsChrg.Text = String.Format((String)ViewState["m_format"], decInsSurchrg);
			//}//endif original_rated_ins not null


		}

		private void txtShpDclrValue_TextChanged(object sender, System.EventArgs e)
		{
			CalculateInsuranceSurcharge();
			CalculateTotalAmt();
		}


		private void CalculateAllCharge()
		{
			decimal fCalcFrgtChrg=0;
			CalculateAllFreightCharge(ref fCalcFrgtChrg);
			calCulateESA_ServiceType();

			//Calculate Other Surcharge
			//if(IsCalculate("original_rated_other") && IsCalculate("original_rated_freight"))
			txtOtherSurcharge.Text = txtOtherSurcharge2.Text = String.Format((string)ViewState["m_format"], GetOtherSurcharge(fCalcFrgtChrg));
			
			CalculateTotalAmt();
		}
		private void CalculateAllFreightCharge(ref decimal fCalcFrgtChrg)
		{			
			fCalcFrgtChrg = 0;
			String strLocalErrorMsg = "";

			if( txtCustID.Text.Trim() != "" && txtSendZip.Text.Trim() != "" &&
				txtRecipZip.Text.Trim() != "" && txtPkgChargeWt.Text.Trim() != "" &&
				txtShpSvcCode.Text.Trim() != "" /*&& IsCalculate("original_rated_freight")*/)
			{
				String strItem = ddbCustType.SelectedItem.Value;
				String strWeight = txtPkgChargeWt.Text;

				decimal decWeight = 0;
				if (strWeight.Length > 0)
				{
					decWeight = Convert.ToDecimal(strWeight);
					if(decWeight == 0)
					{
						strLocalErrorMsg = "The chargeable weight not found ";
					}
				}
				else
				{
					strLocalErrorMsg = "The chargeable weight not found ";
				}

				if (strLocalErrorMsg.Trim() == "")
				{
					if(strItem.Equals("0"))
					{
						strLocalErrorMsg = "Please select the Customer Type ";
					}
				}
			
				if (strLocalErrorMsg.Trim() == "")
				{
					if(txtShipPckUpTime.Text.Trim().Length == 0)
					{
						strLocalErrorMsg = "Please enter the PickUp Date Time";
					}
				}
			

				if (strLocalErrorMsg.Trim() == "")
				{
					///// Comment,Revise and Moved Code to DomesticshipmentMgrDAL.cs By Hong 11-04-2011
					////					Zipcode zipcode = new Zipcode();
					////
					////					zipcode.Populate(appID,enterpriseID,txtSendZip.Text);
					////					String strOrgZone = zipcode.ZoneCode;
					////
					////					zipcode.Populate(appID,enterpriseID,txtRecipZip.Text);
					////					String strDestnZone = zipcode.ZoneCode;
					////
					////					DataSet tmpCustZone = null;
					////					string sendCustZone = null;
					////					string recipCustZone = null;
					////					tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
					////						0, 0, txtCustID.Text.Trim(), txtSendZip.Text.Trim()).ds;
					////					if(tmpCustZone.Tables[0].Rows.Count > 0)
					////					{
					////						foreach(DataRow dr in tmpCustZone.Tables[0].Rows)
					////						{
					////							sendCustZone = dr["zone_code"].ToString().Trim();
					////						}
					////					}
					////					tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
					////						0, 0, txtCustID.Text.Trim(), txtRecipZip.Text.Trim()).ds;
					////					if(tmpCustZone.Tables[0].Rows.Count > 0)
					////					{
					////						foreach(DataRow dr in tmpCustZone.Tables[0].Rows)
					////						{
					////							recipCustZone = dr["zone_code"].ToString().Trim();
					////						}
					////					}
					////
					////					//float fCalcFrgtChrg = 0;
					////
					////					try
					////					{
					////						//Compute the Freight Charge	
					////						//Required fields before calculate the Freight Charge:
					////						//CustID, CustType, SendZip, RecipZip, Package Detail, Service Code
					////						DataSet dsPkgDetails = (DataSet)Session["SESSION_DS2"];
					////						Customer customer = new Customer();
					////						customer.Populate(appID,enterpriseID, txtCustID.Text.Trim());
					////						decimal freightCharge = 0;
					////						decimal freightCharge0 = 0;
					////						decimal sumFreight = 0;
					////						if(customer.IsCustomer_Box == "Y")
					////						{
					////							float weightToFreight = 0;
					////							int sumQty = 0;
					////
					////							if(!customer.IsBoxRateAvailable(appID,enterpriseID,sendCustZone,recipCustZone,txtShpSvcCode.Text.Trim()))
					////							{
					////								lblErrorMsg.Text = "Customer has no Box Rate defined.";
					////								return;
					////							}
					////							else
					////							{
					////								foreach(DataRow dr in dsPkgDetails.Tables[0].Rows)
					////								{
					////									if(float.Parse(dr["tot_wt"].ToString())>float.Parse(dr["tot_dim_wt"].ToString()))
					////									{
					////										weightToFreight = float.Parse(dr["tot_wt"].ToString())/int.Parse(dr["pkg_qty"].ToString());
					////									}
					////									else
					////									{
					////										weightToFreight = float.Parse(dr["tot_dim_wt"].ToString())/int.Parse(dr["pkg_qty"].ToString());
					////									}
					////									sumQty = sumQty+int.Parse(dr["pkg_qty"].ToString());
					////									freightCharge = (decimal)TIESUtility.ComputeFreightCharge(appID, enterpriseID,
					////										txtCustID.Text.Trim(), "C", 
					////										strOrgZone, strDestnZone, weightToFreight,
					////										txtShpSvcCode.Text, txtSendZip.Text.Trim(), txtRecipZip.Text.Trim());
					////									freightCharge = (freightCharge * (int.Parse(dr["pkg_qty"].ToString())));
					////									sumFreight = sumFreight+freightCharge;
					////								}
					////								if(sumQty < int.Parse(customer.Minimum_Box))
					////								{
					////									freightCharge0 = (decimal)TIESUtility.ComputeFreightCharge(appID, enterpriseID,
					////										txtCustID.Text.Trim(), "C", 
					////										strOrgZone, strDestnZone, 0,
					////										txtShpSvcCode.Text, txtSendZip.Text.Trim(), txtRecipZip.Text.Trim());
					////									fCalcFrgtChrg = ((int.Parse(customer.Minimum_Box)-sumQty)*freightCharge0)+sumFreight;
					////								}
					////								else
					////									fCalcFrgtChrg = sumFreight;
					////							}
					////						}
					////						else
					////						{
					////							fCalcFrgtChrg = (decimal)TIESUtility.ComputeFreightCharge(appID, enterpriseID,
					////								txtCustID.Text.Trim(), "C", 
					////								strOrgZone, strDestnZone, (float)Convert.ToDecimal(txtPkgChargeWt.Text),
					////								txtShpSvcCode.Text, txtSendZip.Text.Trim(), txtRecipZip.Text.Trim());
					////						}
					////						if(fCalcFrgtChrg.ToString("#0.00").Equals("0.00") && txtPkgChargeWt.Text.Length >0 && txtShpDclrValue.Text.Trim().Length >0)
					////						{
					////							txtFreightChrg.Text="";
					////							strLocalErrorMsg = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_RATES",utility.GetUserCulture());
					////							lblErrorMsg.Text =  strLocalErrorMsg;
					////					
					////						}
					////						else
					////						{
					////							//Comment and add new statement by Panas 24/04/2009
					////							//txtFreightChrg.Text = String.Format((String)ViewState["m_format"], Rounding(fCalcFrgtChrg));
					////							txtFreightChrg.Text = String.Format((String)ViewState["m_format"], RoundingFreightCharge(fCalcFrgtChrg));
					////						}
					///// End of comment and move code
					try
					{
						DataSet tmpCustZone = null;
						string sendCustZone = null;
						string recipCustZone = null;
						tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
							0, 0, txtCustID.Text.Trim(), txtSendZip.Text.Trim()).ds;
						if(tmpCustZone.Tables[0].Rows.Count > 0)
						{
							foreach(DataRow dr in tmpCustZone.Tables[0].Rows)
							{
								sendCustZone = dr["zone_code"].ToString().Trim();
							}
						}
						tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
							0, 0, txtCustID.Text.Trim(), txtRecipZip.Text.Trim()).ds;
						if(tmpCustZone.Tables[0].Rows.Count > 0)
						{
							foreach(DataRow dr in tmpCustZone.Tables[0].Rows)
							{
								recipCustZone = dr["zone_code"].ToString().Trim();
							}
						}
						Customer customer = new Customer();
						customer.Populate(appID,enterpriseID, txtCustID.Text.Trim());
						if(!customer.IsBoxRateAvailable(appID,enterpriseID,sendCustZone,recipCustZone,txtShpSvcCode.Text.Trim()))
						{
							lblErrorMsg.Text = "Customer has no Box Rate defined.";
							return;
						}
	
						fCalcFrgtChrg= DomesticShipmentMgrDAL.CalculateAllFreightCharge(appID,enterpriseID,txtCustID.Text ,txtShpSvcCode.Text ,
							txtSendZip.Text,txtRecipZip.Text ,txtPkgChargeWt.Text,txtShpDclrValue.Text ,(DataSet)Session["SESSION_DS2"],fCalcFrgtChrg);
						if(fCalcFrgtChrg.ToString("#0.00").Equals("0.00") && txtPkgChargeWt.Text.Length >0 && txtShpDclrValue.Text.Trim().Length >0)
						{
							txtFreightChrg.Text="";
							strLocalErrorMsg = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_RATES",utility.GetUserCulture());
							lblErrorMsg.Text =  strLocalErrorMsg;
								
						}
						else
						{
							txtFreightChrg.Text = String.Format((String)ViewState["m_format"], RoundingFreightCharge(fCalcFrgtChrg));
						}
						DataSet DSCust = new DataSet();
						//Added by GwanG on 08April08
						if(Utility.IsNotDBNull(Session["SESSION_DSCUST"]))
						{
							DSCust = (DataSet)Session["SESSION_DSCUST"];
						}
						else
						{
							DSCust = SysDataMgrDAL.QueryVAS(this.appID,this.enterpriseID,txtCustID.Text);							
						}

						Session["SESSION_DSCUST"] = DSCust;
						if(DSCust.Tables[0].Select("percent is not null").Length > 0)
						{
							for(int i=0;i < DSCust.Tables[0].Select("percent is not null").Length ;i++)
							{
								DataRow drPercent = DSCust.Tables[0].Select("percent is not null")[i];
								if(m_dsVAS.Tables[0].Select("vas_code = '"+drPercent["vas_code"].ToString()+"'").Length > 0)
								{	
									DataRow drEach = m_dsVAS.Tables[0].Select("vas_code = '"+drPercent["vas_code"].ToString()+"'")[0];
									txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], Convert.ToDecimal(txtTotVASSurChrg.Text)- Convert.ToDecimal(drEach["surcharge"]) );
									m_dsVAS.Tables[0].Select("vas_code = '"+drPercent["vas_code"].ToString()+"'")[0].Delete();
									m_dsVAS.AcceptChanges();

									DataSet vasSearchDS = SysDataMgrDAL.GetVASCustDS(this.appID,this.enterpriseID,drPercent["vas_code"].ToString(),txtCustID.Text);	
									if( vasSearchDS.Tables[0].Rows.Count>0)
									{
										AddNewRowVAS(vasSearchDS);	
									}														

									
								}
							}
							m_dsVAS.Tables[0].AcceptChanges();
							Session["SESSION_DS1"] = m_dsVAS;
						}			


					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strLocalErrorMsg = strMsg;
						lblErrorMsg.Text = strMsg;
					}
				}
			}

			//return strLocalErrorMsg;
		}
	
		private decimal GetOtherSurcharge(decimal fCalcFrgtChrg)
		{
			///// Comment,Revise and Moved Code to DomesticshipmentMgrDAL.cs By Hong 11-04-2011

			////			Customer customer = new Customer();
			////			customer.Populate(appID,enterpriseID,txtCustID.Text);
			////
			////			decimal OtherSurcharge = 0;
			////			if((customer.OtherSurchargeAmount == null || customer.OtherSurchargeAmount == "") && (customer.OtherSurchargeAmountPercentage == null || customer.OtherSurchargeAmountPercentage == ""))
			////			{
			////				OtherSurcharge = 0;
			////			}
			////			else
			////			{
			////				if(customer.OtherSurchargeAmount != "")
			////				{ 
			////					OtherSurcharge = Convert.ToDecimal(customer.OtherSurchargeAmount);
			////				}
			////				else if(customer.OtherSurchargeAmountPercentage != null || customer.OtherSurchargeAmountPercentage != "")
			////				{
			////					OtherSurcharge = (Convert.ToDecimal(customer.OtherSurchargeAmountPercentage) * Convert.ToDecimal(fCalcFrgtChrg))/100;
			////
			////					if(customer.OtherSurchargeMin != null && customer.OtherSurchargeMin != "")
			////						OtherSurcharge = Math.Max(OtherSurcharge, Convert.ToDecimal(customer.OtherSurchargeMin));
			////					if(customer.OtherSurchargeMax != null && customer.OtherSurchargeMax != "")
			////						OtherSurcharge = Math.Min(OtherSurcharge, Convert.ToDecimal(customer.OtherSurchargeMax));
			////				}
			////			}
			///// End Comment And Move Code By Hong
			return Rounding(DomesticShipmentMgrDAL.GetOtherSurcharge(appID,enterpriseID,txtCustID.Text,fCalcFrgtChrg.ToString()));
		}
		private void CalculateTotalAmt()
		{
			//Calculate the total Amount
			decimal decInsuranceChrg = 0;
			if(txtInsChrg.Text.Trim().Length > 0)
			{
				decInsuranceChrg = Convert.ToDecimal(txtInsChrg.Text.Trim());
			}

			decimal decFrghtChrg = 0;
			if (txtFreightChrg.Text.Trim().Length > 0)
			{
				decFrghtChrg = Convert.ToDecimal(txtFreightChrg.Text.Trim());
			}

			decimal decTotOtherChrg = 0;
			if (txtOtherSurcharge2.Text.Trim().Length > 0)
			{
				decTotOtherChrg = Convert.ToDecimal(txtOtherSurcharge2.Text.Trim());
			}

			decimal decTotVASSurcharge = 0;
			if(txtTotVASSurChrg.Text.Trim().Length > 0)
			{
				decTotVASSurcharge = Convert.ToDecimal(txtTotVASSurChrg.Text.Trim());
			}

			decimal decESASSurcharge = 0;
			if(txtESASurchrg.Text.Length > 0)
			{
				decESASSurcharge = Convert.ToDecimal(txtESASurchrg.Text);
			}

			decimal decTotalAmt = decTotVASSurcharge + decFrghtChrg + decInsuranceChrg + decTotOtherChrg + decESASSurcharge;	
			txtShpTotAmt.Text = String.Format((string)ViewState["m_format"], Rounding(decTotalAmt));
		}

		private void rbCash_CheckedChanged(object sender, System.EventArgs e)
		{	
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			//			if (((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery ) && ((int)ViewState["DSMode"]!=(int)ScreenMode.Query))
			//			{
			//				if(rbCash.Checked == true)
			//				{
			//					rbtnShipFrghtPre.Enabled = true;
			//					rbtnShipFrghtColl.Enabled = true;
			//				}
			//				else
			//				{
			//					rbtnShipFrghtPre.Enabled = false;
			//					rbtnShipFrghtColl.Enabled = false;
			//				}
			//			}
		}

		private void rbCredit_CheckedChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((rbCredit.Checked == true) && ((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSMode"]!=(int)ScreenMode.Query))
			{
				rbtnShipFrghtPre.Enabled = false;
				rbtnShipFrghtColl.Enabled = false;
			}
		}

		
		protected void dgVAS_Edit(object sender, DataGridCommandEventArgs e)
		{	
			e.Item.Cells[0].Enabled = false;
			dgVAS.EditItemIndex = e.Item.ItemIndex;
			BindVASGrid();
		}

		protected void dgVAS_Cancel(object sender, DataGridCommandEventArgs e)
		{
			bool isDeleted = false;
			TextBox txtVASCode = (TextBox)e.Item.FindControl("txtVASCode");
			if(txtVASCode != null)
			{
				if(txtVASCode.Text.Length == 0)
				{
					//					DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
					//					drCurrent.Delete();
					m_dsVAS.Tables[0].Rows.RemoveAt(dgVAS.EditItemIndex);
					Session["SESSION_DS1"] = m_dsVAS;
					isDeleted = true;
				}
			}

			TextBox txtVASDesc = (TextBox)e.Item.FindControl("txtVASDesc");
			if((txtVASDesc != null) && (isDeleted == false))
			{
				if(txtVASDesc.Text.Length == 0)
				{
					DataRow drCurr = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
					//drCurr["vas_code"] = txtVASCode.Text;
					drCurr.Delete();
					Session["SESSION_DS1"] = m_dsVAS;
				}
			}

			dgVAS.EditItemIndex = -1;
			BindVASGrid();
			GetTotalSurcharge();
		}

		protected void dgVAS_Delete(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			//DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
			//Modify by GwanG on 17Apr08
			DataRow drCurrent = m_dsVAS.Tables[0].Rows[(dgVAS.CurrentPageIndex*dgVAS.PageSize)+e.Item.ItemIndex];
			drCurrent.Delete();
			m_dsVAS.AcceptChanges();

			dgVAS.EditItemIndex = -1;
			Session["SESSION_DS1"] = m_dsVAS;
			BindVASGrid();
			
			decimal totSurcharge = GetTotalSurcharge();
			txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], totSurcharge);

			CalculateTotalAmt();

		}

		protected void dgVAS_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgVAS.CurrentPageIndex = e.NewPageIndex;
			BindVASGrid();
		}
	
		public void dgVAS_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text = "";
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			lblErrorMsg.Text = "";
			String strChkVasCode = null;
			TextBox txtdgVASCode = (TextBox)e.Item.FindControl("txtVASCode");
			
			if(txtdgVASCode != null)
			{
				VAS vas = new VAS();
				vas.Populate(appID,enterpriseID,txtdgVASCode.Text.Trim());
				strChkVasCode = vas.VASCode;

				if(strChkVasCode == null)
				{
					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_VAS",utility.GetUserCulture());
					return;
				}
				else
				{
					//Check whether this VAS service is available.
					bool isExcluded = TIESUtility.IsVASExcluded(appID,enterpriseID,txtdgVASCode.Text.Trim(),txtRecipZip.Text.Trim());
					if(isExcluded)
					{
						lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_VAS",utility.GetUserCulture());
						return;
					}

				}
			}
			
			try
			{
				TextBox txtSurcharge = (TextBox)e.Item.FindControl("txtSurcharge");
				
				if(txtSurcharge != null)
				{
					//					if(Convert.ToDecimal(txtSurcharge.Text) == 0)
					//					{
					//						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SUR_AMT",utility.GetUserCulture());
					//						return;
					//					}
					DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
					decimal TmpSurcharge = TIESUtility.EnterpriseRounding(Convert.ToDecimal(txtSurcharge.Text),(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]);
					//txtSurcharge = String.Format((String)ViewState["m_format"],TmpSurcharge);
					//decimal TmpSurcharge = Convert.ToDecimal(txtSurcharge.Text);
					drCurrent["surcharge"] = TmpSurcharge;
				}

				TextBox txtRemarks = (TextBox)e.Item.FindControl("txtRemarks");
				if(txtRemarks != null)
				{
					DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
					drCurrent["remarks"] = txtRemarks.Text;
				}
			
				TextBox txtVASCode = (TextBox)e.Item.FindControl("txtVASCode");
				String strVASCode = null;
				if(txtVASCode != null)
				{
					DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
					drCurrent["vas_code"] = txtVASCode.Text;
					strVASCode = txtVASCode.Text;
				}

				TextBox txtVASDesc = (TextBox)e.Item.FindControl("txtVASDesc");
				if(txtVASDesc != null)
				{
					DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
					if((txtVASDesc.Text.Length == 0) && (strVASCode != null))
					{
						VAS vas = new VAS();
						vas.Populate(appID,enterpriseID,strVASCode);
						drCurrent["vas_description"] = vas.VasDescription;
					}
					else
						drCurrent["vas_description"] = txtVASDesc.Text;
				}
			}
			catch(System.Data.ConstraintException appConstraintExp)
			{
				String msg = appConstraintExp.ToString();
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_VAS",utility.GetUserCulture());
				return;
			}

			decimal totSurcharge = GetTotalSurcharge();
			txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], totSurcharge);
				
			CalculateTotalAmt();

			Session["SESSION_DS1"] = m_dsVAS;
			dgVAS.EditItemIndex = -1;
			BindVASGrid();
		}

		public void BindVASGrid()
		{

			if(Session["SESSION_DS1"] != null)
			{
				m_dsVAS = (DataSet)Session["SESSION_DS1"];
			}
			
			//Added by GwanG on 17Apr08
			int pgCnt = Convert.ToInt16(Math.Ceiling((double)m_dsVAS.Tables[0].Rows.Count/dgVAS.PageSize));
            
			if(pgCnt != 0)
			{
				if(dgVAS.CurrentPageIndex > pgCnt-1)
				{
					dgVAS.CurrentPageIndex = pgCnt-1;
				}
			}
			//End

			dgVAS.DataSource = m_dsVAS;
			dgVAS.DataBind();
			
			Session["SESSION_DS1"] = m_dsVAS;

		}
		public void BindPKGGrid()
		{

			if(Session["SESSION_DS2"] != null)
			{
				m_dsPkgDetails = (DataSet)Session["SESSION_DS2"];
			}			
			Session["SESSION_DS2"] = m_dsPkgDetails;

		}

		private void btnDGInsert_Click(object sender, System.EventArgs e)
		{
			ViewState["OperationsVAS"] = Operation.Insert;  //Jeab 09 Mar 11
			ViewState["Operations"] = Operation.Insert;
			AddRowInVASGrid();
			dgVAS.EditItemIndex = m_dsVAS.Tables[0].Rows.Count - 1;	
			BindVASGrid();		
			Utility.RegisterScriptFile("setScrollPosition.js","scrolltoinsert",this.Page);
			
		}

		private void AddRowInVASGrid()
		{
			DomesticShipmentMgrDAL.AddNewRowInVAS(m_dsVAS);
			Session["SESSION_DS1"] = m_dsVAS;
		}

		public void dgVAS_Button(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text = "";
			String strCmdNm = e.CommandName;
			if(strCmdNm.Equals("Search"))
			{
				if(txtRecipZip.Text.Trim().Length == 0)
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_RECIP_ZIP",utility.GetUserCulture());
					return;
				}

				TextBox txtVASCode = (TextBox)e.Item.FindControl("txtVASCode");
				String strtxtVASCode = null;
				if(txtVASCode != null)
				{
					strtxtVASCode = txtVASCode.ClientID;
				}
				
				TextBox txtVASDesc = (TextBox)e.Item.FindControl("txtVASDesc");
				String strtxtVASDesc = null;
				if (txtVASDesc != null)
				{
					strtxtVASDesc = txtVASDesc.ClientID;
				}

				TextBox txtSurcharge = (TextBox)e.Item.FindControl("txtSurcharge");
				String strtxtSurcharge = null;
				if (txtSurcharge != null)
				{
					strtxtSurcharge = txtSurcharge.ClientID;
				}
				/*
								String sUrl = "VASPopup.aspx?VASID="+strtxtVASCode+"&VASDESC="+strtxtVASDesc+"&VASSURCHARGE="+strtxtSurcharge+"&FORMID="+"DomesticShipment"+"&DestZipCode="+txtRecipZip.Text.Trim();
								String sFeatures = "'height=520;width=160;left=100;top=50;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'";
								String sScript ="";
								sScript += "<script language=javascript>";
								sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
								sScript += "</script>";
								Response.Write(sScript);
				*/
				String sUrl = "VASPopup.aspx?VASID="+strtxtVASCode+"&VASDESC="+strtxtVASDesc+"&VASSURCHARGE="+strtxtSurcharge+"&FORMID="+"DomesticShipment"+"&DestZipCode="+txtRecipZip.Text.Trim();
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			
		}

		public void dgVAS_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.EditItem)
			{
				e.Item.Cells[3].Enabled = true;	
			}
			else
			{
				e.Item.Cells[3].Enabled = false;
			}
		}

		private void ddbCustType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}
		private decimal GetTotalSurcharge()
		{
			decimal decTotSurchrg = DomesticShipmentMgrDAL.GetTotalSurcharge(m_dsVAS);

			/// Comment & Move Code By Hong
			//			if(m_dsVAS != null)
			//			{
			//			
			//				int cnt = m_dsVAS.Tables[0].Rows.Count;
			//				int i = 0;
			//				
			//
			//				for(i = 0;i<cnt;i++)
			//				{
			//					DataRow drEach = m_dsVAS.Tables[0].Rows[i];
			//
			//					String vas_code = "";
			//					if((drEach["vas_code"] != null) && 
			//						(!drEach["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
			//						(drEach["vas_code"].ToString() != ""))
			//					{
			//						vas_code = drEach["vas_code"].ToString().Trim();
			//					}
			//
			//					//Comment by GwanG on 20/12/2007 
			//					//					if (vas_code == "COD")
			//					//					{
			//					//						DataSet dscustInfo = SysDataMgrDAL.GetCustInforByCustID(appID,enterpriseID, 
			//					//							0, 0, txtCustID.Text.ToString().Trim()).ds;
			//					//
			//					//						if(dscustInfo.Tables[0].Rows.Count > 0)
			//					//						{
			//					//							if(((dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"] != null) && 
			//					//								(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
			//					//								(dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].ToString() != "")) ||
			//					//								((dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"] != null) && 
			//					//								(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
			//					//								(dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].ToString() != "")))
			//					//							{
			//					//								if((dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"] != null) && 
			//					//									(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
			//					//									(dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].ToString() != ""))
			//					//								{
			//					//									decTotSurchrg += (decimal)dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"];
			//					//								}
			//					//
			//					//								if((dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"] != null) && 
			//					//									(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
			//					//									(dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].ToString() != ""))
			//					//								{
			//					//									decTotSurchrg += (Convert.ToDecimal(txtCODAmount.Text) * ((decimal)dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"])) / 100;
			//					//								}
			//					//							}
			//					//							else
			//					//							{
			//					//								decTotSurchrg += (decimal)drEach["surcharge"];
			//					//							}
			//					//						}
			//					//						else
			//					//						{
			//					//							decTotSurchrg += (decimal)drEach["surcharge"];
			//					//						}
			//					//					}
			//					//					else
			//					//					{
			//					//						decTotSurchrg += (decimal)drEach["surcharge"];
			//					//					}
			//
			//					if(drEach["surcharge"]!=System.DBNull.Value)
			//						decTotSurchrg += (decimal)drEach["surcharge"];
			//				}
			//			}
			///end Comment & Move Code By Hong

			return decTotSurchrg;
		}

		private void btnRouteCode_Click(object sender, System.EventArgs e)
		{
			
		}

		private void btnShpSvcCode_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if(txtSendZip.Text.Trim().Length == 0)
			{
				txtShpSvcCode.Text = "";
				lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SENDER_ZIP",utility.GetUserCulture());
				return;
			}
			if(txtRecipZip.Text.Trim().Length == 0)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_RECIP_ZIP",utility.GetUserCulture());
				return;
			}
			
			//Call the popup window.
			/*
			String sUrl = "ServiceCodePopup.aspx?PickUpDtTime="+txtShipPckUpTime.Text.Trim()+"&DestZipCode="+txtRecipZip.Text.Trim();
			String sFeatures = "'height=450,width=670,left=100,top=50,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=yes,toolbar=no'";
			String sScript ="";
			sScript += "<script language=javascript>";
			sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
			sScript += "</script>";
			Response.Write(sScript);
			*/
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
			String sUrl = "ServiceCodePopup.aspx?PickUpDtTime="+txtShipPckUpTime.Text.Trim()+
				"&DestZipCode="+txtRecipZip.Text.Trim()+
				"&SendZipCode="+txtSendZip.Text.Trim()+
				"&CONNO="+txtConsigNo.Text.Trim()+
				"&BOOKINGNO="+txtBookingNo.Text.Trim()+
				"&ServiceCode="+txtShpSvcCode.Text.Trim()+
				"&FORMN=DomesticShipment";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);

			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void Old_ShpSvcCode_TextChanged()
		{
			lblErrorMsg.Text = "";


			if(txtSendZip.Text.Trim().Length == 0)
			{
				txtShpSvcCode.Text = "";
				lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SENDER_ZIP",utility.GetUserCulture());
				return;
			}

			if(txtRecipZip.Text.Trim().Length == 0)
			{
				txtShpSvcCode.Text = "";
				lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_RECIP_ZIP",utility.GetUserCulture());
				return;
			}

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			strErrorMsg = "";
			//Call the calculation of Estimated Delivery date method
			// Comment by Oak: Recalculate Estimated Del Dated & Freight & Other & Total Rated Amount
			// Remark		 : Comment 2 DSMode Conditions
			if((txtShpSvcCode.Text.Trim().Length > 0) /* && ((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSMode"]!=(int)ScreenMode.Query)*/)			
			{
				if((txtShipEstDlvryDt.Text.Length > 0) && (txtShipManifestDt.Text.Length > 0))
				{
					if(txtBookingNo.Text.Trim()=="")
					{
						int iCompare = DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null).CompareTo(DateTime.ParseExact(txtShipManifestDt.Text,"dd/MM/yyyy HH:mm",null));
				
						if(iCompare < 0)
						{
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DELETED_DT_LESS_SHPT_MFST_DT",utility.GetUserCulture());
							txtShipEstDlvryDt.Text = "";
							return;
						}
					}
				}

				Service service = new Service();
				service.Populate(appID,enterpriseID,txtShpSvcCode.Text);
				String strCode = service.ServiceCode;
				String strDesc = service.ServiceDescription;
				txtShpSvcDesc.Text = strDesc;

				if(strCode == null)
				{
					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_SERVICE",utility.GetUserCulture());
					txtShpSvcCode.Text = "";
					return;
				}
				bool IsServiceAvail = false;
				try
				{
					//Check whether the service is provided for the destination zip code
					IsServiceAvail = Service.IsAvailable(appID,enterpriseID,txtShpSvcCode.Text.Trim(), txtSendZip.Text.Trim(), txtRecipZip.Text.Trim());
				}
				catch(ApplicationException appException)
				{
					lblErrorMsg.Text = appException.Message;
					return;
				}

				ViewState["SeriviceNotAvail"] = IsServiceAvail;

				if(IsServiceAvail == true)
				{
					// Comment by	 : Oak Recalculate Estimated Del Dated & Freight & Other & Total Rated Amount
					// Remark		 : Comment Length of txtShipEstDlvryDt == 0
					if((txtShipPckUpTime.Text.Trim().Length > 0) /*&& (txtShipEstDlvryDt.Text.Length == 0)*/)
					{
						domesticMgrBAL = new DomesticShipmentMgrBAL();
						txtShipEstDlvryDt.Text = domesticMgrBAL.CalcDlvryDtTime(appID,enterpriseID,System.DateTime.ParseExact(txtShipPckUpTime.Text,"dd/MM/yyyy HH:mm",null),txtShpSvcCode.Text,txtRecipZip.Text).ToString();

						//HC Return Task
						Zipcode zipCode = new Zipcode();
						zipCode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
						String strStateCode = zipCode.StateCode;

						if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCode.Trim() != ""))
						{
							string strConNo = "";
							string strBookingNo = "";

							if (txtBookingNo.Text.Trim() != "" )
								strBookingNo = txtBookingNo.Text.Trim();
							if (txtConsigNo.Text.Trim() != "" )
								strConNo = txtConsigNo.Text.Trim();

							object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCode,
								strConNo.Trim(), strBookingNo.Trim(), appID,enterpriseID);

							if((tmpReHCDateTime != null) && 
								(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
							}
							else
							{
								txtEstHCDt.Text = "";
							}
						}
						else
						{
							txtEstHCDt.Text = "";
						}
						//HC Return Task
					}
				}
				else if(IsServiceAvail == false)
				{
					strErrorMsg = "Service type is not available";
					txtShipEstDlvryDt.Text = "";
					lblErrorMsg.Text = strErrorMsg;
					txtShpSvcCode.Text = "";
					txtShpSvcDesc.Text = "";
					return;
				}
				//this.calCulateESA_ServiceType();
				CalculateAllCharge();

			}
			lblErrorMsg.Text = strErrorMsg;

			//			decimal fCalcFrgtChrg=0;
			//			//Required fields before calculate the FreightCharge CustID, CustType, SendZip, RecipZip, Package Detail, Service Code
			//			if((txtCustID.Text.Trim() != "") && 
			//				(txtSendZip.Text.Trim() != "") &&
			//				(txtRecipZip.Text.Trim() != "") &&
			//				(txtPkgChargeWt.Text.Trim() != "") &&
			//				(txtShpSvcCode.Text.Trim() != ""))
			//			{
			//				CalculateAllFreightCharge(ref fCalcFrgtChrg);
			//			}
			//
			//			//Calculate Other Surcharge
			//			if(IsCalculate("original_rated_other"))
			//			{
			//				decimal fCalcOtherSurch = GetOtherSurcharge(fCalcFrgtChrg);
			//				txtOtherSurcharge.Text = String.Format((string)ViewState["m_format"], fCalcOtherSurch);			
			//				txtOtherSurcharge2.Text = txtOtherSurcharge.Text;
			//			}
			//
			//			CalculateTotalAmt();
		}

		private void txtShpSvcCode_TextChanged(object sender, System.EventArgs e)
		{
			Old_ShpSvcCode_TextChanged();
		}

		private void txtShipPckUpTime_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			Regularpickupdate.Validate();

			if(Regularpickupdate.IsValid)
			{
				if((int)ViewState["DSMode"] == (int)ScreenMode.Insert)
				{	
					if(txtBookingNo.Text.Trim()=="")
					{
						int iCompare = DateTime.ParseExact(txtShipPckUpTime.Text,"dd/MM/yyyy HH:mm",null).CompareTo(DateTime.ParseExact(txtShipManifestDt.Text,"dd/MM/yyyy HH:mm",null));
				
						if(iCompare < 0)
						{
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PICKPUP_DT_NO_LESS_MNFEST_DT",utility.GetUserCulture());
							//txtShipEstDlvryDt.Text = "";
							return;
						}
					}
				}
				if((bool)ViewState["isTextChanged"] == false)
				{
					ChangeDSState();
					ViewState["isTextChanged"] = true;
				}
				//				if(txtShpSvcCode.Text.Length == 0)
				//				{
				//					lblErrorMsg.Text = "Please enter the service code";
				//					return;
				//				}
				DomesticShipmentMgrBAL  domesticMgrBAL = new DomesticShipmentMgrBAL();
				
				//Call the calculation of Estimated Delivery date method
				//Comment by Oak: Recalculate Estimated Del Dated
				if( (txtShipPckUpTime.Text.Trim().Length > 0) && /*((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSMode"]!=(int)ScreenMode.Query) && */
					(txtShpSvcCode.Text.Length > 0))
				{
					txtShipEstDlvryDt.Text = domesticMgrBAL.CalcDlvryDtTime(appID,enterpriseID,System.DateTime.ParseExact(txtShipPckUpTime.Text,"dd/MM/yyyy HH:mm",null),txtShpSvcCode.Text,txtRecipZip.Text).ToString();


					//HC Return Task
					Zipcode zipCode = new Zipcode();
					zipCode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
					String strStateCode = zipCode.StateCode;

					if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCode.Trim() != ""))
					{
						string strConNo = "";
						string strBookingNo = "";

						if (txtBookingNo.Text.Trim() != "" )
							strBookingNo = txtBookingNo.Text.Trim();
						if (txtConsigNo.Text.Trim() != "" )
							strConNo = txtConsigNo.Text.Trim();

						object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCode,
							strConNo.Trim(), strBookingNo.Trim(), appID, enterpriseID);

						if((tmpReHCDateTime != null) && 
							(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
						}
						else
						{
							txtEstHCDt.Text = "";
						}
					}
					else
					{
						txtEstHCDt.Text = "";
					}
					//HC Return Task
				}

				
				//Check whether the date is less than the required days for the service.
				if((txtShipEstDlvryDt.Text.Length > 0) && (txtShipPckUpTime.Text.Length > 0) && (txtShpSvcCode.Text.Length > 0))
				{
					TransitDayNServiceCode DayNServiceCode = TIESUtility.IsDelvryDtLess(appID,enterpriseID,txtShpSvcCode.Text,DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null),DateTime.ParseExact(txtShipPckUpTime.Text,"dd/MM/yyyy HH:mm",null));
					if(DayNServiceCode.isLess)
					{
						ArrayList paramValues = new ArrayList();
						paramValues.Add(""+DayNServiceCode.strServiceCode);
						paramValues.Add(""+DayNServiceCode.iTransitDay);
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SERVICE_CODE_REQ",utility.GetUserCulture(),paramValues);
						return;
					}
				}
				
				if((txtShipEstDlvryDt.Text.Length > 0) && (txtSendZip.Text.Length > 0)&&(txtRecName.Text.Length > 0) && (txtRecipAddr1.Text.Length > 0) )
				{
					DatePkUpDlvryTextChange();
				}

			}
			else if(Regularpickupdate.IsValid == false)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT_DDMMYYYYHHMM",utility.GetUserCulture());
				return;
			}


		}

		private void DatePkUpDlvryTextChange()
		{
			ServiceAvailable serviceAvailable =  CheckDayServiceAvail();
			if((serviceAvailable.isServiceAvail == true) && ((String)ViewState["AddSurcharge"] == "none"))
			{
				ViewState["ServiceVASCode"] = serviceAvailable.strVASCode;
				//display the popup msg
				//strErrorMsg = "Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge ";
				/*lblConfirmMsg.Text = txtShipEstDlvryDt.Text.ToString()+ " Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge will be imposed. Do you want to continue?";
				DomstcShipPanel.Visible = true;
				divMain.Visible=false;
				divDelOperation.Visible=false;
				divCustIdCng.Visible=false;
				ddbCustType.Visible = false;
				ViewState["AddSurcharge"] = "asking";*/
				//return;
			}

			//if no service available then get the next working day...
			/*if((bool)ViewState["ServiceExists"] == false && (((String)ViewState["Day"] == "WeekEnd" ||((String)ViewState["Day"] == "Holiday"))))
			{
				ReCalculateDlvryDt();
			}
			*/
		}

		private String GetGridVasCode()
		{
			String strVasCode = "";
			int i = 0;

			for(i=0;i<dgVAS.Items.Count;i++)
			{
				Label lblVASCode = (Label)dgVAS.Items[i].FindControl("lblVASCode");
				if(lblVASCode != null)
				{
					if(i == dgVAS.Items.Count - 1)
					{
						strVasCode = strVasCode+lblVASCode.Text;
					}
					else
					{
						strVasCode = strVasCode+lblVASCode.Text+":";
					}
				}
			}
			return strVasCode;
		}
		private void btnPopulateVAS_Click(object sender, System.EventArgs e)
		{
			QuotationData quotationData;
			quotationData.iQuotationVersion = 0;
			quotationData.strQuotationNo = null;
			DataSet dsPopulateVAS = null;
			strErrorMsg = "";

			if(dgVAS.EditItemIndex != -1)
			{
				m_dsVAS.Tables[0].Rows.RemoveAt(dgVAS.EditItemIndex);
				dgVAS.EditItemIndex = -1;
				Session["SESSION_DS1"] = m_dsVAS;
				BindVASGrid();
			}


			if((ddbCustType.SelectedItem.Value != "0") && (txtCustID.Text.Trim().Length > 0))
			{
				try
				{
					quotationData = TIESUtility.CustomerQtnActive(appID,enterpriseID,txtCustID.Text.Trim());		
				}
				catch(ApplicationException appException)
				{
					lblErrorMsg.Text = appException.Message;
					return;
				}
				
				//Get VAS for the active quotation
				if(quotationData.strQuotationNo != null)
				{
					String strVASList = GetGridVasCode();
					try
					{
						dsPopulateVAS = QuotationVAS.GetQuotationVAS(appID,enterpriseID,txtCustID.Text.Trim(),
							quotationData.strQuotationNo,quotationData.iQuotationVersion,strVASList,
							"C",txtRecipZip.Text);
					}
					catch(ApplicationException appException)
					{
						lblErrorMsg.Text = appException.Message;
					}
					dsPopulateVAS.Tables[0].Columns.Add(new DataColumn("isSelected", typeof(bool)));
					int cnt = dsPopulateVAS.Tables[0].Rows.Count;
					int i = 0;
					
					for(i=0;i<cnt;i++)
					{
						DataRow drCurrent = dsPopulateVAS.Tables[0].Rows[i];
						drCurrent["isSelected"] = false;
					}
					Session["SESSION_DS4"] = dsPopulateVAS;
					//Call the popup window.

					String sUrl = "QuotationVASPopup.aspx";
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);

					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);					
				}
				else
				{
					strErrorMsg = "There is no active Quotation.";
				}
			}
			else if((ddbCustType.SelectedItem.Value == "0") || (txtCustID.Text.Trim().Length == 0))
			{
				strErrorMsg =Utility.GetLanguageText(ResourceType.UserMessage,"SEL_CUST_TYPE",utility.GetUserCulture());
			}
			lblErrorMsg.Text = strErrorMsg;
		}

		private void btnBind_Click(object sender, System.EventArgs e)
		{
			refreshVAS();		
		}
		private void refreshVAS()
		{
			ViewState["isTextChanged"] = true;
			if(Session["dtPRConsignmentVAS"]!=null)
			{
				DataTable dtPRConsignmentVAS = (DataTable)Session["dtPRConsignmentVAS"];
				if(dtPRConsignmentVAS.Rows.Count>0)
				{
					dtPRConsignmentVAS.Columns["vas_surcharge"].ColumnName = "surcharge";
					dtPRConsignmentVAS.Columns.Add("remarks", typeof(bool));
					dtPRConsignmentVAS.TableName = "VAS";
					m_dsVAS.Tables.Clear();
					m_dsVAS.Tables.Add(dtPRConsignmentVAS.Copy());
				}

			}
			Session["SESSION_DS1"] = m_dsVAS;
			Session["dtPRConsignmentVAS"]=null;
			
			//Calculate the surcharge
			decimal decTotSurcharge = GetTotalSurcharge();			
			txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], decTotSurcharge);

			CalculateTotalAmt();
			BindVASGrid();
		}

		private void txtRecipZip_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			
			//Get the State & Country
			Enterprise enterprise = new Enterprise();
			enterprise.Populate(appID,enterpriseID);
			String strPayerCountry = enterprise.Country;
	
			Zipcode zipCode = new Zipcode();
			zipCode.Populate(appID,enterpriseID,strPayerCountry,txtRecipZip.Text.Trim());
			if(zipCode.StateName == null)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECIP_ZIP",utility.GetUserCulture());
				txtRecipCity.Text = "";
				txtRecipState.Text = "";
				txtDestination.Text = "";
				txtRouteCode.Text = "";
				txtRecipZip.Text = "";
				return;
			}
			txtRecipCity.Text = zipCode.StateName;
			txtRecipState.Text = strPayerCountry;
			txtDestination.Text = txtRecipCity.Text;
			txtRouteCode.Text = zipCode.DeliveryRoute;
		
			//calCulateESA(); 
			//calCulateESA_ServiceType();
			this.CalculateAllCharge();
			
			//Calculate Estimate Delivery DateTime
			if ((txtRecipZip.Text.Trim() != "") && (txtShpSvcCode.Text.Trim() != ""))
			{
				DomesticShipmentMgrBAL domesticMgrBAL = new DomesticShipmentMgrBAL();
				if(txtShipEstDlvryDt.Text.Length == 0)
				{
					txtShipEstDlvryDt.Text =  domesticMgrBAL.CalcDlvryDtTime(appID,enterpriseID,System.DateTime.ParseExact(txtShipPckUpTime.Text,"dd/MM/yyyy HH:mm",null),txtShpSvcCode.Text,txtRecipZip.Text).ToString();

					//HC Return Task
					Zipcode zipCodeTmp = new Zipcode();
					zipCodeTmp.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
					String strStateCode = zipCodeTmp.StateCode;

					if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCode.Trim() != ""))
					{
						string strConNo = "";
						string strBookingNo = "";

						if (txtBookingNo.Text.Trim() != "" )
							strBookingNo = txtBookingNo.Text.Trim();
						if (txtConsigNo.Text.Trim() != "" )
							strConNo = txtConsigNo.Text.Trim();

						object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCode,
							strConNo.Trim(), strBookingNo.Trim(), appID, enterpriseID);

						if((tmpReHCDateTime != null) && 
							(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
						}
						else
						{
							txtEstHCDt.Text = "";
						}
					}
					else
					{
						txtEstHCDt.Text = "";
					}
					//HC Return Task
				}					
			}

		}

		private void btnToSaveChanges_Click(object sender, System.EventArgs e)
		{
			if((bool)ViewState["PartialCon"]==true&&(bool)ViewState["PopUpRecipZip"]==true)
			{
				txtRecipTel.Text = (string)ViewState["txtRecipTel"];
				txtRecName.Text = (string)ViewState["txtRecName"];
				txtRecipAddr1.Text = (string)ViewState["txtRecipAddr1"];
				txtRecipAddr2.Text = (string)ViewState["txtRecipAddr2"];
				txtRecipZip.Text = (string)ViewState["txtRecipZip"];
				ViewState["PopUpRecipZip"] = false;
				DomstcShipPanel.Visible = false;
				divMain.Visible=true;
				divDelOperation.Visible=false;
				ddbCustType.Visible = true;
				return;
			}
			//Call the save/Update method.
			//Added By Paew on 09-June-08; Validate Customer ID
			Customer cs = new Customer();
			cs.Populate(appID, enterpriseID,txtCustID.Text.Trim());
			if(chkNewCust.Checked == false)
			{
				if(cs.CustomerName == null)
				{
					this.lblErrorMsg.Text = "Customer ID does not exist.";
					DomstcShipPanel.Visible = false;
					divMain.Visible=true;
					divDelOperation.Visible=false;
					ddbCustType.Visible = true;
					ViewState["AddSurcharge"] = "none";
					return;
				}
			}
			//End Added By Paew on 09-June-08; Validate Customer ID

			DomstcShipPanel.Visible = false;
			divCustIdCng.Visible=false;
			divDelOperation.Visible=false;
			divMain.Visible=true;
			//ddbCustType.Visible = true;
			bool isError = false;
			bool isFirst = true;

			String strSatDel = (string)ViewState["StateSatDel"];
			String strSunDel = (string)ViewState["StateSunDel"];
			String strPubDel = (string)ViewState["StatePubDel"]; 

				
			if ((String)ViewState["AddSurcharge"] == "asking")
			{
				CalcAdditionalSurchrg((String)ViewState["ServiceVASCode"]);
				//ViewState["AddSurcharge"] = "updated";
				
				//Added by GwanG on 08April08
				if(Utility.IsNotDBNull(ViewState["StateErr"]))
				{
					if((string)ViewState["StateErr"] == "Y" && Utility.IsNotDBNull(ViewState["MsgErr"]))
					{
						lblErrorMsg.Text = (string)ViewState["MsgErr"];
						return;
					}
				}


				Zipcode tmpZip = new Zipcode();
				tmpZip.Populate(appID,enterpriseID, txtRecipZip.Text.Trim());

				//Get Service details
				Service service = new Service();
				service.Populate(appID, enterpriseID, txtShpSvcCode.Text.Trim());
				DateTime dtCommitTime = service.CommitTime;
				String strCommitTime = dtCommitTime.ToString("HH:mm");
				decimal iTransitDay = service.TransitDay;
				int iTransitHour = service.TransitHour;

				String strDt = "";
				String strDlvryDtTime = "";

				DateTime TMPPickupDate = System.DateTime.ParseExact(txtShipPckUpTime.Text.Trim(),"dd/MM/yyyy HH:mm",null);
				DateTime TMPEstDelDt = System.DateTime.ParseExact(txtShipEstDlvryDt.Text.Trim(),"dd/MM/yyyy HH:mm",null);

				bool vFlag = false;

				while (vFlag == false)
				{
					//IF Shipment Service is SAME DAY by X JAN 07 08
					if(this.txtShpSvcCode.Text.Substring(0,2)=="SD")
					{
					
						//SAVE NOW & EXIT
						SaveUpdateRecord();
						ViewState["fromInsert"] = true;
						vFlag = true;
						break;
					}
					//END if Shipment Service is SAME DAY

					if (vFlag == false && isFirst == true)
					{
						ViewState["TMPServiceDays"] = Convert.ToInt32(ViewState["TMPServiceDays"]) - 1;

						if (Convert.ToInt32(ViewState["TMPServiceDays"]) == 0)
						{
							strDt = TMPEstDelDt.ToString("dd/MM/yyyy");
							strDlvryDtTime = strDt+" "+strCommitTime;
							txtShipEstDlvryDt.Text = strDlvryDtTime;

							//HC Return Task
							Zipcode zipCode = new Zipcode();
							zipCode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
							String strStateCode = zipCode.StateCode;

							if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCode.Trim() != ""))
							{
								string strConNo = "";
								string strBookingNo = "";

								if (txtBookingNo.Text.Trim() != "" )
									strBookingNo = txtBookingNo.Text.Trim();
								if (txtConsigNo.Text.Trim() != "" )
									strConNo = txtConsigNo.Text.Trim();

								object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCode,
									strConNo.Trim(), strBookingNo.Trim(), appID,enterpriseID);

								if((tmpReHCDateTime != null) && 
									(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
								}
								else
								{
									txtEstHCDt.Text = "";
								}
							}
							else
							{
								txtEstHCDt.Text = "";
							}
							//HC Return Task

							//Both Transit Day and  Transit Hour is zero
							if((iTransitDay == 0) && (iTransitHour == 0))
							{
								//Get the Pickup date & Recipient's cut off time from the zipcode
								strDt = TMPPickupDate.ToString("dd/MM/yyyy");
								Zipcode zipcode = new Zipcode();
								zipcode.Populate(appID,enterpriseID, txtRecipZip.Text.Trim());
								DateTime dtCuttOff =  zipcode.CuttOffTime;
								strCommitTime = dtCuttOff.ToString("HH:mm");
								strDlvryDtTime = strDt+" "+strCommitTime;
							}
							else if((iTransitDay > 0) && (iTransitHour == 0) && (dtCommitTime.Hour == 0) && (dtCommitTime.Minute == 0))
							{
								//Add the day to the pickup request date & Get the pickup Date's time...
								strDt = TMPEstDelDt.ToString("dd/MM/yyyy");

								//Get the commit time from pickUp request Date time
								strCommitTime = TMPPickupDate.ToString("HH:mm");
								strDlvryDtTime = strDt+" "+strCommitTime;
							}

							isError = SaveUpdateRecord();
							ViewState["fromInsert"] = true;
							vFlag = true;
								
							return;
						}	

						isFirst = false;
					}

					TMPEstDelDt = TMPEstDelDt.AddDays(1); 
					txtShipEstDlvryDt.Text = TMPEstDelDt.ToString("dd/MM/yyyy HH:mm");
					//HC Return Task
					Zipcode zipCodeTMP = new Zipcode();
					zipCodeTMP.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
					String strStateCodeTMP = zipCodeTMP.StateCode;

					if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCodeTMP.Trim() != ""))
					{
						string strConNo = "";
						string strBookingNo = "";

						if (txtBookingNo.Text.Trim() != "" )
							strBookingNo = txtBookingNo.Text.Trim();
						if (txtConsigNo.Text.Trim() != "" )
							strConNo = txtConsigNo.Text.Trim();

						object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCodeTMP,
							strConNo.Trim(), strBookingNo.Trim(), appID,enterpriseID);

						if((tmpReHCDateTime != null) && 
							(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
						}
						else
						{
							txtEstHCDt.Text = "";
						}
					}
					else
					{
						txtEstHCDt.Text = "";
					}
					//HC Return Task

					// Saturday
					
					if(TMPEstDelDt.DayOfWeek == System.DayOfWeek.Saturday)
					{
						lblConfirmMsg.Text = txtShipEstDlvryDt.Text.ToString()+ " Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge will be imposed. Do you want to continue?";
						DomstcShipPanel.Visible = true;
						divCustIdCng.Visible=false;
						divMain.Visible=false;
						divDelOperation.Visible=false;
						ddbCustType.Visible = false;
						ViewState["AddSurcharge"] = "asking";
						ViewState["ServiceVASCode"] = "SATDEL";

						vFlag = true;
					}

					// Sunday
					if(TMPEstDelDt.DayOfWeek == System.DayOfWeek.Sunday)
					{
						lblConfirmMsg.Text = txtShipEstDlvryDt.Text.ToString()+ " Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge will be imposed. Do you want to continue?";
						DomstcShipPanel.Visible = true;
						divCustIdCng.Visible=false;
						divMain.Visible=false;
						divDelOperation.Visible=false;
						ddbCustType.Visible = false;
						ViewState["AddSurcharge"] = "asking";
						ViewState["ServiceVASCode"] = "SUNDEL";

						vFlag = true;

					}

					// Monday - Friday
					if((TMPEstDelDt.DayOfWeek != System.DayOfWeek.Saturday) && (TMPEstDelDt.DayOfWeek != System.DayOfWeek.Sunday))
					{
						bool isPubDay = DomesticShipmentMgrDAL.IsEnterpriseHolidayOfAutoManifest(appID, enterpriseID, TMPEstDelDt, tmpZip.StateCode);

						if(isPubDay)
						{
							lblConfirmMsg.Text = txtShipEstDlvryDt.Text.ToString()+ " Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge will be imposed. Do you want to continue?";
							DomstcShipPanel.Visible = true;
							divCustIdCng.Visible=false;
							divMain.Visible=false;
							divDelOperation.Visible=false;
							ddbCustType.Visible = false;
							ViewState["AddSurcharge"] = "asking";
							ViewState["ServiceVASCode"] = "PUBDEL";

							vFlag = true;

						}
					}

					if (vFlag == false)
					{
						ViewState["TMPServiceDays"] = Convert.ToInt32(ViewState["TMPServiceDays"]) - 1;

						if (Convert.ToInt32(ViewState["TMPServiceDays"]) == 0)
						{
							strDt = TMPEstDelDt.ToString("dd/MM/yyyy");
							strDlvryDtTime = strDt+" "+strCommitTime;
							txtShipEstDlvryDt.Text = strDlvryDtTime;
							//HC Return Task
							Zipcode zipCode = new Zipcode();
							zipCode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
							String strStateCode = zipCode.StateCode;

							if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCode.Trim() != ""))
							{
								string strConNo = "";
								string strBookingNo = "";

								if (txtBookingNo.Text.Trim() != "" )
									strBookingNo = txtBookingNo.Text.Trim();
								if (txtConsigNo.Text.Trim() != "" )
									strConNo = txtConsigNo.Text.Trim();

								object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCode,
									strConNo.Trim(), strBookingNo.Trim(), appID,enterpriseID);

								if((tmpReHCDateTime != null) && 
									(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
								}
								else
								{
									txtEstHCDt.Text = "";
								}
							}
							else
							{
								txtEstHCDt.Text = "";
							}
							//HC Return Task

							//Both Transit Day and  Transit Hour is zero
							if((iTransitDay == 0) && (iTransitHour == 0))
							{
								//Get the Pickup date & Recipient's cut off time from the zipcode
								strDt = TMPPickupDate.ToString("dd/MM/yyyy");
								Zipcode zipcode = new Zipcode();
								zipcode.Populate(appID,enterpriseID, txtRecipZip.Text.Trim());
								DateTime dtCuttOff =  zipcode.CuttOffTime;
								strCommitTime = dtCuttOff.ToString("HH:mm");
								strDlvryDtTime = strDt+" "+strCommitTime;
							}
							else if((iTransitDay > 0) && (iTransitHour == 0) && (dtCommitTime.Hour == 0) && (dtCommitTime.Minute == 0))
							{
								//Add the day to the pickup request date & Get the pickup Date's time...
								strDt = TMPEstDelDt.ToString("dd/MM/yyyy");

								//Get the commit time from pickUp request Date time
								strCommitTime = TMPPickupDate.ToString("HH:mm");
								strDlvryDtTime = strDt+" "+strCommitTime;
							}

							isError = SaveUpdateRecord();
							ViewState["fromInsert"] = true;
							vFlag = true;
						}	
					}
				}
				//Call the surcharge calculation method
				/*if((String)ViewState["ServiceVASCode"] != "")
				{
					
				}*/
			}
			else
			{
				//Call the save/Update method
				if((String)ViewState["NextOperation"] == "Query")
				{
						
					if(txtConsigNo.Text.Trim()=="")
					{
						lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"SEL_CONSGMT_NO",utility.GetUserCulture());
						return;
					}
					if(txtCustID.Text.Trim()=="")
					{
						lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"SEL_CUST_TYPE",utility.GetUserCulture());
						return;
					}
					if(txtCustAdd1.Text.Trim()=="")
					{
						lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_CUST_ADDR",utility.GetUserCulture());
						return;
					}
					if(txtCustZipCode.Text.Trim()=="")
					{
						lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_ZIP",utility.GetUserCulture());
						return;
					}
					if(txtSendName.Text.Trim()=="")
					{
						lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SEND_NAME",utility.GetUserCulture());
						return;
					}
					if(txtSendAddr1.Text.Trim()=="")
					{
						lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SEND_ADDR",utility.GetUserCulture());
						return;
					}
					if(txtSendZip.Text.Trim()=="")
					{
						lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SEND_ZIP",utility.GetUserCulture());
						return;
					}
					if(txtRecName.Text.Trim()=="")
					{
						lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_RECP_NAME",utility.GetUserCulture());
						return;
					}
					if(txtRecipZip.Text.Trim()=="")
					{
						lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_RECP_ZIP",utility.GetUserCulture());
						return;
					}
						
					//						if(txtPkgCommCode.Text.Trim()=="")
					//						{
					//							lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_CMDY_CODE",utility.GetUserCulture());
					//							return;
					//						}
					if(txtShpSvcCode.Text.Trim()=="")
					{
						lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SERVICE",utility.GetUserCulture());
						return;
					}
					//						if(txtShpDclrValue.Text.Trim()=="")
					//						{
					//							lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DECLARE=",utility.GetUserCulture());
					//							return;
					//						}
																			 
													 
				}
				//bool isError = SaveUpdateRecord();
					
				ViewState["isTextChanged"] = false;
				if(isError == false)
				{
					if((String)ViewState["NextOperation"] == "Insert")
					{
						Insert_Click();
					}
					else if((String)ViewState["NextOperation"] == "ExecuteQuery")
					{
						ExecQry_Click();
					}
					else if((String)ViewState["NextOperation"] == "Query")
					{
						Qry_Click();
					}
					else if((String)ViewState["NextOperation"] == "MoveFirst")
					{
						MoveFirstDS();
						if ((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery)
						{
							FetchDSPKG();
						}
					}
					else if((String)ViewState["NextOperation"] == "MoveNext")
					{
						MoveNextDS();
						if ((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery)
						{
							FetchDSPKG();
						}
					}
					else if((String)ViewState["NextOperation"] == "MoveLast")
					{
						MoveLastDS();
						if ((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery)
						{
							FetchDSPKG();
						}
					}
					else if((String)ViewState["NextOperation"] == "MovePrevious")
					{
						MovePreviousDS();
						if ((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery)
						{
							FetchDSPKG();
						}
					}
				}
			}
		}

		private void ReCalculateDlvryDt()
		{
			ViewState["AddSurcharge"] = "none";

			DateTime dtDlvryDt = System.DateTime.ParseExact(txtShipEstDlvryDt.Text.Trim(),"dd/MM/yyyy HH:mm",null);

			//Calculate the next date 
			dtDlvryDt = dtDlvryDt.AddDays(1);

			txtShipEstDlvryDt.Text = dtDlvryDt.ToString("dd/MM/yyyy HH:mm");

			//HC Return Task
			Zipcode zipCode = new Zipcode();
			zipCode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
			String strStateCode = zipCode.StateCode;

			if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCode.Trim() != ""))
			{
				string strConNo = "";
				string strBookingNo = "";

				if (txtBookingNo.Text.Trim() != "" )
					strBookingNo = txtBookingNo.Text.Trim();
				if (txtConsigNo.Text.Trim() != "" )
					strConNo = txtConsigNo.Text.Trim();

				object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCode,
					strConNo.Trim(), strBookingNo.Trim(), appID, enterpriseID);

				if((tmpReHCDateTime != null) && 
					(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
				}
				else
				{
					txtEstHCDt.Text = "";
				}
			}
			else
			{
				txtEstHCDt.Text = "";
			}
			//HC Return Task
			CheckTheCalcDay();
			
		}

		private void CheckTheCalcDay()
		{
			//Check whether the calculated day has service
			ServiceAvailable serviceAvailable =  CheckDayServiceAvail();

			//Weekend / Holiday
			if(((String)ViewState["Day"] == "WeekEnd") || ((String)ViewState["Day"] == "Holiday"))
			{
				//Check service for weekend/holiday
				if((serviceAvailable.isServiceAvail == true) && ((String)ViewState["AddSurcharge"] == "none"))
				{
					ViewState["ServiceVASCode"] = serviceAvailable.strVASCode;
					//display the popup msg if service available for weekend/holiday
					//strErrorMsg = "Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge ";
					/*lblConfirmMsg.Text = txtShipEstDlvryDt.Text.ToString()+ " Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge will be imposed. Do you want to continue?";
					DomstcShipPanel.Visible = true;
					divMain.Visible=false;
					divDelOperation.Visible=false;
					divCustIdCng.Visible=false;
					ddbCustType.Visible = false;
					ViewState["AddSurcharge"] = "asking";*/
				}
				else
				{
					//No service available weekend/Holiday so add 1 day & again check whether weekend/holiday.
					DateTime dtDlvryDt = System.DateTime.ParseExact(txtShipEstDlvryDt.Text.Trim(),"dd/MM/yyyy HH:mm",null);
					dtDlvryDt = dtDlvryDt.AddDays(1);
					txtShipEstDlvryDt.Text = dtDlvryDt.ToString("dd/MM/yyyy HH:mm");

					//HC Return Task
					Zipcode zipCode = new Zipcode();
					zipCode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
					String strStateCode = zipCode.StateCode;

					if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCode.Trim() != ""))
					{
						string strConNo = "";
						string strBookingNo = "";

						if (txtBookingNo.Text.Trim() != "" )
							strBookingNo = txtBookingNo.Text.Trim();
						if (txtConsigNo.Text.Trim() != "" )
							strConNo = txtConsigNo.Text.Trim();

						object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCode,
							strConNo.Trim(), strBookingNo.Trim(), appID, enterpriseID);

						if((tmpReHCDateTime != null) && 
							(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
						}
						else
						{
							txtEstHCDt.Text = "";
						}
					}
					else
					{
						txtEstHCDt.Text = "";
					}
					//HC Return Task

					CheckTheCalcDay();
				}
			}
		}

		private void btnNotToSave_Click(object sender, System.EventArgs e)
		{
			if(ViewState["ClickNoHoliday"]==null)
				ViewState["ClickNoHoliday"]=0;
			ViewState["ClickNoHoliday"]=Int32.Parse(ViewState["ClickNoHoliday"].ToString())+1;
			if((bool)ViewState["PartialCon"]==true&&(bool)ViewState["PopUpRecipZip"]==true)
			{
				txtRecipTel.Text = (string)ViewState["txtRecipTel"];
				txtRecName.Text = (string)ViewState["txtRecName"];
				txtRecipAddr1.Text = (string)ViewState["txtRecipAddr1"];
				txtRecipAddr2.Text = (string)ViewState["txtRecipAddr2"];
				//txtRecipZip.Text = (string)ViewState["txtRecipZip"];
				ViewState["PopUpRecipZip"] = false;
				DomstcShipPanel.Visible = false;
				divMain.Visible=true;
				divDelOperation.Visible=false;
				ddbCustType.Visible = true;
				return;
			}
			//Added by Gwang on 08April08
			String strSatDel = (string)ViewState["StateSatDel"];
			String strSunDel = (string)ViewState["StateSunDel"];
			String strPubDel = (string)ViewState["StatePubDel"]; 


			DomstcShipPanel.Visible = false;
			divCustIdCng.Visible=false;
			divMain.Visible=true;
			divDelOperation.Visible=false;
			//ddbCustType.Visible = true;
			if ((String)ViewState["AddSurcharge"] == "asking")
			{
				Zipcode tmpZip = new Zipcode();
				tmpZip.Populate(appID,enterpriseID, txtRecipZip.Text.Trim());

				//Get Service details
				Service service = new Service();
				service.Populate(appID, enterpriseID, txtShpSvcCode.Text.Trim());
				DateTime dtCommitTime = service.CommitTime;
				String strCommitTime = dtCommitTime.ToString("HH:mm");
				decimal iTransitDay = service.TransitDay;
				int iTransitHour = service.TransitHour;

				String strDt = "";
				String strDlvryDtTime = "";

				DateTime TMPPickupDate = System.DateTime.ParseExact(txtShipPckUpTime.Text.Trim(),"dd/MM/yyyy HH:mm",null);
				DateTime TMPEstDelDt = System.DateTime.ParseExact(txtShipEstDlvryDt.Text.Trim(),"dd/MM/yyyy HH:mm",null);

				bool vFlag = false;

				while (vFlag == false)
				{
					//IF Shipment Service is SAME DAY by X JAN 07 08
					if(this.txtShpSvcCode.Text.Substring(0,2)=="SD")
					{
					
						//SAVE NOW & EXIT
						SaveUpdateRecord();
						ViewState["fromInsert"] = true;
						vFlag = true;
						break;
					}
					//END if Shipment Service is SAME DAY

					TMPEstDelDt = TMPEstDelDt.AddDays(1); 
					txtShipEstDlvryDt.Text = TMPEstDelDt.ToString("dd/MM/yyyy HH:mm");
					//HC Return Task
					Zipcode zipCode = new Zipcode();
					zipCode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
					String strStateCode = zipCode.StateCode;

					if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCode.Trim() != ""))
					{
						string strConNo = "";
						string strBookingNo = "";

						if (txtBookingNo.Text.Trim() != "" )
							strBookingNo = txtBookingNo.Text.Trim();
						if (txtConsigNo.Text.Trim() != "" )
							strConNo = txtConsigNo.Text.Trim();

						object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCode,
							strConNo.Trim(), strBookingNo.Trim(), appID,enterpriseID);

						if((tmpReHCDateTime != null) && 
							(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
						}
						else
						{
							txtEstHCDt.Text = "";
						}
					}
					else
					{
						txtEstHCDt.Text = "";
					}
					//HC Return Task

					// Saturday
					if(TMPEstDelDt.DayOfWeek == System.DayOfWeek.Saturday)
					{
						lblConfirmMsg.Text = txtShipEstDlvryDt.Text.ToString()+ " Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge will be imposed. Do you want to continue?";
						DomstcShipPanel.Visible = true;
						divCustIdCng.Visible=false;
						divMain.Visible=false;
						divDelOperation.Visible=false;
						ddbCustType.Visible = false;
						ViewState["AddSurcharge"] = "asking";
						ViewState["ServiceVASCode"] = "SATDEL";

						vFlag = true;					
					}

					// Sunday
					if(TMPEstDelDt.DayOfWeek == System.DayOfWeek.Sunday)
					{
						lblConfirmMsg.Text = txtShipEstDlvryDt.Text.ToString()+ " Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge will be imposed. Do you want to continue?";
						DomstcShipPanel.Visible = true;
						divCustIdCng.Visible=false;
						divMain.Visible=false;
						divDelOperation.Visible=false;
						ddbCustType.Visible = false;
						ViewState["AddSurcharge"] = "asking";
						ViewState["ServiceVASCode"] = "SUNDEL";

						vFlag = true;
						
					}

					// Monday - Friday
					if((TMPEstDelDt.DayOfWeek != System.DayOfWeek.Saturday) && (TMPEstDelDt.DayOfWeek != System.DayOfWeek.Sunday))
					{
						bool isPubDay = DomesticShipmentMgrDAL.IsEnterpriseHolidayOfAutoManifest(appID, enterpriseID, TMPEstDelDt, tmpZip.StateCode);

						if(isPubDay)
						{

							lblConfirmMsg.Text = txtShipEstDlvryDt.Text.ToString()+ " Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge will be imposed. Do you want to continue?";
							DomstcShipPanel.Visible = true;
							divCustIdCng.Visible=false;
							divMain.Visible=false;
							divDelOperation.Visible=false;
							ddbCustType.Visible = false;
							ViewState["AddSurcharge"] = "asking";
							ViewState["ServiceVASCode"] = "PUBDEL";

							vFlag = true;

						}
					}

					if (vFlag == false)
					{
						ViewState["TMPServiceDays"] = Convert.ToInt32(ViewState["TMPServiceDays"]) - 1;

						if (Convert.ToInt32(ViewState["TMPServiceDays"]) == 0)
						{
							strDt = TMPEstDelDt.ToString("dd/MM/yyyy");
							strDlvryDtTime = strDt+" "+strCommitTime;
							txtShipEstDlvryDt.Text = strDlvryDtTime;

							//HC Return Task
							Zipcode zipCodeTMP = new Zipcode();
							zipCodeTMP.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
							String strStateCodeTMP = zipCodeTMP.StateCode;

							if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCodeTMP.Trim() != ""))
							{
								string strConNo = "";
								string strBookingNo = "";

								if (txtBookingNo.Text.Trim() != "" )
									strBookingNo = txtBookingNo.Text.Trim();
								if (txtConsigNo.Text.Trim() != "" )
									strConNo = txtConsigNo.Text.Trim();

								object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCodeTMP,
									strConNo.Trim(), strBookingNo.Trim(), appID,enterpriseID);

								if((tmpReHCDateTime != null) && 
									(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
								}
								else
								{
									txtEstHCDt.Text = "";
								}
							}
							else
							{
								txtEstHCDt.Text = "";
							}
							//HC Return Task

							//Both Transit Day and  Transit Hour is zero
							if((iTransitDay == 0) && (iTransitHour == 0))
							{
								//Get the Pickup date & Recipient's cut off time from the zipcode
								strDt = TMPPickupDate.ToString("dd/MM/yyyy");
								Zipcode zipcode = new Zipcode();
								zipcode.Populate(appID,enterpriseID, txtRecipZip.Text.Trim());
								DateTime dtCuttOff =  zipcode.CuttOffTime;
								strCommitTime = dtCuttOff.ToString("HH:mm");
								strDlvryDtTime = strDt+" "+strCommitTime;
							}
							else if((iTransitDay > 0) && (iTransitHour == 0) && (dtCommitTime.Hour == 0) && (dtCommitTime.Minute == 0))
							{
								//Add the day to the pickup request date & Get the pickup Date's time...
								strDt = TMPEstDelDt.ToString("dd/MM/yyyy");

								//Get the commit time from pickUp request Date time
								strCommitTime = TMPPickupDate.ToString("HH:mm");
								strDlvryDtTime = strDt+" "+strCommitTime;
							}

							SaveUpdateRecord();
							ViewState["fromInsert"] = true;
							vFlag = true;
						}	
					}
				}
				//ReCalculateDlvryDt();

				//Check whether it is holiday/Sat/Sun
				//ServiceAvailable serviceAvailable =  CheckDayServiceAvail();

				/*if((serviceAvailable.isServiceAvail == true) && ((String)ViewState["AddSurcharge"] == "none"))
				{
					ViewState["ServiceVASCode"] = serviceAvailable.strVASCode;
					//display the popup msg
					lblConfirmMsg.Text = txtShipEstDlvryDt.Text.ToString()+ " Weekend / Public Holiday Delivery Service is available. Weekend / Public Holiday Surcharge will be imposed. Do you want to continue?";
					DomstcShipPanel.Visible = true;
					divMain.Visible=false;
					divDelOperation.Visible=false;
					ddbCustType.Visible = false;
					ViewState["AddSurcharge"] = "asking";
					return;
				}*/
			}
			else
			{
				ViewState["DSOperation"] = Operation.None;
				ViewState["isTextChanged"] = false;
			}
			
			if((String)ViewState["NextOperation"] == "Insert")
			{
				Insert_Click();
			}
			else if((String)ViewState["NextOperation"] == "ExecuteQuery")
			{
				ExecQry_Click();
			}
			else if((String)ViewState["NextOperation"] == "Query" && Convert.ToInt32(ViewState["TMPServiceDays"]) == 0)
			{
				Qry_Click();
			}
			else if((String)ViewState["NextOperation"] == "MoveFirst")
			{
				MoveFirstDS();
				if ((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery)
				{
					FetchDSPKG();
				}
			}
			else if((String)ViewState["NextOperation"] == "MoveNext")
			{
				MoveNextDS();
				if ((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery)
				{
					FetchDSPKG();
				}
			}
			else if((String)ViewState["NextOperation"] == "MoveLast")
			{
				MoveLastDS();
				if ((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery)
				{
					FetchDSPKG();
				}
			}
			else if((String)ViewState["NextOperation"] == "MovePrevious")
			{
				MovePreviousDS();
				if ((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery)
				{
					FetchDSPKG();
				}
			}
		}

		private void btnToCancel_Click(object sender, System.EventArgs e)
		{
			DomstcShipPanel.Visible = false;
			divMain.Visible=true;
			divDelOperation.Visible=false;
			//ddbCustType.Visible = true;
			ViewState["AddSurcharge"] = "none";
		}

		private void btnSendCust_Click(object sender, System.EventArgs e)
		{
			String strID = txtCustID.Text;

			if(strID == "NEW")
				strID = "";

			strErrorMsg = "";
			if(ddbCustType.SelectedItem.Value == "0")
			{
				strErrorMsg = "Select the Customer type.";
			}
			else 
			{
				String sUrl = "SndRecipPopup.aspx?FORMID="+"DomesticShipment"+
					"&SENDRCPTYPE="+"S"+
					"&SENDERNAME="+txtSendName.Text.Trim()+
					"&ZIPCODE="+txtRecipZip.Text+
					"&CUSTID="+strID+
					"&DestZipCode="+txtRecipZip.Text.Trim()+
					"&SENDZIP="+txtSendZip.Text.Trim()+
					"&CUSTTYPE=C";
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);

				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			
			lblErrorMsg.Text = strErrorMsg;

		}
		private void btnRecipNm_Click(object sender, System.EventArgs e)
		{
			String strID = txtCustID.Text;

			if(strID == "NEW")
				strID = "";

			strErrorMsg = "";
			if(ddbCustType.SelectedItem.Value == "0")
			{
				strErrorMsg = "Select the Customer type.";
			}
			else 
			{
				String sUrl = "SndRecipPopup.aspx?FORMID="+"DomesticShipment"+
					"&SENDRCPTYPE="+"R"+
					"&SENDERNAME="+txtRecName.Text.Trim()+
					"&ZIPCODE="+txtRecipZip.Text+
					"&CUSTID="+strID+
					"&DestZipCode="+txtRecipZip.Text.Trim()+
					"&SENDZIP="+txtSendZip.Text.Trim()+
					"&CUSTTYPE=C" +
					"&ESTDELDT=" + txtShipEstDlvryDt.Text.TrimStart().TrimEnd()+
					"&STRCON=" + txtConsigNo.Text.Trim() +
					"&STRBOOKINNO=" + txtBookingNo.Text.Trim();	//HC Return Task

				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);

				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			
			lblErrorMsg.Text = strErrorMsg;
		}

		private void txtShipEstDlvryDt_TextChanged(object sender, System.EventArgs e)
		{
			//lblErrorMsg.Text = "";
			if(txtShpSvcCode.Text.Length == 0)
			{
				//Check whether service code entry has been done.
				lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SERVICE",utility.GetUserCulture());
				txtShipEstDlvryDt.Text = "";
				return;
			}
			else if(txtRecipZip.Text.Length == 0)
			{
				//Check for receipient zipcode entry.
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_RECIP_ZIP",utility.GetUserCulture());
				txtShipEstDlvryDt.Text = "";
				return;
			}
			

			if(txtShipEstDlvryDt.Text.Length > 0)
			{
				RegularEstDlvryDt.Validate();
               
				if ((RegularEstDlvryDt.IsValid) && (txtShipEstDlvryDt.Text.Length > 0) && (txtShipManifestDt.Text.Length > 0))
				{
					if(txtBookingNo.Text.Trim()=="")
					{
						int iCompare = DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null).CompareTo(DateTime.ParseExact(txtShipManifestDt.Text,"dd/MM/yyyy HH:mm",null));
				
						if(iCompare < 0)
						{
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DELETED_DT_LESS_SHPT_MFST_DT",utility.GetUserCulture());
							//txtShipEstDlvryDt.Text = "";
							return;
						}
					}


					if((bool)ViewState["isTextChanged"] == false)
					{
						ChangeDSState();
						ViewState["isTextChanged"] = true;
					}

					//Check whether the date is less than the required days for the service.
					if((txtShipEstDlvryDt.Text.Length > 0) && (txtShipPckUpTime.Text.Length > 0) && (txtShpSvcCode.Text.Length > 0))
					{
						TransitDayNServiceCode DayNServiceCode = TIESUtility.IsDelvryDtLess(appID,enterpriseID,txtShpSvcCode.Text,DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null),DateTime.ParseExact(txtShipPckUpTime.Text,"dd/MM/yyyy HH:mm",null));
						if(DayNServiceCode.isLess)
						{
							//SERVICE_CODE_REQ=For <#> service code atleast <#> day(s) is/are required
							ArrayList paramValues = new ArrayList();
							paramValues.Add(""+DayNServiceCode.strServiceCode);
							paramValues.Add(""+DayNServiceCode.iTransitDay);
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SERVICE_CODE_REQ",utility.GetUserCulture(),paramValues);
							return;
						}
					}

					//On Changing the Estimated Delivery date 
					if((txtRecName.Text.Length > 0) && (txtRecipAddr1.Text.Length > 0) )
						DatePkUpDlvryTextChange();

					//Get the commit time from the table.
					DomesticShipmentMgrBAL domesticMgrBAL = new DomesticShipmentMgrBAL();
					ServiceTime serviceTime =  domesticMgrBAL.GetCommitTime(appID,enterpriseID,txtShpSvcCode.Text,txtRecipZip.Text);
					if(serviceTime.HourServiceTime != 0)
					{
						//Find the difference of the entered time & system time
						DateTime dtSystem1 = DateTime.Now;
						String dtSysHour = dtSystem1.ToString("HH:mm");
						DateTime dtSystem = System.DateTime.ParseExact(dtSysHour,"HH:mm",null);
						String strEntered = System.DateTime.ParseExact(txtShipEstDlvryDt.Text.Trim(),"dd/MM/yyyy HH:mm",null).ToString("HH:mm");
						DateTime dtEntered = System.DateTime.ParseExact(strEntered,"HH:mm",null);
						System.TimeSpan dtDiff = dtEntered.Subtract(dtSystem);
						int iDiffHrsMin = (dtDiff.Hours * 60);
						int iDiffTotalMin =  dtDiff.Minutes+iDiffHrsMin;
						int iSerMinutes = (serviceTime.HourServiceTime * 60);
						if(!(iDiffTotalMin >= iSerMinutes))
						{
							//If the entered time is less than the required time for delivery recalculate the time.
							String tmpDate = System.DateTime.ParseExact(txtShipEstDlvryDt.Text.Trim(),"dd/MM/yyyy HH:mm",null).ToString("dd/MM/yyyy");
							DateTime dtEstDlvy = System.DateTime.ParseExact(tmpDate,"dd/MM/yyyy",null);
							dtEstDlvy = System.DateTime.ParseExact(dtEstDlvy.ToString("dd/MM/yyyy")+" "+dtSystem.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null);
							txtShipEstDlvryDt.Text = dtEstDlvy.AddMinutes((double)iSerMinutes).ToString("dd/MM/yyyy HH:mm");

							//HC Return Task
							Zipcode zipCode = new Zipcode();
							zipCode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
							String strStateCode = zipCode.StateCode;

							if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCode.Trim() != ""))
							{
								string strConNo = "";
								string strBookingNo = "";

								if (txtBookingNo.Text.Trim() != "" )
									strBookingNo = txtBookingNo.Text.Trim();
								if (txtConsigNo.Text.Trim() != "" )
									strConNo = txtConsigNo.Text.Trim();

								object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCode,
									strConNo.Trim(), strBookingNo.Trim(), appID, enterpriseID);

								if((tmpReHCDateTime != null) && 
									(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
								}
								else
								{
									txtEstHCDt.Text = "";
								}
							}
							else
							{
								txtEstHCDt.Text = "";
							}
							//HC Return Task
						}
					
					}
					else if(serviceTime.RegularTime != null)
					{
						//The entered time should be less than or equal to the commit time.
						System.DateTime.ParseExact(serviceTime.RegularTime,"HH:mm",null);
						String strEnteredTime = System.DateTime.ParseExact(txtShipEstDlvryDt.Text.Trim(),"dd/MM/yyyy HH:mm",null).ToString("HH:mm");
						System.DateTime.ParseExact(strEnteredTime,"HH:mm",null);
						if(!(System.DateTime.ParseExact(strEnteredTime,"HH:mm",null) <= System.DateTime.ParseExact(serviceTime.RegularTime,"HH:mm",null)))
						{
							String tmpDateTime = System.DateTime.ParseExact(txtShipEstDlvryDt.Text.Trim(),"dd/MM/yyyy HH:mm",null).ToString("dd/MM/yyyy")+" "+System.DateTime.ParseExact(serviceTime.RegularTime,"HH:mm",null).ToString("HH:mm");
							DateTime dtTemp =  System.DateTime.ParseExact(tmpDateTime,"dd/MM/yyyy HH:mm",null);
							txtShipEstDlvryDt.Text = dtTemp.ToString("dd/MM/yyyy HH:mm");

							//HC Return Task
							Zipcode zipCode = new Zipcode();
							zipCode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
							String strStateCode = zipCode.StateCode;

							if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCode.Trim() != ""))
							{
								string strConNo = "";
								string strBookingNo = "";

								if (txtBookingNo.Text.Trim() != "" )
									strBookingNo = txtBookingNo.Text.Trim();
								if (txtConsigNo.Text.Trim() != "" )
									strConNo = txtConsigNo.Text.Trim();

								object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCode,
									strConNo.Trim(), strBookingNo.Trim(), appID, enterpriseID);

								if((tmpReHCDateTime != null) && 
									(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
								}
								else
								{
									txtEstHCDt.Text = "";
								}
							}
							else
							{
								txtEstHCDt.Text = "";
							}
							//HC Return Task
						}
					}
					else
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_COMMIT",utility.GetUserCulture());
						return;
					}

				}
				else if(RegularEstDlvryDt.IsValid == false)
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT_DDMMYYYYHHMM",utility.GetUserCulture());
					return;
				}
			}
		}

		private void Qry_Click()
		{
			bool state = false;
			if (ViewState["fromInsert"] != null) 
			{
				state = (bool)ViewState["fromInsert"];
				if(!state)
				{
					if(btnSave.Enabled)
					{
						if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
						{
							lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
							DomstcShipPanel.Visible = true;
							divMain.Visible=false;
							divDelOperation.Visible=false;
							ddbCustType.Visible = false;
							//ViewState["MoveFirst"]=true;
							ViewState["NextOperation"] = "Query";
							return;
						}
					}
					else
					{
						ViewState["isTextChanged"] = false;
						//ViewState["DSOperation"] =Operation.None;
					}
				}
			}
			else
			{
				if(btnSave.Enabled)
				{
					if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
					{
						lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
						DomstcShipPanel.Visible = true;
						divMain.Visible=false;
						divDelOperation.Visible=false;
						ddbCustType.Visible = false;
						//ViewState["MoveFirst"]=true;
						ViewState["NextOperation"] = "Query";
						return;
					}
				}
				else
				{

					ViewState["isTextChanged"] = false;
					//ViewState["DSOperation"] =Operation.None;
				}
			}
			
			btnExecQry.Enabled = true;
			chkNewCust.Enabled = true;
			Query();
			ViewState["fromInsert"] = false;
		}
		private void btnQry_Click(object sender, System.EventArgs e)
		{
			
			//decimal deTest  = TIESUtility.EnterpriseRounding((decimal)2500.65,1,1);
			//			Response.Write("<script>alert('"+deTest.ToString()+"');</script>");
			SetOriginalCtrl(false);
			Qry_Click();
			ViewState["PartialCon"]=false;

			btnViewOldPD.Visible = false;
			EnterpriseConfigurations();
		}

		private void ResetForQuery()
		{
			rbCash.Checked = false;
			rbCredit.Checked = false;
			rbtnShipFrghtColl.Checked = false;
			//			rbtnShipFrghtPre.Checked = false;
			//			rbtnShipFrghtColl.Enabled = true;
			rbtnShipFrghtPre.Enabled = true;
			chkshpRtnHrdCpy.Checked = false;

			//HC Return Task
			chkInvHCReturn.Checked = false;
			//HC Return Task

			txtBookingNo.Enabled = true;
			txtBookDate.Enabled = true;
			txtDelManifest.Enabled = true;
			txtRouteCode.Enabled = true;
			txtConsigNo.Enabled = true;
			m_sdsDomesticShip = DomesticShipmentMgrDAL.GetDomShipSessionDS();
			Session["SESSION_DS3"] = m_sdsDomesticShip;
			txtShipManifestDt.Enabled = true;
		}

		private void Query()
		{
			btnPkgDetails.Enabled = false;
			if (((bool)ViewState["isTextChanged"] == true)&& ((int)ViewState["DSMode"]==(int)ScreenMode.ExecuteQuery)) 
			{
				lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				DomstcShipPanel.Visible = true;
				divCustIdCng.Visible=false;
				divMain.Visible=false;
				divDelOperation.Visible=false;
				ddbCustType.Visible = false;
				ViewState["DSMode"] = ScreenMode.Query;
				return;
			}
			//
			m_dsVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
			Session["SESSION_DS1"] = m_dsVAS;
			BindVASGrid();
			//
			ViewState["DSMode"] = ScreenMode.Query;
			ClearAllFields();
			lblErrorMsg.Text =""; 
			btnExecQry.Enabled = true;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			btnGoToFirstPage.Enabled = false;
			btnNextPage.Enabled = false;
			btnPreviousPage.Enabled = false;
			btnGoToFirstPage.Enabled = false;
			btnGoToLastPage.Enabled = false;
			txtGoToRec.Text = "";			
			ResetForQuery();			
			ViewState["DSOperation"] = Operation.None;
			ViewState["isTextChanged"] = false;
			ViewState["currentPage"] = 0;
			txtBookingNo.AutoPostBack = false;
			txtConsigNo.AutoPostBack=false;
			//			this.rbCash.Enabled = true;
			//			this.rbCredit.Enabled = true;
			rbCash.Enabled = false;
			rbCredit.Enabled = false;
		}

		private void ExecQry_Click()
		{			
			btnPkgDetails.Enabled = true;
			txtBookingNo.Enabled = false;
			txtConsigNo.Enabled = false;
			
			if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
			{
				lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				DomstcShipPanel.Visible = true;
				divCustIdCng.Visible=false;
				divMain.Visible=false;
				divDelOperation.Visible=false;
				ddbCustType.Visible = false;
				//ViewState["MoveFirst"]=true;
				ViewState["NextOperation"] = "ExecuteQuery";
				return;
			}			
			
			ExecuteQueryDS();

			if(txtCustID.Text == "CASH")
			{
				chkNewCust.Checked = false;
				chkNewCust.Enabled = false;
			}
			else
			{
				chkNewCust.Enabled = true;
			}
			if(txtBookingNo.Text.Trim()!="")
			{
				int iBookingNo = Convert.ToInt32(txtBookingNo.Text.Trim());
				DataSet dsPickUpData = DomesticShipmentMgrDAL.GetFromPickUp(appID,enterpriseID,iBookingNo);
				int cnt = dsPickUpData.Tables[0].Rows.Count;
				if(cnt<=0)
				{
					//					this.rbCash.Enabled = true;
					//					this.rbCredit.Enabled = true;
					rbCash.Enabled = false;
					rbCredit.Enabled = false;
				}
				else
				{
					this.rbCash.Enabled = false;
					this.rbCredit.Enabled = false;
				}
			}
		}
		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			try
			{
				ViewState["InitialBooking"] = true;
				btnExecQry.Enabled=false;
				ExecQry_Click();
				EnterpriseConfigurations();
				SetInitialFocus(txtRefNo);
				//				CalculateAllCharge();
			}
			catch(Exception ex)
			{
				Response.Write(ex);
			}
			
		}
		
		private void ExecuteQueryDS()
		{
			bool executeNotInputText = true;
			if (txtConsigNo.Text.Trim() != "")
				executeNotInputText = false;


			GetValuesIntoDS(0);
			ViewState["QUERY_DS"] = m_sdsDomesticShip.ds;
			ViewState["DSMode"] = ScreenMode.ExecuteQuery;
			ViewState["DSOperation"] = Operation.None;
			ViewState["isTextChanged"] = false;
			ViewState["currentPage"] = 0;
			ViewState["currentSet"] = 0;
			FetchDSRecSet();
			if(m_sdsDomesticShip.QueryResultMaxSize == 0)
			{
				lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
				EnableNavigationButtons(false,false,false,false);
				return;
			}
			DisplayRecords();
			if(executeNotInputText == false)
				Compare_PD_SPKG();
			FetchDSPKG();
			btnExecQry.Enabled = false;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			txtDelManifest.Enabled = false;
			txtShipManifestDt.Enabled = false;
			Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete,true,m_moduleAccessRights);
			txtRouteCode.Enabled = false;
			if(m_sdsDomesticShip != null && m_sdsDomesticShip.DataSetRecSize > 0)
			{
				EnableNavigationButtons(false,false,true,true);
			}
		}

		private void FetchDSPKG()
		{
			decimal TOTVol = 0;  //Jeab 22 Feb 2011
			DataSet dsQryPkg = null;

			try 
			{
				dsQryPkg = DomesticShipmentMgrDAL.GetPakageDtls(appID,enterpriseID,Convert.ToInt32(txtBookingNo.Text.Trim()), txtConsigNo.Text.Trim());
				//Jeab 22 Feb 2011	
				int cnt = dsQryPkg.Tables[0].Rows.Count;
				int i = 0;
				for(i = 0;i<cnt;i++)
				{
					DataRow drEach = dsQryPkg.Tables[0].Rows[i];
					TOTVol += Convert.ToDecimal(drEach["pkg_length"])*Convert.ToDecimal(drEach["pkg_breadth"])*Convert.ToDecimal(drEach["pkg_height"])*Convert.ToDecimal(drEach["pkg_qty"]); //Jeab 22 Feb 2011
				}										
					
				txttotVol.Text = TOTVol.ToString("#,##0");
				//Jeab 22 Feb 2011  =========> End
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				lblErrorMsg.Text = strMsg;
				return;
			}
			Compare_PD_SPKG();
			Session["SESSION_DS2"] = dsQryPkg;
		}
		
		private void FetchDSRecSet()
		{
			//int iStartIndex = Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize;
			String tmpStr = ViewState["currentSet"].ToString().Trim();
			int iStartIndex = 0;
			int intIndexOf = tmpStr.IndexOf(".");
	
			if (intIndexOf > 0)
			{
				iStartIndex = Convert.ToInt32(tmpStr.Substring(0, intIndexOf)) * m_iSetSize;
			}
			else
			{
				iStartIndex = Convert.ToInt32(tmpStr) * m_iSetSize;
			}
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			if((int)ViewState["DSMode"] == (int)ScreenMode.Insert)
				m_sdsDomesticShip = DomesticShipmentMgrDAL.GetShipmentData_Partial(appID,enterpriseID,iStartIndex,m_iSetSize,dsQuery);
			else
				m_sdsDomesticShip = DomesticShipmentMgrDAL.GetShipmentData(appID,enterpriseID,iStartIndex,m_iSetSize,dsQuery);
			Session["SESSION_DS3"] = m_sdsDomesticShip;
			decimal pgCnt = (m_sdsDomesticShip.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < Convert.ToDecimal(ViewState["currentSet"]))
			{
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
			}
		}

		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}

		private void DisplayRecords()
		{
			DataRow drEach = m_sdsDomesticShip.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];

			Zipcode zipCode = new Zipcode();
			if(ViewState["InitialBooking"] != null)
				txtBookingNo.Text = drEach["booking_no"].ToString();

			if ((drEach["payer_type"] != null) && (drEach["payer_type"].ToString() != ""))
			{
				ddbCustType.SelectedItem.Selected=false;   
				ddbCustType.Items.FindByValue(drEach["payer_type"].ToString()).Selected=true; 
			}

			if ((drEach["consignment_no"] != null) && (drEach["consignment_no"].ToString() != ""))
			{
				txtConsigNo.Text = drEach["consignment_no"].ToString();
			}
			else
			{
				txtConsigNo.Text = "";
			}

			if ((drEach["ref_no"] != null) && (drEach["ref_no"].ToString() != ""))
			{
				txtRefNo.Text = drEach["ref_no"].ToString();
			}
			//			else
			//			{
			//				txtRefNo.Text = "";
			//			}

			strDt = DateTime.Now.ToString("dd/MM/yyyy HH:mm");

			if ((drEach["booking_datetime"] != null) && (drEach["booking_datetime"].ToString() != ""))
			{
				DateTime dtBooking = (DateTime)drEach["booking_datetime"];
				txtBookDate.Text = dtBooking.ToString("dd/MM/yyyy HH:mm");
			}
			else
			{
				if((bool)ViewState["PartialCon"]==true)
					txtBookDate.Text = strDt;
				else
					txtBookDate.Text = "";
			}

			if ((drEach["shpt_manifest_datetime"] != null) && (drEach["shpt_manifest_datetime"].ToString() != ""))
			{
				DateTime dtManifstDateTime = (DateTime)drEach["shpt_manifest_datetime"];
				txtShipManifestDt.Text = dtManifstDateTime.ToString("dd/MM/yyyy HH:mm");
			}
			else
			{
				if((bool)ViewState["PartialCon"]==true)
					txtShipManifestDt.Text=strDt;
				else
					txtShipManifestDt.Text = "";
			}
			
			if ((drEach["last_status_code"] != null) && (drEach["last_status_code"].ToString() != ""))
			{
				txtLatestStatusCode.Text = drEach["last_status_code"].ToString();
			}
			else
			{
				if((bool)ViewState["PartialCon"]==true)
					txtLatestStatusCode.Text = TIESUtility.getInitialShipmentStatusCode(appID,enterpriseID);
				else
					txtLatestStatusCode.Text = "";
			}

			if ((drEach["last_exception_code"] != null) && (drEach["last_exception_code"].ToString() != ""))
			{
				txtLatestExcepCode.Text = drEach["last_exception_code"].ToString();
			}
			//			else
			//			{
			//				txtLatestExcepCode.Text = "";
			//			}

			if ((drEach["delivery_manifested"] == null) && (drEach["delivery_manifested"].ToString() == ""))
			{
				if((bool)ViewState["PartialCon"]==true)
					txtDelManifest.Text = TIESUtility.getInitialDlvrymanifest(appID,enterpriseID);
			}
			else if(drEach["delivery_manifested"].ToString() == "N")
			{
				txtDelManifest.Text = "No";
				Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete,true,m_moduleAccessRights);
			}
			else if(drEach["delivery_manifested"].ToString() == "Y")
			{
				txtDelManifest.Text = "Yes";
			}

			if ((drEach["route_code"] != null) && (drEach["route_code"].ToString() != ""))
			{
				txtRouteCode.Text = drEach["route_code"].ToString();
			}
			else
			{
				txtRouteCode.Text = "";
			}

			if ((drEach["payerid"] != null) && (drEach["payerid"].ToString() != ""))
			{
				txtCustID.Text = drEach["payerid"].ToString();
			}
			//			else
			//			{
			//				txtCustID.Text = "";
			//			}

			String strNewAcnt = drEach["new_account"].ToString();
			if(strNewAcnt.Equals("Y"))
				chkNewCust.Checked = true;
			else if(strNewAcnt.Equals("N"))
				chkNewCust.Checked = false;

			if ((drEach["payer_name"] != null) && (drEach["payer_name"].ToString() != ""))
			{
				txtCustName.Text = drEach["payer_name"].ToString();
			}
			//			else
			//			{
			//				txtCustName.Text = "";
			//			}

			if ((drEach["payer_address1"] != null) && (drEach["payer_address1"].ToString() != ""))
			{
				txtCustAdd1.Text = drEach["payer_address1"].ToString();
			}
			//			else
			//			{
			//				txtCustAdd1.Text = "";
			//			}

			if ((drEach["payer_address2"] != null) && (drEach["payer_address2"].ToString() != ""))
			{
				txtCustAdd2.Text = drEach["payer_address2"].ToString();
			}
			//			else
			//			{
			//				txtCustAdd2.Text = "";
			//			}

			if ((drEach["payer_fax"] != null) && (drEach["payer_fax"].ToString() != ""))
			{
				txtCustFax.Text = drEach["payer_fax"].ToString();
			}
			//			else
			//			{
			//				txtCustFax.Text = "";
			//			}

			if ((drEach["payer_country"] != null) && (drEach["payer_country"].ToString() != ""))
			{
				txtCustStateCode.Text = drEach["payer_country"].ToString();
			}
			//			else
			//			{
			//				txtCustStateCode.Text = "";
			//			}

			if ((drEach["payer_telephone"] != null) && (drEach["payer_telephone"].ToString() != ""))
			{
				txtCustTelephone.Text = drEach["payer_telephone"].ToString();
			}
			//			else
			//			{
			//				txtCustTelephone.Text = "";
			//			}

			if ((drEach["payer_zipcode"] != null) && (drEach["payer_zipcode"].ToString() != ""))
			{
				txtCustZipCode.Text = drEach["payer_zipcode"].ToString();
			}
			//			else
			//			{
			//				txtCustZipCode.Text = "";
			//			}

			zipCode.Populate(appID,enterpriseID,txtCustStateCode.Text,txtCustZipCode.Text);
			txtCustCity.Text = zipCode.StateName;

			String strPaymentMode = drEach["payment_mode"].ToString();
			if (strPaymentMode.Equals("C"))
			{
				rbCredit.Checked = false;
				rbCash.Checked = true;
			}
			else if (strPaymentMode.Equals("R"))
			{
				rbCash.Checked = false;
				rbCredit.Checked = true;
			}

			if ((drEach["sender_name"] != null) && (drEach["sender_name"].ToString() != ""))
			{
				txtSendName.Text = drEach["sender_name"].ToString();
			}
			//			else
			//			{
			//				txtSendName.Text = "";
			//			}

			if ((drEach["sender_address1"] != null) && (drEach["sender_address1"].ToString() != ""))
			{
				txtSendAddr1.Text = drEach["sender_address1"].ToString();
			}
			//			else
			//			{
			//				txtSendAddr1.Text = "";
			//			}

			if ((drEach["sender_address2"] != null) && (drEach["sender_address2"].ToString() != ""))
			{
				txtSendAddr2.Text = drEach["sender_address2"].ToString();
			}
			//			else
			//			{
			//				txtSendAddr2.Text = "";
			//			}

			if ((drEach["sender_zipcode"] != null) && (drEach["sender_zipcode"].ToString() != ""))
			{
				txtSendZip.Text = drEach["sender_zipcode"].ToString();
			}
			//			else
			//			{
			//				txtSendZip.Text = "";
			//			}

			if ((drEach["sender_contact_person"] != null) && (drEach["sender_contact_person"].ToString() != ""))
			{
				txtSendContPer.Text = drEach["sender_contact_person"].ToString();
			}
			//			else
			//			{
			//				txtSendContPer.Text = "";
			//			}

			if ((drEach["sender_fax"] != null) && (drEach["sender_fax"].ToString() != ""))
			{
				txtSendFax.Text = drEach["sender_fax"].ToString();
			}
			//			else
			//			{
			//				txtSendFax.Text = "";
			//			}

			if ((drEach["sender_country"] != null) && (drEach["sender_country"].ToString() != ""))
			{
				txtSendState.Text = drEach["sender_country"].ToString();
			}
			//			else
			//			{
			//				txtSendState.Text = "";
			//			}

			if ((drEach["sender_telephone"] != null) && (drEach["sender_telephone"].ToString() != ""))
			{
				txtSendTel.Text = drEach["sender_telephone"].ToString();
			}
			//			else
			//			{
			//				txtSendTel.Text = "";
			//			}

			zipCode.Populate(appID,enterpriseID,txtSendState.Text,txtSendZip.Text);
			txtSendCity.Text = zipCode.StateName;
			txtSendCuttOffTime.Text = zipCode.CuttOffTime.ToString("HH:mm");

			if ((drEach["recipient_address1"] != null) && (drEach["recipient_address1"].ToString() != ""))
			{
				txtRecipAddr1.Text = drEach["recipient_address1"].ToString();
			}
			//			else
			//			{
			//				txtRecipAddr1.Text = "";
			//			}

			if ((drEach["recipient_address2"] != null) && (drEach["recipient_address2"].ToString() != ""))
			{
				txtRecipAddr2.Text = drEach["recipient_address2"].ToString();
			}
			//			else
			//			{
			//				txtRecipAddr2.Text = "";
			//			}

			if ((drEach["recipient_fax"] != null) && (drEach["recipient_fax"].ToString() != ""))
			{
				txtRecipFax.Text = drEach["recipient_fax"].ToString();
			}
			//			else
			//			{
			//				txtRecipFax.Text = "";
			//			}

			if ((drEach["recipient_country"] != null) && (drEach["recipient_country"].ToString() != ""))
			{
				txtRecipState.Text = drEach["recipient_country"].ToString();
			}
			//			else
			//			{
			//				txtRecipState.Text = "";
			//			}

			if ((drEach["recipient_telephone"] != null) && (drEach["recipient_telephone"].ToString() != ""))
			{
				txtRecipTel.Text = drEach["recipient_telephone"].ToString();
			}
			//			else
			//			{
			//				txtRecipTel.Text = "";
			//			}


			if ((drEach["recipient_zipcode"] != null) && (drEach["recipient_zipcode"].ToString() != ""))
			{
				txtRecipZip.Text = drEach["recipient_zipcode"].ToString();
			}
			//			else
			//			{
			//				txtRecipZip.Text = "";
			//			}

			if ((drEach["recipient_name"] != null) && (drEach["recipient_name"].ToString() != ""))
			{
				txtRecName.Text = drEach["recipient_name"].ToString();
			}
			//			else
			//			{
			//				txtRecName.Text = "";
			//			}

			if ((drEach["recipient_contact_person"] != null) && (drEach["recipient_contact_person"].ToString() != ""))
			{
				txtRecpContPer.Text = drEach["recipient_contact_person"].ToString();
			}
			//			else
			//			{
			//				txtRecpContPer.Text = "";
			//
			//			}

			zipCode.Populate(appID,enterpriseID,txtRecipState.Text,txtRecipZip.Text);
			txtRecipCity.Text = zipCode.StateName;
			//txtRouteCode.Text = zipCode.DeliveryRoute;

			if(!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				txtPkgActualWt.Text = drEach["tot_wt"].ToString();
				txtActualWeight.Text = drEach["tot_act_wt"].ToString();//TU on 17June08
				txtPkgDimWt.Text = drEach["tot_dim_wt"].ToString();
				txtPkgTotpkgs.Text = drEach["tot_pkg"].ToString();
				txtPkgChargeWt.Text = drEach["chargeable_wt"].ToString();
			}
			
			//			txtPkgChargeWt.Text = drEach["chargeable_wt"].ToString();
			Customer customer = new Customer();
			customer.Populate(appID,enterpriseID,txtCustID.Text.Trim());

			//			if (customer.ApplyDimWt == "Y")
			//			{
			//				txtPkgChargeWt.Text = (Convert.ToDecimal(drEach["tot_wt"])>Convert.ToDecimal(drEach["tot_dim_wt"])? drEach["tot_wt"].ToString(): drEach["tot_dim_wt"].ToString());
			//			}
			//			else
			//			{
			//				txtPkgChargeWt.Text = drEach["chargeable_wt"].ToString();
			//			}

			if ((drEach["commodity_code"] != null) && (drEach["commodity_code"].ToString() != ""))
			{
				txtPkgCommCode.Text = drEach["commodity_code"].ToString();
			}
			//			else
			//			{
			//				txtPkgCommCode.Text = "";
			//			}

			
			CommodityDetails commdetails = new CommodityDetails();
			commdetails.Populate(appID,enterpriseID,txtPkgCommCode.Text);
			txtPkgCommDesc.Text = commdetails.CommodityDescription;

			if ((drEach["est_delivery_datetime"] != null) && (drEach["est_delivery_datetime"].ToString() != ""))
			{
				DateTime dtDlvryDt = (DateTime)drEach["est_delivery_datetime"];
				txtShipEstDlvryDt.Text = dtDlvryDt.ToString("dd/MM/yyyy HH:mm");

				//HC Return Task
				Zipcode zipCodeTmp = new Zipcode();
				zipCodeTmp.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
				String strStateCode = zipCodeTmp.StateCode;

				if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCode.Trim() != ""))
				{
					string strConNo = "";
					string strBookingNo = "";

					if (txtBookingNo.Text.Trim() != "" )
						strBookingNo = txtBookingNo.Text.Trim();
					if (txtConsigNo.Text.Trim() != "" )
						strConNo = txtConsigNo.Text.Trim();

					object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCode,
						strConNo.Trim(), strBookingNo.Trim(), appID, enterpriseID);

					if((tmpReHCDateTime != null) && 
						(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
					}
					//					else
					//					{
					//						txtEstHCDt.Text = "";
					//					}
				}
				//				else
				//				{
				//					txtEstHCDt.Text = "";
				//				}
				//HC Return Task
			}
			//			else
			//			{
			//				txtShipEstDlvryDt.Text = "";
			//			}


			if ((drEach["act_pickup_datetime"] != null) && (drEach["act_pickup_datetime"].ToString() != ""))
			{
				//				if ((drEach["last_status_code"] != null) && (drEach["last_status_code"].ToString() != "") && (drEach["last_status_code"].ToString() == "POD"))
				//				{
				//					
				//					DateTime dtBkg =  DomesticShipmentMgrDAL.PickupBkgDateTimeForPOD(appID,enterpriseID,Convert.ToInt32(txtBookingNo.Text));
				//					if(!dtBkg.ToString("dd/MM/yyyy HH:mm").Equals("01/01/0001 00:00"))
				//						txtShipPckUpTime.Text = dtBkg.ToString("dd/MM/yyyy HH:mm");
				//					else
				//						txtShipPckUpTime.Text="";
				//				}
				//				else
				//				{
				DateTime dtActPickUp = (DateTime)drEach["act_pickup_datetime"];
				txtShipPckUpTime.Text = dtActPickUp.ToString("dd/MM/yyyy HH:mm");
				//				}
			}
			else
			{
				txtShipPckUpTime.Text = txtShipManifestDt.Text;
			}
			if ((drEach["act_delivery_date"] != null) && (drEach["act_delivery_date"].ToString() != ""))
			{
				DateTime dtActDeliveryDt = (DateTime)drEach["act_delivery_date"];
				txtActDelDt.Text = dtActDeliveryDt.ToString("dd/MM/yyyy HH:mm");
			}
			//			else
			//			{
			//				txtActDelDt.Text = "";
			//			}

			//HC Return Task
			if ((drEach["est_hc_return_datetime"] != null) && (drEach["est_hc_return_datetime"].ToString() != ""))
			{
				DateTime dtest_hc_return_datetime = (DateTime)drEach["est_hc_return_datetime"];
				txtEstHCDt.Text = dtest_hc_return_datetime.ToString("dd/MM/yyyy HH:mm");
			}
			//			else
			//			{
			//				txtEstHCDt.Text = "";
			//			}
			if ((drEach["act_hc_return_datetime"] != null) && (drEach["act_hc_return_datetime"].ToString() != ""))
			{
				DateTime dtact_hc_return_datetimet = (DateTime)drEach["act_hc_return_datetime"];
				txtActHCDt.Text = dtact_hc_return_datetimet.ToString("dd/MM/yyyy HH:mm");
			}
			//			else
			//			{
			//				txtActHCDt.Text = "";
			//			}
			//HC Return Task

			if ((drEach["percent_dv_additional"] != null) && (drEach["percent_dv_additional"].ToString() != ""))
			{
				//txtShpAddDV.Text = drEach["percent_dv_additional"].ToString();
				txtShpAddDV.Text = String.Format("{0:F2}", drEach["percent_dv_additional"]);
			}
			else
			{
				txtShpAddDV.Text = "0.00";
			}

			if ((drEach["declare_value"] != null) && (drEach["declare_value"].ToString() != ""))
			{
				txtShpDclrValue.Text = String.Format((String)ViewState["m_format"], drEach["declare_value"]);
			}
			else
			{
				decimal tmpDclrValue = 0;
				txtShpDclrValue.Text = String.Format((String)ViewState["m_format"], tmpDclrValue);
			}

			//Insurance
			decimal decInsSurchrg = 0;
			if (drEach["insurance_surcharge"]!=DBNull.Value)
			{
				decInsSurchrg = Convert.ToDecimal(drEach["insurance_surcharge"]);
			}
			else
			{
				decInsSurchrg= 0;				
			}
			txtShpInsSurchrg.Text = String.Format((String)ViewState["m_format"], decInsSurchrg);

			if ((drEach["max_insurance_cover"] != null) && (drEach["max_insurance_cover"].ToString() != ""))
			{
				txtShpMaxCvrg.Text = String.Format((String)ViewState["m_format"], drEach["max_insurance_cover"]);
			}
			//			else
			//			{
			//				txtShpMaxCvrg.Text = "";
			//			}

			if ((drEach["service_code"] != null) && (drEach["service_code"].ToString() != ""))
			{
				txtShpSvcCode.Text = drEach["service_code"].ToString();
			}
			//			else
			//			{
			//				txtShpSvcCode.Text = "";
			//			}

			//call the method to get the description of service code.
			Service service = new Service();
			service.Populate(appID,enterpriseID,txtShpSvcCode.Text.Trim());
			txtShpSvcDesc.Text = service.ServiceDescription;
			
			String strRutrnPOD = drEach["return_pod_slip"].ToString();
			if(strRutrnPOD.Equals("Y"))
				chkshpRtnHrdCpy.Checked = true;
			else if(strRutrnPOD.Equals("N"))
				chkshpRtnHrdCpy.Checked = false;

			//HC Return Task
			String strRutrnInvoice = drEach["return_invoice_hc"].ToString();
			if(strRutrnInvoice.Equals("Y"))
				chkInvHCReturn.Checked = true;
			else if(strRutrnInvoice.Equals("N"))
				chkInvHCReturn.Checked = false;
			//HC Return Task

			//			String strFright = drEach["payment_type"].ToString();
			//			if(strFright.Equals("FP"))
			//			{
			//				rbtnShipFrghtColl.Checked = false;
			//				rbtnShipFrghtPre.Checked = true;
			//			}
			//			else if(strFright.Equals("FC"))
			//			{
			//				rbtnShipFrghtPre.Checked = false;
			//				rbtnShipFrghtColl.Checked = true;
			//			}

			//Get the Shipment VAS & bind to the VAS grid
			try
			{
				m_dsVAS = DomesticShipmentMgrDAL.GetEmptyVASDS();
				Session["SESSION_DS1"] = m_dsVAS;	
				m_dsVAS = DomesticShipmentMgrDAL.GetVASData(appID,enterpriseID,Convert.ToDecimal(drEach["booking_no"]), (String)drEach["consignment_no"]);
				if(m_dsVAS != null)
				{
					Session["SESSION_DS1"] = m_dsVAS;
				}
				BindVASGrid();
			}
			catch(ApplicationException appException)
			{
				lblErrorMsg.Text = appException.Message;
				return;
			}
			
			if( drEach["tot_freight_charge"]!= DBNull.Value )
			{
				txtFreightChrg.Text = String.Format((String)ViewState["m_format"], (decimal)drEach["tot_freight_charge"]);
			}
			 
			if( drEach["insurance_surcharge"]!=DBNull.Value )
			{
				txtInsChrg.Text = String.Format((String)ViewState["m_format"], (decimal)drEach["insurance_surcharge"]);
			}
			
			if( drEach["other_surch_amount"]!=DBNull.Value )
			{
				txtOtherSurcharge.Text = txtOtherSurcharge2.Text = String.Format((String)ViewState["m_format"], (decimal)drEach["other_surch_amount"]);
			}

			if (drEach["tot_vas_surcharge"]!=DBNull.Value )
			{
				txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], (decimal)drEach["tot_vas_surcharge"]);
			}
			
			if( drEach["esa_surcharge"]!=DBNull.Value)
			{
				txtESASurchrg.Text = String.Format((String)ViewState["m_format"], (decimal)drEach["esa_surcharge"]);
			}
			else
			{
				txtESASurchrg.Text = String.Format((String)ViewState["m_format"], 0);
			}

			//Insert other_surch_amount, 22:25 28/2/2551
			//if (drEach["total_rated_amount"]!=DBNull.Value)
			//{
			//	txtShpTotAmt.Text = String.Format((String)ViewState["m_format"], (decimal)drEach["total_rated_amount"]);
			//}
			//else
			//{
			//	//txtShpTotAmt.Text = String.Format((String)ViewState["m_format"], 0);
			//	CalculateTotalAmt();
			//}

			//PODEX Surcharge
			if(drEach["total_excp_charges"]!=DBNull.Value) 
				txtPODEX.Text = String.Format((String)ViewState["m_format"], (decimal)drEach["total_excp_charges"]);
			//MBG Amount
			if(drEach["mbg_amount"]!=DBNull.Value)
				txtMBGAmt.Text = String.Format((String)ViewState["m_format"], (decimal)drEach["mbg_amount"]);
			//Other Invoice Credit / Debit  ������ Absolute 仴���������Ҩ�����ͧ����� Credit/Debit
			if(drEach["invoice_adjustment"]!=DBNull.Value)
				txtOtherInvC_D.Text = String.Format((String)ViewState["m_format"], Math.Abs((decimal)drEach["invoice_adjustment"]));
			//Total Invoiced Amount
			if(drEach["invoice_amt"]!=DBNull.Value)
				txtTotalInvAmt.Text = String.Format((String)ViewState["m_format"], (decimal)drEach["invoice_amt"]);
			//Credit Notes
			if(drEach["credit_amt"]!=DBNull.Value)
				txtCreditNotes.Text = String.Format((String)ViewState["m_format"], (decimal)drEach["credit_amt"]);
			//Debit Notes
			if(drEach["debit_amt"]!=DBNull.Value)
				txtDebitNotes.Text = String.Format((String)ViewState["m_format"], (decimal)drEach["debit_amt"]);
			//Total Consignment Revenue
			if(drEach["total_cons_revenue"]!=DBNull.Value)
				txtTotalConRevenue.Text = String.Format((String)ViewState["m_format"], (decimal)drEach["total_cons_revenue"]);
			//Paid Revenue
			if(drEach["Paid_revenue"]!=DBNull.Value)
				txtPaidRevenue.Text = String.Format((String)ViewState["m_format"], (decimal)drEach["Paid_revenue"]);

			if ((drEach["origin_state_code"] != null) && (drEach["origin_state_code"].ToString() != ""))
			{
				zipCode.Populate(appID,enterpriseID,txtSendState.Text,txtSendZip.Text);
				txtOrigin.Text = zipCode.StateName;
			}
			//			else
			//			{
			//				txtOrigin.Text = "";
			//			}
			if ((drEach["invoice_no"] != null) && (drEach["invoice_no"].ToString() != ""))
			{
				Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,false,m_moduleAccessRights);
				Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete,false,m_moduleAccessRights);
			}
			else
			{
				Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,true,m_moduleAccessRights);
				if(drEach["delivery_manifested"].ToString() == "Y")
				{
					Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete,false,m_moduleAccessRights);
				}
				else
					Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete,true,m_moduleAccessRights);
			}
			

			if ((drEach["destination_state_code"] != null) && (drEach["destination_state_code"].ToString() != ""))
			{
				zipCode.Populate(appID,enterpriseID,txtRecipState.Text,txtRecipZip.Text);
				txtDestination.Text = zipCode.StateName;;
			}
			//			else
			//			{
			//				txtDestination.Text = "";
			//			}
			if ((drEach["remark"] != null) && (drEach["remark"].ToString() != ""))
			{
				txtSpeHanInstruction.Text = drEach["remark"].ToString();
			}
			//			else
			//			{
			//				txtSpeHanInstruction.Text = "";
			//
			//			}

			if ((drEach["cod_amount"] != null) && (drEach["cod_amount"].ToString() != ""))
			{
				//decimal decTmpCODAmount = TIESUtility.EnterpriseRounding(Convert.ToDecimal(drEach["cod_amount"]),(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]);
				//txtCODAmount.Text = TIESUtility.Round_Enterprise_Currency(appID,enterpriseID,decTmpCODAmount).ToString();
				txtCODAmount.Text = String.Format("{0:F2}",Convert.ToDecimal(drEach["cod_amount"]));
			}
			else
			{
				decimal tmpCod = 0;
				txtCODAmount.Text = String.Format("{0:F2}", tmpCod);
			}

			//opal 20121107
		

			if 	(drEach["created_by"] == System.DBNull.Value)
			{
				if (drEach["booking_datetime"] != System.DBNull.Value) //opal 20121127 if booking_datetime is null >>> enable COD Amt in case booking_datetime = now
				{
					if (DateTime.Compare(Convert.ToDateTime(drEach["BookingDateTimePlus6"]), DateTime.Now) > 0)
					{
						txtCODAmount.Enabled=true;
					}
					else
					{
						txtCODAmount.Enabled=false;
					}
				}
				else
				{
					txtCODAmount.Enabled=true;
				}
			
			}
			else if (drEach["created_by"].ToString()  == utility.GetUserID())
			{
				if (drEach["booking_datetime"] != System.DBNull.Value)//opal 20121127 if booking_datetime is null >>> enable COD Amt in case booking_datetime = now
				{
					if (DateTime.Compare(Convert.ToDateTime(drEach["BookingDateTimePlus6"]), DateTime.Now) == 1)
					{
						txtCODAmount.Enabled=true;
					}
					else
					{
						txtCODAmount.Enabled=false;
					}
				}
				else
				{
					txtCODAmount.Enabled=true;
				}
			}
			else
			{
				txtCODAmount.Enabled=false;
			}

			if ((drEach["pkg_detail_replaced_datetime"] != null) && (drEach["pkg_detail_replaced_datetime"].ToString() != ""))
			{
				//decimal decTmpCODAmount = TIESUtility.EnterpriseRounding(Convert.ToDecimal(drEach["cod_amount"]),(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]);
				//txtCODAmount.Text = TIESUtility.Round_Enterprise_Currency(appID,enterpriseID,decTmpCODAmount).ToString();
				lblPD_Replace_Date.Text = "Pakage Detail Replaced : " + ((DateTime)drEach["pkg_detail_replaced_datetime"]).ToString("dd/MM/yyyy HH:mm");
				lblPD_Replace_Date.Visible = true;
				btnViewOldPD.Visible = true;
			}
			else
			{
				lblPD_Replace_Date.Visible = false;
				btnViewOldPD.Visible = false;
			}

			zipCode.Populate(appID,enterpriseID,txtRecipZip.Text);
			this.txtRecipState.Text = zipCode.Country;
			zipCode.Populate(appID,enterpriseID,this.txtRecipState.Text,txtRecipZip.Text);
			this.txtRecipCity.Text = zipCode.StateName;


			CalculateTotalAmt();

			//Part of Manual Rating Override Panel
			SetOriginalRating(drEach);
			//Insert by tu on 05/11/2009
			DisableWhenOverride();
			//
		}
		//Insert by tu on 05/11/2009
		private void DisableWhenOverride()
		{
			if(txtOriFreightChrg.Visible==true)
			{
				Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,false,m_moduleAccessRights);			
			}
		}
		//
		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			MoveLastDS();
			if ((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery)
			{
				FetchDSPKG();
			}
		}
		private void MoveLastDS()
		{
			lblErrorMsg.Text = "";
			if(btnSave.Enabled)
			{
				if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
				{
					lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
					DomstcShipPanel.Visible = true;
					divCustIdCng.Visible=false;
					divMain.Visible=false;
					divDelOperation.Visible=false;
					ddbCustType.Visible = false;
					ViewState["NextOperation"] = "MoveLast";
					return;
				}
			}
			else
			{
				ViewState["isTextChanged"] = false;
				ViewState["DSOperation"] =Operation.None;
			}
			ViewState["currentSet"] = (m_sdsDomesticShip.QueryResultMaxSize - 1)/m_iSetSize;	
			FetchDSRecSet();
			ViewState["currentPage"] = m_sdsDomesticShip.DataSetRecSize - 1;
			DisplayRecords();	

			EnableNavigationButtons(true,true,false,false);
		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			MoveFirstDS();
			if ((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery)
			{
				FetchDSPKG();
			}
		}

		private void MoveFirstDS()
		{
			lblErrorMsg.Text = "";			
			if(btnSave.Enabled)
			{
				if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
				{
					lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
					DomstcShipPanel.Visible = true;
					divCustIdCng.Visible=false;
					divMain.Visible=false;
					divDelOperation.Visible=false;
					ddbCustType.Visible = false;
					//ViewState["MoveFirst"]=true;
					ViewState["NextOperation"] = "MoveFirst";
					return;
				}
			}
			else
			{
				ViewState["isTextChanged"] = false;
				ViewState["DSOperation"] =Operation.None;
			}
			ViewState["currentSet"] = 0;	
			FetchDSRecSet();
			ViewState["currentPage"] = 0;
			DisplayRecords();
			EnableNavigationButtons(false,false,true,true);
		}
		
		private void btnNextPage_Click(object sender, System.EventArgs e)
		{
			MoveNextDS();
			if ((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery)
			{
				FetchDSPKG();
			}
		}
		
		private void MoveNextDS()
		{
			lblErrorMsg.Text = "";
			if(btnSave.Enabled)
			{
				if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
				{
					lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
					DomstcShipPanel.Visible = true;
					divCustIdCng.Visible=false;
					divMain.Visible=false;
					divDelOperation.Visible=false;
					ddbCustType.Visible = false;
					ViewState["NextOperation"] = "MoveNext";
					ViewState["MoveNext"]=true;
					return;
				}
			}
			else
			{
				ViewState["isTextChanged"] = false;
				ViewState["DSOperation"] =Operation.None;
			}
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  < (m_sdsDomesticShip.DataSetRecSize - 1) )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) + 1;
				DisplayRecords();
			}
			else
			{
				int iTotalRec = (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) + Convert.ToInt32(m_sdsDomesticShip.DataSetRecSize);
		
				if( iTotalRec ==  m_sdsDomesticShip.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) + 1;	
				FetchDSRecSet();
				ViewState["currentPage"] = 0;
				DisplayRecords();
			}
			EnableNavigationButtons(true,true,true,true);
		}
		
		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			MovePreviousDS();
			if ((ScreenMode)ViewState["DSMode"] == ScreenMode.ExecuteQuery)
			{
				FetchDSPKG();
			}
		}
		
		private void MovePreviousDS()
		{
			if(btnSave.Enabled)
			{
				if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
				{
					lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
					DomstcShipPanel.Visible = true;
					divCustIdCng.Visible=false;
					divMain.Visible=false;
					divDelOperation.Visible=false;
					ddbCustType.Visible = false;
					ViewState["MovePrevious"]=true;
					ViewState["NextOperation"] = "MovePrevious";
					return;
				}
			}
			else
			{
				ViewState["isTextChanged"] = false;
				ViewState["DSOperation"] =Operation.None;
			}
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  > 0 )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) - 1;
				DisplayRecords();
			}
			else
			{
				if( (Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) - 1;	
				FetchDSRecSet();
				ViewState["currentPage"] = m_sdsDomesticShip.DataSetRecSize - 1;
				DisplayRecords();
			}
			EnableNavigationButtons(true,true,true,true);
		}
		
		private void txtGoToRec_TextChanged(object sender, System.EventArgs e)
		{
		
		}

		private void txtPkgCommCode_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			strErrorMsg = "";

			if(txtPkgCommCode.Text.Length > 0)
			{
				CommodityDetails commdetails = new CommodityDetails();
				commdetails.Populate(appID,enterpriseID,txtPkgCommCode.Text);
				
				String strDesc = commdetails.CommodityDescription;
				String strCode = commdetails.CommodityCode;
				if(strCode == null)
				{
					strErrorMsg = "Commodity Code does not exists";
					lblErrorMsg.Text = strErrorMsg;

					txtPkgCommCode.Text = "";
					txtPkgCommDesc.Text = "";
				}
				else
				{
					txtPkgCommDesc.Text = strDesc;
				}
			}
			else
			{
				txtPkgCommDesc.Text = "";
			}
		}

		private void rbtnShipFrghtPre_CheckedChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void rbtnShipFrghtColl_CheckedChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtConsigNo_TextChanged(object sender, System.EventArgs e)
		{
			//			if(txtConsigNo.Text.Trim().Length > 0 &&Utility.ValidateConsignmentNo(txtConsigNo.Text.Trim())==false)
			//			{
			//				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSNO_INVALID",utility.GetUserCulture()); 
			//				SetInitialFocus(txtConsigNo);
			//				return;
			//			}
			//			else
			//			{
			//				lblErrorMsg.Text="";
			//			}
			//
			//			if(m_dsPkgDetails!=null)
			//				return;
			//			
			//			Compare_PD_SPKG();
			//
			//			if((bool)ViewState["isTextChanged"] == false)
			//			{
			//				ChangeDSState();
			//				ViewState["isTextChanged"] = true;
			//			}
			//			if ((this.txtConsigNo.Text.Trim().Length > 0) && ((int)ViewState["DSMode"] == (int)ScreenMode.Insert))
			//			{
			//
			//
			//				GetValuesIntoDS(0);
			//				ViewState["QUERY_DS"] = m_sdsDomesticShip.ds;
			//				//ViewState["DSMode"] = ScreenMode.ExecuteQuery;
			//				//ViewState["DSOperation"] = Operation.None;
			//				ViewState["isTextChanged"] = false;
			//				ViewState["currentPage"] = 0;
			//				ViewState["currentSet"] = 0;
			//				FetchDSRecSet();
			//				
			//				//Tumz.
			//				if(m_sdsDomesticShip.ds.Tables[0].Rows.Count > 0)
			//				{
			//					if ((m_sdsDomesticShip.ds.Tables[0].Rows[0]["pkg_detail_replaced_datetime"] != null) && (m_sdsDomesticShip.ds.Tables[0].Rows[0]["pkg_detail_replaced_datetime"].ToString() != ""))
			//					{
			//						lblPD_Replace_Date.Text = "Pakage Detail Replaced : " + ((DateTime)m_sdsDomesticShip.ds.Tables[0].Rows[0]["pkg_detail_replaced_datetime"]).ToString("dd/MM/yyyy HH:mm");
			//						lblPD_Replace_Date.Visible = true;
			//						btnViewOldPD.Visible = true;
			//					}
			//					else
			//					{
			//						lblPD_Replace_Date.Visible = false;
			//						btnViewOldPD.Visible = false;
			//					}
			//				}
			//
			//				if(rbCash.Checked==true)
			//				{
			//					ViewState["payMode"] = "C";
			//				}
			//				else{
			//					ViewState["payMode"] = "R";
			//				}
			//			
			////				if(m_sdsDomesticShip.QueryResultMaxSize == 0)
			////				{
			////					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			////					EnableNavigationButtons(false,false,false,false);
			////					return;
			////				}
			//				if(m_sdsDomesticShip.QueryResultMaxSize>0)
			//				{
			//					ViewState["PartialCon"]=true;
			//					DisplayRecords();
			//					if(txtBookingNo.Text.Length>0)
			//						BookingNoDetails();
			//					if(ViewState["InitialBooking"] != null)
			//						txtBookingNo.Text = null;
			//					SetInitialFocus(txtBookingNo);
			//				}
			//				else
			//				{
			//					string strConsigNo = this.txtConsigNo.Text;
			//					string strBookingNo = this.txtBookingNo.Text;
			//					fromInsertBtn = false;
			//					Insert_Click();
			//					this.txtConsigNo.Text = strConsigNo;
			//					this.txtBookingNo.Text = strBookingNo;
			//					SetInitialFocus(txtBookingNo);
			//					ViewState["PartialCon"]=false;
			//				}
			//
			//			}
			//			if ((ViewState["payMode"] != null) && (ViewState["payMode"].ToString() == "C"))
			//			{
			//				rbCash.Checked =true;
			//				rbCredit.Checked=false;
			//			}

		}

		private void txtRefNo_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtRouteCode_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtCustName_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtCustAdd1_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtCustAdd2_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtCustTelephone_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtCustFax_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtSendName_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtSendAddr1_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtSendAddr2_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtSendContPer_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtSendTel_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtSendFax_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtRecName_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

		}

		private void txtRecipAddr1_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtRecipAddr2_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void txtRecpContPer_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		//		private void txtRecipTel_TextChanged(object sender, System.EventArgs e)
		//		{
		//			if((bool)ViewState["isTextChanged"] == false)
		//			{
		//				ChangeDSState();
		//				ViewState["isTextChanged"] = true;
		//			}
		//		}

		private void txtRecipFax_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void chkshpRtnHrdCpy_CheckedChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
			//HC Return Task
			if (chkshpRtnHrdCpy.Checked == true ||chkInvHCReturn.Checked == true)
			{
				Zipcode zipCode = new Zipcode();
				zipCode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
				String strStateCode = zipCode.StateCode;

				if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCode.Trim() != ""))
				{
					string strConNo = "";
					string strBookingNo = "";

					if (txtBookingNo.Text.Trim() != "" )
						strBookingNo = txtBookingNo.Text.Trim();
					if (txtConsigNo.Text.Trim() != "" )
						strConNo = txtConsigNo.Text.Trim();

					object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCode,
						strConNo.Trim(), strBookingNo.Trim(), appID, enterpriseID);

					if((tmpReHCDateTime != null) && 
						(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
					}
					else
					{
						txtEstHCDt.Text = "";
					}
				}
				else
				{
					txtEstHCDt.Text = "";
				}
			}
			else if(chkshpRtnHrdCpy.Checked == false && chkInvHCReturn.Checked == false)
			{
				txtEstHCDt.Text = "";
			}
			//HC Return Task
		}

		private void ChangeDSState()
		{
			int iDSMode = (int)ViewState["DSMode"];
			int iDSOperation = (int)ViewState["DSOperation"];

			if(((int)ViewState["DSMode"] == (int)ScreenMode.Insert) && ((int)ViewState["DSOperation"] == (int)Operation.None))
			{
				ViewState["DSOperation"] = Operation.Insert;
			}
			else if(((int)ViewState["DSMode"] == (int)ScreenMode.Insert) && ((int)ViewState["DSOperation"] == (int)Operation.Insert))
			{
				//During saving
				ViewState["DSOperation"] = Operation.Saved;
			}
			else if(((int)ViewState["DSMode"] == (int)ScreenMode.Insert) && ((int)ViewState["DSOperation"] == (int)Operation.Saved))
			{
				ViewState["DSOperation"] = Operation.Update;
			}
			else if(((int)ViewState["DSMode"] == (int)ScreenMode.Insert) && ((int)ViewState["DSOperation"] == (int)Operation.Update))
			{
				ViewState["DSOperation"] = Operation.Saved;
			}
			else if(((int)ViewState["DSMode"] == (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSOperation"] == (int)Operation.None))
			{
				ViewState["DSOperation"] = Operation.Update;
			}
			else if(((int)ViewState["DSMode"] == (int)ScreenMode.ExecuteQuery) && ((int)ViewState["DSOperation"] == (int)Operation.Update))
			{
				ViewState["DSOperation"] = Operation.None;
			}
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			
			/*
			lblErrorMsg.Text = "";
			btnPkgDetails.Enabled = true;
			if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
			{
				lblConfirmMsg.Text = "Do you want to save changes? ";
				DomstcShipPanel.Visible = true;	
				divCustIdCng.Visible=false;
				divMain.Visible=false;
				ddbCustType.Visible = false;
				return;
			}
			String strStatus = TIESUtility.getInitialShipmentStatusCode(appID,enterpriseID);
			DomesticShipmentMgrDAL domesticDAL = new DomesticShipmentMgrDAL();
		
			if(txtBookingNo.Text.Length > 0) 
			{
				if((DomesticShipmentMgrDAL.CanDeleteShipment(appID,enterpriseID,Convert.ToInt32(txtBookingNo.Text),strStatus)))
				{
					String[] strDeleteStatusList = TIESUtility.getDeleteAllowStatus(appID,enterpriseID);
					int iRowDeleted = DomesticShipmentMgrDAL.DeleteDomesticShp(appID,enterpriseID,Convert.ToInt32(txtBookingNo.Text),strDeleteStatusList);
					lblErrorMsg.Text = "Deleted successfully";
					return;
				}
				else
				{
					lblErrorMsg.Text = "Shipment cannot be deleted";
					return;
				}
			}
			else
			{
				lblErrorMsg.Text = "Please enter the booking no. to be deleted";
				return;
			}
			*/
			divDelOperation.Visible=true;
			divMain.Visible=false;
			divCustIdCng.Visible=false;
			DomstcShipPanel.Visible=false;
		}

		private void txtShipManifestDt_TextChanged(object sender, System.EventArgs e)
		{
			RegularShpManfstDt.Validate();

			if(RegularShpManfstDt.IsValid)
			{
				if((bool)ViewState["isTextChanged"] == false)
				{
					ChangeDSState();
					ViewState["isTextChanged"] = true;
				}
			}
		}

		private void txtSpeHanInstruction_TextChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}

		private void btnCustIdChgNo_Click(object sender, System.EventArgs e)
		{
			divMain.Visible=true;			
			divCustIdCng.Visible=false;
		}

		private void btnCustIdChgYes_Click(object sender, System.EventArgs e)
		{
			poppulateCustomerAddress();
		}
		private void poppulateCustomerAddress()
		{
			String sUrl = "CustomerPopup.aspx?FORMID="+"DomesticShipment"+
				"&CustType="+ddbCustType.SelectedItem.Value+"&ShpDclrValue="+txtShpDclrValue.Text.Trim();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			divCustIdCng.Visible=false;
			DomstcShipPanel.Visible=false;
			divMain.Visible=true;
		}

		private void btnDelOperationYes_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			btnPkgDetails.Enabled = true;
			if (((int)ViewState["DSOperation"] == (int)Operation.Insert) || ((int)ViewState["DSOperation"] == (int)Operation.Update))
			{
				lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				DomstcShipPanel.Visible = true;
				divDelOperation.Visible=false;
				divCustIdCng.Visible=false;
				divMain.Visible=false;
				ddbCustType.Visible = false;
				return;
			}

			DataSet ds = TrackAndTraceDAL.ShipmentTrackingCheckStatus(appID,enterpriseID,txtConsigNo.Text.ToString().Trim(),"SIP");
			if (ds.Tables[0].Rows.Count>0)
			{
				DomstcShipPanel.Visible=false;
				divCustIdCng.Visible=false;
				divDelOperation.Visible=false;
				divMain.Visible=true;
				lblErrorMsg.Text="Consignment cannot not be deleted once it has been SIP or SOP scanned.";
				return;
			}
			DataSet dss = TrackAndTraceDAL.ShipmentTrackingCheckStatus(appID,enterpriseID,txtConsigNo.Text.ToString().Trim(),"SOP");
			if (dss.Tables[0].Rows.Count>0)
			{
				DomstcShipPanel.Visible=false;
				divCustIdCng.Visible=false;
				divDelOperation.Visible=false;
				divMain.Visible=true;
				lblErrorMsg.Text="Consignment cannot not be deleted once it has been SIP or SOP scanned.";
				return;
			}


			String strStatus = TIESUtility.getInitialShipmentStatusCode(appID,enterpriseID);
			DomesticShipmentMgrDAL domesticDAL = new DomesticShipmentMgrDAL();
		
			if(txtBookingNo.Text.Length > 0) 
			{
				if((DomesticShipmentMgrDAL.CanDeleteShipment(appID,enterpriseID,Convert.ToInt32(txtBookingNo.Text),strStatus)))
				{
					String[] strDeleteStatusList = TIESUtility.getDeleteAllowStatus(appID,enterpriseID);
					int iRowDeleted = DomesticShipmentMgrDAL.DeleteDomesticShp(appID,enterpriseID,Convert.ToInt32(txtBookingNo.Text),txtConsigNo.Text.ToString().Trim(),strDeleteStatusList);
					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
					DomstcShipPanel.Visible=false;
					divCustIdCng.Visible=false;
					divDelOperation.Visible=false;
					divMain.Visible=true;
					return;
				}
				else
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_DEL_SHMPT",utility.GetUserCulture()); 
					DomstcShipPanel.Visible=false;
					divCustIdCng.Visible=false;
					divDelOperation.Visible=false;
					divMain.Visible=true;
					return;
				}
			}
			else
			{
				lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_BKG_NO_DEL",utility.GetUserCulture());
				DomstcShipPanel.Visible=false;
				divCustIdCng.Visible=false;
				divDelOperation.Visible=false;
				divMain.Visible=true;
				return;
			}
			
		}

		private void btnDelOperationNo_Click(object sender, System.EventArgs e)
		{
			DomstcShipPanel.Visible=false;
			divCustIdCng.Visible=false;
			divDelOperation.Visible=false;
			divMain.Visible=true;
		}

		
		private void btnUpdateAutoManifest_Click(object sender, System.EventArgs e)
		{
			divMain.Visible = true;
			divUpdateAutoManifest.Visible = false;
		}

		public void CalculateCODVAS()
		{
			if(txtCODAmount.Text.Trim() != "")
			{
				if(IsNumeric(txtCODAmount.Text.Trim()) == false)
				{
					decimal tmpCod = 0;
					txtCODAmount.Text = String.Format("{0:F2}", tmpCod);
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_NOS",utility.GetUserCulture());
					return;
				}
			}
			else
			{
				decimal tmpCod = 0;
				txtCODAmount.Text = String.Format((String)ViewState["m_format"], tmpCod);

			}

			//decimal tmpValueCod = TIESUtility.EnterpriseRounding(Convert.ToDecimal(txtCODAmount.Text.Trim()),(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]);
			//txtCODAmount.Text = String.Format("{0:F2}", tmpValueCod);
			decimal tmpValueCod = Convert.ToDecimal(txtCODAmount.Text.Trim());
			txtCODAmount.Text = String.Format("{0:F2}",tmpValueCod);
			


			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if(tmpValueCod > 0)
			{
				if(m_dsVAS.Tables[0].Select("vas_code = 'COD'").Length > 0)
				{
					m_dsVAS.Tables[0].Select("vas_code = 'COD'")[0].Delete();
					m_dsVAS.AcceptChanges();
				}

				DataSet dsVas = ImportConsignmentsDAL.GetSurchargeFromVAS(appID, enterpriseID);
				if(dsVas.Tables[0].Rows.Count > 0)
				{
					if(m_dsVAS.Tables[0].Select("vas_code = 'COD'").Length == 0)
					{
						//DataTable dtVAS = m_dsVAS.Tables[0].Copy();
						
						DataRow drNew = m_dsVAS.Tables[0].NewRow(); //dtVAS.NewRow(); //
						
						foreach(DataRow drVAS in dsVas.Tables[0].Rows)
						{
							if((drVAS["vas_code"] != null) && 
								(!drVAS["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
								(drVAS["vas_code"].ToString() != ""))
							{
								drNew[0] = Convert.ToString(drVAS["vas_code"]);
							}

							if((drVAS["vas_description"] != null) && 
								(!drVAS["vas_description"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
								(drVAS["vas_description"].ToString() != ""))
							{
								drNew[1] = Convert.ToString(drVAS["vas_description"]);
							}


							if((drVAS["surcharge"] != null) && 
								(!drVAS["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
								(drVAS["surcharge"].ToString() != ""))
							{
								decimal tmpCODSurchrg = 0;
								DataSet dscustInfo = SysDataMgrDAL.GetCustInforByCustID(appID,enterpriseID, 
									0, 0, txtCustID.Text.ToString().Trim()).ds;

								//Customer Profile
								if (dscustInfo.Tables[0].Rows.Count > 0)
								{
									if(((dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"] != null) && 
										(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].ToString() != "")) ||
										((dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"] != null) && 
										(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].ToString() != "")))
									{
										//Amount
										if((dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"] != null) && 
											(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
											(dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].ToString() != ""))
										{
											tmpCODSurchrg = (decimal)dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"];
										}
										//Percentage
										if((dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"] != null) && 
											(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
											(dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].ToString() != ""))
										{
											tmpCODSurchrg = (Convert.ToDecimal(txtCODAmount.Text) * ((decimal)dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"])) / 100;

											//Boundary Min & Max
											if((dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"] != null) && 
												(!dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
												(dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"].ToString() != ""))
											{ //��ҡ�˹������ꡫ� ��������Թ��ꡫ� �����仹Ш��
												if(tmpCODSurchrg > Convert.ToDecimal(dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"]))
													tmpCODSurchrg = Convert.ToDecimal(dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"]);
											}

											if((dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"] != null) && 
												(!dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
												(dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"].ToString() != ""))
											{//����ѹ���¡����Թ ������Թ仹Ш��
												if(tmpCODSurchrg < Convert.ToDecimal(dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"]))
													tmpCODSurchrg = Convert.ToDecimal(dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"]);
											}
											//Rounding
											tmpCODSurchrg = TIESUtility.EnterpriseRounding(tmpCODSurchrg,(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]);
										}
									}
										//Enterprise
									else
									{
										tmpCODSurchrg = Convert.ToDecimal(drVAS["surcharge"]);
									}
								}
								else
								{
									tmpCODSurchrg = Convert.ToDecimal(drVAS["surcharge"]);
								}
								//������Թ���� min/max COD ����˹�������� ����Թ/�Ҵ ����ྴҹ����˹������ྴҹ����˹�᷹
								//��������� �����������Ǩ�� COD by X NOV 27 07
								//if have the data in surcharge amount will not find min/max
								//but if have the data in serchage percent will calculat the data
								// by Sittichai 09/01/2007
								//								if (dscustInfo.Tables[0].Rows.Count > 0)
								//								{
								//									if((dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"] != null) && 
								//										(!dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								//										(dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"].ToString() != ""))
								//									{ //��ҡ�˹������ꡫ� ��������Թ��ꡫ� �����仹Ш��
								//										if(tmpCODSurchrg > Convert.ToDecimal(dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"]))
								//											tmpCODSurchrg = Convert.ToDecimal(dscustInfo.Tables[0].Rows[0]["max_cod_surcharge"]);
								//
								//									}
								//
								//									if((dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"] != null) && 
								//										(!dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								//										(dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"].ToString() != ""))
								//									{//����ѹ���¡����Թ ������Թ仹Ш��
								//										if(tmpCODSurchrg < Convert.ToDecimal(dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"]))
								//											tmpCODSurchrg = Convert.ToDecimal(dscustInfo.Tables[0].Rows[0]["min_cod_surcharge"]);
								//									}
								//								}
								drNew[2] = tmpCODSurchrg; //������������ͧ���
							}
						}
						try
						{
							m_dsVAS.Tables[0].Rows.Add(drNew);
							m_dsVAS.AcceptChanges();
						}
						catch(Exception ex)
						{
							System.Console.Write(ex.Message);
						}

						Session["SESSION_DS1"] = m_dsVAS;
						BindVASGrid();	
						Utility.RegisterScriptFile("setScrollPosition.js","scrolltoinsert",this.Page);
					}
				}
			}
			else
			{
				if(m_dsVAS.Tables[0].Select("vas_code = 'COD'").Length > 0)
				{
					m_dsVAS.Tables[0].Select("vas_code = 'COD'")[0].Delete();
					m_dsVAS.AcceptChanges();
				}
				
				Session["SESSION_DS1"] = m_dsVAS;
				BindVASGrid();	
				Utility.RegisterScriptFile("setScrollPosition.js","scrolltoinsert",this.Page);
			}


			decimal totSurcharge = GetTotalSurcharge();
			txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], totSurcharge);
		}


		private void txtCODAmount_TextChanged(object sender, System.EventArgs e)
		{
			CalculateCODVAS();
			CalculateTotalAmt();
		}

		//Phase2 - J03
		private void btnTelephonePopup_Click(object sender, System.EventArgs e)
		{
			String sUrl = "RecipientReferencesPopup.aspx?FORMID="+"DomesticShipment" +
				"&TELEPHONENUMBER="+ txtRecipTel.Text.Trim() +
				"&CIDTelephone=" + txtRecipTel.ClientID + 
				"&CIDName=" + txtRecName.ClientID + 
				"&CIDAddress1=" + txtRecipAddr1.ClientID + 
				"&CIDAddress2=" + txtRecipAddr2.ClientID + 
				"&CIDPostalCode=" + txtRecipZip.ClientID + 
				"&CIDCity=" + txtRecipCity.ClientID + 
				"&CIDState=" + txtRecipState.ClientID+ 
				"&CIDContactPerson=" + txtRecpContPer.ClientID + 
				"&CIDFax=" + txtRecipFax.ClientID +
				"&CIDRouteCode=" + txtRouteCode.ClientID +
				"&CIDDestination=" + txtDestination.ClientID +
				"&ESTDELDT=" + txtShipEstDlvryDt.Text.TrimStart().TrimEnd() +	//HC Return Task
				"&CIDESTHCDT=" + txtEstHCDt.ClientID +
				"&STRCON=" + txtConsigNo.Text.Trim() +
				"&STRBOOKINGNO=" + txtBookingNo.Text.Trim(); //HC Return Task
					
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl.Replace("#", "Flat"));

			String sScript = Utility.GetScript("openLargeWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
		//Phase2 - J03

		//Phase2 - J03
		private void txtRecipTel_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			DataSet dsReferences = DomesticShipmentMgrDAL.getReferencesByTelephoneNumber(appID,enterpriseID,
				0, 0, txtRecipTel.Text.TrimStart().TrimEnd()).ds;

			if (dsReferences.Tables[0].Rows.Count > 0)
			{
				DataRow dr = dsReferences.Tables[0].Rows[0];

				
				if((bool)ViewState["PartialCon"]==true&&txtRecipZip.Text != dr["zipcode"].ToString())
				{
					lblConfirmMsg.Text = "Recipeint Postal code from Consignment Details does not match auto-populated value. Overwrite the Recipient Postal Code? ";
					ViewState["txtRecipTel"] = dr["telephone"].ToString();
					ViewState["txtRecName"] = dr["reference_name"].ToString();
					ViewState["txtRecipAddr1"] = dr["address1"].ToString();
					ViewState["txtRecipAddr2"] = dr["address2"].ToString();
					ViewState["txtRecipZip"] = dr["zipcode"].ToString();
					ViewState["PopUpRecipZip"] = true;
					DomstcShipPanel.Visible = true;	
					divCustIdCng.Visible=false;
					divMain.Visible=false;
					ddbCustType.Visible = false;
					return;
				}
				else
				{
					txtRecipTel.Text = dr["telephone"].ToString();
					txtRecName.Text = dr["reference_name"].ToString();
					txtRecipAddr1.Text = dr["address1"].ToString();
					txtRecipAddr2.Text = dr["address2"].ToString();
					txtRecipZip.Text = dr["zipcode"].ToString();
				}


				String strCountry = "";
				String strState = "";
				String routeCode = "";

				Zipcode zipCode = new Zipcode();
				zipCode.Populate(appID,enterpriseID, dr["zipcode"].ToString());
				strCountry = zipCode.Country;
				zipCode.Populate(appID,enterpriseID, strCountry, dr["zipcode"].ToString()); 
				strState=zipCode.StateName;
				routeCode=zipCode.DeliveryRoute;

				txtRecipCity.Text = strState;
				txtRecipState.Text = strCountry;
				txtDestination.Text = strState;
				txtRecpContPer.Text = dr["contactperson"].ToString();
				txtRecipFax.Text = dr["fax"].ToString();
				txtRouteCode.Text = routeCode;

				//HC Return Task
				Zipcode zipCodeTmp = new Zipcode();
				zipCodeTmp.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
				String strStateCode = zipCodeTmp.StateCode;

				if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCode.Trim() != ""))
				{
					string strConNo = "";
					string strBookingNo = "";

					if (txtBookingNo.Text.Trim() != "" )
						strBookingNo = txtBookingNo.Text.Trim();
					if (txtConsigNo.Text.Trim() != "" )
						strConNo = txtConsigNo.Text.Trim();

					object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCode,
						strConNo.Trim(), strBookingNo.Trim(), appID, enterpriseID);

					if((tmpReHCDateTime != null) && 
						(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
					}
					else
					{
						txtEstHCDt.Text = "";
					}
				}
				else
				{
					txtEstHCDt.Text = "";
				}
				//HC Return Task
			}
			else
			{
				//				txtRecName.Text = "";
				//				txtRecipAddr1.Text = "";
				//				txtRecipAddr2.Text = "";
				//				txtRecipZip.Text = "";
				//				txtRecipCity.Text = "";
				//				txtRecipState.Text = "";
				//				txtRecpContPer.Text = "";
				//				txtRecipFax.Text = "";
				//				txtRouteCode.Text = "";

				lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_REF_TELE_NO",utility.GetUserCulture());
				return;
			}
		}
		//Phase2 - J03

		private void txtESASurchrg_TextChanged(object sender, System.EventArgs e)
		{
		
		}
		//
		//		private void calCulateESA()
		//		{
		//			//Calculate the ESA surcharge
		//			if(txtRecipZip.Text.Length > 0 && txtRecipZip.Text != null && 
		//				txtSendZip.Text.Length > 0 && txtSendZip.Text != null)
		//			{
		//				String strApplyDimWt = null;
		//				String strApplyESA = null;
		//
		//				Customer customer = new Customer();
		//				customer.Populate(appID, enterpriseID, txtCustID.Text);
		//				strApplyDimWt = customer.ApplyDimWt;
		//				strApplyESA = customer.ESASurcharge;
		//
		//				if(strApplyESA == "Y")
		//				{
		//					
		//					bool isHasCustZone = true;
		//					decimal surchargeOfSender = 0;
		//					decimal surchargeOfRecip = 0;
		//					decimal decSrchrg = 0;
		//
		//					//***************** Get surcharge from Customer Zone by Sender Zipcode *************************
		//					DataSet tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
		//						0, 0, txtCustID.Text.Trim(), txtSendZip.Text.Trim()).ds;
		//					if(tmpCustZone.Tables[0].Rows.Count > 0)
		//					{
		//						if(((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
		//							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
		//							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != "")) ||
		//							((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
		//							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
		//							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
		//							)
		//						{
		//							if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
		//								(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
		//								(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != ""))
		//							{
		//								surchargeOfSender = (decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"];
		//							}
		//
		//							if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
		//								(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
		//								(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
		//							{
		//								decimal tmpFreight = 0;
		//
		//								if (txtFreightChrg.Text.Trim() != "")
		//									tmpFreight = Convert.ToDecimal(txtFreightChrg.Text.Trim());
		//
		//								surchargeOfSender = (tmpFreight * ((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"])) / 100;
		//							}
		//						}
		//						else
		//						{	
		//							isHasCustZone = false;
		//						}
		//					}
		//					else
		//					{
		//						isHasCustZone = false;
		//					}
		//					//***************** Get surcharge from Customer Zone by Sender Zipcode *************************
		//
		//					//***************** Get surcharge from Customer Zone by Recept Zipcode *************************
		//					tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
		//						0, 0, txtCustID.Text.Trim(), txtRecipZip.Text.Trim()).ds;
		//					if(tmpCustZone.Tables[0].Rows.Count > 0)
		//					{
		//						if(((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
		//							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
		//							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != "")) ||
		//							((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
		//							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
		//							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
		//							)
		//						{
		//							if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
		//								(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
		//								(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != ""))
		//							{
		//								surchargeOfRecip = (decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"];
		//							}
		//
		//							if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
		//								(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
		//								(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
		//							{
		//								decimal tmpFreight = 0;
		//
		//								if (txtFreightChrg.Text.Trim() != "")
		//									tmpFreight = Convert.ToDecimal(txtFreightChrg.Text.Trim());
		//
		//								surchargeOfRecip = (tmpFreight * ((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"])) / 100;
		//							}
		//						}
		//						else
		//						{	
		//							isHasCustZone = false;
		//						}
		//					}
		//					else
		//					{
		//						isHasCustZone = false;
		//					}
		//					//***************** Get surcharge from Customer Zone by Recept Zipcode *************************
		//
		//					if (isHasCustZone)
		//					{
		//						decSrchrg = surchargeOfSender + surchargeOfRecip;
		//					}
		//					else
		//					{
		//						Zipcode zipcode = new Zipcode();
		//						zipcode.Populate(appID,enterpriseID,txtSendZip.Text.Trim());
		//						decSrchrg = zipcode.EASSurcharge;
		//
		//						zipcode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
		//						decSrchrg = decSrchrg + zipcode.EASSurcharge;
		//					}
		//
		//					txtESASurchrg.Text = String.Format((String)ViewState["m_format"], decSrchrg);
		//				}	
		//				else
		//				{
		//					txtESASurchrg.Text = String.Format((String)ViewState["m_format"], 0);
		//				}
		//			}
		//		}

		//Modify By GwanG 
		//New Function CalculateESA On 08/01/08
		private void calCulateESA()
		{
			//Calculate the ESA surcharge
			if( txtRecipZip.Text.Length > 0 && txtRecipZip.Text != null && 
				txtSendZip.Text.Length > 0 && txtSendZip.Text != null /*&& IsCalculate("original_rated_esa")*/)
			{
				String strApplyDimWt = null;
				String strApplyESA = null;
				String strApplyESADel = null;

				Customer customer = new Customer();
				customer.Populate(appID, enterpriseID, txtCustID.Text);
				strApplyDimWt = customer.ApplyDimWt;
				strApplyESA = customer.ESASurcharge;
				strApplyESADel = customer.ESASurchargeDel;
				decimal surchargeOfSender = 0;
				decimal surchargeOfRecip = 0;
				decimal decSrchrg = 0;

				if(strApplyESA == "Y")
				{					
					//***************** Get surcharge from Customer Zone by Sender Zipcode *************************
					DataSet tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
						0, 0, txtCustID.Text.Trim(), txtSendZip.Text.Trim()).ds;
					if(tmpCustZone.Tables[0].Rows.Count > 0)
					{
						//Modify by Gwang on 4-Feb-08
						//A both of them have a value
						if(((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != "")) &&
							((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
							)
						{
							//At least is more than zero
							if(((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != 0) || ((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != 0))
							{
								if((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != 0)
								{
									surchargeOfSender = (decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"];
								}
								else if((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != 0)
								{
									decimal tmpFreight = 0;

									if (txtFreightChrg.Text.Trim() != "")
										tmpFreight = Convert.ToDecimal(txtFreightChrg.Text.Trim());

									surchargeOfSender = (tmpFreight * ((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"])) / 100;
								}

							}
								//A both of them is Zero.the surcharge is Zero
							else
							{
								surchargeOfSender = 0;								
							}
						}
							//At least one has a value
						else if(((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != "")) ||
							((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
							)
						{
							if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != ""))
							{
								surchargeOfSender = (decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"];
							}

							if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
							{
								decimal tmpFreight = 0;

								if (txtFreightChrg.Text.Trim() != "")
									tmpFreight = Convert.ToDecimal(txtFreightChrg.Text.Trim());

								surchargeOfSender = (tmpFreight * ((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"])) / 100;
							}
						}
							//A both of them has Null/""
						else
						{	
							Zipcode zipcode = new Zipcode();
							zipcode.Populate(appID,enterpriseID,txtSendZip.Text.Trim());
							surchargeOfSender = zipcode.EASSurcharge;
						}
					}
				
					else
					{	
						Zipcode zipcode = new Zipcode();
						zipcode.Populate(appID,enterpriseID,txtSendZip.Text.Trim());
						surchargeOfSender = zipcode.EASSurcharge;
					}
				}
				else
				{
					surchargeOfSender = 0;
				}

				if(strApplyESADel == "Y")
				{

					//***************** Get surcharge from Customer Zone by Recept Zipcode *************************
					DataSet tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
						0, 0, txtCustID.Text.Trim(), txtRecipZip.Text.Trim()).ds;
					if(tmpCustZone.Tables[0].Rows.Count > 0)
					{
						//Modify by Gwang on 4-Feb-08
						//A both of them have a value
						if(((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != "")) &&
							((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
							)
						{
							//A is more than zero
							if(((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != 0) || ((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != 0))
							{
								if((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != 0)
								{
									surchargeOfRecip = (decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"];
								}
								else if((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != 0)
								{
									decimal tmpFreight = 0;

									if (txtFreightChrg.Text.Trim() != "")
										tmpFreight = Convert.ToDecimal(txtFreightChrg.Text.Trim());

									surchargeOfRecip = (tmpFreight * ((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"])) / 100;
								}

							}
								//A both of them is Zero.the surcharge is Zero
							else
							{
								surchargeOfRecip = 0;								
							}
						}
							//At least one has a value
						else if(((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != "")) ||
							((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
							(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
							)
						{
							if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"].ToString() != ""))
							{
								surchargeOfRecip = (decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_amt"];
							}

							if((tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"] != null) && 
								(!tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"].ToString() != ""))
							{
								decimal tmpFreight = 0;

								if (txtFreightChrg.Text.Trim() != "")
									tmpFreight = Convert.ToDecimal(txtFreightChrg.Text.Trim());

								surchargeOfRecip = (tmpFreight * ((decimal)tmpCustZone.Tables[0].Rows[0]["extended_area_surcharge_percent"])) / 100;
							}
						}	
						else
						{	
							Zipcode zipcode = new Zipcode();
							zipcode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
							surchargeOfRecip = zipcode.EASSurcharge;
						}
					}
					else
					{	
						Zipcode zipcode = new Zipcode();
						zipcode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
						surchargeOfRecip = zipcode.EASSurcharge;
					}
				}
				else
				{
					surchargeOfRecip = 0;
				}
				//***************** Summary Surcharge *************************

				decSrchrg = surchargeOfSender + surchargeOfRecip;
				txtESASurchrg.Text = String.Format((String)ViewState["m_format"], decSrchrg);
			}
		}


		//NEW ESA CALCULATION by X OCT 25 08
		private void calCulateESA_ServiceType()
		{
			decimal decSrchrg = 0;
			//Calculate the ESA surcharge
			if( txtRecipZip.Text.Length > 0 && txtRecipZip.Text != null && 
				txtSendZip.Text.Length > 0 && txtSendZip.Text != null /*&& IsCalculate("original_rated_esa")*/)
			{
				///// Comment,Revise and Moved Code to DomesticshipmentMgrDAL.cs By Hong 11-04-2011

				//////				String strApplyDimWt = null;
				//////				String strApplyESA = null;
				//////				String strApplyESADel = null;
				//////
				//////				Customer customer = new Customer();
				//////				customer.Populate(appID, enterpriseID, txtCustID.Text);
				//////				strApplyDimWt = customer.ApplyDimWt;
				//////				strApplyESA = customer.ESASurcharge;
				//////				strApplyESADel = customer.ESASurchargeDel;
				//////				decimal surchargeOfSender = 0;
				//////				decimal surchargeOfRecip = 0;
				//////				decimal decSrchrg = 0;
				//////
				//////				if(strApplyESA == "Y")
				//////				{					
				//////					//***************** Get surcharge from Customer Zone by Sender Zipcode *************************
				//////					DataSet tmpCustZone = SysDataManager2.GetESASurchargeDS(utility.GetAppID(), utility.GetEnterpriseID(),
				//////						txtCustID.Text.Trim(), txtSendZip.Text.Trim(),this.txtShpSvcCode.Text.Trim());
				//////					//return ESA Surcharge All just send CustID and ZipCode 
				//////
				//////					//					 tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
				//////					//						0, 0, txtCustID.Text.Trim(), txtSendZip.Text.Trim()).ds;
				//////					if(tmpCustZone.Tables[0].Rows.Count > 0)
				//////					{
				//////						//Modify by Gwang on 4-Feb-08
				//////						//A both of them have a value
				//////						if(((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
				//////							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != "")) &&
				//////							((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
				//////							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
				//////							)
				//////						{
				//////							//At least is more than zero
				//////							if(((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != 0) || ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0))
				//////							{
				//////								if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != 0)
				//////								{
				//////									surchargeOfSender = (decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"];
				//////								}
				//////								else if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0)
				//////								{
				//////									decimal tmpFreight = 0;
				//////
				//////									if (txtFreightChrg.Text.Trim() != "")
				//////										tmpFreight = Convert.ToDecimal(txtFreightChrg.Text.Trim());
				//////
				//////									surchargeOfSender = (tmpFreight * ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"])) / 100;
				//////								}
				//////
				//////							}
				//////								//A both of them is Zero.the surcharge is Zero
				//////							else
				//////							{
				//////								surchargeOfSender = 0;								
				//////							}
				//////						}
				//////							//At least one has a value
				//////						else if(((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
				//////							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != "")) ||
				//////							((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
				//////							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
				//////							)
				//////						{
				//////							if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
				//////								(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////								(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != ""))
				//////							{
				//////								surchargeOfSender = (decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"];
				//////							}
				//////
				//////							if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
				//////								(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////								(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
				//////							{
				//////								decimal tmpFreight = 0;
				//////
				//////								if (txtFreightChrg.Text.Trim() != "")
				//////									tmpFreight = Convert.ToDecimal(txtFreightChrg.Text.Trim());
				//////
				//////								surchargeOfSender = (tmpFreight * ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"])) / 100;
				//////							}
				//////						}
				//////							//A both of them has Null/""
				//////						else
				//////						{	
				//////							//							Zipcode zipcode = new Zipcode();
				//////							//							zipcode.Populate(appID,enterpriseID,txtSendZip.Text.Trim());
				//////							surchargeOfSender = 0;//zipcode.EASSurcharge;
				//////						}
				//////
				//////						//Apply MIN MAX ONLY FOR PERCENT
				//////
				//////						if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
				//////							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
				//////						{
				//////							if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0)
				//////							{
				//////								if((tmpCustZone.Tables[0].Rows[0]["min_esa_amount"] != null) && 
				//////									(!tmpCustZone.Tables[0].Rows[0]["min_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////									(tmpCustZone.Tables[0].Rows[0]["min_esa_amount"].ToString() != ""))
				//////									if(surchargeOfSender<(decimal)tmpCustZone.Tables[0].Rows[0]["min_esa_amount"])					
				//////										surchargeOfSender=(decimal)tmpCustZone.Tables[0].Rows[0]["min_esa_amount"];
				//////							
				//////								if((tmpCustZone.Tables[0].Rows[0]["max_esa_amount"] != null) && 
				//////									(!tmpCustZone.Tables[0].Rows[0]["max_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////									(tmpCustZone.Tables[0].Rows[0]["max_esa_amount"].ToString() != ""))
				//////									if(surchargeOfSender>(decimal)tmpCustZone.Tables[0].Rows[0]["max_esa_amount"])					
				//////										surchargeOfSender=(decimal)tmpCustZone.Tables[0].Rows[0]["max_esa_amount"];
				//////							}
				//////						}
				//////						//END Apply MIN MAX
				//////
				//////					}
				//////				
				//////					else //NO DATA FROM DB
				//////					{	
				//////						//						Zipcode zipcode = new Zipcode();
				//////						//						zipcode.Populate(appID,enterpriseID,txtSendZip.Text.Trim());
				//////						surchargeOfSender = 0;//zipcode.EASSurcharge;
				//////					}
				//////				}
				//////				else //NOT APPLY ESA
				//////				{
				//////					surchargeOfSender = 0;
				//////				}
				//////
				//////				if(strApplyESADel == "Y")
				//////				{
				//////
				//////					//***************** Get surcharge from Customer Zone by Recept Zipcode *************************
				//////					DataSet tmpCustZone = SysDataManager2.GetESASurchargeDS(utility.GetAppID(), utility.GetEnterpriseID(),
				//////						txtCustID.Text.Trim(), this.txtRecipZip.Text.Trim(),this.txtShpSvcCode.Text.Trim());
				//////
				//////					//					DataSet tmpCustZone = SysDataMgrDAL.GetCustZoneData(appID, enterpriseID,
				//////					//						0, 0, txtCustID.Text.Trim(), txtRecipZip.Text.Trim()).ds;
				//////					if(tmpCustZone.Tables[0].Rows.Count > 0)
				//////					{
				//////						//Modify by Gwang on 4-Feb-08
				//////						//A both of them have a value
				//////						if(((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
				//////							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != "")) &&
				//////							((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
				//////							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
				//////							)
				//////						{
				//////							//A is more than zero
				//////							if(((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != 0) || ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0))
				//////							{
				//////								if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != 0)
				//////								{
				//////									surchargeOfRecip = (decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"];
				//////								}
				//////								else if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0)
				//////								{
				//////									decimal tmpFreight = 0;
				//////
				//////									if (txtFreightChrg.Text.Trim() != "")
				//////										tmpFreight = Convert.ToDecimal(txtFreightChrg.Text.Trim());
				//////
				//////									surchargeOfRecip = (tmpFreight * ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"])) / 100;
				//////								}
				//////
				//////							}
				//////								//A both of them is Zero.the surcharge is Zero
				//////							else
				//////							{
				//////								surchargeOfRecip = 0;								
				//////							}
				//////						}
				//////							//At least one has a value
				//////						else if(((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
				//////							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != "")) ||
				//////							((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
				//////							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
				//////							)
				//////						{
				//////							if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"] != null) && 
				//////								(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////								(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"].ToString() != ""))
				//////							{
				//////								surchargeOfRecip = (decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_amt"];
				//////							}
				//////
				//////							if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
				//////								(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////								(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
				//////							{
				//////								decimal tmpFreight = 0;
				//////
				//////								if (txtFreightChrg.Text.Trim() != "")
				//////									tmpFreight = Convert.ToDecimal(txtFreightChrg.Text.Trim());
				//////
				//////								surchargeOfRecip = (tmpFreight * ((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"])) / 100;
				//////							}
				//////						}	
				//////						else
				//////						{	
				//////							//							Zipcode zipcode = new Zipcode();
				//////							//							zipcode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
				//////							surchargeOfRecip = 0;//zipcode.EASSurcharge;
				//////						}
				//////
				//////						//Apply MIN MAX
				//////						if((tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != null) && 
				//////							(!tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////							(tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"].ToString() != ""))
				//////						{
				//////							if((decimal)tmpCustZone.Tables[0].Rows[0]["esa_surcharge_percent"] != 0)
				//////							{
				//////
				//////								if((tmpCustZone.Tables[0].Rows[0]["min_esa_amount"] != null) && 
				//////									(!tmpCustZone.Tables[0].Rows[0]["min_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////									(tmpCustZone.Tables[0].Rows[0]["min_esa_amount"].ToString() != ""))
				//////									if(surchargeOfRecip<(decimal)tmpCustZone.Tables[0].Rows[0]["min_esa_amount"])					
				//////										surchargeOfRecip=(decimal)tmpCustZone.Tables[0].Rows[0]["min_esa_amount"];
				//////							
				//////								if((tmpCustZone.Tables[0].Rows[0]["max_esa_amount"] != null) && 
				//////									(!tmpCustZone.Tables[0].Rows[0]["max_esa_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
				//////									(tmpCustZone.Tables[0].Rows[0]["max_esa_amount"].ToString() != ""))
				//////									if(surchargeOfRecip>(decimal)tmpCustZone.Tables[0].Rows[0]["max_esa_amount"])					
				//////										surchargeOfRecip=(decimal)tmpCustZone.Tables[0].Rows[0]["max_esa_amount"];
				//////							}
				//////						}
				//////						//END Apply MIN MAX
				//////
				//////					}
				//////					else// no data from DB
				//////					{	
				//////						//						Zipcode zipcode = new Zipcode();
				//////						//						zipcode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
				//////						surchargeOfRecip = 0;//zipcode.EASSurcharge;
				//////					}
				//////				}
				//////				else //NO ESA apply
				//////				{
				//////					surchargeOfRecip = 0;
				//////				}
				//////				//***************** Summary Surcharge *************************
				//////
				//////				decSrchrg = surchargeOfSender + surchargeOfRecip;
				///// Comment,Revise and Moved Code to DomesticshipmentMgrDAL.cs By Hong 11-04-2011
				decSrchrg = DomesticShipmentMgrDAL.calCulateESA_ServiceType(appID,enterpriseID,txtCustID.Text,txtSendZip.Text,txtRecipZip.Text,txtShpSvcCode.Text,txtFreightChrg.Text);
				txtESASurchrg.Text = String.Format((String)ViewState["m_format"], decSrchrg);
			}
		}
		//END OF NEW ESA CALCULATION by X OCT 25 08



		//HC Return Task
		private void chkInvHCReturn_CheckedChanged(object sender, System.EventArgs e)
		{
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if (chkshpRtnHrdCpy.Checked == true ||chkInvHCReturn.Checked == true)
			{
				//HC Return Task
				Zipcode zipCode = new Zipcode();
				zipCode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
				String strStateCode = zipCode.StateCode;

				if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCode.Trim() != ""))
				{
					string strConNo = "";
					string strBookingNo = "";

					if (txtBookingNo.Text.Trim() != "" )
						strBookingNo = txtBookingNo.Text.Trim();
					if (txtConsigNo.Text.Trim() != "" )
						strConNo = txtConsigNo.Text.Trim();

					object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCode,
						strConNo.Trim(), strBookingNo.Trim(), appID, enterpriseID);

					if((tmpReHCDateTime != null) && 
						(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
					}
					else
					{
						txtEstHCDt.Text = "";
					}
				}
				else
				{
					txtEstHCDt.Text = "";
				}
				//HC Return Task
			}
			else if(chkshpRtnHrdCpy.Checked == false && chkInvHCReturn.Checked == false)
			{
				txtEstHCDt.Text = "";
			}
		}

		//For Customer_VAS
		private void AddNewRowVAS(DataSet dsInsert)
		{
			AddRowInVASGrid();	
			dgVAS.EditItemIndex = m_dsVAS.Tables[0].Rows.Count - 1;

			DataRow drNew = m_dsVAS.Tables[0].Rows[dgVAS.EditItemIndex];
			decimal tmpCODSurchrg = 0;
			if(dsInsert.Tables[0].Rows.Count > 0)
			{
				DataRow drVAS = dsInsert.Tables[0].Rows[0];

				if((drVAS["vas_code"] != null) && 
					(!drVAS["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
					(drVAS["vas_code"].ToString() != ""))
				{
					drNew[0] = Convert.ToString(drVAS["vas_code"]);
				}
			
				if((drVAS["description"] != null) && 
					(!drVAS["description"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
					(drVAS["description"].ToString() != ""))
				{
					drNew[1] = Convert.ToString(drVAS["description"]);
				}
			
			
				if((drVAS["surcharge"] != null) && 
					(!drVAS["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
					(drVAS["surcharge"].ToString() != ""))
				{
					tmpCODSurchrg =  Convert.ToDecimal(drVAS["surcharge"]);		
				}
				else
				{					
				
					if((drVAS["percent"] != null) && 
						(!drVAS["percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
						(drVAS["percent"].ToString() != ""))
					{
						tmpCODSurchrg = (Convert.ToDecimal(txtFreightChrg.Text) * ((decimal)drVAS["percent"])) / 100;
			
						//Boundary Min & Max
						if((drVAS["max_surcharge"] != null) && 
							(!drVAS["max_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(drVAS["max_surcharge"].ToString() != ""))
						{ //��ҡ�˹������ꡫ� ��������Թ��ꡫ� �����仹Ш��
							if(tmpCODSurchrg > Convert.ToDecimal(drVAS["max_surcharge"]))
								tmpCODSurchrg = Convert.ToDecimal(drVAS["max_surcharge"]);
						}
			
						if((drVAS["min_surcharge"] != null) && 
							(!drVAS["min_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
							(drVAS["min_surcharge"].ToString() != ""))
						{//����ѹ���¡����Թ ������Թ仹Ш��
							if(tmpCODSurchrg < Convert.ToDecimal(drVAS["min_surcharge"]))
								tmpCODSurchrg = Convert.ToDecimal(drVAS["min_surcharge"]);
						}
						//Rounding
						tmpCODSurchrg = TIESUtility.EnterpriseRounding(tmpCODSurchrg,(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]);
					}
				}	
				drNew[2] = tmpCODSurchrg; //������������ͧ���
	
				//Added by GwanG on 06May08
				if(Utility.IsNotDBNull(ViewState["m_dsVAS_old"]) && (drVAS["vas_code"] != null) && 
					(!drVAS["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
					(drVAS["vas_code"].ToString() != ""))
				{
					DataSet m_dsVAS_old = (DataSet)ViewState["m_dsVAS_old"];
					if(m_dsVAS_old.Tables[0].Select("vas_code = '"+drVAS["vas_code"]+"' and remarks is not null" ).Length > 0)
					{
						DataRow drEach_old = m_dsVAS_old.Tables[0].Select("vas_code = '"+drVAS["vas_code"]+"' and remarks is not null")[0];
						drNew[3] = drEach_old["remarks"];
					}

				}


				if(txtTotVASSurChrg.Text.Length > 0)
				{
					txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], Convert.ToDecimal(txtTotVASSurChrg.Text)+tmpCODSurchrg);
				}
				else
				{
					txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], tmpCODSurchrg);
				}
			}
				
			CalculateTotalAmt();
			dgVAS.EditItemIndex = -1;
			BindVASGrid();
		}
	
		//For VAS
		private void AddNewRowVAS1(DataSet dsInsert)
		{
			AddRowInVASGrid();	
			dgVAS.EditItemIndex = m_dsVAS.Tables[0].Rows.Count - 1;

			DataRow drNew = m_dsVAS.Tables[0].Rows[dgVAS.EditItemIndex];

			decimal tmpCODSurchrg = 0;
			if(dsInsert.Tables[0].Rows.Count > 0)
			{
				DataRow drVAS = dsInsert.Tables[0].Rows[0];
				if((drVAS["vas_code"] != null) && 
					(!drVAS["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
					(drVAS["vas_code"].ToString() != ""))
				{
					drNew[0] = Convert.ToString(drVAS["vas_code"]);
				}
			
				if((drVAS["description"] != null) && 
					(!drVAS["description"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
					(drVAS["description"].ToString() != ""))
				{
					drNew[1] = Convert.ToString(drVAS["description"]);
				}
			
			
				if((drVAS["surcharge"] != null) && 
					(!drVAS["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
					(drVAS["surcharge"].ToString() != ""))
				{					
					drNew[2] = drVAS["surcharge"];
					tmpCODSurchrg = Convert.ToDecimal(drVAS["surcharge"]); //������������ͧ���	
				}

				//Added by GwanG on 06May08
				if(Utility.IsNotDBNull(ViewState["m_dsVAS_old"]) && (drVAS["vas_code"] != null) && 
					(!drVAS["vas_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
					(drVAS["vas_code"].ToString() != ""))
				{
					DataSet m_dsVAS_old = (DataSet)ViewState["m_dsVAS_old"];
					if(m_dsVAS_old.Tables[0].Select("vas_code = '"+drVAS["vas_code"]+"' and remarks is not null" ).Length > 0)
					{
						DataRow drEach_old = m_dsVAS_old.Tables[0].Select("vas_code = '"+drVAS["vas_code"]+"' and remarks is not null")[0];
						drNew[3] = drEach_old["remarks"];
					}

				}
				

				if(txtTotVASSurChrg.Text.Length > 0)
				{
					txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], Convert.ToDecimal(txtTotVASSurChrg.Text)+tmpCODSurchrg);
				}
				else
				{
					txtTotVASSurChrg.Text = String.Format((String)ViewState["m_format"], tmpCODSurchrg);
				}
			}
				
			CalculateTotalAmt();
			dgVAS.EditItemIndex = -1;
			BindVASGrid();
		}
	

		
		//HC Return Task
	

		#region Manual Rating Override
		private void SetOriginalCtrl(bool visible)
		{
			txtOriFreightChrg.Visible=visible;
			txtOriInsChrg.Visible=visible;
			txtOriOthersurcharge2.Visible=visible;
			txtOriESASurchrg.Visible=visible;
			txtOriShpTotAmt.Visible=visible;

			lblRateOverrideBy.Visible = visible;
			lblRateOverrideByValue.Visible = visible;
			lblRateOverrideDate.Visible = visible;
			lblRateOverrideDateValue.Visible = visible;		
	
			//Added by GwanG On 15May08
			lblOritotVas.Visible = visible;

			if(visible == false)
			{
				Session["SESSION_DS2"]=null;
				if((int)ViewState["DSMode"] != (int)ScreenMode.ExecuteQuery)
				{
					ViewState["DSMode"] = ScreenMode.Query;
				}				
			}			
		}

		private void EnterpriseConfigurations()
		{
			DomesticShipmentConfigurations conf = new DomesticShipmentConfigurations();
			conf = EnterpriseConfigMgrDAL.GetDomesticShipmentConfigurations(appID,enterpriseID);			
			//----------------- HCSupportedbyEnterprise ---------------------------------------
			lblEstHCDt.Visible = conf.HCSupportedbyEnterprise;
			txtEstHCDt.Visible = conf.HCSupportedbyEnterprise;
			lblActHCDt.Visible = conf.HCSupportedbyEnterprise;
			txtActHCDt.Visible = conf.HCSupportedbyEnterprise;
			chkshpRtnHrdCpy.Visible = conf.HCSupportedbyEnterprise;
			chkInvHCReturn.Visible = conf.HCSupportedbyEnterprise;
//			if(conf.HCSupportedbyEnterprise == false)
//			{
//				chkshpRtnHrdCpy.Checked = false;
//				chkInvHCReturn.Checked = false;
//			}
			//----------------- CODSupportedbyEnterprise ---------------------------------------
			lblCODAmount.Visible = conf.CODSupportedbyEnterprise;
			txtCODAmount.Visible = conf.CODSupportedbyEnterprise;
//			if(conf.CODSupportedbyEnterprise == false)
//			{
//				txtCODAmount.Text="0";
//			}
			//----------------- InsSupportedbyEnterprise ---------------------------------------
			lblShipDclValue.Visible = conf.InsSupportedbyEnterprise;
			txtShpDclrValue.Visible = conf.InsSupportedbyEnterprise;
			lblShipMaxCovg.Visible = conf.InsSupportedbyEnterprise;
			txtShpMaxCvrg.Visible = conf.InsSupportedbyEnterprise;
			lblShipAdpercDV.Visible = conf.InsSupportedbyEnterprise;
			txtShpAddDV.Visible = conf.InsSupportedbyEnterprise;
			lblShipInsSurchrg.Visible = conf.InsSupportedbyEnterprise;
			txtShpInsSurchrg.Visible = conf.InsSupportedbyEnterprise;
//			if(conf.InsSupportedbyEnterprise==false)
//			{
//				txtShpDclrValue.Text="0";
//			}
			//----------------- CommCodeOnShipment ---------------------------------------
			lblPkgCommCode.Visible = conf.CommCodeOnShipment;
			txtPkgCommCode.Visible = conf.CommCodeOnShipment;
			btnPkgCommCode.Visible = conf.CommCodeOnShipment;
//			if(conf.CommCodeOnShipment==false)
//			{
//				txtPkgCommCode.Text="";
//			}

		}

		private void SetOriginalRating(DataRow row)
		{
			if(row["manual_override"].ToString().ToUpper()=="Y")
			{
				//Show Manual Panel
				SetOriginalCtrl(true);

				string m_format = (String)ViewState["m_format"];

				//Total Rated Amount
				decimal tot_rated= txtShpTotAmt.Text!=""?Convert.ToDecimal(txtShpTotAmt.Text):0;				
				

				//Original field
				if(row["original_rated_freight"]!=DBNull.Value)
				{
					decimal original_rated_freight=(decimal)row["original_rated_freight"];
					txtOriFreightChrg.Text=String.Format(m_format, original_rated_freight );

					decimal rated_freight = txtFreightChrg.Text!=""?Convert.ToDecimal(txtFreightChrg.Text):0;
					tot_rated += (original_rated_freight - rated_freight);
				}//ELSE ADD BY X APR 09 08
				else
				{
					txtOriFreightChrg.Text = this.txtFreightChrg.Text;
				}

				if(row["original_rated_ins"]!=DBNull.Value)
				{
					decimal original_rated_ins=(decimal)row["original_rated_ins"];
					txtOriInsChrg.Text=String.Format(m_format, original_rated_ins);	
				
					decimal rated_ins = txtInsChrg.Text!=""?Convert.ToDecimal(txtInsChrg.Text):0;
					tot_rated += (original_rated_ins - rated_ins);
				}//ELSE ADD BY X APR 09 08
				else
				{
					this.txtOriInsChrg.Text= this.txtInsChrg.Text;
				}

				if(row["original_rated_other"]!=DBNull.Value)
				{
					decimal original_rated_other=(decimal)row["original_rated_other"];
					txtOriOthersurcharge2.Text=String.Format(m_format, original_rated_other);

					decimal rated_other = txtOtherSurcharge2.Text!=""?Convert.ToDecimal(txtOtherSurcharge2.Text):0;
					tot_rated += (original_rated_other - rated_other);
				}//ELSE ADD BY X APR 09 08
				else
				{
					this.txtOriOthersurcharge2.Text=this.txtOtherSurcharge2.Text;
				}

				if(row["original_rated_esa"]!=DBNull.Value)
				{
					decimal original_rated_esa=(decimal)row["original_rated_esa"];
					txtOriESASurchrg.Text=String.Format(m_format, original_rated_esa);

					decimal rated_esa = txtESASurchrg.Text!=""?Convert.ToDecimal(txtESASurchrg.Text):0;
					tot_rated += (original_rated_esa - rated_esa);
				}//ELSE ADD BY X APR 09 08
				else
				{
					this.txtOriESASurchrg.Text=this.txtESASurchrg.Text;
				}
				//ADD BY X APR 09 08 (1 Line below)
				this.lblOritotVas.Text=this.txtTotVASSurChrg.Text;
				//if(row["original_rated_total"]!=DBNull.Value)
				//	txtOriShpTotAmt.Text= String.Format(m_format, (decimal)row["original_rated_total"]);				
				txtOriShpTotAmt.Text=String.Format(m_format, tot_rated);

				lblRateOverrideByValue.Text = row["manual_over_user"].ToString();
				lblRateOverrideDateValue.Text = Convert.ToDateTime(row["manual_over_datetime"]).ToString("dd/MM/yyyy HH:mm");

				// ################  Set LockControl  ################

				//Lock ������
				//LockAllManualOverride(true);

				// Origin freight
				if(row["original_rated_freight"]!=DBNull.Value) 
				{
					LockRecipientInfo(true);
					LockSenderInfo(true);
					//LockPackageInfo(true); 
					LockServiceCode(true);
				}

				// Origin insurance
				if(row["original_rated_ins"]!=DBNull.Value) 
				{
					LockDeclareValue(true);
				}

				// Origin other
				if(row["original_rated_other"]!=DBNull.Value) 
				{
					LockCustomerInfo(true);
					LockRecipientInfo(true);
					LockSenderInfo(true);
					LockServiceCode(true);
					//LockPackageInfo(true);					
				}

				// Origin esa
				if(row["original_rated_esa"]!=DBNull.Value) 
				{
					LockRecipientInfo(true);
					LockSenderInfo(true);
					//LockPackageInfo(true);
				}
			}
			else
				SetOriginalCtrl(false);
		}


		/// <summary>
		/// Calculate fields which relate with manual override column fields 
		/// </summary>
		private bool IsCalculate(string fieldname)
		{
			bool _IsCal=true;
			if(m_sdsDomesticShip.ds.Tables.Count>0)
			{
				DataTable tb = m_sdsDomesticShip.ds.Tables[0];
				if(tb.Columns.Contains(fieldname))
				{
					DataRow drCur = tb.Rows[Convert.ToInt32(ViewState["currentPage"])];
					
					object manual_override = drCur["manual_override"];
					if(manual_override==DBNull.Value || manual_override.ToString()=="N" || manual_override.ToString()=="")
						_IsCal = true;
					else if(manual_override.Equals("Y") && drCur[fieldname]!=DBNull.Value)
						_IsCal = false;
				}
			}

			return _IsCal;
		}


		private void LockAllManualOverride(bool islock)
		{
			LockCustomerInfo(islock);
			LockSenderInfo(islock);
			LockRecipientInfo(islock);
			LockPackageInfo(islock);
			LockServiceCode(islock);
			LockDeclareValue(islock);
			LockCODAmount(islock);
			LockVAS(islock);
		}
		private void LockCustomerInfo(bool islock)
		{
			ddbCustType.Enabled = !islock;
			chkNewCust.Enabled = !islock;
			txtCustID.ReadOnly = islock;
			btnDisplayCustDtls.Enabled = !islock;
			txtCustName.ReadOnly = islock;
			rbCash.Enabled=!islock;
			rbCredit.Enabled = !islock;
			txtCustAdd1.ReadOnly = txtCustAdd2.ReadOnly = islock;
			txtCustTelephone.ReadOnly = islock;
			txtCustFax.ReadOnly = islock;
			txtCustZipCode.ReadOnly =islock;
			txtCustCity.ReadOnly = islock;
			txtCustStateCode.ReadOnly = islock;
		}
		private void LockSenderInfo(bool islock)
		{
			chkSendCustInfo.Enabled = !islock;
			txtSendName.ReadOnly=islock;
			btnSendCust.Enabled=!islock;
			txtSendContPer.ReadOnly=islock;
			txtSendAddr1.ReadOnly = txtSendAddr2.ReadOnly=islock;
			txtSendTel.ReadOnly=islock;
			txtSendFax.ReadOnly=islock;
			txtSendZip.ReadOnly=islock;
			txtSendCity.ReadOnly=islock;
			txtSendState.ReadOnly=islock;
			txtSendCuttOffTime.ReadOnly=islock;
		}
		private void LockRecipientInfo(bool islock)
		{
			chkRecip.Enabled=!islock;
			txtRecipTel.ReadOnly=islock;
			btnTelephonePopup.Enabled=!islock;
			txtRecName.ReadOnly=islock;
			btnRecipNm.Enabled=!islock;
			txtRecpContPer.ReadOnly=islock;
			txtRecipAddr1.ReadOnly = txtRecipAddr2.ReadOnly=islock;
			txtRecipFax.ReadOnly=islock;
			txtRecipZip.ReadOnly=islock;
			txtRecipCity.ReadOnly=islock;
			txtRecipState.ReadOnly=islock;
		}
		private void LockPackageInfo(bool islock)
		{
			//��ͧ����� popup ���� ���� popup �����
			//btnPkgDetails.Enabled=!islock;
			//txtPkgCommCode.ReadOnly=islock;
			//btnPkgCommCode.Enabled=!islock;
			//txtPkgActualWt.ReadOnly=islock;
			//txtPkgDimWt.ReadOnly=islock;
			//txtPkgCommDesc.ReadOnly=islock;
			//txtPkgTotpkgs.ReadOnly=islock;
			//txtPkgChargeWt.ReadOnly=islock;
		}
		private void LockServiceCode(bool islock)
		{
			txtShpSvcCode.ReadOnly=islock;
			btnShpSvcCode.Enabled=!islock;
		}
		private void LockDeclareValue(bool islock)
		{
			txtShpDclrValue.ReadOnly=islock;
		}
		private void LockCODAmount(bool islock)
		{
			txtCODAmount.ReadOnly=islock;
		}
		private void LockVAS(bool islock)
		{
			btnPopulateVAS.Enabled=!islock;
			btnDGInsert.Enabled=!islock;
		}
		#endregion

		private decimal Rounding(decimal beforeRound)
		{
			return TIESUtility.EnterpriseRounding(beforeRound,(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]);
		}
		//Add by Panas 24/04/2009
		private decimal RoundingFreightCharge(decimal decFreight)
		{
			return TIESUtility.EnterpriseRounding(decFreight,(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]);
		}

		private void txtFreightChrg_TextChanged(object sender, System.EventArgs e)
		{
			CalculateAllCharge();
		}

		private void btnViewOldPD_Click(object sender, System.EventArgs e)
		{
			if(((int)ViewState["DSMode"] == (int)ScreenMode.ExecuteQuery) && (txtBookingNo.Text.Trim().Length > 0) /*&& ((int)ViewState["DSOperation"] != (int)Operation.Update)*/)
			{
				DataSet dsQryPkg = null;

				try
				{
					//call the method for getting the package details during query
					dsQryPkg = DomesticShipmentMgrDAL.GetPakageDtls(appID,enterpriseID,Convert.ToInt32(txtBookingNo.Text.Trim()), txtConsigNo.Text.Trim());
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					lblErrorMsg.Text = strMsg;
					return;
				}
				if((int)ViewState["DSOperation"] != (int)Operation.Update)
				{
					Session["SESSION_DS2"] = dsQryPkg;
				}
			}

			String strApplyDimWt = null;
			String strApplyESA = null;
			String strDim_By_tot = null;  //Jeab 28 Dec 10

			Customer customer = new Customer();
			customer.Populate(appID,enterpriseID,txtCustID.Text);
			strApplyDimWt = customer.ApplyDimWt;
			strApplyESA = customer.ESASurcharge;
			strDim_By_tot = customer.Dim_By_tot;  //Jeab 28 Dec 10
			
			DataRow drEach = m_sdsDomesticShip.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
			//calCulateESA();
			//calCulateESA_ServiceType();
			this.CalculateAllCharge();

			int iBookingNo = 0;
			if(txtBookingNo.Text.Length > 0)
				iBookingNo = Convert.ToInt32(txtBookingNo.Text);
			else
				iBookingNo = int.Parse(drEach["booking_no"].ToString());
	
			bool OnlyShow = !IsCalculate("original_rated_freight") || !IsCalculate("original_rated_other") || !IsCalculate("original_rated_esa");
			bool PartialCon = (bool)ViewState["PartialCon"];


			ViewState["PickupBooking"] = "yes";
			String sUrl = "PkgDetailsPopup.aspx?FormBtn=ViewOldPD&ApplyDimWt="+strApplyDimWt+
				"&BTNACTIVE="+(String)ViewState["PickupBooking"]+
				"&BOOKINGNO="+iBookingNo+
				"&CUSTID="+txtCustID.Text+
				"&OnlyShow="+ OnlyShow+
				"&PartialCon="+ PartialCon+
				"&TotPackage="+ txtPkgTotpkgs.Text+
				"&ConsignmentNumber="+ txtConsigNo.Text+
				"&ActWeight="+ txtActualWeight.Text+
				"&CODAmount="+ txtCODAmount.Text+
				"&ShipDclrVal="+ txtShpDclrValue.Text+
				"&TotVASSurch="+ this.txtTotVASSurChrg.Text+
				"&ServiceCode="+ this.txtShpSvcCode.Text+
				"&ServiceDesc="+ this.txtShpSvcDesc.Text+
				"&RecipZip="+ this.txtRecipZip.Text+
				"&SendZip="+ this.txtSendZip.Text+
				"&DimWeight="+ txtPkgDimWt.Text+
				"&DimByTOT="+ strDim_By_tot ;	 //Jeab 28 Dec 10

			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			//Modified By Tom Jan 19, 10
			String sScript = Utility.GetScript("openPkg.js",paramList);
			//End Modified By tom Jan 19, 10
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

		}

		private void btnCmd_Click(object sender, System.EventArgs e)
		{
			CalculateInsuranceSurcharge();
		}

		private void btnClientEvent_Click(object sender, System.EventArgs e)
		{
			txtCustID_TextChanged(null,null);
			//			CalculateInsuranceSurcharge();
			//			CalculateCODVAS();
			//			CalculateTotalAmt();
			//poppulateCustomerAddress();
		}

		private void hddConNo_ServerClick(object sender, System.EventArgs e)
		{
			string ConNo="";
			if(txtConsigNo.Text.Trim().Length > 0 &&Utility.ValidateConsignmentNo(txtConsigNo.Text.Trim(), ref ConNo)==false)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSNO_INVALID",utility.GetUserCulture()); 
				SetInitialFocus(txtConsigNo);
				return;
			}
			else
			{
				lblErrorMsg.Text="";
				txtConsigNo.Text=ConNo;
			}

			if(m_dsPkgDetails!=null)
				return;
			
			Compare_PD_SPKG();

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
			if ((this.txtConsigNo.Text.Trim().Length > 0) && ((int)ViewState["DSMode"] == (int)ScreenMode.Insert))
			{
				GetValuesIntoDS(0);
				ViewState["QUERY_DS"] = m_sdsDomesticShip.ds;
				//ViewState["DSMode"] = ScreenMode.ExecuteQuery;
				//ViewState["DSOperation"] = Operation.None;
				ViewState["isTextChanged"] = false;
				ViewState["currentPage"] = 0;
				ViewState["currentSet"] = 0;
				FetchDSRecSet();
				
				//Tumz.
				if(m_sdsDomesticShip.ds.Tables[0].Rows.Count > 0)
				{
					if ((m_sdsDomesticShip.ds.Tables[0].Rows[0]["pkg_detail_replaced_datetime"] != null) && (m_sdsDomesticShip.ds.Tables[0].Rows[0]["pkg_detail_replaced_datetime"].ToString() != ""))
					{
						lblPD_Replace_Date.Text = "Pakage Detail Replaced : " + ((DateTime)m_sdsDomesticShip.ds.Tables[0].Rows[0]["pkg_detail_replaced_datetime"]).ToString("dd/MM/yyyy HH:mm");
						lblPD_Replace_Date.Visible = true;
						btnViewOldPD.Visible = true;
					}
					else
					{
						lblPD_Replace_Date.Visible = false;
						btnViewOldPD.Visible = false;
					}
				}

				if(rbCash.Checked==true)
				{
					ViewState["payMode"] = "C";
				}
				else
				{
					ViewState["payMode"] = "R";
				}
			
				if(m_sdsDomesticShip.QueryResultMaxSize>0)
				{
					ViewState["PartialCon"]=true;
					DisplayRecords();
					if(txtBookingNo.Text.Length>0)
						BookingNoDetails();
					if(ViewState["InitialBooking"] != null)
						txtBookingNo.Text = null;
					SetInitialFocus(txtBookingNo);
				}
				else
				{
					string strConsigNo = this.txtConsigNo.Text;
					string strBookingNo = this.txtBookingNo.Text;
					fromInsertBtn = false;
					Insert_Click();
					this.txtConsigNo.Text = strConsigNo;
					this.txtBookingNo.Text = strBookingNo;
					SetInitialFocus(txtBookingNo);
					ViewState["PartialCon"]=false;
				}

			}			

			if ((ViewState["payMode"] != null) && (ViewState["payMode"].ToString() == "C"))
			{
				rbCash.Checked =true;
				rbCredit.Checked=false;
			}
		}


		private void EstimatedDeliveryDate()
		{			
			DataTable dtEst = null;
			string ActualPickupDT = null;
			string Service = null;
			string PUP_zipcode = null;
			string DEL_zipcode = null;
			if(txtShpSvcCode.Text.Trim() != "")
			{
				Service = txtShpSvcCode.Text.Trim();
			}
			else
			{
				this.EstimatedDeliveryDateData=null;
				return;
			}
			if(txtSendZip.Text.Trim() != "")
			{
				PUP_zipcode = txtSendZip.Text.Trim();
			}
			else
			{
				this.EstimatedDeliveryDateData=null;
				return;
			}
			if(txtRecipZip.Text.Trim() != "")
			{
				DEL_zipcode = txtRecipZip.Text.Trim();
			}
			else
			{
				this.EstimatedDeliveryDateData=null;
				return;
			}
			if(txtShipPckUpTime.Text.Trim() != "")
			{
				DateTime dEst = DateTime.ParseExact(this.txtShipPckUpTime.Text.Trim(),"dd/MM/yyyy HH:mm",null);
				ActualPickupDT = dEst.ToString("yyyy-MM-dd HH:mm");
			}

			if(this.EstimatedDeliveryDateData == null)
			{
				dtEst = PRBMgrDAL.GetEstimatedDeliveryDate(utility.GetAppID(),utility.GetEnterpriseID(),ActualPickupDT,Service,PUP_zipcode,	DEL_zipcode);
			}
			else
			{
				dtEst = this.EstimatedDeliveryDateData;
			}
			this.EstimatedDeliveryDateData = dtEst;
			if(dtEst != null)
			{
				if(dtEst.Rows.Count>0)
				{
					DataRow dr = dtEst.Rows[0];
					DateTime dRequestedPickupDT = (DateTime)dr["ActualPickupDT"];
					DateTime dEst_PickupDT;
					if(dr["EstDel_Date"] != DBNull.Value)
					{
						dEst_PickupDT = (DateTime)dr["EstDel_Date"];
						txtShipEstDlvryDt.Text = dEst_PickupDT.ToString("dd/MM/yyyy HH:mm");
					}
					else
					{
						dEst_PickupDT = DateTime.ParseExact(this.txtShipEstDlvryDt.Text.Trim(),"dd/MM/yyyy HH:mm",null);						
					}

					string sPrompt = dr["Prompt"].ToString();
					
					if(sPrompt == "0")
					{
						if(dRequestedPickupDT.Date != dEst_PickupDT.Date)
						{
							if(EstimatedDeliveryDateType != DeliveryDateType.Today)
							{
								ExceedOK();
								EstimatedDeliveryDateType = DeliveryDateType.Tomorrow;
								ExceedTimeState.Visible = false; 
								divMain.Visible = true;
							}
							else
							{
								lbl_ExceedTimeState.Text="The Delivery is being made after the cut off time for Sender's Postal code. To override the standard cut off time and schedule the delivery at the time requested press Cancel. To reschedule the delivery to the time proposed based on the cut off time, press OK.";
								EstimatedDeliveryDateType = DeliveryDateType.Tomorrow;							
								ExceedTimeState.Visible = true; 
								divMain.Visible = false;
							}

						}
						else
						{
							EstimatedDeliveryDateType = DeliveryDateType.Today;
							ExceedOK();
						}	
					}
					else if(sPrompt == "1")
					{
						lbl_ExceedTimeState.Text="Would the customer accept a surcharge for delivery on Saturday?";
						EstimatedDeliveryDateType = DeliveryDateType.Saturday;
						ExceedTimeState.Visible = true; 
						divMain.Visible = false;
					}
					else if(sPrompt == "2")
					{
						lbl_ExceedTimeState.Text="Would the customer accept a surcharge for delivery on Sunday?";
						EstimatedDeliveryDateType = DeliveryDateType.Sunday;
						ExceedTimeState.Visible = true; 
						divMain.Visible = false;
					}
					else if(sPrompt == "3")
					{
						lbl_ExceedTimeState.Text="Would the customer accept a surcharge for delivery on Public Holiday?";
						EstimatedDeliveryDateType = DeliveryDateType.Holiday;
						ExceedTimeState.Visible = true; 
						divMain.Visible = false;
					}
				}				
				else
				{
					Service service = new Service();
					service.Populate(appID, enterpriseID, txtShpSvcCode.Text.Trim());
					DateTime dtCommitTime = service.CommitTime;
					String strCommitTime = dtCommitTime.ToString("HH:mm");

					txtShipEstDlvryDt.Text = DateTime.Now.ToString("dd/MM/yyyy") + " " + strCommitTime;
					ExceedOK();
					EstimatedDeliveryDateType = DeliveryDateType.Today;
					ExceedTimeState.Visible = false; 
					divMain.Visible = true;
				}
			}
		}

		private void btnExceedOK_Click(object sender, System.EventArgs e)
		{
			ExceedOK();
			ExceedTimeState.Visible = false;
			divMain.Visible = true;		
		}

		private void btnExceedCancel_Click(object sender, System.EventArgs e)
		{
			DataTable dtEst = this.EstimatedDeliveryDateData;
			dtEst.Rows.RemoveAt(0);
			dtEst.AcceptChanges();			
			this.EstimatedDeliveryDateData = dtEst;
			EstimatedDeliveryDate();
		}


		private void ExceedOK()
		{
			DateTime TMPPickupDate = System.DateTime.ParseExact(txtShipPckUpTime.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			DateTime TMPEstDelDt = System.DateTime.ParseExact(txtShipEstDlvryDt.Text.Trim(),"dd/MM/yyyy HH:mm",null);

			

			//Get Service details
			Service service = new Service();
			service.Populate(appID, enterpriseID, txtShpSvcCode.Text.Trim());
			DateTime dtCommitTime = service.CommitTime;
			String strCommitTime = dtCommitTime.ToString("HH:mm");
			decimal iTransitDay = service.TransitDay;
			int iTransitHour = service.TransitHour;

			ViewState["TMPServiceDays"] = service.TransitDay;

			String strDt = "";
			String strDlvryDtTime = "";

			//IF Shipment Service is SAME DAY by X JAN 07 08
			if(this.txtShpSvcCode.Text.Substring(0,2)=="SD")
			{					
				//SAVE NOW & EXIT
				SaveUpdateRecord();
				ViewState["fromInsert"] = true;
			}
			//END if Shipment Service is SAME DAY
			//HC Return Task
			Zipcode zipCode = new Zipcode();
			zipCode.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
			String strStateCode = zipCode.StateCode;

			if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCode.Trim() != ""))
			{
				DataRow drEach = m_sdsDomesticShip.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
				string strConNo = "";
				string strBookingNo = "";

				if (txtBookingNo.Text.Trim() != "" )
					strBookingNo = txtBookingNo.Text.Trim();

				if (txtConsigNo.Text.Trim() != "" )
					strConNo = txtConsigNo.Text.Trim();

				object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCode,
					strConNo.Trim(), strBookingNo.Trim(), appID,enterpriseID);

				if((tmpReHCDateTime != null) && 
					(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
				}
				else
				{
					txtEstHCDt.Text = "";
				}
			}
			else
			{
				txtEstHCDt.Text = "";
			}
			//HC Return Task
			
			ViewState["TMPServiceDays"] = Convert.ToInt32(ViewState["TMPServiceDays"]) - 1;
			ViewState["TMPServiceDays"] = 0;
			if (Convert.ToInt32(ViewState["TMPServiceDays"]) == 0)
			{
				strDt = TMPEstDelDt.ToString("dd/MM/yyyy");
				strDlvryDtTime = strDt+" "+strCommitTime;
				//txtShipEstDlvryDt.Text = strDlvryDtTime;

				//HC Return Task
				Zipcode zipCodeTMP = new Zipcode();
				zipCodeTMP.Populate(appID,enterpriseID,txtRecipZip.Text.Trim());
				String strStateCodeTMP = zipCodeTMP.StateCode;

				if ((txtShipEstDlvryDt.Text.ToString().Trim() != "") && (strStateCode.Trim() != ""))
				{
					string strConNo = "";
					string strBookingNo = "";

					if (txtBookingNo.Text.Trim() != "" )
						strBookingNo = txtBookingNo.Text.Trim();
					if (txtConsigNo.Text.Trim() != "" )
						strConNo = txtConsigNo.Text.Trim();

					object tmpReHCDateTime = DomesticShipmentMgrDAL.calEstHCDateTime(System.DateTime.ParseExact(txtShipEstDlvryDt.Text,"dd/MM/yyyy HH:mm",null), strStateCodeTMP,
						strConNo.Trim(), strBookingNo.Trim(), appID,enterpriseID);

					if((tmpReHCDateTime != null) && 
						(!tmpReHCDateTime.GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						txtEstHCDt.Text = ((DateTime)tmpReHCDateTime).ToString("dd/MM/yyyy HH:mm");
					}
					else
					{
						txtEstHCDt.Text = "";
					}
				}
				else
				{
					txtEstHCDt.Text = "";
				}
				//HC Return Task

				//Both Transit Day and  Transit Hour is zero
				if((iTransitDay == 0) && (iTransitHour == 0))
				{
					//Get the Pickup date & Recipient's cut off time from the zipcode
					strDt = TMPPickupDate.ToString("dd/MM/yyyy");
					Zipcode zipcode = new Zipcode();
					zipcode.Populate(appID,enterpriseID, txtRecipZip.Text.Trim());
					DateTime dtCuttOff =  zipcode.CuttOffTime;
					strCommitTime = dtCuttOff.ToString("HH:mm");
					strDlvryDtTime = strDt+" "+strCommitTime;
				}
				else if((iTransitDay > 0) && (iTransitHour == 0) && (dtCommitTime.Hour == 0) && (dtCommitTime.Minute == 0))
				{
					//Add the day to the pickup request date & Get the pickup Date's time...
					strDt = TMPEstDelDt.ToString("dd/MM/yyyy");

					//Get the commit time from pickUp request Date time
					strCommitTime = TMPPickupDate.ToString("HH:mm");
					strDlvryDtTime = strDt+" "+strCommitTime;
				}

				SaveUpdateRecord();
				ViewState["fromInsert"] = true;
			}

			dgVAS.CurrentPageIndex = 0;
			BindVASGrid();

			// Added on 18-Oct-11
			// Insert into the shipment_pkg_box
			//int iBookingNo = 0;
			//if(txtBookingNo.Text.Length > 0)
			//	iBookingNo = Convert.ToInt32(txtBookingNo.Text);
			//else
			//	iBookingNo = 0;
			//DomesticShipmentMgrDAL.DomesticShipment_SavingPkgDetail(appID,enterpriseID,iBookingNo,txtConsigNo.Text.Trim());
			//********************

			//Added By Tom Jan 19, 10
			if(lblErrorMsg.Text == "Con# " + txtConsigNo.Text + " Saved Successfully")
				PrepareForNewInsert();
			//End Added By Tom Jan 19, 10
			ViewState["OperationsVAS"] = Operation.None;  //Jeab 09 Mar 11
		}

	}
}