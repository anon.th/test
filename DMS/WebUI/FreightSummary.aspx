<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<%@ Page language="c#" Codebehind="FreightSummary.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.FreightSummary" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Freight Summary On-forwarding</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="FreightSummary" method="post" runat="server">
			<asp:label style="Z-INDEX: 103; POSITION: absolute; TOP: 10px; LEFT: 20px" id="lblTitle" runat="server"
				Width="365px" Height="27px" CssClass="maintitleSize">Freight Summary On-forwarding</asp:label><asp:button style="Z-INDEX: 100; POSITION: absolute; TOP: 54px; LEFT: 19px" id="btnQuery" runat="server"
				Width="64px" Height="20" CssClass="queryButton" Text="Query"></asp:button><asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 80px; LEFT: 20px" id="lblErrorMessage"
				runat="server" Width="528px" Height="21px" CssClass="errorMsgColor">Place holder for err msg</asp:label>&nbsp;
			<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 54px; LEFT: 84px" id="btnGenerate"
				runat="server" Width="120px" Height="20" CssClass="queryButton" Text="Execute Query"></asp:button>
			<table style="Z-INDEX: 104; POSITION: absolute; WIDTH: 500px; TOP: 100px; LEFT: 16px" id="tblShipmentTracking"
				border="0" runat="server">
				<tr>
					<td style="WIDTH: 443px">
						<fieldset><legend><asp:label id="lblDates" runat="server" Width="56px" CssClass="tableHeadingFieldset" Font-Bold="True">Dates</asp:label></legend>
							<table>
								<tr>
									<td colSpan="4">
										<asp:radiobutton id="rbInvoicedDate" runat="server" Width="104px" Height="21px" CssClass="tableRadioButton"
											Text="Invoiced Date" AutoPostBack="True" GroupName="QueryDate" Checked="True"></asp:radiobutton>
										<asp:radiobutton id="rbManifestedDate" runat="server" Width="128px" Height="21px" CssClass="tableRadioButton"
											Text="Manifested Date" AutoPostBack="True" GroupName="QueryDate"></asp:radiobutton>
										<asp:radiobutton id="rbNoInvoice" runat="server" Width="96px" Height="21px" CssClass="tableRadioButton"
											Text="Not Invoiced" AutoPostBack="True" GroupName="QueryDate"></asp:radiobutton>
									</td>
								</tr>
								<tr>
									<td><asp:radiobutton id="rbMonth" runat="server" Width="73px" Height="21px" CssClass="tableRadioButton"
											Text="Month" AutoPostBack="True" GroupName="EnterDate" Checked="True"></asp:radiobutton></td>
									<td><asp:dropdownlist id="ddMonth" runat="server" Width="88px" Height="19px" CssClass="textField"></asp:dropdownlist></td>
									<td><asp:label id="Label5" runat="server" Width="30px" Height="22px" CssClass="tableLabel">Year</asp:label></td>
									<td><cc1:mstextbox id="txtYear" runat="server" Width="83" CssClass="textField" MaxLength="5" TextMaskType="msNumeric"
											NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></td>
								</tr>
								<tr>
									<td><asp:radiobutton id="rbPeriod" runat="server" Width="62px" CssClass="tableRadioButton" Text="Period"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton></td>
									<td><cc1:mstextbox id="txtPeriod" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></td>
									<td></td>
									<td><cc1:mstextbox style="Z-INDEX: 0" id="txtTo" runat="server" Width="83" CssClass="textField" MaxLength="10"
											TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></td>
								</tr>
								<TR>
									<TD><asp:radiobutton id="rbDate" runat="server" Width="74px" Height="22px" CssClass="tableRadioButton"
											Text="Date" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton></TD>
									<TD><cc1:mstextbox id="txtDate" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD></TD>
									<TD>
										<asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableHeadingFieldset" Width="160px"
											Font-Bold="True"></asp:label></TD>
								</TR>
							</table>
						</fieldset>
					</td>
					<td>
						<fieldset><legend><asp:label id="Label1" runat="server" Width="77px" CssClass="tableHeadingFieldset" Font-Bold="True">Payer Type</asp:label></legend>
							<table style="WIDTH: 296px; HEIGHT: 102px">
								<tr>
									<td vAlign="top"><asp:label id="Label6" runat="server" Width="88px" Height="22px" CssClass="tableLabel">Payer Type</asp:label></td>
									<td><asp:listbox id="lsbCustType" runat="server" Width="137px" Height="61px" SelectionMode="Multiple"></asp:listbox></td>
								</tr>
								<tr>
									<td><asp:label id="Label10" runat="server" Width="79px" Height="22px" CssClass="tableLabel">Payer Code</asp:label></td>
									<td style="WIDTH: 360px" class="tableLabel"><cc1:mstextbox id="txtPayerCode" runat="server" Width="139px" CssClass="textField" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnPayerCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></td>
								</tr>
							</table>
						</fieldset>
					</td>
				</tr>
                <tr>
                    <td>
                        <fieldset>
                            <legend><asp:label id="lblShipmentType" runat="server" Width="100px" CssClass="tableHeadingFieldset" Font-Bold="True">Shipment Type</asp:label></legend>
                            <asp:radiobutton id="rdoShipmentTypeDomestic" runat="server" Width="104px" Height="25px" CssClass="tableRadioButton" Text="Domestic" AutoPostBack="True" GroupName="ShipmentType"></asp:radiobutton>
						    <asp:radiobutton id="rdoShipmentTypeInternational" runat="server" Width="128px" Height="25px" CssClass="tableRadioButton" Text="Inbound" AutoPostBack="True" GroupName="ShipmentType"></asp:radiobutton>
						    <asp:radiobutton id="rdoShipmentTypeBoth" runat="server" Width="96px" Height="25px" CssClass="tableRadioButton" Text="Both" AutoPostBack="True" GroupName="ShipmentType" Checked="True"></asp:radiobutton>
                        </fieldset>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <div style="visibility:hidden">
                        <fieldset>
                            <legend><asp:label id="Label3" runat="server" Width="100px" CssClass="tableHeadingFieldset" Font-Bold="True">Report Name</asp:label></legend>
                            <asp:radiobutton id="rdoReportNameFreightSummary" runat="server" Width="150px" Height="25px" CssClass="tableRadioButton" Text="Freight Summary" AutoPostBack="True" GroupName="ReportName"  Checked="True"></asp:radiobutton>
						    <asp:radiobutton id="rdoReportNameFedEx" runat="server" Height="25px" CssClass="tableRadioButton" Text="TNT Agency Charges" AutoPostBack="True" GroupName="ReportName"></asp:radiobutton>
                        </fieldset>
                        </div>
                    </td>
                    <td></td>
                </tr>
			</table>
		</form>
	</body>
</HTML>
