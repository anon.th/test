<%@ Page language="c#" Codebehind="RtnToCustRpt.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.RtnToCustRpt" %>
<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentTrackingQuery</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="ShipmentTrackingQueryRpt" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 100; LEFT: 20px; POSITION: absolute; TOP: 40px" runat="server"
				Width="64px" CssClass="queryButton" Text="Query"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 20px; POSITION: absolute; TOP: 71px"
				runat="server" Width="700px" CssClass="errorMsgColor" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 101; LEFT: 84px; POSITION: absolute; TOP: 40px"
				runat="server" Width="123px" CssClass="queryButton" Text="Execute Query"></asp:button><asp:button id="btnCancelQry" style="Z-INDEX: 102; LEFT: 234px; POSITION: absolute; TOP: 40px"
				runat="server" CssClass="queryButton" Text="Cancel Query" Enabled="False" Visible="False"></asp:button>
			<TABLE id="tblInternal" style="Z-INDEX: 104; LEFT: 14px; WIDTH: 800px; POSITION: absolute; TOP: 84px; HEIGHT: 500px"
				width="800" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 468px; HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 456px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" Width="20px" CssClass="tableHeadingFieldset" Font-Bold="True">Dates</asp:label></legend>
							<TABLE id="tblDates" style="WIDTH: 360px; HEIGHT: 91px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;<asp:radiobutton id="rbStatusDate" runat="server" Width="94px" CssClass="tableRadioButton" Text="Status Date"
											Height="22px" Checked="True" GroupName="QueryByDate" AutoPostBack="True"></asp:radiobutton></TD>
									<td></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Width="73px" CssClass="tableRadioButton" Text="Month"
											Height="21px" Checked="True" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" Width="88px" CssClass="textField" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" Width="30px" CssClass="tableLabel" Height="22px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" Width="83" CssClass="textField" MaxLength="5" TextMaskType="msNumeric"
											NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
									<td></td>
								</TR>
								<TR>
									<td style="HEIGHT: 20px"></td>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Width="62px" CssClass="tableRadioButton" Text="Period"
											GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" Width="83" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="HEIGHT: 20px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Width="74px" CssClass="tableRadioButton" Text="Date"
											Height="22px" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 128px"><legend><asp:label id="lblPayerType" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Payer Type</asp:label></legend>
							<TABLE id="tblPayerType" style="WIDTH: 300px; HEIGHT: 73px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<tr>
									<td></td>
									<td colSpan="2" height="1">&nbsp;
										<asp:label id="lblPayerCode" runat="server" Width="79px" CssClass="tableLabel" Height="22px">Payer Code</asp:label></td>
									<td><asp:textbox id="txtPayerCode" runat="server" Width="148px" CssClass="textField"></asp:textbox><asp:button id="btnPayerCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></td>
									<td></td>
								</tr>
								<TR>
									<td></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;</TD>
									<td></td>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" colSpan="2">
						<fieldset style="WIDTH: 792px; HEIGHT: 89px">
							<TABLE id="tblShipment" style="WIDTH: 680px; HEIGHT: 68px" cellSpacing="0" cellPadding="0"
								width="680" align="left" border="0" runat="server">
								<tr>
									<td style="HEIGHT: 4px"></td>
									<TD class="tableLabel" style="HEIGHT: 4px" colSpan="3">&nbsp;
										<asp:label id="lblStatusCode" runat="server" Width="89px" CssClass="tableLabel" Height="22px">Status Code</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtStatusCode" runat="server" Width="161px" CssClass="textField" AutoPostBack="True"
											MaxLength="10" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;
										<asp:button id="btnStatusCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<td style="HEIGHT: 4px"></td>
								</tr>
								<tr>
									<td style="HEIGHT: 30px"></td>
									<TD class="tableLabel" style="HEIGHT: 30px" colSpan="3">&nbsp;
										<asp:label id="lblConsignmentNo" runat="server" Width="110px" CssClass="tableLabel" Height="18px">Consignment No</asp:label>&nbsp;&nbsp;
										<asp:textbox id="txtConsignmentNo" runat="server" Width="161px" CssClass="textField" AutoPostBack="True"
											MaxLength="30"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
									<td style="HEIGHT: 30px"></td>
								</tr>
								<TR>
									<TD style="HEIGHT: 30px"><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT></TD>
									<TD class="tableLabel" style="HEIGHT: 30px" colSpan="3"><FONT face="Tahoma">&nbsp;&nbsp;</FONT>&nbsp;<FONT face="Tahoma">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</FONT><FONT face="Tahoma"></FONT>
									</TD>
									<TD style="HEIGHT: 30px"></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
			</TABLE>
			<asp:label id="Label1" style="Z-INDEX: 103; LEFT: 20px; POSITION: absolute; TOP: 4px" runat="server"
				Width="365px" CssClass="maintitleSize" Height="27px">Return to Customer Report</asp:label></TR></TABLE></TR></TABLE><input 
type=hidden value="<%=strScrollPosition%>" name=ScrollPosition>
		</form>
	</body>
</HTML>
