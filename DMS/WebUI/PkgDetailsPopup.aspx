<%@ Page language="c#" Codebehind="PkgDetailsPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.PackageDetails" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PackageDetails</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script>
			function SetParentWindowPosition()
			{
				if(document.PackageDetails.CloseWindowStatus.value != null && document.PackageDetails.CloseWindowStatus.value =="C")
				{
					window.close();
				}
				{				
					window.opener.scroll(0,window.opener.document.DomesticShipment.ScrollPosition.value);
				}
			}
			
		</script>
	</HEAD>
	<body onload="SetParentWindowPosition();" MS_POSITIONING="GridLayout">
		<form id="PackageDetails" method="post" runat="server">
			<asp:label id="lblErrorMsg" style="Z-INDEX: 101; POSITION: absolute; TOP: 5px; LEFT: 15px"
				runat="server" Height="19px" Width="566px" CssClass="errorMsgColor"></asp:label><input 
type=hidden value="<%=strCloseWindowStatus%>" name=CloseWindowStatus>
			<asp:panel id="panelPackageDetails" style="Z-INDEX: 102; POSITION: absolute; TOP: 26px; LEFT: 8px"
				runat="server" Height="318px" Width="890px">
				<FONT face="Tahoma">
					<TABLE style="WIDTH: 835px; HEIGHT: 272px" id="Table1" border="0" width="835" height="272"
						runat="server">
						<TR height="20%">
							<TD style="HEIGHT: 2.52%" width="25%">
								<DIV style="POSITION: relative; WIDTH: 840px; HEIGHT: 59px" ms_positioning="GridLayout">
									<asp:button style="Z-INDEX: 100; POSITION: absolute; TOP: 8px; LEFT: 141px" id="btnBind" runat="server"
										CssClass="queryButton" Width="66px" Enabled="False" CausesValidation="False" Text="Refresh"
										Visible="False"></asp:button>
									<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 3px" id="btnRetrievePkgs"
										runat="server" CssClass="queryButton" Width="135px" Enabled="False" CausesValidation="False"
										Text="Retrieve Packages" Visible="False"></asp:button>
									<asp:button style="Z-INDEX: 102; POSITION: absolute; TOP: 6px; LEFT: 570px" id="retriveSWBPackage"
										runat="server" CssClass="queryButton" Width="174px" Enabled="False" CausesValidation="False"
										Text="Retrieve SWB Packages" Visible="False"></asp:button>
									<asp:button style="Z-INDEX: 103; POSITION: absolute; TOP: 34px; LEFT: 570px" id="btnRetrievePRPkgs"
										runat="server" CssClass="queryButton" Width="174px" Enabled="False" CausesValidation="False"
										Text="Retrieve PR Packages"></asp:button>
									<asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 0px; LEFT: 0px" id="lblMainTitle"
										runat="server" CssClass="mainTitleSize" Width="550px" Visible="False">Old Package Detail :</asp:label></DIV>
							</TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 169px" width="25%">
								<asp:datagrid id="dgPkgInfo" runat="server" Width="724px" ItemStyle-Height="20" AutoGenerateColumns="False"
									OnPageIndexChanged="dgPkgInfo_PageChange" OnEditCommand="dgPkgInfo_Edit" OnCancelCommand="dgPkgInfo_Cancel"
									OnUpdateCommand="dgPkgInfo_Update" OnDeleteCommand="dgPkgInfo_Delete" OnSelectedIndexChanged="dgPkgInfo_SelectedIndexChanged">
									<ItemStyle Height="20px"></ItemStyle>
									<Columns>
										<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" HeaderText="D"
											CommandName="Delete">
											<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
										</asp:ButtonColumn>
										<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0  title='Update' &gt;"
											HeaderText="O" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel'&gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0  title='Edit' &gt;">
											<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
										</asp:EditCommandColumn>
										<asp:TemplateColumn HeaderText="MPS No">
											<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabel" ID="lblMPSNo" Text='<%#DataBinder.Eval(Container.DataItem,"mps_code")%>' Runat="server">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<cc1:mstextbox CssClass="gridTextBox" ID="txtMPSNo" Text='<%#DataBinder.Eval(Container.DataItem,"mps_code")%>' Runat="server" Enabled="True" NumberPrecision="5" NumberMinValue="1" NumberMaxValue="9999" TextMaskType="msNumeric" MaxLength="7" >
												</cc1:mstextbox>
												<asp:RequiredFieldValidator ID="validMpsNo" Runat="server" BorderWidth="0" Display="None" ErrorMessage="Mps No is required field"
													ControlToValidate="txtMPSNo"></asp:RequiredFieldValidator>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Length">
											<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabelNumber" ID="lblLength" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_length","{0:n}")%>' Runat="server" Enabled="True">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtLength" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_length")%>' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="6" NumberScale="2">
												</cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Breadth">
											<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabelNumber" ID="lblBreadth" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_breadth","{0:n}")%>' Runat="server" Enabled="True" >
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtBreadth" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_breadth")%>' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="6" NumberScale="2">
												</cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Height">
											<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabelNumber" ID="lblHeight" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_height","{0:n}")%>' Runat="server" Enabled="True">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtHeight" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_height")%>' Runat="server" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="6" NumberScale="2">
												</cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Volume">
											<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabelNumber" ID="lblVolume" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_volume","{0:#,##0}")%>' Runat="server">
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Weight">
											<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabelNumber" ID="lblWeight" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_wt","{0:n}")%>' Runat="server">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtWeight" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_wt")%>' Runat="server" Enabled="True" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="6" NumberScale="2">
												</cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Qty">
											<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabelNumber" ID="lblQty" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_qty")%>' Runat="server">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtQty" Text='<%#DataBinder.Eval(Container.DataItem,"pkg_qty")%>' Runat="server" Enabled="True" TextMaskType="msNumeric" MaxLength="4" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="4" NumberScale="0">
												</cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Total Act Wt">
											<HeaderStyle Wrap="False" Width="10%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabelNumber" ID="lblTot_act_Wt" Text='<%#DataBinder.Eval(Container.DataItem,"tot_act_wt","{0:n}")%>' Runat="server">
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Total R Wt">
											<HeaderStyle Wrap="False" Width="10%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabelNumber" ID="lblTotWt" Text='<%#DataBinder.Eval(Container.DataItem,"tot_wt","{0:n}")%>' Runat="server">
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Total R Dim Wt">
											<HeaderStyle Wrap="False" Width="14%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabelNumber" ID="lblDimWt" Text='<%#DataBinder.Eval(Container.DataItem,"tot_dim_wt","{0:n}")%>' Runat="server">
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn Visible="False" HeaderText="ChrgWt">
											<HeaderStyle Width="14%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabelNumber" Visible=False ID="lblChargeableWt" Text='<%#DataBinder.Eval(Container.DataItem,"chargeable_wt","{0:n}")%>' Runat="server">
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
								</asp:datagrid></TD>
						</TR>
						<TR>
							<TD width="25%" align="right"></TD>
							<TD width="25%"></TD>
							<TD width="25%"></TD>
							<TD width="25%"></TD>
						</TR>
						<TR>
							<TD width="25%" align="center">
								<asp:button id="btnOk" runat="server" CssClass="queryButton" Width="62px" CausesValidation="False"
									Text="OK"></asp:button>
								<asp:button id="btnCancel" runat="server" CssClass="queryButton" Width="62px" CausesValidation="False"
									Text="Cancel"></asp:button>
								<asp:button id="btnInsert" runat="server" CssClass="queryButton" Width="66px" CausesValidation="False"
									Text="Insert"></asp:button></TD>
						</TR>
					</TABLE>
				</FONT>
			</asp:panel><asp:panel id="panelPRPkgs" style="Z-INDEX: 103; POSITION: absolute; TOP: 348px; LEFT: 14px"
				runat="server" Height="435px" Width="745px" Visible="False">
				<P>
					<TABLE style="WIDTH: 639px; HEIGHT: 107px" id="Table3" border="0" cellSpacing="2" cellPadding="2"
						width="639">
						<TR>
							<TD style="WIDTH: 134px"></TD>
							<TD></TD>
							<TD style="WIDTH: 371px"></TD>
							<TD></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 134px">
								<asp:label id="Label6" runat="server" CssClass="tableLabel" Width="139px" Height="22px">Recipient Postal Code</asp:label></TD>
							<TD>
								<asp:textbox id="txtPostalCode" tabIndex="20" runat="server" CssClass="textField" Width="60px"
									MaxLength="10" ReadOnly="True"></asp:textbox></TD>
							<TD style="WIDTH: 371px"></TD>
							<TD>
								<asp:button id="btnRetrievePRPkgOK" runat="server" CssClass="queryButton" Width="75px" CausesValidation="False"
									Text="OK"></asp:button></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 134px">
								<asp:label id="Label7" runat="server" CssClass="tableLabel" Width="91px" Height="22px">Service Type</asp:label></TD>
							<TD>
								<cc1:mstextbox style="TEXT-TRANSFORM: uppercase" id="txtServiceType" tabIndex="47" runat="server"
									CssClass="textField" Width="60px" MaxLength="12" ReadOnly="True" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></TD>
							<TD style="WIDTH: 371px"></TD>
							<TD>
								<asp:button id="btnRetrievePRPkgCancel" runat="server" CssClass="queryButton" Width="75px" CausesValidation="False"
									Text="Cancel"></asp:button></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 134px"></TD>
							<TD></TD>
							<TD style="WIDTH: 371px"></TD>
							<TD><FONT face="Tahoma">
									<asp:button id="btnRetrievePRPkgShowAll" runat="server" CssClass="queryButton" Width="75px"
										CausesValidation="False" Text="Show All"></asp:button></FONT></TD>
						</TR>
					</TABLE>
				</P>
				<asp:datagrid id="dgRetrievePRPkg" runat="server" Width="635px" AutoGenerateColumns="False" PageSize="5"
					AllowPaging="True">
					<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
					<SelectedItemStyle Font-Size="10pt" CssClass="gridFieldSelected"></SelectedItemStyle>
					<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
					<HeaderStyle CssClass="gridHeading"></HeaderStyle>
					<Columns>
						<asp:TemplateColumn HeaderText="Select&lt;br&gt; One">
							<HeaderStyle HorizontalAlign="Center" Width="8%" VerticalAlign="Middle"></HeaderStyle>
							<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
							<ItemTemplate>
								<asp:CheckBox ID="chkSelect" Runat="server"></asp:CheckBox>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn DataField="consignmentSeqNo" HeaderText="Seq No.">
							<HeaderStyle HorizontalAlign="Left" Width="10%" VerticalAlign="Middle"></HeaderStyle>
							<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="ServiceType" HeaderText="Service Type">
							<HeaderStyle HorizontalAlign="Left" Width="15%" VerticalAlign="Middle"></HeaderStyle>
							<ItemStyle HorizontalAlign="Left" Width="3%" VerticalAlign="Middle"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="RecPostalCode" HeaderText="Recipient&lt;br&gt;Postal Code">
							<HeaderStyle Wrap="False" HorizontalAlign="Left" Width="15%" VerticalAlign="Middle"></HeaderStyle>
							<ItemStyle HorizontalAlign="Left" Width="10%" VerticalAlign="Middle"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="CODAmount" HeaderText="COD &lt;br&gt;Amount" DataFormatString="{0:n}">
							<HeaderStyle HorizontalAlign="Right" Width="13%" VerticalAlign="Middle"></HeaderStyle>
							<ItemStyle HorizontalAlign="Right" Width="12%" VerticalAlign="Middle"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="DeclaredValue" HeaderText="Declared&lt;br&gt; Value" DataFormatString="{0:n}">
							<HeaderStyle HorizontalAlign="Right" Width="13%" VerticalAlign="Middle"></HeaderStyle>
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="TotalPkg" HeaderText="No. of&lt;br&gt;Packages">
							<HeaderStyle HorizontalAlign="Right" Width="13%" VerticalAlign="Middle"></HeaderStyle>
							<ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="ActualWt" HeaderText="Total&lt;br&gt;Weight">
							<HeaderStyle HorizontalAlign="Right" Width="13%" VerticalAlign="Middle"></HeaderStyle>
							<ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
						</asp:BoundColumn>
					</Columns>
					<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
				</asp:datagrid>
			</asp:panel><asp:panel id="panelPRPkgsConfirm" style="Z-INDEX: 104; POSITION: absolute; TOP: 795px; LEFT: 174px"
				runat="server" Height="139px" Width="490px" Visible="False"><FONT face="Tahoma">
					<TABLE style="WIDTH: 445px; HEIGHT: 128px" id="Table2" border="0" cellSpacing="2" cellPadding="2"
						width="445">
						<TR>
							<TD style="HEIGHT: 14px"></TD>
							<TD style="HEIGHT: 14px">
								<asp:label id="lblBookDate" runat="server" CssClass="tableLabel" Width="91px" Height="22px"
									Font-Bold="True">Which data?</asp:label></TD>
							<TD style="WIDTH: 79px; HEIGHT: 14px">
								<asp:label id="Label4" runat="server" CssClass="tableLabel" Width="74px" Height="22px">In Cons</asp:label></TD>
							<TD style="WIDTH: 111px; HEIGHT: 14px">
								<asp:label id="Label5" runat="server" CssClass="tableLabel" Width="70px" Height="22px">In PR</asp:label></TD>
							<TD style="HEIGHT: 14px"></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 36px"></TD>
							<TD style="HEIGHT: 36px">
								<asp:label id="Label1" runat="server" CssClass="tableLabel" Width="91px" Height="22px">Service Type</asp:label></TD>
							<TD style="WIDTH: 79px; HEIGHT: 36px">
								<cc1:mstextbox style="TEXT-TRANSFORM: uppercase" id="txtConsServiceType" tabIndex="47" runat="server"
									CssClass="textField" Width="74px" MaxLength="12" ReadOnly="True" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></TD>
							<TD style="WIDTH: 111px; HEIGHT: 36px">
								<cc1:mstextbox style="TEXT-TRANSFORM: uppercase" id="txtPRServiceType" tabIndex="47" runat="server"
									CssClass="textField" Width="74px" MaxLength="12" ReadOnly="True" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></TD>
							<TD style="HEIGHT: 36px">
								<asp:CheckBox id="chkCopyServiceType" runat="server" Width="191px" Height="18px" AutoPostBack="True"></asp:CheckBox></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD>
								<asp:label id="Label2" runat="server" CssClass="tableLabel" Width="91px" Height="22px">COD Amount</asp:label></TD>
							<TD style="WIDTH: 79px">
								<asp:textbox id="txtConsCODAmount" runat="server" CssClass="textFieldRightAlign" Width="74px"
									ReadOnly="True"></asp:textbox></TD>
							<TD style="WIDTH: 111px">
								<asp:textbox id="txtPRCODAmount" runat="server" CssClass="textFieldRightAlign" Width="74px" ReadOnly="True"></asp:textbox></TD>
							<TD>
								<asp:CheckBox id="chkCopyCODAmount" runat="server" AutoPostBack="True"></asp:CheckBox></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD>
								<asp:label id="Label3" runat="server" CssClass="tableLabel" Width="91px" Height="22px">Declared Value</asp:label></TD>
							<TD style="WIDTH: 79px">
								<asp:textbox id="txtConsDeclaredValue" runat="server" CssClass="textFieldRightAlign" Width="74px"
									ReadOnly="True"></asp:textbox></TD>
							<TD style="WIDTH: 111px">
								<asp:textbox id="txtPRDeclaredValue" runat="server" CssClass="textFieldRightAlign" Width="74px"
									ReadOnly="True"></asp:textbox></TD>
							<TD>
								<asp:CheckBox id="chkCopyDeclaredValue" runat="server" AutoPostBack="True"></asp:CheckBox></TD>
						</TR>
						<TR>
							<TD style="HEIGHT: 13px"></TD>
							<TD style="HEIGHT: 13px"></TD>
							<TD style="WIDTH: 79px; HEIGHT: 13px"></TD>
							<TD style="WIDTH: 111px; HEIGHT: 13px"></TD>
							<TD style="HEIGHT: 13px"></TD>
						</TR>
						<TR>
							<TD></TD>
							<TD></TD>
							<TD style="WIDTH: 79px">
								<asp:button id="btnConfirmRetrievePRPkgsOK" runat="server" CssClass="queryButton" Width="62px"
									CausesValidation="False" Text="OK"></asp:button></TD>
							<TD style="WIDTH: 111px">
								<asp:button id="btnConfirmRetrievePRPkgsCancel" runat="server" CssClass="queryButton" Width="62px"
									CausesValidation="False" Text="Cancel"></asp:button></TD>
							<TD></TD>
						</TR>
					</TABLE>
				</FONT>
			</asp:panel></form>
	</body>
</HTML>
