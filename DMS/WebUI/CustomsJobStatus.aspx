<%@ Page Language="c#" CodeBehind="CustomsJobStatus.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsJobStatus" %>

<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Customs Job Status</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <style type="text/css">
            .loader {
                font-weight: bold;
                z-index: 100000001;
                position: fixed;
                width: 250px;
                height: 30px;
                margin-left: -60px;
                top: 50%;
                left: 50%;
                vertical-align: middle;
            }

            .overlay {
                position: fixed;
                z-index: 100000002;
                top: 0px;
                left: 0px;
                background-color: #FFFFFF;
                width: 100%;
                height: 100%;
                filter: Alpha(Opacity=70);
                opacity: 0.70;
                -moz-opacity: 0.70;
            }
    </style>

    <meta name="vs_snapToGrid" content="True">
    <meta name="vs_showGrid" content="False">
    <link rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
    <%--<link rel="stylesheet" type="text/css" href="css/waitMe.css" name="stylesheet">--%>
    <meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <!--#INCLUDE FILE="msFormValidations.inc"-->

    <script type="text/javascript" src="Scripts/settingScrollPosition.js"></script>
    <script type="text/javascript" src="Scripts/JScript_TextControl.js"></script>
    <%--<script type="text/javascript" src="Scripts/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="Scripts/waitMe.min.js"></script>--%>
    <script language="javascript" type="text/javascript">
        function funcDisBack() {
            history.go(+1);
        }

        function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }

        function RemoveBadMasterAWBNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^-_/_a-zA-Z0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteMasterAWBNumber(obj) {
            setTimeout(function () {
                RemoveBadMasterAWBNumber(obj.value, obj);
            }, 1); //or 4
        }

        function validate(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        function ValidateMasterAWBNumber(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[-_/_a-zA-Z0-9]/;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }

        //set slide on/off with effect
        //function pageLoad(sender, args) {
        //    smoothAnimation();
        //}

        //function smoothAnimation() {
        //    var collPanel = $find("CollapsiblePanelExtender1");
        //    collPanel._animation._fps = 45;
        //    collPanel._animation._duration = 0.90;
        //}

        //loading block-ui 
        //document.onreadystatechange = function () {
        //    if (document.readyState == "complete") {
        //        // document is ready. Do your stuff here
        //        var btnExecQry = document.getElementById("btnExecQry");
        //        //console.log(btnExecQry);
        //        if(btnExecQry == null)
        //        {
        //            document.getElementById("waitMe_ex").style.visibility = "hidden";
        //        }
        //    }
        //}

    </script>
    
</head>
<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
    onload="SetScrollPosition();" ms_positioning="GridLayout">
    <form id="CustomsJobStatus" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" >
        </asp:ScriptManager>
        <div style="z-index: 102; position: relative; width: 1175px; height: 1248px; top: 32px; left: 24px"
            id="divMain" ms_positioning="GridLayout" runat="server">

            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table style="z-index: 101; position: absolute; top: 8px; left: 0px" id="MainTable" border="0"
                        width="100%" runat="server">
                        <tbody>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblHeader" runat="server" Width="800px" Height="27px" CssClass="mainTitleSize">
							Customs Job Status</asp:Label></td>
                            </tr>
                            <tr>
                                <td style="height: 26px" valign="top" align="left">
                                    <asp:Button ID="btnQry" runat="server" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:Button><label style="width: 5px"></label>
                                    <asp:Button ID="btnExecQry" runat="server" CssClass="queryButton" Text="Execute Query" CausesValidation="False"></asp:Button>                                                                      
                                    <asp:Button ID="btnGenerateReport" runat="server" CssClass="queryButton" Text="Generate Report" CausesValidation="False"></asp:Button>
                                    <%--<input type="button" id="waitMe_ex" class="queryButton" value="Execute Query" causesvalidation="false" />--%>  
                                    <label style="width: 5px"></label>
                                    <label style="width: 5px"></label>
                                    <label style="width: 5px"></label>
                                    <label style="width: 5px"></label>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" align="left">
                                    <table style="z-index: 112; width: 1100px" id="Table1" border="0" width="730" runat="server">
                                        <tr width="100%">
                                            <td width="100%">
                                                <table border="0" cellspacing="1" cellpadding="1" width="900">
                                                    <tbody>
                                                        <tr>
                                                            <td style="height: 19px">
                                                                <asp:Label ID="lblError" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red"></asp:Label></td>
                                                            <td style="height: 19px"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <fieldset>
                                                                    <legend>
                                                                        <asp:Label ID="lblDates" runat="server" Width="20px" CssClass="tableHeadingFieldset" Font-Bold="True">Dates</asp:Label></legend>
                                                                    <table id="tblDates" border="0" cellspacing="0" cellpadding="0" align="left" runat="server">
                                                                        <tr>
                                                                            <td>&nbsp;
																			<%--<asp:radiobutton id="rbinfoReceived" tabIndex="1" runat="server" Width="102px" Height="22px" CssClass="tableRadioButton"
																				Text="Info Received" Checked="True" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbEntryCompiled" tabIndex="2" runat="server" Width="111px" Height="22px" CssClass="tableRadioButton"
																				Text="Entry Compiled" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbExpectedArrival" tabIndex="3" runat="server" Width="123px" Height="22px" CssClass="tableRadioButton"
																				Text="Expected Arrival" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbInvoiced" tabIndex="4" runat="server" Width="75px" Height="22px" CssClass="tableRadioButton"
																				Text="Invoiced" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbManifested" tabIndex="5" runat="server" Width="150px" Height="22px" CssClass="tableRadioButton"
																				Text="Manifest Data Entered" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbDelivered" tabIndex="6" runat="server" Width="75px" Height="22px" CssClass="tableRadioButton"
																				Text="Delivered" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbStationOutProceessed" tabIndex="5" runat="server" Width="150px" Height="22px"
																				CssClass="tableRadioButton" Text="Station Out Processed" GroupName="QueryByDate"></asp:radiobutton>--%>
                                                                                <asp:Label ID="Label18" runat="server" Width="73px" Height="10px" CssClass="tableLabel">Date Type</asp:Label>
                                                                                <%--<asp:dropdownlist runat="server" id="ddlDateType" Height="19px" Width="222px">
                                                                                <asp:listitem text="Info Received" value="0"></asp:listitem>
                                                                                <asp:listitem text="Entry Compiled" value="1"></asp:listitem>
                                                                                <asp:listitem text="Expected Arrival" value="2"></asp:listitem>
                                                                                <asp:listitem text="Invoiced" value="3"></asp:listitem>
                                                                                <asp:listitem text="Manifest Data Entered" value="4"></asp:listitem>
                                                                                <asp:listitem text="Delivered" value="5"></asp:listitem>
                                                                                <asp:listitem text="Station Out Processed" value="6"></asp:listitem>
                                                                            </asp:dropdownlist>--%>                                                                            
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <asp:DropDownList ID="ddDateType" TabIndex="1" runat="server" Width="230px" Height="20px" CssClass="textField" DataTextField="Text" DataValueField="Value"></asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;
																			<asp:RadioButton ID="rbMonth" TabIndex="2" runat="server" Width="73px" Height="21px" CssClass="tableRadioButton"
                                                                                Text="Month" Checked="True" GroupName="EnterDate" AutoPostBack="True"></asp:RadioButton>&nbsp;
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddMonth" TabIndex="3" runat="server" Width="96px" Height="19px" CssClass="textField"></asp:DropDownList>&nbsp;																			
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblYear" runat="server" Width="30px" Height="10px" CssClass="tableLabel">Year</asp:Label>&nbsp;&nbsp;                                                                                
                                                                            </td>
                                                                            <td>                                                                                
                                                                                <cc1:msTextBox ID="txtYear" TabIndex="4" runat="server" Width="83" CssClass="textField" NumberPrecision="4"
                                                                                NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:msTextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;
																			<asp:RadioButton ID="rbPeriod" TabIndex="5" runat="server" Width="73px" Height="21px" CssClass="tableRadioButton"
                                                                                Text="Period" GroupName="EnterDate" AutoPostBack="True"></asp:RadioButton>&nbsp;																			
                                                                            </td>
                                                                            <td>
                                                                                <cc1:msTextBox ID="txtPeriod" TabIndex="6" runat="server" Width="88" CssClass="textField" TextMaskType="msDate"
                                                                                    MaxLength="10" TextMaskString="99/99/9999"></cc1:msTextBox>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="Label19" runat="server" Width="30px" Height="10px" CssClass="tableLabel"></asp:Label>&nbsp;&nbsp;
                                                                            </td>
                                                                            <td>
                                                                                <cc1:msTextBox ID="txtTo" TabIndex="7"  runat="server" Width="83" CssClass="textField" TextMaskType="msDate"
                                                                                MaxLength="10" TextMaskString="99/99/9999"></cc1:msTextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;
																			<asp:RadioButton ID="rbDate" TabIndex="8" runat="server" Width="73px" Height="21px" CssClass="tableRadioButton"
                                                                                Text="Date  " GroupName="EnterDate" AutoPostBack="True"></asp:RadioButton>&nbsp;&nbsp;                                                                            
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <cc1:msTextBox ID="txtDate" TabIndex="9" runat="server" Width="88" CssClass="textField" TextMaskType="msDate"
                                                                                    MaxLength="10" TextMaskString="99/99/9999"></cc1:msTextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                                <table style="z-index: 0; left: 0px" id="TABLE4" border="0" width="887" runat="server">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="width: 222px">
                                                                                <asp:Label ID="Label4" runat="server" CssClass="tableLabel"> Job Entry Number:</asp:Label></td>
                                                                            <td style="width: 300px">
                                                                                <asp:TextBox Style="z-index: 0; text-transform: uppercase" ID="JobEntryNo" TabIndex="10" onkeypress="validate(event)"
                                                                                    onpaste="AfterPasteNonNumber(this)" runat="server" Width="150px" CssClass="textField" MaxLength="10"></asp:TextBox></td>
                                                                            <td>
                                                                                <asp:Label ID="Label5" runat="server" CssClass="tableLabel"> House AWB Number:</asp:Label></td>
                                                                            <td colspan="2"><font face="Tahoma">
                                                                                <asp:TextBox Style="z-index: 0; text-transform: uppercase" ID="HouseAWBNo" TabIndex="11" onkeypress="ValidateMasterAWBNumber(event)"
                                                                                    onpaste="AfterPasteMasterAWBNumber(this)" runat="server" Width="150px" CssClass="textField" MaxLength="30"></asp:TextBox></font></td>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label Style="z-index: 0" ID="Label6" runat="server" CssClass="tableLabel">MAWB Number:</asp:Label></td>
                                                                            <td colspan="4">
                                                                                <font face="Tahoma">
                                                                                    <asp:TextBox Style="z-index: 0; text-transform: uppercase" ID="MasterAWBNo" TabIndex="12" onkeypress="ValidateMasterAWBNumber(event)"
                                                                                        onpaste="AfterPasteMasterAWBNumber(this)" runat="server" Width="150px" CssClass="textField" MaxLength="30"></asp:TextBox>
                                                                                    <asp:CheckBox Style="z-index: 0; margin-left: 5px" ID="SearchMAWBinJobEntry" TabIndex="13" runat="server"
                                                                                            Width="188px" CssClass="tableLabel" Text="Search MAWB in Job Entry"></asp:CheckBox>
                                                                                </font>
                                                                            </td>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label Style="z-index: 0" ID="Label31" runat="server" CssClass="tableLabel"> Customer ID:</asp:Label></td>
                                                                            <td style="width: 235px">
                                                                                <asp:TextBox Style="z-index: 0; text-transform: uppercase" ID="CustomerID" TabIndex="14" runat="server"
                                                                                    Width="150px" CssClass="textField" MaxLength="100"></asp:TextBox><asp:Button Style="z-index: 0;text-align:left;" ID="btnCustomerPopup" TabIndex="15" runat="server" Width="23px"
                                                                                        Height="19px" CssClass="searchButton" Text="..." CausesValidation="False"></asp:Button></td>
                                                                            <td>
                                                                                <asp:Label Style="z-index: 0" ID="Label11" runat="server" CssClass="tableLabel">Customer Group:</asp:Label></td>
                                                                            <td><font face="Tahoma">
                                                                                <asp:DropDownList Style="z-index: 0" ID="CustomerGroup" TabIndex="16" runat="server" Width="150px"></asp:DropDownList></font></td>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr id="TrReportFormat" runat="server">
                                                                            <td><font face="Tahoma">
                                                                                <asp:Label Style="z-index: 0" ID="Label17" runat="server" CssClass="tableLabel"> Report Format: </asp:Label></font></td>
                                                                            <td colspan="4">
                                                                                <asp:DropDownList Style="z-index: 0" ID="ReportFormat" TabIndex="17" runat="server" Width="150px">
                                                                                    <asp:ListItem Value="CustomsJobArrivals" Selected="True">ARRIVALS</asp:ListItem>
                                                                                    <asp:ListItem Value="CustomsJobDetail1">JOB DETAILS 1</asp:ListItem>
                                                                                    <asp:ListItem Value="CustomsJobDetail2">JOB DETAILS 2</asp:ListItem>
                                                                                </asp:DropDownList></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td></td>
                                                        </tr>


                                                        <tr>
                                                            <td>
                                                                <fieldset>
                                                                    <asp:Panel ID="Panel2" runat="server" CssClass="collapsePanelHeader" Height="15px">
                                                                        <legend>
                                                                            <asp:ImageButton ID="CollapseImg" runat="server" ImageUrl="images/collapse-arrow.png" AlternateText="(Show Details...)" Width="15px" Height="15px" ImageAlign="Middle" />
                                                                            <asp:Label ID="Label44" runat="server" CssClass="tableHeadingFieldset" Font-Bold="True" Text="Advance Search">Advance Search</asp:Label>
                                                                            <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server" 
                                                                                TargetControlID="pnAdvanceSearch" 
                                                                                CollapseControlID="Panel2" ExpandControlID="Panel2"
                                                                                Collapsed="true" TextLabelID="Label44" CollapsedText="Advance Search" ExpandedText="Advance Search"
                                                                                CollapsedSize="0"
                                                                                ImageControlID="CollapseImg"
                                                                                ExpandedImage="images/expand-arrow.png"
                                                                                CollapsedImage="images/collapse-arrow.png"></cc1:CollapsiblePanelExtender>
                                                                        </legend>
                                                                    </asp:Panel>

                                                                    <asp:Panel ID="pnAdvanceSearch" runat="server">
                                                                        <table style="z-index: 0; left: 0px" id="TABLE3" border="0" width="860" runat="server">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="width: 220px">
                                                                                        <asp:Label Style="z-index: 0" ID="Label2" runat="server" CssClass="tableLabel">Bonded Warehouse:</asp:Label></td>
                                                                                    <td style="width: 300px">
                                                                                        <asp:DropDownList Style="z-index: 0" ID="BondedWarehouse" TabIndex="21" runat="server" Width="150px"></asp:DropDownList></td>
                                                                                    <td>
                                                                                        <asp:Label Style="z-index: 0" ID="Label3" runat="server" CssClass="tableLabel">Discharge Port:</asp:Label></td>
                                                                                    <td>
                                                                                        <asp:DropDownList Style="z-index: 0" ID="DischargePort" TabIndex="22" runat="server" Width="150px"></asp:DropDownList></td>
                                                                                    <td></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><font face="Tahoma">
                                                                                        <asp:Label Style="z-index: 0" ID="lblHCSupportedbyEnterprise" runat="server" CssClass="tableLabel"> Job Status: </asp:Label></font></td>
                                                                                    <td colspan="4">
                                                                                        <asp:DropDownList Style="z-index: 0" ID="JobStatus" TabIndex="23" runat="server" Width="150px"></asp:DropDownList>
                                                                                        <asp:CheckBox ID="SearchStatusNotAchieved" TabIndex="24" runat="server" Width="200px" CssClass="tableLabel " 
                                                                                            Style="z-index: 0; margin-left: 9px" Text="Search on Status not Achieved"></asp:CheckBox></td>
                                                                                    <td></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="height: 19px">
                                                                                        <asp:Label Style="z-index: 0" ID="Label7" runat="server" CssClass="tableLabel">Entry Type:</asp:Label></td>
                                                                                    <td style="height: 19px" colspan="4">
                                                                                        <asp:DropDownList Style="z-index: 0" ID="EntryType" TabIndex="25" runat="server" Width="150px"></asp:DropDownList></td>
                                                                                    <td style="height: 19px"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label Style="z-index: 0" ID="Label32" runat="server" CssClass="tableLabel"> Ship/Flight No:</asp:Label></td>
                                                                                    <td style="width: 235px"><font face="Tahoma">
                                                                                        <asp:TextBox Style="z-index: 0; text-transform: uppercase" ID="ShipFlightNo" TabIndex="26" runat="server"
                                                                                            Width="150px" CssClass="textField" MaxLength="40" Columns="30"></asp:TextBox></font></td>
                                                                                    <td><font face="Tahoma">
                                                                                        <asp:Label Style="z-index: 0" ID="Label8" runat="server" CssClass="tableLabel">Exporter Name:</asp:Label></font></td>
                                                                                    <td valign="middle" colspan="2"><font face="Tahoma">
                                                                                        <asp:TextBox Style="z-index: 0; text-transform: uppercase" ID="ExporterName" TabIndex="27" runat="server"
                                                                                            Width="150px" CssClass="textField" MaxLength="100"></asp:TextBox></font></td>
                                                                                    <td valign="middle"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label Style="z-index: 0" ID="Label33" runat="server" CssClass="tableLabel">Folio Number:</asp:Label></td>
                                                                                    <td style="width: 235px">
                                                                                        <asp:TextBox Style="z-index: 0; text-transform: uppercase" ID="FolioNumber" TabIndex="28" onkeypress="validate(event)"
                                                                                            onpaste="AfterPasteNonNumber(this)" runat="server" Width="150px" CssClass="textField" MaxLength="20"></asp:TextBox></td>
                                                                                    <td><font face="Tahoma">
                                                                                        <asp:Label Style="z-index: 0" ID="Label12" runat="server" CssClass="tableLabel">Cheque Req. No: </asp:Label></font></td>
                                                                                    <td valign="middle" colspan="2"><font face="Tahoma">
                                                                                        <asp:TextBox Style="z-index: 0; text-transform: uppercase" ID="ChequeReqNo" TabIndex="29" onkeypress="validate(event)"
                                                                                            onpaste="AfterPasteNonNumber(this)" runat="server" Width="150px" CssClass="textField" MaxLength="10"></asp:TextBox></font></td>
                                                                                    <td valign="middle"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label Style="z-index: 0" ID="Label34" runat="server" CssClass="tableLabel"> Customs Profile:</asp:Label></td>
                                                                                    <td style="width: 235px">
                                                                                        <asp:DropDownList Style="z-index: 0" ID="AssignedProfile" TabIndex="30" runat="server" Width="150px"></asp:DropDownList></td>
                                                                                    <td><font face="Tahoma">
                                                                                        <asp:Label Style="z-index: 0" ID="Label13" runat="server" CssClass="tableLabel"> Customs Receipt No: </asp:Label></font></td>
                                                                                    <td valign="middle" colspan="2"><font face="Tahoma">
                                                                                        <asp:TextBox Style="z-index: 0; text-transform: uppercase" ID="ReceiptNumber" TabIndex="31" runat="server"
                                                                                            Width="150px" CssClass="textField" MaxLength="40"></asp:TextBox></font></td>
                                                                                    <td valign="middle"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="height: 22px">
                                                                                        <asp:Label Style="z-index: 0" ID="Label9" runat="server" CssClass="tableLabel">Customs Lodgment (Declaration) No:</asp:Label></td>
                                                                                    <td colspan="4">
                                                                                        <asp:TextBox Style="z-index: 0; text-transform: uppercase" ID="CustomsDeclarationNo" TabIndex="32"
                                                                                            runat="server" Width="150px" CssClass="textField" MaxLength="20"></asp:TextBox>
                                                                                        <asp:CheckBox ID="DutyTaxNotOnChequeReq" TabIndex="33" runat="server" Width="404px" CssClass="tableLabel"
                                                                                               Style="z-index: 0; margin-left: 3px" Text="Show Customs Jobs only if Duty/Tax not on a Cheque Requisition"></asp:CheckBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label Style="z-index: 0" ID="Label14" runat="server" CssClass="tableLabel">Customs Job Compiler:</asp:Label></td>
                                                                                    <td style="width: 235px">
                                                                                        <asp:TextBox Style="z-index: 0; text-transform: uppercase" ID="Compiler" TabIndex="34" runat="server"
                                                                                            Width="150px" CssClass="textField" MaxLength="20"></asp:TextBox></td>
                                                                                    <td>
                                                                                        <asp:Label Style="z-index: 2" ID="Label15" runat="server" CssClass="tableLabel">Customs Job Registrar: </asp:Label></td>
                                                                                    <td valign="middle" colspan="2"><font face="Tahoma">
                                                                                        <asp:TextBox Style="z-index: 0; text-transform: uppercase" ID="Registrar" TabIndex="35" runat="server"
                                                                                            Width="150px" CssClass="textField" MaxLength="10"></asp:TextBox></font></td>
                                                                                    <td valign="middle"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><font face="Tahoma">
                                                                                        <asp:Label Style="z-index: 0" ID="Label16" runat="server" CssClass="tableLabel"> Destination DC: </asp:Label></font></td>
                                                                                    <td colspan="5">
                                                                                        <asp:DropDownList Style="z-index: 0" ID="DestinationDC" TabIndex="36" runat="server" Width="150px"></asp:DropDownList>
                                                                                        <asp:CheckBox ID="NotDestinationDC" TabIndex="37" runat="server" Width="300px" CssClass="tableLabel "
                                                                                            Style="z-index: 0; margin-left: 9px" Text="Show jobs only not going to this DC"></asp:CheckBox></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="5">
                                                                                        <asp:CheckBox Style="z-index: 1" ID="NoSOPorPOD" TabIndex="38" runat="server" Width="500px" CssClass="tableLabel "
                                                                                            Text="Show jobs only not delivered and not station-out-processed"></asp:CheckBox></td>
                                                                                    <td></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </asp:Panel>
                                                                </fieldset>

                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td><font face="Tahoma"></font><font face="Tahoma"></font>
                                                                <br>
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr id="trGrdJobStatus" class="gridHeading" runat="server">
                                                            <td><strong><font size="2">Customs Job Status</font></strong></td>
                                                        </tr>
                                                        <tr id="trSearchJobStatus" runat="server">
                                                            <td colspan="2">
                                                                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                                    <tr>
                                                                        <td width="20%">&nbsp;
																		<asp:Button Style="z-index: 0" ID="btnAddChequeReq" runat="server" CssClass="queryButton" Text="Add to Cheque Req"
                                                                            CausesValidation="False"></asp:Button></td>
                                                                        <td width="80%" align="left">
                                                                            <fieldset style="width: 423px">
                                                                                <legend>
                                                                                    <asp:Label ID="Label1" runat="server" Width="140px" CssClass="tableHeadingFieldset" Font-Bold="True">These Disbursements</asp:Label></legend>
                                                                                <table style="width: 656px; height: 40px" id="Table2" border="0" cellspacing="0" cellpadding="0"
                                                                                    align="left" runat="server">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:RadioButton Style="z-index: 0" ID="rdoDuty" runat="server" CssClass="tableRadioButton" Text="Duty+Tax+EPF"
                                                                                                Checked="True" GroupName="Disbursements"></asp:RadioButton></td>
                                                                                        <td>
                                                                                            <asp:RadioButton Style="z-index: 0" ID="rdoWharfage" runat="server" CssClass="tableRadioButton" Text="Wharfage &amp; Handling"
                                                                                                GroupName="Disbursements"></asp:RadioButton></td>
                                                                                        <td>
                                                                                            <asp:RadioButton Style="z-index: 0" ID="rdoFreight" runat="server" CssClass="tableRadioButton" Text="Freight Collect"
                                                                                                GroupName="Disbursements"></asp:RadioButton></td>
                                                                                        <td>
                                                                                            <asp:RadioButton Style="z-index: 0" ID="rdoDO" runat="server" CssClass="tableRadioButton" Text="DO Fee"
                                                                                                GroupName="Disbursements"></asp:RadioButton></td>
                                                                                        <td>
                                                                                            <asp:RadioButton Style="z-index: 0" ID="rdoStorage" runat="server" CssClass="tableRadioButton" Text="Storage"
                                                                                                GroupName="Disbursements"></asp:RadioButton></td>
                                                                                        <td>
                                                                                            <asp:RadioButton Style="z-index: 0" ID="rdoOther" runat="server" CssClass="tableRadioButton" Text="Other"
                                                                                                GroupName="Disbursements"></asp:RadioButton></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </fieldset>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" width="100%">
                                                                            <asp:Label Style="z-index: 0" ID="lblErrorAddChequeReq" runat="server" Font-Size="X-Small"
                                                                                Font-Bold="True" ForeColor="Red"></asp:Label></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:DataGrid Style="z-index: 0" ID="grdJobStatus" runat="server" Width="1150px" FooterStyle-CssClass="gridHeading"
                                                                    BorderWidth="0px" CellSpacing="1" CellPadding="0" HeaderStyle-CssClass="gridHeading" ItemStyle-CssClass="gridField"
                                                                    AlternatingItemStyle-CssClass="gridField" DataKeyField="JobEntryNo" AutoGenerateColumns="False" AllowPaging="True"
                                                                    PageSize="6">
                                                                    <FooterStyle CssClass="gridHeading"></FooterStyle>
                                                                    <AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
                                                                    <ItemStyle CssClass="gridField"></ItemStyle>
                                                                    <HeaderStyle CssClass="gridHeading"></HeaderStyle>
                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderText="Job Number">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkJobEntryNo" runat="server" Visible="True" CausesValidation="false"
                                                                                    Text='<%# DataBinder.Eval(Container.DataItem,"JobEntryNo") %>'
                                                                                    CommandName="EntryCompilingDate">
                                                                                </asp:LinkButton>
                                                                                <asp:Label ID="lblJobEntryNo" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"JobEntryNo") %>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Registrar / Compiler">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <%# DataBinder.Eval(Container.DataItem,"RegistrarCompiler") %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Customer ID / Name">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" Width="400px"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <%# DataBinder.Eval(Container.DataItem,"CustomerName") %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="House AWB Number">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkHouseAWBNumber" runat="server" Visible="True" CausesValidation="false" CommandName="HouseAWBNumber" Text='<%#DataBinder.Eval(Container.DataItem,"HouseAWBNumber")%>'>
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Created Date">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <%# DataBinder.Eval(Container.DataItem,"CreatedDate","{0:dd/MM/yyyy HH:mm}") %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="MAWB Number">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <%# DataBinder.Eval(Container.DataItem,"MasterAWBNumber") %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Entry Compiling&lt;br/&gt;Date">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblEntryCompilingDate" runat="server" Text=''></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Freight Collect&lt;br/&gt;Entry Date">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkFreightCollectDate" runat="server" Visible="True" CausesValidation="false"
                                                                                    CommandName="FreightCollectDate"></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Invoice Printed Date">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkInvoicedDate" runat="server" Visible="True" CausesValidation="false" CommandName="InvoicedDate"></asp:LinkButton>
                                                                                <asp:Label ID="lblInvoicedDate" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem,"InvoiceStatus") %>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Invoice Received&lt;br/&gt;by Ops">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <%# DataBinder.Eval(Container.DataItem,"InvoiceReceivedDate","{0:dd/MM/yyyy HH:mm}") %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Manifest Data&lt;br/&gt;Entry Date">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lbkManifestDataEntryDate" runat="server" Visible="True" CausesValidation="false" CommandName="ManifestDataEntryDate" Text='<%# DataBinder.Eval(Container.DataItem,"ManifestDataEntryDate","{0:dd/MM/yyyy HH:mm}") %>'>
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Cheque Requisition&lt;br/&gt;Date">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblChequeRequisitionDate" runat="server"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Duty Paid Date">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDutyPaidDate" runat="server"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Delivery Date">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" Width="100px"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <%# DataBinder.Eval(Container.DataItem,"DeliveryDate","{0:dd/MM/yyyy HH:mm}") %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                    </Columns>
                                                                    <PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
                                                                </asp:DataGrid></td>
                                                        </tr>
                                                        <tr id="trSearchJobStatusResults" class="gridHeading" runat="server">
                                                            <td><strong><font size="2">Search Results</font></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:DataGrid Style="z-index: 0" ID="grdSearchJobStatus" TabIndex="42" runat="server" Width="1000px"
                                                                    FooterStyle-CssClass="gridHeading" BorderWidth="0px" CellSpacing="1" CellPadding="0" HeaderStyle-CssClass="gridHeading"
                                                                    ItemStyle-CssClass="gridField" AlternatingItemStyle-CssClass="gridField" AutoGenerateColumns="False" AllowPaging="True"
                                                                    PageSize="6">
                                                                    <FooterStyle CssClass="gridHeading"></FooterStyle>
                                                                    <AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
                                                                    <ItemStyle CssClass="gridField"></ItemStyle>
                                                                    <HeaderStyle CssClass="gridHeading"></HeaderStyle>
                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderText="Select">
                                                                            <HeaderStyle HorizontalAlign="Center" Width="7%"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox runat="server" ID="cbkConsignment_no" CrsID='<%#DataBinder.Eval(Container.DataItem,"Invoice_No")%>' Checked='<%# DataBinder.Eval(Container.DataItem,"Check").ToString().ToLower().Equals("true") %>'></asp:CheckBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Invoice No.">
                                                                            <HeaderStyle HorizontalAlign="Center" Width="8%"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label10" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Invoice_No") %>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Client">
                                                                            <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <%# DataBinder.Eval(Container.DataItem,"CustomerName") %>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Duty + Tax">
                                                                            <HeaderStyle HorizontalAlign="Center" Width="8%"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDutyTax" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"DutyTax", "{0:#,##0.00}") %>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="EPF">
                                                                            <HeaderStyle HorizontalAlign="Center" Width="7%"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblEPF" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"EPF", "{0:#,##0.00}") %>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Wharfage & <br/>Handling">
                                                                            <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblWharfageHandling" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"WharfageHandling", "{0:#,##0.00}") %>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Freight&lt;br/&gt;Collect Entry">
                                                                            <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFreightCollect" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"FreightCollect", "{0:#,##0.00}") %>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Delivery&lt;br/&gt;Order Fee">
                                                                            <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDeliveryOrderFee" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"DeliveryOrderFee", "{0:#,##0.00}") %>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Storage">
                                                                            <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblStorage" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Storage", "{0:#,##0.00}") %>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="Other">
                                                                            <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblOther" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Other", "{0:#,##0.00}") %>'>
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                    </Columns>
                                                                    <PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
                                                                </asp:DataGrid></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div></div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="udProgress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                <ProgressTemplate>
                    <iframe frameborder="0" src="about:blank" class="overlay"></iframe>
                    <asp:Panel ID="LoadingPanel" CssClass="loader" runat="server">
                        <asp:Image ID="Image1" runat="server" ImageAlign="Middle" ImageUrl="~/images/loader1.gif" Width="128px" Height="15px" />
                    </asp:Panel>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        <input value="<%=strScrollPosition%>" type="hidden" name="ScrollPosition">
        <input type="hidden" name="hdnRefresh">        
        <%--<script type="text/javascript">
            //loading ui - waitme js (work on IE9 or above and requried jQuery3.4.1)
            $(function () {

                var current_effect = 'bounce';

                $('#waitMe_ex').click(function () {
                    run_waitMe($('#CustomsJobStatus'), 1, current_effect);
                });

                $('#waitMe_ex_close').click(function () {
                    $('#CustomsJobStatus').waitMe('hide');
                });
            });

            function run_waitMe(el, num, effect) {
                // debug msg...
                //console.log('**start**');
                //console.log(el);
                //console.log(effect);
                //console.log('**end**');
                //text = 'Please wait...';

                text = 'Please wait...';
                fontSize = '';
                switch (num) {
                    case 1:
                        maxSize = '';
                        textPos = 'vertical';
                        break;
                    case 2:
                        text = '';
                        maxSize = 30;
                        textPos = 'vertical';
                        break;
                    case 3:
                        maxSize = 30;
                        textPos = 'horizontal';
                        fontSize = '18px';
                        break;
                }
                el.waitMe({
                    effect: effect,
                    text: text,
                    bg: 'rgba(255,255,255,0.7)',
                    color: '#000',
                    maxSize: maxSize,
                    waitTime: -1,
                    source: 'img.svg',
                    textPos: textPos,
                    fontSize: fontSize,
                    onClose: function (el) { }
                });

                document.getElementById("<%=btnExecQry.ClientID %>").click();
            }
        </script>--%>


    </form>
</body>
</html>
