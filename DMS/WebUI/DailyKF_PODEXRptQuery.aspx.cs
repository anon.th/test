using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;
using Cambro.Web.DbCombo;
using com.common.RBAC;
using com.ties.classes;
using System.Text;
using com.common.DAL;

namespace com.ties
{
	/// <summary>
	/// Summary description for RevenueBySalesIDRptQuery.
	/// </summary>
	public class DailyKF_PODEXRptQuery : BasePage
	{
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Calendar Calendar1;
		protected System.Web.UI.WebControls.Label Label1;

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.ID = "DailyKF_PODEXRptQuery";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		String m_strAppID=null;
		String m_strEnterpriseID=null;
		String m_strCulture=null;
		String m_strUserID =null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			
			com.common.classes.User user = RBACManager.GetUser(m_strAppID, m_strEnterpriseID, (String)Session["userID"]);
			ViewState["usertype"] = user.UserType;
			ViewState["PayerID"] = user.PayerID;
			m_strUserID = user.UserID;

			if(!Page.IsPostBack)
			{
				DefaultScreen();
				Session["toRefresh"]=false;
			}
		}


		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			String strCurDate = "";
			strCurDate=Calendar1.SelectedDate.AddDays(-1).ToString("dd/MM/yyyy");
			
			int iRowsAffected = InsertTempTable(m_strAppID , m_strEnterpriseID,m_strUserID,strCurDate);
			
//			bool icheck =  ValidateValues();
//			if(icheck == false)
//			{
				String strModuleID = Request.Params["MODID"];
				DataSet dsShipment = GetShipmentQueryData();

				String strUrl = null;
				strUrl = "ReportViewer.aspx";
								
				Session["FORMID"] = "DailyKFACTIONLIST";
								
				Session["SESSION_DS1"] = dsShipment;
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindowReport.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
//			}
		}


		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}


		#region "Page Functions"

		private DataTable CreateEmptyDataTable()
		{
			DataTable dtShipment = new DataTable();

			dtShipment.Columns.Add(new DataColumn("userid", typeof(string)));
			//			dtShipment.Columns.Add(new DataColumn("province", typeof(string)));
			//			dtShipment.Columns.Add(new DataColumn("tot_cons", typeof(int)));
			//			dtShipment.Columns.Add(new DataColumn("delay_cons", typeof(int)));
			//			dtShipment.Columns.Add(new DataColumn("percentfail", typeof(decimal)));
			dtShipment.Columns.Add(new DataColumn("rptDate", typeof(string)));

			return dtShipment;
		}
		
		private DataSet GetShipmentQueryData()
		{
			DataSet dsShipment = new DataSet();
			DataTable dtShipment = CreateEmptyDataTable();
			DataRow dr = dtShipment.NewRow(); 
			
			dr["userid"] = m_strUserID.ToString();
			dr["rptDate"] = Calendar1.SelectedDate.AddDays(-1).ToString("dd/MM/yyyy");
						
			dtShipment.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipment);
			return dsShipment;
		}

		private void DefaultScreen()
		{
			Calendar1.SelectedDate= DateTime.Now;
			lblErrorMessage.Text = "";
		}

	
		private int ValidateData()
		{
//			if (txtYear.Text != "" )
//			{
//				if (Convert.ToInt32(txtYear.Text) < 2000 || Convert.ToInt32(txtYear.Text) > 2099)
//				{
//					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
//					return -1;
//				}
//			}
			return 1;
		}
		
	
		private void OpenWindowpage(String strUrl, String strFeatures)
		{
			
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion

		public static DataSet GetKFPODEX(String strAppID, String strEnterpriseID, string strDate)
		{
//			String strDateFrom = strDate.ToString() + " 00:00";
//			String strDateTo = strDate.ToString() + " 23:59";

			DbConnection dbCon= null;
			DataSet dsKFpodex =null;
			IDbCommand dbcmd=null;
			DataSet dsKFpodexData =null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("DailyKF_PODEXRptQuery.aspx","GetKFPODEX.","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry.Append("select  distinct destination_station ");
			strQry.Append(", (select count(consignment_no) from v_Daily_KF_PODEX where destination_station = t1.destination_station ");
//			strQry.Append("and est_delivery_datetime >= '" + strDateFrom + "' ");
//			strQry.Append("and est_delivery_datetime <= '" + strDateTo + "' ) as tot_Cons");
			strQry.Append("and convert(varchar,est_delivery_datetime,103) = '" + strDate + "' ) as tot_Cons");
			strQry.Append(", (select count(consignment_no) from v_Daily_KF_PODEX where destination_station = t1.destination_station ");
			strQry.Append("and convert(varchar,est_delivery_datetime,103)  = '" + strDate + "' ");
			strQry.Append("and (exception_code <> '' and exception_code is not null )) as Delay_Cons ");
			strQry.Append("from v_Daily_KF_PODEX t1 with(nolock) ");
			strQry.Append("where destination_station  is not null and destination_station <> '' ");
			strQry.Append("and convert(varchar,est_delivery_datetime,103)  = '" + strDate + "' ");
			strQry.Append("order by destination_station ");
			
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsKFpodex = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DailyKF_PODEXRptQuery.aspx","GetKFPODEX","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			if(dsKFpodex.Tables[0].Rows.Count > 0)
			{
				dsKFpodexData = GetEmptyKFpodexDS();
				int i = 0;
				for(i = 0;i<dsKFpodex.Tables[0].Rows.Count;i++)
				{	
					DataRow drKFpodex = dsKFpodex.Tables[0].Rows[i];									
					DataRow drEach = dsKFpodexData.Tables[0].NewRow();
					drEach["destination_station"] = drKFpodex["destination_station"];		
					drEach["tot_Cons"] = drKFpodex["tot_Cons"];
					drEach["Delay_Cons"] = drKFpodex["Delay_Cons"];

					decimal decPercent =0;
					if( (drEach["tot_Cons"].ToString()  != "0") &&  (drEach["Delay_Cons"].ToString() != "0")  )
					{
						decPercent = Convert.ToDecimal(drEach["Delay_Cons"])/Convert.ToDecimal(drEach["tot_Cons"]);
					}

					drEach["PercentFail"] = decPercent;
					
					dsKFpodexData.Tables[0].Rows.Add(drEach);
				}
			}

			return dsKFpodexData;
		}


		public static DataSet GetEmptyKFpodexDS()
		{
			DataTable dtKFpodex = new DataTable("KFpodex");
			dtKFpodex.Columns.Add(new DataColumn("destination_station", typeof(String)));
			dtKFpodex.Columns.Add(new DataColumn("tot_Cons",typeof(string)));
			dtKFpodex.Columns.Add(new DataColumn("Delay_Cons",typeof(string)));
			dtKFpodex.Columns.Add(new DataColumn("PercentFail",typeof(string)));			
			DataSet dsKFpodexDT = new DataSet();
			
			DataColumn[] PrimKey = new DataColumn[1];
			PrimKey[0] = dtKFpodex.Columns["destination_station"];
			dtKFpodex.PrimaryKey = PrimKey;

			dsKFpodexDT.Tables.Add(dtKFpodex);

			return  dsKFpodexDT;
		}


		public static int InsertTempTable(String strAppID , String strEnterpriseID ,String strUser , String strDate )
		{
			int iRowsAffected = 0;
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;

			DataSet dsCntPODEX = GetKFPODEX(strAppID,strEnterpriseID,strDate);
					
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("module1","DailyKF_PODEXRptQuery.aspx","btnExecuteQuery_Click()","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("module1","DailyKF_PODEXRptQuery.aspx","btnExecuteQuery_Click()","ERR004","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("module1","DailyKF_PODEXRptQuery.aspx","btnExecuteQuery_Click()","ERR005","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("module1","DailyKF_PODEXRptQuery.aspx","btnExecuteQuery_Click()","ERR006","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("module1","DailyKF_PODEXRptQuery.aspx","btnExecuteQuery_Click()","ERR007","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
					try
					{
						dbCmd = dbCon.CreateCommand("", CommandType.Text);
						dbCmd.Connection = conApp;
						dbCmd.Transaction = transactionApp;

						//Delete  data in Temp table
						StringBuilder strBuilder = new StringBuilder();			
						strBuilder.Append("delete from TMP_KFPODEX ");
						strBuilder.Append("where userid = '");
						strBuilder.Append(strUser + "'");			

						dbCmd.CommandText = strBuilder.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

						if(dsCntPODEX != null)
						{					
							if(dsCntPODEX.Tables[0].Rows.Count > 0)
							{			
								//Insert  data	into Temp table			
								int i = 0;		
								for(i = 0;i<dsCntPODEX.Tables[0].Rows.Count;i++)
								{	
									DataRow drKFpodex = dsCntPODEX.Tables[0].Rows[i];	

									strBuild = new StringBuilder();		
									strBuild.Append("insert into TMP_KFPODEX(userid ,province,tot_cons,delay_cons,percentfail) ");
									strBuild.Append("values( '" + strUser + "' ");
									strBuild.Append(",'"+ drKFpodex["destination_station"]+ "' ");			
									strBuild.Append(","+ drKFpodex["tot_cons"]+ " ");		
									strBuild.Append(","+ drKFpodex["delay_cons"]+ " ");		
									strBuild.Append(","+ drKFpodex["PercentFail"]+ ") ");		

									dbCmd.CommandText = strBuild.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
									Logger.LogDebugInfo("module1","DailyKF_PODEXRptQuery.aspx","InsertTempTable","INF008",iRowsAffected + " rows inserted into shipment_tracking table");
								}
							}	
						}

						transactionApp.Commit();
					} // end try
					catch(ApplicationException appException)
					{
						if(conApp != null)
						{
							conApp.Close();
						}
						Logger.LogTraceError("module1","DailyKF_PODEXRptQuery.aspx","btnExecuteQuery_Click()","ERR005","Error opening connection : "+ appException.Message.ToString());
						throw new ApplicationException("Error opening database connection ",appException);
					}
					catch(Exception exception)
					{
						if(conApp != null)
						{
							conApp.Close();
						}
						Logger.LogTraceError("module1","DailyKF_PODEXRptQuery.aspx","btnExecuteQuery_Click()","ERR006","Error opening connection : "+ exception.Message.ToString());
						throw new ApplicationException("Error opening database connection ",exception);
					}
			return iRowsAffected;
		}

		

	}		
}