using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using com.common.DAL;  
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes; 
using System.Text ;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared ;  
using TIES;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for PickupRequestBooking.
	/// </summary>
	public class PickupRequestBooking : BasePage
	{
		protected System.Web.UI.WebControls.TextBox Txt_CustomerId;
		protected System.Web.UI.WebControls.Label Lb_Name;
		protected System.Web.UI.WebControls.DropDownList Drp_CustomerType;
		protected System.Web.UI.WebControls.Label Lb_CutomerType;
		protected System.Web.UI.WebControls.TextBox TextBox17;
		protected System.Web.UI.WebControls.Label Lb_ContactPerson;
		protected System.Web.UI.WebControls.Button ii;
		private decimal bkgno;
		private int Trow;
		protected System.Web.UI.WebControls.TextBox txtCustAdd2;
		protected System.Web.UI.WebControls.Label Lb_CustomerType;
		protected System.Web.UI.WebControls.Label Lb_Fax;
		protected System.Web.UI.WebControls.Button Test;
		protected System.Web.UI.WebControls.RadioButton Rd_FreightCollect;
		protected System.Web.UI.WebControls.Label Lb_TelephoneNo;
		protected com.common.util.msTextBox txtCustID;
		protected System.Web.UI.WebControls.TextBox txtCustName;
		protected System.Web.UI.WebControls.RadioButton Rd_FrieghtCollect;
		SessionDS m_sdsPickupRequest = null;
		DataSet m_dsDispatch=null; 
		static int m_SetSize = 10;
		private int m_StartIndex=0;
		private DateTime strMyDateTime ;
		protected System.Web.UI.WebControls.TextBox txtCustAddr1;
		protected com.common.util.msTextBox txtCustZipCode;
		protected System.Web.UI.WebControls.TextBox txtSendName;
		protected System.Web.UI.WebControls.TextBox txtSendAddr1;
		string struserID=null;
		protected System.Web.UI.WebControls.Button btnSenZipcode;
		protected System.Web.UI.WebControls.DropDownList Drp_CustType;
		protected System.Web.UI.WebControls.TextBox txtSendCuttOffTime;
		
		protected System.Web.UI.WebControls.Button btn_Query;
		protected System.Web.UI.WebControls.Label Lb_Address;
		protected System.Web.UI.WebControls.Label Lb_ZipCode;
		protected System.Web.UI.WebControls.CheckBox Chk_SameCustomerInfo;
		protected System.Web.UI.WebControls.Label Lb_Sname;
		protected System.Web.UI.WebControls.Label Lb_SAddress;
		protected System.Web.UI.WebControls.Label Lb_SZipCode;
		protected System.Web.UI.WebControls.TextBox txtSendAddr2;
		protected com.common.util.msTextBox txtSendZip;
		protected System.Web.UI.WebControls.TextBox TxtHidden;
		protected System.Web.UI.WebControls.Button Btn_Insert;
		protected System.Web.UI.WebControls.Label LblHeading;
		protected System.Web.UI.WebControls.RegularExpressionValidator Regularpickupdate;
		protected System.Web.UI.WebControls.Label Lb_BookingNo;
		protected com.common.util.msTextBox Txt_BookingNo;
		protected com.common.util.msTextBox Txt_BookingDate;
		protected System.Web.UI.WebControls.Label Lb_StatusCode;
		protected System.Web.UI.WebControls.Button BtnStatusCode;
		protected System.Web.UI.WebControls.Label Lb_BookingType;
		protected System.Web.UI.WebControls.DropDownList Drp_BookingType;
		protected System.Web.UI.WebControls.Label Lb_ExceptionCode;
		protected System.Web.UI.WebControls.Button BtnExceptionCode;
		protected System.Web.UI.WebControls.Button Btn_ExcuteQuery;
		protected System.Web.UI.WebControls.Button Btn_Delete;
		protected System.Web.UI.WebControls.ValidationSummary PageValidSummary;
		protected System.Web.UI.WebControls.Button btnMovenext;
		protected System.Web.UI.WebControls.Button btnMovePrevious;
		protected System.Web.UI.WebControls.TextBox Txt_RecCnt;
		protected System.Web.UI.WebControls.Button btnMoveLast;
		protected System.Web.UI.WebControls.Button btnMovefirst;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.Button Btn_Save;
		protected System.Web.UI.WebControls.Label Lb_BookingDate;
		protected System.Web.UI.WebControls.TextBox Txt_StatusCode;
		protected System.Web.UI.WebControls.TextBox Txt_ExceptionCode;
		protected System.Web.UI.WebControls.RequiredFieldValidator VaidCustID;
		protected System.Web.UI.WebControls.RequiredFieldValidator ValidName;
		protected System.Web.UI.WebControls.RequiredFieldValidator ValidCustAdd;
		protected System.Web.UI.WebControls.RequiredFieldValidator ValidCustZipCode;
		protected System.Web.UI.WebControls.RequiredFieldValidator ValidSenderName;
		protected System.Web.UI.WebControls.RequiredFieldValidator VaildSenderAdd;
		protected System.Web.UI.WebControls.RequiredFieldValidator ValidSenderZipcode;
		protected System.Web.UI.WebControls.RequiredFieldValidator ValidCutofftime;
		protected System.Web.UI.WebControls.Label ErrorMsg;
		protected System.Web.UI.WebControls.Label lblCustomerhead;
		protected System.Web.UI.WebControls.Label lblSenderhead;
		protected System.Web.UI.WebControls.TextBox txtSendCity;
		protected System.Web.UI.WebControls.TextBox txtSendContPer;
		protected System.Web.UI.WebControls.TextBox txtSendTel;
		protected System.Web.UI.WebControls.TextBox txtSendFax;
		protected System.Web.UI.WebControls.TextBox txtSendState;
		protected System.Web.UI.WebControls.Button BtnCustomerID;
		protected System.Web.UI.WebControls.Label Lb_PaymentMode;
		protected System.Web.UI.WebControls.Label lblTelephone;
		protected System.Web.UI.WebControls.TextBox Txt_Telephone;
		protected System.Web.UI.WebControls.Label lblFax;
		protected System.Web.UI.WebControls.TextBox Txt_Fax;
		protected System.Web.UI.WebControls.TextBox Txt_Country;
		protected System.Web.UI.WebControls.TextBox Txt_StateCode;
		protected System.Web.UI.WebControls.Label Lb_Sfax;
		protected System.Web.UI.WebControls.Label LblSTelephone;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.RequiredFieldValidator ValidContactPerson;
		protected System.Web.UI.WebControls.RequiredFieldValidator ValidSenderTelephone;
		protected System.Web.UI.WebControls.CheckBox Chk_NewCustomer;
		protected System.Web.UI.WebControls.RadioButton rd_Credit;
		protected System.Web.UI.WebControls.Button btnCustZipcode;
		protected System.Web.UI.WebControls.RequiredFieldValidator ValidCustTelphone;
		protected System.Web.UI.WebControls.Button BtnSendID;
		protected System.Web.UI.WebControls.Label lblCutoffTime;
		protected System.Web.UI.WebControls.RequiredFieldValidator Requiredfieldvalidator1;
		protected System.Web.UI.WebControls.Label lblPkghead;
		protected System.Web.UI.WebControls.RequiredFieldValidator ValidPickupDatetime;
		protected System.Web.UI.WebControls.Label Lb_EstpickupDateTime;
		protected com.common.util.msTextBox Txt_Estpickupdatetime;
		protected System.Web.UI.WebControls.Label Lb_PackagePaymenttype;
		protected System.Web.UI.WebControls.RadioButton Rd_FrieghtPrepaid;
		protected System.Web.UI.WebControls.RadioButton Rd_FreightCollectd;
		protected System.Web.UI.WebControls.Label Lb_ActualPickupdatetime;
		protected com.common.util.msTextBox Txt_ActualPickupDateTime;
		protected System.Web.UI.WebControls.Label Lb_CashAmt;
		protected com.common.util.msTextBox Txt_CashAmount;
		protected System.Web.UI.WebControls.Label Lb_TotalPackages;
		protected System.Web.UI.WebControls.Label Lb_ActualWeight;
		protected com.common.util.msTextBox Txt_ActualWeight;
		protected System.Web.UI.WebControls.Button BtnShipmentDetails;
		protected System.Web.UI.WebControls.Label Lb_DimWeight;
		protected com.common.util.msTextBox Txt_DimWeight;
		protected System.Web.UI.WebControls.Label Lb_Remarks;
		protected System.Web.UI.WebControls.TextBox Txt_Remarks;
		protected System.Web.UI.WebControls.Label lblConfirmMsg;
		protected System.Web.UI.WebControls.Button btnToCancel;
		protected System.Web.UI.WebControls.Button btnNotToSave;
		protected System.Web.UI.WebControls.Button btnToSaveChanges;
		protected System.Web.UI.HtmlControls.HtmlGenericControl ConfirmPanel;
		protected System.Web.UI.WebControls.Label lblESA;
		protected System.Web.UI.WebControls.TextBox txtESA;
		protected System.Web.UI.WebControls.Label lblESASurcharge;
		protected System.Web.UI.WebControls.TextBox txtESASurcharge;
		protected System.Web.UI.WebControls.Label Lb_RouteCode;
		protected System.Web.UI.WebControls.TextBox txtRouteCode;
		protected System.Web.UI.WebControls.Button btnRouteCode;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnExceedCancel;
		protected System.Web.UI.WebControls.Button btnExceedOK;
		protected System.Web.UI.HtmlControls.HtmlGenericControl ExceedTimeState;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Label lblAddress;
		protected System.Web.UI.WebControls.Label lblPostalCode;
		protected System.Web.UI.WebControls.Label lblTele;
		protected System.Web.UI.WebControls.Label lblSenName;
		protected System.Web.UI.WebControls.Label lblSenAddress;
		protected System.Web.UI.WebControls.Label lblSenPostalCode;
		protected System.Web.UI.WebControls.Label lblSenCutOff;
		protected System.Web.UI.WebControls.Label lblSenContact;
		protected System.Web.UI.WebControls.Label lblSenTele;
		protected System.Web.UI.WebControls.Label lblPkgEst;
		string strMsg=null;
		protected System.Web.UI.WebControls.RadioButton Rd_Cash;
		protected System.Web.UI.WebControls.Button btnService;
		protected System.Web.UI.WebControls.Label Lb_RateCharge;
		protected com.common.util.msTextBox txtRatedCharge;
		protected System.Web.UI.HtmlControls.HtmlGenericControl fs1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl fs2;
		protected System.Web.UI.HtmlControls.HtmlGenericControl fs3;
		protected com.common.util.msTextBox Txt_Totpkg;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label lblCustID;
		protected System.Web.UI.WebControls.Label Lb_CustomerID;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label5;
		Zipcode zipCode = new Zipcode();
		String appID = null;
		String enterpriseID = null;
		protected ArrayList userRoleArray;

		private enum PickupDateType
		{
			Today,
			Tomorrow,
			Saturday,
			Sunday,
			Holiday
		}

		private PickupDateType EstimatedPickupDateType
		{
			get
			{
				if(ViewState["EstimatedPickupDateType"] ==null)
					return PickupDateType.Today;
				else
					return (PickupDateType)ViewState["EstimatedPickupDateType"];
			}
			set
			{
				ViewState["EstimatedPickupDateType"] = value;
			}
		}

		private DataTable EstimatedPickupDateData
		{
			get
			{
				if(ViewState["EstimatedPickupDate"] ==null)
					return null;
				else
					return (DataTable)ViewState["EstimatedPickupDate"];
			}
			set
			{
				ViewState["EstimatedPickupDate"] = value;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)		
		{
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			struserID = utility.GetUserID();

			Rd_FrieghtPrepaid.Checked = true; 
			Rd_FreightCollectd.Enabled = false;

			if(!IsPostBack )
			{
				PageValidSummary.HeaderText = "The following mandatory fields shown with '*'.";
				Btn_Delete.Enabled  = false;

				//Btn_Save.Enabled = false;
				Utility.EnableButton(ref Btn_Save, com.common.util.ButtonType.Save,false,m_moduleAccessRights);

				ViewState["isTextChanged"] = false;
				btnMovenext.Enabled =false;
				btnMovePrevious.Enabled = false;
				btnMovefirst.Enabled=false;
				btnMoveLast.Enabled =false; 
				//Chk_SpecialRates.Enabled = false;
				//				Rd_FrieghtPrepaid.Checked = false;
				ViewState["CurrentSetSize"]=0;
				ViewState["Crow"]=0;
				ViewState["isZipcode"]=false;
				//txtSendCuttOffTime.Enabled = false; 
				Txt_StatusCode.Enabled =false;
				Txt_ExceptionCode.Enabled =false;
				//				Txt_cashcollected.Enabled = false; 
				Txt_BookingDate.ReadOnly=false;
				//txtESA.Enabled = false;
				//txtESASurcharge.Enabled = false; 
				//Txt_Country.Enabled = false; 
				ViewState["PRBMode"]=ScreenMode.None;
				ViewState["PRBOperation"]=Operation.None;
				ViewState["isTextChanged"]=false;
				ViewState["MovePrevious"]=false;
				ViewState["MoveNext"]=false;
				ViewState["MoveLast"]=false;
				ViewState["MoveFirst"]=false;
				BtnExceptionCode.Enabled =false;
				BtnStatusCode.Enabled = false;
				//BtnShipmentDetails.Enabled =false;
				txtSendZip.Enabled = true;
				txtSendZip.AutoPostBack = true;
				Txt_Totpkg.Enabled= true;
				Txt_ActualWeight.Enabled =true;
				Txt_DimWeight.Enabled =true;
				ConfirmPanel.Visible = false; 
				Txt_Estpickupdatetime.Enabled=true;
				Txt_Estpickupdatetime.AutoPostBack=true;
				LoadCustomerTypeList();
				LoadBookingTypeList(); 
							
				Session["SESSION_DS1"] = m_sdsPickupRequest;
				Query();

				ViewState["IsAcceptExceedsTime"] = false;		//Phase2 - K04
				ExceedTimeState.Visible = false;				//Phase2 - K04

				setRequiredFields(false);
				enableInput(CheckRoleM());
				checkCash();
				
			}
			else
			{
				m_sdsPickupRequest = (SessionDS)Session["SESSION_DS1"];
			}
			PageValidSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
			if(ViewState["PRBOperation"].ToString() == Operation.Insert.ToString())
			{
				Txt_BookingDate.TextMaskString="99/99/9999 99:99";
				Txt_BookingDate.TextMaskType =  com.common.util.MaskType.msDateTime;
			}
					
			//Removed by CRTS 954 Turn off Locking from over Credit Limit

			if(txtCustID.Text.Trim().Length>0)
			{
				if(CustomerProfileDAL.Check_IsCreditStatusNotAvail(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text.Trim()))
				{
					this.Btn_Save.Enabled=false;
					this.btnToSaveChanges.Enabled=false;
				}
			}
			//End CRTS 954 Turn off Locking from over Credit Limit

		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//			
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btn_Query.Click += new System.EventHandler(this.btn_Query_Click);
			this.Btn_ExcuteQuery.Click += new System.EventHandler(this.Btn_ExcuteQuery_Click);
			this.Btn_Insert.Click += new System.EventHandler(this.Btn_Insert_Click);
			this.Btn_Save.Click += new System.EventHandler(this.Btn_Save_Click);
			this.Btn_Delete.Click += new System.EventHandler(this.Btn_Delete_Click);
			this.btnService.Click += new System.EventHandler(this.btnService_Click);
			this.btnMovefirst.Click += new System.EventHandler(this.btnMovefirst_Click);
			this.btnMovePrevious.Click += new System.EventHandler(this.btnMovePrevious_Click);
			this.Txt_RecCnt.TextChanged += new System.EventHandler(this.Txt_RecCnt_TextChanged);
			this.btnMovenext.Click += new System.EventHandler(this.btnMovenext_Click);
			this.btnMoveLast.Click += new System.EventHandler(this.btnMoveLast_Click);
			this.Txt_BookingNo.TextChanged += new System.EventHandler(this.Txt_BookingNo_TextChanged);
			this.Txt_BookingDate.TextChanged += new System.EventHandler(this.Txt_BookingDate_TextChanged);
			this.Txt_StatusCode.TextChanged += new System.EventHandler(this.Txt_StatusCode_TextChanged);
			//this.BtnStatusCode.Click += new System.EventHandler(this.BtnStatusCode_Click);
			this.Drp_BookingType.SelectedIndexChanged += new System.EventHandler(this.Drp_BookingType_SelectedIndexChanged);
			this.txtRouteCode.TextChanged += new System.EventHandler(this.txtRouteCode_TextChanged);
			this.btnRouteCode.Click += new System.EventHandler(this.btnRouteCode_Click);
			this.Txt_ExceptionCode.TextChanged += new System.EventHandler(this.Txt_ExceptionCode_TextChanged);
			//this.BtnExceptionCode.Click += new System.EventHandler(this.BtnExceptionCode_Click);
			this.Drp_CustType.SelectedIndexChanged += new System.EventHandler(this.Drp_CustType_SelectedIndexChanged);
			this.Chk_NewCustomer.CheckedChanged += new System.EventHandler(this.Chk_NewCustomer_CheckedChanged);
			this.txtCustID.TextChanged += new System.EventHandler(this.Txt_CustId_TextChanged);
			this.BtnCustomerID.Click += new System.EventHandler(this.BtnCustomerID_Click);
			this.Rd_Cash.CheckedChanged += new System.EventHandler(this.Rd_Cash_CheckedChanged);
			this.rd_Credit.CheckedChanged += new System.EventHandler(this.rd_Credit_CheckedChanged);
			this.txtCustName.TextChanged += new System.EventHandler(this.txtCustName_TextChanged);
			this.Txt_Telephone.TextChanged += new System.EventHandler(this.Txt_Telephone_TextChanged);
			this.txtCustAddr1.TextChanged += new System.EventHandler(this.txtCustAddr1_TextChanged);
			this.Txt_Fax.TextChanged += new System.EventHandler(this.Txt_Fax_TextChanged);
			this.txtCustAdd2.TextChanged += new System.EventHandler(this.txtCustAdd2_TextChanged);
			this.txtCustZipCode.TextChanged += new System.EventHandler(this.txtCustZipCode_TextChanged);
			this.btnCustZipcode.Click += new System.EventHandler(this.btnCustZipcode_Click);
			this.Txt_Country.TextChanged += new System.EventHandler(this.Txt_Country_TextChanged);
			this.Txt_StateCode.TextChanged += new System.EventHandler(this.Txt_StateCode_TextChanged);
			this.Chk_SameCustomerInfo.CheckedChanged += new System.EventHandler(this.Chk_SameCustomerInfo_CheckedChanged);
			this.txtSendName.TextChanged += new System.EventHandler(this.txtSendName_TextChanged);
			this.BtnSendID.Click += new System.EventHandler(this.BtnSendID_Click);
			this.txtSendContPer.TextChanged += new System.EventHandler(this.txtSendContPer_TextChanged);
			this.txtSendAddr1.TextChanged += new System.EventHandler(this.txtSendAddr1_TextChanged);
			this.txtSendTel.TextChanged += new System.EventHandler(this.txtSendTel_TextChanged);
			this.txtSendAddr2.TextChanged += new System.EventHandler(this.txtSendAddr2_TextChanged);
			this.txtSendFax.TextChanged += new System.EventHandler(this.txtSendFax_TextChanged);
			this.txtSendZip.TextChanged += new System.EventHandler(this.txtSendZip_TextChanged);
			this.btnSenZipcode.Click += new System.EventHandler(this.btnSenZipcode_Click);
			this.txtSendCuttOffTime.TextChanged += new System.EventHandler(this.txtSendCuttOffTime_TextChanged);
			this.txtESA.TextChanged += new System.EventHandler(this.txtESA_TextChanged);
			this.Txt_Estpickupdatetime.TextChanged += new System.EventHandler(this.Txt_Estpickupdatetime_TextChanged);
			this.Txt_ActualPickupDateTime.TextChanged += new System.EventHandler(this.Txt_ActualPickupDateTime_TextChanged);
			this.Txt_CashAmount.TextChanged += new System.EventHandler(this.Txt_CashAmount_TextChanged);
			this.Txt_Totpkg.TextChanged += new System.EventHandler(this.Txt_Totpkg_TextChanged);
			this.Txt_ActualWeight.TextChanged += new System.EventHandler(this.Txt_ActualWeight_TextChanged);
			this.BtnShipmentDetails.Click += new System.EventHandler(this.BtnShipmentDetails_Click);
			this.Txt_DimWeight.TextChanged += new System.EventHandler(this.Txt_DimWeight_TextChanged);
			this.Txt_Remarks.TextChanged += new System.EventHandler(this.Txt_Remarks_TextChanged);
			this.btnExceedCancel.Click += new System.EventHandler(this.btnExceedCancel_Click);
			this.btnExceedOK.Click += new System.EventHandler(this.btnExceedOK_Click);
			this.btnToCancel.Click += new System.EventHandler(this.btnToCancel_Click);
			this.btnNotToSave.Click += new System.EventHandler(this.btnNotToSave_Click);
			this.btnToSaveChanges.Click += new System.EventHandler(this.btnToSaveChanges_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void btn_Query_Click(object sender, System.EventArgs e)
		{
			ViewState["Txt_Estpickupdatetime"]=null;
			ErrorMsg.Text="";
			Query();
			Txt_BookingDate.TextMaskString="99/99/9999 99:99";
			Txt_BookingDate.TextMaskType =  com.common.util.MaskType.msAlfaNumericWithUnderscoreSetFormat;
			ViewState["IsAcceptExceedsTime"] = false;
			setRequiredFields(false);
		}
		private void Query()
		{
			Txt_BookingNo.Enabled = true;
			Drp_CustType.Enabled =true;
			Drp_BookingType.Enabled =true;
			//BtnShipmentDetails.Enabled = false; 
			
			ViewState["PRBMode"] = ScreenMode.Query;
			if((int)ViewState["PRBOperation"]== (int)Operation.Insert||(int)ViewState["PRBOperation"]==(int)Operation.Update)
			{
				if (!CheckRoleM())
				{
					return;
				}
				lblConfirmMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				ConfirmPanel.Visible = true;
				Drp_BookingType.Visible = false;
				Drp_CustType.Visible =false; 
				lblCustomerhead.Visible =false;
				ConfirmPanel.Visible = true; 
				divMain.Visible=false;
				ViewState["isTextChanged"] = false;
				return;
			}
			
			cleardata();
			ErrorMsg.Text ="";
			Rd_Cash.Checked =  false;
			rd_Credit.Checked = false;
			Rd_FreightCollectd.Checked = false;
			//			Rd_FrieghtPrepaid.Checked = false; 
			//	Regularpickupdate.ErrorMessage="";  
			Txt_BookingNo.Enabled = true;
			Btn_ExcuteQuery.Enabled = true;
			Btn_Insert.Enabled = true; 
			btnMovefirst.Enabled = false;
			btnMovenext.Enabled =false;
			btnMovePrevious.Enabled =false;
			btnMovefirst.Enabled=false;
			btnMoveLast.Enabled =false;
			ViewState["isZipcode"]=false;
			Drp_BookingType.Visible = true;
			Drp_CustType.Visible = true; 
			Txt_RecCnt.Text =""; 
			ViewState["PRBOperation"]=Operation.None ; 
			
			//SetInitialFocus(txtCustID);  Ryu change from txtCustID to Txt_BookingNo
			SetInitialFocus(Txt_BookingNo);
			m_sdsPickupRequest = PRBMgrDAL.GetEmptyPRBDS();
			Session["SESSION_DS1"] = m_sdsPickupRequest;
			Chk_NewCustomer.Checked = false;
		}
		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.UniqueID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}

		private void setSelectCtrl(string ctrlName)

		{
			string script="<script langauge='javacript'>";
			script+= "document.all['"+ ctrlName +"'].select();";
			script+= "</script>";
			Page.RegisterStartupScript("setSelect",script);
		
		}
		private void Btn_ExcuteQuery_Click(object sender, System.EventArgs e)
		{	
			ViewState["Txt_Estpickupdatetime"] = null;
			//Txt_BookingDate.ReadOnly=true;
			ErrorMsg.Text="";
			if((int)ViewState["PRBOperation"]== (int)Operation.Insert||(int)ViewState["PRBOperation"]==(int)Operation.Update)
			{
				lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				ConfirmPanel.Visible = true;
				divMain.Visible=false;
				Drp_BookingType.Visible = false;
				Drp_CustType.Visible =false; 
				lblCustomerhead.Visible =false;
				ConfirmPanel.Visible = true; 
				ViewState["isTextChanged"] = false;

				return;
			}
			
			Btn_Save.Enabled = true;
			Btn_Insert.Enabled =false;
			BtnShipmentDetails.Enabled = true; 
			ViewState["PRBMode"] = ScreenMode.ExecuteQuery ; 
			//			ChangePRBState();
			ViewState["PRBOperation"]=Operation.None;  
			//			Drp_BookingType.SelectedItem.Selected =false;
			//			Drp_BookingType.Items.FindByText("").Selected=true;
			//			Drp_CustType.SelectedItem.Selected =false;
			//			Drp_CustType.Items.FindByText("").Selected=true;
			if ((int)ViewState["PRBMode"] != (int)ScreenMode.Insert)
			{
				Txt_BookingNo.Enabled = false;
				Drp_CustType.Enabled =false;
				Drp_BookingType.Enabled =false; 
				
				ViewState["QryRes"]=0;
				InsertPickupRequest (0,"query");
				ViewState["QUERY_DS"] = m_sdsPickupRequest.ds;
				m_sdsPickupRequest.DataSetRecSize = 10;
				DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
				m_sdsPickupRequest = PRBMgrDAL.GetPickupRequestDS(utility.GetAppID(),utility.GetEnterpriseID(),0,System.Convert.ToInt32(m_sdsPickupRequest.DataSetRecSize),dsQuery );  
				decimal decQryRes  =  m_sdsPickupRequest.QueryResultMaxSize; 
				ViewState["QryRes"] =  Convert.ToDecimal(decQryRes);
				if ((decimal)ViewState["QryRes"]!=0)
				{
					Trow = System.Convert.ToInt32(m_sdsPickupRequest.DataSetRecSize);
					ExtractData(0);
					ViewState["Crow"]=0;
				
					Btn_Insert.Enabled = true;
					Btn_Save.Enabled=true;
					Btn_Delete.Enabled =true;
					btnMovefirst.Enabled = true;
					btnMovePrevious.Enabled =true;
					btnMovenext.Enabled =true;
					btnMovefirst.Enabled=true;
					btnMoveLast.Enabled =true; 

				}
				else
				{

					ErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
					Btn_Insert.Enabled = false;
					Btn_Save.Enabled=false;
					Btn_Delete.Enabled =false;
					btnMovefirst.Enabled = false;
					btnMovenext.Enabled =false;
					btnMovefirst.Enabled=false;
					btnMoveLast.Enabled =false;
				}
				Session["SESSION_DS1"] = m_sdsPickupRequest;
				Btn_ExcuteQuery.Enabled = false;
				
				//ViewState["m_reccount"]=(int)ViewState["m_reccount"]+1;
				//Txt_RecCnt.Text  =ViewState["m_reccount"].ToString ();
			}
			else
			{
				cleardata();
				ViewState["QryRes"]=0;
				InsertPickupRequest (0,"query");
				ViewState["QUERY_DS"] = m_sdsPickupRequest.ds;
				m_sdsPickupRequest.DataSetRecSize = 10;
				DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
				m_sdsPickupRequest = PRBMgrDAL.GetPickupRequestDS(utility.GetAppID(),utility.GetEnterpriseID(),0,System.Convert.ToInt32(m_sdsPickupRequest.DataSetRecSize),dsQuery );  
				ViewState["QryRes"] =  m_sdsPickupRequest.QueryResultMaxSize; 
				if ((int)ViewState["QryRes"]!=0)
				{
					Trow = System.Convert.ToInt32(m_sdsPickupRequest.DataSetRecSize);
					ExtractData(0);
					ViewState["Crow"]=0;
				}
				else
				{
					ErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
				}
				Session["SESSION_DS1"] = m_sdsPickupRequest;
				Btn_ExcuteQuery.Enabled = false;
			}
			ViewState["IsAcceptExceedsTime"] = false;
			setRequiredFields(false);

			CheckRoleM();
			checkCash();
		}

		private void ExtractData(int iRow)
		{
			string date=null;
			string time =null;
			string sysdate=null;
			DateTime T1;
			DataRow drEach = m_sdsPickupRequest.ds.Tables[0].Rows[iRow];
			if((drEach["booking_no"]!= null) && (!drEach["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				Txt_BookingNo.Text = drEach["booking_no"].ToString().Trim();
				T1 = System.Convert.ToDateTime(drEach["booking_datetime"]);
				date = T1.ToString("dd/MM/yyyy");
				time = T1.ToString("HH:mm"); 
				sysdate = date +" "+time;
				Txt_BookingDate.Text = sysdate;
			}
			else
			{
				Txt_BookingNo.Text="";
			}
			if((drEach["booking_type"]!= null) && (!drEach["booking_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				
				Drp_BookingType.SelectedItem.Selected =false;
				Drp_BookingType.Items.FindByValue(drEach["booking_type"].ToString()).Selected=true;
			}
			if((drEach["payerid"]!= null) && (!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtCustID.Text = drEach["payerid"].ToString() ;
			}
			else
			{
				txtCustID.Text="";
			}
			if((drEach["payer_type"]!= null) && (!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				Drp_CustType.SelectedItem.Selected=false;   
				Drp_CustType.Items.FindByValue(drEach["payer_type"].ToString()).Selected = true; 
			}
			if((drEach["payer_name"]!= null) && (!drEach["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtCustName.Text = drEach["payer_name"].ToString().Trim();
			}
			else
			{
				txtCustName.Text="";
			}
			if((drEach["payer_address1"]!= null) && (!drEach["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtCustAddr1.Text = drEach["payer_address1"].ToString().Trim() ;
			}
			else
			{
				txtCustAddr1.Text="";
			}
			if((drEach["payer_address2"]!= null) && (!drEach["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtCustAdd2.Text=drEach["payer_address2"].ToString ().Trim();
			}
			else
			{
				txtCustAdd2.Text="";

			}
			if((drEach["payer_zipcode"]!= null) && (!drEach["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtCustZipCode.Text = drEach["payer_zipcode"].ToString ().Trim();
			}
			else
			{
				txtCustZipCode.Text="";
			}
			if((drEach["payer_country"]!= null) && (!drEach["payer_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				Txt_Country.Text = drEach["payer_country"].ToString ().Trim();
			}
			else
			{
				Txt_Country.Text="";
			}
			Zipcode zipCode = new Zipcode();
			zipCode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),Txt_Country.Text.Trim(),txtCustZipCode.Text.Trim());
			Txt_StateCode.Text = zipCode.StateName;  

			if((drEach["payer_telephone"]!= null) && (!drEach["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				Txt_Telephone.Text = drEach["payer_telephone"].ToString ().Trim();
			}
			else
			{
				Txt_Telephone.Text="";
			}
			if((drEach["payer_fax"]!= null) && (!drEach["payer_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				Txt_Fax.Text = drEach["payer_fax"].ToString ();
			}
			else
			{
				Txt_Fax.Text="";
			}
			if((drEach["payment_mode"]!= null) && (!drEach["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				if (drEach["payment_mode"].ToString()=="R")
				{
					rd_Credit.Checked =true; 
					Rd_Cash.Checked =false;

				}
				else
				{	
					Rd_Cash.Checked =true;
					rd_Credit.Checked =false; 
				}
			}			

			if((drEach["sender_name"]!= null) && (!drEach["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtSendName.Text =drEach["sender_name"].ToString ();
			}
			else
			{
				txtSendName.Text="";
			}
			if((drEach["sender_address1"]!= null) && (!drEach["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtSendAddr1.Text    = drEach["sender_address1"].ToString ();
			}
			else
			{
				txtSendAddr1.Text ="";
			}
			if((drEach["sender_address2"]!= null) && (!drEach["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtSendAddr2.Text    = drEach["sender_address2"].ToString ();
			}
			else
			{
				txtSendAddr2.Text="";
			}
			if((drEach["sender_zipcode"]!= null) && (!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtSendZip.Text     = drEach["sender_zipcode"].ToString ();
			}
			else
			{
				txtSendZip.Text   ="";
			}

			if((drEach["sender_country"]!= null) && (!drEach["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtSendCity.Text     = drEach["sender_country"].ToString ();
			}
			else
			{
				txtSendCity.Text ="";
			}
			zipCode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtSendCity.Text.Trim(),txtSendZip.Text.Trim());
			txtSendCuttOffTime.Text = zipCode.CuttOffTime.ToString("HH:mm") ;  	  	
			txtSendState.Text= zipCode.StateName;  
			
			if((drEach["sender_contact_person"]!= null) && (!drEach["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtSendContPer.Text=drEach["sender_contact_person"].ToString ();
			}
			else
			{
				txtSendContPer.Text="";
			}
			if((drEach["sender_telephone"]!= null) && (!drEach["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtSendTel.Text   =drEach["sender_telephone"].ToString ();
			}
			else
			{
				txtSendTel.Text ="";
			}
			if((drEach["sender_fax"]!= null) && (!drEach["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtSendFax.Text      =drEach["sender_fax"].ToString ();
			}
			else
			{
				txtSendFax.Text ="";
			}
			if((drEach["req_pickup_datetime"]!= null) && (!drEach["req_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				T1 = System.Convert.ToDateTime(drEach["req_pickup_datetime"]);
				date = T1.ToString("dd/MM/yyyy");
				time = T1.ToString("HH:mm"); 
				Txt_Estpickupdatetime.Text = date +" "+time;
				ViewState["Txt_Estpickupdatetime"] = Txt_Estpickupdatetime.Text;
			}
			else
			{
				ViewState["Txt_Estpickupdatetime"] = null;
				Txt_Estpickupdatetime.Text="";
			}
			if((drEach["act_pickup_datetime"]!= null) && (!drEach["act_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				//Txt_Estpickupdatetime.Text = sysdate;
				T1 = System.Convert.ToDateTime(drEach["act_pickup_datetime"]);
				date = T1.ToString("dd/MM/yyyy");
				time = T1.ToString("HH:mm"); 
				sysdate = date +" "+time;
				Txt_ActualPickupDateTime.Text  = sysdate;
			}
			else
			{
				Txt_ActualPickupDateTime.Text  ="";
			}		
			
			if((drEach["tot_pkg"]!= null) && (!drEach["tot_pkg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				Txt_Totpkg.Text = drEach["tot_pkg"].ToString() ;
			}
			else
			{
				Txt_Totpkg.Text ="";
			}
			if((drEach["tot_wt"]!= null) && (!drEach["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				Txt_ActualWeight.Text = drEach["tot_wt"].ToString() ;
			}
			else
			{
				Txt_ActualWeight.Text="";
			}
			if((drEach["tot_dim_wt"]!= null) && (!drEach["tot_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				Txt_DimWeight.Text = drEach["tot_dim_wt"].ToString();
			}
			else
			{
				Txt_DimWeight.Text ="";
			}
			if((drEach["cash_amount"]!= null) && (!drEach["cash_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				Txt_CashAmount.Text = TIESUtility.Round_Enterprise_Currency(utility.GetAppID(),utility.GetEnterpriseID(),decimal.Parse(drEach["cash_amount"].ToString()));
			}
			else
			{
				Txt_CashAmount.Text ="";
			}

			if((drEach["latest_status_code"]!= null) && (!drEach["latest_status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				Txt_StatusCode.Text = drEach["latest_status_code"].ToString ();
			}
			else
			{
				Txt_StatusCode.Text ="";
			}
			if((drEach["latest_exception_code"]!= null) && (!drEach["latest_exception_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				Txt_ExceptionCode.Text = drEach["latest_exception_code"].ToString ();
			}
			else
			{
				Txt_ExceptionCode.Text="";
			}
			if((drEach["remark"]!= null) && (!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				Txt_Remarks.Text =drEach["remark"].ToString ();
			}
			else
			{
				Txt_Remarks.Text="";
			}

			if((drEach["esa_pickup_surcharge"]!= null) && (!drEach["esa_pickup_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtESA.Text="YES";
				decimal decESASurcharge=Convert.ToDecimal(drEach["esa_pickup_surcharge"]);
				txtESASurcharge.Text = decESASurcharge.ToString("#0.00"); 
				
			}
			else
			{
				txtESA.Text ="NO";
				txtESASurcharge.Text="";
				

			}

			if((drEach["pickup_route"]!= null) && (!drEach["pickup_route"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtRouteCode.Text =drEach["pickup_route"].ToString ();
			}
			else
			{
				txtRouteCode.Text="";
			}

			this.txtRatedCharge.Text="";

			if(this.Txt_BookingNo.Text.Trim().Length>0)
			{
				DataTable dtPickupConsignment = PRBMgrDAL.ReadPickupConsignment(utility.GetAppID(),utility.GetEnterpriseID(),this.Txt_BookingNo.Text.Trim(),"");

				if(dtPickupConsignment!=null)
				{
					object sumRatedCharge = dtPickupConsignment.Compute("sum(TotalCharge)",null);
					if(sumRatedCharge!=null && sumRatedCharge!=DBNull.Value) this.txtRatedCharge.Text = String.Format("{0:0.00}",sumRatedCharge);
				}
			}
			Disable_PaymentMode();		
		}
		private void TextBox2_TextChanged(object sender, System.EventArgs e)
		{
		}
		private void Btn_Insert_Click(object sender, System.EventArgs e)
		{
			ViewState["Txt_Estpickupdatetime"]=null;
			ErrorMsg.Text="";
			Insert();
			SetInitialFocus(txtCustID); 
			ViewState["IsAcceptExceedsTime"] = false;
			//			Txt_BookingDate.TextMaskString="99/99/9999 99:99";
			//			Txt_BookingDate.TextMaskType =  com.common.util.MaskType.msDateTime;
			setRequiredFields(true);
			Session["SESSION_DTCon"] = null;
			Session["SESSION_DTPkg"] = null;
			Session["SESSION_DTVAS"] = null;
		}
			
		private void Insert()
		{
			ViewState["PRBMode"] = ScreenMode.Insert; 			
			BtnCustomerID.Enabled = true; 
			BtnShipmentDetails.Enabled = true; 
			txtRouteCode.Enabled = false;
			btnRouteCode.Enabled = false;	
			if(((int)ViewState["PRBOperation"] == (int)Operation.Insert) || ((int)ViewState["PRBOperation"] == (int)Operation.Update))
			{
				lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SAVE_PROMPT", utility.GetUserCulture());
				ConfirmPanel.Visible = true;
				divMain.Visible = false;
				Drp_BookingType.Visible = false;
				Drp_CustType.Visible = false; 
				lblCustomerhead.Visible = false;
				ConfirmPanel.Visible = true; 
				ViewState["isTextChanged"] = false;
				return;
			}
			ViewState["isTextChanged"] = false;
			cleardata();
			ErrorMsg.Text = "";  
			Txt_BookingNo.Enabled = true;
			Drp_CustType.Enabled = true;
			Drp_BookingType.Enabled = true;
			btnMovenext.Enabled = false;
			btnMovePrevious.Enabled = false;
			btnMovefirst.Enabled = false;
			btnMoveLast.Enabled = false; 
			Btn_Delete.Enabled = false;
			Txt_BookingNo.Enabled = false; 
			Btn_ExcuteQuery.Enabled = false; 
			Txt_StatusCode.Enabled = false;
			Txt_StatusCode.Text = TIESUtility.getInitialDispatchStatus(utility.GetAppID(),utility.GetEnterpriseID());  
			Txt_ExceptionCode.Enabled = false;
			Btn_Save.Enabled = true;
			ViewState["Crow"] = 0;
			DateTime T1 = DateTime.Now; 
			string date = T1.ToString("dd/MM/yyyy");
			string time = T1.ToString("HH:mm"); 
			Txt_BookingDate.Text = date +" "+time;
			Txt_Estpickupdatetime.Text = ""; 
			Btn_Insert.Enabled = false;  
			Chk_NewCustomer.Checked = false; 
			txtESA.Text = "NO";
			Chk_SameCustomerInfo.Checked = false;
			Drp_BookingType.SelectedItem.Selected = false;
			//Drp_BookingType.Items.FindByText("CALL-IN").Selected = true;
			Drp_BookingType.Items.FindByValue("C").Selected = true;
			Drp_CustType.SelectedItem.Selected = false;
			//Drp_CustType.Items.FindByText("Customer").Selected = true;
			Drp_CustType.Items.FindByValue("C").Selected = true;
			ViewState["PRBOperation"] = Operation.None;
			//			ChangePRBState();

			//

		}


		private void Chk_NewCustomer_CheckedChanged(object sender, System.EventArgs e)
		{
			
			if (Chk_NewCustomer.Checked==true)
			{
				txtCustName.Text="";
				txtCustAddr1.Text="";
				txtCustAdd2.Text="";
				txtCustZipCode.Text="";
				Txt_Country.Text="";
				Txt_StateCode.Text="";
				Txt_Telephone.Text="";
				Txt_Fax.Text=""; 
				txtSendCuttOffTime.Text=""; 
				Chk_SameCustomerInfo.Checked =false; 
				rd_Credit.Enabled =false; 
				rd_Credit.Checked = false; 
				Rd_Cash.Checked = true;
				
				BtnCustomerID.Enabled =false;
				Chk_SameCustomerInfo.Enabled = true; 
				txtCustID.Enabled=false; 
				txtCustID.Text = "NEW";
				//Make it to be able to override to A
				//Drp_CustType.SelectedItem.Selected =false;
				//Drp_CustType.Items.FindByValue("0").Selected=true;
			}
			else if (Chk_NewCustomer.Checked==false)
			{
				BtnCustomerID.Enabled = true; 
				txtCustID.Text=""; 
				txtCustName.Text=""; 
				txtCustName.Text="";
				txtCustAddr1.Text="";
				txtCustAdd2.Text="";
				txtCustZipCode.Text="";
				Txt_Country.Text="";
				Txt_StateCode.Text="";
				txtSendCuttOffTime.Text=""; 
				Rd_Cash.Checked = false;
				rd_Credit.Enabled =true; 
				return;
			}

		}

		private void Chk_SameCustomerInfo_CheckedChanged(object sender, System.EventArgs e)
		{			
			string strESAApplied=null;
			decimal decESASurcharge=0;
			if (Chk_SameCustomerInfo.Checked == true) 
			{
				txtSendName.Text = txtCustName.Text;
				txtSendAddr1.Text = txtCustAddr1.Text;
				txtSendAddr2.Text = txtCustAdd2.Text;
				txtSendZip.Text = txtCustZipCode.Text;
				txtSendCity .Text = Txt_Country.Text;
				txtSendState.Text = Txt_StateCode.Text;
				txtSendTel.Text = Txt_Telephone.Text;
				txtSendFax.Text = Txt_Fax.Text;
				txtSendZip.Text = txtCustZipCode.Text ; 
				Zipcode zipCode = new Zipcode();
				zipCode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),Txt_Country.Text.Trim(),txtCustZipCode.Text.Trim());      
				txtSendCuttOffTime.Text =zipCode.CuttOffTime.ToString("HH:mm");
				txtRouteCode.Text = zipCode.PickUpRoute;
				decESASurcharge	= zipCode.EASSurcharge ;
			
				Customer customer= new Customer();
				customer.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text.Trim());
				txtSendContPer.Text=customer.ContactPerson; 	
			}
			else
			{
				txtSendName.Text = "";
				txtSendAddr1.Text = "";
				txtSendAddr2.Text = "";
				txtSendZip.Text = "";
				txtSendCity.Text = "";
				txtSendState.Text = "";
				txtSendTel.Text = "";
				txtSendFax.Text = "";
				txtSendZip.Text = "";	
				txtSendCuttOffTime.Text ="";
				txtRouteCode.Text="";

			}
			
			if (Drp_CustType.SelectedItem.Value =="C")
			{
				Customer customer = new Customer();
				customer.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text);
				strESAApplied =customer.ESASurcharge ;
			}
			else if (Drp_CustType.SelectedItem.Value =="A")
			{
				Agent agent = new Agent();
				agent.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text);
				strESAApplied=agent.ESASurcharge; 

			}
			if ((Rd_Cash.Checked==true) || (rd_Credit.Checked==true ))
			{
				if(decESASurcharge!=0 && strESAApplied=="Y" )
				{
					txtESA.Text="YES"; 
					
					txtESASurcharge.Text = decESASurcharge.ToString("#0.00"); 
				}
				else 
				{
					txtESA.Text="NO"; 
					txtESASurcharge.Text = ""; 
				}
			}
			if((Chk_NewCustomer.Checked ==true )&& (Rd_Cash.Checked==true))
			{
				if(decESASurcharge!=0)
				{
					txtESA.Text="YES"; 
					txtESASurcharge.Text = decESASurcharge.ToString("#0.00"); 
				}
				else
				{
					txtESA.Text="NO"; 
					txtESASurcharge.Text = "";
				}
			}
			txtSendZip_TextChanged(sender,null);						
		}	
		private void txtCustZipCode_TextChanged(object sender, System.EventArgs e)
		{
			String strZipCode=null;
			ErrorMsg.Text="";
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
			Zipcode zipCode = new Zipcode();
			zipCode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtCustZipCode.Text.Trim());
			strZipCode= zipCode.ZipCode; 
			if (strZipCode==null)
			{
				ErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"NO_ZIP",utility.GetUserCulture());  
			}
			Txt_Country.Text = zipCode.Country;
			string strStateCode= zipCode.StateCode;  
			zipCode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),Txt_Country.Text,txtCustZipCode.Text.Trim());
			Txt_StateCode .Text = zipCode.StateName; 
						
		}
		private void Btn_Save_Click(object sender, System.EventArgs e)
		{			
			ErrorMsg.Text="";
			Customer cs = new Customer();
			cs.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text.Trim());
			 
			if(cs.CustomerName == null)
			{
				ErrorMsg.Text = "Customer ID does not exist.";
				return;
			}

			if (Drp_CustType.SelectedItem.Text.Trim() == "")
			{
				ErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_CUST_TYPE",utility.GetUserCulture());
				return;
			}
			if ((int)ViewState["PRBOperation"]==(int)Operation.Insert)  
			{
				string sEst = null;
				if(this.Txt_Estpickupdatetime.Text.Trim() != "")
				{
					DateTime dEst=DateTime.ParseExact(this.Txt_Estpickupdatetime.Text.Trim(),"dd/MM/yyyy HH:mm",null);
					sEst=dEst.ToString();
				}
				this.EstimatedPickupDateType = PickupDateType.Today;
				this.EstimatedPickupDateData = null;
				this.EstimatedPickupDate(sEst);
			}
			else
			{
				ExceedOK();
			}
		}
		//Phase2 - K04
		private void SavetoDatabase()
		{
			//BY X MAY 26 08
			Customer cs = new Customer();
			cs.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text.Trim());
			 
			if(cs.CustomerName == null)
			{
				ViewState["isTextChanged"] = false;
				ViewState["PRBOperation"]=Operation.None;
				ConfirmPanel.Visible = false;
				divMain.Visible=true;
				Drp_BookingType.Visible=true;
				Drp_CustType.Visible =true; 
				lblCustomerhead.Visible =true;
				ErrorMsg.Text = "Customer ID does not exist.";
				return;
			}
			//BY X MAY 26 08

			ErrorMsg.Text = ""; 
			String strMsg =null;

			// Check Payment Mode (Required)
			if((Rd_Cash.Checked ==false)&&(rd_Credit.Checked==false))
			{
				ErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"SEL_PYMT_MODE",utility.GetUserCulture());
				return;
			}
			if(Txt_Totpkg.Text=="" )//|| Txt_ActualWeight.Text =="" ||Txt_DimWeight.Text=="")
			{
				//ErrorMsg.Text= "Please input package detail";
				ErrorMsg.Text= "Please input number of packages to be picked up.";
				return;
			}
			string cEstDate="";
			if(ViewState["Txt_Estpickupdatetime"] != null)
			{
				cEstDate = ViewState["Txt_Estpickupdatetime"].ToString();
			}
			else
			{
				cEstDate = Txt_Estpickupdatetime.Text;
			}

			if(System.DateTime.ParseExact(Txt_Estpickupdatetime.Text,"dd/MM/yyyy HH:mm",null) < System.DateTime.ParseExact(cEstDate,"dd/MM/yyyy HH:mm",null))
			{
				ErrorMsg.Text =  "Earliest Est Pickup Date/Time is  " + cEstDate;
				return;
			}

			if ((int)ViewState["PRBOperation"] != (int)Operation.None)
			{
				bool IsAcceptExceedsTimeState = false;
				DataTable dtPickupConsignment = null;
				DataTable dtPickupConsignmentPKG = null;
				DataTable dtPickupConsignmentVAS = null;
				if(Session["SESSION_DTCon"] != null)
					dtPickupConsignment = (DataTable)Session["SESSION_DTCon"];
				
				if(Session["SESSION_DTPkg"] != null)
					dtPickupConsignmentPKG = (DataTable)Session["SESSION_DTPkg"];

				if(Session["SESSION_DTVAS"] != null)
					dtPickupConsignmentVAS = (DataTable)Session["SESSION_DTVAS"];
					
				if (ViewState["IsAcceptExceedsTime"] != null)
					IsAcceptExceedsTimeState = (bool)ViewState["IsAcceptExceedsTime"];

				if(!IsAcceptExceedsTimeState)
				{
					if ((int)ViewState["PRBOperation"]== (int)Operation.Update||((int)ViewState["PRBOperation"]== (int)Operation.Saved)) 
					{
						ViewState["isTextChanged"] = false;
						InsertPickupRequest((int)ViewState["Crow"], "");
						DataSet dsPickupRequest = m_sdsPickupRequest.ds.GetChanges();
						int Rowsreturned = PRBMgrDAL.ModifyPickupRequest(utility.GetAppID(),utility.GetEnterpriseID(),dsPickupRequest);
						if(Session["SESSION_DTCon"] != null)
						{
							PRBMgrDAL.InsertPickupConsignment(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignment);
							PRBMgrDAL.InsertPickupConsignmentPkg(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignmentPKG);
							PRBMgrDAL.InsertPickupConsignmentVAS(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignmentVAS);
						}
						ErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
						ChangePRBState();
						m_sdsPickupRequest.ds.Tables[0].Rows[(int)ViewState["Crow"]].AcceptChanges();
						setRequiredFields(false);
						Session["SESSION_DTCon"] = null;
						Session["SESSION_DTPkg"] = null;
						Session["SESSION_DTVAS"] = null;
						ViewState["Txt_Estpickupdatetime"] = Txt_Estpickupdatetime.Text.Trim();
						return;
					}
					else if ((int)ViewState["PRBOperation"]==(int)Operation.Insert)  
					{	
						try
						{
							bkgno  = Counter.GetNext(utility.GetAppID(),utility.GetEnterpriseID(),"booking_number") ;
							Txt_BookingNo.Text = bkgno.ToString();
							InsertPickupRequest((int)ViewState["Crow"], "");
							DataSet dsPickupRequest = m_sdsPickupRequest.ds.GetChanges();

							InsertDispatch();
							int Rowsreturned = PRBMgrDAL.AddPickupRequest(utility.GetAppID(),utility.GetEnterpriseID(),dsPickupRequest,m_dsDispatch);
							if(Session["SESSION_DTCon"] != null)
							{
								PRBMgrDAL.InsertPickupConsignment(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignment);
								PRBMgrDAL.InsertPickupConsignmentPkg(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignmentPKG);
								PRBMgrDAL.InsertPickupConsignmentVAS(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignmentVAS);
							}
							ChangePRBState();
							ViewState["isTextChanged"] = false;;
							m_sdsPickupRequest.ds.Tables[0].Rows[(int)ViewState["Crow"]].AcceptChanges();
							ErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INS_SAVED",utility.GetUserCulture());
							BtnShipmentDetails.Enabled =true; 
							setRequiredFields(false);
							Session["SESSION_DTCon"] = null;
							Session["SESSION_DTPkg"] = null;
							Session["SESSION_DTVAS"] = null;
						}	
						catch(ApplicationException appException)
						{
							strMsg = appException.Message;
							if(strMsg.IndexOf("duplicate key") != -1 )
							{
								strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
							}
							else if(strMsg.IndexOf("FK") != -1 )
							{
								strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
							}
							else
							{
								strMsg = appException.Message.ToString();
							}
							ErrorMsg.Text = strMsg; 
							ViewState["PRBOperation"]=Operation.None;  
							return;
						}
					}
				}
				else
				{
					if ((int)ViewState["PRBOperation"]== (int)Operation.Update||((int)ViewState["PRBOperation"]== (int)Operation.Saved)) 
					{
						ViewState["isTextChanged"] = false;
						InsertPickupRequest((int)ViewState["Crow"], "");
						DataSet dsPickupRequest = m_sdsPickupRequest.ds.GetChanges();
						int Rowsreturned = PRBMgrDAL.ModifyPickupRequest(utility.GetAppID(),utility.GetEnterpriseID(),dsPickupRequest);
						if(Session["SESSION_DTCon"] != null)
						{
							PRBMgrDAL.InsertPickupConsignment(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignment);
							PRBMgrDAL.InsertPickupConsignmentPkg(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignmentPKG);
							PRBMgrDAL.InsertPickupConsignmentVAS(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignmentVAS);
						}
						ErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
						ChangePRBState();
						m_sdsPickupRequest.ds.Tables[0].Rows[(int)ViewState["Crow"]].AcceptChanges();
						setRequiredFields(false);
						Session["SESSION_DTCon"] = null;
						Session["SESSION_DTPkg"] = null;
						Session["SESSION_DTVAS"] = null;
						ViewState["Txt_Estpickupdatetime"] = Txt_Estpickupdatetime.Text.Trim();
						return;
					}
					else if ((int)ViewState["PRBOperation"]==(int)Operation.Insert)  
					{
						try
						{
							bkgno  = Counter.GetNext(utility.GetAppID(),utility.GetEnterpriseID(),"booking_number") ;
							Txt_BookingNo.Text = bkgno.ToString();
							InsertPickupRequest((int)ViewState["Crow"], "");
							DataSet dsPickupRequest = m_sdsPickupRequest.ds.GetChanges();

							InsertDispatch();
							int Rowsreturned = PRBMgrDAL.AddPickupRequest(utility.GetAppID(),utility.GetEnterpriseID(),dsPickupRequest,m_dsDispatch);
							if(Session["SESSION_DTCon"] != null)
							{
								PRBMgrDAL.InsertPickupConsignment(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignment);
								PRBMgrDAL.InsertPickupConsignmentPkg(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignmentPKG);
								PRBMgrDAL.InsertPickupConsignmentVAS(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignmentVAS);
							}
							ChangePRBState();
							ViewState["isTextChanged"] = false;;
							m_sdsPickupRequest.ds.Tables[0].Rows[(int)ViewState["Crow"]].AcceptChanges();
							ErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INS_SAVED",utility.GetUserCulture());
							BtnShipmentDetails.Enabled =true; 
							setRequiredFields(false);
							Session["SESSION_DTCon"] = null;
							Session["SESSION_DTPkg"] = null;
							Session["SESSION_DTVAS"] = null;
						}
						catch(ApplicationException appException)
						{
							strMsg = appException.Message;
							if(strMsg.IndexOf("duplicate key") != -1 )
							{
								strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
							}
							else if(strMsg.IndexOf("FK") != -1 )
							{
								strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
							}
							else
							{
								strMsg = appException.Message.ToString();
							}
							ErrorMsg.Text = strMsg; 
							ViewState["PRBOperation"]=Operation.None;  
							return;
						}
					}
				}
			}
			ViewState["Txt_Estpickupdatetime"] = Txt_Estpickupdatetime.Text.Trim();
			Btn_Insert.Enabled = true; 
			BtnShipmentDetails.Enabled=true;
		}
		//Phase2 - K04

		private void btnMovefirst_Click(object sender, System.EventArgs e)
		{
			ErrorMsg.Text="";
			movefirst();
			ViewState["IsAcceptExceedsTime"] = false;
		}
		private void movefirst()
		{
			ViewState["MoveLast"]=true;
			if(((int)ViewState["PRBOperation"]== (int)Operation.Insert)||((int)ViewState["PRBOperation"]==(int)Operation.Update))
			{
				if (!CheckRoleM())
				{
					return;
				}
				lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				ConfirmPanel.Visible = true;
				divMain.Visible=false;
				Drp_BookingType.Visible = false;
				Drp_CustType.Visible =false; 
				lblCustomerhead.Visible =false;
				ConfirmPanel.Visible = true; 
				ViewState["isTextChanged"] = false;
				return;
			}
			ViewState["PRBOperation"]=Operation.None;  
			btnMovePrevious.Enabled = false;  
			btnMovenext.Enabled = true; 
			
			cleardata();
			//			InsertPickupRequest((int)ViewState["Crow"]);
			//			Session["QUERY_DS"] = m_sdsPickupRequest.ds;
			Session["QUERY_DS"] = m_sdsPickupRequest.ds;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsPickupRequest.DataSetRecSize = 10;
			ViewState["CurrentSetSize"]=0;
			m_sdsPickupRequest = PRBMgrDAL.GetPickupRequestDS(utility.GetAppID(),utility.GetEnterpriseID(),0,System.Convert.ToInt32(m_sdsPickupRequest.DataSetRecSize),dsQuery );  
			ViewState["Crow"]=0;
			ExtractData((int)ViewState["Crow"]); 
			Session["SESSION_DS1"] = m_sdsPickupRequest;
			//ViewState["m_reccount"]=1;
			//Txt_RecCnt.Text  =ViewState["m_reccount"].ToString() 
		}

		private void btnMoveLast_Click(object sender, System.EventArgs e)
		{
			ErrorMsg.Text="";
			movelast();
			ViewState["IsAcceptExceedsTime"] = false;
		}
		private void movelast()
		{
			ViewState["MoveLast"]=true;
			if(((int)ViewState["PRBOperation"]== (int)Operation.Insert)||((int)ViewState["PRBOperation"]==(int)Operation.Update))
			{
				if (!CheckRoleM())
				{
					return;
				}
				lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				ConfirmPanel.Visible = true;
				divMain.Visible=false;
				Drp_BookingType.Visible = false;
				Drp_CustType.Visible =false; 
				lblCustomerhead.Visible =false;
				ConfirmPanel.Visible = true; 
				ViewState["isTextChanged"] = false;

				return;
			}
			ViewState["PRBOperation"]=Operation.None;  		

			ViewState["CurrentSetSize"]=(System.Convert.ToInt32(m_sdsPickupRequest.QueryResultMaxSize)-1)/m_SetSize;
			m_StartIndex = ((int)ViewState["CurrentSetSize"])* m_SetSize;
			Session["QUERY_DS"] = m_sdsPickupRequest.ds;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsPickupRequest.DataSetRecSize = 10;
			m_sdsPickupRequest = PRBMgrDAL.GetPickupRequestDS(utility.GetAppID(),utility.GetEnterpriseID(),m_StartIndex,System.Convert.ToInt32(m_sdsPickupRequest.DataSetRecSize),dsQuery );  

			ViewState["Crow"]=Convert.ToInt32(m_sdsPickupRequest.DataSetRecSize)-1;
			ExtractData((int)ViewState["Crow"]);
			btnMovenext.Enabled=false; 
			btnMovePrevious.Enabled = true;
			Session["SESSION_DS1"] = m_sdsPickupRequest;

		}
		private void btnMovenext_Click(object sender, System.EventArgs e)
		{
			ErrorMsg.Text="";
			movenext();
			ViewState["IsAcceptExceedsTime"] = false;
		}
		private void movenext()
		{
			ViewState["MoveNext"]=true;
			btnMovePrevious.Enabled =true; 
			if(((int)ViewState["PRBOperation"]== (int)Operation.Insert)||((int)ViewState["PRBOperation"]==(int)Operation.Update))
			{
				if (!CheckRoleM())
				{
					return;
				}
				lblConfirmMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				ConfirmPanel.Visible = true;
				divMain.Visible=false;
				Drp_BookingType.Visible = false;
				Drp_CustType.Visible =false; 
				lblCustomerhead.Visible =false;
				ConfirmPanel.Visible = true; 
				ViewState["isTextChanged"] = false;
				return;
			}
		
			btnMovePrevious.Enabled = true;
			ViewState["iTotalRec"]=0;																			
			if((int)ViewState["Crow"]  < (m_sdsPickupRequest.DataSetRecSize - 1) )
			{
				ViewState["Crow"]= (int)ViewState["Crow"]+1;
				ExtractData((int)ViewState["Crow"]); 
				//int m_StartIndex = (int)ViewState["CurrentSetSize"]* m_SetSize;
				//	ViewState["m_reccount"]=(int)ViewState["Crow"]+m_StartIndex+1;
			}
			else
			{
				
				int m_StartIndex = (int)ViewState["CurrentSetSize"]* m_SetSize;
				ViewState["iTotalRec"] = m_StartIndex + m_sdsPickupRequest.DataSetRecSize;
				if( (decimal)ViewState["iTotalRec"] ==  m_sdsPickupRequest.QueryResultMaxSize)
				{
					return;

				}
				
				ViewState["CurrentSetSize"]=(int)ViewState["CurrentSetSize"]+1;	
				m_StartIndex = (int)ViewState["CurrentSetSize"]* m_SetSize; 
				Session["QUERY_DS"] = m_sdsPickupRequest.ds;
				DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
				m_sdsPickupRequest = PRBMgrDAL.GetPickupRequestDS(utility.GetAppID(),utility.GetEnterpriseID(),m_StartIndex,System.Convert.ToInt32(m_sdsPickupRequest.DataSetRecSize),dsQuery );  
				ViewState["Crow"] = 0;
				ExtractData((int)ViewState["Crow"]);
				//	ViewState["m_reccount"]= (int)ViewState["Crow"]+m_StartIndex+1 ;
				
			}
			Session["SESSION_DS1"] = m_sdsPickupRequest;
			//Txt_RecCnt.Text = ViewState["m_reccount"].ToString (); 
		}
	
		private void FillPickupRequest(int Rowvalue)
		{
			try
			{
				DataSet dsPickupRequest = PRBMgrDAL.GetPickupRequestData(utility.GetAppID(),utility.GetEnterpriseID());
				DataRow drEach = dsPickupRequest.Tables[0].Rows[Rowvalue];
				Txt_BookingNo.Text = drEach["booking_no"].ToString();
				Txt_BookingDate.Text = drEach["booking_datetime"].ToString();
				txtCustID.Text = drEach["payerid"].ToString() ;
				Drp_CustType.DataTextField = drEach["payer_type"].ToString();
				txtCustName.Text = drEach["payer_name"].ToString();
				txtCustAddr1.Text = (string)drEach["payer_address1"];
				txtCustAdd2.Text=drEach["payer_address2"].ToString ();
				txtCustZipCode.Text = drEach["payer_zipcode"].ToString ();
				Txt_Country.Text = drEach["payer_country"].ToString ();
				Txt_Telephone.Text = drEach["payer_telephone"].ToString ();
				Txt_Fax.Text = drEach["payer_fax"].ToString ();
				//payementmode
				txtSendName.Text =drEach["sender_name"].ToString ();
				txtSendAddr1.Text    = drEach["sender_address1"].ToString ();
				txtSendAddr2.Text    = drEach["sender_address2"].ToString ();
				txtSendZip.Text     = drEach["sender_zipcode"].ToString ();
				txtSendState.Text     = drEach["sender_country"].ToString ();
				txtSendContPer.Text=drEach["sender_contact_person"].ToString ();
				txtSendTel.Text   =drEach["sender_telephone"].ToString ();
				txtSendFax.Text         =drEach["sender_fax"].ToString ();
				Txt_Estpickupdatetime.Text =drEach["req_pickup_datetime"].ToString() ;
				Txt_ActualPickupDateTime.Text = drEach["act_pickup_datetime"].ToString();
				//paymenttype
				Txt_Totpkg.Text = drEach["tot_pkg"].ToString() ;
				Txt_ActualWeight.Text = drEach["tot_wt"].ToString() ;
				Txt_DimWeight.Text = drEach["tot_dim_wt"].ToString();
				//Txt_CashAmount.Text = drEach["cash_amount"].ToString() ;
				Txt_CashAmount.Text = TIESUtility.Round_Enterprise_Currency(utility.GetAppID(),utility.GetEnterpriseID(),decimal.Parse(drEach["cash_amount"].ToString()));
				//				Txt_cashcollected.Text = drEach["cash_collected"].ToString() ;
				//	special rates
				Txt_StatusCode.Text = drEach["latest_status_code"].ToString ();
				Txt_ExceptionCode.Text = drEach["latest_exception_code"].ToString ();
				Txt_Remarks.Text =drEach["remark"].ToString ();
				txtESASurcharge.Text=drEach["esa_pickup_surcharge"].ToString();
				//quotation_no
			}
			catch
			{
				Logger.LogTraceError("PickupRequest","FillPickupRequest","PRB0012","Popualting Datbase failed");
				
			}
		}
		private void InsertPickupRequest(int iRowindex ,string from)
		{
				
			DataRow drEach = m_sdsPickupRequest.ds.Tables[0].Rows[iRowindex];
			if(Txt_BookingNo.Text.Trim().Length >0)  
			{
				drEach["booking_no"]=System.Convert.ToInt64(Txt_BookingNo.Text);
			}
			if (Txt_BookingDate.Text.Trim().Length >0)
			{
				if (from.ToUpper() == "QUERY")
				{
					//						strMyDateTime = System.DateTime.ParseExact(Txt_BookingDate.Text.Trim(),"dd/MM/yyyy",null);
					strMyDateTime = PRBMgrDAL.getLastDatetimeFromDate(utility.GetAppID().ToString(),utility.GetEnterpriseID().ToString(),Txt_BookingDate.Text);
					drEach["booking_datetime"]=strMyDateTime;
				}
				else
				{
					strMyDateTime = System.DateTime.ParseExact(Txt_BookingDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
					drEach["booking_datetime"]=strMyDateTime;
				}
				//				strMyDateTime = System.DateTime.ParseExact(Txt_BookingDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			}
			drEach["booking_type"]=Drp_BookingType.SelectedItem.Value ;
			if(Chk_NewCustomer.Checked==true)
			{
				drEach["new_account"]="Y";
			}
			else if(Chk_NewCustomer.Checked==false)
			{
				drEach["new_account"]="N";
			}
			if (txtCustID.Text.Trim().Length >0)
			{
				drEach["payerid"]=txtCustID.Text.Trim() ;
			}
			if (Drp_CustType.SelectedItem.Value!="0")
			{
				drEach["payer_type"]=Drp_CustType.SelectedItem.Value ;
			}
			if (txtCustName.Text.Trim().Length >0)
			{
				drEach["payer_name"]=txtCustName.Text.Trim();
			}
			if (txtCustAddr1.Text.Trim().Length >0)
			{
				drEach["payer_address1"]=txtCustAddr1.Text.Trim();
			}
			if (txtCustAdd2.Text.Trim().Length >0)
			{
				drEach["payer_address2"]=txtCustAdd2.Text.Trim();
			}
			if (txtCustZipCode.Text.Trim().Length >0)
			{
				drEach["payer_zipcode"]=txtCustZipCode .Text.Trim();
			}
			if (Txt_Country.Text.Trim().Length >0)
			{
				drEach["payer_country"]=Txt_Country.Text.Trim();
			}
			if (Txt_Telephone.Text.Trim().Length >0)
			{
				drEach["payer_telephone"]=Txt_Telephone.Text.Trim();
			}
			if (Txt_Fax.Text.Trim().Length >0)
			{
				drEach["payer_fax"]=Txt_Fax.Text.Trim();
			}
			if (Rd_Cash.Checked.Equals(true))
			{
				drEach["payment_mode"]="C";
			}
			if (rd_Credit.Checked.Equals(true))
			{
				drEach["payment_mode"]="R";
			}
			if (txtSendName.Text.Trim().Length >0)
			{
				drEach["sender_name"]=txtSendName.Text.Trim();
			}
			if (txtSendAddr1.Text.Trim().Length >0)
			{
				drEach["sender_address1"]=txtSendAddr1.Text.Trim();
			}
			if (txtSendAddr2.Text.Trim().Length >0)
			{
				drEach["sender_address2"]=txtSendAddr2.Text.Trim();
			}
			if (txtSendZip.Text.Trim().Length >0)
			{
				drEach["sender_zipcode"]=txtSendZip.Text.Trim();

				zipCode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtSendZip.Text.Trim());	

				String strDC = PRBMgrDAL.GetDC(utility.GetAppID(),utility.GetEnterpriseID(),zipCode.PickUpRoute.ToString());

				if(strDC.Length > 0)
					drEach["location"] = strDC;
				else
					drEach["location"] = System.DBNull.Value;

			}
			if (txtSendCity.Text.Trim().Length >0)
			{
				drEach["sender_country"]=txtSendCity.Text.Trim();
			}
			if (txtSendContPer.Text.Trim().Length >0)
			{
				drEach["sender_contact_person"]=txtSendContPer.Text.Trim();
			}
			if (txtSendTel.Text.Trim().Length >0)
			{
				drEach["sender_telephone"]=txtSendTel.Text.Trim();
			}
			if (Txt_Fax.Text.Trim().Length >0)
			{
				drEach["sender_fax"]=Txt_Fax.Text.Trim();
			}
			if (Txt_Estpickupdatetime.Text.Trim().Length>0)
			{ 
				strMyDateTime = System.DateTime.ParseExact(Txt_Estpickupdatetime.Text.Trim(),"dd/MM/yyyy HH:mm",null);
				drEach["req_pickup_datetime"]=System.Convert.ToDateTime(strMyDateTime);
			}
			if (Txt_ActualPickupDateTime.Text.Trim().Length >0) 
			{
				strMyDateTime = System.DateTime.ParseExact(Txt_ActualPickupDateTime.Text.Trim(),"dd/MM/yyyy HH:mm",null);
				drEach["act_pickup_datetime"]=System.Convert.ToDateTime(strMyDateTime);
			}
			if (Rd_FrieghtPrepaid.Checked.Equals(true))
			{
				drEach["payment_type"]="FP";
			}
			if(Rd_FreightCollectd.Checked.Equals(true)) 
			{
				drEach["payment_type"]="FC";
			}
			if (Txt_Totpkg.Text.Trim().Length >0) 
			{
				drEach["tot_pkg"]=Convert.ToInt64(Txt_Totpkg.Text.Trim());
			}
			if (Txt_ActualWeight.Text.Trim().Length >0) 
			{
				drEach["tot_wt"]=Convert.ToDecimal(Txt_ActualWeight.Text.Trim());
			}
			if (Txt_DimWeight.Text.Trim().Length >0) 
			{
				drEach["tot_dim_wt"]=Convert.ToDecimal(Txt_DimWeight.Text.Trim());
			}
			if (Txt_CashAmount.Text.Trim().Length >0) 
			{
				drEach["cash_amount"]=Convert.ToDecimal(Txt_CashAmount.Text.Trim());
			}
			//			if (Txt_cashcollected.Text.Trim().Length >0) 
			//			{
			//				drEach["cash_collected"]=Convert.ToDecimal(Txt_cashcollected.Text.Trim());
			//			}
			//			if (Chk_SpecialRates.Checked == true)
			//			{
			//				drEach["special_rates"]="Y";
			//			}
			//			else if (Chk_SpecialRates.Checked == false)
			//			{
			//				drEach["special_rates"]="N";
			//			}
			if (Txt_StatusCode.Text.Trim().Length >0)	
			{
				drEach["latest_status_code"]=Txt_StatusCode.Text.Trim();
			}
			if (Txt_ExceptionCode.Text.Trim().Length >0)	
			{
				drEach["latest_exception_code"]=Txt_ExceptionCode.Text.Trim();
			}

			String strLatestStatus=DateTime.Now.ToString("dd/MM/yyy HH:mm");
			strMyDateTime = System.DateTime.ParseExact(strLatestStatus,"dd/MM/yyyy HH:mm",null);
			if(strMyDateTime.ToString() !=null)
			{
				drEach["latest_status_datetime"]=System.Convert.ToDateTime(strMyDateTime);
			}
		
			if (Txt_Remarks.Text.Trim().Length >0)
			{
				drEach["remark"]=Txt_Remarks.Text.Trim();
			}

			if (txtRouteCode.Text.Trim().Length >0)
			{
				drEach["pickup_route"]=txtRouteCode.Text.Trim();
			}
			
			//Get the quotation number & version 
			QuotationData custData;
			custData.iQuotationVersion = 0;
			custData.strQuotationNo= null;

			drEach["quotation_no"] = custData.strQuotationNo;
			drEach["quotation_version"] = custData.iQuotationVersion.ToString();
			
			if (txtESASurcharge.Text.Trim().Length >0) 
			{
				drEach["esa_pickup_surcharge"]=Convert.ToDecimal(txtESASurcharge.Text.Trim()); 
			}
				
		}
		private void InsertDispatch()
		{
			m_dsDispatch = PRBMgrDAL.GetEmptyDispatchTracking(utility.GetAppID(),utility.GetEnterpriseID());
			DataRow drEach = m_dsDispatch.Tables[0].Rows[0];
			drEach["booking_no"]=System.Convert.ToInt64(Txt_BookingNo.Text);
			if (Txt_BookingDate.Text.Trim().Length >0)
			{
				strMyDateTime = System.DateTime.ParseExact(Txt_BookingDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
				drEach["tracking_datetime"]=strMyDateTime;
			}
			drEach["status_code"]=Txt_StatusCode.Text.Trim();
			drEach["deleted"]="N";
			drEach["last_userid"]=struserID;
			drEach["last_updated"]= DateTime.Now;
			drEach["location"]= zipCode.StateCode;
		}

		private void Btn_Delete_Click(object sender, System.EventArgs e)
		{
			
			ErrorMsg.Text = ""; 
			btnMovenext.Enabled =false;
			btnMovePrevious.Enabled = false;
			btnMovefirst.Enabled=false;
			btnMoveLast.Enabled =false; 
			
			ViewState["PRBOperation"]=Operation.Delete;
			if (((int)ViewState["PRBMode"]==(int)ScreenMode.ExecuteQuery))
			{
				if(Txt_StatusCode.Text.Trim()=="DISP")
				{
					if((Txt_BookingNo.ToString() !="")||(Txt_BookingNo!=null)) 
					{
						lblConfirmMsg.Text  = Utility.GetLanguageText(ResourceType.UserMessage,"DEL_PROMPT",utility.GetUserCulture());
						ConfirmPanel.Visible = true;
						divMain.Visible=false;
						Drp_BookingType.Visible = false;
						Drp_CustType.Visible =false; 
						lblCustomerhead.Visible =false;
						ConfirmPanel.Visible = true; 
						return;
					}
					else
					{
						ErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_BOOKING_NO",utility.GetUserCulture());
						return;
					}
				}
			
				else
				{
					ErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_DEL_DISP",utility.GetUserCulture());
					return;
				}
			}
		}
		private void DeleteRecord()
		{
			try
			{
				int RowsDeleted =  PRBMgrDAL.DeletePickupRequest(utility.GetAppID(),utility.GetEnterpriseID(),(System.Convert.ToInt32(Txt_BookingNo.Text)),Txt_StatusCode.Text.Trim());
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1 || strMsg.IndexOf("child record") != -1)
				{
					strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());
				}
				else
				{
					strMsg = appException.Message.ToString();
				}
				ErrorMsg.Text = strMsg; 
				ViewState["PRBOperation"]=Operation.None;  
				return;
			}
			Txt_BookingNo.Text="";
			Txt_BookingDate.Text="";
			Txt_Estpickupdatetime.Text="";
			Txt_ActualPickupDateTime.Text=""; 
			cleardata();
			ErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
		}
		private void btnMovePrevious_Click(object sender, System.EventArgs e)
		{  
			ErrorMsg.Text="";
			moveprevious();
			ViewState["IsAcceptExceedsTime"] = false;
		}
		private void moveprevious()
		{
			ViewState["MovePrevious"]=true;
			btnMovenext.Enabled = true; 
			if(((int)ViewState["PRBOperation"]== (int)Operation.Insert)||((int)ViewState["PRBOperation"]==(int)Operation.Update))
			{
				if (!CheckRoleM())
				{
					return;
				}
				lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				ConfirmPanel.Visible = true;
				divMain.Visible=false;
				Drp_BookingType.Visible = false;
				Drp_CustType.Visible =false; 
				lblCustomerhead.Visible =false;
				ConfirmPanel.Visible = true; 
				ViewState["isTextChanged"] = false;
				return;
			}
			ViewState["PRBOperation"]=Operation.None; 
						
			if((int)ViewState["Crow"]  > 0 )
			{
				ViewState["Crow"]= (int)ViewState["Crow"]-1;
				ExtractData((int)ViewState["Crow"]);
				//int m_StartIndex = (int)ViewState["CurrentSetSize"]* m_SetSize;
				//ViewState["m_reccount"]=m_StartIndex+(int)ViewState["Crow"]+1;
							
			}
			else
			{		
				if ((int)ViewState["CurrentSetSize"]==0)
				{			
					return;
				}
				else
				{	
					ViewState["CurrentSetSize"]=(int)ViewState["CurrentSetSize"]-1;	
				}
				m_StartIndex = (int)ViewState["CurrentSetSize"]* m_SetSize; 
							
				Session["QUERY_DS"] = m_sdsPickupRequest.ds;
				DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
				m_sdsPickupRequest.DataSetRecSize=10;
				m_sdsPickupRequest = PRBMgrDAL.GetPickupRequestDS(utility.GetAppID(),utility.GetEnterpriseID(),m_StartIndex,System.Convert.ToInt32(m_sdsPickupRequest.DataSetRecSize),dsQuery );  
				Session["SESSION_DS1"] = m_sdsPickupRequest;
				ViewState["Crow"]=Convert.ToInt32(m_sdsPickupRequest.DataSetRecSize)-1; 
				ExtractData((int)ViewState["Crow"]);
				//ViewState["m_reccount"]=m_StartIndex+(int)ViewState["Crow"]+1;
			}
			Session["SESSION_DS1"] = m_sdsPickupRequest;
			//Txt_RecCnt.Text = ViewState["m_reccount"].ToString (); 
						
		}
		
		private void cleardata()
		{
			if (m_sdsPickupRequest != null) 
			{
				m_sdsPickupRequest = PRBMgrDAL.GetEmptyPRBDS();
				Session["SESSION_DS1"] = m_sdsPickupRequest;
			}

			Txt_BookingNo.Text = "";
			Txt_BookingDate.Text = "";
			txtCustID.Text = "";
			txtCustName.Text = "";
			txtCustAddr1.Text = "";
			txtCustAdd2.Text = "";
			txtCustZipCode.Text = "";
			Txt_Country.Text = "";
			Txt_Telephone.Text = "";
			Txt_Fax.Text = "";
			Txt_StateCode.Text = "";
			txtSendName.Text = "";
			txtSendAddr1.Text = "";
			txtSendAddr2.Text = "";
			txtSendZip.Text = "";
			txtSendCity.Text = "";
			txtSendState.Text = "";
			txtESA.Text = "";
			txtESASurcharge.Text = "";  
			txtSendCuttOffTime.Text = "";
			txtSendContPer.Text = "";
			txtSendTel.Text = ""; 
			txtSendFax.Text = "";
			Txt_ActualPickupDateTime.Text = "";
			Txt_Estpickupdatetime.Text = "";
			Txt_Totpkg.Text = "";
			Txt_ActualWeight.Text = "";
			Txt_DimWeight.Text = "";
			txtRatedCharge.Text = "";
			Txt_Remarks.Text = "";
			Txt_CashAmount.Text = "";
			//			Txt_cashcollected.Text = "";
			Drp_BookingType.SelectedItem.Selected = false;
			Drp_BookingType.Items.FindByValue("0").Selected = true;
			Drp_CustType.SelectedItem.Selected = false;
			Drp_CustType.Items.FindByValue("0").Selected = true;
			Rd_Cash.Checked = false;
			rd_Credit.Checked = false;
			Rd_FreightCollectd.Checked = false;
			//			Rd_FrieghtPrepaid.Checked = false; 
			Txt_ExceptionCode.Text = "";
			Txt_StatusCode.Text = ""; 
			//			Chk_SpecialRates.Checked = false;  
			txtRouteCode.Text = "";
		}

		private void Rd_Cash_CheckedChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
			if (Rd_Cash.Checked == true)
			{
				Txt_CashAmount.Enabled = true;
				//				Txt_cashcollected.Enabled = true;
				//edit by R 12/06/2013
				//Rd_FreightCollectd.Enabled = true;
				Rd_FrieghtPrepaid.Enabled = true; 

				//				this.Txt_Totpkg.Text="";
				//				this.Txt_ActualWeight.Text="";
				//				this.Txt_DimWeight.Text="";
				//				this.txtRatedCharge.Text="";

				this.Txt_Totpkg.Enabled=true;
				this.Txt_ActualWeight.Enabled=false;
				this.Txt_DimWeight.Enabled=false;
				this.txtRatedCharge.Enabled=false;
			}
		}

		private void Rd_Credit_CheckedChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
			if (rd_Credit.Checked == true)
			{
				Txt_CashAmount.Enabled = true;
				//				Txt_cashcollected.Enabled = false;
				Rd_FreightCollectd.Enabled = false;
				Rd_FrieghtPrepaid.Enabled = false; 
				this.Txt_Totpkg.Enabled=true;
				this.Txt_ActualWeight.Enabled=false;
				this.Txt_DimWeight.Enabled=false;
				this.txtRatedCharge.Enabled=false;
			}
		}

		private void Txt_CustId_TextChanged(object sender, System.EventArgs e)
		{
			ErrorMsg.Text= "";

			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}

			if (PRBMgrDAL.isActiveCustomer(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text.Trim()) == false)
			{
				ErrorMsg.Text= "Inactive customer ID.";
				txtCustName.Text = "";
				txtCustAddr1.Text = "";
				txtCustAdd2.Text = "";
				Txt_Fax.Text = "";
				Txt_Telephone.Text = "";
				txtCustZipCode.Text = "";
				Txt_Country.Text = "";
				Txt_StateCode.Text = "";
				return;
			}
			
			if(Drp_CustType.SelectedIndex == 1)
			{
				
				Customer customer = new Customer();
				customer.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text.Trim());
				txtCustName.Text = customer.CustomerName;
				txtCustAddr1.Text = customer.CustomerAddress1;
				txtCustAdd2.Text = customer.CustomerAddress2;
				Txt_Fax.Text = customer.CustomerFax;
				Txt_Telephone.Text = customer.CustomerTelephone;
				txtCustZipCode.Text = customer.ZipCode;
				Enterprise enterprise = new Enterprise();
				enterprise.Populate(utility.GetAppID(),utility.GetEnterpriseID());
				String strPayerCountry = enterprise.Country;
				Zipcode zipCode = new Zipcode();
				if (txtCustZipCode.Text =="")
				{
					return;
				}
				else
				{
					zipCode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),strPayerCountry,txtCustZipCode.Text.Trim());
				}

				Txt_Country.Text = strPayerCountry;
				Txt_StateCode.Text = zipCode.StateName;
				string time = zipCode.CuttOffTime.ToString("HH:mm") ;
				ViewState["CutOffTime"]=time ;

				//Removed by CRTS 954 Turn off Locking from over Credit Limit
				try
				{
					if(CustomerProfileDAL.Check_IsCreditStatusNotAvail(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text.Trim()))
					{
						this.Rd_Cash.Checked = true;
						this.rd_Credit.Checked = false;
						this.Rd_Cash.Enabled = false;
						this.rd_Credit.Enabled = false;
						this.Btn_Save.Enabled=false;
									
						ErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_PRB_CRSTATUS_NA", utility.GetUserCulture());
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
				}
				//End CRTS 954 Turn off Locking from over Credit Limit


			}
			else if(Drp_CustType.SelectedIndex == 2)
			{
				Agent agent = new Agent();
				agent.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text.Trim());
				txtCustName.Text = agent.AgentName;
				txtCustAddr1.Text = agent.AgentAddress1 ;
				txtCustAdd2.Text = agent.AgentAddress2;
				Txt_Fax.Text = agent.AgentFax ;
				Txt_Telephone.Text = agent.AgentTelephone ;
				txtCustZipCode.Text = agent.ZipCode;
				Enterprise enterprise = new Enterprise();
				enterprise.Populate(utility.GetAppID(),utility.GetEnterpriseID());
				String strPayerCountry = enterprise.Country;
				Zipcode zipCode = new Zipcode();
				zipCode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),strPayerCountry,txtCustZipCode.Text.Trim());
				if (txtCustZipCode.Text=="")
				{
					Txt_Country.Text = "";
				}
				else
				{
					Txt_Country.Text = strPayerCountry;
				}
				Txt_StateCode.Text = zipCode.StateName;
				string time = zipCode.CuttOffTime.ToString("HH:mm") ;
				ViewState["CutOffTime"]=time ;
 
			}
			
			checkCash();
			Check_Disable_PaymentMode_Checkbox();
		}
		private void txtSendCuttOffTime_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
			
		}

		private void Txt_Estpickupdatetime_TextChanged(object sender, System.EventArgs e)
		{
			if(((int)ViewState["PRBMode"]==(int)ScreenMode.ExecuteQuery))
			{
				if ((bool)ViewState["isTextChanged"]==false)
				{
					ChangePRBState();
					ViewState["isTextChanged"]=true;
				}
			}
		}

		private void BtnCustomerID_Click(object sender, System.EventArgs e)
		{
			rd_Credit.Checked =false;
			Rd_Cash.Checked =false; 

			if( Drp_CustType.SelectedIndex ==1) 
			{
				//txtCustID.Text=""; 
				string strCustId =null;
				string strtxtCustId=null;
				if(txtCustID != null)
				{
					strtxtCustId = txtCustID.ClientID;
					strCustId = txtCustID.Text;
				}
				String sUrl = "CustomerPopup.aspx?FORMID=PickupRequestBooking&CUSTID_TEXT="+strCustId+"&BANDCODE="+strtxtCustId;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			else if( Drp_CustType.SelectedIndex ==2) 
			{
				txtCustID.Text=""; 
				string strCustId =null;
				string strtxtCustId=null;
				if(txtCustID != null)
				{
					strtxtCustId = txtCustID.ClientID;
					strCustId = txtCustID.Text;
				}
						

				String sUrl = "AgentPopup.aspx?FORMID=PickupRequestBooking&CUSTID_TEXT="+strCustId+"&BANDCODE="+strtxtCustId;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			else
			{
				ErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_CUST_TYPE",utility.GetUserCulture());
			}
		}
		public void LoadCustomerTypeList()
		{
			ListItem defItem = new ListItem();
			defItem.Text = "";
			defItem.Value = "0";
			Drp_CustType.Items.Add(defItem);
			ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"customer_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				Drp_CustType.Items.Add(lstItem);
			}
		}
		public void LoadBookingTypeList()
		{
			ListItem defItem = new ListItem();
			defItem.Text = "";
			defItem.Value = "0";
			Drp_BookingType.Items.Add(defItem);
			ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"booking_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				Drp_BookingType.Items.Add(lstItem);
			}

		}
		private void Txt_RecCnt_TextChanged(object sender, System.EventArgs e)
		{
		
		}

		private void Txt_BookingNo_TextChanged(object sender, System.EventArgs e)
		{
			//			if ((bool)ViewState["isTextChanged"]==false)
			//			{
			//				ChangePRBState();
			//				ViewState["isTextChanged"]=true;
			//			}
		}

		private void Drp_BookingType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				if ((int)ViewState["PRBMode"]!=(int)ScreenMode.Insert && (int)ViewState["PRBOperation"]!=(int)Operation.Delete)  
				{
					if ((bool)ViewState["isTextChanged"]==false)
					{
						ChangePRBState();
						ViewState["isTextChanged"]=true;
					}
				}
			}
			catch(Exception ex)
			{
			}
		}

		private void Txt_BookingDate_TextChanged(object sender, System.EventArgs e)
		{
			
			if(((int)ViewState["PRBMode"]==(int)ScreenMode.ExecuteQuery))
			{
				if ((bool)ViewState["isTextChanged"]==false)
				{
					ChangePRBState();
					ViewState["isTextChanged"]=true;
				}
			}
		}

		private void Txt_StatusCode_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}
		private void Txt_ExceptionCode_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}
		private void Drp_CustType_SelectedIndexChanged(object sender, System.EventArgs e)
		{

			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
			BtnCustomerID.Enabled = true; 
			if (Drp_CustType.SelectedItem.Value=="A")    
			{	
				BtnCustomerID.Enabled = true; 
				//txtCustID.Text="";
				txtCustName.Text="";
				txtCustAddr1.Text="";
				txtCustAdd2.Text="";
				txtCustZipCode.Text="";
				Txt_Country.Text="";
				Txt_StateCode.Text="";
				Txt_Telephone.Text="";
				Txt_Fax.Text="";
				return;
			}
			
		}
		private void txtCustName_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void txtCustAddr1_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void txtCustAdd2_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Txt_Country_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Txt_StateCode_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Txt_Telephone_TextChanged(object sender, System.EventArgs e)
		{
			
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Txt_Fax_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void txtSendName_TextChanged(object sender, System.EventArgs e)
		{
			DataSet ds = DomesticShipmentManager.GetSenderReceipt(appID,enterpriseID,txtSendName.Text, txtCustID.Text,"","S");		
			if(ds.Tables[0].Rows.Count>0)
			{				
				DataRow dr = ds.Tables[0].Rows[0];
				String strName = dr["snd_rec_name"].ToString();
				String strAddress1 = dr["address1"].ToString();
				String strAddress2 = dr["address2"].ToString();
				String strCountry = dr["country"].ToString();
				String strDestZipCode = dr["zipcode"].ToString().ToUpper();
				String strTelephone = dr["telephone"].ToString();
				String strFax = dr["fax"].ToString();
				String strContactPerson = dr["contact_person"].ToString();
				String strStateCode = dr["state_code"].ToString();
				String strState = dr["state_name"].ToString();

				Zipcode zipCode = new Zipcode();
				zipCode.Populate(appID,enterpriseID,strCountry,strDestZipCode);
				

				DateTime dtCuttOffTime = zipCode.CuttOffTime;
				String strCuttOffTime = dtCuttOffTime.ToString("HH:mm");

				txtSendName.Text	=strName;
				txtSendAddr1.Text	=strAddress1;
				txtSendAddr2.Text	=strAddress2;
				txtSendState.Text	=strCountry;
				txtSendZip.Text		=strDestZipCode.ToUpper();
				txtSendCity.Text	=strState.ToUpper();
				txtSendContPer.Text	=strContactPerson;
				txtSendTel.Text		=strTelephone;
				txtSendFax.Text		=strFax;
				txtSendCuttOffTime.Text	=strCuttOffTime;
			}
			if (Convert.ToBoolean(ViewState["isTextChanged"])==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
			txtSendZip_TextChanged(sender,null);
		}

		private void txtSendContPer_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Txt_Saddress1_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Txt_STelephone_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Txt_Saddress2_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Txt_SCountry_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Txt_SStateCode_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Txt_ActualPickupDateTime_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Txt_Totpkg_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Txt_ActualWeight_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Txt_DimWeight_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Txt_Remarks_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Rd_FrieghtPrepaid_CheckedChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Rd_FreightCollectd_CheckedChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Txt_CashAmount_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void Txt_cashcollected_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void btnToSaveChanges_Click(object sender, System.EventArgs e)
		{		
			//BY X MAY 26 08
			Customer cs = new Customer();
			cs.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text.Trim());
			 
			if(cs.CustomerName == null)
			{
				ViewState["isTextChanged"] = false;
				ViewState["PRBOperation"]=Operation.None;
				ConfirmPanel.Visible = false;
				divMain.Visible=true;
				Drp_BookingType.Visible=true;
				Drp_CustType.Visible =true; 
				lblCustomerhead.Visible =true;
				ErrorMsg.Text = "Customer ID does not exist.";
				return;
			}
			//BY X MAY 26 08

			if(!DomesticShipmentMgrDAL.IsDomesticShpExist(utility.GetAppID(),utility.GetEnterpriseID(),Int32.Parse((Txt_BookingNo.Text == "" ? "0": Txt_BookingNo.Text))))
			{
				if ((int)ViewState["PRBOperation"]==(int)Operation.Delete)
				{
					DeleteRecord();
					ConfirmPanel.Visible = false;
					divMain.Visible=true;
					ViewState["isTextChange"] = false;
					Drp_BookingType.Visible=true;
					Drp_CustType.Visible =true; 
					ViewState["PRBOperation"]=Operation.None;

				}
				else
				{
					SavetoDatabase(); 
					ConfirmPanel.Visible = false;
					divMain.Visible=true;
					ViewState["isTextChange"] = false;
					Drp_BookingType.Visible=true;
					Drp_CustType.Visible =true; 
					ViewState["PRBOperation"]=Operation.None;  
				}
			}
			else
			{
				ViewState["PRBOperation"]=Operation.None;
				ConfirmPanel.Visible=false;
				divMain.Visible=true;
				ErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"BKG_POSTED_MODIFY_DOMESTIC",utility.GetUserCulture());
			}
			enableInput(CheckRoleM());

		}

		private void btnToCancel_Click(object sender, System.EventArgs e)
		{
			//BY X MAY 26 08
			Customer cs = new Customer();
			cs.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text.Trim());
			 
			if(cs.CustomerName == null)
				this.ErrorMsg.Text = "Customer ID does not exist.";
			//BY X MAY 26 08

			ViewState["isTextChanged"] = false;
			ViewState["PRBOperation"]=Operation.None;
			ConfirmPanel.Visible = false;
			divMain.Visible=true;
			Drp_BookingType.Visible=true;
			Drp_CustType.Visible =true; 
			lblCustomerhead.Visible =true;
			//cleardata();
			enableInput(CheckRoleM());
			DataSet dsResult = null;

			//Removed by CRTS 954 Turn off Locking from over Credit Limit
			//			try
			//			{
			//				if(CustomerProfileDAL.Check_IsCreditStatusNotAvail(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text.Trim()))
			//				{
			//					this.Rd_Cash.Checked = true;
			//					this.rd_Credit.Checked = false;
			//					this.Rd_Cash.Enabled = false;
			//					this.rd_Credit.Enabled = false;
			//					ErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_PRB_CRSTATUS_NA", utility.GetUserCulture());
			//								
			//				}					
			//			}
			//
			//			
			//			catch (Exception ex)
			//			{
			//				Console.WriteLine(ex.Message);
			//			}
			//End CRTS 954 Turn off Locking from over Credit Limit

		}

		private void btnNotToSave_Click(object sender, System.EventArgs e)
		{
			if ((int)ViewState["PRBMode"]==(int)ScreenMode.Insert) 
			{
				ConfirmPanel.Visible = false;
				divMain.Visible=true;
				ViewState["isTextChange"] = false;
				ViewState["PRBOperation"]=Operation.None;
				Drp_BookingType.Visible=true;
				Drp_CustType.Visible =true; 
				lblCustomerhead.Visible = true;
				cleardata();
				ViewState["PRBOperation"]=Operation.None;  
				Insert();
			}
			else if ((int)ViewState["PRBMode"]==(int)ScreenMode.Query) 
			{
				ConfirmPanel.Visible = false;
				divMain.Visible=true;
				ViewState["isTextChange"] = false;
				ViewState["PRBOperation"]=Operation.None;
				Drp_BookingType.Visible=true;
				Drp_CustType.Visible =true; 
				lblCustomerhead.Visible = true;
				cleardata();
				ViewState["PRBOperation"]=Operation.None;
				Query();
			}
			else if ((bool)ViewState["MovePrevious"]==true) 
			{
				
				ConfirmPanel.Visible = false;
				divMain.Visible=true;
				ViewState["isTextChange"] = false;
				Drp_BookingType.Visible=true;
				Drp_CustType.Visible =true; 
				lblCustomerhead.Visible = true;
				ViewState["PRBOperation"]=Operation.None;
				moveprevious();
				
			}
			else if((bool)ViewState["MoveNext"]==true) 
			{
				
				ConfirmPanel.Visible = false;
				divMain.Visible=true;
				ViewState["isTextChange"] = false;
				Drp_BookingType.Visible=true;
				Drp_CustType.Visible =true;
				lblCustomerhead.Visible = true;
				ViewState["PRBOperation"]=Operation.None;
				movenext();
			}
			else if((bool)ViewState["MoveLast"]==true) 
			{
				
				ConfirmPanel.Visible = false;
				divMain.Visible=true;
				ViewState["isTextChange"] = false;
				Drp_BookingType.Visible=true;
				Drp_CustType.Visible =true;
				lblCustomerhead.Visible = true;
				ViewState["PRBOperation"]=Operation.None;
				movelast();
			}
			else if((bool)ViewState["MoveFirst"]==true) 
			{
				
				ConfirmPanel.Visible = false;
				divMain.Visible=true;
				ViewState["isTextChange"] = false;
				Drp_BookingType.Visible=true;
				Drp_CustType.Visible =true;
				lblCustomerhead.Visible = true;
				ViewState["PRBOperation"]=Operation.None;
				movefirst();
				
			}
			else
			{
				ConfirmPanel.Visible = false;
				divMain.Visible=true;
				ViewState["isTextChange"] = false;
				Drp_BookingType.Visible=true;
				Drp_CustType.Visible =true;
				lblCustomerhead.Visible = true;
				ViewState["PRBOperation"]=Operation.None;

			}
			enableInput(CheckRoleM());

		}

		private void TxtPaymentmode_TextChanged(object sender, System.EventArgs e)
		{

		}

		private void BtnSendID_Click(object sender, System.EventArgs e)
		{
			String strPayMode=null;
			String strNewCust=null;

			String strID = txtCustID.Text;

			if(strID == "NEW")
				strID = "";
			if(Rd_Cash.Checked ==true)
			{
				strPayMode="C";
			}
			else if(rd_Credit.Checked==true)
			{
				strPayMode="R";
			}
			if(Chk_NewCustomer.Checked ==true)
			{
				strNewCust="Y";
			}
			else
			{
				strNewCust="N";
			}
		
			if(Drp_CustType.SelectedItem.Value == "0")
			{
				ErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_CUST_TYPE",utility.GetUserCulture());
			}
			else if(Drp_CustType.SelectedItem.Value == "C")
			{
				//				String sUrl = "SndRecipPopup.aspx?FORMID="+"PickupRequestBooking"+"&SENDRCPTYPE="+"S"+"&SENDERNAME="+txtSendName.Text.Trim()+"&CUSTID="+strID+"&CUSTTYPE="+Drp_CustType.SelectedItem.Value +"&PAYMODE="+strPayMode+"&NEWCUST="+strNewCust;
				//				String sFeatures = "'height=650;width=1370;left=30;top=50;location=no;menubar=no;resizable=yes;scrollbars=yes;status=no;titlebar=yes;toolbar=no'";
				//				String sScript ="";
				//				sScript += "<script language=javascript>";
				//				sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
				//				sScript += "</script>";
				//				Response.Write(sScript);
				String sUrl = "SndRecipPopup.aspx?FORMID="+"PickupRequestBooking"+"&SENDRCPTYPE="+"S"+"&SENDERNAME="+txtSendName.Text.Trim()+"&CUSTID="+strID+"&CUSTTYPE="+Drp_CustType.SelectedItem.Value +"&PAYMODE="+strPayMode+"&NEWCUST="+strNewCust;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			else if(Drp_CustType.SelectedItem.Value == "A")
			{
				//				String sUrl = "AgentSndRecPopup.aspx?FORMID="+"PickupRequestBooking"+"&SENDRCPTYPE="+"S"+"&SENDERNAME="+txtSendName.Text.Trim();
				//				String sFeatures = "'height=650;width=1370;left=30;top=50;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'";
				//				String sScript ="";
				//				sScript += "<script language=javascript>";
				//				sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
				//				sScript += "</script>";
				//				Response.Write(sScript);
				String sUrl = "AgentSndRecPopup.aspx?FORMID="+"PickupRequestBooking"+"&SENDRCPTYPE="+"S"+"&SENDERNAME="+txtSendName.Text.Trim();
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
		}
			
		private void btnCustZipcode_Click(object sender, System.EventArgs e)
		{
			string strZipcodeClientID=null;
			string strZipcode=null;
			string strStateClientID=null;
			string strCountryClientID=null;
			
			
			if (txtCustZipCode.Text != null)
			{
				strZipcodeClientID = txtCustZipCode.ClientID;
				strZipcode = txtCustZipCode.Text;
			}
			if(Txt_StateCode.Text != null)
			{
				strStateClientID = Txt_StateCode.ClientID;
			}
				
			if(Txt_Country.Text  != null)
			{
				strCountryClientID = Txt_Country.ClientID;
			}

			
				
			//			string sUrl = "ZipcodePopup.aspx?FORMID=PickupRequestBooking&ZIPCODE="+strZipcode+"&ZIPCODE_CID="+strZipcodeClientID+"&STATE_CID="+strStateClientID+"&COUNTRY_CID="+strCountryClientID;
			//			String sFeatures = "'height=650;width=1370;left=30;top=50;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'";
			//			String sScript ="";
			//			sScript += "<script language=javascript>";
			//			sScript += "window.open('" + sUrl + "',''," + sFeatures + ");";
			//			sScript += "</script>";
			//			Response.Write(sScript);
			String sUrl = "ZipcodePopup.aspx?FORMID=PickupRequestBooking&ZIPCODE="+strZipcode+"&ZIPCODE_CID="+strZipcodeClientID+"&STATE_CID="+strStateClientID+"&COUNTRY_CID="+strCountryClientID;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
		
		private void btnSenZipcode_Click(object sender, System.EventArgs e)
		{
			string strZipcodeClientID=null;
			string strZipcode=null;
			string strStateClientID=null;
			string strCountryClientID=null;
			string strSendZipcode="Sender";
			string strCustType=null;
			string strCustId=null;
			

			if (txtCustZipCode.Text != null)
			{
				strZipcodeClientID = txtSendZip.ClientID;
				strZipcode = txtSendZip.Text;
			}
			if(Txt_StateCode.Text != null)
			{
				strStateClientID = txtSendState.ClientID;
			}
				
			if(Txt_Country.Text  != null)
			{
				strCountryClientID = txtSendCity.ClientID;
			}
			if (Drp_CustType.SelectedItem.Value=="C")
			{
				strCustType= Drp_CustType.SelectedItem.Value;
			}
			else if (Drp_CustType.SelectedItem.Value=="A")
			{
				strCustType= Drp_CustType.SelectedItem.Value;
			}
			else if (Chk_NewCustomer.Checked == true) 
			{
				strCustType="NS";
			}
			strCustId=txtCustID.Text; 
			//txtESASurcharge
			//txtESA
			//txtSendCuttOffTime
			String sESASurchargeClientID=null;
			String sESAClientID=null;
			String sCutOffTimeClientID=null;
			sESASurchargeClientID=txtESASurcharge.ClientID;
			sESAClientID=txtESA.ClientID;
			sCutOffTimeClientID=txtSendCuttOffTime.ClientID;

			String sUrl = "ZipcodePopup.aspx?FORMID=PickupRequestBooking&ZIPCODE="+strZipcode+"&ZIPCODE_CID=";
			sUrl += strZipcodeClientID+"&STATE_CID="+strStateClientID+"&COUNTRY_CID="+strCountryClientID;
			sUrl += "&SENDZIPCODE="+strZipcode+"&CUSTTYPE="+strCustType+"&CUSTID="+strCustId+"&SENDZIPCODE=";
			sUrl += strSendZipcode+"&ESASurcharge_CID="+sESASurchargeClientID+"&ESA_CID="+sESAClientID;
			sUrl += "&SendCutOffTime_CID="+sCutOffTimeClientID;

			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

		}

		private void ChangePRBState()
		{
			if(Convert.ToInt16(ViewState["PRBMode"]) == Convert.ToInt16(ScreenMode.Insert) && Convert.ToInt16(ViewState["PRBOperation"]) == Convert.ToInt16(Operation.None))
			{
				ViewState["PRBOperation"] = Operation.Insert;
			}
			else if(Convert.ToInt16(ViewState["PRBMode"]) == Convert.ToInt16(ScreenMode.Insert) && Convert.ToInt16(ViewState["PRBOperation"]) == Convert.ToInt16(Operation.Insert))
			{
				ViewState["PRBOperation"] = Operation.Saved;
			}
			else if(Convert.ToInt16(ViewState["PRBMode"]) == Convert.ToInt16(ScreenMode.Insert) && Convert.ToInt16(ViewState["PRBOperation"]) == Convert.ToInt16(Operation.Saved))
			{
				ViewState["PRBOperation"] = Operation.Update;
			}
			else if(Convert.ToInt16(ViewState["PRBMode"]) == Convert.ToInt16(ScreenMode.Insert) && Convert.ToInt16(ViewState["PRBOperation"]) == Convert.ToInt16(Operation.Update))
			{
				ViewState["PRBOperation"] = Operation.Saved;
			}
			else if(Convert.ToInt16(ViewState["PRBMode"]) == Convert.ToInt16(ScreenMode.ExecuteQuery) && Convert.ToInt16(ViewState["PRBOperation"]) == Convert.ToInt16(Operation.None))
			{
				ViewState["PRBOperation"] = Operation.Update;
			}
			else if(Convert.ToInt16(ViewState["PRBMode"]) == Convert.ToInt16(ScreenMode.ExecuteQuery) && Convert.ToInt16(ViewState["PRBOperation"]) == Convert.ToInt16(Operation.Update))
			{
				ViewState["PRBOperation"] = Operation.None;
			}

			//Check_Disable_PaymentMode_Checkbox();
		}

		private void BtnShipmentDetails_Click(object sender, System.EventArgs e)
		{
			ErrorMsg.Text = "";
			if(txtCustID.Text != "" && txtSendZip.Text != ""&&Txt_Estpickupdatetime.Text!="")
			{
				Session["PickupCustomerID"] = txtCustID.Text;
				Session["SenderPostalCode"] = txtSendZip.Text;
				Session["Estpickupdatetime"] = Txt_Estpickupdatetime.Text;
	
				String sUrl = "PickupRequest_ConRate.aspx?BOOKINGID="+Txt_BookingNo.Text;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openParentWindow.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);	
			}
			else
			{
				if(Txt_Estpickupdatetime.Text!="")
					ErrorMsg.Text = "Please Enter Customer ID and Sender Postal Code.";
				else
					ErrorMsg.Text = "Pleae Enter Estimated Pickup Date Time.";
			}
		}

		private void txtSendTel_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void txtSendFax_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void txtESA_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void txtSendAddr2_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void txtSendAddr1_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void txtSendZip_TextChanged(object sender, System.EventArgs e)
		{
			string strESAApplied=null;
			decimal decESASurcharge=0;
			string strZipCode=null;
			ErrorMsg.Text=null;
			string strPreviousVASAmt="0";
			bool newCheck = false;			
			if(txtSendZip.Text !="")
			{
				// Thosapol.y  > Modify	(01/07/2013)
				if (getRole("CSSU")=="CSSU")
				{					
					txtRouteCode.Enabled = true; 	
					btnRouteCode.Enabled = true;
				}
				else
				{
					txtRouteCode.Enabled = false;
					btnRouteCode.Enabled = false;
				}
			}
			else
			{
				txtRouteCode.Enabled = false;
				btnRouteCode.Enabled = false;
			}
			
			if(txtESASurcharge.Text !="")
				strPreviousVASAmt=txtESASurcharge.Text;

			if(Chk_NewCustomer.Checked)
				newCheck = true;

			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
			//			if (Chk_SameCustomerInfo.Checked == false)
			//			{
			Zipcode zipCode = new Zipcode();
			zipCode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtSendZip.Text.Trim());
			strZipCode= zipCode.ZipCode;  
			if (strZipCode==null)
			{
				ErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_ZIP",utility.GetUserCulture());
				return;
			}
			txtSendCity.Text = zipCode.Country;
			zipCode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtSendCity.Text,txtSendZip.Text.Trim());
			txtSendState.Text = zipCode.StateName; 
			string time = zipCode.CuttOffTime.ToString("HH:mm") ;
			txtSendCuttOffTime.Text =time; 
			txtRouteCode.Text = zipCode.PickUpRoute;

			decESASurcharge	= zipCode.EASSurcharge  ;
			//			}
			
			if (Drp_CustType.SelectedItem.Value =="C")
			{
				Customer customer = new Customer();
				customer.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text);
				strESAApplied =customer.ESASurcharge ;
			}
			else
			{
				Agent agent = new Agent();
				agent.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text);
				strESAApplied=agent.ESASurcharge; 

			}
			if ((Rd_Cash.Checked==true) || (rd_Credit.Checked==true ))
			{
				string strCashAmt="0";

				if(decESASurcharge!=0 && strESAApplied=="Y" )
				{
					txtESA.Text="YES"; 					
					txtESASurcharge.Text = decESASurcharge.ToString("#0.00"); 
					strCashAmt=Txt_CashAmount.Text;
					if((int)ViewState["PRBOperation"]== (int)Operation.Update)
					{

						if(Txt_CashAmount.Text.Trim() !="" && Txt_CashAmount.Text !=null)
						{
							ErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"SHMPT_PRESENT_NO_UPD",utility.GetUserCulture());
							ViewState["isTextChanged"]=false;
							return;
						}
					}
					
				}
				else 
				{
					txtESA.Text="NO"; 
					txtESASurcharge.Text = ""; 
					strCashAmt=Txt_CashAmount.Text;
					if((int)ViewState["PRBOperation"]== (int)Operation.Update)
					{
						if(Txt_CashAmount.Text.Trim() !="" && Txt_CashAmount.Text !=null)
						{
							ErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_DEL_SHMPT_PRESENT",utility.GetUserCulture());
							ViewState["isTextChanged"]=false;
							return;
						}
					}
				}
			}
			if((Chk_NewCustomer.Checked =true )&& (Rd_Cash.Checked==true))
			{
				if(decESASurcharge!=0)
				{
					txtESA.Text="YES"; 
					txtESASurcharge.Text = decESASurcharge.ToString("#0.00"); 
				}
				else
				{
					txtESA.Text="NO"; 
					txtESASurcharge.Text = "";
				}
			}
	
			if(newCheck)
				Chk_NewCustomer.Checked = true;
			else
				Chk_NewCustomer.Checked = false;


	
		
		}

		private void Chk_SpecialRates_CheckedChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void btnRouteCode_Click(object sender, System.EventArgs e)
		{
			TextBox txtRouteCode = (TextBox)Page.FindControl("txtRouteCode");
			String strPickupRoute = null;
			String strtxtPickupRoute = null;
			String strBookNo = null;
			String strSenderZip = null;
			if(txtRouteCode != null)
			{
				strtxtPickupRoute = txtRouteCode.ClientID;
				strPickupRoute = txtRouteCode.Text;
			}
			strBookNo = Txt_BookingNo.Text;
			strSenderZip = txtSendZip.Text;
			String sUrl = "RouteCodePopup.aspx?FORMID=PickupRequestBooking&ROUTECODE_TEXT="+strtxtPickupRoute+"&ROUTECODE="+strPickupRoute+"&BOOKNO="+strBookNo+"&SENDZIP="+strSenderZip;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void txtRouteCode_TextChanged(object sender, System.EventArgs e)
		{
			ErrorMsg.Text= "";

			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}

		}

		//Phase2 - K04
		private bool IsEstLaterThanCuttOffTime() 
		{
			String strEstHr = "";
			String strEstMn = "";
			String strSendCutOffHr = "";
			String strSendCutOffMn = "";
			bool status = false;
			
			if (Txt_Estpickupdatetime.Text.Trim() != "") 
			{
				strEstHr = Txt_Estpickupdatetime.Text.Substring(11, 2);  
				strEstMn = Txt_Estpickupdatetime.Text.Substring(14, 2);  
			}

			if (txtSendCuttOffTime.Text.Trim() != "")
			{
				strSendCutOffHr = txtSendCuttOffTime.Text.Substring(0, 2); 
				strSendCutOffMn = txtSendCuttOffTime.Text.Substring(3, 2); 
			}

			int intEstHr = 0;
			int intEstMn = 0;
			int intSendCutOffHr = 0;
			int intSendCutOffMn = 0;

			if (IsNumeric(strEstHr))
				intEstHr = Convert.ToInt32(strEstHr.Trim());

			if (IsNumeric(strEstMn))
				intEstMn = Convert.ToInt32(strEstMn.Trim());

			if (IsNumeric(strSendCutOffHr))
				intSendCutOffHr = Convert.ToInt32(strSendCutOffHr.Trim());

			if (IsNumeric(strSendCutOffMn))
				intSendCutOffMn = Convert.ToInt32(strSendCutOffMn.Trim());

			DateTime tmpEstTime = new DateTime(1, 1, 1,intEstHr, intEstMn, 0);
			DateTime tmpSendCutOffTime = new DateTime(1, 1, 1, intSendCutOffHr, intSendCutOffMn, 0);

			System.TimeSpan tp = tmpEstTime.Subtract(tmpSendCutOffTime);

			if (tp.Hours > 0)
			{
				//Est.Time.Hour more then CuttOff.Time.Hour
				status =  true;
			}
			else if(tp.Hours == 0)
			{
				if(tp.Minutes > 0)
					//Est.Time.Hour equal CuttOff.Time.Hour
					//But Est.Time.Min more than CuttOff.Time.Min
					status = true;
			}

			return status;
		} 
		//Phase2 - K04

		//Phase2 - K04
		private bool IsNumeric(string str) 
		{
			try 
			{
				Convert.ToInt32(str);
				return true;
			}
			catch(FormatException) 
			{
				return false;
			}
		}
		//Phase2 - K04

		//Phase2 - K04
		private void btnExceedOK_Click(object sender, System.EventArgs e)
		{
			ExceedOK();
			ExceedTimeState.Visible = false;
			divMain.Visible = true;
		}
		//Phase2 - K04

		//Phase2 - K04
		private void btnExceedCancel_Click(object sender, System.EventArgs e)
		{			
			DataTable dtEst = this.EstimatedPickupDateData;
			dtEst.Rows.RemoveAt(0);
			dtEst.AcceptChanges();			
			this.EstimatedPickupDateData = dtEst;
			EstimatedPickupDate(null);
		} 
		//Phase2 - K04

		private void setRequiredFields(bool isVisible)
		{
			lblCustID.Visible = isVisible;
			lblName.Visible = isVisible;
			lblAddress.Visible = isVisible;
			lblPostalCode.Visible = isVisible;
			lblTele.Visible = isVisible;

			lblSenName.Visible = isVisible;
			lblSenAddress.Visible = isVisible;
			lblSenPostalCode.Visible = isVisible;
			lblSenCutOff.Visible = isVisible;
			lblSenContact.Visible = isVisible;
			lblSenTele.Visible = isVisible;

			//lblPkgEst.Visible = isVisible;
		}

		private void rd_Credit_CheckedChanged(object sender, System.EventArgs e)
		{
			ErrorMsg.Text= "";

			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePRBState();
				ViewState["isTextChanged"]=true;
			}
		}

		private void btnService_Click(object sender, System.EventArgs e)
		{
			String sUrl = "ServiceCodePopup.aspx?SendZipCode="+txtSendZip.Text.Trim()+
				"&FORMN=PickupRequestBooking";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);

			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
		protected bool CheckRoleM()
		{
			bool IsM = false;

			DbConnection dbCon = null;

			IDbCommand dbCmd = null;

			String strQry = null;

 

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(utility.GetAppID(),utility.GetEnterpriseID());

			strQry =  "SELECT  DISTINCT  User_Role_Relation.roleid,Role_Name " ;
			strQry += " FROM User_Role_Relation INNER JOIN ";
			strQry += " Role_Master ON User_Role_Relation.applicationid = Role_Master.applicationid AND User_Role_Relation.enterpriseid = Role_Master.enterpriseid AND ";
			strQry += " User_Role_Relation.roleid = Role_Master.roleid ";
			strQry += " WHERE User_Role_Relation.applicationid = '" + utility.GetAppID() + "' and User_Role_Relation.enterpriseid = '" + utility.GetEnterpriseID() + "' and userid = '"+ utility.GetUserID() + "'";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);

			DataSet roleDataSet =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);

			this.Btn_Insert.Enabled=false;
			this.Btn_Save.Enabled=false;
			this.Btn_Delete.Enabled=false;
			this.Btn_Delete.Visible=true;
			this.txtRouteCode.Enabled=false;
			this.btnRouteCode.Enabled=false;

			for(int i = 0; i < roleDataSet.Tables[0].Rows.Count;i++)
			{
				DataRow drEach = roleDataSet.Tables[0].Rows[i];

				int iRoleID = (int)drEach["roleid"];
				string strRoleName =(string)drEach["Role_Name"];
 

				if((strRoleName.ToUpper() =="CSMANAGER" ))
				{
					IsM = true;
					this.Btn_Insert.Enabled=true;
					this.Btn_Save.Enabled=true;
					this.Btn_Delete.Enabled=true;
					//break;
				}
				else if (strRoleName.ToUpper() =="CSSTAFF") 
				{
					IsM=true;
					this.Btn_Insert.Enabled=true;
					this.Btn_Save.Enabled=true;
					this.Btn_Delete.Visible=false;
					//break;
				}
				else if (strRoleName.ToUpper() =="CSSU") 
				{
					this.txtRouteCode.Enabled=true;
					this.btnRouteCode.Enabled=true;
				}
			}
			return IsM;

		}
		private void enableInput(bool b)
		{
			foreach(Control tmpControl in fs1.Controls)
			{
				if(tmpControl is msTextBox)
					((msTextBox)(tmpControl)).Enabled=b;
				else if (tmpControl is  TextBox)
					((TextBox)(tmpControl)).Enabled=b;
				else if (tmpControl is  DropDownList)
					((DropDownList)(tmpControl)).Enabled=b;
				else if (tmpControl is  CheckBox)
					((CheckBox)(tmpControl)).Enabled=b;
				else if (tmpControl is Button)
					((Button)(tmpControl)).Enabled=b;
			}
			foreach(Control tmpControl in fs2.Controls)
			{
				if(tmpControl is msTextBox)
					((msTextBox)(tmpControl)).Enabled=b;
				else if (tmpControl is  TextBox)
					((TextBox)(tmpControl)).Enabled=b;
				else if (tmpControl is  DropDownList)
					((DropDownList)(tmpControl)).Enabled=b;
				else if (tmpControl is  CheckBox)
					((CheckBox)(tmpControl)).Enabled=b;
				else if (tmpControl is Button)
					((Button)(tmpControl)).Enabled=b;
			}
			foreach(Control tmpControl in fs3.Controls)
			{
				if(tmpControl is msTextBox)
					((msTextBox)(tmpControl)).Enabled=b;
				else if (tmpControl is  TextBox)
					((TextBox)(tmpControl)).Enabled=b;
				else if (tmpControl is  DropDownList)
					((DropDownList)(tmpControl)).Enabled=b;
				else if (tmpControl is  CheckBox)
					if(tmpControl.ClientID == "Rd_FreightCollectd")
					{
						((CheckBox)(tmpControl)).Enabled=false;
					}
					else if(tmpControl.ClientID == "Rd_Cash" || tmpControl.ClientID == "rd_Credit")
					{
						((CheckBox)(tmpControl)).Enabled=b;
					}
					else
					{
						((CheckBox)(tmpControl)).Enabled=b;
					}
				else if (tmpControl is Button)
					((Button)(tmpControl)).Enabled=b;
			}
			this.BtnShipmentDetails.Enabled=true;
		}
		private void checkCash()
		{
			if(this.rd_Credit.Checked)
			{
				Txt_Totpkg.ReadOnly = false;
				Txt_ActualWeight.ReadOnly =false;
				Txt_DimWeight.ReadOnly =false;
			}
			else
			{
				Txt_Totpkg.ReadOnly= false;
				Txt_ActualWeight.ReadOnly =true;
				Txt_DimWeight.ReadOnly =true;
			}
		}

		private void EstimatedPickupDate(string PickupDT)
		{			
			DataTable dtEst = null;
			if(this.EstimatedPickupDateData == null)
			{
				dtEst = PRBMgrDAL.GetEstimatedPickupDate(utility.GetAppID(),utility.GetEnterpriseID(),txtSendZip.Text.Trim(),PickupDT);	
			}
			else
			{
				dtEst = this.EstimatedPickupDateData;
				if(dtEst != null && dtEst.Rows.Count>0 && dtEst.Rows[0]["Zipcode"].ToString() != txtSendZip.Text.Trim())
				{
					dtEst = PRBMgrDAL.GetEstimatedPickupDate(utility.GetAppID(),utility.GetEnterpriseID(),txtSendZip.Text.Trim(),PickupDT);	
				}
			}
			this.EstimatedPickupDateData = dtEst;
			if(dtEst != null)
			{
				if(dtEst.Rows.Count>0)
				{
					DataRow dr = dtEst.Rows[0];
					DateTime dRequestedPickupDT = (DateTime)dr["RequestedPickupDT"];
					DateTime dEst_PickupDT = (DateTime)dr["Est_PickupDT"];
					string sPrompt = dr["Prompt"].ToString();
					Txt_Estpickupdatetime.Text = dEst_PickupDT.ToString("dd/MM/yyyy HH:mm");
					if(sPrompt == "0")
					{
						if(dRequestedPickupDT.Date != dEst_PickupDT.Date)
						{
							if(EstimatedPickupDateType != PickupDateType.Today)
							{
								ExceedOK();
								EstimatedPickupDateType = PickupDateType.Tomorrow;
								ExceedTimeState.Visible = false; 
								divMain.Visible = true;								
							}
							else
							{
								Label1.Text="The Pickup Request is being made after the cut off time for Sender's Postal code. To override the standard cut off time and schedule the pickup at the time requested press Cancel. To reschedule the pickup to the time proposed based on the cut off time, press OK.";
								EstimatedPickupDateType = PickupDateType.Tomorrow;							
								ExceedTimeState.Visible = true; 
								divMain.Visible = false;
							}

						}
						else
						{
							EstimatedPickupDateType=PickupDateType.Today;
							ExceedOK();
						}	
					}
					else if(sPrompt == "1")
					{
						Label1.Text="Would the customer accept a surcharge for pickup on Saturday?";
						EstimatedPickupDateType = PickupDateType.Saturday;
						ExceedTimeState.Visible = true; 
						divMain.Visible = false;
					}
					else if(sPrompt == "2")
					{
						Label1.Text="Would the customer accept a surcharge for pickup on Sunday?";
						EstimatedPickupDateType = PickupDateType.Sunday;
						ExceedTimeState.Visible = true; 
						divMain.Visible = false;
					}
					else if(sPrompt == "3")
					{
						Label1.Text="Would the customer accept a surcharge for pickup on Public Holiday?";
						EstimatedPickupDateType = PickupDateType.Holiday;
						ExceedTimeState.Visible = true; 
						divMain.Visible = false;
					}
				}
				else
				{
					Txt_Estpickupdatetime.Text = Txt_BookingDate.Text;
					ExceedOK();
					EstimatedPickupDateType = PickupDateType.Today;
					ExceedTimeState.Visible = false; 
					divMain.Visible = true;					
				}
			}
			
		}

		private void ExceedOK()
		{
			if((int)ViewState["PRBOperation"] == (int)Operation.Update)
			{
				if(Txt_BookingNo.Text !="" && Txt_BookingNo.Text != null)
				{
					if(!DomesticShipmentMgrDAL.IsDomesticShpExist(utility.GetAppID(),utility.GetEnterpriseID(),Int32.Parse(Txt_BookingNo.Text)))
					{
						SavetoDatabase();
					}
					else
					{
						ErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"BKG_POSTED_MODIFY_DOMESTIC",utility.GetUserCulture());
						return;
					}
				}
				else
				{
					SavetoDatabase();
				}
			}
			else
			{
				SavetoDatabase();
			}
		}
		private void ExceedCancel()
		{
			// --> PRESS CANCEL
			// To reschedule the pick up
			// before the cut-off time.
			//ErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"PICKUP_DT_EXCEED",utility.GetUserCulture());
			DataTable dtPickupConsignment = null;
			DataTable dtPickupConsignmentPKG = null;
			DataTable dtPickupConsignmentVAS = null;
			DateTime cutofftime =Convert.ToDateTime(txtSendCuttOffTime.Text);
			Txt_Estpickupdatetime.Text = Txt_BookingDate.Text.Substring(0, 10)+" "+cutofftime.ToString("HH:mm");      
			if(System.DateTime.ParseExact(Txt_Estpickupdatetime.Text,"dd/MM/yyyy HH:mm",null) < System.DateTime.ParseExact(Txt_BookingDate.Text,"dd/MM/yyyy HH:mm",null))
				Txt_BookingDate.Text=Txt_Estpickupdatetime.Text;

			if(Session["SESSION_DTCon"] != null)
				dtPickupConsignment = (DataTable)Session["SESSION_DTCon"];
				
			if(Session["SESSION_DTPkg"] != null)
				dtPickupConsignmentPKG = (DataTable)Session["SESSION_DTPkg"];

			if(Session["SESSION_DTVAS"] != null)
				dtPickupConsignmentVAS = (DataTable)Session["SESSION_DTVAS"];
			if ((int)ViewState["PRBOperation"]== (int)Operation.Update||((int)ViewState["PRBOperation"]== (int)Operation.Saved)) 
			{
				ViewState["IsAcceptExceedsTime"] = false;
				ViewState["isTextChanged"] = false;
				InsertPickupRequest((int)ViewState["Crow"], "");
				DataSet dsPickupRequest = m_sdsPickupRequest.ds.GetChanges();
				int Rowsreturned = PRBMgrDAL.ModifyPickupRequest(utility.GetAppID(),utility.GetEnterpriseID(),dsPickupRequest);
				if(Session["SESSION_DTCon"] != null)
				{
					PRBMgrDAL.InsertPickupConsignment(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignment);
					PRBMgrDAL.InsertPickupConsignmentPkg(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignmentPKG);
					PRBMgrDAL.InsertPickupConsignmentVAS(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignmentVAS);
				}
				ErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
				ChangePRBState();
				m_sdsPickupRequest.ds.Tables[0].Rows[(int)ViewState["Crow"]].AcceptChanges();
				ExceedTimeState.Visible = false;
				divMain.Visible = true;
				setRequiredFields(false);
				return;
			}
			else if ((int)ViewState["PRBOperation"]==(int)Operation.Insert)  
			{
				try 
				{
					ViewState["IsAcceptExceedsTime"] = false;
					bkgno  = Counter.GetNext(utility.GetAppID(),utility.GetEnterpriseID(),"booking_number") ;
					Txt_BookingNo.Text = bkgno.ToString();
					InsertPickupRequest((int)ViewState["Crow"], "");
					DataSet dsPickupRequest = m_sdsPickupRequest.ds.GetChanges();

					InsertDispatch();
					int Rowsreturned = PRBMgrDAL.AddPickupRequest(utility.GetAppID(),utility.GetEnterpriseID(),dsPickupRequest,m_dsDispatch);
					if(Session["SESSION_DTCon"] != null)
					{
						PRBMgrDAL.InsertPickupConsignment(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignment);
						PRBMgrDAL.InsertPickupConsignmentPkg(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignmentPKG);
						PRBMgrDAL.InsertPickupConsignmentVAS(utility.GetAppID(),utility.GetEnterpriseID(),Txt_BookingNo.Text,dtPickupConsignmentVAS);
					}
					ChangePRBState();
					ViewState["isTextChanged"] = false;;
					m_sdsPickupRequest.ds.Tables[0].Rows[(int)ViewState["Crow"]].AcceptChanges();
					ErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INS_SAVED",utility.GetUserCulture());
					BtnShipmentDetails.Enabled =true; 
					ExceedTimeState.Visible = false;
					divMain.Visible = true;
					setRequiredFields(false);
				}
				catch(ApplicationException appException)
				{
					strMsg = appException.Message;
					if(strMsg.IndexOf("duplicate key") != -1 )
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
					}
					else if(strMsg.IndexOf("FK") != -1 )
					{
						strMsg = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
					}
					else
					{
						strMsg = appException.Message.ToString();
					}
					ErrorMsg.Text = strMsg; 
					ViewState["PRBOperation"]=Operation.None;  
					ExceedTimeState.Visible = false;
					divMain.Visible = true;
					return;
				}
			}
			else
			{
				ViewState["IsAcceptExceedsTime"] = false;
				ExceedTimeState.Visible = false;
				divMain.Visible = true;
				return;
			}
		}


		public void Check_Disable_PaymentMode_Checkbox()
		{
			Customer customer = new Customer();
			customer.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text.Trim());

			if((string)customer.PaymentMode == "R")
			{
				this.rd_Credit.Checked = true;
				this.Rd_Cash.Checked = false;
				this.Rd_Cash.Enabled = true;
				this.rd_Credit.Enabled = true;
			}
			else if((string)customer.PaymentMode == "C")
			{
				this.Txt_DimWeight.Text="";

				Rd_Cash.Checked = true;
				this.rd_Credit.Checked = false;
				Rd_Cash.Enabled = false;
				rd_Credit.Enabled = false;
	
				this.Txt_Totpkg.Enabled=true;
				this.Txt_ActualWeight.Enabled=false;
				this.Txt_DimWeight.Enabled=false;
			}
		}

		public void Disable_PaymentMode()
		{
			Rd_Cash.Enabled = true;
			rd_Credit.Enabled = true;

			Customer customerC = new Customer();
			customerC.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtCustID.Text.Trim());

			if((string)customerC.PaymentMode == "C")
			{
				this.Txt_DimWeight.Text="";
				Rd_Cash.Enabled = false;
				rd_Credit.Enabled = false;
	
				this.Txt_Totpkg.Enabled=true;
				this.Txt_ActualWeight.Enabled=false;
				this.Txt_DimWeight.Enabled=false;
			}
		}

		// Thosapol.y  > Add New Function	(01/07/2013)
		private string getRole(String strRole)
		{
			User user = new User();
			user.UserID = struserID;
			userRoleArray = com.common.RBAC.RBACManager.GetAllRoles(appID, enterpriseID, user);
			Role role;

			for(int i=0;i<userRoleArray.Count;i++)
			{
				role = (Role)userRoleArray[i];				
				if (role.RoleName.ToUpper().Trim() == strRole) return strRole;
			}
			return " ";
		}

	}		
}
