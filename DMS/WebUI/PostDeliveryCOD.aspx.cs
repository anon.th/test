using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.ties.BAL;
using com.common.applicationpages;
using System.Text;
using Cambro.Web.DbCombo;
using System.Xml;


namespace com.ties
{

	/// <summary>
	/// Summary description for PostDeliveryCOD.
	/// </summary>
	public class PostDeliveryCOD : BasePage
	{
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected com.common.util.msTextBox txtGoToRec;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.WebControls.Label lblConsgmtNo;
		protected System.Web.UI.WebControls.Label lblRefNo;
		protected System.Web.UI.WebControls.Label lblStatusCode;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected System.Web.UI.WebControls.Label lblExcepCode;
		protected System.Web.UI.WebControls.Label lblDelManifest;
		protected System.Web.UI.WebControls.Label lblCustInfo;
		protected System.Web.UI.WebControls.Label lblCustType;
		protected System.Web.UI.WebControls.Label lblCustID;
		protected System.Web.UI.WebControls.TextBox txtCustID;
		protected System.Web.UI.WebControls.Label lblName;
		protected System.Web.UI.WebControls.Label lblPaymode;
		protected System.Web.UI.WebControls.Label lblAddress;
		protected System.Web.UI.WebControls.Label lblTelphone;
		protected System.Web.UI.WebControls.Label lblFax;
		protected System.Web.UI.WebControls.Label lblZip;
		protected System.Web.UI.WebControls.Label lblSendInfo;
		protected System.Web.UI.WebControls.Label lblRecInfo;
		protected System.Web.UI.WebControls.Label lblRecipTelephone;
		protected System.Web.UI.WebControls.Label lblRecipNm;
		protected System.Web.UI.WebControls.Label lblRecpContPer;
		protected System.Web.UI.WebControls.Label lblRecipAddr1;
		protected System.Web.UI.WebControls.Label lblRecipFax;
		protected System.Web.UI.WebControls.Label lblRecipZip;
		protected System.Web.UI.WebControls.Label lblConf;
		protected System.Web.UI.WebControls.Label lblConfirmMsg;
		protected System.Web.UI.WebControls.Label lblCustIdCngMsg;
		protected System.Web.UI.WebControls.Button btnCustIdChgYes;
		protected System.Web.UI.WebControls.Button btnCustIdChgNo;
		protected System.Web.UI.WebControls.Label lblDelOperation;
		protected System.Web.UI.WebControls.Button btnDelOperationYes;
		protected System.Web.UI.WebControls.Button btnDelOperationNo;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Button btnUpdateAutoManifest;
		protected System.Web.UI.HtmlControls.HtmlTable tblRecepInfo;
		protected System.Web.UI.HtmlControls.HtmlGenericControl DomstcShipPanel;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divCustIdCng;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divDelOperation;
		protected System.Web.UI.HtmlControls.HtmlTable Table3;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divUpdateAutoManifest;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.HtmlControls.HtmlTable TblBookDtl;
		protected System.Web.UI.HtmlControls.HtmlTable Table4;

		protected System.Web.UI.WebControls.TextBox txtConsigNo;
		protected System.Web.UI.WebControls.TextBox txtCustRefNo;
		protected System.Web.UI.HtmlControls.HtmlTable Table5;
		protected System.Web.UI.WebControls.Label Label45;
		protected System.Web.UI.WebControls.Label Label48;
		protected System.Web.UI.WebControls.Label Label50;
		protected System.Web.UI.WebControls.Label Label52;
		protected System.Web.UI.HtmlControls.HtmlTable Table10;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label49;
		protected System.Web.UI.WebControls.Label Label46;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.WebControls.Label Label24;
		protected System.Web.UI.WebControls.Label Label62;
		protected System.Web.UI.WebControls.Label Label28;
		protected System.Web.UI.WebControls.Label Label30;
		protected System.Web.UI.WebControls.Label Label32;
		protected System.Web.UI.WebControls.Label Label36;
		protected System.Web.UI.WebControls.Label Label55;
		protected System.Web.UI.WebControls.Label Label57;
		protected System.Web.UI.WebControls.Label Label59;
		protected System.Web.UI.WebControls.Label Label61;
		protected System.Web.UI.WebControls.Label Label63;
		protected System.Web.UI.HtmlControls.HtmlTable Table7;
		protected System.Web.UI.WebControls.Label Label33;
		protected System.Web.UI.WebControls.Label Label34;
		protected System.Web.UI.WebControls.Label Label53;
		protected System.Web.UI.WebControls.Label Label47;
		protected System.Web.UI.WebControls.Label Label66;
		protected System.Web.UI.WebControls.Label Label77;
		protected System.Web.UI.WebControls.Label Label79;
		protected System.Web.UI.WebControls.Label Label83;
		protected System.Web.UI.WebControls.Label Label86;
		protected System.Web.UI.HtmlControls.HtmlTable Table8;
		protected System.Web.UI.WebControls.Label Label88;
		protected System.Web.UI.WebControls.Label Label90;
		protected System.Web.UI.WebControls.Label Label91;
		protected System.Web.UI.WebControls.Label Label97;
		protected System.Web.UI.WebControls.Label Label99;
		protected System.Web.UI.WebControls.Label Label101;
		protected System.Web.UI.WebControls.Label Label105;
		protected System.Web.UI.WebControls.Label Label108;
		protected System.Web.UI.HtmlControls.HtmlTable Table11;

		String userID = null;
		String appID = null;
		String enterpriseID = null;
		String strErrorMsg = "";
		String strBooking_no;
		String strConsig_no;
		bool boolSave=true;


		protected System.Web.UI.WebControls.Label lblNumRec;
		protected System.Web.UI.WebControls.Label lblSI_consigNo;
		protected System.Web.UI.WebControls.Label lblSI_CusRef;
		protected System.Web.UI.WebControls.Label lblSI_BookingDT;
		protected System.Web.UI.WebControls.Label lblSI_TotPackage;
		protected System.Web.UI.WebControls.Label lblSI_PickupDT;
		protected System.Web.UI.WebControls.Label lblSI_DelivRoute;
		protected System.Web.UI.WebControls.Label lblSI_ActualPOD;
		protected System.Web.UI.WebControls.Label lblSI_ServiceType;
		protected System.Web.UI.WebControls.Label lblSI_CODamVAS;
		protected System.Web.UI.WebControls.Label lblSI_CODam;

		static private int m_iSetSize = 4;
		protected com.common.util.msTextBox txtCODAmountCollectedDT;
		protected com.common.util.msTextBox txtNETCODAmountCollected_Cash;
		protected com.common.util.msTextBox txtNETCODAmountCollected_Check;
		protected com.common.util.msTextBox txtNETCODAmountCollected_Other;
		protected com.common.util.msTextBox txtWithholdingTaxAmount;
		protected System.Web.UI.WebControls.Label lblTotalCODAmount;
		protected System.Web.UI.WebControls.Label txtNetCODAmount;
		protected System.Web.UI.WebControls.TextBox txtCODRemark;
		protected System.Web.UI.WebControls.Label lblCODLastUserUpdated;
		protected System.Web.UI.WebControls.Label lblCODLastUpdateDT;
		protected com.common.util.msTextBox txtNETCODAmountVerifiedDT_Check;
		protected com.common.util.msTextBox txtNETCODAmountVerifiedDT_Other;
		protected com.common.util.msTextBox txtCODRemittanceToCustomerDT_Cash;
		protected com.common.util.msTextBox txtCODRemittanceToCustomerDT_Check;
		protected com.common.util.msTextBox txtCODRemittanceToCustomerDT_Other;
		protected com.common.util.msTextBox txtCODRemittanceAdvanceToCustomer;
		protected System.Web.UI.WebControls.TextBox txtCODRemittanceRemark;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label lblCI_CustType;
		protected System.Web.UI.WebControls.Label lblCI_CustID;
		protected System.Web.UI.WebControls.Label lblCI_PaymentMode;
		protected System.Web.UI.WebControls.Label lblCI_Tel;
		protected System.Web.UI.WebControls.Label lblCI_Fax;
		protected System.Web.UI.WebControls.Label lblCI_Name;
		protected System.Web.UI.WebControls.Label lblCI_Addr1;
		protected System.Web.UI.WebControls.Label lblCI_Addr2;
		protected System.Web.UI.WebControls.Label lblCI_PostalCode;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.Label Label26;
		protected System.Web.UI.WebControls.Label Label27;
		protected System.Web.UI.WebControls.Label Label29;
		protected System.Web.UI.WebControls.Label Fax;
		protected System.Web.UI.WebControls.Label Label25;
		protected System.Web.UI.WebControls.Label RI_Tel;
		protected System.Web.UI.WebControls.Label RI_Name;
		protected System.Web.UI.WebControls.Label RI_Addr1;
		protected System.Web.UI.WebControls.Label RI_Addr2;
		protected System.Web.UI.WebControls.Label RI_PostalCode;
		protected System.Web.UI.WebControls.Label RI_Country;
		protected System.Web.UI.WebControls.Label RI_ContactPerson;
		protected System.Web.UI.WebControls.Label RI_Fax;
		protected System.Web.UI.WebControls.Label RI_Province;
		protected Cambro.Web.DbCombo.DbCombo DbComboDestinationDC;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCode;
		protected System.Web.UI.WebControls.DropDownList ddlCODRefused;
		protected com.common.util.msTextBox txtNETCODAmountVerifiedDT_Cash;
		protected System.Web.UI.WebControls.Label Label69;
		protected System.Web.UI.WebControls.TextBox txtCODVerificationRemark;
		protected System.Web.UI.WebControls.Label Label68;
		protected System.Web.UI.WebControls.Label lblCODVLUser;
		protected System.Web.UI.WebControls.Label lblCODVLUpdateDT;
		protected System.Web.UI.WebControls.Label lblCODRLUpdateUser;
		protected System.Web.UI.WebControls.Label lblCODRLUpdateDT;
		protected System.Web.UI.WebControls.RangeValidator RangeValidator1;
		protected System.Web.UI.WebControls.RangeValidator RangeValidator2;
		protected System.Web.UI.WebControls.RangeValidator RangeValidator3;
		protected System.Web.UI.WebControls.TextBox txtAct_pod_FromX;
		protected System.Web.UI.WebControls.TextBox txtAct_Pod_toX;
		protected com.common.util.msTextBox txtAct_pod_From;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqAct_Pod_From;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqAct_Pod_to;
		protected com.common.util.msTextBox txtAct_Pod_to;
		protected System.Web.UI.WebControls.TextBox txtCODAmountCollectedDTX;
		protected System.Web.UI.WebControls.TextBox txtNETCODAmountVerifiedDT_CashX;
		protected System.Web.UI.WebControls.TextBox txtNETCODAmountVerifiedDT_CheckX;
		protected System.Web.UI.WebControls.TextBox txtNETCODAmountVerifiedDT_OtherX;
		protected System.Web.UI.WebControls.TextBox txtCODRemittanceToCustomerDT_CashX;
		protected System.Web.UI.WebControls.TextBox txtCODRemittanceToCustomerDT_CheckX;
		protected System.Web.UI.WebControls.TextBox txtCODRemittanceToCustomerDT_OtherX;
		protected System.Web.UI.WebControls.TextBox txtNETCODAmountCollected_CashX;
		protected System.Web.UI.WebControls.TextBox txtNETCODAmountCollected_CheckX;
		protected System.Web.UI.WebControls.TextBox txtNETCODAmountCollected_OtherX;
		protected System.Web.UI.WebControls.TextBox txtWithholdingTaxAmountX;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divTotalCODAmountCollected;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divCODREMITTANCE_DT;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label7;
		SessionDS m_sdsShipment = null;
		bool boolInvoiced = true;

//		DateTime T1;
//		String date, time;
		bool boolCODSUPERVISOR=true,boolCODVERIFY=true;
		bool boolUpdateUserCODAmountCollected=false,boolUpdateUserCODVerification=false,boolUpdateUserCODRemittance=false;
		protected System.Web.UI.WebControls.Button btnDivTotalYes;
		protected System.Web.UI.WebControls.Button btnDivTotalNo;
		protected System.Web.UI.WebControls.Button btnDivCODRemitYes;
		protected System.Web.UI.WebControls.Button btnDivCODRemitNo;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Button btnDivCODRefusedYes;
		protected System.Web.UI.WebControls.Button btnDivCODRfusedNo;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divCODRefused;
		protected System.Web.UI.WebControls.Label Label39;
		protected System.Web.UI.WebControls.Button btnDisplayCustDtls;
		protected System.Web.UI.WebControls.Label lblSDI_ContactPerson;
		protected System.Web.UI.WebControls.Label lblSendConPer;
		protected System.Web.UI.WebControls.Label lblSDI_Name;
		protected System.Web.UI.WebControls.Label lblSendNm;
		protected System.Web.UI.WebControls.Label lblSDI_Tel;
		protected System.Web.UI.WebControls.Label lblSendTel;
		protected System.Web.UI.WebControls.Label lblSDI_Addr1;
		protected System.Web.UI.WebControls.Label lblSendAddr1;
		protected System.Web.UI.WebControls.Label lblSDI_Fax;
		protected System.Web.UI.WebControls.Label lblSendFax;
		protected System.Web.UI.WebControls.Label lblSDI_Addr2;
		protected System.Web.UI.WebControls.Label lblSDI_PostalCode;
		protected System.Web.UI.WebControls.Label lblSDI_Province;
		protected System.Web.UI.WebControls.Label lblSDI_Country;
		protected System.Web.UI.WebControls.Label lblSendZip;
		protected System.Web.UI.WebControls.Label lblSDI_CutOffTime;
		protected System.Web.UI.WebControls.Label lblSendCuttOffTime;
		protected System.Web.UI.HtmlControls.HtmlTable tblSenderInfo;
		protected System.Web.UI.WebControls.Label lblCI_Country;
		protected System.Web.UI.WebControls.Label lblCI_Province;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divInvoiced;
		protected System.Web.UI.WebControls.Button btnInvoicedOK;
		protected System.Web.UI.WebControls.Button btnDivTotalCancel;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();
			DbComboDestinationDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			if (!IsPostBack)
			{
		
				ResetScreenForQuery();
				DataSet profileDS = SysDataManager1.GetEnterpriseProfile(appID, enterpriseID);

				if(profileDS.Tables[0].Rows.Count > 0)
				{
					int currency_decimal = (int) profileDS.Tables[0].Rows[0]["currency_decimal"];
					ViewState["m_format"] = "{0:F" + currency_decimal.ToString() + "}";
				}

				this.divCODREMITTANCE_DT.Visible=false;
				this.divTotalCODAmountCollected.Visible=false;
				this.divCODRefused.Visible=false;
				this.divInvoiced.Visible=false;

				ViewState["boolUpdateUserCODAmountCollected"]=false;
				ViewState["boolUpdateUserCODVerification"]=false;
				ViewState["boolUpdateUserCODRemittance"]=false;
				//this.divCODRefused
				//	btnDisplayCustDtls.Attributes.Add("onclick","ShowPupup()");
				//	Utility.RegisterScriptFile("DSCustomerAgent.js","CustomerAgentPopupDisplay",this.Page);
			}
			else
			{
				m_sdsShipment = (SessionDS)Session["SESSION_DS1"];
				this.boolUpdateUserCODAmountCollected=(bool)ViewState["boolUpdateUserCODAmountCollected"];
				this.boolUpdateUserCODVerification=(bool)ViewState["boolUpdateUserCODVerification"];
				this.boolUpdateUserCODRemittance=(bool)ViewState["boolUpdateUserCODRemittance"];		
			}

			string strQuery=null;
			if(Request.Params["hdnRefresh"] != null && Request.Params["hdnRefresh"] == "REFRESH")
			{
				strQuery=Request.Params["hdnRefresh"];
				if((bool)ViewState["isTextChanged"] == false)
				{
//					ChangeDSState();
					ViewState["isTextChanged"] = true;
				}
			}
			if(strQuery != null)
			{
//				refreshVAS();
			}

			if(this.ddlCODRefused.SelectedItem.Value=="Y")
			{
				//���ӹǳ COD ������¨��
				this.txtNETCODAmountCollected_Cash.NumberMinValue=0;
				this.txtNETCODAmountCollected_Check.NumberMinValue=0;
				this.txtNETCODAmountCollected_Other.NumberMinValue=0;
				this.txtWithholdingTaxAmount.NumberMinValue=0;
			}
			else
			{
				this.txtNETCODAmountCollected_Cash.NumberMinValue=1;
				this.txtNETCODAmountCollected_Check.NumberMinValue=1;
				this.txtNETCODAmountCollected_Other.NumberMinValue=1;
				this.txtWithholdingTaxAmount.NumberMinValue=1;
			}

			//���ᴧ ��� TOTAL COD AMT != COD AMT
//			if(!this.lblTotalCODAmount.Text.Equals(this.lblSI_CODam.Text))
//				this.lblTotalCODAmount.ForeColor = Color.Red;
//			else
//				this.lblTotalCODAmount.ForeColor = Color.Black;

			SetDbComboServerStates();

		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.txtGoToRec.TextChanged += new System.EventHandler(this.txtGoToRec_TextChanged);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.btnDisplayCustDtls.Click += new System.EventHandler(this.btnDisplayCustDtls_Click);
			this.txtNETCODAmountCollected_Cash.TextChanged += new System.EventHandler(this.txtNETCODAmountCollected_Cash_TextChanged);
			this.txtNETCODAmountCollected_Check.TextChanged += new System.EventHandler(this.txtNETCODAmountCollected_Check_TextChanged);
			this.txtNETCODAmountCollected_Other.TextChanged += new System.EventHandler(this.txtNETCODAmountCollected_Other_TextChanged);
			this.txtWithholdingTaxAmount.TextChanged += new System.EventHandler(this.txtWithholdingTaxAmount_TextChanged);
			this.btnDivTotalYes.Click += new System.EventHandler(this.btnDivTotalYes_Click);
			this.btnDivTotalNo.Click += new System.EventHandler(this.btnDivTotalNo_Click);
			this.btnDivTotalCancel.Click += new System.EventHandler(this.btnDivTotalCancel_Click);
			this.btnDivCODRemitYes.Click += new System.EventHandler(this.btnDivCODRemitYes_Click);
			this.btnDivCODRemitNo.Click += new System.EventHandler(this.btnDivCODRemitNo_Click);
			this.btnDivCODRefusedYes.Click += new System.EventHandler(this.btnDivCODRefusedYes_Click);
			this.btnDivCODRfusedNo.Click += new System.EventHandler(this.btnDivCODRfusedNo_Click);
			this.btnInvoicedOK.Click += new System.EventHandler(this.btnInvoicedOK_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void SetDbComboServerStates()
		{
			String strDeliveryType=null;
			strDeliveryType="S";

			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
			DbComboPathCode.ServerState = hash;
			DbComboPathCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboDistributionCenterSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,args);	
			return dataset;
		}



		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"'";
					if(strDelType == "S") 
					{
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct delivery_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
				}				
			}
			
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}


		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			
			FillShipmentDataRow(0);

			ViewState["QUERY_DS"] = m_sdsShipment.ds;

			ViewState["CPMode"] = ScreenMode.ExecuteQuery;
			ViewState["CPOperation"] = Operation.None;
			ViewState["IsTextChanged"] = false;
			ViewState["currentPage"] = 0;
			ViewState["currentSet"] = 0;
		
			GetRecSet();

			btnExecQry.Enabled = false;

			//btnInsert.Enabled = true;
			//Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);

			if(m_sdsShipment.QueryResultMaxSize == 0)
			{
				this.lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORDS_FOUND",utility.GetUserCulture());
				EnableNavigationButtons(false,false,false,false);
				return;
			}
			else
				this.lblErrorMsg.Text = "";



		//	LoadComboLists();
		//	EnableTabs(true,true,true,true,true,true);
		
			btnExecQry.Enabled = false;
			//btnInsert.Enabled = true;
	//		Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);



			if(m_sdsShipment != null && m_sdsShipment.DataSetRecSize > 0)
			{
				EnableNavigationButtons(false,false,true,true);
				this.btnSave.Enabled=true;
			}
			else
				this.btnSave.Enabled=false;

			DisplayCurrentPage();

			DisplayRecNum();

//			setQueryFR();
		}

		private void GetRecSet()
		{
			//int iStartIndex = Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize;
			String tmpStr = ViewState["currentSet"].ToString().Trim();
			int iStartIndex = 0;
			int intIndexOf = tmpStr.IndexOf(".");
	
			if (intIndexOf > 0)
			{
				iStartIndex = Convert.ToInt32(tmpStr.Substring(0, intIndexOf)) * m_iSetSize;
			}
			else
			{
				iStartIndex = Convert.ToInt32(tmpStr) * m_iSetSize;
			}
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsShipment = TIESDAL.PostDeliveryCOD.GetPostDeliveryCODDS(utility.GetAppID(),utility.GetEnterpriseID(),iStartIndex,m_iSetSize,dsQuery);
			Session["SESSION_DS1"] = m_sdsShipment;
			decimal pgCnt = (m_sdsShipment.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < Convert.ToInt32(ViewState["currentSet"]))
			{
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
			}
		}


		private void FillShipmentDataRow(int iCurrentRow)
		{
			DataRow drCurrent = m_sdsShipment.ds.Tables[0].Rows[iCurrentRow];

			TextBox txtConsigNo = (TextBox)this.FindControl("txtConsigNo");

			if(txtConsigNo.Text != "")
			{
				drCurrent["consignment_no"] = txtConsigNo.Text.Trim();
			}
			else
			{
				drCurrent["consignment_no"] = System.DBNull.Value;
			}

			if(txtCustRefNo.Text != "")
			{
				drCurrent["ref_no"] = txtCustRefNo.Text.Trim();
			}
			else
			{
				drCurrent["ref_no"] = System.DBNull.Value;
			}
		
			if(txtCustID.Text != "")
			{
				drCurrent["payerid"] = txtCustID.Text.Trim();
			}
			else
			{
				drCurrent["payerid"] = System.DBNull.Value;
			}


			if(this.DbComboPathCode.Text != "")
			{
				drCurrent["route_code"] = this.DbComboPathCode.Value;
			}
			else
			{
				drCurrent["route_code"] = System.DBNull.Value;
			}

			if(this.DbComboDestinationDC.Text != "")
			{
				drCurrent["destination_station"] = this.DbComboDestinationDC.Value;
			}
			else
			{
				drCurrent["destination_station"] = System.DBNull.Value;
			}


			if(this.ddlCODRefused.SelectedItem.Text != "")
			{
				drCurrent["cod_refused"] = this.ddlCODRefused.SelectedItem.Value;
			}
			else
			{
				drCurrent["cod_refused"] = System.DBNull.Value;
			}

			if(txtAct_pod_From.Text != "")
			{
				drCurrent["actual_POD_From"] = DateTime.ParseExact(txtAct_pod_From.Text,"dd/MM/yyyy",null); //txtAct_pod_From.Text;
			}
			else
			{
				drCurrent["actual_POD_From"] = System.DBNull.Value;
			}

			if(txtAct_Pod_to.Text != "")
			{
				drCurrent["actual_POD_To"] = DateTime.ParseExact(txtAct_Pod_to.Text + " " + "23:59:59","dd/MM/yyyy HH:mm:ss",null); //txtAct_Pod_to.Text;
			}
			else
			{
				drCurrent["actual_POD_To"] = System.DBNull.Value;
			}


		}


		
		Decimal cod_amt_collected_cash=0,cod_amt_collected_check=0,cod_amt_collected_other=0,
			wh_tax_amt=0,net_cod_amt=0,total_cod_amt=0;


		private bool FillShipmentDataRow_Update(int iCurrentRow)
		{

			DataRow drCurrent = m_sdsShipment.ds.Tables[0].Rows[iCurrentRow];

			this.boolUpdateUserCODAmountCollected=false;
			this.boolUpdateUserCODRemittance=false;
			this.boolUpdateUserCODVerification=false;

			ViewState["boolUpdateUserCODAmountCollected"]=false;
			ViewState["boolUpdateUserCODVerification"]=false;
			ViewState["boolUpdateUserCODRemittance"]=false;


//			if((this.txtNETCODAmountCollected_Cash.Text==""||this.txtNETCODAmountCollected_Cash.Text=="0.00"||this.txtNETCODAmountCollected_Cash.Text=="0")&&(this.txtNETCODAmountCollected_Check.Text==""||this.txtNETCODAmountCollected_Check.Text=="0.00"||this.txtNETCODAmountCollected_Check.Text=="0")&&
//				(this.txtNETCODAmountCollected_Other.Text==""||this.txtNETCODAmountCollected_Other.Text=="0.00"||this.txtNETCODAmountCollected_Other.Text=="0")&&
//				(this.txtWithholdingTaxAmount.Text==""||this.txtWithholdingTaxAmount.Text=="0.00"||this.txtWithholdingTaxAmount.Text=="0")&&this.ddlCODRefused.SelectedIndex==0)
//				return false;

//			if((this.txtNETCODAmountCollected_Cash.Text != "" ||!(this.txtNETCODAmountCollected_Cash.Text=="" && Convert.ToString(ViewState["cod_amt_collected_cash"])=="0"))&& this.txtNETCODAmountCollected_Cash.Text!=Convert.ToString(ViewState["cod_amt_collected_cash"]) )
			if((this.txtNETCODAmountCollected_Cash.Text != "" &&this.txtNETCODAmountCollected_Cash.Text!=Convert.ToString(ViewState["cod_amt_collected_cash"]))
				||(this.txtNETCODAmountCollected_Cash.Text==""&&ViewState["cod_amt_collected_cash"]!=null&&Convert.ToString(ViewState["cod_amt_collected_cash"])!="0")
				)
		{
				if(this.txtNETCODAmountCollected_Cash.Text != "")
                    drCurrent["cod_amt_collected_cash"] = txtNETCODAmountCollected_Cash.Text;
				else
					drCurrent["cod_amt_collected_cash"] = 0;
					
//				if(this.txtNETCODAmountCollected_Cash.Text == "" && this.ddlCODRefused.SelectedIndex==0)
//					return false;
//				if(this.txtNETCODAmountCollected_Cash.Text == "" && this.ddlCODRefused.SelectedIndex==1)
//					drCurrent["cod_amt_collected_cash"] = 0;
					//ⴹ��䢤�� ������� null ���� COD Refused �ѧ��ҡѺ NO ��������� ���͡��¨��//drCurrent["cod_amt_collected_cash"] = System.DBNull.Value;
					this.boolUpdateUserCODAmountCollected=true;
			}
			else
			{
				drCurrent["cod_amt_collected_cash"] = System.DBNull.Value;
			}
		
//			if((this.txtNETCODAmountCollected_Check.Text != "" ||!(this.txtNETCODAmountCollected_Check.Text=="" && Convert.ToString(ViewState["cod_amt_collected_check"])=="0"))&& this.txtNETCODAmountCollected_Check.Text!=Convert.ToString(ViewState["cod_amt_collected_check"]))
			if((this.txtNETCODAmountCollected_Check.Text != "" &&this.txtNETCODAmountCollected_Check.Text!=Convert.ToString(ViewState["cod_amt_collected_check"]))
				||(this.txtNETCODAmountCollected_Check.Text==""&&ViewState["cod_amt_collected_check"]!=null&&Convert.ToString(ViewState["cod_amt_collected_check"])!="0")
				)
			{
				if(this.txtNETCODAmountCollected_Check.Text != "")
                    drCurrent["cod_amt_collected_check"] = txtNETCODAmountCollected_Check.Text;
				else
					drCurrent["cod_amt_collected_check"] = 0;

//				if(this.txtNETCODAmountCollected_Check.Text == ""&& this.ddlCODRefused.SelectedIndex==0)
//					return false;
//				if(this.txtNETCODAmountCollected_Check.Text == ""&& this.ddlCODRefused.SelectedIndex==1)
//					drCurrent["cod_amt_collected_check"] = 0;

				this.boolUpdateUserCODAmountCollected=true;
			}
			else
			{
				drCurrent["cod_amt_collected_check"] = System.DBNull.Value;
			}

//			if((this.txtNETCODAmountCollected_Other.Text != "" || !(this.txtNETCODAmountCollected_Other.Text=="" && Convert.ToString(ViewState["cod_amt_collected_other"])=="0"))&& this.txtNETCODAmountCollected_Other.Text!=Convert.ToString(ViewState["cod_amt_collected_other"]))
			if((this.txtNETCODAmountCollected_Other.Text != "" &&this.txtNETCODAmountCollected_Other.Text!=Convert.ToString(ViewState["cod_amt_collected_other"]))
				||(this.txtNETCODAmountCollected_Other.Text==""&&ViewState["cod_amt_collected_other"]!=null&&Convert.ToString(ViewState["cod_amt_collected_other"])!="0")
				)
			{
				if(this.txtNETCODAmountCollected_Other.Text != "")
					drCurrent["cod_amt_collected_other"] = txtNETCODAmountCollected_Other.Text;
				else
					drCurrent["cod_amt_collected_other"] = 0;

//				if(this.txtNETCODAmountCollected_Other.Text == ""&& this.ddlCODRefused.SelectedIndex==0)
//					return false;
//				if(this.txtNETCODAmountCollected_Other.Text == ""&& this.ddlCODRefused.SelectedIndex==1)
//					drCurrent["cod_amt_collected_other"] = 0;

				this.boolUpdateUserCODAmountCollected=true;
			}
			else
			{
				drCurrent["cod_amt_collected_other"] = System.DBNull.Value;
			}

			if(this.ddlCODRefused.SelectedItem.Value!= ""&& this.ddlCODRefused.SelectedItem.Value!=Convert.ToString(ViewState["cod_refused"]))
			{
				drCurrent["cod_refused"] = this.ddlCODRefused.SelectedItem.Value;
				this.boolUpdateUserCODAmountCollected=true;
			}
			else
			{
//				if(this.ddlCODRefused.SelectedItem.Value!= "")
					drCurrent["cod_refused"] = this.ddlCODRefused.SelectedItem.Value;
//                else
//					drCurrent["cod_refused"] = System.DBNull.Value;
			}

//			if((this.txtWithholdingTaxAmount.Text != "" || !(this.txtWithholdingTaxAmount.Text=="" && Convert.ToString(ViewState["wh_tax_amt"])=="0")) && this.txtWithholdingTaxAmount.Text!=Convert.ToString(ViewState["wh_tax_amt"]))
			if((this.txtWithholdingTaxAmount.Text != "" &&this.txtWithholdingTaxAmount.Text!=Convert.ToString(ViewState["wh_tax_amt"]))
				||(this.txtWithholdingTaxAmount.Text==""&&ViewState["wh_tax_amt"]!=null&&Convert.ToString(ViewState["wh_tax_amt"])!="0")
				)
			{
				if(this.txtWithholdingTaxAmount.Text != "")
					drCurrent["wh_tax_amt"] = this.txtWithholdingTaxAmount.Text;
				else
					drCurrent["wh_tax_amt"] =  0;
//				if(this.txtWithholdingTaxAmount.Text == ""&& this.ddlCODRefused.SelectedIndex==0)
//					return false;
//				if(this.txtWithholdingTaxAmount.Text == ""&& this.ddlCODRefused.SelectedIndex==1)
//					drCurrent["wh_tax_amt"] = 0;

				this.boolUpdateUserCODAmountCollected=true;
			}
			else
			{
				drCurrent["wh_tax_amt"] = System.DBNull.Value;
			}

			if(this.txtNETCODAmountCollected_Cash.Text != "")
				cod_amt_collected_cash = Convert.ToDecimal(txtNETCODAmountCollected_Cash.Text);
			else
				cod_amt_collected_cash = 0;

			if(this.txtNETCODAmountCollected_Check.Text != "")
				cod_amt_collected_check = Convert.ToDecimal(txtNETCODAmountCollected_Check.Text);
			else
				cod_amt_collected_check = 0;

			if(this.txtNETCODAmountCollected_Other.Text != "")
				cod_amt_collected_other = Convert.ToDecimal(txtNETCODAmountCollected_Other.Text);
			else
				cod_amt_collected_other = 0;

			if(this.txtWithholdingTaxAmount.Text != "")
				wh_tax_amt = Convert.ToDecimal(this.txtWithholdingTaxAmount.Text);
			else
				wh_tax_amt = 0;

			if(this.txtNETCODAmountCollected_Cash.Text!=Convert.ToString(ViewState["cod_amt_collected_cash"])||this.txtNETCODAmountCollected_Check.Text!=Convert.ToString(ViewState["cod_amt_collected_check"])||this.txtNETCODAmountCollected_Other.Text!=Convert.ToString(ViewState["cod_amt_collected_other"])|| this.txtWithholdingTaxAmount.Text!=Convert.ToString(ViewState["wh_tax_amt"]))
			{	net_cod_amt = cod_amt_collected_cash + cod_amt_collected_check + cod_amt_collected_other;
				total_cod_amt = cod_amt_collected_cash + cod_amt_collected_check + cod_amt_collected_other + wh_tax_amt;

				drCurrent["net_cod_amt"] = net_cod_amt;
				drCurrent["total_cod_amt"] = total_cod_amt;
			}
			else
			{
				drCurrent["net_cod_amt"] = System.DBNull.Value;
				drCurrent["total_cod_amt"] = System.DBNull.Value;
			}


			if((this.txtCODRemark.Text != ""||this.txtCODRemark.Text == "")&& this.txtCODRemark.Text!=Convert.ToString(ViewState["cod_remark"]))
			{
				if(this.txtCODRemark.Text != "")
					drCurrent["cod_remark"] = this.txtCODRemark.Text;
				else
					drCurrent["cod_remark"] = "";

				this.boolUpdateUserCODAmountCollected=true;
			}
			else
			{
				drCurrent["cod_remark"] = System.DBNull.Value;
			}

//			if((this.boolUpdateUserCODAmountCollected&&ViewState["cod_amt_collected_datetime"]==null) ||
//				(ViewState["cod_amt_collected_datetime"]!=null&&this.txtCODAmountCollectedDT.Text!=""
//				&& this.txtCODAmountCollectedDT.Text!=Convert.ToString(ViewState["cod_amt_collected_datetime"]))||
//				(ViewState["cod_amt_collected_datetime"]==null&&(this.txtCODAmountCollectedDT.Text!=this.lblSI_ActualPOD.Text)))
			if((this.boolUpdateUserCODAmountCollected)||
				(this.txtCODAmountCollectedDT.Text!=this.lblSI_ActualPOD.Text)||
				(this.txtCODAmountCollectedDT.Text!=Convert.ToString(ViewState["cod_amt_collected_datetime"])&&ViewState["cod_amt_collected_datetime"]!=null))
			{
				drCurrent["cod_amt_collected_datetime"] = DateTime.ParseExact(txtCODAmountCollectedDT.Text,"dd/MM/yyyy HH:mm",null);
				this.boolUpdateUserCODAmountCollected=true;
			}
			else
			{
				drCurrent["cod_amt_collected_datetime"] = System.DBNull.Value;
			}


			//COD VERIFICATION
			//if(Convert.ToBoolean((ViewState["CODVERIFY"]).Equals(false)))
			if(boolCODVERIFY==false)
			{ //����ѹ�������Ѿഷ COD VERIFICATION �����ѹ��� ����ͧʹ� ��� DBNULL �Ŵ
				drCurrent["cod_amt_verified_datetime_cash"] = System.DBNull.Value;
				drCurrent["cod_amt_verified_datetime_check"] = System.DBNull.Value;
				drCurrent["cod_amt_verified_datetime_other"] = System.DBNull.Value;
				drCurrent["cod_verified_remark"] = System.DBNull.Value;
				
			}
			else
			{
				if(this.txtNETCODAmountVerifiedDT_Cash.Text.Length==10)
				{
					drCurrent["cod_amt_verified_datetime_cash"] = DateTime.ParseExact(txtNETCODAmountVerifiedDT_Cash.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null);
					this.boolUpdateUserCODVerification=true;
				}
				else
				{
					if(this.txtNETCODAmountVerifiedDT_Cash.Text==""&&this.txtNETCODAmountVerifiedDT_Cash.Text!=Convert.ToString(ViewState["cod_amt_verified_datetime_cash"]))
					{
						drCurrent["cod_amt_verified_datetime_cash"] = Convert.ToDateTime("01/01/1900 00:00").ToString("dd/MM/yyyy HH:mm");
						this.boolUpdateUserCODVerification=true;
					}
					else
						drCurrent["cod_amt_verified_datetime_cash"] = System.DBNull.Value;
				}

				if(this.txtNETCODAmountVerifiedDT_Check.Text.Length==10)
				{
					drCurrent["cod_amt_verified_datetime_check"] = DateTime.ParseExact(txtNETCODAmountVerifiedDT_Check.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null);
					this.boolUpdateUserCODVerification=true;
				}
				else
				{
					if(this.txtNETCODAmountVerifiedDT_Check.Text==""&&this.txtNETCODAmountVerifiedDT_Check.Text!=Convert.ToString(ViewState["cod_amt_verified_datetime_check"]))
					{
						drCurrent["cod_amt_verified_datetime_check"] = Convert.ToDateTime("01/01/1900 00:00").ToString("dd/MM/yyyy HH:mm");
						this.boolUpdateUserCODVerification=true;
					}
					else
						drCurrent["cod_amt_verified_datetime_check"] = System.DBNull.Value;
				}

				if(this.txtNETCODAmountVerifiedDT_Other.Text.Length==10)
				{
					drCurrent["cod_amt_verified_datetime_other"] = DateTime.ParseExact(txtNETCODAmountVerifiedDT_Other.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null);
					this.boolUpdateUserCODVerification=true;
				}
				else
				{
					if(this.txtNETCODAmountVerifiedDT_Other.Text==""&&this.txtNETCODAmountVerifiedDT_Other.Text!=Convert.ToString(ViewState["cod_amt_verified_datetime_other"]))
					{
						drCurrent["cod_amt_verified_datetime_other"] = Convert.ToDateTime("01/01/1900 00:00").ToString("dd/MM/yyyy HH:mm");
						this.boolUpdateUserCODVerification=true;
					}
					else
						drCurrent["cod_amt_verified_datetime_other"] = System.DBNull.Value;
				}

				if((this.txtCODVerificationRemark.Text != ""||this.txtCODVerificationRemark.Text == "") && this.txtCODVerificationRemark.Text!=Convert.ToString(ViewState["cod_verified_remark"]))
				{
					if(this.txtCODVerificationRemark.Text != "")
						drCurrent["cod_verified_remark"] = this.txtCODVerificationRemark.Text;
					else
						drCurrent["cod_verified_remark"] = "";

					this.boolUpdateUserCODVerification=true;
				}
				else
				{
					drCurrent["cod_verified_remark"] = System.DBNull.Value;
				}
			}

//REMITTANCE
			//if(Convert.ToBoolean(ViewState["CODSUPERVISOR"]).Equals(false))
			if(boolCODSUPERVISOR==false)
			{ //����ѹ�������Ѿഷ CODSUPERVISOR �����ѹ��� ����ͧʹ� ��� DBNULL �Ŵ
				drCurrent["cod_remittance_to_customer_datetime_cash"] = System.DBNull.Value;
				drCurrent["cod_remittance_to_customer_datetime_check"] = System.DBNull.Value;
				drCurrent["cod_remittance_to_customer_datetime_other"] = System.DBNull.Value;
				drCurrent["cod_adv_to_customer"] = System.DBNull.Value;
 			}
			else
			{
				if(this.txtCODRemittanceToCustomerDT_Cash.Text.Length==10)
				{
					drCurrent["cod_remittance_to_customer_datetime_cash"] = DateTime.ParseExact(txtCODRemittanceToCustomerDT_Cash.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null);
					this.boolUpdateUserCODRemittance=true;
				}
				else
				{
					if(this.txtCODRemittanceToCustomerDT_Cash.Text==""&&this.txtCODRemittanceToCustomerDT_Cash.Text!=Convert.ToString(ViewState["cod_remittance_to_customer_datetime_cash"]))
					{
						drCurrent["cod_remittance_to_customer_datetime_cash"] = Convert.ToDateTime("01/01/1900 00:00").ToString("dd/MM/yyyy HH:mm");
						this.boolUpdateUserCODRemittance=true;
					}
					else
						drCurrent["cod_remittance_to_customer_datetime_cash"] = System.DBNull.Value;
				}


				if(this.txtCODRemittanceToCustomerDT_Check.Text.Length==10)
				{
					drCurrent["cod_remittance_to_customer_datetime_check"] = DateTime.ParseExact(txtCODRemittanceToCustomerDT_Check.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null);
					this.boolUpdateUserCODRemittance=true;
				}
				else
				{
					if(this.txtCODRemittanceToCustomerDT_Check.Text==""&&this.txtCODRemittanceToCustomerDT_Check.Text!=Convert.ToString(ViewState["cod_remittance_to_customer_datetime_check"]))
					{
						drCurrent["cod_remittance_to_customer_datetime_check"] = Convert.ToDateTime("01/01/1900 00:00").ToString("dd/MM/yyyy HH:mm");
						this.boolUpdateUserCODRemittance=true;
					}
					else
						drCurrent["cod_remittance_to_customer_datetime_check"] = System.DBNull.Value;
				}


				if(this.txtCODRemittanceToCustomerDT_Other.Text.Length==10)
				{
					drCurrent["cod_remittance_to_customer_datetime_other"] = DateTime.ParseExact(txtCODRemittanceToCustomerDT_Other.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null);
					this.boolUpdateUserCODRemittance=true;
				}
				else
				{
					if(this.txtCODRemittanceToCustomerDT_Other.Text==""&&this.txtCODRemittanceToCustomerDT_Other.Text!=Convert.ToString(ViewState["cod_remittance_to_customer_datetime_other"]))
					{
						drCurrent["cod_remittance_to_customer_datetime_other"] = Convert.ToDateTime("01/01/1900 00:00").ToString("dd/MM/yyyy HH:mm");
						this.boolUpdateUserCODRemittance=true;
					}
					else
						drCurrent["cod_remittance_to_customer_datetime_other"] = System.DBNull.Value;
				}


				if((this.txtCODRemittanceAdvanceToCustomer.Text!=""||this.txtCODRemittanceAdvanceToCustomer.Text=="") && this.txtCODRemittanceAdvanceToCustomer.Text!=Convert.ToString(ViewState["cod_adv_to_customer"]))
				{
					if(this.txtCODRemittanceAdvanceToCustomer.Text!="")
					{
						drCurrent["cod_adv_to_customer"] = txtCODRemittanceAdvanceToCustomer.Text;
						this.boolUpdateUserCODRemittance=true;
					}
					else
					{
						if(this.txtCODRemittanceAdvanceToCustomer.Enabled==false)
							drCurrent["cod_adv_to_customer"] = System.DBNull.Value;
						else
						{
							drCurrent["cod_adv_to_customer"] = 0;
							this.boolUpdateUserCODRemittance=true;
						}
					}
				}
				else
				{
					drCurrent["cod_adv_to_customer"] = System.DBNull.Value;
				}

				if((this.txtCODRemittanceRemark.Text != ""||this.txtCODRemittanceRemark.Text == "") && this.txtCODRemittanceRemark.Text!=Convert.ToString(ViewState["cod_remittance_remark"]))
				{
					if(this.txtCODRemittanceRemark.Text != "")
						drCurrent["cod_remittance_remark"] = this.txtCODRemittanceRemark.Text;
					else
						drCurrent["cod_remittance_remark"] = "";

					this.boolUpdateUserCODRemittance=true;
				}
				else
				{
					drCurrent["cod_remittance_remark"] = System.DBNull.Value;
				}

			}

            return true;

		}


		private DataRow FillShipmentDataRow_CODHistory()
		{
			
			SessionDS mm = TIESDAL.PostDeliveryCOD.GetEmptyShipmentDS(1);
			DataRow drCurrent = mm.ds.Tables[0].Rows[0];
			
			drCurrent["net_cod_amt"] = Double.Parse(txtNetCODAmount.Text);
			if(this.txtNETCODAmountCollected_Cash.Text.Length>0)
				drCurrent["cod_amt_collected_cash"] = Double.Parse(txtNETCODAmountCollected_Cash.Text);//cod_amt_collected_cash;
			else
				drCurrent["cod_amt_collected_cash"] = 0;

			if(this.txtNETCODAmountCollected_Check.Text.Length>0)
				drCurrent["cod_amt_collected_check"] = Double.Parse(txtNETCODAmountCollected_Check.Text);
			else
				drCurrent["cod_amt_collected_check"] = 0;

			if(this.txtNETCODAmountCollected_Other.Text.Length>0)
				drCurrent["cod_amt_collected_other"] = Double.Parse(this.txtNETCODAmountCollected_Other.Text);
			else
				drCurrent["cod_amt_collected_other"] = 0;

			//if((this.boolUpdateUserCODAmountCollected&&C(ViewState["cod_amt_collected_datetime"]))||this.txtCODAmountCollectedDT.Text!="" && this.txtCODAmountCollectedDT.Text!=Convert.ToString(ViewState["cod_amt_collected_datetime"]))
			if((this.boolUpdateUserCODAmountCollected&&ViewState["cod_amt_collected_datetime"]==null) ||
				(ViewState["cod_amt_collected_datetime"]!=null&&this.txtCODAmountCollectedDT.Text!=""
				&& this.txtCODAmountCollectedDT.Text!=Convert.ToString(ViewState["cod_amt_collected_datetime"]))||
				(ViewState["cod_amt_collected_datetime"]==null&&(this.txtCODAmountCollectedDT.Text!=this.lblSI_ActualPOD.Text)))
			{ //IN CASE OF EDIT / ADD NEW - IT SHOULD HAVE VALUE
				//(CHANGE FROM BLANK TO VALUE, OR CHANGE FROM VALUE TO NEW VALUE
				drCurrent["cod_amt_collected_datetime"] = DateTime.ParseExact(txtCODAmountCollectedDT.Text,"dd/MM/yyyy HH:mm",null);
			}
			else//IN CASE OF BLANK (AND IT HAS NOT THE SAME VALUE) MEANS DELETED
				if(this.txtCODAmountCollectedDT.Text=="" && this.txtCODAmountCollectedDT.Text!=Convert.ToString(ViewState["cod_amt_collected_datetime"]))
					drCurrent["cod_amt_collected_datetime"] = System.DBNull.Value;
				else // IN CASE OF DO NOTHING (NOTHING CHANGES)
					drCurrent["cod_amt_collected_datetime"] = System.DateTime.MinValue;


//			if(this.txtCODAmountCollectedDT.Text!="" && this.txtCODAmountCollectedDT.Text!=Convert.ToString(ViewState["cod_amt_collected_datetime"]))
//				drCurrent["cod_amt_collected_datetime"] = DateTime.ParseExact(txtCODAmountCollectedDT.Text,"dd/MM/yyyy HH:mm",null);
//			else
//				if(this.txtCODAmountCollectedDT.Text=="" && this.txtCODAmountCollectedDT.Text!=Convert.ToString(ViewState["cod_amt_collected_datetime"]))
//					drCurrent["cod_amt_collected_datetime"] = System.DBNull.Value;
//				else
//					drCurrent["cod_amt_collected_datetime"] = System.DateTime.MinValue;

//VERIFIED DT
			if(this.txtNETCODAmountVerifiedDT_Cash.Text.Length==10)
				drCurrent["cod_amt_verified_datetime_cash"] = DateTime.ParseExact(txtNETCODAmountVerifiedDT_Cash.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null);
			else
				if(this.txtNETCODAmountVerifiedDT_Cash.Text==""&&this.txtNETCODAmountVerifiedDT_Cash.Text!=Convert.ToString(ViewState["cod_amt_verified_datetime_cash"]))
					drCurrent["cod_amt_verified_datetime_cash"] = System.DBNull.Value;
				else
					drCurrent["cod_amt_verified_datetime_cash"] = DateTime.MinValue;
		

			if(this.txtNETCODAmountVerifiedDT_Check.Text.Length==10)
				drCurrent["cod_amt_verified_datetime_check"] = DateTime.ParseExact(txtNETCODAmountVerifiedDT_Check.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null);
			else
				if(this.txtNETCODAmountVerifiedDT_Check.Text==""&&this.txtNETCODAmountVerifiedDT_Check.Text!=Convert.ToString(ViewState["cod_amt_verified_datetime_check"]))
					drCurrent["cod_amt_verified_datetime_check"] = System.DBNull.Value;
				else
					drCurrent["cod_amt_verified_datetime_check"] = DateTime.MinValue;

			if(this.txtNETCODAmountVerifiedDT_Other.Text.Length==10)
				drCurrent["cod_amt_verified_datetime_other"] = DateTime.ParseExact(txtNETCODAmountVerifiedDT_Other.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null);
			else
				if(this.txtNETCODAmountVerifiedDT_Other.Text==""&&this.txtNETCODAmountVerifiedDT_Other.Text!=Convert.ToString(ViewState["cod_amt_verified_datetime_other"]))
					drCurrent["cod_amt_verified_datetime_other"] = System.DBNull.Value;
				else
					drCurrent["cod_amt_verified_datetime_other"] = DateTime.MinValue;

//END VERIFIED DT

//REMITTANCE DT
			if(this.txtCODRemittanceToCustomerDT_Cash.Text.Length==10)
				drCurrent["cod_remittance_to_customer_datetime_cash"] = DateTime.ParseExact(txtCODRemittanceToCustomerDT_Cash.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null);
			else
				if(this.txtCODRemittanceToCustomerDT_Cash.Text==""&&this.txtCODRemittanceToCustomerDT_Cash.Text!=Convert.ToString(ViewState["cod_remittance_to_customer_datetime_cash"]))
					drCurrent["cod_remittance_to_customer_datetime_cash"] = System.DBNull.Value;
				else
					drCurrent["cod_remittance_to_customer_datetime_cash"] = DateTime.MinValue;

			if(this.txtCODRemittanceToCustomerDT_Check.Text.Length==10)
				drCurrent["cod_remittance_to_customer_datetime_check"] = DateTime.ParseExact(txtCODRemittanceToCustomerDT_Check.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null);
			else
				if(this.txtCODRemittanceToCustomerDT_Check.Text==""&&this.txtCODRemittanceToCustomerDT_Check.Text!=Convert.ToString(ViewState["cod_remittance_to_customer_datetime_check"]))
					drCurrent["cod_remittance_to_customer_datetime_check"] = System.DBNull.Value;
				else
					drCurrent["cod_remittance_to_customer_datetime_check"] = DateTime.MinValue;


			if(this.txtCODRemittanceToCustomerDT_Other.Text.Length==10)
				drCurrent["cod_remittance_to_customer_datetime_other"] = DateTime.ParseExact(txtCODRemittanceToCustomerDT_Other.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null);
			else
				if(this.txtCODRemittanceToCustomerDT_Other.Text==""&&this.txtCODRemittanceToCustomerDT_Other.Text!=Convert.ToString(ViewState["cod_remittance_to_customer_datetime_other"]))
					drCurrent["cod_remittance_to_customer_datetime_other"] = System.DBNull.Value;
				else
					drCurrent["cod_remittance_to_customer_datetime_other"] = DateTime.MinValue;

//END REMITTANCE DT
			return drCurrent;			

		}

		private void ResetScreenForQuery()
		{
			m_sdsShipment = TIESDAL.PostDeliveryCOD.GetEmptyShipmentDS(1);
			Session["SESSION_DS1"] = m_sdsShipment;

			ViewState["CPMode"] = ScreenMode.Query;
			ViewState["CPOperation"] = Operation.None;
			ViewState["IsTextChanged"] = false;
			ViewState["currentPage"] = 0;
			
			boolUpdateUserCODAmountCollected=false;
			boolUpdateUserCODVerification=false;
			boolUpdateUserCODRemittance=false;

			ViewState["boolUpdateUserCODAmountCollected"]=false;
			ViewState["boolUpdateUserCODVerification"]=false;
			ViewState["boolUpdateUserCODRemittance"]=false;

			DisplayCurrentPage();
			lblNumRec.Text = "";
			this.lblErrorMsg.Text = "";
			btnExecQry.Enabled = true;
			btnSave.Enabled=false;
			btnGoToFirstPage.Enabled = false;
			btnGoToLastPage.Enabled = false;
			btnPreviousPage.Enabled = false;
			btnNextPage.Enabled = false;
			this.txtConsigNo.Text="";
			this.txtCustID.Text="";
			this.txtCustRefNo.Text="";
			this.txtGoToRec.Text="";
			this.DbComboDestinationDC.Text="";
			this.DbComboDestinationDC.Value="";
			this.DbComboPathCode.Text="";
			this.DbComboPathCode.Value="";
			
			this.txtAct_pod_From.Text="";
			this.txtAct_Pod_to.Text="";

			if(this.ddlCODRefused.Items.Count > 0)
			{
				this.ddlCODRefused.Items.RemoveAt(1);
				this.ddlCODRefused.Items.RemoveAt(0);
			}


			ListItem yea, nei;

			if(utility.GetUserCulture()=="th-TH")
			{
				yea = new ListItem("��","Y");
				nei = new ListItem("�����","N");
			}
			else
			{
				yea = new ListItem("Yes","Y");
				nei = new ListItem("No","N");
			}

			this.ddlCODRefused.Items.Add(nei);
			this.ddlCODRefused.Items.Add(yea);
			this.ddlCODRefused.SelectedIndex = 0;

		}


		private void SetEnableControls()
		{	
			//CHECK 4.
			if(this.txtNETCODAmountVerifiedDT_Cash.Text!="")
			{
				this.txtNETCODAmountCollected_Cash.Enabled=false;
			}
			else
				this.txtNETCODAmountCollected_Cash.Enabled=true;

			if(this.txtNETCODAmountVerifiedDT_Check.Text!="")
				this.txtNETCODAmountCollected_Check.Enabled=false;
			else
				this.txtNETCODAmountCollected_Check.Enabled=true;

			if(this.txtNETCODAmountVerifiedDT_Other.Text!="")
				this.txtNETCODAmountCollected_Other.Enabled=false;
			else
				this.txtNETCODAmountCollected_Other.Enabled=true;


			//CHECK 5. COMMENTED BY X APR 29,08.
//			if(this.txtCODRemittanceToCustomerDT_Cash.Text!="")
//				this.txtNETCODAmountVerifiedDT_Cash.Enabled=false;
//			else
//				this.txtNETCODAmountVerifiedDT_Cash.Enabled=true;
//
//			if(this.txtCODRemittanceToCustomerDT_Check.Text!="")
//				this.txtNETCODAmountVerifiedDT_Check.Enabled=false;
//			else
//				this.txtNETCODAmountVerifiedDT_Check.Enabled=true;

			//			if(this.txtCODRemittanceToCustomerDT_Other.Text!="")
			//				this.txtNETCODAmountVerifiedDT_Other.Enabled=false;



			if(TIESDAL.PostDeliveryCOD.GetUserRole(utility.GetAppID(),utility.GetEnterpriseID(),this.userID,"CODSUPERVISOR")==0)
			{
				this.txtCODRemittanceAdvanceToCustomer.Enabled = false;
				this.txtCODRemittanceRemark.Enabled=false;
				this.txtCODRemittanceToCustomerDT_Cash.Enabled=false;
				this.txtCODRemittanceToCustomerDT_Check.Enabled=false;
				this.txtCODRemittanceToCustomerDT_Other.Enabled=false;
				//ViewState["CODSUPERVISOR"]=false;
				boolCODSUPERVISOR = false;
			}

			if(TIESDAL.PostDeliveryCOD.GetUserRole(utility.GetAppID(),utility.GetEnterpriseID(),this.userID,"CODVERIFY")==0)
			{
				this.txtCODVerificationRemark.Enabled=false;
				this.txtNETCODAmountVerifiedDT_Cash.Enabled=false;
				this.txtNETCODAmountVerifiedDT_Check.Enabled=false;
				this.txtNETCODAmountVerifiedDT_Other.Enabled=false;
				//ViewState["CODVERIFY"]=false;
				boolCODVERIFY = false;
			}

			//Set Control Validators

		//	this.CV_txtNETCODAmountCollected_Cash.ValueToCompare = DateTime.Now.ToString("dd/MM/yyyy");
		
		}
 
		private void DisplayCurrentPage()
		{

			DataRow drCurrent = m_sdsShipment.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];

				if(drCurrent["consignment_no"]!= null && (!drCurrent["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					lblSI_consigNo.Text = drCurrent["consignment_no"].ToString();
					ViewState["consignment_no"] = lblSI_consigNo.Text; 
				}
				else
				{
					lblSI_consigNo.Text = "";
					ViewState["consignment_no"] = 0;
				}

			if(drCurrent["booking_datetime"]!= null && (!drCurrent["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblSI_BookingDT.Text = Convert.ToDateTime(drCurrent["booking_datetime"]).ToString("dd/MM/yyyy HH:mm");
				ViewState["booking_datetime"] = lblSI_BookingDT.Text; 
			}
			else
			{
				lblSI_BookingDT.Text = "";
			}

			if(drCurrent["booking_no"]!= null && (!drCurrent["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				ViewState["booking_no"] = drCurrent["booking_no"].ToString();
			}
			else
				ViewState["booking_no"] = 0;

			

			if(drCurrent["act_pickup_datetime"]!= null && (!drCurrent["act_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblSI_PickupDT.Text = Convert.ToDateTime(drCurrent["act_pickup_datetime"]).ToString("dd/MM/yyyy HH:mm");
				ViewState["act_pickup_datetime"] = lblSI_PickupDT.Text; 
			}
			else
			{
				lblSI_PickupDT.Text = "";
			}

			if(drCurrent["tracking_datetime"]!= null && (!drCurrent["tracking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblSI_ActualPOD.Text = Convert.ToDateTime(drCurrent["tracking_datetime"]).ToString("dd/MM/yyyy HH:mm");
				ViewState["tracking_datetime"] = lblSI_ActualPOD.Text; 
			}
			else
			{
				lblSI_ActualPOD.Text = "";
			}

			if(drCurrent["ref_no"]!= null && (!drCurrent["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblSI_CusRef.Text = drCurrent["ref_no"].ToString();
				ViewState["ref_no"] = lblSI_CusRef.Text; 
			}
			else
			{
				lblSI_CusRef.Text = "";
			}

			if(drCurrent["tot_pkg"]!= null && (!drCurrent["tot_pkg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblSI_TotPackage.Text = drCurrent["tot_pkg"].ToString();
				ViewState["tot_pkg"] = lblSI_TotPackage.Text; 
			}
			else
			{
				lblSI_TotPackage.Text = "";
			}

			if(drCurrent["route_code"]!= null && (!drCurrent["route_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblSI_DelivRoute.Text = drCurrent["route_code"].ToString();
				ViewState["route_code"] = lblSI_DelivRoute.Text; 
			}
			else
			{
				lblSI_DelivRoute.Text = "";
			}

			if(drCurrent["service_code"]!= null && (!drCurrent["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblSI_ServiceType.Text = drCurrent["service_code"].ToString();
				ViewState["service_code"] = lblSI_ServiceType.Text; 
			}
			else
			{
				lblSI_ServiceType.Text = "";
			}

			int booking_no = Convert.ToInt32(ViewState["booking_no"]);
			String consignment_no = Convert.ToString(ViewState["consignment_no"]);
			Double CODAmountVAS;
			CODAmountVAS = TIESDAL.PostDeliveryCOD.GetCODAmountVAS(appID,enterpriseID,booking_no,consignment_no);
				//TIESDAL.PostDeliveryCOD.GetI();
				//TIESDAL.PostDeliveryCOD.GetCODAmountVAS(appID,enterpriseID,(int)ViewState["booking_no"],(string)ViewState["consignment_no"]);

			if(CODAmountVAS>0)
			{
				lblSI_CODamVAS.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(CODAmountVAS));
				ViewState["CODAmountVAS"] = lblSI_CODamVAS.Text; 
			}
			else
			{
				lblSI_CODamVAS.Text = "0.00";
			}

			if(drCurrent["cod_amount"]!= null && (!drCurrent["cod_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblSI_CODam.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["cod_amount"]));
				ViewState["cod_amount"] = lblSI_CODam.Text; 
			}
			else
			{
				lblSI_CODam.Text = "0.00";
			}

// END OF FIRST SESSION
// COD AMOUNT COLLECTED
			if(drCurrent["cod_amt_collected_datetime"]!= null && (!drCurrent["cod_amt_collected_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				if(Convert.ToDateTime(drCurrent["cod_amt_collected_datetime"]).ToString("dd/MM/yyyy HH:mm")!="01/01/1900 00:00")
				{
					txtCODAmountCollectedDT.Text = Convert.ToDateTime(drCurrent["cod_amt_collected_datetime"]).ToString("dd/MM/yyyy HH:mm");
					ViewState["cod_amt_collected_datetime"] = txtCODAmountCollectedDT.Text; 
				}
				else
					txtCODAmountCollectedDT.Text = this.lblSI_ActualPOD.Text;
			}
			else
			{
				txtCODAmountCollectedDT.Text = this.lblSI_ActualPOD.Text;
			}

			if(drCurrent["cod_refused"]!= null && (!drCurrent["cod_refused"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				if(drCurrent["cod_refused"].Equals("Y"))
				{
					this.ddlCODRefused.SelectedIndex=1;
					ViewState["cod_refused"] = "Y";
				}

				if(drCurrent["cod_refused"].Equals("N"))
				{
					this.ddlCODRefused.SelectedIndex=0;
					ViewState["cod_refused"] = "N";
				}
				//Convert.ToString(drCurrent["cod_refused"]); 
			}
			else
			{
				this.ddlCODRefused.SelectedIndex=0;
				ViewState["cod_refused"] = "N";
			}

			//ADD BY X MAY 08 08
			if(drCurrent["cod_amt_collected_cash"]!= null && (!drCurrent["cod_amt_collected_cash"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				if(Convert.ToString(drCurrent["cod_amt_collected_cash"])=="0")
				{
					txtNETCODAmountCollected_Cash.Text = "";
					ViewState["cod_amt_collected_cash"] = "0";
				}
				else
				{
					txtNETCODAmountCollected_Cash.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["cod_amt_collected_cash"]));
					ViewState["cod_amt_collected_cash"] = txtNETCODAmountCollected_Cash.Text; 
				}
			}
			else
			{
				txtNETCODAmountCollected_Cash.Text = "";
			}

			if(drCurrent["cod_amt_collected_check"]!= null && (!drCurrent["cod_amt_collected_check"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				if(Convert.ToString(drCurrent["cod_amt_collected_check"])=="0")
				{
					txtNETCODAmountCollected_Check.Text = "";
					ViewState["cod_amt_collected_check"] = "0";
				}
				else
				{
					txtNETCODAmountCollected_Check.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["cod_amt_collected_check"]));
					ViewState["cod_amt_collected_check"] = txtNETCODAmountCollected_Check.Text; 
				}

				//				txtNETCODAmountCollected_Check.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["cod_amt_collected_check"]));
				//				ViewState["cod_amt_collected_check"] = txtNETCODAmountCollected_Check.Text; 
			}
			else
			{
				txtNETCODAmountCollected_Check.Text = "";
			}

			if(drCurrent["cod_amt_collected_other"]!= null && (!drCurrent["cod_amt_collected_other"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				if(Convert.ToString(drCurrent["cod_amt_collected_other"])=="0")
				{
					txtNETCODAmountCollected_Other.Text  = "";
					ViewState["cod_amt_collected_other"] = "0";
				}
				else
				{
					txtNETCODAmountCollected_Other.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["cod_amt_collected_other"]));
					ViewState["cod_amt_collected_other"] = txtNETCODAmountCollected_Other.Text; 
				}

				//				txtNETCODAmountCollected_Other.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["cod_amt_collected_other"]));
				//				ViewState["cod_amt_collected_other"] = txtNETCODAmountCollected_Other.Text; 
			}
			else
			{
				txtNETCODAmountCollected_Other.Text = "";
			}

			if(drCurrent["wh_tax_amt"]!= null && (!drCurrent["wh_tax_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				if(Convert.ToString(drCurrent["wh_tax_amt"])=="0")
				{
					txtWithholdingTaxAmount.Text  = "";
					ViewState["wh_tax_amt"] = "0";
				}
				else
				{
					txtWithholdingTaxAmount.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["wh_tax_amt"]));
					ViewState["wh_tax_amt"] = txtWithholdingTaxAmount.Text; 
				}

				//				txtWithholdingTaxAmount.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["wh_tax_amt"]));
				//				ViewState["wh_tax_amt"] = txtWithholdingTaxAmount.Text; 
			}
			else
			{
				txtWithholdingTaxAmount.Text = "";
			}
			//END ADD BY X MAY 08 08

//			if(txtNETCODAmountCollected_Cash.Text == "" && txtNETCODAmountCollected_Check.Text == "" &&
//				txtNETCODAmountCollected_Other.Text == "" && txtWithholdingTaxAmount.Text == "")
//			{
//				txtNETCODAmountCollected_Cash.Text = lblSI_CODam.Text;
//			}

//			if(drCurrent["cod_amt_collected_cash"]!= null && (!drCurrent["cod_amt_collected_cash"].GetType().Equals(System.Type.GetType("System.DBNull"))))
//			{
//				txtNETCODAmountCollected_Cash.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["cod_amt_collected_cash"]));
//				ViewState["cod_amt_collected_cash"] = txtNETCODAmountCollected_Cash.Text; 
//			}
//			else
//			{
//				txtNETCODAmountCollected_Cash.Text = "";
//			}
//
//			if(drCurrent["cod_amt_collected_check"]!= null && (!drCurrent["cod_amt_collected_check"].GetType().Equals(System.Type.GetType("System.DBNull"))))
//			{
//				txtNETCODAmountCollected_Check.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["cod_amt_collected_check"]));
//				ViewState["cod_amt_collected_check"] = txtNETCODAmountCollected_Check.Text; 
//			}
//			else
//			{
//				txtNETCODAmountCollected_Check.Text = "";
//			}
//
//			if(drCurrent["cod_amt_collected_other"]!= null && (!drCurrent["cod_amt_collected_other"].GetType().Equals(System.Type.GetType("System.DBNull"))))
//			{
//				txtNETCODAmountCollected_Other.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["cod_amt_collected_other"]));
//				ViewState["cod_amt_collected_other"] = txtNETCODAmountCollected_Other.Text; 
//			}
//			else
//			{
//				txtNETCODAmountCollected_Other.Text = "";
//			}
//
//			if(drCurrent["wh_tax_amt"]!= null && (!drCurrent["wh_tax_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
//			{
//				txtWithholdingTaxAmount.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["wh_tax_amt"]));
//				ViewState["wh_tax_amt"] = txtWithholdingTaxAmount.Text; 
//			}
//			else
//			{
//				txtWithholdingTaxAmount.Text = "";
//			}

			if(drCurrent["total_cod_amt"]!= null && (!drCurrent["total_cod_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblTotalCODAmount.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["total_cod_amt"]));
				ViewState["total_cod_amt"] = lblTotalCODAmount.Text; 
			}
			else
			{
				lblTotalCODAmount.Text = "0.00";
			}

			if(drCurrent["net_cod_amt"]!= null && (!drCurrent["net_cod_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtNetCODAmount.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["net_cod_amt"]));
				ViewState["net_cod_amt"] = txtNetCODAmount.Text; 
			}
			else
			{
				txtNetCODAmount.Text = "0.00";
			}

			if(drCurrent["cod_remark"]!= null && (!drCurrent["cod_remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtCODRemark.Text = drCurrent["cod_remark"].ToString();
				ViewState["cod_remark"] = txtCODRemark.Text; 
			}
			else
			{
				txtCODRemark.Text = "";
			}


			DataSet CODLastUserUpdatedDS;
			CODLastUserUpdatedDS = TIESDAL.PostDeliveryCOD.GetCODUpdateUser(appID,enterpriseID,booking_no,consignment_no,"C");

			if(CODLastUserUpdatedDS.Tables[0].Rows.Count>0)
			{
				if(CODLastUserUpdatedDS.Tables[0].Rows[0][0]!= null && (!CODLastUserUpdatedDS.Tables[0].Rows[0][0].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					lblCODLastUserUpdated.Text = CODLastUserUpdatedDS.Tables[0].Rows[0][0].ToString();
				}
				else
				{
					lblCODLastUserUpdated.Text = "";
				}
				if(CODLastUserUpdatedDS.Tables[0].Rows[0][1]!= null && (!CODLastUserUpdatedDS.Tables[0].Rows[0][1].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					lblCODLastUpdateDT.Text = Convert.ToDateTime(CODLastUserUpdatedDS.Tables[0].Rows[0][1]).ToString("dd/MM/yyyy HH:mm");
				}
				else
				{
					lblCODLastUpdateDT.Text = "";
				}
			}
			else
			{
					lblCODLastUserUpdated.Text = "";
					lblCODLastUpdateDT.Text = "";
			}


			//���ᴧ ��� TOTAL COD AMT != COD AMT
			if(!this.lblTotalCODAmount.Text.Equals(this.lblSI_CODam.Text))
				this.lblTotalCODAmount.ForeColor = Color.Red;
			else
				this.lblTotalCODAmount.ForeColor = Color.Black;


//END OF COD AMOUNT COLLECTED

//COD VERIFICATION
			if(drCurrent["cod_amt_verified_datetime_cash"]!= null && (!drCurrent["cod_amt_verified_datetime_cash"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtNETCODAmountVerifiedDT_Cash.Text = Convert.ToDateTime(drCurrent["cod_amt_verified_datetime_cash"]).ToString("dd/MM/yyyy HH:mm");
				ViewState["cod_amt_verified_datetime_cash"] = txtNETCODAmountVerifiedDT_Cash.Text; 
			}
			else
			{
				txtNETCODAmountVerifiedDT_Cash.Text = "";
			}


			if(drCurrent["cod_amt_verified_datetime_check"]!= null && (!drCurrent["cod_amt_verified_datetime_check"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtNETCODAmountVerifiedDT_Check.Text = Convert.ToDateTime(drCurrent["cod_amt_verified_datetime_check"]).ToString("dd/MM/yyyy HH:mm");
				ViewState["cod_amt_verified_datetime_check"] = txtNETCODAmountVerifiedDT_Check.Text; 
			}
			else
			{
				txtNETCODAmountVerifiedDT_Check.Text = "";
			}

			if(drCurrent["cod_amt_verified_datetime_other"]!= null && (!drCurrent["cod_amt_verified_datetime_other"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtNETCODAmountVerifiedDT_Other.Text = Convert.ToDateTime(drCurrent["cod_amt_verified_datetime_other"]).ToString("dd/MM/yyyy HH:mm");
				ViewState["cod_amt_verified_datetime_other"] = txtNETCODAmountVerifiedDT_Other.Text; 
			}
			else
			{
				txtNETCODAmountVerifiedDT_Other.Text = "";
			}

			if(drCurrent["cod_verified_remark"]!= null && (!drCurrent["cod_verified_remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtCODVerificationRemark.Text = drCurrent["cod_verified_remark"].ToString();
				ViewState["cod_verified_remark"] = txtCODVerificationRemark.Text; 
			}
			else
			{
				txtCODVerificationRemark.Text = "";
			}


			CODLastUserUpdatedDS = TIESDAL.PostDeliveryCOD.GetCODUpdateUser(appID,enterpriseID,booking_no,consignment_no,"V");

			if(CODLastUserUpdatedDS.Tables[0].Rows.Count>0)
			{
				if(CODLastUserUpdatedDS.Tables[0].Rows[0][0]!= null && (!CODLastUserUpdatedDS.Tables[0].Rows[0][0].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					lblCODVLUser.Text = CODLastUserUpdatedDS.Tables[0].Rows[0][0].ToString();
				}
				else
				{
					lblCODVLUser.Text = "";
				}
				if(CODLastUserUpdatedDS.Tables[0].Rows[0][1]!= null && (!CODLastUserUpdatedDS.Tables[0].Rows[0][1].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					lblCODVLUpdateDT.Text = Convert.ToDateTime(CODLastUserUpdatedDS.Tables[0].Rows[0][1]).ToString("dd/MM/yyyy HH:mm");

					/*
					 * Developer : samart.kumnate@kerrylogistics.com
					 * Description : add condition to block edit cod verify and cod remittance.
					 * */
					this.txtCODVerificationRemark.Enabled=false;
					this.txtNETCODAmountVerifiedDT_Cash.Enabled=false;
					this.txtNETCODAmountVerifiedDT_Check.Enabled=false;
					this.txtNETCODAmountVerifiedDT_Other.Enabled=false;
					boolCODVERIFY = false;
					/*
					 * end of samart coding.
					 * */
				}
				else
				{
					lblCODVLUpdateDT.Text = "";
				}
			}
			else
			{
				lblCODVLUser.Text = "";
				lblCODVLUpdateDT.Text = "";
			}


//END OF COD VERIFICATION


//COD REMITTANCE TO CUSTOMER
			if(drCurrent["cod_remittance_to_customer_datetime_cash"]!= null && (!drCurrent["cod_remittance_to_customer_datetime_cash"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtCODRemittanceToCustomerDT_Cash.Text = Convert.ToDateTime(drCurrent["cod_remittance_to_customer_datetime_cash"]).ToString("dd/MM/yyyy HH:mm");
				ViewState["cod_remittance_to_customer_datetime_cash"] = txtCODRemittanceToCustomerDT_Cash.Text; 
			}
			else
			{
				txtCODRemittanceToCustomerDT_Cash.Text = "";
			}

			if(drCurrent["cod_remittance_to_customer_datetime_check"]!= null && (!drCurrent["cod_remittance_to_customer_datetime_check"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtCODRemittanceToCustomerDT_Check.Text = Convert.ToDateTime(drCurrent["cod_remittance_to_customer_datetime_check"]).ToString("dd/MM/yyyy HH:mm");
				ViewState["cod_remittance_to_customer_datetime_check"] = txtCODRemittanceToCustomerDT_Check.Text; 
			}
			else
			{
				txtCODRemittanceToCustomerDT_Check.Text = "";
			}

			if(drCurrent["cod_remittance_to_customer_datetime_other"]!= null && (!drCurrent["cod_remittance_to_customer_datetime_other"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtCODRemittanceToCustomerDT_Other.Text = Convert.ToDateTime(drCurrent["cod_remittance_to_customer_datetime_other"]).ToString("dd/MM/yyyy HH:mm");
				ViewState["cod_remittance_to_customer_datetime_other"] = txtCODRemittanceToCustomerDT_Other.Text; 
			}
			else
			{
				txtCODRemittanceToCustomerDT_Other.Text = "";
			}

			//ADD BY X MAY 08 08
			if(drCurrent["cod_adv_to_customer"]!= null && (!drCurrent["cod_adv_to_customer"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				if(Convert.ToString(drCurrent["cod_adv_to_customer"])=="0")
				{
					txtCODRemittanceAdvanceToCustomer.Text  = "";
					ViewState["cod_adv_to_customer"] = "0";
				}
				else
				{
					txtCODRemittanceAdvanceToCustomer.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["cod_adv_to_customer"]));
					ViewState["cod_adv_to_customer"] = txtCODRemittanceAdvanceToCustomer.Text; 
				}

				//				txtCODRemittanceAdvanceToCustomer.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["cod_adv_to_customer"]));//drCurrent["cod_adv_to_customer"].ToString();
				//				ViewState["cod_adv_to_customer"] = txtCODRemittanceAdvanceToCustomer.Text; 
			}
			else
			{
				txtCODRemittanceAdvanceToCustomer.Text = "";
			}

			if(drCurrent["cod_remittance_remark"]!= null && (!drCurrent["cod_remittance_remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtCODRemittanceRemark.Text = drCurrent["cod_remittance_remark"].ToString();
				ViewState["cod_remittance_remark"] = txtCODRemittanceRemark.Text; 
			}
			else
			{
				txtCODRemittanceRemark.Text = "";
			}
			//END ADD BY X MAY 08 08


//			if(drCurrent["cod_adv_to_customer"]!= null && (!drCurrent["cod_adv_to_customer"].GetType().Equals(System.Type.GetType("System.DBNull"))))
//			{
//				txtCODRemittanceAdvanceToCustomer.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(drCurrent["cod_adv_to_customer"]));//drCurrent["cod_adv_to_customer"].ToString();
//				ViewState["cod_adv_to_customer"] = txtCODRemittanceAdvanceToCustomer.Text; 
//			}
//			else
//			{
//				txtCODRemittanceAdvanceToCustomer.Text = "";
//			}
//
//			if(drCurrent["cod_remittance_remark"]!= null && (!drCurrent["cod_remittance_remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
//			{
//				txtCODRemittanceRemark.Text = drCurrent["cod_remittance_remark"].ToString();
//				ViewState["cod_remittance_remark"] = txtCODRemittanceRemark.Text; 
//			}
//			else
//			{
//				txtCODRemittanceRemark.Text = "";
//			}
//
			CODLastUserUpdatedDS = TIESDAL.PostDeliveryCOD.GetCODUpdateUser(appID,enterpriseID,booking_no,consignment_no,"R");

			if(CODLastUserUpdatedDS.Tables[0].Rows.Count>0)
			{
				if(CODLastUserUpdatedDS.Tables[0].Rows[0][0]!= null && (!CODLastUserUpdatedDS.Tables[0].Rows[0][0].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					lblCODRLUpdateUser.Text = CODLastUserUpdatedDS.Tables[0].Rows[0][0].ToString();
				}
				else
				{
					lblCODRLUpdateUser.Text = "";
				}
				if(CODLastUserUpdatedDS.Tables[0].Rows[0][1]!= null && (!CODLastUserUpdatedDS.Tables[0].Rows[0][1].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					lblCODRLUpdateDT.Text = Convert.ToDateTime(CODLastUserUpdatedDS.Tables[0].Rows[0][1]).ToString("dd/MM/yyyy HH:mm");

					/*
					 * Developer : samart.kumnate@kerrylogistics.com
					 * Description : add condition to block edit cod verify and cod remittance.
					 * */
					this.txtCODRemittanceAdvanceToCustomer.Enabled = false;
					this.txtCODRemittanceRemark.Enabled=false;
					this.txtCODRemittanceToCustomerDT_Cash.Enabled=false;
					this.txtCODRemittanceToCustomerDT_Check.Enabled=false;
					this.txtCODRemittanceToCustomerDT_Other.Enabled=false;
					boolCODSUPERVISOR = false;
					/*
					 * end of samart coding.
					 * */
				}
				else
				{
					lblCODRLUpdateDT.Text = "";
				}
			}
			else
			{
				lblCODRLUpdateUser.Text = "";
				lblCODRLUpdateDT.Text = "";
			}

//END OF COD REMITTANCE TO CUSTOMER


//***** SHIPMENT DETAILS ****
//CUSTOMER INFORMATION

			if(drCurrent["payer_type"]!= null && (!drCurrent["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				ArrayList mbgOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"customer_type",CodeValueType.StringValue);
				
				foreach(SystemCode mbgSysCode in mbgOptionArray)
				{
					if(drCurrent["payer_type"].ToString()==mbgSysCode.StringValue)
					{
						lblCI_CustType.Text = mbgSysCode.Text;
						ViewState["payer_type"] = mbgSysCode.StringValue; 
					}
				}		
			}
			else
			{
				lblCI_CustType.Text = "";
			}

			if(drCurrent["payerid"]!= null && (!drCurrent["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblCI_CustID.Text = drCurrent["payerid"].ToString();
				ViewState["payerid"] = lblCI_CustID.Text; 
			}
			else
			{
				lblCI_CustID.Text = "";
			}

			if(drCurrent["payer_name"]!= null && (!drCurrent["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblCI_Name.Text = drCurrent["payer_name"].ToString();
				ViewState["payer_name"] = lblCI_Name.Text; 
			}
			else
			{
				lblCI_Name.Text = "";
			}


			if(drCurrent["payer_address1"]!= null && (!drCurrent["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblCI_Addr1.Text = drCurrent["payer_address1"].ToString();
				ViewState["payer_address1"] = lblCI_Addr1.Text; 
			}
			else
			{
				lblCI_Addr1.Text = "";
			}

			if(drCurrent["payer_address2"]!= null && (!drCurrent["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblCI_Addr2.Text = drCurrent["payer_address2"].ToString();
				ViewState["payer_address2"] = lblCI_Addr2.Text; 
			}
			else
			{
				lblCI_Addr2.Text = "";
			}

			if(drCurrent["payer_zipcode"]!= null && (!drCurrent["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblCI_PostalCode.Text = drCurrent["payer_zipcode"].ToString();
				ViewState["payer_zipcode"] = lblCI_PostalCode.Text; 
			}
			else
			{
				lblCI_PostalCode.Text = "";
			}

			if(drCurrent["payer_country"]!= null && (!drCurrent["payer_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblCI_Country.Text = drCurrent["payer_country"].ToString();
				ViewState["payer_country"] = lblCI_Country.Text; 
			}
			else
			{
				lblCI_Country.Text = "";
			}


			//PROVINCE ��� payer_zipcode �� payer_country ����
			String strCustomerStateName;
			String strCountryName;
			String strStateCode,strZipCode;

			if(drCurrent["payer_zipcode"]!= null && (!drCurrent["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))&&drCurrent["payer_country"]!= null && (!drCurrent["payer_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strZipCode = drCurrent["payer_zipcode"].ToString();
				strCountryName = drCurrent["payer_country"].ToString();

				strCustomerStateName = TIESDAL.PostDeliveryCOD.GetProvinceName(appID,enterpriseID,strCountryName,strZipCode);

				if(strCustomerStateName!="0")
				{
					lblCI_Province.Text  = strCustomerStateName;
				}
				else
				{
					lblCI_Province.Text = "";
				}
			}
			else
				lblCI_Province.Text = "";
//END OF Province

			if(drCurrent["payment_mode"]!= null && (!drCurrent["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				ArrayList mbgOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"payment_mode",CodeValueType.StringValue);
				
				foreach(SystemCode mbgSysCode in mbgOptionArray)
				{
					if(drCurrent["payment_mode"].ToString()==mbgSysCode.StringValue)
					{
						lblCI_PaymentMode.Text = mbgSysCode.Text;
						ViewState["payment_mode"] = mbgSysCode.StringValue; 
					}
				}		

//				lblCI_PaymentMode.Text = drCurrent["payment_mode"].ToString();
//				ViewState["payment_mode"] = lblCI_PaymentMode.Text; 
			}
			else
			{
				lblCI_PaymentMode.Text = "";
			}

			if(drCurrent["payer_telephone"]!= null && (!drCurrent["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblCI_Tel.Text = drCurrent["payer_telephone"].ToString();
				ViewState["payer_telephone"] = lblCI_Tel.Text; 
			}
			else
			{
				lblCI_Tel.Text = "";
			}

			if(drCurrent["payer_fax"]!= null && (!drCurrent["payer_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblCI_Fax.Text = drCurrent["payer_fax"].ToString();
				ViewState["payer_fax"] = lblCI_Fax.Text; 
			}
			else
			{
				lblCI_Fax.Text = "";
			}


//END OF CUSTOMER INFORMATION


//SENDER INFORMATION

			if(drCurrent["sender_name"]!= null && (!drCurrent["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblSDI_Name.Text = drCurrent["sender_name"].ToString();
				ViewState["sender_name"] = lblSDI_Name.Text; 
			}
			else
			{
				lblSDI_Name.Text = "";
			}

			if(drCurrent["sender_address1"]!= null && (!drCurrent["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblSDI_Addr1.Text = drCurrent["sender_address1"].ToString();
				ViewState["sender_address1"] = lblSDI_Addr1.Text; 
			}
			else
			{
				lblSDI_Addr1.Text = "";
			}

			if(drCurrent["sender_address2"]!= null && (!drCurrent["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblSDI_Addr2.Text = drCurrent["sender_address2"].ToString();
				ViewState["sender_address2"] = lblSDI_Addr2.Text; 
			}
			else
			{
				lblSDI_Addr2.Text = "";
			}


			if(drCurrent["sender_zipcode"]!= null && (!drCurrent["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblSDI_PostalCode.Text = drCurrent["sender_zipcode"].ToString();
				ViewState["sender_zipcode"] = lblSDI_PostalCode.Text; 
			}
			else
			{
				lblSDI_PostalCode.Text = "";
			}


			if(drCurrent["sender_country"]!= null && (!drCurrent["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblSDI_Country.Text = drCurrent["sender_country"].ToString();
				ViewState["sender_country"] = lblSDI_Country.Text; 
			}
			else
			{
				lblSDI_Country.Text = "";
			}


			String strSenderCutOffTime;

			if(lblSDI_PostalCode.Text.Length > 0)
			{
				strZipCode = ViewState["sender_zipcode"].ToString();
				strSenderCutOffTime = TIESDAL.PostDeliveryCOD.GetCutOffTime(appID,enterpriseID,booking_no,consignment_no,strZipCode);

				if(strSenderCutOffTime!="0")
				{
					lblSDI_CutOffTime.Text  = Convert.ToDateTime(strSenderCutOffTime).ToString("HH:mm");
				}
				else
				{
					lblSDI_CutOffTime.Text = "";
				}
			}
			else
				lblSDI_CutOffTime.Text = "";



			//Province
			/*String strCustomerStateName;
			String strCountryName;
			String strStateCode;*/

			if(drCurrent["origin_state_code"]!= null && (!drCurrent["origin_state_code"].GetType().Equals(System.Type.GetType("System.DBNull")))&&drCurrent["sender_country"]!= null && (!drCurrent["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strStateCode = drCurrent["origin_state_code"].ToString();
				strCountryName = drCurrent["sender_country"].ToString();

				strCustomerStateName = TIESDAL.PostDeliveryCOD.GetStateName(appID,enterpriseID,strCountryName,strStateCode);

				if(strCustomerStateName!="0")
				{
					lblSDI_Province.Text  = strCustomerStateName;
				}
				else
				{
					lblSDI_Province.Text = "";
				}
			}
			else
				lblSDI_Province.Text = "";
			//END Province


			if(drCurrent["sender_contact_person"]!= null && (!drCurrent["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblSDI_ContactPerson.Text = drCurrent["sender_contact_person"].ToString();
				ViewState["sender_contact_person"] = lblSDI_ContactPerson.Text; 
			}
			else
			{
				lblSDI_ContactPerson.Text = "";
			}

			if(drCurrent["sender_telephone"]!= null && (!drCurrent["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblSDI_Tel.Text = drCurrent["sender_telephone"].ToString();
				ViewState["sender_telephone"] = lblSDI_Tel.Text; 
			}
			else
			{
				lblSDI_Tel.Text = "";
			}

			if(drCurrent["sender_fax"]!= null && (!drCurrent["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				lblSDI_Fax.Text = drCurrent["sender_fax"].ToString();
				ViewState["sender_fax"] = lblSDI_Fax.Text; 
			}
			else
			{
				lblSDI_Fax.Text = "";
			}


//END OF Sender INFORMATION



//Recipient INFORMATION

			if(drCurrent["recipient_telephone"]!= null && (!drCurrent["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				RI_Tel.Text = drCurrent["recipient_telephone"].ToString();
				ViewState["recipient_telephone"] = RI_Tel.Text; 
			}
			else
			{
				RI_Tel.Text = "";
			}

			if(drCurrent["recipient_name"]!= null && (!drCurrent["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				RI_Name.Text = drCurrent["recipient_name"].ToString();
				ViewState["recipient_name"] = RI_Name.Text; 
			}
			else
			{
				RI_Name.Text = "";
			}

			if(drCurrent["recipient_address1"]!= null && (!drCurrent["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				RI_Addr1.Text = drCurrent["recipient_address1"].ToString();
				ViewState["recipient_address1"] = RI_Addr1.Text; 
			}
			else
			{
				RI_Addr1.Text = "";
			}


			if(drCurrent["recipient_address2"]!= null && (!drCurrent["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				RI_Addr2.Text = drCurrent["recipient_address2"].ToString();
				ViewState["recipient_address2"] = RI_Addr2.Text; 
			}
			else
			{
				RI_Addr2.Text = "";
			}

			if(drCurrent["recipient_zipcode"]!= null && (!drCurrent["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				RI_PostalCode.Text = drCurrent["recipient_zipcode"].ToString();
				ViewState["recipient_zipcode"] = RI_PostalCode.Text; 
			}
			else
			{
				RI_PostalCode.Text = "";
			}


			//PROVINCE
			/*String strCustomerStateName;
			String strCountryName;
			String strStateCode;*/

			if(drCurrent["destination_state_code"]!= null && (!drCurrent["destination_state_code"].GetType().Equals(System.Type.GetType("System.DBNull")))&&drCurrent["recipient_country"]!= null && (!drCurrent["recipient_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				strStateCode = drCurrent["destination_state_code"].ToString();
				strCountryName = drCurrent["recipient_country"].ToString();

				strCustomerStateName = TIESDAL.PostDeliveryCOD.GetStateName(appID,enterpriseID,strCountryName,strStateCode);

				if(strCustomerStateName!="0")
				{
					RI_Province.Text  = strCustomerStateName;
				}
				else
				{
					RI_Province.Text = "";
				}
			}
			else
				RI_Province.Text = "";
			//END OF Province



			if(drCurrent["recipient_country"]!= null && (!drCurrent["recipient_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				RI_Country.Text = drCurrent["recipient_country"].ToString();
				ViewState["recipient_country"] = RI_Country.Text; 
			}
			else
			{
				RI_Country.Text = "";
			}

			if(drCurrent["recipient_contact_person"]!= null && (!drCurrent["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				RI_ContactPerson.Text = drCurrent["recipient_contact_person"].ToString();
				ViewState["recipient_contact_person"] = RI_ContactPerson.Text; 
			}
			else
			{
				RI_ContactPerson.Text = "";
			}

			if(drCurrent["recipient_fax"]!= null && (!drCurrent["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				RI_Fax.Text = drCurrent["recipient_fax"].ToString();
				ViewState["recipient_fax"] = RI_Fax.Text; 
			}
			else
			{
				RI_Fax.Text = "";
			}

			//by sittichai  07/02/2548
//			if (drCurrent["invoice_date"] != null && (!drCurrent["invoice_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
//			{
//				btnSave.Enabled = false;
//			}
//			else
//			{
//				btnSave.Enabled = true;
//			}
//END OF Recipient INFORMATION


//END OF ***** SHIPMENT DETAILS ****
			SetEnableControls();


		}


		private void DisplayRecNum()
		{
			int iCurrentRec = (Convert.ToInt32(ViewState["currentPage"]) + 1) + (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) ;

			if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
			{
				lblNumRec.Text = ""+ iCurrentRec + " " + Utility.GetLanguageText(ResourceType.ScreenLabel, "of", utility.GetUserCulture()) + " " + m_sdsShipment.QueryResultMaxSize + " " + Utility.GetLanguageText(ResourceType.ScreenLabel, "record(s)", utility.GetUserCulture());
			}
			else
			{
				lblNumRec.Text = ""+ iCurrentRec + " of " + m_sdsShipment.QueryResultMaxSize + " record(s)";
			}
			//ADDED BY X FEB 22 08
			if(iCurrentRec>=m_sdsShipment.QueryResultMaxSize)
			{
				EnableNavigationButtons(true,true,false,false);
				return;
			}
			//END ADDED BY X FEB 22 08

		}

		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}


		private void btnDisplayCustDtls_Click(object sender, System.EventArgs e)
		{
			//by X NOV NOV 29 07
			strErrorMsg = "";			
			poppulateCustomerAddress();
		}

		private void poppulateCustomerAddress()
		{
			String sUrl = "CustomerPopup.aspx?FORMID="+"PostDeliveryCOD";
				/*+
				"&CustType="+"1";*/
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			//this.divCustIdCng.Visi
			//DomstcShipPanel.Visible=false;
			divMain.Visible=true;
		}

		private void btnNextPage_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			EnableNavigationButtons(true,true,true,true);

			if(Convert.ToInt32(ViewState["currentPage"])  < (Convert.ToInt32(m_sdsShipment.DataSetRecSize) - 1) )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) + 1;
				DisplayCurrentPage();
			}
			else
			{
				decimal iTotalRec = (Convert.ToInt32(ViewState["currentRefSet"]) * m_iSetSize) + Convert.ToInt32(m_sdsShipment.DataSetRecSize);
				if( iTotalRec ==  m_sdsShipment.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) + 1;	
				GetRecSet();
				ViewState["currentPage"] = 0;
				DisplayCurrentPage();
			}
			DisplayRecNum();
		}

		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
//			ViewState["currentSet"] = (int)Math.Floor((double)(m_sdsShipment.QueryResultMaxSize - 1)/m_iSetSize);
//			GetRecSet();
//			ViewState["currentPage"] = (int)Math.IEEERemainder((double)(m_sdsShipment.QueryResultMaxSize - 1),(double)m_iSetSize);//m_sdsShipment.DataSetRecSize - 1;
//			System.Threading.Thread.Sleep(3500);
			ViewState["currentSet"] = (int)Math.Floor((double)(m_sdsShipment.QueryResultMaxSize - 1)/m_iSetSize);//(m_sdsShipment.QueryResultMaxSize - 1)/m_iSetSize;	
			GetRecSet();
			//ViewState["currentPage"] = (int)Math.Abs(Math.IEEERemainder((double)(m_sdsShipment.QueryResultMaxSize - 1),(double)m_iSetSize)); //m_sdsShipment.DataSetRecSize - 1;

			int iCurrentPage = (int)(Math.IEEERemainder((double)(m_sdsShipment.QueryResultMaxSize - 1),(double)m_iSetSize)); //.DivRem(iPageEnter-1/iSetSize);

			if(iCurrentPage>=0)
				ViewState["currentPage"] = (int)Math.Abs(iCurrentPage);
			else
				ViewState["currentPage"] = m_iSetSize - (int)Math.Abs(iCurrentPage);

			DisplayCurrentPage();	
			DisplayRecNum();
			EnableNavigationButtons(true,true,false,false);
		}

		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  > 0 )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) - 1;
				DisplayCurrentPage();
			}
			else
			{
				if( (Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) - 1;	
				GetRecSet();
				System.Threading.Thread.Sleep(3500);

				ViewState["currentPage"] = m_sdsShipment.DataSetRecSize - 1;
				DisplayCurrentPage();
			}
			DisplayRecNum();
			EnableNavigationButtons(true,true,true,true);

//			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);
//
//			if(Convert.ToInt32(ViewState["currentPage"])  > 0 )
//			{
//				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) - 1;
//				DisplayCurrentPage();
//			}
//			else
//			{
//				if( (Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)
//				{
//					EnableNavigationButtons(false,false,true,true);
//					return;
//				}
//				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) - 1;	
//				GetRecSet();
//				ViewState["currentPage"] = m_sdsShipment.DataSetRecSize - 1;
//				DisplayCurrentPage();
//			}
//			DisplayRecNum();
//			EnableNavigationButtons(true,true,true,true);
		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			ViewState["currentSet"] = 0;	
			ViewState["currentPage"] = 0;
			GetRecSet();
			DisplayCurrentPage();
			DisplayRecNum();
			EnableNavigationButtons(false,false,true,true);
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			ResetScreenForQuery();
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			bool boolCanUpdate = false;

			btnSave.Enabled = true; //Changed btnSave.Enabled from "false" to be "true" (by Sittichai 08/01/2008)
			//CHECK FOR 2. COD Amount Collected not equals to COD AMOUNT
			boolCanUpdate =FillShipmentDataRow_Update(Convert.ToInt32(ViewState["currentPage"]));

			if(!boolCanUpdate)
			{
				this.lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NET COD AMOUNT OR WITHHOLDING TAX AMOUNT CANNOT BE BLANK.",utility.GetUserCulture());
				return;
			}

			ViewState["boolUpdateUserCODAmountCollected"]=this.boolUpdateUserCODAmountCollected;
			ViewState["boolUpdateUserCODVerification"]=this.boolUpdateUserCODVerification;
			ViewState["boolUpdateUserCODRemittance"]=this.boolUpdateUserCODRemittance;

			if(!this.CheckDateTime())
			{
				this.lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLEASE CHECK DATETIME",utility.GetUserCulture());
				return;
			}

			//ADD BY X ARP 29, 08.
//			if(this.txtCODRemittanceToCustomerDT_Cash.Text.Length>0 && (this.txtCODRemittanceAdvanceToCustomer.Text.Length==0 ||this.txtCODRemittanceAdvanceToCustomer.Text=="0" ))
//			{
//				this.lblErrorMsg.Text = "Please fill COD Remittance Advance to customer. ";
//				return;
//			}
//			if(this.txtCODRemittanceToCustomerDT_Cash.Text.Length==0 && (this.txtCODRemittanceAdvanceToCustomer.Text.Length>0 && this.txtCODRemittanceAdvanceToCustomer.Text!="0"))
//			{
//				this.lblErrorMsg.Text = "Please fill COD Remittance to Customer D/T (Cash).";
//				return;
//			}
			//END ADD BY X APR 29, 08.


			//CHECK COD REFUSED 1. and display warning message
			//1.	If the user presses the Save button and COD Refused = Y:
			//The system will popup the warning message (Remove COD Surcharge? (Yes / No)) with options to:
			//�	Yes = save and remove the original COD Surcharge on the Domestic Shipment
			//�	No = save and retain the original COD Surcharge on the Domestic Shipment
			
//			if(!this.CheckCODRefused())
//				return;

			//by sittichai  14/03/2008
			if(Convert.ToString(ViewState["consignment_no"])!="0")
				strConsig_no = Convert.ToString(ViewState["consignment_no"]);
			else
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MDY_FAIL",utility.GetUserCulture());
				return;
			}

			if(!this.CheckInvoiced(appID,enterpriseID,strConsig_no))
				return;

			
			if(!this.CheckTotalCODAmount())
				return;

			
		}

		private bool CheckTotalCODAmount()
		{
				Double TotalCODAmountCollected=0,txtNETCODAmountCollected_Cash=0,
					txtNETCODAmountCollected_Check=0,txtNETCODAmountCollected_Other=0,
					txtWithholdingTaxAmount=0,CODam=0;

			txtNETCODAmountCollected_Cash = this.txtNETCODAmountCollected_Cash.Text!="" ? Convert.ToDouble(this.txtNETCODAmountCollected_Cash.Text) : 0;
			txtNETCODAmountCollected_Other = this.txtNETCODAmountCollected_Other.Text!="" ? Convert.ToDouble(this.txtNETCODAmountCollected_Other.Text) : 0;
			txtNETCODAmountCollected_Check = this.txtNETCODAmountCollected_Check.Text!="" ? Convert.ToDouble(this.txtNETCODAmountCollected_Check.Text) : 0;
			txtWithholdingTaxAmount = this.txtWithholdingTaxAmount.Text!="" ? Convert.ToDouble(this.txtWithholdingTaxAmount.Text) : 0;
			CODam = Convert.ToDouble(this.lblSI_CODam.Text);
			TotalCODAmountCollected = txtNETCODAmountCollected_Cash+txtNETCODAmountCollected_Check+txtNETCODAmountCollected_Other+txtWithholdingTaxAmount;

			if(TotalCODAmountCollected!=CODam && this.boolUpdateUserCODAmountCollected)//&&(this.txtNETCODAmountCollected_Cash.Enabled&&this.txtNETCODAmountCollected_Check.Enabled&&this.txtNETCODAmountCollected_Other.Enabled))
			{
				this.divMain.Visible=false;
				this.divCODREMITTANCE_DT.Visible=false;
				this.divTotalCODAmountCollected.Visible=true;
				this.divCODRefused.Visible=false;
				this.divInvoiced.Visible=false;
				return false;
			}
			else
			{
				CheckRemittanceDT();
				return true;
			}
				
		}
 
		private void SaveToDB(String strAppID,String strEnterpriseID)
		{
//					FillShipmentDataRow_Update(Convert.ToInt32(ViewState["currentPage"]));
			if(!boolSave)
			{
				boolSave=false;
				return;
			}
			

					DataSet dsToUpdate = m_sdsShipment.ds.GetChanges();

					StringBuilder strXml = new StringBuilder();


					if(Convert.ToString(ViewState["booking_no"])!="0")
						strBooking_no = Convert.ToString(ViewState["booking_no"]);
					else
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MDY_FAIL",utility.GetUserCulture());
						return;
					}

					if(Convert.ToString(ViewState["consignment_no"])!="0")
						strConsig_no = Convert.ToString(ViewState["consignment_no"]);
					else
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MDY_FAIL",utility.GetUserCulture());
						return;
					}

					try
					{
						if(this.boolUpdateUserCODAmountCollected||this.boolUpdateUserCODVerification||this.boolUpdateUserCODRemittance)
						{ //GEN XML FILE
//							if(!drEach["cod_amt_collected_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))
							strXml.Append("<COD_Controls applicationid=\""+ strAppID + "\" enterpriseid=\""+  strEnterpriseID + "\" bookingno=\""+  strBooking_no + "\" consignment_no=\"" + strConsig_no +"\">");

							strXml.Append("<COD_Amount_Collected>");
							strXml.Append("<COD_Amount_Collected_DateTime>");
							if(this.txtCODAmountCollectedDT.Text.Length>10)
								strXml.Append(Convert.ToDateTime(DateTime.ParseExact(this.txtCODAmountCollectedDT.Text,"dd/MM/yyyy HH:mm",null)).ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo ));
							if(this.txtCODAmountCollectedDT.Text.Length==10)
								strXml.Append(Convert.ToDateTime(DateTime.ParseExact(this.txtCODAmountCollectedDT.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null)).ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo ));
							strXml.Append("</COD_Amount_Collected_DateTime>");

							strXml.Append("<NET_COD_Collected_Cash>");
							strXml.Append(Utility.ReplaceSingleQuote(this.txtNETCODAmountCollected_Cash.Text));
							strXml.Append("</NET_COD_Collected_Cash>");

							strXml.Append("<NET_COD_Collected_Check>");
							strXml.Append(Utility.ReplaceSingleQuote(this.txtNETCODAmountCollected_Check.Text));
							strXml.Append("</NET_COD_Collected_Check>");

							strXml.Append("<NET_COD_Collected_Other>");
							strXml.Append(Utility.ReplaceSingleQuote(this.txtNETCODAmountCollected_Other.Text));
							strXml.Append("</NET_COD_Collected_Other>");

							strXml.Append("<WH_Tax_Amount>");
							strXml.Append(Utility.ReplaceSingleQuote(this.txtWithholdingTaxAmount.Text));
							strXml.Append("</WH_Tax_Amount>");

							strXml.Append("<COD_Refused>"+Utility.ReplaceSingleQuote(this.ddlCODRefused.SelectedItem.Value.ToString())+"</COD_Refused>");
							strXml.Append("</COD_Amount_Collected>");

							strXml.Append("<COD_Amount_Verified>");
							strXml.Append("<COD_Amount_Verified_DateTime_Cash>");
							if(this.txtNETCODAmountVerifiedDT_Cash.Text.Length>10)
								strXml.Append(Convert.ToDateTime(DateTime.ParseExact(this.txtNETCODAmountVerifiedDT_Cash.Text,"dd/MM/yyyy HH:mm",null)).ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo ));
							if(this.txtNETCODAmountVerifiedDT_Cash.Text.Length==10)
								strXml.Append(Convert.ToDateTime(DateTime.ParseExact(this.txtNETCODAmountVerifiedDT_Cash.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null)).ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo ));
							strXml.Append("</COD_Amount_Verified_DateTime_Cash>");

							strXml.Append("<COD_Amount_Verified_DateTime_Check>");
							if(this.txtNETCODAmountVerifiedDT_Check.Text.Length>10)
								strXml.Append(Convert.ToDateTime(DateTime.ParseExact(this.txtNETCODAmountVerifiedDT_Check.Text,"dd/MM/yyyy HH:mm",null)).ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo ));
							if(this.txtNETCODAmountVerifiedDT_Check.Text.Length==10)
								strXml.Append(Convert.ToDateTime(DateTime.ParseExact(this.txtNETCODAmountVerifiedDT_Check.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null)).ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo ));
							strXml.Append("</COD_Amount_Verified_DateTime_Check>");

							strXml.Append("<COD_Amount_Verified_DateTime_Other>");
							if(this.txtNETCODAmountVerifiedDT_Other.Text.Length>10)
								strXml.Append(Convert.ToDateTime(DateTime.ParseExact(this.txtNETCODAmountVerifiedDT_Other.Text,"dd/MM/yyyy HH:mm",null)).ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo ));
							if(this.txtNETCODAmountVerifiedDT_Other.Text.Length==10)
								strXml.Append(Convert.ToDateTime(DateTime.ParseExact(this.txtNETCODAmountVerifiedDT_Other.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null)).ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo ));
							strXml.Append("</COD_Amount_Verified_DateTime_Other>");
							strXml.Append("</COD_Amount_Verified>");

							strXml.Append("<COD_Remittance_to_Customer>");
							strXml.Append("<COD_Remittance_to_Customer_DateTime_Cash>");
							if(this.txtCODRemittanceToCustomerDT_Cash.Text.Length>10)
								strXml.Append(Convert.ToDateTime(DateTime.ParseExact(this.txtCODRemittanceToCustomerDT_Cash.Text,"dd/MM/yyyy HH:mm",null)).ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo ));
							if(this.txtCODRemittanceToCustomerDT_Cash.Text.Length==10)
								strXml.Append(Convert.ToDateTime(DateTime.ParseExact(this.txtCODRemittanceToCustomerDT_Cash.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null)).ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo ));
							strXml.Append("</COD_Remittance_to_Customer_DateTime_Cash>");
 
							strXml.Append("<COD_Remittance_to_Customer_DateTime_Check>");
							if(this.txtCODRemittanceToCustomerDT_Check.Text.Length>10)
								strXml.Append(Convert.ToDateTime(DateTime.ParseExact(this.txtCODRemittanceToCustomerDT_Check.Text,"dd/MM/yyyy HH:mm",null)).ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo ));
							if(this.txtCODRemittanceToCustomerDT_Check.Text.Length==10)
								strXml.Append(Convert.ToDateTime(DateTime.ParseExact(this.txtCODRemittanceToCustomerDT_Check.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null)).ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo ));
							strXml.Append("</COD_Remittance_to_Customer_DateTime_Check>");

							strXml.Append("<COD_Remittance_to_Customer_DateTime_Other>");
							if(this.txtCODRemittanceToCustomerDT_Other.Text.Length>10)
								strXml.Append(Convert.ToDateTime(DateTime.ParseExact(this.txtCODRemittanceToCustomerDT_Other.Text,"dd/MM/yyyy HH:mm",null)).ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo ));
							if(this.txtCODRemittanceToCustomerDT_Other.Text.Length==10)
								strXml.Append(Convert.ToDateTime(DateTime.ParseExact(this.txtCODRemittanceToCustomerDT_Other.Text + " " + DateTime.Now.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null)).ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo ));
							strXml.Append("</COD_Remittance_to_Customer_DateTime_Other>");

							strXml.Append("<COD_Remittance_Advance_to_Customer>");
							strXml.Append(Utility.ReplaceSingleQuote(this.txtCODRemittanceAdvanceToCustomer.Text));
							strXml.Append("</COD_Remittance_Advance_to_Customer>");
							strXml.Append("</COD_Remittance_to_Customer>");

							strXml.Append("</COD_Controls>");
						
						}

						if(this.boolUpdateUserCODAmountCollected)
							TIESDAL.PostDeliveryCOD.UpdateCODUpdateUser(utility.GetAppID(),utility.GetEnterpriseID(),strBooking_no,strConsig_no,"C",this.userID,strXml.ToString());
 
						if(this.boolUpdateUserCODVerification)
							TIESDAL.PostDeliveryCOD.UpdateCODUpdateUser(utility.GetAppID(),utility.GetEnterpriseID(),strBooking_no,strConsig_no,"V",this.userID,strXml.ToString());

						if(this.boolUpdateUserCODRemittance)
							TIESDAL.PostDeliveryCOD.UpdateCODUpdateUser(utility.GetAppID(),utility.GetEnterpriseID(),strBooking_no,strConsig_no,"R",this.userID,strXml.ToString());

						Utility ut = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
						//ADDED BY X ARP 04 08
						DataRow drCOD = FillShipmentDataRow_CODHistory();
						TIESDAL.PostDeliveryCOD.UpdateCODHistory(utility.GetAppID(),utility.GetEnterpriseID(),strBooking_no,strConsig_no,drCOD,ut);
						
						TIESDAL.PostDeliveryCOD.UpdatePostDeliveryShipment(utility.GetAppID(),utility.GetEnterpriseID(),strBooking_no,strConsig_no,dsToUpdate,this.boolUpdateUserCODAmountCollected);
						m_sdsShipment.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])].AcceptChanges();
						
						//QUERY THE SAVED VALUE TO SCREEN
						GetRecSet();
						DisplayCurrentPage();
						DisplayRecNum();

						this.lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"MODIFIED_SUCCESSFULLY",utility.GetUserCulture());
						
						//				Logger.LogDebugInfo("DEBUG MODE : PostDeliveryCOD","btnSave_Click","INF004","save in modify mode..");
						//				Logger.LogDebugInfo("module2","DEBUG MODE :(module2 only) PostDeliveryCOD","btnSave_Click","INF004","save in modify mode...");
						this.divMain.Visible=true;
						this.divCODREMITTANCE_DT.Visible=false;
						this.divTotalCODAmountCollected.Visible=false;
						this.divCODRefused.Visible=false;

					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;

						lblErrorMsg.Text = strMsg;


						//				Logger.LogTraceError("TRACE MODE : PostDeliveryCOD","btnSave_Click","ERR004","Error updating record :--- "+strMsg);
						//				Logger.LogTraceError("module2","TRACE MODE : (module2 only)PostDeliveryCOD","btnSave_Click","ERR004","Error updating record : "+strMsg);
						//
						this.divMain.Visible=true;
						this.divCODREMITTANCE_DT.Visible=false;
						this.divTotalCODAmountCollected.Visible=false;
						this.divCODRefused.Visible=false;
						return;
						
					}
//			this.divMain.Visible=true;
//			this.divCODREMITTANCE_DT.Visible=false;
//			this.divTotalCODAmountCollected.Visible=false;
//			this.divCODRefused.Visible=false;
//			return;
			boolSave=false;
//			GetRecSet();
//			return;
					
		}

		private void CheckRemittanceDT()
		{
			DateTime startTimeCash, startTimeCheck, startTimeOther;
			DateTime endTime; // = DateTime.Now.Date;

			if(this.lblSI_ActualPOD.Text!="")
				endTime = DateTime.ParseExact(this.lblSI_ActualPOD.Text.Substring(0,10),"dd/MM/yyyy",null);
			else
				endTime = DateTime.Now.Date;

			if(this.txtCODRemittanceToCustomerDT_Cash.Text.Length==10)
				startTimeCash = DateTime.ParseExact(this.txtCODRemittanceToCustomerDT_Cash.Text,"dd/MM/yyyy",null);
			else
			{
				if(this.lblSI_ActualPOD.Text!="")
					startTimeCash = DateTime.ParseExact(this.lblSI_ActualPOD.Text.Substring(0,10),"dd/MM/yyyy",null);
				else
					startTimeCash = DateTime.Now.Date;
			}

			TimeSpan spanCash = startTimeCash.Subtract(endTime);

			if(this.txtCODRemittanceToCustomerDT_Check.Text.Length==10)
				startTimeCheck = DateTime.ParseExact(this.txtCODRemittanceToCustomerDT_Check.Text,"dd/MM/yyyy",null);
			else
			{
				if(this.lblSI_ActualPOD.Text!="")
					startTimeCheck = DateTime.ParseExact(this.lblSI_ActualPOD.Text.Substring(0,10),"dd/MM/yyyy",null);
				else
					startTimeCheck = DateTime.Now.Date;
			}

			TimeSpan spanCheck = startTimeCheck.Subtract(endTime);

			if(this.txtCODRemittanceToCustomerDT_Other.Text.Length==10)
				startTimeOther = DateTime.ParseExact(this.txtCODRemittanceToCustomerDT_Other.Text,"dd/MM/yyyy",null);
			else
			{
				if(this.lblSI_ActualPOD.Text!="")
					startTimeOther = DateTime.ParseExact(this.lblSI_ActualPOD.Text.Substring(0,10),"dd/MM/yyyy",null);
				else
					startTimeOther = DateTime.Now.Date;
			}


			TimeSpan spanOther = startTimeOther.Subtract(endTime);

			if(spanCash.Days > 10 || spanCheck.Days > 10 || spanOther.Days > 10)
			{
				this.divMain.Visible=false;
				this.divCODREMITTANCE_DT.Visible=true;
				this.divTotalCODAmountCollected.Visible=false;
				this.divCODRefused.Visible=false;
				return;

			}
			else
				SaveToDB(this.appID,this.enterpriseID);

		}

		private void btnDivTotalNo_Click(object sender, System.EventArgs e)
		{
			this.divMain.Visible=true;
			this.btnSave.Enabled=true;
			this.divCODREMITTANCE_DT.Visible=false;
			this.divTotalCODAmountCollected.Visible=false;
			this.divCODRefused.Visible=false;
			this.divInvoiced.Visible=false;
			return;
		}

		private void btnDivTotalYes_Click(object sender, System.EventArgs e)
		{
			CheckRemittanceDT();
		}

		private void btnDivTotalCancel_Click(object sender, System.EventArgs e)
		{
			this.txtNETCODAmountCollected_Cash.Text="0";
			this.txtNETCODAmountCollected_Check.Text="0";
			this.txtNETCODAmountCollected_Other.Text="0";
			this.txtWithholdingTaxAmount.Text="0";
			this.lblTotalCODAmount.Text="0.00"; // data in lblTotalCODAmount = 0.00 when click cancel button (by Sittichai 08/01/2008)
			this.txtNetCODAmount.Text="0.00";   // data in txtNetCODAmount = 0.00 when click cancel button (by Sittichai 08/01/2008)

			this.divTotalCODAmountCollected.Visible=false;
			this.divMain.Visible=true;
			this.btnSave.Enabled=true;
			return;
		}

		private void btnDivCODRemitNo_Click(object sender, System.EventArgs e)
		{
			this.divTotalCODAmountCollected.Visible=false;
			this.divMain.Visible=true;
			this.divCODREMITTANCE_DT.Visible=false; //Suppress divCODREMITTANCE_DT when click "No" button (by Sittichai 08/01/2008)
			this.btnSave.Enabled=true;
			return;
		}

		private void btnDivCODRemitYes_Click(object sender, System.EventArgs e)
		{
			SaveToDB(this.appID,this.enterpriseID);
		}


		private bool CheckDateTime()
		{
			if(this.ddlCODRefused.SelectedItem.Value=="Y")
				return true;

			DateTime maxCODAmountCollectedDT,maxVerifiedDT,maxRemittanceToCustomerDT;
				maxCODAmountCollectedDT=DateTime.ParseExact("01/01/1990","dd/MM/yyyy",null);
			maxVerifiedDT=DateTime.ParseExact("01/01/1990","dd/MM/yyyy",null);//DateTime.Now;
			maxRemittanceToCustomerDT=DateTime.ParseExact("01/01/1990","dd/MM/yyyy",null);//DateTime.Now;
			if(this.txtCODAmountCollectedDT.Text!="")
				maxCODAmountCollectedDT = DateTime.ParseExact(this.txtCODAmountCollectedDT.Text,"dd/MM/yyyy HH:mm",null);
			else
				return false;
			
			if(this.txtNETCODAmountVerifiedDT_Cash.Text.Length==10)
			{
				if(DateTime.ParseExact(this.txtNETCODAmountVerifiedDT_Cash.Text,"dd/MM/yyyy",null)<DateTime.ParseExact(maxCODAmountCollectedDT.ToString("dd/MM/yyyy"),"dd/MM/yyyy",null))
					return false;
			}
			else
			{   if(this.txtNETCODAmountVerifiedDT_Cash.Text.Length>10)
				if(DateTime.ParseExact(maxVerifiedDT.ToString("dd/MM/yyyy"),"dd/MM/yyyy",null)<DateTime.ParseExact(this.txtNETCODAmountVerifiedDT_Cash.Text.Remove(10,6).ToString(),"dd/MM/yyyy",null))
					maxVerifiedDT = DateTime.ParseExact(this.txtNETCODAmountVerifiedDT_Cash.Text,"dd/MM/yyyy HH:mm",null);
			}

			if(this.txtNETCODAmountVerifiedDT_Check.Text.Length==10)
			{
				if(DateTime.ParseExact(this.txtNETCODAmountVerifiedDT_Check.Text,"dd/MM/yyyy",null)<DateTime.ParseExact(maxCODAmountCollectedDT.ToString("dd/MM/yyyy"),"dd/MM/yyyy",null))
					return false;
			}
			else
			{
				if(this.txtNETCODAmountVerifiedDT_Check.Text.Length>10)
				if(DateTime.ParseExact(maxVerifiedDT.ToString("dd/MM/yyyy"),"dd/MM/yyyy",null)<DateTime.ParseExact(this.txtNETCODAmountVerifiedDT_Check.Text.Remove(10,6).ToString(),"dd/MM/yyyy",null))
					maxVerifiedDT = DateTime.ParseExact(this.txtNETCODAmountVerifiedDT_Check.Text,"dd/MM/yyyy HH:mm",null);
			}
			

			if(this.txtNETCODAmountVerifiedDT_Other.Text.Length==10)
			{
					if(DateTime.ParseExact(this.txtNETCODAmountVerifiedDT_Other.Text,"dd/MM/yyyy",null)<DateTime.ParseExact(maxCODAmountCollectedDT.ToString("dd/MM/yyyy"),"dd/MM/yyyy",null))
					return false;
			}
			else
			{
				if(this.txtNETCODAmountVerifiedDT_Other.Text.Length>10)
					if(DateTime.ParseExact(maxVerifiedDT.ToString("dd/MM/yyyy"),"dd/MM/yyyy",null)<DateTime.ParseExact(this.txtNETCODAmountVerifiedDT_Other.Text.Remove(10,6).ToString(),"dd/MM/yyyy",null))
						maxVerifiedDT = DateTime.ParseExact(this.txtNETCODAmountVerifiedDT_Other.Text,"dd/MM/yyyy HH:mm",null);
			}
//REMITTANCE
			if(this.txtCODRemittanceToCustomerDT_Cash.Text.Length==10)
				if(DateTime.ParseExact(this.txtCODRemittanceToCustomerDT_Cash.Text,"dd/MM/yyyy",null)<DateTime.ParseExact(maxVerifiedDT.ToString("dd/MM/yyyy"),"dd/MM/yyyy",null))
					return false;

			if(this.txtCODRemittanceToCustomerDT_Check.Text.Length==10)
				if(DateTime.ParseExact(this.txtCODRemittanceToCustomerDT_Check.Text,"dd/MM/yyyy",null)<DateTime.ParseExact(maxVerifiedDT.ToString("dd/MM/yyyy"),"dd/MM/yyyy",null))
					return false;

			if(this.txtCODRemittanceToCustomerDT_Other.Text.Length==10)
				if(DateTime.ParseExact(this.txtCODRemittanceToCustomerDT_Other.Text,"dd/MM/yyyy",null)<DateTime.ParseExact(maxVerifiedDT.ToString("dd/MM/yyyy"),"dd/MM/yyyy",null))
					return false;
				

			return true;

		}


		private bool CheckCODRefused()
		{
			if(this.ddlCODRefused.SelectedItem.Value=="Y")
			{  	
				this.divMain.Visible=false;
				this.divCODREMITTANCE_DT.Visible=false;
				this.divTotalCODAmountCollected.Visible=false;
				this.divInvoiced.Visible=false;
				this.divCODRefused.Visible=true;
				return false;
			}
			else
			{
				//CheckTotalCODAmount();
				return true;
			}
		}

		private bool CheckInvoiced(String strAppID, String strEnterpriseID, String strConsing_No)
		{
			//by sittichai  14/03/2008
			if(this.ddlCODRefused.SelectedItem.Value=="Y")
			{  	

				boolInvoiced = TIESDAL.PostDeliveryCOD.CheckInvoiceDate(appID,enterpriseID,strConsing_No);
				if (boolInvoiced==true)
				{
					this.divMain.Visible=false;
					this.divCODREMITTANCE_DT.Visible=false;
					this.divTotalCODAmountCollected.Visible=false;
					this.divInvoiced.Visible=false;
					this.divCODRefused.Visible=true;
					return false;							
				}
				else
				{
					this.divInvoiced.Visible=true;
					this.divMain.Visible=false;
					this.divCODREMITTANCE_DT.Visible=false;
					this.divTotalCODAmountCollected.Visible=false;
					this.divCODRefused.Visible=false;
					return false;	
					
				}						
			}
			else
			{
				//CheckTotalCODAmount();
				return true;
			}
		}


		private void btnDivCODRefusedYes_Click(object sender, System.EventArgs e)
		{
			if(Convert.ToString(ViewState["booking_no"])!="0")
				strBooking_no = Convert.ToString(ViewState["booking_no"]);
			else
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MDY_FAIL",utility.GetUserCulture());
				return;
			}

			if(Convert.ToString(ViewState["consignment_no"])!="0")
				strConsig_no = Convert.ToString(ViewState["consignment_no"]);
			else
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MDY_FAIL",utility.GetUserCulture());
				return;
			}

			if(Convert.ToString(ViewState["consignment_no"])!="0")
				strConsig_no = Convert.ToString(ViewState["consignment_no"]);
			else
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MDY_FAIL",utility.GetUserCulture());
				return;
			}


			Double CODAmountVAS;
			CODAmountVAS = TIESDAL.PostDeliveryCOD.GetCODAmountVAS(appID,enterpriseID,int.Parse(this.strBooking_no),this.strConsig_no);

			TIESDAL.PostDeliveryCOD.DeleteCODVAS(utility.GetAppID(),utility.GetEnterpriseID(),this.strBooking_no,this.strConsig_no,CODAmountVAS);

			CheckTotalCODAmount();


		}

		private void btnDivCODRfusedNo_Click(object sender, System.EventArgs e)
		{
			CheckTotalCODAmount();
		}

		private void txtNETCODAmountCollected_Cash_TextChanged(object sender, System.EventArgs e)
		{
			this.lblErrorMsg.Text="";
			//COD Amount VAS and COD Amount have to calculate automatic when fill the data in textbox (by Sittichai 08/01/2008)
			//lblTotalCODAmount.Text = this.txtNETCODAmountCollected_Cash.Text;
			Double TotalCODAmountCollected=0,txtNETCODAmountCollected_Cash=0,
				txtNETCODAmountCollected_Check=0,txtNETCODAmountCollected_Other=0,
				txtWithholdingTaxAmount=0,netCOD=0;

			txtNETCODAmountCollected_Cash = this.txtNETCODAmountCollected_Cash.Text!="" ? Convert.ToDouble(this.txtNETCODAmountCollected_Cash.Text) : 0;
			txtNETCODAmountCollected_Other = this.txtNETCODAmountCollected_Other.Text!="" ? Convert.ToDouble(this.txtNETCODAmountCollected_Other.Text) : 0;
			txtNETCODAmountCollected_Check = this.txtNETCODAmountCollected_Check.Text!="" ? Convert.ToDouble(this.txtNETCODAmountCollected_Check.Text) : 0;
			txtWithholdingTaxAmount = this.txtWithholdingTaxAmount.Text!="" ? Convert.ToDouble(this.txtWithholdingTaxAmount.Text) : 0;

			TotalCODAmountCollected = txtNETCODAmountCollected_Cash+txtNETCODAmountCollected_Check+txtNETCODAmountCollected_Other+txtWithholdingTaxAmount;
			netCOD = txtNETCODAmountCollected_Cash+txtNETCODAmountCollected_Check+txtNETCODAmountCollected_Other;

			lblTotalCODAmount.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(TotalCODAmountCollected));
			txtNetCODAmount.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(netCOD));

			if(!this.lblTotalCODAmount.Text.Equals(this.lblSI_CODam.Text))
				this.lblTotalCODAmount.ForeColor = Color.Red;
			else
				this.lblTotalCODAmount.ForeColor = Color.Black;
		}

		private void txtNETCODAmountCollected_Other_TextChanged(object sender, System.EventArgs e)
		{
			this.lblErrorMsg.Text="";
			//COD Amount VAS and COD Amount have to calculate automatic when fill the data in textbox (by Sittichai 08/01/2008)
				Double TotalCODAmountCollected=0,txtNETCODAmountCollected_Cash=0,
						txtNETCODAmountCollected_Check=0,txtNETCODAmountCollected_Other=0,
						txtWithholdingTaxAmount=0,netCOD=0;

			txtNETCODAmountCollected_Cash = this.txtNETCODAmountCollected_Cash.Text!="" ? Convert.ToDouble(this.txtNETCODAmountCollected_Cash.Text) : 0;
			txtNETCODAmountCollected_Other = this.txtNETCODAmountCollected_Other.Text!="" ? Convert.ToDouble(this.txtNETCODAmountCollected_Other.Text) : 0;
			txtNETCODAmountCollected_Check = this.txtNETCODAmountCollected_Check.Text!="" ? Convert.ToDouble(this.txtNETCODAmountCollected_Check.Text) : 0;
			txtWithholdingTaxAmount = this.txtWithholdingTaxAmount.Text!="" ? Convert.ToDouble(this.txtWithholdingTaxAmount.Text) : 0;

			TotalCODAmountCollected = txtNETCODAmountCollected_Cash+txtNETCODAmountCollected_Check+txtNETCODAmountCollected_Other+txtWithholdingTaxAmount;
			netCOD = txtNETCODAmountCollected_Cash+txtNETCODAmountCollected_Check+txtNETCODAmountCollected_Other;

			lblTotalCODAmount.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(TotalCODAmountCollected));
			txtNetCODAmount.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(netCOD));

			if(!this.lblTotalCODAmount.Text.Equals(this.lblSI_CODam.Text))
				this.lblTotalCODAmount.ForeColor = Color.Red;
			else
				this.lblTotalCODAmount.ForeColor = Color.Black;

		}

		private void txtNETCODAmountCollected_Check_TextChanged(object sender, System.EventArgs e)
		{
			this.lblErrorMsg.Text="";
			//COD Amount VAS and COD Amount have to calculate automatic when fill the data in textbox (by Sittichai 08/01/2008)
			Double TotalCODAmountCollected=0,txtNETCODAmountCollected_Cash=0,
				txtNETCODAmountCollected_Check=0,txtNETCODAmountCollected_Other=0,
				txtWithholdingTaxAmount=0,netCOD=0;

			txtNETCODAmountCollected_Cash = this.txtNETCODAmountCollected_Cash.Text!="" ? Convert.ToDouble(this.txtNETCODAmountCollected_Cash.Text) : 0;
			txtNETCODAmountCollected_Other = this.txtNETCODAmountCollected_Other.Text!="" ? Convert.ToDouble(this.txtNETCODAmountCollected_Other.Text) : 0;
			txtNETCODAmountCollected_Check = this.txtNETCODAmountCollected_Check.Text!="" ? Convert.ToDouble(this.txtNETCODAmountCollected_Check.Text) : 0;
			txtWithholdingTaxAmount = this.txtWithholdingTaxAmount.Text!="" ? Convert.ToDouble(this.txtWithholdingTaxAmount.Text) : 0;

			TotalCODAmountCollected = txtNETCODAmountCollected_Cash+txtNETCODAmountCollected_Check+txtNETCODAmountCollected_Other+txtWithholdingTaxAmount;
			netCOD = txtNETCODAmountCollected_Cash+txtNETCODAmountCollected_Check+txtNETCODAmountCollected_Other;

			lblTotalCODAmount.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(TotalCODAmountCollected));
			txtNetCODAmount.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(netCOD));

			if(!this.lblTotalCODAmount.Text.Equals(this.lblSI_CODam.Text))
				this.lblTotalCODAmount.ForeColor = Color.Red;
			else
				this.lblTotalCODAmount.ForeColor = Color.Black;

		}

		private void txtWithholdingTaxAmount_TextChanged(object sender, System.EventArgs e)
		{
			this.lblErrorMsg.Text="";
			//COD Amount VAS and COD Amount have to calculate automatic when fill the data in textbox (by Sittichai 08/01/2008)
			Double TotalCODAmountCollected=0,txtNETCODAmountCollected_Cash=0,
				txtNETCODAmountCollected_Check=0,txtNETCODAmountCollected_Other=0,
				txtWithholdingTaxAmount=0,netCOD=0;

			txtNETCODAmountCollected_Cash = this.txtNETCODAmountCollected_Cash.Text!="" ? Convert.ToDouble(this.txtNETCODAmountCollected_Cash.Text) : 0;
			txtNETCODAmountCollected_Other = this.txtNETCODAmountCollected_Other.Text!="" ? Convert.ToDouble(this.txtNETCODAmountCollected_Other.Text) : 0;
			txtNETCODAmountCollected_Check = this.txtNETCODAmountCollected_Check.Text!="" ? Convert.ToDouble(this.txtNETCODAmountCollected_Check.Text) : 0;
			txtWithholdingTaxAmount = this.txtWithholdingTaxAmount.Text!="" ? Convert.ToDouble(this.txtWithholdingTaxAmount.Text) : 0;

			TotalCODAmountCollected = txtNETCODAmountCollected_Cash+txtNETCODAmountCollected_Check+txtNETCODAmountCollected_Other+txtWithholdingTaxAmount;
			netCOD = txtNETCODAmountCollected_Cash+txtNETCODAmountCollected_Check+txtNETCODAmountCollected_Other;

			lblTotalCODAmount.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(TotalCODAmountCollected));
			txtNetCODAmount.Text = String.Format("{0:#,##0.00}",Convert.ToDouble(netCOD));

			if(!this.lblTotalCODAmount.Text.Equals(this.lblSI_CODam.Text))
				this.lblTotalCODAmount.ForeColor = Color.Red;
			else
				this.lblTotalCODAmount.ForeColor = Color.Black;

		}

		private void txtGoToRec_TextChanged(object sender, System.EventArgs e)
		{
			int iPageEnter = Convert.ToInt32(this.txtGoToRec.Text);
			if(iPageEnter<=0)
				iPageEnter=0;

			int iSetSize = Convert.ToInt32(m_sdsShipment.DataSetRecSize);

			if( iPageEnter> m_sdsShipment.QueryResultMaxSize)
			{
				this.txtGoToRec.Text="";
				return;
			}

			int iCurrentSet = (int)Math.Floor((double)(iPageEnter-1)/iSetSize);
			ViewState["currentSet"] = iCurrentSet;

			String tmpStr = ViewState["currentSet"].ToString().Trim();
			int iStartIndex = 0;
			int intIndexOf = tmpStr.IndexOf(".");

			if (intIndexOf > 0)
			{
				iStartIndex = Convert.ToInt32(tmpStr.Substring(0, intIndexOf)) * m_iSetSize;
			}
			else
			{
				iStartIndex = Convert.ToInt32(tmpStr) * m_iSetSize;
			}
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsShipment = TIESDAL.PostDeliveryCOD.GetPostDeliveryCODDS(utility.GetAppID(),utility.GetEnterpriseID(),iStartIndex,m_iSetSize,dsQuery);
			Session["SESSION_DS1"] = m_sdsShipment;

			System.Threading.Thread.Sleep(3500);

			int iCurrentPage = (int)(Math.IEEERemainder((iPageEnter-1),iSetSize)); //.DivRem(iPageEnter-1/iSetSize);

			if(iCurrentPage>=0)
				ViewState["currentPage"] = (int)Math.Abs(iCurrentPage);
			else
				ViewState["currentPage"] = iSetSize - (int)Math.Abs(iCurrentPage);

			
			DisplayCurrentPage();
			DisplayRecNum();

			EnableNavigationButtons(true,true,true,true);

			if( iPageEnter >=  m_sdsShipment.QueryResultMaxSize)
			{
				EnableNavigationButtons(true,true,false,false);
				this.txtGoToRec.Text="";
				return;
			}

			if(((Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)&&((Convert.ToInt32(ViewState["currentPage"]) - 1) < 0))
			{
				EnableNavigationButtons(false,false,true,true);
				this.txtGoToRec.Text="";
				return;
			}

			this.txtGoToRec.Text="";

		}

		private void btnInvoicedOK_Click(object sender, System.EventArgs e)
		{
			CheckTotalCODAmount();
		}

	}
}
