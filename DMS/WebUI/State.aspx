<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="State.aspx.cs" AutoEventWireup="false" Inherits="com.ties.State" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>State</title>
		<link href="css/Styles.css" type="text/css" rel="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script runat="server">
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="State" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 105; LEFT: 30px; POSITION: absolute; TOP: 64px" runat="server" CssClass="queryButton" Text="Query" CausesValidation="False" Width="60px"></asp:button>
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 90px; POSITION: absolute; TOP: 64px" runat="server" CssClass="queryButton" Text="Execute Query" CausesValidation="False" Width="130px"></asp:button>
			<asp:button id="btnInsert" style="Z-INDEX: 103; LEFT: 220px; POSITION: absolute; TOP: 64px" runat="server" CssClass="queryButton" Text="Insert" CausesValidation="False" Width="68px"></asp:button>
			<asp:DataGrid id="dgState" style="Z-INDEX: 101; LEFT: 30px; POSITION: absolute; TOP: 140px" runat="server" AllowPaging="True" OnEditCommand="dgState_Edit" OnCancelCommand="dgState_Cancel" OnUpdateCommand="dgState_Update" OnDeleteCommand="dgState_Delete" OnItemDataBound="dgState_Bound" OnPageIndexChanged="dgState_PageChange" AutoGenerateColumns="False" ItemStyle-Height="20" Width="654px" AllowCustomPaging="True">
				<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="4%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
						<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Country" HeaderStyle-Font-Bold="true">
						<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblCountry" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtCountry" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="50">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="cValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtCountry" ErrorMessage="Country"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="State Code" HeaderStyle-Font-Bold="true">
						<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblStateCode" Text='<%#DataBinder.Eval(Container.DataItem,"state_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtStateCode" Text='<%#DataBinder.Eval(Container.DataItem,"state_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="10">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="scValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtStateCode" ErrorMessage="State Code"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="State Name">
						<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblStateName" Text='<%#DataBinder.Eval(Container.DataItem,"state_name")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtStateName" Text='<%#DataBinder.Eval(Container.DataItem,"state_name")%>' Runat="server" MaxLength="100">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<%--  HC Return Task --%>
					<asp:TemplateColumn HeaderText="HC Return Within (Days)">
						<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField" HorizontalAlign=Right></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblReturnDays" style="text-align:right" Text='<%#DataBinder.Eval(Container.DataItem,"hc_return_within_days")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtReturnDays" style="text-align:right" Text='<%#DataBinder.Eval(Container.DataItem,"hc_return_within_days")%>' Runat="server" MaxLength="2">
							</asp:TextBox>
							<asp:RangeValidator id="Rangevalidator1" Runat="server" ErrorMessage="Pls enter valid value from 1 to 10" ControlToValidate="txtReturnDays" Display="None" BorderWidth="0" MaximumValue="10" MinimumValue="1" Type="Integer"></asp:RangeValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<%--  HC Return Task --%>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:DataGrid>
			<asp:label id="lblTitle" style="Z-INDEX: 111; LEFT: 30px; POSITION: absolute; TOP: 12px" runat="server" Width="477px" Height="26px" CssClass="mainTitleSize">State</asp:label>
			<asp:Label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 30px; POSITION: absolute; TOP: 95px" runat="server" Width="588px" Height="36px" CssClass="errorMsgColor">Place holder for err msg</asp:Label>
			<asp:validationsummary id="ValidationSummary1" style="Z-INDEX: 111; LEFT: 88px; POSITION: absolute; TOP: 96px" Height="36px" Width="346px" Runat="server" ShowMessageBox="True" DisplayMode="BulletList" HeaderText="Please enter the missing fields." ShowSummary="False"></asp:validationsummary>
		</form>
	</body>
</HTML>
