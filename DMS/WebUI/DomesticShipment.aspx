<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="DomesticShipment.aspx.cs" AutoEventWireup="false" Inherits="com.ties.DomesticShipment" smartNavigation="False" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DomesticShipment</title>
		<meta content="True" name="vs_snapToGrid">
		<meta content="True" name="vs_showGrid">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
			function callCodeBehind(cusid)
			{//debugger;
				var btn = document.getElementById('btnClientEvent');
				if(btn != null)
				{
					document.getElementById("txtCustID").value=cusid;
					btn.click();
				}else{
					alert('error call code behind');
				}
			}
			
			function callCodeBehindSvcCode()
			{		//debugger;									
				__doPostBack('<%= txtShipPckUpTime.ClientID  %>', '');				
			}
			
			function CalculateTotalAmtFunction()
			{//debugger;
				//var btn = document.getElementById("btnCmd");
				//btn.click();
				var decInsuranceChrg = 0;
				var decFrghtChrg = 0;
				var decTotOtherChrg = 0;
				var decTotVASSurcharge = 0;
				var decESASSurcharge = 0;
				var decTotalAmt = 0;
				
				var InsChrgValue= document.getElementById("txtInsChrg").value;
				var FreightChrgValue= document.getElementById("txtFreightChrg").value;
				var OtherSurcharge2Value= document.getElementById("txtOtherSurcharge2").value;
				var TotVASSurChrgValue= document.getElementById("txtTotVASSurChrg").value;
				var ESASurchrgValue= document.getElementById("txtESASurchrg").value;
				var objInsChrg= document.getElementById("txtShpTotAmt");
				
				if(InsChrgValue.length>0)
				{
					decInsuranceChrg = parseFloat(InsChrgValue);
				}
				if(FreightChrgValue.length>0)
				{
					decFrghtChrg = parseFloat(FreightChrgValue);
				}
				if(OtherSurcharge2Value.length>0)
				{
					decTotOtherChrg = parseFloat(OtherSurcharge2Value);
				}
				if(TotVASSurChrgValue.length>0)
				{
					decTotVASSurcharge = parseFloat(TotVASSurChrgValue);
				}
				if(ESASurchrgValue.length>0)
				{
					decESASSurcharge = parseFloat(ESASurchrgValue);
				}
				decTotalAmt = Math.round(decTotVASSurcharge + decFrghtChrg + decInsuranceChrg + decTotOtherChrg + decESASSurcharge);
				objInsChrg.value = parseFloat(decTotalAmt).toFixed(0);
				
			}
			function ValidateConNo(obj)
			{//debugger;
				//obj.value = Check2FontChar2LastChar(obj);
				__doPostBack('hddConNo','');
				return true;
			}
			
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="DomesticShipment" name="DomesticShipment" method="post" runat="server">
			<input id="hddConNo" style="DISPLAY: none" CausesValidation="False" type="button" name="hddCon"
				runat="server">
			<P><asp:label id="lblMainTitle" style="Z-INDEX: 106; POSITION: absolute; TOP: 8px; LEFT: 8px"
					runat="server" CssClass="mainTitleSize" Width="477px">Domestic Shipment</asp:label></P>
			<DIV id="divMain" style="Z-INDEX: 102; POSITION: relative; WIDTH: 1225px; HEIGHT: 1404px; TOP: 16px; LEFT: -1px"
				MS_POSITIONING="GridLayout" runat="server">
				<TABLE id="MainTable" style="Z-INDEX: 101; POSITION: absolute; WIDTH: 1064px; HEIGHT: 1309px; TOP: 8px; LEFT: 0px"
					width="1064" border="0" runat="server">
					<TR>
						<TD style="HEIGHT: 70px" width="738"><asp:button id="btnQry" runat="server" CssClass="queryButton" Width="61px" CausesValidation="False"
								Text="Query"></asp:button><asp:button id="btnExecQry" runat="server" CssClass="queryButton" Width="130px" CausesValidation="False"
								Text="Execute Query"></asp:button><asp:button id="btnInsert" runat="server" CssClass="queryButton" Width="62px" CausesValidation="False"
								Text="Insert"></asp:button><asp:button id="btnSave" runat="server" CssClass="queryButton" Width="66px" Text="Save"></asp:button><asp:button id="btnDelete" style="DISPLAY: none; VISIBILITY: hidden" runat="server" CssClass="queryButton"
								Width="62px" CausesValidation="False" Text="Delete"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:button id="btnClientEvent" style="DISPLAY: none" runat="server" Text="ClientEvent" CausesValidation="False"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:button id="btnGoToFirstPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False"
								Text="|<"></asp:button><asp:button id="btnPreviousPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False"
								Text="<"></asp:button><asp:textbox id="txtGoToRec" tabIndex="8" runat="server" Width="23px" Enabled="False" Height="19px"></asp:textbox><asp:button id="btnNextPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False"
								Text=">"></asp:button><asp:button id="btnGoToLastPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False"
								Text=">|"></asp:button><asp:label id="lblErrorMsg" runat="server" CssClass="errorMsgColor" Width="566px" Height="19px"></asp:label><asp:regularexpressionvalidator id="Regularpickupdate" runat="server" CssClass="errorMsgColor" Width="467px" ValidationExpression="^((1[0-2]|(0[1-9])|[1][\d]|[2][\d]|[3][0]|[3][1])\/((0[1-9])|(1[0-2]))\/([\d][\d][\d][\d])[\s]((0[0-9])|[1][\d]|(2[0-4]))\:(0[0-9]|(1[\d]|2[\d]|3[\d]|4[\d]|5[\d]|6[0])))"
								Display="None" ControlToValidate="txtShipPckUpTime"></asp:regularexpressionvalidator><asp:regularexpressionvalidator id="RegularEstDlvryDt" runat="server" CssClass="errorMsgColor" Width="467px" ValidationExpression="^((1[0-2]|(0[1-9])|[1][\d]|[2][\d]|[3][0]|[3][1])\/((0[1-9])|(1[0-2]))\/([\d][\d][\d][\d])[\s]((0[0-9])|[1][\d]|(2[0-4]))\:(0[0-9]|(1[\d]|2[\d]|3[\d]|4[\d]|5[\d]|6[0])))"
								Display="None" ControlToValidate="txtShipEstDlvryDt"></asp:regularexpressionvalidator><asp:regularexpressionvalidator id="RegularShpManfstDt" style="Z-INDEX: 117; POSITION: absolute; TOP: 80px; LEFT: 0px"
								runat="server" CssClass="errorMsgColor" Width="467px" ValidationExpression="^((1[0-2]|(0[1-9])|[1][\d]|[2][\d]|[3][0]|[3][1])\/((0[1-9])|(1[0-2]))\/([\d][\d][\d][\d])[\s]((0[0-9])|[1][\d]|(2[0-4]))\:(0[0-9]|(1[\d]|2[\d]|3[\d]|4[\d]|5[\d]|6[0])))"
								Display="None" ControlToValidate="txtShipManifestDt"></asp:regularexpressionvalidator><asp:validationsummary id="PageValidationSummary" runat="server" Height="35px" Visible="True" ShowSummary="False"
								ShowMessageBox="True"></asp:validationsummary><asp:regularexpressionvalidator id="Regularexpressionvalidator1" style="Z-INDEX: 117; POSITION: absolute; TOP: 80px; LEFT: 0px"
								runat="server" CssClass="errorMsgColor" Width="467px" ValidationExpression="^((1[0-2]|(0[1-9])|[1][\d]|[2][\d]|[3][0]|[3][1])\/((0[1-9])|(1[0-2]))\/([\d][\d][\d][\d])[\s]((0[0-9])|[1][\d]|(2[0-4]))\:(0[0-9]|(1[\d]|2[\d]|3[\d]|4[\d]|5[\d]|6[0])))"
								Display="None" ControlToValidate="txtBookDate"></asp:regularexpressionvalidator></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 9px" vAlign="baseline" width="738">
							<TABLE id="Table6" style="WIDTH: 816px; HEIGHT: 8px" cellSpacing="1" cellPadding="1" border="0">
								<TR>
									<TD style="WIDTH: 1px; HEIGHT: 8px" width="1" height="8"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 255px; HEIGHT: 8px" vAlign="baseline" align="left"><FONT face="Tahoma"><asp:label id="lblBookingNo" runat="server" CssClass="tableLabel" Width="80px">Booking #</asp:label><cc1:mstextbox id="txtBookingNo" tabIndex="1" runat="server" CssClass="textField" Width="167px"
												NumberPrecision="10" NumberMaxValue="2147483647" TextMaskType="msNumeric" MaxLength="10" AutoPostBack="True"></cc1:mstextbox></FONT></TD>
									<TD style="WIDTH: 197px; HEIGHT: 8px" vAlign="baseline" align="left"><asp:label id="lblBookDate" runat="server" CssClass="tableLabel" Width="80px">Booking D/T</asp:label><cc1:mstextbox id="txtBookDate" tabIndex="7" runat="server" CssClass="textField" Width="114px"
											TextMaskType="msDateTime" MaxLength="16" TextMaskString="99/99/9999 99:99"></cc1:mstextbox></TD>
									<TD style="WIDTH: 169px; HEIGHT: 8px" vAlign="baseline" align="left"><FONT face="Tahoma"><asp:label id="lblOrigin" runat="server" CssClass="tableLabel" Width="86px">Ori. Province</asp:label><asp:textbox id="txtOrigin" tabIndex="3" runat="server" CssClass="textField" Width="80px" Enabled="False"
												MaxLength="10"></asp:textbox></FONT></TD>
									<TD style="HEIGHT: 8px" vAlign="baseline" align="left"><asp:label id="lblStatusCode" runat="server" CssClass="tableLabel" Width="94px">LAST Status</asp:label><asp:textbox id="txtLatestStatusCode" tabIndex="8" runat="server" CssClass="textField" Width="70px"
											Enabled="False"></asp:textbox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 1px; HEIGHT: 21px"><FONT face="Tahoma"><asp:requiredfieldvalidator id="validConsgNo" Width="3px" ControlToValidate="txtConsigNo" ErrorMessage="Consignment No"
												Runat="server">*</asp:requiredfieldvalidator></FONT></TD>
									<TD style="WIDTH: 255px; HEIGHT: 21px"><asp:label id="lblConsgmtNo" runat="server" CssClass="tableLabel" Width="80px">Con. #</asp:label><asp:textbox id="txtConsigNo" onblur="return ValidateConNo(this);" style="TEXT-TRANSFORM: uppercase"
											tabIndex="4" runat="server" CssClass="textField" Width="167px" MaxLength="21" AutoPostBack="false"></asp:textbox></TD>
									<TD style="WIDTH: 197px; HEIGHT: 21px"><asp:label id="lblShipManifestDt" runat="server" CssClass="tableLabel" Width="80px"> Manifest D/T</asp:label><cc1:mstextbox id="txtShipManifestDt" tabIndex="7" runat="server" CssClass="textField" Width="114px"
											TextMaskType="msDateTime" MaxLength="16" TextMaskString="99/99/9999 99:99"></cc1:mstextbox></TD>
									<TD style="WIDTH: 169px; HEIGHT: 21px"><asp:label id="lblDestination" runat="server" CssClass="tableLabel" Width="86px">Dest. Province</asp:label><asp:textbox id="txtDestination" tabIndex="6" runat="server" CssClass="textField" Width="80px"
											Enabled="False" MaxLength="10"></asp:textbox></TD>
									<TD style="HEIGHT: 21px"><asp:label id="lblExcepCode" runat="server" CssClass="tableLabel" Width="94px">LAST Exception</asp:label><asp:textbox id="txtLatestExcepCode" tabIndex="11" runat="server" CssClass="textField" Width="70px"
											Enabled="False"></asp:textbox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 1px; HEIGHT: 21px"></TD>
									<TD style="WIDTH: 255px; HEIGHT: 21px"><FONT face="Tahoma"><asp:label id="lblRefNo" runat="server" CssClass="tableLabel" Width="80px">Cust. Ref. #</asp:label><asp:textbox id="txtRefNo" tabIndex="5" runat="server" CssClass="textField" Width="167px" MaxLength="20"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 197px; HEIGHT: 21px"><FONT face="Tahoma"><asp:label id="lblDelManifest" runat="server" CssClass="tableLabel" Width="80px">Auto Manifest</asp:label><asp:textbox id="txtDelManifest" tabIndex="12" runat="server" CssClass="textField" Width="40px"
												Enabled="False"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 169px; HEIGHT: 21px"><asp:label id="lblRouteCode" runat="server" CssClass="tableLabel" Width="86px">Route Code</asp:label><asp:textbox id="txtRouteCode" runat="server" CssClass="textField" Width="80px" Enabled="False"
											MaxLength="12" ReadOnly="True"></asp:textbox></TD>
									<TD style="HEIGHT: 21px"><FONT face="Tahoma"><asp:button id="btnRouteCode" tabIndex="10" runat="server" CssClass="searchButton" Width="21px"
												CausesValidation="False" Text="..." Enabled="False" Height="19px" Visible="False"></asp:button></FONT></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<tr>
						<td style="WIDTH: 738px; HEIGHT: 176px" align="left" width="738">
							<TABLE id="Table8" style="WIDTH: 972px; HEIGHT: 154px" cellSpacing="1" cellPadding="1"
								border="0">
								<TR>
									<TD style="WIDTH: 115px; HEIGHT: 3px"><FONT face="Tahoma"><asp:label id="lblCustInfo" CssClass="tableHeadingFieldset" Width="116px" Runat="server">CUSTOMER PROFILE</asp:label></FONT></TD>
									<TD style="WIDTH: 10px; HEIGHT: 3px"><asp:requiredfieldvalidator id="validCustID" Width="3px" ControlToValidate="txtCustID" Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 16px; HEIGHT: 3px"><FONT face="Tahoma"><asp:label id="lblCustID" runat="server" CssClass="tableLabel" Width="16px"> ID</asp:label></FONT></TD>
									<TD style="WIDTH: 218px; HEIGHT: 3px" colSpan="3"><FONT face="Tahoma"><cc1:mstextbox id="txtCustID" style="TEXT-TRANSFORM: uppercase" tabIndex="15" runat="server" CssClass="textField"
												Width="68px" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="20" AutoPostBack="True"></cc1:mstextbox><asp:button id="btnDisplayCustDtls" runat="server" CssClass="searchButton" Width="21px" CausesValidation="False"
												Text="..." Height="19px"></asp:button><asp:dropdownlist id="ddbCustType" tabIndex="13" runat="server" Width="72px" Visible="False"></asp:dropdownlist></FONT></TD>
									<TD style="WIDTH: 217px; HEIGHT: 3px" colSpan="2"><FONT face="Tahoma"><asp:textbox id="txtCustAdd1" tabIndex="18" runat="server" CssClass="textField" Width="40px"
												Visible="False" MaxLength="100" AutoPostBack="True"></asp:textbox><asp:textbox id="txtCustFax" tabIndex="24" runat="server" CssClass="textField" Width="48px" Visible="False"
												MaxLength="20" AutoPostBack="True"></asp:textbox><asp:label id="lblCustType" runat="server" CssClass="tableLabel" Width="64px" Visible="False">Cust Type</asp:label><asp:requiredfieldvalidator id="validCustType" Width="3px" ControlToValidate="ddbCustType" Visible="False" Runat="server">*</asp:requiredfieldvalidator></FONT></TD>
									<TD style="WIDTH: 13px; HEIGHT: 3px"></TD>
									<TD style="WIDTH: 31px; HEIGHT: 3px"><asp:requiredfieldvalidator id="validCustAdd1" Width="3px" ControlToValidate="txtCustAdd1" Visible="False" ErrorMessage="Customer Address"
											Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 215px; HEIGHT: 3px" colSpan="2"><FONT face="Tahoma"><asp:textbox id="txtCustAdd2" tabIndex="19" runat="server" CssClass="textField" Width="65px"
												Visible="False" AutoPostBack="True"></asp:textbox><asp:label id="lblTelphone" tabIndex="100" runat="server" CssClass="tableLabel" Width="58px"
												Visible="False">Telephone</asp:label></FONT></TD>
									<TD style="WIDTH: 212px; HEIGHT: 3px"><FONT face="Tahoma"><asp:checkbox id="chkNewCust" tabIndex="14" runat="server" CssClass="tableLabel" Width="104px"
												Text="New Customer" Height="16px" Visible="False" AutoPostBack="True"></asp:checkbox></FONT></TD>
									<TD style="WIDTH: 129px; HEIGHT: 3px"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px; HEIGHT: 17px"><FONT face="Tahoma">&nbsp;<asp:label id="lblAddress" runat="server" CssClass="tableLabel" Width="48px" Visible="False">Address</asp:label></FONT></TD>
									<TD style="WIDTH: 10px; HEIGHT: 17px"><asp:requiredfieldvalidator id="validCustName" Width="3px" ControlToValidate="txtCustName" Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 16px; HEIGHT: 17px"><FONT face="Tahoma"><asp:label id="lblName" runat="server" CssClass="tableLabel" Width="32px">Name</asp:label></FONT></TD>
									<TD style="WIDTH: 218px; HEIGHT: 17px" colSpan="3"><FONT face="Tahoma"><asp:textbox id="txtCustName" runat="server" CssClass="textField" Width="214px" MaxLength="100"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 217px; HEIGHT: 17px" colSpan="2"><FONT face="Tahoma"><asp:label id="lblPaymode" runat="server" CssClass="tableLabel" Width="64px"> Payment</asp:label><asp:radiobutton id="rbCash" runat="server" Width="16px" Text="Cash" Enabled="False" GroupName="PaymentType"
												Font-Size="Smaller"></asp:radiobutton><asp:radiobutton id="rbCredit" runat="server" Width="16px" Text="Credit" Enabled="False" GroupName="PaymentType"
												Font-Size="Smaller"></asp:radiobutton></FONT></TD>
									<TD style="WIDTH: 13px; HEIGHT: 17px"><asp:requiredfieldvalidator id="validCustZip" Width="3px" ControlToValidate="txtCustZipCode" Visible="False"
											ErrorMessage="Customer Postal Code" Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 31px; HEIGHT: 17px"><asp:label id="lblZip" runat="server" CssClass="tableLabel" Width="48px" Visible="False"> Zipcode</asp:label></TD>
									<TD style="WIDTH: 215px; HEIGHT: 17px" align="left" colSpan="2"><asp:textbox id="txtCustZipCode" tabIndex="20" runat="server" CssClass="textField" Width="45px"
											Visible="False" MaxLength="10" AutoPostBack="True"></asp:textbox><asp:textbox id="txtCustCity" runat="server" CssClass="textField" Width="24px" Visible="False"
											ReadOnly="True"></asp:textbox><asp:textbox id="txtCustTelephone" tabIndex="23" runat="server" CssClass="textField" Width="32px"
											Visible="False" MaxLength="20" AutoPostBack="True"></asp:textbox><asp:label id="lblFax" tabIndex="100" runat="server" CssClass="tableLabel" Width="7px" Visible="False">Fax</asp:label></TD>
									<TD style="WIDTH: 212px; HEIGHT: 17px"><asp:textbox id="txtCustStateCode" runat="server" CssClass="textField" Width="79px" Visible="False"
											ReadOnly="True"></asp:textbox></TD>
									<TD style="WIDTH: 129px; HEIGHT: 17px"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px" vAlign="top" noWrap colSpan="14" height="1">
										<HR style="WIDTH: 842.82%; HEIGHT: 1px" width="842.82%" SIZE="1">
									</TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px; HEIGHT: 12px"><asp:label id="lblSendInfo" CssClass="tableHeadingFieldset" Width="40px" Runat="server">FROM:</asp:label><asp:checkbox id="chkSendCustInfo" runat="server" CssClass="tableLabel" Width="57px" Text="Same"
											AutoPostBack="True"></asp:checkbox></TD>
									<TD style="WIDTH: 10px; HEIGHT: 12px"><asp:requiredfieldvalidator id="validSendName" Width="3px" ControlToValidate="txtSendName" Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 16px; HEIGHT: 12px"><FONT face="Tahoma"><asp:label id="lblSendNm" runat="server" CssClass="tableLabel" Width="34px">Name</asp:label></FONT></TD>
									<TD style="WIDTH: 218px; HEIGHT: 12px" colSpan="3"><FONT face="Tahoma"><asp:textbox id="txtSendName" runat="server" CssClass="textField" Width="192px" MaxLength="100"></asp:textbox><asp:button id="btnSendCust" runat="server" CssClass="searchButton" CausesValidation="False"
												Text="..."></asp:button></FONT></TD>
									<TD style="WIDTH: 217px; HEIGHT: 12px" colSpan="2"><FONT face="Tahoma"></FONT><FONT face="Tahoma"><asp:textbox id="txtSendAddr1" runat="server" CssClass="textField" Width="200px" MaxLength="100"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 13px; HEIGHT: 12px"></TD>
									<TD style="WIDTH: 31px; HEIGHT: 12px"></TD>
									<TD style="WIDTH: 215px; HEIGHT: 12px" colSpan="2"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 212px; HEIGHT: 12px"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 129px; HEIGHT: 12px"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px; HEIGHT: 24px"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 10px; HEIGHT: 24px"><asp:requiredfieldvalidator id="validSenderAdd1" Width="3px" ControlToValidate="txtSendAddr1" Visible="False"
											Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 16px; HEIGHT: 24px"><asp:label id="lblSendAddr1" runat="server" CssClass="tableLabel" Width="42px" Visible="False">Address</asp:label></TD>
									<TD style="WIDTH: 218px; HEIGHT: 24px" colSpan="3"></TD>
									<TD style="WIDTH: 217px; HEIGHT: 24px" colSpan="2"><FONT face="Tahoma"><asp:textbox id="txtSendAddr2" runat="server" CssClass="textField" Width="200px" MaxLength="100"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 13px; HEIGHT: 24px"><asp:requiredfieldvalidator id="validSendZip" Width="3px" ControlToValidate="txtSendZip" Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 31px; HEIGHT: 24px"><asp:label id="lblSendZip" runat="server" CssClass="tableLabel" Width="48px"> Zipcode</asp:label></TD>
									<TD style="HEIGHT: 24px" align="left" width="100%" colSpan="2"><asp:textbox id="txtSendZip" runat="server" CssClass="textField" Width="45px" MaxLength="10"
											AutoPostBack="True"></asp:textbox><asp:textbox id="txtSendCity" runat="server" CssClass="textField" Width="123px" ReadOnly="True"></asp:textbox></TD>
									<TD style="WIDTH: 212px; HEIGHT: 24px"><asp:textbox id="txtSendState" runat="server" CssClass="textField" Width="79px" Visible="False"
											ReadOnly="True"></asp:textbox></TD>
									<TD style="WIDTH: 129px; HEIGHT: 24px"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px; HEIGHT: 17px" height="17"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 10px; HEIGHT: 17px" height="17"></TD>
									<TD style="WIDTH: 16px; HEIGHT: 17px" height="17"><asp:label id="lblSendConPer" runat="server" CssClass="tableLabel" Width="40px">Contact</asp:label></TD>
									<TD style="WIDTH: 143px; HEIGHT: 17px" colSpan="2" height="17"><asp:textbox id="txtSendContPer" runat="server" CssClass="textField" Width="150px" MaxLength="100"></asp:textbox></TD>
									<TD style="WIDTH: 63px; HEIGHT: 17px" height="17"><asp:label id="lblSendTel" tabIndex="100" runat="server" CssClass="tableLabel" Width="52px">Telephone</asp:label></TD>
									<TD style="WIDTH: 217px; HEIGHT: 17px" colSpan="2" height="17"><FONT face="Tahoma"><asp:textbox id="txtSendTel" runat="server" CssClass="textField" Width="200px" MaxLength="20"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 13px; HEIGHT: 17px" height="17"></TD>
									<TD style="WIDTH: 31px; HEIGHT: 17px" height="17"><asp:label id="lblSendFax" tabIndex="100" runat="server" CssClass="tableLabel" Width="20px">Fax</asp:label></TD>
									<TD style="HEIGHT: 17px" width="172" colSpan="2"><FONT face="Tahoma"><asp:textbox id="txtSendFax" runat="server" CssClass="textField" Width="173px" MaxLength="20"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 212px; HEIGHT: 17px" height="17"><cc1:mstextbox id="txtSendCuttOffTime" runat="server" CssClass="textField" Width="62px" Visible="False"
											TextMaskType="msTime" MaxLength="5" TextMaskString="99:99" ReadOnly="True"></cc1:mstextbox></TD>
									<TD style="WIDTH: 129px; HEIGHT: 17px" height="17"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px" vAlign="top" noWrap colSpan="14" height="1">
										<HR style="WIDTH: 842.82%; HEIGHT: 1px" width="842.82%" SIZE="1">
									</TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px; HEIGHT: 8px"><asp:label id="lblRecInfo" CssClass="tableHeadingFieldset" Width="40px" Runat="server">TO:</asp:label><asp:checkbox id="chkRecip" tabIndex="34" runat="server" CssClass="tableLabel" Width="57px" Text="Same"
											AutoPostBack="True"></asp:checkbox></TD>
									<TD style="WIDTH: 10px; HEIGHT: 8px"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 16px; HEIGHT: 8px"><asp:label id="lblRecipTelephone" tabIndex="83" runat="server" CssClass="tableLabel" Width="48px">Tel. 1</asp:label></TD>
									<TD style="WIDTH: 141px; HEIGHT: 8px"><asp:textbox id="txtRecipTel" tabIndex="35" runat="server" CssClass="textField" Width="114px"
											MaxLength="20"></asp:textbox><asp:button id="btnTelephonePopup" tabIndex="36" runat="server" CssClass="searchButton" CausesValidation="False"
											Text="..."></asp:button></TD>
									<TD style="WIDTH: 2px; HEIGHT: 8px"><asp:requiredfieldvalidator id="validRecipName" Width="1px" ControlToValidate="txtRecName" Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 63px; HEIGHT: 8px"><asp:label id="lblRecipNm" tabIndex="100" runat="server" CssClass="tableLabel" Width="26px">Name</asp:label></TD>
									<TD style="WIDTH: 217px; HEIGHT: 8px" colSpan="2"><FONT face="Tahoma"><asp:textbox id="txtRecName" tabIndex="37" runat="server" CssClass="textField" Width="178px"
												MaxLength="100"></asp:textbox><asp:button id="btnRecipNm" tabIndex="38" runat="server" CssClass="searchButton" CausesValidation="False"
												Text="..."></asp:button></FONT></TD>
									<TD style="WIDTH: 13px; HEIGHT: 8px"></TD>
									<TD style="WIDTH: 31px; HEIGHT: 8px"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 215px; HEIGHT: 8px" colSpan="2"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 212px; HEIGHT: 8px"><asp:label id="lblSendCuttOffTime" runat="server" CssClass="tableLabel" Width="43px" Visible="False">Cut-off</asp:label></TD>
									<TD style="WIDTH: 129px; HEIGHT: 8px"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px; HEIGHT: 22px"></TD>
									<TD style="WIDTH: 10px; HEIGHT: 22px"></TD>
									<TD style="WIDTH: 16px; HEIGHT: 22px"></TD>
									<TD style="WIDTH: 141px; HEIGHT: 22px"></TD>
									<TD style="WIDTH: 2px; HEIGHT: 22px"><asp:requiredfieldvalidator id="validRecipAdd1" Width="3px" ControlToValidate="txtRecipAddr1" Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 63px; HEIGHT: 22px"><asp:label id="lblRecipAddr1" tabIndex="100" runat="server" CssClass="tableLabel" Width="32px">Address</asp:label></TD>
									<TD style="WIDTH: 217px; HEIGHT: 22px" colSpan="2"><asp:textbox id="txtRecipAddr1" tabIndex="39" runat="server" CssClass="textField" Width="200px"
											MaxLength="100"></asp:textbox></TD>
									<TD style="WIDTH: 13px; HEIGHT: 22px"></TD>
									<TD style="WIDTH: 268px; HEIGHT: 22px" colSpan="3"><FONT face="Tahoma"><asp:textbox id="txtRecipAddr2" tabIndex="40" runat="server" CssClass="textField" Width="225px"
												MaxLength="100"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 212px; HEIGHT: 22px"><asp:textbox id="txtRecipState" runat="server" CssClass="textField" Width="56px" Visible="False"
											ReadOnly="True"></asp:textbox></TD>
									<TD style="WIDTH: 129px; HEIGHT: 22px"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 115px">
										<P><FONT face="Tahoma"><asp:button id="btnPopulateVAS" tabIndex="57" CssClass="queryButton" Width="112px" CausesValidation="False"
													Text="GetQuotationVAS" Enabled="False" Visible="False" Runat="server"></asp:button></FONT></P>
									</TD>
									<TD style="WIDTH: 10px"><FONT face="Tahoma"></FONT></TD>
									<TD style="WIDTH: 16px"><asp:label id="lblRecpContPer" tabIndex="100" runat="server" CssClass="tableLabel" Width="40px">Contact</asp:label></TD>
									<TD style="WIDTH: 143px" colSpan="2"><asp:textbox id="txtRecpContPer" tabIndex="42" runat="server" CssClass="textField" Width="150px"
											MaxLength="100"></asp:textbox></TD>
									<TD style="WIDTH: 63px"><asp:label id="lblRecipFax" tabIndex="85" runat="server" CssClass="tableLabel" Width="40px">Tel. 2</asp:label></TD>
									<TD style="WIDTH: 217px" colSpan="2"><FONT face="Tahoma"><asp:textbox id="txtRecipFax" tabIndex="43" runat="server" CssClass="textField" Width="200px"
												MaxLength="20"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 13px"><asp:requiredfieldvalidator id="validRecipZip" Width="3px" ControlToValidate="txtRecipZip" Runat="server">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 31px"><FONT face="Tahoma"><asp:label id="lblRecipZip" tabIndex="100" runat="server" CssClass="tableLabel" Width="48px"> Zipcode</asp:label></FONT></TD>
									<TD style="WIDTH: 215px" align="left" colSpan="2"><asp:textbox id="txtRecipZip" tabIndex="41" runat="server" CssClass="textField" Width="45px"
											MaxLength="10" AutoPostBack="True"></asp:textbox><asp:textbox id="txtRecipCity" runat="server" CssClass="textField" Width="123px" ReadOnly="True"></asp:textbox></TD>
									<TD style="WIDTH: 212px"><FONT face="Tahoma"><asp:button id="btnBind" tabIndex="58" CssClass="queryButton" CausesValidation="False" Text="Refresh"
												Visible="False" Runat="server"></asp:button></FONT></TD>
									<TD style="WIDTH: 129px"></TD>
								</TR>
							</TABLE>
						</td>
					</tr>
					<TR>
						<TD style="WIDTH: 738px; HEIGHT: 18px" align="left" width="738">
							<TABLE id="Table5" style="WIDTH: 930px; HEIGHT: 36px" cellSpacing="1" cellPadding="1" border="0">
								<TR>
									<TD style="WIDTH: 96px"><asp:button id="btnPkgDetails" tabIndex="46" runat="server" CssClass="queryButton" Width="112px"
											CausesValidation="False" Text="Package Detail"></asp:button></TD>
									<TD style="WIDTH: 100px"><asp:label id="lblPkgActualWt" runat="server" CssClass="tableLabel" Width="80px">Total Act. Wt.</asp:label></TD>
									<TD style="WIDTH: 73px"><FONT face="Tahoma"><asp:textbox id="txtActualWeight" runat="server" CssClass="textFieldRightAlign" Width="80px"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 92px"><asp:label id="lblPkgTotPkgs" runat="server" CssClass="tableLabel" Width="88px">Total Pkgs.</asp:label></TD>
									<TD style="WIDTH: 69px"><asp:textbox id="txtPkgTotpkgs" runat="server" CssClass="textFieldRightAlign" Width="80px" ReadOnly="False"></asp:textbox></TD>
									<TD style="WIDTH: 69px"><asp:label id="lblPkgChargeWt" runat="server" CssClass="tableLabel" Width="88px">Total Chg.Wt.</asp:label></TD>
									<TD><FONT face="Tahoma"><asp:textbox id="txtPkgChargeWt" runat="server" CssClass="textFieldRightAlign" Width="80px" ReadOnly="False"></asp:textbox></FONT></TD>
									<td><FONT face="Tahoma"><asp:label id="lblPkgCommCode" runat="server" CssClass="tableLabel" Width="100px" Visible="true">Commodity Code</asp:label></FONT></td>
									<td><FONT face="Tahoma"></FONT><asp:textbox id="txtPkgCommCode" tabIndex="44" runat="server" CssClass="textField" Width="100px"
											Visible="true" MaxLength="12" AutoPostBack="True"></asp:textbox><asp:button id="btnPkgCommCode" tabIndex="45" runat="server" CssClass="searchButton" CausesValidation="False"
											Text="..."></asp:button></td>
								</TR>
								<TR>
									<TD style="WIDTH: 96px"><asp:button id="btnViewOldPD" tabIndex="46" runat="server" CssClass="queryButton" Width="112px"
											CausesValidation="False" Text="View Old PD" Visible="False"></asp:button><asp:textbox id="txtPkgCommDesc" runat="server" CssClass="textField" Width="112px" Enabled="False"
											Visible="False" ReadOnly="False"></asp:textbox></TD>
									<TD style="WIDTH: 100px"><asp:label id="Label6" runat="server" CssClass="tableLabel" Width="96px">Total Act. R Wt.</asp:label></TD>
									<TD style="WIDTH: 73px"><FONT face="Tahoma"><asp:textbox id="txtPkgActualWt" runat="server" CssClass="textFieldRightAlign" Width="80px" ReadOnly="False"></asp:textbox></FONT></TD>
									<TD style="WIDTH: 92px"><asp:label id="lblPkgDimWt" runat="server" CssClass="tableLabel" Width="86px">Total Dim. Wt.</asp:label></TD>
									<TD style="WIDTH: 69px"><asp:textbox id="txtPkgDimWt" runat="server" CssClass="textFieldRightAlign" Width="80px" ReadOnly="False"></asp:textbox></TD>
									<TD style="WIDTH: 69px"><FONT face="Tahoma"><asp:label id="lblTotalVol" runat="server" CssClass="tableLabel" Width="88px">Total Vol.</asp:label></FONT></TD>
									<TD><asp:textbox id="txttotVol" runat="server" CssClass="textFieldRightAlign" Width="80px" ReadOnly="False"></asp:textbox></TD>
									<td colSpan="2"><FONT face="Tahoma"><asp:label id="lblPD_Replace_Date" runat="server" CssClass="tableLabel" Visible="False"></asp:label></FONT></td>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<tr>
						<td style="WIDTH: 738px; HEIGHT: 52px" align="left" width="738"><FONT face="Tahoma">
								<TABLE id="tblShipService" style="WIDTH: 938px; HEIGHT: 16px" width="938" border="0" runat="server">
									<TR height="25">
										<TD style="WIDTH: 16px; HEIGHT: 21px" width="16"><asp:requiredfieldvalidator id="validSrvcCode" ControlToValidate="txtShpSvcCode" Runat="server">*</asp:requiredfieldvalidator></TD>
										<TD style="WIDTH: 156px; HEIGHT: 21px" width="156"><asp:label id="lblShipSerCode" runat="server" CssClass="tableLabel" Width="100px">Service Type</asp:label></TD>
										<TD style="WIDTH: 46px; HEIGHT: 21px" width="46"><cc1:mstextbox id="txtShpSvcCode" style="TEXT-TRANSFORM: uppercase" tabIndex="47" runat="server"
												CssClass="textField" Width="74px" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" AutoPostBack="True"></cc1:mstextbox></TD>
										<TD style="WIDTH: 3px; HEIGHT: 21px" width="3"><asp:button id="btnShpSvcCode" tabIndex="48" runat="server" CssClass="searchButton" CausesValidation="False"
												Text="..."></asp:button></TD>
										<TD style="WIDTH: 36px; HEIGHT: 21px" width="36" colSpan="2"><asp:textbox id="txtShpSvcDesc" runat="server" CssClass="textField" Width="202px" Visible="False"
												ReadOnly="True"></asp:textbox></TD>
										<TD style="WIDTH: 126px; HEIGHT: 21px" width="126"><asp:label id="lblShipPickUpTime" runat="server" CssClass="tableLabel" Width="108px">Actual Pickup D/T</asp:label></TD>
										<TD style="HEIGHT: 21px" width="20%"><asp:textbox id="txtShipPckUpTime" runat="server" CssClass="textField" Width="114px" AutoPostBack="True"></asp:textbox></TD>
										<TD style="WIDTH: 101px; HEIGHT: 21px" width="101"></TD>
										<TD style="HEIGHT: 21px" width="20%"></TD>
									</TR>
									<TR height="25">
										<TD style="WIDTH: 16px; HEIGHT: 15px" width="16"></TD>
										<TD style="WIDTH: 156px; HEIGHT: 15px" width="156"><asp:label id="lblShipDclValue" runat="server" CssClass="tableLabel" Width="120px">Declared Value THB</asp:label></TD>
										<TD style="WIDTH: 46px; HEIGHT: 15px" width="46"><asp:textbox id="txtShpDclrValue" tabIndex="49" runat="server" CssClass="textFieldRightAlign"
												Width="74px" AutoPostBack="True"></asp:textbox></TD>
										<TD style="WIDTH: 3px; HEIGHT: 15px" width="3"></TD>
										<TD style="WIDTH: 77px; HEIGHT: 15px" width="77"><FONT face="Tahoma"><asp:label id="lblShipMaxCovg" runat="server" CssClass="tableLabel" Width="120px">Maximum Coverage</asp:label></FONT></TD>
										<TD style="WIDTH: 19px; HEIGHT: 15px" width="19"><asp:textbox id="txtShpMaxCvrg" runat="server" CssClass="textFieldRightAlign" Width="74px" ReadOnly="True"></asp:textbox></TD>
										<TD style="WIDTH: 126px; HEIGHT: 15px" width="126"><asp:label id="lblShipEstDlvryDt" tabIndex="108" runat="server" CssClass="tableLabel" Width="107px">Est. Delivery D/T</asp:label></TD>
										<TD style="HEIGHT: 15px" width="20%"><asp:textbox id="txtShipEstDlvryDt" runat="server" CssClass="textField" Width="114px" AutoPostBack="True"></asp:textbox></TD>
										<TD style="WIDTH: 101px; HEIGHT: 15px" width="101"><asp:label id="lblEstHCDt" tabIndex="108" runat="server" CssClass="tableLabel" Width="107px">Est. HCR D/T</asp:label></TD>
										<TD style="HEIGHT: 15px" width="20%"><asp:textbox id="txtEstHCDt" runat="server" CssClass="textField" Width="114px" AutoPostBack="True"
												ReadOnly="True"></asp:textbox></TD>
									</TR>
									<TR height="25">
										<TD style="WIDTH: 16px; HEIGHT: 23px" width="16"></TD>
										<TD style="WIDTH: 156px; HEIGHT: 23px" width="156"><asp:label id="lblShipAdpercDV" runat="server" CssClass="tableLabel" Width="125px">Insurance Sur. %</asp:label></TD>
										<TD style="WIDTH: 46px; HEIGHT: 23px" width="46"><asp:textbox id="txtShpAddDV" runat="server" CssClass="textFieldRightAlign" Width="74px" ReadOnly="True"></asp:textbox></TD>
										<TD style="WIDTH: 3px; HEIGHT: 23px" width="3"></TD>
										<TD style="WIDTH: 77px; HEIGHT: 23px" width="77"><asp:label id="lblShipInsSurchrg" runat="server" CssClass="tableLabel" Width="120px"> Insurance Surcharge</asp:label></TD>
										<TD style="WIDTH: 19px; HEIGHT: 23px" width="19"><asp:textbox id="txtShpInsSurchrg" runat="server" CssClass="textFieldRightAlign" Width="74px"
												ReadOnly="True"></asp:textbox></TD>
										<TD style="WIDTH: 126px; HEIGHT: 23px" width="126"><asp:label id="Label1" tabIndex="108" runat="server" CssClass="tableLabel" Width="112px">Actual Delivery D/T</asp:label></TD>
										<TD style="HEIGHT: 23px" width="20%"><asp:textbox id="txtActDelDt" runat="server" CssClass="textField" Width="114px" AutoPostBack="True"
												ReadOnly="True"></asp:textbox></TD>
										<TD style="WIDTH: 101px; HEIGHT: 23px" width="101"><asp:label id="lblActHCDt" tabIndex="108" runat="server" CssClass="tableLabel" Width="96px">Actual HCR D/T</asp:label></TD>
										<TD style="HEIGHT: 23px" width="20%"><asp:textbox id="txtActHCDt" runat="server" CssClass="textField" Width="114px" AutoPostBack="True"
												ReadOnly="True"></asp:textbox></TD>
									</TR>
									<TR height="25">
										<TD style="WIDTH: 16px; HEIGHT: 12px" width="16"></TD>
										<TD style="WIDTH: 156px; HEIGHT: 12px" width="156"><asp:label id="Label5" runat="server" CssClass="tableLabel" Width="120px"> Other Surcharge</asp:label></TD>
										<TD style="WIDTH: 46px; HEIGHT: 12px" width="46"><asp:textbox id="txtOtherSurcharge" runat="server" CssClass="textFieldRightAlign" Width="74px"></asp:textbox></TD>
										<TD style="WIDTH: 3px; HEIGHT: 12px" width="3"></TD>
										<TD style="WIDTH: 77px; HEIGHT: 12px" width="77"><asp:label id="lblCODAmount" runat="server" CssClass="tableLabel" Width="88px">C.O.D. Amount</asp:label></TD>
										<TD style="WIDTH: 19px; HEIGHT: 12px" width="19"><cc1:mstextbox id="txtCODAmount" tabIndex="52" runat="server" CssClass="textFieldRightAlign" Width="74px"
												NumberPrecision="10" TextMaskType="msNumericCOD" MaxLength="11" AutoPostBack="True" NumberScale="2" NumberMinValue="0" NumberMaxValueCOD="99999999.99"></cc1:mstextbox></TD>
										<TD style="WIDTH: 126px; HEIGHT: 12px" align="right" width="126"></TD>
										<TD style="HEIGHT: 12px" align="right" width="20%"><FONT face="Tahoma"><asp:checkbox id="chkshpRtnHrdCpy" runat="server" CssClass="tableLabel" Width="14px" Text="HCR"
													AutoPostBack="True"></asp:checkbox><asp:checkbox id="chkInvHCReturn" runat="server" CssClass="tableLabel" Width="38px" Text="INVR"
													AutoPostBack="True"></asp:checkbox></FONT></TD>
										<TD style="WIDTH: 101px; HEIGHT: 12px" width="101"><asp:radiobutton id="rbtnShipFrghtPre" runat="server" CssClass="tableLabel" Width="112px" Text="Freight Prepaid"
												Height="16px" GroupName="Freight"></asp:radiobutton></TD>
										<TD style="HEIGHT: 12px" width="20%"><asp:radiobutton id="rbtnShipFrghtColl" tabIndex="55" runat="server" CssClass="tableLabel" Width="108px"
												Text="Freight Collect" Height="16px" GroupName="Freight"></asp:radiobutton></TD>
									</TR>
								</TABLE>
							</FONT>
						</td>
					</tr>
					<%--  HC Return Task --%>
					<%--  HC Return Task --%>
					<%--  HC Return Task --%>
					<TR>
						<TD style="WIDTH: 738px; HEIGHT: 32px" vAlign="baseline" align="left" width="738">
							<TABLE id="Table7" style="WIDTH: 947px; HEIGHT: 4px" cellSpacing="1" cellPadding="1" width="0"
								border="0">
								<TR>
									<TD style="WIDTH: 62px">
										<P><asp:label id="lblSpeHanInstruction" runat="server" CssClass="tableLabel" Width="72px">Special Del. Instructions</asp:label></P>
									</TD>
									<TD><FONT face="Tahoma">
											<P><asp:textbox id="txtSpeHanInstruction" tabIndex="56" runat="server" Width="864px" Height="36px"
													MaxLength="200" AutoPostBack="True" TextMode="MultiLine"></asp:textbox></P>
										</FONT>
									</TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<%--  HC Return Task --%>
					<TR vAlign="top">
						<td style="WIDTH: 738px; HEIGHT: 54px" align="left" width="738"><asp:datagrid id="dgVAS" tabIndex="115" runat="server" Width="722px" HeaderStyle-Height="20px"
								ItemStyle-Height="20px" PageSize="4" OnUpdateCommand="dgVAS_Update" OnDeleteCommand="dgVAS_Delete" OnCancelCommand="dgVAS_Cancel" OnPageIndexChanged="dgVAS_PageChange"
								AllowPaging="True" OnEditCommand="dgVAS_Edit" AutoGenerateColumns="False" OnItemCommand="dgVAS_Button" OnItemDataBound="dgVAS_ItemDataBound">
								<Columns>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton"
										CommandName="Delete">
										<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
									</asp:ButtonColumn>
									<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0  title='Update' &gt;"
										CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel'&gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0  title='Edit' &gt;">
										<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
									</asp:EditCommandColumn>
									<asp:TemplateColumn HeaderText="VAS">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label ID="lblVASCode" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBox" runat="server" ID="txtVASCode" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>'>
											</cc1:mstextbox>
											<asp:RequiredFieldValidator ID="validVASCode" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtVASCode"
												ErrorMessage="The VAS Code is required"></asp:RequiredFieldValidator>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton"
										CommandName="Search">
										<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
									</asp:ButtonColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label runat="server" CssClass="gridLabel" ID="lblVASDesc" Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtVASDesc" Enabled=True ReadOnly=True EnableViewState=True Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Surcharge">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label runat="server" CssClass="gridLabelNumber" ID="lblSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge",(String)ViewState["m_format"])%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox CssClass="gridTextBoxNumber" runat="server" ID="txtSurcharge" TextMaskType="msNumeric" MaxLength="9" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="8" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge",(String)ViewState["m_format"])%>'>
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Remarks">
										<HeaderStyle Width="56%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label runat="server" CssClass="gridLabel" ID="lblRemarks" Text='<%#DataBinder.Eval(Container.DataItem,"remarks")%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtRemarks" Text='<%#DataBinder.Eval(Container.DataItem,"remarks")%>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle NextPageText="Next" Font-Size="Smaller" PrevPageText="Previous" HorizontalAlign="Right"
									Height="20"></PagerStyle>
							</asp:datagrid></td>
					</TR>
					<tr vAlign="top">
						<td style="WIDTH: 722px; HEIGHT: 26px" align="right" width="738" height="26"><asp:button id="btnDGInsert" tabIndex="59" runat="server" CssClass="queryButton" Width="69px"
								CausesValidation="False" Text="Insert"></asp:button></td>
					</tr>
					<TR vAlign="top">
						<TD style="WIDTH: 738px" align="left" width="738">
							<TABLE id="tblCharges" style="Z-INDEX: 120; WIDTH: 737px; HEIGHT: 158px" runat="server">
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label id="lblFreightChrg" tabIndex="117" runat="server" CssClass="tableLabel" Width="141px"
											Height="20px">Freight Charge</asp:label></td>
									<td width="20%"><asp:textbox id="txtFreightChrg" runat="server" CssClass="textFieldRightAlign" Width="131px"
											ReadOnly="True"></asp:textbox></td>
									<td width="10%"></td>
									<td align="right" width="30%"><asp:textbox id="txtOriFreightChrg" runat="server" CssClass="textFieldRightAlign" Width="131px"
											ReadOnly="True"></asp:textbox></td>
									<TD width="50%"></TD>
								</tr>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label id="lblInsChrg" tabIndex="119" runat="server" CssClass="tableLabel" Width="141px"
											Height="20px">Insurance Surcharge</asp:label></td>
									<td width="20%"><asp:textbox id="txtInsChrg" tabIndex="120" runat="server" CssClass="textFieldRightAlign" Width="131px"
											ReadOnly="True"></asp:textbox></td>
									<td width="10%"></td>
									<td align="right" width="30%"><asp:textbox id="txtOriInsChrg" tabIndex="120" runat="server" CssClass="textFieldRightAlign"
											Width="131px" ReadOnly="True"></asp:textbox></td>
									<TD width="50%"></TD>
								</tr>
								<%--By Aoo 22/02/2008--%>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label id="lblOtherSurcharge" tabIndex="119" runat="server" CssClass="tableLabel" Width="141px"
											Height="20px">Other Surcharge</asp:label></td>
									<td width="20%"><asp:textbox id="txtOtherSurcharge2" tabIndex="120" runat="server" CssClass="textFieldRightAlign"
											Width="131px" ReadOnly="True"></asp:textbox></td>
									<td width="10%"></td>
									<td align="right" width="30%"><asp:textbox id="txtOriOthersurcharge2" tabIndex="120" runat="server" CssClass="textFieldRightAlign"
											Width="131px" ReadOnly="True"></asp:textbox></td>
									<TD width="50%"></TD>
								</tr>
								<%--End By Aoo 22/02/2008--%>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label id="lblTotVASSurChrg" tabIndex="121" runat="server" CssClass="tableLabel" Width="141px"
											Height="20px">Total VAS Surcharge</asp:label></td>
									<td width="20%"><asp:textbox id="txtTotVASSurChrg" tabIndex="122" runat="server" CssClass="textFieldRightAlign"
											Width="131px" ReadOnly="True"></asp:textbox></td>
									<td width="10%"></td>
									<td align="right" width="30%"><asp:label id="lblOritotVas" tabIndex="121" runat="server" CssClass="tableLabel" Height="12px"></asp:label></td>
									<TD width="50%"></TD>
								</tr>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label id="lblESASurchrg" tabIndex="123" runat="server" CssClass="tableLabel" Width="141px"
											Height="20px">ESA Surcharge</asp:label></td>
									<td width="20%"><asp:textbox id="txtESASurchrg" tabIndex="124" runat="server" CssClass="textFieldRightAlign"
											Width="131px" ReadOnly="True"></asp:textbox></td>
									<td width="10%"></td>
									<td align="right" width="30%"><asp:textbox id="txtOriESASurchrg" tabIndex="124" runat="server" CssClass="textFieldRightAlign"
											Width="131px" ReadOnly="True"></asp:textbox></td>
									<TD width="50%"></TD>
								</tr>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label id="lblShpTotAmt" tabIndex="125" runat="server" CssClass="tableLabel" Width="141px"
											Height="20px">Total Rated Amount</asp:label></td>
									<td width="20%"><asp:textbox id="txtShpTotAmt" tabIndex="126" runat="server" CssClass="textFieldRightAlign" Width="131px"
											ReadOnly="True"></asp:textbox></td>
									<td width="10%"></td>
									<td align="right" width="30%"><asp:textbox id="txtOriShpTotAmt" tabIndex="126" runat="server" CssClass="textFieldRightAlign"
											Width="131px" ReadOnly="True"></asp:textbox></td>
									<TD width="50%"></TD>
								</tr>
								<%--By Aoo 22/02/2008--%>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label id="lblPODEX" tabIndex="121" runat="server" CssClass="tableLabel" Width="141px"
											Height="20px">PODEX Surcharges</asp:label></td>
									<td width="20%"><asp:textbox id="txtPODEX" tabIndex="122" runat="server" CssClass="textFieldRightAlign" Width="131px"
											ReadOnly="True"></asp:textbox></td>
									<td width="10%"></td>
									<td align="right" width="30%"><asp:label id="lblRateOverrideBy" tabIndex="121" runat="server" CssClass="tableLabel" Height="20px">Rating Override by : </asp:label></td>
									<TD width="50%"><asp:label id="lblRateOverrideByValue" tabIndex="121" runat="server" CssClass="tableLabel"
											Height="20px" Font-Bold="True"></asp:label></TD>
								</tr>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label id="lblMBGAmt" tabIndex="123" runat="server" CssClass="tableLabel" Width="141px"
											Height="20px">MBG Amount</asp:label></td>
									<td width="20%"><asp:textbox id="txtMBGAmt" tabIndex="124" runat="server" CssClass="textFieldRightAlign" Width="131px"
											ReadOnly="True"></asp:textbox></td>
									<td width="10%"></td>
									<td align="right" width="30%"><asp:label id="lblRateOverrideDate" tabIndex="121" runat="server" CssClass="tableLabel" Height="20px">on : </asp:label></td>
									<TD width="50%"><asp:label id="lblRateOverrideDateValue" tabIndex="121" runat="server" CssClass="tableLabel"
											Height="20px" Font-Bold="True"></asp:label></TD>
								</tr>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label id="lblOtherInvC_D" tabIndex="125" runat="server" CssClass="tableLabel" Width="141px"
											Height="20px">Other Invoice Credit/Debit</asp:label></td>
									<td width="20%"><asp:textbox id="txtOtherInvC_D" tabIndex="126" runat="server" CssClass="textFieldRightAlign"
											Width="131px" ReadOnly="True"></asp:textbox></td>
									<td width="10%"></td>
									<td align="right" width="30%"></td>
									<TD width="50%"></TD>
								</tr>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label id="lblTotalInvAmt" tabIndex="121" runat="server" CssClass="tableLabel" Width="141px"
											Height="20px">Total Invoiced Amount</asp:label></td>
									<td width="20%"><asp:textbox id="txtTotalInvAmt" tabIndex="122" runat="server" CssClass="textFieldRightAlign"
											Width="131px" ReadOnly="True"></asp:textbox></td>
									<td width="10%"></td>
									<td align="right" width="30%"></td>
									<TD width="50%"></TD>
								</tr>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label id="lblCreditNotes" tabIndex="123" runat="server" CssClass="tableLabel" Width="141px"
											Height="20px">Credit Notes</asp:label></td>
									<td width="20%"><asp:textbox id="txtCreditNotes" tabIndex="124" runat="server" CssClass="textFieldRightAlign"
											Width="131px" ReadOnly="True"></asp:textbox></td>
									<td width="10%"></td>
									<td align="right" width="30%"></td>
									<TD width="50%"></TD>
								</tr>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label id="lblDebitNotes" tabIndex="125" runat="server" CssClass="tableLabel" Width="141px"
											Height="20px">Debit Notes</asp:label></td>
									<td width="20%"><asp:textbox id="txtDebitNotes" tabIndex="126" runat="server" CssClass="textFieldRightAlign"
											Width="131px" ReadOnly="True"></asp:textbox></td>
									<td width="10%"></td>
									<td align="right" width="30%"></td>
									<TD></TD>
								</tr>
								<tr height="25">
									<td style="WIDTH: 144px" width="144"><asp:label id="lblTotalConRevenue" tabIndex="121" runat="server" CssClass="tableLabel" Width="141px"
											Height="20px">Total Consignment Revenue</asp:label></td>
									<td width="20%"><asp:textbox id="txtTotalConRevenue" tabIndex="122" runat="server" CssClass="textFieldRightAlign"
											Width="131px" ReadOnly="True"></asp:textbox></td>
									<td width="10%"></td>
									<td align="right" width="30%"></td>
									<TD width="50%"></TD>
								</tr>
								<TR>
									<TD style="WIDTH: 144px" width="144"><FONT face="Tahoma"><asp:label id="lblPaidRevenue" tabIndex="121" runat="server" CssClass="tableLabel" Width="141px"
												Height="20px">Paid Revenue</asp:label></FONT></TD>
									<TD width="20%"><FONT face="Tahoma"><asp:textbox id="txtPaidRevenue" tabIndex="122" runat="server" CssClass="textFieldRightAlign"
												Width="131px" ReadOnly="True"></asp:textbox></FONT></TD>
									<TD width="10%"></TD>
									<TD align="right" width="30%"></TD>
									<TD width="50%"></TD>
								</TR>
								<%--End By Aoo 22/02/2008--%>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
			</DIV>
			&nbsp;
			<DIV id="DomstcShipPanel" style="Z-INDEX: 101; POSITION: relative; WIDTH: 444px; HEIGHT: 224px; TOP: 46px; LEFT: 70px; relative: absolute"
				MS_POSITIONING="GridLayout" runat="server">
				<FIELDSET style="Z-INDEX: 80; POSITION: relative; WIDTH: 374px; HEIGHT: 178px; TOP: 46px; LEFT: 70px; relative: absolute"
					MS_POSITIONING="GridLayout"><LEGEND><asp:label id="lblConf" CssClass="tableHeadingFieldset" Runat="server">Confirmation</asp:label></LEGEND>
					<TABLE id="Table1" style="Z-INDEX: 75; POSITION: absolute; WIDTH: 346px; HEIGHT: 153px; TOP: 23px; LEFT: 7px"
						runat="server">
						<TR>
							<TD>
								<P align="center"><asp:label id="lblConfirmMsg" runat="server" Width="321px"></asp:label></P>
								<P>
								<P align="center"><asp:button id="btnToSaveChanges" runat="server" CssClass="queryButton" Text="Yes"></asp:button><asp:button id="btnNotToSave" runat="server" CssClass="queryButton" CausesValidation="False"
										Text="No"></asp:button><asp:button id="btnToCancel" runat="server" CssClass="queryButton" CausesValidation="False"
										Text="Cancel"></asp:button></P>
								<P></P>
							</TD>
						</TR>
					</TABLE>
				</FIELDSET>
			</DIV>
			<DIV id="divCustIdCng" style="Z-INDEX: 103; POSITION: relative; WIDTH: 637px; HEIGHT: 176px; TOP: 46px; LEFT: 7px"
				MS_POSITIONING="GridLayout" runat="server">
				<TABLE id="Table2" style="Z-INDEX: 105; POSITION: absolute; WIDTH: 530px; HEIGHT: 129px; TOP: 16px; LEFT: 43px"
					runat="server">
					<TR>
						<TD>
							<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:label id="lblCustIdCngMsg" runat="server" Width="337px" Height="46px">All changes cannot be reverted back <br>Do you want to change to another customer ID ?</asp:label></P>
							<P>
							<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:button id="btnCustIdChgYes" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Yes"></asp:button>&nbsp;
								<asp:button id="btnCustIdChgNo" runat="server" CssClass="queryButton" CausesValidation="False"
									Text=" No "></asp:button></P>
						</TD>
					</TR>
				</TABLE>
			</DIV>
			<DIV id="divDelOperation" style="Z-INDEX: 105; POSITION: relative; WIDTH: 625px; HEIGHT: 217px; TOP: 46px; LEFT: 22px"
				MS_POSITIONING="GridLayout" runat="server">
				<TABLE id="Table3" style="Z-INDEX: 109; POSITION: absolute; WIDTH: 545px; HEIGHT: 107px; TOP: 20px; LEFT: 21px"
					runat="server">
					<TR>
						<TD>
							<P align="center"><asp:label id="lblDelOperation" runat="server" Width="400px" Height="36px">Record Deleted cannot be reverted back. Confirm Deletion ?</asp:label></P>
							<P align="center"><asp:button id="btnDelOperationYes" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Yes"></asp:button>&nbsp;
								<asp:button id="btnDelOperationNo" runat="server" CssClass="queryButton" CausesValidation="False"
									Text=" No "></asp:button></P>
						</TD>
					</TR>
				</TABLE>
			</DIV>
			<DIV id="divUpdateAutoManifest" style="Z-INDEX: 104; POSITION: relative; WIDTH: 625px; HEIGHT: 217px; TOP: 46px; LEFT: 22px"
				MS_POSITIONING="GridLayout" runat="server">
				<TABLE id="Table4" style="Z-INDEX: 109; POSITION: absolute; WIDTH: 545px; HEIGHT: 107px; TOP: 20px; LEFT: 21px"
					runat="server">
					<TR>
						<TD>
							<P align="center"><asp:label id="Label2" runat="server" Width="400px" Height="36px">Shipment was originally manifested on a different route /
day. This shipment will be reassigned to a new route.
You must manually remove the shipment from prior
linehaul and / or delivery manifests.</asp:label></P>
							<P align="center"><asp:button id="btnUpdateAutoManifest" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="OK"></asp:button></P>
						</TD>
					</TR>
				</TABLE>
			</DIV>
			<DIV id="ExceedTimeState" style="Z-INDEX: 103; POSITION: relative; WIDTH: 439px; HEIGHT: 218px; TOP: 40px; LEFT: 46px"
				MS_POSITIONING="GridLayout" runat="server"><BR>
				<P align="center"><asp:label id="lbl_ExceedTimeState" style="Z-INDEX: 110; POSITION: absolute; TOP: 39px; LEFT: 41px"
						runat="server" Width="359px">The Estimated Pickup Date/Time exceeds the Cut-off time for Sender Postal code. To override the standard cut-off time and still schecule the pick up at the time you have entered press Cancel. To reschedule the pickup before the Cut-off time press OK.</asp:label></P>
				<P align="center"><asp:button id="btnExceedCancel" style="Z-INDEX: 101; POSITION: absolute; TOP: 153px; LEFT: 220px"
						runat="server" CssClass="queryButton" Width="65px" CausesValidation="False" Text="Cancel"></asp:button><asp:button id="btnExceedOK" style="Z-INDEX: 103; POSITION: absolute; TOP: 153px; LEFT: 169px"
						runat="server" CssClass="queryButton" Width="44px" CausesValidation="False" Text="OK"></asp:button></P>
			</DIV>
			<INPUT 
type=hidden value="<%=strScrollPosition%>" name=ScrollPosition> <INPUT type="hidden" name="hdnRefresh">
		</form>
	</body>
</HTML>
