using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TIESDAL;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.common.applicationpages;
using com.ties;
using System.Text;
using System.Globalization;
using System.IO;



namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for ConfigureCustomerSelfService1.
	/// </summary>
	public class ConfigureCustomerSelfService1 : BasePage
	{
		protected System.Web.UI.WebControls.ValidationSummary Validationsummary1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.WebControls.DropDownList DropDownList1;
		protected System.Web.UI.WebControls.Button btnHidReport;
		protected System.Web.UI.WebControls.DropDownList ddlPayerID;
		protected System.Web.UI.WebControls.DataGrid dgConfigSSPNG;
		protected System.Web.UI.WebControls.DataGrid dgConfigSSCustomer;
		protected System.Web.UI.WebControls.Label lbl;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Label lblNote;

		String m_strEnterpriseID=null;
		String m_strAppID=null;
		String m_strUserLoggin=null;
		String m_strPayerID=null;
		bool m_isGridEmpty = false ;
		bool m_IsMaster = false;
		private DataSet	m_sdsConfigSSPNG = null ;
		private DataTable m_sdtSenderName = null ;
		protected System.Web.UI.WebControls.Label lblAccountEdit;
		protected System.Web.UI.WebControls.Label lblCustomerAccountLogin;
		protected System.Web.UI.WebControls.TextBox txtCustomerAccountLogin;
		protected com.common.util.msTextBox txtCustomerCopiesEdit;
		protected com.common.util.msTextBox txtEnterpriseCopiesEdit;
		
		com.common.classes.User user = new com.common.classes.User();

		private void Page_Load(object sender, System.EventArgs e)
		{
		
			lblNote.Text = "* Enterprise/Customer Copies is number of con note copies plus one per pkg";
			try
			{
				// Put user code to initialize the page here
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
				m_strAppID = utility.GetAppID();
				m_strEnterpriseID = utility.GetEnterpriseID();
				m_strUserLoggin = utility.GetUserID();
				user = RBACManager.GetUser(m_strAppID, m_strEnterpriseID, m_strUserLoggin);	
				if(user != null)
				{
					m_strPayerID = user.PayerID;
					if(user.PayerID == "")
					{
						//show
						lblCustomerAccountLogin.Visible = true;
						txtCustomerAccountLogin.Visible = true;

						btnQry.Visible = true;
						btnExecQry.Visible = true;
						ddlPayerID.Enabled = true;
					}
					else
					{   //hide
						lblCustomerAccountLogin.Visible = false;
						txtCustomerAccountLogin.Visible = false;

						btnQry.Visible = false;
						btnExecQry.Visible = false;

						ddlPayerID.Enabled = false;
					}
						
					//
					m_IsMaster = ConfigureCustomerSelfServiceMgrDAL.IsMaster(m_strAppID,m_strEnterpriseID,user.PayerID,user.UserID);
//					if(!m_IsMaster)
//					{
//						ddlPayerID.Enabled = false;
//					}
//					else
//					{
//						ddlPayerID.Enabled = true;
//					}
					
				}

				if(!Page.IsPostBack)
				{
						BindPayID();
						RetreiveSelectedPage(true);
					if(user.PayerID != "" && !m_IsMaster)
					{
						dgConfigSSPNG.Visible = false ;
					}
					else
					{
						dgConfigSSPNG.Visible = true ;
					}

					lblNote.Visible = dgConfigSSPNG.Visible;
					
				}
				else
				{
					m_sdsConfigSSPNG = (DataSet)Session["SESSION_DS_ConfigSS"];
				}
			}
			catch (Exception ex)
			{
				lblErrorMsg.Text = ex.Message.ToString();
			}
		}

		
		#region "Payer ID: DropDownList"

		private void BindPayID()
		{
			//
			//blank selection
			DataTable dtBlank = new DataTable(); 
			dtBlank.Columns.Add(new DataColumn("custid", typeof(string)));
			DataRow drEach = dtBlank.NewRow();
			drEach[0] = "";
			dtBlank.Rows.Add(drEach);

			bool chkExitPayID =false;
			DataTable dtPayerID = ConfigureCustomerSelfServiceMgrDAL.GetPayerID( m_strAppID,m_strEnterpriseID);	
			foreach(DataRow row in dtPayerID.Rows)
			{
				if(m_strPayerID.Equals(row[0]))
				{
					chkExitPayID=true;
				}
				drEach = dtBlank.NewRow();
				drEach[0] = row[0];
				dtBlank.Rows.Add(drEach);
			}
			this.ddlPayerID.DataSource= dtBlank;
			this.ddlPayerID.DataTextField= "custid";
			this.ddlPayerID.DataValueField= "custid";
			this.ddlPayerID.DataBind();

			//ddlPayerID
			if(chkExitPayID)
			{
				ddlPayerID.SelectedIndex = ddlPayerID.Items.IndexOf(ddlPayerID.Items.FindByValue(m_strPayerID));
			}
			else
			{
				ddlPayerID.SelectedIndex = 0;
			}
		}


		#endregion

		#region "Set : Control"

		private void SetControl()
		{
			
		}


		#endregion

		private bool ValidateField()
		{
			bool isValidate = false;
			if(ddlPayerID.Visible ==true && txtCustomerAccountLogin.Visible==true)
			{
				if(ddlPayerID.SelectedValue.Equals(String.Empty)&& txtCustomerAccountLogin.Text=="")
				{

					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"REQ_CSS_CRITERIA",utility.GetUserCulture());//"Please specify either Payer ID or Customer Login Account."; 
				}
				else
				{
					isValidate = true;
				}
			}
			else if (ddlPayerID.SelectedValue.Equals(String.Empty) && txtCustomerAccountLogin.Visible==false)
			{
				
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"REQ_CSS_CRITERIA_SELECTION",utility.GetUserCulture());//"Please select selection criteria."; 
			}
			else
			{
				isValidate = true;
			}

			return isValidate;
			
		}
		private void BindDataGrid()
		{	
			m_sdsConfigSSPNG  = (DataSet)Session["SESSION_DS_ConfigSS"] ;
			if(m_sdsConfigSSPNG !=null && m_sdsConfigSSPNG.Tables[0].Rows.Count > 0)
			{
				if(Convert.ToInt32( m_sdsConfigSSPNG.Tables[0].Rows[0]["ErrorCode"]) > 0)
				{
					lblErrorMsg.Text = m_sdsConfigSSPNG.Tables[0].Rows[0]["ErrorMessage"].ToString();
					BindEmptyData();
				}
				else
				{
					dgConfigSSPNG.DataSource = m_sdsConfigSSPNG.Tables[1];
					if(m_sdsConfigSSPNG.Tables[1] !=null && m_sdsConfigSSPNG.Tables[1].Rows.Count <=0)
					{
						BindEmptyData();
					}
					else
					{
						Session["SESSION_DS_ConfigSS"] = m_sdsConfigSSPNG;
						dgConfigSSPNG.DataBind(); 
					}
				}
			}
			else
			{
				BindEmptyData();
			}
		}

		private void RetreiveSelectedPage(bool isPageLoad )
		{
			String strPayerID = ddlPayerID.SelectedValue;
			String strCustomerAccount = txtCustomerAccountLogin.Text;
			if( strPayerID !=""  || !isPageLoad )
			{
				m_sdsConfigSSPNG = ConfigureCustomerSelfServiceMgrDAL.SearchConfigCusSelfService(m_strAppID, m_strEnterpriseID, m_strUserLoggin, strPayerID,strCustomerAccount);
			}
		
			if(strPayerID == "" && m_sdsConfigSSPNG != null &&  m_sdsConfigSSPNG.Tables[1] != null && m_sdsConfigSSPNG.Tables[1].Rows.Count>0)
			{
				try
				{
					m_strPayerID=m_sdsConfigSSPNG.Tables[1].Rows[0]["payerid"].ToString();
					ddlPayerID.SelectedIndex = ddlPayerID.Items.IndexOf(ddlPayerID.Items.FindByValue(m_strPayerID));
				}
				catch
				{

				}
			}

			dgConfigSSPNG.SelectedIndex = -1;
			dgConfigSSPNG.EditItemIndex = -1;

			Session["SESSION_DS_ConfigSS"] = m_sdsConfigSSPNG;
			BindDataGrid();

		}
		private DataTable LoadSenderNameList(String strValue)
		{
			DataTable dtBlank = new DataTable(); 
			try
			{
				//blank selection
				String strPayerID = ddlPayerID.SelectedValue;
				dtBlank.Columns.Add(new DataColumn("snd_rec_name", typeof(string)));
				DataRow drEach = dtBlank.NewRow();
				drEach[0] = "";
				dtBlank.Rows.Add(drEach);

				String strPayID = ddlPayerID.SelectedValue;
				DataTable dtSender = ConfigureCustomerSelfServiceMgrDAL.GetSenderName(m_strAppID,m_strEnterpriseID,m_strUserLoggin,strPayerID);	

				if(dtSender !=null && dtSender.Rows.Count<=0)
				{	//default blank and retrive data
					if(strValue != String.Empty)
					{
						drEach = dtBlank.NewRow();
						drEach[0] = strValue;
						dtBlank.Rows.Add(drEach);
					}
				}
				else
				{
					foreach(DataRow row in dtSender.Rows)
					{
						drEach = dtBlank.NewRow();
						drEach[0] = row[0];
						dtBlank.Rows.Add(drEach);
					}
				}
				m_sdtSenderName = dtBlank;
				Session["SESSION_DT_CSendernameList"] = dtBlank;
				
			}
			catch(Exception err)
			{
				lblErrorMsg.Text = err.Message;
			}
			return dtBlank;
			
		}
		
		private void BindEmptyData()
		{
			DataSet dtEmpty = ConfigureCustomerSelfServiceMgrDAL.GetEmptyConfigureCustomerSelfService();
			dgConfigSSPNG.DataSource = dtEmpty.Tables[0];
			m_isGridEmpty = true ;
			Session["SESSION_DS_ConfigEmpty"] = dtEmpty;
			dgConfigSSPNG.DataBind();
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			BindPayID();
			lblErrorMsg.Text = "";
			btnExecQry.Enabled = true;
			txtCustomerAccountLogin.Text="";
			Session["SESSION_DS_ConfigSS"] = null;
			m_sdsConfigSSPNG = null;
			RetreiveSelectedPage(true);

		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			//dgConfigSSCustomer.Visible = true;
			lblErrorMsg.Text = "";
			try
			{
				if(ValidateField())
				{
					RetreiveSelectedPage(false);
					if(lblErrorMsg.Text =="")
					{
						btnExecQry.Enabled = false;
					}
				}
			}
			catch(Exception err)
			{
				String msg = err.ToString();
				lblErrorMsg.Text = err.Message;//Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
			}
		}

		protected void OnEdit_dgConfigSSPNG(object sender, DataGridCommandEventArgs e)
		{		
			lblErrorMsg.Text="";
			int rowIndex = e.Item.ItemIndex;
			ViewState["index"] = rowIndex;
//			if( dgConfigSSPNG.EditItemIndex >0)
//			{
//				m_sdsConfigSSPNG.Tables[1].Rows.RemoveAt(dgConfigSSCustomer.EditItemIndex);
//			}
			dgConfigSSPNG.EditItemIndex = rowIndex;
			BindDataGrid();
		}
		

		protected void OnCancel_dgConfigSSPNG(object sender, DataGridCommandEventArgs e)
		{		
			lblErrorMsg.Text="";
			dgConfigSSPNG.EditItemIndex = -1;
			m_sdsConfigSSPNG = (DataSet)Session["SESSION_DS_ConfigSS"];
			int rowIndex = e.Item.ItemIndex;

			BindDataGrid();
	
		}
		protected void OnUpdate_dgConfigSSPNG(object sender, DataGridCommandEventArgs e)
		{		
			bool isChange=false;
			lblErrorMsg.Text="";
			int rowIndex = e.Item.ItemIndex;
			//update Source

			DataRow drSelected = m_sdsConfigSSPNG.Tables[1].Rows[e.Item.ItemIndex];
			DropDownList ddSenderNameEdit = (DropDownList)e.Item.FindControl("ddlSenderNameEdit");
			CheckBox chkMasterEdit = (CheckBox)e.Item.FindControl("chkMasterEdit");
			msTextBox txtEnterPrise = (msTextBox)e.Item.FindControl("txtEnterpriseCopiesEdit");
			msTextBox txtCustomeCopies = (msTextBox)e.Item.FindControl("txtCustomerCopiesEdit");

			
			String senderName ="";
			String userID = (String)drSelected["userid"];
			int intEnterPrise = 0 ;
			int intCustomerCopies  = 0 ;
			bool isMaster = false ;

			if(ddSenderNameEdit != null)
			{
			    senderName = ddSenderNameEdit.SelectedValue ;
			}
			if(chkMasterEdit != null)
			{
				if(chkMasterEdit.Checked)
				{
					isMaster = true ;
				}
				else
				{
					isMaster = false ;
				}
			}
			if(txtEnterPrise !=null)
			{
				intEnterPrise = Convert.ToInt32( txtEnterPrise.Text == ""?	"0":txtEnterPrise.Text);
			}
			if(txtCustomeCopies !=null)
			{
				intCustomerCopies = Convert.ToInt32( txtCustomeCopies.Text==""? "0":txtCustomeCopies.Text);
			}

			if( dgConfigSSPNG.EditItemIndex > -1)
			{
				DataRow row  =	m_sdsConfigSSPNG.Tables[1].Rows[dgConfigSSPNG.EditItemIndex];
				try
				{
					if(Convert.ToInt32(row["IsMaster"]) != (isMaster? 1:0 ))
					{
						isChange=true;
					}
					if(isChange ==false && Convert.ToInt32(row["EnterpriseCopies"])  != intEnterPrise)
					{
						isChange=true;
					}
					if(isChange ==false && Convert.ToInt32(row["CustomerCopies"])  != intCustomerCopies)
					{
						isChange=true;
					}
					if(isChange ==false && Convert.ToString(row["snd_rec_name"])  != senderName)
					{
						isChange=true;
					}
				}
				catch
				{

				}

				int isMasterOld =	Convert.ToInt32(row["IsMaster"]);
				int enterPriseOld =		Convert.ToInt32(row["EnterpriseCopies"]);
				int customerCopyOld =	Convert.ToInt32(row["CustomerCopies"]);
				//Check sender name cannot empty 
				if(senderName =="")
				{
					if(isMasterOld != (isMaster? 1:0 )
						|| enterPriseOld != intEnterPrise 
						|| customerCopyOld != intCustomerCopies)
					{
						//edit
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"REQ_CSS_SENDER_SELECTION",utility.GetUserCulture());//"Assign Sender Name to modify record.";
						return;
					}
				}
				
			}
			
			//
			m_sdsConfigSSPNG = (DataSet)Session["SESSION_DS_ConfigSS"];
			
			try
			{

				String strPayerID = ddlPayerID.SelectedValue;
				bool isCustomer = false ;
				if(m_strPayerID =="")
				{
					isCustomer = false ;
				}
				else
				{
					isCustomer = true ;
				}

				if(isChange)
				{
					DataSet dsCCSUpdate = ConfigureCustomerSelfServiceMgrDAL.UpdateConfigCusSelfService(m_strAppID,m_strEnterpriseID,strPayerID,userID,m_strUserLoggin,isMaster,senderName,intEnterPrise,intCustomerCopies,isCustomer);
					if(dsCCSUpdate !=null && dsCCSUpdate.Tables[0].Rows.Count > 0)
					{
						if(Convert.ToInt32( dsCCSUpdate.Tables[0].Rows[0]["ErrorCode"]) > 0)
						{
							lblErrorMsg.Text = dsCCSUpdate.Tables[0].Rows[0]["ErrorMessage"].ToString();
						}
						else
						{
							String strCustomerAccount = txtCustomerAccountLogin.Text;
							if(strPayerID !="" && strCustomerAccount != "")
							{
								strCustomerAccount="";
							}
							m_sdsConfigSSPNG = ConfigureCustomerSelfServiceMgrDAL.SearchConfigCusSelfService(m_strAppID, m_strEnterpriseID, m_strUserLoggin, strPayerID,strCustomerAccount);

							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());	
						}
					}

				
					Session["SESSION_DS_ConfigSS"] = m_sdsConfigSSPNG;
				}
				dgConfigSSPNG.EditItemIndex = -1;
				BindDataGrid();
			}
			catch(ApplicationException appErr)
			{
				lblErrorMsg.Text = appErr.Message;
			}
			catch(Exception ex)
			{
				lblErrorMsg.Text = ex.Message;
			}			
		}

		protected void OnItemBound_dgConfigSSPNG(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			
			LinkButton editButton = (LinkButton)e.Item.FindControl("GridEditCommandColumn");
			if(editButton !=null)
			{
				if (m_isGridEmpty)
				{
					editButton.Visible = false;
				}
				else
				{
					editButton.Visible = true;
				}
			}
			
			if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
			{
				if (m_isGridEmpty)
				{
					((LinkButton)e.Item.Cells[0].Controls[0]).Visible = false;
				}
			}
		
			DataRow drSelected;
			if(m_isGridEmpty)
			{
				DataSet dtEmpty = (DataSet)Session["SESSION_DS_ConfigEmpty"] ;
				drSelected = dtEmpty.Tables[0].Rows[e.Item.ItemIndex];
			}
			else
			{
				drSelected = m_sdsConfigSSPNG.Tables[1].Rows[e.Item.ItemIndex];
			}

			

			//Sender Name
			DropDownList ddSenderName = (DropDownList)e.Item.FindControl("ddlSenderName");
			DropDownList ddSenderNameEdit = (DropDownList)e.Item.FindControl("ddlSenderNameEdit");

			if(ddSenderName != null)
			{
				if(m_isGridEmpty)
				{
					ddSenderName.Visible = false ;
				}
				else
				{
					ddSenderName.Visible = true ;
					String strSenderName = "";
					if(!drSelected["snd_rec_name"] .GetType().Equals(System.Type.GetType("System.DBNull")))
					{
					    strSenderName = (String)drSelected["snd_rec_name"];
					}

					ddSenderName.DataSource = LoadSenderNameList(strSenderName);
					ddSenderName.DataTextField = "snd_rec_name";
					ddSenderName.DataValueField = "snd_rec_name";
					ddSenderName.DataBind();

					ddSenderName.SelectedIndex = ddSenderName.Items.IndexOf(ddSenderName.Items.FindByValue(strSenderName));
					if(ddSenderName.SelectedValue.Equals(String.Empty))
					{
						ddSenderName.SelectedIndex =0;
					}
				
				}
				
			}
			if(ddSenderNameEdit != null)
			{
				String strSenderName = "";
				if(!drSelected["snd_rec_name"] .GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strSenderName = (String)drSelected["snd_rec_name"];
				}
				ddSenderNameEdit.DataSource = LoadSenderNameList(strSenderName);
				ddSenderNameEdit.DataTextField = "snd_rec_name";
				ddSenderNameEdit.DataValueField = "snd_rec_name";
				ddSenderNameEdit.DataBind();

				ddSenderNameEdit.SelectedIndex = ddSenderNameEdit.Items.IndexOf(ddSenderNameEdit.Items.FindByValue(strSenderName));
				if(ddSenderNameEdit.SelectedValue.Equals(String.Empty))
				{
					ddSenderNameEdit.SelectedIndex =0;
				}
			}


			//chkMaster/chkMasterEdit
			CheckBox chkMaster = (CheckBox)e.Item.FindControl("chkMaster");
			CheckBox chkMasterEdit = (CheckBox)e.Item.FindControl("chkMasterEdit");
			bool check = false ;
			if(!drSelected["IsMaster"] .GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				String strMaster = drSelected["IsMaster"].ToString();
				if(strMaster.Equals("1")){check = true ;}
				else{	check = false ;}
			}
			if(chkMasterEdit !=null){ chkMasterEdit.Checked = check;}

			if(chkMaster !=null)
			{ 	
				chkMaster.Checked = check;
				if(m_isGridEmpty)
				{
					chkMaster.Visible = false ;
				}
				else
				{
					chkMaster.Visible = true ;
				}
			}

			if(m_strPayerID != String.Empty)
			{
				//if(chkMaster !=null){ chkMaster.Enabled = false; }
				if(chkMasterEdit !=null){ chkMasterEdit.Enabled =false; }
			}
			else
			{
				//if(chkMaster !=null){ chkMaster.Enabled = true; }
				if(chkMasterEdit !=null){ chkMasterEdit.Enabled =true; }
			}

			msTextBox txtEnterPrise = (msTextBox)e.Item.FindControl("txtEnterpriseCopiesEdit");
			if(txtEnterPrise !=null)
			{
				//if(!m_IsMaster)
				if(m_strPayerID != String.Empty)
				{
					txtEnterPrise.Enabled = false ;
				}
			}
			


																						  
		}

		
		protected void CheckMaster(object sender, System.EventArgs e)
		{
			CheckBox chkCon = (CheckBox)sender;
			DataGridItem dgItem = (DataGridItem)chkCon.NamingContainer;
			//DataRow drSelected = m_sdsConfigSSPNG.Tables[1].Rows[dgItem.ItemIndex];

			if(chkCon.Checked)
			{
				string defaultSenderName = ConfigureCustomerSelfServiceMgrDAL.GetDefaultSenderName(m_strAppID,m_strEnterpriseID);
				DropDownList ddSenderNameEdit = (DropDownList)dgConfigSSPNG.Items[dgItem.ItemIndex].FindControl("ddlSenderNameEdit");
				if(ddSenderNameEdit != null)
				{//Automatic Selected BILLING
					ddSenderNameEdit.DataSource = LoadSenderNameList(defaultSenderName);
					ddSenderNameEdit.DataTextField = "snd_rec_name";
					ddSenderNameEdit.DataValueField = "snd_rec_name";
					ddSenderNameEdit.DataBind();

					ddSenderNameEdit.SelectedIndex = ddSenderNameEdit.Items.IndexOf(ddSenderNameEdit.Items.FindByValue(defaultSenderName));
					if(ddSenderNameEdit.SelectedValue.Equals(String.Empty))
					{
						ddSenderNameEdit.SelectedIndex =0;
					}
				}
			}

		}
	}
}
