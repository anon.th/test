<%@ Page language="c#" Codebehind="BandIdPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.BandIdPopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>BandIdPopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="BandIdPopup" method="post" runat="server">
			<asp:label id="lblBanId" style="Z-INDEX: 106; LEFT: 85px; POSITION: absolute; TOP: 63px" runat="server" Width="100px" CssClass="tableLabel" Height="16px">Band ID</asp:label><asp:textbox id="txtBandId" style="Z-INDEX: 104; LEFT: 215px; POSITION: absolute; TOP: 60px" runat="server" Width="116px" CssClass="textField" Height="19px"></asp:textbox>
			<asp:button id="btnSearch" style="Z-INDEX: 105; LEFT: 416px; POSITION: absolute; TOP: 60px" runat="server" Width="84px" CssClass="buttonProp" Text="Search" Height="21px" CausesValidation="False"></asp:button>
			<asp:button id="btnOk" style="Z-INDEX: 102; LEFT: 511px; POSITION: absolute; TOP: 61px" runat="server" Width="84px" CssClass="buttonProp" Text="Close" Height="21px" CausesValidation="False"></asp:button>
			<asp:datagrid id="dgBandId" style="Z-INDEX: 103; LEFT: 78px; POSITION: absolute; TOP: 94px" runat="server" OnPageIndexChanged="Paging" PageSize="10" AllowPaging="True" Width="522px" AutoGenerateColumns="False" BorderColor="#CCCCCC" BackColor="White" CellPadding="3" BorderWidth="1px" BorderStyle="None" CssClass="gridHeading" AllowCustomPaging="True">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
				<ItemStyle CssClass="drilldownField"></ItemStyle>
				<HeaderStyle Width="10%" CssClass="drilldownHeading"></HeaderStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="band_code" HeaderText="Band ID"></asp:BoundColumn>
					<asp:BoundColumn DataField="percent_discount" HeaderText="Percentage Discount"></asp:BoundColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select"></asp:ButtonColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" ForeColor="#000066" BackColor="White"></PagerStyle>
			</asp:datagrid></form>
	</body>
</HTML>
