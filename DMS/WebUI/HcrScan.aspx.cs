using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using com.ties.DAL;

using com.common.util;
using com.common.classes;
using com.common.DAL;
using com.common.applicationpages;


namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for HcrScan.
	/// </summary>
	public class HcrScan : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Label lblUserID;
		protected System.Web.UI.WebControls.Label lblStatusAvail;
		protected System.Web.UI.WebControls.ListBox lbFrom;
		protected System.Web.UI.WebControls.Label lblToUpdateStatus;
		protected System.Web.UI.WebControls.ListBox lbTo;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox val;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label lblScanDate;
		protected com.common.util.msTextBox txtScanDate;
		protected System.Web.UI.WebControls.Label lblTo;
		protected com.common.util.msTextBox txtScanTo;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentTracking;
		protected System.Web.UI.HtmlControls.HtmlTable tblDataMissing;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.Button btnExportData;
		protected System.Web.UI.HtmlControls.HtmlTable tblDataMissCriteria;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnExportData.Click += new System.EventHandler(this.btnExportData_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private String strAppID = null;
		private String strEnterpriseID = null;
		private Utility utility = null;
		protected System.Web.UI.WebControls.TextBox txtCount;
		protected System.Web.UI.WebControls.TextBox txtKey;
		protected System.Web.UI.WebControls.TextBox txtKeyCons;
		DataSet ds = null;
		private string FileName = "";


		private void Page_Load(object sender, System.EventArgs e)
		{
			if(!Page.IsPostBack)
			{
				lblUserID.Text = Session["userID"].ToString().ToUpper();

				RefreshData();
				BindStatusCode();
			}
		}

				
		private void RefreshData()
		{
			DbConnection dbConApp = null;
			
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();

			//lblUserID = utility.GetUserID();

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("HcrScan.aspx.cs","RefreshData","HcrScan","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append("SELECT distinct status_code FROM Status_Code ");
			strQry.Append(" WHERE applicationid = '" + strAppID + "' ");
			strQry.Append(" AND enterpriseid = '" + strEnterpriseID + "'");
			strQry.Append(" AND is_hcr_scan = 'Y'");

			try
			{
				ds = (DataSet)dbConApp.ExecuteQuery(strQry.ToString(), ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("HcrScan.aspx.cs","RefreshData","HcrScan","Error in the query String");
				throw appExpection;
			}
			finally
			{
				ds.Dispose();
			}
		}

		
		private void BindStatusCode()
		{
			lbFrom.DataSource = ds;
			lbFrom.DataTextField = ds.Tables[0].Columns["status_code"].ColumnName.ToString();
			lbFrom.DataValueField = ds.Tables[0].Columns["status_code"].ColumnName.ToString();
			lbFrom.DataBind();
		}


#region ExportDataMissing

		private void btnExportData_Click(object sender, System.EventArgs e)
		{			
			ExportData(txtScanDate.Text, txtScanTo.Text);
		}

		private DataSet GetDataMissing(string paramFrom, string paramTo)
		{
			DataSet dsReturn = null;
			DbConnection dbConApp = null;
			
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("HcrScan.aspx.cs","GetDataMissing","HcrScan","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();			 

			strQry.Append("SELECT CONSIGNMENT, SCANDATETIME, STATUS_CODE, REMARK ");
			strQry.Append(" FROM HCR_Scan_Tracking ");
			strQry.Append(" WHERE IsError = 'Y' ");
			strQry.Append(" AND CONVERT(datetime, CONVERT(varchar(10),SCANDATETIME,103), 103) ");
			strQry.Append(" Between CONVERT(datetime, '" + paramFrom + "', 103) ");
			strQry.Append(" And CONVERT(datetime, '" + paramTo + "', 103) ");
			strQry.Append(" ORDER BY SCANDATETIME DESC ");


			try
			{
				dsReturn = (DataSet)dbConApp.ExecuteQuery(strQry.ToString(), ReturnType.DataSetType);

				return dsReturn;
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("HcrScan.aspx.cs","GetDataMissing","HcrScan","Error in the query String");
				throw appExpection;
			}
			finally
			{
				dsReturn.Dispose();
			}
		}

		private void ExportData(string strFrom, string strTo) 
		{   
			DataSet dsReport = new DataSet();
			DataTable dtReport = new DataTable();
			DataTable dtExport = new DataTable();

			dtExport.Columns.Add("Consignment");
			dtExport.Columns.Add("ScanDateTime");
			dtExport.Columns.Add("StatusCode");
			dtExport.Columns.Add("Remark");

			try
			{
				//Get DataTable Report to send into Excel
				dsReport = GetDataMissing(strFrom, strTo);

				if (dsReport != null && dsReport.Tables.Count > 0 && dsReport.Tables[0] != null && dsReport.Tables[0].Rows.Count > 0)
				{
					dtReport = dsReport.Tables[0];

					for (int i = 0; i < dtReport.Rows.Count; i++) 
					{
						DataRow drReport = null;
						drReport = dtReport.Rows[i];

						DataRow dr;
						dr = dtExport.NewRow();

						dr["Consignment"] = drReport["CONSIGNMENT"];
						dr["ScanDateTime"] = drReport["ScanDateTime"].ToString();
						dr["StatusCode"] = drReport["STATUS_CODE"];
						dr["Remark"] = drReport["REMARK"];
					
						dtExport.Rows.Add(dr);
					}
				}
				else
				{
					DataRow dr;
					dr = dtExport.NewRow();

					dr["Consignment"] = " ";
					dr["ScanDateTime"] = " ";
					dr["StatusCode"] = " ";
					dr["Remark"] = " ";
					
					dtExport.Rows.Add(dr);
				}

				//Create Excel 
				ExportData(dtExport);	
			}
			catch (Exception err)
			{
				lblErrorMessage.Text = err.Message.ToString();
			}
			finally
			{
				dsReport.Dispose();
				dtReport.Dispose();
				dtExport.Dispose();
			}
		}

		private void ExportData(DataTable dtSource)
		{
			DataTable dtCons = new DataTable();
			string Res = "";			
 
			try
			{
				dtCons = dtSource;

				if ((dtCons != null) && (dtCons.Columns.Count > 0) && (dtCons.Rows.Count > 0))
				{
					TIESClasses.CreateXls Xls = new TIESClasses.CreateXls();
					// Set type column
					string[] ColumnType = new string[dtCons.Columns.Count];
					ColumnType[1] = Xls.TypeText; 

					Res = Xls.createXlsConsign(dtCons, GetPathExport(), ColumnType);

					if (Res == "0")
					{	
						GetDownloadFile(Res);
				
						// Kill Process after save
						Xls.KillProcesses("EXCEL");
					} 
					else
					{
						lblErrorMessage.Text = Res;
					}
				}
			}
			catch(Exception ex)
			{
				lblErrorMessage.Text = ex.Message.ToString() ;
			}
			finally
			{
				dtCons.Dispose();
				dtSource.Dispose();
			}
		}

		private string  GetPathExport()
		{
			FileName = Session["userID"].ToString().ToUpper() + "_HCRScanning_" + String.Format("{0:yyyyMMdd}", DateTime.Now);
			string mapFile = Server.MapPath("..\\WebUI\\Export") + "\\" + FileName + ".xls";
			return mapFile;
		}

		private void GetDownloadFile(string Res)
		{
			if (Res == "0")
			{
				FileName = Session["userID"].ToString().ToUpper() + "_HCRScanning_" + String.Format("{0:yyyyMMdd}", DateTime.Now);
				Response.ContentType="application/ms-excel";
				Response.AddHeader( "content-disposition","attachment; filename=" + FileName + ".xls");
				FileStream fs = new FileStream(GetPathExport(),FileMode.Open);
				long FileSize;
				FileSize = fs.Length;
				byte[] getContent = new byte[(int)FileSize];
				fs.Read(getContent, 0, (int)fs.Length);
				fs.Close();

				Response.BinaryWrite(getContent);
			} 
		}
#endregion ExportDataMissing

	}
}
