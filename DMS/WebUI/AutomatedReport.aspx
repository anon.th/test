<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<%@ Page language="c#" Codebehind="AutomatedReport.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.AutomatedReport" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Freight Summary On-forwarding</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="FreightSummary" method="post" runat="server">
			<asp:label style="Z-INDEX: 103; POSITION: absolute; TOP: 10px; LEFT: 20px" id="lblTitle" runat="server"
				CssClass="maintitleSize" Height="27px" Width="365px">Automated Report</asp:label><asp:button style="Z-INDEX: 100; POSITION: absolute; TOP: 54px; LEFT: 19px" id="btnQuery" runat="server"
				CssClass="queryButton" Height="20" Width="64px" Text="Query"></asp:button><asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 80px; LEFT: 20px" id="lblErrorMessage"
				runat="server" CssClass="errorMsgColor" Height="21px" Width="528px">Place holder for err msg</asp:label>&nbsp;
			<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 54px; LEFT: 84px" id="btnGenerate"
				runat="server" CssClass="queryButton" Height="20" Width="80px" Text="Generate"></asp:button>
			<table style="Z-INDEX: 104; POSITION: absolute; WIDTH: 500px; TOP: 100px; LEFT: 16px" id="tblShipmentTracking"
				border="0" runat="server">
				<tr>
					<td>
						<fieldset><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="77px" Font-Bold="True">Dates</asp:label></legend>
							<table style="HEIGHT: 109px">
								<tr>
									<td colSpan="4"><asp:radiobutton id="rbInvoicedDate" runat="server" CssClass="tableRadioButton" Height="21px" Width="128px"
											Text="Invoiced Date" GroupName="QueryDate" AutoPostBack="True"></asp:radiobutton><asp:radiobutton id="rbManifestedDate" runat="server" CssClass="tableRadioButton" Height="21px" Width="128px"
											Text="Manifested Date" GroupName="QueryDate" AutoPostBack="True" Checked="True"></asp:radiobutton></td>
								</tr>
								<tr>
									<td><asp:radiobutton id="rbMonth" runat="server" CssClass="tableRadioButton" Height="21px" Width="73px"
											Text="Month" Checked="True" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton></td>
									<td><asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Height="19px" Width="88px"></asp:dropdownlist></td>
									<td><asp:label id="Label5" runat="server" CssClass="tableLabel" Height="22px" Width="30px">Year</asp:label></td>
									<td><cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" NumberPrecision="4"
											NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></td>
								</tr>
								<tr>
									<td><asp:radiobutton id="rbPeriod" runat="server" CssClass="tableRadioButton" Width="62px" Text="Period"
											GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton></td>
									<td><cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></td>
									<td></td>
									<td><cc1:mstextbox style="Z-INDEX: 0" id="txtTo" runat="server" CssClass="textField" Width="83" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></td>
								</tr>
								<TR>
									<TD><asp:radiobutton id="rbDate" runat="server" CssClass="tableRadioButton" Height="22px" Width="74px"
											Text="Date" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton></TD>
									<TD><cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="82" TextMaskType="msDate"
											MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD></TD>
									<TD></TD>
								</TR>
							</table>
						</fieldset>
					</td>
					<td>
						<fieldset><legend><asp:label id="Label1" runat="server" CssClass="tableHeadingFieldset" Width="77px" Font-Bold="True">Payer Type</asp:label></legend>
							<table style="WIDTH: 296px; HEIGHT: 109px">
								<tr>
									<td vAlign="top"><asp:label id="Label6" runat="server" CssClass="tableLabel" Height="22px" Width="88px">Payer Type</asp:label></td>
									<td><asp:listbox id="lsbCustType" runat="server" Height="61px" Width="137px" SelectionMode="Multiple"></asp:listbox></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr>
									<td><asp:label id="Label10" runat="server" CssClass="tableLabel" Height="22px" Width="79px">Payer Code</asp:label></td>
									<td style="WIDTH: 360px" class="tableLabel"><cc1:mstextbox id="txtPayerCode" runat="server" CssClass="textField" Width="139px" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="10"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnPayerCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</table>
						</fieldset>
					</td>
				</tr>
				<TR>
					<TD>
						<fieldset><legend><asp:label id="lbStatisticsType" runat="server" CssClass="tableHeadingFieldset" Width="104px"
									Font-Bold="True">Report Type</asp:label></legend>
							<table>
								<tr>
									<td><asp:radiobutton id="rbtByDomesticShipments" runat="server" CssClass="tableRadioButton" Height="21px"
											Width="228px" Text="Domestic Shipments" Checked="True" GroupName="StatisticsType" AutoPostBack="True"></asp:radiobutton></td>
								</tr>
								<tr>
									<td><asp:radiobutton id="rbtByDomesticShipmentsBySpend" runat="server" CssClass="tableRadioButton" Height="21px"
											Width="288px" Text="Domestic Shipments (Spend per Destination)" GroupName="StatisticsType" AutoPostBack="True"></asp:radiobutton></td>
								</tr>
								<tr>
									<td><asp:radiobutton id="rbtByCustomsInvoices" runat="server" CssClass="tableRadioButton" Height="21px"
											Width="228px" Text="Customs Invoices" GroupName="StatisticsType" AutoPostBack="True"></asp:radiobutton></td>
								</tr>
							</table>
						</fieldset>
					</TD>
					<TD></TD>
				</TR>
			</table>
		</form>
	</body>
</HTML>
