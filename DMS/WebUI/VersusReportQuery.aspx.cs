using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;
using Cambro.Web.DbCombo;

namespace com.ties
{
	/// <summary>
	/// Summary description for CODAmountCollectedReport.
	/// </summary>
	public class VersusReportQuery : BasePage
	{
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.Label lblRouteType;
		protected System.Web.UI.WebControls.Label lblOriginDisCenter;
		protected Cambro.Web.DbCombo.DbCombo DbComboOriginDC;
		protected System.Web.UI.WebControls.Label lblDesDisCenter;
		protected Cambro.Web.DbCombo.DbCombo DbComboDestinationDC;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentTracking;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.HtmlControls.HtmlTable tblRouteType;
		protected System.Web.UI.WebControls.RadioButton RadioButton1;
		protected System.Web.UI.WebControls.RadioButton RadioButton2;
		protected System.Web.UI.WebControls.RadioButton rbActualPickupDate;
		protected System.Web.UI.WebControls.Label lblVersusStatus;
		protected System.Web.UI.HtmlControls.HtmlTable tblVersusStatus;
		protected System.Web.UI.WebControls.RadioButton rbVFirstCond;
		protected System.Web.UI.WebControls.RadioButton rbVSecondCond;
		protected System.Web.UI.WebControls.RadioButton rbVFourthCond;
		protected System.Web.UI.WebControls.Label lblStatusCode1;
		protected com.common.util.msTextBox txtFSC;
		protected System.Web.UI.WebControls.Label lblStatusCode2;
		protected com.common.util.msTextBox txtSSC;
		protected System.Web.UI.WebControls.Label Label5;
		protected com.common.util.msTextBox txtTSC;
		protected System.Web.UI.HtmlControls.HtmlTable tblStatusCode;
		protected System.Web.UI.WebControls.RadioButton rbVThirdCond;
	
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.rbActualPickupDate.CheckedChanged += new System.EventHandler(this.rbActualPickupDate_CheckedChanged);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.rbVFirstCond.CheckedChanged += new System.EventHandler(this.rbVFirstCond_CheckedChanged);
			this.rbVSecondCond.CheckedChanged += new System.EventHandler(this.rbVSecondCond_CheckedChanged);
			this.rbVThirdCond.CheckedChanged += new System.EventHandler(this.rbVThirdCond_CheckedChanged);
			this.ID = "ShipmentPendingCOD";
			this.Load += new System.EventHandler(this.Page_Load);

		}

		#endregion

		String m_strAppID=null;
		String m_strEnterpriseID=null;
		String m_strCulture=null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			DbComboOriginDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboDestinationDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			if(!Page.IsPostBack)
			{
				DefaultScreen();
				ddlmonths();
				Session["toRefresh"]=false;
			}

			SetDbComboServerStates();
		}
        

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			//Modified By Tom Jun 4, 2010
			if(rbVFourthCond.Checked)
			{
				lblErrorMessage.Text = "";
				bool icheck =  false;
				if(icheck == false)
				{
					String strModuleID = Request.Params["MODID"];
					DataSet dsShipment = GetShipmentQueryData();

					String strUrl = null;
					strUrl = "ReportViewer.aspx";
					Session["FORMID"] = "VersusReportQuery";
					Session["SESSION_DS1"] = dsShipment;
					ArrayList paramList = new ArrayList();
					paramList.Add(strUrl);
					String sScript = Utility.GetScript("openParentWindow.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

				}
			}
			else
			{
			lblErrorMessage.Text = "";
			bool icheck =  ValidateValues();
				if(icheck == false)
				{
					String strModuleID = Request.Params["MODID"];
					DataSet dsShipment = GetShipmentQueryData();

					String strUrl = null;
					strUrl = "ReportViewerDataSet.aspx";
					Session["FORMID"] = "VersusReportQuery";
					Session["SESSION_DS1"] = dsShipment;
					ArrayList paramList = new ArrayList();
					paramList.Add(strUrl);
					String sScript = Utility.GetScript("openParentWindow.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

				}
			}
			//End Modified By Tom Jun 4, 2010
		}


		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}


		#region "Page Functions"

		private DataTable CreateEmptyDataTable()
		{
			DataTable dtShipment = new DataTable();

			#region "Dates"
			dtShipment.Columns.Add(new DataColumn("tran_date", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
			dtShipment.Columns.Add(new DataColumn("end_date", typeof(DateTime)));
			#endregion
		
			#region "Route / DC Selection"
			dtShipment.Columns.Add(new DataColumn("origin_dc", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("destination_dc", typeof(string)));
			#endregion

			#region "Versus"
			dtShipment.Columns.Add(new DataColumn("versus_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("state_code1", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("state_code2", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("state_code3", typeof(string)));
			#endregion


			return dtShipment;
		}


		private DataSet GetShipmentQueryData()
		{
			DataSet dsShipment = new DataSet();
			DataTable dtShipment = CreateEmptyDataTable();
			DataRow dr = dtShipment.NewRow(); 
			
			#region "Dates"
			if (rbActualPickupDate.Checked) 
			{	// Pickup Date
				dr["tran_date"] = "U";
			}
//			if (rbManifestDate.Checked) 
//			{	// Actual HCR Return Date
//				dr["tran_date"] = "M";
//			}
			
			string strMonth =null;
			if (rbMonth.Checked) 
			{	// Month Selected
				strMonth = ddMonth.SelectedItem.Value;
				if (strMonth != "" && txtYear.Text != "") 
				{
					dr["start_date"] = DateTime.ParseExact ("01/"+strMonth.ToString()+"/"+txtYear.Text, "dd/MM/yyyy", null);
					int intLastDay = 0;
					intLastDay = LastDayOfMonth(strMonth, txtYear.Text);
					dr["end_date"] = DateTime.ParseExact (intLastDay.ToString()+"/"+strMonth.ToString()+"/"+txtYear.Text+" 23:59", "dd/MM/yyyy HH:mm", null);
				}
			}
			if (rbPeriod.Checked) 
			{	// Period Selected
				if (txtPeriod.Text != ""  && txtTo.Text != "") 
				{
					dr["start_date"] = DateTime.ParseExact(txtPeriod.Text ,"dd/MM/yyyy",null);
					dr["end_date"] = DateTime.ParseExact(txtTo.Text+" 23:59" ,"dd/MM/yyyy HH:mm",null);
				}
			}
			if (rbDate.Checked) 
			{	// Date Selected
				if (txtDate.Text != "") 
				{
					dr["start_date"] = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy",null);
					dr["end_date"] = DateTime.ParseExact(txtDate.Text+" 23:59", "dd/MM/yyyy HH:mm",null);
				}
			}
			#endregion

			#region "Route / DC Selection"

			dr["origin_dc"] = DbComboOriginDC.Value;
			dr["destination_dc"] = DbComboDestinationDC.Value;
			#endregion

			#region "Versus"
			if(rbVFirstCond.Checked)
			{
				dr["versus_type"] = "M1";
				dr["state_code1"] = txtFSC.Text;
				dr["state_code2"] = txtSSC.Text;
				dr["state_code3"] = "";
			}
			if(rbVSecondCond.Checked)
			{
				dr["versus_type"] = "M2";
				dr["state_code1"] = "";
				dr["state_code2"] = txtFSC.Text;
				dr["state_code3"] = "";
			}
			if(rbVThirdCond.Checked)
			{
				dr["versus_type"] = "M3";
				dr["state_code1"] = txtFSC.Text;
				dr["state_code2"] = txtSSC.Text;
				dr["state_code3"] = txtTSC.Text;
			}

			#endregion

			dtShipment.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipment);
			return dsShipment;
		}


		private void DefaultScreen()
		{
			#region "Dates"
			/*rbBookingDate.Checked = true;
			rbActualPickupDate.Checked = false;
			rbActualPODDate.Checked = false;
			rbActualHCReturnDate.Checked = false;*/
			rbMonth.Checked = true;
			rbPeriod.Checked = false;
			rbDate.Checked = false;

			ddMonth.Enabled = true;
			ddMonth.SelectedIndex = -1;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;

			#endregion

			#region "Route / DC Selection"

			DbComboOriginDC.Text="";
			DbComboOriginDC.Value="";

			DbComboDestinationDC.Text="";
			DbComboDestinationDC.Value="";
			#endregion

			#region "Versus"
			rbActualPickupDate.Checked = true;
			rbVFirstCond.Checked = true;
			rbVSecondCond.Checked = false;
			rbVThirdCond.Checked = false;
			txtFSC.ReadOnly = false;
			txtSSC.ReadOnly = false;
			txtTSC.ReadOnly = true;
			txtFSC.Text = "";
			txtSSC.Text = "";
			txtTSC.Text = "";
			#endregion
			
			lblErrorMessage.Text = "";
		}


		private bool ValidateValues()
		{
			bool iCheck=false;
	
			if((ddMonth.SelectedIndex==0)&&(txtYear.Text=="")&&(txtPeriod.Text=="")&&(txtTo.Text=="")&&(txtDate.Text==""))
			{				
				lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT",utility.GetUserCulture());
				return iCheck=true;			
			}
			if(rbMonth.Checked == true)
			{
				if((ddMonth.SelectedIndex>0) &&(txtYear.Text==""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((ddMonth.SelectedIndex==0) &&(txtYear.Text!=""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
//				else
//				{
//					return iCheck=false;
//				}
			}
			if(rbPeriod.Checked == true )
			{
				if((txtPeriod.Text!="")&&(txtTo.Text==""))
				{					
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_END_DT",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((txtPeriod.Text=="")&&(txtTo.Text!=""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_START_DT",utility.GetUserCulture());					
					return iCheck=true;			
				}
//				else
//				{
//					return iCheck=false;
//				}
			}
			if(rbDate.Checked == true )
			{
				if(txtDate.Text=="")
				{					
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT",utility.GetUserCulture());	
					return iCheck=true;				
				}
			}
			return iCheck;
			
		}


		private int ValidateData()
		{
			if (txtYear.Text != "" )
			{
				if (Convert.ToInt32(txtYear.Text) < 2000 || Convert.ToInt32(txtYear.Text) > 2099)
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
					return -1;
				}
			}
			return 1;
		}
		

		private int LastDayOfMonth(String strMonth, String strYear)
		{
			int intLastDay = 0;
			switch (strMonth)
			{
				case "01":
				case "03":
				case "05":
				case "07":
				case "08":
				case "10":
				case "12":
					intLastDay = 31;
					break;
				case "04":
				case "06":
				case "09":
				case "11":
					intLastDay = 30;
					break;
				case "02":
					intLastDay = 28;
					break;
			}
			return intLastDay;
		}


		private void OpenWindowpage(String strUrl, String strFeatures)
		{
			
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion

		#region "Dates Part : Controls"

		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = true;
		}


		#endregion

		#region "Route / DC Selection : Controls"

		


		#endregion

		#region "Dates Part : DropDownList"

		private void ddlmonths()
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);

			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}


		#endregion

	
		#region "Dates Part : DropDownList"

		private void ddlmonthss()
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);

			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}


		#endregion

		#region "Payer Type Part : DropDownList"

		public void LoadCustomerTypeListt()
		{
			ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"customer_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
			}
		}


		#endregion

		#region "Route / DC Selection : DropDownList"

		private void SetDbComboServerStates()
		{
			String strDeliveryType=null;
				strDeliveryType="A";

			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDeliveryType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDeliveryType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDeliveryType)+"'";
					if(strDeliveryType == "S") 
					{
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct delivery_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
				}				
			}
			
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboDistributionCenterSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,args);	
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboStatusSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.VersusStatusQuery(strAppID,strEnterpriseID,args);	
			return dataset;
		}


		#endregion


		private void rbVFirstStatus_CheckedChanged(object sender, System.EventArgs e)
		{
			lblStatusCode1.Text = "Existing Status Code";
			lblStatusCode2.Text = "Missing Status Code";
		}

		private void rbVOtherStatus_CheckedChanged(object sender, System.EventArgs e)
		{
			lblStatusCode1.Text = "Status Code 1";
			lblStatusCode2.Text = "Status Code 2";
		}

		private void rbBookingDate_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void rbActualPickupDate_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void rbVFirstCond_CheckedChanged(object sender, System.EventArgs e)
		{
			txtFSC.ReadOnly = false;
			txtSSC.ReadOnly = false;
			txtTSC.ReadOnly = true;
			txtFSC.Text = "";
			txtSSC.Text = "";
			txtTSC.Text = "";
		}

		private void rbVSecondCond_CheckedChanged(object sender, System.EventArgs e)
		{
			txtFSC.ReadOnly = false;
			txtSSC.ReadOnly = true;
			txtTSC.ReadOnly = true;
			txtFSC.Text = "";
			txtSSC.Text = "";
			txtTSC.Text = "";
		}

		private void rbVThirdCond_CheckedChanged(object sender, System.EventArgs e)
		{
			txtFSC.ReadOnly = false;
			txtSSC.ReadOnly = false;
			txtTSC.ReadOnly = false;
			txtFSC.Text = "";
			txtSSC.Text = "";
			txtTSC.Text = "";
		}

		private void rbVFourthCond_CheckedChanged(object sender, System.EventArgs e)
		{
			txtFSC.ReadOnly = false;
			txtSSC.ReadOnly = false;
			txtTSC.ReadOnly = false;
			txtFSC.Text = "";
			txtSSC.Text = "";
			txtTSC.Text = "";
		}
	
	}
}
