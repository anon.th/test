		//-------------------------------------------------------
		//this is to select or unselect the datagrid check boxes 
		function DGSelectOrUnselectAll(grdid,obj,objlist){ 
		//this function decides whether to check or uncheck all
		if (document.getElementById(obj).checked)
			DGSelectAll(grdid,objlist) 
		else
			DGUnselectAll(grdid,objlist) 
		} 
		//---------- 
		 
		function DGSelectAll(grdid,objid){ 
		//.this function is to check all the items
			var chkbox; 
			var i=3; 
		    
			chkbox=document.getElementById(grdid + 
					'__ctl' + i + '_' + objid); 

			while(chkbox!=null){ 
				chkbox.checked=true; 
				i=i+1; 
				chkbox=document.getElementById(grdid + 
						'__ctl' + i + '_' + objid); 
			} 

		}//-------------- 

		function DGUnselectAll(grdid,objid){ 
		//.this function is to uncheckcheck all the items
			var chkbox; 
			var i=3; 

			chkbox=document.getElementById(grdid + 
					'__ctl' + i + '_' + objid); 

			while(chkbox!=null){ 
				chkbox.checked=false; 
				i=i+1; 
				chkbox=document.getElementById(grdid + 
						'__ctl' + i + '_' + objid); 
			} 
		}