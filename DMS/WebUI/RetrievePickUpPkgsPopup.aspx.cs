using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.util;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for RetrievePickUpPkgs.
	/// </summary>
	public class RetrievePickUpPkgs : BasePopupPage
	{
		protected System.Web.UI.WebControls.DataGrid dgShipUpdate;
		DataSet m_dsPickupPkgs = null;
	//	Utility utility = null;
		String appID = null;
		String enterpriseID = null;
		int iBookingNo = 0;
		protected System.Web.UI.WebControls.Button btnCancel;
		String strBookingNo = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		//	utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();

			strBookingNo = Request.Params["BOOKINGNO"];
			
			if(strBookingNo.Length > 0)
				iBookingNo = Convert.ToInt32(strBookingNo);
			

			if(!Page.IsPostBack)
			{	
				if(Session["SESSION_DS5"] == null)
				{
					//call the method to populate the dataset.
					m_dsPickupPkgs = DomesticShipmentMgrDAL.GetPickupShipment(appID,enterpriseID,iBookingNo);
					//Disable the row for which shipment is already created.
					m_dsPickupPkgs.Tables[0].Columns.Add(new DataColumn("isCreated", typeof(bool)));
					int cnt = m_dsPickupPkgs.Tables[0].Rows.Count;
					int i = 0;
					
					for(i=0;i<cnt;i++)
					{
						DataRow drCurrent = m_dsPickupPkgs.Tables[0].Rows[i];
						//call the method to check whether the shipment is created or not
						if(drCurrent["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull")))
						{
							drCurrent["isCreated"] = false;
						}
						else
						{
							drCurrent["isCreated"] = true;
						}
					}



					Session["SESSION_DS5"] = m_dsPickupPkgs;
				}
				else
				{
					m_dsPickupPkgs = (DataSet)Session["SESSION_DS5"];
				}
				BindGrid();	
			}
			getPageControls(Page);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.dgShipUpdate.SelectedIndexChanged += new System.EventHandler(this.dgShipUpdate_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void BindGrid()
		{
			dgShipUpdate.DataSource = m_dsPickupPkgs;
			dgShipUpdate.DataBind();
		}

		private void dgShipUpdate_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			
			Label lblSerialNo = (Label)dgShipUpdate.SelectedItem.FindControl("lblSerialNo");
			String strSerialNo = lblSerialNo.Text;
			Session["SerialNo"] = strSerialNo;

			Label lblRecipientZip = (Label)dgShipUpdate.SelectedItem.FindControl("lblRecipientZip");
			String strRecipientZip = lblRecipientZip.Text;

			int iSerialNo = Convert.ToInt32(strSerialNo);
			//Get the package details
			DataSet dsPkgData = PRBMgrDAL.GetPkgData(appID,enterpriseID,strBookingNo,iSerialNo);
			Session["SESSION_DS2"] = dsPkgData;

			DataSet dsVASData = PRBMgrDAL.GetVASData(appID,enterpriseID,strBookingNo,iSerialNo);
			Session["SESSION_DS1"] = dsVASData;

			DataSet dsSerialShpData = PRBMgrDAL.GetPikupShpSerial(appID,enterpriseID,iBookingNo,iSerialNo);
			Session["SESSION_DS6"] = dsSerialShpData;

			Session["SESSION_DS5"] = null;


			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);

		}

		protected void dgShipUpdate_Bound(object sender, DataGridItemEventArgs e)
		{
			//Label lblIsCreated = (Label)dgShipUpdate..SelectedItem.FindControl("lblIsCreated");
			Label lblIsCreated = (Label)e.Item.FindControl("lblIsCreated");
			if(lblIsCreated != null)
			{
				String strIsCreated = lblIsCreated.Text;
			
				if(strIsCreated.Equals("True"))
					e.Item.Enabled = false;
				else if(strIsCreated.Equals("False"))
					e.Item.Enabled = true;
			}
		}
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
	}
}
