using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties;
using com.ties.DAL;
using com.ties.classes;
using com.common.applicationpages;


namespace com.ties
{
	/// <summary>
	/// Summary description for CreditDebitTracking.
	/// </summary>
	public class CreditDebitTracking : com.common.applicationpages.BasePage
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnGenerate;
		protected System.Web.UI.WebControls.Label lblDateType;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected System.Web.UI.WebControls.Label lblTo;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected System.Web.UI.WebControls.Label lblPayerType;
		protected System.Web.UI.WebControls.RadioButton rbCustomer;
		protected System.Web.UI.WebControls.RadioButton rbAgent;
		protected System.Web.UI.WebControls.RadioButton rbBoth;
		protected System.Web.UI.WebControls.Label lblPayer;
		protected System.Web.UI.WebControls.Label lblPayerAc;
		protected com.common.util.msTextBox txtYear;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.RadioButton rbCreditNote;
		protected System.Web.UI.WebControls.RadioButton rbDebitNote;		
		protected System.Web.UI.WebControls.Label lblInvoiceNo;
		protected com.common.util.msTextBox txtConsignmentNo;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.RadioButton rbCreationDate;
		protected System.Web.UI.WebControls.RadioButton rbInvoiceDate;
		
		protected Cambro.Web.DbCombo.DbCombo dbCmbPayerID;
		protected Cambro.Web.DbCombo.DbCombo dbCmbPayerName;
		protected Cambro.Web.DbCombo.DbCombo DbCreditDebitNo;
		protected Cambro.Web.DbCombo.DbCombo DbInvoiceNo;
		protected Cambro.Web.DbCombo.DbCombo DbConsignmentNo;

		static private int m_iSetSize = 4;
		SessionDS m_sdsCDDetail = null;
		SessionDS m_sdsCreditNote = null;
		SessionDS m_sdsCreditDetail=null;

		private String sSearchType=null;		
		private String sInvoiceNo=null;
		private String sCreditDebitNo=null;
		private String sConsignmentNo=null;

		private String sDateType=null;		
		DateTime dtStartDate=DateTime.Now.AddYears(-1);
		DateTime dtEndDate=DateTime.Now;

		private String sPayerType=null;
		private String sPayerID=null;
		private String sPayerName=null;

		private String appID=null;
		private String enterpriseID=null;
		private String culture=null;

		//private Utility utility=null;
		protected System.Web.UI.HtmlControls.HtmlGenericControl mainPanel;
		protected System.Web.UI.HtmlControls.HtmlTable tblCreditDebitQry;
		protected System.Web.UI.HtmlControls.HtmlGenericControl listPanel;
		protected System.Web.UI.HtmlControls.HtmlTable listTable;
		protected System.Web.UI.HtmlControls.HtmlGenericControl detailPanel;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.WebControls.DataGrid dgCreditDebitList;
		protected System.Web.UI.WebControls.Button btnQuery1;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Button btnPrint;
		protected System.Web.UI.WebControls.Label lblCreditNoteDate;
		protected com.common.util.msTextBox txtCreditNoteDate;
		protected System.Web.UI.WebControls.Label lblInvoiceDate;
		protected com.common.util.msTextBox txtInvoiceDate;
		protected System.Web.UI.WebControls.Label lblPayerCode;
		protected System.Web.UI.WebControls.Label lblTotalCreditAmt;
		protected System.Web.UI.WebControls.TextBox txtTotalCreditAmt;
		protected System.Web.UI.WebControls.TextBox txtRemarks;
		protected System.Web.UI.WebControls.Button btnInsertCons;
		protected System.Web.UI.WebControls.DataGrid dgAssignedConsignment;
		protected System.Web.UI.WebControls.TextBox txtCreditNoteNo;
		protected System.Web.UI.WebControls.TextBox txtPayerID;
		protected System.Web.UI.WebControls.TextBox txtPayerName;
		protected System.Web.UI.WebControls.TextBox txtInvoiceNo;
		protected System.Web.UI.WebControls.Label lblInvoice1;
		protected System.Web.UI.WebControls.Label lblSearchType;
		protected System.Web.UI.HtmlControls.HtmlGenericControl scrollCDList;
		protected System.Web.UI.WebControls.Button btnListBack;
		protected System.Web.UI.WebControls.Button btnDetailBack;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.Button btnNO;
		protected System.Web.UI.HtmlControls.HtmlGenericControl msgPanel;
		protected System.Web.UI.WebControls.Label lblConsignmentNo;
		protected System.Web.UI.WebControls.Label lblCreditDebitNo;
		protected System.Web.UI.WebControls.Label lblCreditNoteNo;
		protected System.Web.UI.WebControls.Label lblPayerName;
		protected System.Web.UI.WebControls.Label lblRemarks;
		protected System.Web.UI.WebControls.Button btnToSaveChanges;
		protected System.Web.UI.WebControls.Button btnNotToSave;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label1;
		private DataView m_dvMonths;

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
			this.rbCreditNote.CheckedChanged += new System.EventHandler(this.rbCreditNote_CheckedChanged);
			this.rbDebitNote.CheckedChanged += new System.EventHandler(this.rbDebitNote_CheckedChanged);
			this.rbCreationDate.CheckedChanged += new System.EventHandler(this.rbCreationDate_CheckedChanged);
			this.rbInvoiceDate.CheckedChanged += new System.EventHandler(this.rbInvoiceDate_CheckedChanged);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.rbCustomer.CheckedChanged += new System.EventHandler(this.rbCustomer_CheckedChanged);
			this.rbAgent.CheckedChanged += new System.EventHandler(this.rbAgent_CheckedChanged);
			this.rbBoth.CheckedChanged += new System.EventHandler(this.rbBoth_CheckedChanged);
			this.btnListBack.Click += new System.EventHandler(this.btnListBack_Click);
			this.dgCreditDebitList.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgCreditDebitList_Button);
			this.btnQuery1.Click += new System.EventHandler(this.btnQuery1_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
			this.btnDetailBack.Click += new System.EventHandler(this.btnDetailBack_Click);
			this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged_1);
			this.btnInsertCons.Click += new System.EventHandler(this.btnInsertCons_Click);
			this.btnToSaveChanges.Click += new System.EventHandler(this.btnToSaveChanges_Click);
			this.btnNotToSave.Click += new System.EventHandler(this.btnNotToSave_Click);
			this.btnNO.Click += new System.EventHandler(this.btnNO_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Page_Load(object sender, System.EventArgs e)
		{			
			// Put user code to initialize the page here
			//Added regkey here			
			DbCreditDebitNo.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbInvoiceNo.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbPayerID.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbPayerName.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			utility = new Utility (System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			culture=utility.GetUserCulture();
			if(!Page.IsPostBack)
			{	
				ViewState["CNOperation"] = Operation.None;	
				ViewState["CreditDetailMode"] = ScreenMode.None;
				ViewState["CreditDetailOperation"] = Operation.None;
				Session["SESSION_DS2"]=null;
				Session["SESSION_DS3"]=null;
				Session["SESSION_DS4"]=null;
				DefaultFirstScreen(); // Show only the FIRST Page
				
			}
			else
			{
				if(Session["SESSION_DS4"] != null)
				{
					m_sdsCDDetail = (SessionDS)Session["SESSION_DS4"];
				}
				if(Session["SESSION_DS2"] != null)
				{
					m_sdsCreditNote = (SessionDS)Session["SESSION_DS2"];
				}
				if(Session["SESSION_DS3"] != null)
				{
					m_sdsCreditDetail = (SessionDS)Session["SESSION_DS3"];
				}		
				sSearchType=(String) ViewState["SearchType"];
				sInvoiceNo=(String) ViewState["InvoiceNo"];
			}

			addDbComboEventHandler();
			SetInvoiceNoServerStates();
			SetCreditDebitServerStates();
			SetPayerIDServerStates();
			SetPayerNameServerStates();
		}
		private void addDbComboEventHandler()
		{
			if(dgAssignedConsignment.EditItemIndex == -1)
				return;

			this.DbConsignmentNo = (Cambro.Web.DbCombo.DbCombo)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("DbConsignmentNo");
			if(this.DbConsignmentNo != null)
			{
				this.DbConsignmentNo.SelectedItemChanged += new System.EventHandler(this.DbConsignmentNo_OnSelectedIndexChanged);
				SetDbComboServerStates();
			}
		}
		private void SetDbComboServerStates()
		{			
			Hashtable hash = new Hashtable();
			
			if (sInvoiceNo=="")
				sInvoiceNo=(String) ViewState["InvoiceNo"];

			hash.Add("InvoiceNo", sInvoiceNo);
			if(this.DbConsignmentNo == null)
				return;

			this.DbConsignmentNo.ServerState = hash;
			this.DbConsignmentNo.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}
		
		private void SetPayerNameServerStates()
		{			
			if (rbCustomer.Checked==true)
				sPayerType="C";
			else if (rbAgent.Checked==true)
				sPayerType="A";
			else if (rbBoth.Checked==true)
				sPayerType="B";
			else
				sPayerType="B";
			
			sPayerID=dbCmbPayerID.Value;
			if (sPayerID=="")
				sPayerID=dbCmbPayerID.Text;

			Hashtable hash = new Hashtable();
			hash.Add("sPayerType", sPayerType);
			if (sPayerID !="")					//Add to hash table OnLy if sPayerID is <> ""
				hash.Add("sPayerID", sPayerID);

			dbCmbPayerName.ServerState = hash;
			dbCmbPayerName.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}

		private void SetPayerIDServerStates()
		{			
			if (rbCustomer.Checked==true)
				sPayerType="C";
			else if (rbAgent.Checked==true)
				sPayerType="A";
			else if (rbBoth.Checked==true)
				sPayerType="B";
			else
				sPayerType="B";

			sPayerName=dbCmbPayerName.Value;
			if (sPayerName=="")
				sPayerName=dbCmbPayerName.Text;
			
			Hashtable hash = new Hashtable();
			hash.Add("sPayerType", sPayerType);
			if (sPayerName !="")					//Add to hash table OnLy if sPayerName is <> ""
				hash.Add("sPayerName", sPayerName);

			dbCmbPayerID.ServerState = hash;
			dbCmbPayerID.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";			
		}

		private void SetCreditDebitServerStates()
		{					
			if (rbCreditNote.Checked==true)
				sSearchType="C";		
			else if (rbDebitNote.Checked==true)
				sSearchType="D";					
			
			sInvoiceNo =DbInvoiceNo.Value;
			if (sInvoiceNo=="")					// If DbInvoiceNo.Value is "", Load from DbInvoiceNo.Text
				sInvoiceNo=DbInvoiceNo.Text;
			
			Hashtable hash = new Hashtable();
			hash.Add("sSearchType", sSearchType);
			if (sInvoiceNo !="")				//Add to hash table only if sInvoiceNo is NOT NULL
				hash.Add("sInvoiceNo", sInvoiceNo);

			DbCreditDebitNo.ServerState = hash;
			DbCreditDebitNo.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}

		private void SetInvoiceNoServerStates()
		{			
			if (rbCreditNote.Checked==true)
				sSearchType="C";		
			else if (rbDebitNote.Checked==true)
				sSearchType="D";		
			
			sCreditDebitNo=DbCreditDebitNo.Value;
			if (sCreditDebitNo=="")					// If DbCreditDebitNo.Value is "", Load from DbCreditDebitNo.Text
				sCreditDebitNo=DbCreditDebitNo.Text;
			
			Hashtable hash = new Hashtable();
			hash.Add("sSearchType", sSearchType);			
			if (sCreditDebitNo !="")				//Add to hash only if sCreditDebitNo is NOT NULL
				hash.Add("sCreditDebitNo", sCreditDebitNo);

			DbInvoiceNo.ServerState = hash;
			DbInvoiceNo.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";			
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]		
		public static object CreditDebitServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
					
			String strSearchType ="";
			String strInvoiceNo="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["sSearchType"] != null && args.ServerState["sSearchType"].ToString().Length > 0)
					strSearchType = args.ServerState["sSearchType"].ToString();
				if(args.ServerState["sInvoiceNo"] != null && args.ServerState["sInvoiceNo"].ToString().Length > 0)
					strInvoiceNo = args.ServerState["sInvoiceNo"].ToString();
			}
			
			DataSet dataset = DbComboDAL.CreditDebitQuery(strAppID,strEnterpriseID,args, strSearchType, strInvoiceNo);	
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethod(true)]
		public static object InvoiceServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strSearchType ="";
			String strCreditDebitNo="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["sSearchType"] != null && args.ServerState["sSearchType"].ToString().Length > 0)
					strSearchType = args.ServerState["sSearchType"].ToString();
				if (args.ServerState["sCreditDebitNo"] != null && args.ServerState["sCreditDebitNo"].ToString().Length > 0 )
					strCreditDebitNo = (args.ServerState["sCreditDebitNo"].ToString());
			}	

			DataSet dataset = DbComboDAL.InvoiceQuery(strAppID,strEnterpriseID,args, strSearchType, strCreditDebitNo);	
			return dataset;                
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]		
		public static object PayerIDServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
					
			String strPayerType="";
			String strPayerName="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["sPayerType"] != null && args.ServerState["sPayerType"].ToString().Length > 0)
					strPayerType = args.ServerState["sPayerType"].ToString();
				if(args.ServerState["sPayerName"] != null && args.ServerState["sPayerName"].ToString().Length > 0)
					strPayerName = args.ServerState["sPayerName"].ToString();	

			}
			
			DataSet dataset = DbComboDAL.PayerIDQuery(strAppID,strEnterpriseID,args, strPayerType, strPayerName);	
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethod(true)]
		public static object PayerNameServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strPayerType="";
			String strPayerID= "";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["sPayerType"] != null && args.ServerState["sPayerType"].ToString().Length > 0)
					strPayerType = args.ServerState["sPayerType"].ToString();
				if (args.ServerState["sPayerID"] != null && args.ServerState["sPayerID"].ToString().Length > 0 )
					strPayerID = (args.ServerState["sPayerID"].ToString());
			}	

			DataSet dataset = DbComboDAL.PayerNameQuery(strAppID,strEnterpriseID,args, strPayerType, strPayerID);	
			return dataset;                
		}
		
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbConsignmentNoSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			String strInvoiceNo ="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["InvoiceNo"] != null && args.ServerState["InvoiceNo"].ToString().Length > 0)
				{
					strInvoiceNo = args.ServerState["InvoiceNo"].ToString();
				}				
			}
			DataSet dataset = DbComboDAL.ConsignmentQuery(strAppID,strEnterpriseID,strInvoiceNo,args);	
			return dataset;
		}		

		protected void DbConsignmentNo_OnSelectedIndexChanged(object sender, System.EventArgs e)
		{
			String strInvoiceNo = (String)ViewState["InvoiceNo"];
			int iOperation = (int) ViewState["CreditDetailOperation"];
			if(iOperation == (int)Operation.Update)
				return;

			String strConsignmentNo = this.DbConsignmentNo.Value;
			String strInvoiceAmt = CreditDebitMgrDAL.GetInvoiceAmt(appID,enterpriseID,strConsignmentNo);
			if(dgAssignedConsignment.EditItemIndex == -1)
			{
				return;
			}
			DataRow drCurrent = m_sdsCreditDetail.ds.Tables[0].Rows[dgAssignedConsignment.EditItemIndex];
			drCurrent["Consignment_No"] = strConsignmentNo;	
			ViewState["ConsignmnetNo"]=strConsignmentNo;
			BindConsGrid();

			Label lblInvoiceAmt = (Label)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("lblInvoiceAmt");			
			if (lblInvoiceAmt!= null)					
				lblInvoiceAmt.Text=strInvoiceAmt;
			TextBox txtRemark = (TextBox)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("txtRemark");			
			if(txtRemark!=null && txtRemark.Text.Trim()!="")
			{
				drCurrent["remark"] = txtRemark.Text;
			}
			TextBox txtInvoiceAmt = (TextBox)dgAssignedConsignment.Items[dgAssignedConsignment.EditItemIndex].FindControl("txtInvoiceAmt");			
			if (txtInvoiceAmt!= null)					
				txtInvoiceAmt.Text=strInvoiceAmt;			

		}

		
		private void btnGenerate_Click(object sender, System.EventArgs e)
		{			
//Check for From Date and To Date, if Both Credit/Debit No and Invoice No are null
			if (DbCreditDebitNo.Value.Trim() =="" && DbInvoiceNo.Value.Trim()=="")
			{
				if (rbMonth.Checked==true)
				{
					if (ddMonth.SelectedItem.Value == "" || txtYear.Text.Trim() =="")
					{
						//lblErrorMessage.Text = "Month and Year are required. Please enter values.";
						lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MM_YEAR_REQ", utility.GetUserCulture());
						return;
					}
				}
				if (rbPeriod.Checked==true)
				{
					if (txtPeriod.Text.Trim()== "" || txtTo.Text.Trim() =="")
					{
						lblErrorMessage.Text="From-Date and To-Date are required. Please enter values.";
						return;
					}
				}
				if (rbDate.Checked==true)
				{
					if (txtDate.Text.Trim() =="")
					{
						lblErrorMessage.Text="Date is required. Please enter value.";
						return;
					}
				}
			}
// Check the Text and Value Properties of DbCombo
			if (DbCreditDebitNo.Value.Trim() != DbCreditDebitNo.Text.Trim())
			{
				lblErrorMessage.Text="Please DON'T change the Credit/Debit No.";
				return;
			}

			if (DbInvoiceNo.Value.Trim() != DbInvoiceNo.Text.Trim())
			{
				lblErrorMessage.Text="Please DON'T change the Invoice No.";
				return;
			}

			if (dbCmbPayerID.Value.Trim() != dbCmbPayerID.Text.Trim())
			{
				lblErrorMessage.Text="Please DON'T change the Payer ID.";
				return;
			}

			if (dbCmbPayerName.Value.Trim() != dbCmbPayerName.Text.Trim())
			{
				lblErrorMessage.Text="Please DON'T change the Payer Name.";
				return;
			}

			lblErrorMessage.Text = "";

			GetValues();
			EnableDIVs (false,true,false,false);
			

		}

		private void GetValues()
		{
			//get the NOTE-type
			if (rbCreditNote.Checked==true)
				sSearchType="C";
			else if (rbDebitNote.Checked==true)
				sSearchType="D";			
			//Get the Credit/Debit No
			sCreditDebitNo=DbCreditDebitNo.Value.Trim();
			if (sCreditDebitNo.Trim()=="")
				sCreditDebitNo=DbCreditDebitNo.Text.Trim();
			//get the Invoice No if any
			sInvoiceNo=DbInvoiceNo.Value.Trim();
			if (sInvoiceNo.Trim()=="")
				sInvoiceNo=DbInvoiceNo.Text.Trim();
			//get the Consignment No if any
			if(txtConsignmentNo.Text.Trim() !="")
				sConsignmentNo=txtConsignmentNo.Text.Trim();
			else
				sConsignmentNo="";

			//Get the Date-Type
			if(rbCreationDate.Checked==true)
				sDateType="C";
			else if (rbInvoiceDate.Checked==true)
				sDateType="I";
			//get the Date-From and Date-To
			String strStartDate =null;
			if (rbMonth.Checked==true)
			{	
				if(ddMonth.SelectedItem.Value !="" && txtYear.Text.Trim() !="")
				{
					if(ddMonth.SelectedItem.Value != null && Convert.ToInt32(ddMonth.SelectedItem.Value) < 10)
						strStartDate="01"+"/0"+ddMonth.SelectedItem.Value+"/"+txtYear.Text.Trim();
					else
						strStartDate="01"+"/"+ddMonth.SelectedItem.Value+"/"+txtYear.Text.Trim();
					dtStartDate=System.DateTime.ParseExact(strStartDate,"dd/MM/yyyy",null);				
					dtEndDate=dtStartDate.AddMonths(1).AddDays(-1);				
				}
			}
			if (rbPeriod.Checked==true)
			{	
				if(txtPeriod.Text.Trim() !="" && txtTo.Text.Trim()!="")
				{
					dtStartDate=System.DateTime.ParseExact(txtPeriod.Text.Trim(),"dd/MM/yyyy",null);									
					dtEndDate=System.DateTime.ParseExact(txtTo.Text.Trim(),"dd/MM/yyyy",null);
				}
			}
			if (rbDate.Checked==true)
			{	
				if (txtDate.Text.Trim() !="")
				{
					dtStartDate=System.DateTime.ParseExact(txtDate.Text.Trim(),"dd/MM/yyyy",null);
					dtEndDate=dtStartDate.AddDays(1).AddMinutes(-1);				
				}
			}		

			//get the Payer-Type
			if (rbCustomer.Checked==true)
				sPayerType="C";
			else if(rbAgent.Checked==true)
				sPayerType="A";
			else if(rbBoth.Checked==true)
				sPayerType="B";
			//get the PayerID
			sPayerID=dbCmbPayerID.Value.Trim();
			if (sPayerID.Trim()=="")
				sPayerID=dbCmbPayerID.Text.Trim();
			//get the PayerName
			sPayerName=dbCmbPayerName.Value.Trim();
			if (sPayerID.Trim()=="")
				sPayerName=dbCmbPayerName.Text.Trim();

			DataSet dsCDList =GetValuesIntoDS();
			ShowCurrentList(dsCDList);

			ViewState["SearchType"]=sSearchType;			
		}

		private DataSet GetValuesIntoDS()
		{
			DataSet dsCDList=GetCDListDS();
			DataRow drCD = dsCDList.Tables[0].NewRow();

			if(sSearchType!="")
				drCD["Search_Type"]=sSearchType;
			if(sCreditDebitNo!="")
				drCD["Credit_Debit_No"]=sCreditDebitNo;
			if(sInvoiceNo!="")
				drCD["Invoice_No"]=sInvoiceNo;
			if(sConsignmentNo!="")
				drCD["Consignment_No"]=sConsignmentNo;
			if (sDateType!="")
				drCD["Date_Type"]=sDateType;
			if (dtStartDate.ToString()!="")
				drCD["From_Date"]=dtStartDate;
			if (dtEndDate.ToString()!="")
				drCD["To_Date"]=dtEndDate;
			if(sPayerType!="")
				drCD["Payer_Type"]=sPayerType;
			if(sPayerID!="")
				drCD["Payer_ID"]=sPayerID;
			if(sPayerName!="")
				drCD["Payer_Name"]=sPayerName;
			
			dsCDList.Tables[0].Rows.Add(drCD);				
			return dsCDList;

		}

		private DataSet GetCDListDS()
		{
			DataTable dtCDList = new DataTable(); 
			dtCDList.Columns.Add(new DataColumn("Search_Type", typeof(string)));
			dtCDList.Columns.Add(new DataColumn("Credit_Debit_No", typeof(string)));
			dtCDList.Columns.Add(new DataColumn("Invoice_No", typeof(string)));
			dtCDList.Columns.Add(new DataColumn("Consignment_No", typeof(string)));
			dtCDList.Columns.Add(new DataColumn("Date_Type", typeof(string)));
			dtCDList.Columns.Add(new DataColumn("From_Date", typeof(DateTime)));
			dtCDList.Columns.Add(new DataColumn("To_Date", typeof(DateTime)));
			dtCDList.Columns.Add(new DataColumn("Payer_Type", typeof(string)));
			dtCDList.Columns.Add(new DataColumn("Payer_ID", typeof(string)));
			dtCDList.Columns.Add(new DataColumn("Payer_Name", typeof(string)));

			DataSet dsCD = new DataSet();
			dsCD.Tables.Add(dtCDList);
			
			return  dsCD;				
		}

		
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultFirstScreen();
		}
		private void DefaultFirstScreen()
		{
			EnableDIVs(true,false,false,false);
			rbCreationDate.Checked=true;
			rbInvoiceDate.Checked=false;			
			rbMonth.Checked = true;rbPeriod.Checked=false;rbDate.Checked=false;
			ddMonth.Enabled = true;
			m_dvMonths = CreateMonths(true);
			BindMonths(m_dvMonths);
			txtYear.Text = "";txtPeriod.Text = "";txtTo.Text = "";txtDate.Text = "";
			txtYear.Enabled = true;txtPeriod.Enabled = false;txtTo.Enabled = false;txtDate.Enabled = false;

			DbInvoiceNo.Value=""; DbInvoiceNo.Text="";
			DbCreditDebitNo.Value=""; DbCreditDebitNo.Text="";			
			dbCmbPayerID.Value=""; dbCmbPayerID.Text="";
			dbCmbPayerName.Value=""; dbCmbPayerName.Text="";						
			txtConsignmentNo.Text="";
			lblErrorMessage.Text = "";
			ViewState["currentSet"] = 0;
			//	Database 
			m_sdsCDDetail = CreditDebitMgrDAL.GetEmptyCDDetailDS(1);
			Session["SESSION_DS4"] = m_sdsCDDetail;
			m_sdsCreditNote = CreditDebitMgrDAL.GetEmptyCreditNoteDS(1);
			m_sdsCreditDetail = CreditDebitMgrDAL.GetEmptyCreditDetailDS(1);
		}

		private void InsertMode()
		{			
			txtCreditNoteNo.ReadOnly=true;

			if (sSearchType=="")
				sSearchType=(String)ViewState["SearchType"];

			if (sSearchType=="C")
			{
				m_sdsCreditNote = CreditDebitMgrDAL.GetEmptyCreditNoteDS(1);
				m_sdsCreditDetail = CreditDebitMgrDAL.GetEmptyCreditDetailDS(1);
			}
			else if (sSearchType=="D")
			{
				m_sdsCreditNote = CreditDebitMgrDAL.GetEmptyDebitNoteDS(1);
				m_sdsCreditDetail = CreditDebitMgrDAL.GetEmptyDebitDetailDS(1);
			}
			Session["SESSION_DS2"] = m_sdsCreditNote;			
			Session["SESSION_DS3"] = m_sdsCreditDetail;
			
			ViewState["CreditNote"]="";
			ViewState["InvoiceNo"]="";
			ViewState["CNMode"] = ScreenMode.Insert;
			ViewState["CNOperation"] = Operation.Insert;
			ViewState["IsTextChanged"] = false;
			ViewState["currentPage"] = 0;
			
			DisplayCurrentPage();

			String strDt = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
			txtCreditNoteDate.Text=strDt;

			DbInvoiceNo.Enabled=true;
			//			btnExecQry.Enabled = false;
			btnInsertCons.Enabled=true;
			Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,true,m_moduleAccessRights);
			
			lblErrorMessage.Text = "";
		}

		private void QueryMode()
		{			
			ResetScreenForQuery();
			//			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,true,m_moduleAccessRights);
			txtCreditNoteNo.Enabled=true; txtCreditNoteNo.ReadOnly=false;
			txtCreditNoteDate.Enabled=true; txtCreditNoteDate.ReadOnly=false;
			txtInvoiceDate.Enabled=true; txtInvoiceDate.ReadOnly=false;
			DbInvoiceNo.Enabled=true;
			txtPayerID.Enabled=false; txtPayerID.ReadOnly=true;
			txtPayerName.Enabled=false; txtPayerName.ReadOnly=true;
			txtRemarks.Enabled=true;txtRemarks.ReadOnly=false;
		}

		private void ResetScreenForQuery()
		{			
			txtCreditNoteNo.Enabled=false;
			if (sSearchType=="")
				sSearchType=(String)ViewState["SearchType"];

			if (sSearchType=="C")
			{
				m_sdsCreditNote = CreditDebitMgrDAL.GetEmptyCreditNoteDS(1);
				m_sdsCreditDetail = CreditDebitMgrDAL.GetEmptyCreditDetailDS(1);
			}
			else if (sSearchType=="D")
			{
				m_sdsCreditNote = CreditDebitMgrDAL.GetEmptyDebitNoteDS(1);
				m_sdsCreditDetail = CreditDebitMgrDAL.GetEmptyDebitDetailDS(1);
			}
			Session["SESSION_DS2"] = m_sdsCreditNote;			
			Session["SESSION_DS3"] = m_sdsCreditDetail;
			
			ViewState["CreditNote"]="";
			ViewState["InvoiceNo"]="";
			//			ShowCurrConsPage();
			ViewState["CNMode"] = ScreenMode.Query;
			ViewState["CNOperation"] = Operation.None;
			ViewState["IsTextChanged"] = false;
			ViewState["currentPage"] = 0;
			//			btnExecQry.Enabled = true;					
			lblErrorMessage.Text = "";						
			DisplayCurrentPage();
			btnInsertCons.Enabled=false;
			//			btnCancel.Enabled=false;
			
		}
		private void EnableDIVs(bool mainVisible, bool listVisible, bool detailVisible, bool msgVisible )
		{
			mainPanel.Visible=mainVisible;
			listPanel.Visible=listVisible;
			detailPanel.Visible=detailVisible;
			msgPanel.Visible=msgVisible;
		}

		
		private DataView CreateMonths(bool showNilOption)
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
			ArrayList listMonthTxt = Utility.GetCodeValues(appID,culture, "months", CodeValueType.StringValue);
			if(showNilOption)
			{
				DataRow drNilRow = dtMonths.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtMonths.Rows.Add(drNilRow);
			}
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);
			return dvMonths;
		}

		private void BindMonths(DataView dvMonths)
		{
			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}


		private void rbBoth_CheckedChanged(object sender, System.EventArgs e)
		{			
			ClearPayerDBCombo();
			dbCmbPayerName.Enabled=false;
			dbCmbPayerID.Enabled=false;
			sPayerType="B";
		}

		private void rbAgent_CheckedChanged(object sender, System.EventArgs e)
		{			
			ClearPayerDBCombo();
			dbCmbPayerName.Enabled=true;
			dbCmbPayerID.Enabled=true;
			sPayerType="A";
		}

		private void rbCustomer_CheckedChanged(object sender, System.EventArgs e)
		{
			ClearPayerDBCombo();
			dbCmbPayerName.Enabled=true;
			dbCmbPayerID.Enabled=true;
			sPayerType="C";
		}

		private void rbCreditNote_CheckedChanged(object sender, System.EventArgs e)
		{
			ClearSearchDBCombo();
			lblCreditDebitNo.Text="Credit No";
			sSearchType="C";
		}

		private void rbDebitNote_CheckedChanged(object sender, System.EventArgs e)
		{
			ClearSearchDBCombo();
			lblCreditDebitNo.Text="Debit No";
			sSearchType="D";
		}

		private void rbCreationDate_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			sDateType="C";		//Creation Date
		}

		private void rbInvoiceDate_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			sDateType="I";		//Invoice Date
		}

		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			rbMonth.Checked=true;ddMonth.Enabled=true;txtYear.Enabled=true;
			rbPeriod.Checked=false;txtPeriod.Enabled=false;txtTo.Enabled=false;
			rbDate.Checked=false;txtDate.Enabled=false;
		}

		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			rbPeriod.Checked=true;txtPeriod.Enabled=true;txtTo.Enabled=true;
			rbMonth.Checked=false; ddMonth.Enabled=false;txtYear.Enabled=false;
			rbDate.Checked=false; txtDate.Enabled=false;
		}

		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			rbDate.Checked=true; txtDate.Enabled=true;
			rbMonth.Checked=false; ddMonth.Enabled=false;txtYear.Enabled=false;
			rbPeriod.Checked=false;txtPeriod.Enabled=false;txtTo.Enabled=false;
		}

		private void ClearPayerDBCombo()
		{
			lblErrorMessage.Text="";
			dbCmbPayerID.Text="";
			dbCmbPayerID.Value="";
			dbCmbPayerName.Text="";			
			dbCmbPayerName.Value="";
			SetPayerIDServerStates();
			SetPayerNameServerStates();
		}
		
		private void ClearSearchDBCombo()
		{
			lblErrorMessage.Text="";
			DbCreditDebitNo.Text="";
			DbCreditDebitNo.Value="";
			DbInvoiceNo.Text="";			
			DbInvoiceNo.Value="";
			SetCreditDebitServerStates();
			SetInvoiceNoServerStates();
		}


		private void ShowCurrentList(DataSet dsCDList)
		{	
			int iStartIndex = dgCreditDebitList.CurrentPageIndex * dgCreditDebitList.PageSize;
			m_sdsCDDetail = CreditDebitMgrDAL.GetCreditDebitList(appID,enterpriseID,iStartIndex,dgCreditDebitList.PageSize,dsCDList);
			ViewState["Query_DS"]=dsCDList;
			Session["SESSION_DS4"] = m_sdsCDDetail;
			if (m_sdsCDDetail.ds.Tables[0].Rows.Count<=0)
			{
				//lblErrorMessage.Text="No Records Found.";
				lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "NO_RECORDS_FOUND", utility.GetUserCulture());
			}
			decimal pgCnt = (m_sdsCDDetail.QueryResultMaxSize - 1)/dgCreditDebitList.PageSize;
			if(pgCnt < dgCreditDebitList.CurrentPageIndex)
			{
				dgCreditDebitList.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}		
			BindCDListGrid();		
		}

		private void BindCDListGrid()
		{
			dgCreditDebitList.VirtualItemCount = System.Convert.ToInt32(m_sdsCDDetail.QueryResultMaxSize);
			dgCreditDebitList.DataSource = m_sdsCDDetail.ds;
			dgCreditDebitList.DataBind();
			Session["SESSION_DS4"] = m_sdsCDDetail;
		}

		private void SetDetailsLabels()
		{
			if (sSearchType=="C")
			{
				lblCreditNoteNo.Text=Utility.GetLanguageText(ResourceType.ScreenLabel,"Credit No.",utility.GetUserCulture());
				lblCreditNoteDate.Text=Utility.GetLanguageText(ResourceType.ScreenLabel,"Credit Date",utility.GetUserCulture());
			}
			else if(sSearchType=="D")
			{
				lblCreditNoteNo.Text=Utility.GetLanguageText(ResourceType.ScreenLabel,"Debit No.",utility.GetUserCulture());
				lblCreditNoteDate.Text=Utility.GetLanguageText(ResourceType.ScreenLabel,"Debit Date",utility.GetUserCulture());
			}
		}


		private void DisplayCurrentPage()
		{
			txtTotalCreditAmt.Text="";
			DataRow drCurrent = m_sdsCreditNote.ds.Tables[0].Rows[(int)ViewState["currentPage"]];
			sSearchType=(String) ViewState["SearchType"];

		if (sSearchType=="C")
			{
				if(Utility.IsNotDBNull(drCurrent["credit_no"])==true && drCurrent["credit_no"].ToString() !="")
				{
					txtCreditNoteNo.Text=(String) drCurrent["credit_no"];
					ViewState["CreditNote"]=txtCreditNoteNo.Text.Trim();
				}
				else
					txtCreditNoteNo.Text="";

				if(Utility.IsNotDBNull(drCurrent["credit_date"])==true && drCurrent["credit_date"].ToString() !="")
				{
					DateTime dtCreditNoteDate=System.Convert.ToDateTime(drCurrent["credit_date"]);
					txtCreditNoteDate.Text= dtCreditNoteDate.ToString("dd/MM/yyyy HH:mm",null);
				}
				else
					txtCreditNoteDate.Text="";
			}

			if (sSearchType=="D")
			{
				if(Utility.IsNotDBNull(drCurrent["debit_no"])==true && drCurrent["debit_no"].ToString() !="")
				{
					txtCreditNoteNo.Text=(String) drCurrent["debit_no"];
					ViewState["CreditNote"]=txtCreditNoteNo.Text.Trim();
				}
				else
					txtCreditNoteNo.Text="";

				if(Utility.IsNotDBNull(drCurrent["debit_date"])==true && drCurrent["debit_date"].ToString() !="")
				{
					DateTime dtCreditNoteDate=System.Convert.ToDateTime(drCurrent["debit_date"]);
					txtCreditNoteDate.Text= dtCreditNoteDate.ToString("dd/MM/yyyy HH:mm",null);
				}
				else
					txtCreditNoteDate.Text="";
			}

			if(Utility.IsNotDBNull(drCurrent["Invoice_No"])==true && drCurrent["Invoice_No"].ToString() !="")
			{
				txtInvoiceNo.Text=(String) drCurrent["Invoice_No"].ToString();				
				ViewState["InvoiceNo"]=txtInvoiceNo.Text.Trim();
			}				
			else
				txtInvoiceNo.Text="";
			
			if (Utility.IsNotDBNull(drCurrent["Invoice_Date"])==true && drCurrent["Invoice_Date"].ToString() !="")
			{	
				DateTime dtInvoiceDate=System.Convert.ToDateTime(drCurrent["Invoice_Date"]);
				txtInvoiceDate.Text=dtInvoiceDate.ToString("dd/MM/yyyy HH:mm",null);			
			}
			else
				txtInvoiceDate.Text="";

			if (Utility.IsNotDBNull(drCurrent["PayerId"])==true && drCurrent["PayerId"].ToString() !="")
				txtPayerID.Text=(String)drCurrent["PayerId"];			
			else
				txtPayerID.Text="";

			if (Utility.IsNotDBNull(drCurrent["Payer_Name"])==true && drCurrent["Payer_Name"].ToString() !="")
				txtPayerName.Text=(String)drCurrent["Payer_Name"];
			else
				txtPayerName.Text="";

			if (Utility.IsNotDBNull(drCurrent["Remark"])==true && drCurrent["Remark"].ToString() !="")
				txtRemarks.Text=(String)drCurrent["Remark"];
			else
				txtRemarks.Text="";

			if (Utility.IsNotDBNull(drCurrent["Deleted"])==true && drCurrent["Deleted"].ToString() =="Y")
			{
				txtCreditNoteNo.Enabled=false;
				txtCreditNoteDate.Enabled=false;
				txtInvoiceNo.Enabled=false;
				txtInvoiceDate.Enabled=false;
				txtRemarks.Enabled=false;
				txtTotalCreditAmt.Enabled=false;
				txtPayerID.Enabled=false;
				txtPayerName.Enabled=false;
				btnInsertCons.Enabled=false;
				Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,false,m_moduleAccessRights);
			}	
			else
			{
				txtCreditNoteNo.ReadOnly=true;
				txtCreditNoteDate.Enabled=true;
				txtInvoiceNo.Enabled=false;
				txtInvoiceDate.ReadOnly=true;
				txtRemarks.Enabled=true;
				txtTotalCreditAmt.Enabled=true;
				txtPayerID.ReadOnly=true;
				txtPayerName.ReadOnly=true;
				btnInsertCons.Enabled=true;
				Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,true,m_moduleAccessRights);
			}

			SetDbComboServerStates();
			ShowCurrConsPage();
		}
		private void GetCNRecSet()
		{
			int iStartIndex = (int)ViewState["currentSet"] * m_iSetSize;
			
			if (sCreditDebitNo =="")
				sCreditDebitNo=(String)ViewState["CreditDebitNo"];
			if (sInvoiceNo =="")
				sInvoiceNo=(String)ViewState["InvoiceNo"];
			if (sSearchType=="")
				sSearchType=(String)ViewState["SearchType"];
			
			if (sSearchType=="C")
				m_sdsCreditNote.ds = CreditDebitMgrDAL.GetCreditNoteDS(appID, enterpriseID,sCreditDebitNo,sInvoiceNo);								
			else if (sSearchType=="D")
				m_sdsCreditNote.ds = CreditDebitMgrDAL.GetDebitNoteDS(appID, enterpriseID,sCreditDebitNo,sInvoiceNo);				

			Session["SESSION_DS2"] = m_sdsCreditNote;			
			decimal pgCnt = (m_sdsCreditNote.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < (int)ViewState["currentSet"])
			{
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
			}
			
		}
		
		
		public void dgCreditDebitList_Button(object source, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text="";
			int iSelIndex = e.Item.ItemIndex;
			if (iSelIndex< 0)
			{
				return;
			}
			DataGridItem dgRow = dgCreditDebitList.Items[iSelIndex];
			
			sCreditDebitNo=dgRow.Cells[2].Text.Trim();
			sInvoiceNo = dgRow.Cells[3].Text.Trim();
			sConsignmentNo = dgRow.Cells[4].Text.Trim();						
			Decimal dInvoiceAmt = System.Convert.ToDecimal(dgRow.Cells[5].Text.Trim());
			String sInvoiceDate = dgRow.Cells[6].Text.Trim();			
			
			//check if there is html blank-space??
			if(sCreditDebitNo=="&nbsp;")
				sCreditDebitNo="";
			if(sInvoiceNo == "&nbsp;")			
				sInvoiceNo = "";			
			if (sInvoiceDate =="&nbsp;")
				sInvoiceDate="";
			if (sConsignmentNo =="&nbsp;")
				sConsignmentNo="";			
			// check for Command, Clicked on "S" button or hyperlink
			String strCmdNm = e.CommandName;
			if (strCmdNm.Equals("Select"))
			{			
				ViewState["CreditDebitNo"]=sCreditDebitNo;
				ViewState["InvoiceNo"] = sInvoiceNo;
				ViewState["InvoiceDate"] = sInvoiceDate;
				ViewState["ConsignmentNo"] = sConsignmentNo;
				ViewState["InvoiceAmt"] = dInvoiceAmt;
				EnableDIVs(false,true,false,false);
				//				ShowCurrentConsginment();
				//				BindConsGrid();		
			}
			else if(strCmdNm.Equals("Choose"))
			{
				ViewState["CreditDebitNo"]=sCreditDebitNo;
				ViewState["InvoiceNo"] = sInvoiceNo;
				ViewState["InvoiceDate"] = sInvoiceDate;
				ViewState["ConsignmentNo"] = sConsignmentNo;
				ViewState["InvoiceAmt"] = dInvoiceAmt;	
				ResetScreenForQuery();
				GetCNRecSet();
				if (m_sdsCreditNote.ds.Tables[0].Rows.Count >0)
				{
					DisplayCurrentPage();
				}
				
				EnableDIVs(false,false,true,false);
				SetDetailsLabels();
			}

		}

		public void dgCreditDebitList_OnPaging(Object sender, DataGridPageChangedEventArgs e)
		{	
			DataSet dsCDList=(DataSet) ViewState["Query_DS"];
			dgCreditDebitList.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentList(dsCDList);
//			BindConsGrid();
		}
		protected void dgAssignedConsignment_Bound(object sender, DataGridItemEventArgs e)
		{
			EnableDIVs(false,false,true,false);
			if(e.Item.ItemIndex == -1)			
				return;					

			int iOperation = (int) ViewState["CNOperation"];
			DataRow drSelected = m_sdsCreditDetail.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)			
				return;			
			

			this.DbConsignmentNo = (Cambro.Web.DbCombo.DbCombo) e.Item.FindControl("DbConsignmentNo");										
			if(this.DbConsignmentNo != null && iOperation == (int)Operation.Update)
			{
				this.DbConsignmentNo.Enabled = false;	
			}			
			//find the Controls and Bind them
			Label lblCreditNoteDescription=(Label) e.Item.FindControl("lblCreditNoteDescription");
			TextBox txtCreditNoteDescription=(TextBox) e.Item.FindControl("txtCreditNoteDescription");
			Label lblCreditAmt=(Label) e.Item.FindControl("lblCreditAmt");
			msTextBox txtCreditAmt=(msTextBox) e.Item.FindControl("txtCreditAmt");
				
			//Decimal dAmt=0;			
			if (iOperation==4)
			{
				Decimal dCreditAmt=0;
				Decimal dTotCreditAmt=0;
				Decimal dCurrCreditAmt=0;		
				if (lblCreditAmt !=null && lblCreditAmt.Text.ToString() != "")
				{
					dCreditAmt=System.Convert.ToDecimal(lblCreditAmt.Text);
				}			
				if (txtTotalCreditAmt!= null && txtTotalCreditAmt.Text != "") 
				{
					dTotCreditAmt=System.Convert.ToDecimal(txtTotalCreditAmt.Text);
				}
				dCurrCreditAmt=dCreditAmt+dTotCreditAmt;
				txtTotalCreditAmt.Text=dCurrCreditAmt.ToString("#.00");
			}

			SetDbComboServerStates();

		}
		
		protected void dgAssignedConsignment_Edit(object sender, DataGridCommandEventArgs e)
		{			
			EnableDIVs(false,false,true,false);
			if( (int) ViewState["CreditDetailOperation"] == (int)Operation.Insert && dgAssignedConsignment.EditItemIndex > 0)
			{
				m_sdsCreditDetail.ds.Tables[0].Rows.RemoveAt(dgAssignedConsignment.EditItemIndex);						
			}
			
			dgAssignedConsignment.EditItemIndex = e.Item.ItemIndex;
			ViewState["CreditDetailOperation"] = Operation.Update;
			txtTotalCreditAmt.Text="";
			BindConsGrid();
			lblErrorMessage.Text = "";
			Logger.LogDebugInfo("CreditNote","CreditNote","dgAssignedConsignment_Edit","INF004","updating data grid...");					
//			EnableDIVs(false,false,true,false);
		}

		public void dgAssignedConsignment_Update(object sender, DataGridCommandEventArgs e)
		{
			EnableDIVs(false,false,true,false);
			lblErrorMessage.Text="";
			if (FillConsDataRow(e.Item,e.Item.ItemIndex)==false) 
			{
				return;
			}			
			//Data has changed in the GRID
			ViewState["IsTextChanged"]=true;
			
			Session["SESSION_DS3"] = m_sdsCreditDetail;
			int iOperation = (int)ViewState["CNOperation"];
			String strCreditNote = (String)ViewState["CreditNote"];
			String strInvoiceNo = (String)ViewState["InvoiceNo"];		
			btnInsertCons.Enabled = true;	
			dgAssignedConsignment.EditItemIndex=-1;	
			txtTotalCreditAmt.Text="";
			BindConsGrid();			
		}

		protected void dgAssignedConsignment_Cancel(object sender, DataGridCommandEventArgs e)
		{
			EnableDIVs(false,false,true,false);
			dgAssignedConsignment.EditItemIndex = -1;
			if((int)ViewState["CreditDetailOperation"] == (int)Operation.Insert)
			{
				m_sdsCreditDetail.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);			
			}
			ViewState["CreditDetailOperation"] = Operation.None;
			txtTotalCreditAmt.Text="";
			BindConsGrid();
			btnInsertCons.Enabled = true;
			lblErrorMessage.Text = "";
			Logger.LogDebugInfo("CreditNote","CreditNote","dgAssignedConsignment_Cancel","INF004","updating data grid...");			
		}

		protected void dgAssignedConsignment_Delete(object sender, DataGridCommandEventArgs e)
		{
			EnableDIVs(false,false,true,false);
			//Consignment Number
			String strConsignmentNo=null;
			this.DbConsignmentNo = (Cambro.Web.DbCombo.DbCombo)e.Item.FindControl("DbConsignmentNo");
			Label lblConsignmentNo = (Label)e.Item.FindControl("lblConsignmentNo");			
			if (this.DbConsignmentNo !=null)
				strConsignmentNo=this.DbConsignmentNo.Value;
			if (strConsignmentNo =="")
				strConsignmentNo=this.DbConsignmentNo.Text;
			if (lblConsignmentNo !=null)
				strConsignmentNo=lblConsignmentNo.Text.Trim();
			
			//Credit Amount
			Decimal dCreditAmount=0;
			msTextBox txtCreditAmt=(msTextBox) e.Item.FindControl("txtCreditAmt");
			Label lblCreditAmt=(Label) e.Item.FindControl("lblCreditAmt");
			if (txtCreditAmt !=null && txtCreditAmt.Text !=null)
				dCreditAmount=System.Convert.ToDecimal(txtCreditAmt.Text);
			else if(lblCreditAmt !=null && lblCreditAmt.Text !=null )
				dCreditAmount=System.Convert.ToDecimal(lblCreditAmt.Text);
			//Current CreditNo and InvoiceNo
			String strCreditNote = (String)ViewState["CreditNote"];		
			String strInvoiceNo = (String)ViewState["InvoiceNo"];
			try
			{
				if((int)ViewState["CreditDetailMode"] == (int)ScreenMode.Insert)
				{
					CreditDebitMgrDAL.DeleteCreditDetailDS(appID,enterpriseID,strInvoiceNo, strCreditNote,strConsignmentNo,dCreditAmount);
					m_sdsCreditDetail.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);						
				}
				else
				{
					if(dgAssignedConsignment.EditItemIndex == e.Item.ItemIndex)
						m_sdsCreditDetail.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
					else
					{
						CreditDebitMgrDAL.DeleteCreditDetailDS(appID,enterpriseID,strInvoiceNo, strCreditNote,strConsignmentNo,dCreditAmount);
						m_sdsCreditDetail.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);	
						if(dgAssignedConsignment.EditItemIndex > 0)
							m_sdsCreditDetail.ds.Tables[0].Rows.RemoveAt(dgAssignedConsignment.EditItemIndex-1);
					}
				}
				dgAssignedConsignment.EditItemIndex = -1;
				txtTotalCreditAmt.Text="";
				BindConsGrid();
				lblErrorMessage.Text = "Successfully deleted the Credit note detail.";
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1 || strMsg.IndexOf("child record") != -1)
				{
					lblErrorMessage.Text = "Error Deleting Credit Detail. Credit Detail being used in some transaction(s)";					
				}
				return;
			}				
			Logger.LogDebugInfo("CreditNote","CreditNote","dgAssignedConsignment_Delete","INF004","Deleted row in Credit_Detail table..");
			ViewState["CreditDetailOperation"] = Operation.None;
			btnInsertCons.Enabled = true;
			
		}

		private void dgAssignedConsignment_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			EnableDIVs(false,false,true,false);
			int iOperation=(int)ViewState["CNOperation"];
			if ( iOperation== (int)Operation.Insert)
			{
				ViewState["IsTextChanged"]=true;
			}
		}

	
		private void ShowCurrConsPage()
		{
			EnableDIVs(false,false,true,false);
			if (sSearchType=="")
			sSearchType=(String)ViewState["SearchType"];

			String strCreditNote = (String)ViewState["CreditNote"];
			String strInvoiceNo = (String)ViewState["InvoiceNo"];
			String strConsignmentNo=(String)ViewState["ConsignmnetNo"];
			if(sSearchType=="C")
				m_sdsCreditDetail.ds = CreditDebitMgrDAL.GetCreditDetailDS(appID,enterpriseID,strInvoiceNo, strCreditNote);
			else if (sSearchType=="D")
				m_sdsCreditDetail.ds = CreditDebitMgrDAL.GetDebitDetailDS(appID,enterpriseID,strInvoiceNo, strCreditNote);
			
			dgAssignedConsignment.EditItemIndex = -1;
			BindConsGrid();
			lblErrorMessage.Text = "";
		}
		
		private void BindConsGrid()
		{			
			dgAssignedConsignment.DataSource = m_sdsCreditDetail.ds;
			dgAssignedConsignment.DataBind();


			//dgAssignedConsignment
			Session["SESSION_DS3"] = m_sdsCreditDetail;	
			EnableDIVs(false,false,true,false);
		}

		private Boolean FillConsDataRow(DataGridItem item, int drIndex)
		{
			EnableDIVs(false,false,true,false);
			Boolean validated=false;

			String strConsignmentNo=null;
			DataRow drCurrent = m_sdsCreditDetail.ds.Tables[0].Rows[drIndex];
			this.DbConsignmentNo = (Cambro.Web.DbCombo.DbCombo)item.FindControl("DbConsignmentNo");	
			TextBox txtCreditNoteDescription = (TextBox) item.FindControl("txtCreditNoteDescription");
			TextBox txtRemark = (TextBox) item.FindControl("txtRemark");
			msTextBox txtCreditAmt=(msTextBox)item.FindControl("txtCreditAmt");
			TextBox txtInvoiceAmt =(TextBox) item.FindControl("txtInvoiceAmt");
			if (this.DbConsignmentNo == null)
			{//DbConsignment control is NOT CREATED/FOUND
				lblErrorMessage.Text="Consignment No NOT set. Please try again.";
				return validated;
			}			
			if (this.DbConsignmentNo.Value.ToString() == "")
			{//DbConsignment has NO Value/Null Value
				if (this.DbConsignmentNo.Text.ToString() =="")
				{
					lblErrorMessage.Text="Please select a Consignment No.";
					return validated;
				}
				else
				{//DbConsignment Control has Text property with SOME value
					this.DbConsignmentNo.Value=this.DbConsignmentNo.Text;
					strConsignmentNo=this.DbConsignmentNo.Text;
					validated=true;
				}				
			}
			else
			{//DbConsignmnet has SOME Value
				strConsignmentNo=this.DbConsignmentNo.Value;
				if(this.DbConsignmentNo.Text !="") 
				{	
					strConsignmentNo=this.DbConsignmentNo.Text;
					if (this.DbConsignmentNo.Value.ToString()!=  this.DbConsignmentNo.Text.ToString())
					{	//Text and Value properties have different values, because USER has changed
						lblErrorMessage.Text="Consignment No is invalid! Please DO NOT change the Consignment No.";
						return validated;
					}
				}	
			}
			
			//Now Assign the Values here........
			drCurrent["Consignment_No"]=strConsignmentNo;
			if(txtCreditNoteDescription != null && txtCreditNoteDescription.Text != "")
			{
				if (sSearchType=="C")
					drCurrent["Description"] = txtCreditNoteDescription.Text;
				else
					drCurrent["Description"] = txtCreditNoteDescription.Text;
			}
			else
			{
				if (sSearchType=="C")
					lblErrorMessage.Text="Credit Note Description is mandatory.";
				else
					lblErrorMessage.Text="Debit Note Description is mandatory.";

				validated=false;
				return validated;
			}
			if(txtInvoiceAmt != null && txtInvoiceAmt.Text !="")				
			{
				Decimal dAmt = System.Convert.ToDecimal(txtInvoiceAmt.Text);				
				drCurrent["Invoice_Amt"]=dAmt;
				validated=true;
			}
			if(txtCreditAmt != null && txtCreditAmt.Text !="")				
			{
				Decimal dAmt = System.Convert.ToDecimal(txtCreditAmt.Text);				
				drCurrent["Amt"]=dAmt;
				validated=true;
			}
			else
			{
				lblErrorMessage.Text="Credit Note amount is mandatory.";
				validated=false;
				return validated;
			}			

			if(txtRemark != null && txtRemark.Text !="")				
			{
				drCurrent["Remark"]=txtRemark.Text;
			}

			int iCounter=0;
			for (int i=0;i<m_sdsCreditDetail.ds.Tables[0].Rows.Count;i++)
			{//Check for duplication of Consignment No
				DataRow dr = m_sdsCreditDetail.ds.Tables[0].Rows[i];
				if(strConsignmentNo == dr["Consignment_No"].ToString())
					iCounter++;
			}
			if (iCounter > 1)
			{
				lblErrorMessage.Text="Duplicate Consignment No! Please select another Consignment No.";
				validated=false;
				return validated;
			}			
			else
			{//Consignment is found but UNIQUE				
				validated=true;
			}
			validated=true;
			return validated;
		}

		private void btnInsertCons_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			//ShowCurrConsPage();// Let this be...
			AddRowInConsignmentGrid();	
			dgAssignedConsignment.EditItemIndex = m_sdsCreditDetail.ds.Tables[0].Rows.Count - 1;			
			BindConsGrid();		
			btnInsertCons.Enabled=false;
			ViewState["CreditDetailOperation"] = Operation.Insert;
			EnableDIVs(false,false,true,false);
		}

		private void AddRowInConsignmentGrid()
		{
			lblErrorMessage.Text="";
			int i=m_sdsCreditDetail.ds.Tables[0].Rows.Count-1;
			DataRow drCurr=m_sdsCreditDetail.ds.Tables[0].Rows[i];
			
			if (Utility.IsNotDBNull(drCurr["Consignment_no"]) || drCurr["Consignment_no"].ToString()!="")
			{				
				String sConsignmentNo=(String) drCurr["Consignment_no"];
				if (sConsignmentNo=="")
				{
					return;
				}
			}
			CreditDebitMgrDAL.AddNewRowInConsDS(m_sdsCreditDetail.ds);
			Session["SESSION_DS3"] = m_sdsCreditDetail;
			EnableDIVs(false,false,true,false);
		}

				
		private void btnListBack_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			EnableDIVs(true,false,false,false);
		}

		private void Back_Click()
		{			
			if ((bool)ViewState["IsTextChanged"]==true)
			{
				ViewState["CNOperation"]=Operation.None;
				EnableDIVs(false,false,false,true);
				lblMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				ViewState["NextOperation"]="Back";
			}
			else
			{
				EnableDIVs(false,true,false,false);
			}
		}
		private void btnDetailBack_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";			
			Back_Click();
		}

		private void txtRemarks_TextChanged(object sender, System.EventArgs e)
		{
			EnableDIVs(false,false,true,false);
			int iOperation=(int)ViewState["CNOperation"];
			if ( iOperation== (int)Operation.Insert)
			{
				ViewState["IsTextChanged"]=true;
			}
		}

		private void btnYes_Click(object sender, System.EventArgs e)
		{
			ViewState["IsTextChanged"] = false;
			EnableDIVs(false,false,true,false);			
			if ((bool)ViewState["IsTextChanged"]==true && (int)ViewState["CNOperation"]==(int)Operation.Insert)
				InsertMode();			
			else if((bool)ViewState["IsTextChanged"]==true && (int)ViewState["CNOperation"]==(int)Operation.None)
				QueryMode();
			else
				QueryMode();
		}

		private void btnNO_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			EnableDIVs(false,false,true,false);			
		}

		private void Query_Click()
		{
			if ((bool)ViewState["IsTextChanged"]==true)
			{
				ViewState["CNOperation"]=Operation.None;
				lblMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				ViewState["NextOperation"]="Query";
				EnableDIVs(false,false,false,true);				
				return;
			}			
			else
			{
				QueryMode();
				EnableDIVs(true,false,false,false);					
			}
		}

		private void btnQuery1_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";	
			Query_Click();						
		}

		private void FillCreditNote(int iCurrentRow)
		{
			DataRow drCurrent = m_sdsCreditNote.ds.Tables[0].Rows[iCurrentRow];
			if (sSearchType=="C")
			{
				if(txtCreditNoteNo.Text != null && txtCreditNoteNo.Text.Trim() != "")			
					drCurrent["credit_no"] = txtCreditNoteNo.Text.Trim();			
				if(txtCreditNoteDate.Text != null && txtCreditNoteDate.Text.Trim() != "")
				{
					drCurrent["credit_date"] = System.DateTime.ParseExact(txtCreditNoteDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
				}
			}
			else if (sSearchType=="D")
			{
				if(txtCreditNoteNo.Text != null && txtCreditNoteNo.Text.Trim() != "")			
					drCurrent["Debit_no"] = txtCreditNoteNo.Text.Trim();			
				if(txtCreditNoteDate.Text != null && txtCreditNoteDate.Text.Trim() != "")
				{
					drCurrent["Debit_date"] = System.DateTime.ParseExact(txtCreditNoteDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
				}
			}

			if(DbInvoiceNo.Value != null && DbInvoiceNo.Value.Trim() != "")
			{
				drCurrent["invoice_no"] = DbInvoiceNo.Value.Trim();
			}
			if(txtRemarks.Text != null && txtRemarks.Text.Trim() != "")			
				drCurrent["remark"] = txtRemarks.Text.Trim();							
		}

		private bool SaveUpdateRecord()
		{
			bool isError=false;

			FillCreditNote(0);
			//Credit Note Date Validation
			DateTime dtCreditdate=DateTime.Now;
			DateTime dtInvoicedate=DateTime.Now;
			if(txtCreditNoteDate.Text != null && txtCreditNoteDate.Text.Trim() != "")
			{
				dtCreditdate = System.DateTime.ParseExact(txtCreditNoteDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			}
			if(txtInvoiceDate.Text != null && txtInvoiceDate.Text.Trim() != "")
			{
				dtInvoicedate = System.DateTime.ParseExact(txtInvoiceDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			}
			if (txtRemarks.Text.Length > 200)
			{
				lblErrorMessage.Text="Remarks cannot exceed 200 Characters.";
				return isError;
			}
			if (dtCreditdate < dtInvoicedate)
			{
				lblErrorMessage.Text="Credit Note Date cannot be earlier than Invoice Date.";
				return isError;
			}

			if (m_sdsCreditNote.ds.Tables[0].Rows.Count==0)
			{
				lblErrorMessage.Text="Please add at least a Credit Note detail.";
				return isError;
			}

			int iOperation = (int)ViewState["CNOperation"];
			try
			{
				if (sSearchType=="C")
					CreditDebitMgrDAL.ModifyCreditNoteDS(appID,enterpriseID,m_sdsCreditNote.ds, m_sdsCreditDetail.ds);
				else
					CreditDebitMgrDAL.ModifyDebitNoteDS(appID,enterpriseID,m_sdsCreditNote.ds, m_sdsCreditDetail.ds);

				ViewState["IsTextChanged"] = false;
				ChangeCNState();
				m_sdsCreditNote.ds.Tables[0].Rows[(int)ViewState["currentPage"]].AcceptChanges();						
				if (sSearchType=="C")
					lblErrorMessage.Text = "Credit Note modified successfully.";
				else
					lblErrorMessage.Text = "Debit Note modified successfully.";

				Logger.LogDebugInfo("DEBUG MODE : CreditNote","btnSave_Click","INF004","save in modify mode..");
				Logger.LogDebugInfo("module2","DEBUG MODE :(module2 only) CreditNote","btnSave_Click","INF004","save in modify mode...");

			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				strMsg = appException.InnerException.Message;												
						
				if(strMsg.IndexOf("FK") != -1  || strMsg.IndexOf("child record") != -1)
				{
					if(strMsg.IndexOf("duplicate key") != -1 )
						lblErrorMessage.Text = "Duplicate Consignment No not allowed!";
					else if (strMsg.IndexOf("conflict") != -1 ) 
						lblErrorMessage.Text = "Consignment No is not valid! Duplicate Consignment No not allowed";
					else
					{
						lblErrorMessage.Text = strMsg;
					}
				}
				else
				{
					lblErrorMessage.Text = strMsg;
				}						
						
				Logger.LogTraceError("TRACE MODE : Credit Note","btnSave_Click","ERR004","Error updating record :--- "+strMsg);
				Logger.LogTraceError("module2","TRACE MODE : (module2 only)Credit Note","btnSave_Click","ERR004","Error updating record : "+strMsg);
				isError=true;
				return isError;						
			}
			btnInsertCons.Enabled=true;
			Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete,true,m_moduleAccessRights);
			DbInvoiceNo.Enabled=false;//after saving the Record, INVOICE must NOT be changed
			ViewState["CNOperation"]=Operation.None;
			return isError;

		}
		private void btnSave_Click(object sender, System.EventArgs e)
		{
			//Save the record and return the status
			bool isError = SaveUpdateRecord();
			if (isError==true)
			{
				return;
			}
		}
		
		private void ChangeCNState()
		{
			if((int)ViewState["CNMode"] == (int) ScreenMode.Insert && (int)ViewState["CNOperation"] == (int)Operation.None)
			{
				ViewState["CNOperation"] = Operation.Insert;
			}
			else if((int)ViewState["CNMode"] == (int) ScreenMode.Insert && (int)ViewState["CNOperation"] == (int)Operation.Insert)
			{
				ViewState["CNOperation"] = Operation.Saved;
			}
			else if((int)ViewState["CNMode"] == (int) ScreenMode.Insert && (int)ViewState["CNOperation"] == (int)Operation.Saved)
			{
				ViewState["CNOperation"] = Operation.Update;
			}
			else if((int)ViewState["CNMode"] == (int) ScreenMode.Insert && (int)ViewState["CNOperation"] == (int)Operation.Update)
			{
				ViewState["CNOperation"] = Operation.Saved;
			}
			else if((int)ViewState["CNMode"] == (int) ScreenMode.ExecuteQuery && (int)ViewState["CNOperation"] == (int)Operation.None)
			{
				ViewState["CNOperation"] = Operation.Update;
			}
			else if((int)ViewState["CNMode"] == (int) ScreenMode.ExecuteQuery && (int)ViewState["CNOperation"] == (int)Operation.Update)
			{
				ViewState["CNOperation"] = Operation.None;
			}
			
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			if (sSearchType=="")
				sSearchType=(String) ViewState["SearchType"];
			if (sInvoiceNo =="")
				sInvoiceNo=(String)ViewState["InvoiceNo"];
			if(sCreditDebitNo=="")
				sCreditDebitNo=(String)ViewState["CreditNote"];

			if (sInvoiceNo=="" || sCreditDebitNo == "")
			{
				lblErrorMessage.Text="Credit Note No/Invoice No cannot be null";
				return;
			}
			
			if (sSearchType=="C")
			{
				CreditDebitMgrDAL.DeleteCreditNoteDS(appID,enterpriseID,sInvoiceNo, sCreditDebitNo);
				lblErrorMessage.Text="Successfully deleted the Credit Note and its details.";
			}
			else
			{
				CreditDebitMgrDAL.DeleteDebitNoteDS(appID,enterpriseID,sInvoiceNo, sCreditDebitNo);
				lblErrorMessage.Text="Successfully deleted the Debit Note and its details.";
			}
			
			//Disable All Controls, Clear the DataGrid immediately after deleting
			txtCreditNoteNo.Enabled=false;txtCreditNoteDate.Enabled=false;
			txtInvoiceNo.Enabled=false;	txtInvoiceDate.Enabled=false;
			txtRemarks.Enabled=false;txtTotalCreditAmt.Enabled=false;
			txtPayerID.Enabled=false; txtPayerName.Enabled=false;
			btnInsertCons.Enabled=false;
			Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,false,m_moduleAccessRights);
			//Clear the dgAssignedConsignment DataGrid
			m_sdsCreditDetail.ds.Tables[0].Rows.Clear();
			txtTotalCreditAmt.Text="";
			BindConsGrid();
		}

		private void btnPrint_Click(object sender, System.EventArgs e)
		{
			if (sCreditDebitNo=="")
				sCreditDebitNo=(String)ViewState["CreditNote"];
			if (sSearchType=="")
				sSearchType=(String)ViewState["SearchType"];
			
			if (sSearchType=="C")
			{
				Session["FORMID"]="CreditTracking";
				//Session["SESSION_DS1"]=sCreditDebitNo;
				SetParamDataSetCN(sCreditDebitNo);
			}
			else if(sSearchType=="D")
			{
				Session["FORMID"]="DebitTracking";
				//Session["SESSION_DS1"]=sCreditDebitNo;
				SetParamDataSetDN(sCreditDebitNo);
			}
			
			String sUrl = "ReportViewer.aspx";
			
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindowReport.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

		}

		private void SetParamDataSetCN(string sCreditDebitNo)
		{
			// add new dt for report coz , this form used 'Session["SESSION_DS1"]'
			DataTable dt = new DataTable();
			dt.TableName = "DT_CNNO_Selected";
			dt.Columns.Add("CN_NO",typeof(string));

			DataRow dr = dt.NewRow();
			dr["CN_NO"] = sCreditDebitNo;
			dt.Rows.Add(dr);
			dt.AcceptChanges();

			//Session["SESSION_DS1"] = this.txtCreditNoteNo.Text.Trim();//this.txtDNtest.Text.Trim();
			SessionDS sds = (SessionDS)Session["SESSION_DS1"];
			DataSet ds = sds.ds;

			if (ds.Tables["DT_CNNO_Selected"] != null)
			{
				ds.Tables.Remove("DT_CNNO_Selected");
				ds.AcceptChanges();
				ds.Tables.Add(dt);
			}
			else
				ds.Tables.Add(dt);

			ds.AcceptChanges();
			m_sdsCreditNote.ds = ds;
			Session["SESSION_DS1"] = m_sdsCreditNote;
		}



		private void SetParamDataSetDN(string sCreditDebitNo)
		{
			// add new dt for report coz , this form used 'Session["SESSION_DS1"]'
			DataTable dt = new DataTable();
			dt.TableName = "DT_DNNO_Selected";
			dt.Columns.Add("DN_NO",typeof(string));

			DataRow dr = dt.NewRow();
			dr["DN_NO"] = sCreditDebitNo;
			dt.Rows.Add(dr);
			dt.AcceptChanges();

			//Session["SESSION_DS1"] = this.txtDebitNoteNo.Text.Trim();//this.txtDNtest.Text.Trim();
			SessionDS sds = (SessionDS)Session["SESSION_DS1"];
			DataSet ds = sds.ds;

			if (ds.Tables["DT_DNNO_Selected"] != null)
			{
				ds.Tables.Remove("DT_DNNO_Selected");
				ds.AcceptChanges();
				ds.Tables.Add(dt);
			}
			else
				ds.Tables.Add(dt);

			ds.AcceptChanges();
			m_sdsCreditNote.ds = ds;
			Session["SESSION_DS1"] = m_sdsCreditNote;
		}


		private void txtCreditNoteDate_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["IsTextChanged"]==false)
			{
				ViewState["IsTextChanged"]=true;
			}
		}

		private void txtRemarks_TextChanged_1(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["IsTextChanged"]==false)
			{
				ViewState["IsTextChanged"]=true;
			}
		}

		private void btnToSaveChanges_Click(object sender, System.EventArgs e)
		{
			ViewState["IsTextChanged"] = false;						
			bool isError = SaveUpdateRecord();		
			if(isError == false)
			{
				if((String)ViewState["NextOperation"] == "Back")
					Back_Click();
				else if((String)ViewState["NextOperation"] == "Query")
					Query_Click();
			}
			
		}

		private void btnNotToSave_Click(object sender, System.EventArgs e)
		{
			if((String)ViewState["NextOperation"] == "Back")
				Back_Click();
			else if((String)ViewState["NextOperation"] == "Query")
				Query_Click();		
		}
 
	}
}
