<%@ Page language="c#" Codebehind="RevenueByStateReport.aspx.cs" AutoEventWireup="false" Inherits="com.ties.RevenueByStateReport" EnableEventValidation="False" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>RevenueByStateReport</title>
		<meta content="False" name="vs_showGrid">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript">
			function makeUppercase(objId)
			{
				document.getElementById(objId).value = document.getElementById(objId).value.toUpperCase();
			}
		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout" XMLNS:DbCombo="http://schemas.cambro.net/dbcombo">
		<form id="RevenueByStateReport" method="post" runat="server">
			<asp:label id="lblTitle" style="Z-INDEX: 102; POSITION: relative; TOP: -1px; LEFT: 10px" runat="server"
				CssClass="mainTitleSize" Width="477px" Height="26px">Revenue by State Report</asp:label><asp:label id="lblErrorMessage" style="Z-INDEX: 103; POSITION: absolute; TOP: 72px; LEFT: 18px"
				runat="server" Width="500px" Height="14px" ForeColor="Red"></asp:label><asp:button id="btnQuery" style="Z-INDEX: 105; POSITION: absolute; TOP: 40px; LEFT: 16px" runat="server"
				CssClass="queryButton" Width="64px" Text="Query"></asp:button><asp:button id="btnGenerate" style="Z-INDEX: 106; POSITION: absolute; TOP: 40px; LEFT: 81px"
				runat="server" CssClass="queryButton" Width="88px" Text="Generate"></asp:button>
			<div id="divCustomer" style="Z-INDEX: 101; POSITION: absolute; WIDTH: 723px; HEIGHT: 421px; TOP: 100px; LEFT: 16px"
				MS_POSITIONING="GridLayout" runat="server">
				<TABLE id="tblCustAgent" width="792" align="left" border="0" runat="server" style="WIDTH: 792px; HEIGHT: 370px">
					<TR>
						<TD style="WIDTH: 450px; HEIGHT: 137px" vAlign="top">
							<fieldset style="WIDTH: 439px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="80px" Font-Bold="True">Booking Date</asp:label></legend>
								<TABLE id="tblDates" style="WIDTH: 360px; HEIGHT: 91px" cellSpacing="0" cellPadding="0"
									align="left" border="0" runat="server">
									<TR>
										<td style="HEIGHT: 18px"></td>
										<TD style="HEIGHT: 18px" colSpan="3">&nbsp;
											<asp:radiobutton id="rbMonth" runat="server" CssClass="tableRadioButton" Width="73px" Height="21px"
												Text="Month" AutoPostBack="True" GroupName="EnterDate" Checked="True"></asp:radiobutton>&nbsp;
											<asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Width="88px" Height="19px"></asp:dropdownlist>&nbsp;
											<asp:label id="lblYear" runat="server" CssClass="tableLabel" Width="30px" Height="22px">Year</asp:label>&nbsp;
											<cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" MaxLength="5" TextMaskType="msNumeric"
												NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
										<td style="HEIGHT: 18px"></td>
									</TR>
									<TR>
										<td style="HEIGHT: 14px"></td>
										<TD style="HEIGHT: 14px" colSpan="3">&nbsp;
											<asp:radiobutton id="rbPeriod" runat="server" CssClass="tableRadioButton" Width="62px" Text="Period"
												AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
											<cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
												TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
											&nbsp;
											<cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" MaxLength="10" TextMaskType="msDate"
												TextMaskString="99/99/9999"></cc1:mstextbox></TD>
										<td style="HEIGHT: 14px"></td>
									</TR>
									<TR>
										<td style="HEIGHT: 23px"></td>
										<TD style="HEIGHT: 23px" colSpan="3">&nbsp;
											<asp:radiobutton id="rbDate" runat="server" CssClass="tableRadioButton" Width="74px" Height="22px"
												Text="Date" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
												TextMaskString="99/99/9999"></cc1:mstextbox></TD>
										<td style="HEIGHT: 23px"></td>
									</TR>
								</TABLE>
							</fieldset>
						</TD>
						<TD style="HEIGHT: 137px" vAlign="top">
							<fieldset style="WIDTH: 320px; HEIGHT: 105px"><legend><asp:label id="lblPayerType" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Payer Type</asp:label></legend>
								<TABLE id="tblPayerType" style="WIDTH: 300px; HEIGHT: 73px" cellSpacing="0" cellPadding="0"
									align="left" border="0" runat="server">
									<tr>
										<td></td>
										<td colSpan="2" height="1">&nbsp;</td>
										<td></td>
										<td></td>
									</tr>
									<TR>
										<td></td>
										<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
											<asp:label id="Label1" runat="server" CssClass="tableLabel" Width="90px" Height="22px">Payer Type</asp:label></TD>
										<td><asp:listbox id="lsbCustType" runat="server" Width="137px" Height="61px" SelectionMode="Multiple"></asp:listbox></td>
										<td></td>
									</TR>
									<TR height="33">
										<td bgColor="blue"></td>
										<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
											<asp:label id="lblPayerCode" runat="server" CssClass="tableLabel" Width="79px" Height="22px">Payer Code</asp:label>&nbsp;&nbsp;</TD>
										<td><asp:TextBox id="txtPayerCode" runat="server" Width="148px" CssClass="textField"></asp:TextBox><FONT face="Tahoma">&nbsp;&nbsp;</FONT><asp:button id="btnPayerCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></td>
										<td></td>
									</TR>
								</TABLE>
							</fieldset>
						</TD>
					</TR>
					<TR>
						<TD style="WIDTH: 450px; HEIGHT: 134px" vAlign="top">
							<fieldset style="WIDTH: 438px; HEIGHT: 127px"><legend><asp:label id="lblRouteType" runat="server" CssClass="tableHeadingFieldset" Width="145px" Font-Bold="True">Route / DC Selection</asp:label></legend>
								<TABLE id="tblRouteType" style="WIDTH: 431px; HEIGHT: 113px" cellSpacing="0" cellPadding="0"
									align="left" border="0" runat="server">
									<TR>
										<TD class="tableLabel" colSpan="2">&nbsp;
											<asp:radiobutton id="rbLongRoute" runat="server" CssClass="tableRadioButton" Width="131px" Height="22px"
												Text="Linehaul" AutoPostBack="True" GroupName="QueryByRoute" Checked="True"></asp:radiobutton><asp:radiobutton id="rbShortRoute" runat="server" CssClass="tableRadioButton" Width="131px" Height="22px"
												Text="Delivery Route" AutoPostBack="True" GroupName="QueryByRoute"></asp:radiobutton><asp:radiobutton id="rbAirRoute" runat="server" CssClass="tableRadioButton" Width="151px" Height="22px"
												Text="Air Route" AutoPostBack="True" GroupName="QueryByRoute"></asp:radiobutton></TD>
									</TR>
									<tr>
										<TD class="tableLabel" width="20%">&nbsp;<asp:label id="lblRouteCode" runat="server" CssClass="tableLabel" Width="111px" Height="22px">Route Code</asp:label></TD>
										<td><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
												RegistrationKey=" " ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="DbComboPathCodeSelect"
												TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></td>
									</tr>
									<TR>
										<TD class="tableLabel">&nbsp;<asp:label id="Label2" runat="server" CssClass="tableLabel" Width="154px" Height="22px">Origin Distribution Center</asp:label></TD>
										<TD><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
												RegistrationKey=" " ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="DbComboDistributionCenterSelect"
												TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></TD>
									</TR>
									<TR>
										<TD class="tableLabel">&nbsp;<asp:label id="Label3" runat="server" CssClass="tableLabel" Width="181px" Height="22px">Destination Distribution Center</asp:label></TD>
										<TD><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
												RegistrationKey=" " ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="DbComboDistributionCenterSelect"
												TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></TD>
									</TR>
								</TABLE>
							</fieldset>
						</TD>
						<TD style="HEIGHT: 134px" vAlign="top">
							<fieldset style="WIDTH: 320px; HEIGHT: 89px"><legend><asp:label id="lblDestination" runat="server" CssClass="tableHeadingFieldset" Width="79px"
										Font-Bold="True">Destination</asp:label></legend>
								<TABLE id="tblDestination" style="WIDTH: 300px; HEIGHT: 89px" cellSpacing="0" cellPadding="0"
									align="left" border="0" runat="server">
									<tr height="37">
										<td></td>
										<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
											<asp:label id="lblZipCode" runat="server" CssClass="tableLabel" Width="84px" Height="22px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
											<cc1:mstextbox id="txtZipCode" runat="server" CssClass="textField" Width="139px" MaxLength="10"
												TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
											<asp:button id="btnZipCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
											<asp:label id="lblStateCode" runat="server" CssClass="tableLabel" Width="100px" Height="22px">State / Province</asp:label><cc1:mstextbox id="txtStateCode" runat="server" CssClass="textField" Width="139" Height="22" MaxLength="10"
												TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
											<asp:button id="btnStateCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
										<td></td>
									</tr>
								</TABLE>
							</fieldset>
						</TD>
					</TR>
					<tr id="SendRecpID" runat="server">
						<td colSpan="2">
							<FIELDSET style="WIDTH: 648px"><LEGEND><asp:label id="Label4" CssClass="tableHeadingFieldset" Runat="server">Sender/Recipient</asp:label></LEGEND>
								<TABLE id="tblSenRecpPayer" style="WIDTH: 99.34%" runat="server">
									<TR id="Tr1" runat="server">
										<TD><asp:radiobutton id="rbSender" runat="server" CssClass="tableRadioButton" Text="Sender" AutoPostBack="True"
												GroupName="QueryByType" Checked="True"></asp:radiobutton></TD>
										<TD><asp:radiobutton id="rbRecipient" runat="server" CssClass="tableRadioButton" Width="95px" Height="22px"
												Text="Recipient" AutoPostBack="True" GroupName="QueryByType"></asp:radiobutton></TD>
										<td colSpan="2">&nbsp;</td>
									</TR>
									<TR height="27">
										<TD><asp:label id="lblSenderName" runat="server" CssClass="tableLabel" Width="79px" Height="22px">Sender Name</asp:label></TD>
										<TD><dbcombo:dbcombo id="dbCmbSenderName" tabIndex="13" runat="server" Width="100px" Height="17px" AutoPostBack="True"
												ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="SenderNameServerMethod" TextBoxColumns="6"></dbcombo:dbcombo></TD>
										<TD><asp:label id="lblRecpname" runat="server" CssClass="tableLabel" Width="89px" Height="22px">Recipient Name</asp:label></TD>
										<TD><dbcombo:dbcombo id="dbCmbRecpName" tabIndex="13" runat="server" Width="100px" Height="17px" AutoPostBack="True"
												ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="RecpNameServerMethod" TextBoxColumns="6"></dbcombo:dbcombo></TD>
									</TR>
								</TABLE>
							</FIELDSET>
						</td>
					</tr>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
