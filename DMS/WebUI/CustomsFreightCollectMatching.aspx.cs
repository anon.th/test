using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.applicationpages;
using TIESClasses;
using TIESDAL;
using com.ties;
using com.ties.DAL;
using com.ties.classes;
using com.common.util;
using System.Text;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CustomsFreightCollectMatching.
	/// </summary>
	public class CustomsFreightCollectMatching : BasePage
	{
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnGenerateMatchingReport;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox txtYear;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.DropDownList ddlWeek;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.DropDownList ddlShipper;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.WebControls.Button btnBrowse;
		protected System.Web.UI.WebControls.Button btnImport;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.DataGrid GrdAWBDetails;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;

		Utility Utility = null;
		String strEnterpriseID = null;
		String strAppID = null;
		protected System.Web.UI.WebControls.TextBox txtFilePath;
		protected System.Web.UI.HtmlControls.HtmlInputFile inFile;
		String strUserLoggin = null;
	
		public string currency_decimal
		{
			get
			{
				if(ViewState["currency_decimal"] == null)
				{
					return "00";
				}
				else
				{
					return (string)ViewState["currency_decimal"];
				}
			}
			set
			{
				ViewState["currency_decimal"]=value;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			Utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = Utility.GetAppID();
			strEnterpriseID = Utility.GetEnterpriseID();
			strUserLoggin = Utility.GetUserID();

			if(!IsPostBack)
			{
				currency_decimal =  this.GetCurrencyDigitsOfEnterpriseConfiguration();	
				BindControl();
				btnImport.Attributes.Add("onclick","return ImportFile();");
				btnBrowse.Attributes.Add("OnClick", "getFile()");
			}

			this.btnImport.Attributes.Add("OnClick", "disableImport();" + GetPostBackEventReference(this.btnImport)+";");
			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnGenerateMatchingReport.Click += new System.EventHandler(this.btnGenerateMatchingReport_Click);
			this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
			this.GrdAWBDetails.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.GrdAWBDetails_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void BindControl()
		{
			generateValueToWeekDropdownlist();
			getnerateValueToShipperDropdownlist();
			GrdAWBDetails.DataBind();
			SetInitialFocus(txtYear);
		}

		public void generateValueToWeekDropdownlist()
		{
			ddlWeek.Items.Add(new ListItem("", ""));

			for(int i = 1; i <= 53; i++)
			{
				if(i >= 1 && i <= 9)
				{
					ddlWeek.Items.Add(new ListItem("0" + i.ToString(), "0" + i.ToString()));
				}
				else
				{
					ddlWeek.Items.Add(new ListItem(i.ToString(), i.ToString()));
				}
			}
		}

		public void getnerateValueToShipperDropdownlist()
		{
			ddlShipper.Items.Add(new ListItem("TNT", "TNT"));
		}

		private void btnImport_Click(object sender, System.EventArgs e)
		{
			try
			{
				string year = txtYear.Text;
				string week = ddlWeek.SelectedItem.Value;
				string shipper = ddlShipper.SelectedItem.Value;
				string fileName = txtFilePath.Text;
			
				if(year.Equals(string.Empty))
				{
					lblError.Text = "Year is required.";
					btnImport.Enabled = false;
					btnBrowse.Enabled=true;
					SetInitialFocus(txtYear);
				}
				else if(week.Equals(string.Empty))
				{
					lblError.Text = "Week number is required.";
					btnImport.Enabled = false;
					btnBrowse.Enabled=true;
					SetInitialFocus(ddlWeek);
				}
				else
				{
					string pathFileImport = uploadFiletoServer();
					if(pathFileImport != "")
					{			
						string textString="";
						StringBuilder sbTxt = new StringBuilder();
						using(System.IO.StreamReader file = new System.IO.StreamReader(pathFileImport))
						{
							string line;
							
							while((line = file.ReadLine()) != null)
							{			
								if(textString.Length>0)
								{
									textString+="\r\n"+line;
								}
								else
								{
									textString+=line;
								}
								
							}
						}
						if(textString.Length>0)
						{
							textString+="\r\n";
						}
						DataSet ds = CustomsFreightCollectMatchingDAL.Customs_Import_FCInvoice3(strAppID, strEnterpriseID, strUserLoggin, shipper, year, week, null,1,textString);
						lblError.Text = ds.Tables[0].Rows[0]["ErrorMessage"].ToString();

						if(ds.Tables[0].Rows[0]["ErrorCode"].ToString().Equals("0"))
						{
							ds.Tables[1].Columns["No. of Cons"].ColumnName="NoOfCons";
							ds.Tables[1].Columns["No. of Unique Cons"].ColumnName="NoOfUniqueCons";
							ds.Tables[1].AcceptChanges();
							GrdAWBDetails.DataSource = ds.Tables[1];
							GrdAWBDetails.DataBind();
						}

						txtFilePath.Text="";
					}
				}
				btnQry.Enabled=true;
				btnExecQry.Enabled=true;
			}
			catch(Exception ex)
			{
				txtFilePath.Text = "";
				lblError.Text=ex.Message.ToString();
			}

		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			lblError.Text="";
			txtFilePath.Text="";
			string year = txtYear.Text;
			string week = ddlWeek.SelectedItem.Value;
			string shipper = ddlShipper.SelectedItem.Value;

			DataSet ds = CustomsFreightCollectMatchingDAL.Customs_Query_FCInvoices(strAppID, strEnterpriseID, strUserLoggin, shipper, year, week, "1");
			ds.Tables[0].Columns["No. of Cons"].ColumnName="NoOfCons";
			ds.Tables[0].Columns["No. of Unique Cons"].ColumnName="NoOfUniqueCons";
			ds.Tables[0].AcceptChanges();
			GrdAWBDetails.DataSource = ds.Tables[0];
			GrdAWBDetails.DataBind();
		}

		private string uploadFiletoServer()
		{			
			string result = "";

			if((inFile.PostedFile != null ) && inFile.PostedFile.ContentLength > 0)
			{
				string extension = System.IO.Path.GetExtension(inFile.PostedFile.FileName);

				if(extension.ToUpper() == ".TXT" || extension.ToUpper() == ".CSV")
				{
					string fn = System.IO.Path.GetFileName(inFile.PostedFile.FileName);					
					string tempFileForStorage = System.Configuration.ConfigurationSettings.AppSettings["InvoiceExportPath"].ToString();
					tempFileForStorage += fn;

					try
					{						
						inFile.PostedFile.SaveAs(tempFileForStorage);											
						lblError.Text = "";
						result = tempFileForStorage;
					}
					catch(Exception ex)
					{
						txtFilePath.Text = "";
						throw  ex;
					}
				}
				else
				{
					lblError.Text = "Invalid file extension - should be .CSV or .TXT";
					txtFilePath.Text = "";
					result = "";
				}
			}
			else
			{
				lblError.Text = "Please select a file to upload.";
				txtFilePath.Text = "";
				result = "";
			}

			return result;
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			lblError.Text = "";
			txtYear.Text = "";
			ddlWeek.SelectedIndex = 0;
			ddlShipper.SelectedIndex = 0;
			txtFilePath.Text = "";
			btnImport.Enabled = false;
			GrdAWBDetails.DataBind();
			SetInitialFocus(txtYear);
		}

		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				System.Text.StringBuilder s = new System.Text.StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}

		private void btnGenerateMatchingReport_Click(object sender, System.EventArgs e)
		{
			string shipper = string.Empty;
			string year = string.Empty;
			string week = string.Empty;

			foreach (DataGridItem row in GrdAWBDetails.Items) 
			{
				CheckBox chkSelect = row.FindControl("cbRows") as CheckBox;
				Label lblIShipper = row.FindControl("lblIShipper") as Label;
				Label lblInvoicePeriod = row.FindControl("lblInvoicePeriod") as Label;

				if(chkSelect != null)
				{
					if(chkSelect.Checked)
					{
						shipper = lblIShipper.Text;
						year = lblInvoicePeriod.Text.Substring(0,4);
						week = lblInvoicePeriod.Text.Substring(lblInvoicePeriod.Text.IndexOf("-") + 1);
						break;
					}
				}
			}

			if(shipper != string.Empty && year != string.Empty && week != string.Empty)
			{
				DataTable dtParam = new DataTable();
				DataRow row = dtParam.NewRow();

				dtParam.Columns.Add("param1");
				dtParam.Columns.Add("param2");
				dtParam.Columns.Add("param3");
				dtParam.Columns.Add("param4");
				dtParam.Columns.Add("param5");

				row["param1"] = strEnterpriseID;
				row["param2"] = strUserLoggin;
				row["param3"] = shipper;
				row["param4"] = year;
				row["param5"] = week;

				dtParam.Rows.Add(row);
			
				//Set and call report
				String strUrl = "ReportViewer.aspx";
				Session["FORMID"] = "CustomsFreightCollectMatching";
				Session["RptParam"] = dtParam;
				
				ArrayList paramList = new ArrayList();
				
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindowReport.js", paramList);
				//String sScript = "<script language=javascript>openPopup('" + strUrl + "');</script>"; 
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}

			btnGenerateMatchingReport.Enabled = true;
		}

		private void GrdAWBDetails_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				DataRowView dr = (DataRowView)e.Item.DataItem;
				if(dr["Total Charges"] != DBNull.Value && dr["Total Charges"].ToString() != "")
				{
					Label lblTotalCharges = (Label)e.Item.FindControl("lblTotalCharges");
					try
					{
						lblTotalCharges.Text = Convert.ToDecimal(dr["Total Charges"]).ToString(currency_decimal); 
					}
					catch
					{
						lblTotalCharges.Text = dr["Total Charges"].ToString();
					}	
				}
			}
		}
		public string GetCurrencyDigitsOfEnterpriseConfiguration()
		{
			DataSet dsEprise = SysDataManager1.GetEnterpriseProfile(strAppID,strEnterpriseID);
			DataRow dr = dsEprise.Tables[0].Rows[0];
			
			string tempplate = "00";
			if(dr["currency_decimal"].ToString()!="")
			{
				tempplate="";
				int number_digit = Convert.ToInt32(dr["currency_decimal"].ToString());
				for (int i = 0 ; i< number_digit ; i++)
				{
					tempplate = tempplate+"0";
				}
			}
			return "#,##0." + tempplate;
		}
	}
}
