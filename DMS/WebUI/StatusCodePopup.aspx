<%@ Page language="c#" Codebehind="StatusCodePopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.StatusCodePopup" SmartNavigation="False" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>StatusCodePopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
  </HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="StatusCodePopup" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 8px; WIDTH: 501px; POSITION: absolute; HEIGHT: 130px" cellSpacing="1" cellPadding="1" width="501" border="0">
				<tr>
					<td colspan="3"><asp:label id="Label5" style="Z-INDEX: 111; LEFT: 15px" runat="server" CssClass="mainTitleSize" Height="36px" Width="382px">Status Code</asp:label>
					</td>
				</tr>
				<TR>
					<TD><asp:label id="Label1" runat="server" CssClass="tableLabel">Status Code</asp:label></TD>
					<TD><asp:textbox id="txtStatusSrch" runat="server" Width="152" CssClass="textField"></asp:textbox></TD>
					<TD>
						<asp:button id="btnSearch" runat="server" Text="Search" CssClass="queryButton" Width="67px"></asp:button></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label2" runat="server" CssClass="tableLabel">Status Description</asp:label></TD>
					<TD><asp:textbox id="txtDescSrch" runat="server" Width="152" CssClass="textField"></asp:textbox></TD>
					<TD>
						<asp:button id="btnClose" runat="server" Width="67" CssClass="queryButton" Text="Close"></asp:button></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label7" runat="server" CssClass="tableLabel">Status Type</asp:label></TD>
					<TD>
						<asp:DropDownList id="ddlStatusType" runat="server" Width="152" CssClass="textField"></asp:DropDownList></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label3" runat="server" CssClass="tableLabel">Status Close</asp:label></TD>
					<TD>
						<asp:DropDownList id="ddlCloseSrch" runat="server" Width="152" CssClass="textField"></asp:DropDownList></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:label id="Label4" runat="server" CssClass="tableLabel">Invoiceable</asp:label></TD>
					<TD>
						<asp:DropDownList id="ddlInvoiceSrch" runat="server" Width="152" CssClass="textField"></asp:DropDownList></TD>
					<TD></TD>
				</TR>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">
						<asp:datagrid id="m_dgStatusCode" style="Z-INDEX: 102; LEFT: 0px; POSITION: absolute; TOP: 191px" runat="server" CssClass="gridHeading" BorderStyle="None" BorderWidth="1px" CellPadding="3" BackColor="White" BorderColor="#CCCCCC" AllowPaging="True" AllowCustomPaging="True" OnPageIndexChanged="OnPaging_Dispatch" AutoGenerateColumns="False" Width="581px">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle CssClass="popupGridField"></ItemStyle>
							<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
							<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
							<Columns>
								<asp:BoundColumn ItemStyle-HorizontalAlign="Left" DataField="status_code" HeaderText="Status Code"></asp:BoundColumn>
								<asp:BoundColumn ItemStyle-HorizontalAlign="Left" DataField="status_description" HeaderText="Description"></asp:BoundColumn>
								<asp:BoundColumn ItemStyle-HorizontalAlign="Left" DataField="status_type" HeaderText="Type"></asp:BoundColumn>
								<asp:BoundColumn ItemStyle-HorizontalAlign="Left" DataField="status_close" HeaderText="Status Close"></asp:BoundColumn>
								<asp:BoundColumn ItemStyle-HorizontalAlign="Left" DataField="invoiceable" HeaderText="Invoiceable"></asp:BoundColumn>
								<asp:ButtonColumn ItemStyle-HorizontalAlign="Left" Text="Select" CommandName="Select"></asp:ButtonColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
					</td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>
