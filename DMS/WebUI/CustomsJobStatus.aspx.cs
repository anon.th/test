using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TIESDAL;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using System.Text.RegularExpressions;
using System.Text;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CustomsJobStatus.
	/// </summary>
	public class CustomsJobStatus : com.common.applicationpages.BasePage
	{
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label31;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label lblHCSupportedbyEnterprise;
		protected System.Web.UI.WebControls.Label Label32;
		protected System.Web.UI.WebControls.Label Label33;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label34;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.WebControls.DropDownList ddlCustomsProfile;
        protected System.Web.UI.WebControls.DropDownList ddDateType;
        protected System.Web.UI.UpdatePanel UpdatePanel1;
        //protected System.Web.UI.WebControls.RadioButton rbinfoReceived;
        //protected System.Web.UI.WebControls.RadioButton rbEntryCompiled;
        //protected System.Web.UI.WebControls.RadioButton rbExpectedArrival;
        //protected System.Web.UI.WebControls.RadioButton rbInvoiced;
        //protected System.Web.UI.WebControls.RadioButton rbManifested;
        //protected System.Web.UI.WebControls.RadioButton rbDelivered;
        protected System.Web.UI.HtmlControls.HtmlTable Table1;

		protected System.Web.UI.WebControls.DataGrid grdJobStatus;

		public int DSMode
		{
			get
			{
				if(ViewState["iDSMode"]==null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["iDSMode"];
				}
			}
			set
			{
				ViewState["iDSMode"]=value;
			}
		}

		public string currency_decimal
		{
			get
			{
				if(ViewState["currency_decimal"] == null)
				{
					return "00";
				}
				else
				{
					return (string)ViewState["currency_decimal"];
				}
			}
			set
			{
				ViewState["currency_decimal"]=value;
			}
		}

		public int DSOperation
		{
			get
			{
				if(ViewState["iDSOperation"]==null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["iDSOperation"];
				}
			}
			set
			{
				ViewState["iDSOperation"]=value;
			}
		}



		// MAY Add For JobDetail Report
		public string strJobMenu
		{
			get
			{
				if(ViewState["strJobMenu"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["strJobMenu"];
				}
			}
			set
			{
				ViewState["strJobMenu"]=value;
			}
		}


		public string RequisitionNumber
		{
			get
			{
				if(ViewState["RequisitionNumber"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["RequisitionNumber"];
				}
			}
			set
			{
				ViewState["RequisitionNumber"]=value;
			}
		}

		public string Payee
		{
			get
			{
				if(ViewState["Payee"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["Payee"];
				}
			}
			set
			{
				ViewState["Payee"]=value;
			}
		}


		public string RequisitionType
		{
			get
			{
				if(ViewState["RequisitionType"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["RequisitionType"];
				}
			}
			set
			{
				ViewState["RequisitionType"]=value;
			}
		}


		private string userID = null;
		private string appID = null;
		private string enterpriseID = null;
		private string m_strCulture;
		protected System.Web.UI.WebControls.TextBox JobEntryNo;
		protected System.Web.UI.WebControls.TextBox HouseAWBNo;
		protected System.Web.UI.WebControls.TextBox MasterAWBNo;
		protected System.Web.UI.WebControls.CheckBox SearchMAWBinJobEntry;
		protected System.Web.UI.WebControls.TextBox CustomerID;
		protected System.Web.UI.WebControls.DropDownList CustomerGroup;
		protected System.Web.UI.WebControls.DropDownList BondedWarehouse;
		protected System.Web.UI.WebControls.DropDownList DischargePort;
		protected System.Web.UI.WebControls.DropDownList JobStatus ;
		protected System.Web.UI.WebControls.DropDownList EntryType;
		protected System.Web.UI.WebControls.TextBox ShipFlightNo;
		protected System.Web.UI.WebControls.TextBox ExporterName;
		protected System.Web.UI.WebControls.TextBox FolioNumber;
		protected System.Web.UI.WebControls.TextBox ChequeReqNo;
		protected System.Web.UI.WebControls.DropDownList AssignedProfile;
		protected System.Web.UI.WebControls.TextBox ReceiptNumber;
		protected System.Web.UI.WebControls.TextBox CustomsDeclarationNo;
		protected System.Web.UI.WebControls.Label lblGrd;
		protected System.Web.UI.WebControls.Label lblHeader;
		protected System.Web.UI.WebControls.Button btnGenerateReport;
		protected System.Web.UI.WebControls.TextBox txtFocus;
		protected System.Web.UI.HtmlControls.HtmlTableRow trSearchJobStatus;
		protected System.Web.UI.HtmlControls.HtmlTableRow trGrdJobStatus;
		protected System.Web.UI.WebControls.Button btnAddChequeReq;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.RadioButton Radiobutton3;
		protected System.Web.UI.WebControls.RadioButton Radiobutton4;
		protected System.Web.UI.WebControls.RadioButton Radiobutton5;
		protected System.Web.UI.WebControls.RadioButton Radiobutton6;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.WebControls.RadioButton rdoDuty;
		protected System.Web.UI.WebControls.RadioButton rdoFreight ;
		protected System.Web.UI.WebControls.RadioButton rdoDO;
		protected System.Web.UI.WebControls.RadioButton rdoStorage;
		protected System.Web.UI.WebControls.RadioButton rdoOther;
		protected System.Web.UI.WebControls.DataGrid grdSearchJobStatus;
		protected System.Web.UI.HtmlControls.HtmlTableRow trSearchJobStatusResults;
		protected System.Web.UI.WebControls.RadioButton rdoWharfage;
		protected System.Web.UI.WebControls.CheckBox DutyTaxNotOnChequeReq;
		protected System.Web.UI.WebControls.CheckBox SearchStatusNotAchieved;
		protected System.Web.UI.WebControls.Label lblErrorAddChequeReq;
		protected System.Web.UI.WebControls.Button btnCustomerPopup;
		//protected System.Web.UI.WebControls.RadioButton rbStationOutProceessed;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.TextBox Compiler;
		protected System.Web.UI.WebControls.TextBox Registrar;
		protected System.Web.UI.WebControls.DropDownList DestinationDC;
		protected System.Web.UI.WebControls.CheckBox NotDestinationDC;
		protected System.Web.UI.WebControls.CheckBox NoSOPorPOD;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.DropDownList ReportFormat;
		protected System.Web.UI.HtmlControls.HtmlTableRow TrReportFormat;
        protected System.Web.UI.HtmlControls.HtmlGenericControl message;
        private static CustomsJobStatusDAL customjobstatusdal = new CustomsJobStatusDAL();
	
		private void Page_Load(object sender, System.EventArgs e)
		{						
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();
			m_strCulture = utility.GetUserCulture();
			
			if(!Page.IsPostBack)
			{
				currency_decimal =  this.GetCurrencyDigitsOfEnterpriseConfiguration();	
				//MAY Add For Job Detail Report MODID=02JobDetails
				strJobMenu = ""; 
				RequisitionNumber="";
				Payee="";
				RequisitionType="";
				lblHeader.Text = "Customs Job Status";
				btnExecQry.Visible = true;
				btnGenerateReport.Visible=false;
				grdJobStatus.Visible=true;
				trGrdJobStatus.Visible=true;
				trSearchJobStatus.Visible=false;
				grdSearchJobStatus.Visible=false;
				trSearchJobStatusResults.Visible=false;
				TrReportFormat.Visible=false;
				if(Request.QueryString["pagemode"] != null && Request.QueryString["pagemode"].ToString()=="CustomsReport_JobDetails")
				{
					strJobMenu = "CustomsReport_JobDetails";
					btnExecQry.Visible = false;
					btnGenerateReport.Visible=true;
					grdJobStatus.Visible=false;
					trGrdJobStatus.Visible=false;
					trSearchJobStatus.Visible=false;
					TrReportFormat.Visible=true;
					lblHeader.Text = "Customs Jobs Listings";
				}
				else if(Request.QueryString["pagemode"] != null && Request.QueryString["pagemode"].ToString()=="CustomsCheckRequisition")
				{
					if(Request.QueryString["requisitionnumber"] != null)
					{
						RequisitionNumber=Request.QueryString["requisitionnumber"].ToString();
					}
					if(Request.QueryString["Payee"] != null)
					{
						Payee=Request.QueryString["Payee"].ToString();
					}
					if(Request.QueryString["RequisitionType"] != null)
					{
						RequisitionType=Request.QueryString["RequisitionType"].ToString();
					}
					strJobMenu = "CustomsCheckRequisition";
					btnExecQry.Visible = true;
					btnGenerateReport.Visible=false;
					grdJobStatus.Visible=false;
					trGrdJobStatus.Visible=false;
					trSearchJobStatus.Visible=true;
					grdSearchJobStatus.Visible=true;
					trSearchJobStatusResults.Visible=true;
					lblHeader.Text = "Search Customs Jobs to Add to a Cheque Requistion";
				}
                
				GenDropDownListDisplayValue();
				ResetScreenForQuery();
				SetInitialFocus(JobEntryNo);

				ddMonth.Attributes.Add("onfocus","document.getElementById('"+rbMonth.ClientID+"').checked = true;");
				txtYear.Attributes.Add("onfocus","document.getElementById('"+rbMonth.ClientID+"').checked = true;");

				txtPeriod.Attributes.Add("onfocus","document.getElementById('"+rbPeriod.ClientID+"').checked = true;");
				txtTo.Attributes.Add("onfocus","document.getElementById('"+rbPeriod.ClientID+"').checked = true;");

				txtDate.Attributes.Add("onfocus","document.getElementById('"+rbDate.ClientID+"').checked = true;");
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnGenerateReport.Click += new System.EventHandler(this.btnGenerateReport_Click);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.btnCustomerPopup.Click += new System.EventHandler(this.btnCustomerPopup_Click);
			this.btnAddChequeReq.Click += new System.EventHandler(this.btnAddChequeReq_Click);
			this.grdJobStatus.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.grdJobStatus_ItemCommand);
			this.grdJobStatus.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.grdJobStatus_PageIndexChanged);
			this.grdJobStatus.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdJobStatus_ItemDataBound);
			this.grdSearchJobStatus.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.grdSearchJobStatus_PageIndexChanged);
			this.grdSearchJobStatus.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.grdSearchJobStatus_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}


		#endregion

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			GetDefaultValueToControl();
			SetInitialFocus(JobEntryNo);
		}


		private void ResetScreenForQuery()
		{
			GetDefaultValueToControl();
		}


		private void GetDefaultValueToControl()
		{
			lblError.Text = "";
			lblErrorAddChequeReq.Text="";
            //rbinfoReceived.Checked = true;
            ddDateType.SelectedIndex = 0;
			rbMonth.Checked = true;
			ddMonth.SelectedIndex = 0;

			txtYear.Text = "";
			txtPeriod.Text = "";
			txtTo.Text = "";
			txtDate.Text = "";
			JobEntryNo.Text = "";
			MasterAWBNo.Text = "";
			SearchMAWBinJobEntry.Checked = false;
			CustomerID.Text = "";
			BondedWarehouse.SelectedIndex = 0;
			JobStatus.SelectedIndex = 0;
			SearchStatusNotAchieved.Checked = false;
			EntryType.SelectedIndex = 0;
			ShipFlightNo.Text = "";
			FolioNumber.Text = "";
			AssignedProfile.SelectedIndex = 0;
			CustomsDeclarationNo.Text = "";
			HouseAWBNo.Text = "";
			CustomerGroup.SelectedIndex = 0;
			DischargePort.SelectedIndex = 0;
			ExporterName.Text = "";
			ChequeReqNo.Text = "";
			ReceiptNumber.Text = "";
			rdoDuty.Checked=true;

			rbMonth.Checked=true;
			rbPeriod.Checked=false;
			rbDate.Checked=false;

            //rbinfoReceived.Checked = true;
            ddDateType.Enabled = true;
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;

			rdoDuty.Enabled=true;
			rdoDuty.Checked=true;
			rdoWharfage.Enabled=false;
			rdoFreight.Enabled=false;
			rdoDO.Enabled=false;
			rdoStorage.Enabled=false;
			rdoOther.Enabled=false;
			DutyTaxNotOnChequeReq.Checked = false;
			
			Compiler.Text = null;
			Registrar.Text = null;
			DestinationDC.SelectedIndex = 0;
			NotDestinationDC.Checked = false;
			NoSOPorPOD.Checked = false;

			if(RequisitionType != "" && RequisitionType =="A")
			{
				rdoDuty.Enabled=false;
				rdoWharfage.Enabled=true;
				rdoFreight.Enabled=true;
				rdoDO.Enabled=true;
				rdoStorage.Enabled=true;
				rdoOther.Enabled=true;

				rdoOther.Checked=true;
			}

			grdJobStatus.CurrentPageIndex=0;
			grdJobStatus.PageSize=EnterpriseConfigMgrDAL.CustomsJobsSearchGridRows(appID,enterpriseID);
			grdJobStatus.DataSource=CustomsDAL.QueryJobsStatusDataTable();
			grdJobStatus.DataBind();

			grdSearchJobStatus.CurrentPageIndex=0;
			grdSearchJobStatus.PageSize=EnterpriseConfigMgrDAL.CustomsJobsSearchGridRows(appID,enterpriseID);
			grdSearchJobStatus.DataSource=CustomsCheckRequisitionDAL.QueryInvoiceChargesDataTable();
			grdSearchJobStatus.DataBind();
		}


		private void GenDropDownListDisplayValue()
		{
			CustomsJobStatusDAL custom = new CustomsJobStatusDAL();
			DataSet ds_BondedWarehouses = CustomJobCompilingDAL.GetDropDownListDisplayValue(appID,enterpriseID,"BondedWarehouses");
			DataSet ds_CustomsStatus = custom.GetJobStatus(appID,enterpriseID);

			DataSet ds_EntryType = GetDropDownListDisplayValue("EntryType","GetCustomsConfiguration");
			DataSet ds_AssignedProfile = GetDropDownListDisplayValue("AssignedProfile","GetCustomsConfiguration");
			DataSet ds_CustomerGroupings = custom.GetCustomerGroupings(appID,enterpriseID);
			DataSet ds_DischargePort = GetDropDownListDisplayValue("DischargePort","GetCustomsConfiguration");
			
			DataSet ds_DestinationDC = custom.GetOriginDC(appID,enterpriseID);

			if(ds_BondedWarehouses != null)
			{
				BondedWarehouse.DataSource = ds_BondedWarehouses.Tables[0];
				BondedWarehouse.DataTextField = "DropDownListDisplayValue";
				BondedWarehouse.DataValueField = "CodedValue";
				BondedWarehouse.DataBind();
			}
			
			if(ds_CustomsStatus != null)
			{
				JobStatus.DataSource = ds_CustomsStatus.Tables[0];
				JobStatus.DataTextField = "DropDownListDisplayValue";
				JobStatus.DataValueField = "CodedValue";
				JobStatus.DataBind();
			}
			
			if(ds_EntryType != null)
			{
				EntryType.DataSource = ds_EntryType.Tables[0];
				EntryType.DataTextField = "DropDownListDisplayValue";
				EntryType.DataValueField = "CodedValue";
				EntryType.DataBind();
			}
			
			if(ds_AssignedProfile != null)
			{
				AssignedProfile.DataSource = ds_AssignedProfile.Tables[0];
				AssignedProfile.DataTextField = "DropDownListDisplayValue";
				AssignedProfile.DataValueField = "CodedValue";
				AssignedProfile.DataBind();
			}
			
			if(ds_CustomerGroupings != null)
			{
				CustomerGroup.DataSource = ds_CustomerGroupings.Tables[0];
				CustomerGroup.DataTextField = "DropDownListDisplayValue";
				CustomerGroup.DataValueField = "CodedValue";
				CustomerGroup.DataBind();
			}
			
			if(ds_DischargePort != null)
			{
				DischargePort.DataSource = ds_DischargePort.Tables[0];
				DischargePort.DataTextField = "DropDownListDisplayValue";
				DischargePort.DataValueField = "CodedValue";
				DischargePort.DataBind();
			}

			if(ds_DestinationDC != null)
			{
				DestinationDC.DataSource = ds_DestinationDC.Tables[0];
				DestinationDC.DataTextField = "origin_code";
				DestinationDC.DataValueField = "origin_code";
				DestinationDC.DataBind();
			}

			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));

			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(appID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);

			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();

            if (!this.IsPostBack)
            {
                ListItemCollection items = new ListItemCollection
                {
                    new ListItem("Info Received", "0"),
                    new ListItem("Entry Compiled", "1"),
                    new ListItem("Expected Arrival", "2"),
                    new ListItem("Invoiced", "3"),
                    new ListItem("Manifest Data Entered", "4"),
                    new ListItem("Delivered", "5"),
                    new ListItem("Station Out Processed", "6")
                };
                if(ddDateType == null)
                {
                    ddDateType = new DropDownList();
                }
                ddDateType.DataSource = items;
                ddDateType.DataBind();
            }
        }


		private DataSet GetDropDownListDisplayValue(String codeID, String sqlFunction)
		{
			DataSet ds = new DataSet();
			ds = customjobstatusdal.GetDropDownListDisplayValue(utility.GetAppID(),utility.GetEnterpriseID(), codeID, sqlFunction);
			return ds;
		}


		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			grdJobStatus.CurrentPageIndex=0;
			grdJobStatus.PageSize=EnterpriseConfigMgrDAL.CustomsJobsSearchGridRows(appID,enterpriseID);
			grdJobStatus.DataSource = CustomsDAL.QueryJobsStatusDataTable();
			grdJobStatus.DataBind();            
            ExecQry();                        
        }


		private void ExecQry()
		{
			lblError.Text="";
			DataTable dtParam = CustomsDAL.QueryJobsStatusParameter();
			System.Data.DataRow drNew = dtParam.NewRow();
			drNew["enterpriseid"] =enterpriseID;
			drNew["userloggedin"] =userID;
			int DateType = 0;
			DateTime StartDate = DateTime.MinValue;
			DateTime EndDate = DateTime.MinValue;

            if (ddDateType.SelectedItem.Value != null && Convert.ToInt32(ddDateType.SelectedItem.Value) < 10)
                DateType = Convert.ToInt32(ddDateType.SelectedItem.Value);
            //if(rbinfoReceived.Checked==true)
            //{
            //	DateType=0;
            //}
            //else if(rbEntryCompiled.Checked==true)
            //{
            //	DateType=1;
            //}
            //else if(rbExpectedArrival.Checked==true)
            //{
            //	DateType=2;
            //}
            //else if(rbInvoiced.Checked==true)
            //{
            //	DateType=3;
            //}
            //else if(rbManifested.Checked==true)
            //{
            //	DateType=4;
            //}
            //else if(rbDelivered.Checked==true)
            //{
            //	DateType=5;
            //}
            //else if (rbStationOutProceessed.Checked==true)
            //{
            //	DateType=6;
            //}

            if (rbMonth.Checked)
			{
				string strStartDate="";
				if(ddMonth.SelectedIndex != 0)
				{		
					if (txtYear.Text.Trim() == "" || !Regex.IsMatch(txtYear.Text, @"^[0-9]{4}$"))
					{
						lblError.Text = "Year (format: YYYY) is required when month is selected";
						return;
					}

					if(ddMonth.SelectedItem.Value != null && Convert.ToInt32(ddMonth.SelectedItem.Value) < 10)
						strStartDate="01"+"/0"+ddMonth.SelectedItem.Value+"/"+txtYear.Text;
					else
						strStartDate="01"+"/"+ddMonth.SelectedItem.Value+"/"+txtYear.Text.Trim();
					StartDate=System.DateTime.ParseExact(strStartDate,"dd/MM/yyyy",null);				
					EndDate=StartDate.AddMonths(1).AddDays(-1);	
				}				
			}
			if (rbPeriod.Checked==true)
			{	
				if(txtPeriod.Text.Trim() != "")
				{
					StartDate=System.DateTime.ParseExact(txtPeriod.Text.Trim(),"dd/MM/yyyy",null);									
				}		
				if(txtTo.Text.Trim() != "")
				{
					EndDate=System.DateTime.ParseExact(txtTo.Text.Trim(),"dd/MM/yyyy",null);
				}

				if(txtPeriod.Text.Trim() != "" && txtTo.Text.Trim() == "")
				{
					lblError.Text="Period end date is required";
					return;
				}

				if(EndDate != DateTime.MinValue && StartDate == DateTime.MinValue)
				{
					lblError.Text="Period from is required";
					return;
				}
				TimeSpan diffPeriod = DateTime.ParseExact(txtTo.Text, "dd/MM/yyyy", null) - DateTime.ParseExact(txtPeriod.Text, "dd/MM/yyyy", null);
				if(diffPeriod.TotalDays < 0)
				{
					lblError.Text = "Period end date must be greater than or equal to period start date";
					return;
				}
			}
			if (rbDate.Checked==true)
			{				
				if(txtDate.Text.Trim() != "")
				{
					StartDate=System.DateTime.ParseExact(txtDate.Text.Trim(),"dd/MM/yyyy",null);
					EndDate=StartDate.AddDays(1).AddMinutes(-1);	
				}
			
			}
			if(StartDate != DateTime.MinValue)
			{
				drNew["StartDate"] =StartDate.ToString("yyyy-MM-dd");
			}
			if(EndDate != DateTime.MinValue)
			{
				drNew["EndDate"] =EndDate.ToString("yyyy-MM-dd");
			}
			

			drNew["DateType"] =DateType;

			foreach(System.Data.DataColumn col in dtParam.Columns)
			{
				string colName = col.ColumnName;
				System.Web.UI.Control ctrol = this.Page.FindControl(colName);
				if(ctrol != null)
				{
					if(ctrol.GetType()==typeof(TextBox))
					{
						TextBox txt = (TextBox)ctrol;
						if(txt.Text.Trim() != "")
						{
							drNew[colName]=txt.Text.Trim();							
						}						
					}
					else if(ctrol.GetType()==typeof(com.common.util.msTextBox))
					{
						com.common.util.msTextBox txt = (com.common.util.msTextBox)ctrol;
						if(txt.TextMaskType==MaskType.msDate)
						{
							drNew[colName]=DateTime.ParseExact(txt.Text,"dd/MM/yyyy",null).ToString("yyyy-MM-dd");
						}
						else
						{
							drNew[colName]=txt.Text.Trim();
						}
						
					}
					else if(ctrol.GetType()==typeof(DropDownList))
					{
						DropDownList ddl = (DropDownList)ctrol;
						if(ddl.SelectedIndex != 0)
						{
							drNew[colName]=ddl.SelectedValue;
						}

					}		
					else if(ctrol.GetType()==typeof(CheckBox))
					{
						CheckBox chk = (CheckBox)ctrol;
						if(chk.Checked==true)
						{
							drNew[colName]=1;
						}
						else
						{
							drNew[colName]=0;
						}
					}	
				}
			}

			dtParam.Rows.Add(drNew);
			
			if (strJobMenu == "CustomsReport_JobDetails")
			{
				//Set and call report
				String strUrl = "ReportViewer.aspx";
				Session["FORMID"] = "CustomsReportJobDetails";
				Session["RptParam"] = dtParam;
				Session["REPORT_TEMPLATE"]=ReportFormat.SelectedValue;		
				ArrayList paramList = new ArrayList();
							
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindowReport.js", paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

			}
			else if (strJobMenu == "CustomsCheckRequisition")
			{
				DataSet ds = CustomsCheckRequisitionDAL.Customs_QueryInvoiceCharges(appID,enterpriseID,userID,dtParam);
				// CAll From Menu Report Job Detail
				DataTable dtCons = ds.Tables[0];
				DataColumn colCheck = new DataColumn("Check",typeof(bool));
				colCheck.DefaultValue = false;
				dtCons.Columns.Add(colCheck);

				grdSearchJobStatus.DataSource=dtCons;
				grdSearchJobStatus.DataBind();	
				lblError.Text = dtCons.Rows.Count.ToString()+ " row(s) returned";
			}
			else
			{
				DataSet ds = CustomsDAL.Customs_QueryJobsStatus(appID,enterpriseID,userID,dtParam);
				// CAll From Menu Report Job Detail
				grdJobStatus.DataSource=ds;
				grdJobStatus.DataBind();	
				lblError.Text = ds.Tables["RowReturn"].Rows[0][0].ToString() + " row(s) returned";
			}
			this.strScrollPosition = "500";

            //ScriptManager.RegisterStartupScript(this.UpdatePanel1, this.UpdatePanel1.GetType(), "blah", "$('#CustomsJobStatus').waitMe('hide');", true);
            if (btnExecQry.Visible == true)
            {
                SetInitialFocus(ddDateType);
            }                
        }


		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			lblError.Text="";
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;

			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;

			txtDate.Text = null;
			txtDate.Enabled = false;

			SetInitialFocus(ddMonth);
		}


		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			lblError.Text="";
			ddMonth.Enabled = false;
			ddMonth.SelectedIndex = 0;
			txtYear.Text = null;
			txtYear.Enabled = false;

			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;

			txtDate.Text = null;
			txtDate.Enabled = false;

			SetInitialFocus(txtPeriod);
		}


		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			lblError.Text="";
			ddMonth.Enabled = false;
			ddMonth.SelectedIndex = 0;
			txtYear.Text = null;
			txtYear.Enabled = false;

			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;

			txtDate.Text = null;
			txtDate.Enabled = true;

			SetInitialFocus(txtDate);
		}


		private bool ValidateValues()
		{
			bool iCheck=false;
			if((ddMonth.SelectedIndex==0)&&(txtYear.Text=="")&&(txtPeriod.Text=="")&&(txtTo.Text=="")&&(txtDate.Text==""))
			{
				lblError.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVL",utility.GetUserCulture());
				return iCheck=true;			
			}
			if(rbMonth.Checked == true)
			{
				if((ddMonth.SelectedIndex>0) &&(txtYear.Text==""))
				{
					lblError.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((ddMonth.SelectedIndex==0) &&(txtYear.Text!=""))
				{
					lblError.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbPeriod.Checked == true )
			{
				if((txtPeriod.Text!="")&&(txtTo.Text==""))
				{
					lblError.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_END_DT",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((txtPeriod.Text=="")&&(txtTo.Text!=""))
				{
					lblError.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_START_DT",utility.GetUserCulture());			
					return iCheck=true;
			
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbDate.Checked == true )
			{
				if(txtDate.Text=="")
				{
					lblError.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT",utility.GetUserCulture());	
					return iCheck=true;			
				}
			}
			
			return iCheck;
			
		}


		private void grdJobStatus_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				DataRowView dr = (DataRowView)e.Item.DataItem;
				if(dr["EntryCompilingDate"] != DBNull.Value && dr["EntryCompilingDate"].ToString() != "")
				{
					Label lblEntryCompilingDate = (Label)e.Item.FindControl("lblEntryCompilingDate");
					try
					{
						lblEntryCompilingDate.Text = DateTime.ParseExact(dr["EntryCompilingDate"].ToString(),"yyyy-MM-dd",null).ToString("dd/MM/yyyy");
					}
					catch
					{
						lblEntryCompilingDate.Text = dr["EntryCompilingDate"].ToString();
					}
					
				}

				if(dr["ChequeRequisitionDate"] != DBNull.Value && dr["ChequeRequisitionDate"].ToString() != "")
				{
					Label lblChequeRequisitionDate = (Label)e.Item.FindControl("lblChequeRequisitionDate");
					try
					{
						lblChequeRequisitionDate.Text = DateTime.ParseExact(dr["ChequeRequisitionDate"].ToString(),"yyyy-MM-dd",null).ToString("dd/MM/yyyy");
					}
					catch
					{
						lblChequeRequisitionDate.Text = dr["ChequeRequisitionDate"].ToString();
					}
					
				}

				if(dr["FreightCollectDate"] != DBNull.Value && dr["FreightCollectDate"].ToString() != "")
				{
					LinkButton lnkFreightCollectDate = (LinkButton)e.Item.FindControl("lnkFreightCollectDate");
					try
					{
						lnkFreightCollectDate.Text = DateTime.Parse(dr["FreightCollectDate"].ToString()).ToString("dd/MM/yyyy");
					}
					catch
					{
						lnkFreightCollectDate.Text = dr["FreightCollectDate"].ToString();
					}
					
				}

				if(dr["ManifestDataEntryDate"] != DBNull.Value && dr["ManifestDataEntryDate"].ToString() != "")
				{
					LinkButton lbkManifestDataEntryDate = (LinkButton)e.Item.FindControl("lbkManifestDataEntryDate");
					try
					{
						//lbkManifestDataEntryDate.Text = DateTime.ParseExact(dr["ManifestDataEntryDate"].ToString(),"yyyy-MM-dd",null).ToString("dd/MM/yyyy");
						lbkManifestDataEntryDate.Text = dr["ManifestDataEntryDate"].ToString();
					}
					catch
					{
						lbkManifestDataEntryDate.Text = dr["EntryCompilingDate"].ToString();
					}
					
				}

				if(dr["InvoiceStatus"] != DBNull.Value && dr["InvoiceStatus"].ToString() != "")
				{
					LinkButton lnkInvoicedDate = (LinkButton)e.Item.FindControl("lnkInvoicedDate");
					try
					{
						lnkInvoicedDate.Text = DateTime.ParseExact(dr["InvoiceStatus"].ToString(),"dd/MM/yyyy HH:mm",null).ToString("dd/MM/yyyy HH:mm");
					}
					catch
					{
						lnkInvoicedDate.Text = dr["InvoiceStatus"].ToString();
					}
				}

				
				if(dr["DutyPaidDate"] != DBNull.Value && dr["DutyPaidDate"].ToString() != "")
				{
					Label lblDutyPaidDate = (Label)e.Item.FindControl("lblDutyPaidDate");
					try
					{
						lblDutyPaidDate.Text = DateTime.ParseExact(dr["DutyPaidDate"].ToString(),"yyyy-MM-dd",null).ToString("dd/MM/yyyy");
					}
					catch
					{
						lblDutyPaidDate.Text = dr["DutyPaidDate"].ToString();
					}
					
				}
			}
		}


		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}


		private void grdJobStatus_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			DataRowView dr = (DataRowView)e.Item.DataItem;
			string cmd = e.CommandName;
			string sUrl = "";
			string sScript="";
			if(cmd=="HouseAWBNumber")
			{
				LinkButton lnkHouseAWBNumber = (LinkButton)e.Item.FindControl("lnkHouseAWBNumber");
				sUrl = "CreateUpdateConsignment.aspx?pagemode=popup&consignment_no="+lnkHouseAWBNumber.Text;	

				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				sScript = Utility.GetScriptPopupSetSize("openParentWindowCustomSize.js",paramList,600,1000);
			}
			else if(cmd=="EntryCompilingDate")
			{
				Label lblJobEntryNo = (Label)e.Item.FindControl("lblJobEntryNo");
				LinkButton lnkJobEntryNo = (LinkButton)e.Item.FindControl("lnkJobEntryNo");
				sUrl = "CustomsJobEntryCompiling.aspx?pagemode=popup&jobentryno="+lblJobEntryNo.Text;	

				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				sScript = Utility.GetScriptPopupSetSize("openParentWindowCustomSize.js",paramList,600,1000);
			}
			else if(cmd=="FreightCollectDate")
			{
				Label lblJobEntryNo = (Label)e.Item.FindControl("lblJobEntryNo");
				sUrl = "CustomsFreightCollectCharges.aspx?pagemode=popup&jobentryno="+lblJobEntryNo.Text;	

				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				sScript = Utility.GetScriptPopupSetSize("openParentWindowCustomSize.js",paramList,600,800);
			}
			else if(cmd=="InvoicedDate")
			{
				Label lblJobEntryNo = (Label)e.Item.FindControl("lblJobEntryNo");
				sUrl = "CustomsInvoicing.aspx?pagemode=popup&jobentryno="+lblJobEntryNo.Text;	

				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				sScript = Utility.GetScriptPopupSetSize("openParentWindowCustomSize.js",paramList,600,900);
			}
			else if(cmd=="ManifestDataEntryDate")
			{
				LinkButton lnkHouseAWBNumber = (LinkButton)e.Item.FindControl("lnkHouseAWBNumber");
				com.ties.ShipmentTrackingQuery_New shp = new com.ties.ShipmentTrackingQuery_New();
				DataSet dsShipment = new DataSet();
				DataTable dtShipment = shp.CreateEmptyDataTable();
				DataRow drShipment = dtShipment.NewRow(); //16 columns
				//Enterprise
				drShipment[0] = enterpriseID;
				//UserId
				drShipment[1] = userID;
				//Start Date
				drShipment[12]=lnkHouseAWBNumber.Text;
				dtShipment.Rows.Add(drShipment);
				dsShipment.Tables.Add(dtShipment);

				SessionDS sessionds = new SessionDS();
				sessionds = ShipmentTrackingMgrDAL.QueryShipmentTracking_New(dsShipment, appID, enterpriseID, 0, 50);

				String strUrl = null;
				strUrl = "ShipmentTracking.aspx?FORMID=TRUE_CHECK";
				//String strFeatures = "'height=500,width=900,left=30,top=30,location=no,menubar=no,scrollbars=yes,resizable=yes,status=yes,titlebar=no,toolbar=no'";
				Session["SESSION_DS1"] = sessionds;
				Session["QUERY_DS"] = dsShipment;
				Session["toRefresh"]=false;
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				//sScript = Utility.GetScript("openParentWindow.js",paramList);
				sScript = Utility.GetScriptPopupSetSize("openParentWindowCustomSize.js",paramList,600,900);
				
			}


			if(sScript != "")
			{
				Utility.RegisterScriptString(sScript,"ShowPopupScript"+DateTime.Now.ToString("ddMMyyyyHHmmss"),this.Page);	
			}
			
		}


		private void grdJobStatus_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			grdJobStatus.CurrentPageIndex = e.NewPageIndex;
			ExecQry();
		}

		private void btnGenerateReport_Click(object sender, System.EventArgs e)
		{
			ExecQry();
			this.strScrollPosition = "0";
		}

		private void btnAddChequeReq_Click(object sender, System.EventArgs e)
		{
			this.lblErrorAddChequeReq.Text="";		
			string cons="";
			string strError="";
			foreach(DataGridItem item in grdSearchJobStatus.Items)
			{
				CheckBox chkConNo = (CheckBox)item.FindControl("cbkConsignment_no");
				if(chkConNo != null && chkConNo.Checked==true)
				{
					string id = chkConNo.Attributes["CrsID"].ToString();
					if(cons.Length>0)
					{
						cons+=";";
					}
					cons+=id;
				}
			}
			if(cons.Length>0)
			{
				Int64 requisition_no = Convert.ToInt64(RequisitionNumber);
				int Charge_Type = 0;
				if(RequisitionType != "" && RequisitionType =="A")
				{
					if(rdoWharfage.Checked)
					{
						Charge_Type=1;
					}
					else if(rdoFreight.Checked)
					{
						Charge_Type=2;
					}
					else if(rdoDO.Checked)
					{
						Charge_Type=3;
					}
					else if(rdoStorage.Checked)
					{
						Charge_Type=4;
					}
					else if(rdoOther.Checked)
					{
						Charge_Type=5;
					}
				}
				DataSet ds = CustomsCheckRequisitionDAL.Customs_RetrieveInvoicesToChequeReq(appID, enterpriseID, userID, requisition_no,cons,Charge_Type);
				if(ds.Tables[0].Rows.Count>0 && ds.Tables[0].Rows[0]["ErrorCode"].ToString() != "0")
				{
					this.lblErrorAddChequeReq.Text = ds.Tables[0].Rows[0]["ErrorMessage"].ToString();
				}
				else
				{
					Session["RetrieveInvoicesToChequeReq"]=ds.Tables[0].Rows[0]["ErrorMessage"].ToString();
					string sScript = "";
					sScript += "<script language=javascript>";
					sScript += "  window.opener.callback();  window.close();";
					sScript += "</script>";
					Response.Write(sScript);
				}
			}
			else
			{
				this.lblErrorAddChequeReq.Text = "No Invoice No selected.";
			}
		}

		private void grdSearchJobStatus_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			grdSearchJobStatus.CurrentPageIndex = e.NewPageIndex;
			ExecQry();
		}

		private void grdSearchJobStatus_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				DataRowView dr = (DataRowView)e.Item.DataItem;

				Label lblDutyTax = (Label)e.Item.FindControl("lblDutyTax");
				if(lblDutyTax != null)
				{
					if(dr["DutyTax"] != DBNull.Value && dr["DutyTax"].ToString() != "")
					{
						lblDutyTax.Text= Convert.ToDecimal(dr["DutyTax"]).ToString(currency_decimal); 
					}
					else
					{
						lblDutyTax.Text="";
					}
				}

				Label lblEPF = (Label)e.Item.FindControl("lblEPF");
				if(lblEPF != null)
				{
					if(dr["EPF"] != DBNull.Value && dr["EPF"].ToString() != "")
					{
						lblEPF.Text= Convert.ToDecimal(dr["EPF"]).ToString(currency_decimal); 
					}
					else
					{
						lblEPF.Text="";
					}	
				}

				Label lblWharfageHandling = (Label)e.Item.FindControl("lblWharfageHandling");
				if(lblWharfageHandling != null)
				{
					if(dr["WharfageHandling"] != DBNull.Value && dr["WharfageHandling"].ToString() != "")
					{
						lblWharfageHandling.Text= Convert.ToDecimal(dr["WharfageHandling"]).ToString(currency_decimal); 
					}
					else
					{
						lblWharfageHandling.Text="";
					}	
				}

				Label lblFreightCollect = (Label)e.Item.FindControl("lblFreightCollect");
				if(lblFreightCollect != null)
				{
					if(dr["FreightCollect"] != DBNull.Value && dr["FreightCollect"].ToString() != "")
					{
						lblFreightCollect.Text= Convert.ToDecimal(dr["FreightCollect"]).ToString(currency_decimal); 
					}
					else
					{
						lblFreightCollect.Text="";
					}	
				}

				Label lblDeliveryOrderFee = (Label)e.Item.FindControl("lblDeliveryOrderFee");
				if(lblDeliveryOrderFee != null)
				{
					if(dr["DeliveryOrderFee"] != DBNull.Value && dr["DeliveryOrderFee"].ToString() != "")
					{
						lblDeliveryOrderFee.Text= Convert.ToDecimal(dr["DeliveryOrderFee"]).ToString(currency_decimal); 
					}
					else
					{
						lblDeliveryOrderFee.Text="";
					}	
				}

				Label lblStorage = (Label)e.Item.FindControl("lblStorage");
				if(lblStorage != null)
				{
					if(dr["Storage"] != DBNull.Value && dr["Storage"].ToString() != "")
					{
						lblStorage.Text= Convert.ToDecimal(dr["Storage"]).ToString(currency_decimal); 
					}
					else
					{
						lblStorage.Text="";
					}	
				}

				Label lblOther = (Label)e.Item.FindControl("lblOther");
				if(lblStorage != null)
				{
					if(dr["Other"] != DBNull.Value && dr["Other"].ToString() != "")
					{
						lblOther.Text= Convert.ToDecimal(dr["Other"]).ToString(currency_decimal); 
					}
					else
					{
						lblOther.Text="";
					}	
				}
			}
		}

		public string GetCurrencyDigitsOfEnterpriseConfiguration()
		{
			DataSet dsEprise = SysDataManager1.GetEnterpriseProfile(utility.GetAppID(),utility.GetEnterpriseID());
			DataRow dr = dsEprise.Tables[0].Rows[0];
			
			string tempplate = "00";
			if(dr["currency_decimal"].ToString()!="")
			{
				tempplate="";
				int number_digit = Convert.ToInt32(dr["currency_decimal"].ToString());
				//NumberScale=number_digit;
				for (int i = 0 ; i< number_digit ; i++)
				{
					tempplate = tempplate+"0";
				}
			}
			return "#,##0." + tempplate;
		}

		private void btnCustomerPopup_Click(object sender, System.EventArgs e)
		{
			String sUrl = "CustomerPopup.aspx?FORMID=CustomsJobStatus";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			if(Request.QueryString["pagemode"] != null && Request.QueryString["pagemode"].ToString()=="CustomsCheckRequisition")
			{
				String sScript = Utility.GetScript("openLargerChildParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			else
			{
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}

		}

	}
}
