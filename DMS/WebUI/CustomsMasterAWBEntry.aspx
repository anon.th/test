<%@ Page language="c#" Codebehind="CustomsMasterAWBEntry.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsMasterAWBEntry" %>
<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomsMasterAWBEntry</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<style>
			.ImgNoMargin { MARGIN-LEFT: 10px !important; MARGIN-RIGHT: 10px !important }
		</style>
		<script language="javascript" type="text/javascript">
		function funcDisBack(){
			history.go(+1);
		}

        function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function RemoveBadMasterAWBNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^-_/_a-zA-Z0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteMasterAWBNumber(obj) {
            setTimeout(function () {
                RemoveBadMasterAWBNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function validate(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		function ValidateMasterAWBNumber(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[-_/_a-zA-Z0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		function callback()
        {
				var btn = document.getElementById('btnClientEvent');
				if(btn != null)
				{					
					btn.click();
				}else{
					alert('error call code behind');
				}				        
			
        }
        
        function callbackOverallStatus(number)
        {
			
			var MasterAWB_no = document.getElementById('MasterAWB_no');
			var btn = document.getElementById('btnClientEvent');
			MasterAWB_no.value=number;
			if(btn != null){					
				btn.click();
			}else{
				alert('error call code behind');
			}				        
        }
        
		function callbackbtnVerifyClient()
        {
				var btn = document.getElementById('btnVerifyClient');
				if(btn != null)
				{				
					btn.click();
				}else{
					alert('error call code behind');
				}				        
			
        }
        
        function WeightKeyPress()
        {
			var TotalWeight = document.getElementById('TotalWeight');        
			var txtTotalWeightDesc = document.getElementById('txtTotalWeightDesc');
			txtTotalWeightDesc.value = TotalWeight.value;
        }
        
		function AWBsKeyPress()
        {
			var TotalAWBs = document.getElementById('TotalAWBs');        
			var txtTotalAWBsDesc = document.getElementById('txtTotalAWBsDesc');
			txtTotalAWBsDesc.value = TotalAWBs.value;        
        }
        
        function ClickBrowseFile()
        {
			var ofd = document.getElementById("fileUpload");
			ofd.click();  
        }
        
        function BrowseFileChange()
        {			
			document.getElementById("lblError").innerHTML = "";
			var selectedConsolidation=document.getElementById("ConsolidationShipper").selectedIndex;			
			if(selectedConsolidation > 0)
			{
				var btnExecQry = document.getElementById("btnExecQry");
				var btnInsert = document.getElementById("btnInsert");
				var btnImport = document.getElementById("btnImport");
				
				btnExecQry.disabled=true;  
				btnInsert.disabled=true;  
				btnImport.disabled=true;  	
				
				var s = document.all['inFile'];
				s.click();
				
				return true;	
			}else{
				//document.getElementById("lblError").innerHTML = "Consolidation shipper is required for import.";			
				return false;
			}
			
        }
        
        function uploadFile(value)
		{
			
			var s = document.all['inFile'];
			document.all['txtFileName'].innerText = s.value;

			if(s.value != '') 
			{
				var btnImport = document.getElementById("btnImport");
				btnImport.disabled = false;
				btnImport.focus();
			}
			else
			{
				//alert('xxx');
				var btnExecQry = document.getElementById("btnExecQry");
				var btnInsert = document.getElementById("btnInsert");
				var btnImport = document.getElementById("btnImport");
				
				btnExecQry.disabled=false;  
				btnInsert.disabled=false;  
				btnImport.disabled=true; 
				
				document.all['txtFileName'].innerText="";

			}
			
		}
		
		function disableImport()
		{
			var btnBrowse = document.getElementById("btnBrowse");
			var btnQry = document.getElementById("btnQry");
			var btnExecQry = document.getElementById("btnExecQry");
			var btnInsert = document.getElementById("btnInsert");
			var btnImport = document.getElementById("btnImport");
			var inFile = document.getElementById("inFile");
			var divFileUpload = document.getElementById("divFileUpload");
			btnBrowse.disabled = true;
			btnQry.disabled = true;
			btnExecQry.disabled = true;
			btnInsert.disabled = true;
			btnImport.disabled = true;
			inFile.onclick = function(){ return false; };
			divFileUpload.className = divFileUpload.className + " disabled";
			return true;
		}
		
		function upBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-up.GIF)';
		}
		
		function downBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-down.GIF)';
		}
		
		function clearValue()
		{
			if(document.all['txtFileName'].innerText == '') {
				document.location.reload();
			}
		}
				
		function getFile()
        {
            document.getElementById("File1").click();
            document.getElementById("txtFileName").value = document.getElementById("File1").value;
        }
        
        function FileNameChanged()
        {
			var selectedConsolidation = document.getElementById("ConsolidationShipper").selectedIndex;			
			if (document.getElementById("txtFileName").value != "")
			{
				if (selectedConsolidation == 0)
				{
					document.getElementById("lblError").innerHTML = "Consolidation shipper is required for import.";
					document.getElementById("ConsolidationShipper").focus();
					document.getElementById("btnImport").disabled = true;	
				}
				else
				{
					document.getElementById("btnImport").disabled = false;
				}
			}
			else
			{
				document.getElementById("btnImport").disabled = true;				
			}	
        }
        
        function checkConsolidationShipper()
        {
			var ConsolidationShipper = document.getElementById("ConsolidationShipper");
			var lblError = document.getElementById("lblError");
			
			if(ConsolidationShipper[ConsolidationShipper.selectedIndex].value==""){
				lblError.innerHTML = "Consolidation shipper is required for import.";
				ConsolidationShipper.focus();
				return false;
			}
        }
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="CustomsMasterAWBEntry" encType="multipart/form-data" runat="server">
			<div style="POSITION: relative; WIDTH: 1208px; HEIGHT: 1543px; TOP: 34px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0" width="100%"
					runat="server">
					<tbody>
						<tr>
							<td colSpan="2" align="left"><asp:label id="Label1" runat="server" Width="421px" Height="27px" CssClass="mainTitleSize">
								Master AWB Entry</asp:label></td>
						</tr>
						<tr>
							<td colSpan="2" vAlign="top" align="left"><asp:button id="btnQry" runat="server" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExecQry" runat="server" CssClass="queryButton" Text="Execute Query" CausesValidation="False"></asp:button><asp:button id="btnInsert" runat="server" CssClass="queryButton" Text="Insert" CausesValidation="False"></asp:button><asp:button id="btnSave" runat="server" CssClass="queryButton" Text="Save" CausesValidation="False"
									Enabled="False"></asp:button><asp:button id="btnVerify" runat="server" CssClass="queryButton" Text="Verify" CausesValidation="False"
									Enabled="False"></asp:button><asp:button id="btExport" runat="server" CssClass="queryButton" Text="Export" CausesValidation="False"
									Enabled="False"></asp:button><asp:button id="btExportV2" runat="server" CssClass="queryButton" Text="Export V2" CausesValidation="False"
									Enabled="False"></asp:button><asp:button style="DISPLAY: none" id="btnClientEvent" runat="server" Text="ClientEvent" CausesValidation="False"></asp:button><asp:button style="DISPLAY: none" id="btnVerifyClient" runat="server" CssClass="queryButton"
									Text="btnVerifyClient" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 0" id="btnDelete" runat="server" CssClass="queryButton" Text="Delete"
									CausesValidation="False" Enabled="False"></asp:button><FONT face="Tahoma">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</FONT>
								<asp:button id="btnOverallStatus" runat="server" CssClass="queryButton" Text="Overall Status"
									CausesValidation="False"></asp:button>
							</td>
						</tr>
						<tr>
							<td style="HEIGHT: 19px" colSpan="2"><asp:label id="lblError" runat="server" ForeColor="Red" Font-Size="X-Small" Font-Bold="True"></asp:label></td>
						</tr>
						<tr>
							<td colSpan="2">
								<TABLE style="Z-INDEX: 0; WIDTH: 1200px; HEIGHT: 296px" id="Table1" border="0" width="1000"
									runat="server">
									<tr>
										<td>
											<TABLE id="Table2" border="0" width="100%" runat="server">
												<tr>
													<td style="WIDTH: 14%">
														<asp:label style="Z-INDEX: 0" id="Label30" runat="server" CssClass="tableLabel">Master AWB Number</asp:label><asp:label id="Label31" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
													</td>
													<td colSpan="4">
														<asp:textbox onblur="if( this.value !=''){document.getElementById('btnBrowse').disabled = true;}"
															style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="MasterAWB_no" tabIndex="1" onkeypress="ValidateMasterAWBNumber(event)"
															onpaste="AfterPasteMasterAWBNumber(this)" runat="server" Width="137px" CssClass="textField"
															MaxLength="30"></asp:textbox>
														<asp:CheckBox style="Z-INDEX: 0" id="IgnoreMAWBValidation" runat="server" CssClass="tableRadioButton"
															Text="Ignore MAWB validation" tabIndex="2"></asp:CheckBox>
													</td>
													<td vAlign="top" rowSpan="7" align="left">
														<fieldset style="WIDTH: 350px"><legend style="COLOR: black">Import Manifest in XML 
																Format</legend>
															<table style="MARGIN: 5px" width="340">
																<tr>
																	<td colSpan="2"><FONT face="Tahoma"></FONT>
																		<asp:button id="btnBrowse" class="queryButton" runat="server" Text="Browse" style="DISPLAY:none"></asp:button>
																		<div id="divFileUpload" class="fileUpload queryButton" style="MARGIN:5px 0px 0px; DISPLAY:inline"
																			onclick="return checkConsolidationShipper();">
																			<span style="VERTICAL-ALIGN: 50%">Browse</span> <INPUT style="DISPLAY: block" id="inFile" onchange="uploadFile(this.value)" type="file"
																				name="inFile" runat="server" enableViewState="True" class="upload">
																		</div>
																		<asp:button id="btnImport" runat="server" Width="62px" CssClass="queryButton" Text="Import"
																			CausesValidation="False" style="DISPLAY:inline"></asp:button></td>
																</tr>
																<TR>
																	<TD width="100"><asp:label style="Z-INDEX: 0" id="Label29" runat="server" CssClass="tableLabel">Format:</asp:label></TD>
																	<td><FONT face="Tahoma"><asp:dropdownlist style="Z-INDEX: 0" id="ddlXMLFormat" tabIndex="23" runat="server" Width="120px"
																				Height="20px" AutoPostBack="True">
																				<asp:ListItem Value="0">COMNEX</asp:ListItem>
																			</asp:dropdownlist></FONT></td>
																</TR>
																<tr>
																	<td><asp:label style="Z-INDEX: 0" id="Label37" runat="server" CssClass="tableLabel">File name:</asp:label></td>
																	<td><FONT face="Tahoma"><asp:textbox onblur="FileNameChanged();" style="Z-INDEX: 0" onkeydown="return false;" id="txtFileName"
																				tabIndex="24" runat="server" Width="220px" Height="30px" CssClass="textField" TextMode="MultiLine"></asp:textbox></FONT></td>
																</tr>
															</table>
														</fieldset>
														<br>
														<fieldset style="WIDTH: 350px" id="fsOtherData" runat="server"><legend style="COLOR: black">Other 
																Data Captured on Import</legend>
															<table style="MARGIN: 5px" width="340">
																<tr>
																	<td width="100"><asp:label style="Z-INDEX: 0" id="Label39" runat="server" CssClass="tableLabel">Flight Number:</asp:label></td>
																	<td><FONT face="Tahoma"><asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="txtImportedFlightNo" tabIndex="14"
																				runat="server" Width="220px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></FONT></td>
																</tr>
																<tr>
																	<td><asp:label style="Z-INDEX: 0" id="Label40" runat="server" CssClass="tableLabel">Transport Mode:</asp:label></td>
																	<td><asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="txtImportedTransportMode" tabIndex="14"
																			runat="server" Width="220px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
																</tr>
															</table>
														</fieldset>
													</td>
												</tr>
												<TR>
													<TD style="WIDTH: 14%"><asp:label style="Z-INDEX: 0" id="Label22" runat="server" CssClass="tableLabel"> Consolidation shipper </asp:label></TD>
													<TD colSpan="4"><asp:dropdownlist style="Z-INDEX: 0" id="ConsolidationShipper" tabIndex="2" runat="server" Width="200px"
															AutoPostBack="True">
															<asp:ListItem Selected="True"></asp:ListItem>
															<asp:ListItem Value="AU">AU</asp:ListItem>
														</asp:dropdownlist></TD>
												</TR>
												<TR>
													<TD colSpan="5"><asp:label style="Z-INDEX: 0" id="Label24" runat="server" CssClass="tableLabel">Consolidation shipper  details:</asp:label></TD>
												</TR>
												<TR>
													<TD colSpan="5"><asp:label style="POSITION: absolute" id="Label27" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; MARGIN-LEFT: 10px" id="ConsolidatorName"
															tabIndex="3" runat="server" Width="372px" CssClass="textField" MaxLength="100" AutoPostBack="True"></asp:textbox></TD>
												</TR>
												<TR>
													<TD colSpan="5"><asp:label style="POSITION: absolute" id="Label28" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; MARGIN-LEFT: 10px" id="ConsolidatorAddress1"
															tabIndex="4" runat="server" Width="372px" CssClass="textField" MaxLength="100"></asp:textbox></TD>
												</TR>
												<TR>
													<TD colSpan="5"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; MARGIN-LEFT: 10px" id="ConsolidatorAddress2"
															tabIndex="5" runat="server" Width="372px" CssClass="textField" MaxLength="100"></asp:textbox></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 14%"><asp:label style="Z-INDEX: 0" id="Label23" runat="server" CssClass="tableLabel">Country:</asp:label><asp:dropdownlist style="Z-INDEX: 0" id="ConsolidatorCountry" tabIndex="6" runat="server" Width="80px"
															AutoPostBack="True">
															<asp:ListItem Selected="True"></asp:ListItem>
															<asp:ListItem Value="AU">AU</asp:ListItem>
														</asp:dropdownlist></TD>
													<TD colSpan="4"><FONT face="Tahoma"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="ConsolidatorCountry_Desc"
																tabIndex="9" runat="server" Width="200px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></FONT></TD>
												</TR>
												<TR>
													<TD style="HEIGHT: 22px"><asp:label style="Z-INDEX: 0" id="Label34" runat="server" CssClass="tableLabel">Ship/Flight No</asp:label><asp:label id="Label36" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
													<td style="HEIGHT: 22px" colSpan="2"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="ShipFlightNo" tabIndex="7" runat="server"
															Width="137px" CssClass="textField" MaxLength="30"></asp:textbox></td>
													<td style="HEIGHT: 22px" colSpan="2"><asp:label style="Z-INDEX: 0" id="Label32" runat="server" CssClass="tableLabel">Customs Office</asp:label><asp:label style="Z-INDEX: 0" id="Label33" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
													<td style="HEIGHT: 22px"><asp:dropdownlist style="Z-INDEX: 0" id="CustomsOffice" tabIndex="8" runat="server" Width="80px" AutoPostBack="True"></asp:dropdownlist><asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="txtCustomsOfficeDesc" tabIndex="14"
															runat="server" Width="220px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
												</TR>
												<tr>
													<td style="WIDTH: 14%; HEIGHT: 14px"><asp:label style="Z-INDEX: 0" id="Label13" runat="server" CssClass="tableLabel">&nbsp;</asp:label></td>
													<td style="WIDTH: 9%; HEIGHT: 14px"><asp:label style="Z-INDEX: 0" id="Label9" runat="server" CssClass="tableLabel">Date</asp:label></td>
													<td style="WIDTH: 6%; HEIGHT: 14px"><asp:label style="Z-INDEX: 0" id="Label10" runat="server" CssClass="tableLabel">Time</asp:label></td>
													<td style="WIDTH: 6%; HEIGHT: 14px"><asp:label style="Z-INDEX: 0" id="Label11" runat="server" CssClass="tableLabel">City</asp:label></td>
													<td style="WIDTH: 8%; HEIGHT: 14px"><asp:label style="Z-INDEX: 0" id="Label12" runat="server" CssClass="tableLabel">Country</asp:label></td>
													<td style="HEIGHT: 14px"><FONT face="Tahoma"></FONT></td>
												</tr>
												<tr>
													<td style="WIDTH: 132px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label7" runat="server" CssClass="tableLabel">Departure</asp:label><asp:label id="Label8" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
													<td style="WIDTH: 106px; HEIGHT: 21px"><cc1:mstextbox id="DepartureDate" tabIndex="9" runat="server" Width="75px" CssClass="textField"
															MaxLength="12" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></td>
													<td style="WIDTH: 70px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label15" runat="server" CssClass="tableLabel">&nbsp;</asp:label></td>
													<td style="WIDTH: 8px; HEIGHT: 21px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="DepartureCity" tabIndex="10" runat="server"
															Width="50px" CssClass="textField" MaxLength="3"></asp:textbox></td>
													<td style="WIDTH: 85px; HEIGHT: 21px"><asp:dropdownlist style="Z-INDEX: 0" id="DepartureCountry" tabIndex="11" runat="server" Width="70px"
															Height="20px" AutoPostBack="True"></asp:dropdownlist></td>
													<td style="HEIGHT: 21px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtDepartureDes"
															tabIndex="14" runat="server" Width="178px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
												</tr>
												<tr>
													<td style="WIDTH: 132px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label14" runat="server" CssClass="tableLabel">Arrival</asp:label><asp:label id="Label16" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
													<td style="WIDTH: 106px; HEIGHT: 21px"><cc1:mstextbox style="Z-INDEX: 0" id="ArrivalDate" tabIndex="12" runat="server" Width="75px" CssClass="textField"
															MaxLength="12" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></td>
													<td style="WIDTH: 70px; HEIGHT: 21px"><cc1:mstextbox style="TEXT-ALIGN: right" id="ArrivalTime" tabIndex="13" runat="server" Width="45px"
															CssClass="textField" MaxLength="5" AutoPostBack="True" ReadOnly="false" TextMaskType="msTimeNoReturn" TextMaskString="99:99"></cc1:mstextbox></td>
													<td style="WIDTH: 8px; HEIGHT: 21px"><asp:textbox style="Z-INDEX: 0" id="ArrivalCity" tabIndex="14" runat="server" Width="50px" CssClass="textField"
															MaxLength="3"></asp:textbox></td>
													<td style="WIDTH: 85px; HEIGHT: 21px"><asp:dropdownlist style="Z-INDEX: 0" id="ArrivalCountry" tabIndex="15" runat="server" Width="70px"
															Height="20px" AutoPostBack="True"></asp:dropdownlist></td>
													<td style="HEIGHT: 21px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtArrivalDes"
															tabIndex="14" runat="server" Width="178px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
												</tr>
												<tr>
													<td style="WIDTH: 132px"><asp:label style="Z-INDEX: 0" id="Label17" runat="server" CssClass="tableLabel">Carrier</asp:label></td>
													<td><asp:dropdownlist style="Z-INDEX: 0" id="Carrier" tabIndex="16" runat="server" Width="75px" Height="20px"
															AutoPostBack="True"></asp:dropdownlist></td>
													<td style="WIDTH: 408px" colSpan="4"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtCarrierDes"
															tabIndex="14" runat="server" Width="191px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
												</tr>
												<tr>
													<td style="WIDTH: 132px"><asp:label style="Z-INDEX: 0" id="Label19" runat="server" CssClass="tableLabel">Mode of Transport</asp:label><asp:label id="Label21" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
													<td><asp:dropdownlist style="Z-INDEX: 0" id="ModeOfTransport" tabIndex="17" runat="server" Width="75px"
															Height="20px" AutoPostBack="True"></asp:dropdownlist></td>
													<td style="WIDTH: 408px" colSpan="4"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtModeOfTransportDes"
															tabIndex="14" runat="server" Width="191px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
												</tr>
												<tr>
													<td style="WIDTH: 132px"><asp:label style="Z-INDEX: 0" id="Label18" runat="server" CssClass="tableLabel">Folio Number</asp:label></td>
													<td colSpan="5"><asp:textbox style="Z-INDEX: 107" id="FolioNumber" tabIndex="18" onkeypress="validate(event)"
															onpaste="AfterPasteNonNumber(this)" runat="server" Width="75px" CssClass="textField" MaxLength="6"></asp:textbox></td>
												</tr>
												<!---->
												<tr>
													<td style="WIDTH: 10.55%" rowSpan="4" colSpan="3">
														<fieldset><legend style="COLOR: black">Totals/Remaining</legend>
															<table style="MARGIN: 5px 0px 5px 5px">
																<tbody>
																	<tr>
																		<td><asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel"> Weight</asp:label><asp:label id="Label3" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
																		<td><cc1:mstextbox style="Z-INDEX: 0" id="TotalWeight" tabIndex="19" onkeypress="WeightKeyPress();"
																				onkeyup="WeightKeyPress();" onpaste="AfterPasteNonNumber(this)" runat="server" Width="80px"
																				CssClass="textField" MaxLength="14" TextMaskType="msNumericCOD" NumberScale="1" NumberMaxValueCOD="999999.9"
																				NumberPrecision="8"></cc1:mstextbox></td>
																		<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtTotalWeightDesc"
																				tabIndex="14" runat="server" Width="80px" CssClass="textField" Enabled="False" MaxLength="100"
																				ReadOnly="True"></asp:textbox></td>
																	</tr>
																	<tr>
																		<td><asp:label style="Z-INDEX: 0" id="Label4" runat="server" CssClass="tableLabel">House AWBs</asp:label><asp:label id="Label5" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
																		<td><asp:textbox onkeydown="AWBsKeyPress();" id="TotalAWBs" tabIndex="20" onkeypress="validate(event);AWBsKeyPress();"
																				onkeyup="WeightKeyPress();" onpaste="AfterPasteNonNumber(this)" runat="server" Width="80px"
																				CssClass="textField" MaxLength="3"></asp:textbox></td>
																		<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtTotalAWBsDesc"
																				tabIndex="14" runat="server" Width="80px" CssClass="textField" Enabled="False" MaxLength="100"
																				ReadOnly="True"></asp:textbox></td>
																	</tr>
																</tbody>
															</table>
														</fieldset>
													</td>
													<td style="TEXT-ALIGN: right; HEIGHT: 3px" colSpan="2"><asp:label style="Z-INDEX: 0; MARGIN-RIGHT: 5px" id="Label6" runat="server" CssClass="tableLabel">Entry by</asp:label></td>
													<td style="HEIGHT: 3px" colSpan="2"><asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="UpdatedBy" tabIndex="14" runat="server"
															Width="80px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox><asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="UpdatedDT" tabIndex="14" runat="server"
															Width="110px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
												</tr>
												<tr>
													<td style="TEXT-ALIGN: right; HEIGHT: 3px" colSpan="2"><asp:label style="Z-INDEX: 0; MARGIN-RIGHT: 5px" id="Label20" runat="server" CssClass="tableLabel">Verified by</asp:label></td>
													<td style="HEIGHT: 3px" colSpan="2"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="VerifiedBy"
															tabIndex="14" runat="server" Width="80px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="VerifiedDT"
															tabIndex="14" runat="server" Width="114px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
												</tr>
												<tr>
													<td style="TEXT-ALIGN: right" colSpan="2"><asp:label style="Z-INDEX: 0; MARGIN-RIGHT: 5px" id="Label35" runat="server" CssClass="tableLabel">Exported by</asp:label></td>
													<td colSpan="2"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="ExportedBy"
															tabIndex="14" runat="server" Width="80px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="ExportedDT"
															tabIndex="14" runat="server" Width="114px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
												</tr>
												<tr>
													<td height="25" colSpan="4"><asp:label style="Z-INDEX: 0" id="Label38" runat="server" CssClass="tableLabel">&nbsp;</asp:label></td>
												</tr>
												<!----></TABLE>
										</td>
									</tr>
									<tr class="gridHeading">
										<td><STRONG><FONT size="2">AWB Details</FONT></STRONG></td>
									</tr>
									<tr>
										<td><INPUT id="hdnGrd" type="hidden" name="hdnGrd" runat="server">
											<asp:datagrid style="Z-INDEX: 0" id="GrdAWBDetails" tabIndex="21" runat="server" Width="100%"
												AutoGenerateColumns="False" ShowFooter="True" DataKeyField="MAWBNumber" AlternatingItemStyle-CssClass="gridField"
												ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridHeading" CellPadding="0" CellSpacing="1"
												BorderWidth="0px" FooterStyle-CssClass="gridHeading">
												<FooterStyle CssClass="gridHeading"></FooterStyle>
												<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
												<ItemStyle CssClass="gridField"></ItemStyle>
												<HeaderStyle CssClass="gridHeading"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="100px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<asp:LinkButton id="btnEditItem" Runat="server" Text="<img style='margin-left: 2px !important ;margin-right: 2px !important ;' src='images/butt-edit.gif' alt='edit' border=0>"
																CommandName="EDIT_ITEM" />
															<asp:LinkButton id="btnDeleteItem" Runat="server" Text="<img style='margin-left: 2px !important ;margin-right: 2px !important ;'  src='images/butt-delete.gif' alt='delete' border=0>"
																CommandName="DELETE_ITEM" />
															<asp:LinkButton id="btnPrintItem" Runat="server" Text="<img style='margin-left: 2px !important ;margin-right: 2px !important ;'  src='images/icons/PrintQuotation2.ico' alt='Print' border=0>"
																CommandName="PRINT_ITEM" />
														</ItemTemplate>
														<FooterStyle HorizontalAlign="Center"></FooterStyle>
														<FooterTemplate>
															<asp:LinkButton id="btnAddItem" Runat="server" Text="<img src='images/butt-add.gif' alt='add' border=0>"
																CommandName="ADD_ITEM" />
														</FooterTemplate>
														<EditItemTemplate>
															<asp:LinkButton id="btnSaveItem" Runat="server" Text="<img src='images/butt-update.gif' alt='save' border=0>"
																CommandName="SAVE_ITEM" />
															<asp:LinkButton id="btnCancelItem" Runat="server" Text="<img src='images/butt-red.gif' alt='cancel' border=0>"
																CommandName="CANCEL_ITEM" />
														</EditItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Rec�d">
														<HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<asp:CheckBox ID ="cbkRec" AutoPostBack="True" Runat="server" OnCheckedChanged="chk1_CheckedChanged" CrsID='<%# Container.ItemIndex %>' onclick="GetScrollPosition();" >
															</asp:CheckBox>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Number">
														<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<asp:label id="lblMAWBNumber" Text='<%# DataBinder.Eval(Container.DataItem,"MAWBNumber") %>' runat="server" Visible="False">
															</asp:label>
															<asp:label id="lblConsignment_no" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"consignment_no") %>'>
															</asp:label>
															<asp:label id="lblShipFlightNo" runat="server" Visible="False" Text ='<%# DataBinder.Eval(Container.DataItem,"ShipFlightNo") %>'>
															</asp:label>
															<asp:label id="lblMAWBHasBeenSaved" runat="server" Visible="False" Text ='<%# DataBinder.Eval(Container.DataItem,"MAWBHasBeenSaved") %>'>
															</asp:label>
															<asp:label id="lblSourceOfData" runat="server" Visible="False" Text ='<%# DataBinder.Eval(Container.DataItem,"SourceOfData") %>'>
															</asp:label>
														</ItemTemplate>
														<FooterStyle HorizontalAlign="Left"></FooterStyle>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Exporter">
														<HeaderStyle HorizontalAlign="Center" Width="150px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<asp:label id="lblExporterName" runat="server">
																<%# DataBinder.Eval(Container.DataItem,"ExporterName") %>
															</asp:label>
														</ItemTemplate>
														<FooterStyle HorizontalAlign="Left"></FooterStyle>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Consignee">
														<HeaderStyle HorizontalAlign="Center" Width="200px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<asp:label id="lblConsigneeName" runat="server">
																<%# DataBinder.Eval(Container.DataItem,"ConsigneeName") %>
															</asp:label>
														</ItemTemplate>
														<FooterStyle HorizontalAlign="Left"></FooterStyle>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Goods Description">
														<HeaderStyle HorizontalAlign="Center" Width="200px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<asp:label id="Label25" runat="server">
																<%# DataBinder.Eval(Container.DataItem,"GoodsDescription") %>
															</asp:label>
														</ItemTemplate>
														<FooterStyle HorizontalAlign="Left"></FooterStyle>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Weight">
														<HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<asp:label id="Label26" runat="server">
																<%# DataBinder.Eval(Container.DataItem,"WeightKG", "{0:#,##0.0}") %>
															</asp:label>
														</ItemTemplate>
														<FooterStyle HorizontalAlign="Left"></FooterStyle>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Qty">
														<HeaderStyle HorizontalAlign="Center" Width="70px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<asp:label id="lblQuantity" runat="server">
																<%# DataBinder.Eval(Container.DataItem,"Quantity", "{0:#,##0}") %>
															</asp:label>
														</ItemTemplate>
														<FooterStyle HorizontalAlign="Center"></FooterStyle>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="SIP Qty">
														<HeaderStyle HorizontalAlign="Center" Width="70px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<asp:label id="Label41" runat="server">
																<%# DataBinder.Eval(Container.DataItem,"InitialSIPCount", "{0:#,##0}") %>
															</asp:label>
														</ItemTemplate>
														<FooterStyle HorizontalAlign="Center"></FooterStyle>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Date Received">
														<HeaderStyle HorizontalAlign="Center" Width="130px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<asp:label id="Label42" runat="server">
																<%# DataBinder.Eval(Container.DataItem,"ReceivedIntoBond", "{0:dd/MM/yyyy HH:mm}") %>
															</asp:label>
														</ItemTemplate>
														<FooterStyle HorizontalAlign="Center"></FooterStyle>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Rec�d By">
														<HeaderStyle HorizontalAlign="Center" Width="100px"></HeaderStyle>
														<ItemStyle HorizontalAlign="Center"></ItemStyle>
														<ItemTemplate>
															<asp:label id="Label43" runat="server">
																<%# DataBinder.Eval(Container.DataItem,"ReceivedIntoBondBy") %>
															</asp:label>
														</ItemTemplate>
														<FooterStyle HorizontalAlign="Center"></FooterStyle>
													</asp:TemplateColumn>
												</Columns>
											</asp:datagrid></td>
									</tr>
								</TABLE>
							</td>
						</tr>
					</tbody>
				</TABLE>
			</div>
			<INPUT 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition> <INPUT type="hidden" name="hdnRefresh">
		</form>
	</body>
</HTML>
