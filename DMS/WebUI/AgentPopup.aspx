<%@ Page language="c#" Codebehind="AgentPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.AgentPopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AgentPopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<script>
			function CloseWindow()
			{
				//set return value of the dialog box or dialog result
				//top.returnValue = document.getElementById("lstCustomers").value;

				//close the dialog window
				window.close();
			}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="AgentPopup" method="post" runat="server">
			<asp:textbox id="txtAgentId" style="Z-INDEX: 106; LEFT: 47px; POSITION: absolute; TOP: 51px" runat="server" CssClass="textField" Width="123px"></asp:textbox>
			<asp:label id="lblErrorMsg" style="Z-INDEX: 108; LEFT: 47px; POSITION: absolute; TOP: 88px" runat="server" Width="501px" CssClass="errorMsgColor" Height="32px"></asp:label>
			<asp:button id="btnRefresh" style="DISPLAY: none; Z-INDEX: 107; LEFT: 9px; POSITION: absolute; TOP: 8px" runat="server" Width="1px" Height="23px" Text="HIDDEN"></asp:button>
			<asp:label id="lblAgentID" style="Z-INDEX: 101; LEFT: 47px; POSITION: absolute; TOP: 20px" runat="server" Height="16px" CssClass="tableLabel" Width="100px">Agent ID</asp:label>
			<asp:label id="lblAgentName" style="Z-INDEX: 103; LEFT: 200px; POSITION: absolute; TOP: 20px" runat="server" Height="16px" CssClass="tableLabel" Width="108px">Agent Name</asp:label>
			<asp:textbox id="txtAgentName" style="Z-INDEX: 104; LEFT: 200px; POSITION: absolute; TOP: 51px" runat="server" CssClass="textField" Width="116px"></asp:textbox>
			<asp:button id="btnSearch" style="Z-INDEX: 105; LEFT: 342px; POSITION: absolute; TOP: 51px" runat="server" Height="21" CssClass="buttonProp" Width="84px" CausesValidation="False" Text="Search"></asp:button>
			<asp:button id="btnOk" style="Z-INDEX: 102; LEFT: 431px; POSITION: absolute; TOP: 51px" runat="server" CssClass="buttonProp" Width="84px" CausesValidation="False" Text="Close"></asp:button>
			<asp:datagrid id="dgAgent" runat="server" CssClass="gridHeading" BorderStyle="None" BorderWidth="1px" CellPadding="3" BackColor="White" BorderColor="#CCCCCC" AutoGenerateColumns="False" AllowPaging="True" PageSize="4" OnPageIndexChanged="Paging" style="Z-INDEX: 109; LEFT: 45px; POSITION: absolute; TOP: 119px" Width="638px">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
				<ItemStyle CssClass="popupGridField"></ItemStyle>
				<HeaderStyle CssClass="gridHeading"></HeaderStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="agentid" HeaderText="Agent ID"></asp:BoundColumn>
					<asp:BoundColumn DataField="agent_name" HeaderText="Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="agent_address1" HeaderText="Address 1"></asp:BoundColumn>
					<asp:BoundColumn DataField="agent_address2" HeaderText="Address 2"></asp:BoundColumn>
					<asp:BoundColumn DataField="zipcode" HeaderText="Postal Code" HeaderStyle-Width="100px"></asp:BoundColumn>
					<asp:BoundColumn DataField="country" HeaderText="Country"></asp:BoundColumn>
					<asp:BoundColumn DataField="pod_slip_required" HeaderText="pod_slip_required" Visible="False"></asp:BoundColumn>
					<asp:BoundColumn DataField="telephone" HeaderText="Telephone" Visible="False"></asp:BoundColumn>
					<asp:BoundColumn DataField="fax" HeaderText="Fax" Visible="False"></asp:BoundColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select"></asp:ButtonColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
			</asp:datagrid>
		</form>
	</body>
</HTML>
