using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.applicationpages;
using TIESClasses;
using TIESDAL;
using com.ties;
using com.ties.DAL;
using com.ties.classes;
using com.common.util;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using BarcodeLib;
using System.IO;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CustomsMasterAWBEntry.
	/// </summary>
	public class CustomsMasterAWBEntry : BasePage
	{
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnVerify;
		protected System.Web.UI.WebControls.Button btExport;
		protected System.Web.UI.WebControls.Label Label35;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.TextBox txtDepartureDes;
		protected System.Web.UI.WebControls.TextBox txtArrivalDes;
		protected System.Web.UI.WebControls.TextBox txtCarrierDes;
		protected System.Web.UI.WebControls.TextBox txtModeOfTransportDes;
		protected System.Web.UI.WebControls.Label Label30;
		protected System.Web.UI.WebControls.Label Label31;
		protected System.Web.UI.WebControls.Label Label32;
		protected System.Web.UI.WebControls.Label Label34;
		protected System.Web.UI.WebControls.Label Label36;
		protected System.Web.UI.WebControls.Label Label38;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
	
		private string appID = null;
		private string enterpriseID = null;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.TextBox MasterAWB_no;
		protected System.Web.UI.WebControls.TextBox ShipFlightNo;
		protected System.Web.UI.WebControls.DropDownList CustomsOffice;
		protected com.common.util.msTextBox DepartureDate;
		protected System.Web.UI.WebControls.TextBox DepartureCity;
		protected System.Web.UI.WebControls.DropDownList DepartureCountry;
		protected com.common.util.msTextBox ArrivalDate;
		protected com.common.util.msTextBox ArrivalTime;
		protected System.Web.UI.WebControls.TextBox ArrivalCity;
		protected System.Web.UI.WebControls.DropDownList ArrivalCountry;
		protected System.Web.UI.WebControls.DropDownList Carrier;
		protected System.Web.UI.WebControls.DropDownList ModeOfTransport;
		protected com.common.util.msTextBox TotalWeight;
		protected System.Web.UI.WebControls.TextBox FolioNumber;
		protected System.Web.UI.WebControls.TextBox TotalAWBs;
		protected System.Web.UI.WebControls.TextBox txtTotalWeightDesc;
		protected System.Web.UI.WebControls.TextBox txtTotalAWBsDesc;
		protected System.Web.UI.WebControls.TextBox VerifiedBy;
		protected System.Web.UI.WebControls.TextBox VerifiedDT;
		protected System.Web.UI.WebControls.TextBox ExportedBy;
		protected System.Web.UI.WebControls.TextBox ExportedDT;
		protected System.Web.UI.WebControls.TextBox txtCustomsOfficeDesc;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGrd;
		protected System.Web.UI.WebControls.DataGrid GrdAWBDetails;
		protected System.Web.UI.WebControls.Button btnClientEvent;
		protected System.Web.UI.WebControls.TextBox UpdatedBy;
		protected System.Web.UI.WebControls.TextBox UpdatedDT;
		protected System.Web.UI.WebControls.Label Label33;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.WebControls.Label Label24;
		protected System.Web.UI.WebControls.DropDownList ConsolidationShipper ;
		protected System.Web.UI.WebControls.TextBox ConsolidatorName;
		protected System.Web.UI.WebControls.TextBox ConsolidatorAddress1;
		protected System.Web.UI.WebControls.TextBox ConsolidatorAddress2;
		protected System.Web.UI.WebControls.DropDownList ConsolidatorCountry;
		protected System.Web.UI.WebControls.TextBox ConsolidatorCountry_Desc;
		protected System.Web.UI.WebControls.Label Label27;
		protected System.Web.UI.WebControls.Label Label28;
		protected System.Web.UI.WebControls.Button btnVerifyClient;
		protected System.Web.UI.WebControls.Label Label29;
		protected System.Web.UI.WebControls.Label Label37;
		protected System.Web.UI.WebControls.Label Label39;
		protected System.Web.UI.WebControls.Label Label40;
		protected System.Web.UI.WebControls.TextBox Textbox2;
		protected System.Web.UI.WebControls.TextBox Textbox3;
		protected System.Web.UI.WebControls.DropDownList ddlXMLFormat;
		protected System.Web.UI.WebControls.TextBox txtFileName;
		protected System.Web.UI.HtmlControls.HtmlGenericControl fsOtherData;
		protected System.Web.UI.HtmlControls.HtmlInputFile fileUpload;
		protected System.Web.UI.WebControls.Button btnImport;
		protected System.Web.UI.WebControls.TextBox txtImportedFlightNo;
		protected System.Web.UI.WebControls.TextBox txtImportedTransportMode;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Button btnBrowse;
		protected System.Web.UI.HtmlControls.HtmlInputFile inFile;
		protected System.Web.UI.WebControls.Button btnOverallStatus;
		protected System.Web.UI.WebControls.Button btExportV2;
		protected System.Web.UI.WebControls.CheckBox IgnoreMAWBValidation;
		private string userID = null;

		public bool AddItemGrid
		{
			get
			{
				if(ViewState["AddGrid"]==null)
				{
					return false;
				}
				else
				{
					return (bool)ViewState["AddGrid"];
				}
			}
			set
			{
				ViewState["AddGrid"]=value;
			}
		}


		public int DSMode
		{
			get
			{
				if(ViewState["iDSMode"]==null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["iDSMode"];
				}
			}
			set
			{
				ViewState["iDSMode"]=value;
			}
		}

		public int DSOperation
		{
			get
			{
				if(ViewState["iDSOperation"]==null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["iDSOperation"];
				}
			}
			set
			{
				ViewState["iDSOperation"]=value;
			}
		}

		public bool IsBondStore
		{
			get
			{
				if(ViewState["IsBondStore"]==null)
				{
					return false;
				}
				else
				{
					return (bool)ViewState["IsBondStore"];
				}
			}
			set
			{
				ViewState["IsBondStore"]=value;
			}
		}


		public string XMLExportFolder
		{
			get
			{
				if(ViewState["XMLExportFolder"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["XMLExportFolder"];
				}
			}
			set
			{
				ViewState["XMLExportFolder"]=value;
			}
		}

		public string MiniLicensePlateTemplate
		{
			get
			{
				if(ViewState["MiniLicensePlateTemplate"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["MiniLicensePlateTemplate"];
				}
			}
			set
			{
				ViewState["MiniLicensePlateTemplate"]=value;
			}
		}


		private void Page_Load(object sender, System.EventArgs e)
		{
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();
			
			if(this.IsPostBack==false)
			{
				DataSet ds = SysDataMgrDAL.UserAssignedRoles(appID,enterpriseID,userID,"BONDSTORE");
				if(ds.Tables[0].Rows.Count>0 && ds.Tables[0].Rows[0]["IsAssigned"].ToString()=="1")
				{
					IsBondStore=true;
				}
				else
				{
					IsBondStore=false;
				}
				
				DataSet dsConfig = CustomsDAL.GetCustomsMAWBConfigurations(appID,enterpriseID);
				foreach(DataRow dr in dsConfig.Tables[0].Rows)
				{
					if(dr["key"] != null && dr["key"].ToString().ToLower() =="xmlexportfolder")
					{
						XMLExportFolder = dr["value"].ToString();
					}
					if(dr["key"] != null && dr["key"].ToString().ToLower() =="minilicenseplatetemplate")
					{
						MiniLicensePlateTemplate = dr["value"].ToString();
					}
				}

				clearscreen();
				//BindControl();
				btnBrowse.Attributes.Add("OnClick", "BrowseFileChange()");
				SetInitialFocus(MasterAWB_no);
				//this.btExport.Attributes.Add("onclick", "javascript:document.getElementById('"+ this.btExport.ClientID + "').disabled=true;" + GetPostBackEventReference(this.btExport));
			}
			this.btExport.Attributes.Add("onclick", "javascript:document.getElementById('"+ this.btExport.ClientID + "').disabled=true;" + GetPostBackEventReference(this.btExport));
			this.btExportV2.Attributes.Add("onclick", "javascript:document.getElementById('"+ this.btExportV2.ClientID + "').disabled=true;" + GetPostBackEventReference(this.btExportV2));

//			string script="javascript:document.getElementById('"+ this.btnImport.ClientID + "').disabled=true;";
//			script +="javascript:document.getElementById('"+ this.btnQry.ClientID + "').disabled=true;";
//			script +="javascript:document.getElementById('"+ this.btnBrowse.ClientID + "').disabled=true;";
			
			this.btnImport.Attributes.Add("OnClick", "disableImport();" + GetPostBackEventReference(this.btnImport)+";");
			//Session["VerifiedBy"] = null;
			//Session["StrverifiedDT"] = null;
			//btnBrowseManifestXML.Attributes.Add("onclick","ClickBrowseFile();");
		}


		private void Page_Unload(object sender, System.EventArgs e)
		{

		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnVerify.Click += new System.EventHandler(this.btnVerify_Click);
			this.btExport.Click += new System.EventHandler(this.btExport_Click);
			this.btExportV2.Click += new System.EventHandler(this.btExportV2_Click);
			this.btnClientEvent.Click += new System.EventHandler(this.btnClientEvent_Click);
			this.btnVerifyClient.Click += new System.EventHandler(this.btnVerifyClient_Click);
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			this.btnOverallStatus.Click += new System.EventHandler(this.btnOverallStatus_Click);
			this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
			this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
			this.ConsolidationShipper.SelectedIndexChanged += new System.EventHandler(this.ConsolidationShipper_SelectedIndexChanged);
			this.ConsolidatorCountry.SelectedIndexChanged += new System.EventHandler(this.ExporterCountry_SelectedIndexChanged);
			this.CustomsOffice.SelectedIndexChanged += new System.EventHandler(this.CustomsOffice_SelectedIndexChanged);
			this.DepartureCountry.SelectedIndexChanged += new System.EventHandler(this.DepartureCountry_SelectedIndexChanged);
			this.ArrivalCountry.SelectedIndexChanged += new System.EventHandler(this.ArrivalCountry_SelectedIndexChanged);
			this.Carrier.SelectedIndexChanged += new System.EventHandler(this.Carrier_SelectedIndexChanged);
			this.ModeOfTransport.SelectedIndexChanged += new System.EventHandler(this.ModeOfTransport_SelectedIndexChanged);
			this.GrdAWBDetails.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.GrdAWBDetails_ItemCommand);
			this.GrdAWBDetails.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.GrdAWBDetails_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void clearscreen()
		{
			Session["MAWBNumberverallStatus"]=null;
			lblError.Text="";
			MasterAWB_no.Text="";
			MasterAWB_no.Enabled=true;
			ConsolidationShipper.Items.Clear();
			ConsolidatorName.Text="";
			ConsolidatorAddress1.Text="";
			ConsolidatorAddress2.Text="";
			ConsolidatorCountry.Items.Clear();
			ConsolidatorCountry_Desc.Text="";

			ShipFlightNo.Text="";
			ShipFlightNo.Enabled=true;
			DepartureDate.Text="";
			DepartureCity.Text="";
			DepartureCountry.Items.Clear();
			txtDepartureDes.Text="";
			ArrivalDate.Text="";
			ArrivalTime.Text="";
			ArrivalCity.Text="";
			ArrivalCountry.Items.Clear();
			txtArrivalDes.Text="";
			Carrier.Items.Clear();
			txtCarrierDes.Text="";
			ModeOfTransport.Items.Clear();
			txtModeOfTransportDes.Text="";
			FolioNumber.Text="";
			TotalWeight.Text="";
			txtTotalWeightDesc.Text="";
			TotalAWBs.Text="";
			txtTotalAWBsDesc.Text="";
			VerifiedBy.Text="";
			VerifiedDT.Text="";
			ExportedBy.Text="";
			ExportedDT.Text="";
			UpdatedBy.Text="";
			UpdatedDT.Text="";

			txtFileName.Text="";
			txtImportedFlightNo.Text="";
			txtImportedTransportMode.Text="";

			DataSet dsConsolidatorDetails = SysDataMgrDAL.GetConsolidatorDetails(appID,enterpriseID,null);
			ConsolidationShipper.DataSource=dsConsolidatorDetails;
			ConsolidationShipper.DataTextField="DropDownListValue";
			ConsolidationShipper.DataValueField="DropDownListValue";
			ConsolidationShipper.DataBind();

			Session["ConsolidatorName"]=null;
			Session["ConsolidatorAddress1"]=null;
			Session["ConsolidatorAddress2"]=null;
			Session["ConsolidatorCountry"]=null;
			Session["ConsolidatorCountry_Desc"]=null;

			BindMasterAWBDetail(null);

			AddItemGrid=false;
			btnQry.Enabled=true;
			btnExecQry.Enabled=true;
			btnInsert.Enabled=true;
			btnSave.Enabled=false;
			btnVerify.Enabled=false;
			btExport.Enabled=false;	
			btExportV2.Enabled=false;		
			fsOtherData.Visible=false;
			btnBrowse.Enabled=true;
			inFile.Disabled=false;
			btnDelete.Enabled=false;
			btnImport.Enabled=false;
			//btnBrowseManifestXML.Enabled=true;
			//btnImportManifestXML.Enabled=true;
			GrdAWBDetails.DataSource=CustomsDAL.AWBDetailDataTable();
			GrdAWBDetails.DataBind();
		}


		private void BindControl()
		{
			
			DataSet dsExporterCountry = CustomJobCompilingDAL.GetCountryDefaultValue(appID,enterpriseID);
			DataSet dsTransportMode = CustomJobCompilingDAL.GetDropDownListDefaultDisplayValue(appID,enterpriseID,"TransportModeASYCUDA");
			DataSet dsDepartureCountry = CustomJobCompilingDAL.GetCountry(appID,enterpriseID);
			DataSet dsArrivalCountry = CustomJobCompilingDAL.GetCountry(appID,enterpriseID);
			DataSet dsCustomsOffice = CustomJobCompilingDAL.GetDropDownListDefaultDisplayValue(appID,enterpriseID,"CustomsOfficeASYCUDA");
			DataSet dsCarrier = CustomJobCompilingDAL.GetDropDownListDisplayValue(appID,enterpriseID,"CarrierASYCUDA");

			DataRow[] defaultTransport = dsTransportMode.Tables[0].Select("DefaultValue=1");
			DataRow[] defaultCustomsOffice = dsCustomsOffice.Tables[0].Select("DefaultValue=1");
			DataRow[] defaultCarrier = dsCarrier.Tables[0].Select("DefaultValue=1");

			DepartureDate.Text=DateTime.Now.ToString("dd/MM/yyyy");
			ArrivalDate.Text=DateTime.Now.ToString("dd/MM/yyyy");
			ArrivalCity.Text="POM";



			ConsolidatorCountry.DataSource=dsExporterCountry;
			ConsolidatorCountry.DataTextField="CountryCode";
			ConsolidatorCountry.DataValueField="CountryCode";
			ConsolidatorCountry.DataBind();
			ConsolidatorCountry_Desc.Text="";
			if(dsExporterCountry.Tables[0].Rows.Count>0)
			{
				ConsolidatorCountry_Desc.Text=CustomJobCompilingDAL.GetCountryName(appID,enterpriseID,ConsolidatorCountry.SelectedValue);
			}

			txtModeOfTransportDes.Text="";
			txtDepartureDes.Text="";
			txtArrivalDes.Text="";
			txtCustomsOfficeDesc.Text="";
			txtCarrierDes.Text="";

			ModeOfTransport.DataSource=dsTransportMode;
			ModeOfTransport.DataTextField="DropDownListDisplayValue";
			ModeOfTransport.DataValueField="CodedValue";
			ModeOfTransport.DataBind();
			if(defaultTransport.Length > 0)
			{
				ModeOfTransport.SelectedValue=defaultTransport[0]["CodedValue"].ToString();
				txtModeOfTransportDes.Text=defaultTransport[0]["Description"].ToString();
			}


			Carrier.DataSource=dsCarrier;
			Carrier.DataTextField="DropDownListDisplayValue";
			Carrier.DataValueField="CodedValue";
			Carrier.DataBind();
			if(defaultCarrier.Length > 0)
			{
				Carrier.SelectedValue=defaultCarrier[0]["CodedValue"].ToString();
				txtCarrierDes.Text=defaultCarrier[0]["Description"].ToString();
			}

			CustomsOffice.DataSource=dsCustomsOffice;
			CustomsOffice.DataTextField="DropDownListDisplayValue";
			CustomsOffice.DataValueField="CodedValue";
			CustomsOffice.DataBind();
			if(defaultCustomsOffice.Length > 0)
			{
				CustomsOffice.SelectedValue=defaultCustomsOffice[0]["CodedValue"].ToString();
				txtCustomsOfficeDesc.Text=defaultCustomsOffice[0]["Description"].ToString();
			}


			DepartureCountry.DataSource=dsDepartureCountry;
			DepartureCountry.DataTextField="CountryCode";
			DepartureCountry.DataValueField="CountryCode";
			DepartureCountry.DataBind();
			if(dsDepartureCountry.Tables[0].Rows.Count>0)
			{
				txtDepartureDes.Text=dsDepartureCountry.Tables[0].Rows[0]["CountryName"].ToString();
			}

			ArrivalCountry.DataSource=dsArrivalCountry;
			ArrivalCountry.DataTextField="CountryCode";
			ArrivalCountry.DataValueField="CountryCode";
			ArrivalCountry.DataBind();
			try
			{
				ArrivalCountry.SelectedValue="PG";
				txtArrivalDes.Text=CustomJobCompilingDAL.GetCountryName(appID,enterpriseID,ArrivalCountry.SelectedValue);
			}
			catch
			{
				txtArrivalDes.Text="";
			}

			DSMode = (int)ScreenMode.None;
			DSOperation = (int)Operation.None;
		}


		private void ExecQry()
		{
			if(Session["MAWBNumberverallStatus"] != null && Session["MAWBNumberverallStatus"].ToString() != "")
			{
				MasterAWB_no.Text = Session["MAWBNumberverallStatus"].ToString();
				Session["MAWBNumberverallStatus"]=null;
			}
			DataTable dtParams = CustomsDAL.MasterAWBEntryParameter();
			System.Data.DataRow drNew = dtParams.NewRow();
			drNew["action"] ="0";
			drNew["enterpriseid"] =enterpriseID;
			drNew["userloggedin"] =userID;
			if(MasterAWB_no.Text.Trim() !="")
			{
				drNew["MasterAWB_no"] =MasterAWB_no.Text.Trim();
			}
			else
			{
				drNew["MasterAWB_no"] = DBNull.Value;
				lblError.Text="Master AWB number is required.";
				return;
			}
					
			dtParams.Rows.Add(drNew);
			DataSet ds = CustomsDAL.Customs_MAWB(appID,enterpriseID,userID,dtParams);
			System.Data.DataTable dtStatus = ds.Tables[0];
			System.Data.DataTable dtAWB= ds.Tables[1];
			System.Data.DataTable dtAWBDetail= ds.Tables[2];
			if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
			{				
				clearscreen();
				BindControl();
				BindMasterAWBInfo(dtAWB);	
				MasterAWB_no.Enabled=false;
				GrdAWBDetails.Enabled=true;
				BindMasterAWBDetail(dtAWBDetail);
				btnExecQry.Enabled=false;
				btnInsert.Enabled=false;
				btnSave.Enabled=true;
				btnVerify.Enabled=true;
				btExport.Enabled=true;
				btExportV2.Enabled=true;
				btnBrowse.Enabled=false;
				btnDelete.Enabled=true;

			}
			else
			{
				lblError.Text=(dtStatus.Rows.Count>0?dtStatus.Rows[0]["ErrorMessage"].ToString():"");
			}

			DSMode = (int)ScreenMode.ExecuteQuery;
			DSOperation = (int)Operation.None;
		}


		private void ExecQryMAWB_Detail()
		{
			DataTable dtParams = CustomsDAL.MasterAWBDetailParameter();
			System.Data.DataRow drNew = dtParams.NewRow();
			drNew["action"] ="1";
			drNew["enterpriseid"] =enterpriseID;
			drNew["userloggedin"] =userID;
			if(MasterAWB_no.Text.Trim() !="")
			{
				drNew["MasterAWB_no"] =MasterAWB_no.Text.Trim();
			}
			else
			{
				drNew["MasterAWB_no"] = DBNull.Value;
			}		
			if(ShipFlightNo.Text.Trim() !="")
			{
				drNew["ShipFlightNo"] =ShipFlightNo.Text.Trim();
			}
			else
			{
				drNew["ShipFlightNo"] = DBNull.Value;
			}
			drNew["consignment_no"] = DBNull.Value;

			dtParams.Rows.Add(drNew);
			DataSet ds = CustomsDAL.Customs_MAWB_Detail(appID,enterpriseID,userID,dtParams);
			System.Data.DataTable dtStatus = ds.Tables[0];
			System.Data.DataTable dtAWBDetail= ds.Tables[1];
			if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
			{
				BindMasterAWBDetail(dtAWBDetail);
			}
		}


		private void BindMasterAWBInfo(System.Data.DataTable dtAWB)
		{
			Session["ConsolidatorName"]=null;
			Session["ConsolidatorAddress1"]=null;
			Session["ConsolidatorAddress2"]=null;
			Session["ConsolidatorCountry"]=null;
			Session["ConsolidatorCountry_Desc"]=null;

			if(dtAWB.Rows.Count>0)
			{
				DataRow dr = dtAWB.Rows[0];
				MasterAWB_no.Text=dr["MAWBNumber"].ToString();
				ShipFlightNo.Text=dr["ShipFlightNo"].ToString();
				if(ShipFlightNo.Text.Trim() != "")
				{
					ShipFlightNo.Enabled=false;
				}
				else
				{
					ShipFlightNo.Enabled=true;
				}

				DepartureDate.Text=(dr["DepartureDate"] != DBNull.Value ? DateTime.Parse(dr["DepartureDate"].ToString()).ToString("dd/MM/yyyy") :"");
				DepartureCity.Text=dr["DepartureCity"].ToString();

				ConsolidatorName.Text=dr["ConsolidatorName"].ToString();
				ConsolidatorAddress1.Text=dr["ConsolidatorAddress1"].ToString();
				ConsolidatorAddress2.Text=dr["ConsolidatorAddress2"].ToString();
				if(dr["ConsolidatorCountry"] != DBNull.Value && dr["ConsolidatorCountry"].ToString().Trim() !="")
				{
					ConsolidatorCountry.SelectedValue=dr["ConsolidatorCountry"].ToString();
					ConsolidatorCountry_Desc.Text=CustomJobCompilingDAL.GetCountryName(appID,enterpriseID,ConsolidatorCountry.SelectedValue);
				}

				Session["ConsolidatorName"]=dr["ConsolidatorName"].ToString();
				Session["ConsolidatorAddress1"]=dr["ConsolidatorAddress1"].ToString();
				Session["ConsolidatorAddress2"]=dr["ConsolidatorAddress2"].ToString();
				Session["ConsolidatorCountry"]=dr["ConsolidatorCountry"].ToString();
				Session["ConsolidatorCountry_Desc"]=ConsolidatorCountry_Desc.Text;
				
				txtCustomsOfficeDesc.Text="";
				if(dr["CustomsOffice"] != DBNull.Value && dr["CustomsOffice"].ToString().Trim() !="")
				{
					CustomsOffice.SelectedValue=dr["CustomsOffice"].ToString();
					txtCustomsOfficeDesc.Text=dr["CustomsOffice_Desc"].ToString();
				}

				txtDepartureDes.Text="";
				if(dr["DepartureCountry"] != DBNull.Value && dr["DepartureCountry"].ToString().Trim() !="")
				{
					DepartureCountry.SelectedValue=dr["DepartureCountry"].ToString();
					txtDepartureDes.Text=dr["DepartureCountry_Desc"].ToString();
				}
				
				ArrivalDate.Text=(dr["ArrivalDate"] != DBNull.Value ? DateTime.Parse(dr["ArrivalDate"].ToString()).ToString("dd/MM/yyyy") :"");
				ArrivalTime.Text=(dr["ArrivalTime"] != DBNull.Value ? DateTime.Parse(dr["ArrivalTime"].ToString()).ToString("HH:mm") :"");
				ArrivalCity.Text=dr["ArrivalCity"].ToString();

				txtArrivalDes.Text="";
				if(dr["ArrivalCountry"] != DBNull.Value && dr["ArrivalCountry"].ToString().Trim() !="")
				{
					ArrivalCountry.SelectedValue=dr["ArrivalCountry"].ToString();
					txtArrivalDes.Text=dr["ArrivalCountry_Desc"].ToString();
				}

				txtCarrierDes.Text="";
				if(dr["Carrier"] != DBNull.Value && dr["Carrier"].ToString().Trim() !="")
				{
					Carrier.SelectedValue=dr["Carrier"].ToString();
					txtCarrierDes.Text=dr["Carrier_Desc"].ToString();
				}

				txtModeOfTransportDes.Text="";
				if(dr["ModeOfTransport"] != DBNull.Value && dr["ModeOfTransport"].ToString().Trim() !="")
				{
					ModeOfTransport.SelectedValue=dr["ModeOfTransport"].ToString();
					txtModeOfTransportDes.Text=dr["ModeOfTransport_Desc"].ToString();
				}

				FolioNumber.Text=dr["FolioNumber"].ToString();

				TotalWeight.Text=(dr["TotalWeight"] != DBNull.Value?Decimal.Parse(dr["TotalWeight"].ToString()).ToString("N1"): "0");
				txtTotalWeightDesc.Text=(dr["RemainingWeight"] != DBNull.Value?Decimal.Parse(dr["RemainingWeight"].ToString()).ToString("N1"): "0");

				TotalAWBs.Text=(dr["TotalAWBs"] != DBNull.Value?Decimal.Parse(dr["TotalAWBs"].ToString()).ToString("N0"): "0");
				txtTotalAWBsDesc.Text=(dr["RemainingAWBs"] != DBNull.Value?Decimal.Parse(dr["RemainingAWBs"].ToString()).ToString("N0"): "0");

				VerifiedBy.Text=dr["VerifiedBy"].ToString();
				VerifiedDT.Text=(dr["VerifiedDT"] != DBNull.Value ? DateTime.Parse(dr["VerifiedDT"].ToString()).ToString("dd/MM/yyyy HH:mm") :"");
				ExportedBy.Text=dr["ExportedBy"].ToString();
				ExportedDT.Text=(dr["ExportedDT"] != DBNull.Value ? DateTime.Parse(dr["ExportedDT"].ToString()).ToString("dd/MM/yyyy HH:mm") :"");
				UpdatedBy.Text=dr["InsertedBy"].ToString();
				UpdatedDT.Text=(dr["InsertedDT"] != DBNull.Value ? DateTime.Parse(dr["InsertedDT"].ToString()).ToString("dd/MM/yyyy HH:mm") :"");

				if(dr["Imported"] != DBNull.Value && dr["Imported"].ToString().Trim() =="1")
				{
					fsOtherData.Visible=true;
				}
				txtImportedFlightNo.Text=dr["ImportedFlightNo"].ToString();
				txtImportedTransportMode.Text=dr["ImportedTransportMode"].ToString();

				AddItemGrid=false;
				if(UpdatedBy.Text.Trim() != "")
				{
					AddItemGrid=true;
				}
			}
		}


		private void BindMasterAWBDetail(System.Data.DataTable dtDetail)
		{
			GrdAWBDetails.DataSource=dtDetail;
			GrdAWBDetails.DataBind();
		}


		private void btnQry_Click(object sender, System.EventArgs e)
		{
			clearscreen();
			//BindControl();
			SetInitialFocus(MasterAWB_no);
		}


		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			if(Utility.GetConsNoRegex(MasterAWB_no.Text.Trim())==false)
			{
				lblError.Text = "Master AWB Number is invalid.";
				SetInitialFocus(MasterAWB_no);
				return;
			}
			ExecQry();
			if(lblError.Text.Trim()=="")
			{
				//btnBrowseManifestXML.Enabled=false;
				//btnImportManifestXML.Enabled=false;
				SetInitialFocus(ConsolidatorName);
			}
			else
			{
				SetInitialFocus(MasterAWB_no);
			}					
		}


		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			lblError.Text="";
			if(Utility.GetConsNoRegex(MasterAWB_no.Text.Trim())==false)
			{
				lblError.Text = "Master AWB Number is invalid.";
				SetInitialFocus(MasterAWB_no);
				return;
			}
			DataTable dtParams = CustomsDAL.MasterAWBEntryParameter();
			System.Data.DataRow drNew = dtParams.NewRow();
			drNew["action"] ="1";
			drNew["enterpriseid"] =enterpriseID;
			drNew["userloggedin"] =userID;
			if(MasterAWB_no.Text.Trim() !="")
			{
				drNew["MasterAWB_no"] =MasterAWB_no.Text.Trim();
			}
			else
			{
				drNew["MasterAWB_no"] = DBNull.Value;
				lblError.Text="Master AWB number is required.";
				return;
			}
			drNew["IgnoreMAWBValidation"] = (IgnoreMAWBValidation.Checked==true?1:0);

			dtParams.Rows.Add(drNew);
			DataSet ds = CustomsDAL.Customs_MAWB(appID,enterpriseID,userID,dtParams);
			System.Data.DataTable dtStatus = ds.Tables[0];
			System.Data.DataTable dtAWB= ds.Tables[1];
			System.Data.DataTable dtAWBDetail= ds.Tables[2];
			if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
			{				
				clearscreen();
				BindControl();
				BindMasterAWBInfo(dtAWB);
				BindMasterAWBDetail(dtAWBDetail);
				MasterAWB_no.Enabled=false;
				btnExecQry.Enabled=false;
				btnInsert.Enabled=false;
				btnSave.Enabled=true;
				btnVerify.Enabled=false;
				btExport.Enabled=false;
				btExportV2.Enabled=false;
				btnBrowse.Enabled=false;
				SetInitialFocus(ConsolidatorName);
			}
			else
			{
				lblError.Text=(dtStatus.Rows.Count>0?dtStatus.Rows[0]["ErrorMessage"].ToString():"");
				SetInitialFocus(MasterAWB_no);
			}

			DSMode = (int)ScreenMode.Insert;
			DSOperation = (int)Operation.Insert;
		}


		private void btnSave_Click(object sender, System.EventArgs e)
		{
			if(MasterAWB_no.Text.Trim() =="")
			{
				lblError.Text="Master AWB number is required";
				return;
			}

			DataTable dtParams = CustomsDAL.MasterAWBEntryParameter();
			System.Data.DataRow drNew = dtParams.NewRow();
			foreach(System.Data.DataColumn col in dtParams.Columns)
			{
				string colName = col.ColumnName;
				System.Web.UI.Control ctrol = this.Page.FindControl(colName);
				if(ctrol != null)
				{
					if(ctrol.GetType()==typeof(TextBox))
					{
						TextBox txt = (TextBox)ctrol;
						if(txt.Text.Trim() != "")
						{
							if(txt.ID=="sender_zipcode" || txt.ID=="recipient_zipcode")
							{
								drNew[colName]=txt.Text.ToUpper().Trim();
							}
							else
							{
								drNew[colName]=txt.Text.Trim();
							}							
						}						
					}
					else if(ctrol.GetType()==typeof(com.common.util.msTextBox))
					{
						com.common.util.msTextBox txt = (com.common.util.msTextBox)ctrol;
						if(txt.TextMaskType==MaskType.msDate)
						{
							drNew[colName]=DateTime.ParseExact(txt.Text,"dd/MM/yyyy",null).ToString("yyyy-MM-dd");
						}
						else
						{
							if(colName=="TotalWeight" || colName=="txtTotalWeightDesc")
							{
								drNew[colName]=txt.Text.Trim().Replace(",","");
							}
							else
							{
								drNew[colName]=txt.Text.Trim();
							}
						}
						
					}
					else if(ctrol.GetType()==typeof(DropDownList))
					{
						DropDownList ddl = (DropDownList)ctrol;
						drNew[colName]=ddl.SelectedValue;
					}				
				}
			}
			drNew["action"] ="2";
			drNew["enterpriseid"] =enterpriseID;
			drNew["userloggedin"] =userID;
			drNew["MasterAWB_no"] =MasterAWB_no.Text.Trim();
			
			dtParams.Rows.Add(drNew);

			DataSet ds = CustomsDAL.Customs_MAWB(appID,enterpriseID,userID,dtParams);
			System.Data.DataTable dtStatus = ds.Tables[0];
			System.Data.DataTable dtAWB = ds.Tables[1];
			if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
			{				
				ExecQry();
				MasterAWB_no.Enabled=false;
				lblError.Text = dtStatus.Rows[0]["ErrorMessage"].ToString();
			}
			else
			{
				lblError.Text=(dtStatus.Rows.Count>0?dtStatus.Rows[0]["ErrorMessage"].ToString():"");
			}
		}


		private void btnVerify_Click(object sender, System.EventArgs e)
		{
			lblError.Text = string.Empty;
			Session["MAWBNumber"] = MasterAWB_no.Text;
//			Session["VerifiedBy"]=null;
//			Session["StrverifiedDT"]=null;
//			if(Session["VerifiedBy"] != null && Session["StrverifiedDT"] != null)
//			{
//				VerifiedBy.Text = Session["VerifiedBy"].ToString();
//				VerifiedDT.Text = Session["StrverifiedDT"].ToString();
//			}

			string sUrl = "CustomsMasterAWBXMLVerification.aspx";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScriptPopupSetSize("openParentWindowCustomSize.js",paramList,400,1200);
			Utility.RegisterScriptString(sScript,"ShowPopupScript"+DateTime.Now.ToString("ddMMyyyyHHmmss"),this.Page);
		}


		private void ExportXMLDataText()
		{
			try
			{
				
				string itemList = string.Empty;
				string pathFile = CustomsDAL.GetPathCustomsMAWB(appID, enterpriseID);
				string fileName = "mawb" + DateTime.Now.ToString("yyyyMMddHHmm");

				DataSet dsResult = new DataSet();
				dsResult = CustomsDAL.Customs_MAWB_ExportXML(appID, enterpriseID, userID, MasterAWB_no.Text.Trim());
				
				this.lblError.Text = dsResult.Tables[0].Rows[0][1].ToString();
				if(!dsResult.Tables[0].Rows[0][0].ToString().Equals("0"))
					return;

				Response.Clear();
				Response.ContentType = "text/csv";
				Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}", fileName + ".txt"));
				Response.Write(dsResult.Tables[1].Rows[0][0].ToString());
				Context.Response.End();

			}
			catch (Exception ex)
			{
				this.lblError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}


		public static void SetInitialExportUser(Control control, string data1) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				System.Text.StringBuilder s = new System.Text.StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialExportUser()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].value = " + data1 + ";\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialExportUser;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("ExportUser", s.ToString()); 
			} 
		}


		private void btExport_Click(object sender, System.EventArgs e)
		{
			//ExportXMLDataText();
			try
			{				
				//ExecQry();

				lblError.Text = string.Empty;
				string itemList = string.Empty;
				string pathFile = CustomsDAL.GetPathCustomsMAWB(appID, enterpriseID);
				string fileName = "";
				try
				{
					if(MasterAWB_no.Text.Trim().Length<=8)
					{
						fileName=MasterAWB_no.Text.Trim();
					}
					else
					{
						int idx=MasterAWB_no.Text.Trim().Length - 8;
						fileName=MasterAWB_no.Text.Trim().Substring(idx);
					}
				}
				catch
				{
					fileName=MasterAWB_no.Text.Trim();
				}
				
				DataSet dsResult = new DataSet();
				dsResult = CustomsDAL.Customs_MAWB_ExportXML(appID, enterpriseID, userID, MasterAWB_no.Text.Trim());
				
				this.lblError.Text = dsResult.Tables[0].Rows[0][1].ToString();
				if(!dsResult.Tables[0].Rows[0][0].ToString().Equals("0"))
					return;

				//SetInitialExportUser(ExportedBy, dsResult.Tables[2].Rows[0]["ExportedBy"].ToString());
				//SetInitialExportUser(ExportedDT, dsResult.Tables[2].Rows[0]["ExportedDT"].ToString());

				ExportedBy.Text = dsResult.Tables[2].Rows[0]["ExportedBy"].ToString();
				//ExportedDT.Text = dsResult.Tables[2].Rows[0]["ExportedDT"].ToString("dd/MM/yyyy HH:mm:ss");
				ExportedDT.Text = DateTime.Parse(dsResult.Tables[2].Rows[0]["ExportedDT"].ToString()).ToString("dd/MM/yyyy HH:mm");									

//				string sScript = @"<script language='javascript'> 
//										document.getElementById('{0}').value = '{1}';
//										document.getElementById('{2}').value = '{3}';
//									</script>";
//				sScript = string.Format(sScript, ExportedBy.ClientID, dsResult.Tables[2].Rows[0]["ExportedBy"].ToString(), ExportedDT.ClientID, dsResult.Tables[2].Rows[0]["ExportedDT"].ToString());
//				
//				//Response.Write(sScript);
//				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

				//Response.Clear();
				//Response.ContentType = "text/csv";
				//Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}", fileName + ".txt"));
				//Response.Write(dsResult.Tables[1].Rows[0][0].ToString());


				//Response.End();
				String strUrl = "TempExprot.aspx";
				Session["FORMID"] = "ExportXMLAWB";
				Session["DSExport"] = dsResult; 

				Session["FileName"] = fileName;
				Session["XMLText"] = dsResult.Tables[1].Rows[0][0].ToString();
				
				ArrayList paramList = new ArrayList();
				
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentExport.js", paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				
				//ExecQry();
			}
			catch (Exception ex)
			{
				this.lblError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
			
		}


		private void DepartureCountry_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			txtDepartureDes.Text=CustomJobCompilingDAL.GetCountryName(appID,enterpriseID,DepartureCountry.SelectedValue);
			SetInitialFocus(DepartureCountry);
		}


		private void ArrivalCountry_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			txtArrivalDes.Text=CustomJobCompilingDAL.GetCountryName(appID,enterpriseID,ArrivalCountry.SelectedValue);
			SetInitialFocus(ArrivalCountry);
		}


		private void Carrier_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DataSet dsCarrier = CustomJobCompilingDAL.GetDropDownListDisplayValue(appID,enterpriseID,"CarrierASYCUDA",Carrier.SelectedValue);			
			txtCarrierDes.Text="";			
			if(dsCarrier.Tables[0].Rows.Count>0)
			{
				txtCarrierDes.Text=dsCarrier.Tables[0].Rows[0]["Description"].ToString();
			}
			SetInitialFocus(Carrier);
		}


		private void ModeOfTransport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DataSet dsTransportMode = CustomJobCompilingDAL.GetDropDownListDisplayValue(appID,enterpriseID,"TransportModeASYCUDA",ModeOfTransport.SelectedValue);			
			txtModeOfTransportDes.Text="";			
			if(dsTransportMode.Tables[0].Rows.Count>0)
			{
				txtModeOfTransportDes.Text=dsTransportMode.Tables[0].Rows[0]["Description"].ToString();
			}
			SetInitialFocus(ModeOfTransport);
		}


		private void CustomsOffice_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DataSet dsCustomsOffice = CustomJobCompilingDAL.GetDropDownListDisplayValue(appID,enterpriseID,"CustomsOfficeASYCUDA",CustomsOffice.SelectedValue);			
			txtCustomsOfficeDesc.Text="";			
			if(dsCustomsOffice.Tables[0].Rows.Count>0)
			{
				txtCustomsOfficeDesc.Text=dsCustomsOffice.Tables[0].Rows[0]["Description"].ToString();
			}
			SetInitialFocus(CustomsOffice);
		}


		private void GrdAWBDetails_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			Label lblMAWBNumber = (Label)e.Item.FindControl("lblMAWBNumber");
			Label lblConsignment_no = (Label)e.Item.FindControl("lblConsignment_no");
			Label lblShipFlightNo = (Label)e.Item.FindControl("lblShipFlightNo");
			Label lblMAWBHasBeenSaved = (Label)e.Item.FindControl("lblMAWBHasBeenSaved");
			Label lblSourceOfData = (Label)e.Item.FindControl("lblSourceOfData");

			lblError.Text="";
			if(lblMAWBHasBeenSaved != null && lblMAWBHasBeenSaved.Text=="0")
			{
				lblError.Text="Master AWB Number has not been saved.";
				return;
			}
			string MasterAWB_no="";
			string Consignment_no="";
			string ShipFlightNo="";
			if(lblMAWBNumber != null)
			{
				MasterAWB_no=lblMAWBNumber.Text;
			}
			if(lblConsignment_no != null)
			{
				Consignment_no=lblConsignment_no.Text;
			}
			if(lblShipFlightNo != null)
			{
				ShipFlightNo=lblShipFlightNo.Text;
			}

			string cmd = e.CommandName;
			string sUrl = "";
			if(cmd=="ADD_ITEM")
			{
				sUrl = "HouseAWBDetailEntry.aspx";
				sUrl+="?appID="+this.appID+"&enterpriseID="+this.enterpriseID+"&userID="+this.userID;
				sUrl+= "&ShipFlightNo="+this.ShipFlightNo.Text+"&MasterAWB_no="+this.MasterAWB_no.Text;;
				sUrl+="&DSOperation="+((int)Operation.Insert).ToString()+"&SourceOfData=1&DepartureCountry="+DepartureCountry.SelectedValue;
				
			}
			else if(cmd=="EDIT_ITEM")
			{
				LinkButton btnDeleteItem = (LinkButton)e.Item.FindControl("btnDeleteItem");

				sUrl = "HouseAWBDetailEntry.aspx";
				sUrl+="?appID="+this.appID+"&enterpriseID="+this.enterpriseID+"&userID="+this.userID;
				if(btnDeleteItem.Enabled==false)
				{
					sUrl+="&cons_no="+Consignment_no+"&MasterAWB_no="+MasterAWB_no;
				}
				else
				{
					sUrl+="&cons_no="+Consignment_no+"&ShipFlightNo="+ShipFlightNo+"&MasterAWB_no="+MasterAWB_no;
				}
				
				sUrl+="&DSOperation="+((int)Operation.Update).ToString()+"&SourceOfData="+lblSourceOfData.Text.Trim();				
			}
			else if(cmd=="DELETE_ITEM")
			{
				DataTable dtParams = CustomsDAL.MasterAWBDetailParameter();
				System.Data.DataRow drNew = dtParams.NewRow();
				drNew["action"] ="3";
				drNew["enterpriseid"] =enterpriseID;
				drNew["userloggedin"] =userID;
				if(MasterAWB_no !="")
				{
					drNew["MasterAWB_no"] =MasterAWB_no;
				}
				else
				{
					drNew["MasterAWB_no"] = DBNull.Value;
				}		
				if(ShipFlightNo !="")
				{
					drNew["ShipFlightNo"] =ShipFlightNo;
				}
				else
				{
					drNew["ShipFlightNo"] = DBNull.Value;
				}
				if(Consignment_no !="")
				{
					drNew["consignment_no"] =Consignment_no;
				}
				else
				{
					drNew["consignment_no"] = DBNull.Value;
				}
				dtParams.Rows.Add(drNew);
				DataSet ds = CustomsDAL.Customs_MAWB_Detail(appID,enterpriseID,userID,dtParams);
				System.Data.DataTable dtStatus = ds.Tables[0];
				if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
				{				
					ExecQry();
					lblError.Text = dtStatus.Rows[0]["ErrorMessage"].ToString();
				}
				else
				{
					lblError.Text=(dtStatus.Rows.Count>0?dtStatus.Rows[0]["ErrorMessage"].ToString():"");
				}

			}
			else if(cmd=="PRINT_ITEM")
			{
				
				string pdfFileName;
				DataSet dsLicensePlate = CustomsDAL.GetMiniLicensePlates(appID,enterpriseID,userID,MasterAWB_no,ShipFlightNo,Consignment_no);
				if(dsLicensePlate.Tables[0].Rows.Count > 0)
				{
					DataTable dtPrint = new DataTable();
					dtPrint.Columns.Add("consignment");
					dtPrint.Columns.Add("reference_name");
					dtPrint.Columns.Add("telephone");
					dtPrint.Columns.Add("address");
					dtPrint.Columns.Add("contactperson");
					dtPrint.Columns.Add("zip");
					dtPrint.Columns.Add("route1");
					dtPrint.Columns.Add("route2");
					dtPrint.Columns.Add("service");
					dtPrint.Columns.Add("derv");
					dtPrint.Columns.Add("province");
					dtPrint.Columns.Add("sender_name");
					dtPrint.Columns.Add("sender_tel");
					dtPrint.Columns.Add("cust_ref");
					dtPrint.Columns.Add("uid");
					dtPrint.Columns.Add("swbid");
					dtPrint.Columns.Add("dclocation");
					dtPrint.Columns.Add("dateprint");
					dtPrint.Columns.Add("datescan");
					dtPrint.Columns.Add("package");
					dtPrint.Columns.Add("Label");
                    dtPrint.Columns.Add("imgBarcode", System.Type.GetType("System.Byte[]"));

                    dtPrint = fillLicensePlateData(dsLicensePlate,dtPrint);

					//TIES.WebUI.ConsignmentNote.MiniLicensePlate crReportDocument = new TIES.WebUI.ConsignmentNote.MiniLicensePlate();
					TIES.WebUI.ConsignmentNote.LicensePlate crReportDocument = new TIES.WebUI.ConsignmentNote.LicensePlate();
					
					crReportDocument.ResourceName=MiniLicensePlateTemplate;
					ParameterFieldDefinitions paramFldDefs ;
					ParameterValues paramVals = new ParameterValues() ;
					ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();

					string printer = System.Configuration.ConfigurationSettings.AppSettings["PrinterName"].ToString();
					crReportDocument.PrintOptions.PrinterName = System.Configuration.ConfigurationSettings.AppSettings["PrinterName"].ToString();

                    //paramFldDefs = rptSource.DataDefinition.ParameterFields;
                    crReportDocument.SetDataSource(dtPrint);
                    paramFldDefs = crReportDocument.DataDefinition.ParameterFields;

					foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
					{
						switch(paramFldDef.ParameterFieldName)
						{
							case "label_test":
								paramDVal.Value = System.Configuration.ConfigurationSettings.AppSettings["label_test"].ToString();
								break;
							default:
								continue;
						}
						paramVals = paramFldDef.CurrentValues;
						paramVals.Add(paramDVal);
						paramFldDef.ApplyCurrentValues(paramVals);
					}

					ExportOptions crExportOptions  = new ExportOptions();
					DiskFileDestinationOptions crDiskFileDestinationOptions  = new DiskFileDestinationOptions();
 
					String Fname = "";
					string con_no="MPL"+Consignment_no+DateTime.Now.ToString("ddMMyyHHmmss");
					
					Fname = Server.MapPath(".\\Export") + "\\"+con_no+".pdf";

					System.Uri uri = HttpContext.Current.Request.Url;
					sUrl = uri.Scheme + "://" + uri.Host;
					string sPath = PathExport(uri.PathAndQuery);
					string strHost = Request.Url.Host;
					string strPort = Request.Url.Port.ToString();
					pdfFileName="";
					if (strPort != null )
					{
						if(strPort != String.Empty)
						{
							pdfFileName = sUrl+":"+strPort+sPath+"/Export/"+ con_no +".pdf";						
						}
						else
						{
							pdfFileName = sUrl+sPath+"/Export/"+ Consignment_no +".pdf";
						}
					}
				
					crDiskFileDestinationOptions = new DiskFileDestinationOptions();
					crDiskFileDestinationOptions.DiskFileName = Fname;
					crExportOptions = crReportDocument.ExportOptions;

					crExportOptions.DestinationOptions = crDiskFileDestinationOptions;
					crExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
					crExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;

					crExportOptions.FormatOptions = new PdfRtfWordFormatOptions();
					crReportDocument.Export();

                    crReportDocument.Close();
                    crReportDocument.Dispose();

					bool flgAutoPrint = CustomsDAL.IsAutoPrint(appID,enterpriseID,"LicensePlateDirectPrint",ArrivalCity.Text);
					if(flgAutoPrint == true ) 
					{	
						string strUrl = "Manifest/Popup_PrintLicensePlate_Direct.aspx";
						Session["AutoPrint"] = "onload=\"PrintRemotePDFFile()\"";
						Session["CreatePrint"] = "<object id=\"pdfDoc\"  name=\"pdfDoc\" classid=\"clsid:CA8A9780-280D-11CF-A24D-444553540000\" width=\"1px\" height=\"1px\" VIEWASTEXT><param name=\"SRC\" value=\""+pdfFileName+"\" /></object>";
						ArrayList paramList = new ArrayList();
						paramList.Add(strUrl);
						//String sScript = Utility.GetScript("openNewChildParamDirect.js",paramList);
						String sScript = Utility.GetScriptPopupSetSize("OpenSetSize.js",paramList,200,400);
						Utility.RegisterScriptString(sScript,"ShowPopupScript"+DateTime.Now.ToString("ddMMyyyyhhmmss"),this.Page);
						return;
					}
					else
					{
						string sFilePdf = Fname;
						//string sFilePdf = "C:\\Inetpub\\wwwroot\\PRD\\TIES\\WebUI\\Export\\" +conName+".pdf";		// For Test
						bool flgExists = false;
						do
						{
							if( IsfileExists(sFilePdf) == true )
							{
								flgExists = true;
							}
							else
							{
								flgExists = false;
								System.Threading.Thread.Sleep(3000);
							}
						} while (flgExists == false);
					
						string paramUrl = "Manifest/PopUp_PrintCreteSIP.aspx?fname=" + con_no+".pdf";
						Response.Write("<script type='text/javascript'>window.open('" + paramUrl + "','_blank');</script>");
					}
				}
			}

			if(sUrl != "")
			{
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScriptPopupSetSize("openParentWindowCustomSize.js",paramList,600,900);
				Utility.RegisterScriptString(sScript,"ShowPopupScript"+DateTime.Now.ToString("ddMMyyyyHHmmss"),this.Page);	
			}
		}


		private void btnClientEvent_Click(object sender, System.EventArgs e)
		{
			ExecQry();
		}


		private void GrdAWBDetails_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Footer)
			{
				LinkButton btnAddItem = (LinkButton)e.Item.FindControl("btnAddItem");
				if(btnAddItem != null)
				{
					btnAddItem.Enabled=AddItemGrid;
				}
			}
			else if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{				
				LinkButton btnEditItem = (LinkButton)e.Item.FindControl("btnEditItem");
				LinkButton btnDeleteItem = (LinkButton)e.Item.FindControl("btnDeleteItem");
				LinkButton btnPrintItem = (LinkButton)e.Item.FindControl("btnPrintItem");
				CheckBox cbkRec = (CheckBox)e.Item.FindControl("cbkRec");
				DataRowView dr = (DataRowView)e.Item.DataItem;
				if(dr["SourceOfData"] != DBNull.Value && dr["SourceOfData"].ToString().Trim() != "")
				{
					if(dr["SourceOfData"].ToString() =="0")
					{
						e.Item.BackColor=System.Drawing.ColorTranslator.FromHtml("#e4952c");
						btnDeleteItem.Enabled=false;
					}
				}
			
				btnEditItem.Enabled=!IsBondStore;
				btnDeleteItem.Enabled=!IsBondStore;
				btnPrintItem.Visible=IsBondStore;

				cbkRec.Enabled=IsBondStore;
				cbkRec.Checked=false;
				if(dr["IsChecked"] != DBNull.Value && dr["IsChecked"].ToString().Trim() != "")
				{
					if(dr["IsChecked"].ToString() =="1")
					{
						cbkRec.Checked=true;
					}
				}
				
			}
		}


		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				System.Text.StringBuilder s = new System.Text.StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}

		private void ConsolidationShipper_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DataSet ds = SysDataMgrDAL.GetConsolidatorDetails(appID,enterpriseID,ConsolidationShipper.SelectedValue);
			if(ds.Tables[0].Rows.Count>0)
			{
				DataRow dr = ds.Tables[0].Rows[0];
				ConsolidatorName.Text=dr["ConsolidatorName"].ToString();
				ConsolidatorAddress1.Text=dr["ConsolidatorAddress1"].ToString();
				ConsolidatorAddress2.Text=dr["ConsolidatorAddress2"].ToString();
				ConsolidatorCountry_Desc.Text="";
				if(dr["ConsolidatorCountry"] != DBNull.Value)
				{
					ConsolidatorCountry.SelectedValue=dr["ConsolidatorCountry"].ToString();	
					ConsolidatorCountry_Desc.Text=CustomJobCompilingDAL.GetCountryName(appID,enterpriseID,ConsolidatorCountry.SelectedValue);
				}
			}
			SetInitialFocus(ConsolidationShipper);
			if (txtFileName.Text != "")
			{
				btnImport.Enabled = true;
			}
		}

		private void ExporterCountry_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ConsolidatorCountry_Desc.Text=CustomJobCompilingDAL.GetCountryName(appID,enterpriseID,ConsolidatorCountry.SelectedValue);
		}

		private void btnVerifyClient_Click(object sender, System.EventArgs e)
		{
			if(Session["VerifiedBy"] != null && Session["StrverifiedDT"] != null)
			{
				VerifiedBy.Text = Session["VerifiedBy"].ToString();
				VerifiedDT.Text = Session["StrverifiedDT"].ToString();
				Session["VerifiedBy"]=null;
				Session["StrverifiedDT"]=null;
			}
		}

		private void btnImport_Click(object sender, System.EventArgs e)
		{
			try
			{
				string path = uploadFiletoServer();

				if(path != "")
				{
					string textString="";
					StringBuilder sbTxt = new StringBuilder();
					using(System.IO.StreamReader file = new System.IO.StreamReader(path))
					{
						string line;
							
						while((line = file.ReadLine()) != null)
						{								
							//sbTxt.Append(line);
							textString+=line;
						}
					}

					DataSet ds = CustomsDAL.Customs_MAWB_ImportXML(appID,enterpriseID,userID,ddlXMLFormat.SelectedValue,null,ConsolidationShipper.SelectedValue,1,textString);
					System.Data.DataTable dtStatus = ds.Tables[0];
					System.Data.DataTable dtAWB= ds.Tables[1];
					System.Data.DataTable dtAWBDetail= ds.Tables[2];

					if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
					{				
						clearscreen();
						BindControl();
						BindMasterAWBInfo(dtAWB);	
						GrdAWBDetails.Enabled=true;
						BindMasterAWBDetail(dtAWBDetail);

						MasterAWB_no.Enabled=false;
						btnQry.Enabled=true;
						btnBrowse.Enabled=false;
						inFile.Disabled=true;
						btnImport.Enabled=false;
						btnExecQry.Enabled=false;
						btnInsert.Enabled=false;
						btnSave.Enabled=true;	
						btnVerify.Enabled=true;
						btnDelete.Enabled=true;
						lblError.Text = dtStatus.Rows[0]["ErrorMessage"].ToString();
						SetInitialFocus(ArrivalTime);
					}
					else
					{
						lblError.Text=(dtStatus.Rows.Count>0?dtStatus.Rows[0]["ErrorMessage"].ToString():"");
						btnQry.Enabled=true;
						btnBrowse.Enabled=true;
						btnExecQry.Enabled=true;
						btnInsert.Enabled=true;
						btnSave.Enabled=false;				
						btnVerify.Enabled=false;
						btExport.Enabled=false;
						btExportV2.Enabled=false;
						btnDelete.Enabled=false;
						btnImport.Enabled=false;
						txtFileName.Text="";
						SetInitialFocus(MasterAWB_no);
					}
				}
			}
			catch(Exception ex)
			{
				lblError.Text = ex.Message.ToString();
			}
			

		}

		private string uploadFiletoServer()
		{			
			string path="";

			//string pathDb = (string)System.Configuration.ConfigurationSettings.AppSettings["PathDatabaseFolder"];
			string strDate = DateTime.Now.ToString("ddMMyyhhmmss");
			string fn = System.IO.Path.GetFileName(inFile.PostedFile.FileName);

			if((inFile.PostedFile != null ) && inFile.PostedFile.ContentLength > 0)
			{
				string extension = System.IO.Path.GetExtension(inFile.PostedFile.FileName);

				//if(extension.ToUpper() == ".TXT" || extension.ToUpper() == ".XML")
				if(extension.ToUpper() != "")
				{
					
					
					//string pathShare=(string)System.Configuration.ConfigurationSettings.AppSettings["PathDatabaseShareFolder"];
					
					//string mappedPath = pathShare + "\\" +  strDate + "_" + fn;
					string tempFileForStorage = System.Configuration.ConfigurationSettings.AppSettings["InvoiceExportPath"].ToString();
					tempFileForStorage +=  strDate + "_" + fn;
					//mappedPath=tempFileForStorage;
					try
					{
						inFile.PostedFile.SaveAs(tempFileForStorage);						
						path=tempFileForStorage;
						lblError.Text = "";
					}
					catch(Exception ex)
					{
						//throw  ex;
						lblError.Text = ex.Message;
					}
				}
				else
				{
					lblError.Text = "Not a valid file.";
				}
			}
			else
			{
				lblError.Text = "Please select a file to upload.";
			}
			return path;
		}

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			DataTable dtParams = CustomsDAL.MasterAWBEntryParameter();
			System.Data.DataRow drNew = dtParams.NewRow();
			drNew["action"] ="3";
			drNew["enterpriseid"] =enterpriseID;
			drNew["userloggedin"] =userID;
			if(MasterAWB_no.Text.Trim() !="")
			{
				drNew["MasterAWB_no"] =MasterAWB_no.Text.Trim();
			}
			else
			{
				drNew["MasterAWB_no"] = DBNull.Value;
				lblError.Text="Master AWB number is required.";
				return;
			}
					
			dtParams.Rows.Add(drNew);
			DataSet ds = CustomsDAL.Customs_MAWB(appID,enterpriseID,userID,dtParams);
			System.Data.DataTable dtStatus = ds.Tables[0];
			System.Data.DataTable dtAWB= ds.Tables[1];
			System.Data.DataTable dtAWBDetail= ds.Tables[2];
			
			if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
			{				
				clearscreen();			
				DSMode = (int)ScreenMode.None;
				DSOperation = (int)Operation.None;
			}
			lblError.Text=(dtStatus.Rows.Count>0?dtStatus.Rows[0]["ErrorMessage"].ToString():"");

		}

		private void Button1_Click(object sender, System.EventArgs e)
		{

		}

		private void btnBrowse_Click(object sender, System.EventArgs e)
		{
			if(ConsolidationShipper.SelectedValue=="")
			{
				lblError.Text="Consolidation shipper is required for import.";
			}
		}

		private void btnSubmit_ServerClick(object sender, System.EventArgs e)
		{
			uploadFiletoServer();
		}

		private void btnOverallStatus_Click(object sender, System.EventArgs e)
		{
			String strUrl = null;
			string arrivaldate = DateTime.Now.ToString("ddMMyyyy");
			if(ArrivalDate.Text.Trim() != "")
			{
				try
				{
					arrivaldate = DateTime.ParseExact(ArrivalDate.Text.Trim(),"dd/MM/yyyy",null).ToString("ddMMyyyy");
				}
				catch
				{

				}
			}
			strUrl = "MAWBOverallStatusPopup.aspx?arrivaldate="+arrivaldate;
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindowCustomSize.js",paramList,550,830);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		protected void chk1_CheckedChanged(object sender, EventArgs e)
		{
			CheckBox cbkRec = (CheckBox)sender;
			
			int index = Int32.Parse(cbkRec.Attributes["CrsID"].ToString());
			int ReceivedByBond = (cbkRec.Checked==true?1:0);

			string MAWBNumber="";
			string ShipFlightNo="";
			string consignment_no="";
			if(GrdAWBDetails.Items[index] != null)
			{
				Label lblMAWBNumber = (Label)GrdAWBDetails.Items[index].Cells[2].FindControl("lblMAWBNumber");
				Label lblConsignment_no = (Label)GrdAWBDetails.Items[index].Cells[2].FindControl("lblConsignment_no");
				Label lblShipFlightNo = (Label)GrdAWBDetails.Items[index].Cells[2].FindControl("lblShipFlightNo");
				MAWBNumber = (lblMAWBNumber != null?lblMAWBNumber.Text.Trim():"");
				ShipFlightNo = (lblShipFlightNo != null?lblShipFlightNo.Text.Trim():"");
				consignment_no = (lblConsignment_no != null?lblConsignment_no.Text.Trim():"");

				DataSet ds = CustomsDAL.Customs_MAWB_ConsRecdByBond(appID,enterpriseID,userID,ReceivedByBond,MAWBNumber,ShipFlightNo,consignment_no);
				ExecQry();
			}
		}

		private DataTable fillLicensePlateData(DataSet dsLicensePlate,DataTable dtPrint)
		{
			string pkgNum = dsLicensePlate.Tables[0].Rows.Count.ToString();
			foreach (DataRow drLicensePlate in dsLicensePlate.Tables[0].Rows) 
			{
				DataRow dr = dtPrint.NewRow();
				dr["consignment"] = drLicensePlate["consignment_no"].ToString();
				dr["package"]=drLicensePlate["PkgNo"].ToString() + " of " +drLicensePlate["TotalPkgs"].ToString();
				dr["uid"] = drLicensePlate["UserID"].ToString();
				dr["dateprint"] = DateTime.Parse(drLicensePlate["PrintedDT"].ToString()).ToString("dd/MM/yyyy HH:mm");
				dr["Label"] = "*" + drLicensePlate["BarCode"].ToString() + "*";

				dr["reference_name"] = drLicensePlate["reference_name"];
				dr["telephone"] = drLicensePlate["telephone"];
				dr["address"] = drLicensePlate["address"];
				dr["contactperson"] = drLicensePlate["contactperson"];
				dr["zip"] = drLicensePlate["ZIP_Code"];
				dr["route1"] = drLicensePlate["linehault"];
				dr["route2"] = drLicensePlate["linehault2"];
				dr["service"] = drLicensePlate["service_type"];
				dr["province"] = drLicensePlate["province"];
				dr["sender_name"] = drLicensePlate["sender_name"];
				dr["sender_tel"] = drLicensePlate["sender_tel"];
				dr["cust_ref"] = drLicensePlate["cust_ref"];
				dr["dclocation"] = drLicensePlate["dc_location"];

                #region generate barcode by Kat [2019-05-15]
                //string Barcode = "*" + drLicensePlate["consignment_no"].ToString().ToUpper() + " " + drLicensePlate["PkgNo"].ToString() + " " + drLicensePlate["TotalPkgs"].ToString() + " " + drLicensePlate["delivery_route_code"].ToString() + "*";
                string Barcode = "*" + drLicensePlate["BarCode"].ToString() + "*";
                //string pathBarcode = Server.MapPath("..\\Export") + "\\barcode.Png";

                Barcode bcd = new Barcode();
                bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                bcd.IncludeLabel = false;
                bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                //bcd.LabelPosition = LabelPositions.BOTTOMCENTER;
                bcd.LabelFont = new Font("Tahoma", 7, System.Drawing.FontStyle.Regular);

                int barcodeWidth = Convert.ToInt32(Barcode.Length * 13.69);
                int barcodeHeight = Convert.ToInt32(Barcode.Length * 1.30);

                //bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 315, 30);
                bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, barcodeWidth, barcodeHeight);

                MemoryStream ms = new MemoryStream();
                bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                Byte[] oByte = ms.ToArray();

                dr["imgBarcode"] = oByte;

                #endregion

                dtPrint.Rows.Add(dr);
			}
			return dtPrint;
		}

		private string PathExport(string sPath)
		{
			string[] arrurl = sPath.Split('/');
			string rUrl = string.Empty;
			for(int i=0; i<arrurl.Length-1; i++)
			{
				if(arrurl[i] != string.Empty)
				{
					rUrl = rUrl + "/" + arrurl[i];					
				}	
			}

			return rUrl;
		}

		private bool IsfileExists(string curFile)
		{
			//string curFile = @"c:\temp\test.txt";
			if (System.IO.File.Exists(curFile) == true)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		private void btExportV2_Click(object sender, System.EventArgs e)
		{
			//ExportXMLDataText();
			try
			{				
				//ExecQry();

				lblError.Text = string.Empty;
				string itemList = string.Empty;
				string pathFile = CustomsDAL.GetPathCustomsMAWB(appID, enterpriseID);
				string fileName = "";
				try
				{
					if(MasterAWB_no.Text.Trim().Length<=8)
					{
						fileName=MasterAWB_no.Text.Trim();
					}
					else
					{
						int idx=MasterAWB_no.Text.Trim().Length - 8;
						fileName=MasterAWB_no.Text.Trim().Substring(idx);
					}
				}
				catch
				{
					fileName=MasterAWB_no.Text.Trim();
				}
				
				DataSet dsResult = new DataSet();
				dsResult = CustomsDAL.Customs_MAWB_ExportXMLV2(appID, enterpriseID, userID, MasterAWB_no.Text.Trim());
				
				this.lblError.Text = dsResult.Tables[0].Rows[0][1].ToString();
				if(!dsResult.Tables[0].Rows[0][0].ToString().Equals("0"))
					return;

				ExportedBy.Text = dsResult.Tables[2].Rows[0]["ExportedBy"].ToString();
				ExportedDT.Text = DateTime.Parse(dsResult.Tables[2].Rows[0]["ExportedDT"].ToString()).ToString("dd/MM/yyyy HH:mm");									

				String strUrl = "TempExprot.aspx";
				Session["FORMID"] = "ExportXMLAWB";
				Session["DSExport"] = dsResult; 

				Session["FileName"] = fileName;
				Session["XMLText"] = dsResult.Tables[1].Rows[0][0].ToString();
				
				ArrayList paramList = new ArrayList();
				
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentExport.js", paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				
				//ExecQry();
			}
			catch (Exception ex)
			{
				this.lblError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}
	}

}
