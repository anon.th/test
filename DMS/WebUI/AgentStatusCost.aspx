<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="AgentStatusCost.aspx.cs" AutoEventWireup="false" Inherits="com.ties.AgentStatusCost" smartNavigation="false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AgentStatusCost</title>
		<LINK rev="stylesheet" href="css/styles.css" rel="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="AgentStatusCost" method="post" runat="server">
			<asp:label id="lblTitle" style="Z-INDEX: 104; LEFT: 24px; POSITION: absolute; TOP: 8px" runat="server" CssClass="mainTitleSize" Height="32px" Width="336px">Agent Status Cost</asp:label><asp:button id="btnGoToFirstPage" style="Z-INDEX: 111; LEFT: 608px; POSITION: absolute; TOP: 48px" tabIndex="5" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text="|<"></asp:button><asp:button id="btnPreviousPage" style="Z-INDEX: 105; LEFT: 632px; POSITION: absolute; TOP: 48px" tabIndex="6" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text="<"></asp:button><asp:textbox id="txtCountRec" style="Z-INDEX: 109; LEFT: 656px; POSITION: absolute; TOP: 48px" tabIndex="7" runat="server" Height="19px" Width="24px"></asp:textbox><asp:button id="btnNextPage" style="Z-INDEX: 106; LEFT: 680px; POSITION: absolute; TOP: 48px" tabIndex="8" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text=">"></asp:button><asp:button id="btnGoToLastPage" style="Z-INDEX: 108; LEFT: 704px; POSITION: absolute; TOP: 48px" tabIndex="9" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text=">|"></asp:button><asp:label id="lblAgentName" style="Z-INDEX: 110; LEFT: 384px; POSITION: absolute; TOP: 112px" runat="server" CssClass="tableLabel" Width="100px">Agent Name</asp:label><asp:label id="lblAgentCode" style="Z-INDEX: 107; LEFT: 24px; POSITION: absolute; TOP: 112px" runat="server" CssClass="tableLabel" Width="100px">Agent Code</asp:label><asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 22px; POSITION: absolute; TOP: 48px" runat="server" CssClass="queryButton" Width="62px" CausesValidation="False" Text="Query"></asp:button><asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 84px; POSITION: absolute; TOP: 48px" runat="server" CssClass="queryButton" Width="130px" CausesValidation="False" Text="Execute Query"></asp:button><asp:label id="lblSCMessage" style="Z-INDEX: 103; LEFT: 24px; POSITION: absolute; TOP: 80px" runat="server" CssClass="errorMsgColor" Width="688px">Error Message</asp:label></TD></TD>&nbsp;</TD>
			<INPUT type=hidden value="<%=strScrollPosition%>" name=ScrollPosition>
			<asp:datagrid id="dgException" AllowPaging="True" PageSize="5" AllowCustomPaging="True" style="Z-INDEX: 112; LEFT: 24px; POSITION: absolute; TOP: 352px" runat="server" Width="704px" OnPageIndexChanged="dgException_PageChange" OnUpdateCommand="dgException_Update" OnDeleteCommand="dgException_Delete" OnItemDataBound="dgException_ItemDataBound" OnCancelCommand="dgException_Cancel" OnEditCommand="dgException_Edit" OnItemCommand="dgException_Button" AutoGenerateColumns="False">
				<ItemStyle HorizontalAlign="Left" CssClass="gridfield"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Exception Code">
						<HeaderStyle Font-Bold="True" HorizontalAlign="Left" Width="25%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblException" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Exception_code")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtException" Text='<%#DataBinder.Eval(Container.DataItem,"Exception_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumeric" MaxLength="10">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="reqException" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtException" ErrorMessage="Exception Code "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="ExceptionSearch">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Exception Description">
						<HeaderStyle Width="35%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblExceptionDescp" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"Exception_description")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtExceptionDescp" CssClass="gridTextBox" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Exception_description")%>' MaxLength="200" ReadOnly="True">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="MBG">
						<HeaderStyle HorizontalAlign="Left" Width="10%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:DropDownList CssClass="gridDropDown" ID="ddlExMBG" Runat="server"></asp:DropDownList>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Cost">
						<HeaderStyle HorizontalAlign="Left" Width="15%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblExcepSurcharge" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Surcharge","{0:#0.00}")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtExcepSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"Surcharge","{0:#0.00}")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" >
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:datagrid id="dgStatus" AllowPaging="True" PageSize="5" AllowCustomPaging="True" style="Z-INDEX: 113; LEFT: 24px; POSITION: absolute; TOP: 136px" runat="server" Width="704px" OnPageIndexChanged="dgStatus_PageChange" OnUpdateCommand="dgStatus_Update" OnDeleteCommand="dgStatus_Delete" OnItemDataBound="dgStatus_ItemDataBound" OnCancelCommand="dgStatus_Cancel" OnEditCommand="dgStatus_Edit" OnItemCommand="dgStatus_Button" AutoGenerateColumns="False" SelectedItemStyle-CssClass="gridFieldSelected" OnSelectedIndexChanged="dgStatus_SelectedIndexChanged">
				<ItemStyle HorizontalAlign="Left" CssClass="gridfield"></ItemStyle>
				<Columns>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;" CommandName="Select">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Status Code">
						<HeaderStyle Font-Bold="True" HorizontalAlign="Left" Width="25%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblStatus" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Status_code")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtStatus" Text='<%#DataBinder.Eval(Container.DataItem,"Status_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumeric" MaxLength="10">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="reqStatus" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtStatus" ErrorMessage="Status Code "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="StatusSearch">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Status Description">
						<HeaderStyle Width="35%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblStatusDescp" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"Status_description")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox ID="txtStatusDescp" CssClass="gridTextBox" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Status_description")%>' MaxLength="200">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Surcharge">
						<HeaderStyle HorizontalAlign="Left" Width="20%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblStatusSurcharge" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Surcharge","{0:#0.00}")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtStatusSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"Surcharge")%>' Runat="server" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" >
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:button id="btnInsertStatus" style="Z-INDEX: 114; LEFT: 215px; POSITION: absolute; TOP: 48px" runat="server" CssClass="queryButton" Width="64px" CausesValidation="False" Text="Insert"></asp:button><asp:button id="btnInsertException" style="Z-INDEX: 115; LEFT: 24px; POSITION: absolute; TOP: 328px" runat="server" CssClass="queryButton" Width="54px" CausesValidation="False" Text="Insert" EnableViewState="False"></asp:button><asp:label id="lblEXMessage" style="Z-INDEX: 116; LEFT: 88px; POSITION: absolute; TOP: 328px" runat="server" CssClass="errorMsgColor" Width="632px">Error Message</asp:label><dbcombo:dbcombo id="dbCmbAgentName" style="Z-INDEX: 117; LEFT: 488px; POSITION: absolute; TOP: 104px" tabIndex="2" runat="server" Height="17px" Width="176px" RegistrationKey=" " ReQueryOnLoad="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentNameServerMethod" TextBoxColumns="25" AutoPostBack="True"></dbcombo:dbcombo><dbcombo:dbcombo id="dbCmbAgentId" style="Z-INDEX: 118; LEFT: 136px; POSITION: absolute; TOP: 104px" tabIndex="1" runat="server" Height="17px" Width="140px" RegistrationKey=" " ReQueryOnLoad="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentIdServerMethod" TextBoxColumns="20" AutoPostBack="True"></dbcombo:dbcombo><asp:requiredfieldvalidator id="reqAgentId" Runat="server" BorderWidth="0" Display="None" ControlToValidate="dbCmbAgentId" ErrorMessage="Agent Id "></asp:requiredfieldvalidator><asp:validationsummary id="PageValidationSummary" style="Z-INDEX: 104; LEFT: 606px; POSITION: absolute; TOP: 5px" runat="server" ShowMessageBox="True" ShowSummary="False" DisplayMode="BulletList" HeaderText="Please enter the missing  fields."></asp:validationsummary></form>
	</body>
</HTML>
