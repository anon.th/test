<%@ Page language="c#" Codebehind="CreditStatusTermPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CreditStatusTermPopup" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomerPopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" type="text/javascript">
			function checkbeforesave()
			{
				var CreditLimit =	document.getElementById('txtCreditLimit').value;
				var CreditThred =	document.getElementById('txtCreditThred').value;
				var CreditTerms =	document.getElementById('ddlCreditTerms').value;
				var CreditStatus =	document.getElementById('ddlCreditStatus').value;
				var Reason =	document.getElementById('ddlReason').value;
				var radCredit =	document.getElementById('radCredit');				

				if(document.getElementById('hdpage').value != "MA")
				{
					if(radCredit.checked){
					if(CreditLimit == ""||
						 		(parseInt(CreditLimit)==0 ||parseInt(CreditLimit)==NaN))
					{
						alert('Please Input Credit Limit');
						document.getElementById('txtCreditLimit').focus();
						return false;
					}
					
					if(CreditThred == "")
					{
						alert('Please Input Credit Threshold(%)');
						document.getElementById('txtCreditThred').focus();
						return false;
					}
					if(CreditTerms == ""){
						alert('Please Select Credit Terms');
						return false;
					}
					if(CreditStatus == "")
					{
						alert('Please Select Credit Status');
						return false;
					}
					
					//if(Reason == "")
					//{
					//	alert('Please Select Reason Code');
					//	return false;
					//}				
					//alert('a');
					if(document.getElementById('radCredit').checked == true)
					{
					//alert('b');
						if(document.getElementById('txtCreditLimit').value == ''||
						 		document.getElementById('txtCreditLimit').value == '0'||
						 		(parseInt(CreditLimit)==0 ||parseInt(CreditLimit)==NaN))
						{
							alert('Please Input Credit Limit');
							document.getElementById('txtCreditLimit').select();
							return false;
						}
					//	var intt  =  document.getElementById('radPaymentCash11').value 
						
						
					}
					}
					
				}
				else
				{
					//if(document.getElementById('hdmode').value == "E")
					//{
					if(radCredit.checked){
						if(CreditLimit == ""||
						 		(parseInt(CreditLimit)==0 ||parseInt(CreditLimit)==NaN))
						{
							alert('Please Input Credit Limit');
							document.getElementById('txtCreditLimit').focus();
							return false;
						}
					
						if(CreditThred == "")
						{
							alert('Please Credit Threshold(%)');
							document.getElementById('txtCreditThred').focus();
							return false;
						}
						if(CreditTerms == ""){
							alert('Please Select Credit Terms');
							return false;
						}
						if(CreditStatus == "")
						{
							alert('Please Select Credit Status');
							return false;
						}
					//if(Reason == "")
					//{
					//	alert('Please Select Reason Code');
					//	return false;
					//}	
						
					}
				}
				
				if(document.getElementById('hdmode').value == "E")
				{
					if(Reason == "")
					{
						alert('Please Select Reason Code');
						return false;
					}
				}
				
				return true;
			}
		</script>
	</HEAD>
	<body>
		<form id="CreditStatusTermPopup" method="post" runat="server">
			<asp:button id="btncancel" style="Z-INDEX: 105; LEFT: 656px; POSITION: absolute; TOP: 48px"
				runat="server" CausesValidation="False" Text="Cancel" Width="85" CssClass="buttonProp" Height="21px"></asp:button><asp:button id="btnOk" style="Z-INDEX: 102; LEFT: 568px; POSITION: absolute; TOP: 48px" runat="server"
				CausesValidation="False" Text="OK" Width="85px" CssClass="buttonProp" Height="21px"></asp:button>
			<TABLE id="Table1" style="Z-INDEX: 111; LEFT: 5px; WIDTH: 750px; POSITION: absolute; TOP: 80px"
				cellSpacing="1" cellPadding="1" border="0">
				<TR id="trcustomerID" runat="server">
					<TD><asp:label id="lblCustIDName" runat="server" Width="120px" CssClass="tableLabel">Customer ID/Name:</asp:label></TD>
					<TD><asp:textbox id="txtCustID" runat="server" Width="100px" CssClass="textField" ReadOnly="True"
							BackColor="#EFEFEF"></asp:textbox><FONT face="Tahoma">&nbsp;</FONT><asp:textbox id="txtCustName" runat="server" Width="235px" CssClass="textField" ReadOnly="True"
							BackColor="#EFEFEF"></asp:textbox></TD>
					<TD style="WIDTH: 85px"></TD>
					<TD></TD>
				</TR>
				<TR id="trMasterAccount" runat="server">
					<TD><asp:label id="Label2" runat="server" Width="120px" CssClass="tableLabel">Master Account ID</asp:label></TD>
					<TD><asp:textbox id="txtMasterAccount" runat="server" Width="160px" CssClass="textField" BackColor="#EFEFEF"
							Enabled="False"></asp:textbox></TD>
					<TD style="WIDTH: 85px"></TD>
					<TD></TD>
				</TR>
				<TR id="trMALCU" runat="server">
					<TD></TD>
					<TD><asp:checkbox id="chkcreditLimitUsed" runat="server" Text="Master Account Limit Credit used" CssClass="tableLabel"
							AutoPostBack="True"></asp:checkbox></TD>
					<TD style="WIDTH: 85px"></TD>
					<TD></TD>
				</TR>
				<TR id="trPayment" runat="server">
					<TD><asp:label id="lblPaymentMode" runat="server" Width="88px" CssClass="tableLabel">Payment Mode</asp:label></TD>
					<TD><asp:radiobutton id="radPaymentCash" tabIndex="27" runat="server" Text="Cash" Width="65px" CssClass="tableRadioButton"
							AutoPostBack="true" GroupName="PaymentType" Font-Size="Smaller"></asp:radiobutton><asp:radiobutton id="radCredit" tabIndex="28" runat="server" Text="Credit" Width="96px" CssClass="tableRadioButton"
							AutoPostBack="true" GroupName="PaymentType" Font-Size="Smaller"></asp:radiobutton></TD>
					<TD style="WIDTH: 85px"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblCreditLimit" runat="server" Width="88px" CssClass="tableLabel">Credit Limit:</asp:label><asp:label id="Label3" runat="server" Width="8px" CssClass="tableLabel" Height="15px" ForeColor="Red">*</asp:label></TD>
					<TD noWrap><cc1:mstextbox id="txtCreditLimit" runat="server" Width="89px" CssClass="textfield" Height="20px"
							NumberScale="1" NumberPrecision="10" NumberMinValue="0" NumberMaxValue="99999999" TextMaskType="msNumeric"></cc1:mstextbox><FONT face="Tahoma">&nbsp;</FONT><asp:label id="Label1" runat="server" Width="134px" CssClass="tableLabel">Credit Threshold(%):</asp:label><asp:label id="Label4" runat="server" Width="8px" CssClass="tableLabel" Height="15px" ForeColor="Red">*</asp:label><FONT face="Tahoma">&nbsp;</FONT><cc1:mstextbox id="txtCreditThred" runat="server" Width="89px" CssClass="textfield" Height="21px"
							NumberScale="2" NumberPrecision="3" NumberMinValue="1" NumberMaxValue="100" TextMaskType="msNumeric"></cc1:mstextbox></TD>
					<TD style="WIDTH: 86px"><asp:label id="lblCreditAvailable" runat="server" Width="99px" CssClass="tableLabel">Credit Available:</asp:label></TD>
					<TD><cc1:mstextbox id="txtcreditAva" runat="server" Width="89px" CssClass="textfield" Height="19px"
							ReadOnly="True" BackColor="#EFEFEF" NumberScale="7" NumberPrecision="19" NumberMinValue="0"
							NumberMaxValue="99999999" TextMaskType="msNumeric"></cc1:mstextbox></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblCreditTerm" runat="server" Width="88px" CssClass="tableLabel">Credit Terms:</asp:label><asp:label id="lblreCreditTerms" runat="server" Width="8px" CssClass="tableLabel" Height="15px"
							ForeColor="Red">*</asp:label></TD>
					<TD><asp:dropdownlist id="ddlCreditTerms" runat="server" Width="120px" CssClass="textField" Height="19px"></asp:dropdownlist></TD>
					<TD style="WIDTH: 86px"><asp:label id="lblCreditUsed" runat="server" Width="101px" CssClass="tableLabel">Credit Used:</asp:label></TD>
					<TD><cc1:mstextbox id="txtcreditused" runat="server" Width="89px" CssClass="textfield" Height="19px"
							ReadOnly="True" BackColor="#EFEFEF" NumberScale="7" NumberPrecision="10" NumberMinValue="0"
							NumberMaxValue="99999999" TextMaskType="msNumeric"></cc1:mstextbox></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblCreditStatus" runat="server" Width="88px" CssClass="tableLabel">Credit Status:</asp:label><asp:label id="lblrestatus" runat="server" Width="8px" CssClass="tableLabel" Height="15px"
							ForeColor="Red">*</asp:label></TD>
					<TD><asp:dropdownlist id="ddlCreditStatus" runat="server" Width="120px" CssClass="textField"></asp:dropdownlist></TD>
					<TD style="WIDTH: 86px"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblReasonCode" runat="server" Width="168px" CssClass="tableLabel" Height="14px">Reason Code (for Change):</asp:label><asp:label id="lblreReason" runat="server" Width="8px" CssClass="tableLabel" Height="15px"
							ForeColor="Red">*</asp:label></TD>
					<TD colSpan="3"><asp:dropdownlist id="ddlReason" runat="server" Width="120px" CssClass="textField" AutoPostBack="True"></asp:dropdownlist><FONT face="Tahoma">&nbsp;</FONT><asp:textbox id="txtReason" runat="server" Width="220px" CssClass="textField" ReadOnly="True"
							BackColor="#EFEFEF"></asp:textbox>
					</TD>
				</TR>
				<TR>
					<TD colSpan="4"><asp:label id="CustIDNameErr" runat="server" Width="598px" ForeColor="Red"></asp:label></TD>
				</TR>
				<tr>
					<td colSpan="4"><asp:datagrid id="dgdata" runat="server" Width="753px" CssClass="gridField" AutoGenerateColumns="False"
							ItemStyle-Height="20px">
							<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
							<ItemStyle Height="20px" CssClass="drilldownField"></ItemStyle>
							<HeaderStyle CssClass="gridHeading"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="creditlimit" HeaderText="Credit Limit" DataFormatString="{0:###}">
									<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="creditterms" HeaderText="Credit Term">
									<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="paymentmode" HeaderText="Payment Mode">
									<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="creditstatus" HeaderText="Credit Status">
									<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="creditthreshold" HeaderText="Credit Threshold(%)">
									<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
									<ItemStyle HorizontalAlign="Right"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="reason" HeaderText="Reason">
									<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="reasondescription" HeaderText="Reason Descriptipn">
									<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="updatedby" HeaderText="Update By">
									<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="updateddate" HeaderText="Update Date" DataFormatString="{0:dd/MM/yyyy HH:mm}">
									<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:BoundColumn>
								<asp:TemplateColumn HeaderText="MACC Limit Credit Used">
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:CheckBox Runat="server" ID="chklimitcreditused" Enabled="False"></asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</TABLE>
			<P align="left"><asp:label id="lblMainTitle" runat="server" Width="477px" CssClass="mainTitleSize" Height="26px">Credit Status and Terms</asp:label><INPUT id="hdmode" type="hidden" name="Hidden1" runat="server">
				<INPUT id="hdpage" type="hidden" name="Hidden1" runat="server">
			</P>
		</form>
	</body>
</HTML>
