using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.applicationpages;
using TIESDAL;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for MAWBOverallStatusPopup.
	/// </summary>
	public class MAWBOverallStatusPopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label Label65;
		protected System.Web.UI.WebControls.DataGrid GrdAWBDetails;
		protected System.Web.UI.HtmlControls.HtmlTable Table5;
		
		protected decimal TotalWeight = 0;

		private string appID = null;
		private string enterpriseID = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if(this.IsPostBack==false)
			{
				appID = utility.GetAppID();
				enterpriseID = utility.GetEnterpriseID();

				DateTime arrivalDate = DateTime.Now;
				if(Request.QueryString["arrivaldate"] != null && Request.QueryString["arrivaldate"].ToString().Trim() != "")
				{
					try
					{
						arrivalDate = DateTime.ParseExact(Request.QueryString["arrivaldate"].ToString().Trim(),"ddMMyyyy",null);
					}
					catch
					{

					}
				}
				DataSet dtMAWBDetail = CustomsDAL.Customs_MAWB_OverallStatus(appID,enterpriseID,arrivalDate);
				GrdAWBDetails.DataSource=dtMAWBDetail;
				GrdAWBDetails.DataBind();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.GrdAWBDetails.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.GrdAWBDetails_ItemCommand);
			this.GrdAWBDetails.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.GrdAWBDetails_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void GrdAWBDetails_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			string cmd = e.CommandName;
			Session["MAWBNumberverallStatus"]="";
			if(cmd=="MAWBNumber")
			{
				
				LinkButton lnkMAWBNumber = (LinkButton)e.Item.FindControl("lnkMAWBNumber");
				string number =lnkMAWBNumber.Text.Trim();
				string sScript = "";
				Session["MAWBNumberverallStatus"]=number;
				sScript += "<script language=javascript>";
				sScript += "    window.opener.callbackOverallStatus('"+number+"');";
				sScript += "    window.close();";
				sScript += "</script>";

				this.Page.RegisterStartupScript("CmdMAWBNumber"+DateTime.Now.ToString("ddMMyyyyHHmmss"),sScript);
			}
		}

		private void GrdAWBDetails_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			DataRowView dr = (DataRowView)e.Item.DataItem;
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{				
				if(dr["FullyReceived"] != DBNull.Value && dr["FullyReceived"].ToString().Trim() != "" 
					&& dr["FullyReceived"].ToString() =="1")
				{
					e.Item.BackColor=System.Drawing.ColorTranslator.FromHtml("#54f751");
				}

				if(dr["TotalWeight"] != DBNull.Value && dr["TotalWeight"].ToString().Trim() != "")
				{
					TotalWeight = TotalWeight + decimal.Parse(dr["TotalWeight"].ToString());
				}
			}

			if(e.Item.ItemType == ListItemType.Footer)
			{				
				e.Item.Cells[8].Text = TotalWeight.ToString("#,##0");
			}
		}
	}
}
