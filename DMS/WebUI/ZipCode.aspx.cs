using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.ties.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for ZipCode.
	/// </summary>
	public class ZipCode : BasePage
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.DataGrid dgZipCode;
		protected System.Web.UI.WebControls.Label lblErrorMessage;

		//Utility utility = null;
		private String m_strAppID;
		private String m_strEnterpriseID;
		SessionDS m_sdsZipCode = null;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.Button btnGenerate;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		//	utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();

			if (!Page.IsPostBack)
			{
				QueryMode();
				this.btnQuery_Click(this,System.EventArgs.Empty);
			}
			else
			{
				m_sdsZipCode = (SessionDS) Session["SESSION_DS1"];
			}
			lblErrorMessage.Text="";
			ValidationSummary1.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected void dgZipCode_Update(object sender, DataGridCommandEventArgs e)
		{
			string strPckRouteCode=null;
			string strDlvRouteCode=null;
			lblErrorMessage.Text = "";
			if (btnExecuteQuery.Enabled)
			{
				return;
			}
			int iRowsAffected = 0;
			int rowIndex = e.Item.ItemIndex;
			GetChangedRow(e.Item, e.Item.ItemIndex);
			TextBox txtPickupRoute = (TextBox)e.Item.FindControl("txtPickupRoute");
			TextBox txtDeliveryRoute = (TextBox)e.Item.FindControl("txtDeliveryRoute");
			
			if(txtPickupRoute.Text != "")
			{
				com.ties.classes.RouteCode  Pckroutecode = new  com.ties.classes.RouteCode();
				Pckroutecode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtPickupRoute.Text);         
				strPckRouteCode = Pckroutecode.Route_Code;
			}
			if(txtDeliveryRoute.Text != "")
			{
				com.ties.classes.RouteCode  dlvroutecode = new  com.ties.classes.RouteCode();
				dlvroutecode.Populate(utility.GetAppID(),utility.GetEnterpriseID(),txtDeliveryRoute.Text);         
				strDlvRouteCode = dlvroutecode.Route_Code;
			}
			if(strPckRouteCode == null)		
			{
				lblErrorMessage .Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROUTE_NOPCKAVB",utility.GetUserCulture());
				return;
			}
			if(strDlvRouteCode == null)		
			{
				lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROUTE_NODLVAVB",utility.GetUserCulture());
				return;
			}
			
			int iOperation = (int)ViewState["Operation"];
			try
			{
				if (iOperation == (int)Operation.Insert)
				{
					iRowsAffected = SysDataManager2.InsertZipCode(m_sdsZipCode.ds, rowIndex, m_strAppID, m_strEnterpriseID);
					m_sdsZipCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					ArrayList paramValues = new ArrayList();
					paramValues.Add(""+iRowsAffected);						
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_INS",utility.GetUserCulture(),paramValues);
				}
				else
				{
					DataSet dsChangedRow = m_sdsZipCode.ds.GetChanges();
					iRowsAffected = SysDataManager2.UpdateZipCode(dsChangedRow, m_strAppID, m_strEnterpriseID);
					m_sdsZipCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					ArrayList paramValues = new ArrayList();
					paramValues.Add(""+iRowsAffected);						
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_UPD",utility.GetUserCulture(),paramValues);					
				}
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				strMsg = appException.InnerException.Message;
				strMsg = appException.InnerException.InnerException.Message;
				if(strMsg.IndexOf("duplicate key") != -1 || strMsg.IndexOf("PRIMARY") != -1)
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_ZIP",utility.GetUserCulture());							
				}
				else if(strMsg.IndexOf("FOREIGN KEY") != -1 )
				{
					lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"NO_COUNTRY_STATE_ROUTE",utility.GetUserCulture());
				}										
				if(strMsg.IndexOf("unique") != -1 )
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_ZIP",utility.GetUserCulture());							
				}
				Logger.LogTraceError("ZipCode.aspx.cs","dgZipCode_Update","RBAC003",appException.Message.ToString());					
				return;
			}			
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			ViewState["Operation"] = Operation.None;
			dgZipCode.EditItemIndex = -1;
			BindGrid();						
		}

		protected void dgZipCode_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			int rowIndex = e.Item.ItemIndex;
			if ((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int)ViewState["Operation"] == (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				m_sdsZipCode.ds.Tables[0].Rows.RemoveAt(rowIndex);
			}
			ViewState["Operation"] = Operation.None;
			dgZipCode.EditItemIndex = -1;
			BindGrid();
			Logger.LogDebugInfo("ZipCode","dgZipCode_Cancel","INF004","updating data grid...");
		}

		protected void dgZipCode_Edit(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			if((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int) ViewState["Operation"] == (int)Operation.Insert && dgZipCode.EditItemIndex > 0)
			{
				m_sdsZipCode.ds.Tables[0].Rows.RemoveAt(dgZipCode.EditItemIndex);
				m_sdsZipCode.QueryResultMaxSize--;
				dgZipCode.CurrentPageIndex = 0;
			}
			dgZipCode.EditItemIndex = e.Item.ItemIndex;
			ViewState["Operation"] = Operation.Update;
			BindGrid();
			Logger.LogDebugInfo("ZipCode","dgZipCode_Edit","INF004","updating data grid...");
		}

		protected void dgZipCode_Delete(object sender, DataGridCommandEventArgs e)
		{
			if (!btnExecuteQuery.Enabled)
			{
				if ((int)ViewState["Operation"] == (int)Operation.Update)
				{
					dgZipCode.EditItemIndex = -1;
				}
				BindGrid();

				int rowIndex = e.Item.ItemIndex;
				m_sdsZipCode = (SessionDS)Session["SESSION_DS1"];

				try
				{
					// delete from table
					int iRowsDeleted = SysDataManager2.DeleteZipCode(m_sdsZipCode.ds, rowIndex, m_strAppID, m_strEnterpriseID);
					ArrayList paramValues = new ArrayList();
					paramValues.Add(""+iRowsDeleted);						
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_DLD",utility.GetUserCulture(),paramValues);
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					if(strMsg.IndexOf("FK") != -1)
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_ZIP_TRANS",utility.GetUserCulture());
					}
					if(strMsg.IndexOf("child record") != -1 || strMsg.IndexOf("REFERENCE") != -1)
					{//Oralce Child Record
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_ZIP_TRANS",utility.GetUserCulture());
					}
					Logger.LogTraceError("ZipCode.aspx.cs","dgZipCode_Delete","RBAC003",appException.Message.ToString());
					return;
				}

				// remove the data from grid
				if((int)ViewState["Mode"] == (int)ScreenMode.Insert)
				{
					m_sdsZipCode.ds.Tables[0].Rows.RemoveAt(rowIndex);
				}
				else
				{
					RetreiveSelectedPage();
				}
				
				//Set ViewStates to None
				ViewState["Operation"] = Operation.None;
				//Make the row in non-edit Mode
				EditHRow(false);			
				BindGrid();
				Logger.LogDebugInfo("ZipCode","dgZipCode_Delete","INF004","updating data grid...");			
			}
		}

		protected void dgZipCode_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsZipCode.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			if ((int)ViewState["Operation"] != (int)Operation.Insert && (!btnExecuteQuery.Enabled))
			{
				msTextBox txtZipCode = (msTextBox)e.Item.FindControl("txtZipCode");
				if(txtZipCode != null) 
				{
					txtZipCode.Enabled = false;
				}
				msTextBox txtStateCode = (msTextBox)e.Item.FindControl("txtStateCode");
				if(txtStateCode != null) 
				{
					txtStateCode.Enabled = false;
				}
				msTextBox txtCountry = (msTextBox)e.Item.FindControl("txtCountry");
				if(txtCountry != null) 
				{
					txtCountry.Enabled = false;
				}
				e.Item.Cells[4].Enabled=false;
			}

			if ((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int)ViewState["Operation"] == (int)Operation.Insert)
			{
				e.Item.Cells[1].Enabled=false;
			}
		}

		protected void dgZipCode_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgZipCode.CurrentPageIndex = e.NewPageIndex;
			RetreiveSelectedPage();
			Logger.LogDebugInfo("ZipCode","dgZipCode_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			dgZipCode.CurrentPageIndex = 0;
			QueryMode();
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			//Since Query mode is always one row in edit mode, the rowindex is set to zero
			int rowIndex = 0; 

			GetChangedRow(dgZipCode.Items[0], rowIndex);
			Session["QUERY_DS"] = m_sdsZipCode.ds;

			RetreiveSelectedPage();

			lblErrorMessage.Text = "";
			btnExecuteQuery.Enabled = false;

			//set all records to non-edit mode after execute query
			EditHRow(false);
			dgZipCode.Columns[0].Visible=true;
			dgZipCode.Columns[1].Visible=true;
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			//clear the rows from datagrid 
			if (btnExecuteQuery.Enabled == true)
			{
				btnExecuteQuery.Enabled = false;
			}
			if((int)ViewState["Mode"] != (int)ScreenMode.Insert || m_sdsZipCode.ds.Tables[0].Rows.Count >= dgZipCode.PageSize)
				m_sdsZipCode = SysDataManager2.GetEmptyZipCode();
			else
				AddRow();

			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,false,m_moduleAccessRights);
			EditHRow(false);
			ViewState["Mode"] = ScreenMode.Insert;
			ViewState["Operation"] = Operation.Insert;
			dgZipCode.Columns[0].Visible=true;
			dgZipCode.Columns[1].Visible=true;
			dgZipCode.EditItemIndex = m_sdsZipCode.ds.Tables[0].Rows.Count - 1;
			dgZipCode.CurrentPageIndex = 0;
			BindGrid();
			getPageControls(Page);
			Logger.LogDebugInfo("ZipCode","btnInsert_Click","INF003","Data Grid Items count"+dgZipCode.Items.Count);
		}

		public void dgZipCode_Button(object sender, DataGridCommandEventArgs e)
		{
			int iOperation = (int)ViewState["Operation"];
			String strCmdNm = e.CommandName;
			String strCountry = null;
			String strStateCode = null;
			String strZoneCode = null;
			String strPickupRoute = null;
			String strDeliveryRoute = null;

			if ((iOperation == (int)Operation.Insert) || (btnExecuteQuery.Enabled))
			{				
				if(strCmdNm.Equals("StateSearch"))
				{
					msTextBox txtStateCode = (msTextBox)e.Item.FindControl("txtStateCode");
					String strtxtStateCode = null;
					if(txtStateCode != null)
					{
						strtxtStateCode = txtStateCode.ClientID;
						strStateCode = txtStateCode.Text;
					}
					msTextBox txtCountry = (msTextBox)e.Item.FindControl("txtCountry");
					String strtxtCountry = null;
					if(txtCountry != null)
					{
						strtxtCountry = txtCountry.ClientID;
						strCountry = txtCountry.Text;
					}
				
					String sUrl = "StatePopup.aspx?FORMID=ZipCode&COUNTRY_TEXT="+strCountry+"&COUNTRY="+strtxtCountry+"&STATECODE_TEXT="+strStateCode+"&STATECODE="+strtxtStateCode;
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
			}
			if ((iOperation == (int)Operation.Insert) || (iOperation == (int)Operation.Update) || (btnExecuteQuery.Enabled))
			{				
				if(strCmdNm.Equals("ZoneSearch"))
				{
					msTextBox txtZoneCode = (msTextBox)e.Item.FindControl("txtZoneCode");
					String strtxtZoneCode = null;
					if(txtZoneCode != null)
					{
						strtxtZoneCode = txtZoneCode.ClientID;
						strZoneCode = txtZoneCode.Text;
					}
				
					String sUrl = "ZonePopup.aspx?FORMID=ZipCode&ZONECODE_TEXT="+strZoneCode+"&ZONECODE="+strtxtZoneCode;
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
			}
			if ((iOperation == (int)Operation.Insert) || (iOperation == (int)Operation.Update) || (btnExecuteQuery.Enabled))
			{				
				if(strCmdNm.Equals("PickupRouteSearch"))
				 {
					 msTextBox txtPickupRoute = (msTextBox)e.Item.FindControl("txtPickupRoute");

					 String strtxtPickupRoute = null;
					 if(txtPickupRoute != null)
					 {
						 strtxtPickupRoute = txtPickupRoute.ClientID;
						 strPickupRoute = txtPickupRoute.Text;
					 }
				
					 String sUrl = "RouteCodePopup.aspx?FORMID=ZipCode&ROUTECODE_TEXT="+strtxtPickupRoute+"&ROUTECODE="+strPickupRoute;
					 ArrayList paramList = new ArrayList();
					 paramList.Add(sUrl);
					 String sScript = Utility.GetScript("openWindowParam.js",paramList);
					 Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				 }
			}
			if ((iOperation == (int)Operation.Insert) || (iOperation == (int)Operation.Update) || (btnExecuteQuery.Enabled))
			{				
				if(strCmdNm.Equals("DeliveryRouteSearch"))
				{
					msTextBox txtDeliveryRoute = (msTextBox)e.Item.FindControl("txtDeliveryRoute");

					String strtxtDeliveryRoute = null;
					if(txtDeliveryRoute != null)
					{
						strtxtDeliveryRoute = txtDeliveryRoute.ClientID;
						strDeliveryRoute = txtDeliveryRoute.Text;
					}
				
					String sUrl = "RouteCodePopup.aspx?FORMID=ZipCode&ROUTECODE_TEXT="+strtxtDeliveryRoute+"&ROUTECODE="+strDeliveryRoute;
				
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
			}
		}

		private void btnGenerate_Click(object sender, System.EventArgs e)
		{
		}

		private void BindGrid()
		{
			dgZipCode.VirtualItemCount = System.Convert.ToInt32(m_sdsZipCode.QueryResultMaxSize);
			dgZipCode.DataSource = m_sdsZipCode.ds;
			dgZipCode.DataBind();
			Session["SESSION_DS1"] = m_sdsZipCode;
		}

		private void EditHRow(bool bItemIndex)
		{
			foreach (DataGridItem item in dgZipCode.Items)
			{ 
				if (bItemIndex) 
				{
					dgZipCode.EditItemIndex = item.ItemIndex; }
				else 
				{
					dgZipCode.EditItemIndex = -1; }
			}
			dgZipCode.DataBind();
		}

		private void QueryMode()
		{
			//Set Query Mode
			ViewState["Mode"] = ScreenMode.None;
			ViewState["Operation"] = Operation.None;
			//Set Button Enable Properties
			btnQuery.Enabled=true;
			btnExecuteQuery.Enabled=true;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			//inset an empty row for query 
			m_sdsZipCode = SysDataManager2.GetEmptyZipCode();
			m_sdsZipCode.DataSetRecSize = 1;
			Session["SESSION_DS1"] = m_sdsZipCode;
			BindGrid();
			//Make the row in Edit Mode
			EditHRow(true);
			dgZipCode.Columns[0].Visible=false;
			dgZipCode.Columns[1].Visible=false;
		}

		private void AddRow()
		{
			SysDataManager2.AddNewRowInZipCodeDS(ref m_sdsZipCode);
			BindGrid();
		}

		private void GetChangedRow(DataGridItem item, int rowIndex)
		{
			msTextBox txtZipCode = (msTextBox)item.FindControl("txtZipCode");
			msTextBox txtCountry = (msTextBox)item.FindControl("txtCountry");
			msTextBox txtStateCode = (msTextBox)item.FindControl("txtStateCode");
			msTextBox txtZoneCode = (msTextBox)item.FindControl("txtZoneCode");
			msTextBox txtCutoffTime = (msTextBox)item.FindControl("txtCutoffTime");
			TextBox txtPickupRoute = (TextBox)item.FindControl("txtPickupRoute");
			TextBox txtDeliveryRoute = (TextBox)item.FindControl("txtDeliveryRoute");
			TextBox txtArea = (TextBox)item.FindControl("txtArea");
			TextBox txtRegion = (TextBox)item.FindControl("txtRegion");
			msTextBox txtEsaSurcharge = (msTextBox)item.FindControl("txtEsaSurcharge");
			
			string strZip = txtZipCode.Text.ToString();
			string strCountry = txtCountry.Text.ToString();
			string strState = txtStateCode.Text.ToString();
			string strZone = txtZoneCode.Text.ToString();
			string strCutoff = txtCutoffTime.Text.ToString();
			string strPickup = txtPickupRoute.Text.ToString();
			string strDelivery = txtDeliveryRoute.Text.ToString();
			string strArea = txtArea.Text.ToString();
			string strRegion = txtRegion.Text.ToString();
			decimal decESurcharge = 0;

			if ((txtEsaSurcharge.Text !=null) && (txtEsaSurcharge.Text != ""))
			{
				decESurcharge = Convert.ToDecimal(txtEsaSurcharge.Text.ToString());
			}

			DataRow dr = m_sdsZipCode.ds.Tables[0].Rows[rowIndex];
			String tmpDate = System.DateTime.Now.ToString("dd/MM/yyyy");
			int vDate = Convert.ToInt32(tmpDate.Substring(0,2));
			int vMonth = Convert.ToInt32(tmpDate.Substring(3,2));
			int vYear = Convert.ToInt32(tmpDate.Substring(6,4));
			DateTime dtTmp = new DateTime(vYear,vMonth,vDate,0,0,0);
			dr[0] = strZip;
			dr[1] = strCountry;
			dr[2] = strState;
			dr[3] = strZone;
			if (strCutoff!=null && strCutoff!="")
			{
				int vHour = Convert.ToInt32(strCutoff.Substring(0, 2));
				int vMin = Convert.ToInt32(strCutoff.Substring(3, 2));
				dtTmp = new DateTime(vYear,vMonth,vDate,vHour,vMin,0);
				dr[4] = dtTmp;
			}
			else
			{
				if (!btnExecuteQuery.Enabled)
				{
					dr[4] = dtTmp;
				}
			}
			dr[5] = strPickup;
			dr[6] = strDelivery;
			dr[7] = strArea;
			dr[8] = strRegion;
			dr[9] = decESurcharge;
		}

		private void RetreiveSelectedPage()
		{
			int iStartRow = dgZipCode.CurrentPageIndex * dgZipCode.PageSize;
			DataSet dsQuery = (DataSet)Session["QUERY_DS"];
			m_sdsZipCode = SysDataManager2.GetZipCodeDS(utility.GetAppID(), utility.GetEnterpriseID(), iStartRow, dgZipCode.PageSize, dsQuery);
			int iPageCnt = Convert.ToInt32((m_sdsZipCode.QueryResultMaxSize - 1))/dgZipCode.PageSize;
			if(iPageCnt < dgZipCode.CurrentPageIndex)
			{
				dgZipCode.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			dgZipCode.SelectedIndex = -1;
			dgZipCode.EditItemIndex = -1;

			BindGrid();
			Session["SESSION_DS1"] = m_sdsZipCode;
		}

		private void dgZipCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

	}
}
