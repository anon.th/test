using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using System.Text;
using System.Configuration;
using System.Data.OleDb;
using Cambro.Web.DbCombo;
using TIESDAL; 

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CustomsReport_CheckRequisitions.
	/// </summary>
	public class CustomsReport_CheckRequisitions : BasePage
	{
		String strEnterpriseID=null;
		String strAppID=null;
		String strUserLoggin=null; 
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.Button btnPrintChequeReq;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.DropDownList ddlRequisitionType;
		protected com.common.util.msTextBox txtRequisitionNumber;
		protected System.Web.UI.WebControls.TextBox txtPayee;
		protected System.Web.UI.WebControls.TextBox txtStatus;
		protected System.Web.UI.WebControls.TextBox txtChequeNumber;
		protected com.common.util.msTextBox txtDateIssued;
		protected System.Web.UI.WebControls.TextBox txtReceiptNumber;
		protected com.common.util.msTextBox txtReceiptDate;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.TextBox txtRequestedDate;
		protected System.Web.UI.WebControls.TextBox txtRequestedBy;
		protected System.Web.UI.WebControls.Label Label18; 
		protected System.Web.UI.WebControls.Label Label17; 
		protected System.Web.UI.WebControls.Label Label19; 
		protected System.Web.UI.WebControls.Button btnSearchJobs;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.TextBox txtMaximumInvoices;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.TextBox txtRemainingInvoices;
		protected System.Web.UI.WebControls.Label Label22;
		protected com.common.util.msTextBox txtTotalRequisitionAmount;
		protected System.Web.UI.WebControls.TextBox txtModifiedBy;
		protected System.Web.UI.WebControls.TextBox txtModifiedDate;
		protected System.Web.UI.WebControls.TextBox txtApprovedBy;
		protected System.Web.UI.WebControls.TextBox txtApprovedDate;
		protected System.Web.UI.WebControls.TextBox txtPaidBy;
		protected System.Web.UI.WebControls.TextBox txtPaidDate;
		protected System.Web.UI.WebControls.DataGrid gvCheqRequisitionA;
		protected System.Web.UI.WebControls.DataGrid gvCheqRequisitionN;
		protected System.Web.UI.WebControls.Label lblGridError;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.WebControls.TextBox txtChequeExportedBy;
		protected System.Web.UI.WebControls.TextBox txtChequeExportedDT;
		protected System.Web.UI.WebControls.Button btnClientEvent;
		protected System.Web.UI.WebControls.DataGrid gvCheqRequisitionFormal;
		protected System.Web.UI.WebControls.DataGrid gvITF;

		DataSet m_dsQuery = null;

		public string GridCmd
		{
			get
			{
				if(ViewState["GridCmd"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["GridCmd"];
				}
			}
			set
			{
				ViewState["GridCmd"]=value;
			}
		}


		public string currency_decimal
		{
			get
			{
				if(ViewState["currency_decimal"] == null)
				{
					return "00";
				}
				else
				{
					return (string)ViewState["currency_decimal"];
				}
			}
			set
			{
				ViewState["currency_decimal"]=value;
			}
		}

		public string NormalReportTemplate
		{
			get
			{
				if(ViewState["NormalReportTemplate"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["NormalReportTemplate"];
				}
			}
			set
			{
				ViewState["NormalReportTemplate"] = value;
			}
		}

		public string AdhocRportTemplate
		{
			get
			{
				if(ViewState["AdhocRportTemplate"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["AdhocRportTemplate"];
				}
			}
			set
			{
				ViewState["AdhocRportTemplate"] = value;
			}
		}

		public string FormalReportTemplate
		{
			get
			{
				if(ViewState["FormalReportTemplate"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["FormalReportTemplate"];
				}
			}
			set
			{
				ViewState["FormalReportTemplate"] = value;
			}
		}

		public string ITFReportTemplate
		{
			get
			{
				if(ViewState["ITFReportTemplate"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["ITFReportTemplate"];
				}
			}
			set
			{
				ViewState["ITFReportTemplate"] = value;
			}
		}


		public int IsCUSTOMSCR
		{
			get
			{
				if(ViewState["IsCUSTOMSCR"] == null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["IsCUSTOMSCR"];
				}
			}
			set
			{
				ViewState["IsCUSTOMSCR"]=value;
			}
		}


		public int NumberScale
		{
			get
			{
				if(ViewState["NumberScale"] == null)
				{
					return 2;
				}
				else
				{
					return (int)ViewState["NumberScale"];
				}
			}
			set
			{
				ViewState["NumberScale"]=value;
			}
		}


		public int GridRows
		{
			get
			{
				if(ViewState["GridRows"] == null)
				{
					return 25;
				}
				else
				{
					return (int)ViewState["GridRows"];
				}
			}
			set
			{
				ViewState["GridRows"]=value;
			}
		}


		public DataSet dsRequisitionType
		{
			get
			{
				if(ViewState["dsRequisitionType"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsRequisitionType"];
				}
			}
			set
			{
				ViewState["dsRequisitionType"]=value;
			} 
		}


		public DataSet dsAllResult
		{
			get
			{
				if(ViewState["dsAllResult"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsAllResult"];
				}
			}
			set
			{
				ViewState["dsAllResult"]=value;
			} 
		}


		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();
			strUserLoggin = utility.GetUserID();
			if(!Page.IsPostBack)
			{ 
				currency_decimal =  this.GetCurrencyDigitsOfEnterpriseConfiguration();	
				dsRequisitionType =   CustomsCheckRequisitionDAL.GetChequeRequisitionTypes(strAppID, strEnterpriseID); 
				ddlRequisitionType.DataSource= dsRequisitionType;
				ddlRequisitionType.DataTextField="DropDownListDisplayValue";
				ddlRequisitionType.DataValueField="CodedValue";
				ddlRequisitionType.DataBind();  			

				ClearScreen();
				SetInitialFocus(txtRequisitionNumber);
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//			
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnPrintChequeReq.Click += new System.EventHandler(this.btnPrintChequeReq_Click);
			this.btnClientEvent.Click += new System.EventHandler(this.btnClientEvent_Click);
			this.btnSearchJobs.Click += new System.EventHandler(this.btnSearchJobs_Click);
			this.gvCheqRequisitionN.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.gvCheqRequisitionN_ItemCommand);
			this.gvCheqRequisitionN.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.gvCheqRequisitionN_PageIndexChanged);
			this.gvCheqRequisitionN.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.gvCheqRequisitionN_ItemDataBound);
			this.gvCheqRequisitionA.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.gvCheqRequisitionA_ItemCommand);
			this.gvCheqRequisitionA.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.gvCheqRequisitionA_PageIndexChanged);
			this.gvCheqRequisitionA.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.gvCheqRequisitionA_ItemDataBound);
			this.gvCheqRequisitionFormal.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.gvCheqRequisitionFormal_ItemCommand);
			this.gvCheqRequisitionFormal.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.gvCheqRequisitionFormal_PageIndexChanged);
			this.gvCheqRequisitionFormal.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.gvCheqRequisitionFormal_ItemDataBound);
			this.gvITF.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.gvITF_ItemCommand);
			this.gvITF.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.gvITF_PageIndexChanged);
			this.gvITF.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.gvITF_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
 
		#region Event Click

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			InsertData();
			SetInitialFocus(txtPayee);
		}


		private void btnPrintChequeReq_Click(object sender, System.EventArgs e)
		{
			if(ddlRequisitionType.SelectedItem.Value == "N")
			{
				m_dsQuery = CustomsCheckRequisitionDAL.GetChequeReqDataToReport(utility.GetAppID(), utility.GetEnterpriseID(), txtRequisitionNumber.Text);

				String strUrl = null;
				strUrl = "ReportViewer1.aspx";
				Session["REPORT_TEMPLATE"] = NormalReportTemplate;
				Session["FORMID"] = "TNT_Normal_ChequeReq";
				Session["SESSION_DS_PRINTCHEQUEREQ"] = m_dsQuery;
				Session["FormatType"] = ".pdf";
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindowReport.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);	
			}
			else if(ddlRequisitionType.SelectedItem.Value == "A")
			{
				m_dsQuery = CustomsCheckRequisitionDAL.GetChequeReqAdhocDataToReport(utility.GetAppID(), utility.GetEnterpriseID(), txtRequisitionNumber.Text);

				String strUrl = null;
				strUrl = "ReportViewer1.aspx";
				Session["REPORT_TEMPLATE"] = AdhocRportTemplate;
				Session["FORMID"] = "TNT_Normal_ChequeReq_Adhoc";
				Session["SESSION_DS_PRINTCHEQUEREQ_ADHOC"] = m_dsQuery;
				Session["FormatType"] = ".pdf";
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindowReport.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);	
			}
			else if(ddlRequisitionType.SelectedItem.Value == "F")
			{
				m_dsQuery = CustomsCheckRequisitionDAL.GetChequeReqFormalDataToReport(utility.GetAppID(), utility.GetEnterpriseID(), txtRequisitionNumber.Text);
				String strUrl = null;
				strUrl = "ReportViewer1.aspx";
				Session["REPORT_TEMPLATE"] = FormalReportTemplate;
				Session["FORMID"] = "TNT_Formal_ChequeReq_Adhoc";
				Session["SESSION_DS_PRINTCHEQUEREQ_FORMAL"] = m_dsQuery;
				Session["FormatType"] = ".pdf";
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindowReport.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			else if(ddlRequisitionType.SelectedItem.Value == "I")
			{
				m_dsQuery = CustomsCheckRequisitionDAL.GetChequeReqITFDataToReport(utility.GetAppID(), utility.GetEnterpriseID(), txtRequisitionNumber.Text);
				String strUrl = null;
				strUrl = "ReportViewer1.aspx";
				Session["REPORT_TEMPLATE"] = ITFReportTemplate;
				Session["FORMID"] = "TNT_ITF_ChequeReq";
				Session["SESSION_DS_PRINTCHEQUEREQ_ITF"] = m_dsQuery;
				Session["FormatType"] = ".pdf";
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindowReport.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			SaveData();
		}


		private void btnQry_Click(object sender, System.EventArgs e)
		{
			ClearScreen();
			SetInitialFocus(txtRequisitionNumber);
		}
		

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			GridCmd=null;
			lblError.Text = string.Empty;
			if(txtRequisitionNumber.Text == "" && txtChequeNumber.Text == "" && txtReceiptNumber.Text == "")
			{
				lblError.Text="Execute Query action requires either Requisition, Cheque or Receipt Number";
				SetInitialFocus(txtRequisitionNumber);
				return  ;
			}			
			ExecQryData();
			if(lblError.Text.Trim() == "")
			{
				btnExecQry.Enabled=false;
				btnInsert.Enabled=false;
				txtRequisitionNumber.Enabled=false;
				btnPrintChequeReq.Enabled=true;
				ddlRequisitionType.Enabled=false;
				if (ddlRequisitionType.SelectedValue == "F")
				{
					btnSearchJobs.Enabled=false;
					if(gvCheqRequisitionFormal.Items.Count>0)
					{
						
						TextBox txtAddInvoice_No = (TextBox)gvCheqRequisitionFormal.Controls[0].Controls[gvCheqRequisitionFormal.Controls[0].Controls.Count - 1].FindControl("txtAddInvoice_NoFormal");
						//SetInitialFocus(txtAddInvoice_No);
						SetInitialFocus(txtPayee); 
					}	
				}
				else if (ddlRequisitionType.SelectedValue == "N")
				{
					if(gvCheqRequisitionN.Items.Count>0)
					{
						TextBox txtAddInvoice_No = (TextBox)gvCheqRequisitionN.Controls[0].Controls[gvCheqRequisitionN.Controls[0].Controls.Count - 1].FindControl("txtAddInvoice_No");
						//SetInitialFocus(txtAddInvoice_No);
						SetInitialFocus(txtPayee); 
					}					
				}
				else if (ddlRequisitionType.SelectedValue == "I")
				{
					if(gvITF.Items.Count>0)
					{
						TextBox txtAddMasterAWBNumberITF = (TextBox)gvITF.Controls[0].Controls[gvITF.Controls[0].Controls.Count - 1].FindControl("txtAddMasterAWBNumberITF");
						//SetInitialFocus(txtAddInvoice_No);
						SetInitialFocus(txtAddMasterAWBNumberITF); 
					}					
				}
				else
				{
					if(gvCheqRequisitionA.Items.Count>0)
					{
						TextBox txtAddInvoice_NoA = (TextBox)gvCheqRequisitionA.Controls[0].Controls[gvCheqRequisitionA.Controls[0].Controls.Count - 1].FindControl("txtAddInvoice_NoA");
						//SetInitialFocus(txtAddInvoice_NoA);
						SetInitialFocus(txtPayee);
					}

				}
			}
			SetInitialFocus(txtPayee);

		}
		

		#endregion Event Click

		#region Method
		private void InsertData()
		{
			try
			{
				DataTable dtParams = CustomsCheckRequisitionDAL.CustomsChequeRequisition();
				System.Data.DataRow drNew = dtParams.NewRow();
				drNew["action"] ="1";
				drNew["enterpriseid"] =strEnterpriseID;
				drNew["userloggedin"] =strUserLoggin;
				drNew["requisition_no"] = txtRequisitionNumber.Text;
				drNew["Cheque_No"] = txtChequeNumber.Text;
				drNew["Receipt_No"] =txtReceiptNumber.Text;
				drNew["payee"] =txtPayee.Text;	
				drNew["ChequeReq_Type"] = ddlRequisitionType.SelectedValue;
				drNew["Receipt_Date"] = txtReceiptDate.Text;				
				dtParams.Rows.Add(drNew);
 
				dsAllResult = CustomsCheckRequisitionDAL.Customs_ChequeRequisition(strAppID, strEnterpriseID, strUserLoggin, dtParams );
				System.Data.DataTable dtStatus = dsAllResult.Tables[0];
				System.Data.DataTable dtCheck= dsAllResult.Tables[1];

				if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
				{
					if(dtCheck.Rows.Count<=0)
					{
						return;
					}
					DataRow dr = dtCheck.Rows[0];
					txtStatus.Text=dr["CheqReq_Status"].ToString();
					txtPayee.Text=dr["payee"].ToString();  

					BindText(dsAllResult); 
					ddlRequisitionType.Enabled=false;
					txtRequisitionNumber.Enabled=false;
					btnInsert.Enabled=false;
					btnExecQry.Enabled=false;
					btnPrintChequeReq.Enabled=false;
					btnSearchJobs.Enabled=false;
				}
				else
				{
					lblError.Text=(dtStatus.Rows.Count>0?dtStatus.Rows[0]["ErrorMessage"].ToString():"");
				}
			}
			catch (Exception ex)
			{
				lblError.Text= ex.Message;
			}
		    
		}


		private void SaveData()
		{
			try
			{
				DataTable dtParams = CustomsCheckRequisitionDAL.CustomsChequeRequisition();
				System.Data.DataRow drNew = dtParams.NewRow();
				drNew["action"] ="2";
				drNew["enterpriseid"] =strEnterpriseID;
				drNew["userloggedin"] =strUserLoggin;
				drNew["requisition_no"] = txtRequisitionNumber.Text;
				drNew["Cheque_No"] = txtChequeNumber.Text;
				drNew["Receipt_No"] =txtReceiptNumber.Text;
				drNew["payee"] =txtPayee.Text;	
				drNew["ChequeReq_Type"] = ddlRequisitionType.SelectedValue;
				drNew["Receipt_Date"] = (txtReceiptDate.Text.Trim() != "" ? DateTime.ParseExact(txtReceiptDate.Text,"dd/MM/yyyy",null).ToString("yyyy-MM-dd"):"");		
				drNew["Date_Cheque_Issued"] = (txtDateIssued.Text.Trim() != "" ? DateTime.ParseExact(txtDateIssued.Text,"dd/MM/yyyy",null).ToString("yyyy-MM-dd"):"");	
				dtParams.Rows.Add(drNew);
 
				dsAllResult = CustomsCheckRequisitionDAL.Customs_ChequeRequisition(strAppID, strEnterpriseID, strUserLoggin, dtParams);
				System.Data.DataTable dtStatus = dsAllResult.Tables[0];
				System.Data.DataTable dtCheck= dsAllResult.Tables[1];

				if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
				{
					if(dtCheck.Rows.Count<=0)
					{
						return;
					}
					DataRow dr = dtCheck.Rows[0];
					txtStatus.Text=dr["CheqReq_Status"].ToString();
					txtPayee.Text=dr["payee"].ToString();  
					BindText(dsAllResult);
					lblError.Text=(dtStatus.Rows.Count>0?dtStatus.Rows[0]["ErrorMessage"].ToString():"");
				}
				else
				{
					lblError.Text=(dtStatus.Rows.Count>0?dtStatus.Rows[0]["ErrorMessage"].ToString():"");
				}
			}
			catch (Exception ex)
			{
				lblError.Text= ex.Message;
			}
		    
		}


		private void ClearScreen()
		{
			lblError.Text= "";
			//lblPagingInfo.Text="";
			lblGridError.Text = "";
			txtPayee.Text="";

			ddlRequisitionType.SelectedIndex = 0;
			txtRequisitionNumber.Text = "";
			txtRequisitionNumber.Enabled = true;
			txtStatus.Text="";
			txtPayee.Text = "";
			txtChequeNumber.Enabled = true; //.Text = dsResult.Tables[1].Rows[0]["Cheque_no"].ToString();
			txtChequeNumber.Text = "";
			txtDateIssued.Text="";
			txtReceiptNumber.Text="";
			txtReceiptDate.Text="";
			txtRequestedBy.Text="";
			txtRequestedDate.Text="";
			txtModifiedBy.Text="";
			txtModifiedDate.Text="";
			txtApprovedBy.Text="";
			txtApprovedDate.Text="";
			txtChequeExportedBy.Text="";
			txtChequeExportedDT.Text="";
			txtPaidBy.Text="";
			txtPaidDate.Text=""; 
			txtChequeNumber.Enabled = true;
			txtDateIssued.Enabled = true;
			txtReceiptNumber.Enabled = true; 
			txtReceiptDate.Enabled = true;
			txtRequisitionNumber.Enabled=true;

			txtChequeNumber.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
			txtDateIssued.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
			txtReceiptNumber.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
			txtReceiptDate.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");

			txtMaximumInvoices.Text = "";
			txtRemainingInvoices.Text = "";
			txtTotalRequisitionAmount.Text = "";
			
			GridCmd=null;
			ddlRequisitionType.Enabled=true;
			btnExecQry.Enabled = true;
			btnInsert.Enabled = true;
			btnSave.Enabled = false;
			btnPrintChequeReq.Enabled = false;
			btnSearchJobs.Enabled=false;

			IsCUSTOMSCR=0;

			gvCheqRequisitionA.DataSource = null;

			if (ddlRequisitionType.SelectedValue == "F")
			{
				gvCheqRequisitionFormal.DataSource = CustomsCheckRequisitionDAL.CustomsChequeRequisition();
				gvCheqRequisitionFormal.DataBind();

				gvCheqRequisitionN.DataSource = null;
				gvCheqRequisitionN.DataBind();

				gvCheqRequisitionA.DataSource = null;
				gvCheqRequisitionA.DataBind();

				gvITF.DataSource = null;
				gvITF.DataBind();
			}
			else if (ddlRequisitionType.SelectedValue == "N")
			{
				gvCheqRequisitionN.DataSource = CustomsCheckRequisitionDAL.CustomsChequeRequisition();
				gvCheqRequisitionN.DataBind();

				gvCheqRequisitionA.DataSource = null;
				gvCheqRequisitionA.DataBind();

				gvCheqRequisitionFormal.DataSource = null;
				gvCheqRequisitionFormal.DataBind();

				gvITF.DataSource = null;
				gvITF.DataBind();
			}
			else if (ddlRequisitionType.SelectedValue == "A")
			{
				gvCheqRequisitionA.DataSource = CustomsCheckRequisitionDAL.CustomsChequeRequisition();
				gvCheqRequisitionA.DataBind();

				gvCheqRequisitionN.DataSource = null;
				gvCheqRequisitionN.DataBind();

				gvCheqRequisitionFormal.DataSource = null;
				gvCheqRequisitionFormal.DataBind();

				gvITF.DataSource = null;
				gvITF.DataBind();
			}	
			else if (ddlRequisitionType.SelectedValue == "I")
			{
				gvITF.DataSource = CustomsCheckRequisitionDAL.CustomsChequeRequisitionITF();
				gvITF.DataBind();

				gvCheqRequisitionA.DataSource = null;
				gvCheqRequisitionA.DataBind();

				gvCheqRequisitionN.DataSource = null;
				gvCheqRequisitionN.DataBind();

				gvCheqRequisitionFormal.DataSource = null;
				gvCheqRequisitionFormal.DataBind();
			}	
		}


		private void ExecQryData()
		{
			try
			{
				DataTable dtParams = CustomsCheckRequisitionDAL.CustomsChequeRequisition();
				System.Data.DataRow drNew = dtParams.NewRow();
				drNew["action"] ="0";
				drNew["enterpriseid"] =strEnterpriseID;
				drNew["userloggedin"] =strUserLoggin;
				drNew["requisition_no"] = txtRequisitionNumber.Text; 
				drNew["Cheque_No"] = txtChequeNumber.Text;
				drNew["Receipt_No"] = txtReceiptNumber.Text;

				dtParams.Rows.Add(drNew);
 
				dsAllResult = CustomsCheckRequisitionDAL.Customs_ChequeRequisition(strAppID, strEnterpriseID, strUserLoggin, dtParams );
				System.Data.DataTable dtStatus = dsAllResult.Tables[0];
				System.Data.DataTable dtCheck= dsAllResult.Tables[1];

				if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
				{
					if(dtCheck.Rows.Count<=0)
					{
						lblError.Text="Requisition number not found";
						return;
					}
					BindText(dsAllResult);				
				}
				else
				{
					lblError.Text=(dtStatus.Rows.Count>0?dtStatus.Rows[0]["ErrorMessage"].ToString():"");
				}
			}
			catch (Exception ex)
			{
				lblError.Text= ex.Message;
			}
		    
		}


		private void BindText(DataSet dsResult )
		{ 
			//lblPagingInfo.Text="";
			lblError.Text = "";
			try
			{		 
				IsCUSTOMSCR=0;
				ddlRequisitionType.SelectedValue = dsResult.Tables[1].Rows[0]["ChequeReq_Type"].ToString();	
				txtRequisitionNumber.Text = dsResult.Tables[1].Rows[0]["requisition_no"].ToString();
				txtPayee.Text=dsResult.Tables[1].Rows[0]["payee"].ToString();  
				txtStatus.Text=dsResult.Tables[1].Rows[0]["CheqReq_Status"].ToString();
				
				txtChequeNumber.Text= dsResult.Tables[1].Rows[0]["Cheque_No"].ToString();
				//txtDateIssued.Text=(dsResult.Tables[1].Rows[0]["Date_Cheque_Issued"] != DBNull.Value ? DateTime.ParseExact(dsResult.Tables[1].Rows[0]["Date_Cheque_Issued"].ToString(),"yyyy-MM-dd",null).ToString("dd/MM/yyyy") :"");
                txtDateIssued.Text = (dsResult.Tables[1].Rows[0]["Date_Cheque_Issued"] != DBNull.Value ? DateTime.Parse(dsResult.Tables[1].Rows[0]["Date_Cheque_Issued"].ToString()).ToString("dd/MM/yyyy") : "");

                txtReceiptNumber.Text = dsResult.Tables[1].Rows[0]["Receipt_No"].ToString();
				//txtReceiptDate.Text=(dsResult.Tables[1].Rows[0]["Receipt_Date"] != DBNull.Value ? DateTime.Parse(dsResult.Tables[1].Rows[0]["Receipt_Date"].ToString()).ToString("dd/MM/yyyy HH:mm") :"");
				txtReceiptDate.Text=(dsResult.Tables[1].Rows[0]["Receipt_Date"] != DBNull.Value ? DateTime.Parse(dsResult.Tables[1].Rows[0]["Receipt_Date"].ToString()).ToString("dd/MM/yyyy") :"");

				txtMaximumInvoices.Text = dsResult.Tables[0].Rows[0]["MaxInvoicesOnCheqReq"].ToString();
				txtRemainingInvoices.Text = dsResult.Tables[0].Rows[0]["RemainingInvoices"].ToString();
				txtTotalRequisitionAmount.Text = Convert.ToDecimal(dsResult.Tables[0].Rows[0]["TotalRequisitionAmount"]).ToString(currency_decimal);
											
				txtRequestedBy.Text = dsResult.Tables[1].Rows[0]["insertedBy"].ToString(); 
				txtRequestedDate.Text = (dsResult.Tables[1].Rows[0]["insertedDT"] != DBNull.Value ? DateTime.Parse(dsResult.Tables[1].Rows[0]["insertedDT"].ToString()).ToString("dd/MM/yyyy HH:mm") :"");

				txtModifiedBy.Text = dsResult.Tables[1].Rows[0]["UpdatedBy"].ToString(); 
				txtModifiedDate.Text = (dsResult.Tables[1].Rows[0]["UpdatedDT"] != DBNull.Value ? DateTime.Parse(dsResult.Tables[1].Rows[0]["UpdatedDT"].ToString()).ToString("dd/MM/yyyy HH:mm") :"");

				txtApprovedBy.Text = dsResult.Tables[1].Rows[0]["ApprovedBy"].ToString(); 
				txtApprovedDate.Text = (dsResult.Tables[1].Rows[0]["ApprovedDT"] != DBNull.Value ? DateTime.Parse(dsResult.Tables[1].Rows[0]["ApprovedDT"].ToString()).ToString("dd/MM/yyyy HH:mm") :"");

				txtPaidBy.Text = dsResult.Tables[1].Rows[0]["PaidBy"].ToString(); 
				txtPaidDate.Text = (dsResult.Tables[1].Rows[0]["PaidDT"] != DBNull.Value ? DateTime.Parse(dsResult.Tables[1].Rows[0]["PaidDT"].ToString()).ToString("dd/MM/yyyy HH:mm") :"");

				txtChequeExportedBy.Text = dsResult.Tables[1].Rows[0]["Cheque_ExportedBy"].ToString(); 
				txtChequeExportedDT.Text = (dsResult.Tables[1].Rows[0]["Cheque_ExportedDT"] != DBNull.Value ? DateTime.Parse(dsResult.Tables[1].Rows[0]["Cheque_ExportedDT"].ToString()).ToString("dd/MM/yyyy HH:mm") :"");
					
				NormalReportTemplate = dsResult.Tables[0].Rows[0]["NormalReportTemplate"].ToString();
				AdhocRportTemplate = dsResult.Tables[0].Rows[0]["AdhocReportTemplate"].ToString();
				FormalReportTemplate = dsResult.Tables[0].Rows[0]["FormalReportTemplate"].ToString();
				ITFReportTemplate = dsResult.Tables[0].Rows[0]["ITFReportTemplate"].ToString();

				if (dsResult.Tables[0].Rows[0]["UserIsCUSTOMSSU"].ToString() == "0" && dsResult.Tables[0].Rows[0]["UserIsCUSTOMSCR"].ToString() == "0")
				{
					txtChequeNumber.Enabled = false;
					txtDateIssued.Enabled = false;
					txtReceiptNumber.Enabled = false; //.Text = dsResult.Tables[1].Rows[0]["Cheque_no"].ToString();
					txtReceiptDate.Enabled = false;

					txtChequeNumber.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
					txtDateIssued.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
					txtReceiptNumber.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
					txtReceiptDate.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
				}
				else if (dsResult.Tables[0].Rows[0]["UserIsCUSTOMSSU"].ToString() == "1" || dsResult.Tables[0].Rows[0]["UserIsCUSTOMSCR"].ToString() == "1")
				{
					txtChequeNumber.Enabled = true;
					txtDateIssued.Enabled = true;
					txtReceiptNumber.Enabled = true; 
					txtReceiptDate.Enabled = true;

					txtChequeNumber.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
					txtDateIssued.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
					txtReceiptNumber.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
					txtReceiptDate.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
					if(dsResult.Tables[0].Rows[0]["UserIsCUSTOMSCR"].ToString() == "1")
					{
						IsCUSTOMSCR=1;
					}
				}

				btnSearchJobs.Enabled=true;
				if (dsResult.Tables[0].Rows[0]["IsApproved"].ToString() == "1")
				{
					txtChequeNumber.Enabled = false;
					txtDateIssued.Enabled = false;
					txtChequeNumber.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
					txtDateIssued.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
					btnSearchJobs.Enabled=false;
				}

				if (dsResult.Tables[0].Rows[0]["IsClosed"].ToString() == "1")
				{
					txtReceiptNumber.Enabled = false; //.Text = dsResult.Tables[1].Rows[0]["Cheque_no"].ToString();
					txtReceiptDate.Enabled = false;
					txtReceiptNumber.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
					txtReceiptDate.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
				}
				
				// CR14-03
				// This allows the CUSTOMSSU user to reverse the approval/closure process by removing either receipt number/date and/or cheque number/issue date
				if (dsResult.Tables[0].Rows[0]["UserIsCUSTOMSSU"].ToString() == "1" 
					&& (dsResult.Tables[0].Rows[0]["IsApproved"].ToString() == "1" || dsResult.Tables[0].Rows[0]["IsClosed"].ToString() == "1"))
				{
					txtChequeNumber.Enabled = true;
					txtDateIssued.Enabled = true;
					txtChequeNumber.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
					txtDateIssued.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");

					txtReceiptNumber.Enabled = true; 
					txtReceiptDate.Enabled = true;
					txtReceiptNumber.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
					txtReceiptDate.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
				}
				//
 				
				btnExecQry.Enabled = false;
				btnInsert.Enabled = false;
				btnSave.Enabled = true;
				btnPrintChequeReq.Enabled = true;
				
				if (ddlRequisitionType.SelectedValue == "F")
				{			
					gvCheqRequisitionFormal.EditItemIndex = -1;
					gvCheqRequisitionFormal.PageSize=this.GridRows;
					gvCheqRequisitionFormal.CurrentPageIndex=0;
					gvCheqRequisitionFormal.DataSource = dsResult.Tables[4];
					gvCheqRequisitionFormal.DataBind();

					gvCheqRequisitionN.DataSource = null;
					gvCheqRequisitionN.DataBind();

					gvCheqRequisitionA.DataSource = null;
					gvCheqRequisitionA.DataBind();

					btnSearchJobs.Enabled=false;


					//lblPagingInfo.Text="Page "+ (gvCheqRequisitionFormal.CurrentPageIndex+1).ToString() + " of " +gvCheqRequisitionFormal.PageCount.ToString();
				}
				else if (ddlRequisitionType.SelectedValue == "N")
				{
					gvCheqRequisitionN.EditItemIndex = -1;
					gvCheqRequisitionN.PageSize=this.GridRows;
					gvCheqRequisitionN.CurrentPageIndex=0;
					gvCheqRequisitionN.DataSource = dsResult.Tables[2];
					gvCheqRequisitionN.DataBind();

					gvCheqRequisitionA.DataSource = null;
					gvCheqRequisitionA.DataBind();

					gvCheqRequisitionFormal.DataSource = null;
					gvCheqRequisitionFormal.DataBind();

					btnSearchJobs.Enabled=true;
					//lblPagingInfo.Text="Page "+ (gvCheqRequisitionN.CurrentPageIndex+1).ToString() + " of " +gvCheqRequisitionN.PageCount.ToString();
				}
				else if (ddlRequisitionType.SelectedValue == "A")
				{
					gvCheqRequisitionA.EditItemIndex = -1;
					gvCheqRequisitionA.PageSize=this.GridRows;
					gvCheqRequisitionA.CurrentPageIndex=0;
					gvCheqRequisitionA.DataSource = dsResult.Tables[3];
					gvCheqRequisitionA.DataBind();

					gvCheqRequisitionN.DataSource = null;
					gvCheqRequisitionN.DataBind();

					gvCheqRequisitionFormal.DataSource = null;
					gvCheqRequisitionFormal.DataBind();

					gvITF.DataSource = null;
					gvITF.DataBind();

					btnSearchJobs.Enabled=true;
					//lblPagingInfo.Text="Page "+ (gvCheqRequisitionA.CurrentPageIndex+1).ToString() + " of " +gvCheqRequisitionA.PageCount.ToString();
				}
				else if (ddlRequisitionType.SelectedValue == "I")
				{
					gvITF.EditItemIndex = -1;
					gvITF.PageSize=this.GridRows;
					gvITF.CurrentPageIndex=0;
					gvITF.DataSource = dsResult.Tables[5];
					gvITF.DataBind();

					gvCheqRequisitionA.DataSource = null;
					gvCheqRequisitionA.DataBind();

					gvCheqRequisitionN.DataSource = null;
					gvCheqRequisitionN.DataBind();

					gvCheqRequisitionFormal.DataSource = null;
					gvCheqRequisitionFormal.DataBind();

					btnSearchJobs.Enabled=false;
					//lblPagingInfo.Text="Page "+ (gvCheqRequisitionA.CurrentPageIndex+1).ToString() + " of " +gvCheqRequisitionA.PageCount.ToString();
				}
				
			}
			catch (Exception ex)
			{
				lblError.Text= ex.Message;
			}
		    
		}


		private void ExecQry()
		{
			DataTable dtParams = CustomsCheckRequisitionDAL.CustomsChequeRequisition();
			System.Data.DataRow drNew = dtParams.NewRow();
			drNew["action"] ="0";
			drNew["enterpriseid"] =strEnterpriseID;
			drNew["userloggedin"] =strUserLoggin;
			drNew["requisition_no"] = txtRequisitionNumber.Text; 
			drNew["Cheque_No"] = txtChequeNumber.Text;
			drNew["Receipt_No"] = txtReceiptNumber.Text;

			dtParams.Rows.Add(drNew);
 
			dsAllResult = CustomsCheckRequisitionDAL.Customs_ChequeRequisition(strAppID, strEnterpriseID, strUserLoggin, dtParams );
			System.Data.DataTable dtStatus = dsAllResult.Tables[0];
			System.Data.DataTable dtCheck= dsAllResult.Tables[1];

			if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
			{
				if(dtCheck.Rows.Count<=0)
				{
					lblError.Text="Requisition number not found";
					return;
				}
				if (ddlRequisitionType.SelectedValue == "F")
				{					
					gvCheqRequisitionFormal.DataSource = dsAllResult.Tables[4];
					gvCheqRequisitionFormal.DataBind();

					gvCheqRequisitionN.DataSource = null;
					gvCheqRequisitionN.DataBind();

					gvCheqRequisitionA.DataSource = null;
					gvCheqRequisitionA.DataBind();

					btnSearchJobs.Enabled=false;
					//lblPagingInfo.Text="Page "+ (gvCheqRequisitionFormal.CurrentPageIndex+1).ToString() + " of " +gvCheqRequisitionFormal.PageCount.ToString();
				}
				else if (ddlRequisitionType.SelectedValue == "N")
				{
					gvCheqRequisitionN.DataSource = dsAllResult.Tables[2];
					gvCheqRequisitionN.DataBind();

					gvCheqRequisitionA.DataSource = null;
					gvCheqRequisitionA.DataBind();

					gvCheqRequisitionFormal.DataSource = null;
					gvCheqRequisitionFormal.DataBind();

					btnSearchJobs.Enabled=true;
					//lblPagingInfo.Text="Page "+ (gvCheqRequisitionN.CurrentPageIndex+1).ToString() + " of " +gvCheqRequisitionN.PageCount.ToString();
				}
				else if (ddlRequisitionType.SelectedValue == "A")
				{
					gvCheqRequisitionA.DataSource = dsAllResult.Tables[3];
					gvCheqRequisitionA.DataBind();

					gvCheqRequisitionN.DataSource = null;
					gvCheqRequisitionN.DataBind();

					gvCheqRequisitionFormal.DataSource = null;
					gvCheqRequisitionFormal.DataBind();

					btnSearchJobs.Enabled=true;
					//lblPagingInfo.Text="Page "+ (gvCheqRequisitionA.CurrentPageIndex+1).ToString() + " of " +gvCheqRequisitionA.PageCount.ToString();
				}
				else if (ddlRequisitionType.SelectedValue == "I")
				{
					gvITF.DataSource = dsAllResult.Tables[5];
					gvITF.DataBind();

					gvCheqRequisitionA.DataSource = null;
					gvCheqRequisitionA.DataBind();

					gvCheqRequisitionN.DataSource = null;
					gvCheqRequisitionN.DataBind();

					gvCheqRequisitionFormal.DataSource = null;
					gvCheqRequisitionFormal.DataBind();

					btnSearchJobs.Enabled=false;
					//lblPagingInfo.Text="Page "+ (gvCheqRequisitionA.CurrentPageIndex+1).ToString() + " of " +gvCheqRequisitionA.PageCount.ToString();
				}
			}
			else
			{
				lblError.Text=(dtStatus.Rows.Count>0?dtStatus.Rows[0]["ErrorMessage"].ToString():"");
			}
		}

		private void CheckRequisition(DataTable dtCheck)
		{
			if(dtCheck.Rows.Count<=0)
			{
				return;
			}
			DataRow dr = dtCheck.Rows[0];
			txtStatus.Text=dr["ChequeReq_Status"].ToString();
			txtPayee.Text=dr["payee"].ToString();  
		}


		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("SetScrollPosition();}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus"+DateTime.Now.ToString("ddMMyyyyHHmmss"), s.ToString()); 
			} 
		}


		#endregion Method
 
		private void gvCheqRequisitionN_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			lblGridError.Text="";
			GridCmd=e.CommandName;
			string cmd = e.CommandName; 
			if(cmd=="ADD_ITEM")
			{ 
				try
				{
					DataTable dtInvoice = dsAllResult.Tables[2]; 
					msTextBox txtAddInvoice_No =  (msTextBox)e.Item.FindControl("txtAddInvoice_No");  
					if(txtAddInvoice_No.Text.Trim() == "" )
					{			
						lblGridError.Text="Invoice is required on update and insert";
						SetInitialFocus(txtAddInvoice_No);
						return; 
					}  
					DataTable dtParams = CustomsCheckRequisitionDAL.CustomsChequeRequisition();
					System.Data.DataRow drNew = dtParams.NewRow();
					drNew["action"] ="0";
					drNew["enterpriseid"] =strEnterpriseID;
					drNew["userloggedin"] =strUserLoggin;
					//drNew["consignment_no"] = txt.Text;
					drNew["requisition_no"] = txtRequisitionNumber.Text;
					drNew["payee"] =txtPayee.Text;				
					drNew["Invoice_No"] = txtAddInvoice_No.Text;
					dtParams.Rows.Add(drNew);

					DataSet ds = CustomsCheckRequisitionDAL.Customs_ChequeRequisitionDetails(strAppID, strEnterpriseID, strUserLoggin, dtParams );

					if(ds !=null && ds.Tables[0].Rows.Count > 0)
					{
						if(Convert.ToInt32(ds.Tables[0].Rows[0]["ErrorCode"]) > 0)
						{
							lblGridError.Text = ds.Tables[0].Rows[0]["ErrorMessage"].ToString();
						}
						else
						{
							GridCmd="ADD_ITEM";
							ExecQryData();
							lblGridError.Text="";
						}
					}
				}
				catch (Exception ex)
				{
					lblGridError.Text = ex.Message;
				}								 
			}
			else if(cmd=="DELETE_ITEM")
			{
				Label lblRequisition_no =  (Label)e.Item.FindControl("lblRequisition_no");  
				Label lblConsignment_no =  (Label)e.Item.FindControl("lblConsignment_no");  
				Label lblPayerid =  (Label)e.Item.FindControl("lblPayerid");  
				Label lblInvoice_No =  (Label)e.Item.FindControl("lblInvoice_No");  
				DataTable dtParams = CustomsCheckRequisitionDAL.CustomsChequeRequisition();
				System.Data.DataRow drNew = dtParams.NewRow();
				drNew["action"] ="2";
				drNew["enterpriseid"] =strEnterpriseID;
				drNew["userloggedin"] =strUserLoggin;
				drNew["requisition_no"] = lblRequisition_no.Text;
				drNew["consignment_no"] = lblConsignment_no.Text;				
				drNew["payerid"] =lblPayerid.Text;				
				drNew["Invoice_No"] =lblInvoice_No.Text;
				dtParams.Rows.Add(drNew);

				DataSet ds = CustomsCheckRequisitionDAL.Customs_ChequeRequisitionDetails(strAppID, strEnterpriseID, strUserLoggin, dtParams );
				if(ds !=null && ds.Tables[0].Rows.Count > 0)
				{
					if(Convert.ToInt32( ds.Tables[0].Rows[0]["ErrorCode"]) > 0)
					{
						lblGridError.Text = ds.Tables[0].Rows[0]["ErrorMessage"].ToString();
					}
					else
					{
						ExecQryData();
						lblGridError.Text = "";
					}
				}  
			}		
		}


		private void gvCheqRequisitionA_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			lblGridError.Text="";
			string cmd = e.CommandName; 
			GridCmd=e.CommandName;
			if(cmd=="ADD_ITEM")
			{ 
				try
				{
					DataTable dtInvoice = dsAllResult.Tables[2]; 
					msTextBox txtAddInvoice_NoA =  (msTextBox)e.Item.FindControl("txtAddInvoice_NoA");
					TextBox txtAddDisbursementTypeA =  (TextBox)e.Item.FindControl("txtAddDisbursementTypeA");
					com.common.util.msTextBox txtAddAmountA =  (com.common.util.msTextBox)e.Item.FindControl("txtAddAmountA");
					if(txtAddInvoice_NoA.Text.Trim() == "" )
					{			
						lblGridError.Text="Invoice is required on update and insert";
						SetInitialFocus(txtAddInvoice_NoA);
						return; 
					}  
					if(txtAddDisbursementTypeA.Text.Trim() == "" )
					{			
						lblGridError.Text="Not saved - Disbursement Type is required for an AD-HOC Cheque Requisition";
						SetInitialFocus(txtAddDisbursementTypeA);
						return; 
					}  
					DataTable dtParams = CustomsCheckRequisitionDAL.CustomsChequeRequisition();
					System.Data.DataRow drNew = dtParams.NewRow();
					drNew["action"] ="0";
					drNew["enterpriseid"] =strEnterpriseID;
					drNew["userloggedin"] =strUserLoggin;
					//drNew["consignment_no"] = txt.Text;
					drNew["requisition_no"] = txtRequisitionNumber.Text;
					drNew["payee"] =txtPayee.Text;				
					drNew["Invoice_No"] = txtAddInvoice_NoA.Text;
					drNew["DisbursementType"] = txtAddDisbursementTypeA.Text;
					drNew["OtherDisbursement"] = txtAddAmountA.Text;
					dtParams.Rows.Add(drNew);

					DataSet ds = CustomsCheckRequisitionDAL.Customs_ChequeRequisitionDetails(strAppID, strEnterpriseID, strUserLoggin, dtParams );

					if(ds !=null && ds.Tables[0].Rows.Count > 0)
					{
						if(Convert.ToInt32( ds.Tables[0].Rows[0]["ErrorCode"]) > 0)
						{
							lblGridError.Text = ds.Tables[0].Rows[0]["ErrorMessage"].ToString();
						}
						else
						{
							ExecQryData();
							lblGridError.Text="";
						}
					}
				}
				catch (Exception ex)
				{
					lblGridError.Text = ex.Message;
				}								 
			}
			else if(cmd=="DELETE_ITEM")
			{
				Label lblRequisition_noA =  (Label)e.Item.FindControl("lblRequisition_noA");  
				Label lblConsignment_noA =  (Label)e.Item.FindControl("lblConsignment_noA");  
				Label lblPayeridA =  (Label)e.Item.FindControl("lblPayeridA");  
				Label lblInvoice_NoA =  (Label)e.Item.FindControl("lblInvoice_NoA");  
  
				DataTable dtParams = CustomsCheckRequisitionDAL.CustomsChequeRequisition();
				System.Data.DataRow drNew = dtParams.NewRow();
				drNew["action"] ="2";
				drNew["enterpriseid"] =strEnterpriseID;
				drNew["userloggedin"] =strUserLoggin;
				drNew["requisition_no"] = lblRequisition_noA.Text;
				drNew["consignment_no"] = lblConsignment_noA.Text;				
				drNew["payerid"] =lblPayeridA.Text;				
				drNew["Invoice_No"] =lblInvoice_NoA.Text;
				dtParams.Rows.Add(drNew);

				DataSet ds = CustomsCheckRequisitionDAL.Customs_ChequeRequisitionDetails(strAppID, strEnterpriseID, strUserLoggin, dtParams );
				if(ds !=null && ds.Tables[0].Rows.Count > 0)
				{
					if(Convert.ToInt32( ds.Tables[0].Rows[0]["ErrorCode"]) > 0)
					{
						lblGridError.Text = ds.Tables[0].Rows[0]["ErrorMessage"].ToString();
					}
					else
					{
						ExecQryData();
						lblGridError.Text = "";
					}
				}  
			}
			else if(cmd=="EDIT_ITEM")
			{
				btnSearchJobs.Enabled=false;
				gvCheqRequisitionA.EditItemIndex=e.Item.ItemIndex; 
				gvCheqRequisitionA.DataSource = dsAllResult.Tables[3];
				gvCheqRequisitionA.DataBind(); 
			}
			else if(cmd=="CANCEL_ITEM")
			{
				btnSearchJobs.Enabled=txtChequeNumber.Enabled;
				gvCheqRequisitionA.EditItemIndex = -1;
				gvCheqRequisitionA.DataSource = dsAllResult.Tables[3];
				gvCheqRequisitionA.DataBind(); 
			} 
			else if(cmd=="SAVE_ITEM")
			{ 
				try
				{
					btnSearchJobs.Enabled=txtChequeNumber.Enabled;
					Label lblConsignment_noEditA =  (Label)e.Item.FindControl("lblConsignment_noEditA");  
					Label lbllblPayeridEditA =  (Label)e.Item.FindControl("lbllblPayeridEditA");  
					Label lblRequisition_noEditA =  (Label)e.Item.FindControl("lblRequisition_noEditA");  

					DataTable dtInvoice = dsAllResult.Tables[2]; 
					msTextBox txtEditInvoice_NoA =  (msTextBox)e.Item.FindControl("txtEditInvoice_NoA");
					TextBox txtEditDisbursementTypeA =  (TextBox)e.Item.FindControl("txtEditDisbursementTypeA");
					com.common.util.msTextBox txtEditAmountA =  (com.common.util.msTextBox)e.Item.FindControl("txtEditAmountA");
					if(txtEditInvoice_NoA.Text.Trim() == "" )
					{			
						lblGridError.Text="Invoice is required on update and insert";
						SetInitialFocus(txtEditInvoice_NoA);
						return; 
					}  
					if(txtEditDisbursementTypeA.Text.Trim() == "" )
					{			
						lblGridError.Text="Not saved - Disbursement Type is required for an AD-HOC Cheque Requisition";
						SetInitialFocus(txtEditDisbursementTypeA);
						return; 
					}  
					DataTable dtParams = CustomsCheckRequisitionDAL.CustomsChequeRequisition();
					System.Data.DataRow drNew = dtParams.NewRow();
					drNew["action"] ="1";
					drNew["enterpriseid"] =strEnterpriseID;
					drNew["userloggedin"] =strUserLoggin;
					drNew["consignment_no"] = lblConsignment_noEditA.Text;
					drNew["requisition_no"] = lblRequisition_noEditA.Text;
					drNew["payerid"] =lbllblPayeridEditA.Text;				
					drNew["Invoice_No"] = txtEditInvoice_NoA.Text;
					drNew["DisbursementType"] = txtEditDisbursementTypeA.Text;
					drNew["OtherDisbursement"] = txtEditAmountA.Text;
					dtParams.Rows.Add(drNew);

					DataSet ds = CustomsCheckRequisitionDAL.Customs_ChequeRequisitionDetails(strAppID, strEnterpriseID, strUserLoggin, dtParams );

					if(ds !=null && ds.Tables[0].Rows.Count > 0)
					{
						if(Convert.ToInt32( ds.Tables[0].Rows[0]["ErrorCode"]) > 0)
						{
							lblGridError.Text = ds.Tables[0].Rows[0]["ErrorMessage"].ToString();
						}
						else
						{
							gvCheqRequisitionA.EditItemIndex = -1;
							ExecQryData();
							lblGridError.Text="";
						}
					}
				}
				catch (Exception ex)
				{
					lblGridError.Text = ex.Message;
				}								 
			}
	
		}


		private void gvCheqRequisitionA_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				DataRowView dr = (DataRowView)e.Item.DataItem;
				if(dr["OtherDisbursement"] != DBNull.Value && dr["OtherDisbursement"].ToString() != "")
				{
					Label lblOtherDisbursementA = (Label)e.Item.FindControl("lblOtherDisbursementA");
					lblOtherDisbursementA.Text = Convert.ToDecimal(dr["OtherDisbursement"]).ToString(currency_decimal); 
				}
				if(!txtChequeNumber.Text.Trim().Equals("") || !txtReceiptNumber.Text.Trim().Equals(""))
				{
					LinkButton btnEditItemA = (LinkButton)e.Item.FindControl("btnEditItemA");
					LinkButton btnDeleteItemA = (LinkButton)e.Item.FindControl("btnDeleteItemA");
					btnEditItemA.Visible = false;
					btnDeleteItemA.Visible = false;
				}
			}

			if((e.Item.ItemType == ListItemType.EditItem))
			{
				DataRowView dr = (DataRowView)e.Item.DataItem;
				com.common.util.msTextBox txtEditAmountA = (com.common.util.msTextBox)e.Item.FindControl("txtEditAmountA");
				if(txtEditAmountA != null)
				{
					if(dr["OtherDisbursement"] != DBNull.Value && dr["OtherDisbursement"].ToString() != "")
					{
						txtEditAmountA.Text= Convert.ToDecimal(dr["OtherDisbursement"]).ToString(currency_decimal); 
					}				
					txtEditAmountA.NumberScale=NumberScale;
				}
				TextBox txtEditDisbursementTypeA = (TextBox)e.Item.FindControl("txtEditDisbursementTypeA");
				SetInitialFocus(txtEditDisbursementTypeA);
			}

			if((e.Item.ItemType == ListItemType.Footer))
			{
				LinkButton btnAddItem = (LinkButton)e.Item.FindControl("btnAddItemA");
				if(txtRequisitionNumber.Text.Trim() !="")
				{
					msTextBox txtAddAmountA = (msTextBox)e.Item.FindControl("txtAddAmountA");
					if(txtAddAmountA != null)
					{
						txtAddAmountA.NumberScale=NumberScale;
					}

					if (dsAllResult.Tables[0].Rows[0]["IsApproved"].ToString() == "1")
					{
						btnAddItem.Visible = false;
					}
					else
					{
						btnAddItem.Visible = true;
					}
				}
				else
				{
					btnAddItem.Visible=false;
				}
				if(GridCmd =="ADD_ITEM")
				{
					GridCmd=null;
					msTextBox txtAddInvoice_NoA =  (msTextBox)e.Item.FindControl("txtAddInvoice_NoA");
					SetInitialFocus(txtAddInvoice_NoA);					
				}
			}
		}


		private void gvCheqRequisitionN_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				DataRowView dr = (DataRowView)e.Item.DataItem;

				if(dr["DutyTax"] != DBNull.Value && dr["DutyTax"].ToString() != "")
				{
					Label lblDutyTax = (Label)e.Item.FindControl("lblDutyTax");
					lblDutyTax.Text = Convert.ToDecimal(dr["DutyTax"]).ToString(currency_decimal); 
				}
				if(dr["EntryProcessingFee"] != DBNull.Value && dr["EntryProcessingFee"].ToString() != "")
				{
					Label lblEntryProcessingFee = (Label)e.Item.FindControl("lblEntryProcessingFee");
					lblEntryProcessingFee.Text = Convert.ToDecimal(dr["EntryProcessingFee"]).ToString(currency_decimal); 
				}
				if(dr["DisbursementTotal"] != DBNull.Value && dr["DisbursementTotal"].ToString() != "")
				{
					Label lblDisbursementTotal = (Label)e.Item.FindControl("lblDisbursementTotal");
					lblDisbursementTotal.Text = Convert.ToDecimal(dr["DisbursementTotal"]).ToString(currency_decimal); 
				}
				if(!txtChequeNumber.Text.Trim().Equals("") || !txtReceiptNumber.Text.Trim().Equals(""))
				{
					LinkButton btnDeleteItem = (LinkButton)e.Item.FindControl("btnDeleteItem");
					btnDeleteItem.Visible = false;
				}
			}
			if((e.Item.ItemType == ListItemType.Footer))
			{
				LinkButton btnAddItem = (LinkButton)e.Item.FindControl("btnAddItem");
				if(txtRequisitionNumber.Text.Trim() !="")
				{
					if (dsAllResult.Tables[0].Rows[0]["IsApproved"].ToString() == "1")
					{
						btnAddItem.Visible = false;
					}
					else
					{
						btnAddItem.Visible = true;
					}
				}
				else
				{
					btnAddItem.Visible=false;
				}
				if(GridCmd =="ADD_ITEM")
				{
					GridCmd=null;
					msTextBox txtAddInvoice_No =  (msTextBox)e.Item.FindControl("txtAddInvoice_No");
					SetInitialFocus(txtAddInvoice_No);
				}
			}

		}


		public string GetCurrencyDigitsOfEnterpriseConfiguration()
		{
			DataSet dsEprise = SysDataManager1.GetEnterpriseProfile(utility.GetAppID(),utility.GetEnterpriseID());
			DataRow dr = dsEprise.Tables[0].Rows[0];
			
			string tempplate = "00";
			if(dr["currency_decimal"].ToString()!="")
			{
				tempplate="";
				int number_digit = Convert.ToInt32(dr["currency_decimal"].ToString());
				NumberScale=number_digit;
				for (int i = 0 ; i< number_digit ; i++)
				{
					tempplate = tempplate+"0";
				}
			}
			return "#,##0." + tempplate;
		}

		private void btnSearchJobs_Click(object sender, System.EventArgs e)
		{
			Session["RetrieveInvoicesToChequeReq"]=null;
			lblGridError.Text="";
			String strUrl = null;
			strUrl = "CustomsJobStatus.aspx?pagemode=CustomsCheckRequisition&requisitionnumber="+txtRequisitionNumber.Text.Trim();
			strUrl+="&appID="+strAppID+"&enterpriseID="+strEnterpriseID+"&userID="+strUserLoggin+"&Payee="+txtPayee.Text.Trim();
			strUrl+="&RequisitionType="+ddlRequisitionType.SelectedValue;
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindowCustomSize.js",paramList,800,1100);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnClientEvent_Click(object sender, System.EventArgs e)
		{
			ExecQryData();
			lblGridError.Text="";
			if(Session["RetrieveInvoicesToChequeReq"] != null)
			{
				lblGridError.Text=Session["RetrieveInvoicesToChequeReq"].ToString();
			}
		}

		private void gvCheqRequisitionFormal_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				DataRowView dr = (DataRowView)e.Item.DataItem;
				if(dr["AssessmentNumber"] != DBNull.Value && dr["AssessmentNumber"].ToString() != "")
				{
					Label lblAssessmentNumberFormal = (Label)e.Item.FindControl("lblAssessmentNumberFormal");
					lblAssessmentNumberFormal.Text = dr["AssessmentNumber"].ToString(); 
				}
				if(dr["AssessmentAmount"] != DBNull.Value && dr["AssessmentAmount"].ToString() != "")
				{
					Label lblAssessmentAmountFormal = (Label)e.Item.FindControl("lblAssessmentAmountFormal");
					lblAssessmentAmountFormal.Text = Convert.ToDecimal(dr["AssessmentAmount"]).ToString(currency_decimal); 
				}
				if(dr["AssessmentDate"] != DBNull.Value && dr["AssessmentDate"].ToString() != "")
				{
					Label lblAssessmentDateFormal = (Label)e.Item.FindControl("lblAssessmentDateFormal");
					lblAssessmentDateFormal.Text = (dr["AssessmentDate"] != DBNull.Value ? DateTime.Parse(dr["AssessmentDate"].ToString()).ToString("dd/MM/yyyy") :"");
				}
				if(dr["EntryProcessingFee"] != DBNull.Value && dr["EntryProcessingFee"].ToString() != "")
				{
					Label lblEntryProcessingFeeFormal = (Label)e.Item.FindControl("lblEntryProcessingFeeFormal");
					lblEntryProcessingFeeFormal.Text = Convert.ToDecimal(dr["EntryProcessingFee"]).ToString(currency_decimal); 
				}
				if(dr["DisbursementTotal"] != DBNull.Value && dr["DisbursementTotal"].ToString() != "")
				{
					Label lblDisbursementTotalFormal = (Label)e.Item.FindControl("lblDisbursementTotalFormal");
					lblDisbursementTotalFormal.Text = Convert.ToDecimal(dr["DisbursementTotal"]).ToString(currency_decimal); 
				}
				if(!txtChequeNumber.Text.Trim().Equals("") || !txtReceiptNumber.Text.Trim().Equals(""))
				{
					LinkButton btnDeleteItemFormal = (LinkButton)e.Item.FindControl("btnDeleteItemFormal");
					btnDeleteItemFormal.Visible = false;
				}
			}
			if((e.Item.ItemType == ListItemType.EditItem))
			{
				DataRowView dr = (DataRowView)e.Item.DataItem;
				TextBox txtAssessmentNumberEdit =  (TextBox)e.Item.FindControl("txtAssessmentNumberEdit");
				msTextBox txtAssessmentAmountEdit =  (msTextBox)e.Item.FindControl("txtAssessmentAmountEdit");
				msTextBox txtAssessmentDateEdit =  (msTextBox)e.Item.FindControl("txtAssessmentDateEdit");
				txtAssessmentNumberEdit.Text=dr["AssessmentNumber"].ToString();
				if(txtAssessmentAmountEdit != null)
				{
					if(dr["AssessmentAmount"] != DBNull.Value && dr["AssessmentAmount"].ToString() != "")
					{
						txtAssessmentAmountEdit.Text= Convert.ToDecimal(dr["AssessmentAmount"]).ToString(currency_decimal); 
					}				
					txtAssessmentAmountEdit.NumberScale=NumberScale;
				}
				txtAssessmentDateEdit.Text = (dr["AssessmentDate"] != DBNull.Value ? DateTime.Parse(dr["AssessmentDate"].ToString()).ToString("dd/MM/yyyy") :"");
				SetInitialFocus(txtAssessmentNumberEdit);
			}
			if((e.Item.ItemType == ListItemType.Footer))
			{
				LinkButton btnAddItemFormal = (LinkButton)e.Item.FindControl("btnAddItemFormal");
				msTextBox txtAssessmentDateAdd =  (msTextBox)e.Item.FindControl("txtAssessmentDateAdd");
				txtAssessmentDateAdd.Text=DateTime.Today.ToString("dd/MM/yyyy");
				if(txtRequisitionNumber.Text.Trim() !="")
				{
					msTextBox txtAssessmentAmountAdd = (msTextBox)e.Item.FindControl("txtAssessmentAmountAdd");
					if(txtAssessmentAmountAdd != null)
					{
						txtAssessmentAmountAdd.NumberScale=NumberScale;
					}

					if (dsAllResult.Tables[0].Rows[0]["IsApproved"].ToString() == "1")
					{
						btnAddItemFormal.Visible = false;
					}
					else
					{
						btnAddItemFormal.Visible = true;
					}
				}
				else
				{
					btnAddItemFormal.Visible=false;
				}
				if(GridCmd =="ADD_ITEM")
				{
					GridCmd=null;
					msTextBox txtAddInvoice_NoFormal =  (msTextBox)e.Item.FindControl("txtAddInvoice_NoFormal");
					SetInitialFocus(txtAddInvoice_NoFormal);
				}
			}		
		}

		private void gvCheqRequisitionFormal_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			lblGridError.Text="";
			string cmd = e.CommandName; 
			GridCmd=e.CommandName;
			if(cmd=="ADD_ITEM")
			{ 
				try
				{
					msTextBox txtAddInvoice_NoFormal =  (msTextBox)e.Item.FindControl("txtAddInvoice_NoFormal");
					TextBox txtAssessmentNumberAdd =  (TextBox)e.Item.FindControl("txtAssessmentNumberAdd");
					msTextBox txtAssessmentAmountAdd =  (msTextBox)e.Item.FindControl("txtAssessmentAmountAdd");
					msTextBox txtAssessmentDateAdd =  (msTextBox)e.Item.FindControl("txtAssessmentDateAdd");
					if(txtAddInvoice_NoFormal.Text.Trim() == "" )
					{			
						lblGridError.Text="Invoice is required on update and insert";
						SetInitialFocus(txtAddInvoice_NoFormal);
						return; 
					}  

					if(txtAssessmentAmountAdd.Text.Trim() == "" )
					{			
						lblGridError.Text="Not saved - Assessment amount must be >= 0 for a FORMAL Cheque Requisition";
						SetInitialFocus(txtAssessmentAmountAdd);
						return; 
					}  
					if(txtAssessmentDateAdd.Text.Trim() == "" )
					{			
						lblGridError.Text="Not saved - Assessment date must be specified for a FORMAL Cheque Requisition";
						SetInitialFocus(txtAssessmentDateAdd);
						return; 
					}  
					DataTable dtParams = CustomsCheckRequisitionDAL.CustomsChequeRequisition();
					System.Data.DataRow drNew = dtParams.NewRow();
					drNew["action"] ="0";
					drNew["enterpriseid"] =strEnterpriseID;
					drNew["userloggedin"] =strUserLoggin;
					//drNew["consignment_no"] = txt.Text;
					drNew["requisition_no"] = txtRequisitionNumber.Text;
					drNew["payee"] =txtPayee.Text;				
					drNew["Invoice_No"] = txtAddInvoice_NoFormal.Text.ToUpper();
					drNew["AssessmentNumber"] = (txtAssessmentNumberAdd.Text.Trim() == "" ? null:txtAssessmentNumberAdd.Text.Trim());
					drNew["AssessmentAmount"] = txtAssessmentAmountAdd.Text;
					drNew["AssessmentDate"] = DateTime.ParseExact(txtAssessmentDateAdd.Text,"dd/MM/yyyy",null).ToString("yyyy-MM-dd");
					dtParams.Rows.Add(drNew);

					DataSet ds = CustomsCheckRequisitionDAL.Customs_ChequeRequisitionDetails(strAppID, strEnterpriseID, strUserLoggin, dtParams );

					if(ds !=null && ds.Tables[0].Rows.Count > 0)
					{
						if(Convert.ToInt32( ds.Tables[0].Rows[0]["ErrorCode"]) > 0)
						{
							lblGridError.Text = ds.Tables[0].Rows[0]["ErrorMessage"].ToString();
						}
						else
						{
							GridCmd="ADD_ITEM";
							ExecQryData();
							lblGridError.Text="";
						}
					}
				}
				catch (Exception ex)
				{
					lblGridError.Text = ex.Message;
				}								 
				btnSearchJobs.Enabled=false;
			}
			else if(cmd=="DELETE_ITEM")
			{
				Label lblRequisition_noA =  (Label)e.Item.FindControl("lblRequisition_noFormal");  
				Label lblConsignment_noA =  (Label)e.Item.FindControl("lblConsignment_noFormal");  
				Label lblPayeridA =  (Label)e.Item.FindControl("lblPayeridFormal");  
				Label lblInvoice_NoA =  (Label)e.Item.FindControl("lblInvoice_NoFormal");  
  
				DataTable dtParams = CustomsCheckRequisitionDAL.CustomsChequeRequisition();
				System.Data.DataRow drNew = dtParams.NewRow();
				drNew["action"] ="2";
				drNew["enterpriseid"] =strEnterpriseID;
				drNew["userloggedin"] =strUserLoggin;
				drNew["requisition_no"] = lblRequisition_noA.Text;
				drNew["consignment_no"] = lblConsignment_noA.Text;				
				drNew["payerid"] =lblPayeridA.Text;				
				drNew["Invoice_No"] =lblInvoice_NoA.Text.ToUpper();
				dtParams.Rows.Add(drNew);

				DataSet ds = CustomsCheckRequisitionDAL.Customs_ChequeRequisitionDetails(strAppID, strEnterpriseID, strUserLoggin, dtParams );
				if(ds !=null && ds.Tables[0].Rows.Count > 0)
				{
					if(Convert.ToInt32( ds.Tables[0].Rows[0]["ErrorCode"]) > 0)
					{
						lblGridError.Text = ds.Tables[0].Rows[0]["ErrorMessage"].ToString();
					}
					else
					{
						ExecQryData();
						lblGridError.Text = "";
					}
				} 
				btnSearchJobs.Enabled=false;
			}
			
		}

		private void gvCheqRequisitionN_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			gvCheqRequisitionN.CurrentPageIndex = e.NewPageIndex;
			ExecQry();
		}

		private void gvCheqRequisitionA_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			gvCheqRequisitionA.CurrentPageIndex = e.NewPageIndex;
			ExecQry();
		}

		private void gvCheqRequisitionFormal_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			gvCheqRequisitionFormal.CurrentPageIndex = e.NewPageIndex;
			ExecQry();
		}

		private void gvITF_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			lblGridError.Text="";
			string cmd = e.CommandName; 
			GridCmd=e.CommandName;

			if(cmd=="ADD_ITEM")
			{
				#region ADD_ITEM
				try
				{
					TextBox txtAddMasterAWBNumberITF =  (TextBox)e.Item.FindControl("txtAddMasterAWBNumberITF");
					CheckBox cbAddIgnoreMAWBValidation = (CheckBox)e.Item.FindControl("cbAddIgnoreMAWBValidation");
					msTextBox txtAddITFAmount =  (msTextBox)e.Item.FindControl("txtAddITFAmount");
					msTextBox txtAddFreightColloct =  (msTextBox)e.Item.FindControl("txtAddFreightColloct");
					if(txtAddMasterAWBNumberITF.Text.Trim() == "" )
					{			
						lblGridError.Text = "Not saved - Master AWB number is required for an ITF cheque requisition";
						SetInitialFocus(txtAddMasterAWBNumberITF);
						return; 
					}
					if(txtAddITFAmount.Text.Trim() == "" && txtAddFreightColloct.Text.Trim() == "")
					{			
						lblGridError.Text = "Not saved - ITF or Freight Collect amount is required for an ITF Cheque Requisition";
						return; 
					}

					DataTable dtParams = CustomsCheckRequisitionDAL.CustomsChequeRequisitionDetailsRequest();
					System.Data.DataRow drNew = dtParams.NewRow();
					drNew["action"] ="0";
					drNew["enterpriseid"] =strEnterpriseID;
					drNew["userloggedin"] =strUserLoggin;
					drNew["requisition_no"] = txtRequisitionNumber.Text;			
					drNew["consignment_no"] = txtAddMasterAWBNumberITF.Text;
					drNew["ITFAmount"] = txtAddITFAmount.Text;
					drNew["FreightCollectAmount"] = txtAddFreightColloct.Text;
					drNew["IgnoreMAWBValidation"] = (cbAddIgnoreMAWBValidation.Checked ? "1" : "0");
					dtParams.Rows.Add(drNew);

					DataSet dsResult = CustomsCheckRequisitionDAL.ITFCustomsChequeRequisitionDetails(strAppID, strEnterpriseID, strUserLoggin, dtParams );

					if(dsResult !=null && dsResult.Tables[0].Rows.Count > 0)
					{
						if(Convert.ToInt32( dsResult.Tables[0].Rows[0]["ErrorCode"]) > 0)
						{
							lblGridError.Text = dsResult.Tables[0].Rows[0]["ErrorMessage"].ToString();
						}
						else
						{
							ExecQryData();
							lblGridError.Text="";
						}
					}
				}
				catch(Exception ex)
				{
					lblGridError.Text = ex.Message;
				}
				#endregion
			}
			else if(cmd=="DELETE_ITEM")
			{
				#region DELETE_ITEM
				try
				{
					Label lblMasterAWBNumberITF =  (Label)e.Item.FindControl("lblMasterAWBNumberITF");
					
					DataTable dtParams = CustomsCheckRequisitionDAL.CustomsChequeRequisitionDetailsRequest();
					System.Data.DataRow drNew = dtParams.NewRow();
					drNew["action"] ="2";
					drNew["enterpriseid"] =strEnterpriseID;
					drNew["userloggedin"] =strUserLoggin;
					drNew["requisition_no"] = txtRequisitionNumber.Text;			
					drNew["consignment_no"] = lblMasterAWBNumberITF.Text;
					dtParams.Rows.Add(drNew);

					DataSet dsResult = CustomsCheckRequisitionDAL.ITFCustomsChequeRequisitionDetails(strAppID, strEnterpriseID, strUserLoggin, dtParams );

					if(dsResult !=null && dsResult.Tables[0].Rows.Count > 0)
					{
						if(Convert.ToInt32( dsResult.Tables[0].Rows[0]["ErrorCode"]) > 0)
						{
							lblGridError.Text = dsResult.Tables[0].Rows[0]["ErrorMessage"].ToString();
						}
						else
						{
							ExecQryData();
							lblGridError.Text="";
						}
					}
				}
				catch(Exception ex)
				{
					lblGridError.Text = ex.Message;
				}
				#endregion
			}
			else if(cmd=="EDIT_ITEM")
			{
				#region EDIT_ITEM
				btnSearchJobs.Enabled=false;
				gvITF.EditItemIndex=e.Item.ItemIndex; 
				gvITF.DataSource = dsAllResult.Tables[5];
				gvITF.DataBind(); 
				#endregion
			}
			else if(cmd=="CANCEL_ITEM")
			{
				#region CANCEL_ITEM
				btnSearchJobs.Enabled=txtChequeNumber.Enabled;
				gvITF.EditItemIndex = -1;
				gvITF.DataSource = dsAllResult.Tables[5];
				gvITF.DataBind(); 
				#endregion
			} 
			else if(cmd=="SAVE_ITEM")
			{ 
				#region SAVE_ITEM
				try
				{
					TextBox txtEditMasterAWBNumberITF =  (TextBox)e.Item.FindControl("txtEditMasterAWBNumberITF");
					msTextBox txtEditITFAmount =  (msTextBox)e.Item.FindControl("txtEditITFAmount");
					msTextBox txtEditFreightColloct =  (msTextBox)e.Item.FindControl("txtEditFreightColloct");

					if(txtEditMasterAWBNumberITF.Text.Trim() == "" )
					{			
						lblGridError.Text = "Not saved - Master AWB number is required for an ITF cheque requisition";
						SetInitialFocus(txtEditMasterAWBNumberITF);
						return; 
					}
					if(txtEditITFAmount.Text.Trim() == "" && txtEditFreightColloct.Text.Trim() == "")
					{			
						lblGridError.Text = "Not saved - ITF or Freight Collect amount is required for an ITF Cheque Requisition";
						return; 
					}

					DataTable dtParams = CustomsCheckRequisitionDAL.CustomsChequeRequisitionDetailsRequest();
					System.Data.DataRow drNew = dtParams.NewRow();
					drNew["action"] ="1";
					drNew["enterpriseid"] =strEnterpriseID;
					drNew["userloggedin"] =strUserLoggin;
					drNew["requisition_no"] = txtRequisitionNumber.Text;			
					drNew["consignment_no"] = txtEditMasterAWBNumberITF.Text;
					drNew["ITFAmount"] = txtEditITFAmount.Text;
					drNew["FreightCollectAmount"] = txtEditFreightColloct.Text;
					dtParams.Rows.Add(drNew);

					DataSet dsResult = CustomsCheckRequisitionDAL.ITFCustomsChequeRequisitionDetails(strAppID, strEnterpriseID, strUserLoggin, dtParams );

					if(dsResult !=null && dsResult.Tables[0].Rows.Count > 0)
					{
						if(Convert.ToInt32( dsResult.Tables[0].Rows[0]["ErrorCode"]) > 0)
						{
							lblGridError.Text = dsResult.Tables[0].Rows[0]["ErrorMessage"].ToString();
						}
						else
						{
							gvITF.EditItemIndex = -1;
							ExecQryData();
							lblGridError.Text="";
						}
					}
				}
				catch(Exception ex)
				{
					lblGridError.Text = ex.Message;
				}
				#endregion
			}
		}

		private void gvITF_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			gvITF.CurrentPageIndex = e.NewPageIndex;
			ExecQry();
		}

		private void gvITF_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				DataRowView dr = (DataRowView)e.Item.DataItem;
				CheckBox cbIgnoreMAWBValidation = (CheckBox)e.Item.FindControl("cbIgnoreMAWBValidation");
				
				if(cbIgnoreMAWBValidation != null)
					cbIgnoreMAWBValidation.Enabled = false;

				if(!txtChequeNumber.Text.Trim().Equals("") || !txtReceiptNumber.Text.Trim().Equals(""))
				{
					LinkButton btnEditItemITF = (LinkButton)e.Item.FindControl("btnEditItemITF");
					LinkButton btnDeleteItemITF = (LinkButton)e.Item.FindControl("btnDeleteItemITF");

					btnEditItemITF.Visible = false;
					btnDeleteItemITF.Visible = false;
				}
			}
			if((e.Item.ItemType == ListItemType.EditItem))
			{
				DataRowView dr = (DataRowView)e.Item.DataItem;

				TextBox txtEditMasterAWBNumberITF = (TextBox)e.Item.FindControl("txtEditMasterAWBNumberITF");
				com.common.util.msTextBox txtEditITFAmount = (com.common.util.msTextBox)e.Item.FindControl("txtEditITFAmount");
				com.common.util.msTextBox txtEditFreightColloct = (com.common.util.msTextBox)e.Item.FindControl("txtEditFreightColloct");
				if(txtEditITFAmount != null)			
					txtEditITFAmount.NumberScale = NumberScale;
				if(txtEditFreightColloct != null)			
					txtEditFreightColloct.NumberScale = NumberScale;
				txtEditMasterAWBNumberITF.Enabled = false;

				SetInitialFocus(txtEditITFAmount);
			}
			if((e.Item.ItemType == ListItemType.Footer))
			{
				LinkButton btnAddItemITF = (LinkButton)e.Item.FindControl("btnAddItemITF");
				if(txtRequisitionNumber.Text.Trim() !="")
				{
					msTextBox txtAddITFAmount = (msTextBox)e.Item.FindControl("txtAddITFAmount");
					msTextBox txtAddFreightColloct = (msTextBox)e.Item.FindControl("txtAddFreightColloct");
					if(txtAddITFAmount != null)
						txtAddITFAmount.NumberScale = NumberScale;
					if(txtAddFreightColloct != null)
						txtAddFreightColloct.NumberScale = NumberScale;

					if (dsAllResult.Tables[0].Rows[0]["IsApproved"].ToString() == "1")
						btnAddItemITF.Visible = false;
					else
						btnAddItemITF.Visible = true;
				}
				else
				{
					btnAddItemITF.Visible=false;
				}
				//if(GridCmd =="ADD_ITEM")
				//{
				//	GridCmd=null;
				//	msTextBox txtAddInvoice_NoFormal =  (msTextBox)e.Item.FindControl("txtAddInvoice_NoFormal");
				//	SetInitialFocus(txtAddInvoice_NoFormal);
				//}
			}
		}
	}
}
