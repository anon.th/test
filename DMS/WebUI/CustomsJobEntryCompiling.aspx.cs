using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using System.Text;
using System.Configuration;
using System.Data.OleDb;
using Cambro.Web.DbCombo;
using TIESDAL; 

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CustomsJobEntryCompiling.
	/// </summary>
	public class CustomsJobEntryCompiling : BasePage
	{
		String strEnterpriseID=null;
		String strAppID=null;
		String strUserLoggin=null; 

		public string GridCmd
		{
			get
			{
				if(ViewState["GridCmd"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["GridCmd"];
				}
			}
			set
			{
				ViewState["GridCmd"]=value;
			}
		}

		#region Code Gen
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnEntryLines;
		protected System.Web.UI.WebControls.Button btnPrintDeclaration;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.TextBox txt9999;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Label lblChargeNos;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected com.common.util.msTextBox txtAmount3;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label lblHeader;
		protected System.Web.UI.WebControls.DataGrid gvMarksAndNo;
		protected System.Web.UI.WebControls.DataGrid gvCharge;
		protected System.Web.UI.WebControls.Label Label64;
		protected System.Web.UI.WebControls.Button btnDangerousYes;
		protected System.Web.UI.WebControls.Button btnDangerousNo;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divDangerous;
		protected System.Web.UI.HtmlControls.HtmlTable Table4;
		protected System.Web.UI.WebControls.Button btnExecQryHidden;
		protected System.Web.UI.WebControls.Label Label2;
		protected com.common.util.msTextBox txtJobEntryNumber;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.TextBox txtLoadingPort;
		protected System.Web.UI.WebControls.DropDownList ddlLoadingPort;
		protected System.Web.UI.WebControls.TextBox txtLoadingPortDes;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label59;
		protected System.Web.UI.WebControls.TextBox txtHouseAwbNumber;
		protected System.Web.UI.WebControls.Label lblDischargePort;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.DropDownList ddlDischargePort;
		protected System.Web.UI.WebControls.CheckBox cbkDoorToDoor;
		protected System.Web.UI.WebControls.Label lblOriginCity;
		protected System.Web.UI.WebControls.DropDownList ddlOriginCity;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.TextBox txtMasterAwbnumber;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.DropDownList ddlBondedWarehouse;
		protected System.Web.UI.WebControls.TextBox txtBondedWarehouse;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.TextBox txtConsigneeId;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.TextBox txtTaxCode;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.DropDownList ddlCurrencyName;
		protected System.Web.UI.WebControls.Label Label16;
		protected com.common.util.msTextBox txtRateDate;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.TextBox txtRate;
		protected System.Web.UI.WebControls.TextBox txtConsigneeName;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.DropDownList ddlDeliveryTerms;
		protected System.Web.UI.WebControls.TextBox txtDeliveryTerms;
		protected System.Web.UI.WebControls.TextBox txtConsignee1;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.DropDownList ddlEntryType;
		protected System.Web.UI.WebControls.TextBox txtEntryType;
		protected System.Web.UI.WebControls.TextBox txtConsignee2;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.WebControls.DropDownList ddlCustomsAgent;
		protected System.Web.UI.WebControls.TextBox txtCustomsAgent;
		protected System.Web.UI.WebControls.TextBox txtConsigneeZipcode;
		protected System.Web.UI.WebControls.TextBox txtConsigneeState;
		protected System.Web.UI.WebControls.Label Label24;
		protected System.Web.UI.WebControls.Label Label26;
		protected com.common.util.msTextBox txtNumberOfPkgs;
		protected System.Web.UI.WebControls.Label Label27;
		protected System.Web.UI.WebControls.Label Label25;
		protected com.common.util.msTextBox txtWeight;
		protected System.Web.UI.WebControls.Label Label65;
		protected com.common.util.msTextBox txtChargeableWeight;
		protected System.Web.UI.WebControls.TextBox txtConsigneeTelephone;
		protected System.Web.UI.WebControls.Label Label29;
		protected System.Web.UI.WebControls.Label Label28;
		protected System.Web.UI.WebControls.TextBox txtGoodsDescription;
		protected System.Web.UI.WebControls.Label Label30;
		protected System.Web.UI.WebControls.Label Label31;
		protected System.Web.UI.WebControls.Label Label32;
		protected System.Web.UI.WebControls.DropDownList ddlModelOfDeclaration;
		protected System.Web.UI.WebControls.TextBox txtModelOfDeclaration;
		protected System.Web.UI.WebControls.Label Label33;
		protected System.Web.UI.WebControls.DropDownList ddlGeneralProcedureCode;
		protected System.Web.UI.WebControls.TextBox txtGeneralProcedureCode;
		protected System.Web.UI.WebControls.Label Label34;
		protected System.Web.UI.WebControls.Label Label35;
		protected System.Web.UI.WebControls.DropDownList ddlCustomsProcedureCode;
		protected System.Web.UI.WebControls.TextBox txtCustomsProcedureCode;
		protected System.Web.UI.WebControls.Label Label36;
		protected System.Web.UI.WebControls.DropDownList ddlAdditional;
		protected System.Web.UI.WebControls.TextBox txtAdditional;
		protected System.Web.UI.WebControls.Label Label37;
		protected System.Web.UI.WebControls.TextBox txtExporter1;
		protected System.Web.UI.WebControls.Label Label40;
		protected System.Web.UI.WebControls.TextBox txtExporter2;
		protected System.Web.UI.WebControls.Label Label38;
		protected System.Web.UI.WebControls.TextBox txtExporter3;
		protected System.Web.UI.WebControls.Label lblCountry;
		protected System.Web.UI.WebControls.Label Label39;
		protected System.Web.UI.WebControls.DropDownList ddlCountry;
		protected System.Web.UI.WebControls.TextBox txtCountry;
		protected System.Web.UI.WebControls.Label Label41;
		protected System.Web.UI.WebControls.Label Label42;
		protected com.common.util.msTextBox txtInfoReceivedDate;
		protected System.Web.UI.WebControls.Label Label47;
		protected System.Web.UI.WebControls.Label Label48;
		protected System.Web.UI.WebControls.DropDownList ddlProfile;
		protected System.Web.UI.WebControls.Label Label45;
		protected System.Web.UI.WebControls.TextBox txtDeclartionNumber;
		protected System.Web.UI.WebControls.Label Label43;
		protected com.common.util.msTextBox txtDateOfAssessment;
		protected System.Web.UI.WebControls.Label Label55;
		protected com.common.util.msTextBox AssessmentAmount;
		protected System.Web.UI.WebControls.Label Label50;
		protected com.common.util.msTextBox txtDeclartionFromPages;
		protected System.Web.UI.WebControls.Label Label46;
		protected com.common.util.msTextBox txtDuty;
		protected System.Web.UI.WebControls.Label Label44;
		protected com.common.util.msTextBox txtTax;
		protected System.Web.UI.WebControls.Label Label49;
		protected com.common.util.msTextBox txtExcise;
		protected System.Web.UI.WebControls.Label Label51;
		protected com.common.util.msTextBox txtEPF;
		protected System.Web.UI.WebControls.Label Label52;
		protected com.common.util.msTextBox txtTotal;
		protected System.Web.UI.WebControls.Label Label63;
		protected System.Web.UI.WebControls.TextBox txtInvoiceStatus;
		protected System.Web.UI.WebControls.Label Label72;
		protected System.Web.UI.WebControls.TextBox txtHistory_RegisteredBy;
		protected System.Web.UI.WebControls.TextBox txtHistory_RegisteredDT;
		protected System.Web.UI.WebControls.Label Label70;
		protected System.Web.UI.WebControls.TextBox txtHistory_CompiledBy;
		protected System.Web.UI.WebControls.TextBox txtHistory_CompiledDT;
		protected System.Web.UI.WebControls.Label Label69;
		protected System.Web.UI.WebControls.TextBox txtHistory_LastUpdatedBy;
		protected System.Web.UI.WebControls.TextBox txtHistory_LastUpdatedDT;
		protected System.Web.UI.WebControls.Label Label53;
		protected System.Web.UI.WebControls.Label Label54;
		protected System.Web.UI.WebControls.TextBox txtShipFlightNo;
		protected System.Web.UI.WebControls.Label Label56;
		protected System.Web.UI.WebControls.Label Label57;
		protected com.common.util.msTextBox txtExpectedArrivalDate;
		protected System.Web.UI.WebControls.Label Label58;
		protected com.common.util.msTextBox txtActualGoodsArrivalDate;
		protected System.Web.UI.WebControls.Label Label60;
		protected System.Web.UI.WebControls.Label Label61;
		protected System.Web.UI.WebControls.DropDownList ddlModeOfTransport;
		protected System.Web.UI.WebControls.TextBox txtModeOfTransport;
		protected System.Web.UI.WebControls.Label Label62;
		protected System.Web.UI.WebControls.TextBox txtFolioNo;
		protected System.Web.UI.WebControls.Label Label66;
		protected System.Web.UI.WebControls.TextBox txtRemark;
		protected System.Web.UI.WebControls.Button btnCharges;
		protected System.Web.UI.WebControls.Label lblMaximumEntries;
		protected System.Web.UI.WebControls.TextBox txtMaximumEntries;
		protected System.Web.UI.WebControls.Label lblRemainingEntries;
		protected System.Web.UI.WebControls.TextBox txtRemainingEntries;
		protected System.Web.UI.WebControls.CheckBox IgnoreMAWBValidation;
		protected System.Web.UI.WebControls.Button btnClientEvent;
		#endregion
	 
		public DataSet dsDischargePort
		{
			get
			{
				if(ViewState["dsDischargePort"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsDischargePort"];
				}
			}
			set
			{
				ViewState["dsDischargePort"]=value;
			}
		}

		public Int16 indCharge
		{
			get
			{
				if(ViewState["indCharge"] == null)
				{
					return 0;
				}
				else
				{
					return (Int16)ViewState["indCharge"];
				}
			}
			set
			{
				ViewState["indCharge"]=value;
			}
		}
		public Int32 EntryLinesExist
		{
			get
			{
				if(ViewState["EntryLinesExist"] == null)
				{
					return 0;
				}
				else
				{
					return (Int32)ViewState["EntryLinesExist"];
				}
			}
			set
			{
				ViewState["EntryLinesExist"]=value;
			}
		}
		public Int32 ExistsCustoms_JobDetails
		{
			get
			{
				if(ViewState["ExistsCustoms_JobDetails"] == null)
				{
					return 0;
				}
				else
				{
					return (Int32)ViewState["ExistsCustoms_JobDetails"];
				}
			}
			set
			{
				ViewState["ExistsCustoms_JobDetails"]=value;
			}
		}
		public Int32 currency_decimal
		{
			get
			{
				if(ViewState["currency_decimal"] == null)
				{
					return 0;
				}
				else
				{
					return (Int32)ViewState["currency_decimal"];
				}
			}
			set
			{
				ViewState["currency_decimal"]=value;
			}
		}
		public Int16 StdMarksNosEntries
		{
			get
			{
				if(ViewState["StdMarksNosEntries"] == null)
				{
					return 0;
				}
				else
				{
					return (Int16)ViewState["StdMarksNosEntries"];
				}
			}
			set
			{
				ViewState["StdMarksNosEntries"]=value;
			}
		}
		public Int16 MaxMarksNosEntries
		{
			get
			{
				if(ViewState["MaxMarksNosEntries"] == null)
				{
					return 0;
				}
				else
				{
					return (Int16)ViewState["MaxMarksNosEntries"];
				}
			}
			set
			{
				ViewState["MaxMarksNosEntries"]=value;
			}
		}
		public DataSet dsBondedWarehouses
		{
			get
			{
				if(ViewState["dsBondedWarehouses"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsBondedWarehouses"];
				}
			}
			set
			{
				ViewState["dsBondedWarehouses"]=value;
			}
		}

		public DataSet dsEntryType
		{
			get
			{
				if(ViewState["dsEntryType"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsEntryType"];
				}
			}
			set
			{
				ViewState["dsEntryType"]=value;
			}
		}
		public DataSet dsCustomsAgents
		{
			get
			{
				if(ViewState["dsCustomsAgents"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsCustomsAgents"];
				}
			}
			set
			{
				ViewState["dsCustomsAgents"]=value;
			}
		}

		public DataSet dsModelOfDeclaration
		{
			get
			{
				if(ViewState["dsModelOfDeclaration"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsModelOfDeclaration"];
				}
			}
			set
			{
				ViewState["dsModelOfDeclaration"]=value;
			}
		}

		public DataSet dsGeneralProcedureCode
		{
			get
			{
				if(ViewState["dsGeneralProcedureCode"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsGeneralProcedureCode"];
				}
			}
			set
			{
				ViewState["dsGeneralProcedureCode"]=value;
			}
		}

		public DataSet dsCustomsProcedureCode
		{
			get
			{
				if(ViewState["dsCustomsProcedureCode"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsCustomsProcedureCode"];
				}
			}
			set
			{
				ViewState["dsCustomsProcedureCode"]=value;
			}
		}

		public DataSet dsCPCAdditional
		{
			get
			{
				if(ViewState["dsCPCAdditional"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsCPCAdditional"];
				}
			}
			set
			{
				ViewState["dsCPCAdditional"]=value;
			}
		}

		public DataSet dsModeOfTransport
		{
			get
			{
				if(ViewState["dsModeOfTransport"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsModeOfTransport"];
				}
			}
			set
			{
				ViewState["dsModeOfTransport"]=value;
			}
		}
		public DataSet dsDeliveryTerms
		{
			get
			{
				if(ViewState["dsDeliveryTerms"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsDeliveryTerms"];
				}
			}
			set
			{
				ViewState["dsDeliveryTerms"]=value;
			}
		}
		public DataSet dsExpressCities
		{
			get
			{
				if(ViewState["dsExpressCities"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsExpressCities"];
				}
			}
			set
			{
				ViewState["dsExpressCities"]=value;
			}
		}

		public DataSet dsCountry
		{
			get
			{
				if(ViewState["dsCountry"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsCountry"];
				}
			}
			set
			{
				ViewState["dsCountry"]=value;
			}
		}

		public DataSet dsCurrency
		{
			get
			{
				if(ViewState["dsCurrency"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsCurrency"];
				}
			}
			set
			{
				ViewState["dsCurrency"]=value;
			}
		}

		public DataSet dsLoadingPort
		{
			get
			{
				if(ViewState["dsLoadingPort"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsLoadingPort"];
				}
			}
			set
			{
				ViewState["dsLoadingPort"]=value;
			}
		}

		public DataSet dsAllResult
		{
			get
			{
				if(ViewState["dsAllResult"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsAllResult"];
				}
			}
			set
			{
				ViewState["dsAllResult"]=value;
			}
		}
  
		public DataTable dtMarksData
		{
			get
			{
				if(ViewState["dtMarksData"] == null)
				{
					return new DataTable();
				}
				else
				{
					return (DataTable)ViewState["dtMarksData"];
				}
			}
			set
			{
				ViewState["dtMarksData"]=value;
			} 
		}
		public DataTable dtCharge
		{
			get
			{
				if(ViewState["dtCharge"] == null)
				{
					return new DataTable();
				}
				else
				{
					return (DataTable)ViewState["dtCharge"];
				}
			}
			set
			{
				ViewState["dtCharge"]=value;
			} 
		}

		public string m_strSeq
		{
			get{ return (ViewState["m_strSeq"]==null)? "": (String)ViewState["m_strSeq"];}
			set{ViewState["m_strSeq"]=value;}
		}
 

		public string  EntryTypeBegin
		{
			get{ return (ViewState["EntryTypeBegin"]==null)? "": (String)ViewState["EntryTypeBegin"];}
			set{ViewState["EntryTypeBegin"]=value;}
		}
 

		public string strCase
		{
			get{ return (ViewState["strCase"]==null)? "": (String)ViewState["strCase"];}
			set{ViewState["strCase"]=value;}
		}
 
		public string strEntryType
		{
			get{ return (ViewState["strEntryType"]==null)? "": (String)ViewState["strEntryType"];}
			set{ViewState["strEntryType"]=value;}
		}
 
		public bool IsChange
		{
			get{ return ( bool)ViewState["IsChange"];}
			set{ViewState["IsChange"]=value;}
		}
 
		public bool IsCurrencyChange
		{
			get{ return ( bool)ViewState["IsCurrencyChange"];}
			set{ViewState["IsCurrencyChange"]=value;}
		}
 
		public bool DeleteCharge
		{
			get
			{
				if(ViewState["DeleteCharge"] == null)
				{
					return false;
				}
				else
				{
					return (bool)ViewState["DeleteCharge"];
				}
			}
			set
			{
				ViewState["DeleteCharge"]=value;
			}
		}

		public bool isAssignedRoles
		{
			get
			{
				if(ViewState["isAssignedRoles"] == null)
				{
					return false;
				}
				else
				{
					return (bool)ViewState["isAssignedRoles"];
				}
			}
			set
			{
				ViewState["isAssignedRoles"]=value;
			}
		}

		public bool isMarksAndNo
		{
			get
			{
				if(ViewState["isMarksAndNo"] == null)
				{
					return false;
				}
				else
				{
					return (bool)ViewState["isMarksAndNo"];
				}
			}
			set
			{
				ViewState["isMarksAndNo"]=value;
			}
		}

		
		public int Index
		{
			get
			{
				if(ViewState["ChargeIndex"]==null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["ChargeIndex"];
				}
			}
			set
			{
				ViewState["ChargeIndex"]=value;
			}
		}

		//		public int nextSeq
		//		{
		//			get
		//			{
		//				if(ViewState["nextSeq"] == null)
		//				{
		//					return 0;
		//				}
		//				else
		//				{
		//					return (int)ViewState["nextSeq"];
		//				}
		//			}
		//			set
		//			{
		//				ViewState["nextSeq"]=value;
		//			}
		//		}
		

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here			
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();
			strUserLoggin = utility.GetUserID();
			if(!Page.IsPostBack)
			{ 
				GridCmd=null;
				currency_decimal =  GetDigitOfEnterpriseConfiguration();
				strCase = "ExceQuery";
				IsChange = true;
				isMarksAndNo = true;
				IsCurrencyChange = true;
				//SetInitialFocus(btnExecQry);
				this.divDangerous.Visible=false;
				SetDropDownListDisplayValue("DischargePort", ddlDischargePort);
				SetDropDownListDisplayValue("BondedWarehouses", ddlBondedWarehouse);
				SetDropDownListDisplayValue("EntryType", ddlEntryType);
				SetDropDownListDisplayValue("CustomsAgents", ddlCustomsAgent);
				SetDropDownListDisplayValue("ModelOfDeclaration", ddlModelOfDeclaration );
				SetDropDownListDisplayValue("GeneralProcedureCode", ddlGeneralProcedureCode);
				SetDropDownListDisplayValue("CustomsProcedureCode", ddlCustomsProcedureCode);
				SetDropDownListDisplayValue("CPCAdditional", ddlAdditional);
				SetDropDownListDisplayValue("Incoterms", ddlDeliveryTerms);
				SetDropDownListDisplayValue("TransportMode", ddlModeOfTransport);
				SetDropDownListDisplayValue("AssignedProfile", ddlProfile);
				SetDropDownListDisplayValue("ExpressCities", ddlOriginCity);

				DataSet dsCurrencyName =   CustomJobCompilingDAL.GetCurrencies(strAppID, strEnterpriseID); 
				ddlCurrencyName.DataSource= dsCurrencyName;
				ddlCurrencyName.DataTextField="CurrencyCode";
				ddlCurrencyName.DataValueField="CurrencyCode";
				ddlCurrencyName.DataBind(); 
				dsCurrency =  dsCurrencyName;
				 
				DataSet dsGetCountry =   CustomJobCompilingDAL.GetCountryDefaultValue(strAppID, strEnterpriseID); 
				ddlCountry.DataSource= dsGetCountry;
				ddlCountry.DataTextField="CountryCode";
				ddlCountry.DataValueField="CountryCode";
				ddlCountry.DataBind();  				 
				dsCountry = dsGetCountry;
				try
				{ 
					txtCountry.Text = dsCountry.Tables[0].Rows[0]["CountryName"].ToString(); 
				}
				catch
				{
					txtCountry.Text = "";
				}

				ddlLoadingPort.DataSource= dsGetCountry;
				ddlLoadingPort.DataTextField="CountryCode";
				ddlLoadingPort.DataValueField="CountryCode";
				ddlLoadingPort.DataBind();  				 
				dsLoadingPort = dsGetCountry;
				try
				{ 
					txtLoadingPortDes.Text = dsCountry.Tables[0].Rows[0]["CountryName"].ToString(); 
				}
				catch
				{
					txtLoadingPortDes.Text = "";
				}

				GetConfigMarksNos();

				DataSet AssignedRoles =   CustomJobCompilingDAL.GetUserAssignedRoles(strAppID, strEnterpriseID,strUserLoggin); 
				try
				{
					if (AssignedRoles.Tables[0].Rows[0]["IsAssigned"].ToString() == "1")
					{
						isAssignedRoles = true;
					}
					else
					{
						isAssignedRoles = false;
					}
				}
				catch
				{}
				
				if(Request.QueryString["jobentryno"] != null && Request.QueryString["jobentryno"].ToString().Trim() != "")
				{
					txtJobEntryNumber.Text=Request.QueryString["jobentryno"].ToString().Trim();
					btnExecQry_Click(null,null);
					ddlEntryType_SelectedIndexChanged(null,null);
				}

                //aw_Khatawut comment 2019-06-14 : DMSUpgrade 2.4.1) After searching Job Status, there should be able to update data on the search result
                //if(Request.QueryString["pagemode"] != null && Request.QueryString["pagemode"].ToString().Trim() == "popup")
                //{
                //	btnQry.Visible=false;
                //	btnExecQry.Visible=false;
                //	btnSave.Visible = false;
                //	//btnEntryLines.Visible = false;
                //	btnPrintDeclaration.Visible = false;
                //}

                lblOriginCity.Visible=false;
				//lblOriginCityReq.Visible=true;
				ddlOriginCity.Visible=false;
				ddlOriginCity.SelectedIndex=0;
				SetInitialFocus(txtJobEntryNumber);
				//isAssignedRoles
 
				
			}
			this.btnSave.Attributes.Add("onclick","javascript:document.getElementById('"+ this.btnSave.ClientID + "').disabled=true;document.getElementById('"+lblErrorMsg.ClientID+"').innerHTML = '';" + GetPostBackEventReference(this.btnSave));
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnEntryLines.Click += new System.EventHandler(this.btnEntryLines_Click);
			this.btnPrintDeclaration.Click += new System.EventHandler(this.btnPrintDeclaration_Click);
			this.btnClientEvent.Click += new System.EventHandler(this.btnClientEvent_Click);
			this.btnExecQryHidden.Click += new System.EventHandler(this.btnExecQryHidden_Click);
			this.ddlLoadingPort.SelectedIndexChanged += new System.EventHandler(this.ddlLoadingPort_SelectedIndexChanged);
			this.ddlDischargePort.SelectedIndexChanged += new System.EventHandler(this.ddlDischargePort_SelectedIndexChanged);
			this.cbkDoorToDoor.CheckedChanged += new System.EventHandler(this.cbkDoorToDoor_CheckedChanged);
			this.ddlBondedWarehouse.SelectedIndexChanged += new System.EventHandler(this.ddlBondedWarehouse_SelectedIndexChanged);
			this.ddlCurrencyName.SelectedIndexChanged += new System.EventHandler(this.ddlCurrencyName_SelectedIndexChanged);
			this.ddlDeliveryTerms.SelectedIndexChanged += new System.EventHandler(this.ddlDeliveryTerms_SelectedIndexChanged);
			this.ddlEntryType.SelectedIndexChanged += new System.EventHandler(this.ddlEntryType_SelectedIndexChanged);
			this.ddlCustomsAgent.SelectedIndexChanged += new System.EventHandler(this.ddlCustomsAgent_SelectedIndexChanged);
			this.ddlModelOfDeclaration.SelectedIndexChanged += new System.EventHandler(this.ddlModelOfDeclaration_SelectedIndexChanged);
			this.ddlGeneralProcedureCode.SelectedIndexChanged += new System.EventHandler(this.ddlGeneralProcedureCode_SelectedIndexChanged);
			this.ddlCustomsProcedureCode.SelectedIndexChanged += new System.EventHandler(this.ddlCustomsProcedureCode_SelectedIndexChanged);
			this.ddlAdditional.SelectedIndexChanged += new System.EventHandler(this.ddlAdditional_SelectedIndexChanged);
			this.ddlCountry.SelectedIndexChanged += new System.EventHandler(this.ddlCountry_SelectedIndexChanged);
			this.ddlProfile.SelectedIndexChanged += new System.EventHandler(this.ddlProfile_SelectedIndexChanged);
			this.ddlModeOfTransport.SelectedIndexChanged += new System.EventHandler(this.ddlModeOfTransport_SelectedIndexChanged);
			this.btnCharges.Click += new System.EventHandler(this.btnCharges_Click);
			this.gvMarksAndNo.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.gvMarksAndNo_ItemCommand);
			this.gvMarksAndNo.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.gvMarksAndNo_ItemDataBound);
			this.gvCharge.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.gvCharge_ItemCommand);
			this.gvCharge.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.gvCharge_ItemDataBound);
			this.btnDangerousYes.Click += new System.EventHandler(this.btnDangerousYes_Click);
			this.btnDangerousNo.Click += new System.EventHandler(this.btnDangerousNo_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		#region "Event Click"
		private void btnEntryLines_Click(object sender, System.EventArgs e)
		{
			String strUrl = null;
			strUrl = "CustomsJobEntryCompilingEntryLines.aspx?payerid="+dsAllResult.Tables[2].Rows[0]["CustomerID"].ToString()+"&consignment_no="+txtHouseAwbNumber.Text.Trim();
			strUrl+="&appID="+strAppID+"&enterpriseID="+strEnterpriseID+"&userID="+strUserLoggin+"&JobEntryNo="+txtJobEntryNumber.Text;
			strUrl+="&countryCode="+ddlCountry.SelectedValue+"&currencyCode="+ddlCurrencyName.SelectedValue;
			strUrl+="&Models="+ddlModelOfDeclaration.SelectedValue+"&Declaretion="+ddlCustomsProcedureCode.SelectedValue;
			
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindowCustomSize.js",paramList,500,1300);
			if(Request.QueryString["pagemode"] != null && Request.QueryString["pagemode"].ToString().Trim() == "popup")
			{
				strUrl+="&pagemode=popup";
				sScript="<script language=javascript>var win = window.open('"+strUrl+"','','height=500,width=1300,left=100,top=50,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no');childwindows[childwindows.length] = win;</script>";
				Utility.RegisterScriptString(sScript,"ShowPopupScript"+DateTime.Now.ToString("ddMMyyyyHHmmss"),this.Page);
			}
			else
			{
				Utility.RegisterScriptString(sScript,"ShowPopupScript"+DateTime.Now.ToString("ddMMyyyyHHmmss"),this.Page);
			}
			
		}


		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			lblError.Text="";
			if(Utility.GetConsNoRegex(txtMasterAwbnumber.Text.Trim())==false)
			{
				lblError.Text = "Master AWB Number is invalid.";
				SetInitialFocus(txtMasterAwbnumber);
				return;
			}
			GridCmd=null;
			BindData();

            //aw_Khatawut comment 2019-06-14 : DMSUpgrade 2.4.1) After searching Job Status, there should be able to update data on the search result
            //if(Request.QueryString["pagemode"] != null && Request.QueryString["pagemode"].ToString().Trim() == "popup")
            //{
            //	gvMarksAndNo.ShowFooter=false;
            //	gvMarksAndNo.Columns[0].Visible=false;
            //	gvCharge.Columns[0].Visible=false;
            //	gvCharge.ShowFooter=false;
            //}

            gvMarksAndNo.DataSource=dsAllResult.Tables[2];
			gvMarksAndNo.DataBind();
			dtMarksData=dsAllResult.Tables[2]; 
			
			lblMaximumEntries.Visible = true;
			lblRemainingEntries.Visible = true;
			txtMaximumEntries.Visible = true;
			txtRemainingEntries.Visible = true;
			gvMarksAndNo.EditItemIndex = -1;	
			gvCharge.DataSource=null;
			gvCharge.DataBind();
			gvCharge.EditItemIndex = -1;
			//btnExecQry.Enabled = true;					
			SetInitialFocus(txtMasterAwbnumber);
		}
 

		private void btnCharges_Click(object sender, System.EventArgs e)
		{
			lblError.Text = "";
			if (btnCharges.Text == "Charges")
			{
				isMarksAndNo = true;
				lblHeader.Text = "Charges";
				btnCharges.Text =  "Marks & Nos";
				lblMaximumEntries.Visible = false;
				lblRemainingEntries.Visible = false;
				txtMaximumEntries.Visible = false;
				txtRemainingEntries.Visible = false;

				if (dsAllResult.Tables[3].Rows.Count < 1)
				{
					gvCharge.DataSource=null;
					gvCharge.DataBind();
					lblError.Text = "Please save to see default charges";
				}
				else
				{
					gvCharge.DataSource=dsAllResult.Tables[3];
					gvCharge.DataBind();
				}
				
				gvMarksAndNo.DataSource=null;
				gvMarksAndNo.DataBind();
				gvCharge.EditItemIndex = -1;
				gvMarksAndNo.EditItemIndex = -1;
			}
			else
			{
				isMarksAndNo = false;
				lblHeader.Text = "Marks & Nos.";
				btnCharges.Text =  "Charges";
				lblMaximumEntries.Visible = true;
				lblRemainingEntries.Visible = true;
				txtMaximumEntries.Visible = true;
				txtRemainingEntries.Visible = true;
				gvCharge.DataSource=null;
				gvCharge.DataBind();
				gvCharge.EditItemIndex = -1; 
				gvMarksAndNo.DataSource=dsAllResult.Tables[2];
				gvMarksAndNo.DataBind();
				gvMarksAndNo.EditItemIndex = -1;
			}
			
		}


		private void btnQry_Click(object sender, System.EventArgs e)
		{
			GridCmd=null;
			CleareScreen();

			SetInitialFocus(txtJobEntryNumber);		
		}


		private void btnDangerousYes_Click(object sender, System.EventArgs e)
		{ 
			lblError.Text = "";
			try
			{
				//TO DO Delete Charge	
				CustomJobCompilingDAL.Customs_JobDeleteDetails(strAppID,
					strEnterpriseID,
					strUserLoggin ,
					txtHouseAwbNumber.Text,
					dsAllResult.Tables[1].Rows[0]["CustomerID"].ToString(),
					dsAllResult.Tables[1].Rows[0]["JobEntryNo"].ToString());

				

				divMain.Visible=true;
				divDangerous.Visible=false;
				DeleteCharge= true;  
				//btnCharges.Enabled = false;
				lblHeader.Text = "Marks & Nos.";
				btnCharges.Text =  "Charges";
				BindCharge();

				gvCharge.DataSource=null;
				gvCharge.DataBind();
				gvCharge.EditItemIndex = -1; 

				SaveData();
			}
			catch (Exception ex) {}
 			btnSave.Enabled=true;
		}


		private void btnDangerousNo_Click(object sender, System.EventArgs e)
		{
			try
			{
				ddlEntryType.SelectedValue = EntryTypeBegin;
				lblError.Text = "";
				divMain.Visible=true;			
				divDangerous.Visible=false;
				DeleteCharge=false; 
	            
				IsChange = false;
				btnEntryLines.Enabled = true;
				strEntryType = ddlEntryType.SelectedValue;
				btnCharges.Enabled = true; 
				DataRow[] result=  dsEntryType.Tables[0].Select("CodedValue = '" + ddlEntryType.SelectedValue + "'");
				txtEntryType.Text = result[0]["Description"].ToString();

			}
			catch
			{				
				txtEntryType.Text = "";			
			}
			btnSave.Enabled=true;
		}

		
		private void btnClientEvent_Click(object sender, System.EventArgs e)
		{
			ExistsCustoms_JobDetails =  GetExistsCustoms_JobDetails();
			btnPrintDeclaration.Enabled = false;
			if(EntryLinesExist>0)
			{
				btnPrintDeclaration.Enabled = true;
			}
		}
 

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			btnSave.Enabled = false;
			//strCase = "btnSave";
			lblError.Text = "";
			if(Utility.GetConsNoRegex(txtMasterAwbnumber.Text.Trim())==false)
			{
				lblError.Text = "Master AWB Number is invalid.";
				SetInitialFocus(txtMasterAwbnumber);
				btnSave.Enabled =true;
				return;
			}
			try
			{

				if (EntryTypeBegin != "F" && ddlEntryType.SelectedValue  == "F")
				{
					ExistsCustoms_JobDetails =  GetExistsCustoms_JobDetails();
					if(ExistsCustoms_JobDetails > 0)
					{
						divMain.Visible=false; 	
						divDangerous.Visible=true; 			    
						btnCharges.Enabled = false;
					}
					else
					{
						SaveData();
					}					
				}
				else
				{
					SaveData();
				}
			}
			catch (Exception ex)
			{
				lblErrorMsg.Text = ex.Message;
			}

			btnSave.Enabled = true;
			this.strScrollPosition = "0";
		}


		private void SaveData()
		{
			if(ValidateData())
			{		
				DataTable dtParams = CustomJobCompilingDAL.CustomJobCompilingParameter();
				System.Data.DataRow drNew = dtParams.NewRow();
				drNew["action"] ="0"; 
				drNew["strAppID"] = strAppID;
				drNew["enterpriseid"] = strEnterpriseID; 
				drNew["userloggedin"] =strUserLoggin;
				drNew["consignment_no"] = txtHouseAwbNumber.Text;
				drNew["JobEntryNo"] = txtJobEntryNumber.Text;
				drNew["TransportMode"] =  ddlModeOfTransport.SelectedValue; 
				drNew["ExporterName"] =txtExporter1.Text;
				drNew["ExporterAddress1"] =txtExporter2.Text;
				drNew["ExporterAddress2"] =txtExporter3.Text;
				drNew["ExporterCountry"] =ddlCountry.SelectedValue;
				drNew["LoadingPortCity"] =txtLoadingPort.Text;
				drNew["LoadingPortCountry"] =ddlLoadingPort.SelectedValue;
				drNew["DischargePort"] = ddlDischargePort.SelectedValue; 
				drNew["CurrencyCode"] =ddlCurrencyName.SelectedValue ;
				drNew["InfoReceivedDate"] =System.DateTime.ParseExact(txtInfoReceivedDate.Text.Trim(),"dd/MM/yyyy",null);
				drNew["MasterAWBNumber"] =txtMasterAwbnumber.Text;
				drNew["ShipFlightNo"] =txtShipFlightNo.Text;

				if (txtExpectedArrivalDate.Text.Trim() == "") {drNew["ExpectedArrivalDate"] = null;}
				else {drNew["ExpectedArrivalDate"] =System.DateTime.ParseExact(txtExpectedArrivalDate.Text.Trim(),"dd/MM/yyyy",null);}

				if (txtActualGoodsArrivalDate.Text.Trim() == "") {drNew["ActualArrivalDate"] = null;}
				else {drNew["ActualArrivalDate"] = System.DateTime.ParseExact(txtActualGoodsArrivalDate.Text.Trim(),"dd/MM/yyyy",null);}
 
				drNew["EntryType"] =ddlEntryType.SelectedValue;
				drNew["DeliveryTerms"] =ddlDeliveryTerms.SelectedValue;

				if (txtFolioNo.Text.Trim() == "") {drNew["FolioNumber"] = null;}
				else {drNew["FolioNumber"] = txtFolioNo.Text;}
 
				drNew["BondedWarehouse"] =ddlBondedWarehouse.SelectedValue ;
				if (txtRateDate.Text.Trim() == "")
				{
					drNew["CurrencyRateDate"] = DBNull.Value;
				}
				else
				{
					drNew["CurrencyRateDate"] = System.DateTime.ParseExact(txtRateDate.Text.Trim(),"dd/MM/yyyy",null);//System.DateTime.ParseExact(txtRateDate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
				}
				drNew["CustomsAgent"] =ddlCustomsAgent.SelectedValue;
				drNew["ModelOfDeclaration"] =ddlModelOfDeclaration.SelectedValue;

				if (ddlGeneralProcedureCode.SelectedValue.Trim() == "") {drNew["GeneralProcedureCode"] = null;}
				else {drNew["GeneralProcedureCode"] =  ddlGeneralProcedureCode.SelectedValue;}
 
				drNew["CustomsProcedureCode"] =ddlCustomsProcedureCode.SelectedValue;

				if (ddlAdditional.SelectedValue.Trim() == "") {drNew["CustomsAddlCode"] = null;}
				else {drNew["CustomsAddlCode"] =  ddlAdditional.SelectedValue;}
 
				drNew["CustomsDeclarationNo"] =txtDeclartionNumber.Text ;

				if (txtDateOfAssessment.Text.Trim() == "")
				{drNew["DateOfAssessment"] = null;}
				else
				{drNew["DateOfAssessment"] =System.DateTime.ParseExact(txtDateOfAssessment.Text.Trim(),"dd/MM/yyyy",null);}					
					
				drNew["AssignedProfile"] =ddlProfile.SelectedValue;					 
				drNew["DeclarationFormPages"] =txtDeclartionFromPages.Text ;  					
				drNew["Total_pkgs"] =txtNumberOfPkgs.Text;
				drNew["Total_weight"] =txtWeight.Text.Replace(",","");
				if(txtChargeableWeight.Text.Trim() != "")
				{
					drNew["ChargeableWeight"] =txtChargeableWeight.Text.Replace(",","");
				}
				drNew["Remarks"] =txtRemark.Text.Trim();
				drNew["GoodsDescription"] =txtGoodsDescription.Text;

				if(!AssessmentAmount.Text.Trim().Equals(""))
					drNew["AssessmentAmount"] = AssessmentAmount.Text.Trim();

				if(cbkDoorToDoor.Checked)
				{
					drNew["ExpressDoorToDoor"] = 1;
					if(ddlOriginCity.SelectedValue  !="")
					{
						drNew["ExpressCity"] = ddlOriginCity.SelectedValue;
					}
						
				}
				else
				{
					drNew["ExpressDoorToDoor"] = 0;
					drNew["ExpressCity"] = DBNull.Value;
				}
				drNew["IgnoreMAWBValidation"] =(IgnoreMAWBValidation.Checked==true?1:0);
				dtParams.Rows.Add(drNew);
				DataSet dsJobCompiling = CustomJobCompilingDAL.SaveCustomJobCompiling(dtParams);
					
				if(dsJobCompiling !=null && dsJobCompiling.Tables[0].Rows.Count > 0)
				{
					if(Convert.ToInt32( dsJobCompiling.Tables[0].Rows[0]["ErrorCode"]) > 0)
					{
						lblErrorMsg.Text = dsJobCompiling.Tables[0].Rows[0]["ErrorMessage"].ToString();
					}
					else
					{												
						if (dsJobCompiling.Tables[0].Rows[0]["EnableOtherButtons"].ToString() == "0")
						{
							btnCharges.Enabled = false;
							btnEntryLines.Enabled = false;
						}
						else
						{
							btnCharges.Enabled = true;
							btnEntryLines.Enabled = true;
						}
						ExistsCustoms_JobDetails =  GetExistsCustoms_JobDetails();
						if(EntryLinesExist>0)
						{
							btnPrintDeclaration.Enabled=true;
						}
						else
						{
							btnPrintDeclaration.Enabled=false;
						}

						#region Display detail

						txtJobEntryNumber.Text =dsJobCompiling.Tables[1].Rows[0]["JobEntryNo"].ToString(); 
						txtHouseAwbNumber.Text =dsJobCompiling.Tables[1].Rows[0]["HouseAWBNo"].ToString(); 
						txtMasterAwbnumber.Text =dsJobCompiling.Tables[1].Rows[0]["MasterAWBNumber"].ToString();  
						txtLoadingPort.Text =dsJobCompiling.Tables[1].Rows[0]["LoadingPortCity"].ToString(); 
						txtNumberOfPkgs.Text = dsJobCompiling.Tables[1].Rows[0]["Tot_Pkgs"].ToString(); 
						//txtWeight.Text = (dsJobCompiling.Tables[1].Rows[0]["Tot_Wt"] != DBNull.Value ? Convert.ToDecimal(dsJobCompiling.Tables[1].Rows[0]["Tot_Wt"]).ToString("#,##0") :"");
							
						if(dsJobCompiling.Tables[1].Rows[0]["Tot_Wt"] != DBNull.Value)
						{
							txtWeight.Text = Decimal.Parse(dsJobCompiling.Tables[1].Rows[0]["Tot_Wt"].ToString()).ToString("N1");
						}

						if(dsJobCompiling.Tables[1].Rows[0]["ChargeableWeight"] != DBNull.Value)
						{
							txtChargeableWeight.Text = Decimal.Parse(dsJobCompiling.Tables[1].Rows[0]["ChargeableWeight"].ToString()).ToString("N1");
						}
						txtRemark.Text = dsJobCompiling.Tables[1].Rows[0]["Remarks"].ToString(); 

						txtHistory_RegisteredBy.Text=dsJobCompiling.Tables[1].Rows[0]["History_RegisteredBy"].ToString();
						txtHistory_RegisteredDT.Text=(dsJobCompiling.Tables[1].Rows[0]["History_RegisteredDT"] != DBNull.Value ? DateTime.Parse(dsJobCompiling.Tables[1].Rows[0]["History_RegisteredDT"].ToString()).ToString("dd/MM/yyyy HH:mm") :"");
						txtHistory_CompiledBy.Text=dsJobCompiling.Tables[1].Rows[0]["History_CompiledBy"].ToString();
						txtHistory_CompiledDT.Text=(dsJobCompiling.Tables[1].Rows[0]["History_CompiledDT"] != DBNull.Value ? DateTime.Parse(dsJobCompiling.Tables[1].Rows[0]["History_CompiledDT"].ToString()).ToString("dd/MM/yyyy HH:mm") :"");
						txtHistory_LastUpdatedBy.Text=dsJobCompiling.Tables[1].Rows[0]["History_LastUpdatedBy"].ToString();
						txtHistory_LastUpdatedDT.Text=(dsJobCompiling.Tables[1].Rows[0]["History_LastUpdatedDT"] != DBNull.Value ? DateTime.Parse(dsJobCompiling.Tables[1].Rows[0]["History_LastUpdatedDT"].ToString()).ToString("dd/MM/yyyy HH:mm") :"");

						//txtWeight.Text = Math.Round(Double.Parse(dsJobCompiling.Tables[1].Rows[0]["Tot_Wt"].ToString())).ToString(); 
						txtGoodsDescription.Text = dsJobCompiling.Tables[1].Rows[0]["GoodsDescription"].ToString(); 
						txtRateDate.Text = (dsJobCompiling.Tables[1].Rows[0]["CurrencyRateDate"] != DBNull.Value ? DateTime.Parse(dsJobCompiling.Tables[1].Rows[0]["CurrencyRateDate"].ToString()).ToString("dd/MM/yyyy") :"");
						txtRate.Text = dsJobCompiling.Tables[1].Rows[0]["Exchange_rate"].ToString(); 

						//Consignee 
						txtConsigneeId.Text = dsJobCompiling.Tables[1].Rows[0]["CustomerID"].ToString(); 
						txtTaxCode.Text = dsJobCompiling.Tables[1].Rows[0]["CustomerTaxCode"].ToString(); 
						txtConsigneeName.Text = dsJobCompiling.Tables[1].Rows[0]["Consignee_name"].ToString(); 
						txtConsignee1.Text = dsJobCompiling.Tables[1].Rows[0]["Consignee_address1"].ToString(); 
						txtConsignee2.Text = dsJobCompiling.Tables[1].Rows[0]["Consignee_address2"].ToString(); 
						txtConsigneeZipcode.Text = dsJobCompiling.Tables[1].Rows[0]["Consignee_zipcode"].ToString();
						txtConsigneeState.Text = dsJobCompiling.Tables[1].Rows[0]["Consignee_state"].ToString();
						txtConsigneeTelephone.Text = dsJobCompiling.Tables[1].Rows[0]["Consignee_telephone"].ToString();

						//Custom Formal Declaration
						txtDeclartionNumber.Text = dsJobCompiling.Tables[1].Rows[0]["CustomsDeclarationNo"].ToString(); 
						txtDateOfAssessment.Text = (dsJobCompiling.Tables[1].Rows[0]["DateOfAssessment"] != DBNull.Value ? DateTime.Parse(dsJobCompiling.Tables[1].Rows[0]["DateOfAssessment"].ToString()).ToString("dd/MM/yyyy") :"");
						txtDeclartionFromPages.Text = dsJobCompiling.Tables[1].Rows[0]["DeclarationFormPages"].ToString(); 
						string tempplate = "";
						for (int i = 0 ; i< currency_decimal ; i++)
						{
							tempplate = tempplate+"0";
						}
						if(dsJobCompiling.Tables[1].Rows[0]["Duty_Amount"].ToString() != "")
						{
							txtDuty.Text =  Convert.ToDecimal(dsJobCompiling.Tables[1].Rows[0]["Duty_Amount"]).ToString("##0." + tempplate);  //dtAllData.Rows[0]["Duty_Amount"].ToString(); 
						}
						if(dsJobCompiling.Tables[1].Rows[0]["Tax_Amount"].ToString() != "")
						{
							txtTax.Text = Convert.ToDecimal(dsJobCompiling.Tables[1].Rows[0]["Tax_Amount"]).ToString("##0." + tempplate);  //dtAllData.Rows[0]["Tax_Amount"].ToString();
						}
						if(dsJobCompiling.Tables[1].Rows[0]["Excise_Amount"].ToString() != "")
						{
							txtExcise.Text = Convert.ToDecimal(dsJobCompiling.Tables[1].Rows[0]["Excise_Amount"]).ToString("##0." + tempplate);  // dtAllData.Rows[0]["Excise_Amount"].ToString(); 
						}
						if(dsJobCompiling.Tables[1].Rows[0]["EntryProcessingFee"].ToString() != "")
						{
							txtEPF.Text = Convert.ToDecimal(dsJobCompiling.Tables[1].Rows[0]["EntryProcessingFee"]).ToString("##0." + tempplate); 
						}
						if(dsJobCompiling.Tables[1].Rows[0]["Total_Amount"].ToString() != "")
						{
							txtTotal.Text = Convert.ToDecimal(dsJobCompiling.Tables[1].Rows[0]["Total_Amount"]).ToString("##0." + tempplate);  //dtAllData.Rows[0]["Total_Amount"].ToString(); 
						}
				 
						//txtMaximumEntries.Text = dtStatus.Rows[0]["MaximumEntries"].ToString(); 
						//txtRemainingEntries.Text = dtStatus.Rows[0]["RemainingEntries"].ToString(); 
	 
						//Eport
						txtExporter1.Text = dsJobCompiling.Tables[1].Rows[0]["ExporterName"].ToString(); 
						txtExporter2.Text = dsJobCompiling.Tables[1].Rows[0]["ExporterAddress1"].ToString(); 
						txtExporter3.Text = dsJobCompiling.Tables[1].Rows[0]["ExporterAddress2"].ToString(); 
 
						txtInfoReceivedDate.Text =(dsJobCompiling.Tables[1].Rows[0]["InfoReceivedDate"] != DBNull.Value ? DateTime.Parse(dsJobCompiling.Tables[1].Rows[0]["InfoReceivedDate"].ToString()).ToString("dd/MM/yyyy") :"");
						txtShipFlightNo.Text = dsJobCompiling.Tables[1].Rows[0]["ShipFlightNo"].ToString(); 
						txtExpectedArrivalDate.Text = (dsJobCompiling.Tables[1].Rows[0]["ExpectedArrivalDate"] != DBNull.Value ? DateTime.Parse(dsJobCompiling.Tables[1].Rows[0]["ExpectedArrivalDate"].ToString()).ToString("dd/MM/yyyy") :"");
						txtActualGoodsArrivalDate.Text = (dsJobCompiling.Tables[1].Rows[0]["ActualArrivalDate"] != DBNull.Value ? DateTime.Parse(dsJobCompiling.Tables[1].Rows[0]["ActualArrivalDate"].ToString()).ToString("dd/MM/yyyy") :"");
						txtFolioNo.Text =dsJobCompiling.Tables[1].Rows[0]["FolioNumber"].ToString();  
   
						ddlDischargePort.SelectedValue = dsJobCompiling.Tables[1].Rows[0]["DischargePort"].ToString();
						ddlBondedWarehouse.SelectedValue = dsJobCompiling.Tables[1].Rows[0]["BondedWarehouse"].ToString();
						ddlEntryType.SelectedValue = dsJobCompiling.Tables[1].Rows[0]["EntryType"].ToString();
						EntryTypeBegin = dsJobCompiling.Tables[1].Rows[0]["EntryType"].ToString();
						if(ddlEntryType.SelectedValue.Equals("F"))
						{
							txtDeclartionFromPages.Enabled = true;
							txtDeclartionNumber.Enabled = true;
							txtDateOfAssessment.Enabled = true;
							AssessmentAmount.Enabled = true;
						}
						else
						{
							txtDeclartionFromPages.Enabled = false;
							txtDeclartionNumber.Enabled = false;
							txtDateOfAssessment.Enabled = false;
							AssessmentAmount.Enabled = false;
						}
						ddlCustomsAgent.SelectedValue = dsJobCompiling.Tables[1].Rows[0]["CustomsAgent"].ToString();
						ddlModelOfDeclaration.SelectedValue = dsJobCompiling.Tables[1].Rows[0]["ModelOfDeclaration"].ToString();
						ddlGeneralProcedureCode.SelectedValue = dsJobCompiling.Tables[1].Rows[0]["GeneralProcedureCode"].ToString();
						ddlCustomsProcedureCode.SelectedValue = dsJobCompiling.Tables[1].Rows[0]["CustomsProcedureCode"].ToString();
						ddlAdditional.SelectedValue = dsJobCompiling.Tables[1].Rows[0]["CustomsAddlCode"].ToString();
						ddlDeliveryTerms.SelectedValue = dsJobCompiling.Tables[1].Rows[0]["DeliveryTerms"].ToString();
						ddlModeOfTransport.SelectedValue = dsJobCompiling.Tables[1].Rows[0]["TransportMode"].ToString();
						ddlProfile.SelectedValue = dsJobCompiling.Tables[1].Rows[0]["AssignedProfile"].ToString();
						strEntryType = dsJobCompiling.Tables[1].Rows[0]["EntryType"].ToString();
						txtBondedWarehouse.Text = dsJobCompiling.Tables[1].Rows[0]["BondedWarehouse_Desc"].ToString();
						txtEntryType.Text = dsJobCompiling.Tables[1].Rows[0]["EntryType_Desc"].ToString();
						txtCustomsAgent.Text = dsJobCompiling.Tables[1].Rows[0]["CustomsAgent_Desc"].ToString();
						txtModelOfDeclaration.Text = dsJobCompiling.Tables[1].Rows[0]["ModelOfDeclaration_Desc"].ToString();
						txtGeneralProcedureCode.Text = dsJobCompiling.Tables[1].Rows[0]["GeneralProcedureCode_Desc"].ToString();
						txtCustomsProcedureCode.Text = dsJobCompiling.Tables[1].Rows[0]["CustomsProcedureCode_Desc"].ToString();
						txtAdditional.Text = dsJobCompiling.Tables[1].Rows[0]["CustomsAddlCode_Desc"].ToString();
						txtDeliveryTerms.Text = dsJobCompiling.Tables[1].Rows[0]["DeliveryTerms_Desc"].ToString();
						txtModeOfTransport.Text = dsJobCompiling.Tables[1].Rows[0]["TransportMode_Desc"].ToString();
 			
						ddlCurrencyName.SelectedValue = dsJobCompiling.Tables[1].Rows[0]["CurrencyCode"].ToString();  				
						ddlCountry.SelectedValue = dsJobCompiling.Tables[1].Rows[0]["ExporterCountry"].ToString();
						ddlLoadingPort.SelectedValue = dsJobCompiling.Tables[1].Rows[0]["ExporterCountry"].ToString();
			
						if(!dsJobCompiling.Tables[1].Rows[0]["AssessmentAmount"].ToString().Trim().Equals(""))
							AssessmentAmount.Text = Convert.ToDecimal(dsJobCompiling.Tables[1].Rows[0]["AssessmentAmount"].ToString()).ToString("N" + currency_decimal.ToString());

						#endregion
	 
						dsAllResult = dsJobCompiling;

						if (btnCharges.Text != "Charges")
						{  			
							gvCharge.DataSource=null;
							gvCharge.DataBind();
							gvCharge.EditItemIndex = -1; 
							gvCharge.DataSource=dsJobCompiling.Tables[3];
							gvCharge.DataBind(); 

							gvMarksAndNo.DataSource=null;
							gvMarksAndNo.DataBind();
							gvCharge.EditItemIndex = -1;
							gvMarksAndNo.EditItemIndex = -1;
						}
						else
						{		
							gvMarksAndNo.EditItemIndex = -1;
							gvCharge.DataSource=null;
							gvCharge.DataBind();
							gvCharge.EditItemIndex = -1; 
							gvMarksAndNo.DataSource=dsJobCompiling.Tables[2];
							gvMarksAndNo.DataBind();
								
						}							

						lblErrorMsg.Text = dsJobCompiling.Tables[0].Rows[0]["ErrorMessage"].ToString();
					}
				}
				else
				{
					// Case Save Fail
				}					  
			}
		}


		#endregion
		
		#region "Event SelectedIndexChanged"
		private void ddlDischargePort_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//txtdi = ds.Table(0).[Select]("ID = " + newiddd.ToString());
			SetInitialFocus(ddlDischargePort);
		}


		private void ddlBondedWarehouse_SelectedIndexChanged(object sender, System.EventArgs e)
		{  
			 
			try
			{
				DataRow[] result=  dsBondedWarehouses.Tables[0].Select("CodedValue = '" + ddlBondedWarehouse.SelectedValue + "'");
				txtBondedWarehouse.Text = result[0]["Description"].ToString();
				SetInitialFocus(ddlBondedWarehouse);
			}
			catch
			{
				txtBondedWarehouse.Text = "";
			}
				
		}


		private void ddlDeliveryTerms_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				DataRow[] result=  dsDeliveryTerms.Tables[0].Select("CodedValue = '" + ddlDeliveryTerms.SelectedValue + "'");
				txtDeliveryTerms.Text = result[0]["Description"].ToString();
				SetInitialFocus(ddlDeliveryTerms);
			}
			catch
			{
				txtDeliveryTerms.Text = "";
			}
		}


		private void ddlEntryType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblError.Text = "";
			if (!IsChange)
			{
				IsChange = false;
			} 
			try
			{		
				DataRow[] result=  dsEntryType.Tables[0].Select("CodedValue = '" + ddlEntryType.SelectedValue + "'");
				txtEntryType.Text = result[0]["Description"].ToString();	
				if (ddlEntryType.SelectedValue  == "F" ) 
				{
					gvCharge.DataSource=null;
					gvCharge.DataBind();
					lblHeader.Text = "Marks & Nos.";
					btnCharges.Text =  "Charges";
					gvMarksAndNo.DataSource= dsAllResult.Tables[2];
					gvMarksAndNo.DataBind();
					SetInitialFocus(gvMarksAndNo);
				}
				else
				{	
					IsChange = true;
				} 
				SetInitialFocus(ddlEntryType);
			}
			catch
			{
				if (ddlEntryType.SelectedIndex == 0)
				{
					txtEntryType.Text = "";
				}			  
			} 
		}


		private void ddlCustomsAgent_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				DataRow[] result=  dsCustomsAgents.Tables[0].Select("CodedValue = '" + ddlCustomsAgent.SelectedValue + "'");
				txtCustomsAgent.Text = result[0]["Description"].ToString();
				SetInitialFocus(ddlCustomsAgent);
			}
			catch
			{
				txtCustomsAgent.Text = "";
			}	 
		}


		private void ddlModelOfDeclaration_SelectedIndexChanged(object sender, System.EventArgs e)
		{ 
			try
			{
				DataRow[] result=  dsModelOfDeclaration.Tables[0].Select("CodedValue = '" + ddlModelOfDeclaration.SelectedValue + "'");
				txtModelOfDeclaration.Text = result[0]["Description"].ToString();
				SetInitialFocus(ddlModelOfDeclaration);
			}
			catch
			{
				txtModelOfDeclaration.Text = "";
			}	 
		}


		private void ddlGeneralProcedureCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				DataRow[] result=  dsGeneralProcedureCode.Tables[0].Select("CodedValue = '" + ddlGeneralProcedureCode.SelectedValue + "'");
				txtGeneralProcedureCode.Text = result[0]["Description"].ToString();		
				SetInitialFocus(ddlGeneralProcedureCode);
			}
			catch
			{
				txtGeneralProcedureCode.Text = "";
			}
		}


		private void ddlCustomsProcedureCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{ 
			try
			{
				DataRow[] result=  dsCustomsProcedureCode.Tables[0].Select("CodedValue = '" + ddlCustomsProcedureCode.SelectedValue + "'");
				txtCustomsProcedureCode.Text = result[0]["Description"].ToString();
				SetInitialFocus(ddlCustomsProcedureCode);
			}
			catch
			{
				txtCustomsProcedureCode.Text = "";
			}		
		}


		private void ddlAdditional_SelectedIndexChanged(object sender, System.EventArgs e)
		{ 
			try
			{
				DataRow[] result=  dsCPCAdditional.Tables[0].Select("CodedValue = '" + ddlAdditional.SelectedValue + "'");
				txtAdditional.Text = result[0]["Description"].ToString();
				SetInitialFocus(ddlAdditional);
			}
			catch
			{
				txtAdditional.Text = "";
			}
		}


		private void ddlModeOfTransport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				DataRow[] result=  dsModeOfTransport.Tables[0].Select("CodedValue = '" + ddlModeOfTransport.SelectedValue + "'");
				txtModeOfTransport.Text = result[0]["Description"].ToString();
				SetInitialFocus(ddlModeOfTransport);
			}
			catch
			{
				txtModeOfTransport.Text = "";
			}
		}


		private void ddlCountry_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				DataRow[] result=  dsCountry.Tables[0].Select("CountryCode = '" + ddlCountry.SelectedValue + "'");
				txtCountry.Text = result[0]["CountryName"].ToString();
				ddlCurrencyName.SelectedValue = result[0]["Currencycode"].ToString(); 
				IsCurrencyChange = false;
				GetCurrencyRate();
				SetInitialFocus(ddlCountry);
			}
			catch 
			{
				txtCountry.Text = "";
			}
		}


		private void ddlLoadingPort_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				DataRow[] result=  dsLoadingPort.Tables[0].Select("CountryCode = '" + ddlLoadingPort.SelectedValue + "'");
				txtLoadingPortDes.Text = result[0]["CountryName"].ToString();
				SetInitialFocus(ddlLoadingPort);
			}
			catch
			{
				txtLoadingPortDes.Text = "";
			}
		}
		

		private void ddlCurrencyName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				if (IsCurrencyChange)
				{
					GetCurrencyRate();
					SetInitialFocus(ddlCurrencyName);
				}
				else
				{
					IsCurrencyChange = true;
				}
			
				//			
				//				DataRow[] result=  dsCountry.Tables[0].Select("Currencycode = '" + ddlCurrencyName.SelectedValue + "'"); 
				//				ddlCountry.SelectedValue = result[0]["CountryCode"].ToString();
				//				//SetInitialFocus(ddlCurrencyName);
			}
			catch
			{}
			SetInitialFocus(ddlCurrencyName);
		}

		public void txtChargeAmount_TextChanged(object sender, System.EventArgs e)
		{
			CalculateCharge();
		}

		public void ddlChargeCurrencyCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			CalculateCharge();
		}
		#endregion
		
		#region Grid Event
		private void gvCharge_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			lblError.Text = "";
			this.lblError.Text="";
			string cmd = e.CommandName; 
			 
			if(cmd=="EDIT_ITEM")
			{ 
				string ss =strScrollPosition;
				gvCharge.EditItemIndex=e.Item.ItemIndex;
				indCharge=(short)e.Item.ItemIndex;
				gvCharge.DataSource=dtCharge;
				gvCharge.DataBind();
			} 
			else if(cmd=="SAVE_ITEM")
			{
				
				com.common.util.msTextBox txtChargeAmount =  (com.common.util.msTextBox)e.Item.FindControl("txtChargeAmount"); 
				//				if (txtChargeAmount.Text =="" ) 
				//				{
				//					lblError.Text = "Charge amount must be >0 (or =0 to delete)";
				//					return;
				//				}
				//				else
				//				{ 
				CalculateCharge();
				DropDownList ddlChargeCurrencyCode = (DropDownList)e.Item.FindControl("ddlChargeCurrencyCode");
				TextBox txtExchange_Rate  =  (TextBox)e.Item.FindControl("txtExchange_Rate");
				TextBox txtValuation =  (TextBox)e.Item.FindControl("txtValuation"); 
				TextBox txtCharges_Code =  (TextBox)e.Item.FindControl("txtCharges_Code"); 
				txtChargeAmount =  (com.common.util.msTextBox)e.Item.FindControl("txtChargeAmount"); 
				if (txtExchange_Rate.Text == "")
				{
					lblError.Text = "Currency exchange rate does not exist or has expired";
					return;
				} 
				//					dtCharge.Rows[e.Item.ItemIndex]["Charges_Code"] = txtCharges_Code.Text;
				//					dtCharge.Rows[e.Item.ItemIndex]["Amount"]= txtChargeAmount.Text;
				//					dtCharge.Rows[e.Item.ItemIndex]["CurrencyCode"]= ddlChargeCurrencyCode.SelectedValue;
				//					dtCharge.Rows[e.Item.ItemIndex]["Valuation"]= txtValuation.Text;
				//					dtCharge.AcceptChanges();
				//					gvCharge.EditItemIndex = -1;
				//					gvCharge.DataSource= dtCharge;
				//					gvCharge.DataBind();

				try
				{
					DataSet ds = CustomJobCompilingDAL.SaveCustomJobCharge(strAppID,
						strEnterpriseID,
						strUserLoggin ,
						txtHouseAwbNumber.Text,
						dsAllResult.Tables[2].Rows[0]["CustomerID"].ToString(),
						dsAllResult.Tables[2].Rows[0]["JobEntryNo"].ToString(),
						dtCharge.Rows[e.Item.ItemIndex]["SeqNo"].ToString(),
						txtCharges_Code.Text,
						txtChargeAmount.Text,
						ddlChargeCurrencyCode.SelectedValue);

					//							dtCharge.Rows[e.Item.ItemIndex]["SeqNo"].ToString(),
					//							dtCharge.Rows[e.Item.ItemIndex]["Charges_Code"].ToString(),
					//							dtCharge.Rows[e.Item.ItemIndex]["Amount"].ToString(),
					//							dtCharge.Rows[e.Item.ItemIndex]["CurrencyCode"].ToString());
					if(ds !=null && ds.Tables[0].Rows.Count > 0)
					{
						if(Convert.ToInt32( ds.Tables[0].Rows[0]["ErrorCode"]) > 0)
						{
							lblError.Text = ds.Tables[0].Rows[0]["ErrorMessage"].ToString();
						}
						else
						{
							BindCharge();
							gvCharge.EditItemIndex = -1;
						}
					}
				}
				catch
				{
				}
			}

				//			}
			else if(cmd=="CANCEL_ITEM")
			{
				gvCharge.EditItemIndex = -1;
				gvCharge.DataSource= dtCharge;
				gvCharge.DataBind();
			}
		
		}


		private void gvMarksAndNo_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{ 
			lblError.Text = "";
			this.lblError.Text="";
			string cmd = e.CommandName; 
			GridCmd=cmd;
			if(cmd=="ADD_ITEM")
			{

				if (txtRemainingEntries.Text == "0")
				{
					return;
				} 
				TextBox txtCode =  (TextBox)e.Item.FindControl("txtCode");
				TextBox txtDescription =  (TextBox)e.Item.FindControl("txtDescription");    
				if(txtCode.Text.Trim() == "" )
				{	 
					lblError.Text="Code is required on update and insert";
					SetInitialFocus(txtCode);
					return; 
				} 
				if(txtDescription.Text.Trim() == "" )
				{ 
					lblError.Text="Description is required on update and insert";
					SetInitialFocus(txtCode);
					return; 
				} 
  

				DataSet ds = CustomJobCompilingDAL.SaveCustomJobMraksNos(strAppID,
					strEnterpriseID,
					strUserLoggin , 
					"0" ,
					txtHouseAwbNumber.Text,
					dsAllResult.Tables[2].Rows[0]["CustomerID"].ToString(),
					dsAllResult.Tables[2].Rows[0]["JobEntryNo"].ToString(),
					dsAllResult.Tables[0].Rows[0]["NextSeqNoMarksNos"].ToString(),
					txtCode.Text,
					txtDescription.Text);
				if(ds !=null && ds.Tables[0].Rows.Count > 0)
				{
					if(Convert.ToInt32( ds.Tables[0].Rows[0]["ErrorCode"]) > 0)
					{
						lblError.Text = ds.Tables[0].Rows[0]["ErrorMessage"].ToString();
					}
					else
					{
						BindMarkNose();
						lblError.Text="";  
						gvMarksAndNo.EditItemIndex = -1; 
					}
				} 
			}
			else if(cmd=="EDIT_ITEM")
			{
				gvMarksAndNo.EditItemIndex=e.Item.ItemIndex; 
				gvMarksAndNo.DataSource = dtMarksData;							
				gvMarksAndNo.DataBind(); 
			}
			else if(cmd=="DELETE_ITEM")
			{ 
				lblError.Text="";
				TextBox txtCode =  (TextBox)e.Item.FindControl("txtCode");
				TextBox txtDescription =  (TextBox)e.Item.FindControl("txtDescription"); 
				Label lblSeq =  (Label)e.Item.FindControl("lblSeq");  
				DataSet dsResult = CustomJobCompilingDAL.SaveCustomJobMraksNos(strAppID,
					strEnterpriseID,
					strUserLoggin , 
					"1" ,
					txtHouseAwbNumber.Text,
					dsAllResult.Tables[2].Rows[0]["CustomerID"].ToString(),
					dsAllResult.Tables[2].Rows[0]["JobEntryNo"].ToString(),
					lblSeq.Text,
					"",
					"");

				BindMarkNose();

				if(dsResult.Tables[0].Rows.Count > 0)
					if(!dsResult.Tables[0].Rows[0][0].ToString().Equals("0"))
						lblError.Text = dsResult.Tables[0].Rows[0][1].ToString();
			}
			else if(cmd=="SAVE_ITEM")
			{
				TextBox txtMarksNos_Code =  (TextBox)e.Item.FindControl("txtMarksNos_Code");
				TextBox txtMarkDescription =  (TextBox)e.Item.FindControl("txtMarkDescription"); 
				TextBox txtMarkSeq =  (TextBox)e.Item.FindControl("txtMarkSeq");  
				if(txtMarksNos_Code.Text.Trim() == "" )
				{								 
					lblError.Text="Code is required on update and insert";
					SetInitialFocus(txtMarksNos_Code);
					return; 
				}
				if(txtMarkDescription.Text.Trim() == "" )
				{								 
					lblError.Text="Description is required on update and insert";
					SetInitialFocus(txtMarkDescription);
					return; 
				}
				DataSet ds = CustomJobCompilingDAL.SaveCustomJobMraksNos(strAppID,
					strEnterpriseID,
					strUserLoggin , 
					"0" ,
					txtHouseAwbNumber.Text,
					dsAllResult.Tables[2].Rows[0]["CustomerID"].ToString(),
					dsAllResult.Tables[2].Rows[0]["JobEntryNo"].ToString(),
					txtMarkSeq.Text,
					txtMarksNos_Code.Text,
					txtMarkDescription.Text);

				if(ds !=null && ds.Tables[0].Rows.Count > 0)
				{
					if(Convert.ToInt32( ds.Tables[0].Rows[0]["ErrorCode"]) > 0)
					{
						lblError.Text = ds.Tables[0].Rows[0]["ErrorMessage"].ToString();
					}
					else
					{
						lblError.Text="";
						gvMarksAndNo.EditItemIndex = -1;
						BindMarkNose();
						gvMarksAndNo.DataSource= dtMarksData;
						gvMarksAndNo.DataBind(); 
						
					}
				}
				

			}
			else if(cmd=="CANCEL_ITEM")
			{
				gvMarksAndNo.EditItemIndex = -1;
				gvMarksAndNo.DataSource= dtMarksData;
				gvMarksAndNo.DataBind(); 
			} 
		} 


		private void gvCharge_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if((e.Item.ItemType == ListItemType.EditItem))
			{
				DataRowView dr = (DataRowView)e.Item.DataItem;
				DropDownList ddlChargeCurrencyCode = (DropDownList)e.Item.FindControl("ddlChargeCurrencyCode");//  (DropDownList)e.Item.FindControl("ddlChargeCurrencyCode");
				if (ddlChargeCurrencyCode != null)
				{
					ddlChargeCurrencyCode.SelectedIndexChanged += new System.EventHandler(this.ddlChargeCurrencyCode_SelectedIndexChanged);
					TextBox	 txtExchange_Rate =  (TextBox)e.Item.FindControl("txtExchange_Rate");
					txtExchange_Rate.TextChanged += new System.EventHandler(this.txtChargeAmount_TextChanged); 

					ddlChargeCurrencyCode.DataTextField="CurrencyCode";
					ddlChargeCurrencyCode.DataValueField="CurrencyCode";
					ddlChargeCurrencyCode.DataSource = dsCurrency;
					ddlChargeCurrencyCode.DataBind();   
					if (txtExchange_Rate.Text == "")
					{
						txtExchange_Rate.Text = txtRate.Text;
						ddlChargeCurrencyCode.SelectedValue = ddlCurrencyName.SelectedValue;
					}
					else
					{
						txtExchange_Rate.Text = dr["Exchange_Rate"].ToString();
						ddlChargeCurrencyCode.SelectedValue = dr["CurrencyCode"].ToString();
					}
					
				}
			} 
			if((e.Item.ItemType == ListItemType.EditItem))
			{
				string ss =strScrollPosition;
				com.common.util.msTextBox txtChargeAmount =  (com.common.util.msTextBox)e.Item.FindControl("txtChargeAmount"); 
				SetInitialFocus(txtChargeAmount);
			}			
		}


		private void gvMarksAndNo_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			txtRemainingEntries.Text = dsAllResult.Tables[0].Rows[0]["RemainingEntries"].ToString(); 
			if((e.Item.ItemType == ListItemType.Footer))
			{
				LinkButton btnAddItem = (LinkButton)e.Item.FindControl("btnAddItem");
				if (txtRemainingEntries.Text == "0")
				{
					btnAddItem.Visible = false;
				}
				else
				{
					btnAddItem.Visible = true;
				}
				
				if(GridCmd=="ADD_ITEM")
				{
					GridCmd=null;
					TextBox txtCode = (TextBox)e.Item.FindControl("txtCode");
					SetInitialFocus(txtCode);
				}
			}	
			else if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				LinkButton btnDeleteItem = (LinkButton)e.Item.FindControl("btnDeleteItem");
				LinkButton btnEditItem = (LinkButton)e.Item.FindControl("btnEditItem");
				
				DataRowView dr = (DataRowView)e.Item.DataItem;
				if(Convert.ToInt32(dr["SeqNo"]) >= StdMarksNosEntries)
				{
					btnDeleteItem.Visible = true;
					btnEditItem.Visible = true;
				}
				else
				{
					btnDeleteItem.Visible = false;
					btnEditItem.Visible = false;
				}
			}
			else if((e.Item.ItemType == ListItemType.EditItem))
			{
				string ss =strScrollPosition;
				TextBox txtMarksNos_Code =  (TextBox)e.Item.FindControl("txtMarksNos_Code"); 
				SetInitialFocus(txtMarksNos_Code);
			}	
		}
	
	
		#endregion Grid Event 

		#region "Method"
		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("SetScrollPosition();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}


		private void GetConfigMarksNos()
		{
			DataSet dsConfigMarksNos =   CustomJobCompilingDAL.GetConfigMarksNos(strAppID, strEnterpriseID);  
			if (dsConfigMarksNos != null)
			{
				for (int i = 0 ; i < dsConfigMarksNos.Tables[0].Rows.Count; i++)
				{
					if (dsConfigMarksNos.Tables[0].Rows[i]["key"].ToString() == "MaxMarksNosEntries")
					{
						MaxMarksNosEntries=Convert.ToInt16( dsConfigMarksNos.Tables[0].Rows[i]["value"].ToString()); 
					}
				}				 
			}
		}


		private void GetCurrencyRate()
		{	
			txtRate.Text=  "" ;
			if (txtRateDate.Text == "")
			{
				return;
			}				
			try 
			{		
				string[] tempDate; //DateTime.Parse(txtRateDate.Text).ToString("yyyy-MM-dd").ToString();
				string strDate = "";
				tempDate = txtRateDate.Text.Split('/');
				strDate = tempDate[2]+"-"+tempDate[1]+"-" + tempDate[0];
				DataSet dsExchangeRate =   CustomJobCompilingDAL.GetExchangeRate(strAppID, strEnterpriseID,ddlCurrencyName.SelectedValue, strDate );  
				if (dsExchangeRate != null)
				{	
					if (dsExchangeRate.Tables[0].Rows.Count > 0)
					{					    
						txtRate.Text=  dsExchangeRate.Tables[0].Rows[0]["exchange_rate"].ToString() ;
					}
				}
			}
			catch
			{
				
			}	
		}  


		private void BindData()
		{				  
			DataSet dsResult = CustomJobCompilingDAL.SearchCustomJobCompiling(strAppID,strEnterpriseID,strUserLoggin,txtHouseAwbNumber.Text,txtJobEntryNumber.Text);
			  
			System.Data.DataTable dtStatus = dsResult.Tables[0];
			System.Data.DataTable dtAllData = dsResult.Tables[1];
			dtMarksData = dsResult.Tables[2];
			dtCharge = dsResult.Tables[3];
			dsAllResult = dsResult;
			

			lblErrorMsg.Text="";
			lblError.Text = "";
			IsChange = true;
			if(dtStatus.Rows.Count>0 && dtStatus.Rows[0]["ErrorCode"].ToString() != "0")
			{
				lblErrorMsg.Text=dtStatus.Rows[0]["ErrorMessage"].ToString();
			}
			else
			{ 
				
				//lblErrorMsg.Text = ExistsCustoms_JobDetails.ToString();
				StdMarksNosEntries = Convert.ToInt16(dtStatus.Rows[0]["FirstEdSeqNoMarksNos"].ToString());
				btnExecQry.Enabled = false;
				dsAllResult = dsResult;
				btnSave.Enabled = true; 
				btnCharges.Enabled = true;
				//lblHeader.Text = "Marks & Nos.";
				btnEntryLines.Enabled = true;
				txtJobEntryNumber.Enabled = false;
				btnPrintDeclaration.Enabled = true;
				txtHouseAwbNumber.Enabled = false;
 
				txtJobEntryNumber.Text =dtAllData.Rows[0]["JobEntryNo"].ToString(); 
				txtHouseAwbNumber.Text =dtAllData.Rows[0]["HouseAWBNo"].ToString(); 
				txtMasterAwbnumber.Text =dtAllData.Rows[0]["MasterAWBNumber"].ToString();  
				txtLoadingPort.Text =dtAllData.Rows[0]["LoadingPortCity"].ToString(); 
				txtNumberOfPkgs.Text = dtAllData.Rows[0]["Tot_Pkgs"].ToString(); 
				//txtWeight.Text = Math.Round(Double.Parse(dtAllData.Rows[0]["Tot_Wt"].ToString())).ToString(); 
				

				if(dtAllData.Rows[0]["Tot_Wt"] != DBNull.Value)
				{					
					txtWeight.Text = Decimal.Parse(dtAllData.Rows[0]["Tot_Wt"].ToString()).ToString("N1");
				}
				if(dtAllData.Rows[0]["ChargeableWeight"] != DBNull.Value)
				{					
					txtChargeableWeight.Text = Decimal.Parse(dtAllData.Rows[0]["ChargeableWeight"].ToString()).ToString("N1");
				}
				txtRemark.Text = dtAllData.Rows[0]["Remarks"].ToString(); 

				txtHistory_RegisteredBy.Text=dtAllData.Rows[0]["History_RegisteredBy"].ToString();
				txtHistory_RegisteredDT.Text=(dtAllData.Rows[0]["History_RegisteredDT"] != DBNull.Value ? DateTime.Parse(dtAllData.Rows[0]["History_RegisteredDT"].ToString()).ToString("dd/MM/yyyy HH:mm") :"");
				txtHistory_CompiledBy.Text=dtAllData.Rows[0]["History_CompiledBy"].ToString();
				txtHistory_CompiledDT.Text=(dtAllData.Rows[0]["History_CompiledDT"] != DBNull.Value ? DateTime.Parse(dtAllData.Rows[0]["History_CompiledDT"].ToString()).ToString("dd/MM/yyyy HH:mm") :"");
				txtHistory_LastUpdatedBy.Text=dtAllData.Rows[0]["History_LastUpdatedBy"].ToString();
				txtHistory_LastUpdatedDT.Text=(dtAllData.Rows[0]["History_LastUpdatedDT"] != DBNull.Value ? DateTime.Parse(dtAllData.Rows[0]["History_LastUpdatedDT"].ToString()).ToString("dd/MM/yyyy HH:mm") :"");

				txtGoodsDescription.Text = dtAllData.Rows[0]["GoodsDescription"].ToString();
                //txtRateDate.Text =dtAllData.Rows[0]["CurrencyRateDate"].ToString();
                txtRateDate.Text = (dtAllData.Rows[0]["CurrencyRateDate"] != DBNull.Value ? DateTime.Parse(dtAllData.Rows[0]["CurrencyRateDate"].ToString()).ToString("dd/MM/yyyy") :"");
                //pongsakorn.s 02/07/2019
                string[] tempDate = txtRateDate.Text.Split('/');
                string strDate = tempDate[2] + "-" + tempDate[1] + "-" + tempDate[0];
                DataSet dsExchangeRate = CustomJobCompilingDAL.GetExchangeRate(strAppID, strEnterpriseID, ddlCurrencyName.SelectedValue, strDate);
                if (dsExchangeRate != null)
                {
                    if (dsExchangeRate.Tables[0].Rows.Count > 0)
                    {
                        txtRate.Text = dsExchangeRate.Tables[0].Rows[0]["exchange_rate"].ToString();
                    }
                }//
                //txtRate.Text =dtAllData.Rows[0]["Exchange_rate"].ToString();//coment 02/07/2019 pongsakorn.s 

                //Consignee 
                txtConsigneeId.Text =dtAllData.Rows[0]["CustomerID"].ToString(); 
				txtTaxCode.Text =dtAllData.Rows[0]["CustomerTaxCode"].ToString(); 
				txtConsigneeName.Text =dtAllData.Rows[0]["Consignee_name"].ToString(); 
				txtConsignee1.Text =dtAllData.Rows[0]["Consignee_address1"].ToString(); 
				txtConsignee2.Text =dtAllData.Rows[0]["Consignee_address2"].ToString(); 
				txtConsigneeZipcode.Text =dtAllData.Rows[0]["Consignee_zipcode"].ToString();
				txtConsigneeState.Text =dtAllData.Rows[0]["Consignee_state"].ToString();
				txtConsigneeTelephone.Text =dtAllData.Rows[0]["Consignee_telephone"].ToString();

				//Custom Formal Declaration
				txtDeclartionNumber.Text =dtAllData.Rows[0]["CustomsDeclarationNo"].ToString(); 
				txtDateOfAssessment.Text =(dtAllData.Rows[0]["DateOfAssessment"] != DBNull.Value ? DateTime.Parse(dtAllData.Rows[0]["DateOfAssessment"].ToString()).ToString("dd/MM/yyyy") :"");
				
				txtDeclartionFromPages.Text =dtAllData.Rows[0]["DeclarationFormPages"].ToString(); 
				string tempplate = "";
				for (int i = 0 ; i< currency_decimal ; i++)
				{
					tempplate = tempplate+"0";
				}
				if(dtAllData.Rows[0]["Duty_Amount"].ToString() != "")
				{
					txtDuty.Text =  Convert.ToDecimal(dtAllData.Rows[0]["Duty_Amount"]).ToString("##0." + tempplate);  //dtAllData.Rows[0]["Duty_Amount"].ToString(); 
				}
				if(dtAllData.Rows[0]["Tax_Amount"].ToString() != "")
				{
					txtTax.Text = Convert.ToDecimal(dtAllData.Rows[0]["Tax_Amount"]).ToString("##0." + tempplate);  //dtAllData.Rows[0]["Tax_Amount"].ToString();
				}
				if(dtAllData.Rows[0]["Excise_Amount"].ToString() != "")
				{
					txtExcise.Text = Convert.ToDecimal(dtAllData.Rows[0]["Excise_Amount"]).ToString("##0." + tempplate);  // dtAllData.Rows[0]["Excise_Amount"].ToString(); 
				}
				if(dtAllData.Rows[0]["EntryProcessingFee"].ToString() != "")
				{
					txtEPF.Text = Convert.ToDecimal(dtAllData.Rows[0]["EntryProcessingFee"]).ToString("##0." + tempplate); 
				}
				if(dtAllData.Rows[0]["Total_Amount"].ToString() != "")
				{
					txtTotal.Text = Convert.ToDecimal(dtAllData.Rows[0]["Total_Amount"]).ToString("##0." + tempplate);  //dtAllData.Rows[0]["Total_Amount"].ToString(); 
				}
				 
				txtMaximumEntries.Text = dtStatus.Rows[0]["MaximumEntries"].ToString(); 
				txtRemainingEntries.Text = dtStatus.Rows[0]["RemainingEntries"].ToString(); 
	 
				//Eport
				txtExporter1.Text =dtAllData.Rows[0]["ExporterName"].ToString(); 
				txtExporter2.Text =dtAllData.Rows[0]["ExporterAddress1"].ToString(); 
				txtExporter3.Text =dtAllData.Rows[0]["ExporterAddress2"].ToString(); 
 
				txtInfoReceivedDate.Text =(dtAllData.Rows[0]["InfoReceivedDate"] != DBNull.Value ? DateTime.Parse(dtAllData.Rows[0]["InfoReceivedDate"].ToString()).ToString("dd/MM/yyyy") :"");
				txtShipFlightNo.Text =dtAllData.Rows[0]["ShipFlightNo"].ToString(); 
				txtExpectedArrivalDate.Text = (dtAllData.Rows[0]["ExpectedArrivalDate"] != DBNull.Value ? DateTime.Parse(dtAllData.Rows[0]["ExpectedArrivalDate"].ToString()).ToString("dd/MM/yyyy") :"");
				txtActualGoodsArrivalDate.Text = (dtAllData.Rows[0]["ActualArrivalDate"] != DBNull.Value ? DateTime.Parse(dtAllData.Rows[0]["ActualArrivalDate"].ToString()).ToString("dd/MM/yyyy") :"");
				txtFolioNo.Text =dtAllData.Rows[0]["FolioNumber"].ToString();  
   
				ddlDischargePort.SelectedValue = dtAllData.Rows[0]["DischargePort"].ToString();
				ddlBondedWarehouse.SelectedValue = dtAllData.Rows[0]["BondedWarehouse"].ToString();
				ddlEntryType.SelectedValue = dtAllData.Rows[0]["EntryType"].ToString();
				if(ddlEntryType.SelectedValue.Equals("F"))
				{
					txtDeclartionFromPages.Enabled = true;
					txtDeclartionNumber.Enabled = true;
					txtDateOfAssessment.Enabled = true;
					AssessmentAmount.Enabled = true;
				}
				else
				{
					txtDeclartionFromPages.Enabled = false;
					txtDeclartionNumber.Enabled = false;
					txtDateOfAssessment.Enabled = false;
					AssessmentAmount.Enabled = false;
				}
				ddlCustomsAgent.SelectedValue = dtAllData.Rows[0]["CustomsAgent"].ToString();
				ddlModelOfDeclaration.SelectedValue = dtAllData.Rows[0]["ModelOfDeclaration"].ToString();
				ddlGeneralProcedureCode.SelectedValue = dtAllData.Rows[0]["GeneralProcedureCode"].ToString();
				ddlCustomsProcedureCode.SelectedValue = dtAllData.Rows[0]["CustomsProcedureCode"].ToString();
				ddlAdditional.SelectedValue = dtAllData.Rows[0]["CustomsAddlCode"].ToString();
				ddlDeliveryTerms.SelectedValue = dtAllData.Rows[0]["DeliveryTerms"].ToString();
				ddlModeOfTransport.SelectedValue = dtAllData.Rows[0]["TransportMode"].ToString();
				ddlProfile.SelectedValue = dtAllData.Rows[0]["AssignedProfile"].ToString();
				strEntryType = dtAllData.Rows[0]["EntryType"].ToString();
				txtBondedWarehouse.Text = dtAllData.Rows[0]["BondedWarehouse_Desc"].ToString();
				txtEntryType.Text = dtAllData.Rows[0]["EntryType_Desc"].ToString();
				txtCustomsAgent.Text = dtAllData.Rows[0]["CustomsAgent_Desc"].ToString();
				txtModelOfDeclaration.Text = dtAllData.Rows[0]["ModelOfDeclaration_Desc"].ToString();
				txtGeneralProcedureCode.Text = dtAllData.Rows[0]["GeneralProcedureCode_Desc"].ToString();
				txtCustomsProcedureCode.Text = dtAllData.Rows[0]["CustomsProcedureCode_Desc"].ToString();
				txtAdditional.Text = dtAllData.Rows[0]["CustomsAddlCode_Desc"].ToString();
				txtDeliveryTerms.Text = dtAllData.Rows[0]["DeliveryTerms_Desc"].ToString();
				txtModeOfTransport.Text = dtAllData.Rows[0]["TransportMode_Desc"].ToString();
				//				ddlCurrencyName 
 			
				ddlCurrencyName.SelectedValue = dtAllData.Rows[0]["CurrencyCode"].ToString();  				
				ddlCountry.SelectedValue = dtAllData.Rows[0]["ExporterCountry"].ToString();
				ddlLoadingPort.SelectedValue = dtAllData.Rows[0]["ExporterCountry"].ToString();

				DataRow[] result=  dsCountry.Tables[0].Select("CountryCode = '" + ddlCountry.SelectedValue + "'");
				txtCountry.Text = result[0]["CountryName"].ToString();
			 
				result=  dsLoadingPort.Tables[0].Select("CountryCode = '" + ddlLoadingPort.SelectedValue + "'");
				txtLoadingPortDes.Text = result[0]["CountryName"].ToString();
				EntryTypeBegin = dtAllData.Rows[0]["EntryType"].ToString();
				txtInvoiceStatus.Text= dtAllData.Rows[0]["InvoiceStatus"].ToString();
				cbkDoorToDoor.Checked=false;

				if(!dtAllData.Rows[0]["AssessmentAmount"].ToString().Trim().Equals(""))
					AssessmentAmount.Text = Convert.ToDecimal(dtAllData.Rows[0]["AssessmentAmount"].ToString()).ToString("N" + currency_decimal.ToString());

				lblOriginCity.Visible=false;
				//lblOriginCityReq.Visible=true;
				ddlOriginCity.Visible=false;
				if(dtAllData.Rows[0]["ExpressFlag"] != DBNull.Value && dtAllData.Rows[0]["ExpressFlag"].ToString().Trim() =="1")
				{
					cbkDoorToDoor.Checked=true;
					lblOriginCity.Visible=true;
					//lblOriginCityReq.Visible=true;
					ddlOriginCity.Visible=true;
					try
					{
						ddlOriginCity.SelectedValue=dtAllData.Rows[0]["ExpressCity"].ToString();
					}
					catch
					{
						ddlOriginCity.SelectedIndex=0;
					}
					
				}

				IgnoreMAWBValidation.Checked=false;
				if(dtAllData.Rows[0]["IgnoreMAWBValidation"] != DBNull.Value && dtAllData.Rows[0]["IgnoreMAWBValidation"].ToString().Equals("1"))
				{
					IgnoreMAWBValidation.Checked=true;
				}

				ExistsCustoms_JobDetails =  GetExistsCustoms_JobDetails();
				if (dsResult.Tables[0].Rows[0]["EnableOtherButtons"].ToString() == "0")
				{
					btnCharges.Enabled = false;
					btnEntryLines.Enabled = false;
				}
				else
				{
					btnCharges.Enabled = true;
					btnEntryLines.Enabled = true;					
				}
				btnPrintDeclaration.Enabled = false;
				if(EntryLinesExist>0)
				{
					btnPrintDeclaration.Enabled = true;
				}
				 
				
			}
		}


		private void BindMarkNose()
		{				  
			DataSet dsResult = CustomJobCompilingDAL.SearchCustomJobCompiling(strAppID,strEnterpriseID,strUserLoggin,txtHouseAwbNumber.Text,txtJobEntryNumber.Text);
			  
			System.Data.DataTable dtStatus = dsResult.Tables[0];
			System.Data.DataTable dtAllData = dsResult.Tables[1];
			dtMarksData = dsResult.Tables[2];
			dtCharge = dsResult.Tables[3];
			dsAllResult = dsResult;
			

			lblErrorMsg.Text="";
			lblError.Text = "";
			IsChange = true;
			if(dtStatus.Rows.Count>0 && dtStatus.Rows[0]["ErrorCode"].ToString() != "0")
			{
				lblErrorMsg.Text=dtStatus.Rows[0]["ErrorMessage"].ToString();
			}
			else
			{  
				txtMaximumEntries.Text = dtStatus.Rows[0]["MaximumEntries"].ToString(); 
				txtRemainingEntries.Text = dtStatus.Rows[0]["RemainingEntries"].ToString(); 
	  
				gvMarksAndNo.DataSource=dsResult.Tables[2];
				gvMarksAndNo.DataBind();
				dtMarksData=dsResult.Tables[2];
				//nextSeq = (int)dsResult.Tables[2].Compute("MAX(SeqNo)","1=1");

				lblMaximumEntries.Visible = true;
				lblRemainingEntries.Visible = true;
				txtMaximumEntries.Visible = true;
				txtRemainingEntries.Visible = true;
				gvMarksAndNo.EditItemIndex = -1;
				
				gvCharge.DataSource=null;
				gvCharge.DataBind();
				gvCharge.EditItemIndex = -1;
				if (dsResult.Tables[0].Rows[0]["EnableOtherButtons"].ToString() == "0")
				{
					btnCharges.Enabled = false;
					btnEntryLines.Enabled = false;
					btnPrintDeclaration.Enabled = false;
				}
				else
				{
					btnCharges.Enabled = true;
					btnEntryLines.Enabled = true;
					btnPrintDeclaration.Enabled = true;
				}
			}
		}


		private void BindCharge()
		{				  
			DataSet dsResult = CustomJobCompilingDAL.SearchCustomJobCompiling(strAppID,strEnterpriseID,strUserLoggin,txtHouseAwbNumber.Text,txtJobEntryNumber.Text);
			  
			System.Data.DataTable dtStatus = dsResult.Tables[0];
			System.Data.DataTable dtAllData = dsResult.Tables[1];
			dtMarksData = dsResult.Tables[2];
			dtCharge = dsResult.Tables[3];
			dsAllResult = dsResult;
			

			lblErrorMsg.Text="";
			lblError.Text = "";
			IsChange = true;
			if(dtStatus.Rows.Count>0 && dtStatus.Rows[0]["ErrorCode"].ToString() != "0")
			{
				lblErrorMsg.Text=dtStatus.Rows[0]["ErrorMessage"].ToString();
			}
			else
			{   
				gvCharge.EditItemIndex = -1;
				gvCharge.DataSource=dsResult.Tables[3];
				gvCharge.DataBind();
				 
			}
		}


		private void CleareScreen()	
		{
			//etInitialFocus(btnExecQry);
			lblError.Text = "";
			lblErrorMsg.Text = "";
			btnExecQry.Enabled = true;
			btnSave.Enabled = false;
			btnEntryLines.Enabled = true;
			btnCharges.Enabled = false;
			txtJobEntryNumber.Enabled = true;
			txtHouseAwbNumber.Enabled = true;
			btnEntryLines.Enabled = false;
			btnPrintDeclaration.Enabled = false;
			lblMaximumEntries.Visible = true;
			lblRemainingEntries.Visible = true;
			txtMaximumEntries.Visible = true;
			txtRemainingEntries.Visible = true;
			btnCharges.Text = "Charges";
			lblHeader.Text = "Marks & Nos.";
			txtJobEntryNumber.Text =""; 
			txtHouseAwbNumber.Text =""; 
			txtMasterAwbnumber.Text ="";  
			txtLoadingPort.Text =""; 
			txtNumberOfPkgs.Text = ""; 
			txtWeight.Text = ""; 
			txtChargeableWeight.Text="";
			txtRemark.Text = "";
			txtHistory_RegisteredBy.Text="";
			txtHistory_RegisteredDT.Text="";
			txtHistory_CompiledBy.Text="";
			txtHistory_CompiledDT.Text="";
			txtHistory_LastUpdatedBy.Text="";
			txtHistory_LastUpdatedDT.Text="";

			txtGoodsDescription.Text = ""; 
			txtRateDate.Text = ""; 
			txtRate.Text = ""; 

			//Consignee 
			txtConsigneeId.Text =""; 
			txtTaxCode.Text =""; 
			txtConsigneeName.Text ="";  
			txtConsignee1.Text =""; 
			txtConsignee2.Text =""; 
			txtConsigneeZipcode.Text ="";  
			txtConsigneeState.Text =""; 
			txtConsigneeTelephone.Text =""; 

			//Custom Formal Declaration
			txtDeclartionNumber.Text =""; 
			txtDateOfAssessment.Text =""; 
			txtDeclartionFromPages.Text =""; 
			txtDuty.Text =""; 
			txtTax.Text =""; 
			txtExcise.Text =""; 
			txtEPF.Text ="";  
			txtTotal.Text =""; 
			//txtCosigneeTelephone.Text =dtAllData.Rows[0]["Consignee_telephone"].ToString(); 
			txtMaximumEntries.Text = ""; 
			txtRemainingEntries.Text = ""; 
	 
			//Eport
			txtExporter1.Text =""; 
			txtExporter2.Text =""; 
			txtExporter3.Text =""; 

			txtInfoReceivedDate.Text =""; 
			txtShipFlightNo.Text =""; 
			txtExpectedArrivalDate.Text =""; 
			txtActualGoodsArrivalDate.Text =""; 
			txtFolioNo.Text =""; 

			txtInvoiceStatus.Text="";

			//Set DropDownList
			ddlDischargePort.SelectedIndex = 0;
			ddlBondedWarehouse.SelectedIndex = 0;
			ddlEntryType.SelectedIndex = 0;
			ddlCustomsAgent.SelectedIndex = 0;
			ddlModelOfDeclaration.SelectedIndex = 0;
			ddlGeneralProcedureCode.SelectedIndex = 0;
			ddlCustomsProcedureCode.SelectedIndex = 0;
			ddlAdditional.SelectedIndex = 0;
			ddlDeliveryTerms.SelectedIndex = 0;
			ddlModeOfTransport.SelectedIndex = 0;
			ddlProfile.SelectedIndex = 0;
 
			txtBondedWarehouse.Text = "";
			txtEntryType.Text = "";
			EntryTypeBegin="";
			txtCustomsAgent.Text ="";
			txtModelOfDeclaration.Text = "";
			txtGeneralProcedureCode.Text = "";
			txtCustomsProcedureCode.Text = "";
			txtAdditional.Text = "";
			txtDeliveryTerms.Text = "";
			txtModeOfTransport.Text = "";
			EntryLinesExist=0;
			//				ddlCurrencyName 
			AssessmentAmount.Text = string.Empty;

			lblOriginCity.Visible=false;
			//lblOriginCityReq.Visible=true;
			ddlOriginCity.Visible=false;
			ddlOriginCity.SelectedIndex=0;

			cbkDoorToDoor.Checked=false;
			lblError.Text =  "";
			ddlCurrencyName.SelectedIndex = 0; 
			ddlCountry.SelectedIndex = 0;
			ddlLoadingPort.SelectedIndex = 0;
 
			gvMarksAndNo.DataSource=null;
			gvMarksAndNo.DataBind(); 

			gvCharge.DataSource=null;
			gvCharge.DataBind();

			IgnoreMAWBValidation.Checked=false;
			SetInitialFocus(txtJobEntryNumber);

		}


		private void SetDropDownListDisplayValue_Old( string ConType, DropDownList ddl, string defaulVal )
		{		 
			DataSet dsDropDownList = new DataSet(); 
			try
			{
				dsDropDownList = CustomJobCompilingDAL.GetDropDownListDisplayValue(strAppID ,strEnterpriseID,ConType); 
				ddl.DataSource= dsDropDownList;
				ddl.DataTextField="DropDownListDisplayValue";
				ddl.DataValueField="CodedValue";
				ddl.DataBind();  
				
				ddl.SelectedValue = defaulVal; 
				switch(ConType)
				{
					case "DischargePort": dsDischargePort = dsDropDownList;
						break;
					case "BondedWarehouses": dsBondedWarehouses = dsDropDownList; 
						break;
					case "EntryType": dsEntryType = dsDropDownList;
						break;
					case "CustomsAgents": dsCustomsAgents = dsDropDownList;
						break;
					case "ModelOfDeclaration": dsModelOfDeclaration = dsDropDownList;
						break;
					case "GeneralProcedureCode": dsGeneralProcedureCode = dsDropDownList;
						break;
					case "CustomsProcedureCode": dsCustomsProcedureCode = dsDropDownList;
						break;
					case "CPCAdditional": dsCPCAdditional = dsDropDownList;
						break;
					case "Incoterms": dsDeliveryTerms = dsDropDownList;
						break;
					case "TransportMode": dsModeOfTransport = dsDropDownList;
						break;
					default: break;
				}

			}
			catch(Exception ex)
			{
				if (dsDropDownList.Tables[0].Rows.Count > 0) 
				{
					ddl.SelectedIndex = 0; 	 
				}
			}

		}


		private void SetDropDownListDisplayValue(string ConType , DropDownList ddl)
		{		 
			DataSet dsDropDownList = new DataSet(); 
			try
			{
				dsDropDownList = CustomJobCompilingDAL.GetDropDownListDefaultDisplayValue(strAppID ,strEnterpriseID,ConType); 
				ddl.DataSource= dsDropDownList;
				ddl.DataTextField="DropDownListDisplayValue";
				ddl.DataValueField="CodedValue";
				ddl.DataBind(); 
				switch(ConType)
				{
					case "DischargePort": dsDischargePort = dsDropDownList;
						break;
					case "BondedWarehouses": dsBondedWarehouses = dsDropDownList; 
						break;
					case "EntryType": dsEntryType = dsDropDownList;
						break;
					case "CustomsAgents": dsCustomsAgents = dsDropDownList;
						break;
					case "ModelOfDeclaration": dsModelOfDeclaration = dsDropDownList;
						break;
					case "GeneralProcedureCode": dsGeneralProcedureCode = dsDropDownList;
						break;
					case "CustomsProcedureCode": dsCustomsProcedureCode = dsDropDownList;
						break;
					case "CPCAdditional": dsCPCAdditional = dsDropDownList;
						break;
					case "Incoterms": dsDeliveryTerms = dsDropDownList;
						break;
					case "TransportMode": dsModeOfTransport = dsDropDownList;
						break;
					case "ExpressCities": dsExpressCities = dsDropDownList;
						break;
					default: break;
				}

			}
			catch(Exception ex)
			{ 
			}

		}


		private bool ValidateData()
		{
			if(txtHouseAwbNumber.Text.Trim() == "")
			{
				lblErrorMsg.Text="House AWB Number is required";
				SetInitialFocus(txtHouseAwbNumber);
				return false;
			}

			if(txtMasterAwbnumber.Text.Trim() == "")
			{
				lblErrorMsg.Text="Master AWB Number is required";
				SetInitialFocus(txtMasterAwbnumber);
				return false;
			}

			if(ddlDischargePort.SelectedValue == "")
			{
				lblErrorMsg.Text="Discharge Port is required";
				SetInitialFocus(ddlDischargePort);
				return false;
			}

			if(ddlBondedWarehouse.SelectedValue == "")
			{
				lblErrorMsg.Text="Bonded Warehouse is required";
				SetInitialFocus(ddlBondedWarehouse);
				return false;
			}

			if(ddlDeliveryTerms.SelectedValue == "")
			{
				lblErrorMsg.Text="Delivery terms are required";
				SetInitialFocus(ddlDeliveryTerms);
				return false;
			}

			if(ddlCustomsAgent.SelectedValue == "")
			{
				lblErrorMsg.Text="Customs agent is required";
				SetInitialFocus(ddlCustomsAgent);
				return false;
			}

			if(txtNumberOfPkgs.Text.Trim() == "")
			{
				lblErrorMsg.Text="Number Of Pkgs is required";
				SetInitialFocus(txtNumberOfPkgs);
				return false;
			}

			if(txtGoodsDescription.Text.Trim() == "")
			{
				lblErrorMsg.Text="Goods description is required";
				SetInitialFocus(txtGoodsDescription);
				return false;
			}

			if(txtExporter1.Text.Trim() == "")
			{
				lblErrorMsg.Text="Exporter name is required";
				SetInitialFocus(txtExporter1);
				return false;
			}

			if(txtExporter2.Text.Trim() == "")
			{
				lblErrorMsg.Text="Exporter address is required";
				SetInitialFocus(txtExporter2);
				return false;
			}

			if(ddlModelOfDeclaration.SelectedValue == "")
			{
				lblErrorMsg.Text="Model of declaration is required";
				SetInitialFocus(ddlModelOfDeclaration);
				return false;
			}

			if(ddlCustomsProcedureCode.SelectedValue == "")
			{
				lblErrorMsg.Text="Customs procedure code is required";
				SetInitialFocus(ddlCustomsProcedureCode);
				return false;
			}

			if(txtInfoReceivedDate.Text.Trim() == "")
			{
				lblErrorMsg.Text="Info received date is required";
				SetInitialFocus(txtInfoReceivedDate);
				return false;
			}

			if(txtShipFlightNo.Text.Trim() == "")
			{
				lblErrorMsg.Text="Ship/flight number is required";
				SetInitialFocus(txtShipFlightNo);
				return false;
			}

			if(txtExpectedArrivalDate.Text.Trim() == "")
			{
				lblErrorMsg.Text="Expected arrival date is required";
				SetInitialFocus(txtExpectedArrivalDate);
				return false;
			}
			if(ddlModeOfTransport.SelectedValue == "")
			{
				lblErrorMsg.Text="Transport mode is required";
				SetInitialFocus(ddlModeOfTransport);
				return false;
			}

			if(ddlProfile.SelectedValue == "")
			{
				lblErrorMsg.Text="Assigned profile is required";
				SetInitialFocus(ddlProfile);
				return false;
			}
			if(txtRate.Text.Trim() == "")
			{
				lblErrorMsg.Text="Currency exchange rate does not exist or has expired";
				SetInitialFocus(ddlProfile);
				return false;
			}
			
			if(txtNumberOfPkgs.Text.Trim() == "")
			{
				lblErrorMsg.Text="Package quantity must be greater than zero";
				SetInitialFocus(txtNumberOfPkgs);
				return false;
			}
			if(txtNumberOfPkgs.Text.Trim() == "0")
			{
				lblErrorMsg.Text="Package quantity must be greater than zero";
				SetInitialFocus(txtNumberOfPkgs);
				return false;
			}

			if(txtWeight.Text.Trim() == "")
			{
				lblErrorMsg.Text="Total weight is required";
				SetInitialFocus(txtWeight);
				return false;
			}
			//			if (Convert.ToDecimal(txtWeight.Text) < 1 ) 
			//			{
			//				lblErrorMsg.Text="Total weight must be greater than zero";
			//				SetInitialFocus(txtWeight);
			//				return false;
			//			}
			//
			//			if(cbkDoorToDoor.Checked && ddlOriginCity.SelectedValue=="")
			//			{
			//				lblErrorMsg.Text="Origin city is required for Express door-to-door customs jobs";
			//				SetInitialFocus(ddlOriginCity);
			//				return false;
			//			}

			return true;
		}


		private void CalculateCharge()
		{
			DropDownList ddlChargeCurrencyCode  =  (DropDownList)gvCharge.Items [indCharge].FindControl("ddlChargeCurrencyCode");
			TextBox txtExchange_Rate  =  (TextBox)gvCharge.Items [indCharge].FindControl("txtExchange_Rate");
			TextBox txtValuation =  (TextBox)gvCharge.Items[indCharge].FindControl("txtValuation"); 
			com.common.util.msTextBox txtChargeAmount =  (com.common.util.msTextBox)gvCharge.Items[indCharge].FindControl("txtChargeAmount");  

			string[] tempDate; //DateTime.Parse(txtRateDate.Text).ToString("yyyy-MM-dd").ToString();
			string strDate = "";
			tempDate = txtRateDate.Text.Split('/');
			strDate = tempDate[2]+"-"+tempDate[1]+"-" + tempDate[0];
			DataSet dsExchangeRate =   CustomJobCompilingDAL.GetExchangeRate(strAppID, strEnterpriseID, ddlChargeCurrencyCode.SelectedValue, strDate );  
			if (dsExchangeRate != null)
			{
				try
				{		
					txtExchange_Rate.Text = "";
					txtValuation.Text = "";

					if (dsExchangeRate.Tables[0].Rows.Count > 0)
					{
						txtExchange_Rate.Text=  dsExchangeRate.Tables[0].Rows[0]["exchange_rate"].ToString() ;
						if (txtChargeAmount.Text.Trim() != "")
						{
							Decimal rateEx ;
							if (txtExchange_Rate.Text == "")
							{
								rateEx = 1;
							}
							else
							{
								rateEx = Convert.ToDecimal(txtExchange_Rate.Text);
							}
							Decimal Valu = Convert.ToDecimal(txtChargeAmount.Text) / rateEx;
							txtValuation.Text = Valu.ToString("#.##");
							if (txtValuation.Text == "")
							{
								txtValuation.Text = "0.00";
							}
						}
					}
				}
				catch
				{
					txtValuation.Text = "0.00";
				}
			}
		}


		protected void OnEdit_Charge(object sender, DataGridCommandEventArgs e)
		{		 
			int rowIndex = e.Item.ItemIndex;
			Index = rowIndex;
			try
			{
				//				if(gvCharge.EditItemIndex >0)
				//				{
				//					TextBox txtSeqNo = (TextBox)gvCharge.Items[rowIndex].FindControl("txtSeqNo");				
				//					if(txtSeqNo !=null && txtSeqNo.Text=="")	
				//					{
				//						m_strSeq = txtSeqNo.Text;
				//						dsCharge.Tables[3].Rows.RemoveAt(gvCharge.EditItemIndex);
				//					}
				//
				//					//				TextBox txtIMEI = (TextBox)dgPhoneIMEI.Items[rowIndex].FindControl("txtIMEIGrid");				
				//					//				if(txtIMEI !=null && txtIMEI.Text=="")	
				//					//				{
				//					//					m_strIMEI = txtIMEI.Text;
				//					//					dsCharge.Tables[3].Rows.RemoveAt(dgPhoneIMEI.EditItemIndex);
				//					//				} 
				//				} 
			}
			catch(Exception ex)
			{
				lblError.Text = ex.Message;
			}
			
		}
		

		private System.Data.DataSet GetDropDownListDisplayValue(string strAppID,string strEnterpriseID, string ConType)
		{				  
			DataSet dsResult = CustomJobCompilingDAL.GetDropDownListDisplayValue(strAppID ,strEnterpriseID,ConType);
			return dsResult; 
		}


		#endregion		

		private void ddlProfile_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			SetInitialFocus(ddlProfile);
		}

		public int GetDigitOfEnterpriseConfiguration()
		{
			DataSet dsEprise = SysDataManager1.GetEnterpriseProfile(utility.GetAppID(),utility.GetEnterpriseID());
			DataRow dr = dsEprise.Tables[0].Rows[0];
			
			int  number_digit = 3;
			if(dr["currency_decimal"].ToString()!="")
			{
				number_digit = Convert.ToInt32(dr["currency_decimal"].ToString());
			}

			return number_digit;
		}

		public int GetExistsCustoms_JobDetails()
		{
			int intResult = 0;
			try
			{
				DataSet ds  =   CustomJobCompilingDAL.GetExistsCustoms_JobDetails(strAppID, strEnterpriseID,txtHouseAwbNumber.Text , dsAllResult.Tables[2].Rows[0]["CustomerID"].ToString(), txtJobEntryNumber.Text );  
				if (ds != null)
				{	
					if (ds.Tables[0].Rows.Count > 0)
					{				 
						intResult = Convert.ToInt16(ds.Tables[0].Rows[0]["ChargesExist"].ToString()) + Convert.ToInt16(ds.Tables[0].Rows[0]["EntryLinesExist"].ToString());
						EntryLinesExist = Convert.ToInt32(ds.Tables[0].Rows[0]["EntryLinesExist"].ToString());
					}
				}
			}
			catch (Exception ex)
			{
				//lblErrorMsg.Text = ex.Message;
			}

			return intResult;
		}

		private void btnPrintDeclaration_Click(object sender, System.EventArgs e)
		{
			DataSet dsCustomDeclaration = CustomJobCompilingDAL.GetViewCustomsDeclarationForReport(utility.GetAppID(),utility.GetEnterpriseID(), txtJobEntryNumber.Text);
			
			if(dsCustomDeclaration.Tables[0].Rows.Count > 0)
			{
				String strUrl = null;
				strUrl = "ReportViewer1.aspx";
				String reportTemplate = "CustomsDeclarationForm";
				Session["REPORT_TEMPLATE"] = reportTemplate;
				Session["FORMID"] = "CustomsDeclarationForm";
				Session["SESSION_DS_CUSTOMS_DECLARATION"] = dsCustomDeclaration;
				Session["FormatType"] = ".pdf";
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindowReport.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
		}

		private void btnExecQryHidden_Click(object sender, System.EventArgs e)
		{
			btnExecQry_Click(null,null);
		}

		private void cbkDoorToDoor_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbkDoorToDoor.Checked)
			{
				lblOriginCity.Visible=true;
				//lblOriginCityReq.Visible=true;
				ddlOriginCity.Visible=true;
				if(ddlOriginCity.Items.Count>0)
				{
					ddlOriginCity.SelectedIndex=0;
				}
			}
			else
			{
				lblOriginCity.Visible=false;
				//lblOriginCityReq.Visible=true;
				ddlOriginCity.Visible=false;
			}
		}
        
    }
}
