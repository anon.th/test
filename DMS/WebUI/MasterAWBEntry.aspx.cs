using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.applicationpages;
using TIESClasses;
using TIESDAL;
using com.ties;
using com.ties.DAL;
using com.ties.classes;
using com.common.util;
using System.Text;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for MasterAWBEntry.
	/// </summary>
	public class MasterAWBEntry : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnPrintConsNote;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.Button Button2;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.DropDownList ddlCustomsOffice;
		protected System.Web.UI.WebControls.TextBox txtCustomsOffice;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.TextBox txtAWBNumber;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.TextBox Textbox2;
		protected System.Web.UI.WebControls.TextBox Textbox3;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.TextBox Textbox1;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label15;
		protected com.common.util.msTextBox txtDateFrom;
		protected System.Web.UI.WebControls.TextBox txtCity;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist2;
		protected System.Web.UI.WebControls.TextBox Textbox16;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label17;
		protected com.common.util.msTextBox Mstextbox1;
		protected com.common.util.msTextBox Mstextbox2;
		protected System.Web.UI.WebControls.TextBox Textbox4;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist1;
		protected System.Web.UI.WebControls.TextBox Textbox5;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist3;
		protected System.Web.UI.WebControls.TextBox Textbox6;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.Label Label27;
		protected System.Web.UI.WebControls.TextBox Textbox8;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.TextBox Textbox7;
		protected System.Web.UI.WebControls.Label Label31;
		protected System.Web.UI.WebControls.Label Label28;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist4;
		protected System.Web.UI.WebControls.TextBox Textbox9;
		protected System.Web.UI.WebControls.Label lblHCSupportedbyEnterprise;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label Label32;
		protected System.Web.UI.WebControls.TextBox Textbox12;
		protected System.Web.UI.WebControls.TextBox Textbox14;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.TextBox Textbox10;
		protected System.Web.UI.WebControls.Label Label33;
		protected System.Web.UI.WebControls.TextBox Textbox13;
		protected System.Web.UI.WebControls.TextBox Textbox15;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.TextBox Textbox11;
		protected System.Web.UI.WebControls.Label Label34;
		protected System.Web.UI.WebControls.DataGrid PackageDetails;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			DataTable dt = new DataTable();
			dt.Columns.Add("Number");
			dt.Columns.Add("Exporter");
			dt.Columns.Add("Consignee");
			dt.Columns.Add("Qty");
			dt.Columns.Add("No");
			
//			DataRow dr = dt.NewRow();
//			dt.Rows.Add(dr);
			dt.AcceptChanges();

			PackageDetails.DataSource=dt;
			PackageDetails.DataBind();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnPrintConsNote.Click += new System.EventHandler(this.btnPrintConsNote_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.Button1.Click += new System.EventHandler(this.Button1_Click);
			this.Button2.Click += new System.EventHandler(this.Button2_Click);
			this.PackageDetails.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.PackageDetails_ItemCommand);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void clearscreen()
		{
			lblError.Text="";
			ddlCustomsOffice.Items.Clear();
			txtCustomsOffice.Text ="";
			Dropdownlist4.Items.Clear();
			Textbox9.Text="";
			txtAWBNumber.Text="";
			Textbox1.Text="";

			txtDateFrom.Text="";
			txtCity.Text="";
			Dropdownlist2.Items.Clear();
			Textbox16.Text="";

			Mstextbox1.Text="";
			Mstextbox2.Text="";
			Textbox4.Text="";
			Dropdownlist1.Items.Clear();
			Textbox5.Text="";

			Dropdownlist3.Items.Clear();
			Textbox6.Text="";

			Textbox7.Text="";
			Textbox8.Text="";
			Textbox10.Text="";
			Textbox11.Text="";
			Textbox12.Text="";
			Textbox13.Text="";
			Textbox14.Text="";
			Textbox15.Text="";

			Textbox2.Text="";
			Textbox3.Text="";


		}

		private void btnPrintConsNote_Click(object sender, System.EventArgs e)
		{
			btnExecQry.Enabled=false;
			btnPrintConsNote.Enabled=false;
			Button1.Enabled=false;
			Button2.Enabled=false;
			btnSave.Enabled=true;

			ddlCustomsOffice.Items.Clear();
			ddlCustomsOffice.Items.Add(new ListItem("JAS",""));

			txtCustomsOffice.Text = "CUSTOMS OFFICE � 6 MILE";

			Dropdownlist4.Items.Clear();
			Dropdownlist4.Items.Add(new ListItem("4",""));
			Textbox9.Text="AIR TRANSPORT";

			txtAWBNumber.Text="231-2311123";
			Textbox1.Text="PX003";

			txtDateFrom.Text="23/05/2014";
			txtCity.Text="BNE";
			Dropdownlist2.Items.Add(new ListItem("AU",""));
			Textbox16.Text="AUSTRALIA";

			Mstextbox1.Text="23/05/2014";
			Mstextbox2.Text="";
			Textbox4.Text="POM";
			Dropdownlist1.Items.Add(new ListItem("PG",""));
			Textbox5.Text="PAPUA NEW GUINEA";

			Dropdownlist3.Items.Add(new ListItem("ANG",""));
			Textbox6.Text="AIR NIUGINI";

			Textbox7.Text="25";
			Textbox8.Text="2";

			Textbox10.Text="25";
			Textbox11.Text="2";
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			clearscreen();
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			btnExecQry.Enabled=false;
			btnPrintConsNote.Enabled=false;
			btnSave.Enabled=true;
			Button1.Enabled=true;
			Button2.Enabled=false;

			lblError.Text="Master AWB saved.";
			Textbox2.Text="DCAMPS";
			Textbox3.Text="23/05/2014 09:15";

		}

		private void Button1_Click(object sender, System.EventArgs e)
		{
			btnExecQry.Enabled=false;
			btnPrintConsNote.Enabled=false;
			btnSave.Enabled=true;
			Button1.Enabled=false;
			Button2.Enabled=true;

			lblError.Text="Master AWB verified.";
			Textbox12.Text="DCAMPS";
			Textbox14.Text="23/05/2014 09:30";
		}

		private void Button2_Click(object sender, System.EventArgs e)
		{
			btnExecQry.Enabled=false;
			btnPrintConsNote.Enabled=false;
			btnSave.Enabled=true;
			Button1.Enabled=false;
			Button2.Enabled=true;

			lblError.Text="Master AWB exported.";
			Textbox13.Text="DCAMPS";
			Textbox15.Text="23/05/2014 09:45";
		}

		private void PackageDetails_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			string cmd = e.CommandName;
			if(cmd=="ADD_ITEM")
			{
				string sUrl = "HouseAWBDetailEntry.aspx";
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScriptPopupSetSize("openLargeWindowParam.js",paramList,550,820);
				Utility.RegisterScriptString(sScript,"ShowPopupScript"+DateTime.Now.ToString("ddMMyyyyHHmmss"),this.Page);	
			}
		}
	}
}
