using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;
using Cambro.Web.DbCombo;
using com.common.RBAC;
using System.Text ;
using com.common.DAL;

namespace com.ties
{
	/// <summary>
	/// Summary description for ShipmentTrackingQuery.
	/// </summary>
	public class DEOManual : BasePage
	{
		
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox txtPeriod2;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.Label lbPayerName;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label lbPickupRoute;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label lbstatus;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label lbBooking;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.DropDownList dlReasonCode;
		protected System.Web.UI.WebControls.Label Label2;
		protected com.common.util.msTextBox txtConsignmentNo;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Button btExecute;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.TextBox txtReasonDesc;
		protected System.Web.UI.WebControls.TextBox txtRemark;
		protected System.Web.UI.WebControls.Label lbDeleted;
		protected com.common.util.msTextBox txtBooking;
		protected System.Web.UI.WebControls.Label lblBooking;
		protected System.Web.UI.WebControls.Button btQuery;

		string m_strAppID=null;
		string m_strEnterpriseID=null;
		string m_strCulture=null;
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btQuery.Click += new System.EventHandler(this.btQuery_Click);
			this.btExecute.Click += new System.EventHandler(this.btExecute_Click);
			this.txtConsignmentNo.TextChanged += new System.EventHandler(this.txtConsignmentNo_TextChanged);
			this.Button1.Click += new System.EventHandler(this.Button1_Click);
			this.dlReasonCode.SelectedIndexChanged += new System.EventHandler(this.dlReasonCode_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		DataSet ds;
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page her
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			if(!Page.IsPostBack)
			{				
				txtBooking.Text="";
				lblBooking.Visible=false;
				txtBooking.Visible=false;
				LoaddropdownReason();
			}
			else
			{
				ds = (DataSet)Session["ssDs"];
			}
			btExecute.Attributes.Add("Onclick","javascript:return confirm('You are delete?')");
		}

		private void txtConsignmentNo_TextChanged(object sender, System.EventArgs e)
		{
			Button1_Click(sender,e);
		}

		public void Populate()
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			lblBooking.Visible=false;
			txtBooking.Visible=false;
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID, m_strEnterpriseID);
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select consignment_no,booking_no,last_status_code,Route_Code,payer_name from shipment  ");
			strQry.Append(" where consignment_no = '");
			strQry.Append(txtConsignmentNo.Text.Trim());
			strQry.Append("'");
			//strQry.Append(Utility.ReplaceSingleQuote(serviceCode));

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			DataSet dsCon = null;
			dsCon =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);

			string str_booking="";
			string str_status="";
			string str_pickup="";
			string str_payer="";
			foreach(DataRow dr in dsCon.Tables[0].Rows)
			{
				if(str_booking.Trim().Length==0)
				{
					str_booking=dr["Booking_no"].ToString();
				}
				else
				{
					str_booking+=","+dr["Booking_no"].ToString();
				}
				if(str_status.Trim().Length==0)
				{
					str_status=dr["last_status_code"].ToString();
				}
				else
				{
					str_status+=","+dr["last_status_code"].ToString();
				}
				if(str_pickup.Trim().Length==0)
				{
					str_pickup=dr["Route_Code"].ToString();
				}
				else
				{
					str_pickup+=","+dr["Route_Code"].ToString();
				}

				if(str_payer.Trim().Length==0)
				{
					str_payer=dr["Payer_Name"].ToString();
				}
				else
				{
					str_payer+=","+dr["Payer_Name"].ToString();
				}
			}
			if(dsCon.Tables[0].Rows.Count>1)
			{
				txtBooking.Text="";
				lblBooking.Visible=true;				
				txtBooking.Visible=true;
			}
			else
			{
				txtBooking.Text="";
				lblBooking.Visible=false;				
				txtBooking.Visible=false;
			}
			lbBooking.Text = str_booking;
			lbstatus.Text = str_status;
			lbPickupRoute.Text = str_pickup;
			lbPayerName.Text  = str_payer;
		}

		private void btQuery_Click(object sender, System.EventArgs e)
		{
			txtBooking.Text="";
			lblBooking.Visible=false;
			txtBooking.Visible=false;
			txtConsignmentNo.Text ="";
			dlReasonCode.SelectedIndex = 0;
			if(ds.Tables[0].Rows.Count > 0)
				txtReasonDesc.Text = ds.Tables[0].Rows[dlReasonCode.SelectedIndex]["Delete_desc"].ToString();
			txtRemark.Text ="";
			lbBooking.Text ="";
			lbstatus.Text ="";
			lbPickupRoute.Text ="";
			lbPayerName.Text ="";
			lbDeleted.Text="";

		}

		private void btExecute_Click(object sender, System.EventArgs e)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			IDbConnection conApp = dbCon.GetConnection();

//			lbDeleted.Text ="Processing...";
			string str_status=lbstatus.Text;
			string str_pickup=lbPickupRoute.Text;
			string str_payer=lbPayerName.Text;
            string str_booking_delete = "";
            string sql_booking = "";

            if (txtBooking.Visible == true)
            {
                str_booking_delete = txtBooking.Text.Trim();
            }

            if (str_booking_delete.Length > 0)
            {
                sql_booking = "and booking_no = " + str_booking_delete;
            }
            else
            {
                str_booking_delete = lbBooking.Text.Trim();
                sql_booking = "and booking_no = " + str_booking_delete;
            }

            string strCheck = "select consignment_no, booking_no, invoice_no from shipment where consignment_no='" + txtConsignmentNo.Text.Trim() +"' " + sql_booking;
			IDbCommand dbCmdCh=null;
			dbCmdCh = dbCon.CreateCommand(strCheck,CommandType.Text);
			DataSet dsch = null;
			dsch =(DataSet)dbCon.ExecuteQuery(dbCmdCh,com.common.DAL.ReturnType.DataSetType);

			if(dsch.Tables[0].Rows.Count > 0 && dsch.Tables[0].Rows[0]["invoice_no"].ToString().Trim()!="")
			{
				lbDeleted.Text ="Can't delete consignment no. "+ dsch.Tables[0].Rows[0]["consignment_no"].ToString().Trim() +" which has been invoiced";
				return;
			}

			if(txtBooking.Visible==true)
			{
				int parse_booking=0;
				if(txtBooking.Text.Trim().Length<=0)
				{
					lbDeleted.Text ="Booking No. is required.";
					return;
				}
				try
				{
					parse_booking=int.Parse(txtBooking.Text.Trim());
				}
				catch
				{
					lbDeleted.Text ="Invalid Booking.";
					return;
				}
				string sql_booking_check= "select consignment_no,booking_no,last_status_code,Route_Code,payer_name from shipment where consignment_no='"+ txtConsignmentNo.Text.Trim() +"' and booking_no="+txtBooking.Text.Trim();
				IDbCommand dbCmdBooking = null;
				dbCmdBooking = dbCon.CreateCommand(sql_booking_check,CommandType.Text);
				DataSet dsch_booking = null;
				dsch_booking =(DataSet)dbCon.ExecuteQuery(dbCmdBooking,com.common.DAL.ReturnType.DataSetType);
				if(dsch_booking.Tables[0].Rows.Count> 0)
				{
					DataRow dr = dsch_booking.Tables[0].Rows[0];

					str_status=dr["last_status_code"].ToString();
					str_pickup=dr["Route_Code"].ToString();
					str_payer=dr["Payer_Name"].ToString();
				}
				else
				{
					lbDeleted.Text ="No Booking.";
					return;
				}
			}
			

			if(dsch.Tables[0].Rows.Count > 0)
			{
				str_booking_delete = "";			
				sql_booking = "";
				if(txtBooking.Visible==true)
				{
					str_booking_delete = txtBooking.Text.Trim();
				}
				
				if(str_booking_delete.Length > 0)
				{
					sql_booking = "and booking_no = "+ str_booking_delete;
				}
				else
				{
					str_booking_delete=lbBooking.Text.Trim();
				}
				
				string strsql ="delete from shipment_vas where consignment_no='"+ txtConsignmentNo.Text.Trim() + "' " + sql_booking;

				strsql += "delete from Shipment_Audit_Trial where consignment_no='"+ txtConsignmentNo.Text.Trim() +"' " + sql_booking;
				strsql += "delete from Shipment_Cost where consignment_no='"+ txtConsignmentNo.Text.Trim() +"' " + sql_booking;
				strsql += "delete from Shipment_PKG where consignment_no='"+ txtConsignmentNo.Text.Trim() +"' " + sql_booking;
				strsql += "delete from Shipment_PKG_Box where consignment_no='"+ txtConsignmentNo.Text.Trim() +"' " + sql_booking;
				strsql += "delete from Shipment_Tracking where consignment_no='"+ txtConsignmentNo.Text.Trim() +"' " + sql_booking;
				strsql += "delete from Shipment where consignment_no='"+ txtConsignmentNo.Text.Trim() +"' " + sql_booking;

				IDbCommand dbCmd = null;
				dbCmd = dbCon.CreateCommand(strsql, CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.CommandTimeout = 0;
				int iResult = dbCon.ExecuteNonQuery(dbCmd);
				
				if(iResult>0)
				{
					strCheck ="select consignment_no,invoice_no  from shipment where consignment_no='"+ txtConsignmentNo.Text.Trim() +"'";
					dbCmdCh=null;
					dbCmdCh = dbCon.CreateCommand(strCheck,CommandType.Text);
					dsch = null;
					dsch =(DataSet)dbCon.ExecuteQuery(dbCmdCh,com.common.DAL.ReturnType.DataSetType);
					if(dsch.Tables[0].Rows.Count<=0)
					{
						strsql = "delete from MF_Batch_Manifest_Detail  where consignment_no='"+ txtConsignmentNo.Text.Trim() +"'";
						strsql += " delete from Delivery_Manifest_Detail where consignment_no='"+ txtConsignmentNo.Text.Trim() +"'";
						strsql += " delete from Invoice_Detail where consignment_no='"+ txtConsignmentNo.Text.Trim() +"'";
						dbCmd = null;
						dbCmd = dbCon.CreateCommand(strsql, CommandType.Text);
						dbCmd.Connection = conApp;
						dbCmd.CommandTimeout = 0;
						dbCon.ExecuteNonQuery(dbCmd);
					}
					lbDeleted.Text ="Consignment No. " + txtConsignmentNo.Text.Trim() +" has been Deleted...";
				}
				else
				{
					lbDeleted.Text ="Can't delete consignment no. "+txtConsignmentNo.Text.Trim();
					return;
				}
				
				
				string strInsert = "insert into Consignment_Delete(consignment_no,Delete_Date,User_delete,Reason_code,booking_no,last_status_code,Route_code,Payer_name,Remark) ";
				strInsert +=" values('"+ txtConsignmentNo.Text.Trim() +"',Getdate(),'"+ utility.GetUserID() +"','"+ dlReasonCode.SelectedItem.Value.Trim() +"',"+ str_booking_delete +",'"+ str_status +"','"+ str_status +"','"+ str_payer +"','"+ txtRemark.Text.Trim() +"')";
			
				IDbCommand dbCmdI = null;
				dbCmdI = dbCon.CreateCommand(strInsert, CommandType.Text);
				dbCmdI.Connection = conApp;
				dbCon.ExecuteNonQuery(dbCmdI);

			}
			else
			{
				lbDeleted.Text ="No Consignment No.";
			}
		}

		private void dlReasonCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			txtReasonDesc.Text = ds.Tables[0].Rows[dlReasonCode.SelectedIndex]["Delete_desc"].ToString();
		}

		public void LoaddropdownReason()
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			
			string strsql ="select * from Consignment_Delete_Code";

			dbCmd = dbCon.CreateCommand(strsql,CommandType.Text);

			ds = new DataSet();
			ds =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);

			dlReasonCode.DataSource = ds.Tables[0];
			dlReasonCode.DataValueField = "Delete_code";
			dlReasonCode.DataMember ="Delete_desc";
			dlReasonCode.DataBind();

			//edit by Tumz.
			if(ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
				txtReasonDesc.Text = ds.Tables[0].Rows[0]["Delete_desc"].ToString();

			Session["ssDs"] = ds;
		}

		private void Button1_Click(object sender, System.EventArgs e)
		{
			if(txtConsignmentNo.Text.Trim()!="")
			{
				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID );
				IDbConnection conApp = dbCon.GetConnection();

				string strCheck ="select consignment_no from shipment where consignment_no='"+ txtConsignmentNo.Text.Trim() +"'";
				IDbCommand dbCmdCh=null;
				dbCmdCh = dbCon.CreateCommand(strCheck,CommandType.Text);
				DataSet dsch = null;
				dsch =(DataSet)dbCon.ExecuteQuery(dbCmdCh,com.common.DAL.ReturnType.DataSetType);

				if(dsch.Tables[0].Rows.Count > 0)
				{
					Populate();
					lbDeleted.Text ="";
				}
				else
				{
					lbDeleted.Text ="No Consignment No.";
					lbBooking.Text ="";
					lbstatus.Text ="";
					lbPickupRoute.Text ="";
					lbPayerName.Text ="";
				}
			}
			else
			{
				lbDeleted.Text ="Please Entry Consignment Data";
				lbBooking.Text ="";
				lbstatus.Text ="";
				lbPickupRoute.Text ="";
				lbPayerName.Text ="";
			}
		}
		
	}
}