using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.ties.BAL;
using com.common.applicationpages;
using System.Text;
using Cambro.Web.DbCombo;
using System.Xml;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for TrackShipment_EUM.
	/// </summary>
	public class TrackShipment_EUM : com.common.applicationpages.BasePage  
	{
		protected System.Web.UI.WebControls.Button				btnQry;
		protected System.Web.UI.WebControls.Button				btnExecQry;
		protected System.Web.UI.WebControls.Label				lblMainTitle;
		protected System.Web.UI.WebControls.Label				lblConsgmtNo;
		protected System.Web.UI.WebControls.TextBox				txtConsigNo;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable			MainTable;
		protected System.Web.UI.WebControls.Label				lblRefNo;
		protected System.Web.UI.WebControls.TextBox				txtCustRefNo;
		protected System.Web.UI.HtmlControls.HtmlTable			TblBookDtl;	
		private Utility utility;
		String userID = null;
		String appID = null;
		String enterpriseID = null;
		String PlayerId = null;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.DropDownList ddlTransporter;
		String strErrorMsg = "";

	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();
			PlayerId = TrackAndTraceDAL.QueryPlayerIdTrackandTrace(appID,enterpriseID,userID);

			if(!IsPostBack)
			{
				DefaultScreen();
				SetInitialSelect(txtConsigNo);
			}

			
		}

		private void DefaultScreen()
		{
			txtCustRefNo.Text = null;
			txtConsigNo.Text  = null;

		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			DefaultScreen();
		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if(((txtConsigNo.Text == null)||(txtConsigNo.Text.Trim() == ""))&&((txtCustRefNo.Text == null)||(txtCustRefNo.Text.Trim() == "")))
			{
				//lblErrorMsg.Text = "Error:You cannot track a shipment without Consignment Number or Customer Reference Number.";
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"Error: You cannot track a shipment without either a consignment number or a customer reference number.",utility.GetUserCulture());
				return;
			}
			else
			{
				if(((txtConsigNo.Text != null)&&(txtConsigNo.Text.Trim() != ""))&&((txtCustRefNo.Text != null)&&(txtCustRefNo.Text.Trim() != "")))
				{
					// Thosapol Yennam 16/08/2013 Change Message
					lblErrorMsg.Text = "";
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR: YOU CANNOT ENTER BOTH CONSIGNMENT_NO_AND_REFERENCE_NO.",utility.GetUserCulture());//"Error:You cannot enter BOTH Consignment Number & Customer Reference Number to track a shipment.";					
					return;
				}
				else if(((txtConsigNo.Text != null)&&(txtConsigNo.Text.Trim() != ""))||((txtCustRefNo.Text != null)&&(txtCustRefNo.Text.Trim() != "")))
				{
					if((txtConsigNo.Text != null)&&(txtConsigNo.Text.Trim() != ""))
					{
						String strConsgNo = txtConsigNo.Text.Trim();
						//Is exist ConsigNo.
						// Thosapol Yennam 19/08/2013 Comment ==> No longer required [Message : You are not authorized to track this shipment.]
						//						if(TrackAndTraceDAL.IsConsgmentExist(appID,enterpriseID,txtConsigNo.Text.Trim()) == true)
						//						{
						//							// Thosapol Yennam 16/08/2013 Comment ==> No longer required [Message : You are not authorized to track this shipment.]
						//							if(TrackAndTraceDAL.EnableViewByConsgment(appID,enterpriseID,userID,strConsgNo)== true)
						//							{
						//								lblErrorMsg.Text = "";
						//								OpenWindowpage("ShipmentStatus.aspx?Type=CONSGNO&Number="+strConsgNo+"&PlayerId="+PlayerId+"&Err=F");
						//							}
						//							else
						//							{
						//								lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"Error: You are not authorized to track this shipment.",utility.GetUserCulture());
						//								return;
						//							}
						//							// Thosapol Yennam 16/08/2013 Add new Code
						//							lblErrorMsg.Text = "";
						//							OpenWindowpage("ShipmentStatus.aspx?Type=CONSGNO&Number="+strConsgNo+"&PlayerId="+PlayerId+"&Err=F");
						//
						//						}
						//						else 
						//						{
						//							lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"Error: Specified Consignment Number does not exist.",utility.GetUserCulture());//"Error:Specified Consignment Number does not exist.";
						//							return;
						//						}		

						//Is exist ConsigNo.
						// Thosapol Yennam 19/08/2013 Add new Code
						if(TrackAndTraceDAL.IsTrackandTraceExist(appID,enterpriseID,txtConsigNo.Text.Trim(),null,PlayerId,"1") == true)
						{						
							lblErrorMsg.Text = "";
							string sUrl = TrackAndTraceDAL.ConvertSharpToAscii("ShipmentStatus.aspx?Type=CONSGNO&Number="+strConsgNo+"&PlayerId="+PlayerId+"&Err=F");
							OpenWindowpage(sUrl);
						}
						else 
						{
							lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"Error: Specified Consignment Number does not exist.",utility.GetUserCulture());//"Error:Specified Consignment Number does not exist.";
							return;
						}	
					}
						//Query by CustRefNo.
					else if((txtCustRefNo.Text != null)&&(txtCustRefNo.Text.Trim() != ""))
					{
						String strCustRefNo = txtCustRefNo.Text.Trim();

						//					    // Thosapol Yennam 19/08/2013 Comment ==> No longer required [Message : You are not authorized to track this shipment.]
						//Is exist CustRefNo.
						//						if(TrackAndTraceDAL.IsCustRefExist(appID,enterpriseID,strCustRefNo) == true)
						//						{
						//							int count = TrackAndTraceDAL.EnableViewByCustRef(appID,enterpriseID,userID,strCustRefNo);
						//							if(count > 0 )
						//							{
						//								if(count == 1)
						//								{
						//									lblErrorMsg.Text = "";
						//									OpenWindowpage("ShipmentStatus.aspx?Type=CUSTREF&Number="+strCustRefNo+"&PlayerId="+PlayerId+"&Err=F");
						//								}
						//								else
						//								{
						//									lblErrorMsg.Text = "";
						//									OpenWindowpage("ShipmentStatus.aspx?Type=CUSTREF&Number="+strCustRefNo+"&PlayerId="+PlayerId+"&Err=T");															
						//								}
						//							}
						//							else
						//							{
						//								lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"Error: You are not authorized to track this shipment.",utility.GetUserCulture());
						//								return;							
						//							}
						//
						//							if(count == 1)
						//							{
						//								lblErrorMsg.Text = "";
						//								OpenWindowpage("ShipmentStatus.aspx?Type=CUSTREF&Number="+strCustRefNo+"&PlayerId="+PlayerId+"&Err=F");
						//							}
						//							else
						//							{
						//								lblErrorMsg.Text = "";
						//								OpenWindowpage("ShipmentStatus.aspx?Type=CUSTREF&Number="+strCustRefNo+"&PlayerId="+PlayerId+"&Err=T");															
						//							}


						// Thosapol Yennam 19/08/2013 Add new Code
						//Is exist CustRefNo.
						if(TrackAndTraceDAL.IsTrackandTraceExist(appID,enterpriseID,null,strCustRefNo,PlayerId,"1") == true)
						{
							lblErrorMsg.Text = "";
							string sUrl = TrackAndTraceDAL.ConvertSharpToAscii("ShipmentStatus.aspx?Type=CUSTREF&Number="+strCustRefNo+"&PlayerId="+PlayerId+"&Err=F");

							OpenWindowpage(sUrl);							
						}
						else 
						{				
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"Error: Specified Customer Reference Number does not exist.",utility.GetUserCulture());
							return;
						}	
					
					}
				}
			}
		}

		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("OpenLargerWindoParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		public static void SetInitialSelect(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.UniqueID); 
				s.Append("'].select();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		} 

	}
}
