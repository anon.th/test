using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.common.RBAC;
using com.ties.DAL;
using com.ties.classes;
using System.Configuration;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for MonthlyCostAccount.
	/// </summary>
	public class MonthlyCostAccount : BasePage
	{
		protected System.Web.UI.WebControls.Label lblAstrik;
		protected System.Web.UI.WebControls.Label lblAstrik2;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Label lblMessage;

		protected com.common.util.msTextBox txtAccYear;

		protected System.Web.UI.WebControls.Button btnAddAllCostCode;
		protected System.Web.UI.WebControls.Button btnCCInsert;
		
		protected System.Web.UI.HtmlControls.HtmlTable tblMonthlyCostAccount;

		protected System.Web.UI.WebControls.DropDownList ddlAccMonth;
		protected System.Web.UI.WebControls.DataGrid dgCostCodes;
		protected Cambro.Web.DbCombo.DbCombo DbComboCostCode;


		DataSet m_dsCostCode = null;

		//protected System.Web.UI.WebControls.Button btnAddRow;
		//Utility utility = null;
		AccessRights m_moduleAccessRights = null;
		DataView m_dvMonthsOptions = null;
		protected System.Web.UI.WebControls.ValidationSummary mcaValidation;
		protected System.Web.UI.WebControls.Label lblAccYear;
		protected System.Web.UI.WebControls.Label lblAccMonth;
		protected System.Web.UI.WebControls.RequiredFieldValidator validateAccNo;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			loadErrorMsg();
			if(!Page.IsPostBack)
			{
				//SecurityCheck();
				ViewState["MCAOperation"] = Operation.None;
				//12/11/2002
				ViewState["currentQuoteSet"] = 0;	
							
				/* for Quotation*/
				
				LoadComboLists();
				BindComboLists();
				ResetScreen();
			}
			else
			{
				if(Session["SESSION_DS1"] != null)
				{
					m_dsCostCode = (DataSet)Session["SESSION_DS1"];
				}


				if(ViewState["MODULE_RIGHTS"] != null)
				{
					m_moduleAccessRights  = (AccessRights)ViewState["MODULE_RIGHTS"];
				}
			}
			addDbComboEventHandler();

		}

		private void loadErrorMsg()
		{
			if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
			{
				validateAccNo.ErrorMessage = Utility.GetLanguageText(ResourceType.UserMessage, "ACC_YEAR_REQ", utility.GetUserCulture());
			}
			else
			{
				validateAccNo.ErrorMessage = "Account Year is required field.";
			}
		}

		private void ResetScreen()
		{
			m_dsCostCode = MonthlyCostAccountMgrDAL.GetEmptyCostCodeDS(0);
			dgCostCodes.EditItemIndex = -1;
			BindMCAGrid();
		}

		private void addDbComboEventHandler()
		{
			if(dgCostCodes.EditItemIndex == -1)
				return;

			this.DbComboCostCode = (Cambro.Web.DbCombo.DbCombo)dgCostCodes.Items[dgCostCodes.EditItemIndex].FindControl("DbComboCostCode");
			if(this.DbComboCostCode != null)
			{
				this.DbComboCostCode.SelectedItemChanged += new System.EventHandler(this.DbComboCostCode_OnSelectedIndexChanged);
				SetDbComboServerStates();
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ddlAccMonth.SelectedIndexChanged += new System.EventHandler(this.ddlAccMonth_SelectedIndexChanged);
			this.btnAddAllCostCode.Click += new System.EventHandler(this.btnAddAllCostCode_Click);
			this.btnCCInsert.Click += new System.EventHandler(this.btnCCInsert_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected void dgCostCodes_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int) ViewState["MCAOperation"] == (int)Operation.Insert && dgCostCodes.EditItemIndex > 0)
			{
				m_dsCostCode.Tables[0].Rows.RemoveAt(dgCostCodes.EditItemIndex);
			}

			dgCostCodes.EditItemIndex = e.Item.ItemIndex;
			ViewState["MCAOperation"] = Operation.Update;
			BindMCAGrid();
			lblMessage.Text = "";
			Logger.LogDebugInfo("CostException","dgCostCodes_Edit","INF004","updating data grid...");			
		}
		
		public void dgCostCodes_Update(object sender, DataGridCommandEventArgs e)
		{

			if(ValidateYearNMonthInput() == false)
				return;

			FillMCADataRow(e.Item,e.Item.ItemIndex);

			int iAccountYear = Convert.ToInt32(txtAccYear.Text);
			int iAccountMonth = Convert.ToInt32(ddlAccMonth.SelectedItem.Value);


			int iOperation = (int)ViewState["MCAOperation"];
			
			switch(iOperation)
			{

				case (int)Operation.Update:

					DataSet dsToUpdate = m_dsCostCode.GetChanges();
					MonthlyCostAccountMgrDAL.ModifyCostCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),iAccountYear,iAccountMonth,dsToUpdate);
					btnCCInsert.Enabled = true;
					m_dsCostCode.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					lblMessage.Text = "Cost code updated successfully";
					Logger.LogDebugInfo("CostException","dgCostCodes_OnUpdate","INF004","update in modify mode..");

					break;

				case (int)Operation.Insert:

					DataSet dsToInsert = m_dsCostCode.GetChanges();
					
					if(dsToInsert != null)
					{
						DataRow drCurrent = dsToInsert.Tables[0].Rows[0];
						//this.DbComboCostCode = (Cambro.Web.DbCombo.DbCombo)e.Item.FindControl("DbComboCostCode");
						//SetDbComboServerStates();

						if(drCurrent["cost_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
						{
							lblMessage.Text = "Cost Code is required field.";
							return;
						}
					}

					try
					{
						MonthlyCostAccountMgrDAL.AddCostCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),iAccountYear,iAccountMonth,dsToInsert);
						btnCCInsert.Enabled = true;
						lblMessage.Text = "1 cost code inserted successfully.";
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblMessage.Text = "Duplicate Cost Code is not allowed. Please select another cost code name.";
							
						}
						else
						{
							lblMessage.Text = "Error inserting cost code.";
						}
						return;
						
					}
					m_dsCostCode.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("CostException","dgCostCodes_OnUpdate","INF004","update in Insert mode");
					break;

			}
			dgCostCodes.EditItemIndex = -1;
			ViewState["MCAOperation"] = Operation.None;
			BindMCAGrid();
		}

		protected void dgCostCodes_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgCostCodes.EditItemIndex = -1;

			if((int)ViewState["MCAOperation"] == (int)Operation.Insert)
			{
				m_dsCostCode.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
			}
			ViewState["MCAOperation"] = Operation.None;
			BindMCAGrid();
			btnCCInsert.Enabled = true;
			lblMessage.Text = "";
			Logger.LogDebugInfo("CostException","dgCostCodes_Cancel","INF004","updating data grid...");			
		}

		public void dgCostCodes_Delete(object sender, DataGridCommandEventArgs e)
		{
			if(ValidateYearNMonthInput() == false)
				return;

			this.DbComboCostCode = (Cambro.Web.DbCombo.DbCombo)e.Item.FindControl("DbComboCostCode");
			Label lblCostCode = (Label)e.Item.FindControl("lblCostCode");
			
			int iAccountYear = Convert.ToInt32(txtAccYear.Text);
			int iAccountMonth = Convert.ToInt32(ddlAccMonth.SelectedItem.Value);

			String strCostCode = null;
			if(this.DbComboCostCode != null)
			{
				strCostCode = this.DbComboCostCode.Text;
			}

			if(lblCostCode != null)
			{
				strCostCode = lblCostCode.Text;
			}

			try
			{
				if((int) ViewState["MCAOperation"] != (int)Operation.Insert)
				{
					MonthlyCostAccountMgrDAL.DeleteCostCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),iAccountYear,iAccountMonth,strCostCode);
					m_dsCostCode.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				}
				else
				{
					if(dgCostCodes.EditItemIndex == e.Item.ItemIndex)
					{
						m_dsCostCode.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
					}
					else
					{
						MonthlyCostAccountMgrDAL.DeleteCostCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),iAccountYear,iAccountMonth,strCostCode);
						m_dsCostCode.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);

						if(dgCostCodes.EditItemIndex > 0)
						{
							m_dsCostCode.Tables[0].Rows.RemoveAt(dgCostCodes.EditItemIndex-1);
						}
					}
				}

				dgCostCodes.EditItemIndex = -1;
				BindMCAGrid();
				lblMessage.Text = strCostCode+" "+"code deleted successfully.";
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				lblMessage.Text = "Error Deleting cost code";
				return;
			}

			Logger.LogDebugInfo("CostException","dgCostCodes_Delete","INF004","Deleted row in Monthly_Cost_Code table..");
			ViewState["MCAOperation"] = Operation.None;
			btnCCInsert.Enabled = true;
		}


		protected void dgCostCodes_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_dsCostCode.Tables[0].Rows[e.Item.ItemIndex];

			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}



			if(e.Item.ItemType == ListItemType.EditItem)
			{
				btnCCInsert.Enabled = false;
			}
	
			DataRow drCurrent = m_dsCostCode.Tables[0].Rows[e.Item.ItemIndex];
			this.DbComboCostCode = (Cambro.Web.DbCombo.DbCombo)e.Item.FindControl("DbComboCostCode");
			if(this.DbComboCostCode != null)
			{


				int iOperation = (int) ViewState["MCAOperation"];

				if(iOperation == (int)Operation.Update)
				{
					DbComboCostCode.Enabled = false;
				}

				//Set server states to facilitate extra where clause
				SetDbComboServerStates();

			}
		}


		private void txtAccYear_TextChanged(object sender, System.EventArgs e)
		{
			if(ValidateYearNMonthInput() == false)
				return;

			ShowMCARecords();
			btnCCInsert.Enabled = true;
		}

		private void ddlAccMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(ValidateYearNMonthInput() == false)
			{
				ResetScreen();
				return;
			}

			ShowMCARecords();
			btnCCInsert.Enabled = true;
		}

		private void ShowMCARecords()
		{
			int iAccountYear = Convert.ToInt32(txtAccYear.Text);
			int iAccountMonth = Convert.ToInt32(ddlAccMonth.SelectedItem.Value);
		
			m_dsCostCode = MonthlyCostAccountMgrDAL.GetCostCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),iAccountYear,iAccountMonth);
			dgCostCodes.EditItemIndex = -1;
			BindMCAGrid();
		}

		protected void DbComboCostCode_OnSelectedIndexChanged(object sender, System.EventArgs e)
		{
			int iOperation = (int) ViewState["MCAOperation"];

			if(iOperation == (int)Operation.Update)
			{
				return;
			}

			String strCostCode = DbComboCostCode.Text;
			String strCostDescription = MonthlyCostAccountMgrDAL.GetCostDescription(utility.GetAppID(),utility.GetEnterpriseID(),strCostCode);
			if(dgCostCodes.EditItemIndex != -1)
			{
				DataRow drCurrent = m_dsCostCode.Tables[0].Rows[dgCostCodes.EditItemIndex];
				drCurrent["cost_code"] = strCostCode;
				drCurrent["cost_code_description"] =  strCostDescription;
				BindMCAGrid();
			}
		}


		private void btnAddAllCostCode_Click(object sender, System.EventArgs e)
		{
			if(ValidateYearNMonthInput() == false)
				return;

			int iAccountYear = Convert.ToInt32(txtAccYear.Text);
			int iAccountMonth = Convert.ToInt32(ddlAccMonth.SelectedItem.Value);

			try
			{
				int iRowsAdded = MonthlyCostAccountMgrDAL.AddAllCostCodes(utility.GetAppID(),utility.GetEnterpriseID(),iAccountYear,iAccountMonth);
				lblMessage.Text = ""+iRowsAdded+" "+"Cost Codes added.";
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				lblMessage.Text = strMsg;
				return;
			}

			ShowMCARecords();
			lblMessage.Text = "";
		}

		private void btnCCInsert_Click(object sender, System.EventArgs e)
		{
			if(ValidateYearNMonthInput() == false)
				return;

			if(m_dsCostCode == null)
			{
				ShowMCARecords();
			}

			AddRowInMCAGrid();	
			dgCostCodes.EditItemIndex = m_dsCostCode.Tables[0].Rows.Count - 1;
			BindMCAGrid();
			ViewState["MCAOperation"] = Operation.Insert;
		}


		private void BindMCAGrid()
		{
			dgCostCodes.DataSource = m_dsCostCode;
			dgCostCodes.DataBind();
			Session["SESSION_DS1"] = m_dsCostCode;
		}


		private void SetDbComboServerStates()
		{
			
			Hashtable hash = new Hashtable();
			hash.Add("strAccountYear", txtAccYear.Text);
			hash.Add("strAccountMonth",ddlAccMonth.SelectedItem.Value);

			if(this.DbComboCostCode == null)
			{
				return;
			}

			this.DbComboCostCode.ServerState = hash;
			this.DbComboCostCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}

		private void AddRowInMCAGrid()
		{
			MonthlyCostAccountMgrDAL.AddNewRowInCostCodeDS(m_dsCostCode);
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboCostCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			int iAccountYear = -1;
			int iAccountMonth = -1;

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strAccountYear"] != null && args.ServerState["strAccountYear"].ToString().Length > 0)
				{
					iAccountYear = Convert.ToInt32(args.ServerState["strAccountYear"].ToString());
				}

				if(args.ServerState["strAccountMonth"] != null && args.ServerState["strAccountMonth"].ToString().Length > 0)
				{
					iAccountMonth = Convert.ToInt32(args.ServerState["strAccountMonth"].ToString());
				}
			}

			DataSet dataset = DbComboDAL.CostCodeQuery(strAppID,strEnterpriseID,iAccountYear,iAccountMonth,args);	
			return dataset;
		}


		private void FillMCADataRow(DataGridItem item, int drIndex)
		{

			DataRow drCurrent = m_dsCostCode.Tables[0].Rows[drIndex];

			this.DbComboCostCode = (Cambro.Web.DbCombo.DbCombo)item.FindControl("DbComboCostCode");

			if(this.DbComboCostCode != null)
			{
				if(this.DbComboCostCode.Text == "")
				{
					drCurrent["cost_code"] = System.DBNull.Value;
				}
				else
				{
					drCurrent["cost_code"] = this.DbComboCostCode.Text;
				}
			}


			Label lblCostDescription = (Label)item.FindControl("lblCostDescription");
			
			if(lblCostDescription != null)
			{
				drCurrent["cost_code_description"] = lblCostDescription.Text;
			}

			msTextBox txtAmount = (msTextBox)item.FindControl("txtAmount");
			if(txtAmount != null)
			{
				if(txtAmount.Text == "")
				{
					drCurrent["cost_amt"] = System.DBNull.Value;
				}
				else
				{
					drCurrent["cost_amt"] = txtAmount.Text;
				}
			}
		}

		//private void SecurityCheck()
		private new void SecurityCheck()
		{
			String strParentModuleID = Request.Params["PARENTMODID"];
			String strModuleID = Request.Params["MODID"]; 

			m_moduleAccessRights = RBACManager.GetModuleAccessRights(utility.GetAppID(),utility.GetEnterpriseID(),utility.GetUserID(),strModuleID,strParentModuleID);
			ViewState["MODULE_RIGHTS"] = m_moduleAccessRights;

			if(m_moduleAccessRights.Module == false)
			{
				Response.Redirect("AccessDeniedPage.aspx",true);
			}
		}



		private DataView GetMonthsOptions(bool showNilOption) 
		{
			DataTable dtMonthsOptions = new DataTable();
 
			dtMonthsOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonthsOptions.Columns.Add(new DataColumn("NumValue", typeof(int)));

			ArrayList monthOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"months",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtMonthsOptions.NewRow();
				dtMonthsOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode monthSysCode in monthOptionArray)
			{
				DataRow drEach = dtMonthsOptions.NewRow();
				drEach[0] = monthSysCode.Text;
				drEach[1] = monthSysCode.StringValue;
				dtMonthsOptions.Rows.Add(drEach);
			}

			DataView dvMonthsOptions = new DataView(dtMonthsOptions);
			return dvMonthsOptions;
		}

		private void LoadComboLists()
		{
			m_dvMonthsOptions = GetMonthsOptions(true);
		}

		private void BindComboLists()
		{
//			DataRow drCurrent = m_sdsCustomerProfile.ds.Tables[0].Rows[(int)ViewState["currentPage"]];


			ddlAccMonth.DataSource = (ICollection)m_dvMonthsOptions;
			ddlAccMonth.DataTextField = "Text";
			ddlAccMonth.DataValueField = "NumValue";
			ddlAccMonth.DataBind();


//			if((drCurrent["apply_dim_wt"]!= null) && (!drCurrent["apply_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
//			{
//				String strApplyDimWt = (String)drCurrent["apply_dim_wt"];
//				ddlAccMonth.SelectedIndex = ddlAccMonth.Items.IndexOf(ddlAccMonth.Items.FindByValue(strApplyDimWt));//.FindByValue("N"));
//			}
//


		}


		private bool ValidateYearNMonthInput()
		{
			bool bYearValid = false;
			bool bMonthValid = false;
			lblMessage.Text = "";

			validateAccNo.Validate();

			if(!validateAccNo.IsValid)
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ACC_YEAR_REQ", utility.GetUserCulture());
				} 
				else
				{
					lblMessage.Text = "Account Year is required field.";
				}
				return false;
			}
			else
			{
				lblMessage.Text = "";
				bYearValid = true;
			}
			//
			if(Convert.ToInt32(txtAccYear.Text.ToString())<1900)
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ACC_YEAR_MORE", utility.GetUserCulture());
				} 
				else
				{
					lblMessage.Text = "Account Year should be more than 1900";
				}
				return false;
			}
			else
			{
				lblMessage.Text = "";
				bYearValid = true;
			}
			//
			String strAccountMonth = ddlAccMonth.SelectedItem.Value;

			if(strAccountMonth == null || (strAccountMonth != null && strAccountMonth.Length <= 0))
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					lblMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ACC_YEAR_REQ", utility.GetUserCulture());
				} 
				else
				{
					lblMessage.Text = "Account Year is required field.";
				}
				return false;
			}
			else
			{
				lblMessage.Text = "";
				bMonthValid = true;
			}

			return (bYearValid && bMonthValid);
		}


	}
}
