<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<%@ Page language="c#" Codebehind="AgentProfile.aspx.cs" AutoEventWireup="false" Inherits="com.ties.AgentProfile" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AgentProfile</title>
        <style type="text/css">
        .Initial
        {
            display: block;
            padding: 4px 18px 4px 18px;
            float: left;
            background: url("../Images/InitialImage.png") no-repeat right top;
            color: Black;
            font-weight: bold;
        }
        .Initial:hover
        {
            color: White;
            background: url("../Images/SelectedButton.png") no-repeat right top;
        }
        .Clicked
        {
            float: left;
            display: block;
            background: url("../Images/SelectedButton.png") no-repeat right top;
            padding: 4px 18px 4px 18px;
            color: Black;
            font-weight: bold;
            color: White;
        }
        </style>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<META content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<META content="C#" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<BODY onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<FORM id="AgentProfile" method="post" runat="server">
			<!--
                <iewc:Tab Text="Special&#160;Information" TargetID="SpecialInfoPage" ToolTip="Agent Special Information" TabIndex="1"></iewc:Tab>
				<iewc:Tab Text="Placement&#160;&amp;&#160;Payment" TargetID="PlacementPage" TabIndex="4"></iewc:Tab>
				<iewc:Tab Text="Quotation&#160;Status" TargetID="QuotationPage" TabIndex="5"></iewc:Tab>
		    -->
            <!--
            <iewc:tabstrip id="TabStrip1" style="Z-INDEX: 102; LEFT: 29px; POSITION: absolute; TOP: 73px" tabIndex="10" runat="server" TargetID="agentProfileMultiPage" TabSelectedStyle="background-color:#0000ff;color:#000000;" TabHoverStyle="background-color:#777777" TabDefaultStyle="font-family:verdana;font-weight:bold;font-size:8pt;color:#0000ff;width:79;height:21;text-align:center;" Width="699px" Height="35px">
				<iewc:Tab Text="Agent" TargetID="AgentPage"></iewc:Tab>
				<iewc:Tab Text="Reference" TargetID="ReferencePage" TabIndex="2"></iewc:Tab>
				<iewc:Tab Text="Status & Exception" TargetID="StatusPage" TabIndex="3"></iewc:Tab>
				<iewc:Tab Text="Zone & Postal Code" TargetID="ZipcodePage" TabIndex="3"></iewc:Tab>
				<iewc:Tab Text="Quotation Status" TargetID="QuotationPage" TabIndex="5"></iewc:Tab>
			</iewc:tabstrip>
            -->
            <asp:label id="lblMainTitle" style="Z-INDEX: 116; LEFT: 29px; POSITION: absolute; TOP: 8px" runat="server" Width="477px" Height="26px" CssClass="mainTitleSize">Agent Profile</asp:label>
            <asp:label id="lblNumRec" style="Z-INDEX: 115; LEFT: 460px; POSITION: absolute; TOP: 104px" runat="server" Width="274px" Height="19px" CssClass="RecMsg"></asp:label>
            <asp:button id="btnQry" style="Z-INDEX: 111; LEFT: 27px; POSITION: absolute; TOP: 44px" runat="server" Width="62px" Height="22px" CssClass="queryButton" CausesValidation="False" Text="Query"></asp:button>
            <asp:button id="btnExecQry" style="Z-INDEX: 101; LEFT: 89px; POSITION: absolute; TOP: 44px" tabIndex="1" runat="server" Width="130px" Height="22px" CssClass="queryButton" CausesValidation="False" Text="Execute Query"></asp:button>
            <asp:button id="btnInsert" style="Z-INDEX: 109; LEFT: 219px; POSITION: absolute; TOP: 44px" tabIndex="2" runat="server" Width="62px" Height="22px" CssClass="queryButton" CausesValidation="False" Text="Insert"></asp:button>
            <asp:button id="btnSave" style="Z-INDEX: 100; LEFT: 280px; POSITION: absolute; TOP: 44px" tabIndex="3" runat="server" Width="66px" Height="22px" CssClass="queryButton" CausesValidation="True" Text="Save"></asp:button>
            <asp:button id="btnDelete" style="Z-INDEX: 110; LEFT: 346px; POSITION: absolute; TOP: 45px" tabIndex="4" runat="server" Width="62px" Height="21px" CssClass="queryButton" CausesValidation="False" Text="Delete" Visible="False"></asp:button>
            <asp:button id="btnGoToFirstPage" style="Z-INDEX: 108; LEFT: 609px; POSITION: absolute; TOP: 43px" tabIndex="5" runat="server" Width="24px" CssClass="queryButton" CausesValidation="False" Text="|<"></asp:button>
            <asp:button id="btnPreviousPage" style="Z-INDEX: 104; LEFT: 633px; POSITION: absolute; TOP: 43px" tabIndex="6" runat="server" Width="24px" CssClass="queryButton" CausesValidation="False" Text="<"></asp:button><asp:button id="btnNextPage" style="Z-INDEX: 105; LEFT: 679px; POSITION: absolute; TOP: 43px" tabIndex="8" runat="server" Width="24px" CssClass="queryButton" CausesValidation="False" Text=">"></asp:button><asp:button id="btnGoToLastPage" style="Z-INDEX: 106; LEFT: 703px; POSITION: absolute; TOP: 43px" tabIndex="9" runat="server" Width="24px" CssClass="queryButton" CausesValidation="False" Text=">|"></asp:button><asp:textbox id="txtCountRec" style="Z-INDEX: 107; LEFT: 657px; POSITION: absolute; TOP: 44px" tabIndex="7" runat="server" Width="24px" Height="19px"></asp:textbox>

            <!--
                <asp:MultiView id="agentProfileMultiPage" style="Z-INDEX: 103; LEFT: 5px; POSITION: absolute; TOP: 100px" runat="server" Width="734px" Height="470px" SelectedIndex="4">
            -->
			<table style="Z-INDEX: 103; LEFT: 5px; POSITION: absolute; TOP: 60px" width="80%">
                <tr>
                    <td>
                        <asp:Button Text="Agent" BorderStyle="None" ID="Button0" CssClass="Initial" runat="server" OnClick="TabStrip_Click" CommandArgument="0"/>
                        <asp:Button Text="Reference" BorderStyle="None" ID="Button1" CssClass="Initial" runat="server" OnClick="TabStrip_Click" CommandArgument="1"/>
                        <asp:Button Text="Status & Exception" BorderStyle="None" ID="Button2" CssClass="Initial" runat="server" OnClick="TabStrip_Click" CommandArgument="2"/>
                        <asp:Button Text="Zone & Postal" BorderStyle="None" ID="Button3" CssClass="Initial" runat="server" OnClick="TabStrip_Click" CommandArgument="3"/>
                        <asp:Button Text="Quotation Status" BorderStyle="None" ID="Button4" CssClass="Initial" runat="server" OnClick="TabStrip_Click" CommandArgument="4"/>
                    </td>
                </tr>
            </table>
            
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <asp:View id="AgentPage" runat="server">
					    <TABLE id="AgentInfoTable" style="Z-INDEX: 112; WIDTH: 730px" width="730" border="0" runat="server">
						    <TR width="100%">
							    <TD width="100%">
								    <fieldset>
									    <legend>
										    <asp:Label id="Label3" runat="server" CssClass="tableHeadingFieldset">Agent Information</asp:Label></legend>
									    <TABLE id="tblCustInfo" width="100%" align="left" border="0" runat="server">
										    <TR height="5">
											    <TD width="2%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="8%"></TD>
										    </TR>
										    <TR height="25">
											    <TD align="right" colSpan="1">
												    <asp:requiredfieldvalidator id="validateAccNo" Width="100%" Runat="server" ControlToValidate="txtAccNo" Display="None" ErrorMessage="Account No."></asp:requiredfieldvalidator>
												    <asp:label id="Label1" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											    <TD colSpan="3">
												    <asp:label id="lblAccNo" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Account No.</asp:label></TD>
											    <TD colSpan="3">
												    <cc1:mstextbox id="txtAccNo" runat="server" Width="100%" CssClass="textField" AutoPostBack="False" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore" tabIndex="11"></cc1:mstextbox></TD>
											    <TD colSpan="1"></TD>
											    <TD colSpan="4">
												    <asp:label id="lblRefNo" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Agent Ref No</asp:label></TD>
											    <TD colSpan="3">
												    <cc1:mstextbox id="txtRefNo" tabIndex="12" runat="server" Width="100%" CssClass="textField" Enabled="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></TD>
											    <TD colSpan="2" align="right">
												    <asp:label id="lblStatus" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Status</asp:label></TD>
											    <TD colSpan="3">
												    <asp:DropDownList id="ddlStatus" tabIndex="13" runat="server" Width="100%" CssClass="textField" Enabled="True"></asp:DropDownList></TD>
										    </TR>
										    <TR height="10">
											    <TD colSpan="20"></TD>
										    </TR>
										    <tr height="25">
											    <TD colSpan="1">
												    <asp:label id="Label22" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											    <TD colSpan="3">
												    <asp:requiredfieldvalidator id="Requiredfieldvalidator10" Width="100%" Runat="server" ControlToValidate="txtAgentName" Display="None" ErrorMessage="Agent Name   "></asp:requiredfieldvalidator>
												    <asp:label id="lblAgentName" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Agent Name</asp:label></TD>
											    <TD colSpan="8">
												    <asp:textbox id="txtAgentName" runat="server" Width="100%" CssClass="textField" tabIndex="14" MaxLength="100"></asp:textbox></TD>
											    <TD colSpan="1">
												    <asp:label id="reqlblContactPerson" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor" Visible="False">&nbsp;*&nbsp;</asp:label>
												    <asp:requiredfieldvalidator id="ReqtxtContactPerson" Width="100%" Runat="server" ControlToValidate="txtContactPerson" Display="None" ErrorMessage="Contact Person  " Enabled="False"></asp:requiredfieldvalidator>
											    </TD>
											    <TD colSpan="3">
												    <asp:label id="lblContactPerson" runat="server" Height="22px" Width="100%" CssClass="tableLabel" Visible="True">Contact Person</asp:label></TD>
											    <TD colSpan="4">
												    <asp:textbox id="txtContactPerson" runat="server" Width="100%" CssClass="textField" tabIndex="15" Visible="True" MaxLength="100"></asp:textbox></TD>
										    </tr>
										    <tr height="25">
											    <TD colSpan="1">
												    <asp:label id="Label23" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											    <TD colSpan="3">
												    <asp:requiredfieldvalidator id="Requiredfieldvalidator11" Width="100%" Runat="server" ControlToValidate="txtAddress1" Display="None" ErrorMessage="Address1  "></asp:requiredfieldvalidator>
												    <asp:label id="lblAddress1" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Address 1</asp:label></TD>
											    <TD colSpan="8">
												    <asp:textbox id="txtAddress1" runat="server" Width="100%" CssClass="textField" tabIndex="16" MaxLength="100"></asp:textbox></TD>
											    <TD colSpan="1">
												    <asp:label id="Label24" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											    <TD colSpan="3">
												    <asp:requiredfieldvalidator id="Requiredfieldvalidator12" Width="100%" Runat="server" ControlToValidate="txtTelephone" Display="None" ErrorMessage="Telephone  "></asp:requiredfieldvalidator>
												    <asp:label id="lblTelphone" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Telephone</asp:label></TD>
											    <TD colSpan="4">
												    <asp:textbox id="txtTelephone" runat="server" Width="100%" CssClass="textField" tabIndex="20" MaxLength="20"></asp:textbox></TD>
										    </tr>
										    <tr height="25">
											    <TD colSpan="1">&nbsp;</TD>
											    <TD colSpan="3">
												    <asp:label id="lblAddress2" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Address 2</asp:label></TD>
											    <TD colSpan="8">
												    <asp:textbox id="txtAddress2" runat="server" Width="100%" CssClass="textField" tabIndex="17" MaxLength="100"></asp:textbox></TD>
											    <TD colSpan="1">&nbsp;</TD>
											    <TD colSpan="3">
												    <asp:label id="lblFax" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Fax</asp:label></TD>
											    <TD colSpan="4">
												    <asp:textbox id="txtFax" runat="server" Width="100%" CssClass="textField" tabIndex="21" MaxLength="20"></asp:textbox></TD>
										    </tr>
										    <tr height="25">
											    <TD align="right" colSpan="1">
												    <asp:label id="Label25" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											    <TD colSpan="3">
												    <asp:requiredfieldvalidator id="Requiredfieldvalidator13" Width="100%" Runat="server" ControlToValidate="txtZipCode" Display="None" ErrorMessage="Postal Code"></asp:requiredfieldvalidator>
												    <asp:label id="lblZip" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Postal Code</asp:label></TD>
											    <TD colSpan="2">
												    <cc1:mstextbox id="txtZipCode" runat="server" Width="100%" tabIndex="18" CssClass="textField" AutoPostBack="False" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></TD>
											    <TD colSpan="1">
												    <asp:button id="btnZipcodePopup" runat="server" Width="100%" Text="..." CausesValidation="False" CssClass="queryButton" tabIndex="19"></asp:button></TD>
											    <TD colSpan="1">
												    <asp:label id="lblState" runat="server" Height="22px" Width="100%" CssClass="tableLabel">State</asp:label></TD>
											    <TD colSpan="3">
												    <asp:textbox id="txtState" runat="server" Width="100%" CssClass="textField" ReadOnly="True"></asp:textbox></TD>
											    <TD colSpan="2">
												    <asp:RegularExpressionValidator ID="regEmailValidate" ControlToValidate="txtEmail" Runat="server" Display="None" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Invalid Email"></asp:RegularExpressionValidator></TD>
											    <TD colSpan="3">
												    <asp:label id="lblEmail" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Email</asp:label></TD>
											    <TD colSpan="4">
												    <asp:textbox id="txtEmail" runat="server" Width="100%" tabIndex="22" CssClass="textField" MaxLength="50"></asp:textbox></TD>
										    </tr>
										    <tr height="25">
											    <TD align="right" colSpan="1"></TD>
											    <TD colSpan="3">
												    <asp:label id="lblCountry" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Country</asp:label></TD>
											    <TD colSpan="4">
												    <asp:textbox id="txtCountry" runat="server" Width="100%" CssClass="textField" AutoPostBack="False" ReadOnly="True"></asp:textbox></TD>
											    <TD colSpan="5">&nbsp;</TD>
											    <TD colSpan="3">
												    <asp:label id="lblCreatedBy" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Created By</asp:label></TD>
											    <TD colSpan="4">
												    <asp:textbox id="txtCreatedBy" runat="server" Width="100%" CssClass="textField" AutoPostBack="False" ReadOnly="True"></asp:textbox></TD>
										    </tr>
									    </TABLE>
								    </fieldset>
							    </TD>
						    </TR>
						    <TR width="100%">
							    <TD width="100%">
								    <fieldset>
									    <legend>
										    <asp:Label id="Label4" runat="server" CssClass="tableHeadingFieldset">Special Information</asp:Label></legend>
									    <TABLE id="tblSpecialInfo" width="100%" align="left" border="0" runat="server">
										    <TR height="5">
											    <TD width="2%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="5%"></TD>
											    <TD width="8%"></TD>
										    </TR>
										    <tr height="5">
											    <TD colSpan="1">&nbsp;</TD>
											    <TD colSpan="5">
												    <asp:label id="Label5" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Free Insurance Amount</asp:label></TD>
											    <TD colSpan="2">
												    <cc1:mstextbox id="txtFreeInsuranceAmt" runat="server" tabIndex="23" Width="100%" CssClass="textFieldRightAlign" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2"></cc1:mstextbox></TD>
											    <TD colSpan="2">&nbsp;</TD>
											    <TD colSpan="1">&nbsp;</TD>
											    <TD colSpan="5">
												    <asp:label id="Label6" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Insurance % Surcharge</asp:label></TD>
											    <TD colSpan="2">
												    <cc1:mstextbox id="txtInsurancePercentSurcharge" runat="server" tabIndex="24" Width="100%" CssClass="textFieldRightAlign" AutoPostBack="False" MaxLength="6" TextMaskType="msNumeric" NumberMaxValue="100" NumberMinValue="0" NumberPrecision="3" NumberScale="2"></cc1:mstextbox></TD>
											    <TD colSpan="2">&nbsp;</TD>
										    </tr>
										    <tr height="25">
											    <TD colSpan="1">
												    <asp:requiredfieldvalidator id="Requiredfieldvalidator4" Width="100%" Runat="server" ControlToValidate="ddlApplyDimWt" Display="None" ErrorMessage="Apply Dim Wt "></asp:requiredfieldvalidator>
												    <asp:label id="Label20" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label>
											    </TD>
											    <TD colSpan="5">
												    <asp:label id="Label16" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Apply Dim Wt</asp:label></TD>
											    <TD colSpan="2">
												    <asp:DropDownList id="ddlApplyDimWt" runat="server" tabIndex="25" Width="100%" CssClass="textField" AutoPostBack="False"></asp:DropDownList></TD>
											    <TD colSpan="2">&nbsp;</TD>
											    <td colspan="1">
												    <asp:requiredfieldvalidator id="Requiredfieldvalidator5" Width="100%" Runat="server" ControlToValidate="ddlPodSlipRequired" Display="None" ErrorMessage="Pod Slip Required "></asp:requiredfieldvalidator>
												    <asp:label id="Label21" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></td>
											    <TD colSpan="5">
												    <asp:label id="Label17" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Pod Slip Required</asp:label></TD>
											    <TD colSpan="2">
												    <asp:DropDownList id="ddlPodSlipRequired" runat="server" tabIndex="26" Width="100%" CssClass="textField"></asp:DropDownList></TD>
											    <TD colSpan="2">&nbsp;</TD>
										    </tr>
										    <tr height="25">
											    <TD align="right" colSpan="1">
												    <asp:requiredfieldvalidator id="Requiredfieldvalidator6" Width="100%" Runat="server" ControlToValidate="ddlApplyEsaSurcharge" Display="None" ErrorMessage="Apply ESA Surcharge "></asp:requiredfieldvalidator>
												    <asp:label id="Label7" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											    <TD colSpan="5">
												    <asp:label id="Label18" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Apply ESA Surcharge</asp:label></TD>
											    <TD colSpan="2">
												    <asp:DropDownList id="ddlApplyEsaSurcharge" runat="server" tabIndex="27" Width="100%" CssClass="textField" AutoPostBack="False"></asp:DropDownList></TD>
											    <TD colSpan="2"></TD>
											    <TD colSpan="1"></TD>
											    <TD colSpan="5">
												    <asp:label id="lblSalesmanID" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Salesman ID</asp:label></TD>
											    <TD colSpan="3">
												    <asp:textbox id="txtSalesmanID" runat="server" tabIndex="28" Width="100%" CssClass="textField" MaxLength="20" ReadOnly="True"></asp:textbox></TD>
											    <TD colSpan="1">
												    <asp:button id="btnSearchSalesman" runat="server" tabIndex="51" Width="30px" CssClass="queryButton" Height="22px" CausesValidation="False" Text="..." OnClick="btnSearchSalesman_Click"></asp:button></TD>
										    </tr>
										    <TR height="25">
											    <TD align="right" colSpan="1"></TD>
											    <TD colSpan="4">
												    <asp:label id="lblCreditTerm" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Credit Term</asp:label></TD>
											    <TD colSpan="3">
												    <cc1:mstextbox id="txtCreditTerm" runat="server" tabIndex="29" Width="100%" CssClass="textFieldRightAlign" AutoPostBack="False" MaxLength="4" TextMaskType="msNumeric" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="4" NumberScale="0"></cc1:mstextbox></TD>
											    <TD colSpan="2"></TD>
											    <TD colSpan="1">&nbsp;</TD>
											    <TD colSpan="5">
												    <asp:label id="lblCreditLimit" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Credit Limit</asp:label></TD>
											    <TD colSpan="4">
												    <cc1:mstextbox id="txtCreditLimit" tabIndex="30" runat="server" Width="100%" CssClass="textFieldRightAlign" Enabled="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2"></cc1:mstextbox></TD>
										    </TR>
										    <tr height="25">
											    <TD colSpan="1">&nbsp;</TD>
											    <TD colSpan="4">
												    <asp:label id="lblCreditOutstanding" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Credit Outstanding</asp:label></TD>
											    <TD colSpan="3">
												    <cc1:mstextbox id="txtCreditOutstanding" runat="server" tabIndex="31" Width="100%" CssClass="textFieldRightAlign" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2"></cc1:mstextbox></TD>
											    <TD colSpan="2"></TD>
											    <TD colSpan="1">&nbsp;</TD>
											    <TD colSpan="5">
												    <asp:label id="lblPaymentMode" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Payment Mode</asp:label></TD>
											    <TD colSpan="2">
												    <asp:radiobutton id="radioBtnCash" runat="server" tabIndex="32" Height="15px" Width="100%" Text="Cash" AutoPostBack="False" Font-Size="Smaller" GroupName="PaymentType" CssClass="tableRadioButton"></asp:radiobutton></TD>
											    <TD colSpan="2">
												    <asp:radiobutton id="radioBtnCredit" runat="server" tabIndex="33" Height="15px" Width="100%" Text="Credit" AutoPostBack="False" Font-Size="Smaller" GroupName="PaymentType" CssClass="tableRadioButton"></asp:radiobutton></TD>
										    </tr>
										    <tr height="25">
											    <TD colSpan="1">&nbsp;</TD>
											    <TD colSpan="4">
												    <asp:label id="lblMBG" runat="server" Height="22px" Width="100%" CssClass="tableLabel">MBG</asp:label></TD>
											    <TD colSpan="3">
												    <asp:DropDownList id="ddlMBG" runat="server" tabIndex="34" Width="100%" CssClass="textField"></asp:DropDownList></TD>
											    <TD colSpan="2"></TD>
											    <TD colSpan="1">&nbsp;</TD>
											    <TD colSpan="5">
												    <asp:label id="lblActiveQuatationNo" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Active Quotation</asp:label></TD>
											    <TD colSpan="4">
												    <cc1:mstextbox id="txtActiveQuatationNo" runat="server" tabIndex="36" Width="100%" CssClass="textField" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore" ReadOnly="True"></cc1:mstextbox></TD>
										    </tr>
										    <tr height="25">
											    <TD colSpan="1">&nbsp;</TD>
											    <TD colSpan="4">
												    <asp:label id="Label19" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Remarks</asp:label></TD>
											    <TD colSpan="15">
												    <asp:textbox id="txtRemark" runat="server" Width="100%" tabIndex="35" CssClass="textField" MaxLength="200"></asp:textbox></TD>
										    </tr>
									    </TABLE>
								    </fieldset>
							    </TD>
						    </TR>
					    </TABLE>
				    </asp:View>
				    <asp:View id="ReferencePage" runat="server">
					<TABLE id="Table2" style="Z-INDEX: 112; WIDTH: 730px" width="730" border="0" runat="server">
						<TR width="100%">
							<TD width="100%">
								<fieldset>
									<legend>
										<asp:Label id="Label8" runat="server" CssClass="tableHeadingFieldset">Reference</asp:Label></legend>
									<TABLE id="tblReferences" width="100%" align="left" border="0" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblCustAccDispTab3" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Agent Account</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCustAccDispTab3" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="3">
												<asp:label id="lblCustNameDispTab3" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Agent Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtCustNameDispTab3" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
										</tr>
										<TR height="10">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD style="HEIGHT: 26px" colSpan="1">&nbsp;</TD>
											<TD colSpan="13" align="left">
												<asp:button id="btnRefViewAll" runat="server" tabIndex="50" Width="80px" CssClass="queryButton" CausesValidation="False" Text="View All"></asp:button>
												<asp:button id="btnRefInsert" runat="server" tabIndex="51" Width="62px" CssClass="queryButton" CausesValidation="False" Text="Insert"></asp:button>
												<asp:button id="btnRefSave" runat="server" tabIndex="52" Width="66px" CssClass="queryButton" Text="Save"></asp:button>
												<asp:button id="btnRefDelete" runat="server" tabIndex="53" Width="62px" CssClass="queryButton" CausesValidation="False" Text="Delete" Visible="True"></asp:button>
												<asp:button id="btnFirstRef" runat="server" tabIndex="54" Width="24px" CssClass="queryButton" CausesValidation="False" Text="|<"></asp:button>
												<asp:button id="btnPreviousRef" runat="server" tabIndex="55" Width="24px" CssClass="queryButton" CausesValidation="False" Text="<"></asp:button>
												<asp:textbox id="txtRefJumpCount" runat="server" tabIndex="56" Width="24px" Height="19px"></asp:textbox>
												<asp:button id="btnNextRef" runat="server" tabIndex="57" Width="24px" CssClass="queryButton" CausesValidation="False" Text=">"></asp:button>
												<asp:button id="btnLastRef" runat="server" tabIndex="58" Width="24px" CssClass="queryButton" CausesValidation="False" Text=">|"></asp:button></TD>
											<TD colSpan="6">
												<asp:label id="lblNumRefRec" runat="server" Width="100%" CssClass="RecMsg" Height="22px"></asp:label></TD>
										</TR>
										<TR height="10">
											<TD colSpan="20"></TD>
										</TR>
										<tr height="25">
											<TD align="right" colSpan="1">
												<asp:requiredfieldvalidator id="validateRefSndRecName" Width="100%" Runat="server" ControlToValidate="supposrtValidator" Display="None" ErrorMessage="Name "></asp:requiredfieldvalidator>
												<asp:requiredfieldvalidator id="valContactPerson" Width="100%" Runat="server" ControlToValidate="suppContactPerson" Display="None" ErrorMessage="Contact Person "></asp:requiredfieldvalidator>
												<asp:requiredfieldvalidator id="valAddress1" Width="100%" Runat="server" ControlToValidate="suppAddress1" Display="None" ErrorMessage="Address1 "></asp:requiredfieldvalidator>
												<asp:requiredfieldvalidator id="valTelephone" Width="100%" Runat="server" ControlToValidate="suppTelephone" Display="None" ErrorMessage="Telephone "></asp:requiredfieldvalidator>
												<asp:requiredfieldvalidator id="valZipcode" Width="100%" Runat="server" ControlToValidate="suppZipcode" Display="None" ErrorMessage="Zipcode "></asp:requiredfieldvalidator>
												<asp:label id="Label2" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefSndRecName" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtRefSndRecName" runat="server" tabIndex="59" Width="100%" CssClass="textField" AutoPostBack="False"></asp:textbox>
												<asp:textbox id="supposrtValidator" runat="server" Width="0" Visible="False">Text</asp:textbox>
												<asp:textbox id="suppContactPerson" runat="server" Width="0" Visible="False">Text</asp:textbox>
												<asp:textbox id="suppAddress1" runat="server" Width="0" Visible="False">Text</asp:textbox>
												<asp:textbox id="suppTelephone" runat="server" Width="0" Visible="False">Text</asp:textbox>
												<asp:textbox id="suppZipcode" runat="server" Width="0" Visible="False">Text</asp:textbox>
											</TD>
											<TD colSpan="1">
												<asp:label id="Label33" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefContactPerson" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Contact Person</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtRefContactPerson" runat="server" tabIndex="60" Width="100%" CssClass="textField" AutoPostBack="False" MaxLength="100"></asp:textbox></TD>
										</tr>
										<tr height="25">
											<TD colSpan="1">
												<asp:label id="Label31" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefAddress1" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Address 1</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtRefAddress1" runat="server" tabIndex="61" Width="100%" CssClass="textField" MaxLength="100"></asp:textbox></TD>
											<TD colSpan="1">
												<asp:label id="Label37" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefType" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Reference Type</asp:label></TD>
											<TD colSpan="2">
												<asp:checkbox id="chkSender" runat="server" tabIndex="66" Width="100%" Height="15px" AutoPostBack="true" Text="Sender" Font-Size="Smaller" CssClass="tableRadioButton"></asp:checkbox></TD>
											<TD colSpan="4">
												<asp:checkbox id="chkRecipient" runat="server" tabIndex="67" Width="100%" Height="15px" AutoPostBack="true" Text="Recipient" Font-Size="Smaller" CssClass="tableRadioButton"></asp:checkbox></TD>
										</tr>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="3">
												<asp:label id="lblRefAddress2" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Address 2</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtRefAddress2" runat="server" tabIndex="62" Width="100%" CssClass="textField" MaxLength="100"></asp:textbox></TD>
											<TD colSpan="1">
												<asp:label id="Label35" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefTelephone" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Telephone</asp:label></TD>
											<TD colSpan="3">
												<asp:textbox id="txtRefTelephone" runat="server" tabIndex="68" Width="100%" CssClass="textField" MaxLength="20"></asp:textbox></TD>
											<TD colSpan="3">&nbsp;</TD>
										</tr>
										<tr height="25">
											<TD align="right" colSpan="1">
												<asp:label id="Label32" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:label></TD>
											<TD colSpan="3">
												<asp:label id="lblRefZipcode" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Postal Code</asp:label></TD>
											<TD colSpan="2">
												<cc1:mstextbox id="txtRefZipcode" runat="server" tabIndex="63" Width="100%" CssClass="textField" AutoPostBack="False" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></TD>
											<TD colSpan="1">
												<asp:button id="btnRefZipcodeSearch" runat="server" tabIndex="64" Width="100%" CssClass="queryButton" CausesValidation="False" Text="..."></asp:button></TD>
											<TD colSpan="1">
												<asp:label id="lblRefState" runat="server" Width="100%" CssClass="tableLabel" Height="22px">State</asp:label></TD>
											<TD colSpan="3">
												<asp:textbox id="txtRefState" runat="server" Width="100%" CssClass="textField" ReadOnly="True"></asp:textbox></TD>
											<TD colSpan="3">
												<asp:label id="lblRefFax" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Fax</asp:label></TD>
											<TD colSpan="3">
												<asp:textbox id="txtRefFax" runat="server" Width="100%" tabIndex="69" CssClass="textField" MaxLength="50"></asp:textbox></TD>
											<TD colSpan="3">&nbsp;</TD>
										</tr>
										<tr height="25">
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="3">
												<asp:label id="lblRefCountry" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Country</asp:label></TD>
											<TD colSpan="2">
												<asp:textbox id="txtRefCountry" runat="server" tabIndex="65" Width="100%" CssClass="textField" AutoPostBack="False" ReadOnly="True"></asp:textbox></TD>
											<TD colSpan="5">
												<asp:RegularExpressionValidator ID="reqExpValidateRefEmail" ControlToValidate="txtRefEmail" Runat="server" Display="None" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Invalid Reference Email"></asp:RegularExpressionValidator></TD>
											<TD colSpan="3">
												<asp:label id="lblRefEmail" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Email</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtRefEmail" runat="server" tabIndex="70" Width="100%" CssClass="textField" MaxLength="50"></asp:textbox></TD>
										</tr>
									</TABLE>
								</fieldset>
							</TD>
						</TR>
					</TABLE>
				</asp:View>
				    <asp:View id="StatusPage" runat="server">
					<TABLE id="Table3" style="Z-INDEX: 112; WIDTH: 730px" width="730" border="0" runat="server">
						<TR width="100%">
							<TD width="100%">
								<fieldset>
									<legend>
										<asp:Label id="Label9" runat="server" CssClass="tableHeadingFieldset">Status & Exception Surcharges</asp:Label></legend>
									<TABLE id="tblStatus" width="100%" align="left" border="0" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblCustAccDispTab4" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Agent Account</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCustAccDispTab4" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="3">
												<asp:label id="lblCustNameDispTab4" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Agent Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtCustNameDispTab4" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
										</tr>
										<TR height="5">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD style="HEIGHT: 26px" colSpan="1">&nbsp;</TD>
											<TD colSpan="12" align="left">
												<asp:button id="btnSCViewAll" runat="server" tabIndex="50" Width="80px" CssClass="queryButton" Height="22px" CausesValidation="False" Text="View All" OnClick="btnSCViewAll_Click"></asp:button>
												<asp:button id="btnSCInsert" runat="server" tabIndex="51" Width="62px" CssClass="queryButton" Height="22px" CausesValidation="False" Text="Insert" OnClick="btnSCInsert_Click"></asp:button>
											</TD>
											<TD colSpan="7">
												<asp:label id="lblSCRecCnt" runat="server" Width="100%" CssClass="RecMsg" Height="22px"></asp:label>
											</TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="19">
												<asp:label id="lblSCMessage" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor">Status Error.....</asp:label></TD>
										</tr>
										<TR>
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="17">
												<asp:datagrid id="dgStatusCodes" style="Z-INDEX: 100" runat="server" Width="100%" SelectedItemStyle-CssClass="gridFieldSelected" OnDeleteCommand="dgStatusCodes_Delete" AllowCustomPaging="True" PageSize="3" AllowPaging="True" OnSelectedIndexChanged="dgStatusCodes_SelectedIndexChanged" OnUpdateCommand="dgStatusCodes_Update" OnCancelCommand="dgStatusCodes_Cancel" OnEditCommand="dgStatusCodes_Edit" OnItemDataBound="dgStatusCodes_Bound" OnItemCommand="dgStatusCodes_Button" OnPageIndexChanged="dgStatusCodes_PageChange" AutoGenerateColumns="False" ItemStyle-Height="20">
													<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
													<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
													<Columns>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;" ButtonType="LinkButton" CommandName="Select">
															<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:ButtonColumn>
														<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
															<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Left"></ItemStyle>
														</asp:EditCommandColumn>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
															<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:ButtonColumn>
														<asp:TemplateColumn HeaderText="Status Code">
															<HeaderStyle Font-Bold="True" Width="15%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle Wrap="False"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblStatusCode" Text='<%#DataBinder.Eval(Container.DataItem,"status_code")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:mstextbox CssClass="gridTextBox" ID="txtStatusCode" Text='<%#DataBinder.Eval(Container.DataItem,"status_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
																</cc1:mstextbox>
																<asp:RequiredFieldValidator ID="scValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtStatusCode" ErrorMessage="Status Code"></asp:RequiredFieldValidator>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton" CommandName="search">
															<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
														</asp:ButtonColumn>
														<asp:TemplateColumn HeaderText="Status Description">
															<HeaderStyle Width="46%" CssClass="gridHeading"></HeaderStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblStatusDescription" Text='<%#DataBinder.Eval(Container.DataItem,"status_description")%>' Runat="server" Enabled="True">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox CssClass="gridTextBox" ID="txtStatusDescription" Text='<%#DataBinder.Eval(Container.DataItem,"status_description")%>' Runat="server" ReadOnly="True" Enabled="True">
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Charge Amount">
															<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblSCChargeAmount" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>' Runat="server" Enabled="True">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:msTextBox CssClass="gridTextBox" ID="txtSCChargeAmount" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>' Runat="server" Enabled="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2">
																</cc1:msTextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
												</asp:datagrid>
											</TD>
											<TD colSpan="2">&nbsp;</TD>
										</TR>
										<TR height="5">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD style="HEIGHT: 26px" colSpan="1">&nbsp;</TD>
											<TD colSpan="12" align="left">
												<asp:button id="btnExInsert" runat="server" tabIndex="51" Width="62px" CssClass="queryButton" Height="22px" CausesValidation="False" Text="Insert" OnClick="btnExInsert_Click"></asp:button>
											</TD>
											<TD colSpan="5">
												<asp:label id="lblExRecCnt" runat="server" Width="100%" CssClass="RecMsg" Height="22px"></asp:label>
											</TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="19">
												<asp:label id="lblEXMessage" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor"></asp:label></TD>
										</tr>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="17">
												<asp:datagrid id="dgExceptionCodes" style="Z-INDEX: 101" runat="server" Width="100%" OnDeleteCommand="dgExceptionCodes_Delete" AllowCustomPaging="True" PageSize="3" AllowPaging="True" OnUpdateCommand="dgExceptionCodes_Update" OnCancelCommand="dgExceptionCodes_Cancel" OnEditCommand="dgExceptionCodes_Edit" OnItemDataBound="dgExceptionCodes_Bound" OnItemCommand="dgExceptionCodes_Button" OnPageIndexChanged="dgExceptionCodes_PageChange" AutoGenerateColumns="False" ItemStyle-Height="20">
													<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
													<Columns>
														<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
															<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
														</asp:EditCommandColumn>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
															<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
														</asp:ButtonColumn>
														<asp:TemplateColumn HeaderText="Exception Code">
															<HeaderStyle Font-Bold="True" Width="15%" CssClass="gridHeading"></HeaderStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblExceptionCode" Text='<%#DataBinder.Eval(Container.DataItem,"exception_code")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:mstextbox CssClass="gridTextBox" ID="txtExceptionCode" Text='<%#DataBinder.Eval(Container.DataItem,"exception_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
																</cc1:mstextbox>
																<asp:RequiredFieldValidator ID="exValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtExceptionCode" ErrorMessage="Exception Code"></asp:RequiredFieldValidator>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton" CommandName="search">
															<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
														</asp:ButtonColumn>
														<asp:TemplateColumn HeaderText="Exception Description">
															<HeaderStyle Width="59%" CssClass="gridHeading"></HeaderStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblExceptionDescription" Text='<%#DataBinder.Eval(Container.DataItem,"exception_description")%>' Runat="server" Enabled="True">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox CssClass="gridTextBox" ID="txtExceptionDescription" Text='<%#DataBinder.Eval(Container.DataItem,"exception_description")%>' Runat="server" Enabled="True" ReadOnly="True">
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Charge Amount">
															<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblEXChargeAmount" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>' Runat="server" Enabled="True">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:msTextBox CssClass="gridTextBox" ID="txtEXChargeAmount" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>' Runat="server" Enable="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2">
																</cc1:msTextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
												</asp:datagrid>
											</TD>
											<TD colSpan="2">&nbsp;</TD>
										</tr>
									</TABLE>
								</fieldset>
							</TD>
						</TR>
					</TABLE>
				</asp:View>
				    <asp:View id="PlacementPage" runat="server">
					<TABLE id="Table4" style="Z-INDEX: 112; WIDTH: 730px" width="730" border="0" runat="server">
						<TR width="100%">
							<TD width="100%">
								<fieldset>
									<legend>
										<asp:Label id="Label10" runat="server" CssClass="tableHeadingFieldset">Payment & Placement</asp:Label></legend>
									<TABLE id="tblPaymentAndPlacement" width="100%" align="left" border="0" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
									</TABLE>
								</fieldset>
							</TD>
						</TR>
					</TABLE>
				</asp:View>
				    <asp:View id="ZipcodePage" runat="server">
					<TABLE id="Table6" style="Z-INDEX: 112; WIDTH: 730px" width="730" border="0" runat="server">
						<TR width="100%">
							<TD width="100%">
								<fieldset>
									<legend>
										<asp:Label id="Label11" runat="server" CssClass="tableHeadingFieldset">Zone & Postal Code</asp:Label></legend>
									<TABLE id="tblZipcode" width="100%" align="left" border="0" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblCustAccDispTab6" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Agent Account</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCustAccDispTab6" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="3">
												<asp:label id="lblCustNameDispTab6" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Agent Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtCustNameDispTab6" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
										</tr>
										<TR height="5">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD style="HEIGHT: 26px" colSpan="1">&nbsp;</TD>
											<TD colSpan="12" align="left">
												<asp:button id="btnZipcodeViewAll" runat="server" tabIndex="50" Width="80px" CssClass="queryButton" Height="22px" CausesValidation="False" Text="View All" OnClick="btnZoneViewAll_Click"></asp:button>
												<asp:button id="btnZipcodeInsert" runat="server" tabIndex="51" Width="62px" CssClass="queryButton" Height="22px" CausesValidation="False" Text="Insert" OnClick="btnZoneInsert_Click"></asp:button>
											</TD>
											<TD colSpan="7">
												<asp:label id="lblZipcodeRecCnt" runat="server" Width="100%" CssClass="RecMsg" Height="22px"></asp:label>
											</TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="19">
												<asp:label id="lblZoneMessage" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor"></asp:label></TD>
										</tr>
										<TR>
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="17">
												<asp:datagrid id="dgAgentZonecodes" style="Z-INDEX: 100" runat="server" Width="100%" SelectedItemStyle-CssClass="gridFieldSelected" OnDeleteCommand="dgAgentZonecodes_Delete" AllowCustomPaging="True" PageSize="3" AllowPaging="True" OnSelectedIndexChanged="dgAgentZonecodes_SelectedIndexChanged" OnUpdateCommand="dgAgentZonecodes_Update" OnCancelCommand="dgAgentZonecodes_Cancel" OnEditCommand="dgAgentZonecodes_Edit" OnItemDataBound="dgAgentZonecodes_Bound" OnItemCommand="dgAgentZonecodes_Button" OnPageIndexChanged="dgAgentZonecodes_PageChange" AutoGenerateColumns="False" ItemStyle-Height="20">
													<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
													<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
													<Columns>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;" ButtonType="LinkButton" CommandName="Select">
															<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:ButtonColumn>
														<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
															<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Left"></ItemStyle>
														</asp:EditCommandColumn>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
															<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:ButtonColumn>
														<asp:TemplateColumn HeaderText="Zone Code">
															<HeaderStyle Font-Bold="True" Width="8%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle Wrap="False"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblZoneCode" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"zone_code")%>'>
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:mstextbox CssClass="gridTextBox" ID="txtZoneCode" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"zone_code")%>' Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" ReadOnly="True">
																</cc1:mstextbox>
																<asp:RequiredFieldValidator ID="ZipCodeValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtZoneCode" ErrorMessage="Zone Code"></asp:RequiredFieldValidator>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton" CommandName="search">
															<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:ButtonColumn>
														<asp:TemplateColumn HeaderText="Zone Description">
															<HeaderStyle Font-Bold="True" Width="16%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle Wrap="False"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblZoneDescription" Runat="server" Enabled="True" Text='<%#DataBinder.Eval(Container.DataItem,"zone_description")%>'>
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox Runat="server" ID="txtZoneDescription" Runat="server" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"zone_description")%>'>
																</asp:TextBox>
																</cc1:mstextbox>
															</EditItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
												</asp:datagrid>
											</TD>
											<TD colSpan="2">&nbsp;</TD>
										</TR>
										<TR height="5">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD style="HEIGHT: 26px" colSpan="1">&nbsp;</TD>
											<TD colSpan="12" align="left">
												<asp:button id="btnAgentCostInsert" runat="server" tabIndex="51" Width="62px" CssClass="queryButton" Height="22px" CausesValidation="False" Text="Insert" OnClick="btnAgentZoneZipInsert_Click"></asp:button>
											</TD>
											<TD colSpan="5">
												<asp:label id="lblAgentCostRecCnt" runat="server" Width="100%" CssClass="RecMsg" Height="22px"></asp:label>
											</TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="19">
												<asp:label id="lblAgentZoneZipMessage" runat="server" Height="22px" Width="100%" CssClass="errorMsgColor"></asp:label></TD>
										</tr>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="17">
												<asp:datagrid id="dgZoneZipcodIncluded" runat="server" Width="630px" ItemStyle-Height="20" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" AllowCustomPaging="True" OnDeleteCommand="dgZoneZipcodIncluded_Delete" OnPageIndexChanged="dgAgentZoneZip_PageChange" OnEditCommand="dgAgentZoneZip_Edit" OnCancelCommand="dgAgentZoneZip_Cancel" OnUpdateCommand="dgAgentZoneZip_Update">
													<ItemStyle Height="20px"></ItemStyle>
													<Columns>
														<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
															<HeaderStyle Width="4%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
														</asp:EditCommandColumn>
														<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
															<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
														</asp:ButtonColumn>
														<asp:TemplateColumn HeaderText="Postal Code">
															<HeaderStyle Width="20%" Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblZoneZipCode" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>'>
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:mstextbox CssClass="gridTextBox" ID="txtZoneZipCode" Runat="server" Enabled="True" MaxLength="10" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' TextMaskType="msUpperAlfaNumericWithUnderscore" ReadOnly="true">
																</cc1:mstextbox>
																<asp:RequiredFieldValidator ID="Requiredfieldvalidator1" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtZoneZipCode" ErrorMessage="Zipcode is required field"></asp:RequiredFieldValidator>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="State Name">
															<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblZoneZipState" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"state_name")%>'>
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox CssClass="gridTextBox" ID="txtZoneZipState" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"state_name")%>' ReadOnly="true">
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Country">
															<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblZoneZipCountry" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>'>
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox CssClass="gridTextBox" ID="txtZoneZipCountry" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' ReadOnly="true">
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="ESA Surcharge">
															<HeaderStyle Width="35%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblESA" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"esa_surcharge","{0:n}")%>'>
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<cc1:mstextbox CssClass="gridTextBox" ID="txtESA" Text='<%#DataBinder.Eval(Container.DataItem,"esa_surcharge","{0:n}")%>' Runat="server" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2">
																</cc1:mstextbox>
															</EditItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
												</asp:datagrid>
											</TD>
											<TD colSpan="2">&nbsp;</TD>
										</tr>
									</TABLE>
								</fieldset>
							</TD>
						</TR>
					</TABLE>
				</asp:View>
				    <asp:View id="QuotationPage" runat="server">
					<TABLE id="Table5" style="Z-INDEX: 112; WIDTH: 730px" width="730" border="0" runat="server">
						<TR width="100%">
							<TD width="100%">
								<fieldset>
									<legend>
										<asp:Label id="Label12" runat="server" CssClass="tableHeadingFieldset">Quotation Status</asp:Label></legend>
									<TABLE id="tblQuotationStatus" width="100%" align="left" border="0" runat="server">
										<TR height="5">
											<TD width="2%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="5%"></TD>
											<TD width="8%"></TD>
										</TR>
										<tr height="25">
											<TD colSpan="1">&nbsp;</TD>
											<TD colSpan="5">
												<asp:label id="lblCustAccDispTab5" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Agent Account</asp:label></TD>
											<TD colSpan="4">
												<asp:textbox id="txtCustAccDispTab5" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
											<TD align="right" colSpan="1"></TD>
											<TD colSpan="3">
												<asp:label id="lblCustNameDispTab5" runat="server" Height="22px" Width="100%" CssClass="tableLabel">Agent Name</asp:label></TD>
											<TD colSpan="6">
												<asp:textbox id="txtCustNameDispTab5" runat="server" Width="100%" CssClass="textField" Enabled="False"></asp:textbox></TD>
										</tr>
										<TR height="5">
											<TD colSpan="20"></TD>
										</TR>
										<TR height="25">
											<TD style="HEIGHT: 26px" colSpan="1">&nbsp;</TD>
											<TD colSpan="13" align="left">
												<asp:button id="btnQuoteViewAll" runat="server" tabIndex="50" Width="80px" CssClass="queryButton" Height="22px" CausesValidation="False" Text="View All"></asp:button></TD>
											<TD colSpan="6">
												<asp:label id="lblNumQuoteRec" runat="server" Width="100%" CssClass="RecMsg" Height="22px"></asp:label></TD>
										</TR>
										<tr>
											<td colspan="20">
												<asp:datagrid id="dgQuotation" style="Z-INDEX: 104; LEFT: 18px; POSITION: absolute; TOP: 125px" runat="server" OnItemDataBound="OnItemBound_Quotation" AllowCustomPaging="True" PageSize="7" PagerStyle-PageButtonCount="7" AllowPaging="True" OnPageIndexChanged="OnQuotation_PageChange" AutoGenerateColumns="False" ItemStyle-Height="20" Width="580px">
													<Columns>
														<asp:TemplateColumn HeaderText="Quotation No.">
															<HeaderStyle Width="40%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblQuoteNo" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"Quotation_No")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Version">
															<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblQuoteVersion" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"Quotation_version")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Date">
															<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblQuoteDate" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"Quotation_Date","{0:dd/MM/yyyy}")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Status">
															<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
															<ItemStyle CssClass="gridField"></ItemStyle>
															<ItemTemplate>
																<asp:Label CssClass="gridLabel" ID="lblQuoteStatus" Visible="True" Text='<%#DataBinder.Eval(Container.DataItem,"Quotation_Status")%>' Runat="server">
																</asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
													<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
												</asp:datagrid>
											</td>
										</tr>
									</TABLE>
								</fieldset>
							</TD>
						</TR>
					</TABLE>
				</asp:View>
			</asp:MultiView>
            <asp:validationsummary id="custValidationSummary" style="Z-INDEX: 113; LEFT: 65px; POSITION: absolute; TOP: 70px" Width="346px" Height="36px" Runat="server" ShowMessageBox="True" HeaderText="Please enter the missing fields." DisplayMode="BulletList" ShowSummary="False"></asp:validationsummary><asp:label id="lblCPErrorMessage" style="Z-INDEX: 114; LEFT: 54px; POSITION: absolute; TOP: 104px" runat="server" Width="395px" Height="19px" CssClass="errorMsgColor"></asp:label></FORM>
	</BODY>
</HTML>
