using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.applicationpages;
using com.ties.DAL;
using TIESDAL;

namespace TIES
{
	/// <summary>
	/// Summary description for CustomsFreightCollectCharges.
	/// </summary>
	public class CustomsFreightCollectCharges : BasePage
	{
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox txtJobEntryNumber;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.TextBox txtHouseAwbNumber;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.TextBox txtCustomer;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.TextBox txtShipFlightNo;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.TextBox txtFrom2;
		protected System.Web.UI.WebControls.TextBox txtFrom3;
		protected System.Web.UI.WebControls.TextBox txtInvoicePrinted1;
		protected System.Web.UI.WebControls.TextBox txtInvoicePrinted2;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.TextBox txtAmount;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.TextBox txtCaf;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.DropDownList ddlCurrency;
		protected System.Web.UI.WebControls.TextBox txtCurrency;
		protected System.Web.UI.WebControls.TextBox txtRateDate;
		protected System.Web.UI.WebControls.TextBox txtAmountLocalCurrency;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.WebControls.TextBox txtFrom1;
		protected com.common.util.msTextBox txtFreightDate;
		protected System.Web.UI.WebControls.Label Label8;
		protected com.common.util.msTextBox txtAmount2;
		protected com.common.util.msTextBox txtAmount3;
		protected System.Web.UI.WebControls.TextBox txtArrivalDate;
		protected System.Web.UI.WebControls.Button btnSave;

		Utility Utility = null;
		String strEnterpriseID = null;
		String strAppID = null;
		protected System.Web.UI.WebControls.Button btnExecQryHidden;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.TextBox txtExpressDiscount;
		String strUserLoggin = null; 
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			Utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = Utility.GetAppID();
			strEnterpriseID = Utility.GetEnterpriseID();
			strUserLoggin = Utility.GetUserID();
			GetAmountDigit();

			if(this.IsPostBack==false)
			{
				GenCurrency();
				SetInitialFocus(txtJobEntryNumber);

				//---------- ����ź�ç����Ѻ  ���¡��ҹ˹�ҹ��ҡ˹�����
				if(Request.QueryString["jobentryno"] != null && Request.QueryString["jobentryno"].ToString().Trim() != "")
				{
					txtJobEntryNumber.Text=Request.QueryString["jobentryno"].ToString().Trim();
					btnExecuteQuery_Click(null,null);
				}
                //aw_Khatawut comment 2019-06-14 : DMSUpgrade 2.4.1) After searching Job Status, there should be able to update data on the search result
                //if(Request.QueryString["pagemode"] != null && Request.QueryString["pagemode"].ToString().Trim() == "popup")
                //{
                //	btnQuery.Visible=false;
                //	btnExecuteQuery.Visible=false;
                //	btnSave.Visible = false;
                //}
                //----------
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnExecQryHidden.Click += new System.EventHandler(this.btnExecQryHidden_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void GetAmountDigit()
		{
			// Set NumberScale to textbox
			txtAmount3.NumberScale = 2;
		}

		private void GenCurrency()
		{
			DataSet dsCurrencyName = CustomsFreightCollectChargesDAL.GetCurrencies(strAppID, strEnterpriseID); 
			DataRow drCurencyEmpty = dsCurrencyName.Tables[0].NewRow();
			drCurencyEmpty["CurrencyCode"] = "";
			dsCurrencyName.Tables[0].Rows.InsertAt(drCurencyEmpty,0);
			ddlCurrency.DataSource = dsCurrencyName.Tables[0];
			ddlCurrency.DataTextField = "CurrencyCode";
			ddlCurrency.DataValueField = "CurrencyCode";
			ddlCurrency.DataBind(); 
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			if(txtJobEntryNumber.Text != "" || txtHouseAwbNumber.Text != "")
			{

				DataSet dsCustomsFreight = CustomsFreightCollectChargesDAL.Customs_Freight_Collect_Charges(strAppID, strEnterpriseID, strUserLoggin,
					"0",txtHouseAwbNumber.Text, txtJobEntryNumber.Text, "","","","");

				System.Data.DataTable dtStatus = dsCustomsFreight.Tables[0];
				System.Data.DataTable dtCustomsFreightCollect= dsCustomsFreight.Tables[1];

				if(!dtStatus.Rows[0]["ErrorCode"].ToString().Equals("0"))
				{
					lblError.Text = dtStatus.Rows[0]["ErrorMessage"].ToString();
				}
				else
				{
					clearscreen();
					if(dtCustomsFreightCollect.Rows.Count > 0)
					{
						//lblError.Text = dtStatus.Rows[0]["ErrorMessage"].ToString();
						BindCustomsFreightCollect(dtCustomsFreightCollect);
					}
					else
					{
						lblError.Text = dtStatus.Rows[0]["ErrorMessage"].ToString();
					}
				}
				
			}			
			else
			{
				lblError.Text = "On Execute Query you must specify either Job Entry Number or House AWB Number";
				SetInitialFocus(txtJobEntryNumber);
			}
		}

		private void BindCustomsFreightCollect(DataTable dt)
		{
			DataRow dr = dt.Rows[0];

			// consignment_no
			if(dr["consignment_no"] != DBNull.Value && dr["consignment_no"].ToString() !="")
			{
				txtHouseAwbNumber.Text = dr["consignment_no"].ToString();
			}

			// payerid
			if(dr["payerid"] != DBNull.Value && dr["payerid"].ToString() !="")
			{
				Session["payerid"] = dr["payerid"].ToString();
			}

			// JobEntryNo
			if(dr["JobEntryNo"] != DBNull.Value && dr["JobEntryNo"].ToString() !="")
			{
				txtJobEntryNumber.Text = dr["JobEntryNo"].ToString();
			}
			
			String format_digit = "N" + GetCurrencyDecimalOfEnterpriseConfiguration();

			SetInitialFocus(txtAmount3);
			// Amount
			if(dr["Amount"] != DBNull.Value && dr["Amount"].ToString() !="")
			{
				txtAmount3.Text = Convert.ToDecimal(dr["Amount"]).ToString("N2");
			}

			// FreightCollect_DT
			if(dr["FreightCollect_DT"] != DBNull.Value && dr["FreightCollect_DT"].ToString() !="")
			{
				txtFreightDate.Text = Convert.ToDateTime(dr["FreightCollect_DT"]).ToString("dd/MM/yyyy");
			}

			// CurrencyCode
			if(dr["CurrencyCode"] != DBNull.Value && dr["CurrencyCode"].ToString() !="")
			{
				ddlCurrency.SelectedValue = dr["CurrencyCode"].ToString();
			}

			// CustomerName
			if(dr["CustomerName"] != DBNull.Value && dr["CustomerName"].ToString() !="")
			{
				txtCustomer.Text = dr["CustomerName"].ToString();
			}

			// ShipFlightNo
			if(dr["ShipFlightNo"] != DBNull.Value && dr["ShipFlightNo"].ToString() !="")
			{
				txtShipFlightNo.Text = dr["ShipFlightNo"].ToString();
			}

			// ArrivalDate
			if(dr["ArrivalDate"] != DBNull.Value && dr["ArrivalDate"].ToString() !="")
			{
				txtArrivalDate.Text = Convert.ToDateTime(dr["ArrivalDate"]).ToString("dd/MM/yyyy");
			}

			// LoadingPortCity
			if(dr["LoadingPortCity"] != DBNull.Value && dr["LoadingPortCity"].ToString() !="")
			{
				txtFrom1.Text = dr["LoadingPortCity"].ToString();
			}

			// LoadingPortCountry
			if(dr["LoadingPortCountry"] != DBNull.Value && dr["LoadingPortCountry"].ToString() !="")
			{
				txtFrom2.Text = dr["LoadingPortCountry"].ToString();
			}

			// CountryName
			if(dr["CountryName"] != DBNull.Value && dr["CountryName"].ToString() !="")
			{
				txtFrom3.Text = dr["CountryName"].ToString();
			}

			// Invoice_PrintedBy
			if(dr["Invoice_PrintedBy"] != DBNull.Value && dr["Invoice_PrintedBy"].ToString() !="")
			{
				txtInvoicePrinted1.Text = dr["Invoice_PrintedBy"].ToString();
			}

			// Invoice_PrintedDT
			if(dr["Invoice_PrintedDT"] != DBNull.Value && dr["Invoice_PrintedDT"].ToString() !="")
			{
				txtInvoicePrinted2.Text = Convert.ToDateTime(dr["Invoice_PrintedDT"]).ToString("dd/MM/yyyy");
			}

			// CurrencyAdjFactor
			if(dr["CurrencyAdjFactor"] != DBNull.Value && dr["CurrencyAdjFactor"].ToString() !="")
			{
				txtCaf.Text = Convert.ToDecimal(dr["CurrencyAdjFactor"]).ToString(format_digit);
			}

			// CurrencyRate
			if(dr["CurrencyRate"] != DBNull.Value && dr["CurrencyRate"].ToString() !="")
			{
				txtCurrency.Text = Convert.ToDecimal(dr["CurrencyRate"]).ToString("N4");
			}

			// CurrencyRateDate
			if(dr["CurrencyRateDate"] != DBNull.Value && dr["CurrencyRateDate"].ToString()!="")
			{
				txtRateDate.Text = Convert.ToDateTime(dr["CurrencyRateDate"]).ToString("dd/MM/yyyy");
			}

			// Valuation2
			if(dr["Valuation2"] != DBNull.Value && dr["Valuation2"].ToString() != "")
			{
				txtAmountLocalCurrency.Text = Convert.ToDecimal(dr["Valuation2"]).ToString(format_digit);
			}

			// ExpressDiscount
			if(dr["ExpressDiscount"] != DBNull.Value && dr["ExpressDiscount"].ToString() != "")
			{
				txtExpressDiscount.Text = Convert.ToDecimal(dr["ExpressDiscount"]).ToString(format_digit);
			}
			// ExpressFlag
			if(dr["ExpressFlag"] != DBNull.Value && dr["ExpressFlag"].ToString() != "")
				if(dr["ExpressFlag"].ToString().Equals("1"))
					btnSave.Enabled = false;
				else
					btnSave.Enabled = true;

			btnExecuteQuery.Enabled = false;
			//btnSave.Enabled = true;
			txtJobEntryNumber.Enabled = false;
			txtHouseAwbNumber.Enabled = false;
		
		}

		private void clearscreen()
		{
			lblError.Text = "";
			txtJobEntryNumber.Text = "";
			txtHouseAwbNumber.Text = "";
			txtCustomer.Text = "";
			txtShipFlightNo.Text = "";
			txtArrivalDate.Text = "";
			txtFrom1.Text = "";
			txtFrom2.Text = "";
			txtFrom3.Text = "";
			txtInvoicePrinted1.Text = "";
			txtInvoicePrinted2.Text = "";
			txtAmount3.Text = "";
			txtCaf.Text = "";
			txtFreightDate.Text = "";
			ddlCurrency.SelectedIndex = 0;
			txtCurrency.Text = "";
			txtRateDate.Text = "";
			txtAmountLocalCurrency.Text = "";
			txtExpressDiscount.Text = string.Empty;
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			clearscreen();
			btnExecuteQuery.Enabled = true;
			btnSave.Enabled = false;
			txtJobEntryNumber.Enabled = true;
			txtHouseAwbNumber.Enabled = true;
			SetInitialFocus(txtJobEntryNumber);
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{

			if(txtFreightDate.Text.Equals(String.Empty))
			{
				lblError.Text = "Not saved - Freight Collect Charge date is required";
				SetInitialFocus(txtFreightDate);
				return;
			}

			if(ddlCurrency.SelectedValue.Equals(String.Empty))
			{
				lblError.Text = "Not saved - Currency code is required";
				SetInitialFocus(ddlCurrency);
				return;
			}

			string strFreightDate = DateTime.ParseExact(txtFreightDate.Text,"dd/MM/yyyy",null).ToString("yyyy-MM-dd");

			DataSet dsResult = CustomsFreightCollectChargesDAL.Customs_Freight_Collect_Charges(strAppID, strEnterpriseID, strUserLoggin,
				"1",txtHouseAwbNumber.Text, txtJobEntryNumber.Text, Convert.ToString(Session["payerid"]), txtAmount3.Text, strFreightDate, ddlCurrency.SelectedItem.Value);

			lblError.Text = Convert.ToString(dsResult.Tables[0].Rows[0]["ErrorMessage"]);

			DataTable dtCustomsFreightCollect = dsResult.Tables[1];

			if(dtCustomsFreightCollect.Rows.Count > 0)
			{
				BindCustomsFreightCollect(dtCustomsFreightCollect);
			}
		}

		public int GetCurrencyDecimalOfEnterpriseConfiguration()
		{
			DataSet dsEprise = SysDataManager1.GetEnterpriseProfile(strAppID, strEnterpriseID);
			DataRow dr = dsEprise.Tables[0].Rows[0];
			
			int number_digit = 0;
			if(dr["currency_decimal"].ToString()!="" && dr["currency_decimal"] != DBNull.Value)
			{
				number_digit = Convert.ToInt32(dr["currency_decimal"].ToString());
			}

			return number_digit;
		}

		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				System.Text.StringBuilder s = new System.Text.StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}

		private void ddlCurrency_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string currencySelected = ddlCurrency.SelectedItem.Value;
			
			if(currencySelected.Equals(String.Empty))
			{
				txtCurrency.Text = "";
				txtAmountLocalCurrency.Text = "";
				txtRateDate.Text = "";
				return;
			}

			if(txtFreightDate.Text.Equals(String.Empty))
			{
				txtAmountLocalCurrency.Text = "";
				return;
			}

			string strFreightDate = DateTime.ParseExact(txtFreightDate.Text,"dd/MM/yyyy",null).ToString("yyyy-MM-dd");
			DataSet dsCurrency = CustomsFreightCollectChargesDAL.GetExchangeRate(strAppID, strEnterpriseID, currencySelected, strFreightDate);
			
			if(dsCurrency != null)
			{
				if(dsCurrency.Tables[0].Rows.Count > 0)
				{
					txtCurrency.Text = dsCurrency.Tables[0].Rows[0]["exchange_rate"].ToString();
					txtRateDate.Text = DateTime.ParseExact(dsCurrency.Tables[0].Rows[0]["rate_date"].ToString(),"yyyy-MM-dd",null).ToString("dd/MM/yyyy");
					calculate();
				}
				else
				{
					txtCurrency.Text = "";
					txtRateDate.Text = "";
					txtAmountLocalCurrency.Text = "";
				}
			}

			SetInitialFocus(ddlCurrency);
		}

		public void calculate()
		{
			if(txtAmount3.Text.Equals(String.Empty))
			{
				txtAmountLocalCurrency.Text = "";
				return;
			}

			if(txtCaf.Text.Equals(String.Empty))
			{
				txtAmountLocalCurrency.Text = "";
				return;
			}

			if(ddlCurrency.SelectedValue.Equals(String.Empty))
			{
				txtCurrency.Text = "";
				txtAmountLocalCurrency.Text = "";
			}

			if(txtCurrency.Text.Equals(String.Empty))
			{
				txtAmountLocalCurrency.Text = "";
				return;
			}

			if(txtCaf.Text.Equals(String.Empty))
			{
				txtAmountLocalCurrency.Text = "";
				return;
			}

			// (Amount(AUD)/Rate) * (1. + (CAF(%)/100.))
			Decimal amount = Convert.ToDecimal(txtAmount3.Text);
			Decimal rate = Convert.ToDecimal(txtCurrency.Text);
			Decimal caf = Convert.ToDecimal(txtCaf.Text);
			Decimal amountLC = Convert.ToDecimal(Convert.ToDecimal(amount/rate).ToString("N2")) * (1 + (caf/100));
			txtAmountLocalCurrency.Text = Convert.ToDecimal(amountLC).ToString("N2");


		}

		private void Amount_TextChanged(object sender, System.EventArgs e)
		{
			calculate();
		}

		private void btnExecQryHidden_Click(object sender, System.EventArgs e)
		{
			btnExecuteQuery_Click(null,null);
		}
	}
}
