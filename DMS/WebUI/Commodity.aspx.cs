using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.common.applicationpages;
namespace com.ties
{
	/// <summary>
	/// Summary description for Commodity.
	/// </summary>
	public class Commodity : BasePage
	{
		
		protected System.Web.UI.WebControls.DataGrid dgCommodity;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnInsert;

		private SessionDS m_sdsCommodity;
		private String m_strAppID;
		private String m_strEnterpriseID;
		private String m_strCulture;
		protected System.Web.UI.WebControls.Label lblValCommodity;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		private DataView m_dvTypeOptions = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			if(!Page.IsPostBack)
			{			
				ViewState["m_strCode"] = "";
				ViewState["index"]=0;
				ViewState["prevRow"] = 0;
				ViewState["SCMode"] = ScreenMode.None;
				ViewState["SCOperation"] = Operation.None;
				
				dgCommodity.EditItemIndex = -1;
				m_sdsCommodity = SysDataManager1.GetEmptyCommodity(1);
				enableEditColumn(false);
				BindCommodity();
				Session["SESSION_DS1"] = m_sdsCommodity;
				Session["QUERY_DS"] = m_sdsCommodity;
				//----START QUERYING------
				lblValCommodity.Text = "";
				m_sdsCommodity = SysDataManager1.GetEmptyCommodity(1);
				dgCommodity.CurrentPageIndex = 0;
				btnExecuteQuery.Enabled = true;
				ViewState["SCMode"] = ScreenMode.Query;
				ViewState["SCOperation"] = Operation.None;
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				ViewState["m_strCode"] = "";
				ViewState["prevRow"]=0;
				ViewState["index"]=0;
				dgCommodity.EditItemIndex = 0;
				enableEditColumn(false);
				m_dvTypeOptions = CreateTypeOptions(true);
				BindCommodity();
			}
			else
			{
				m_sdsCommodity = (SessionDS)Session["SESSION_DS1"];
				int iMode = (int) ViewState["SCMode"];
				switch(iMode)
				{
					case (int) ScreenMode.Query:
						m_dvTypeOptions = CreateTypeOptions(false);
						break;
					case (int) ScreenMode.Insert:
						m_dvTypeOptions = CreateTypeOptions(false);
						break;
					default:
						m_dvTypeOptions = CreateTypeOptions(true);
						break;
				}

				if((int)ViewState["SCOperation"]==(int)Operation.Update)
				{
					m_dvTypeOptions = CreateTypeOptions(false);
				}
			}
			PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}
		/// <summary>
		/// To bind data to datagrid
		/// </summary>
		private void BindCommodity()
		{
			dgCommodity.VirtualItemCount = System.Convert.ToInt32(m_sdsCommodity.QueryResultMaxSize);
			dgCommodity.DataSource = m_sdsCommodity.ds;
			dgCommodity.DataBind();
			Session["SESSION_DS1"] = m_sdsCommodity;
		}
		/// <summary>
		/// On editing to allow text input
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnEdit_Commodity(object sender, DataGridCommandEventArgs e)
		{		
			lblValCommodity.Text = "";
			int rowIndex = e.Item.ItemIndex;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			ViewState["index"] = rowIndex;
			Label lblCode = (Label)dgCommodity.Items[rowIndex].FindControl("lblCode");
			ViewState["m_strCode"] = lblCode.Text;
			
			if((int)ViewState["SCOperation"] == (int)Operation.Insert)
				ViewState["SCMode"]= ScreenMode.Insert;
			else
				ViewState["SCMode"] = ScreenMode.None;

			if(dgCommodity.EditItemIndex >0)
			{
				msTextBox txtCode = (msTextBox)dgCommodity.Items[rowIndex].FindControl("txtCode");				
				if(txtCode !=null && txtCode.Text=="")	
				{
					ViewState["m_strCode"] = txtCode.Text;
					m_sdsCommodity.ds.Tables[0].Rows.RemoveAt(dgCommodity.EditItemIndex);
				}
			}
			dgCommodity.EditItemIndex = rowIndex;
			BindCommodity();
	
		}
		/// <summary>
		/// To return to viewing mode
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnCancel_Commodity(object sender, DataGridCommandEventArgs e)
		{		
			lblValCommodity.Text = "";
			dgCommodity.EditItemIndex = -1;
			m_sdsCommodity = (SessionDS)Session["SESSION_DS1"];
			int rowIndex = e.Item.ItemIndex;
			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
			{
				m_sdsCommodity.ds.Tables[0].Rows.RemoveAt(rowIndex);
			}
			enableDeleteColumn(true);
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			BindCommodity();
	
		}
		/// <summary>
		/// To update new text entered into database
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnUpdate_Commodity(object sender, DataGridCommandEventArgs e)
		{		
			int rowIndex = e.Item.ItemIndex;
			updateLatestComodity(rowIndex);
			ViewState["SCOperation"] = Operation.Update;
			m_sdsCommodity = (SessionDS)Session["SESSION_DS1"];
			DataSet dsCommodity = m_sdsCommodity.ds.GetChanges();
			SessionDS sdsCommodity = new SessionDS();
			sdsCommodity.ds = dsCommodity;
			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
			{
					try
					{
						SysDataManager1.InsertCommodity(sdsCommodity,m_strAppID,m_strEnterpriseID);
						lblValCommodity.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());							
						m_sdsCommodity.ds.Tables[0].Rows[rowIndex].AcceptChanges();
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;

						if(strMsg.IndexOf("duplicate key") != -1  || strMsg.IndexOf("unique") != -1)
						{
							lblValCommodity.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
						} 
						return;				
					}
					catch(Exception err)
					{
						String msg = err.ToString();
						lblValCommodity.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
					}				
			}
			else
			{
				try
				{
					SysDataManager1.UpdateCommodity(sdsCommodity,m_strAppID,m_strEnterpriseID);
					lblValCommodity.Text=Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());	
					m_sdsCommodity.ds.Tables[0].Rows[rowIndex].AcceptChanges();
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					strMsg = appException.InnerException.Message;

					if(strMsg.IndexOf("duplicate key") != -1  || strMsg.IndexOf("unique") != -1)
					{
						lblValCommodity.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
					} 
					return;				
				}
				catch(Exception err)
				{
					String msg = err.ToString();
					lblValCommodity.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
				}			
				
			}

			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			ViewState["SCOperation"] = Operation.None;
			dgCommodity.EditItemIndex = -1;
			BindCommodity();	
		}

		/// <summary>
		/// To delete a particular row in database
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnDelete_Commodity(object sender, DataGridCommandEventArgs e)
		{	
			if (dgCommodity.EditItemIndex == e.Item.ItemIndex)
			{
				return;
			}
			if(checkEmptyFields(e.Item.ItemIndex))
			{
				ViewState["SCOperation"] = Operation.Delete;
				int rowNum = e.Item.ItemIndex;				
				DeleteCommodity(rowNum);								
				showCurrentPage();
				
				if(dgCommodity.EditItemIndex < -1)
				{
					m_sdsCommodity.ds.Tables[0].Rows.RemoveAt(dgCommodity.EditItemIndex);
					//original -- m_sdsCommodity.ds.Tables[0].Rows.RemoveAt(dgCommodity.EditItemIndex-1);
				}	
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				BindCommodity();
			}

		}
		/// <summary>
		/// To delete the record
		/// </summary>
		/// <param name="rowIndex">to delete at this row index</param>
		private void DeleteCommodity(int rowIndex)
		{
			m_sdsCommodity = (SessionDS)Session["SESSION_DS1"];
			DataRow dr = m_sdsCommodity.ds.Tables[0].Rows[rowIndex];
			String strCode = (String)dr[0];
			try
			{
				SysDataManager1.DeleteCommodity(m_strAppID,m_strEnterpriseID,strCode);
				m_sdsCommodity.ds.Tables[0].Rows.RemoveAt(rowIndex);
				lblValCommodity.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());	
			}
			catch(Exception err)
			{
				String strMsg=err.Message;
				if(strMsg.IndexOf("FK") != -1)
					lblValCommodity.Text =Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_COMMODITY_TRANS",utility.GetUserCulture());
				else if(strMsg.IndexOf("child record found") != -1)
					lblValCommodity.Text=Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_COMMODITY_TRANS",utility.GetUserCulture());
				else 
					lblValCommodity.Text=strMsg;

				return;
			}
			
		}
		/// <summary>
		/// To show code colum or not
		/// </summary>
		/// <param name="toEnable">to enable or not</param>
		private void enableCodeColumn(bool toEnable)
		{	
			dgCommodity.Columns[2].Visible=toEnable;//Code column
		}
		/// <summary>
		/// to show delete column or not
		/// </summary>
		/// <param name="toEnable"></param>
		private void enableDeleteColumn(bool toEnable)
		{
			dgCommodity.Columns[1].Visible=toEnable;//Delete column
		}
		/// <summary>
		/// To add another row
		/// </summary>
//		private void AddRow()
//		{
//			m_sdsCommodity = (SessionDS)Session["SESSION_DS1"];
//			DataRow drNew = m_sdsCommodity.ds.Tables[0].NewRow();
//
//			drNew[0] = "";
//			drNew[1] = "";
//			
//			m_sdsCommodity.ds.Tables[0].Rows.Add(drNew);
//			
//		}

		private void AddRow()
		{
			SysDataManager1.AddNewRowInCommodityDS(ref m_sdsCommodity);
			BindCommodity();
		}
		/// <summary>
		/// To get text from textboxes and store into a dataset
		/// </summary>
		/// <param name="rowIndex"></param>
		private void updateLatestComodity(int rowIndex)
		{
			msTextBox txtCode = (msTextBox)dgCommodity.Items[rowIndex].FindControl("txtCode");
			DropDownList ddType = (DropDownList)dgCommodity.Items[rowIndex].FindControl("ddType");
			TextBox txtDescription = (TextBox)dgCommodity.Items[rowIndex].FindControl("txtDescription");
			DataRow dr = m_sdsCommodity.ds.Tables[0].Rows[rowIndex];
			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
			{
				dr[0] = txtCode.Text.ToString();
			}
			else
			{
				dr[0] = (String)ViewState["m_strCode"];
			}
			String strType = ddType.SelectedItem.Value;
			dr[1] = strType;
			String strDesc = txtDescription.Text.ToString();
			dr[2] = strDesc;
			
			Session["SESSION_DS1"] = m_sdsCommodity;
		}
		/// <summary>
		/// To check if there are empty fields
		/// </summary>
		/// <param name="rowIndex">to check at this row index</param>
		/// <returns></returns>
		private bool checkEmptyFields(int rowIndex)
		{			
			msTextBox txtCode = (msTextBox)dgCommodity.Items[rowIndex].FindControl("txtCode");
			DropDownList ddType = (DropDownList)dgCommodity.Items[rowIndex].FindControl("ddType");
			TextBox txtDescription = (TextBox)dgCommodity.Items[rowIndex].FindControl("txtDescription");
			if(txtCode!=null && txtDescription!=null && ddType!=null)
			{
				DataRow dr = m_sdsCommodity.ds.Tables[0].Rows[rowIndex];
				if((int)ViewState["SCMode"] == (int)ScreenMode.Query)
				{
					dr[0] = txtCode.Text;
					
				}
				
				dr[1] = ddType.SelectedItem.Value;
				dr[2] = txtDescription.Text;

				Session["SESSION_DS1"] = m_sdsCommodity;
				Session["QUERY_DS"] = m_sdsCommodity;
				return false;
			}
			return true;
		}
		/// <summary>
		/// to check if code is an empty text box and do Validation
		/// </summary>
		/// <param name="rowIndex"></param>
		/// <returns></returns>
		private bool isCodeEmpty(int rowIndex)
		{
			msTextBox txtCode = (msTextBox)dgCommodity.Items[rowIndex].FindControl("txtCode");
			if(txtCode!=null)
			{
				DataRow dr = m_sdsCommodity.ds.Tables[0].Rows[rowIndex];
				if(txtCode.Text!="")
				{
					if(txtCode.Text.IndexOf(" ",0)<1)
					{
						dr[0] = txtCode.Text;
					}
					else
					{
						lblValCommodity.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CDE_NO_SPACE",utility.GetUserCulture());
						return true;
					}
					return false;
				}
			}
			lblValCommodity.Text =Utility.GetLanguageText(ResourceType.UserMessage,"CDE_COLUMN_REQ",utility.GetUserCulture());
			ViewState["SCOperation"] = Operation.Insert;
			return true;
		}

		/// <summary>
		/// To do a query on the user input
		/// </summary>
		private void QueryCommodity()
		{
			checkEmptyFields((int)ViewState["index"]);
			SessionDS sdsCommodity = (SessionDS)Session["SESSION_DS1"];
			m_sdsCommodity = SysDataManager1.QueryCommodity(sdsCommodity, m_strAppID,m_strEnterpriseID, 0 , dgCommodity.PageSize);		
			dgCommodity.VirtualItemCount = System.Convert.ToInt32(m_sdsCommodity.QueryResultMaxSize);
			if(m_sdsCommodity.QueryResultMaxSize<1)
			{
				lblValCommodity.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			}
			Session["SESSION_DS1"] = m_sdsCommodity;
			
		}
		/// <summary>
		/// to show edit column or not
		/// </summary>
		/// <param name="toEnable"></param>
		private void enableEditColumn(bool toEnable)
		{
			dgCommodity.Columns[0].Visible=toEnable;
			dgCommodity.Columns[1].Visible=toEnable;
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		/// <summary>
		/// Insert button event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			lblValCommodity.Text = "";
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,false,m_moduleAccessRights);
			btnExecuteQuery.Enabled = false;
			dgCommodity.CurrentPageIndex = 0;
			if((int)ViewState["SCMode"] != (int)ScreenMode.Insert || m_sdsCommodity.ds.Tables[0].Rows.Count >= dgCommodity.PageSize)
			{
				m_sdsCommodity = SysDataManager1.GetEmptyCommodity(1);
				dgCommodity.EditItemIndex = m_sdsCommodity.ds.Tables[0].Rows.Count-1;
			}
			else
			{
				AddRow();				
				dgCommodity.EditItemIndex = m_sdsCommodity.ds.Tables[0].Rows.Count-1;
			}

			dgCommodity.VirtualItemCount = m_sdsCommodity.ds.Tables[0].Rows.Count;
			ViewState["SCMode"] = ScreenMode.Insert;
			enableEditColumn(true);
			m_dvTypeOptions = CreateTypeOptions(false);
			BindCommodity();
			getPageControls(Page);
		}

		/// <summary>
		/// Query button event handler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblValCommodity.Text = "";
			m_sdsCommodity = SysDataManager1.GetEmptyCommodity(1);
			dgCommodity.CurrentPageIndex = 0;
			btnExecuteQuery.Enabled = true;
			ViewState["SCMode"] = ScreenMode.Query;
			ViewState["SCOperation"] = Operation.None;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			ViewState["m_strCode"] = "";
			ViewState["prevRow"]=0;
			ViewState["index"]=0;
			dgCommodity.EditItemIndex = 0;
			enableEditColumn(false);
			m_dvTypeOptions = CreateTypeOptions(true);
			BindCommodity();
		}

		/// <summary>
		/// Execute query event handler, enables or disables certain window components
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			dgCommodity.EditItemIndex = -1;
			QueryCommodity();
			
			btnExecuteQuery.Enabled = false;
			enableEditColumn(true);
			enableDeleteColumn(true);
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			BindCommodity();
		}
		/// <summary>
		/// Page change event handler, where it refreshes the screen
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnCommodity_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			SessionDS m_sdsQueryCommodity = (SessionDS)Session["QUERY_DS"];
			lblValCommodity.Text = "";
			dgCommodity.CurrentPageIndex = e.NewPageIndex;
			int iStartIndex = dgCommodity.CurrentPageIndex * dgCommodity.PageSize;
			
			m_sdsCommodity = SysDataManager1.QueryCommodity(m_sdsQueryCommodity,m_strAppID,m_strEnterpriseID, iStartIndex,dgCommodity.PageSize);
			Session["SESSION_DS1"] = m_sdsCommodity;
			dgCommodity.VirtualItemCount = System.Convert.ToInt32(m_sdsCommodity.QueryResultMaxSize);
			dgCommodity.CurrentPageIndex = e.NewPageIndex;
		
			dgCommodity.SelectedIndex = -1;
			dgCommodity.EditItemIndex = -1;

			BindCommodity();
		}
		/// <summary>
		/// To populate options type in the drop down list
		/// </summary>
		/// <param name="showNilOption">to show an empty option or not</param>
		/// <returns></returns>
		private DataView CreateTypeOptions(bool showNilOption) 
		{
			DataTable dtTypeOptions = new DataTable();
 
			dtTypeOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtTypeOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList typeOptionArray = Utility.GetCodeValues(m_strAppID,m_strCulture,"commodity_type",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtTypeOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtTypeOptions.Rows.Add(drNilRow);
			}
			foreach(SystemCode typeSysCode in typeOptionArray)
			{
				DataRow drEach = dtTypeOptions.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtTypeOptions.Rows.Add(drEach);
			}

			DataView dvTypeOptions = new DataView(dtTypeOptions);
			return dvTypeOptions;
		}
		/// <summary>
		/// To load the options
		/// </summary>
		/// <returns></returns>
		private ICollection LoadTypeOptions()
		{
			return m_dvTypeOptions;
		}
		/// <summary>
		/// On item bound event handler, populates the drop down list everytime
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void OnItemBound_Commodity(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}
			DataRow drSelected = m_sdsCommodity.ds.Tables[0].Rows[e.Item.ItemIndex];

			DropDownList ddType = (DropDownList)e.Item.FindControl("ddType");
			
			if(ddType != null)
			{
				ddType.DataSource = LoadTypeOptions();
				ddType.DataTextField = "Text";
				ddType.DataValueField = "StringValue";
				ddType.DataBind();

				
				String strType = (String)drSelected["commodity_type"];


				ddType.SelectedIndex = ddType.Items.IndexOf(ddType.Items.FindByValue(strType));
			}

			msTextBox txtCode = (msTextBox)e.Item.FindControl("txtCode");
			if(txtCode!=null && ( (int)ViewState["SCMode"] != (int)ScreenMode.Insert && (int)ViewState["SCMode"] != (int)ScreenMode.Query) )
			{
				txtCode.Enabled = false;
			}
		}
		/// <summary>
		/// To refresh the data
		/// </summary>
		private void showCurrentPage()
		{
			SessionDS m_sdsQueryCommodity = (SessionDS)Session["QUERY_DS"];
			int iStartIndex = dgCommodity.CurrentPageIndex * dgCommodity.PageSize;
			
			m_sdsCommodity = SysDataManager1.QueryCommodity(m_sdsQueryCommodity,m_strAppID,m_strEnterpriseID, iStartIndex,dgCommodity.PageSize);
			int pgCnt = (Convert.ToInt32(m_sdsCommodity.QueryResultMaxSize - 1))/dgCommodity.PageSize;
			if(pgCnt < dgCommodity.CurrentPageIndex)
			{
				dgCommodity.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			Session["SESSION_DS1"] = m_sdsCommodity;
			
		}
	}
}
