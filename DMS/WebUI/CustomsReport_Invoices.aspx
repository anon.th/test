<%@ Page language="c#" Codebehind="CustomsReport_Invoices.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsReport_Invoices" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//Dtd HTML 4.0 transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomsReport_Invoices</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
		function funcDisBack(){
			history.go(+1);
		}

        function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }
        function validate(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventdefault) theEvent.preventdefault();
			}
		}
		
		function closeChildWin(){
			for(var i=0; i<childwindows.length; i++)
			{
				try{
					childwindows[i].close()
				}catch(e){
					
				}
			}
		}
		
		function UncheckAll()
		{
			all = document.getElementsByTagName("input");
			for(i=0;i<all.length;i++)
			{
				if(all[i].type=="checkbox" && all[i].id.indexOf("gvInvoiceDetails") > -1)
				{
					all[i].checked = false;
				}
			}
		}
		
		function CheckAll(chk)
		{
			all = document.getElementsByTagName("input");
			for(i=0;i<all.length;i++)
			{
				if(all[i].type=="checkbox" && all[i].id.indexOf("gvInvoiceDetails") > -1)
				{
					all[i].checked = chk.checked;
				}
			}
			b = document.getElementById('<%=btnPrintSelected.ClientID%>');	
			if(chk.checked==true)
			{				
				b.disabled=false;	
			}		
		}

		function Check(chk)
		{
			        b = document.getElementById('<%=btnPrintSelected.ClientID%>');	
			        
					all = document.getElementsByTagName("input");
					for(i=0;i<all.length;i++)
					{
					    if(all[i].type=="checkbox" && all[i].id.indexOf("gvInvoiceDetails") > -1)
						{
							if (all[i].checked) 
							{ 
								b.disabled = false; 
								return;
							} 
							else 
							{
								b.disabled = true; 
							}
						}
						
					}
			}
			
			function closeChildWin(){
			for(var i=0; i<childwindows.length; i++)
			{
				try{
					childwindows[i].close()
				}catch(e){
					
				}
			}
		}
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="CustomsReportInvoices" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 770px; HEIGHT: 1100px; TOP: 8px; LEFT: 56px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tbody>
						<tr>
							<td colSpan="3" align="left"><asp:label id="Label1" runat="server" Width="421px" Height="27px" CssClass="mainTitleSize">
								Customs Invoice Printing</asp:label></td>
						</tr>
						<tr>
							<td vAlign="top" colSpan="3" align="left"><asp:button id="btnQry" runat="server" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExecQry" tabIndex="1" runat="server" CssClass="queryButton" Text="Execute Query"
									CausesValidation="False"></asp:button><asp:button id="btnPrintSelected" tabIndex="2" runat="server" CssClass="queryButton" Text="Print Selected"
									CausesValidation="False" Enabled="true"></asp:button></td>
						</tr>
						<tr>
							<td style="HEIGHT: 19px" colSpan="3"><asp:label id="lblErrorMsg" runat="server" ForeColor="Red" Font-Size="X-Small" Font-Bold="true"></asp:label><asp:label id="Label12" runat="server"></asp:label></td>
						</tr>
						<tr>
							<td colSpan="3">
								<fieldset><legend style="COLOR: black">Dates</legend>
									<table style="MARGIN: 5px" id="table_fieldset_Dates">
										<tbody>
											<tr>
												<td colSpan="3"><asp:radiobutton id="rbInfoReceived" tabIndex="3" runat="server" Width="110px" Height="22px" CssClass="tableRadioButton"
														Text="Info Received" Checked="true" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbEntryCompiled" tabIndex="4" runat="server" Width="121px" Height="22px" CssClass="tableRadioButton"
														Text="Entry Compiled" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbExpectedArrival" tabIndex="5" runat="server" Width="127px" Height="22px" CssClass="tableRadioButton"
														Text="Expected Arrival" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbInvoiced" tabIndex="6" runat="server" Width="82px" Height="22px" CssClass="tableRadioButton"
														Text="Invoiced" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbManifested" tabIndex="7" runat="server" Width="93px" Height="22px" CssClass="tableRadioButton"
														Text="Manifested" GroupName="QueryByDate"></asp:radiobutton><asp:radiobutton id="rbDelivered" tabIndex="8" runat="server" Width="116px" Height="22px" CssClass="tableRadioButton"
														Text="Delivered" GroupName="QueryByDate"></asp:radiobutton></td>
											</tr>
											<tr>
												<td style="HEIGHT: 25px" colSpan="3"><asp:radiobutton id="rbMonth" tabIndex="9" runat="server" Width="73px" Height="21px" CssClass="tableRadioButton"
														Text="Month" Checked="true" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;
													<asp:dropdownlist id="ddMonth" tabIndex="10" runat="server" Width="88px" Height="19px" CssClass="textField"></asp:dropdownlist>&nbsp;
													<asp:label id="lblYear" runat="server" Width="30px" Height="15px" CssClass="tableLabel">Year</asp:label>&nbsp;
													<cc1:mstextbox id="txtYear" tabIndex="11" runat="server" Width="83" CssClass="textField" MaxLength="5"
														TextMaskType="msNumeric" NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></td>
											</tr>
											<tr>
												<td style="HEIGHT: 20px" colSpan="3"><asp:radiobutton id="rbPeriod" tabIndex="12" runat="server" Width="62px" CssClass="tableRadioButton"
														Text="Period" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
													<cc1:mstextbox style="Z-INDEX: 0" id="txtPeriod" tabIndex="13" runat="server" Width="82" CssClass="textField"
														Enabled="False" MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
													&nbsp;
													<cc1:mstextbox id="txtTo" tabIndex="14" runat="server" Width="83" CssClass="textField" Enabled="False"
														MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></td>
											</tr>
											<tr>
												<td colSpan="3"><asp:radiobutton id="rbDate" tabIndex="15" runat="server" Width="74px" Height="22px" CssClass="tableRadioButton"
														Text="Date" GroupName="EnterDate" AutoPostBack="True"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtdate" tabIndex="16" runat="server" Width="82" CssClass="textField" Enabled="False"
														MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox>
												</td>
											</tr>
										</tbody>
									</table>
								</fieldset>
							</td>
						</tr>
						<tr>
							<td style="WIDTH: 145px" width="145"><asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel">Invoice Number</asp:label></td>
							<td width="25%"><asp:textbox style="Z-INDEX: 0" id="txtInvoiceNumberFrom" tabIndex="17" onkeypress="return validate(event)"
									runat="server" Width="145px" CssClass="textField" MaxLength="19"></asp:textbox></td>
							<td><asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel">To</asp:label><asp:label id="Label4" Width="12px" Runat="server">&nbsp;</asp:label><asp:textbox style="Z-INDEX: 0; MARGIN-LEFT: 8px" id="txtInvoiceNumberTo" tabIndex="18" onkeypress="return validate(event)"
									runat="server" CssClass="textField" MaxLength="19"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 145px" width="145"><asp:label style="Z-INDEX: 0" id="lblHouseAwbNumber" runat="server" CssClass="tableLabel">House AWB Number</asp:label></td>
							<td colSpan="2"><asp:textbox style="Z-INDEX: 0" id="txtHouseAwbNumber" tabIndex="19" runat="server" Width="145px"
									CssClass="textField" MaxLength="30"></asp:textbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 145px; HEIGHT: 22px" width="145"><asp:label style="Z-INDEX: 0" id="Label5" runat="server" CssClass="tableLabel">Customer ID</asp:label></td>
							<td style="HEIGHT: 22px" colSpan="2"><asp:textbox style="Z-INDEX: 0" id="txtCustomerId" tabIndex="20" runat="server" Width="145px"
									CssClass="textField" MaxLength="20"></asp:textbox>
								<asp:button style="Z-INDEX: 0" id="btnCustomerPopup" tabIndex="11" runat="server" CssClass="searchButton"
									Height="19px" Width="21px" CausesValidation="False" Text="..."></asp:button></td>
						</tr>
						<tr>
							<td style="WIDTH: 145px" width="145"><asp:label style="Z-INDEX: 0" id="Label6" runat="server" CssClass="tableLabel">Customer Group</asp:label></td>
							<td colSpan="2"><asp:dropdownlist style="Z-INDEX: 0" id="ddlCustomerGroup" tabIndex="21" runat="server" Width="145px"
									Height="19px" CssClass="textField"></asp:dropdownlist></td>
						</tr>
						<tr>
							<td style="WIDTH: 145px" width="145"><asp:label style="Z-INDEX: 0" id="Label7" runat="server" CssClass="tableLabel">Bonded Warehouse</asp:label></td>
							<td colSpan="2"><asp:dropdownlist style="Z-INDEX: 0" id="ddlBondedWarehouse" tabIndex="22" runat="server" Width="145px"
									Height="19px" CssClass="textField"></asp:dropdownlist></td>
						</tr>
						<tr>
							<td style="WIDTH: 145px" width="145"><asp:label style="Z-INDEX: 0" id="Label8" runat="server" CssClass="tableLabel">Job Status</asp:label></td>
							<td><asp:dropdownlist style="Z-INDEX: 0" id="ddlJobStatus" tabIndex="23" runat="server" Width="145px"
									Height="19px" CssClass="textField"></asp:dropdownlist></td>
							<td><asp:checkbox id="chkSearchOnStatusNotAchieved" tabIndex="24" runat="server" CssClass="tableLabel"
									Text="Search on Status not Achieved"></asp:checkbox></td>
						</tr>
						<tr>
							<td style="WIDTH: 145px; HEIGHT: 22px" width="145"><asp:label style="Z-INDEX: 0" id="Label9" runat="server" CssClass="tableLabel">Entry Type</asp:label></td>
							<td style="HEIGHT: 22px" colSpan="2"><asp:dropdownlist style="Z-INDEX: 0" id="ddlEntryType" tabIndex="24" runat="server" Width="145px"
									Height="19px" CssClass="textField"></asp:dropdownlist></td>
						</tr>
						<tr>
							<td style="WIDTH: 145px" width="145"><asp:label style="Z-INDEX: 0" id="Label10" runat="server" CssClass="tableLabel">Customs Profile</asp:label></td>
							<td colSpan="2"><asp:dropdownlist style="Z-INDEX: 0" id="ddlCustomsProfile" tabIndex="25" runat="server" Width="145px"
									Height="19px" CssClass="textField"></asp:dropdownlist></td>
						</tr>
						<tr>
							<td style="WIDTH: 145px" width="145"><asp:label style="Z-INDEX: 0" id="Label11" runat="server" CssClass="tableLabel">Customs Declaration No.</asp:label></td>
							<td colSpan="2"><asp:textbox style="Z-INDEX: 0" id="txtCustomsDeclarationNo" tabIndex="27" runat="server" Width="145px"
									CssClass="textField" MaxLength="20"></asp:textbox></td>
						</tr>
						<tr>
							<td colSpan="3"><asp:checkbox style="MARGIN-LEFT: -3px" id="chkShowAllModifiedSinceLastPrinted" tabIndex="28"
									runat="server" CssClass="tableLabel" Text="Show all modified since last printed"></asp:checkbox></td>
						</tr>
						<tr>
							<td colSpan="3"><STRONG class="gridHeading"><FONT style="WIDTH: 100%" size="2">Invoices 
										Matching Selection Criteria</FONT></STRONG>
							</td>
						</tr>
						<tr>
							<td colSpan="3"><asp:datagrid style="Z-INDEX: 0" id="gvInvoiceDetails" tabIndex="29" runat="server" Width="100%"
									AllowPaging="True" AutoGenerateColumns="False" DataKeyField="Invoice_No" AlternatingItemStyle-CssClass="gridField"
									ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridHeading" CellPadding="0" CellSpacing="1" BorderWidth="0px"
									FooterStyle-CssClass="gridHeading">
									<FooterStyle CssClass="gridHeading"></FooterStyle>
									<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn>
											<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<HeaderTemplate>
												<asp:CheckBox Runat="server" ID="chkHeader" onclick="javascript:CheckAll(this);"></asp:CheckBox>
											</HeaderTemplate>
											<ItemTemplate>
												<asp:CheckBox Runat="server" ID="cbRows" onclick="javascript:Check(this);"></asp:CheckBox>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Invoice No">
											<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblInvoiceNo" Runat=server Text='<%#DataBinder.Eval(Container.DataItem,"Invoice_No") %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Customer">
											<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"payerid") %>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Customer Name">
											<HeaderStyle HorizontalAlign="Left" Width="30%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"cust_name") %>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Printed Date">
											<HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"PrintedDT" , "{0:dd/MM/yyyy HH:mm}" ) %>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Last Modified">
											<HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"LastModifiedDT", "{0:dd/MM/yyyy HH:mm}" ) %>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Amount">
											<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblInvoiceAmount" Runat=server Text='<%#DataBinder.Eval(Container.DataItem,"InvoiceAmount") %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
								</asp:datagrid></td>
						</tr>
					</tbody>
				</TABLE>
			</div>
			<INPUT 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition> <INPUT type="hidden" name="hdnRefresh">
		</form>
	</body>
</HTML>
