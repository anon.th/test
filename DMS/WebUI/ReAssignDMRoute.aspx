<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Page language="c#" Codebehind="ReAssignDMRoute.aspx.cs" AutoEventWireup="false" enableViewState="true" Inherits="com.ties.ReAssignDMRoute" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Re-Assign Delivery Manifest Route</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<META content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<META content="C#" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"> <!--#INCLUDE FILE="msFormValidations.inc"-->
		<SCRIPT language="javascript" src="Scripts/settingScrollPosition.js"></SCRIPT>
	</HEAD>
	<BODY onclick="GetScrollPosition();" onload="SetScrollPosition();" XMLNS:DbCombo="http://schemas.cambro.net/dbcombo"
		MS_POSITIONING="GridLayout">
		<FORM id="ReAssignDMRoute" method="post" runat="server">
			<asp:validationsummary id="PageValidationSummary" runat="server" HeaderText="Please check the following Invalid/Mandatory field(s):<br><br>"
				DisplayMode="BulletList" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary><asp:label id="lblMainTitle" style="Z-INDEX: 104; LEFT: 16px; POSITION: absolute; TOP: 4px"
				runat="server" CssClass="mainTitleSize" Height="26px" Width="577px"> Re-assign Delivery Route Manifest</asp:label><asp:label id="lblErrorMsg" style="Z-INDEX: 105; LEFT: 17px; POSITION: absolute; TOP: 32px"
				runat="server" CssClass="errorMsgColor" Height="19px" Width="566px"></asp:label>
			<TABLE id="tableMain" style="Z-INDEX: 110; LEFT: 8px; WIDTH: 639px; POSITION: absolute; TOP: 55px; HEIGHT: 756px"
				width="639" border="0" runat="server">
				<TR>
					<TD style="HEIGHT: 131px" vAlign="top">
						<FIELDSET style="WIDTH: 671px; HEIGHT: 136px"><LEGEND><asp:label id="lblRoutPath" CssClass="tableHeadingFieldset" Runat="server">Get Existing Path/Route</asp:label></LEGEND>
							<TABLE id="DeliveryManifestTable" style="WIDTH: 656px; HEIGHT: 118px" border="0" runat="server">
								<TR height="27">
									<TD colSpan="1"></TD>
									<TD colSpan="3"><asp:label id="lblDlvryPath" runat="server" CssClass="tableLabel">Delivery Path Code</asp:label></TD>
									<TD colSpan="3"><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="1" Runat="server" TextBoxColumns="18" ServerMethod="DbComboPathCodeSelect"
											AutoPostBack="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" "></DBCOMBO:DBCOMBO></TD>
									<TD style="WIDTH: 3px" colSpan="1"></TD>
									<TD style="WIDTH: 3px" colSpan="1"></TD>
									<TD colSpan="7"><asp:textbox id="txtPathDesc" tabIndex="23" runat="server" CssClass="textField" Width="160px"
											ReadOnly="True"></asp:textbox>&nbsp;&nbsp;<asp:textbox id="txtDelPath" runat="server" CssClass="txtReadOnly" Width="100px"></asp:textbox>
									</TD>
									<TD colSpan="4"></TD>
								</TR>
								<TR height="27">
									<TD colSpan="1"></TD>
									<TD colSpan="3"><asp:label id="lblVehicle_Flight_No" runat="server" CssClass="tableLabel">Vehicle No</asp:label></TD>
									<TD colSpan="3"><DBCOMBO:DBCOMBO id="dbFlightVehicleNo" tabIndex="2" Runat="server" TextBoxColumns="18" ServerMethod="ShowFlightVehicleNo"
											AutoPostBack="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" " NewVersionAvailable="True"></DBCOMBO:DBCOMBO></TD>
									<TD style="WIDTH: 3px" colSpan="1"></TD>
									<TD style="WIDTH: 3px" colSpan="1"></TD>
									<TD style="WIDTH: 130px" colSpan="3"><asp:label id="lblLineHaulCost" runat="server" CssClass="tableLabel">Line Haul Cost</asp:label></TD>
									<TD colSpan="4"><cc1:mstextbox id="txtLineHaulCost" onblur="round(this,2)" tabIndex="3" runat="server" CssClass="textFieldRightAlign"
											Width="136px" ReadOnly="True" TextMaskType="msNumeric" MaxLength="9" NumberMaxValue="999999" NumberMinValue="0"
											NumberPrecision="8" NumberScale="2"></cc1:mstextbox></TD>
									<TD colSpan="4"></TD>
								</TR>
								<%--Comment by Gwang on 16June08;<TR height="27">
									<TD colSpan="1"></TD>
									<TD colSpan="3"><asp:label id="lblManifestedDate" runat="server" CssClass="tableLabel">Manifested Date</asp:label></TD>
									<TD colSpan="3"><DBCOMBO:DBCOMBO id="dbManifestedDate" tabIndex="3" Runat="server" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" AutoPostBack="True" ServerMethod="ShowManifestedDate" TextBoxColumns="18" NewVersionAvailable="True"></DBCOMBO:DBCOMBO></TD>
									<TD style="WIDTH: 3px" colSpan="1"></TD>
									<TD style="WIDTH: 3px" colSpan="1"></TD>
									<TD style="WIDTH: 130px" colSpan="3"><asp:label id="lblDepartDate" runat="server" CssClass="tableLabel">Departure Date</asp:label></TD>
									<TD colSpan="4"><cc1:mstextbox id="txtDepartDate" tabIndex="20" runat="server" Width="136px" CssClass="textField" ReadOnly="True" MaxLength="16" TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99"></cc1:mstextbox></TD>
									<TD colSpan="4"></TD>
								</TR>--%>
								<TR height="27">
									<TD colSpan="1"></TD>
									<TD colSpan="3"><asp:label id="lblDepartDate" runat="server" CssClass="tableLabel">Departure Date</asp:label></TD>
									<TD colSpan="3"><DBCOMBO:DBCOMBO id="dbDepartDate" tabIndex="3" Runat="server" TextBoxColumns="18" ServerMethod="ShowManifestedDate"
											AutoPostBack="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" " NewVersionAvailable="True"></DBCOMBO:DBCOMBO></TD>
									<TD style="WIDTH: 3px" colSpan="1"></TD>
									<TD style="WIDTH: 3px" colSpan="1"></TD>
									<TD style="WIDTH: 130px" colSpan="3"><asp:label id="lblManifestedDate" runat="server" CssClass="tableLabel">Manifested Date</asp:label></TD>
									<TD colSpan="4"><cc1:mstextbox id="txtManifestDate" tabIndex="20" runat="server" CssClass="textField" Width="136px"
											ReadOnly="True" TextMaskType="msDateTime" MaxLength="16" TextMaskString="99/99/9999 99:99"></cc1:mstextbox></TD>
									<TD colSpan="4"></TD>
								</TR>
								<TR height="27">
									<TD colSpan="1"></TD>
									<TD colSpan="3"><asp:label id="lblDriver_Flight_AWB" runat="server" CssClass="tableLabel">Driver Name</asp:label></TD>
									<TD colSpan="3"><cc1:mstextbox id="txtDriver_Flight_AWB" tabIndex="4" runat="server" CssClass="textField" ReadOnly="True"
											TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12"></cc1:mstextbox></TD>
									<TD style="WIDTH: 3px" colSpan="1"></TD>
									<TD style="WIDTH: 3px" colSpan="1"></TD>
									<TD colSpan="1"><asp:textbox id="txtDelvryType" runat="server" Visible="False"></asp:textbox></TD>
									<TD colSpan="4"><FONT face="Tahoma"></FONT></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 285px" vAlign="top">
						<FIELDSET style="WIDTH: 671px; HEIGHT: 252px"><LEGEND><asp:label id="Label1" CssClass="tableHeadingFieldset" Runat="server">Select Consignment to Re-Assign</asp:label></LEGEND>
							<TABLE id="DeliveryManifestTable1" style="WIDTH: 666px; HEIGHT: 252px" width="666" border="0"
								runat="server">
								<TR>
									<TD vAlign="top" colSpan="10">
										<DIV style="BORDER-RIGHT: #99ffcc 1px solid; BORDER-TOP: #99ffcc 1px solid; OVERFLOW: auto; BORDER-LEFT: #99ffcc 1px solid; WIDTH: 554px; BORDER-BOTTOM: #99ffcc 1px solid; HEIGHT: 250px"><asp:datagrid id="dgAssignedConsignment" tabIndex="7" runat="server" AutoGenerateColumns="False"
												WIDTH="545px">
												<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
												<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
														<ItemStyle CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
														<ItemTemplate>
															<asp:CheckBox ID="chkSelect" Runat="server"></asp:CheckBox>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="Consignment">
														<HeaderStyle Width="50%" CssClass="gridHeading"></HeaderStyle>
														<ItemStyle Wrap="False"></ItemStyle>
														<ItemTemplate>
															<asp:Label CssClass="gridLabel" ID="lblConsgmntNo" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' Runat="server">
															</asp:Label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="ORG">
														<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
														<ItemTemplate>
															<asp:Label CssClass="gridLabel" ID="lblDGOrigin" Text='<%#DataBinder.Eval(Container.DataItem,"origin_state_code")%>' Runat="server" Enabled="True">
															</asp:Label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="DST">
														<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
														<ItemStyle VerticalAlign="Middle"></ItemStyle>
														<ItemTemplate>
															<asp:Label CssClass="gridLabel" ID="lblDGDestination" Text='<%#DataBinder.Eval(Container.DataItem,"destination_state_code")%>' Runat="server" Enabled="True">
															</asp:Label>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="RTE">
														<HeaderStyle Width="15%" CssClass="gridHeading" Wrap="False"></HeaderStyle>
														<ItemStyle VerticalAlign="Middle"></ItemStyle>
														<ItemTemplate>
															<asp:Label CssClass="gridLabel" ID="lblDGRoute" Text='<%#DataBinder.Eval(Container.DataItem,"Route_Code")%>' Runat="server" Enabled="True">
															</asp:Label>
														</ItemTemplate>
													</asp:TemplateColumn>
												</Columns>
											</asp:datagrid></DIV>
									</TD>
									<TD vAlign="top" align="left" colSpan="10"><asp:button id="btnCheckAll" tabIndex="4" runat="server" CssClass="queryButton" CausesValidation="False"
											Text="Check All" width="96px"></asp:button><BR>
										<asp:button id="btnClearAll" tabIndex="5" runat="server" CssClass="queryButton" CausesValidation="False"
											Text="Clear All" width="96px"></asp:button></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
				<TR>
					<TD vAlign="top">
						<FIELDSET style="WIDTH: 673px; HEIGHT: 226px"><LEGEND><asp:label id="Label2" CssClass="tableHeadingFieldset" Runat="server">New Delivery Path/Route</asp:label></LEGEND>
							<TABLE id="DeliveryManifestTable2" width="100%" border="0" runat="server">
								<TR>
									<TD colSpan="20"><asp:label id="lblDelType" CssClass="tableLabel" Runat="server">Delivery Type</asp:label></TD>
								</TR>
								<TR>
									<TD colSpan="1"></TD>
									<TD colSpan="15">
										<asp:radiobutton id="rbtnLong" tabIndex="6" CssClass="tableRadioButton" Runat="server" AutoPostBack="True"
											Text="Linehaul" GroupName="GrpDeliveryType" Font-Size="Smaller"></asp:radiobutton>
										<asp:radiobutton id="rbtnShort" tabIndex="7" CssClass="tableRadioButton" Runat="server" AutoPostBack="True"
											Text="Short Route" GroupName="GrpDeliveryType" Font-Size="Smaller"></asp:radiobutton>
										<asp:radiobutton id="rbtnAir" tabIndex="8" CssClass="tableRadioButton" Runat="server" AutoPostBack="True"
											Text="Air Route" GroupName="GrpDeliveryType" Font-Size="Smaller"></asp:radiobutton></TD>
								</TR>
								<TR>
									<TD align="right" rowSpan="1"><asp:requiredfieldvalidator id="validDlvryPath" Runat="server" ControlToValidate="DbComboPathCodeNew" display="None"
											ErrorMessage="Delivery Path Code">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 197px" colSpan="3"><asp:label id="lblDlvryPath1" runat="server" CssClass="tableLabel">Delivery Path Code</asp:label></TD>
									<TD colSpan="3"><DBCOMBO:DBCOMBO id="DbComboPathCodeNew" tabIndex="9" Runat="server" TextBoxColumns="18" ServerMethod="DbComboPathCodeSelectNew"
											AutoPostBack="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" " NewVersionAvailable="True"></DBCOMBO:DBCOMBO></TD>
									<TD colSpan="1"></TD>
									<TD colSpan="5"><asp:textbox id="txtPathDesc1" runat="server" CssClass="textField" Width="100%" ReadOnly="True"></asp:textbox></TD>
									<TD colSpan="4"></TD>
									<TD colSpan="3"></TD>
								</TR>
								<TR>
									<TD align="right" rowSpan="1"><asp:requiredfieldvalidator id="reqFlightVehicleNo" tabIndex="-32768" Runat="server" ControlToValidate="dbFlightVehicleNoNew"
											ErrorMessage="">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 197px" colSpan="3"><asp:label id="lblVehicle_Flight_No1" runat="server" CssClass="tableLabel">Vehicle No</asp:label></TD>
									<TD colSpan="3"><DBCOMBO:DBCOMBO id="dbFlightVehicleNoNew" tabIndex="10" Runat="server" TextBoxColumns="18" ServerMethod="ShowFlightVehicleNo"
											AutoPostBack="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" " NewVersionAvailable="True"></DBCOMBO:DBCOMBO></TD>
									<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="reqLineHaulCost1" tabIndex="-32768" Runat="server" ControlToValidate="txtLineHaulCost1"
											ErrorMessage="Line Haul Cost">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 103px" colSpan="3"><asp:label id="lblLineHaulCost1" runat="server" CssClass="tableLabel" Width="115px">Line Haul Cost</asp:label></TD>
									<TD colSpan="4"><cc1:mstextbox id="txtLineHaulCost1" onblur="round(this,2)" tabIndex="14" runat="server" CssClass="textFieldRightAlign"
											Width="128px" TextMaskType="msNumeric" MaxLength="9" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8"
											NumberScale="2"></cc1:mstextbox></TD>
									<TD colSpan="4"></TD>
									<TD colSpan="1"></TD>
								</TR>
								<%--Comment by Gwang on 16June08;<TR>
									<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="reqManifestedDate" tabIndex="-32768" Runat="server" ErrorMessage="Manifested Date" ControlToValidate="dbManifestedDateNew">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 197px" colSpan="3"><asp:label id="lblManifestedDate1" runat="server" CssClass="tableLabel">Manifested Date</asp:label></TD>
									<TD colSpan="3"><DBCOMBO:DBCOMBO id="dbManifestedDateNew" tabIndex="11" Runat="server" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" AutoPostBack="True" ServerMethod="ShowManifestedDate" TextBoxColumns="18" NewVersionAvailable="True"></DBCOMBO:DBCOMBO></TD>
									<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="reqDepartDate1" tabIndex="-32768" Runat="server" ErrorMessage="Departure Date" ControlToValidate="txtDepartDate1">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 103px" colSpan="3"><asp:label id="lblDepartDate1" runat="server" CssClass="tableLabel">Departure Date</asp:label></TD>
									<TD colSpan="4"><cc1:mstextbox id="txtDepartDate1" tabIndex="15" runat="server" Width="128px" CssClass="textField" MaxLength="16" TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99"></cc1:mstextbox></TD>
									<TD colSpan="4"></TD>
									<TD colSpan="1"></TD>
								</TR>--%>
								<TR>
									<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="reqManifestedDate" tabIndex="-32768" Runat="server" ControlToValidate="dbDepartDateNew"
											ErrorMessage="Departure Date">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 197px" colSpan="3"><asp:label id="lblDepartDate1" runat="server" CssClass="tableLabel">Departure Date</asp:label></TD>
									<TD colSpan="3"><DBCOMBO:DBCOMBO id="dbDepartDateNew" tabIndex="11" Runat="server" TextBoxColumns="18" ServerMethod="ShowManifestedDate"
											AutoPostBack="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" " NewVersionAvailable="True"></DBCOMBO:DBCOMBO></TD>
									<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="reqDepartDate1" tabIndex="-32768" Runat="server" ControlToValidate="txtManifestDate1"
											ErrorMessage="Manifested Date">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 103px" colSpan="3"><asp:label id="lblManifestedDate1" runat="server" CssClass="tableLabel">Manifested Date</asp:label></TD>
									<TD colSpan="4"><cc1:mstextbox id="txtManifestDate1" tabIndex="15" runat="server" CssClass="textField" Width="128px"
											TextMaskType="msDateTime" MaxLength="16" TextMaskString="99/99/9999 99:99"></cc1:mstextbox></TD>
									<TD colSpan="4"></TD>
									<TD colSpan="1"></TD>
								</TR>
								<TR>
									<TD align="right" colSpan="1"><asp:requiredfieldvalidator id="reqDriver_Flight_AWB1" tabIndex="-32768" Runat="server" ControlToValidate="txtDriver_Flight_AWB1"
											ErrorMessage="">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 197px" colSpan="3"><asp:label id="lblDriver_Flight_AWB1" runat="server" CssClass="tableLabel">Driver Name</asp:label></TD>
									<TD colSpan="3"><cc1:mstextbox id="txtDriver_Flight_AWB1" tabIndex="12" runat="server" CssClass="textField" TextMaskType="msUpperAlfaNumericWithUnderscore"
											MaxLength="12"></cc1:mstextbox></TD>
									<td colSpan="1"></td>
									<td colSpan="1"></td>
									<TD colSpan="3"></TD>
									<td colSpan="4"></td>
									<td colSpan="4"></td>
								</TR>
								<tr>
									<TD colSpan="1"></TD>
									<TD style="WIDTH: 197px" vAlign="top" colSpan="3"><asp:label id="lblRemarks" runat="server" CssClass="tableLabel">Remarks</asp:label></TD>
									<TD colSpan="6"><asp:textbox id="txtRemarks" tabIndex="13" runat="server" CssClass="textField" Height="70px"
											MaxLength="200" width="100%" TextMode="MultiLine" Rows="4"></asp:textbox></TD>
									<td style="WIDTH: 53px" colSpan="1"></td>
									<td style="WIDTH: 75px" vAlign="top" colSpan="3"><asp:button id="btnSave" tabIndex="16" runat="server" CssClass="queryButton" CausesValidation="True"
											Text="Save" width="50px"></asp:button></td>
									<td colSpan="4"></td>
									<td colSpan="2"></td>
								</tr>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
			</TABLE>
			<input type=hidden value="<%=strScrollPosition%>" 
name=ScrollPosition>
		</FORM>
	</BODY>
</HTML>
