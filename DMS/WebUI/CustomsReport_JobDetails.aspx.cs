using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;

namespace TIES
{
	/// <summary>
	/// Summary description for CustomsReport_JobDetails.
	/// </summary>
	public class CustomsReport_JobDetails : BasePage
	{
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label71;
		protected System.Web.UI.WebControls.RadioButton rbInfoReceived;
		protected System.Web.UI.WebControls.RadioButton rbEntryCompiled;
		protected System.Web.UI.WebControls.RadioButton rbExpectedArrival;
		protected System.Web.UI.WebControls.RadioButton rbInvoiced;
		protected System.Web.UI.WebControls.RadioButton rbManifested;
		protected System.Web.UI.WebControls.RadioButton rbDelivered;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtdate;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox txtInvoiceNumberFrom;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.Label Label4_;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnGenerateReport;
		protected System.Web.UI.WebControls.CheckBox chkSearchMawbInJobEntry_;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.WebControls.Label Label3_empty;
		protected System.Web.UI.WebControls.Label Label13_empty;
		protected System.Web.UI.WebControls.TextBox txtHouseAwbNumber;
		protected System.Web.UI.WebControls.TextBox txtMawbNumber;
		protected System.Web.UI.WebControls.TextBox txtCustomerID;
		protected System.Web.UI.WebControls.TextBox txtCustomerGroup;
		protected System.Web.UI.WebControls.TextBox txtBondedWarehouse;
		protected System.Web.UI.WebControls.TextBox txtDischargePort;
		protected System.Web.UI.WebControls.TextBox txtJobStatus;
		protected System.Web.UI.WebControls.CheckBox chkSearchOnStatusNotAchieved;
		protected System.Web.UI.WebControls.TextBox txtEntryType;
		protected System.Web.UI.WebControls.TextBox txtShipFlightNo;
		protected System.Web.UI.WebControls.TextBox txtExporterName;
		protected System.Web.UI.WebControls.TextBox FolioNumber;
		protected System.Web.UI.WebControls.TextBox txtChequeReqNo;
		protected System.Web.UI.WebControls.TextBox txtCustomsProfile;
		protected System.Web.UI.WebControls.TextBox txtCustomsReceiptNo;
		protected System.Web.UI.WebControls.TextBox txtCustomsLodgmentNo;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;

		private string appID = string.Empty;
		private string enterpriseID = string.Empty;
		private string userLoggin = string.Empty;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings, Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userLoggin = utility.GetUserID();

			Response.Redirect("CustomsJobStatus.aspx?MODID=JobStatus&pagemode=CustomsReport_JobDetails");
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//

			base.OnInit(e);
			InitializeComponent();

		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnGenerateReport.Click += new System.EventHandler(this.btnGenerateReport_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnGenerateReport_Click(object sender, System.EventArgs e)
		{
			DataTable dtParam = new DataTable();
			DataRow row = dtParam.NewRow();

			dtParam.Columns.Add("param1");
			dtParam.Columns.Add("param2");

			row["param1"] = enterpriseID;
			row["param2"] = "10260";

			dtParam.Rows.Add(row);
			
			//Set and call report
			String strUrl = "ReportViewer.aspx";
			Session["FORMID"] = "CustomsReportJobDetails";
			Session["RptParam"] = dtParam;
			Session["RptUserLogin"] = userLoggin;
				
			ArrayList paramList = new ArrayList();
			
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindowReport.js", paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
	}
}
