<%@ Page language="c#" Codebehind="UserProfile.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.UserProfile" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>UserProfile</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
  </HEAD>
	<body MS_POSITIONING="GridLayout" onunload="window.top.displayBanner.fnCloseAll(0);">
		<form id="UserProfile" method="post" runat="server">
			&nbsp;
			<asp:button id="btnSave" style="Z-INDEX: 105; POSITION: absolute; TOP: 51px; LEFT: 14px" tabIndex="5"
				runat="server" CausesValidation="true" Text="Save" CssClass="queryButton" Width="66px" Height="22px"></asp:button>
			<asp:label id="lblMessages" style="Z-INDEX: 106; POSITION: absolute; TOP: 55px; LEFT: 97px"
				runat="server" Width="557px" Height="23px" ForeColor="Red"></asp:label>
			<asp:Label id="lblTitle" style="Z-INDEX: 107; POSITION: absolute; TOP: 13px; LEFT: 17px" runat="server"
				Width="683px" CssClass="mainTitleSize" Height="34px">User Profile</asp:Label>
			<asp:ValidationSummary ID="reqSummary" Runat="server" DisplayMode="BulletList" ShowMessageBox="True" HeaderText="Please check the following information:"
				ShowSummary="False"></asp:ValidationSummary>
			<FIELDSET style="Z-INDEX: 108; POSITION: absolute; WIDTH: 298px; HEIGHT: 210px; VISIBILITY: hidden; TOP: 387px; LEFT: 434px"><LEGEND>
					<asp:Label id="lblUsrInfo" CssClass="tableHeadingFieldset" Runat="server">Permission Grants</asp:Label></LEGEND></FIELDSET>
			<TABLE id="tblPermissions" style="Z-INDEX: 109; POSITION: absolute; WIDTH: 277px; HEIGHT: 147px; VISIBILITY: hidden; TOP: 403px; LEFT: 437px"
				cellSpacing="1" cellPadding="1" width="277" border="0" runat="server">
				<TR>
					<TD>
						<asp:checkbox id="chkGrantsIns" tabIndex="26" Width="20%" AutoPostBack="True" Runat="server"></asp:checkbox></TD>
					<TD class="tableLabel">
						<asp:label id="lblInsRec" Width="80%" Runat="server">Insert Record</asp:label></TD>
				</TR>
				<TR>
					<TD>
						<asp:checkbox id="chkGrantsUpd" tabIndex="27" Width="20%" AutoPostBack="True" Runat="server"></asp:checkbox></TD>
					<TD class="tableLabel">
						<asp:label id="lblUpdRec" Width="80%" Runat="server">Update Record</asp:label></TD>
				</TR>
				<TR>
					<TD>
						<asp:checkbox id="chkGrantsDel" tabIndex="28" Width="20%" AutoPostBack="True" Runat="server"></asp:checkbox></TD>
					<TD class="tableLabel">
						<asp:label id="lblDelRec" Width="80%" Runat="server">Delete Record</asp:label></TD>
				</TR>
			</TABLE>
			<fieldset style="Z-INDEX: 110; POSITION: absolute; WIDTH: 416px; HEIGHT: 295px; TOP: 77px; LEFT: 14px"><LEGEND>
					<asp:Label id="lblUsrInfos" CssClass="tableHeadingFieldset" Runat="server">User Information</asp:Label></LEGEND>
				<TABLE id="tblRoleInfo" style="WIDTH: 335px; HEIGHT: 217px" height="217" width="335" border="0"
					runat="server">
					<TR>
						<TD style="WIDTH: 15px" width="15"></TD>
						<TD class="tableLabel" width="123" style="WIDTH: 123px">
							<asp:label id="lblUserID" tabIndex="7" Width="106px" Runat="server">User ID</asp:label></TD>
						<TD width="45%">
							<cc1:mstextbox id="txtUserID" tabIndex="8" Runat="server" TextMaskType="msUpperAlfaNumericWithUnderscore"
								MaxLength="40" Enabled="False" Width="153"></cc1:mstextbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 15px" width="15"></TD>
						<TD class="tableLabel" width="123" style="WIDTH: 123px">
							<asp:label id="lblUserPswd" tabIndex="9" Width="108px" Runat="server">Password</asp:label></TD>
						<TD width="45%">
							<asp:button id="btnResetPswd" tabIndex="10" runat="server" Width="153" CssClass="queryButton"
								Text="Reset Password" Height="21px"></asp:button></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 15px" width="15"></TD>
						<TD class="tableLabel" width="123" style="WIDTH: 123px">
							<asp:label id="lblUsrName" tabIndex="11" Width="109px" Runat="server">User Name</asp:label></TD>
						<TD>
							<cc1:mstextbox id="txtUserName" tabIndex="12" Width="153" Runat="server" TextMaskType="msAlfaNumericWithSpace"
								MaxLength="100"></cc1:mstextbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 15px" width="15"></TD>
						<TD class="tableLabel" width="123" style="WIDTH: 123px">
							<asp:label id="Label2" tabIndex="13" Width="109px" Runat="server">User Culture</asp:label></TD>
						<TD>
							<asp:dropdownlist id="ddbCulture" tabIndex="14" runat="server" Width="153"></asp:dropdownlist></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 15px" width="15">
							<asp:RegularExpressionValidator id="regEmailValidate" Runat="server" ErrorMessage="Invalid Email" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
								Display="None" ControlToValidate="txtEmail"></asp:RegularExpressionValidator></TD>
						<TD class="tableLabel" width="123" style="WIDTH: 123px">
							<asp:label id="lblEmail" tabIndex="15" Width="118px" Runat="server">Email 1</asp:label></TD>
						<TD>
							<asp:textbox id="txtEmail" tabIndex="16" Width="153" Runat="server" MaxLength="50"></asp:textbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 15px; HEIGHT: 21px" width="15"></TD>
						<TD class="tableLabel" style="WIDTH: 123px; HEIGHT: 21px" width="123">
							<asp:label id="lblDept" tabIndex="17" Width="109px" Runat="server">Department</asp:label></TD>
						<TD style="HEIGHT: 21px">
							<cc1:mstextbox id="txtDepartment" tabIndex="18" Width="153" Runat="server" TextMaskType="msAlfaNumericWithSpace"
								MaxLength="100"></cc1:mstextbox></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 15px" width="15"></TD>
						<TD class="tableLabel" width="123" style="WIDTH: 123px">
							<asp:label id="lblUserType" tabIndex="19" Width="109px" Runat="server">User Type</asp:label></TD>
						<TD>
							<asp:dropdownlist id="ddbUserType" tabIndex="20" runat="server" Width="153" Enabled="False"></asp:dropdownlist></TD>
					</TR>
				</TABLE>
			</fieldset>
			<fieldset style="Z-INDEX: 111; POSITION: absolute; WIDTH: 289px; HEIGHT: 296px; VISIBILITY: hidden; TOP: 78px; LEFT: 440px"><LEGEND>
					<asp:Label id="lblAddFnRol" CssClass="tableHeadingFieldset" Runat="server">Additional Functional Roles</asp:Label></LEGEND>
				<TABLE id="tblAdditionalRoles" style="WIDTH: 287px; HEIGHT: 230px" cellSpacing="1" cellPadding="1"
					width="287" border="0" runat="server">
					<TR>
						<TD>
							<asp:treeview id="funcTree" tabIndex="21" ExpandDepth="0" runat="server" AutoPostBack="True" style="height:270px; width:265px;"></asp:treeview>
						</TD>
					</TR>
				</TABLE>
			</fieldset>
			<FIELDSET style="Z-INDEX: 112; POSITION: absolute; WIDTH: 419px; HEIGHT: 192px; VISIBILITY: hidden; TOP: 393px; LEFT: 10px">
				<table>
					<tr>
						<td>
							<asp:listbox id="lbRoleMaster" runat="server" Width="190px" Height="194px" SelectionMode="Multiple"
								AutoPostBack="True"></asp:listbox>
						</td>
						<td>
							<asp:button id="btnRemove" tabIndex="22" runat="server" Width="37px" Text="<" CausesValidation="False"
								CssClass="queryButton"></asp:button>
							<asp:button id="btnAdd" tabIndex="23" runat="server" Width="37px" CssClass="queryButton" Text=">"
								CausesValidation="False"></asp:button>
							<asp:button id="btnRemoveAll" tabIndex="24" runat="server" Width="37px" CssClass="queryButton"
								Text="<<" CausesValidation="False"></asp:button>
							<asp:button id="btnAddAll" tabIndex="25" runat="server" Width="37px" CssClass="queryButton"
								Text=">>" CausesValidation="False"></asp:button>
						</td>
						<td>
							<asp:listbox id="lbRolesAdded" runat="server" Width="168px" Height="189px" SelectionMode="Multiple"
								AutoPostBack="True"></asp:listbox>
						</td>
					</tr>
				</table>
			</FIELDSET>
			<table style=" POSITION: absolute; WIDTH: 320px; HEIGHT: 32px; TOP: 622px; LEFT: 19px">
				<tr>
					<td>&nbsp;
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
