<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="TopNReport.aspx.cs" AutoEventWireup="false" Inherits="com.ties.TopNReport" EnableEventValidation="False" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>TopNReport</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript">
			function makeUppercase(objId)
			{
				document.getElementById(objId).value = document.getElementById(objId).value.toUpperCase();
			}
		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="TopNReport" method="post" runat="server">
			<asp:button style="Z-INDEX: 100; POSITION: absolute; TOP: 54px; LEFT: 19px" id="btnQuery" runat="server"
				Text="Query" Width="64px" Height="20" CssClass="queryButton"></asp:button><asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 80px; LEFT: 20px" id="lblErrorMessage"
				runat="server" Width="528px" Height="21px" CssClass="errorMsgColor">Place holder for err msg</asp:label>&nbsp;
			<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 54px; LEFT: 84px" id="btnGenerate"
				runat="server" Text="Generate" Width="82px" Height="20" CssClass="queryButton"></asp:button>
			<TABLE style="Z-INDEX: 104; POSITION: absolute; WIDTH: 799px; HEIGHT: 479px; TOP: 100px; LEFT: 16px"
				id="tblShipmentTracking" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 458px; HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 446px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" Width="77px" CssClass="tableHeadingFieldset" Font-Bold="True">Booking Date</asp:label></legend>
							<TABLE style="WIDTH: 360px; HEIGHT: 91px" id="Table1" border="0" cellSpacing="0" cellPadding="0"
								align="left" runat="server">
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Text="Month" Width="73px" Height="21px" CssClass="tableRadioButton"
											AutoPostBack="True" GroupName="EnterDate" Checked="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" Width="88px" Height="19px" CssClass="textField"></asp:dropdownlist>&nbsp;
										<asp:label id="Label5" runat="server" Width="30px" Height="22px" CssClass="tableLabel">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" Width="83" CssClass="textField" MaxLength="5" TextMaskType="msNumeric"
											NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
									<td></td>
								</TR>
								<TR>
									<td style="HEIGHT: 20px"></td>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Text="Period" Width="62px" CssClass="tableRadioButton"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" Width="83" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="HEIGHT: 20px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Text="Date" Width="74px" Height="22px" CssClass="tableRadioButton"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" Width="82" CssClass="textField" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 105px"><legend><asp:label id="lblPayerType" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Payer Type</asp:label></legend>
							<TABLE style="WIDTH: 300px; HEIGHT: 73px" id="tblPayerType" border="0" cellSpacing="0"
								cellPadding="0" align="left" runat="server">
								<tr>
									<td></td>
									<td height="1" colSpan="2">&nbsp;</td>
									<td></td>
									<td></td>
								</tr>
								<TR>
									<td></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;<asp:label id="Label6" runat="server" Width="88px" Height="22px" CssClass="tableLabel">Payer Type</asp:label></TD>
									<td><asp:listbox id="lsbCustType" runat="server" Width="137px" Height="61px" SelectionMode="Multiple"></asp:listbox></td>
									<td></td>
								</TR>
								<TR height="33">
									<td bgColor="blue"></td>
									<TD class="tableLabel" colSpan="2">&nbsp;
										<asp:label id="Label10" runat="server" Width="79px" Height="22px" CssClass="tableLabel">Top</asp:label></TD>
									<td><asp:dropdownlist id="ddTop" runat="server" Width="110px" CssClass="textField"></asp:dropdownlist></td>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 458px; HEIGHT: 134px" vAlign="top">
						<fieldset style="WIDTH: 447px; HEIGHT: 127px"><legend><asp:label id="lblRouteType" runat="server" Width="145px" CssClass="tableHeadingFieldset" Font-Bold="True">Route / DC Selection</asp:label></legend>
							<TABLE style="WIDTH: 438px; HEIGHT: 113px" id="tblRouteType" border="0" cellSpacing="0"
								cellPadding="0" align="left" runat="server">
								<TR>
									<TD style="WIDTH: 430px" class="tableLabel" colSpan="2">&nbsp;
										<asp:radiobutton id="rbLongRoute" runat="server" Text="Linehaul" Width="141px" Height="22px" CssClass="tableRadioButton"
											AutoPostBack="True" GroupName="QueryByRoute" Checked="True"></asp:radiobutton><asp:radiobutton id="rbShortRoute" runat="server" Text="Delivery Route" Width="122px" Height="22px"
											CssClass="tableRadioButton" AutoPostBack="True" GroupName="QueryByRoute"></asp:radiobutton><asp:radiobutton id="rbAirRoute" runat="server" Text="Air Route" Width="155px" Height="22px" CssClass="tableRadioButton"
											AutoPostBack="True" GroupName="QueryByRoute"></asp:radiobutton></TD>
								</TR>
								<tr>
									<TD class="tableLabel" width="20%">&nbsp;<asp:label id="lblRouteCode" runat="server" Width="111px" Height="22px" CssClass="tableLabel">Route Code</asp:label></TD>
									<td style="WIDTH: 243px"><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" Width="25px" CssClass="tableLabel" 
											Runat="server" TextBoxColumns="18" RegistrationKey="" TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboPathCodeSelect"></DBCOMBO:DBCOMBO></td>
								</tr>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label7" runat="server" Width="154px" Height="22px" CssClass="tableLabel">Origin Distribution Center</asp:label></TD>
									<TD style="WIDTH: 243px"><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" Width="25px" CssClass="tableLabel" 
											Runat="server" TextBoxColumns="18" RegistrationKey="" TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelect"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel">&nbsp;
                                        <asp:label id="Label8" runat="server" Width="181px" Height="22px" CssClass="tableLabel">Destination Distribution Center</asp:label>
									</TD>
									<TD style="WIDTH: 243px">
                                        <DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" Width="25px" CssClass="tableLabel" 
											Runat="server" TextBoxColumns="18" RegistrationKey="" TextUpLevelSearchButton="v" 
                                            ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelect"></DBCOMBO:DBCOMBO>
									</TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 89px"><legend><asp:label id="lblDestination" runat="server" Width="79px" CssClass="tableHeadingFieldset"
									Font-Bold="True">Destination</asp:label></legend>
							<TABLE style="WIDTH: 300px; HEIGHT: 89px" id="tblDestination" border="0" cellSpacing="0"
								cellPadding="0" align="left" runat="server">
								<tr height="37">
									<td></td>
									<TD style="WIDTH: 360px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblZipCode" runat="server" Width="84px" Height="22px" CssClass="tableLabel">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCode" runat="server" Width="139px" CssClass="textField" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<TD style="WIDTH: 360px" class="tableLabel" colSpan="3">&nbsp;
										<asp:label id="lblStateCode" runat="server" Width="100px" Height="22px" CssClass="tableLabel">State / Province</asp:label><cc1:mstextbox id="txtStateCode" runat="server" Width="139" CssClass="textField" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD>
						<table>
							<tr>
								<td>
									<FIELDSET style="Z-INDEX: 0; WIDTH: 248px; HEIGHT: 92px"><LEGEND><asp:label id="Label2" Runat="server">Retrieval Basis on</asp:label></LEGEND>
										<TABLE id="tblDates" border="0" width="100%" align="left" runat="server">
											<TR>
												<TD colSpan="20"><asp:radiobutton id="rbBooked" runat="server" Text="All Booked Shipments" Width="100%" Height="22px"
														CssClass="tableRadioButton" AutoPostBack="True" GroupName="Basis" Checked="True"></asp:radiobutton></TD>
											</TR>
											<TR>
												<TD colSpan="20"><asp:radiobutton id="rbInvoiced" runat="server" Text="Only Invoiced Shipments" Width="100%" Height="22px"
														CssClass="tableRadioButton" AutoPostBack="True" GroupName="Basis"></asp:radiobutton></TD>
											</TR>
											<TR>
												<TD colSpan="20"><asp:radiobutton id="rbDM" runat="server" Text="Only Delivery Manifested Shipments" Width="100%"
														Height="22px" CssClass="tableRadioButton" AutoPostBack="True" GroupName="Basis"></asp:radiobutton></TD>
											</TR>
										</TABLE>
									</FIELDSET>
								</td>
								<td>
									<FIELDSET style="Z-INDEX: 0; WIDTH: 248px; HEIGHT: 92px"><LEGEND><asp:label id="Label1" Runat="server">Revenue by Product Line</asp:label></LEGEND>
										<TABLE id="Table2" border="0" width="100%" align="left" runat="server">
											<TR>
												<TD colSpan="20"><asp:radiobutton id="rbtDomestic" runat="server" Text="Domestic Freight" Width="100%" Height="22px"
														CssClass="tableRadioButton" AutoPostBack="True" GroupName="ProductLine" Checked="True"></asp:radiobutton></TD>
											</TR>
											<TR>
												<TD colSpan="20"><asp:radiobutton id="rbtExports" runat="server" Text="Exports" Width="100%" Height="22px" CssClass="tableRadioButton"
														AutoPostBack="True" GroupName="ProductLine"></asp:radiobutton></TD>
											</TR>
											<TR>
												<TD colSpan="20"><asp:radiobutton id="rbtImport" runat="server" Text="Imports" Width="100%" Height="22px" CssClass="tableRadioButton"
														AutoPostBack="True" GroupName="ProductLine"></asp:radiobutton></TD>
											</TR>
										</TABLE>
									</FIELDSET>
								</td>
							</tr>
						</table>
					</TD>
					<TD>
						<FIELDSET style="Z-INDEX: 0; WIDTH: 248px; HEIGHT: 92px" align="top"><LEGEND><asp:label id="Label3" Runat="server">For Dom. Freight and Exports Show</asp:label></LEGEND>
							<TABLE id="Table3" border="0" width="100%" align="left" runat="server">
								<TR>
									<TD colSpan="20"><asp:radiobutton id="rbtCombined" runat="server" Text="Combined Revenue" Width="100%" Height="22px"
											CssClass="tableRadioButton" AutoPostBack="True" GroupName="ExportsShow" Checked="True"></asp:radiobutton></TD>
								</TR>
								<TR>
									<TD colSpan="20"><asp:radiobutton id="rbtFreight" runat="server" Text="Freight Revenue Only" Width="100%" Height="22px"
											CssClass="tableRadioButton" AutoPostBack="True" GroupName="ExportsShow"></asp:radiobutton></TD>
								</TR>
								<TR>
									<TD colSpan="20"><asp:radiobutton id="rbtDocument" runat="server" Text="Document Revenue Only" Width="100%" Height="22px"
											CssClass="tableRadioButton" AutoPostBack="True" GroupName="ExportsShow"></asp:radiobutton></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" colSpan="2" align="left">&nbsp;
						<fieldset style="WIDTH: 760px; HEIGHT: 61px" align="left"><LEGEND><asp:label id="Label4" Runat="server">Sort By</asp:label></LEGEND>
							<TABLE style="WIDTH: 480px; HEIGHT: 6px">
								<TR height="27">
									<TD colSpan="5"><asp:radiobutton id="rbRevenue" runat="server" Text="Revenue" Width="100%" Height="22px" CssClass="tableRadioButton"
											AutoPostBack="True" GroupName="Sort" Checked="True"></asp:radiobutton></TD>
									<TD colSpan="5"><asp:radiobutton id="rbChrgWt" runat="server" Text="Chargeable Weight" Width="100%" Height="22px"
											CssClass="tableRadioButton" AutoPostBack="True" GroupName="Sort"></asp:radiobutton></TD>
									<TD colSpan="5"><asp:radiobutton id="rbVolume" runat="server" Text="Volume" Width="100%" Height="22px" CssClass="tableRadioButton"
											AutoPostBack="True" GroupName="Sort"></asp:radiobutton></TD>
									<TD colSpan="5"><asp:radiobutton id="rbConsignment" runat="server" Text="Consignment" Width="100%" Height="22px"
											CssClass="tableRadioButton" AutoPostBack="True" GroupName="Sort"></asp:radiobutton></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
			</TABLE>
			<asp:label style="Z-INDEX: 103; POSITION: absolute; TOP: 10px; LEFT: 20px" id="lblTitle" runat="server"
				Width="365px" Height="27px" CssClass="maintitleSize">Top N Report</asp:label></form>
		</FORM>
	</body>
</HTML>
