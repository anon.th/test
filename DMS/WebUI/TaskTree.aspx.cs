using System;
using System.Globalization;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
//using System.Web.UI.WebControls;
//using System.Web.Services;
using System.Resources;
using System.IO;
using System.Threading;
using System.Data.SqlClient;
using com.common.RBAC;
using com.common.classes;
using com.common.util;
using com.common.applicationpages;

//namespace commom.webui.aspx.MainFrame
namespace com.ties
{
	/// <summary>
	/// This tree is created dynamically depending on the permission of the logged in user
	/// </summary>
	public class TaskTree : BasePopupPage
	{
		protected System.Web.UI.WebControls.TreeView TreeView1;
		//private Role role;
		protected String strNaviBgImage = null;
		protected String strStyleSheet = null;
		private TreeNode tn;
		private ArrayList childList;
		private CoreEnterprise enterprise = null;
	//	private Utility utility = null;
		private void Page_Load(object sender, System.EventArgs e)
		{
			enterprise = (CoreEnterprise)Session["OneEnterprise"];

			if(enterprise == null)
			{
				//return;
				throw new ApplicationException("Enterprise object is null");
			}

			strNaviBgImage = enterprise.NavigationBgImageURL;
			String tempStyle = enterprise.StyleSheetURL;
			if(tempStyle!="")
			{
				strStyleSheet = tempStyle;
			}
			else
			{
				//Default Style
				strStyleSheet = "css/Styles.css";
			}

            Module rootM = new Module();
            //utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
            rootM.ModuleId = "root";

            String strAppID = utility.GetAppID();
            String strEnterpriseID = utility.GetEnterpriseID();
            User user = RBACManager.GetUser(strAppID, strEnterpriseID, utility.GetUserID());

            tn = buildModulesTree(rootM, strAppID, strEnterpriseID, user);

            TreeView1.Nodes.AddAt(0, (TreeNode)this.CloneTreeNode(tn.ChildNodes[0]));

            if (!Page.IsPostBack)
            {
				TreeView1.Attributes.Add("onclick","window.top.displayBanner.fnCloseAll(0);");
				//TreeView1.CollapseAll();
            }

//			if(!Page.IsPostBack)
//			{
//				//Response.Buffer=true;
//				
//				Module rootM = new Module();
//				//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
//				rootM.ModuleId  = "root";			
//				tn = buildModulesTree(rootM);			
//				TreeView1.Nodes.AddAt(0,(TreeNode)tn.Nodes[0].Clone()); 
//			}
			
		}


        public TreeNode buildModulesTree(Module parentModule, string strAppID, string strEnterpriseID, User user)
        {
            TreeNode node = new TreeNode();

            if (parentModule.ModuleName != null)
            {
                String strModuleName = Utility.GetLanguageText(ResourceType.ModuleName, parentModule.ModuleName, utility.GetUserCulture());
                strModuleName = "<font face=" + System.Configuration.ConfigurationSettings.AppSettings["treeFontName"] + " size=" + System.Configuration.ConfigurationSettings.AppSettings["treeFontSize"] 
                                //+ " style='font-style: normal;font-variant: normal;font-weight: 400;color: black;'"
                                + ">" + strModuleName + "</font>";
                node.Text = strModuleName;
            }

            node.ImageUrl = parentModule.ModuleIconImage;
            node.Value = parentModule.ModuleId;
            node.NavigateUrl = parentModule.ModuleLinkURL;

            String moduleID = parentModule.ModuleId;

            try
            {
                childList = (ArrayList)RBACManager.GetAllModules(strAppID, strEnterpriseID, user, moduleID);
            }
            catch (ApplicationException appException)
            {
                String msg = appException.ToString();
                Logger.LogTraceError("TaskTree", "buildModulesTree", "TT001", "Error displaying tree");
                childList = new ArrayList();
                throw new ApplicationException("");
            }

            int cnt = childList.Count;

            foreach (Module child in childList)
            {
                //
                TreeNode childNode = buildModulesTree(child, strAppID, strEnterpriseID, user);
                if (childNode != null)
                {
                    childNode.NavigateUrl += "?MODID=" + childNode.Value + "&PARENTMODID=" + node.Value;
                    childNode.Target = "displayContent";
                }
                //node.SelectAction = TreeNodeSelectAction.Expand;
                node.SelectAction = TreeNodeSelectAction.SelectExpand;
                node.ChildNodes.Add(childNode);
            }

            return node;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{ 
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

//		private void TreeView1_SelectedIndexChange(object sender, System.Web.UI.WebControls.TreeViewSelectEventArgs e)
//		{
//			Session["btnExecQry_Con"] = "";
//			Session["BatchDate_Con"] = "";
//			Session["Location_Con"] = "";
//		}



	}
}
