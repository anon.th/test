<%@ Page language="c#" Codebehind="ZipCodeGeneratePopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.ZipCodeGeneratePopup" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ZipCodeGeneratePopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script runat="server">
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="ZipCodeGeneratePopup" method="post" runat="server">
			<TABLE id="tZipCode" style="Z-INDEX: 101; LEFT: 50px; WIDTH: 449px; POSITION: absolute; TOP: 41px; HEIGHT: 309px" cellSpacing="1" cellPadding="1" width="449" border="1">
				<TR>
					<TD width="3%"><asp:requiredfieldvalidator id="validCountry" ControlToValidate="txtCountry" Runat="server">*</asp:requiredfieldvalidator></TD>
					<TD style="WIDTH: 148px; HEIGHT: 21px"><asp:label id="lblCountry" runat="server" Width="142px" CssClass="tableLabel">Country</asp:label></TD>
					<TD style="HEIGHT: 21px"><cc1:mstextbox id="txtCountry" Width="243px" CssClass="tableField" Runat="server" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="10"></cc1:mstextbox></TD>
					<TD style="HEIGHT: 21px"><asp:button id="btnCountrySearch" runat="server" Text="..."></asp:button></TD>
				</TR>
				<TR>
					<TD width="3%"><asp:requiredfieldvalidator id="validStateCode" ControlToValidate="txtStateCode" Runat="server">*</asp:requiredfieldvalidator></TD>
					<TD style="WIDTH: 148px"><asp:label id="lblStateCode" runat="server" Width="142px" CssClass="tableLabel">State Code</asp:label></TD>
					<TD style="HEIGHT: 21px"><asp:textbox id="txtStateCode" Width="243px" CssClass="tableField" Runat="server"></asp:textbox></TD>
					<TD style="HEIGHT: 21px"><asp:button id="btnStateSearch" runat="server" Text="..."></asp:button></TD>
				</TR>
				<TR>
					<TD width="3%"><asp:requiredfieldvalidator id="validZoneCode" ControlToValidate="txtZoneCode" Runat="server">*</asp:requiredfieldvalidator></TD>
					<TD style="WIDTH: 148px"><asp:label id="lblZoneCode" runat="server" Width="142px" CssClass="tableLabel">Zone Code</asp:label></TD>
					<TD style="HEIGHT: 21px"><asp:textbox id="txtZoneCode" Width="243px" CssClass="tableField" Runat="server"></asp:textbox></TD>
					<TD style="HEIGHT: 21px"><asp:button id="btnZoneSearch" runat="server" Text="..."></asp:button></TD>
				</TR>
				<TR>
					<TD width="3%"></TD>
					<TD style="WIDTH: 148px"><asp:label id="lblCutoffTime" runat="server" Width="143px" CssClass="tableLabel">CutOff Time</asp:label></TD>
					<TD style="HEIGHT: 21px"><asp:textbox id="txtCutoffTime" Width="243px" CssClass="tableField" Runat="server"></asp:textbox></TD>
				</TR>
				<TR>
					<TD width="3%"></TD>
					<TD style="WIDTH: 148px"><asp:label id="lblPickupRoute" runat="server" Width="143px" CssClass="tableLabel">Pickup Route</asp:label></TD>
					<TD style="HEIGHT: 21px"><asp:textbox id="txtPickupRoute" Width="243px" CssClass="tableField" Runat="server"></asp:textbox></TD>
				</TR>
				<TR>
					<TD width="3%"></TD>
					<TD style="WIDTH: 148px"><asp:label id="lblDeliveryRoute" runat="server" Width="145px" CssClass="tableLabel">Delivery Route</asp:label></TD>
					<TD style="HEIGHT: 21px"><asp:textbox id="txtDeliveryRoute" Width="243px" CssClass="tableField" Runat="server"></asp:textbox></TD>
				</TR>
				<TR>
					<TD width="3%"></TD>
					<TD style="WIDTH: 148px"><asp:label id="lblArea" runat="server" Width="145px" CssClass="tableLabel">Area</asp:label></TD>
					<TD style="HEIGHT: 21px"><asp:textbox id="txtArea" Width="243px" CssClass="tableField" Runat="server"></asp:textbox></TD>
				</TR>
				<TR>
					<TD width="3%"></TD>
					<TD style="WIDTH: 148px"><asp:label id="lblRegion" runat="server" Width="145px" CssClass="tableLabel">Region</asp:label></TD>
					<TD style="HEIGHT: 21px"><asp:textbox id="txtRegion" Width="243px" CssClass="tableField" Runat="server"></asp:textbox></TD>
				</TR>
				<TR>
					<TD width="3%"><asp:requiredfieldvalidator id="validFZipCode" ControlToValidate="txtFZipCode" Runat="server">*</asp:requiredfieldvalidator></TD>
					<TD style="WIDTH: 148px"><asp:label id="lblFZipCode" runat="server" Width="142px" CssClass="tableLabel">From Zip Code</asp:label></TD>
					<TD style="HEIGHT: 21px"><asp:textbox id="txtFZipCode" Width="243px" CssClass="tableField" Runat="server"></asp:textbox></TD>
				</TR>
				<TR>
					<TD width="3%"><asp:requiredfieldvalidator id="validTZipCode" ControlToValidate="txtTZipCode" Runat="server">*</asp:requiredfieldvalidator></TD>
					<TD style="WIDTH: 151px"><asp:label id="lblTZipCode" runat="server" Width="142px" CssClass="tableLabel">To Zip Code</asp:label></TD>
					<TD style="HEIGHT: 21px"><asp:textbox id="txtTZipCode" Width="243px" CssClass="tableField" Runat="server"></asp:textbox></TD>
				</TR>
				<TR>
					<TD align="middle" colSpan="4"><asp:button id="btnGenerate" runat="server" Width="70px" Text="Generate"></asp:button><asp:button id="btnCancel" runat="server" Width="70px" Text="Cancel"></asp:button></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
