using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using TIESDAL;

namespace TIES.WebUI
{
	public class CustomsReport_ExportInvoices_Cheques : BasePage
	{
		#region Web Form Designer generated code
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.RadioButton rbInvoices;
		protected System.Web.UI.WebControls.RadioButton rbCheques;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.CheckBox chkShowOnlyNotExported;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lbError;
		protected System.Web.UI.WebControls.TextBox txtForNumberRange;
		protected System.Web.UI.WebControls.TextBox txtToNumberRange;
		protected System.Web.UI.WebControls.DataGrid gridInvoice;
		protected System.Web.UI.WebControls.DataGrid gridCheques;
		protected System.Web.UI.WebControls.Label lbInvoicesHeader;
		protected System.Web.UI.WebControls.Label lbCheques;
		protected System.Web.UI.WebControls.TextBox txtDefaultPath;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.TextBox txtInvoicesList;
		protected System.Web.UI.HtmlControls.HtmlTableRow trInvoiceList;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.rbInvoices.CheckedChanged += new System.EventHandler(this.rbInvoices_CheckedChanged);
			this.rbCheques.CheckedChanged += new System.EventHandler(this.rbCheques_CheckedChanged);
			this.gridInvoice.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.gridInvoice_PageIndexChanged);
			this.gridInvoice.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.gridInvoice_ItemDataBound);
			this.gridCheques.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.gridCheques_PageIndexChanged);
			this.gridCheques.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.gridCheques_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private string appID = string.Empty;
		private string enterpriseID = string.Empty;
		private string userLoggin = string.Empty;

		public DataSet dsExport
		{
			get
			{
				if(ViewState["dsExport"] == null)
					return new DataSet();
				else
					return (DataSet)ViewState["dsExport"];
			}
			set
			{
				ViewState["dsExport"] = value;
			} 
		}

		public int NumberScale
		{
			get
			{
				if(ViewState["NumberScale"] == null)
				{
					return 2;
				}
				else
				{
					return (int)ViewState["NumberScale"];
				}
			}
			set
			{
				ViewState["NumberScale"]=value;
			}
		}

		public int InvoicesGridRows
		{
			get
			{
				if(ViewState["InvoicesGridRows"] == null)
				{
					return 20;
				}
				else
				{
					return (int)ViewState["InvoicesGridRows"];
				}
			}
			set
			{
				ViewState["InvoicesGridRows"]=value;
			}
		}

		public int ChequesGridRows
		{
			get
			{
				if(ViewState["ChequesGridRows"] == null)
				{
					return 20;
				}
				else
				{
					return (int)ViewState["ChequesGridRows"];
				}
			}
			set
			{
				ViewState["ChequesGridRows"]=value;
			}
		}

		public string currency_decimal
		{
			get
			{
				if(ViewState["currency_decimal"] == null)
				{
					return "00";
				}
				else
				{
					return (string)ViewState["currency_decimal"];
				}
			}
			set
			{
				ViewState["currency_decimal"]=value;
			}
		}


		public string GetCurrencyDigitsOfEnterpriseConfiguration()
		{
			DataSet dsEprise = SysDataManager1.GetEnterpriseProfile(this.appID, this.enterpriseID);
			DataRow dr = dsEprise.Tables[0].Rows[0];
			
			string tempplate = "00";
			if(dr["currency_decimal"].ToString()!="")
			{
				tempplate="";
				int number_digit = Convert.ToInt32(dr["currency_decimal"].ToString());
				NumberScale = number_digit;
				for (int i = 0 ; i< number_digit ; i++)
				{
					tempplate = tempplate+"0";
				}
			}
			return "#,##0." + tempplate;
		}
		public  string GenInvoicesList( )
		{
			string strResult = "";
			try
			{
				foreach (DataGridItem row in gridInvoice.Items) 
				{
					Label lblInvoiceNo = row.FindControl("lbInvoiceNo") as Label;
					CheckBox chkSelect = row.FindControl("chkItem") as CheckBox;
					 
					if (chkSelect != null)
					{
						if (chkSelect.Checked)
							strResult = strResult + ";"+ lblInvoiceNo.Text;
					}
				}
				if (strResult != "")
				{
					if (strResult.StartsWith(";"))
						strResult = strResult.Substring(1, strResult.Length - 1);
					else
						strResult = strResult.Substring(0, strResult.Length - 1);
				}
			}
			catch (Exception ex)
			{
				this.lbError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
			return strResult;

		}
		public  string GenChequesList( )
		{
			string strResult = "";
			try
			{
				foreach (DataGridItem row in gridCheques.Items) 
				{
					Label lbChequeNo = row.FindControl("lbChequeNo") as Label;
					CheckBox chkSelect = row.FindControl("chkItem2") as CheckBox;
					 
					if (chkSelect != null)
					{
						if (chkSelect.Checked)
							strResult = strResult + ";"+ lbChequeNo.Text;
					}
				}
				if (strResult != "")
				{
					if (strResult.StartsWith(";"))
						strResult = strResult.Substring(1, strResult.Length - 1);
					else
						strResult = strResult.Substring(0, strResult.Length - 1);
				}
			}
			catch (Exception ex)
			{
				this.lbError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
			return strResult;

		}
		private void DisplayGridExport()
		{
			try
			{
				//if(txtForNumberRange.Text.Trim().Equals(""))
				//{
				//	this.lbError.Text = "Please enter number Range";
				//	return;
				//}

				if(!txtToNumberRange.Text.Trim().Equals(""))
				{
					int forNumberRange = 0;
					if(!txtForNumberRange.Text.Trim().Equals(""))
					{
						forNumberRange = int.Parse(txtForNumberRange.Text.Trim());
					}

					int toNumberRang = int.Parse(txtToNumberRange.Text.Trim());

					if(toNumberRang < forNumberRange)
					{
						this.lbError.Text = "Start rang must by greater than End rang";
						return;
					}
				}

				this.lbError.Text = string.Empty;

				string typeExport = (rbInvoices.Checked) ? "0" : "1";
				string checkShowOnly = (chkShowOnlyNotExported.Checked) ? "1" : "0";

				ExportInvoicesChequesDAL db = new ExportInvoicesChequesDAL(this.appID, this.enterpriseID);
				TIESClasses.ExportInvoicesCheques objInfo = new TIESClasses.ExportInvoicesCheques(this.enterpriseID,
					this.userLoggin,
					typeExport,
					this.txtForNumberRange.Text.Trim(),
					this.txtToNumberRange.Text.Trim(),
					checkShowOnly,
					txtInvoicesList.Text.Trim());
				dsExport = db.ExecCustomsSearchInvoicesCheques(objInfo);
				if(typeExport.Equals("0"))
				{
					gridInvoice.PageSize=InvoicesGridRows;
					gridInvoice.CurrentPageIndex = 0;
					gridInvoice.DataSource = dsExport.Tables[0];
					gridInvoice.DataBind();

					this.lbError.Text = string.Format("{0} row(s) returned", dsExport.Tables[1].Rows[0][0].ToString());
				}
				else if(typeExport.Equals("1"))
				{
					gridCheques.PageSize=ChequesGridRows;
					gridCheques.CurrentPageIndex = 0;
					gridCheques.DataSource = dsExport.Tables[0];
					gridCheques.DataBind();

					this.lbError.Text = string.Format("{0} row(s) returned", dsExport.Tables[1].Rows[0][0].ToString());
				}
			}
			catch(Exception ex)
			{
				this.lbError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void ExportData()
		{
			try
			{
				//btnExportSelected.Enabled = false;

				string typeExport = (rbInvoices.Checked) ? "0" : "1";
				string itemList = string.Empty;
				string fileName = string.Empty;

				StringBuilder csvData = new StringBuilder();
				DataSet dsResult = new DataSet();
			
				ExportInvoicesChequesDAL db = new ExportInvoicesChequesDAL(this.appID, this.enterpriseID);
				if(typeExport.Equals("0"))
				{
					itemList = GenInvoicesList();
					fileName = "Invoice" + DateTime.Now.ToString("yyyyMMddHHmm");
				}
				else
				{
					itemList = GenChequesList();
					fileName = "Cheque" + DateTime.Now.ToString("yyyyMMddHHmm");
				}
			
				TIESClasses.ExportInvoicesCheques objInfo = new TIESClasses.ExportInvoicesCheques(this.enterpriseID,
					this.userLoggin,
					typeExport,
					itemList);
				dsResult = db.ExecCustomsExportInvoicesCheques(objInfo);
				
				foreach(DataRow row in dsResult.Tables[0].Rows)
					csvData.Append(row[1].ToString() + Environment.NewLine);				

				String strUrl = "TempExprot.aspx";
				Session["FORMID"] = "ExportInvoiceAndCheques";
				Session["FileName"] = fileName;
				Session["CSVData"] = csvData.ToString();
				
				ArrayList paramList = new ArrayList();
				
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentExport.js", paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

				Query();
		
				//Response.Clear();
				//Response.ContentType = "text/csv";
				//Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}", fileName + ".csv"));
				//Response.Write(csvData.ToString());
				//Context.Response.End();
			}
			catch (Exception ex)
			{
				this.lbError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void InitGridInvoice()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("Invoice_No");
			dt.Columns.Add("cust_name");
			dt.Columns.Add("PrintedDT");
			dt.Columns.Add("LastModifiedDT");
			dt.Columns.Add("ExportedDT");
			dt.Columns.Add("Amount");

			dt.AcceptChanges();

			gridInvoice.EditItemIndex = -1;
			gridInvoice.CurrentPageIndex = 0;

			gridInvoice.DataSource = dt;
			gridInvoice.DataBind();
		}

		private void InitGridCheques()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("Cheque_No");
			dt.Columns.Add("requisition_no");
			dt.Columns.Add("payee");
			dt.Columns.Add("ApprovedDT");
			dt.Columns.Add("ExportedDT");
			dt.Columns.Add("Amount");

			dt.AcceptChanges();

			gridCheques.EditItemIndex = -1;
			gridCheques.CurrentPageIndex = 0;

			gridCheques.DataSource = dt;
			gridCheques.DataBind();
		}

		private void InitPath()
		{
			ExportInvoicesChequesDAL db = new ExportInvoicesChequesDAL(appID, enterpriseID);
			DataSet dsConfig = db.GetEnterpriseConfigurationsPath(enterpriseID);
			txtDefaultPath.Text =String.Empty;
			foreach(DataRow dr in dsConfig.Tables[0].Rows)
			{
				if(dr["key"].ToString()=="ExportFolder")
				{
					txtDefaultPath.Text=dr["value"].ToString();
				}
				else if(dr["key"].ToString()=="InvoicesGridRows")
				{
					InvoicesGridRows = (dr["value"] != DBNull.Value?Convert.ToInt32(dr["value"].ToString()):200);
				}
				else if(dr["key"].ToString()=="ChequesGridRows")
				{
					ChequesGridRows = (dr["value"] != DBNull.Value?Convert.ToInt32(dr["value"].ToString()):200);
				}
			}
		}
		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				System.Text.StringBuilder s = new System.Text.StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}
		private void Query()
		{
			this.InitGridInvoice();
			this.InitGridCheques();
			this.txtForNumberRange.Text = string.Empty;
			this.txtToNumberRange.Text = string.Empty;
			txtInvoicesList.Text = string.Empty;
			this.chkShowOnlyNotExported.Checked = false;
			//this.btnExportSelected.Enabled = false;
			this.lbInvoicesHeader.Visible= true;
			this.lbCheques.Visible = false;
			this.gridInvoice.Visible = true;
			this.gridCheques.Visible = false;
			this.rbInvoices.Checked = true;
			this.rbCheques.Checked = false;
			trInvoiceList.Visible=true;
			this.lbError.Text = string.Empty;
			SetInitialFocus(this.txtForNumberRange);
			RegisterStartupScript("DisabledExport", "<script language=JavaScript>document.getElementById('btnExport').disabled = true;</script>");
		}
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				string ServerAction = string.Empty;

				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings, Page.Session);
				appID = utility.GetAppID();
				enterpriseID = utility.GetEnterpriseID();
				userLoggin = utility.GetUserID();

				if(!Page.IsPostBack)
				{
					InitPath();
					ServerAction = Request.QueryString["ServerAction"] + string.Empty;
					currency_decimal =  this.GetCurrencyDigitsOfEnterpriseConfiguration();
					InitGridInvoice();
					SetInitialFocus(this.txtForNumberRange);
				}
				else
					ServerAction = Request.Form["ServerAction"] + string.Empty;
				
				switch (ServerAction)
				{
					case "EXPORTDATA":
						ExportData();
						//RegisterStartupScript("DisabledExport", "<script language=JavaScript>document.getElementById('btnExport').disabled = false;</script>");
						break;
				}

			}
			catch(Exception ex)
			{
				this.lbError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			DisplayGridExport();
		}

		private void gridInvoice_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				this.lbError.Text = string.Empty;
				if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
				{
					DataRowView dr = (DataRowView)e.Item.DataItem;

					Label lbPrintedDT = (Label)e.Item.FindControl("lbPrintedDT");
					Label lbLastModifiedDT = (Label)e.Item.FindControl("lbLastModifiedDT");
					Label lbExportedDT = (Label)e.Item.FindControl("lbExportedDT");
					Label lbAmount = (Label)e.Item.FindControl("lbAmount");
					Label lbInvoiceNo = (Label)e.Item.FindControl("lbInvoiceNo");

					if(lbInvoiceNo != null)
						lbInvoiceNo.Text = dr["Invoice_No"].ToString();
					if(lbPrintedDT != null)
					{
						if(!dr["PrintedDT"].ToString().Equals(""))
							lbPrintedDT.Text = DateTime.Parse(dr["PrintedDT"].ToString()).ToString("dd/MM/yyyy hh:mm");
						else
							lbPrintedDT.Text = string.Empty;
					}
					if(lbLastModifiedDT != null)
					{
						if(!dr["LastModifiedDT"].ToString().Equals(""))
							lbLastModifiedDT.Text = DateTime.Parse(dr["LastModifiedDT"].ToString()).ToString("dd/MM/yyyy hh:mm");
						else
							lbLastModifiedDT.Text = string.Empty;
					}
					if(lbExportedDT != null)
					{
						if(!dr["ExportedDT"].ToString().Equals(""))
							lbExportedDT.Text = DateTime.Parse(dr["ExportedDT"].ToString()).ToString("dd/MM/yyyy hh:mm");
						else
							lbExportedDT.Text = string.Empty;
					}
					if(lbAmount != null)
					{
						if(!dr["Amount"].ToString().Equals(""))
							lbAmount.Text = double.Parse(dr["Amount"].ToString()).ToString(currency_decimal);
						else
							lbAmount.Text = string.Empty;
					}
				}
			}
			catch(Exception ex)
			{
				this.lbError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void gridInvoice_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			try
			{
				gridInvoice.CurrentPageIndex = e.NewPageIndex;

				gridInvoice.DataSource = dsExport.Tables[0];
				gridInvoice.DataBind();
			}
			catch(Exception ex)
			{
				this.lbError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void btnExportSelected_Click(object sender, System.EventArgs e)
		{
			ExportData();
		}

		private void rbInvoices_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbInvoices.Checked)
			{
				this.InitGridInvoice();
				this.InitGridCheques();
				this.txtForNumberRange.Text = string.Empty;
				this.txtToNumberRange.Text = string.Empty;
				//this.btnExportSelected.Enabled = false;
				this.lbInvoicesHeader.Visible= true;
				this.chkShowOnlyNotExported.Checked = false;
				this.lbCheques.Visible = false;
				this.gridInvoice.Visible = true;
				this.gridCheques.Visible = false;
				this.lbError.Text = string.Empty;
				SetInitialFocus(this.txtForNumberRange);
				RegisterStartupScript("DisabledExport", "<script language=JavaScript>document.getElementById('btnExport').disabled = true;</script>");
				trInvoiceList.Visible=true;
			}

		}

		private void rbCheques_CheckedChanged(object sender, System.EventArgs e)
		{
			this.InitGridInvoice();
			this.InitGridCheques();
			this.txtForNumberRange.Text = string.Empty;
			this.txtToNumberRange.Text = string.Empty;
			//this.btnExportSelected.Enabled = false;
			this.lbInvoicesHeader.Visible= false;
			this.chkShowOnlyNotExported.Checked = false;
			this.lbCheques.Visible = true;
			trInvoiceList.Visible=true;
			this.gridInvoice.Visible = false;
			this.gridCheques.Visible = true;
			this.lbError.Text = string.Empty;
			SetInitialFocus(this.txtForNumberRange);

			RegisterStartupScript("DisabledExport", "<script language=JavaScript>document.getElementById('btnExport').disabled = true;</script>");
		}

		private void gridCheques_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			try
			{
				gridCheques.CurrentPageIndex = e.NewPageIndex;

				gridCheques.DataSource = dsExport.Tables[0];
				gridCheques.DataBind();
			}
			catch(Exception ex)
			{
				this.lbError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void gridCheques_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				this.lbError.Text = string.Empty;
				if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
				{
					DataRowView dr = (DataRowView)e.Item.DataItem;

					Label lbChequeNo = (Label)e.Item.FindControl("lbChequeNo");
					Label lbApprovedDT = (Label)e.Item.FindControl("lbApprovedDT");
					Label lbChequeExportedDT = (Label)e.Item.FindControl("lbChequeExportedDT");
					Label lbChequeAmount = (Label)e.Item.FindControl("lbChequeAmount");
					
					if(lbChequeNo != null)
						lbChequeNo.Text = dr["Cheque_No"].ToString();
					if(lbApprovedDT != null)
					{
						if(!dr["ApprovedDT"].ToString().Equals(""))
							lbApprovedDT.Text = DateTime.Parse(dr["ApprovedDT"].ToString()).ToString("dd/MM/yyyy hh:mm");
						else
							lbApprovedDT.Text = string.Empty;
					}
					if(lbChequeExportedDT != null)
					{
						if(!dr["ExportedDT"].ToString().Equals(""))
							lbChequeExportedDT.Text = DateTime.Parse(dr["ExportedDT"].ToString()).ToString("dd/MM/yyyy hh:mm");
						else
							lbChequeExportedDT.Text = string.Empty;
					}
					if(lbChequeAmount != null)
					{
						if(!dr["Amount"].ToString().Equals(""))
							lbChequeAmount.Text = double.Parse(dr["Amount"].ToString()).ToString(currency_decimal);
						else
							lbChequeAmount.Text = string.Empty;
					}
				}
			}
			catch(Exception ex)
			{
				this.lbError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			Query();
		}
	}
}
