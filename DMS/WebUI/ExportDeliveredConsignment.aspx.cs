using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using TIESDAL;
using TIESClasses;

using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;

using com.common.applicationpages;
using com.ties;
using System.Text;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Configuration;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for ExportDeliveredConsignment.
	/// </summary>
	public class ExportDeliveredConsignment : com.common.applicationpages.BasePage
	{
		#region VALIABLE

		private string appID = null;
		private string PODDate2 = null;
		private string enterpriseID = null;
		private string userID = null;
		public int uncheck = 0;
		public int dtCount = 0;
		DataSet dsConfig = null;
		string m_strDeploymentEnv = "false";
		string m_strEnterpriseID = null;
		string m_strUserID = null;
		HttpSessionState m_session = null;

		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.WebControls.ValidationSummary Validationsummary1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Label lblServiceType;
		protected System.Web.UI.WebControls.Label lblShowOnly;
		protected System.Web.UI.WebControls.Label lblInclude;
		protected System.Web.UI.WebControls.Label lblOnIn;
		protected System.Web.UI.WebControls.Label lblPOD;
		protected System.Web.UI.HtmlControls.HtmlTableRow TrNonCustoms;
		protected System.Web.UI.HtmlControls.HtmlTableRow TrCustoms;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.DropDownList ddlExportFormat;
		protected System.Web.UI.WebControls.DataGrid dgExportDelivered;
		protected System.Web.UI.WebControls.CheckBox chkOnInterlMAWB;
		protected System.Web.UI.WebControls.CheckBox chkOnIfNot;
		protected System.Web.UI.WebControls.CheckBox chkIncludeInter;
		protected System.Web.UI.WebControls.TextBox txtTest;
		protected System.Web.UI.WebControls.CheckBox chkNo;
		protected com.common.util.msTextBox txtPodDate2;
		protected com.common.util.msTextBox txtPodDate;
		protected System.Web.UI.WebControls.Label lblTo;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.TextBox MasterAWBNo;
		protected System.Web.UI.WebControls.TextBox HouseAWBNo;
		protected System.Web.UI.WebControls.Button btnExportConsign;
        protected System.Web.UI.WebControls.Button btnExportConsignFTP;
        #endregion

        #region VIEWSTATE_VALIABLE
        public int GridRows
		{
			get
			{
				if(ViewState["GridRows"]==null)
				{
					return 2;
				}
				else
				{
					return (int)ViewState["GridRows"];
				}
			}
			set
			{
				ViewState["GridRows"]=value;
			}
		}

		public DateTime ExcPodDate
		{
			get
			{
				if(ViewState["ExcPodDate"]==null)
				{
					return DateTime.MinValue;
				}
				else
				{
					return (DateTime)ViewState["ExcPodDate"];
				}
			}	
			set
			{
				ViewState["ExcPodDate"]=value;
			}
		}
		public DateTime ExcPodDate2
		{
			get
			{
				if(ViewState["ExcPodDate2"]==null)
				{
					return DateTime.MinValue;
				}
				else
				{
					return (DateTime)ViewState["ExcPodDate2"];
				}
			}	
			set
			{
				ViewState["ExcPodDate2"]=value;
			}
		}
		
		public int ExcOnIntMAWB
		{
			get
			{
				if(ViewState["ExcOnIntMAWB"]==null)
				{
					return -1;
				}
				else
				{
					return (int)ViewState["ExcOnIntMAWB"];
				}
			}
			set
			{
				ViewState["ExcOnIntMAWB"]=value;
			}
		}
		public int ExcIncludeIntlDocs
		{
			get
			{
				if(ViewState["ExcIncludeIntlDocs"]==null)
				{
					return -1;
				}
				else
				{
					return (int)ViewState["ExcIncludeIntlDocs"];
				}
			}
			set
			{
				ViewState["ExcIncludeIntlDocs"]=value;
			}
		}
		public int OnlyIfNotExported
		{
			get
			{
				if(ViewState["OnlyIfNotExported"]==null)
				{
					return -1;
				}
				else
				{
					return (int)ViewState["OnlyIfNotExported"];
				}
			}
			set
			{
				ViewState["OnlyIfNotExported"]=value;
			}
		}

		public DataTable dtList
		{
			get
			{
				if(ViewState["dtList"]==null)
				{
					return null;
				}
				else
				{
					return (DataTable)ViewState["dtList"];
				}
			}
			set
			{
				ViewState["dtList"]=value;
			}
		}

		public string ExcExportFormat
			 {
				 get
				 {
					 if(ViewState["ExcExportFormat"]==null)
					 {
						 return "";
					 }
					 else
					 {
						 return (string)ViewState["ExcExportFormat"];
					 }
				 }
				 set
				 {
					 ViewState["ExcExportFormat"]=value;
				 }
			 }

		public string ExcConsignment
		{
			get
			{
				if(ViewState["ExcConsignment"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["ExcConsignment"];
				}
			}
			set
			{
				ViewState["ExcConsignment"]=value;
			}
		}

		public string tpFilename
		{
			get
			{
				if(ViewState["tpFilename"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["tpFilename"];
				}
			}
			set
			{
				ViewState["tpFilename"]=value;
			}
		}

		#endregion

		#region ACTION

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			// enterpriseID = "PNGAF";
			
			
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);

			appID = GetAppID();
			enterpriseID = GetEnterpriseID();
			userID = utility.GetUserID();
					
			if(!Page.IsPostBack)
			{
				dsConfig = EnterpriseConfigMgrDAL.ExportDelConsGridRows(appID,enterpriseID);
				
				if(dsConfig.Tables[0].Rows.Count>0 && dsConfig.Tables[0].Rows[0]["key"] != DBNull.Value
					&& dsConfig.Tables[0].Rows[0]["key"].ToString().ToLower() =="gridrows")
				{
					try
					{
						GridRows=Convert.ToInt32(dsConfig.Tables[0].Rows[0]["value"].ToString());
						tpFilename = dsConfig.Tables[0].Rows[1]["value"].ToString();
					}
					catch
					{
						GridRows=10;
						Logger.LogTraceError("EnterpriseConfigMgrDAL","ConsStatusPrintingGridRows","","Error ");
					}

				}
				Session["DATA"] = null;
				BindDDL();
				BindControl();
			}
			else
			{
				if(dtList != null)
				{
					SaveTempData();
					btnExportConsign.Enabled = false;
                    btnExportConsignFTP.Enabled = false;

                    foreach (DataRow x in dtList.Rows)
					{
						if((bool)x["Check"] == true)
						{
							btnExportConsign.Enabled = true;
                            btnExportConsignFTP.Enabled = true;
                            break;
						}
					}
				}
			}
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnExportConsign.Click += new System.EventHandler(this.btnExportConsign_Click);
            this.btnExportConsignFTP.Click += new System.EventHandler(this.btnExportConsignFTP_Click);
            this.dgExportDelivered.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgExportDelivered_PageIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void SaveTempData()
		{
			foreach(DataRow row in dtList.Rows)
			{
				foreach(DataGridItem x in dgExportDelivered.Items)
				{
					Label lblBookNo = (Label)x.FindControl("lblBookNo");
					CheckBox chkNo = (CheckBox)x.FindControl("chkNo");
					if(row["booking_no"].ToString() ==lblBookNo.Text)
					{
						row["Check"] = chkNo.Checked;
					}

				}
			}
		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			//Binding Data
			this.ExcPodDate =DateTime.MinValue;
			bool isValidate =true;
			if(HouseAWBNo.Text.Trim() =="" && MasterAWBNo.Text.Trim()=="")
			{
				if(txtPodDate.Text.Trim() != "")
				{
					try
					{
						this.ExcPodDate  = DateTime.ParseExact(txtPodDate.Text.Trim(),"dd/MM/yyyy",null);
					}
					catch
					{
						this.ExcPodDate = DateTime.MinValue;
					}
				}
				else
				{

					this.ExcPodDate = DateTime.MinValue;
					lblErrorMsg.Text = "Actual POD Date is required";
					isValidate=false;
					return;
				}

				if(txtPodDate2.Text.Trim() != "")
				{
					try
					{
						this.ExcPodDate2  = DateTime.ParseExact(txtPodDate2.Text.Trim(),"dd/MM/yyyy",null);
						if(ExcPodDate > ExcPodDate2)
						{				
							lblErrorMsg.Text = "To Date must be greater than or equal to from date.";
							isValidate=false;
							return;
						}
					}
					catch
					{
						this.ExcPodDate2 = DateTime.MinValue;
					}
				}
				else
				{
					this.ExcPodDate2 = ExcPodDate;
				}
			}
			
			this.ExcOnIntMAWB =0;
			if(chkOnInterlMAWB.Checked)
			{
				try
				{
					this.ExcOnIntMAWB  = 1;
				}
				catch
				{
					lblErrorMsg.Text ="ERROR ExcOnIntMAWB";
					isValidate=false;
					return;
				}
			}
			else
			{
				this.ExcOnIntMAWB  = 0;
			}

			this.ExcIncludeIntlDocs =0;
			if(chkIncludeInter.Checked)
			{
				try
				{
					this.ExcIncludeIntlDocs  = 1;
				}
				catch
				{
					lblErrorMsg.Text ="ERROR ExcIncludeIntlDocs";
					isValidate=false;
					return;
				}
			}
			else
			{
				this.ExcIncludeIntlDocs  = 0;
			}

			this.OnlyIfNotExported = 0;
			if(chkOnIfNot.Checked)
			{
				try
				{
					this.OnlyIfNotExported  = 1;
				}
				catch
				{
					lblErrorMsg.Text ="ERROR OnlyIfNotExported";
					isValidate=false;
					return;
				}
			}
			else
			{
				this.OnlyIfNotExported  = 0;
			}

			if(ddlExportFormat.SelectedValue == "COMNEX")
			{
				ExcExportFormat = ddlExportFormat.SelectedValue.ToString();
			}

			if(isValidate==true)
			{
				string strAppID = this.appID;
				string strEnterpriseID = this.enterpriseID;
				string PODDate = (this.ExcPodDate != DateTime.MinValue? this.ExcPodDate.ToString("yyyy-MM-dd"):"");
				PODDate2="";
				if(txtPodDate2.Text.Trim() != "")
				{
					PODDate2 = this.ExcPodDate2.ToString("yyyy-MM-dd");
				}
				int OnIntlMAWB = this.ExcOnIntMAWB;
				int IncludeIntlDocs = this.ExcIncludeIntlDocs;
				int OnlyIfNotExported = this.OnlyIfNotExported;
				string ExportFormat = this.ExcExportFormat;

				DataSet dsResult = ExportDeliveredConsignmentDAL.SearchPODs(strAppID,strEnterpriseID,PODDate,PODDate2,OnIntlMAWB,IncludeIntlDocs,OnlyIfNotExported,MasterAWBNo.Text.Trim(),HouseAWBNo.Text.Trim(),ExportFormat);
				dtList = dsResult.Tables[0];
				Session["dtCount"] = dtList.Rows.Count;

				
				dtList.Columns.Add("Check",typeof(bool));
				foreach (DataRow d in dtList.Rows)
				{ 
					d["Check"] = true;
				}
				
				BindGrid();
			}			
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			BindControl();
		}


		private void btnExportConsign_Click(object sender, System.EventArgs e)
		{
			ExportConsign(); 
		}

        private void btnExportConsignFTP_Click(object sender, System.EventArgs e)
        {
            ExportConsignFTP();
        }

        private void dgExportDelivered_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
		
			SaveTempData();
			
			dgExportDelivered.CurrentPageIndex = e.NewPageIndex;
			
			
			BindGrid();
		}
		#endregion	

		#region FUNCTION

		private string getCheckbox()
		{
			dtCount = dtList.Rows.Count;

			string[] data = new string[dtCount];
			int i=0;

            ExcConsignment = "";

            foreach (DataRow row in dtList.Rows)
			{
				if((bool)row["Check"] == true)
				{
					data[i] = row["booking_no"].ToString() + "," + row["consignment_no"].ToString()+ ";";
					ExcConsignment = ExcConsignment + data[i];
				}
				i++;
			}
			ExcConsignment = ExcConsignment.TrimEnd(ExcConsignment[ExcConsignment.Length -1]);

			//				CheckBox chkCheck = (CheckBox)dgExportDelivered.Items[i].FindControl("chkNo");
			//				Label lblBookNo = (Label)dgExportDelivered.Items[i].FindControl("lblBookNo");
			//				Label lblCon = (Label)dgExportDelivered.Items[i].FindControl("lblConNo");
			//				
			//				if(chkCheck.Checked == true)
			//				{
			//					data[i] = lblBookNo.Text + "," + lblCon.Text + ";";
			//					ExcConsignment = ExcConsignment + data[i];
			//				}
			//
			//			}
			//			ExcConsignment = ExcConsignment.TrimEnd(ExcConsignment[ExcConsignment.Length -1]);

			return ExcConsignment;
		}
		
		public String GetEnterpriseID()
		{
			if (m_strDeploymentEnv.Equals("false"))
			{
				m_strEnterpriseID = System.Configuration.ConfigurationSettings.AppSettings["enterpriseID"];
			}
			else
			{
				m_strEnterpriseID = (String)m_session["enterpriseID"];
			}
			return m_strEnterpriseID;
		}

		public String GetAppID()
		{
			string m_strAppID = null;
			m_strAppID = System.Configuration.ConfigurationSettings.AppSettings["applicationID"];
			return m_strAppID;
		}

		private void BindGrid()
		{
			lblErrorMsg.Text = dtList.Rows.Count.ToString() + " row(s) returned";
			if(dtList != null)
			{
				this.dgExportDelivered.DataSource = dtList;
				this.dgExportDelivered.PageSize = GridRows;
				this.dgExportDelivered.DataBind();
			}
			else
			{
				this.dgExportDelivered.DataSource = null;
				this.dgExportDelivered.PageSize = GridRows;
				this.dgExportDelivered.DataBind();
			}
			
			if(dtList.Rows.Count >0 )
			{
				btnExecQry.Enabled= false;
				btnExportConsign.Enabled = true;
                btnExportConsignFTP.Enabled = true;
			}
			else
			{
				btnExecQry.Enabled= true;
			}
		
			btnExportConsign.Enabled = false;
            btnExportConsignFTP.Enabled = false;


            if (dtList != null)
			{
				btnExportConsign.Enabled = false;
                btnExportConsignFTP.Enabled = false;

                foreach (DataRow x in dtList.Rows)
				{
					if((bool)x["Check"] == true)
					{
						btnExportConsign.Enabled = true;
                        btnExportConsignFTP.Enabled = true;

                        break;
					}
				}
			}
		}

		private void BindControl()
		{
			dtList =null;

			btnExecQry.Enabled = true;
			txtPodDate.Text = "";
			txtPodDate2.Text = "";
			lblErrorMsg.Text = "";
			chkIncludeInter.Checked = true;
			chkOnIfNot.Checked = true;
			chkOnInterlMAWB.Checked = true;
			btnExportConsign.Enabled = false;
            btnExportConsignFTP.Enabled = false;
            this.MasterAWBNo.Text="";
			this.HouseAWBNo.Text="";
			DataTable table = new DataTable();
			table.Columns.Add(" ", typeof(string));
			table.Columns.Add("Number", typeof(string));
			table.Columns.Add("POD Date/Time", typeof(string));
			table.Columns.Add("Consignee", typeof(string));
			table.Columns.Add("Reference No.", typeof(string));
			table.Columns.Add("Delivery DC", typeof(string));
			table.Columns.Add("Exported", typeof(string));
			table.Columns.Add("Intl. MAWB", typeof(string));
			table.Columns.Add("Pkgs", typeof(string));

			this.dgExportDelivered.DataSource = table;
			this.dgExportDelivered.PageSize = GridRows;
			this.dgExportDelivered.CurrentPageIndex = 0;
			this.dgExportDelivered.DataBind();
		}

		private void ExportConsign()
		{
			string strAppID = this.appID;
			string strEnterpriseID = this.enterpriseID;
			string userloggedin = this.userID;
			string strConsignment = getCheckbox();
			string ExportFormat = this.ExcExportFormat;

			DataSet dsResult = ExportDeliveredConsignmentDAL.ExportPODs(strAppID,strEnterpriseID,userloggedin,strConsignment,ExportFormat);
			System.Data.DataTable dtListEx = dsResult.Tables[0];
			string stringData  = dtListEx.Rows[0]["@FileString"].ToString();
					
			ExportTxt(stringData);
			BindControl();
			
		}

        private void ExportConsignFTP()
        {
            string strAppID = this.appID;
            string strEnterpriseID = this.enterpriseID;
            string userloggedin = this.userID;
            string strConsignment = getCheckbox();
            string ExportFormat = this.ExcExportFormat;

            DataSet dsResult = ExportDeliveredConsignmentDAL.ExportPODFTP(strAppID, strEnterpriseID, userloggedin, strConsignment, ExportFormat);
            System.Data.DataTable dtListEx = dsResult.Tables[0];
            string stringData = dtListEx.Rows[0]["@FileString"].ToString();

            if (ExportFTP(stringData))
            {
                ExportDeliveredConsignmentDAL.ExportPODs(strAppID, strEnterpriseID, userloggedin, strConsignment, ExportFormat);
                DownloadTextFile();
            }

            BindControl();
        }

        private void BindDDL()
		{
			ddlExportFormat.Items.Insert(0, new ListItem("COMNEX","COMNEX"));
		}

		private void ExportTxt(string stringData)
		{
            Session["XMLText"] = stringData;
			Session["XMLFilename"] = tpFilename;

			Response.Write("<script language='javascript'>");
			Response.Write("var child=window.open('TempExportDelivery.aspx', '', 'width=1,height=1');");	
			Response.Write("child.moveTo(10000,10000);");	
			Response.Write("</script>");	

		}

        private Boolean ExportFTP(string stringData)
        {
            string XMLText = stringData;
            string XMLFilename = tpFilename;

            // Split File
            string[] lsFilename = Regex.Split(XMLFilename, "YYYY");
            string[] strline = XMLText.Split('\n');

            string path = Server.MapPath("~") + "/WebUI/Export/";
            string filename = lsFilename[0] + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";

            // Write File
            StreamWriter file = new StreamWriter(path + filename);
            foreach (string x in strline)
            {
                if (x.Trim() != "")
                {
                    file.WriteLine(x);
                }
            }

            file.Close();

            try
            {
                UploadFtpFile(ConfigurationSettings.AppSettings["DescPathFTP"], path + filename);

                Session["XMLFileName"] = filename;
                Session["XMLFilePath"] = path;

                return true;
            }
            catch (Exception ex)
            {
                Page.RegisterStartupScript("warning", "<script>alert('The system cannot transfer files to the destination. Please check FTP !!!');</script>");

                return false;
            }
        }

        public void UploadFtpFile(string folderName, string fileName)
        {
            FtpWebRequest request;

            string absoluteFileName = Path.GetFileName(fileName);

            request = WebRequest.Create(new Uri(string.Format(@"ftp://{0}/{1}/{2}", ConfigurationSettings.AppSettings["ServerFTP"], folderName, absoluteFileName))) as FtpWebRequest;
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.UseBinary = true;
            request.UsePassive = true;
            request.KeepAlive = true;
            request.Credentials = new NetworkCredential(ConfigurationSettings.AppSettings["UserFTP"], ConfigurationSettings.AppSettings["PassFTP"]);
            request.ConnectionGroupName = "group";

            using (FileStream fs = File.OpenRead(fileName))
            {
                byte[] buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                fs.Close();
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(buffer, 0, buffer.Length);
                requestStream.Flush();
                requestStream.Close();
            }         
        }

        public void DownloadTextFile()
        {
            Response.Write("<script language='javascript'>");
            Response.Write("var child=window.open('TempExportFTP.aspx', '', 'width=1,height=1');");
            Response.Write("child.moveTo(10000,10000);");
            Response.Write("</script>");
        }
        #endregion
    }
}
 