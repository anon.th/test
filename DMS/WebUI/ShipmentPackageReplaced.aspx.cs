using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for ShipmentTrackingQuery.
	/// </summary>
	public class ShipmentPackageReplaced : BasePage
	{
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnCancelQry;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.RadioButton rbBookingDate;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.Label lblConsignmentNo;
		protected System.Web.UI.WebControls.TextBox txtConsignmentNo;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentTracking;
		protected System.Web.UI.WebControls.Label lblPayerCode;
		protected com.common.util.msTextBox txtPayerCode;
		//Utility utility = null;
		String m_strAppID=null;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		String m_strEnterpriseID=null;
		String m_strCulture=null;
		protected System.Web.UI.WebControls.TextBox txtPeriod2;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.HtmlControls.HtmlTable tblPayerType;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.Label lblPayerType;
		protected com.common.util.msTextBox txtBookingNo;
		protected System.Web.UI.WebControls.Label lblBookingNo;
		protected System.Web.UI.WebControls.RadioButton rbEstDate;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.DropDownList ddlPayerType;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCode;
		protected Cambro.Web.DbCombo.DbCombo DbComboOriginDC;
		protected System.Web.UI.WebControls.Label Label3;
		protected Cambro.Web.DbCombo.DbCombo DbComboDestinationDC;
		protected System.Web.UI.WebControls.TextBox txtBookingType;
		protected System.Web.UI.WebControls.Label lblRefNo;
		protected System.Web.UI.WebControls.TextBox txtRefNo;
		protected System.Web.UI.WebControls.Label lblOther;
		protected System.Web.UI.HtmlControls.HtmlTable tblOther;
		protected System.Web.UI.WebControls.RadioButton rbActualPickUpDate;
		protected System.Web.UI.WebControls.RadioButton rbActualPODDate;
		protected System.Web.UI.WebControls.CheckBox chkMorethanWeight;
		protected System.Web.UI.WebControls.Label lblMoreThanWeight;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox txtOriginDC;
		protected System.Web.UI.WebControls.Label Label5;
		private DataView m_dvMonths;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			if(!Page.IsPostBack)
			{
				DefaultScreen();
				Session["toRefresh"]=false;
				//LoadBookingTypeList();
				decimal numMoreThan = Convert.ToDecimal( System.Configuration.ConfigurationSettings.AppSettings["PackageReplaceShowMoreThan"]);
				lblMoreThanWeight.Text = numMoreThan.ToString();
				if(numMoreThan >= 3)
					chkMorethanWeight.Checked = true;
			}
			//btnExecuteQuery.Enabled = false;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.ID = "ShipmentTrackingQueryRpt";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{

			//btnExecuteQuery.Enabled = false;
			lblErrorMessage.Text = "";
			int intChk = 0;
			intChk = ValidateData();
			if(intChk < 1)
			{
				return;
			}
			String strModuleID = Request.Params["MODID"];
			DataSet dsShipment = GetShipmentQueryData();

			String strUrl = null;
			
			strUrl = "ReportViewer.aspx";
			
			Session["FORMID"] = "ShipmentPackageReplace";

			Session["SESSION_DS1"] = dsShipment;
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void OpenWindowpage(String strUrl, String strFeatures)
		{
			
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			//btnExecuteQuery.Enabled = true;
			DefaultScreen();
		}

		private DataTable CreateEmptyDataTable()
		{
			DataTable dtShipment = new DataTable();
			dtShipment.Columns.Add(new DataColumn("tran_date", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
			dtShipment.Columns.Add(new DataColumn("end_date", typeof(DateTime)));
			dtShipment.Columns.Add(new DataColumn("payer_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("consignment_no", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("booking_no", typeof(string)));
			//#Start:PSA 2009_029 By Gwang on 13Mar09#
			dtShipment.Columns.Add(new DataColumn("ref_no",typeof(string)));
			dtShipment.Columns.Add(new DataColumn("origin_dc",typeof(string)));
			dtShipment.Columns.Add(new DataColumn("morethan_show",typeof(int)));
			//#End:PSA 2009_029

			return dtShipment;
		}
		
		private DataSet GetShipmentQueryData()
		{
			DataSet dsShipment = new DataSet();
			DataTable dtShipment = CreateEmptyDataTable();
			DataRow dr = dtShipment.NewRow(); //8 columns
			string strMonth =null;
			if (rbBookingDate.Checked) 
			{
				dr[0] = "B";
			}
			if (rbActualPickUpDate.Checked) 
			{
				dr[0] = "PI";
			}
			if (rbActualPODDate.Checked) 
			{
				dr[0] = "PO";
			}
			if (rbMonth.Checked) 
			{
				strMonth = ddMonth.SelectedItem.Value;
				if (strMonth != "" && txtYear.Text != "")
				{
					dr[1] = DateTime.ParseExact ("01/"+strMonth.ToString()+"/"+txtYear.Text, "dd/MM/yyyy", null);
					int intLastDay = 0;
					intLastDay = LastDayOfMonth(strMonth, txtYear.Text);
					dr[2] = DateTime.ParseExact (intLastDay.ToString()+"/"+strMonth.ToString()+"/"+txtYear.Text+" 23:59", "dd/MM/yyyy HH:mm", null);
				}
			}
			if (rbPeriod.Checked) 
			{
				if (txtPeriod.Text != ""  && txtTo.Text != "")
				{
					dr[1] = DateTime.ParseExact(txtPeriod.Text ,"dd/MM/yyyy",null);
					dr[2] = DateTime.ParseExact(txtTo.Text+" 23:59" ,"dd/MM/yyyy HH:mm",null);
				}
			}
			if (rbDate.Checked) 
			{
				if (txtDate.Text != "")
				{
					dr[1] = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy",null);
					dr[2] = DateTime.ParseExact(txtDate.Text+" 23:59", "dd/MM/yyyy HH:mm",null);
				}
			}
			dr[3] = txtPayerCode.Text;
			dr[4] = txtConsignmentNo.Text;
			dr[5] = txtBookingNo.Text;


			//#Start:PSA 2009_029 By Gwang on 13Mar09#			
			if (this.txtRefNo.Text.Trim() != "")
			{
				dr["ref_no"] = txtRefNo.Text;
			}
			//#End:PSA 2009_029

			if (this.txtOriginDC.Text.Trim() != "")
			{
				dr["origin_dc"] = txtOriginDC.Text;
			}

			if(chkMorethanWeight.Checked)
			if (System.Configuration.ConfigurationSettings.AppSettings["PackageReplaceShowMoreThan"].ToString() != "")
			{
				dr["morethan_show"] = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["PackageReplaceShowMoreThan"].ToString());
			}
			else
				dr["morethan_show"] = 0;

			dtShipment.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipment);
			return dsShipment;
		}

		private int ValidateData()
		{
			if(rbMonth.Checked == true)
				if (txtYear.Text != "" )
				{
					if (Convert.ToInt32(txtYear.Text) < 2000 || Convert.ToInt32(txtYear.Text) > 2099)
					{
						//lblErrorMessage.Text = "Enter year between 2000 to 2099";
						lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
						return -1;
					}
				}
				else 
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
					return -1;
				}
				
			if(rbPeriod.Checked == true)
				if(txtPeriod.Text != "" && txtTo.Text != "")
				{
					return 1;
				}
				else
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ENTER_PERIOD",utility.GetUserCulture());
					return -1;
				}

			if(rbDate.Checked == true )
			{
				if(txtDate.Text=="")
				{					
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT",utility.GetUserCulture());	
					return -1;				
				}
			}

			return 1;
		}

		private DataView CreateMonths(bool showNilOption)
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			if(showNilOption)
			{
				DataRow drNilRow = dtMonths.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtMonths.Rows.Add(drNilRow);
			}
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);
			return dvMonths;
		}

		private void BindMonths(DataView dvMonths)
		{
			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}

		private int LastDayOfMonth(String strMonth, String strYear)
		{
			int intLastDay = 0;
			switch (strMonth)
			{
				case "01":
				case "03":
				case "05":
				case "07":
				case "08":
				case "10":
				case "12":
					intLastDay = 31;
					break;
				case "04":
				case "06":
				case "09":
				case "11":
					intLastDay = 30;
					break;
				case "02":
					intLastDay = 28;
					break;
			}
			return intLastDay;
		}

		private void DefaultScreen()
		{
			rbBookingDate.Checked = true;
			rbActualPickUpDate.Checked = false;
			rbActualPODDate.Checked = false;
			rbMonth.Checked = true;
			rbPeriod.Checked = false;
			rbDate.Checked = false;
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
			txtPayerCode.Text = null;
			txtConsignmentNo.Text = null;
			txtBookingNo.Text = null;
			txtRefNo.Text = null;
			txtOriginDC.Text = null;
			m_dvMonths = CreateMonths(true);
			BindMonths(m_dvMonths);
			lblErrorMessage.Text = "";
		}

		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}

		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}

		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = true;
		}

		private void rbCustomer_CheckedChanged(object sender, System.EventArgs e)
		{
			txtPayerCode.Text = null;
		}

		private void rbAgent_CheckedChanged(object sender, System.EventArgs e)
		{
			txtPayerCode.Text = null;
		}

		private void rbInvoiceYes_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void rbInvoiceNo_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void btnRouteCode_Click(object sender, System.EventArgs e)
		{
		}

	}
}
