<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="AgentZipCodePopup.aspx.cs" AutoEventWireup="false" Inherits="com.ties.AgentZipCodePopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AgentZipCodePopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="AgentZipCodePopup" name="AgentZipCodePopup" method="post" runat="server">
			<asp:button id="Button1" style="Z-INDEX: 108; LEFT: 385px; POSITION: absolute; TOP: 122px" runat="server" CssClass="queryButton" Text="Cancel"></asp:button>
			<asp:label id="lblTitle" runat="server" Height="26px" Width="196px" CssClass="mainTitleSize"> Postal Codes
		</asp:label>
			<TABLE style="Z-INDEX: 100; LEFT: 32px; WIDTH: 288px; POSITION: absolute; TOP: 61px; HEIGHT: 79px">
				<TR>
					<TD><asp:label id="lblCountry" runat="server" CssClass="tablelabel"> Country</asp:label></TD>
					<TD>
						<cc1:mstextbox id="txtCountry" CssClass="gridTextBox" Width="125px" Runat="server" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore" Enabled="True"></cc1:mstextbox></TD>
					<TD><asp:button id="BtnCountry" runat="server" CssClass="searchButton" CausesValidation="False" Text="...." Enabled="False" Visible="False"></asp:button></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 27px"><asp:label id="lblStateCode" runat="server" CssClass="tablelabel">State Name</asp:label></TD>
					<TD style="HEIGHT: 27px"><cc1:mstextbox id="txtStateName" Width="125px" CssClass="gridTextBox" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" Runat="server"></cc1:mstextbox></TD>
					<TD style="HEIGHT: 27px"><asp:button id="btnStateCode" runat="server" CssClass="searchButton" CausesValidation="False" Text="...." Enabled="False" Visible="False"></asp:button></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblZipCode" runat="server" CssClass="tablelabel">Postal Code</asp:label></TD>
					<TD><cc1:mstextbox id="txtZipCode" Width="125px" CssClass="gridTextBox" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" Runat="server"></cc1:mstextbox></TD>
					<TD><asp:button id="btnZipCode" runat="server" CssClass="searchButton" CausesValidation="False" Text="...." Enabled="False" Visible="False"></asp:button></TD>
				</TR>
			</TABLE>
			<asp:datagrid id="dgZipcode" style="Z-INDEX: 101; LEFT: 25px; POSITION: absolute; TOP: 177px" runat="server" Width="522px" CssClass="gridHeading" AllowCustomPaging="false" BorderStyle="None" BorderWidth="1px" CellPadding="3" BackColor="White" BorderColor="#CCCCCC" AutoGenerateColumns="False" AllowPaging="true">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
				<ItemStyle CssClass="drilldownField"></ItemStyle>
				<HeaderStyle Width="10%" CssClass="drilldownHeading"></HeaderStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:TemplateColumn>
						<HeaderTemplate>
							<asp:CheckBox id="chkFormHeader" AutoPostBack="True" OnCheckedChanged="chkAll" runat="server" EnableViewState="True"></asp:CheckBox>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:CheckBox id="chkFormId" runat="server" OnCheckedChanged="chkRow" AutoPostBack="True"></asp:CheckBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="zipcode" HeaderText="Postal Code"></asp:BoundColumn>
					<asp:BoundColumn DataField="state_code" HeaderText="State Code"></asp:BoundColumn>
					<asp:BoundColumn DataField="state_name" HeaderText="State Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="country" HeaderText="Country"></asp:BoundColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" ForeColor="#000066" BackColor="White"></PagerStyle>
			</asp:datagrid><cc1:mstextbox id="txtEAS" style="Z-INDEX: 102; LEFT: 355px; POSITION: absolute; TOP: 155px" Width="92px" CssClass="gridTextBox" Enabled="True" TextMaskType="msPrice" MaxLength="12" Runat="server"></cc1:mstextbox><asp:label id="lblESA" style="Z-INDEX: 103; LEFT: 218px; POSITION: absolute; TOP: 161px" runat="server" Width="131px" CssClass="tablelabel">Default ESA Surcharge</asp:label><asp:button id="btnSearch" style="Z-INDEX: 105; LEFT: 326px; POSITION: absolute; TOP: 81px" runat="server" CssClass="queryButton" Text="Search"></asp:button><asp:button id="btnOK" style="Z-INDEX: 106; LEFT: 326px; POSITION: absolute; TOP: 122px" runat="server" CssClass="queryButton" Text="OK" Width="58px"></asp:button><asp:label id="lblErrorMsg" style="Z-INDEX: 107; LEFT: 32px; POSITION: absolute; TOP: 161px" runat="server" Width="236px" ForeColor="Red"></asp:label></form>
	</body>
</HTML>
