<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Page language="c#" Codebehind="DailyKF_PODEXRptQuery.aspx.cs" AutoEventWireup="false" Inherits="com.ties.DailyKF_PODEXRptQuery" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Daily KF Action List Report</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="DailyKF_PODEXRptQuery" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 20px; POSITION: absolute; TOP: 40px" runat="server"
				Width="64px" CssClass="queryButton" Text="Query"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 104; LEFT: 24px; POSITION: absolute; TOP: 69px"
				runat="server" Width="700px" CssClass="errorMsgColor" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 90px; POSITION: absolute; TOP: 40px"
				runat="server" Width="123px" CssClass="queryButton" Text="Execute Query"></asp:button><asp:label id="Label1" style="Z-INDEX: 103; LEFT: 20px; POSITION: absolute; TOP: 4px" runat="server"
				Width="365px" CssClass="maintitleSize" Height="27px">Daily KF Action List Report</asp:label></TR></TABLE></TR></TABLE><input 
style="Z-INDEX: 105; LEFT: 222px; POSITION: absolute; TOP: 35px" type=hidden 
value="<%=strScrollPosition%>" name=ScrollPosition>
			<asp:Calendar id="Calendar1" style="Z-INDEX: 106; LEFT: 24px; POSITION: absolute; TOP: 88px" runat="server"
				Font-Size="8pt" Font-Names="Tahoma">
				<TodayDayStyle BackColor="#FF8080"></TodayDayStyle>
				<SelectedDayStyle ForeColor="Blue" BackColor="DarkGray"></SelectedDayStyle>
				<WeekendDayStyle ForeColor="Red"></WeekendDayStyle>
				<OtherMonthDayStyle ForeColor="DarkGray"></OtherMonthDayStyle>
			</asp:Calendar></form>
	</body>
</HTML>
