<%@ Page Language="c#" CodeBehind="MainFrame.aspx.cs" AutoEventWireup="false"
    Inherits="com.ties.MainFrame" SmartNavigation="True" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title><%=System.Configuration.ConfigurationSettings.AppSettings["DMSVERSION"] %> </title>
    <script language="javascript">
    </script>
    <link href="<%=strStyleSheet%>" type="text/css" rel="stylesheet" name="stylesheet">
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <base target="_base">
    <script language="javascript" src="Scripts/ClosignMainWindow.js"></script>
</head>
<frameset border="0" framespacing="0" rows="103,100%" frameborder="0">
    <frame name="displayBanner" marginwidth="0" marginheight="0" src="TopBanner.aspx" frameborder="no"
        scrolling="no" width="500">
    <frameset id="fr_nav_content" border="0" cols="250,100%">
        <frame name="taskTree" marginwidth="0"  scrolling="no" marginheight="2" src="TaskTree.aspx" frameborder="no">
        <frame
            class="Content" name="displayContent" marginwidth="2" marginheight="2"
            src="<%=strWelcomePagePath%>" frameborder="yes">
    </frameset>
</frameset>
</html>
