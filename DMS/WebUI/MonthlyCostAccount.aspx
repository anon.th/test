<%@ Page language="c#" Codebehind="MonthlyCostAccount.aspx.cs" AutoEventWireup="false" Inherits="com.ties.MonthlyCostAccount" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>MonthlyCostAccount</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="MonthlyCostAccount" method="post" runat="server">
			<asp:label id="lblMainTitle" style="Z-INDEX: 111; LEFT: 29px; POSITION: absolute; TOP: 8px" runat="server" CssClass="mainTitleSize" Height="26px" Width="477px">Monthly Cost Account</asp:label><asp:label id="lblMessage" style="Z-INDEX: 112; LEFT: 29px; POSITION: absolute; TOP: 41px" runat="server" CssClass="errorMsgColor" Height="19px" Width="650px"></asp:label>
			<TABLE id="tblMonthlyCostAccount" style="Z-INDEX: 110; LEFT: 10px; WIDTH: 730px; POSITION: absolute; TOP: 68px" width="730" border="0" runat="server">
				<TR height="25">
					<TD width="2%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="5%"></TD>
					<TD width="8%"></TD>
				</TR>
				<TR height="25">
					<TD align="right" colSpan="2"><asp:requiredfieldvalidator id="validateAccNo" Width="100%" ErrorMessage="Account Year is required field." Display="None" ControlToValidate="txtAccYear" Runat="server" EnableClientScript="False"></asp:requiredfieldvalidator><asp:label id="lblAstrik" runat="server" CssClass="errorMsgColor" Height="22px" Width="100%">&nbsp;*&nbsp;</asp:label></TD>
					<TD colSpan="5"><asp:label id="lblAccYear" runat="server" CssClass="tablelabel">Year</asp:label></TD>
					<TD colSpan="1"><cc1:mstextbox id="txtAccYear" tabIndex="11" runat="server" CssClass="textField" Width="100%" AutoPostBack="True" NumberScale="0" NumberPrecision="4" NumberMaxValue="2900" MaxLength="4" TextMaskType="msNumeric"></cc1:mstextbox></TD>
					<TD colSpan="12"></TD>
				</TR>
				<TR height="25">
					<TD align="right" colSpan="2"><asp:label id="lblAstrik2" runat="server" CssClass="errorMsgColor" Height="22px" Width="100%">&nbsp;*&nbsp;</asp:label></TD>
					<TD colSpan="5"><asp:label id="lblAccMonth" runat="server" CssClass="tablelabel">Month</asp:label></TD>
					<TD colSpan="3"><asp:dropdownlist id="ddlAccMonth" tabIndex="29" runat="server" CssClass="textField" Width="100%" AutoPostBack="True"></asp:dropdownlist></TD>
					<TD colSpan="10"></TD>
				</TR>
				<TR height="25">
					<TD colSpan="15"></TD>
					<TD align="right" colSpan="4"><asp:button id="btnAddAllCostCode" runat="server" CssClass="queryButton" Width="100%" CausesValidation="True" Text="Add All Cost Code"></asp:button></TD>
					<TD colSpan="1"></TD>
				</TR>
				<TR>
					<TD colSpan="1">&nbsp;</TD>
					<TD colSpan="18"><asp:datagrid id="dgCostCodes" style="Z-INDEX: 100" runat="server" Width="100%" SelectedItemStyle-CssClass="gridFieldSelected" OnDeleteCommand="dgCostCodes_Delete" OnUpdateCommand="dgCostCodes_Update" OnCancelCommand="dgCostCodes_Cancel" OnEditCommand="dgCostCodes_Edit" OnItemDataBound="dgCostCodes_Bound" AutoGenerateColumns="False" ItemStyle-Height="20">
							<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
							<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
							<Columns>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
									<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:EditCommandColumn>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
									<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Cost Code">
									<HeaderStyle Font-Bold="True" Width="38%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblCostCode" Text='<%#DataBinder.Eval(Container.DataItem,"cost_code")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<dbCombo:DbCombo id="DbComboCostCode" ShowDbComboLink="False" runat="server" ServerMethod="DbComboCostCodeSelect" AutoPostBack="True" Text='<%#DataBinder.Eval(Container.DataItem,"cost_code")%>' RegistrationKey='<%#ConfigurationSettings.AppSettings["DbComboRegKey"]%>'>
										</dbCombo:DbCombo>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Description">
									<HeaderStyle Width="39%" CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblCostDescription" Text='<%#DataBinder.Eval(Container.DataItem,"cost_code_description")%>' Runat="server" Enabled="True">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Amount">
									<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabelNumber" ID="lblAmount" Text='<%#DataBinder.Eval(Container.DataItem,"cost_amt","{0:n}")%>' Runat="server" Enabled="True" >
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:msTextBox CssClass="gridTextBoxNumber" ID="txtAmount" Text='<%#DataBinder.Eval(Container.DataItem,"cost_amt", "{0:n}")%>' Runat="server" Enabled="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2">
										</cc1:msTextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></TD>
					<TD vAlign="bottom" align="left" colSpan="1"><asp:button id="btnCCInsert" tabIndex="50" runat="server" CssClass="queryButton" Height="22px" CausesValidation="True" Text=" + "></asp:button></TD>
				</TR>
				<TR height="10">
					<TD colSpan="20"></TD>
				</TR>
			</TABLE>
			<asp:validationsummary id="mcaValidation" style="Z-INDEX: 113; LEFT: 207px; POSITION: absolute; TOP: 453px" runat="server" DisplayMode="SingleParagraph" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary></form>
	</body>
</HTML>
