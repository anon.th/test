using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CrystalDecisions.Web;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using com.common.util;
using com.common.DAL;
using com.ties.DAL;
using com.common.classes;
using com.common.applicationpages;
using System.Collections.Generic;
using System.IO;
using ClosedXML.Excel;

namespace com.ties
{
    /// <summary>
    /// Summary description for ReportViewerDataSet.
    /// </summary>
    public class ReportViewerDataSet : BasePopupPage
    {
        string strFORMID = null;
        string strUserId = null;
        bool bShowGroupTree = false;
        ReportDocument rptSource = null;
        string m_strAppid = null;
        string m_strEnterpriseid = null;
        DataSet m_dsQuery = null;
        DataSet m_dsResult = null;
        DataSet dsFreight = null;
        protected System.Web.UI.WebControls.Button btnSearch;
        protected System.Web.UI.WebControls.TextBox txtTextToSearch;
        protected System.Web.UI.WebControls.DropDownList ddbZoom;
        protected System.Web.UI.WebControls.Label lblZoom;
        protected System.Web.UI.WebControls.Button btnExport;
        protected System.Web.UI.WebControls.Button btnExportExcel;
        protected System.Web.UI.WebControls.DropDownList ddbExport;
        protected System.Web.UI.WebControls.Button btnMoveLast;
        protected System.Web.UI.WebControls.Button btnMoveNext;
        protected System.Web.UI.WebControls.Button btnMovePrevious;
        protected System.Web.UI.WebControls.Button btnMoveFirst;
        protected System.Web.UI.WebControls.Button btnShowGrpTree;
        protected CrystalDecisions.Web.CrystalReportViewer rptViewer;
        protected com.common.util.msTextBox txtGoTo;
        protected System.Web.UI.WebControls.Label lblErrorMesssage;
        //Utility utility = null;
        private void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            utility = new Utility(System.Configuration.ConfigurationManager.AppSettings, Page.Session);
            m_strAppid = utility.GetAppID();
            m_strEnterpriseid = utility.GetEnterpriseID();
            strUserId = utility.GetUserID();
            strFORMID = (String)Session["FORMID"];
            if (!IsPostBack)
            {
                LoadExportFormatList();
                ViewState["ShowGroupTree"] = bShowGroupTree;
            }
            else
            {
                bShowGroupTree = (bool)ViewState["ShowGroupTree"];
            }

            if (strFORMID.Equals("FreightSummary"))
            {
                btnExportExcel.Enabled = true;
            }

            SetReportInstance();
            BindReport();

            if (strFORMID.Equals("TopNReport"))
            {
                rptViewer.DisplayGroupTree = bShowGroupTree;
            }
        }

        private void Page_Unload(object sender, System.EventArgs e)
        {
            CloseReports(rptSource);
            rptViewer.Dispose();
            rptViewer = null;
        }

        private void CloseReports(ReportDocument reportDocument)
        {
            Sections sections = reportDocument.ReportDefinition.Sections;
            foreach (Section section in sections)
            {
                ReportObjects reportObjects = section.ReportObjects;
                foreach (ReportObject reportObject in reportObjects)
                {
                    if (reportObject.Kind == ReportObjectKind.SubreportObject)
                    {
                        SubreportObject subreportObject = (SubreportObject)reportObject;
                        ReportDocument subReportDocument = subreportObject.OpenSubreport(subreportObject.SubreportName);
                        subReportDocument.Close();
                    }
                }
            }
            reportDocument.Close();
        }
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            base.OnInit(e);
            InitializeComponent();
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnShowGrpTree.Click += new System.EventHandler(this.btnShowGrpTree_Click);
            this.btnMoveFirst.Click += new System.EventHandler(this.btnMoveFirst_Click);
            this.btnMovePrevious.Click += new System.EventHandler(this.btnMovePrevious_Click);
            this.btnMoveNext.Click += new System.EventHandler(this.btnMoveNext_Click);
            this.btnMoveLast.Click += new System.EventHandler(this.btnMoveLast_Click);
            this.ddbExport.SelectedIndexChanged += new System.EventHandler(this.ddbExport_SelectedIndexChanged);
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            this.ddbZoom.SelectedIndexChanged += new System.EventHandler(this.ddbZoom_SelectedIndexChanged);
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            this.Unload += new System.EventHandler(this.Page_Unload);
        }
        #endregion

        private void SetReportInstance()
        {
            if (strFORMID != null)
            {
                //create your own report objects as a class
                if (strFORMID.Equals("TopNReport"))
                {
                    // declare invoice printing report object to rptSource class
                    //rptSource = new TopNRpt();

                    //get parameter dataset from session_ds1
                    m_dsQuery = (DataSet)Session["SESSION_DS1"];
                    m_dsResult = ReportDAL.GetTopNPayer(m_strAppid, m_strEnterpriseid, m_dsQuery);
                    rptSource = new TIES.WebUI.TopNReportRpt();
                    SetTopNReportDataSet(m_dsResult);
                    SetTopNReport();
                    //pass the parameters to report
                    //SetInvoiceParams();
                }
                else if (strFORMID == "SHIPMENT PENDING REPORT OPTIMIZED")
                {
                    m_dsQuery = (DataSet)Session["SESSION_DS1"];

                    if ((m_dsQuery != null) &&
                        (!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
                    {
                        if ((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) &&
                            (!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
                            (m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
                        {
                            if (m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() == "C")
                            {
                                rptSource = new TIES.WebUI.ShipmentPendingPODRpt3PA_Optimized();
                            }
                            else
                            {
                                rptSource = new TIES.WebUI.ShipmentPendingPODRpt_Optimized();
                            }
                        }
                    }
                    //Extract parameters from SetShipPendingPOD();

                    SetShipPendingPOD();

                    rptViewer.Width = 1200;
                }
                else if (strFORMID.Equals("ServiceQualityIndicatorQuery"))
                {
                    m_dsQuery = (DataSet)Session["SESSION_DS1"];
                    m_dsResult = ReportDAL.GetSQIData(m_strAppid, m_strEnterpriseid, m_dsQuery);

                    if ((m_dsQuery != null) &&
                        (!m_dsQuery.GetType().Equals(System.Type.GetType("System.DBNull"))))
                    {
                        if ((m_dsQuery.Tables[0].Rows[0]["RptType"] != null) &&
                            (!m_dsQuery.Tables[0].Rows[0]["RptType"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
                            (m_dsQuery.Tables[0].Rows[0]["RptType"].ToString() != ""))
                        {
                            rptSource = new TIES.WebUI.SQIReport();
                        }
                    }

                    SetSQIDataSet(m_dsResult);
                    SetSQIReportParams();

                }
                else if (strFORMID.Equals("VersusReportQuery"))
                {
                    m_dsQuery = (DataSet)Session["SESSION_DS1"];
                    m_dsResult = ReportDAL.GetSQIDataVersus(m_strAppid, m_strEnterpriseid, m_dsQuery);

                    if (m_dsQuery != null)
                    {
                        rptSource = new TIES.WebUI.VersusReportScreen();
                    }

                    SetSQIVersusDataSet(m_dsResult);
                    SetSQIVersusReportParams();

                }
                else if (strFORMID.Equals("sopscanreport"))
                {
                    m_dsQuery = (DataSet)Session["SESSION_DS1"];
                    m_dsResult = ReportDAL.GetSopScaning(m_strAppid, m_strEnterpriseid, m_dsQuery);

                    if (m_dsQuery != null)
                    {
                        rptSource = new TIES.WebUI.SopScanRpt();
                        rptSource.PrintOptions.PaperSize = PaperSize.PaperA4;
                        rptSource.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
                    }

                    SetSopScanReportDataSet(m_dsResult);
                }
                else if (strFORMID.Equals("FreightSummary"))
                {
                    m_dsQuery = (DataSet)Session["SESSION_DS1"];
                    if (m_dsQuery.Tables[0].Rows[0]["report_name"].ToString() == "FreightSummary") {
                        if (m_dsQuery.Tables[0].Rows[0]["payer_type"].ToString() == "'P'")
                        {
                            rptSource = new TIES.WebUI.FreighSummaryInboundPrepaid();
                            m_dsResult = ReportDAL.GetFreightSummaryFedEx_TNT_Invoices(m_strAppid, m_strEnterpriseid, m_dsQuery);
                        }
                        else if (m_dsQuery.Tables[0].Rows[0]["shipment_type"].ToString() == "I" || m_dsQuery.Tables[0].Rows[0]["shipment_type"].ToString() == "B")
                        {
                            rptSource = new TIES.WebUI.FreighSummaryInbound();
                            m_dsResult = ReportDAL.GetFreightSummary(m_strAppid, m_strEnterpriseid, m_dsQuery);
                        }
                        else
                        {
                            rptSource = new TIES.WebUI.FreighSummary();
                            m_dsResult = ReportDAL.GetFreightSummary(m_strAppid, m_strEnterpriseid, m_dsQuery);
                        }                     
                        rptSource.PrintOptions.PaperSize = PaperSize.PaperA4;
                        rptSource.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
                        SetFreightSummary(m_dsResult, m_dsQuery.Tables[0].Rows[0]["date_mode"].ToString());
                    }
                    /*
                    else if (m_dsQuery.Tables[0].Rows[0]["report_name"].ToString() == "FedEx_TNT")
                    {
                        m_dsResult = ReportDAL.GetFreightSummaryFedEx_TNT_Invoices(m_strAppid, m_strEnterpriseid, m_dsQuery);
                        rptSource = new TIES.WebUI.FreighSummaryInbound();
                        rptSource.PrintOptions.PaperSize = PaperSize.PaperA4;
                        rptSource.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
                        SetFreightSummary(m_dsResult, m_dsQuery.Tables[0].Rows[0]["date_mode"].ToString());
                    }
                    */
                    
                }
                else if (strFORMID.Equals("AccPacExport"))
                {
                    string param = (string)Session["SESSION_Param"];
                    m_dsQuery = (DataSet)Session["SESSION_DS1"];
                    m_dsResult = ReportDAL.GetAccPacExport(m_strAppid, m_strEnterpriseid, param);

                    rptSource = new TIES.WebUI.AccPacExport();
                    SetAccPacExport(m_dsResult);
                    rptSource.PrintOptions.PaperSize = PaperSize.PaperA4;
                    rptSource.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
                }
                else if (strFORMID.Equals("UpliftStatistics"))
                {
                    m_dsQuery = (DataSet)Session["SESSION_DS1"];
                    m_dsResult = ReportDAL.GetUpliftStatistics(m_strAppid, m_strEnterpriseid, m_dsQuery);

                    //if(m_dsQuery != null)
                    //{
                    if (m_dsQuery.Tables[0].Rows[0]["reportType"].ToString().Equals("1"))
                        rptSource = new TIES.WebUI.UpliftStatisticsByBranch();
                    if (m_dsQuery.Tables[0].Rows[0]["reportType"].ToString().Equals("2"))
                        rptSource = new TIES.WebUI.UpliftStatisticsExHub();

                    rptSource.PrintOptions.PaperSize = PaperSize.PaperA4;
                    rptSource.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
                    //}

                    string dateFrom = "";
                    string dateTo = "";
                    string payerType = "";
                    string data_type = "";

                    DataRow dr = m_dsQuery.Tables[0].Rows[0];
                    if (Utility.IsNotDBNull(dr["dateFrom"]))
                    {
                        dateFrom = dr["dateFrom"].ToString();
                        string[] tempDateFrom = dateFrom.Split('/');
                        dateFrom = (tempDateFrom[1].Length == 1 ? "0" + tempDateFrom[1] : tempDateFrom[1]) + "/" + (tempDateFrom[0].Length == 1 ? "0" + tempDateFrom[0] : tempDateFrom[0]) + "/" + tempDateFrom[2].Substring(0, 4);
                    }
                    if (Utility.IsNotDBNull(dr["dateTo"]))
                    {
                        dateTo = dr["dateTo"].ToString();
                        string[] tempDateTo = dateTo.Split('/');
                        dateTo = (tempDateTo[1].Length == 1 ? "0" + tempDateTo[1] : tempDateTo[1]) + "/" + (tempDateTo[0].Length == 1 ? "0" + tempDateTo[0] : tempDateTo[0]) + "/" + tempDateTo[2].Substring(0, 4);
                    }
                    if (Utility.IsNotDBNull(dr["payer_type"]))
                    {
                        payerType = dr["payer_type"].ToString().Replace(@"""", "").Replace("'", "");
                        if (payerType.Equals("C"))
                            payerType = "Customer";
                        else if (payerType.Equals("N"))
                            payerType = "Non-revenue";
                        else if (payerType.Equals("F"))
                            payerType = "On-Forwarding";
                    }
                    if (Utility.IsNotDBNull(dr["dateType"]))
                    {
                        data_type = dr["dateType"].ToString();
                        if (data_type.Equals("1"))
                            data_type = "I";
                        else if (data_type.Equals("2"))
                            data_type = "M";
                    }

                    if (dateTo == "")
                        dateTo = dateFrom;

                    if (m_dsQuery.Tables[0].Rows[0]["reportType"].ToString().Equals("1"))
                        SetUpliftStatistics(m_dsResult, dateFrom, dateTo, payerType, data_type);
                    if (m_dsQuery.Tables[0].Rows[0]["reportType"].ToString().Equals("2"))
                        SetUpliftStatisticsExHub(m_dsResult, dateFrom, dateTo, payerType, data_type);

                }
                else if (strFORMID.Equals("AutomatedReport"))
                {
                    m_dsQuery = (DataSet)Session["SESSION_DS1"];
                    m_dsResult = ReportDAL.GetAutomatedReport(m_strAppid, m_strEnterpriseid, m_dsQuery);
                    rptSource = new TIES.WebUI.AutomatedReportGraph();
                    rptSource.PrintOptions.PaperSize = PaperSize.PaperA4;
                    rptSource.PrintOptions.PaperOrientation = PaperOrientation.Portrait;

                    string dateFrom = "";
                    string dateTo = "";
                    string payerType = "";
                    string data_type = "";

                    DataRow dr = m_dsQuery.Tables[0].Rows[0];
                    if (Utility.IsNotDBNull(dr["dateFrom"]))
                    {
                        dateFrom = dr["dateFrom"].ToString();
                        string[] tempDateFrom = dateFrom.Split('/');
                        dateFrom = (tempDateFrom[1].Length == 1 ? "0" + tempDateFrom[1] : tempDateFrom[1]) + "/" + (tempDateFrom[0].Length == 1 ? "0" + tempDateFrom[0] : tempDateFrom[0]) + "/" + tempDateFrom[2].Substring(0, 4);
                    }
                    if (Utility.IsNotDBNull(dr["dateTo"]))
                    {
                        dateTo = dr["dateTo"].ToString();
                        string[] tempDateTo = dateTo.Split('/');
                        dateTo = (tempDateTo[1].Length == 1 ? "0" + tempDateTo[1] : tempDateTo[1]) + "/" + (tempDateTo[0].Length == 1 ? "0" + tempDateTo[0] : tempDateTo[0]) + "/" + tempDateTo[2].Substring(0, 4);
                    }
                    if (Utility.IsNotDBNull(dr["payer_type"]))
                    {
                        payerType = dr["payer_type"].ToString().Replace(@"""", "").Replace("'", "");
                        if (payerType.Equals("C"))
                            payerType = "Customer";
                        else if (payerType.Equals("N"))
                            payerType = "Non-revenue";
                        else if (payerType.Equals("F"))
                            payerType = "On-Forwarding";
                    }
                    if (Utility.IsNotDBNull(dr["dateType"]))
                    {
                        data_type = dr["dateType"].ToString();
                        if (data_type.Equals("1"))
                            data_type = "I";
                        else if (data_type.Equals("2"))
                            data_type = "M";
                    }

                    if (dateTo == "")
                        dateTo = dateFrom;

                    SetAutomatedReport(m_dsResult, dateFrom, dateTo, payerType, data_type);
                }
                else if (strFORMID.Equals("AutomatedReportBySpend"))
                {
                    m_dsQuery = (DataSet)Session["SESSION_DS1"];
                    m_dsResult = ReportDAL.GetAutomatedReportBySpend(m_strAppid, m_strEnterpriseid, m_dsQuery);
                    rptSource = new TIES.WebUI.AutomatedReportGraphBySpend();
                    rptSource.PrintOptions.PaperSize = PaperSize.PaperA4;
                    rptSource.PrintOptions.PaperOrientation = PaperOrientation.Portrait;

                    string dateFrom = "";
                    string dateTo = "";
                    string payerType = "";
                    string data_type = "";

                    DataRow dr = m_dsQuery.Tables[0].Rows[0];
                    if (Utility.IsNotDBNull(dr["dateFrom"]))
                    {
                        dateFrom = dr["dateFrom"].ToString();
                        string[] tempDateFrom = dateFrom.Split('/');
                        dateFrom = (tempDateFrom[1].Length == 1 ? "0" + tempDateFrom[1] : tempDateFrom[1]) + "/" + (tempDateFrom[0].Length == 1 ? "0" + tempDateFrom[0] : tempDateFrom[0]) + "/" + tempDateFrom[2].Substring(0, 4);
                    }
                    if (Utility.IsNotDBNull(dr["dateTo"]))
                    {
                        dateTo = dr["dateTo"].ToString();
                        string[] tempDateTo = dateTo.Split('/');
                        dateTo = (tempDateTo[1].Length == 1 ? "0" + tempDateTo[1] : tempDateTo[1]) + "/" + (tempDateTo[0].Length == 1 ? "0" + tempDateTo[0] : tempDateTo[0]) + "/" + tempDateTo[2].Substring(0, 4);
                    }
                    if (Utility.IsNotDBNull(dr["payer_type"]))
                    {
                        payerType = dr["payer_type"].ToString().Replace(@"""", "").Replace("'", "");
                        if (payerType.Equals("C"))
                            payerType = "Customer";
                        else if (payerType.Equals("N"))
                            payerType = "Non-revenue";
                        else if (payerType.Equals("F"))
                            payerType = "On-Forwarding";
                    }
                    if (Utility.IsNotDBNull(dr["dateType"]))
                    {
                        data_type = dr["dateType"].ToString();
                        if (data_type.Equals("1"))
                            data_type = "I";
                        else if (data_type.Equals("2"))
                            data_type = "M";
                    }

                    if (dateTo == "")
                        dateTo = dateFrom;

                    SetAutomatedReport(m_dsResult, dateFrom, dateTo, payerType, data_type);
                }
                else
                    return;
            }

        }

        private void SetAccPacExport(DataSet dsResult)
        {
            int i = 0;
            TIES.WebUI.DSAccPacExport DSShippingXSDds = new TIES.WebUI.DSAccPacExport();

            int iRecCnt = dsResult.Tables[0].Rows.Count;
            for (i = 0; i < iRecCnt; i++)
            {
                DataRow drEach = dsResult.Tables[0].Rows[i];
                DataRow drXSD = DSShippingXSDds.Tables["AccPacExport"].NewRow();

                #region set value

                if (Utility.IsNotDBNull(drEach["FreightNo"]))
                    drXSD["FreightNo"] = drEach["FreightNo"];

                if (Utility.IsNotDBNull(drEach["Date"]))
                    drXSD["Date"] = drEach["Date"];

                if (Utility.IsNotDBNull(drEach["Origin"]))
                    drXSD["Origin"] = drEach["Origin"];

                if (Utility.IsNotDBNull(drEach["Destination"]))
                    drXSD["Destination"] = drEach["Destination"];

                if (Utility.IsNotDBNull(drEach["CustomerID"]))
                    drXSD["CustomerID"] = drEach["CustomerID"];

                if (Utility.IsNotDBNull(drEach["Customer"]))
                    drXSD["Customer"] = drEach["Customer"];

                if (Utility.IsNotDBNull(drEach["Docs"]))
                    drXSD["Docs"] = drEach["Docs"];

                if (Utility.IsNotDBNull(drEach["Weight"]))
                    drXSD["Weight"] = drEach["Weight"];

                if (Utility.IsNotDBNull(drEach["Service"]))
                    drXSD["Service"] = drEach["Service"];

                if (Utility.IsNotDBNull(drEach["VAS"]))
                    drXSD["VAS"] = drEach["VAS"];

                if (Utility.IsNotDBNull(drEach["Revenue"]))
                    drXSD["Revenue"] = drEach["Revenue"];

                if (Utility.IsNotDBNull(drEach["Tax"]))
                    drXSD["Tax"] = drEach["Tax"];

                if (Utility.IsNotDBNull(drEach["exported_by"]))
                    drXSD["exported_by"] = drEach["exported_by"];

                if (Utility.IsNotDBNull(drEach["jobid"]))
                    drXSD["jobid"] = drEach["jobid"];

                #endregion

                DSShippingXSDds.Tables["AccPacExport"].Rows.Add(drXSD);
            }

            rptSource.SetDataSource(DSShippingXSDds);

            BindReport();
        }

        private void SetAutomatedReport(DataSet dsResult, string date_from, string date_to, string customer_type, string data_type)
        {
            ParameterFieldDefinitions paramFldDefs;
            ParameterValues paramVals = new ParameterValues();
            ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
            paramFldDefs = rptSource.DataDefinition.ParameterFields;

            foreach (ParameterFieldDefinition paramFldDef in paramFldDefs)
            {
                switch (paramFldDef.ParameterFieldName)
                {
                    case "date_from":
                        paramDVal.Value = date_from;
                        break;
                    case "date_to":
                        paramDVal.Value = date_to;
                        break;
                    case "customer_type":
                        paramDVal.Value = customer_type;
                        break;
                    case "data_type":
                        paramDVal.Value = data_type;
                        break;
                    default:
                        continue;
                }

                paramVals = paramFldDef.CurrentValues;
                paramVals.Add(paramDVal);
                paramFldDef.ApplyCurrentValues(paramVals);
            }

            int i;
            int iRecCnt;
            TIES.WebUI.DSAutomatedReport dsAutomatedReport = new TIES.WebUI.DSAutomatedReport();

            //SetResult
            i = 0;
            iRecCnt = dsResult.Tables[0].Rows.Count;
            for (i = 0; i < iRecCnt; i++)
            {
                DataRow drEach = dsResult.Tables[0].Rows[i];
                DataRow drXSD = dsAutomatedReport.Tables["AutomatedReport"].NewRow();

                #region set value

                if (Utility.IsNotDBNull(drEach["RowId"]))
                    drXSD["RowId"] = drEach["RowId"];

                if (Utility.IsNotDBNull(drEach["Name"]))
                    drXSD["Name"] = drEach["Name"].ToString();

                if (Utility.IsNotDBNull(drEach["PayerCode"]))
                    drXSD["PayerCode"] = drEach["PayerCode"].ToString();

                if (Utility.IsNotDBNull(drEach["PayerName"]))
                    drXSD["PayerName"] = drEach["PayerName"].ToString();

                if (Utility.IsNotDBNull(drEach["Month"]))
                    drXSD["Month"] = drEach["Month"].ToString();

                if (Utility.IsNotDBNull(drEach["Value"]))
                    drXSD["Value"] = drEach["Value"];

                if (Utility.IsNotDBNull(drEach["ReportType"]))
                    drXSD["ReportType"] = drEach["ReportType"].ToString();

                if (Utility.IsNotDBNull(drEach["ReportDate"]))
                    drXSD["ReportDate"] = drEach["ReportDate"];

                #endregion

                dsAutomatedReport.Tables["AutomatedReport"].Rows.Add(drXSD);
            }
            rptSource.SetDataSource(dsAutomatedReport);

            BindReport();
        }

        private void SetUpliftStatistics(DataSet dsResult, string date_from, string date_to, string customer_type, string data_type)
        {
            ParameterFieldDefinitions paramFldDefs;
            ParameterValues paramVals = new ParameterValues();
            ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
            paramFldDefs = rptSource.DataDefinition.ParameterFields;

            //foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
            //{
            //	switch(paramFldDef.ParameterFieldName)
            //	{
            //		case "date_from":
            //			paramDVal.Value = date_from; 
            //			break;
            //		case "date_to":
            //			paramDVal.Value = date_to; 
            //			break;
            //		case "customer_type":
            //			paramDVal.Value = customer_type; 
            //			break;
            //		case "data_type":
            //			paramDVal.Value = data_type;
            //			break;
            //		default:
            //			continue;
            //	}

            //	paramVals = paramFldDef.CurrentValues;
            //	paramVals.Add(paramDVal);
            //	paramFldDef.ApplyCurrentValues(paramVals);
            //}

            TIES.WebUI.DSUpliftStatistics dsUpliftStatistics = new TIES.WebUI.DSUpliftStatistics();

            DataRow dr;
            int countNewRow = 1;

            if (m_dsResult != null)
                if (m_dsResult.Tables[0] != null)
                    if (m_dsResult.Tables[0].Rows.Count > 0)
                    {
                        dr = dsUpliftStatistics.Tables["UpliftStatisticsByBranch"].NewRow();

                        for (int i = 0; i < m_dsResult.Tables[0].Rows.Count; i++)
                        {
                            DataRow drEach = dsResult.Tables[0].Rows[i];

                            if (Utility.IsNotDBNull(drEach["dateDescription"]))
                                dr["dateDescription"] = drEach["dateDescription"];

                            if (Utility.IsNotDBNull(drEach["FROM"]))
                                dr["from" + countNewRow.ToString()] = drEach["FROM"].ToString();
                            if (Utility.IsNotDBNull(drEach["TO"]))
                                dr["to" + countNewRow.ToString()] = drEach["TO"].ToString();
                            if (Utility.IsNotDBNull(drEach["WEIGHT"]))
                                dr["weight" + countNewRow.ToString()] = drEach["WEIGHT"];

                            if (countNewRow == 27)
                            {
                                dsUpliftStatistics.Tables["UpliftStatisticsByBranch"].Rows.Add(dr);
                                dr = dsUpliftStatistics.Tables["UpliftStatisticsByBranch"].NewRow();

                                countNewRow = 0;
                            }

                            countNewRow++;
                        }
                    }


            rptSource.SetDataSource(dsUpliftStatistics);

            //22-05-2019 aw_nungs fixed error : after rptSource.SetDataSource() the CurrentValues of paramter has been delete by this command.
            foreach (ParameterFieldDefinition paramFldDef in paramFldDefs)
            {
                switch (paramFldDef.ParameterFieldName)
                {
                    case "date_from":
                        paramDVal.Value = date_from;
                        break;
                    case "date_to":
                        paramDVal.Value = date_to;
                        break;
                    case "customer_type":
                        paramDVal.Value = customer_type;
                        break;
                    case "data_type":
                        paramDVal.Value = data_type;
                        break;
                    default:
                        continue;
                }

                paramVals = paramFldDef.CurrentValues;
                paramVals.Add(paramDVal);
                paramFldDef.ApplyCurrentValues(paramVals);
            }

            BindReport();
        }

        private void SetUpliftStatisticsExHub(DataSet dsResult, string date_from, string date_to, string customer_type, string data_type)
        {
            ParameterFieldDefinitions paramFldDefs;
            ParameterValues paramVals = new ParameterValues();
            ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
            paramFldDefs = rptSource.DataDefinition.ParameterFields;

            //foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
            //{
            //	switch(paramFldDef.ParameterFieldName)
            //	{
            //		case "date_from":
            //			paramDVal.Value = date_from; 
            //			break;
            //		case "date_to":
            //			paramDVal.Value = date_to; 
            //			break;
            //		case "customer_type":
            //			paramDVal.Value = customer_type ; 
            //			break;
            //		case "data_type":
            //			paramDVal.Value = data_type;
            //			break;
            //		default:
            //			continue;
            //	}

            //	paramVals = paramFldDef.CurrentValues;
            //	paramVals.Add(paramDVal);
            //	paramFldDef.ApplyCurrentValues(paramVals);
            //}

            TIES.WebUI.DSUpliftStatistics dsUpliftStatistics = new TIES.WebUI.DSUpliftStatistics();

            DataRow dr;
            int countNewRow = 1;

            if (m_dsResult != null)
                if (m_dsResult.Tables[0] != null)
                    if (m_dsResult.Tables[0].Rows.Count > 0)
                    {
                        dr = dsUpliftStatistics.Tables["UpliftStatisticsExHub"].NewRow();

                        for (int i = 0; i < m_dsResult.Tables[0].Rows.Count; i++)
                        {
                            DataRow drEach = dsResult.Tables[0].Rows[i];
                            DataRow drGTotal = dsResult.Tables[1].Rows[countNewRow - 1];

                            if (Utility.IsNotDBNull(drEach["dateDescription"]))
                                dr["dateDescription"] = drEach["dateDescription"];

                            //							if (Utility.IsNotDBNull(drEach["FROM"]))
                            //								dr["from" + countNewRow.ToString()] = drEach["FROM"].ToString();
                            //							if (Utility.IsNotDBNull(drEach["TO"]))
                            //								dr["to" + countNewRow.ToString()] = drEach["TO"].ToString();
                            if (Utility.IsNotDBNull(drEach["WEIGHT"]))
                                dr["weight" + countNewRow.ToString()] = drEach["WEIGHT"];
                            if (Utility.IsNotDBNull(drGTotal["WEIGHT"]))
                                dr["grand_total" + countNewRow.ToString()] = drGTotal["WEIGHT"];


                            if (countNewRow == 27)
                            {
                                //Fix : remove  sunday row  23/02/2018  
                                //if (Utility.IsNotDBNull(drEach["DateDescription"]))
                                //dr["from"] = drEach["DateDescription"].ToString();
                                //if (Utility.IsNotDBNull(drEach["IsTotal"]))
                                //	dr["IsTotal"] = drEach["IsTotal"];
                                if (Utility.IsNotDBNull(drEach["Date"]))
                                {
                                    //									if(dr["from"].Equals("Sunday"))
                                    //									{
                                    //										dr["date"]
                                    //									}
                                    dr["date"] = drEach["Date"].ToString();
                                }

                                dsUpliftStatistics.Tables["UpliftStatisticsExHub"].Rows.Add(dr);
                                dr = dsUpliftStatistics.Tables["UpliftStatisticsExHub"].NewRow();

                                countNewRow = 0;
                            }

                            countNewRow++;
                        }
                    }
            rptSource.SetDataSource(dsUpliftStatistics);

            //22-05-2019 aw_nungs fixed error : after rptSource.SetDataSource() the CurrentValues of paramter has been delete by this command.
            foreach (ParameterFieldDefinition paramFldDef in paramFldDefs)
            {
                switch (paramFldDef.ParameterFieldName)
                {
                    case "date_from":
                        paramDVal.Value = date_from;
                        break;
                    case "date_to":
                        paramDVal.Value = date_to;
                        break;
                    case "customer_type":
                        paramDVal.Value = customer_type;
                        break;
                    case "data_type":
                        paramDVal.Value = data_type;
                        break;
                    default:
                        continue;
                }

                paramVals = paramFldDef.CurrentValues;
                paramVals.Add(paramDVal);
                paramFldDef.ApplyCurrentValues(paramVals);
            }
            BindReport();
        }

        private void SetFreightSummary(DataSet dsResult, string mode)
        {
            int i = 0;
            TIES.WebUI.DSFreightSummary DSShippingXSDds = new TIES.WebUI.DSFreightSummary();

            int iRecCnt = dsResult.Tables[0].Rows.Count;
            for (i = 0; i < iRecCnt; i++)
            {
                DataRow drEach = dsResult.Tables[0].Rows[i];
                DataRow drXSD = DSShippingXSDds.Tables["FreightSummary"].NewRow();

                #region set value

                if (Utility.IsNotDBNull(drEach["shpt_manifest_datetime"]))
                    drXSD["shpt_manifest_datetime"] = drEach["shpt_manifest_datetime"];

                if (Utility.IsNotDBNull(drEach["consignment_no"]))
                    drXSD["consignment_no"] = drEach["consignment_no"].ToString();

                if (Utility.IsNotDBNull(drEach["origin_station"]))
                    drXSD["origin_station"] = drEach["origin_station"].ToString();

                if (Utility.IsNotDBNull(drEach["destination_station"]))
                    drXSD["destination_station"] = drEach["destination_station"].ToString();

                if (Utility.IsNotDBNull(drEach["destination_station"]))
                    drXSD["destination_station"] = drEach["destination_station"].ToString();

                if (Utility.IsNotDBNull(drEach["tot_act_wt"]))
                    drXSD["tot_act_wt"] = drEach["tot_act_wt"];

                if (Utility.IsNotDBNull(drEach["rate"]))
                    drXSD["rate"] = drEach["rate"];

                if (Utility.IsNotDBNull(drEach["basic_charge"]))
                    drXSD["basic_charge"] = drEach["basic_charge"];

                if (Utility.IsNotDBNull(drEach["tot_freight_charge"]))
                    drXSD["tot_freight_charge"] = drEach["tot_freight_charge"];

                if (Utility.IsNotDBNull(drEach["other_surch_amount"]))
                    drXSD["other_surch_amount"] = drEach["other_surch_amount"];

                if (Utility.IsNotDBNull(drEach["tot_vas_surcharge"]))
                    drXSD["tot_vas_surcharge"] = drEach["tot_vas_surcharge"];

                if (Utility.IsNotDBNull(drEach["tax"]))
                    drXSD["tax"] = drEach["tax"];

                if (Utility.IsNotDBNull(drEach["MasterAWBNumber"]))
                    drXSD["MasterAWBNumber"] = drEach["MasterAWBNumber"];

                if (Utility.IsNotDBNull(drEach["payerid"]))
                    drXSD["payerid"] = drEach["payerid"].ToString();

                if (Utility.IsNotDBNull(drEach["cust_name"]))
                    drXSD["cust_name"] = drEach["cust_name"].ToString();

                if (Utility.IsNotDBNull(drEach["total_rated_amount"]))
                    drXSD["total_rated_amount"] = drEach["total_rated_amount"].ToString();

                if (Utility.IsNotDBNull(drEach["invoice_amt"]))
                    drXSD["invoice_amt"] = drEach["invoice_amt"].ToString();

                if (Utility.IsNotDBNull(drEach["mode"]))
                    drXSD["mode"] = drEach["mode"].ToString();

                if (Utility.IsNotDBNull(drEach["dateFrom"]))
                    drXSD["dateFrom"] = drEach["dateFrom"].ToString();

                if (Utility.IsNotDBNull(drEach["dateTo"]))
                    drXSD["dateTo"] = drEach["dateTo"].ToString();

                if (Utility.IsNotDBNull(drEach["code_text"]))
                    drXSD["code_text"] = drEach["code_text"].ToString();

                if (Utility.IsNotDBNull(drEach["cost_centre"]))
                    drXSD["cost_centre"] = drEach["cost_centre"].ToString();

                if (Utility.IsNotDBNull(drEach["service_code"]))
                    drXSD["service_code"] = drEach["service_code"].ToString();

                if (Utility.IsNotDBNull(drEach["ConsignmentType"]))
                    drXSD["ConsignmentType"] = drEach["ConsignmentType"].ToString();

                if (Utility.IsNotDBNull(drEach["AgencyFees"]))
                    drXSD["AgencyFees"] = drEach["AgencyFees"].ToString();

                if (Utility.IsNotDBNull(drEach["Disbursements"]))
                    drXSD["Disbursements"] = drEach["Disbursements"].ToString();

                if (Utility.IsNotDBNull(drEach["TotalInvoice"]))
                    drXSD["TotalInvoice"] = drEach["TotalInvoice"].ToString();

                if (Utility.IsNotDBNull(drEach["pngaf_invoice"]))
                    drXSD["pngaf_invoice"] = drEach["pngaf_invoice"].ToString();

                if (Utility.IsNotDBNull(drEach["tot_pkg"]))
                    drXSD["tot_pkg"] = drEach["tot_pkg"].ToString();
                #endregion

                DSShippingXSDds.Tables["FreightSummary"].Rows.Add(drXSD);


            }
            rptSource.SetDataSource(DSShippingXSDds);
            dsFreight = DSShippingXSDds;

            ParameterFieldDefinitions paramFldDefs;
            ParameterValues paramVals = new ParameterValues();
            ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
            paramFldDefs = rptSource.DataDefinition.ParameterFields;

            foreach (ParameterFieldDefinition paramFldDef in paramFldDefs)
            {
                switch (paramFldDef.ParameterFieldName)
                {
                    case "mode":
                        paramDVal.Value = mode;
                        break;
                    default:
                        continue;
                }

                paramVals = paramFldDef.CurrentValues;
                paramVals.Add(paramDVal);
                paramFldDef.ApplyCurrentValues(paramVals);
            }

            BindReport();
        }

        private void SetSopScanReportDataSet(DataSet dsResult)
        {
            int i = 0;
            TIES.WebUI.DSShippingReport DSShippingXSDds = new TIES.WebUI.DSShippingReport();

            int iRecCnt = dsResult.Tables[0].Rows.Count;
            for (i = 0; i < iRecCnt; i++)
            {
                DataRow drEach = dsResult.Tables[0].Rows[i];
                DataRow drXSD = DSShippingXSDds.Tables["DSShippingReport"].NewRow();

                #region set value
                if (Utility.IsNotDBNull(drEach["consignment_no"]))
                    drXSD["consignment_no"] = drEach["consignment_no"].ToString();

                if (Utility.IsNotDBNull(drEach["package_no"]))
                    drXSD["package_no"] = drEach["package_no"].ToString();

                if (Utility.IsNotDBNull(drEach["status_code"]))
                    drXSD["status_code"] = drEach["status_code"].ToString();

                if (Utility.IsNotDBNull(drEach["scanned_in_datetime"]))
                    drXSD["scanned_in_datetime"] = drEach["scanned_in_datetime"].ToString();

                if (Utility.IsNotDBNull(drEach["dc_out_route"]))
                    drEach["dc_out_route"] = drEach["dc_out_route"].ToString();

                if (Utility.IsNotDBNull(drEach["line_haul1"]))
                    drXSD["line_haul1"] = drEach["line_haul1"].ToString();

                if (Utility.IsNotDBNull(drEach["line_haul2"]))
                    drXSD["line_haul2"] = drEach["line_haul2"].ToString();

                if (Utility.IsNotDBNull(drEach["delivery_route_code"]))
                    drXSD["delivery_route_code"] = drEach["delivery_route_code"].ToString();

                if (Utility.IsNotDBNull(drEach["process_status"]))
                    drXSD["process_status"] = drEach["process_status"].ToString();

                if (Utility.IsNotDBNull(drEach["print_datetime"]))
                    drXSD["print_datetime"] = drEach["print_datetime"].ToString();

                if (Utility.IsNotDBNull(drEach["OriginDC"]))
                    drXSD["OriginDC"] = drEach["OriginDC"].ToString();

                if (Utility.IsNotDBNull(drEach["DestDC"]))
                    drXSD["DestDC"] = drEach["DestDC"].ToString();

                if (Utility.IsNotDBNull(drEach["swb_id"]))
                    drXSD["swb_id"] = drEach["swb_id"].ToString();

                if (Utility.IsNotDBNull(drEach["dest_zipcode"]))
                    drXSD["dest_zipcode"] = drEach["dest_zipcode"].ToString();

                if (Utility.IsNotDBNull(drEach["act_Weight"]))
                    drXSD["act_Weight"] = Convert.ToDouble(drEach["act_Weight"]);

                if (Utility.IsNotDBNull(drEach["vol_m3"]))
                    drXSD["vol_m3"] = Convert.ToDecimal(drEach["vol_m3"]);

                if (Utility.IsNotDBNull(drEach["service_type"]))
                    drXSD["service_type"] = drEach["service_type"].ToString();

                if (Utility.IsNotDBNull(drEach["user_id"]))
                    drXSD["user_id"] = drEach["user_id"].ToString();

                //Modified by GwanG on 10March08
                //Query Header
                DataSet dsHeader = ReportDAL.GetHeaderCompName(m_strAppid, m_strEnterpriseid);
                if (dsHeader.Tables[0].Rows.Count > 0)
                {
                    DataRow drHeader = dsHeader.Tables[0].Rows[0];
                    //companyName,companyAddress, countryName
                    if (Utility.IsNotDBNull(drHeader["companyName"]))
                        drXSD["enterprise_name"] = drHeader["companyName"].ToString();

                    if (Utility.IsNotDBNull(drHeader["companyAddress"]))
                        drXSD["enterprise_address"] = drHeader["companyAddress"].ToString() + " " + drHeader["countryName"].ToString();

                }

                drXSD["report_name_label"] = "SOP Shipping Report";

                drXSD["user_print_id"] = strUserId;

                if (Utility.IsNotDBNull(drEach["total_pkgs"]))
                    drXSD["total_pkgs"] = Convert.ToInt32(drEach["total_pkgs"]);

                if (Utility.IsNotDBNull(drEach["DriverName"]))
                    drXSD["DriverName"] = drEach["DriverName"].ToString();

                if (Utility.IsNotDBNull(drEach["VehicleNumber"]))
                    drXSD["VehicleNumber"] = drEach["VehicleNumber"].ToString();
                #endregion

                #region binding value
                if (Utility.IsNotDBNull(drXSD["consignment_no"]))
                    DSShippingXSDds.DSShippingReportDT.consignment_noColumn.DefaultValue = drXSD["consignment_no"];

                if (Utility.IsNotDBNull(drXSD["package_no"]))
                    DSShippingXSDds.DSShippingReportDT.package_noColumn.DefaultValue = drXSD["package_no"];

                if (Utility.IsNotDBNull(drXSD["status_code"]))
                    DSShippingXSDds.DSShippingReportDT.status_codeColumn.DefaultValue = drXSD["status_code"];

                if (Utility.IsNotDBNull(drXSD["scanned_in_datetime"]))
                    DSShippingXSDds.DSShippingReportDT.scanned_in_datetimeColumn.DefaultValue = drXSD["scanned_in_datetime"];

                if (Utility.IsNotDBNull(drXSD["dc_out_route"]))
                    DSShippingXSDds.DSShippingReportDT.dc_out_routeColumn.DefaultValue = drXSD["dc_out_route"];

                if (Utility.IsNotDBNull(drXSD["line_haul1"]))
                    DSShippingXSDds.DSShippingReportDT.line_haul1Column.DefaultValue = drXSD["line_haul1"];

                if (Utility.IsNotDBNull(drXSD["line_haul2"]))
                    DSShippingXSDds.DSShippingReportDT.line_haul2Column.DefaultValue = drXSD["line_haul2"];

                if (Utility.IsNotDBNull(drXSD["delivery_route_code"]))
                    DSShippingXSDds.DSShippingReportDT.delivery_route_codeColumn.DefaultValue = drXSD["delivery_route_code"];

                if (Utility.IsNotDBNull(drXSD["process_status"]))
                    DSShippingXSDds.DSShippingReportDT.process_statusColumn.DefaultValue = drXSD["process_status"];

                if (Utility.IsNotDBNull(drXSD["print_datetime"]))
                    DSShippingXSDds.DSShippingReportDT.print_datetimeColumn.DefaultValue = drXSD["print_datetime"];

                if (Utility.IsNotDBNull(drXSD["OriginDC"]))
                    DSShippingXSDds.DSShippingReportDT.OriginDCColumn.DefaultValue = drXSD["OriginDC"];

                if (Utility.IsNotDBNull(drXSD["DestDC"]))
                    DSShippingXSDds.DSShippingReportDT.DestDCColumn.DefaultValue = drXSD["DestDC"];

                if (Utility.IsNotDBNull(drXSD["swb_id"]))
                    DSShippingXSDds.DSShippingReportDT.swb_idColumn.DefaultValue = drXSD["swb_id"];

                if (Utility.IsNotDBNull(drXSD["dest_zipcode"]))
                    DSShippingXSDds.DSShippingReportDT.dest_zipcodeColumn.DefaultValue = drXSD["dest_zipcode"];

                if (Utility.IsNotDBNull(drXSD["act_Weight"]))
                    DSShippingXSDds.DSShippingReportDT.act_WeightColumn.DefaultValue = drXSD["act_Weight"];

                if (Utility.IsNotDBNull(drXSD["vol_m3"]))
                    DSShippingXSDds.DSShippingReportDT.vol_m3Column.DefaultValue = drXSD["vol_m3"];

                if (Utility.IsNotDBNull(drXSD["service_type"]))
                    DSShippingXSDds.DSShippingReportDT.service_typeColumn.DefaultValue = drXSD["service_type"];

                if (Utility.IsNotDBNull(drXSD["user_id"]))
                    DSShippingXSDds.DSShippingReportDT.user_idColumn.DefaultValue = drXSD["user_id"];

                if (Utility.IsNotDBNull(drXSD["enterprise_name"]))
                    DSShippingXSDds.DSShippingReportDT.enterprise_nameColumn.DefaultValue = drXSD["enterprise_name"];

                if (Utility.IsNotDBNull(drXSD["enterprise_address"]))
                    DSShippingXSDds.DSShippingReportDT.enterprise_addressColumn.DefaultValue = drXSD["enterprise_address"];


                if (Utility.IsNotDBNull(drXSD["report_name_label"]))
                    DSShippingXSDds.DSShippingReportDT.report_name_labelColumn.DefaultValue = drXSD["report_name_label"];

                if (Utility.IsNotDBNull(drXSD["user_print_id"]))
                    DSShippingXSDds.DSShippingReportDT.user_print_idColumn.DefaultValue = drXSD["user_print_id"].ToString();

                if (Utility.IsNotDBNull(drXSD["total_pkgs"]))
                    DSShippingXSDds.DSShippingReportDT.total_pkgsColumn.DefaultValue = drXSD["total_pkgs"];

                if (Utility.IsNotDBNull(drXSD["DriverName"]))
                    DSShippingXSDds.DSShippingReportDT.DriverNameColumn.DefaultValue = drXSD["DriverName"].ToString();

                if (Utility.IsNotDBNull(drXSD["VehicleNumber"]))
                    DSShippingXSDds.DSShippingReportDT.VehicleNumberColumn.DefaultValue = drXSD["VehicleNumber"].ToString();

                #endregion

                DSShippingXSDds.Tables["DSShippingReport"].Rows.Add(drXSD);
            }

            rptSource.SetDataSource(DSShippingXSDds);

            BindReport();
        }
        private void SetLogonInfo()
        {
            //create a structure object to store connection details
            ConnectionDetails conDet = new ConnectionDetails();

            //get connection details from dbcon manager
            conDet = DbConnectionManager.GetConnectionDetails(m_strAppid, m_strEnterpriseid);

            TableLogOnInfo MyLogin;
            //get the no of tables in the main report and 
            //assign connection details to all
            int intTabCnt = rptSource.Database.Tables.Count, i = 0;
            for (i = 0; i < intTabCnt; i++)
            {
                MyLogin = rptSource.Database.Tables[i].LogOnInfo;
                if (Utility.IsNotDBNull(conDet.DatabaseName))
                    MyLogin.ConnectionInfo.DatabaseName = conDet.DatabaseName;
                MyLogin.ConnectionInfo.ServerName = conDet.ServerName;
                MyLogin.ConnectionInfo.UserID = conDet.UserID;
                MyLogin.ConnectionInfo.Password = conDet.Password;
                rptSource.Database.Tables[i].ApplyLogOnInfo(MyLogin);
                rptSource.Database.Tables[i].Location = rptSource.Database.Tables[i].Name.ToUpper();

            }

            //get subreports from the main report
            Sections crSections; //new Sections();
            ReportObjects crRepObjects; //new ReportObjects();
            SubreportObject crSubRepObj; //new SubreportObject();
            ReportDocument crSubRep; //new ReportDocument();

            crSections = rptSource.ReportDefinition.Sections;
            int intSecCnt = rptSource.ReportDefinition.Sections.Count;
            for (i = 0; i < intSecCnt; i++)
            {
                crRepObjects = crSections[i].ReportObjects;
                int intSubCnt = crSections[i].ReportObjects.Count;
                for (int j = 0; j < intSubCnt; j++)
                {
                    if (crRepObjects[j].Kind == ReportObjectKind.SubreportObject)
                    {
                        crSubRepObj = (SubreportObject)crRepObjects[j];
                        crSubRep = crSubRepObj.OpenSubreport(crSubRepObj.SubreportName);

                        //get the no of tables in the sub report and 
                        //assign connection details to all
                        int intSubTabCnt = crSubRep.Database.Tables.Count;
                        for (int k = 0; k < intSubTabCnt; k++)
                        {
                            MyLogin = crSubRep.Database.Tables[k].LogOnInfo;
                            if (Utility.IsNotDBNull(conDet.DatabaseName))
                                MyLogin.ConnectionInfo.DatabaseName = conDet.DatabaseName;
                            MyLogin.ConnectionInfo.ServerName = conDet.ServerName;
                            MyLogin.ConnectionInfo.UserID = conDet.UserID;
                            MyLogin.ConnectionInfo.Password = conDet.Password;
                            crSubRep.Database.Tables[k].ApplyLogOnInfo(MyLogin);
                            crSubRep.Database.Tables[k].Location = crSubRep.Database.Tables[k].Name.ToUpper();
                        }
                    }
                }
            }
        }

        private void BindReport()
        {
            rptViewer.ReportSource = rptSource;

        }

        private void SetTopNReportDataSet(DataSet dsResult)
        {
            int i = 0;
            TIES.WebUI.TopNReportds TopNXSDds = new TIES.WebUI.TopNReportds();
            int iRecCnt = dsResult.Tables[0].Rows.Count;
            for (i = 0; i < iRecCnt; i++)
            {
                DataRow drEach = dsResult.Tables[0].Rows[i];
                DataRow drXSD = TopNXSDds.Tables["TopN"].NewRow();

                if (Utility.IsNotDBNull(drEach["enterprise_name"]))
                    drXSD["enterprise_name"] = (string)drEach["enterprise_name"];
                if (Utility.IsNotDBNull(drEach["currency"]))
                    drXSD["currency"] = (string)drEach["currency"];
                if (Utility.IsNotDBNull(drEach["payerid"]))
                    drXSD["payerid"] = (string)drEach["payerid"];
                if (Utility.IsNotDBNull(drEach["payer_name"]))
                    drXSD["payer_name"] = (string)drEach["payer_name"];
                if (Utility.IsNotDBNull(drEach["payer_type"]))
                    drEach["payer_type"] = (string)drEach["payer_type"];
                if (Utility.IsNotDBNull(drEach["no_of_consignment"]))
                    drXSD["no_of_consignment"] = Convert.ToDecimal(drEach["no_of_consignment"]);
                if (Utility.IsNotDBNull(drEach["chargeable_wt"]))
                    drXSD["chargeable_wt"] = Convert.ToDecimal(drEach["chargeable_wt"]);
                if (Utility.IsNotDBNull(drEach["tot_volume"]))
                    drXSD["tot_volume"] = Convert.ToDecimal(drEach["tot_volume"]);
                if (Utility.IsNotDBNull(drEach["revenue"]))
                    drXSD["revenue"] = Convert.ToDecimal(drEach["revenue"]);


                if (Utility.IsNotDBNull(drXSD["enterprise_name"]))
                    TopNXSDds.TopN.enterprise_nameColumn.DefaultValue = drXSD["enterprise_name"];
                if (Utility.IsNotDBNull(drXSD["currency"]))
                    TopNXSDds.TopN.currencyColumn.DefaultValue = drXSD["currency"];
                if (Utility.IsNotDBNull(drXSD["payerid"]))
                    TopNXSDds.TopN.payeridColumn.DefaultValue = drXSD["payerid"];
                if (Utility.IsNotDBNull(drXSD["payer_name"]))
                    TopNXSDds.TopN.payer_nameColumn.DefaultValue = drXSD["payer_name"];
                if (Utility.IsNotDBNull(drXSD["payer_type"]))
                    TopNXSDds.TopN.payer_typeColumn.DefaultValue = drXSD["payer_type"];
                if (Utility.IsNotDBNull(drXSD["no_of_consignment"]))
                    TopNXSDds.TopN.no_of_consignmentColumn.DefaultValue = drXSD["no_of_consignment"];
                if (Utility.IsNotDBNull(drXSD["chargeable_wt"]))
                    TopNXSDds.TopN.no_of_consignmentColumn.DefaultValue = drXSD["chargeable_wt"];
                if (Utility.IsNotDBNull(drXSD["tot_volume"]))
                    TopNXSDds.TopN.chargeable_wtColumn.DefaultValue = drXSD["tot_volume"];
                if (Utility.IsNotDBNull(drXSD["revenue"]))
                    TopNXSDds.TopN.revenueColumn.DefaultValue = drXSD["revenue"];

                TopNXSDds.Tables["TopN"].Rows.Add(drXSD);
            }
            rptSource.SetDataSource(TopNXSDds);

            BindReport();
        }

        private void SetTopNReport()
        {
            //declare instance for parameterfields class
            ParameterFieldDefinitions paramFldDefs;
            ParameterValues paramVals = new ParameterValues();
            ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
            paramFldDefs = rptSource.DataDefinition.ParameterFields;

            DateTime dtStartDate = DateTime.Now;
            DateTime dtEndDate = DateTime.Now;
            String strCustPayerType = "";
            int iTopN = 0;
            String strRouteType = "";
            String strRouteCode = "";
            String strorigin_dc = "";
            String strdestination_dc = "";
            String strdelPathorigin_dc = "";
            String strdelPathdestination_dc = "";
            String strZipCode = "";
            String strStateCode = "";
            String strSortBy = "";
            String strBasis = "";

            DataRow dr = m_dsQuery.Tables[0].Rows[0];

            if (Utility.IsNotDBNull(dr["start_date"]))
                dtStartDate = Convert.ToDateTime(dr["start_date"]);
            else
                dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
            if (Utility.IsNotDBNull(dr["end_date"]))
                dtEndDate = Convert.ToDateTime(dr["end_date"]);
            else
                dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

            if (Utility.IsNotDBNull(dr["payer_type"]))
                strCustPayerType = dr["payer_type"].ToString();

            if (Utility.IsNotDBNull(dr["top_n"]))
                iTopN = Convert.ToInt32(dr["top_n"]);
            else
                // case selected Top 'All'
                iTopN = -1;

            if (Utility.IsNotDBNull(dr["route_type"]))
                strRouteType = dr["route_type"].ToString();
            if (Utility.IsNotDBNull(dr["route_code"]))
                strRouteCode = dr["route_code"].ToString();

            if (Utility.IsNotDBNull(dr["origin_dc"]))
                strorigin_dc = dr["origin_dc"].ToString();
            if (Utility.IsNotDBNull(dr["destination_dc"]))
                strdestination_dc = dr["destination_dc"].ToString();

            if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
                strdelPathorigin_dc = dr["delPath_origin_dc"].ToString();
            if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
                strdelPathdestination_dc = dr["delPath_destination_dc"].ToString();

            if (Utility.IsNotDBNull(dr["zip_code"]))
                strZipCode = dr["zip_code"].ToString();
            if (Utility.IsNotDBNull(dr["state_code"]))
                strStateCode = dr["state_code"].ToString();

            if (Utility.IsNotDBNull(dr["basis"]))
                strBasis = dr["basis"].ToString();

            if (Utility.IsNotDBNull(dr["sort_by"]))
                strSortBy = dr["sort_by"].ToString();

            foreach (ParameterFieldDefinition paramFldDef in paramFldDefs)
            {
                switch (paramFldDef.ParameterFieldName)
                {
                    case "prmTopN":
                        paramDVal.Value = iTopN;
                        break;
                    case "prmSortBy":
                        paramDVal.Value = strSortBy;
                        break;
                    case "prmStartDate":
                        paramDVal.Value = dtStartDate;
                        break;
                    case "prmEndDate":
                        paramDVal.Value = dtEndDate;
                        break;
                    case "prmUserId":
                        paramDVal.Value = strUserId;
                        break;
                    case "prmBasis":
                        paramDVal.Value = strBasis;
                        break;
                    case "prmCustPayerType":
                        paramDVal.Value = strCustPayerType;
                        break;
                    default:
                        continue;
                }
                paramVals = paramFldDef.CurrentValues;
                paramVals.Add(paramDVal);
                paramFldDef.ApplyCurrentValues(paramVals);

            }
        }

        private void SetShipPendingPOD()
        {
            DateTime dtStartDate, dtEndDate;
            String strShpPending = "";
            String strRouteCode = "*", strRouteType = "*";
            String strPayerID = "*", strPayerType = "";
            String strorigin_dc = "*", strdestination_dc = "*";
            String strdelPath_origin_dc = "*", strdelPath_destination_dc = "*";
            String strzip_code = "*", strstate_code = "*";

            //declare instance for parameterfields class
            ParameterFieldDefinitions paramFldDefs;
            ParameterValues paramVals = new ParameterValues();
            ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
            paramFldDefs = rptSource.DataDefinition.ParameterFields;
            DataRow dr = m_dsQuery.Tables[0].Rows[0];

            if (Utility.IsNotDBNull(dr["ShipType"]))
                strShpPending = dr["ShipType"].ToString();

            if (Utility.IsNotDBNull(dr["start_date"]))
                dtStartDate = Convert.ToDateTime(dr["start_date"]);
            else
                dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
            if (Utility.IsNotDBNull(dr["end_date"]))
                dtEndDate = Convert.ToDateTime(dr["end_date"]);
            else
                dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

            if (Utility.IsNotDBNull(dr["route_code"]))
                strRouteCode = dr["route_code"].ToString();
            if (Utility.IsNotDBNull(dr["route_type"]))
                strRouteType = dr["route_type"].ToString();

            if (Utility.IsNotDBNull(dr["payer_code"]))
                strPayerID = dr["payer_code"].ToString();
            if (Utility.IsNotDBNull(dr["payer_type"]))
                strPayerType = dr["payer_type"].ToString();

            if (Utility.IsNotDBNull(dr["origin_dc"]))
                strorigin_dc = dr["origin_dc"].ToString();
            if (Utility.IsNotDBNull(dr["destination_dc"]))
                strdestination_dc = dr["destination_dc"].ToString();

            if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
                strdelPath_origin_dc = dr["delPath_origin_dc"].ToString();
            if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
                strdelPath_destination_dc = dr["delPath_destination_dc"].ToString();

            if (Utility.IsNotDBNull(dr["zip_code"]))
                strzip_code = dr["zip_code"].ToString();
            if (Utility.IsNotDBNull(dr["state_code"]))
                strstate_code = dr["state_code"].ToString();

            foreach (ParameterFieldDefinition paramFldDef in paramFldDefs)
            {
                switch (paramFldDef.ParameterFieldName)
                {
                    case "prmAppid":
                        paramDVal.Value = m_strAppid;
                        break;
                    case "prmEnterpriseId":
                        paramDVal.Value = m_strEnterpriseid;
                        break;
                    case "prmUserId":
                        paramDVal.Value = strUserId;
                        break;
                    case "prmShipType":
                        paramDVal.Value = strShpPending;
                        break;
                    case "prmStartDate":
                        paramDVal.Value = dtStartDate;
                        break;
                    case "prmEndDate":
                        paramDVal.Value = dtEndDate;
                        break;
                    case "prmRouteCode":
                        paramDVal.Value = strRouteCode;
                        break;
                    case "prmRouteType":
                        paramDVal.Value = strRouteType;
                        break;
                    case "prmPayerID":
                        paramDVal.Value = strPayerID;
                        break;
                    case "prmPayerType":
                        paramDVal.Value = strPayerType;
                        break;
                    case "prmOriginDC":
                        paramDVal.Value = strorigin_dc;
                        break;
                    case "prmDestinationDC":
                        paramDVal.Value = strdestination_dc;
                        break;
                    case "prmDELOriginDC":
                        paramDVal.Value = strdelPath_origin_dc;
                        break;
                    case "prmDELDestinationDC":
                        paramDVal.Value = strdelPath_destination_dc;
                        break;
                    case "prmZipCode":
                        paramDVal.Value = strzip_code;
                        break;
                    case "prmStateCode":
                        paramDVal.Value = strstate_code;
                        break;
                    default:
                        continue;
                }

                paramVals = paramFldDef.CurrentValues;
                paramVals.Add(paramDVal);
                paramFldDef.ApplyCurrentValues(paramVals);
            }
            //in SetShipPendingPOD Get DataSet from GetShipmentPendingPOD
            //Assign DataSet result to rptSource
            TIES.WebUI.ShipmentPendingPOD dsShipmentPendingPOD = new TIES.WebUI.ShipmentPendingPOD();
            DataSet dsTemp = GetShipmentPendingPOD(m_strAppid, m_strEnterpriseid, dtStartDate.ToString("yyyy-MM-dd HH:mm:ss"), dtEndDate.ToString("yyyy-MM-dd HH:mm:ss"));

            int i = 0;
            int iRecCnt = dsTemp.Tables[0].Rows.Count;
            for (i = 0; i < iRecCnt; i++)
            {
                DataRow drEach = dsTemp.Tables[0].Rows[i];
                DataRow drPOD = dsShipmentPendingPOD.Tables["v_ShipmentPendingReport"].NewRow();

                if (Utility.IsNotDBNull(drEach["applicationid"]))
                    drPOD["applicationid"] = (string)drEach["applicationid"];
                if (Utility.IsNotDBNull(drEach["enterpriseid"]))
                    drPOD["enterpriseid"] = (string)drEach["enterpriseid"];
                if (Utility.IsNotDBNull(drEach["booking_no"]))
                    drPOD["booking_no"] = (int)drEach["booking_no"];
                if (Utility.IsNotDBNull(drEach["booking_datetime"]))
                    drPOD["booking_datetime"] = (DateTime)drEach["booking_datetime"];
                if (Utility.IsNotDBNull(drEach["consignment_no"]))
                    drPOD["consignment_no"] = (string)drEach["consignment_no"];
                if (Utility.IsNotDBNull(drEach["payer_name"]))
                    drPOD["payer_name"] = (string)drEach["payer_name"];
                if (Utility.IsNotDBNull(drEach["payerid"]))
                    drPOD["payerid"] = (string)drEach["payerid"];
                if (Utility.IsNotDBNull(drEach["sender_name"]))
                    drPOD["sender_name"] = (string)drEach["sender_name"];
                if (Utility.IsNotDBNull(drEach["recipient_name"]))
                    drPOD["recipient_name"] = (string)drEach["recipient_name"];
                if (Utility.IsNotDBNull(drEach["RecipientAddress"]))
                    drPOD["RecipientAddress"] = (string)drEach["RecipientAddress"];
                if (Utility.IsNotDBNull(drEach["RecipientZipCode"]))
                    drPOD["RecipientZipCode"] = (string)drEach["RecipientZipCode"];
                if (Utility.IsNotDBNull(drEach["RecipientCountry"]))
                    drPOD["RecipientCountry"] = (string)drEach["RecipientCountry"];
                if (Utility.IsNotDBNull(drEach["recipient_telephone"]))
                    drPOD["recipient_telephone"] = (string)drEach["recipient_telephone"];
                if (Utility.IsNotDBNull(drEach["est_delivery_datetime"]))
                    drPOD["est_delivery_datetime"] = (DateTime)drEach["est_delivery_datetime"];
                if (Utility.IsNotDBNull(drEach["origin_state_code"]))
                    drPOD["origin_state_code"] = (string)drEach["origin_state_code"];
                if (Utility.IsNotDBNull(drEach["destination_state_code"]))
                    drPOD["destination_state_code"] = (string)drEach["destination_state_code"];
                if (Utility.IsNotDBNull(drEach["origin_station"]))
                    drPOD["origin_station"] = (string)drEach["origin_station"];
                if (Utility.IsNotDBNull(drEach["destination_station"]))
                    drPOD["destination_station"] = (string)drEach["destination_station"];
                if (Utility.IsNotDBNull(drEach["Dlv_Route"]))
                    drPOD["Dlv_Route"] = (string)drEach["Dlv_Route"];
                if (Utility.IsNotDBNull(drEach["PickUP_Route"]))
                    drPOD["PickUP_Route"] = (string)drEach["PickUP_Route"];
                if (Utility.IsNotDBNull(drEach["enterprise_name"]))
                    drPOD["enterprise_name"] = (string)drEach["enterprise_name"];
                if (Utility.IsNotDBNull(drEach["Enterprise_Address"]))
                    drPOD["Enterprise_Address"] = (string)drEach["Enterprise_Address"];
                if (Utility.IsNotDBNull(drEach["remark"]))
                    drPOD["remark"] = (string)drEach["remark"];
                if (Utility.IsNotDBNull(drEach["last_status_code"]))
                    drPOD["last_status_code"] = (string)drEach["last_status_code"];
                if (Utility.IsNotDBNull(drEach["status_close"]))
                    drPOD["status_close"] = (string)drEach["status_close"];
                if (Utility.IsNotDBNull(drEach["departure_datetime"]))
                    drPOD["departure_datetime"] = (DateTime)drEach["departure_datetime"];
                if (Utility.IsNotDBNull(drEach["tracking_datetime"]))
                    drPOD["tracking_datetime"] = (DateTime)drEach["tracking_datetime"];
                if (Utility.IsNotDBNull(drEach["payer_type"]))
                    drPOD["payer_type"] = (string)drEach["payer_type"];
                if (Utility.IsNotDBNull(drEach["cod_amount"]))
                    drPOD["cod_amount"] = (decimal)drEach["cod_amount"];
                if (Utility.IsNotDBNull(drEach["ref_no"]))
                    drPOD["ref_no"] = (string)drEach["ref_no"];
                if (Utility.IsNotDBNull(drEach["state_name"]))
                    drPOD["state_name"] = (string)drEach["state_name"];
                if (Utility.IsNotDBNull(drEach["last_status_code_dly"]))
                    drPOD["last_status_code_dly"] = (string)drEach["last_status_code_dly"];

                if (Utility.IsNotDBNull(drPOD["applicationid"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.applicationidColumn.DefaultValue = drPOD["applicationid"];
                if (Utility.IsNotDBNull(drPOD["enterpriseid"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.enterpriseidColumn.DefaultValue = drPOD["enterpriseid"];
                if (Utility.IsNotDBNull(drPOD["booking_no"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.booking_noColumn.DefaultValue = drPOD["booking_no"];
                if (Utility.IsNotDBNull(drPOD["booking_datetime"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.booking_datetimeColumn.DefaultValue = drPOD["booking_datetime"];
                if (Utility.IsNotDBNull(drPOD["consignment_no"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.consignment_noColumn.DefaultValue = drPOD["consignment_no"];
                if (Utility.IsNotDBNull(drPOD["payer_name"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.payer_nameColumn.DefaultValue = drPOD["payer_name"];
                if (Utility.IsNotDBNull(drPOD["payerid"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.payeridColumn.DefaultValue = drPOD["payerid"];
                if (Utility.IsNotDBNull(drPOD["sender_name"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.sender_nameColumn.DefaultValue = drPOD["sender_name"];
                if (Utility.IsNotDBNull(drPOD["recipient_name"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.recipient_nameColumn.DefaultValue = drPOD["recipient_name"];
                if (Utility.IsNotDBNull(drPOD["RecipientAddress"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.RecipientAddressColumn.DefaultValue = drPOD["RecipientAddress"];
                if (Utility.IsNotDBNull(drPOD["RecipientZipCode"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.RecipientZipCodeColumn.DefaultValue = drPOD["RecipientZipCode"];
                if (Utility.IsNotDBNull(drPOD["RecipientCountry"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.RecipientCountryColumn.DefaultValue = drPOD["RecipientCountry"];
                if (Utility.IsNotDBNull(drPOD["recipient_telephone"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.recipient_telephoneColumn.DefaultValue = drPOD["recipient_telephone"];
                if (Utility.IsNotDBNull(drPOD["est_delivery_datetime"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.est_delivery_datetimeColumn.DefaultValue = drPOD["est_delivery_datetime"];
                if (Utility.IsNotDBNull(drPOD["origin_state_code"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.origin_state_codeColumn.DefaultValue = drPOD["origin_state_code"];
                if (Utility.IsNotDBNull(drPOD["destination_state_code"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.destination_state_codeColumn.DefaultValue = drPOD["destination_state_code"];
                if (Utility.IsNotDBNull(drPOD["origin_station"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.origin_stationColumn.DefaultValue = drPOD["origin_station"];
                if (Utility.IsNotDBNull(drPOD["destination_station"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.destination_stationColumn.DefaultValue = drPOD["destination_station"];
                if (Utility.IsNotDBNull(drPOD["Dlv_Route"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.Dlv_RouteColumn.DefaultValue = drPOD["Dlv_Route"];
                if (Utility.IsNotDBNull(drPOD["PickUP_Route"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.PickUP_RouteColumn.DefaultValue = drPOD["PickUP_Route"];
                if (Utility.IsNotDBNull(drPOD["enterprise_name"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.enterprise_nameColumn.DefaultValue = drPOD["enterprise_name"];
                if (Utility.IsNotDBNull(drPOD["Enterprise_Address"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.Enterprise_AddressColumn.DefaultValue = drPOD["Enterprise_Address"];
                if (Utility.IsNotDBNull(drPOD["remark"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.remarkColumn.DefaultValue = drPOD["remark"];
                if (Utility.IsNotDBNull(drPOD["last_status_code"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.last_status_codeColumn.DefaultValue = drPOD["last_status_code"];
                if (Utility.IsNotDBNull(drPOD["status_close"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.status_closeColumn.DefaultValue = drPOD["status_close"];
                if (Utility.IsNotDBNull(drPOD["departure_datetime"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.departure_datetimeColumn.DefaultValue = drPOD["departure_datetime"];
                if (Utility.IsNotDBNull(drPOD["tracking_datetime"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.tracking_datetimeColumn.DefaultValue = drPOD["tracking_datetime"];
                if (Utility.IsNotDBNull(drPOD["payer_type"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.payer_typeColumn.DefaultValue = drPOD["payer_type"];
                if (Utility.IsNotDBNull(drPOD["cod_amount"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.cod_amountColumn.DefaultValue = drPOD["cod_amount"];
                if (Utility.IsNotDBNull(drPOD["ref_no"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.ref_noColumn.DefaultValue = drPOD["ref_no"];
                if (Utility.IsNotDBNull(drPOD["state_name"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.state_nameColumn.DefaultValue = drPOD["state_name"];
                if (Utility.IsNotDBNull(drPOD["last_status_code_dly"]))
                    dsShipmentPendingPOD.v_ShipmentPendingReport.last_status_code_dlyColumn.DefaultValue = drPOD["last_status_code_dly"];

                dsShipmentPendingPOD.Tables["v_ShipmentPendingReport"].Rows.Add(drPOD);
            }
            rptSource.SetDataSource(dsShipmentPendingPOD);
            this.rptViewer.DisplayGroupTree = false;
            rptSource.PrintOptions.PaperSize = PaperSize.PaperA4;
            rptSource.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
            BindReport();

        }

        private void SetSQIDataSet(DataSet dsResult)
        {
            int i = 0;
            TIES.WebUI.SQIDs SQIXSD = new TIES.WebUI.SQIDs();
            int iRecCnt = dsResult.Tables[0].Rows.Count;
            ViewState["prmSumCons"] = "";
            int prmSumCons = 0;

            for (i = 0; i < iRecCnt; i++)
            {
                DataRow drEach = dsResult.Tables[0].Rows[i];
                DataRow drXSD = SQIXSD.Tables["SQIRPT"].NewRow();
                string strException_code = "NOT EXCEPT";
                string strAudit_status = "LATE AUDIT";

                if (Utility.IsNotDBNull(drEach["del_status"]))
                    drXSD["del_status"] = (string)drEach["del_status"];
                if (Utility.IsNotDBNull(drEach["audit_status"]))
                {
                    drXSD["audit_status"] = (string)drEach["audit_status"];
                    strAudit_status = (string)drEach["audit_status"];
                }
                if (Utility.IsNotDBNull(drEach["exception_code"]))
                {
                    drXSD["exception_code"] = (string)drEach["exception_code"];
                    strException_code = (string)drEach["exception_code"];
                }
                if (Utility.IsNotDBNull(drEach["exception_type"]))
                    drXSD["exception_type"] = (string)drEach["exception_type"];
                if (Utility.IsNotDBNull(drEach["exception_description"]))
                    drXSD["exception_description"] = (string)drEach["exception_description"];

                if (Utility.IsNotDBNull(drXSD["consignment_no"]))
                    SQIXSD.SQIRPT.consignment_noColumn.DefaultValue = drXSD["consignment_no"];
                if (Utility.IsNotDBNull(drXSD["del_status"]))
                    SQIXSD.SQIRPT.del_statusColumn.DefaultValue = drXSD["del_status"];
                if (Utility.IsNotDBNull(drXSD["audit_status"]))
                    SQIXSD.SQIRPT.audit_statusColumn.DefaultValue = drXSD["audit_status"];
                if (Utility.IsNotDBNull(drXSD["exception_code"]))
                    SQIXSD.SQIRPT.exception_codeColumn.DefaultValue = drXSD["exception_code"];
                if (Utility.IsNotDBNull(drXSD["exception_type"]))
                    SQIXSD.SQIRPT.exception_typeColumn.DefaultValue = drXSD["exception_type"];
                if (Utility.IsNotDBNull(drXSD["exception_description"]))
                    SQIXSD.SQIRPT.exception_descriptionColumn.DefaultValue = drXSD["exception_description"];

                if (Utility.IsNotDBNull(drEach["consignment_no"]))
                {
                    drXSD["consignment_no"] = Convert.ToInt32(drEach["consignment_no"]);
                    //drXSD["consignment_no"]= (string)(drEach["consignment_no"]);
                    //if(strException_code != "NOT EXCEP" && strAudit_status=="LATE AUDIT")
                    //{
                    if (strException_code != "NOT EXCEP")
                    {
                        prmSumCons = prmSumCons + Convert.ToInt32(drEach["consignment_no"]);
                    }
                    //}					
                }

                SQIXSD.Tables["SQIRPT"].Rows.Add(drXSD);
            }
            ViewState["prmSumCons"] = prmSumCons;
            rptSource.SetDataSource(SQIXSD);

            BindReport();
        }

        private void SetSQIReportParams()
        {
            //			//declare instance for parameterfields class
            //			ParameterValues paramvals = new ParameterValues();
            //			ParameterFieldDefinitions paramFlds; 
            //			ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
            //
            //			//getting parameter field values
            //			String strDateType="";
            //			DateTime dtStartDate, dtEndDate;
            //			
            //			DataRow dr = m_dsQuery.Tables[0].Rows[0];
            //
            //			strDateType = dr["tran_date"].ToString();
            //			if (Utility.IsNotDBNull(dr["start_date"])) 
            //				dtStartDate = (DateTime) dr["start_date"];
            //			else
            //				dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
            //			if (Utility.IsNotDBNull(dr["end_date"])) 
            //				dtEndDate = (DateTime) dr["end_date"];
            //			else
            //				dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
            //
            //			//get all parameter fields
            //			paramFlds = rptSource.DataDefinition.ParameterFields;
            //
            //			//passing parameter for parameter fields
            //			foreach(ParameterFieldDefinition paramFld in paramFlds)
            //			{
            //				switch(paramFld.ParameterFieldName)
            //				{
            //		
            //					case "DateType":
            //						paramDiscVal.Value = strDateType;
            //						break;
            //					case "FromDate":
            //						paramDiscVal.Value = dtStartDate;
            //						break;
            //					case "ToDate":
            //						paramDiscVal.Value = dtEndDate;
            //						break;
            //					default:
            //						continue;
            //				}
            //				paramvals = paramFld.CurrentValues;
            //				paramvals.Add(paramDiscVal);
            //				paramFld.ApplyCurrentValues(paramvals);
            //			}

            //declare instance for parameterfields class
            ParameterValues paramvals = new ParameterValues();
            ParameterFieldDefinitions paramFlds;
            ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();

            //getting parameter field values
            String strDateType = "";
            DateTime dtStartDate, dtEndDate;
            String strPayerType = "";
            String strPayerCode = "";
            String strOriginDC = "";
            String strRouteCode = "";
            String strPostalCode = "";
            String strDestinationDC = "";
            String strDestinationState = "";
            String strRouteType = "";
            String strPathOriginDC = "";
            String strPathDestinationDC = "";
            // Added by Gwang on 2May08
            String strServiceType = "";
            String strMaster_account = "";  //Jeab 18 Jul 2011

            DataRow dr = m_dsQuery.Tables[0].Rows[0];

            strDateType = dr["tran_date"].ToString();
            if (Utility.IsNotDBNull(dr["start_date"]))
                dtStartDate = (DateTime)dr["start_date"];
            else
                dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
            if (Utility.IsNotDBNull(dr["end_date"]))
                dtEndDate = (DateTime)dr["end_date"];
            else
                dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

            if (Utility.IsNotDBNull(dr["payer_type"]))
                strPayerType = dr["payer_type"].ToString();
            if (Utility.IsNotDBNull(dr["payer_code"]))
                strPayerCode = dr["payer_code"].ToString();

            if (Utility.IsNotDBNull(dr["route_type"]))
                strRouteType = dr["route_type"].ToString();
            if (Utility.IsNotDBNull(dr["route_code"]))
                strRouteCode = dr["route_code"].ToString();

            if (Utility.IsNotDBNull(dr["zip_code"]))
                strPostalCode = dr["zip_code"].ToString();
            if (Utility.IsNotDBNull(dr["state_code"]))
                strDestinationState = dr["state_code"].ToString();

            if (Utility.IsNotDBNull(dr["origin_dc"]))
                strOriginDC = dr["origin_dc"].ToString();
            if (Utility.IsNotDBNull(dr["destination_dc"]))
                strDestinationDC = dr["destination_dc"].ToString();

            if (Utility.IsNotDBNull(dr["delPath_origin_dc"]))
                strPathOriginDC = dr["delPath_origin_dc"].ToString();
            if (Utility.IsNotDBNull(dr["delPath_destination_dc"]))
                strPathDestinationDC = dr["delPath_destination_dc"].ToString();

            // Added by Gwang on 2May08
            if (Utility.IsNotDBNull(dr["service_type"]))
                strServiceType = dr["service_type"].ToString();

            //Jeab 18 Jul 2011
            if (Utility.IsNotDBNull(dr["master_account"]))
                strMaster_account = dr["master_account"].ToString();
            //Jeab 18 Jul 2011  =========> End

            //Modified by GwanG on 10March08
            //Query Header
            DataSet dsHeader = ReportDAL.GetHeaderCompName(m_strAppid, m_strEnterpriseid);
            String strCompName = "";
            String strCompAddr = "";
            String strCountry = "";
            if (dsHeader.Tables[0].Rows.Count > 0)
            {
                DataRow drHeader = dsHeader.Tables[0].Rows[0];
                //companyName,companyAddress, countryName
                if (Utility.IsNotDBNull(drHeader["companyName"]))
                    strCompName = drHeader["companyName"].ToString();
                if (Utility.IsNotDBNull(drHeader["companyAddress"]))
                    strCompAddr = drHeader["companyAddress"].ToString();
                if (Utility.IsNotDBNull(drHeader["countryName"]))
                    strCountry = drHeader["countryName"].ToString();

            }



            //get all parameter fields
            paramFlds = rptSource.DataDefinition.ParameterFields;

            //passing parameter for parameter fields
            foreach (ParameterFieldDefinition paramFld in paramFlds)
            {
                switch (paramFld.ParameterFieldName)
                {
                    case "DateType":
                        paramDiscVal.Value = strDateType;
                        break;
                    case "FromDate":
                        paramDiscVal.Value = dtStartDate;
                        break;
                    case "ToDate":
                        paramDiscVal.Value = dtEndDate;
                        break;
                    case "PayerType":
                        paramDiscVal.Value = strPayerType;
                        break;
                    case "PayerCode":
                        paramDiscVal.Value = strPayerCode;
                        break;
                    case "OriginDC":
                        paramDiscVal.Value = strOriginDC;
                        break;
                    case "RouteCode":
                        paramDiscVal.Value = strRouteCode;
                        break;
                    case "PostalCode":
                        paramDiscVal.Value = strPostalCode;
                        break;
                    case "DestinationDC":
                        paramDiscVal.Value = strDestinationDC;
                        break;
                    case "ApplicationID":
                        paramDiscVal.Value = m_strAppid;
                        break;
                    case "DestinationState":
                        paramDiscVal.Value = strDestinationState;
                        break;
                    case "RouteType":
                        paramDiscVal.Value = strRouteType;
                        break;
                    case "PathOriginDC":
                        paramDiscVal.Value = strPathOriginDC;
                        break;
                    case "PathDestinationDC":
                        paramDiscVal.Value = strPathDestinationDC;
                        break;
                    case "prmUserId":
                        paramDiscVal.Value = strUserId;
                        break;
                    case "EnterpriseID":
                        paramDiscVal.Value = m_strEnterpriseid;
                        break;
                    case "prmCompName":
                        paramDiscVal.Value = strCompName;
                        break;
                    case "prmCompAddr":
                        paramDiscVal.Value = strCompAddr;
                        break;
                    case "prmCountryName":
                        paramDiscVal.Value = strCountry;
                        break;
                    // Added by Gwang on 2May08
                    case "ServiceType":
                        paramDiscVal.Value = strServiceType;
                        break;
                    //Jeab 18 Jul 2011
                    case "MasterAccount":
                        paramDiscVal.Value = strMaster_account;
                        break;
                    //Chai 03 Apr 2012
                    case "prmSumCons":
                        if (ViewState["prmSumCons"] != null)
                        {
                            paramDiscVal.Value = ViewState["prmSumCons"].ToString();
                        }
                        else
                        {
                            paramDiscVal.Value = "0";
                        }

                        break;
                    default:
                        continue;
                }
                paramvals = paramFld.CurrentValues;
                paramvals.Add(paramDiscVal);
                paramFld.ApplyCurrentValues(paramvals);
            }

        }


        private void btnShowGrpTree_Click(object sender, System.EventArgs e)
        {
            if (!bShowGroupTree)
            {
                if (!utility.GetUserCulture().ToUpper().Equals("EN-US"))
                {
                    btnShowGrpTree.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Hide Group Tree", utility.GetUserCulture());
                }
                else
                {
                    btnShowGrpTree.Text = "Hide Group Tree";
                }
                bShowGroupTree = true;
                rptViewer.DisplayGroupTree = bShowGroupTree;
                ViewState["ShowGroupTree"] = bShowGroupTree;
            }
            else
            {
                if (!utility.GetUserCulture().ToUpper().Equals("EN-US"))
                {
                    btnShowGrpTree.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Show Group Tree", utility.GetUserCulture());
                }
                else
                {
                    btnShowGrpTree.Text = "Show Group Tree";
                }
                bShowGroupTree = false;
                rptViewer.DisplayGroupTree = bShowGroupTree;
                ViewState["ShowGroupTree"] = bShowGroupTree;
            }
            /*if (btnShowGrpTree.Text=="Show Group Tree")
			{
				btnShowGrpTree.Text="Hide Group Tree";
				bShowGroupTree=true;
				rptViewer.DisplayGroupTree=bShowGroupTree;				
				ViewState["ShowGroupTree"]=bShowGroupTree;
			}
			else
			{
				btnShowGrpTree.Text="Show Group Tree";
				bShowGroupTree=false;
				rptViewer.DisplayGroupTree=bShowGroupTree;				
				ViewState["ShowGroupTree"]=bShowGroupTree;
			}*/
        }

        private void btnMoveFirst_Click(object sender, System.EventArgs e)
        {
            rptViewer.ShowFirstPage();
        }

        public string vsGoto
        {
            get
            {
                if (ViewState["Goto"] != null)
                    return (string)ViewState["Goto"];
                else
                    return "1";
            }
            set
            {
                ViewState["Goto"] = value;
            }
        }

        private void btnMovePrevious_Click(object sender, System.EventArgs e)
        {
            if (txtGoTo.Text.Trim() != "")
            {
                //rptViewer.ShowNthPage(System.Convert.ToInt32(txtGoTo.Text)+1);
                ViewState["Goto"] = Int32.Parse(ViewState["Goto"].ToString()) - 1;
                rptViewer.ShowNthPage(System.Convert.ToInt32(ViewState["Goto"]));
                txtGoTo.Text = "";
            }
            else
            {
                if (vsGoto != "1")
                    vsGoto = (Int32.Parse(vsGoto) - 1).ToString();
                rptViewer.ShowNthPage(System.Convert.ToInt32(vsGoto));
            }
            //rptViewer.ShowPreviousPage();
        }

        private void btnMoveNext_Click(object sender, System.EventArgs e)
        {
            if (txtGoTo.Text.Trim() != "")
            {
                //rptViewer.ShowNthPage(System.Convert.ToInt32(txtGoTo.Text)-1);
                vsGoto = (Int32.Parse(vsGoto) + 1).ToString();
                rptViewer.ShowNthPage(System.Convert.ToInt32(vsGoto));
                txtGoTo.Text = "";
            }
            else
            {
                vsGoto = (Int32.Parse(vsGoto) + 1).ToString();
                rptViewer.ShowNthPage(System.Convert.ToInt32(vsGoto));
            }
            //rptViewer.ShowNextPage();
        }

        private void btnMoveLast_Click(object sender, System.EventArgs e)
        {
            rptViewer.ShowLastPage();
        }

        public void LoadExportFormatList()
        {
            ddbExport.Items.Clear();
            ArrayList systemCodes = Utility.GetCodeValues(m_strAppid, utility.GetUserCulture(), "export", CodeValueType.StringValue);
            foreach (SystemCode sysCode in systemCodes)
            {
                ListItem lstItem = new ListItem();
                lstItem.Text = sysCode.Text;
                lstItem.Value = sysCode.StringValue;
                ddbExport.Items.Add(lstItem);
            }
        }

        private void btnExport_Click(object sender, System.EventArgs e)
        {
            ExportOptions ExportOptions = new ExportOptions();
            DiskFileDestinationOptions DiskFileDstOptions = new DiskFileDestinationOptions();
            String strExportFile = null;

            if (ddbExport.SelectedItem.Value.Trim() == "Formats:")
            {
                return;
            }

            string strExportFolder = System.Configuration.ConfigurationSettings.AppSettings["ReportExportFolder"];
            strExportFolder = Server.MapPath(strExportFolder);
            if (!System.IO.Directory.Exists(strExportFolder))
            {
                lblErrorMesssage.Text = "Export folder not found. Please create the folder :'" + strExportFolder + "' and export";
                return;
            }

            ///**********IMPORTANT:Get the Export File directory from Web.config, Permissions must be given for
            ///**********"aspnet" user on the Folder read from "ReportExportFolder"
            strExportFile = strExportFolder + "\\" + Session.SessionID.ToString();
            ///**********strExportFile = "C:\\" + Session.SessionID.ToString();

            DiskFileDstOptions.DiskFileName = strExportFile;
            ExportOptions = rptSource.ExportOptions;
            ExportOptions.DestinationOptions = DiskFileDstOptions;
            ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;

            switch (ddbExport.SelectedItem.Value.Trim().ToString())
            {
                case ".pdf":
                    ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    ExportOptions.FormatOptions = new PdfRtfWordFormatOptions();
                    break;
                case ".doc":
                    ExportOptions.ExportFormatType = ExportFormatType.WordForWindows;
                    ExportOptions.FormatOptions = new PdfRtfWordFormatOptions();
                    break;
                case ".rtf":
                    ExportOptions.ExportFormatType = ExportFormatType.RichText;
                    ExportOptions.FormatOptions = new PdfRtfWordFormatOptions();
                    break;
                case ".xls":
                    if (ddbExport.SelectedItem.Text.IndexOf("Data", 1) > 0)
                    {
                        ExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
                    }
                    else
                    {
                        ExportOptions.ExportFormatType = ExportFormatType.Excel;
                    }
                    ExportOptions.FormatOptions = new ExcelFormatOptions();
                    break;
                //				case ".rpt":
                //					ExportOptions.ExportFormatType = ExportFormatType.CrystalReport;	
                //					ExportOptions.FormatOptions=new PdfRtfWordFormatOptions();
                //					break;
                //				case ".htm":
                //					ExportOptions.ExportFormatType = ExportFormatType.HTML40;										
                //					HTMLFormatOptions htmlOpt=new HTMLFormatOptions();
                //					htmlOpt.HTMLBaseFolderName="C:\\Crystal\\";
                //                    htmlOpt.HTMLFileName=strExportFile="C:\\Crystal\\"+ Session.SessionID.ToString() + ddbExport.SelectedItem.Value.Trim();
                //					htmlOpt.HTMLHasPageNavigator = true;
                //					ExportOptions.FormatOptions=htmlOpt;
                //					break;

                default:
                    ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    ExportOptions.FormatOptions = new PdfRtfWordFormatOptions();
                    break;
            }

            rptSource.Export();
            Response.ClearContent();
            Response.ClearHeaders();

            switch (ddbExport.SelectedItem.Value.Trim().ToString())
            {
                case ".pdf":
                    Response.ContentType = "application/pdf";
                    break;
                case ".doc":
                case ".rtf":
                    Response.ContentType = "application/msword";
                    break;
                case ".xls":
                    Response.ContentType = "application/vnd.ms-excel";
                    break;
                case ".htm":
                    Response.ContentType = "text/html";
                    break;
            }

            Response.WriteFile(strExportFile);
            Response.Flush();
            //Response.Close();

            //Delete the generated File from the DISK
            System.IO.File.Delete(strExportFile);
            //To Print 3 Copies
            //rptSource.PrintToPrinter(3, true, 1, 1);	
        }

        private void btnExportExcel_Click(object sender, System.EventArgs e)
        {
            //try
            //{
            double total = 0;
            var dt = new System.Data.DataTable("Table1");
            dt.Columns.Add("DATE");
            dt.Columns.Add("CON#");
            dt.Columns.Add("COST CENTER");
            dt.Columns.Add("SERVICE");
            dt.Columns.Add("ORIGIN");
            dt.Columns.Add("DEST");
            dt.Columns.Add("WEIGHT");
            dt.Columns.Add("RATE");
            dt.Columns.Add("BASIC CHARGE");
            dt.Columns.Add("FREIGHT CHARGE");
            dt.Columns.Add("SSC");

            dt.Columns.Add("Agency");
            dt.Columns.Add("Disbursements");

            dt.Columns.Add("SUB-TOTAL");
            dt.Columns.Add("GST");
            dt.Columns.Add("TOTAL");
       
            //dt.Columns.Add("AWB Number"); 
            //dt.Columns.Add("TotalInvoice");

            for (int i = 0; i < dsFreight.Tables[0].Rows.Count; i++)
            {
                dt.Rows.Add(Convert.ToDateTime(
                            dsFreight.Tables[0].Rows[i]["shpt_manifest_datetime"]).ToString("dd-MM-yyyy")
                            , dsFreight.Tables[0].Rows[i]["consignment_no"].ToString()
                            , dsFreight.Tables[0].Rows[i]["cost_centre"].ToString()
                            , dsFreight.Tables[0].Rows[i]["service_code"].ToString()
                            , dsFreight.Tables[0].Rows[i]["origin_station"].ToString()
                            , dsFreight.Tables[0].Rows[i]["destination_station"].ToString()
                            , dsFreight.Tables[0].Rows[i]["tot_act_wt"].ToString()
                            , dsFreight.Tables[0].Rows[i]["rate"].ToString()
                            , dsFreight.Tables[0].Rows[i]["basic_charge"].ToString()
                            , dsFreight.Tables[0].Rows[i]["tot_freight_charge"].ToString()
                            , dsFreight.Tables[0].Rows[i]["tot_vas_surcharge"].ToString()

                            , dsFreight.Tables[0].Rows[i]["AgencyFees"].ToString()
                            , dsFreight.Tables[0].Rows[i]["Disbursements"].ToString()

                            , dsFreight.Tables[0].Rows[i]["total_rated_amount"].ToString()
                            , dsFreight.Tables[0].Rows[i]["tax"].ToString()
                            , dsFreight.Tables[0].Rows[i]["invoice_amt"].ToString()
                            
                            //, dsFreight.Tables[0].Rows[i]["MasterAWBNumber"].ToString()
                            //, dsFreight.Tables[0].Rows[i]["TotalInvoice"].ToString()

                            );

                if (dsFreight.Tables[0].Rows[i]["invoice_amt"].ToString() != "")
                    total += double.Parse(dsFreight.Tables[0].Rows[i]["invoice_amt"].ToString());
            }

            dt.Rows.Add("", "", "", "", "", "", "", "", "", "", "", "", "", "", "Total", total);

            XLWorkbook workbook = new XLWorkbook();
            var ws = workbook.Worksheets.Add(dt, "Freight Summary");

            ws.Table("Table1").ShowAutoFilter = false;
            ws.Table("Table1").Theme = XLTableTheme.None;
            ws.Range("A1:Q1").Style.Font.Bold = true;

            string RptName = Server.UrlEncode("FreightSummary.xlsx");
            MemoryStream stream = GetStream(workbook);

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=" + RptName);
            Response.ContentType = "application/vnd.ms-excel";
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }

        private void SetSQIVersusDataSet(DataSet dsResult)
        {
            int i = 0;
            TIES.WebUI.Versus SQIXSD = new TIES.WebUI.Versus();
            int iRecCnt = dsResult.Tables[0].Rows.Count;
            for (i = 0; i < iRecCnt; i++)
            {
                DataRow drEach = dsResult.Tables[0].Rows[i];
                DataRow drXSD = SQIXSD.Tables["SQLVersus"].NewRow();

                if (Utility.IsNotDBNull(drEach["consignment_no"]))
                    drXSD["consignment_no"] = (string)drEach["consignment_no"];
                if (Utility.IsNotDBNull(drEach["sCode1"]))
                    drXSD["sCode1"] = (string)drEach["sCode1"];
                if (Utility.IsNotDBNull(drEach["sCode2"]))
                    drXSD["sCode2"] = (string)drEach["sCode2"];
                if (Utility.IsNotDBNull(drEach["sCode3"]))
                    drXSD["sCode3"] = (string)drEach["sCode3"];
                if (Utility.IsNotDBNull(drEach["last_userid"]))
                    drXSD["last_userid"] = (string)drEach["last_userid"];

                if (Utility.IsNotDBNull(drEach["sCode1DT"]))
                    drXSD["sCode1DT"] = (string)drEach["sCode1DT"].ToString();
                if (Utility.IsNotDBNull(drEach["sCode2DT"]))
                    drXSD["sCode2DT"] = (string)drEach["sCode2DT"].ToString();
                if (Utility.IsNotDBNull(drEach["sCode3DT"]))
                    drXSD["sCode3DT"] = (string)drEach["sCode3DT"].ToString();
                if (Utility.IsNotDBNull(drEach["TotalTime"]))
                    drXSD["TotalTime"] = (string)drEach["TotalTime"].ToString();

                if (Utility.IsNotDBNull(drEach["booking_no"]))
                    drXSD["booking_no"] = (string)drEach["booking_no"].ToString();
                if (Utility.IsNotDBNull(drEach["ref_no"]))
                    drXSD["ref_no"] = (string)drEach["ref_no"];
                if (Utility.IsNotDBNull(drEach["payerid"]))
                    drXSD["paerid"] = (string)drEach["payerid"];
                if (Utility.IsNotDBNull(drEach["origin_station"]))
                    drXSD["org_dc"] = (string)drEach["origin_station"];
                if (Utility.IsNotDBNull(drEach["destination_station"]))
                    drXSD["des_dc"] = (string)drEach["destination_station"];
                if (Utility.IsNotDBNull(drEach["route_code"]))
                    drXSD["del_route"] = (string)drEach["route_code"];
                if (Utility.IsNotDBNull(drEach["shpt_manifest_datetime"]))
                    drXSD["manifest_date"] = (string)drEach["shpt_manifest_datetime"].ToString();
                if (Utility.IsNotDBNull(drEach["act_pickup_datetime"]))
                    drXSD["pickup_date"] = (string)drEach["act_pickup_datetime"].ToString();
                if (Utility.IsNotDBNull(drEach["last_updated_date"]))
                    drXSD["last_updated_date"] = (string)drEach["last_updated_date"].ToString();
                SQIXSD.Tables["SQLVersus"].Rows.Add(drXSD);

                /*
				if (Utility.IsNotDBNull(drXSD["consignment_no"]))
					SQIXSD.SQLVersus.consignment_noColumn.DefaultValue = drXSD["consignment_no"];
				if (Utility.IsNotDBNull(drXSD["sCode1"]))
					SQIXSD.SQLVersus.consignment_noColumn.DefaultValue = drXSD["sCode1"];
				if (Utility.IsNotDBNull(drXSD["sCode2"]))
					SQIXSD.SQLVersus.consignment_noColumn.DefaultValue = drXSD["sCode2"];
				if (Utility.IsNotDBNull(drXSD["sCode3"]))
					SQIXSD.SQLVersus.consignment_noColumn.DefaultValue = drXSD["sCode3"];
				if (Utility.IsNotDBNull(drXSD["sCode1DT"]))
					SQIXSD.SQLVersus.consignment_noColumn.DefaultValue = drXSD["sCode1DT"];
				if (Utility.IsNotDBNull(drXSD["sCode2DT"]))
					SQIXSD.SQLVersus.consignment_noColumn.DefaultValue = drXSD["sCode2DT"];
				if (Utility.IsNotDBNull(drXSD["sCode3DT"]))
					SQIXSD.SQLVersus.consignment_noColumn.DefaultValue = drXSD["sCode3DT"];
				if (Utility.IsNotDBNull(drXSD["TotalTime"]))
					SQIXSD.SQLVersus.consignment_noColumn.DefaultValue = drXSD["TotalTime"];

				SQIXSD.Tables["SQLVersus"].Rows.Add(drXSD);
				*/
            }
            rptSource.SetDataSource(SQIXSD);

            BindReport();
        }

        //End Modified By Tom
        //Modified By Tom
        private void SetSQIVersusReportParams()
        {
            //declare instance for parameterfields class
            ParameterValues paramvals = new ParameterValues();
            ParameterFieldDefinitions paramFlds;
            ParameterDiscreteValue paramDiscVal = new ParameterDiscreteValue();
            rptViewer.Width = 1500;

            //getting parameter field values
            String strDateType = "";
            DateTime dtStartDate, dtEndDate;
            String strPayerCode = "";
            String strOriginDC = "";
            String strRouteCode = "";
            String strPostalCode = "";
            String strDestinationDC = "";
            String strDestinationState = "";
            String strStatus1 = "";
            String strStatus2 = "";
            String strStatus3 = "";
            String strStatusType = "";

            DataRow dr = m_dsQuery.Tables[0].Rows[0];

            strDateType = dr["tran_date"].ToString();
            if (Utility.IsNotDBNull(dr["start_date"]))
                dtStartDate = (DateTime)dr["start_date"];
            else
                dtStartDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
            if (Utility.IsNotDBNull(dr["end_date"]))
                dtEndDate = (DateTime)dr["end_date"];
            else
                dtEndDate = System.DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);

            if (Utility.IsNotDBNull(dr["origin_dc"]))
                strOriginDC = dr["origin_dc"].ToString();
            if (Utility.IsNotDBNull(dr["destination_dc"]))
                strDestinationDC = dr["destination_dc"].ToString();

            if (Utility.IsNotDBNull(dr["state_code1"]))
                strStatus1 = dr["state_code1"].ToString();
            if (Utility.IsNotDBNull(dr["state_code2"]))
                strStatus2 = dr["state_code2"].ToString();
            if (Utility.IsNotDBNull(dr["state_code3"]))
                strStatus3 = dr["state_code3"].ToString();

            if (Utility.IsNotDBNull(dr["versus_type"]))
                strStatusType = dr["versus_type"].ToString();

            //Modified by GwanG on 10March08
            //Query Header
            DataSet dsHeader = ReportDAL.GetHeaderCompName(m_strAppid, m_strEnterpriseid);
            String strCompName = "";
            String strCompAddr = "";
            String strCountry = "";
            if (dsHeader.Tables[0].Rows.Count > 0)
            {
                DataRow drHeader = dsHeader.Tables[0].Rows[0];
                //companyName,companyAddress, countryName
                if (Utility.IsNotDBNull(drHeader["companyName"]))
                    strCompName = drHeader["companyName"].ToString();
                if (Utility.IsNotDBNull(drHeader["companyAddress"]))
                    strCompAddr = drHeader["companyAddress"].ToString();
                if (Utility.IsNotDBNull(drHeader["countryName"]))
                    strCountry = drHeader["countryName"].ToString();

            }



            //get all parameter fields
            paramFlds = rptSource.DataDefinition.ParameterFields;

            //passing parameter for parameter fields
            foreach (ParameterFieldDefinition paramFld in paramFlds)
            {
                switch (paramFld.ParameterFieldName)
                {

                    case "PayerCode":
                        paramDiscVal.Value = strPayerCode;
                        break;
                    case "OriginDC":
                        paramDiscVal.Value = strOriginDC;
                        break;
                    case "RouteCode":
                        paramDiscVal.Value = strRouteCode;
                        break;
                    case "PostalCode":
                        paramDiscVal.Value = strPostalCode;
                        break;
                    case "DestinationDC":
                        paramDiscVal.Value = strDestinationDC;
                        break;
                    case "DestinationState":
                        paramDiscVal.Value = strDestinationState;
                        break;
                    case "DateType":
                        paramDiscVal.Value = strDateType;
                        break;
                    case "FromDate":
                        paramDiscVal.Value = dtStartDate;
                        break;
                    case "ToDate":
                        paramDiscVal.Value = dtEndDate;
                        break;
                    case "StatusCode1":
                        paramDiscVal.Value = strStatus1;
                        break;
                    case "StatusCode2":
                        paramDiscVal.Value = strStatus2;
                        break;
                    case "StatusCode3":
                        paramDiscVal.Value = strStatus3;
                        break;
                    case "prmCompName":
                        paramDiscVal.Value = strCompName;
                        break;
                    case "prmCompAddr":
                        paramDiscVal.Value = strCompAddr;
                        break;
                    case "prmCountryName":
                        paramDiscVal.Value = strCountry;
                        break;
                    case "StatusType":
                        paramDiscVal.Value = strStatusType;
                        break;
                    case "prmUserID":
                        paramDiscVal.Value = strUserId;
                        break;
                    default:
                        continue;
                }
                paramvals = paramFld.CurrentValues;
                paramvals.Add(paramDiscVal);
                paramFld.ApplyCurrentValues(paramvals);
            }

        }

        //End Modified By Tom
        private void btnSearch_Click(object sender, System.EventArgs e)
        {
            String strSearchText = txtTextToSearch.Text.Trim();
            if (strSearchText == "")
            {
                return;
            }
            rptViewer.SearchForText(strSearchText, SearchDirection.Forward);
        }

        private void ddbZoom_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (System.Convert.ToInt32(ddbZoom.SelectedItem.Value) < 125)
            {
                rptViewer.Width = 920;
                rptViewer.Height = 1200;
            }
            else if (System.Convert.ToInt32(ddbZoom.SelectedItem.Value) == 125)
            {
                rptViewer.Width = 1200;
                rptViewer.Height = 1500;
            }
            else if (System.Convert.ToInt32(ddbZoom.SelectedItem.Value) == 150)
            {
                rptViewer.Width = 1400;
                rptViewer.Height = 1800;
            }
            else if (System.Convert.ToInt32(ddbZoom.SelectedItem.Value) == 175)
            {
                rptViewer.Width = 1600;
                rptViewer.Height = 2000;
            }
            else if (System.Convert.ToInt32(ddbZoom.SelectedItem.Value) == 200)
            {
                rptViewer.Width = 1800;
                rptViewer.Height = 2200;
            }
            rptViewer.Zoom(System.Convert.ToInt32(ddbZoom.SelectedItem.Value));
        }

        private void ddbExport_SelectedIndexChanged(object sender, System.EventArgs e)
        {

        }

        private DataSet GetShipmentPendingPOD(String strAppID, String strEnterpriseID, String strStartDate, String strEndDate)
        {

            DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
            System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
            storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid", strAppID));
            storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid", strEnterpriseID));
            storedParams.Add(new System.Data.SqlClient.SqlParameter("@StartDate", strStartDate));
            storedParams.Add(new System.Data.SqlClient.SqlParameter("@EndDate", strEndDate));

            DataSet ds = (DataSet)dbCon.ExecuteProcedure("ShipmentPendingPODReport", storedParams, ReturnType.DataSetType);

            return ds;
        }

        private void SetReportDataSource(ReportDocument oReport, DataSet dsData, string strTableName)

        {

            DataSet dsNew = dsData.Copy();



            // Remove primary key info. CR9 does not appreciate this information!!!

            foreach (DataTable dataTable in dsNew.Tables)

            {
                dataTable.TableName = strTableName;
                foreach (DataColumn dataCol in dataTable.PrimaryKey)

                {

                    dataCol.AutoIncrement = false;

                }

                dataTable.PrimaryKey = null;

            }


            // Now assign the dataset to all tables in the main report

            foreach (CrystalDecisions.CrystalReports.Engine.Table oTable in oReport.Database.Tables)
            {
                oTable.SetDataSource(dsNew);

            }



            // Now loop through all the sections and its objects to do the same for the subreports

            foreach (CrystalDecisions.CrystalReports.Engine.Section crSection in oReport.ReportDefinition.Sections)

            {

                // In each section we need to loop through all the reporting objects

                foreach (CrystalDecisions.CrystalReports.Engine.ReportObject crObject in crSection.ReportObjects)

                {

                    if (crObject.Kind == ReportObjectKind.SubreportObject)

                    {

                        SubreportObject crSubReport = (SubreportObject)crObject;

                        ReportDocument crSubDoc = crSubReport.OpenSubreport(crSubReport.SubreportName);



                        foreach (CrystalDecisions.CrystalReports.Engine.Table oTable in crSubDoc.Database.Tables)

                        {

                            oTable.SetDataSource(dsNew);

                        }

                    }

                }

            }

        }

    }
}
