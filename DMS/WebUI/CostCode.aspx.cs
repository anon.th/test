using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for CostCode.
	/// </summary>
	public class CostCode : BasePage
	{
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.DataGrid dgCostCode;
		protected System.Web.UI.WebControls.ValidationSummary Pagevalid;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Label Label1;
		string m_strAppID=null;
		string m_strEnterpriseID=null;
		//Utility utility=null;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		SessionDS m_sdsCostCode = null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			if(!IsPostBack)
			{
				QueryMode();
					  
			}
			else
			{
				m_sdsCostCode = (SessionDS) Session["SESSION_DS1"];
			}
			PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}
		public void BindCCGrid()
		{
			dgCostCode.VirtualItemCount = System.Convert.ToInt32(m_sdsCostCode.QueryResultMaxSize);
			dgCostCode.DataSource = m_sdsCostCode.ds;
			dgCostCode.DataBind(); 
			Session["SESSION_DS1"] = m_sdsCostCode;
		}
		public void QueryMode()
		{
			ViewState["CCOperation"]=Operation.None;
			ViewState["CCMode"]=ScreenMode.Query;
			 	
			btnQuery.Enabled=true;
			btnExecuteQuery.Enabled=true;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);

			m_sdsCostCode = SysDataMgrDAL.GetEmptyCostCode() ;  

			m_sdsCostCode.DataSetRecSize = 1;
			AddRow(); 
			Session["SESSION_DS1"] = m_sdsCostCode;
			BindCCGrid();
			lblErrorMessage.Text="";
			//LoadDeliveryPath();

			EditHRow(true);
			dgCostCode.Columns[0].Visible=false;
			dgCostCode.Columns[1].Visible=false;
		}

		
		private void AddRow()
		{
			SysDataMgrDAL.AddNewRowInCostCodeDS(ref m_sdsCostCode);
			BindCCGrid();
		}
		private void EditHRow(bool bItemIndex)
		{
			foreach (DataGridItem item in dgCostCode.Items)
			{ 
				if (bItemIndex) 
				{
					dgCostCode.EditItemIndex = item.ItemIndex; }
				else 
				{
					dgCostCode.EditItemIndex = -1; }
			}
			dgCostCode.DataBind();
		}

		private void GetChangedRow(DataGridItem item, int rowIndex)
		{
			msTextBox txtCostCode = (msTextBox)item.FindControl("txtCostCode");
			TextBox txtCCDescp = (TextBox)item.FindControl("txtCCDescp");
			TextBox txtCostType = (TextBox)item.FindControl("txtCostType");
			
				
			string strCostCode = txtCostCode.Text.ToString();
			string strCCDescp = txtCCDescp.Text.ToString();
			string strCostType = txtCostType.Text.ToString();
			
			DataRow dr = m_sdsCostCode.ds.Tables[0].Rows[rowIndex];
			dr[0] = strCostCode;
			dr[1] = strCCDescp;
			dr[2] = strCostType;
			Session["SESSION_DS1"] = m_sdsCostCode;
			
		}
		protected void  dgCostCode_Edit(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			if((int)ViewState["CCMode"] == (int)ScreenMode.Insert && (int) ViewState["CCOperation"] == (int)Operation.Insert && dgCostCode.EditItemIndex > 0)
			{
				m_sdsCostCode.ds.Tables[0].Rows.RemoveAt(dgCostCode.EditItemIndex);
				dgCostCode.CurrentPageIndex = 0;
			}
			dgCostCode.EditItemIndex = e.Item.ItemIndex;
			ViewState["CCOperation"] = Operation.Update;
			BindCCGrid();
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			dgCostCode .CurrentPageIndex = 0;
			QueryMode();
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			
			if (btnExecuteQuery.Enabled == true)
			{
				btnExecuteQuery.Enabled = false;
			}
			if((int)ViewState["CCMode"] != (int)ScreenMode.Insert || m_sdsCostCode.ds.Tables[0].Rows.Count >= dgCostCode.PageSize)
			{
				m_sdsCostCode = SysDataMgrDAL.GetEmptyCostCode();
				dgCostCode.EditItemIndex = m_sdsCostCode.ds.Tables[0].Rows.Count - 1;
				dgCostCode.CurrentPageIndex = 0;
				AddRow();
				
			}
			else
			{
				AddRow();
			}
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,false,m_moduleAccessRights);
			EditHRow(false);
			ViewState["CCMode"] = ScreenMode.Insert;
			ViewState["CCOperation"] = Operation.Insert;
			dgCostCode.Columns[0].Visible=true;
			dgCostCode.Columns[1].Visible=true;
			dgCostCode.EditItemIndex = m_sdsCostCode.ds.Tables[0].Rows.Count - 1;
			dgCostCode.CurrentPageIndex = 0;
			BindCCGrid();
			getPageControls(Page);
		}
		
		protected void dgCostCode_ItemDataBound(object sender, DataGridItemEventArgs e)
		{

			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsCostCode.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}
			
			if ((int)ViewState["CCOperation"] != (int)Operation.Insert && (!btnExecuteQuery.Enabled))
			{
				msTextBox txtCostCode = (msTextBox)e.Item.FindControl("txtCostCode");
				if(txtCostCode != null ) 
				{
					txtCostCode.Enabled = false;
				}
			}
			if ((int)ViewState["CCMode"] == (int)ScreenMode.Insert && (int)ViewState["CCOperation"] == (int)Operation.Insert)
			{
				e.Item.Cells[1].Enabled=false;
			}
			if((int)ViewState["CCMode"]==(int)ScreenMode.Insert)
			{
				e.Item.Cells[1].Enabled = false;	
			}
			else
			{
				e.Item.Cells[1].Enabled = true;

			}
		}
		protected void dgCostCode_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			int rowIndex = e.Item.ItemIndex;
			if ((int)ViewState["CCMode"] == (int)ScreenMode.Insert && (int)ViewState["CCOperation"] == (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				m_sdsCostCode.ds.Tables[0].Rows.RemoveAt(rowIndex);
				
			}
			ViewState["CCOperation"] = Operation.None;
			dgCostCode.EditItemIndex = -1;
			BindCCGrid();
			
		}
		protected void dgCostCode_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgCostCode.CurrentPageIndex = e.NewPageIndex;
			RetreiveSelectedPage();
		}
		private void RetreiveSelectedPage()
		{
			int iStartRow = dgCostCode.CurrentPageIndex * dgCostCode.PageSize;
			DataSet dsQuery = (DataSet)Session["QUERY_DS"];
			m_sdsCostCode = SysDataMgrDAL.GetCostCodeDS(utility.GetAppID(), utility.GetEnterpriseID(), iStartRow, dgCostCode.PageSize, dsQuery);
			int iPageCnt = Convert.ToInt32((m_sdsCostCode.QueryResultMaxSize - 1))/dgCostCode.PageSize;
			if(iPageCnt < dgCostCode .CurrentPageIndex)
			{
				dgCostCode.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			
			dgCostCode.SelectedIndex = -1;
			dgCostCode.EditItemIndex = -1;

			BindCCGrid();
			Session["SESSION_DS1"] = m_sdsCostCode;
		}
		protected void dgCostCode_Delete(object sender, DataGridCommandEventArgs e)
		{
			if (!btnExecuteQuery.Enabled)
			{
				if ((int)ViewState["CCOperation"] == (int)Operation.Update)
				{
					dgCostCode.EditItemIndex = -1;
				}
				BindCCGrid ();

				int rowIndex = e.Item.ItemIndex;
				m_sdsCostCode  = (SessionDS)Session["SESSION_DS1"];

				try
				{
					// delete from table
					int iRowsDeleted = SysDataMgrDAL.DeleteCostCode(m_sdsCostCode.ds, rowIndex, m_strAppID, m_strEnterpriseID);
					ArrayList paramValues = new ArrayList();
					paramValues.Add(iRowsDeleted.ToString());
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_DLD",utility.GetUserCulture(),paramValues);
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					if(strMsg.IndexOf("FK") != -1)
					{
						lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_COST",utility.GetUserCulture());
					}
					if(strMsg.ToLower().IndexOf("child record found") != -1 )
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());							
					}			
					Logger.LogTraceError("CostCode.aspx.cs","dgCostCode_Delete","RBAC003",appException.Message.ToString());
					return;
				}
				if((int)ViewState["CCMode"] == (int)ScreenMode.Insert)
				{
					m_sdsCostCode.ds.Tables[0].Rows.RemoveAt(rowIndex);
				}
				else
				{
					RetreiveSelectedPage();
				}
				ViewState["CCOperation"] = Operation.None;
				//Make the row in non-edit Mode
				EditHRow(false);
				BindCCGrid();
				Logger.LogDebugInfo("CostCode","dgCostCode_Delete","INF004","updating data grid...");			
			}
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			btnExecuteQuery.Enabled = false;
			ViewState["CCMode"]=ScreenMode.ExecuteQuery; 
			//Since Query mode is always one row in edit mode, the rowindex is set to zero
			int rowIndex = 0; 
			GetChangedRow(dgCostCode.Items[0], rowIndex);
			Session["QUERY_DS"] = m_sdsCostCode.ds;
			RetreiveSelectedPage();
			if(m_sdsCostCode.ds.Tables[0].Rows.Count==0)
			{
				lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
				dgCostCode.EditItemIndex = -1; 
				return;
			}
			//set all records to non-edit mode after execute query
			EditHRow(false);
			dgCostCode.Columns[0].Visible=true;
			dgCostCode.Columns[1].Visible=true;
		}

		protected void dgCostCode_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			if (!btnExecuteQuery.Enabled)
			{
				int iRowsAffected = 0;
				int rowIndex = e.Item.ItemIndex;
				GetChangedRow(e.Item, e.Item.ItemIndex);

				//int iMode = (int)ViewState["Mode"];
				int iOperation = (int)ViewState["CCOperation"];
				try
				{
					if (iOperation == (int)Operation.Insert)
					{
						iRowsAffected = SysDataMgrDAL.InsertCostCode(m_sdsCostCode.ds, rowIndex, m_strAppID, m_strEnterpriseID);
						ArrayList paramValues = new ArrayList();
						paramValues.Add(iRowsAffected.ToString());
						lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_INS",utility.GetUserCulture(),paramValues);;
					}
					else
					{
						DataSet dsChangedRow = m_sdsCostCode.ds.GetChanges();
						iRowsAffected = SysDataMgrDAL.UpdateCostCode(dsChangedRow,m_strAppID, m_strEnterpriseID);
						ArrayList paramValues = new ArrayList();
						paramValues.Add(iRowsAffected.ToString());
						lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_UPD",utility.GetUserCulture(),paramValues);;
						m_sdsCostCode.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					}
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					strMsg = appException.InnerException.Message;					
					if(strMsg.IndexOf("PRIMARY KEY") != -1)
					{
						lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_COST",utility.GetUserCulture());
					}
					else if(strMsg.IndexOf("duplicate key") != -1 )
					{
						lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_PATH",utility.GetUserCulture());							
					}
					else if(strMsg.IndexOf("CREATE RULE ")!= -1)
					{
						lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DLV_TYPE",utility.GetUserCulture());						
					}			

					Logger.LogTraceError("DeliveryPathCode.aspx.cs","dgState_Update","RBAC003",appException.Message.ToString());
					return;
				}

				if (iRowsAffected == 0)
				{
					return;
				}
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				ViewState["CCOperation"] = Operation.None;
				dgCostCode.EditItemIndex = -1;
				BindCCGrid();
				Logger.LogDebugInfo("State","dgDelvPath_Update","INF004","updating data grid...");			
			}
		}

	
		

	}
}

