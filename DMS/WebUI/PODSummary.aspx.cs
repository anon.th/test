using System;
using System.IO;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using TIESDAL;
using ClosedXML.Excel;
using System.Configuration;
using System.Globalization;
using System.Text.RegularExpressions;

namespace com.ties
{
    /// <summary>
    /// Summary description for PODInTime.
    /// </summary>
    public class PODSummary : com.common.applicationpages.BasePage
    {

        private string appID = string.Empty;
        private string enterpriseID = string.Empty;
        private string userLoggin = string.Empty;

        #region Web Form Designer generated code
        protected System.Web.UI.WebControls.TextBox txtfromYears;
        protected System.Web.UI.WebControls.Button btnQuery;
        protected System.Web.UI.WebControls.Label lblErrorMessage;
        protected System.Web.UI.WebControls.Label lblTitle;
        protected System.Web.UI.WebControls.Button btnGenerate;
        protected System.Web.UI.WebControls.Label lblYear;
        protected System.Web.UI.WebControls.TextBox txtToYears;
        protected System.Web.UI.WebControls.Label lblPayerType;
        protected System.Web.UI.WebControls.Label Label4;
        protected System.Web.UI.WebControls.ListBox lsbCustType;
        protected System.Web.UI.WebControls.Label lblPayerCode;
        protected System.Web.UI.WebControls.TextBox txtPayerCode;
        protected System.Web.UI.WebControls.Button btnPayerCode;

        string filePath = @"Z:\POD_SUM_Template.xltm";
        string filePathTmp = @"Z:\TEMP\POD_SUM_Template" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xltm";

        override protected void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeComponent();
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            this.btnPayerCode.Click += new System.EventHandler(this.btnPayerCode_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        private String m_strAppID;
        private String m_strEnterpriseID;
        private String m_strCulture;



        private void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            m_strAppID = utility.GetAppID();
            m_strEnterpriseID = utility.GetEnterpriseID();
            m_strCulture = utility.GetUserCulture();

            utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings, Page.Session);

            appID = utility.GetAppID();
            enterpriseID = utility.GetEnterpriseID();
            userLoggin = utility.GetUserID();

            if (!IsPostBack)
            {
                // Bindyear();
                DefaultScreen();
                LoadCustomerTypeList();
            }

        }

        private void btnGenerate_Click(object sender, System.EventArgs e)
        {

            lblErrorMessage.Text = "";
            if (!ValidateValues())
            {
                return;
            }
            ReportPODSummaryDAL RepoPODSum = new ReportPODSummaryDAL(appID, enterpriseID);
            DataSet ds = RepoPODSum.GetPODSummary(enterpriseID, txtfromYears.Text, txtToYears.Text, txtPayerCode.Text, lsbCustType.SelectedValue);
            if (ds.Tables[0].Rows.Count > 0)
            {
                this.BindDatatoFile(ds);
            }
            else
            {
                lblErrorMessage.Text = "No data";
            }

        }

        public void BindDatatoFile(DataSet ds)
        {
            filePath = this.MapPath(ConfigurationSettings.AppSettings["PODSummaryTemplatePath"]);
            System.IO.FileInfo file = new System.IO.FileInfo(filePath);

            filePathTmp = this.MapPath(String.Format("{0}/PODSummary_{1}.{2}", ConfigurationSettings.AppSettings["TempPath"], DateTime.Now.ToString("yyyyMMdd_hhmmss"), file.Extension));
            File.Copy(filePath, filePathTmp);
            DataTable tbl = ds.Tables[0];

            DateTimeFormatInfo mfi = new DateTimeFormatInfo();
            using (var excelWorkbook = new XLWorkbook(filePathTmp))
            {
                excelWorkbook.Worksheet(2).Clear();

                // column headings
                for (var i = 0; i < tbl.Columns.Count; i++)
                {

                    excelWorkbook.Worksheet(2).Cell(1, i + 1).Value = tbl.Columns[i].ColumnName;
                    //= tbl.Columns[i].ColumnName;
                }

                // rows
                for (var i = 0; i < tbl.Rows.Count; i++)
                {
                    // to do: format datetime values before printing
                    for (var j = 0; j < tbl.Columns.Count; j++)
                    {
                        excelWorkbook.Worksheet(2).Cell(i + 2, j + 1).Value = tbl.Rows[i][j];
                    }
                }
                excelWorkbook.Save();
            }
            RespontoLoadFile(filePathTmp);

        }

        private void RespontoLoadFile(string filePathTmp)
        {
            System.IO.FileInfo file = new System.IO.FileInfo(filePathTmp);
            if (file.Exists)
            {
                byte[] Content = File.ReadAllBytes(filePathTmp);

                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("content-disposition", "attachment; filename=" + file.Name);
                Response.BufferOutput = true;
                Response.OutputStream.Write(Content, 0, Content.Length);
                Response.End();
            }

        }

        private void btnQuery_Click(object sender, System.EventArgs e)
        {
            DefaultScreen();
        }


        private void Bindyear()
        {
            txtfromYears.Text = (DateTime.Now.Year - 2).ToString();
            txtToYears.Text = DateTime.Now.Year.ToString();
        }



        private void DefaultScreen()
        {
            Bindyear();
            lsbCustType.SelectedIndex = -1;
            txtPayerCode.Text = null;
            lblErrorMessage.Text = "";
        }


        private bool ValidateValues()
        {
            bool isValid = true;
            Regex regex = new Regex(@"[0-9]{4}");

            if (string.IsNullOrEmpty(txtfromYears.Text.Trim()) || string.IsNullOrEmpty(txtToYears.Text.Trim()))
            {
                isValid = false;
                lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_YEAR", utility.GetUserCulture());
            }
            else if (!regex.IsMatch(txtfromYears.Text.Trim()) || !regex.IsMatch(txtToYears.Text.Trim()))
            {
                isValid = false;
                lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "INVALID_YYYY", utility.GetUserCulture());
            }
            else if (Math.Abs(int.Parse(txtfromYears.Text.Trim()) - (int.Parse(txtToYears.Text.Trim()))) >= 5)
            {
                isValid = false;
                lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "INVALID_Year5", utility.GetUserCulture());
            }

            return isValid;

        }



        #region "Payer Type Part : Controls

        private void btnPayerCode_Click(object sender, System.EventArgs e)
        {
            string strCustPayerType = "";
            for (int i = 0; i <= lsbCustType.Items.Count - 1; i++)
            {
                if (lsbCustType.Items[i].Selected == true)
                {
                    strCustPayerType += lsbCustType.Items[i].Value + ",";
                }
            }
            if ((strCustPayerType != null) && (strCustPayerType.Trim().Length > 0))
                strCustPayerType = strCustPayerType.Substring(0, strCustPayerType.Length - 1);

            String sUrl = "CustomerPopup.aspx?FORMID=" + "PODSummary" +
                "&CustType=" + strCustPayerType.ToString();
            ArrayList paramList = new ArrayList();
            paramList.Add(sUrl);
            String sScript = Utility.GetScript("openWindowParam.js", paramList);
            Utility.RegisterScriptString(sScript, "ShowPopupScript", this.Page);
        }

        #endregion



        #region "Payer Type Part : DropDownList"

        public void LoadCustomerTypeList()
        {
            ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(), utility.GetUserCulture(), "customer_type", CodeValueType.StringValue);
            foreach (SystemCode sysCode in systemCodes)
            {
                ListItem lstItem = new ListItem();
                lstItem.Text = sysCode.Text;
                lstItem.Value = sysCode.StringValue;
                lsbCustType.Items.Add(lstItem);
            }
        }

        #endregion


    }
}
