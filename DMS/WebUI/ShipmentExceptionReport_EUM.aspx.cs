using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.ties.classes;
using com.common.applicationpages;
using com.common.RBAC;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for ShipmentExceptionReport_EUM.
	/// </summary>
	public class ShipmentExceptionReport_EUM : BasePage
	{
		
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnGenerate;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtYear;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.Label lblMBG;
		protected System.Web.UI.WebControls.Label lblTitle;		
		protected com.common.util.msTextBox txtRouteType;
		protected System.Web.UI.WebControls.DropDownList dbMbgStatus;
		protected System.Web.UI.WebControls.Label lblPayerType;
		protected System.Web.UI.WebControls.Label lblStatusCode;
		protected System.Web.UI.WebControls.TextBox txtStatus;
		protected System.Web.UI.WebControls.Button btnStatusSrch;
		protected System.Web.UI.WebControls.Label lblExceptionCode;
		protected System.Web.UI.WebControls.TextBox txtException;
		protected System.Web.UI.WebControls.Button btnExceptionSrch;
		protected System.Web.UI.WebControls.Label lblConsignmentNo;
		protected System.Web.UI.WebControls.TextBox txtConsignmentNo;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.RadioButton rbEstDate;
		protected System.Web.UI.WebControls.Label lblPayerCode;
		protected System.Web.UI.WebControls.TextBox txtPayerCode;
		protected System.Web.UI.WebControls.Button btnPayerCode;
		protected System.Web.UI.WebControls.Label lblRouteType;
		protected System.Web.UI.WebControls.RadioButton rbLongRoute;
		protected System.Web.UI.WebControls.RadioButton rbShortRoute;
		protected System.Web.UI.WebControls.RadioButton rbAirRoute;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCode;
		protected Cambro.Web.DbCombo.DbCombo DbComboOriginDC;
		protected System.Web.UI.WebControls.Label Label5;
		protected Cambro.Web.DbCombo.DbCombo DbComboDestinationDC;
		protected System.Web.UI.WebControls.Label lblDestination;
		protected System.Web.UI.WebControls.Label lblZipCode;
		protected com.common.util.msTextBox txtZipCode;
		protected System.Web.UI.WebControls.Button btnZipCode;
		protected System.Web.UI.WebControls.Label lblStateCode;
		protected com.common.util.msTextBox txtStateCode;
		protected System.Web.UI.WebControls.Button btnStateCode;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentTracking;
		protected System.Web.UI.HtmlControls.HtmlTable tblPayerType;
		protected System.Web.UI.HtmlControls.HtmlTable tblRouteType;
		protected System.Web.UI.WebControls.RadioButton rbActualDate;
		protected System.Web.UI.WebControls.ListBox lsbCustType;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.RadioButton rbBookingDateCust;
		protected System.Web.UI.WebControls.RadioButton rbMonthCust;
		protected System.Web.UI.WebControls.DropDownList ddMonthCust;
		protected System.Web.UI.WebControls.Label Label18;
		protected com.common.util.msTextBox txtYearCust;
		protected System.Web.UI.WebControls.RadioButton rbPeriodCust;
		protected com.common.util.msTextBox txtPeriodCust;
		protected com.common.util.msTextBox txtToCust;
		protected System.Web.UI.WebControls.RadioButton rbDateCust;
		protected com.common.util.msTextBox txtDateCust;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label9;
		protected com.common.util.msTextBox txtZipCodeCust;
		protected System.Web.UI.WebControls.Button btnZipCodeCust;
		protected System.Web.UI.WebControls.Label Label7;
		protected com.common.util.msTextBox txtStateCodeCust;
		protected System.Web.UI.WebControls.Button btnStateCodeCust;
		protected System.Web.UI.HtmlControls.HtmlTable tblExternal;
		protected System.Web.UI.HtmlControls.HtmlTable Table3;
		protected System.Web.UI.HtmlControls.HtmlTable Table6;
		protected System.Web.UI.WebControls.Label lblServiceCode;
		protected Cambro.Web.DbCombo.DbCombo DbCmbServiceCode;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.DropDownList ddlTransporter;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist1;
		protected System.Web.UI.HtmlControls.HtmlTable tblDestination;

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
			this.rbMonthCust.CheckedChanged += new System.EventHandler(this.rbMonthCust_CheckedChanged);
			this.rbPeriodCust.CheckedChanged += new System.EventHandler(this.rbPeriodCust_CheckedChanged);
			this.rbDateCust.CheckedChanged += new System.EventHandler(this.rbDateCust_CheckedChanged);
			this.btnZipCodeCust.Click += new System.EventHandler(this.btnZipCodeCust_Click);
			this.btnStateCodeCust.Click += new System.EventHandler(this.btnStateCodeCust_Click);
			this.rbEstDate.CheckedChanged += new System.EventHandler(this.rbEstDate_CheckedChanged);
			this.rbActualDate.CheckedChanged += new System.EventHandler(this.rbActualDate_CheckedChanged);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.btnPayerCode.Click += new System.EventHandler(this.btnPayerCode_Click);
			this.rbLongRoute.CheckedChanged += new System.EventHandler(this.rbLongRoute_CheckedChanged);
			this.rbShortRoute.CheckedChanged += new System.EventHandler(this.rbShortRoute_CheckedChanged);
			this.rbAirRoute.CheckedChanged += new System.EventHandler(this.rbAirRoute_CheckedChanged);
			this.btnZipCode.Click += new System.EventHandler(this.btnZipCode_Click);
			this.btnStateCode.Click += new System.EventHandler(this.btnStateCode_Click);
			this.btnStatusSrch.Click += new System.EventHandler(this.btnStatusSrch_Click);
			this.btnExceptionSrch.Click += new System.EventHandler(this.btnExceptionSrch_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private String m_strAppID;
		private String m_strEnterpriseID;
		private String m_strCulture;
		DataSet m_dsQuery=null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			DbComboOriginDC.ClientOnSelectFunction="makeUppercase('DbComboOriginDC:ulbTextBox');";
			DbComboDestinationDC.ClientOnSelectFunction="makeUppercase('DbComboDestinationDC:ulbTextBox');";

			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboOriginDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboDestinationDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbCmbServiceCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];   //Jeab 19 Jul 2011
		
			com.common.classes.User user = RBACManager.GetUser(m_strAppID, m_strEnterpriseID, (String)Session["userID"]);
			ViewState["usertype"] = user.UserType;
			ViewState["PayerID"] = user.PayerID;
			if ((String)ViewState["usertype"]  == "C")
			{
				tblShipmentTracking.Visible = false;
				tblExternal.Visible = true;
			}
			else
			{
				tblShipmentTracking.Visible = true;
				tblExternal.Visible = false;
			}
			Page.DataBind();
			if(!Page.IsPostBack)
			{
				DefaultScreen();
				ddlmonths();
				ddlmonthscust();
				LoadCustomerTypeList();
				ddlMBG();
			}
			SetServiceCodeServerStates();  //Jeab 19 Jul 2011
			SetDbComboServerStates();
		}


		private void btnGenerate_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			if((String)ViewState["usertype"] != "C")
			{
				if (rbMonth.Checked==true)
				{
					if (ddMonth.SelectedItem.Value == "" || txtYear.Text.Trim() =="")
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
						return;
					}
				}
				if (rbPeriod.Checked==true)
				{
					if (txtPeriod.Text.Trim()== "" || txtTo.Text.Trim() =="")
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"FROM_TO_DT_REQ",utility.GetUserCulture());
						return;
					}
				}
				if (rbDate.Checked==true)
				{
					if (txtDate.Text.Trim() =="")
					{					
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DT_REQ",utility.GetUserCulture());
						return;
					}
				}
			}
			else
			{
				if (rbMonthCust.Checked==true)
				{
					if (ddMonthCust.SelectedItem.Value == "" || txtYearCust.Text.Trim() =="")
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
						return;
					}
				}
				if (rbPeriodCust.Checked==true)
				{
					if (txtPeriodCust.Text.Trim()== "" || txtToCust.Text.Trim() =="")
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"FROM_TO_DT_REQ",utility.GetUserCulture());
						return;
					}
				}
				if (rbDateCust.Checked==true)
				{
					if (txtDateCust.Text.Trim() =="")
					{					
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DT_REQ",utility.GetUserCulture());
						return;
					}
				}
			}

			m_dsQuery = GetShipmentQueryData();
			if ((String)ViewState["usertype"] == "C")
			{
				Session["FORMID"] = "ShipmentException3PA";
			}
			else
			{
				Session["FORMID"] = "ShipmentExceptionReport";
			}
			Session["SESSION_DS1"] = m_dsQuery;

			String sUrl = "ReportViewer.aspx";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			
		}


		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}


		#region "Page Functions"

		private DataTable CreateEmptyDataTable()
		{
			DataTable dtShipment = new DataTable();

			#region "Dates"
			dtShipment.Columns.Add(new DataColumn("tran_date", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
			dtShipment.Columns.Add(new DataColumn("end_date", typeof(DateTime)));
			#endregion
			
			#region "Payer Type"
			dtShipment.Columns.Add(new DataColumn("payer_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payer_code", typeof(string)));
			#endregion
		
			#region "Route / DC Selection"
			dtShipment.Columns.Add(new DataColumn("route_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("route_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("origin_dc", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("destination_dc", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("delPath_origin_dc", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("delPath_destination_dc", typeof(string)));
			#endregion

			#region "Destination"
			dtShipment.Columns.Add(new DataColumn("zip_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("state_code", typeof(string)));
			#endregion

			#region "Status & Exception"
			dtShipment.Columns.Add(new DataColumn("Status", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("Exception", typeof(string)));
			#endregion

			#region "MBG Status"
			dtShipment.Columns.Add(new DataColumn("EntitledMBG", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("ConsignmentNo", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("Service_code", typeof(string))); //Jeab 19 Jul 2011
			#endregion

			return dtShipment;
		}


		private DataSet GetShipmentQueryData()
		{
			DataSet dsShipment = new DataSet();
			DataTable dtShipment = CreateEmptyDataTable();
			DataRow dr = dtShipment.NewRow(); 
			
			#region "Dates"
			if((String)ViewState["usertype"] != "C")
			{
				if (rbEstDate.Checked) 
				{	// Estimated Delivery Date Selected
					dr["tran_date"] = "E";
				}
				if (rbActualDate.Checked) 
				{	// Actual POD Date Selected
					dr["tran_date"] = "A";
				}
			
				String strStartDate = null;
				DateTime dtStartDate = DateTime.Now;
				DateTime dtEndDate = DateTime.Now;

				if (rbMonth.Checked == true)
				{					
					// Avoid single month issue so put PadLeft function to make the month having two digits.
					strStartDate = "01"+"/"+ddMonth.SelectedItem.Value.PadLeft(2, '0')+"/"+txtYear.Text.Trim();

					//dtStartDate = System.DateTime.ParseExact(strStartDate, "dd/MM/yyyy", null);	
					//dtEndDate = dtStartDate.AddMonths(1).AddDays(-1);	

					dtStartDate = System.DateTime.ParseExact(strStartDate+" 00:00:01","dd/MM/yyyy HH:mm:ss",null);				
					dtEndDate = System.DateTime.ParseExact(dtStartDate.AddMonths(1).AddDays(-1).ToString("dd/MM/yyyy")+" 23:59:59","dd/MM/yyyy HH:mm:ss",null);
				}
				if (rbPeriod.Checked == true)
				{	
					//dtStartDate = System.DateTime.ParseExact(txtPeriod.Text.Trim(),"dd/MM/yyyy",null);									
					//dtEndDate = System.DateTime.ParseExact(txtTo.Text.Trim(),"dd/MM/yyyy",null);

					dtStartDate=System.DateTime.ParseExact(txtPeriod.Text.Trim()+" 00:00:01","dd/MM/yyyy HH:mm:ss",null);									
					String tmpEndDate = txtTo.Text.Trim()+" 23:59:59";
					dtEndDate=System.DateTime.ParseExact(tmpEndDate,"dd/MM/yyyy HH:mm:ss",null);
				}

				if (rbDate.Checked == true)
				{				
					//dtStartDate = System.DateTime.ParseExact(txtDate.Text.Trim(),"dd/MM/yyyy",null);
					//dtEndDate = dtStartDate.AddDays(1).AddMinutes(-1);				

					dtStartDate = System.DateTime.ParseExact(txtDate.Text.Trim()+" 00:00:01","dd/MM/yyyy HH:mm:ss",null);
					dtEndDate = System.DateTime.ParseExact(dtStartDate.AddDays(1).AddMinutes(-1).ToString("dd/MM/yyyy")+" 23:59:59","dd/MM/yyyy HH:mm:ss",null);
				}

				dr["start_date"] = dtStartDate;
				dr["end_date"] = dtEndDate;
			}
			else
			{
				if (rbBookingDateCust.Checked) 
				{	// Booking Date Selected
					dr["tran_date"] = "A";
				}
			
				string strMonth =null;
			

				if (rbMonthCust.Checked) 
				{	// Month Selected
					strMonth = ddMonthCust.SelectedItem.Value;
					if (strMonth != "" && txtYearCust.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact ("01/"+strMonth.ToString()+"/"+txtYearCust.Text+" 00:00:01", "dd/MM/yyyy HH:mm:ss", null);
						int intLastDay = 0;
						intLastDay = LastDayOfMonth(strMonth, txtYearCust.Text);
						dr["end_date"] = DateTime.ParseExact (intLastDay.ToString()+"/"+strMonth.ToString()+"/"+txtYearCust.Text+" 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
					}
				}
				if (rbPeriodCust.Checked) 
				{	// Period Selected
					if (txtPeriodCust.Text != ""  && txtToCust.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact(txtPeriodCust.Text+" 00:00:01","dd/MM/yyyy HH:mm:ss",null);
						dr["end_date"] = DateTime.ParseExact(txtToCust.Text+" 23:59:59" ,"dd/MM/yyyy HH:mm:ss",null);
					}
				}
				if (rbDateCust.Checked) 
				{	// Date Selected
					if (txtDateCust.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact(txtDateCust.Text+" 00:00:01", "dd/MM/yyyy HH:mm:ss",null);
						dr["end_date"] = DateTime.ParseExact(txtDateCust.Text+" 23:59:59", "dd/MM/yyyy HH:mm:ss",null);
					}
				}
			}
			#endregion

			#region "Payer Type"

			string strCustPayerType = "";

			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
			{
				if(lsbCustType.Items[i].Selected == true)
				{
					strCustPayerType += "\"" + lsbCustType.Items[i].Value + "\",";
				}
			}

			if (strCustPayerType != "") 
				dr["payer_type"] = "(" + strCustPayerType.Substring(0,strCustPayerType.Length - 1) + ")";
			else
				dr["payer_type"] = System.DBNull.Value;

			if ((String)ViewState["usertype"] == "C")

				dr["payer_code"] = (String)ViewState["PayerID"];		
			else		
				dr["payer_code"] = txtPayerCode.Text.Trim();			

			#endregion
			
			#region "Route / DC Selection"
			if (rbLongRoute.Checked) 
			{
				dr["route_type"] = "L";
			}
			if (rbShortRoute.Checked) 
			{
				dr["route_type"] = "S";
			}
			if (rbAirRoute.Checked) 
			{
				dr["route_type"] = "A";
			}

			dr["route_code"] = DbComboPathCode.Value;
			dr["origin_dc"] = DbComboOriginDC.Value.ToUpper();
			dr["destination_dc"] = DbComboDestinationDC.Value.ToUpper();
			
			if (rbLongRoute.Checked || rbAirRoute.Checked)
			{
				com.ties.classes.DeliveryPath delPath = new com.ties.classes.DeliveryPath();
				delPath.Populate(m_strAppID, m_strEnterpriseID, DbComboPathCode.Value);

				dr["delPath_origin_dc"] = delPath.OriginStation;
				dr["delPath_destination_dc"] = delPath.DestinationStation;
			}
			else
			{
				dr["delPath_origin_dc"] = "";
				dr["delPath_destination_dc"] = "";
			}
			#endregion

			#region "Destination"
			if((String)ViewState["usertype"] == "C")
			{
				dr["zip_code"] = txtZipCodeCust.Text.Trim();
				dr["state_code"] = txtStateCodeCust.Text.Trim();
			}
			else
			{
				dr["zip_code"] = txtZipCode.Text;
				dr["state_code"] = txtStateCode.Text;
			}
			#endregion

			#region "Status & Exception"
			dr["Status"] = txtStatus.Text;
			dr["Exception"] = txtException.Text;
			#endregion

			#region "MBG Status"

			string strEntitledMBG = null;
			strEntitledMBG = dbMbgStatus.SelectedItem.Value;
			if (strEntitledMBG != "") 
				dr["EntitledMBG"] = dbMbgStatus.SelectedItem.Value;
			else
				dr["EntitledMBG"] = System.DBNull.Value;

			dr["ConsignmentNo"] = txtConsignmentNo.Text;

			//Jeab 19 Jul  2011
			string strServiceCode = null;
			strServiceCode = DbCmbServiceCode.Value;
			if (strServiceCode != "") 
				dr["service_code"] = strServiceCode;
			else
				dr["service_code"] = System.DBNull.Value;
			//Jeab 19 Jul  2011  =========> End
			#endregion

			dtShipment.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipment);
			return dsShipment;
		}


		private void DefaultScreen()
		{
			#region "Dates"

			rbEstDate.Checked = true;
			rbActualDate.Checked = false;
			rbMonth.Checked = true;
			rbMonthCust.Checked = true;
			rbPeriod.Checked = false;
			rbPeriodCust.Checked = false;
			rbDate.Checked = false;
			rbDateCust.Checked = false;

			ddMonth.Enabled = true;
			ddMonth.SelectedIndex = -1;
			ddMonthCust.Enabled = true;
			ddMonthCust.SelectedIndex = -1;
			txtYear.Text = null;
			txtYearCust.Text = null;
			txtYear.Enabled = true;
			txtYearCust.Enabled = true;
			txtPeriod.Text = null;
			txtPeriodCust.Text = null;
			txtPeriod.Enabled = false;
			txtPeriodCust.Enabled = false;
			txtTo.Text = null;
			txtToCust.Text = null;
			txtTo.Enabled = false;
			txtToCust.Enabled = false;
			txtDate.Text = null;
			txtDateCust.Text = null;
			txtDate.Enabled = false;
			txtDateCust.Enabled = false;

			#endregion

			#region "Payer Type"

			lsbCustType.SelectedIndex = -1;
			txtPayerCode.Text = null;

			#endregion
			
			#region "Route / DC Selection"
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";

			DbComboOriginDC.Text="";
			DbComboOriginDC.Value="";

			DbComboDestinationDC.Text="";
			DbComboDestinationDC.Value="";
			#endregion

			#region "Destination"

			txtZipCode.Text = null;
			txtZipCodeCust.Text = null;
			txtStateCode.Text = null;
			txtStateCodeCust.Text = null;

			#endregion

			#region "Status & Exception"

			txtStatus.Text="";
			txtException.Text="";

			#endregion

			#region "MBG Status"

			dbMbgStatus.SelectedIndex = -1;
			txtConsignmentNo.Text="";
			//Jeab 19 Jul 2011
			DbCmbServiceCode.Text="";
			DbCmbServiceCode.Value="";
			//Jeab 19 Jul 2011  =========> End

			#endregion

			lblErrorMessage.Text = "";
		}


		private int ValidateData()
		{
			if (txtYear.Text != "" )
			{
				if (Convert.ToInt32(txtYear.Text) < 2000 || Convert.ToInt32(txtYear.Text) > 2099)
				{					
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
					return -1;
				}
			}
			return 1;
		}


		private int LastDayOfMonth(String strMonth, String strYear)
		{
			int intLastDay = 0;
			switch (strMonth)
			{
				case "01":
				case "03":
				case "05":
				case "07":
				case "08":
				case "10":
				case "12":
					intLastDay = 31;
					break;
				case "04":
				case "06":
				case "09":
				case "11":
					intLastDay = 30;
					break;
				case "02":
					intLastDay = 28;
					break;
			}
			return intLastDay;
		}


		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
		

		#endregion


		#region "Dates Part : Controls"

		private void rbEstimatedDate_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
		}


		private void rbActualPODDate_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
		}


		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = true;
		}


		#endregion

		#region "Payer Type Part : Controls

		private void btnPayerCode_Click(object sender, System.EventArgs e)
		{
			string strCustPayerType = "";
			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
			{
				if(lsbCustType.Items[i].Selected == true)
				{
					strCustPayerType += lsbCustType.Items[i].Value + ",";
				}
			}
			if((strCustPayerType != null) && (strCustPayerType.Trim().Length > 0))
				strCustPayerType = strCustPayerType.Substring(0, strCustPayerType.Length - 1);

			String sUrl = "CustomerPopup.aspx?FORMID="+"ShipmentExceptionReport"+
				"&CustType="+strCustPayerType.ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion

		#region "Route / DC Selection : Controls"

		private void rbLongRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}


		private void rbShortRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";	
		}


		private void rbAirRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}


		#endregion

		#region "Destination : Controls"

		private void btnZipCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "ZipCodePopup.aspx?FORMID=ShipmentExceptionReport"+"&ZIPCODE_CID="+txtZipCode.ClientID+"&ZIPCODE="+txtZipCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openPostalWindow.js",paramList); //Thosapol.y  Modify (26/06/2013)
			//String sScript = Utility.GetScript("openParentWindow.js",paramList); //Thosapol.y  Comment (26/06/2013)
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		private void btnStateCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "StatePopup.aspx?FORMID=ShipmentExceptionReport"+"&STATECODE="+txtStateCode.ClientID+"&STATECODE_TEXT="+txtStateCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion

		#region "MBG Status"

		private void btnStatusSrch_Click(object sender, System.EventArgs e)
		{		
			OpenWindowpage("StatusCodePopup.aspx?FORMID=ShipmentExceptionReport&STATUSCODE="+txtStatus.Text);		
		}


		private void btnExceptionSrch_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("ExceptionCodePopup.aspx?FORMID=ShipmentExceptionReport&STATUSCODE="+txtStatus.Text+"&EXCEPTIONCODE="+txtException.Text);
		}


		#endregion

		
		#region "Dates Part : DropDownList"

		private void ddlmonths()
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);

			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}


		#endregion
		
		#region "Payer Type Part : DropDownList"

		public void LoadCustomerTypeList()
		{
			ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"customer_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				lsbCustType.Items.Add(lstItem);
			}
		}


		#endregion

		#region "Route / DC Selection : DropDownList"

		private void SetDbComboServerStates()
		{
			String strDeliveryType=null;
			if (rbLongRoute.Checked==true)
				strDeliveryType="L";
			else if (rbShortRoute.Checked==true)
				strDeliveryType="S";
			else if (rbAirRoute.Checked==true)
				strDeliveryType="A";

			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
			DbComboPathCode.ServerState = hash;
			DbComboPathCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"' ";
					if(strDelType == "S") 
					{
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct delivery_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
				}				
			}
			
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboDistributionCenterSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,args);	
			return dataset;
		}


		#endregion

		//Jeab 19 Jul 2011
		#region "Service Code : DropDownList"
		private void SetServiceCodeServerStates()
		{
					
			String strServiceCode=DbCmbServiceCode.Value;
			
			Hashtable hash = new Hashtable();
			hash.Add("strServiceCode", strServiceCode);
			DbCmbServiceCode.ServerState = hash;
			DbCmbServiceCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4";			
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object ServiceCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strServiceCode="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strServiceCode"] != null && args.ServerState["strServiceCode"].ToString().Length > 0)
				{
					strServiceCode = args.ServerState["strServiceCode"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.ServiceCodeQuery (strAppID,strEnterpriseID,args);	
			return dataset;                
		}
		#endregion
		//Jeab 19 Jul 2011  =========> End

		#region "MBG Status : DropDownList"

		private void ddlMBG()
		{
			DataTable dtMBG = new DataTable();
			dtMBG.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMBG.Columns.Add(new DataColumn("StringValue", typeof(string)));

			DataRow drNilRow = dtMBG.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMBG.Rows.Add(drNilRow);

			ArrayList listMBGTxt = Utility.GetCodeValues(m_strAppID, m_strCulture, "mbg", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMBGTxt)
			{
				DataRow drEach = dtMBG.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMBG.Rows.Add(drEach);
			}
			DataView dvMBG = new DataView(dtMBG);

			dbMbgStatus.DataSource = dvMBG;
			dbMbgStatus.DataTextField = "Text";
			dbMbgStatus.DataValueField = "StringValue";
			dbMbgStatus.DataBind();
		}


		#endregion

		private void rbEstDate_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}

		private void rbActualDate_CheckedChanged(object sender, System.EventArgs e)
		{
		
		}
		private void ddlmonthscust()
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);

			ddMonthCust.DataSource = dvMonths;
			ddMonthCust.DataTextField = "Text";
			ddMonthCust.DataValueField = "StringValue";
			ddMonthCust.DataBind();	
		}

		private void btnZipCodeCust_Click(object sender, System.EventArgs e)
		{
			String sUrl = "ZipCodePopup.aspx?FORMID=ShipmentExceptionReport"+"&ZIPCODE_CID="+txtZipCodeCust.ClientID+"&ZIPCODE="+txtZipCodeCust.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnStateCodeCust_Click(object sender, System.EventArgs e)
		{
			String sUrl = "StatePopup.aspx?FORMID=ShipmentExceptionReport"+"&STATECODE="+txtStateCodeCust.ClientID+"&STATECODE_TEXT="+txtStateCodeCust.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void rbPeriodCust_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			ddMonthCust.SelectedIndex = -1;
			ddMonthCust.Enabled = false;
			txtYearCust.Text = null;
			txtYearCust.Enabled = false;
			txtPeriodCust.Text = null;
			txtPeriodCust.Enabled = true;
			txtToCust.Text = null;
			txtToCust.Enabled = true;
			txtDateCust.Text = null;
			txtDateCust.Enabled = false;
		}

		private void rbMonthCust_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			ddMonthCust.SelectedIndex = -1;
			ddMonthCust.Enabled = true;
			txtYearCust.Text = null;
			txtYearCust.Enabled = true;
			txtPeriodCust.Text = null;
			txtPeriodCust.Enabled = false;
			txtToCust.Text = null;
			txtToCust.Enabled = false;
			txtDateCust.Text = null;
			txtDateCust.Enabled = false;
		}

		private void rbDateCust_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			ddMonthCust.SelectedIndex = -1;
			ddMonthCust.Enabled = false;
			txtYearCust.Text = null;
			txtYearCust.Enabled = false;
			txtPeriodCust.Text = null;
			txtPeriodCust.Enabled = false;
			txtToCust.Text = null;
			txtToCust.Enabled = false;
			txtDateCust.Text = null;
			txtDateCust.Enabled = true;
		}
	}
}
