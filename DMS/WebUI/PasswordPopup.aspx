<%@ Page language="c#" Codebehind="PasswordPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.PasswordPopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PasswordPopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="PasswordPopup" method="post" runat="server">
			<asp:label id="lblMainHeader" style="Z-INDEX: 100; LEFT: 100px; POSITION: absolute; TOP: 25px" CssClass="mainTitleSize" Height="24px" Width="277px" Runat="server">User Password</asp:label>
			<asp:Label id="lblErrorMsg" style="Z-INDEX: 109; LEFT: 100px; POSITION: absolute; TOP: 75px" runat="server" Width="521px" Height="16px" CssClass="errorMsgColor"></asp:Label>
			<asp:textbox id="txtRetypePswd" style="Z-INDEX: 107; LEFT: 250px; POSITION: absolute; TOP: 174px" tabIndex="2" runat="server" Width="149px" CssClass="textField" TextMode="Password"></asp:textbox>
			<asp:label id="lblRetypePswd" style="Z-INDEX: 106; LEFT: 100px; POSITION: absolute; TOP: 177px" runat="server" Width="131px" Height="22px" CssClass="tableLabel">Confirm Password</asp:label>
			<asp:button id="btnCancel" TabIndex="4" style="Z-INDEX: 102; LEFT: 198px; POSITION: absolute; TOP: 227px" runat="server" CssClass="queryButton" Height="21px" Width="62px" CausesValidation="False" Text="Cancel"></asp:button>
			<asp:button id="btnOk" TabIndex="3" style="Z-INDEX: 101; LEFT: 131px; POSITION: absolute; TOP: 226px" runat="server" CssClass="queryButton" Height="22px" Width="62px" CausesValidation="False" Text="OK"></asp:button>
			<asp:label id="lblPassword" runat="server" Width="108px" Height="22px" CssClass="tableLabel" style="Z-INDEX: 103; LEFT: 100px; POSITION: absolute; TOP: 131px">Password</asp:label>
			<asp:textbox id="txtPassword" tabIndex="1" runat="server" Width="149px" CssClass="textField" style="Z-INDEX: 105; LEFT: 250px; POSITION: absolute; TOP: 126px" TextMode="Password"></asp:textbox>
		</form>
	</body>
</HTML>
