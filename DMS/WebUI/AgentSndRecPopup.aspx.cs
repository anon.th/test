using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using System.Text;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for AgentSndRec.
	/// </summary>
	public class AgentSndRec : BasePopupPage
	{
		protected System.Web.UI.WebControls.DataGrid dgSndRecpMaster;
		protected System.Web.UI.WebControls.TextBox txtName;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Button btnOk;
		private String strSndRcpType = null;
		SessionDS m_sdsSendRecip = null;
		String strFormID = "";
		private String appID = null;
		private String enterpriseID = null;
		//Utility utility = null;
		String strPayMode=null;
		string strESASurcharge=null;
		string strESAApplied=null;
		String strtxtESA=null;
		String strtxtESASurcharge=null;
		private string strCustType=null;
		private string  strCustID=null;
		String strZipCode = null;
		String strSendZip = null;
		String strESAValue = "0.00";
		String strDestZipCode = null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			strFormID = Request.Params["FORMID"];
			strSndRcpType = Request.Params["SENDRCPTYPE"];
			strCustType=Request.Params["CUSTTYPE"];
			strCustID=Request.Params["CUSTID"]; 
			strPayMode=Request.Params["PAYMODE"];
			strZipCode = Request.Params["ZIPCODE"];
			strDestZipCode = Request.Params["DestZipCode"]; 
			strSendZip = Request.Params["SENDZIP"];

			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();

			if(!Page.IsPostBack)
			{
				txtName.Text = Request.Params["SENDERNAME"];
				m_sdsSendRecip = GetEmptySndRcpDS(0);
				BindGrid();
			}
			else
			{
				m_sdsSendRecip = (SessionDS)ViewState["SNDRCP_DS"];

			}
		}
		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			//runs when one of the page control is clicked
			//update the current page from the parameter values
			
			dgSndRecpMaster.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentPage();
		
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		public static SessionDS GetEmptySndRcpDS(int iNumRows)
		{
			DataTable dtSndRcp = new DataTable();
 
			dtSndRcp.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtSndRcp.Columns.Add(new DataColumn("state_name", typeof(string)));
			dtSndRcp.Columns.Add(new DataColumn("country", typeof(string)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtSndRcp.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";
				drEach[3] = "";
				drEach[4] = "";

				dtSndRcp.Rows.Add(drEach);
			}

			DataSet dsSndRcp = new DataSet();
			dsSndRcp.Tables.Add(dtSndRcp);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsSndRcp;
			sessionDS.DataSetRecSize = dsSndRcp.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;	
		}
		private SessionDS GetSndRcpDS(int iCurrent, int iDSRecSize, String strQuery)
		{

			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SndRcipPopup.aspx.cs","GetSndRcpDS","ERR001","DbConnection object is null!!");
				return sessionDS;
			}

			
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"SenderRecipentData");

			return  sessionDS;
			
		}

		private void BindGrid()
		{
			dgSndRecpMaster.VirtualItemCount = System.Convert.ToInt32(m_sdsSendRecip.QueryResultMaxSize);
			dgSndRecpMaster.DataSource = m_sdsSendRecip.ds;
			dgSndRecpMaster.DataBind();
			ViewState["SNDRCP_DS"] = m_sdsSendRecip;
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgSndRecpMaster.CurrentPageIndex * dgSndRecpMaster.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsSendRecip = GetSndRcpDS(iStartIndex,dgSndRecpMaster.PageSize,sqlQuery);
			decimal pgCnt = (m_sdsSendRecip.QueryResultMaxSize - 1)/dgSndRecpMaster.PageSize;
			if(pgCnt < dgSndRecpMaster.CurrentPageIndex)
			{
				dgSndRecpMaster.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgSndRecpMaster.SelectedIndex = -1;
			dgSndRecpMaster.EditItemIndex = -1;

			BindGrid();
		}
		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			String strSndRcpName = txtName.Text.Trim();

			StringBuilder strQry = new StringBuilder();
			strQry.Append("select * from Agent_snd_rec where applicationid='");
			strQry.Append(appID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(enterpriseID+"'");

			if(strSndRcpName != null && strSndRcpName != "")
			{
				strQry.Append(" and snd_rec_name like N'%");
				strQry.Append(Utility.ReplaceSingleQuote(strSndRcpName));
				strQry.Append("%' ");
			}
			
			if(strCustID != null && strCustID != "")
			{
				strQry.Append(" and agentid like '%");
				strQry.Append(Utility.ReplaceSingleQuote(strCustID));
				strQry.Append("%' ");
			}

			strQry.Append(" and (snd_rec_type = 'B' or snd_rec_type = '");
			strQry.Append(strSndRcpType+"')");

			if(strZipCode != null && strZipCode != "")
			{
				strQry.Append(" and zipcode like '%");
				strQry.Append(Utility.ReplaceSingleQuote(strZipCode));
				strQry.Append("%' ");
			}

			String strSQLQuery = strQry.ToString();

			ViewState["SQL_QUERY"] = strSQLQuery;
			dgSndRecpMaster.CurrentPageIndex = 0;

			ShowCurrentPage();
		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		public void dgSndRecpMaster_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			int iSelIndex = dgSndRecpMaster.SelectedIndex;
			DataGridItem dgRow = dgSndRecpMaster.Items[iSelIndex];
			String strName = dgRow.Cells[0].Text;
			String strAddress1 = dgRow.Cells[1].Text;
			String strAddress2 = dgRow.Cells[2].Text;
			String strCountry = dgRow.Cells[3].Text;
			String strZipcode = dgRow.Cells[4].Text;
			String strTelephone = dgRow.Cells[5].Text;
			String strFax = dgRow.Cells[6].Text;
			String strContactPerson = dgRow.Cells[7].Text;

			//Used for calculation of ESA surcharge of Domestic Shipment
			strSendZip = strZipcode;

			//Get the Country
			if(strCountry.Length == 0)
			{
				Enterprise enterprise = new Enterprise();
				enterprise.Populate(appID,enterpriseID);
				strCountry = enterprise.Country;
			}

			//Get the state & display...
			Zipcode zipCode = new Zipcode();
			zipCode.Populate(appID,enterpriseID,strCountry,strZipcode);
			String strState = zipCode.StateName;
			DateTime dtCuttOffTime = zipCode.CuttOffTime;
			String strCuttOffTime = dtCuttOffTime.ToString("HH:mm");
			strESASurcharge = zipCode.EASSurcharge.ToString ();


			if(strAddress1 == "&nbsp;")
			{
				strAddress1 = "";
			}
			if(strAddress2 == "&nbsp;")
			{
				strAddress2 = "";
			}
			if(strZipcode == "&nbsp;")
			{
				strZipcode = "";
			}
			if(strCountry == "&nbsp;")
			{
				strCountry = "";
			}
			if(strTelephone == "&nbsp;")
			{
				strTelephone = "";
			}
			if(strFax == "&nbsp;")
			{
				strFax = "";
			}
			if(strContactPerson == "&nbsp;")
			{
				strContactPerson = "";
			}

			String sScript = "";
			sScript += "<script language=javascript>";
			if (strFormID=="ShipmentDetails")
			{
				CalculateESA();
				sScript += "  window.opener."+strFormID+".txtRecipZip.value = \""+strZipcode+ "\";";
				sScript += "  window.opener."+strFormID+".txtESA.value =\""+strtxtESA+ "\";";
				sScript += "  window.opener."+strFormID+".txtESASurcharge.value = \""+strtxtESASurcharge+ "\";";
				
			}
			else
			{
				CalDomesticShipmentESA();

				if(strSndRcpType.Equals("S"))
				{
					sScript += "  window.opener."+strFormID+".txtSendName.value = \""+strName+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendAddr1.value = \""+strAddress1+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendAddr2.value = \""+strAddress2+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendState.value = \""+strCountry+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendZip.value = \""+strZipcode+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendCity.value = \""+strState+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendContPer.value = \""+strContactPerson+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendTel.value = \""+strTelephone+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendFax.value = \""+strFax+ "\";";
					sScript += "  window.opener."+strFormID+".txtSendCuttOffTime.value = \""+strCuttOffTime+ "\";";
					sScript += "  window.opener."+strFormID+".txtESASurchrg.value = \""+strESAValue+"\";";
				}
				else if(strSndRcpType.Equals("R"))
				{
					sScript += "  window.opener."+strFormID+".txtRecName.value = \""+strName+ "\";";
					sScript += "  window.opener."+strFormID+".txtRecipAddr1.value = \""+strAddress1+ "\";";
					sScript += "  window.opener."+strFormID+".txtRecipAddr2.value = \""+strAddress2+ "\";";
					sScript += "  window.opener."+strFormID+".txtRecipState.value = \""+strCountry+ "\";";
					sScript += "  window.opener."+strFormID+".txtRecipZip.value = \""+strZipcode+ "\";";
					sScript += "  window.opener."+strFormID+".txtRecipCity.value = \""+strState+ "\";";
					sScript += "  window.opener."+strFormID+".txtRecpContPer.value = \""+strContactPerson+ "\";";
					sScript += "  window.opener."+strFormID+".txtRecipTel.value = \""+strTelephone+ "\";";
					sScript += "  window.opener."+strFormID+".txtRecipFax.value = \""+strFax+ "\";";
					sScript += "  window.opener."+strFormID+".txtESASurchrg.value =\""+strESAValue+ "\";";
				}
			}
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		
		}

		private void CalculateESA()
		{
			if (Request.Params["CUSTTYPE"]=="C")
			{
			
				Customer customer = new Customer() ;
				customer.Populate(appID ,enterpriseID,Request.Params["CUSTID"]);    
				strESAApplied = customer.ESASurcharge;
			}
			else if (Request.Params["CUSTTYPE"]== "A")
			{
				Agent agent = new Agent();
				agent.Populate(appID ,enterpriseID,Request.Params["CUSTID"]); 
				strESAApplied = agent.ESASurcharge; 
			}

			if ((Request.Params["PAYMODE"]=="C") || (Request.Params["PAYMODE"]=="R"))
			{
				if(strESASurcharge!="0" && strESAApplied=="Y" )
				{
					strtxtESA="YES"; 
					strtxtESASurcharge = strESASurcharge; 
				}
				else 
				{
					strtxtESA="NO"; 
					strtxtESASurcharge = ""; 
				}
			}
			if((Request.Params["NEWCUST"]=="Y")&& (Request.Params["PAYMODE"]=="C"))
			{
				if(strESASurcharge!="0")
				{
					strtxtESA="YES"; 
					strtxtESASurcharge = strESASurcharge; 
				}
				else
				{
					strtxtESA="NO"; 
					strtxtESASurcharge = "";
				}
			}
		}
		private void CalDomesticShipmentESA()
		{
			if((strCustID != null) && (strCustType != null))
			{
				String strESAApply = null;
				if(strCustType == "C")
				{
					Customer customer = new Customer();
					customer.Populate(appID,enterpriseID,strCustID);
					strESAApply = customer.ESASurcharge;
				}
				else if(strCustType == "A")
				{
					Agent agent = new Agent();
					agent.Populate(appID,enterpriseID,strCustID);
					strESAApply = agent.ESASurcharge;
				}
				else if(strCustType == "N")
				{
					strESAApply = "Y";
				}
				if(strESAApply == "Y")
				{
					decimal decSendSrchrg = 0;
					decimal decRecipSrchrg = 0;
					Zipcode zipCode = new Zipcode();
					if(strDestZipCode != null)
					{
						zipCode.Populate(appID,enterpriseID,strDestZipCode);
						decSendSrchrg = zipCode.EASSurcharge;
					}
					else if(strDestZipCode != null)
					{
						zipCode.Populate(appID,enterpriseID,strSendZip);
						decRecipSrchrg = zipCode.EASSurcharge;
					}
					strESAValue = (decSendSrchrg+decRecipSrchrg).ToString("#0.00");
				}
			}
		}
	}
}
