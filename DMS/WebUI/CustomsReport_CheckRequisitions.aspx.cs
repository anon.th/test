using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using TIESDAL;
using System.Text.RegularExpressions;

namespace TIES.WebUI
{
	public class CustomsReport_CheckRequisitions1 : BasePage
	{
		#region Web Form Designer generated code
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnGenerateReport;
		protected System.Web.UI.WebControls.RadioButton rbRequested;
		protected System.Web.UI.WebControls.RadioButton rbApproved;
		protected System.Web.UI.WebControls.RadioButton rbPaid;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtdate;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox txtChequeNumberFrom;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.TextBox txtChequeNumberTo;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.TextBox txtRequisitionNumberFrom;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.TextBox txtRequisitionTo;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.DropDownList ddlRequisitionType;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.TextBox txtPayeeName;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.TextBox txtReceiptNumber;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.Label lbError;
		protected System.Web.UI.WebControls.RadioButton rbtNoDetail;
		protected System.Web.UI.WebControls.RadioButton rbtSomeDetail;
		protected System.Web.UI.WebControls.RadioButton rbtIgnoreThis;
		protected System.Web.UI.WebControls.RadioButtonList rdoSearchDetail;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.TextBox txtInvoicesList;
		protected System.Web.UI.HtmlControls.HtmlTable tbSearchDetail;
		protected System.Web.UI.WebControls.Label lblListNumberDetail;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		#endregion
	
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnGenerateReport.Click += new System.EventHandler(this.btnGenerateReport_Click);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.ddlRequisitionType.SelectedIndexChanged += new System.EventHandler(this.ddlRequisitionType_SelectedIndexChanged);
			this.rdoSearchDetail.SelectedIndexChanged += new System.EventHandler(this.rdoSearchDetail_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private string appID = string.Empty;
		private string enterpriseID = string.Empty;
		private string userLoggin = string.Empty;
		private string culture = string.Empty;

		private DataTable RequisitionTypes
		{
			set{ViewState["RequisitionTypes"]=value;}
			get
			{
				if(ViewState["RequisitionTypes"] ==null)
				{
					ViewState["RequisitionTypes"]=new DataTable();
				}
				return (DataTable)ViewState["RequisitionTypes"];
			}
		}

		private void DdlMonths()
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(appID, culture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);

			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}

		private void DisplayRequisitionType()
		{
			try
			{
				DataSet dsResult = new DataSet();

				DataRow dr = null;
				DataTable dt = new DataTable();
				dt.Columns.Add("DropDownListDisplayValue");
				dt.Columns.Add("CodedValue");

				dr = dt.NewRow();
				dr["DropDownListDisplayValue"] = string.Empty;
				dr["CodedValue"] = string.Empty;

				dt.Rows.Add(dr);

				dsResult =   CustomsCheckRequisitionDAL.GetChequeRequisitionTypes(this.appID, this.enterpriseID,1);
				
				ddlRequisitionType.DataSource = dsResult.Tables[0];
				ddlRequisitionType.DataTextField ="DropDownListDisplayValue";
				ddlRequisitionType.DataValueField ="CodedValue";
				ddlRequisitionType.DataBind();

				RequisitionTypes = dsResult.Tables[0];

				rdoSearchDetail.Items.Clear();
				tbSearchDetail.Visible=false;
			}
			catch(Exception ex)
			{
				this.lbError.Text = ex.Message;
			}
		}

		private bool ValidateValues()
		{
			bool iCheck=false;
	
			if(rbMonth.Checked == true)
			{
				if((ddMonth.SelectedIndex>0) &&(txtYear.Text==""))
				{
					lbError.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((ddMonth.SelectedIndex==0) &&(txtYear.Text!=""))
				{
					lbError.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbPeriod.Checked == true )
			{
				if((txtPeriod.Text!="")&&(txtTo.Text==""))
				{					
					lbError.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_END_DT",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((txtPeriod.Text=="")&&(txtTo.Text!=""))
				{
					lbError.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_START_DT",utility.GetUserCulture());					
					return iCheck=true;			
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbDate.Checked == true )
			{
				if(txtdate.Text=="")
				{					
					lbError.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT",utility.GetUserCulture());	
					return iCheck=true;				
				}
			}			

			return iCheck;
			
		}

		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				System.Text.StringBuilder s = new System.Text.StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings, Page.Session);
				appID = utility.GetAppID();
				enterpriseID = utility.GetEnterpriseID();
				userLoggin = utility.GetUserID();
				culture = utility.GetUserCulture();

				if(!IsPostBack)
				{
					DdlMonths();
					DisplayRequisitionType();
					SetInitialFocus(txtChequeNumberFrom);

					#region "Set Dates"

					rbMonth.Checked = true;
					rbPeriod.Checked = false;
					rbDate.Checked = false;

					ddMonth.Enabled = true;
					ddMonth.SelectedIndex = -1;
					txtYear.Text = null;
					txtYear.Enabled = true;
					txtPeriod.Text = null;
					txtPeriod.Enabled = false;
					txtTo.Text = null;
					txtTo.Enabled = false;
					txtdate.Text = null;
					txtdate.Enabled = false;

					#endregion
				}
			}
			catch(Exception ex)
			{
				this.lbError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}
		private void btnGenerateReport_Click(object sender, System.EventArgs e)
		{
			try
			{
				lbError.Text = string.Empty;

				//if(ValidateValues())
				//	return;

				TIESClasses.ReportCheckRequisitions objInfo = new TIESClasses.ReportCheckRequisitions();
				objInfo.Enterpriseid = this.enterpriseID;

				#region Check and set date

				string startDate = string.Empty;
				string endDate = string.Empty;
				string sStartDate = string.Empty;
				string strMonth = null;

				if (rbMonth.Checked)
				{
					if(ddMonth.SelectedIndex > 0)
						if (txtYear.Text.Trim() == "" || !Regex.IsMatch(txtYear.Text, @"^[0-9]{4}$"))
						{
							lbError.Text = "Year (format: YYYY) is required when month is selected";
							return;
						}
					strMonth = ddMonth.SelectedItem.Value;
					if (strMonth != "" && txtYear.Text != "")
					{
						if(ddMonth.SelectedItem.Value != null && Convert.ToInt32(ddMonth.SelectedItem.Value) < 10)
							sStartDate = "01" + "/0" + ddMonth.SelectedItem.Value + "/" + txtYear.Text;
						else
							sStartDate = "01" + "/" + ddMonth.SelectedItem.Value + "/" + txtYear.Text.Trim();

						
						objInfo.StartDate = System.DateTime.ParseExact(sStartDate,"dd/MM/yyyy",null).ToString("yyyy-MM-dd");
						objInfo.EndDate = System.DateTime.ParseExact(sStartDate,"dd/MM/yyyy",null).AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd");
					}
				} 
				else if(rbPeriod.Checked)
				{
					if(!txtPeriod.Text.Trim().Equals(""))
					{
						if(txtTo.Text.Trim().Equals(""))
						{
							lbError.Text = "Period end date is required";
							return;
						}

						TimeSpan diffPeriod = DateTime.ParseExact(txtTo.Text, "dd/MM/yyyy", null) - DateTime.ParseExact(txtPeriod.Text, "dd/MM/yyyy", null);
						if(diffPeriod.TotalDays < 0)
						{
							lbError.Text = "Period end date must be greater than or equal to period start date";
							return;
						}

						startDate = DateTime.ParseExact(txtPeriod.Text, "dd/MM/yyyy", null).ToString("yyyy-MM-dd");
						
						if(!txtTo.Text.Trim().Equals(""))
							endDate = DateTime.ParseExact(txtTo.Text, "dd/MM/yyyy", null).ToString("yyyy-MM-dd");
						else
							endDate = DateTime.ParseExact(txtPeriod.Text, "dd/MM/yyyy", null).AddDays(1).ToString("yyyy-MM-dd");
					}
					
					objInfo.StartDate = startDate;
					objInfo.EndDate = endDate;
				}
				else if(rbDate.Checked)
				{					
					if(!txtdate.Text.Trim().Equals(""))
					{
						startDate = DateTime.ParseExact(txtdate.Text, "dd/MM/yyyy", null).ToString("yyyy-MM-dd");
						endDate = DateTime.ParseExact(txtdate.Text, "dd/MM/yyyy", null).AddDays(1).ToString("yyyy-MM-dd");
					}
					objInfo.StartDate = startDate;
					objInfo.EndDate = endDate;
				}

				#endregion
				
				string dateType = string.Empty;
				if(rbRequested.Checked)
					dateType = "0";
				else if(rbApproved.Checked)
					dateType = "1";
				else if(rbPaid.Checked)
					dateType = "2";

				string optional = string.Empty;
				if(rbtIgnoreThis.Checked)
					optional = "0";
				else if(rbtNoDetail.Checked)
					optional = "1";
				else if(rbtSomeDetail.Checked)
					optional = "2";

				objInfo.DateType = dateType;
				objInfo.ChequeNo = txtChequeNumberFrom.Text.Trim();
				objInfo.ChequeNoTo = txtChequeNumberTo.Text.Trim();
				objInfo.RequisitionNo =txtRequisitionNumberFrom.Text.Trim();
				objInfo.RequisitionNoTo = txtRequisitionTo.Text.Trim();
				objInfo.ChequeReqType = ddlRequisitionType.SelectedValue;
				objInfo.PayeeName = txtPayeeName.Text.Trim();
				objInfo.ReceiptNumber = txtReceiptNumber.Text.Trim();
				objInfo.ShowCheqReqDetail = optional;
				objInfo.SearchDetailsFor=(rdoSearchDetail.Items.Count>0?rdoSearchDetail.SelectedValue:"");
				objInfo.ListOfItems=txtInvoicesList.Text.Trim();

				//Set and call report
				String strUrl = "ReportViewer.aspx";
				Session["FORMID"] = "CustomsReportCheckRequisitions";
				Session["RptParam"] = objInfo;
				Session["UserLogin"] = userLoggin;
				
				ArrayList paramList = new ArrayList();
				
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindowReport.js", paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			catch(Exception ex)
			{
				this.lbError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtdate.Text = null;
			txtdate.Enabled = false;
			lbError.Text = string.Empty;

			SetInitialFocus(ddMonth);
		}

		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;
			txtdate.Text = null;
			txtdate.Enabled = false;
			ddMonth.SelectedIndex = 0;
			lbError.Text = string.Empty;

			SetInitialFocus(txtPeriod);
		}

		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtdate.Text = null;
			txtdate.Enabled = true;
			ddMonth.SelectedIndex = 0;
			lbError.Text = string.Empty;

			SetInitialFocus(txtdate);
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			rbPaid.Checked = false;
			rbApproved.Checked = false;
			rbRequested.Checked = true;
			
			rbDate.Checked = false;
			rbPeriod.Checked = false;
			rbMonth.Checked = true;

			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtdate.Text = null;
			txtdate.Enabled = false;

			txtChequeNumberFrom.Text = string.Empty;
			txtChequeNumberTo.Text = string.Empty;
			txtRequisitionNumberFrom.Text = string.Empty;
			txtRequisitionTo.Text = string.Empty;
			ddlRequisitionType.SelectedIndex = 0;
			txtPayeeName.Text = string.Empty;
			txtReceiptNumber.Text = string.Empty;
			lbError.Text = string.Empty;
			ddMonth.SelectedIndex = 0;

			rdoSearchDetail.Items.Clear();
			txtInvoicesList.Text="";
			tbSearchDetail.Visible=false;

			SetInitialFocus(txtChequeNumberFrom);
		}

		private void GetRadioSearchList(string desc)
		{	
			DataTable dt = new DataTable();
			dt.Columns.Add("Text");
			dt.Columns.Add("Value");
			string []str = desc.Split(',');
			foreach(string txt in str)
			{
				DataRow dr = dt.NewRow();
				string descrption="";
				if(txt.ToLower().Trim()=="decno")
				{
					descrption="Assessment Number(s)";
				}
				else if(txt.ToLower().Trim()=="invoiceno")
				{
					descrption="Job/Invoice Number(s)";
				}
				else if(txt.ToLower().Trim()=="consno")
				{
					descrption="House AWB Number(s)";
				}
				else if(txt.ToLower().Trim()=="mawb")
				{
					descrption="MAWB";
				}
				dr["Text"]=descrption;
				dr["Value"]=txt;
				dt.Rows.Add(dr);
			}
			rdoSearchDetail.DataSource = dt;
			rdoSearchDetail.DataTextField="Text";
			rdoSearchDetail.DataValueField="Value";
			rdoSearchDetail.DataBind();

			txtInvoicesList.Text="";
			rdoSearchDetail.SelectedIndex=0;
			if(rdoSearchDetail.SelectedValue=="decno")
			{
				lblListNumberDetail.Visible=true;
			}
			SetAttributesTextBoxDetail();
		}

		private void ddlRequisitionType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			txtInvoicesList.Attributes.Clear();
			lblListNumberDetail.Visible=false;
			DataRow[] result = RequisitionTypes.Select("CodedValue ='"+ddlRequisitionType.SelectedValue+"'");
			if(result.Length>0)
			{
				tbSearchDetail.Visible=true;
				GetRadioSearchList(result[0]["Description"].ToString());
			}
			else
			{
				rdoSearchDetail.Items.Clear();
				tbSearchDetail.Visible=false;
			}
			SetInitialFocus(ddlRequisitionType);
		}

		private void rdoSearchDetail_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			lblListNumberDetail.Visible=false;
			SetAttributesTextBoxDetail();
			//RadioButton obj = rdoSearchDetail.SelectedItem as RadioButton;

//			System.Text.StringBuilder s = new System.Text.StringBuilder(); 
//			s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
//			s.Append("getCheckedRadio();");
//			s.Append("</SCRIPT>"); 
//			this.Page.RegisterClientScriptBlock("xxdasd",s.ToString());
			this.RegisterStartupScript(
				"SetControlFocus", 
				string.Format("<script language=\"JavaScript\">document.getElementById('{0}').focus();</script>", "rdoSearchDetail_"+rdoSearchDetail.SelectedIndex.ToString()));
		}

		private void SetAttributesTextBoxDetail()
		{
			txtInvoicesList.Attributes.Clear();
			string txt= rdoSearchDetail.SelectedValue;
			txtInvoicesList.Text="";
			if(txt.ToLower().Trim()=="decno")
			{
				lblListNumberDetail.Visible=true;
				txtInvoicesList.Attributes.Add("onkeypress","ValidateDecNoList(event);");
				txtInvoicesList.Attributes.Add("onpaste","AfterPasteDecNoList(this);");
			}
			else if(txt.ToLower().Trim()=="invoiceno")
			{
				txtInvoicesList.Attributes.Add("onkeypress","ValidateDecNoList(event);");
				txtInvoicesList.Attributes.Add("onpaste","AfterPasteDecNoList(this);");
			}
			else if(txt.ToLower().Trim()=="consno")
			{
				txtInvoicesList.Attributes.Add("onkeypress","ValidateConsignmentList(event);");
				txtInvoicesList.Attributes.Add("onpaste","AfterPasteConsignmentList(this);");
			}
			else if(txt.ToLower().Trim()=="mawb")
			{
				txtInvoicesList.Attributes.Add("onkeypress","ValidateMAWBList(event);");
				txtInvoicesList.Attributes.Add("onpaste","AfterPasteMAWBList(this);");
			}
		}

	}
}
