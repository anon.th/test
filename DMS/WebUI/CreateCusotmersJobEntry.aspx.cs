using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.BAL;
using com.common.applicationpages;
//HC Return Task
using com.ties.classes;
using com.ties.DAL;
//HC Return Task

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CreateCusotmersJobEntry.
	/// </summary>
	public class CreateCusotmersJobEntry : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.Label Label21;
		protected com.common.util.msTextBox txtDateFrom;
		protected System.Web.UI.WebControls.TextBox Textbox2;
		protected System.Web.UI.WebControls.TextBox Textbox3;
		protected System.Web.UI.WebControls.TextBox Textbox4;
		protected System.Web.UI.WebControls.TextBox Textbox5;
		protected System.Web.UI.WebControls.TextBox Textbox6;
		protected System.Web.UI.WebControls.TextBox Textbox7;
		protected System.Web.UI.WebControls.TextBox Textbox8;
		protected System.Web.UI.WebControls.TextBox Textbox9;
		protected System.Web.UI.WebControls.TextBox Textbox11;
		protected System.Web.UI.WebControls.TextBox Textbox12;
		protected System.Web.UI.WebControls.DropDownList service_code;
		protected System.Web.UI.WebControls.TextBox Textbox13;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist3;
		protected System.Web.UI.WebControls.TextBox Textbox18;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist5;
		protected System.Web.UI.WebControls.DropDownList Dropdownlist6;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.WebControls.Label Label24;
		protected System.Web.UI.WebControls.Label Label25;
		protected System.Web.UI.WebControls.Label Label28;
		protected System.Web.UI.WebControls.Label Label29;
		protected System.Web.UI.WebControls.Label Label30;
		protected System.Web.UI.WebControls.Label Label32;
		protected System.Web.UI.WebControls.Label Label33;
		protected System.Web.UI.WebControls.Label Label35;
		protected System.Web.UI.WebControls.Label Label36;
		protected System.Web.UI.WebControls.Label Label37;
		protected System.Web.UI.WebControls.Label Label34;
		protected System.Web.UI.WebControls.Label Label39;
		protected System.Web.UI.WebControls.TextBox txtJobEntryID;
		protected System.Web.UI.WebControls.Label Label27;
		protected System.Web.UI.WebControls.Label Label31;
		protected System.Web.UI.WebControls.Label Label38;
		protected System.Web.UI.WebControls.Label Label40;
		protected System.Web.UI.WebControls.Button btnV2;
		protected System.Web.UI.WebControls.TextBox txtHouseAWBNumber;
		protected System.Web.UI.WebControls.TextBox txtMasterAWBNumber;
		protected System.Web.UI.WebControls.TextBox txtExporter1;
		protected System.Web.UI.WebControls.DropDownList ddlCountry;
		protected System.Web.UI.WebControls.TextBox txtCountryName;
		protected System.Web.UI.WebControls.TextBox txtLoadingPortCity;
		protected System.Web.UI.WebControls.DropDownList ddlLoadingPortCountry;
		protected System.Web.UI.WebControls.TextBox txtLoadingPortCountryName;
		protected System.Web.UI.WebControls.TextBox txtShipFlightNo;
		protected com.common.util.msTextBox txtExpectedArrivalDate;
		protected com.common.util.msTextBox txtActualGoodsArrivalDate;
		protected System.Web.UI.WebControls.DropDownList ddlCurrencyName;
		protected System.Web.UI.WebControls.TextBox txtExchangeRate;
		protected System.Web.UI.WebControls.Button btnV3;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.btnV2.Click += new System.EventHandler(this.btnV2_Click);
			this.btnV3.Click += new System.EventHandler(this.btnV3_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnV2_Click(object sender, System.EventArgs e)
		{
			txtHouseAWBNumber.Text = "543232234";
			txtMasterAWBNumber.Text = "231-2311123";
			txtExporter1.Text = "Microchannel Electronics";
			txtExporter1.Enabled = true;
			ddlCountry.SelectedValue = "AU";
			txtCountryName.Text = "AUSTRALIA";
			txtLoadingPortCity.Text = "BNE";
			ddlLoadingPortCountry.SelectedValue = "AU";
			txtLoadingPortCountryName.Text = "AUSTRALIA";
			txtShipFlightNo.Text = "PX003";
			txtExpectedArrivalDate.Text = "23/05/2014";
			txtActualGoodsArrivalDate.Text = "23/05/2014";
			txtExchangeRate.Text = "0.4023";
			ddlCurrencyName.SelectedValue = "AUD";
			btnSave.Enabled = true;
			btnClose.Enabled = true;
		}

		private void btnV3_Click(object sender, System.EventArgs e)
		{
			lblError.Text="Job entry saved";
			txtJobEntryID.Text="304412";
		}
	}
}
