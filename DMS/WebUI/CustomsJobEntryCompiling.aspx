<%@ Page language="c#" Codebehind="CustomsJobEntryCompiling.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsJobEntryCompiling" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomsJobEntryCompiling</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
			
			var childwindows = new Array();

			function funcDisBack(){
				history.go(+1);
			}
			
			function RemoveBadNonNumberWeight(strTemp, obj) {
			strTemp = strTemp.replace(/[^._0-9]/g, ''); //replace non-numeric
            strTemp = strTemp.replace(/\s/g, ''); // replace space
            
			var res = strTemp.split(".");     			
			for (var i=0; i<res.length-1; i++) {
				if(i==0)
				{
					if (res[i].length > 6) 
					{
						strTemp=res[i].substring(res[i].length - 6);			
					}else{
						strTemp=res[i];
					}
				}
				if(i==1)
				{
					strTemp=strTemp+"."+res[i].substring(0,1);									
				}
			}

			if(res.length == 1)
			{
				strTemp=strTemp.substring(strTemp.length - 6);				
			}				
			obj.value=strTemp;
        }
        
        function AfterPasteNonNumberWeight(obj) {
            setTimeout(function () {
                RemoveBadNonNumberWeight(obj.value, obj);
            }, 1); //or 4
        }			
			
			function RemoveBadJobNumber(strTemp, obj) {
				strTemp = strTemp.replace(/[^-_/_a-zA-Z0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			
			function AfterPasteJobNumber(obj) {
						setTimeout(function () {
							RemoveBadJobNumber(obj.value, obj);
						}, 1); //or 4
					}
			
			function RemoveBadPaseNumber(strTemp, obj) {
				strTemp = strTemp.replace(/[^.0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			
			function AfterPasteNumber(obj) {
						setTimeout(function () {
							RemoveBadPaseNumber(obj.value, obj);
						}, 1); //or 4
					}
			
			
			function RemoveBadNonNumber(strTemp, obj) {
				strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			function AfterPasteNumber(obj) {
				setTimeout(function () {
					RemoveBadNonNumber(obj.value, obj);
				}, 1); //or 4
			}
			 function callback()
			{
					var btn = document.getElementById('btnClientEvent');
					if(btn != null)
					{					
						btn.click();
					}else{
						alert('error call code behind');
					}				        
				
			}
			
			function funcDisBack(){
			history.go(+1);
		}

        function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function RemoveBadMasterAWBNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^-_/_a-zA-Z0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteMasterAWBNumber(obj) {
            setTimeout(function () {
                RemoveBadMasterAWBNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function validate(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		function ValidateMasterAWBNumber(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[-_/_a-zA-Z0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		function closeChildWin(){
			for(var i=0; i<childwindows.length; i++)
			{
				try{
					childwindows[i].close()
				}catch(e){
					
				}
			}
		}
		
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="CustomsJobEntryCompiling" method="post" name="CustomsJobEntryCompiling" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 1081px; HEIGHT: 1331px; TOP: 34px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tbody>
						<tr>
							<td align="left"><asp:label id="Label1" runat="server" Width="421px" Height="27px" CssClass="mainTitleSize">Customs Job Entry Compiling</asp:label></td>
						</tr>
						<tr>
							<td style="HEIGHT: 21px"><asp:button id="btnQry" runat="server" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExecQry" tabIndex="1" runat="server" CssClass="queryButton" Text="Execute Query"
									CausesValidation="False"></asp:button><asp:button id="btnSave" tabIndex="2" runat="server" CssClass="queryButton" Text="Save" CausesValidation="False"
									Enabled="False"></asp:button><asp:button id="btnEntryLines" tabIndex="3" runat="server" CssClass="queryButton" Text="Entry Lines"
									CausesValidation="True" Enabled="False"></asp:button><asp:button id="btnPrintDeclaration" tabIndex="4" runat="server" CssClass="queryButton" Text="Print Declaration"
									CausesValidation="True" Enabled="False"></asp:button><asp:button style="DISPLAY: none" id="btnClientEvent" runat="server" Text="ClientEvent" CausesValidation="False"></asp:button><asp:button style="Z-INDEX: 0; DISPLAY: none" id="btnExecQryHidden" runat="server" Width="130px"
									Text="Execute Query" CausesValidation="False"></asp:button></td>
						</tr>
						<tr>
							<td style="HEIGHT: 18px"><asp:label id="lblErrorMsg" runat="server" ForeColor="Red" Font-Size="X-Small" Font-Bold="True"></asp:label></td>
						</tr>
						<tr>
							<td>
								<table id="Table1">
									<tr>
										<td style="WIDTH: 156px"><asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel">Job Entry Number</asp:label></td>
										<td style="WIDTH: 244px"><cc1:mstextbox onblur="if( this.value !=''){document.getElementById('btnExecQryHidden').click();}"
												style="Z-INDEX: 0" id="txtJobEntryNumber" tabIndex="5" onpaste="AfterPasteNumber(this)" runat="server" Width="224px"
												CssClass="textField" NumberPrecision="10" NumberMinValue="0" NumberMaxValueCOD="999999999999" TextMaskType="msNumericCOD"
												MaxLength="15"></cc1:mstextbox></td>
										<td style="WIDTH: 133px"><asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel">Loading Port</asp:label><asp:label id="Label4" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
										<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtLoadingPort" tabIndex="18"
												onkeypress="ValidateMasterAWBNumber(event)" onpaste="AfterPasteMasterAWBNumber(this)" runat="server"
												Width="50px" CssClass="textField" MaxLength="3"></asp:textbox>&nbsp;<asp:dropdownlist style="Z-INDEX: 0" id="ddlLoadingPort" tabIndex="19" runat="server" Width="50px"
												AutoPostBack="True"></asp:dropdownlist>&nbsp;<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtLoadingPortDes"
												tabIndex="14" runat="server" Width="184px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>&nbsp;</td>
									</tr>
									<tr>
										<td style="WIDTH: 156px; HEIGHT: 6px"><asp:label style="Z-INDEX: 0" id="Label5" runat="server" CssClass="tableLabel">House AWB Number</asp:label><asp:label id="Label59" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
										<td style="WIDTH: 244px; HEIGHT: 6px"><asp:textbox style="Z-INDEX: 0" id="txtHouseAwbNumber" tabIndex="6" onkeypress="ValidateMasterAWBNumber(event)"
												onpaste="AfterPasteMasterAWBNumber(this)" runat="server" Width="224px" CssClass="textField" MaxLength="30"></asp:textbox></td>
										<td style="WIDTH: 133px; HEIGHT: 6px"><asp:label style="Z-INDEX: 0" id="lblDischargePort" runat="server" CssClass="tableLabel">Discharge Port</asp:label><asp:label id="Label7" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
										<td style="HEIGHT: 6px"><asp:dropdownlist style="Z-INDEX: 0" id="ddlDischargePort" tabIndex="20" runat="server" Width="160px"
												Height="20px"></asp:dropdownlist><asp:checkbox style="Z-INDEX: 0" id="cbkDoorToDoor" tabIndex="20" runat="server" CssClass="tableRadioButton"
												Text="Express door-to-door" AutoPostBack="True"></asp:checkbox><asp:label style="Z-INDEX: 0; MARGIN-LEFT: 5px" id="lblOriginCity" runat="server" CssClass="tableLabel">Origin City</asp:label><asp:dropdownlist style="Z-INDEX: 0" id="ddlOriginCity" tabIndex="19" runat="server" Width="60px"
												AutoPostBack="True"></asp:dropdownlist></td>
									</tr>
									<tr>
										<td style="WIDTH: 156px"><asp:label style="Z-INDEX: 0" id="Label8" runat="server" CssClass="tableLabel">Master AWB Number</asp:label><asp:label id="Label9" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
										<td style="WIDTH: 244px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtMasterAwbnumber" tabIndex="7"
												onkeypress="ValidateMasterAWBNumber(event)" onpaste="AfterPasteMasterAWBNumber(this)" runat="server" Width="224px"
												CssClass="textField" MaxLength="30"></asp:textbox></td>
										<td style="WIDTH: 133px"><asp:label style="Z-INDEX: 0" id="Label10" runat="server" CssClass="tableLabel">Bonded Warehouse</asp:label><asp:label id="Label11" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
										<td><asp:dropdownlist style="Z-INDEX: 0" id="ddlBondedWarehouse" tabIndex="21" runat="server" Width="160px"
												AutoPostBack="True"></asp:dropdownlist>&nbsp;<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtBondedWarehouse"
												tabIndex="14" runat="server" Width="224px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
									</tr>
									<TR>
										<TD style="WIDTH: 156px"></TD>
										<TD style="WIDTH: 244px">
											<asp:CheckBox style="Z-INDEX: 0" id="IgnoreMAWBValidation" runat="server" CssClass="tableRadioButton"
												Text="Ignore MAWB validation" tabIndex="7"></asp:CheckBox></TD>
										<TD style="WIDTH: 133px">
											<asp:label style="Z-INDEX: 0" id="Label14" runat="server" CssClass="tableLabel">Currency Name</asp:label>
											<asp:label id="Label15" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
										<TD style="Z-INDEX: 0">
											<asp:dropdownlist style="Z-INDEX: 0" id="ddlCurrencyName" tabIndex="22" runat="server" Width="80px"
												AutoPostBack="True"></asp:dropdownlist>&nbsp;
											<asp:label style="Z-INDEX: 0" id="Label16" runat="server" CssClass="tableLabel">Rate Date</asp:label>&nbsp;
											<cc1:mstextbox id="txtRateDate" tabIndex="23" runat="server" CssClass="textField" Width="114px"
												MaxLength="12" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;
											<asp:label style="Z-INDEX: 0" id="Label17" runat="server" CssClass="tableLabel">Rate</asp:label>&nbsp;
											<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtRate"
												tabIndex="14" runat="server" CssClass="textField" Width="90px" Enabled="False" MaxLength="100"
												ReadOnly="True"></asp:textbox></TD>
									</TR>
									<tr>
										<td style="WIDTH: 404px; HEIGHT: 22px" colSpan="2"><asp:label style="Z-INDEX: 0" id="Label12" runat="server" CssClass="tableLabel">Consignee - ID</asp:label>&nbsp;<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtConsigneeId"
												tabIndex="14" runat="server" Width="114px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>&nbsp;<asp:label style="Z-INDEX: 0" id="Label13" runat="server" CssClass="tableLabel">Tax Code</asp:label>&nbsp;<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtTaxCode"
												tabIndex="14" runat="server" Width="110px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
										<td style="Z-INDEX: 0; WIDTH: 133px; HEIGHT: 22px">
											<asp:label style="Z-INDEX: 0" id="Label19" runat="server" CssClass="tableLabel">Delivery Terms</asp:label>
											<asp:label id="Label18" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
										<td style="Z-INDEX: 0; HEIGHT: 22px">
											<asp:dropdownlist style="Z-INDEX: 0" id="ddlDeliveryTerms" tabIndex="24" runat="server" Width="80px"
												AutoPostBack="True"></asp:dropdownlist>&nbsp;
											<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtDeliveryTerms"
												tabIndex="14" runat="server" CssClass="textField" Width="208px" Enabled="False" MaxLength="100"
												ReadOnly="True"></asp:textbox></td>
									</tr>
									<tr>
										<%--Address--%>
										<td style="WIDTH: 404px" colSpan="2"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtConsigneeName"
												tabIndex="14" runat="server" Width="384px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
										<td style="WIDTH: 133px">
											<asp:label style="Z-INDEX: 0" id="Label20" runat="server" CssClass="tableLabel">Entry Type</asp:label>
											<asp:label id="Label21" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
										<td style="Z-INDEX: 0">
											<asp:dropdownlist style="Z-INDEX: 0" id="ddlEntryType" tabIndex="25" runat="server" Width="144px"
												AutoPostBack="True"></asp:dropdownlist>&nbsp;
											<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtEntryType"
												tabIndex="14" runat="server" CssClass="textField" Width="144px" Enabled="False" MaxLength="100"
												ReadOnly="True"></asp:textbox></td>
									</tr>
									<tr>
										<%--Address--%>
										<td style="WIDTH: 404px" colSpan="2"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtConsignee1"
												tabIndex="14" runat="server" Width="384px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
										<td style="WIDTH: 133px">
											<asp:label style="Z-INDEX: 0" id="Label22" runat="server" CssClass="tableLabel">Customs Agent</asp:label>
											<asp:label id="Label23" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
										<td style="Z-INDEX: 0">
											<asp:dropdownlist style="Z-INDEX: 0" id="ddlCustomsAgent" tabIndex="26" runat="server" Width="144px"
												AutoPostBack="True"></asp:dropdownlist>&nbsp;
											<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtCustomsAgent"
												tabIndex="14" runat="server" CssClass="textField" Width="144px" Enabled="False" MaxLength="100"
												ReadOnly="True"></asp:textbox></td>
									</tr>
									<tr>
										<%--Address--%>
										<td style="WIDTH: 404px" colSpan="2"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtConsignee2"
												tabIndex="14" runat="server" Width="384px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
										<td style="WIDTH: 133px">
											<asp:label style="Z-INDEX: 0" id="Label24" runat="server" CssClass="tableLabel">Number of Pkgs</asp:label>
											<asp:label id="Label26" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
										<td style="Z-INDEX: 0">
											<cc1:mstextbox style="Z-INDEX: 0" id="txtNumberOfPkgs" tabIndex="27" onkeypress="ValidateMasterAWBNumber(event)"
												onpaste="AfterPasteMasterAWBNumber(this)" runat="server" CssClass="textFieldRightAlign" Width="72px"
												MaxLength="6" TextMaskType="msNumericCOD" NumberMaxValueCOD="999999" NumberMinValue="0" NumberPrecision="10"></cc1:mstextbox>&nbsp;
											<asp:label style="Z-INDEX: 0" id="Label27" runat="server" CssClass="tableLabel">Weight (kg)</asp:label>
											<asp:label id="Label25" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>&nbsp;
											<cc1:mstextbox style="Z-INDEX: 0" id="txtWeight" tabIndex="28" onkeypress="ValidateMasterAWBNumber(event)"
												onpaste="AfterPasteNonNumberWeight(this)" runat="server" CssClass="textFieldRightAlign" Width="72px"
												MaxLength="14" TextMaskType="msNumericCOD" NumberMaxValueCOD="999999.9" NumberPrecision="8"
												NumberScale="1"></cc1:mstextbox>&nbsp;
											<asp:label style="Z-INDEX: 0" id="Label65" runat="server" CssClass="tableLabel">Chg Wt</asp:label>&nbsp;
											<cc1:mstextbox style="Z-INDEX: 0" id="txtChargeableWeight" tabIndex="28" onkeypress="ValidateMasterAWBNumber(event)"
												onpaste="AfterPasteNonNumberWeight(this)" runat="server" CssClass="textFieldRightAlign" Width="72px"
												MaxLength="14" TextMaskType="msNumericCOD" NumberMaxValueCOD="999999.9" NumberPrecision="8"
												NumberScale="1"></cc1:mstextbox></td>
									</tr>
									<tr>
										<%--Address--%>
										<td style="WIDTH: 404px; HEIGHT: 21px" colSpan="2"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtConsigneeZipcode"
												tabIndex="14" runat="server" Width="114px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>&nbsp;<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtConsigneeState"
												tabIndex="14" runat="server" Width="264px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
										<td style="Z-INDEX: 0; WIDTH: 133px; HEIGHT: 21px">
											<asp:label style="Z-INDEX: 0" id="Label29" runat="server" CssClass="tableLabel">Goods Description</asp:label>
											<asp:label id="Label28" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
										<td style="HEIGHT: 21px"><asp:textbox style="Z-INDEX: 0" id="txtGoodsDescription" tabIndex="29" runat="server" Width="296px"
												CssClass="textField" MaxLength="100"></asp:textbox></td>
									</tr>
									<tr>
										<%--Address--%>
										<td style="WIDTH: 404px" colSpan="2"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtConsigneeTelephone"
												tabIndex="14" runat="server" Width="205px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
										<td style="WIDTH: 133px"></td>
										<td></td>
									</tr>
									<tr>
										<td style="WIDTH: 404px" colSpan="2"><asp:label style="Z-INDEX: 0" id="Label30" runat="server" CssClass="tableLabel">Exporter</asp:label></td>
										<td rowSpan="5" colSpan="2">
											<fieldset style="Z-INDEX: 0"><legend style="COLOR: black">Customs Procedure Codes
												</legend>
												<table style="MARGIN: 5px; WIDTH: 98%" id="Table2">
													<tbody>
														<tr>
															<td style="WIDTH: 163px; HEIGHT: 23px"><asp:label style="Z-INDEX: 0" id="Label31" runat="server" CssClass="tableLabel">Model of Declaration</asp:label><asp:label id="Label32" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
															<td style="WIDTH: 2px; HEIGHT: 23px"><asp:dropdownlist style="Z-INDEX: 0" id="ddlModelOfDeclaration" tabIndex="30" runat="server" Width="80px"
																	AutoPostBack="True"></asp:dropdownlist></td>
															<td style="HEIGHT: 23px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtModelOfDeclaration"
																	tabIndex="14" runat="server" Width="315px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
														</tr>
														<tr>
															<td style="WIDTH: 163px"><asp:label style="Z-INDEX: 0" id="Label33" runat="server" CssClass="tableLabel">General Procedure Code</asp:label></td>
															<td style="WIDTH: 2px"><asp:dropdownlist style="Z-INDEX: 0" id="ddlGeneralProcedureCode" tabIndex="31" runat="server" Width="80px"
																	AutoPostBack="True"></asp:dropdownlist></td>
															<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtGeneralProcedureCode"
																	tabIndex="14" runat="server" Width="315px" CssClass="textField" Enabled="False" MaxLength="100"
																	ReadOnly="True"></asp:textbox></td>
														</tr>
														<tr>
															<td style="WIDTH: 163px"><asp:label style="Z-INDEX: 0" id="Label34" runat="server" CssClass="tableLabel">Customs Procedure Code</asp:label><asp:label id="Label35" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
															<td style="WIDTH: 2px"><asp:dropdownlist style="Z-INDEX: 0" id="ddlCustomsProcedureCode" tabIndex="32" runat="server" Width="80px"
																	AutoPostBack="True"></asp:dropdownlist></td>
															<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtCustomsProcedureCode"
																	tabIndex="14" runat="server" Width="315px" CssClass="textField" Enabled="False" MaxLength="100"
																	ReadOnly="True"></asp:textbox></td>
														</tr>
														<tr>
															<td style="WIDTH: 163px"><asp:label style="Z-INDEX: 0" id="Label36" runat="server" CssClass="tableLabel">Additional Code</asp:label></td>
															<td style="WIDTH: 2px"><asp:dropdownlist style="Z-INDEX: 0" id="ddlAdditional" tabIndex="33" runat="server" Width="80px"
																	AutoPostBack="True"></asp:dropdownlist></td>
															<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtAdditional"
																	tabIndex="14" runat="server" Width="315px" CssClass="textField" Enabled="False" MaxLength="100"
																	ReadOnly="True"></asp:textbox></td>
														</tr>
													</tbody>
												</table>
											</fieldset>
										</td>
									</tr>
									<tr>
										<td style="WIDTH: 404px" colSpan="2"><asp:label id="Label37" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtExporter1" tabIndex="8" runat="server"
												Width="373px" CssClass="textField" MaxLength="100"></asp:textbox></td>
									</tr>
									<tr>
										<td style="WIDTH: 404px" colSpan="2"><asp:label id="Label40" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtExporter2" tabIndex="9" runat="server"
												Width="373px" CssClass="textField" MaxLength="100"></asp:textbox></td>
									</tr>
									<tr>
										<td style="WIDTH: 404px" colSpan="2"><asp:label style="Z-INDEX: 0; MARGIN-LEFT: 3px" id="Label38" runat="server" CssClass="tableLabel">&nbsp;</asp:label><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtExporter3" tabIndex="10" runat="server"
												Width="373px" CssClass="textField" MaxLength="100"></asp:textbox></td>
									</tr>
									<tr>
										<td><asp:label style="Z-INDEX: 0" id="lblCountry" runat="server" CssClass="tableLabel">Country</asp:label><asp:label id="Label39" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>&nbsp;<asp:dropdownlist style="Z-INDEX: 0" id="ddlCountry" tabIndex="11" runat="server" Width="90px" AutoPostBack="True"></asp:dropdownlist></td>
										<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtCountry"
												tabIndex="14" runat="server" Width="224px" CssClass="textField" Enabled="False" MaxLength="100"
												ReadOnly="True"></asp:textbox></td>
									</tr>
									<tr>
										<td style="WIDTH: 156px; HEIGHT: 8px"><asp:label style="Z-INDEX: 0" id="Label41" runat="server" CssClass="tableLabel">Info Received Date</asp:label><asp:label id="Label42" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
										<td style="WIDTH: 244px; HEIGHT: 8px"><cc1:mstextbox id="txtInfoReceivedDate" tabIndex="12" runat="server" Width="136px" CssClass="textField"
												TextMaskType="msDate" MaxLength="12" TextMaskString="99/99/9999"></cc1:mstextbox></td>
										<td vAlign="top" rowSpan="8" colSpan="2">
											<table id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
												<tr>
													<td style="WIDTH: 314px" vAlign="top">
														<fieldset style="WIDTH: 98%"><legend style="COLOR: black">Customs Declarations</legend>
															<table style="MARGIN: 5px; WIDTH: 98%" id="Table5">
																<tr>
																	<td style="WIDTH: 162px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label47" runat="server" CssClass="tableLabel">Profile</asp:label><asp:label style="Z-INDEX: 0" id="Label48" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
																	<td style="WIDTH: 172px"><asp:dropdownlist style="Z-INDEX: 0" id="ddlProfile" tabIndex="34" runat="server" Width="114px" AutoPostBack="True"></asp:dropdownlist></td>
																</tr>
																<tr>
																	<td style="WIDTH: 162px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label45" runat="server" CssClass="tableLabel">Declaration Number</asp:label></td>
																	<td style="WIDTH: 172px"><asp:textbox style="Z-INDEX: 0" id="txtDeclartionNumber" tabIndex="35" onkeypress="ValidateMasterAWBNumber(event)"
																			onpaste="AfterPasteMasterAWBNumber(this)" runat="server" CssClass="textField" MaxLength="20"></asp:textbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 162px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label43" runat="server" CssClass="tableLabel">Date of Assessment</asp:label></td>
																	<td style="WIDTH: 172px"><cc1:mstextbox style="Z-INDEX: 0" id="txtDateOfAssessment" tabIndex="36" runat="server" Width="112px"
																			CssClass="textField" TextMaskType="msDate" MaxLength="12" TextMaskString="99/99/9999"></cc1:mstextbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 162px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label55" runat="server" CssClass="tableLabel">Amount of Assessment</asp:label></td>
																	<td style="WIDTH: 172px; HEIGHT: 21px"><cc1:mstextbox style="Z-INDEX: 0" id="AssessmentAmount" tabIndex="37" onpaste="AfterPasteMasterAWBNumber(this)"
																			runat="server" Width="112px" CssClass="textFieldRightAlign" NumberPrecision="10" NumberMinValue="0" NumberMaxValueCOD="99999999.99"
																			TextMaskType="msNumericCOD" MaxLength="11" AutoPostBack="True" NumberScale="2"></cc1:mstextbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 338px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label50" runat="server" CssClass="tableLabel">Declaration Form Pages</asp:label></td>
																	<td><cc1:mstextbox style="Z-INDEX: 0" id="txtDeclartionFromPages" tabIndex="38" onkeypress="ValidateMasterAWBNumber(event)"
																			onpaste="AfterPasteMasterAWBNumber(this)" runat="server" Width="112px" CssClass="textFieldRightAlign"
																			NumberPrecision="7" NumberMinValue="0" NumberMaxValueCOD="9999999" TextMaskType="msNumericCOD"
																			MaxLength="7" AutoPostBack="True"></cc1:mstextbox></td>
																</tr>
																<TR>
																	<TD style="WIDTH: 338px; HEIGHT: 21px"></TD>
																	<TD></TD>
																</TR>
															</table>
														</fieldset>
													</td>
													<td style="WIDTH: 260px" vAlign="top">
														<fieldset style="WIDTH: 98%"><legend style="COLOR: black">Invoice Details</legend>
															<table style="MARGIN: 5px; WIDTH: 98%" id="Table6">
																<tr>
																	<td style="WIDTH: 62px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label46" runat="server" CssClass="tableLabel">Duty</asp:label></td>
																	<td><cc1:mstextbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtDuty"
																			tabIndex="37" runat="server" Width="114px" CssClass="textFieldRightAlign" Enabled="False" NumberPrecision="10"
																			NumberMinValue="0" NumberMaxValueCOD="999999999" TextMaskType="msNumericCOD" MaxLength="10"
																			AutoPostBack="True"></cc1:mstextbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 62px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label44" runat="server" CssClass="tableLabel">Tax</asp:label></td>
																	<td style="HEIGHT: 21px"><cc1:mstextbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtTax"
																			tabIndex="37" runat="server" Width="114px" CssClass="textFieldRightAlign" Enabled="False" NumberPrecision="10" NumberMinValue="0"
																			NumberMaxValueCOD="999999999" TextMaskType="msNumericCOD" MaxLength="10" AutoPostBack="True"></cc1:mstextbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 62px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label49" runat="server" CssClass="tableLabel">Excise</asp:label></td>
																	<td><cc1:mstextbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtExcise"
																			tabIndex="37" runat="server" Width="114px" CssClass="textFieldRightAlign" Enabled="False" NumberPrecision="10"
																			NumberMinValue="0" NumberMaxValueCOD="999999999" TextMaskType="msNumericCOD" MaxLength="10"
																			AutoPostBack="True"></cc1:mstextbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 62px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label51" runat="server" CssClass="tableLabel">EPF</asp:label></td>
																	<td style="HEIGHT: 20px"><cc1:mstextbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtEPF"
																			tabIndex="28" runat="server" Width="114px" CssClass="textFieldRightAlign" Enabled="False" NumberPrecision="10" NumberMinValue="0"
																			NumberMaxValueCOD="999999999" TextMaskType="msNumericCOD" MaxLength="10" AutoPostBack="True"></cc1:mstextbox></td>
																</tr>
																<tr>
																	<td style="WIDTH: 62px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label52" runat="server" CssClass="tableLabel">Total</asp:label></td>
																	<td><cc1:mstextbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtTotal"
																			tabIndex="37" runat="server" Width="114px" CssClass="textFieldRightAlign" Enabled="False" NumberPrecision="10"
																			NumberMinValue="0" NumberMaxValueCOD="999999999" TextMaskType="msNumericCOD" MaxLength="10"
																			AutoPostBack="True"></cc1:mstextbox></td>
																</tr>
																<TR>
																	<TD style="WIDTH: 62px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label63" runat="server" CssClass="tableLabel">Status</asp:label></TD>
																	<TD><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtInvoiceStatus"
																			tabIndex="37" runat="server" Width="170px" CssClass="textField" Enabled="False" MaxLength="200"
																			ReadOnly="True"></asp:textbox></TD>
																</TR>
															</table>
														</fieldset>
													</td>
												</tr>
												<TR>
													<TD style="WIDTH: 314px" vAlign="top">
														<FIELDSET style="Z-INDEX: 0; WIDTH: 98%; HEIGHT: 125px"><LEGEND style="COLOR: black">History</LEGEND>
															<TABLE style="MARGIN: 5px; WIDTH: 98%" id="Table7">
																<TR>
																	<TD style="WIDTH: 80px; HEIGHT: 21px">
																		<asp:label style="Z-INDEX: 0" id="Label72" runat="server" CssClass="tableLabel">Registration</asp:label></TD>
																	<TD>
																		<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="txtHistory_RegisteredBy" tabIndex="14"
																			runat="server" CssClass="textField" Width="80px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>&nbsp;
																		<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="txtHistory_RegisteredDT" tabIndex="14"
																			runat="server" CssClass="textField" Width="110px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 80px; HEIGHT: 22px">
																		<asp:label style="Z-INDEX: 0" id="Label70" runat="server" CssClass="tableLabel"> Compiling</asp:label></TD>
																	<TD>
																		<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="txtHistory_CompiledBy" tabIndex="14"
																			runat="server" CssClass="textField" Width="80px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>&nbsp;
																		<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="txtHistory_CompiledDT" tabIndex="14"
																			runat="server" CssClass="textField" Width="110px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 80px; HEIGHT: 21px">
																		<asp:label style="Z-INDEX: 0" id="Label69" runat="server" CssClass="tableLabel"> Last Updated</asp:label></TD>
																	<TD>
																		<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="txtHistory_LastUpdatedBy" tabIndex="14"
																			runat="server" CssClass="textField" Width="80px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>&nbsp;
																		<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="txtHistory_LastUpdatedDT" tabIndex="14"
																			runat="server" CssClass="textField" Width="110px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></TD>
																</TR>
															</TABLE>
														</FIELDSET>
													</TD>
													<TD style="WIDTH: 260px" vAlign="top"><FONT face="Tahoma"></FONT></TD>
												</TR>
											</table>
										</td>
									</tr>
									<tr>
										<td style="WIDTH: 156px"><asp:label style="Z-INDEX: 0" id="Label53" runat="server" CssClass="tableLabel">Ship/Flight No</asp:label><asp:label id="Label54" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
										<td style="WIDTH: 244px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtShipFlightNo" tabIndex="13"
												onkeypress="ValidateMasterAWBNumber(event)" onpaste="AfterPasteMasterAWBNumber(this)" runat="server" Width="224px"
												CssClass="textField" MaxLength="40"></asp:textbox></td>
									</tr>
									<tr>
										<td style="WIDTH: 156px"><asp:label style="Z-INDEX: 0" id="Label56" runat="server" CssClass="tableLabel">Expected Arrival Date</asp:label><asp:label id="Label57" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
										<td style="WIDTH: 244px"><cc1:mstextbox id="txtExpectedArrivalDate" tabIndex="14" runat="server" Width="136px" CssClass="textField"
												TextMaskType="msDate" MaxLength="12" TextMaskString="99/99/9999"></cc1:mstextbox></td>
									</tr>
									<tr>
										<td style="WIDTH: 156px; HEIGHT: 24px"><asp:label style="Z-INDEX: 0" id="Label58" runat="server" CssClass="tableLabel">Actual Goods Arrival Date</asp:label></td>
										<td style="WIDTH: 244px"><cc1:mstextbox id="txtActualGoodsArrivalDate" tabIndex="15" runat="server" Width="136px" CssClass="textField"
												TextMaskType="msDate" MaxLength="12" TextMaskString="99/99/9999"></cc1:mstextbox></td>
									</tr>
									<tr>
										<td style="WIDTH: 156px"><asp:label style="Z-INDEX: 0" id="Label60" runat="server" CssClass="tableLabel">Mode of Transport</asp:label><asp:label id="Label61" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></td>
										<td style="WIDTH: 244px"><asp:dropdownlist style="Z-INDEX: 0" id="ddlModeOfTransport" tabIndex="16" runat="server" Width="50px"
												AutoPostBack="True"></asp:dropdownlist>&nbsp;<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="txtModeOfTransport"
												tabIndex="14" runat="server" Width="168px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
									</tr>
									<tr>
										<td style="WIDTH: 156px"><asp:label style="Z-INDEX: 0" id="Label62" runat="server" CssClass="tableLabel">Folio No</asp:label></td>
										<td style="WIDTH: 244px"><asp:textbox style="Z-INDEX: 0" id="txtFolioNo" tabIndex="17" onkeypress="ValidateMasterAWBNumber(event)"
												onpaste="AfterPasteMasterAWBNumber(this)" runat="server" Width="136px" CssClass="textField" MaxLength="20"></asp:textbox></td>
									</tr>
									<TR>
										<TD style="WIDTH: 156px" vAlign="top"><FONT face="Tahoma"><asp:label style="Z-INDEX: 0" id="Label66" runat="server" CssClass="tableLabel">Remarks</asp:label></FONT></TD>
										<TD style="WIDTH: 244px"><FONT face="Tahoma"><asp:textbox style="Z-INDEX: 0" id="txtRemark" tabIndex="17" onkeypress="if (this.value.length > 200) { return false; }"
													onkeyup="if (this.value.length > 200) { this.value=this.value.substring(0,200) }" onpaste="if (this.value.length > 200) { return false; }"
													runat="server" Width="224px" Rows="8" CssClass="textField" MaxLength="200" TextMode="MultiLine"></asp:textbox></FONT></TD>
									</TR>
									<TR>
										<TD colSpan="2" valign="bottom">
											<table id="Table8" border="0" cellSpacing="0" cellPadding="0" width="100%">
												<tr>
													<td style="WIDTH: 110px"><asp:button id="btnCharges" tabIndex="38" runat="server" CssClass="queryButton" Text="Charges"
															CausesValidation="False" Enabled="False"></asp:button></td>
													<td><asp:label style="Z-INDEX: 0" id="lblMaximumEntries" runat="server" CssClass="tableLabel">Maximum Entries</asp:label><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea; MARGIN-LEFT: 5px"
															id="txtMaximumEntries" tabIndex="14" runat="server" Width="32px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox><asp:label style="Z-INDEX: 0" id="lblRemainingEntries" runat="server" CssClass="tableLabel">Remaining Entries</asp:label><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea; MARGIN-LEFT: 5px"
															id="txtRemainingEntries" tabIndex="14" runat="server" Width="32px" CssClass="textField" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></td>
												</tr>
											</table>
										</TD>
									</TR>
								</table>
							</td>
						</tr>
						<tr>
							<TD style="HEIGHT: 23px"><asp:label id="lblError" runat="server" ForeColor="Red" Font-Size="Smaller"></asp:label></TD>
						</tr>
						<tr>
							<td style="HEIGHT: 23px"><asp:label id="lblHeader" runat="server" Width="65%" ForeColor="White" Font-Bold="True" BackColor="#669999">Marks & Nos.</asp:label></td>
						</tr>
						<tr>
							<td><asp:datagrid style="Z-INDEX: 0" id="gvMarksAndNo" tabIndex="39" runat="server" Width="65%" ShowFooter="True"
									AutoGenerateColumns="False" DataKeyField="SeqNo" AlternatingItemStyle-CssClass="gridField"
									ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridHeading" CellPadding="0" CellSpacing="1"
									BorderWidth="0px" FooterStyle-CssClass="gridHeading">
									<FooterStyle CssClass="gridHeading"></FooterStyle>
									<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
									<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
									<ItemStyle Height="15px" CssClass="gridField"></ItemStyle>
									<HeaderStyle Font-Bold="True" HorizontalAlign="Center" Width="2%" CssClass="gridHeading"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn>
											<HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center" Height="24px"></ItemStyle>
											<ItemTemplate>
												<asp:LinkButton id="btnEditItem" Runat="server" Text="<img src='images/butt-edit.gif' alt='edit' border=0>"
													CommandName="EDIT_ITEM" />
												<asp:LinkButton id="btnDeleteItem" Runat="server" Text="<img src='images/butt-delete.gif' alt='delete' border=0>"
													CommandName="DELETE_ITEM" />
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Center"></FooterStyle>
											<FooterTemplate>
												<asp:LinkButton id="btnAddItem" Runat="server" Text="<img src='images/butt-add.gif' alt='add' border=0>"
													CommandName="ADD_ITEM" />
											</FooterTemplate>
											<EditItemTemplate>
												<asp:LinkButton id="Linkbutton2" Runat="server" Text="<img src='images/butt-update.gif' alt='save' border=0>"
													CommandName="SAVE_ITEM" />
												<asp:LinkButton id="Linkbutton3" Runat="server" Text="<img src='images/butt-red.gif' alt='cancel' border=0>"
													CommandName="CANCEL_ITEM" />
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Seq No">
											<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblSeq" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"SeqNo")%>' Runat="server">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:textbox id="txtMarkSeq" runat="server" Enabled=False CssClass="gridText" MaxLength="20" Width="95%" Text='<%#DataBinder.Eval(Container.DataItem,"SeqNo")%>'>
												</asp:textbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Code">
											<HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center" Height="15px"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"MarksNos_Code") %>
											</ItemTemplate>
											<FooterTemplate>
												<asp:textbox id="txtCode" runat="server" CssClass="textField" MaxLength="12" Width="95%"></asp:textbox>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:textbox id="txtMarksNos_Code" runat="server" CssClass="textField" MaxLength="12" Width="95%" Text='<%#DataBinder.Eval(Container.DataItem,"MarksNos_Code")%>'>
												</asp:textbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Description">
											<HeaderStyle HorizontalAlign="Left" Width="60%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Left"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"Description") %>
											</ItemTemplate>
											<FooterTemplate>
												<asp:textbox id="txtDescription" runat="server" CssClass="textField" MaxLength="100" Width="95%"></asp:textbox>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:textbox id="txtMarkDescription" runat="server" CssClass="textField" MaxLength="100" Width="95%" Text='<%#DataBinder.Eval(Container.DataItem,"Description")%>'>
												</asp:textbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
								</asp:datagrid><asp:datagrid style="Z-INDEX: 0" id="gvCharge" tabIndex="42" runat="server" Width="65%" AutoGenerateColumns="False"
									DataKeyField="SeqNo" AlternatingItemStyle-CssClass="gridField" ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridHeading"
									CellPadding="0" CellSpacing="1" BorderWidth="0px" FooterStyle-CssClass="gridHeading" PageSize="6">
									<FooterStyle CssClass="gridHeading"></FooterStyle>
									<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn>
											<HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:LinkButton id="btnEditItem" Runat="server" Text="<img src='images/butt-edit.gif' alt='edit' border=0>"
													CommandName="EDIT_ITEM" />
											</ItemTemplate>
											<EditItemTemplate>
												<asp:LinkButton id="btnSaveItem" Runat="server" Text="<img src='images/butt-update.gif' alt='save' border=0>"
													CommandName="SAVE_ITEM" />
												<asp:LinkButton id="btnCancelItem" Runat="server" Text="<img src='images/butt-red.gif' alt='cancel' border=0>"
													CommandName="CANCEL_ITEM" />
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Seq No">
											<HeaderStyle HorizontalAlign="Center" Width="8%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblSeqNo" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"SeqNo")%>' Runat="server">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:textbox id="txtEditSeqNo" runat="server" Enabled=False CssClass="textField" MaxLength="20" Width="95%" Text='<%#DataBinder.Eval(Container.DataItem,"SeqNo")%>'>
												</asp:textbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Code">
											<HeaderStyle HorizontalAlign="Center" Width="14%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<%# DataBinder.Eval(Container.DataItem,"Charges_Code") %>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:textbox id="txtCharges_Code" runat="server" Enabled=False CssClass="gridTextBoxNumber" MaxLength="12" Width="95%" Text='<%#DataBinder.Eval(Container.DataItem,"Charges_Code")%>'>
												</asp:textbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Amount">
											<HeaderStyle HorizontalAlign="Right" Width="14%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblChargeAmount" Runat=server Text='<%#DataBinder.Eval(Container.DataItem,"Amount","{0:###,##0.00}")%>'>
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<cc1:mstextbox tabIndex="101" CssClass="gridTextBoxNumber" ID="txtChargeAmount" onpaste="AfterPasteNonNumber(this)" Runat="server" Enabled="True" TextMaskType="msNumeric" MaxLength="7" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="6" NumberScale="2" Text='<%#DataBinder.Eval(Container.DataItem,"Amount","{0:###,##0.00}")%>'>
												</cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Currency">
											<HeaderStyle HorizontalAlign="Center" Width="14%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblCurrencyCode" Runat=server Text='<%#DataBinder.Eval(Container.DataItem,"CurrencyCode")%>'>
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:dropdownlist style="Z-INDEX: 0" id="ddlChargeCurrencyCode" tabIndex="102" runat="server" Width="160px"
													OnSelectedIndexChanged="ddlChargeCurrencyCode_SelectedIndexChanged" AutoPostBack="True"></asp:dropdownlist>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Rate">
											<HeaderStyle HorizontalAlign="Right" Width="16%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lblChargeExchange_Rate" Runat=server Text='<%#DataBinder.Eval(Container.DataItem,"Exchange_Rate") %>'>
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:textbox id="txtExchange_Rate" runat="server" Enabled=False CssClass="gridTextBoxNumber" MaxLength="20" Width="95%" Text='<%#DataBinder.Eval(Container.DataItem,"Exchange_Rate")%>'>
												</asp:textbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Valuation">
											<HeaderStyle HorizontalAlign="Right" Width="16%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="Label6" Runat=server Text='<%# DataBinder.Eval(Container.DataItem,"Valuation","{0:###,##0.00}") %>'>
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtValuation" onpaste="AfterPasteNonNumber(this)" Runat="server" Enabled="False" TextMaskType="msNumeric" MaxLength="20" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="6" NumberScale="2" Text='<%#DataBinder.Eval(Container.DataItem,"Valuation","{0:###,##0.00}")%>'>
												</cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
								</asp:datagrid></td>
						</tr>
					</tbody>
				</TABLE>
			</div>
			<DIV style="Z-INDEX: 0; POSITION: relative; WIDTH: 625px; HEIGHT: 217px; TOP: 34px; LEFT: 22px"
				id="divDangerous" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="POSITION: absolute; WIDTH: 545px; HEIGHT: 107px; TOP: 20px; LEFT: 21px" id="Table4"
					runat="server">
					<TR>
						<TD>
							<P align="center"><asp:label id="Label64" runat="server" Width="500px" Height="36px">Changing the Entry Type to FORMAL requires that all Charges and Entry Lines be deleted. &nbsp; Do you wish to proceed with this change?</asp:label></P>
							<P align="center"><asp:button id="btnDangerousYes" runat="server" CssClass="queryButton" Text="Yes" CausesValidation="False"></asp:button>&nbsp;
								<asp:button id="btnDangerousNo" runat="server" CssClass="queryButton" Text=" No " CausesValidation="False"></asp:button></P>
						</TD>
					</TR>
				</TABLE>
			</DIV>
			<INPUT 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition> <INPUT type="hidden" name="hdnRefresh">
		</form>
	</body>
</HTML>
