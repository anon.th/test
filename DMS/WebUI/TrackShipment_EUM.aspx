<%@ Page language="c#" Codebehind="TrackShipment_EUM.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.TrackShipment_EUM" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentStatus</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript">
			function SetInitialFocus(name)
			{			
				var contrl = document.getElementById(name);
				contrl.focus();
			}
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="PostDeliveryCOD" method="post" runat="server">
			<P><asp:label id="lblMainTitle" style="Z-INDEX: 102; POSITION: absolute; TOP: 8px; LEFT: 11px"
					runat="server" CssClass="mainTitleSize" Width="477px" Height="26px">Track and Trace</asp:label></P>
			<DIV id="divMain" style="Z-INDEX: 101; POSITION: absolute; WIDTH: 838px; HEIGHT: 218px; TOP: 23px; LEFT: 11px"
				MS_POSITIONING="GridLayout" runat="server">
				<TABLE id="MainTable" style="Z-INDEX: 101; POSITION: absolute; WIDTH: 838px; HEIGHT: 157px; TOP: 5px; LEFT: 0px"
					width="838" border="0" runat="server">
					<TR>
						<TD style="HEIGHT: 20px" vAlign="top" width="738"></TD>
					</TR>
					<TR vAlign="top">
						<TD vAlign="top" width="738" style="HEIGHT: 44px"><asp:button id="btnQry" runat="server" CssClass="queryButton" Width="61px" CausesValidation="False"
								Text="Query"></asp:button><asp:button id="btnExecQry" runat="server" CssClass="queryButton" Width="130px" CausesValidation="False"
								Text="Execute Query"></asp:button>&nbsp;
							<DIV style="WIDTH: 333px; DISPLAY: inline; HEIGHT: 19px" ms_positioning="FlowLayout"></DIV>
						</TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 38px" vAlign="top" width="738">
							<asp:label id="lblErrorMsg" runat="server" CssClass="errorMsgColor" Width="830px" Height="19px"></asp:label></TD>
					</TR>
					<TR>
						<TD width="738">
							<TABLE id="TblBookDtl" style="WIDTH: 737px" width="737" border="0" runat="server">
								<TR>
									<TD style="WIDTH: 196px" width="180">
										<asp:label style="Z-INDEX: 0" id="Label1" runat="server" Height="22px" Width="180px" CssClass="tableLabel">Transporter</asp:label></TD>
									<TD style="WIDTH: 210px" width="156">
										<asp:dropdownlist style="Z-INDEX: 0" id="ddlTransporter" tabIndex="16" runat="server" Width="150px"
											AutoPostBack="True">
											<asp:ListItem Value="BRAIN">BRAIN</asp:ListItem>
											<asp:ListItem Value="TURNER">TURNER</asp:ListItem>
										</asp:dropdownlist></TD>
									<TD style="WIDTH: 102px" width="102"></TD>
									<TD style="WIDTH: 191px" width="191"></TD>
									<TD style="WIDTH: 146px" width="146"></TD>
								</TR>
								<TR height="25">
									<TD style="WIDTH: 196px" width="180">
										<asp:label id="lblConsgmtNo" runat="server" Height="22px" Width="180px" CssClass="tableLabel">Consignment Number</asp:label></TD>
									<td style="WIDTH: 210px" width="156"><asp:textbox id="txtConsigNo" style="TEXT-TRANSFORM: uppercase" tabIndex="4" runat="server" CssClass="textField"
											Width="150px" MaxLength="20"></asp:textbox></td>
									<td style="WIDTH: 102px" width="102"></td>
									<TD style="WIDTH: 191px" width="191"></TD>
									<td style="WIDTH: 146px" width="146"></td>
								</TR>
								<TR>
									<TD style="WIDTH: 180px" width="180"><asp:label id="lblRefNo" runat="server" CssClass="tableLabel" Width="180px" Height="22px">Customer Reference Number</asp:label></TD>
									<TD style="WIDTH: 210px" width="156"><asp:textbox id="txtCustRefNo" style="TEXT-TRANSFORM: uppercase" tabIndex="5" runat="server"
											CssClass="textField" Width="150px" MaxLength="20" AutoPostBack="True"></asp:textbox></TD>
									<TD style="WIDTH: 102px" width="102"></TD>
									<TD style="WIDTH: 191px" width="191"></TD>
									<TD style="WIDTH: 146px" width="146"></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
			</DIV>
		</form>
	</body>
</HTML>
