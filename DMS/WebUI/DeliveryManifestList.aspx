<%@ Page language="c#" Codebehind="DeliveryManifestList.aspx.cs" AutoEventWireup="false" Inherits="com.ties.DeliveryManifestList" enableViewState="True" SmartNavigation="False" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DeliveryManifestList</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onunload="window.opener.top.displayBanner.fnCloseAll(1);">
		<form id="DeliveryManifestList" method="post" runat="server">
			<asp:label id="lblMainTitle" style="Z-INDEX: 102; LEFT: 4px; POSITION: absolute; TOP: 4px" runat="server" CssClass="mainTitleSize" Height="26px" Width="477px">Query Result on Delivery Manifest</asp:label>
			<div id="gridPanel" style="Z-INDEX: 103; LEFT: 0px; WIDTH: 833px; POSITION: relative; TOP: 49px; HEIGHT: 578px" runat="server"><asp:button id="btnQry" tabIndex="1" runat="server" Text="Query" CausesValidation="False" CssClass="queryButton" Width="64px"></asp:button><asp:button id="btnDelete" tabIndex="2" runat="server" Text="Delete" CausesValidation="False" CssClass="queryButton" Width="64px"></asp:button><asp:button id="btnPrint" tabIndex="3" runat="server" Text="Print" CausesValidation="False" CssClass="queryButton" Width="64px"></asp:button><asp:Button id="btnCheckAll" runat="server" Width="102px" Text="Check All" CssClass="queryButton"></asp:Button><asp:Button id="btnClearAll" runat="server" Width="82px" Text="Clear All" CssClass="queryButton"></asp:Button>
				<asp:datagrid id="dgDMList" runat="server" Width="825px" AutoGenerateColumns="False" AllowPaging="True" AllowCustomPaging="True" OnPageIndexChanged="OnPaging_DMList" OnItemCommand="dgDMList_Button">
					<SelectedItemStyle Font-Size="10pt" CssClass="gridFieldSelected"></SelectedItemStyle>
					<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
					<HeaderStyle CssClass="gridHeading"></HeaderStyle>
					<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
					<Columns>
						<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;" ButtonType="LinkButton" CommandName="Select">
							<ItemStyle HorizontalAlign="Center"></ItemStyle>
						</asp:ButtonColumn>
						<asp:TemplateColumn>
							<ItemTemplate>
								<asp:CheckBox ID="chkSelect" Runat="server"></asp:CheckBox>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:ButtonColumn DataTextField="Path_Code" HeaderText="Path" CommandName="Choose">
							<ItemStyle Width="5%"></ItemStyle>
						</asp:ButtonColumn>
						<asp:BoundColumn Visible="False" DataField="Path_Code" HeaderText="Path">
							<ItemStyle></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Delivery_Type" HeaderText="Type">
							<ItemStyle HorizontalAlign="Center" Width="3%"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Departure_DateTime" HeaderText="Dep. Date" DataFormatString="{0:dd/MM/yyyy HH:mm}">
							<ItemStyle Width="16%"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Flight_Vehicle_No" HeaderText="Veh/Flt No">
							<ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="AWB_Driver_Name" HeaderText="Driver/AWB">
							<ItemStyle HorizontalAlign="Left" Width="16%"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Total_Consignment" HeaderText="Tot. Consmnt">
							<ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Total_Weight" HeaderText="Total Chg. Wt" DataFormatString="{0:n}">
							<ItemStyle HorizontalAlign="Right" Width="12%"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Total_Package" HeaderText="Tot. Pkg">
							<ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn Visible="False" DataField="Printed_DateTime" HeaderText="Print Date" DataFormatString="{0:dd/MM/yyyy HH:mm}">
							<ItemStyle></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn Visible="False" DataField="Path_Description" HeaderText="Path Desc">
							<ItemStyle></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Delivery_Manifest_DateTime" HeaderText="Manifest Date" DataFormatString="{0:dd/MM/yyyy HH:mm}">
							<ItemStyle Width="16%"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn Visible="False" DataField="Line_Haul_Cost" HeaderText="Line Haul Cost" DataFormatString="{0:n}">
							<ItemStyle></ItemStyle>
						</asp:BoundColumn>
					</Columns>
					<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
				</asp:datagrid><asp:datagrid id="dgConsignment" runat="server" Width="825px" AutoGenerateColumns="False" AllowPaging="True" AllowCustomPaging="True" OnPageIndexChanged="OnPaging_ConsignmentList" OnItemCommand="dgCons_Delete">
					<ItemStyle CssClass="gridField"></ItemStyle>
					<HeaderStyle CssClass="gridHeading"></HeaderStyle>
					<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
					<Columns>
						<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
							<ItemStyle HorizontalAlign="Center" Width="3%"></ItemStyle>
						</asp:ButtonColumn>
						<asp:BoundColumn DataField="Consignment_No" HeaderText="Consignment No.">
							<ItemStyle Width="22%"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Origin_State_Code" HeaderText="ORG">
							<ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Destination_State_Code" HeaderText="DST">
							<ItemStyle Width="10%"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Route_Code" HeaderText="Route">
							<ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Chargeable_Wt" HeaderText="Chargeable Weight" DataFormatString="{0:n}">
							<ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Pkg_Vol" HeaderText="Volume" DataFormatString="{0:n}">
							<ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Tot_Pkg" HeaderText="Total Pkg">
							<ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
						</asp:BoundColumn>
					</Columns>
					<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
				</asp:datagrid></div>
			<DIV id="msgPanel" style="CLEAR: both; Z-INDEX: 104; LEFT: 0px; WIDTH: 472px; POSITION: relative; TOP: 49px; HEIGHT: 119px" runat="server" ms_positioning="GridLayout"><asp:label id="lblMessage" style="Z-INDEX: 101; LEFT: 27px; POSITION: absolute; TOP: 17px" runat="server" CssClass="tableLabel" Height="46px" Width="420px">You have some checkboxes checked.<br> Page Navigation will clear them, Are you sure?</asp:label><asp:button id="btnOk" style="Z-INDEX: 102; LEFT: 80px; POSITION: absolute; TOP: 64px" runat="server" Text="Ok" Width="56px" CssClass="queryButton"></asp:button><asp:button id="btnCancel" style="Z-INDEX: 103; LEFT: 136px; POSITION: absolute; TOP: 64px" runat="server" Text="Cancel" Width="64px" CssClass="queryButton"></asp:button></DIV>
			<asp:label id="lblErrorMsg" style="Z-INDEX: 101; LEFT: 9px; POSITION: absolute; TOP: 40px" runat="server" CssClass="errorMsgColor" Height="19px" Width="566px"></asp:label></form>
	</body>
</HTML>
