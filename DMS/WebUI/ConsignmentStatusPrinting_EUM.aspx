<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="ConsignmentStatusPrinting_EUM.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.ConsignmentStatusPrinting_EUM" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Consignment Status Printing</title>
		<meta content="True" name="vs_snapToGrid">
		<meta content="True" name="vs_showGrid">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script>function funcDisBack()
{history.go(+1);}
		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="ConsignmentStatusPrinting" method="post" runat="server">
			<INPUT style="Z-INDEX: 101; POSITION: absolute; TOP: 680px; LEFT: 16px" type="hidden" name="ScrollPosition">
			<asp:validationsummary style="Z-INDEX: 105; POSITION: absolute; COLOR: red; TOP: 672px; LEFT: 240px" id="Validationsummary1"
				ShowSummary="False" HeaderText="Please enter the missing  fields." ShowMessageBox="True" DisplayMode="BulletList"
				Runat="server"></asp:validationsummary>
			<div style="Z-INDEX: 102; POSITION: relative; HEIGHT: 1248px; TOP: 32px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tr>
						<td colSpan="2" align="left"><asp:label id="Label1" runat="server" Width="421px" Height="27px" CssClass="mainTitleSize">
							Shipping List Operations</asp:label></td>
					</tr>
					<tr>
						<td style="HEIGHT: 24px" vAlign="top" colSpan="2" align="left"><asp:button id="btnQry" runat="server" Width="61px" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExecQry" runat="server" Width="130px" CssClass="queryButton" Text="Execute Query"
								CausesValidation="False"></asp:button><asp:button id="btnPrintConsNote" runat="server" Width="150px" CssClass="queryButton" Text="Reprint Con Note"
								CausesValidation="False" Enabled="False"></asp:button><asp:button id="btnPrintShipList" runat="server" Width="150px" CssClass="queryButton" Text="Show Rating"
								CausesValidation="False" Enabled="False"></asp:button><asp:button id="btnReprintConsNote" runat="server" Width="180px" CssClass="queryButton" Text="Assign to Transporter"
								CausesValidation="False" Enabled="False"></asp:button></td>
					</tr>
					<tr>
						<td vAlign="top" colSpan="2" align="left">
							<TABLE style="Z-INDEX: 112; WIDTH: 740px" id="Table1" border="0" width="730" runat="server">
								<TR width="100%">
									<TD width="100%" style="Z-INDEX: 0">
										<fieldset><legend><asp:label id="lblSearch" runat="server" CssClass="tableHeadingFieldset">Search Criteria</asp:label></legend>
											<br>
											<table border="0" cellSpacing="1" cellPadding="1">
												<TBODY>
													<tr>
														<td style="WIDTH: 107px"><asp:label id="lblSenderName" runat="server" Width="100px" CssClass="tableLabel">Sender Name</asp:label></td>
														<td width="345" style="WIDTH: 345px"><asp:dropdownlist id="ddlSenderName" tabIndex="13" runat="server" Width="106px" CssClass="textField"
																Enabled="False">
																<asp:ListItem text="BSPLae" Value="-1" Selected="True"></asp:ListItem>
															</asp:dropdownlist></td>
													</tr>
													<tr>
														<td style="WIDTH: 107px">
															<asp:label style="Z-INDEX: 0" id="lblShipList" runat="server" CssClass="tableLabel">Shipping List</asp:label></td>
														<td width="345" style="WIDTH: 345px">
															<asp:textbox style="Z-INDEX: 0" id="txtShipList" tabIndex="14" runat="server" CssClass="textField"
																Width="185px" MaxLength="50"></asp:textbox></td>
														<td><asp:label id="lblStatus" runat="server" Width="100px" CssClass="tableLabel">Date Created</asp:label></td>
														<td width="450"><FONT face="Tahoma">
																<cc1:mstextbox style="Z-INDEX: 0" id="Mstextbox1" tabIndex="21" runat="server" CssClass="textField"
																	Width="114px" MaxLength="12" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></FONT></td>
													</tr>
													<tr>
														<td style="WIDTH: 107px">
															<asp:label style="Z-INDEX: 0" id="Label6" runat="server" CssClass="tableLabel" Width="111px">Transporter</asp:label></td>
														<td style="WIDTH: 345px"><FONT face="Tahoma">
																<asp:dropdownlist style="Z-INDEX: 0" id="ddlTransporter" tabIndex="16" runat="server" Width="150px"
																	AutoPostBack="True">
																	<asp:ListItem Selected="True"></asp:ListItem>
																	<asp:ListItem Value="BRAIN">BRAIN</asp:ListItem>
																	<asp:ListItem Value="TURNER">TURNER</asp:ListItem>
																</asp:dropdownlist></FONT>
														</td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td style="WIDTH: 107px">
															<asp:label style="Z-INDEX: 0" id="lblDateFrom" runat="server" CssClass="tableLabel" Width="100">Date From</asp:label></td>
														<td style="WIDTH: 345px"><FONT face="Tahoma">
																<cc1:mstextbox style="Z-INDEX: 0" id="txtDateFrom" tabIndex="21" runat="server" CssClass="textField"
																	Width="90px" MaxLength="12" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></FONT>
															<asp:label style="Z-INDEX: 0" id="lblTo" runat="server" CssClass="tableLabel">To</asp:label>
															<cc1:mstextbox style="Z-INDEX: 0" id="txtDateTo" tabIndex="22" runat="server" CssClass="textField"
																Width="90px" MaxLength="12" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></td>
														<td><FONT face="Tahoma"></FONT></td>
														<td></td>
													</tr>
												</TBODY>
											</table>
										</fieldset>
									</TD>
								</TR>
								<tr>
									<td colSpan="2"><asp:label style="Z-INDEX: 0" id="lblErrorMsg" runat="server" Width="566px" Height="19px" Font-Size="X-Small"
											Font-Bold="True" ForeColor="Red"></asp:label></td>
								</tr>
								<tr>
									<td colSpan="2">
										<asp:datagrid style="Z-INDEX: 0" id="dgConsignmentStatus" runat="server" SelectedItemStyle-CssClass="gridFieldSelected"
											PageSize="25" AutoGenerateColumns="False" HorizontalAlign="Left">
											<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
											<ItemStyle Height="15px" CssClass="gridField"></ItemStyle>
											<HeaderStyle Font-Bold="True" HorizontalAlign="Center" Height="40px" Width="2%" CssClass="gridHeading"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn>
													<ItemStyle HorizontalAlign="Center" Height="25px" Width="70px"></ItemStyle>
													<ItemTemplate>
														<asp:CheckBox Runat="server" ID="chkConNo" CrsID='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' Checked='<%# DataBinder.Eval(Container.DataItem,"Check").ToString().Equals("true") %>'>
														</asp:CheckBox>
													</ItemTemplate>
													<EditItemTemplate>
														<asp:Label></asp:Label>
													</EditItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="ShippingList_no" HeaderText="Shipping List">
													<HeaderStyle Width="185px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="recipient_telephone" HeaderText="Date Created">
													<HeaderStyle Width="120px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="recipient_name" HeaderText="Transporter">
													<HeaderStyle Width="150px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="pkg_qty" HeaderText="No. of Consignments">
													<HeaderStyle Width="100px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="pkg_qty" HeaderText="No. of Pkgs">
													<HeaderStyle Width="100px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
											</Columns>
											<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
										</asp:datagrid>
									</td>
								</tr>
							</TABLE>
						</td>
					</tr>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
