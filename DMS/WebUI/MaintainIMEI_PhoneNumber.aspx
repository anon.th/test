<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="MaintainIMEI_PhoneNumber.aspx.cs" 
AutoEventWireup="false" Inherits="TIES.WebUI.MaintainIMEI_PhoneNumber" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Maintain IMEIs and Phone Numbers for OTR POD</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" src="Scripts/WorkProgress.js"></script>
		<script language="javascript" type="text/javascript">
function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]/;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}

	//for field number
        function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }
        
function setDate(myInput)
{

	if (myInput.value.length>0)
	{
		myInput.value = nTrim(myInput.value);
		var error=0;
		var myDate='';
		var myMonth='';
		var myYear='';
		
		for(i=0; i<2 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myDate=myDate + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}
 
		for(i=3; i<5 ; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myMonth=myMonth + myInput.value.substr(i,1)
			}
			else
			{
				error=1;
			}
		}
 
		for(i=6; i<10; i++)
		{
			if(isDigit(myInput.value.substr(i,1)))
			{
				myYear=myYear + myInput.value.substr(i,1);
			}
			else
			{
				error=1;
			}
		}
 
		if(error==0)
		{
			if(parseInt(myDate) <= getDaysInMonth(myMonth, myYear))
			{
				if(parseInt(myMonth) <= 12)
				{
					if((parseInt(myYear) < 2100) && (parseInt(myYear) > 1999))
					{
								var currentDate = new Date();
								var inputDate = new Date(myYear,myMonth-1,myDate);
								if ( currentDate.setHours(0,0,0,0) <= inputDate.setHours(0,0,0,0) ) 
								{
								
								}else{
								
									error=7;
								}
					}
					else
					{
						error=4;
					}
				}
				else
				{
					error=3;
				}
			}
			else
			{
				error=2;
			}
		
			switch(error)
			{
				case 2:
					alert('Incorrect date in dd/mm/yyyy : '+myDate);
					myInput.value='';
					myInput.focus();
					return false;
				case 3:
					alert('Enter month between 1 to 12');
					myInput.value='';
					myInput.focus();
					return false;
				case 4:
					alert('Enter year between 2000 to 2099');
					myInput.value='';
					myInput.focus();
					return false;
					
				case 7:
					alert('Deactivated date must be greater than or equal to today�s date.');
					//var mes = '<%= deactive_date_warning %>';
					//alert(mes);
					myInput.value='';
					myInput.focus();
					return false;
				case 0:
					return true;
				default:
					return false;
			}
		}	
		else
		{
			alert('Incorrect date. Please enter correct date.');
			myInput.value='';
			myInput.focus();
			return false;
		}
	}
}

		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="PhoneIMEI" method="post" runat="server">
			<asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 63px; LEFT: 18px" id="btnQuery" runat="server"
				CausesValidation="False" CssClass="queryButton" Width="100px" Text="Query"></asp:button><asp:button style="Z-INDEX: 103; POSITION: absolute; TOP: 63px; LEFT: 119px" id="btnExecuteQuery"
				runat="server" CausesValidation="False" CssClass="queryButton" Width="120px" Text="Execute Query" Enabled="True"></asp:button><asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 94px; LEFT: 22px" id="lblValPhoneIMEI"
				runat="server" CssClass="errorMsgColor" Width="669px"></asp:label><asp:label style="Z-INDEX: 106; POSITION: absolute; TOP: 16px; LEFT: 27px" id="Label1" runat="server"
				CssClass="mainTitleSize" Width="500px" Height="30px">Maintain IMEIs and Phone Numbers for OTR POD</asp:label><asp:validationsummary style="Z-INDEX: 105; POSITION: absolute; TOP: 56px; LEFT: 728px" id="PageValidationSummary"
				runat="server" HeaderText="Please enter the missing  fields." DisplayMode="BulletList" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary><asp:label style="Z-INDEX: 107; POSITION: absolute; TOP: 122px; LEFT: 18px" id="Label2" runat="server"
				CssClass="tableLabel">Phone Number: </asp:label><asp:textbox style="Z-INDEX: 107; POSITION: absolute; TOP: 120px; LEFT: 120px" id="txtPhoneNo"
				tabIndex="0" onkeypress="validate(event)" onpaste="AfterPasteNonNumber(this)" runat="server" CssClass="textField" Width="150px" MaxLength="20"></asp:textbox><asp:label style="Z-INDEX: 107; POSITION: absolute; TOP: 122px; LEFT: 300px" id="Label3" runat="server"
				CssClass="tableLabel">IMEI: </asp:label><asp:textbox style="Z-INDEX: 107; POSITION: absolute; TOP: 120px; LEFT: 345px" id="txtIMEI" onkeypress="validate(event)"
				onpaste="AfterPasteNonNumber(this)" runat="server" CssClass="textField" Width="150px" MaxLength="15"></asp:textbox>
			<div style="Z-INDEX: 107; POSITION: absolute; TEXT-ALIGN: justify; BORDER-LEFT: 1px; WIDTH: 900px; HEIGHT: 20px; TOP: 152px; FONT-WEIGHT: bold; LEFT: 18px"
				class="gridHeading">Allowed IMEIs and Phone Numbers
			</div>
			<asp:datagrid style="Z-INDEX: 104; POSITION: absolute; TOP: 175px; LEFT: 18px" id="dgPhoneIMEI"
				runat="server" Width="900px" AllowPaging="True" OnPageIndexChanged="dgPhoneIMEI_PageChange"
				AlternatingItemStyle-CssClass="gridField" ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridHeading"
				CellPadding="0" CellSpacing="1" BorderWidth="0px" FooterStyle-CssClass="gridHeading" AutoGenerateColumns="False"
				ShowFooter="True" ShowHeader="True">
				<FooterStyle CssClass="gridHeading"></FooterStyle>
				<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
				<ItemStyle CssClass="gridField"></ItemStyle>
				<HeaderStyle Font-Bold="True" HorizontalAlign="Center" Height="30px" Width="2%" CssClass="gridHeading"></HeaderStyle>
				<Columns>
					<asp:TemplateColumn>
						<HeaderStyle Width="13%"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
						<ItemTemplate>
							<asp:LinkButton id="btnEditItem" Runat="server" Text="<img src='images/butt-edit.gif' alt='edit' border=0>"
								CommandName="EDIT_ITEM" />
							<asp:LinkButton id="btnDeleteItem" Runat="server" Text="<img src='images/butt-delete.gif' alt='delete' border=0>"
								CommandName="DELETE_ITEM" />
						</ItemTemplate>
						<FooterStyle HorizontalAlign="Center"></FooterStyle>
						<FooterTemplate>
							<asp:LinkButton id="btnAddItem" Runat="server" Text="<img src='images/butt-add.gif' alt='add' border=0>"
								CommandName="ADD_ITEM" />
						</FooterTemplate>
						<EditItemTemplate>
							<asp:LinkButton id="btnSaveItem" Runat="server" Text="<img src='images/butt-update.gif' alt='save' border=0>"
								CommandName="SAVE_ITEM" />
							<asp:LinkButton id="btnCancelItem" Runat="server" Text="<img src='images/butt-cancel.gif' alt='cancel' border=0>"
								CommandName="CANCEL_ITEM" />
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Phone Number/Sim Serial Number">
						<HeaderStyle Width="27%" CssClass="gridHeading" Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>
						<ItemStyle HorizontalAlign="left" Width="27%"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblPhoneNo" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"phoneNo")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:textbox id="txtPhoneNoGrid" runat="server" Enabled=False CssClass="textField" MaxLength="20" Width="95%" onkeypress='validate(event)' Text = '<%#DataBinder.Eval(Container.DataItem,"phoneNo")%>' onpaste="AfterPasteNonNumber(this)">
							</asp:textbox>
						</EditItemTemplate>
						<FooterTemplate>
							<asp:textbox id="txtFooterPhone" runat="server" CssClass="textField" MaxLength="20" Width="95%" onkeypress='validate(event)' Text = '<%#DataBinder.Eval(Container.DataItem,"phoneNo")%>' onpaste="AfterPasteNonNumber(this)">
							</asp:textbox>
						</FooterTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="IMEI">
						<HeaderStyle Width="16%" CssClass="gridHeading" Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>
						<ItemStyle HorizontalAlign="left" Width="16%"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblIMEI" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"imei")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:textbox id="txtIMEIGrid" runat="server" CssClass="textField" Enabled=False MaxLength="15" Width="95%" onkeypress='validate(event)' Text = '<%#DataBinder.Eval(Container.DataItem,"imei")%>' onpaste="AfterPasteNonNumber(this)">
							</asp:textbox>
						</EditItemTemplate>
						<FooterTemplate>
							<asp:textbox id="txtFooterIMEI" runat="server" MaxLength="15" Width="95%" CssClass="textField"
								onkeypress='validate(event)' onpaste="AfterPasteNonNumber(this)"></asp:textbox>
						</FooterTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Deactivated Date">
						<HeaderStyle Width="14%" CssClass="gridHeading" Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>
						<ItemStyle HorizontalAlign="left" Width="14%"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblDate" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"DeactivatedDate")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox id="txtDeactivated" runat="server" CssClass="textField" Width="95%" Text='<%#DataBinder.Eval(Container.DataItem,"DeactivatedDate")%>' TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999">
							</cc1:mstextbox>
						</EditItemTemplate>
						<FooterTemplate>
							<cc1:mstextbox id="txtFooterDeactivated" runat="server" CssClass="textField" Width="95%" Text=''
								TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>
						</FooterTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Remark">
						<HeaderStyle Width="35%" CssClass="gridHeading" Font-Bold="True" HorizontalAlign="Left"></HeaderStyle>
						<ItemStyle HorizontalAlign="left" Width="35%"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblRemark" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"Remark")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:textbox id="txtRemarkEdit" runat="server" CssClass="textField" Enabled=true MaxLength="200" Width="95%" Text = '<%#DataBinder.Eval(Container.DataItem,"Remark")%>' >
							</asp:textbox>
						</EditItemTemplate>
						<FooterTemplate>
							<asp:textbox id="txtRemarkFooter" runat="server" CssClass="textField" Enabled=true MaxLength="200" Width="95%"  Text = '<%#DataBinder.Eval(Container.DataItem,"Remark")%>' >
							</asp:textbox>
						</FooterTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" CssClass="normalText" PrevPageText="Previous" HorizontalAlign="Left"></PagerStyle>
			</asp:datagrid></form>
	</body>
</HTML>
