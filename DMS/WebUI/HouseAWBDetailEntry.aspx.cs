using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.applicationpages;
using TIESClasses;
using TIESDAL;
using com.ties;
using com.ties.DAL;
using com.ties.classes;
using com.common.util;
using System.Text;
using com.common.RBAC;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for HouseAWBDetailEntry.
	/// </summary>
	public class HouseAWBDetailEntry : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label Label38;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label Label30;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.Label Label32;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label Label27;
		protected System.Web.UI.WebControls.TextBox CustomerID;
		protected System.Web.UI.WebControls.TextBox MAWBNumber;
		protected System.Web.UI.WebControls.TextBox CustomerTaxCode;
		protected System.Web.UI.WebControls.TextBox consignment_no;
		protected System.Web.UI.WebControls.TextBox ExporterName;
		protected System.Web.UI.WebControls.TextBox ExporterAddress1;
		protected System.Web.UI.WebControls.TextBox ExporterAddress2;
		protected System.Web.UI.WebControls.DropDownList ExporterCountry;
		protected System.Web.UI.WebControls.TextBox ExporterCountry_Desc;
		protected System.Web.UI.WebControls.DropDownList CustomsShed_Location;
		protected System.Web.UI.WebControls.TextBox LocationCustomsShed_Desc;
		protected System.Web.UI.WebControls.TextBox GoodsDescription;
		protected System.Web.UI.WebControls.DropDownList Kind_UOM;
		protected System.Web.UI.WebControls.TextBox Kind_UOM_Desc;
		protected System.Web.UI.WebControls.TextBox QuantityOfKind;
		protected com.common.util.msTextBox WeightKG;
		protected System.Web.UI.WebControls.DropDownList Nature;
		protected System.Web.UI.WebControls.TextBox Nature_Desc;
		protected System.Web.UI.WebControls.TextBox ConsigneeName;
		protected System.Web.UI.WebControls.TextBox ConsigneeAddress1;
		protected System.Web.UI.WebControls.TextBox ConsigneeAddress2;
		protected System.Web.UI.WebControls.TextBox ConsigneeCountry;
		protected System.Web.UI.WebControls.TextBox ConsigneeTelephone;
		protected System.Web.UI.WebControls.CheckBox cbkSameConsignee;
		protected System.Web.UI.WebControls.TextBox NotifyName;
		protected System.Web.UI.WebControls.TextBox NotifyAddress1;
		protected System.Web.UI.WebControls.TextBox NotifyAddress2  ;
		protected System.Web.UI.WebControls.DropDownList NotifyCountry;
		protected System.Web.UI.WebControls.TextBox NotifyCountry_Desc;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.WebControls.TextBox TempConsigneeCountry;
		protected System.Web.UI.WebControls.TextBox ConsigneeZipcode;
		protected System.Web.UI.WebControls.Label lblRegConsigneeName;
		protected System.Web.UI.WebControls.Label lblRegConsigneeAddress1;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.Label Label24;
		protected System.Web.UI.WebControls.Button btnDisplayCustDtls;
		protected System.Web.UI.WebControls.CheckBox cbkOverrideConsignee;
		protected System.Web.UI.WebControls.Label Label25;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.CheckBox cbkSameShipper;
		protected System.Web.UI.WebControls.Label Label39;
		protected System.Web.UI.WebControls.Label Label40;
		protected System.Web.UI.HtmlControls.HtmlGenericControl fsOtherData;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.Label Label26;
		protected System.Web.UI.WebControls.Label Label28;
		protected System.Web.UI.WebControls.Label Label29;
		protected System.Web.UI.WebControls.Label Label31;
		protected System.Web.UI.WebControls.Label Label33;
		protected System.Web.UI.WebControls.Label Label34;
		protected System.Web.UI.WebControls.Label Label35;
		protected System.Web.UI.WebControls.Label Label36;
		protected System.Web.UI.WebControls.Label Label37;
		protected System.Web.UI.WebControls.Label Label41;
		protected System.Web.UI.WebControls.TextBox ImportedAssignedPieces;
		protected System.Web.UI.WebControls.TextBox ImportedActualGrossWt;
		protected System.Web.UI.WebControls.TextBox ImportedVolume;
		protected System.Web.UI.WebControls.TextBox ImportedTermsOfPayment;
		protected System.Web.UI.WebControls.TextBox ImportedOriginDepot;
		protected System.Web.UI.WebControls.TextBox ImportedDestinationDepot;
		protected System.Web.UI.WebControls.TextBox ImportedTariffID;
		protected System.Web.UI.WebControls.TextBox ImportedInvValue;
		protected System.Web.UI.WebControls.TextBox ImportedDivision;
		protected System.Web.UI.WebControls.TextBox ImportedProduct;
		protected System.Web.UI.WebControls.TextBox ImportedItemQuantity;
		protected System.Web.UI.WebControls.TextBox ImportedValueIndicator;
		protected System.Web.UI.WebControls.TextBox ImportedUnitID;
		protected System.Web.UI.WebControls.Label Label42;
		protected System.Web.UI.WebControls.TextBox Gross_Mass;
		protected System.Web.UI.WebControls.TextBox Shipping_Marks;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
	
		public string appID
		{
			get
			{
				if(ViewState["appID"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["appID"];
				}
			}
			set
			{
				ViewState["appID"]=value;
			}
		}

		public string enterpriseID
		{
			get
			{
				if(ViewState["enterpriseID"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["enterpriseID"];
				}
			}
			set
			{
				ViewState["enterpriseID"]=value;
			}
		}

		public string userID
		{
			get
			{
				if(ViewState["userID"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["userID"];
				}
			}
			set
			{
				ViewState["userID"]=value;
			}
		}

		public string cust_id
		{
			get
			{
				if(ViewState["cust_id"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["cust_id"];
				}
			}
			set
			{
				ViewState["cust_id"]=value;
			}
		}

		public string cons_no
		{
			get
			{
				if(ViewState["cons_no"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["cons_no"];
				}
			}
			set
			{
				ViewState["cons_no"]=value;
			}
		}

		public string MasterAWB_no
		{
			get
			{
				if(ViewState["MasterAWB_no"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["MasterAWB_no"];
				}
			}
			set
			{
				ViewState["MasterAWB_no"]=value;
			}
		}

		public string ShipFlightNo
		{
			get
			{
				if(ViewState["ShipFlightNo"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["ShipFlightNo"];
				}
			}
			set
			{
				ViewState["ShipFlightNo"]=value;
			}
		}

		public string DepartureCountry
		{
			get
			{
				if(ViewState["DepartureCountry"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["DepartureCountry"];
				}
			}
			set
			{
				ViewState["DepartureCountry"]=value;
			}
		}


		public string ConsigneeNameTemp
		{
			get
			{
				if(ViewState["ConsigneeNameTemp"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["ConsigneeNameTemp"];
				}
			}
			set
			{
				ViewState["ConsigneeNameTemp"]=value;
			}
		}

		public string ConsigneeAddress1Temp
		{
			get
			{
				if(ViewState["ConsigneeAddress1Temp"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["ConsigneeAddress1Temp"];
				}
			}
			set
			{
				ViewState["ConsigneeAddress1Temp"]=value;
			}
		}

		public string ConsigneeAddress2Temp
		{
			get
			{
				if(ViewState["ConsigneeAddress2Temp"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["ConsigneeAddress2Temp"];
				}
			}
			set
			{
				ViewState["ConsigneeAddress2Temp"]=value;
			}
		}


		public bool SourceOfData
		{
			get
			{
				if(ViewState["SourceOfData"] == null)
				{
					return false;
				}
				else
				{
					return (bool)ViewState["SourceOfData"];
				}
			}
			set
			{
				ViewState["SourceOfData"]=value;
			}
		}


		public int DSOperation
		{
			get
			{
				if(ViewState["iDSOperation"]==null)
				{
					return 1;
				}
				else
				{
					return (int)ViewState["iDSOperation"];
				}
			}
			set
			{
				ViewState["iDSOperation"]=value;
			}
		}


		public DataTable AWBDetailData
		{
			get
			{
				if(ViewState["AWBDetailData"] == null)
				{
					return null;
				}
				else
				{
					return (DataTable)ViewState["AWBDetailData"];
				}
			}
			set
			{
				ViewState["AWBDetailData"]=value;
			}
		}


		private void Page_Load(object sender, System.EventArgs e)
		{
			if(this.IsPostBack==false)
			{
				appID = Request.QueryString["appID"].ToString();
				enterpriseID = Request.QueryString["enterpriseID"].ToString();
				userID = Request.QueryString["userID"].ToString();

				if(Request.QueryString["cust_id"] != null)
				{
					cust_id = Request.QueryString["cust_id"].ToString();
				}

				if(Request.QueryString["MasterAWB_no"] != null)
				{
					MasterAWB_no = Request.QueryString["MasterAWB_no"].ToString();
				}

				if(Request.QueryString["cons_no"] != null)
				{
					cons_no = Request.QueryString["cons_no"].ToString();
				}

				if(Request.QueryString["ShipFlightNo"] != null)
				{
					ShipFlightNo = Request.QueryString["ShipFlightNo"].ToString();
				}
				else
				{
					ShipFlightNo=null;
				}

				if(Request.QueryString["DSOperation"] != null)
				{
					DSOperation = int.Parse(Request.QueryString["DSOperation"].ToString());
				}

				if(Request.QueryString["SourceOfData"] != null)
				{
					if(int.Parse(Request.QueryString["SourceOfData"].ToString())!=0)
					{
						SourceOfData=true;
					}
				}

				if(Request.QueryString["DepartureCountry"] != null)
				{
					DepartureCountry = Request.QueryString["DepartureCountry"].ToString();
				}
				clearscreen();				
				BindControl();
				
				if(cbkOverrideConsignee.Checked)
				{
					SetInitialFocus(ConsigneeName);
				}
				else
				{
					if(consignment_no.Enabled==true)
					{
						SetInitialFocus(consignment_no);
					}
					else
					{
						if(CustomerID.Enabled)
						{
							SetInitialFocus(CustomerID);	
						}
						else
						{
							SetInitialFocus(ExporterName);	
						}
											
					}					
				}
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.consignment_no.TextChanged += new System.EventHandler(this.consignment_no_TextChanged);
			this.cbkSameShipper.CheckedChanged += new System.EventHandler(this.cbkSameShipper_CheckedChanged);
			this.ExporterCountry.SelectedIndexChanged += new System.EventHandler(this.ExporterCountry_SelectedIndexChanged);
			this.CustomerID.TextChanged += new System.EventHandler(this.CustomerID_TextChanged);
			this.btnDisplayCustDtls.Click += new System.EventHandler(this.btnDisplayCustDtls_Click);
			this.cbkOverrideConsignee.CheckedChanged += new System.EventHandler(this.cbkOverrideConsignee_CheckedChanged);
			this.cbkSameConsignee.CheckedChanged += new System.EventHandler(this.cbkSameConsignee_CheckedChanged);
			this.NotifyCountry.SelectedIndexChanged += new System.EventHandler(this.NotifyCountry_SelectedIndexChanged);
			this.CustomsShed_Location.SelectedIndexChanged += new System.EventHandler(this.CustomsShed_Location_SelectedIndexChanged);
			this.Kind_UOM.SelectedIndexChanged += new System.EventHandler(this.Kind_UOM_SelectedIndexChanged);
			this.Nature.SelectedIndexChanged += new System.EventHandler(this.Nature_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void clearscreen()
		{
			lblError.Text="";
			MAWBNumber.Text="";
			CustomerID.Text="";
			CustomerTaxCode.Text="";
			consignment_no.Text="";
			ExporterName.Text="";
			ExporterAddress1.Text="";
			ExporterAddress2.Text="";
			ExporterCountry.Items.Clear();
			ExporterCountry_Desc.Text="";
			CustomsShed_Location.Items.Clear();
			LocationCustomsShed_Desc.Text="";
			GoodsDescription.Text="";
			Kind_UOM.Items.Clear();
			Kind_UOM_Desc.Text="";

			QuantityOfKind.Text="";
			WeightKG.Text="";
			Nature.Items.Clear();
			Nature_Desc.Text="";


			ConsigneeName.Text="";
			ConsigneeAddress1.Text="";
			ConsigneeAddress2.Text="";
			ConsigneeZipcode.Text="";
			ConsigneeCountry.Text="";
			TempConsigneeCountry.Text="";
			ConsigneeTelephone.Text="";

			cbkSameConsignee.Checked=false;
			NotifyName.Text="";
			NotifyAddress1.Text="";
			NotifyAddress2.Text="";
			NotifyCountry.Items.Clear();
			NotifyCountry_Desc.Text="";

			lblRegConsigneeName.Visible=false;
			lblRegConsigneeAddress1.Visible=false;
			ConsigneeName.Enabled=false;
			ConsigneeAddress1.Enabled=false;
			ConsigneeAddress2.Enabled=false;

			ConsigneeNameTemp="";
			ConsigneeAddress1Temp="";
			ConsigneeAddress2Temp="";

			ConsigneeName.BackColor=System.Drawing.ColorTranslator.FromHtml("#eaeaea");
			ConsigneeAddress1.BackColor=System.Drawing.ColorTranslator.FromHtml("#eaeaea");
			ConsigneeAddress2.BackColor=System.Drawing.ColorTranslator.FromHtml("#eaeaea");

			CustomerID.Enabled=true;
			CustomerID.BackColor=System.Drawing.Color.White;

			GoodsDescription.Enabled=true;
			consignment_no.Enabled=SourceOfData;

			fsOtherData.Visible=false;
		}

		private void BindControl()
		{
			DataSet dsExporterCountry = CustomJobCompilingDAL.GetCountryDefaultValue(appID,enterpriseID);
			DataSet dsNotifyCountry = CustomJobCompilingDAL.GetCountry(appID,enterpriseID,"");
			DataSet dsCustomsShed_Location = CustomJobCompilingDAL.GetDropDownListDefaultDisplayValue(appID,enterpriseID,"CustomsTransitShed");
			DataSet dsKind_UOM = CustomJobCompilingDAL.GetDropDownListDefaultDisplayValue(appID,enterpriseID,"KindOfPkgASYCUDA");
			DataSet dsNature = CustomJobCompilingDAL.GetDropDownListDefaultDisplayValue(appID,enterpriseID,"NatureASYCUDA");

			DataRow[] defaultKindUOM = dsKind_UOM.Tables[0].Select("DefaultValue=1");
			DataRow[] defaultCustomsShedLocation = dsCustomsShed_Location.Tables[0].Select("DefaultValue=1");
			DataRow[] defaultNature = dsNature.Tables[0].Select("DefaultValue=1");

			ExporterCountry_Desc.Text="";
			LocationCustomsShed_Desc.Text="";
			Kind_UOM_Desc.Text="";
			Nature_Desc.Text="";
			NotifyCountry_Desc.Text="";

			ExporterCountry.DataSource=dsExporterCountry;
			ExporterCountry.DataTextField="CountryCode";
			ExporterCountry.DataValueField="CountryCode";
			ExporterCountry.DataBind();

			if(DepartureCountry.Trim() != "")
			{
				ExporterCountry.SelectedValue=DepartureCountry;
				ExporterCountry_Desc.Text=CustomJobCompilingDAL.GetCountryName(appID,enterpriseID,ExporterCountry.SelectedValue);
			}
			else
			{
				if(dsExporterCountry.Tables[0].Rows.Count>0)
				{
					ExporterCountry_Desc.Text=dsExporterCountry.Tables[0].Rows[0]["CountryName"].ToString();
				}
			}


			NotifyCountry.DataSource=dsNotifyCountry;
			NotifyCountry.DataTextField="CountryCode";
			NotifyCountry.DataValueField="CountryCode";
			NotifyCountry.DataBind();

			if(dsNotifyCountry.Tables[0].Rows.Count>0)
			{
				NotifyCountry_Desc.Text=dsNotifyCountry.Tables[0].Rows[0]["CountryName"].ToString();
			}


			CustomsShed_Location.DataSource=dsCustomsShed_Location;
			CustomsShed_Location.DataTextField="DropDownListDisplayValue";
			CustomsShed_Location.DataValueField="CodedValue";
			CustomsShed_Location.DataBind();
			if(defaultCustomsShedLocation.Length > 0)
			{
				CustomsShed_Location.SelectedValue=defaultCustomsShedLocation[0]["CodedValue"].ToString();
				LocationCustomsShed_Desc.Text=defaultCustomsShedLocation[0]["Description"].ToString();
			}

			Kind_UOM.DataSource=dsKind_UOM;
			Kind_UOM.DataTextField="DropDownListDisplayValue";
			Kind_UOM.DataValueField="CodedValue";
			Kind_UOM.DataBind();
			if(defaultKindUOM.Length > 0)
			{
				Kind_UOM.SelectedValue=defaultKindUOM[0]["CodedValue"].ToString();
				Kind_UOM_Desc.Text=defaultKindUOM[0]["Description"].ToString();
			}

			Nature.DataSource=dsNature;
			Nature.DataTextField="DropDownListDisplayValue";
			Nature.DataValueField="CodedValue";
			Nature.DataBind();
			if(defaultNature.Length > 0)
			{
				Nature.SelectedValue=defaultNature[0]["CodedValue"].ToString();
				Nature_Desc.Text=defaultNature[0]["Description"].ToString();
			}

			lblError.Text="";
			MAWBNumber.Text=MasterAWB_no;
			if(cons_no != "")
			{
				consignment_no.Text=cons_no;
				consignment_no.Enabled = false;
			}
			

			DataTable dtParams = CustomsDAL.MasterAWBDetailParameter();
			System.Data.DataRow drNew = dtParams.NewRow();
			if(DSOperation==(int)Operation.Insert)
			{
				drNew["action"] ="1";
			}
			else
			{
				drNew["action"] ="1";
			}

			drNew["enterpriseid"] =enterpriseID;
			drNew["userloggedin"] =userID;
			drNew["MasterAWB_no"] =MasterAWB_no;
			drNew["ShipFlightNo"] =ShipFlightNo;
			drNew["consignment_no"] =this.consignment_no.Text;
			
			dtParams.Rows.Add(drNew);
			DataSet ds = CustomsDAL.Customs_MAWB_Detail(appID,enterpriseID,userID,dtParams);

			System.Data.DataTable dtStatus = ds.Tables[0];
			System.Data.DataTable dtDetail= ds.Tables[1];
			if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
			{
				AWBDetailData=dtDetail;
				BindMasterAWBDetail();
			}
			else
			{
				lblError.Text=(dtStatus.Rows.Count>0?dtStatus.Rows[0]["ErrorMessage"].ToString():"");
			}

		}

		private void BindMasterAWBDetail()
		{
			DataTable dtDetail = AWBDetailData;
			if(dtDetail.Rows.Count<=0)
			{
				CustomerID.Enabled=true;
				CustomerID.BackColor=System.Drawing.Color.White;
				return;
			}
			DataRow dr = dtDetail.Rows[0];
			MAWBNumber.Text=dr["MAWBNumber"].ToString();
			cbkOverrideConsignee.Checked=false;
			if(dr["OverrideConsigneeDetails"] != DBNull.Value && dr["OverrideConsigneeDetails"].ToString().Trim() =="1")
			{
				cbkOverrideConsignee.Checked=true;
			}
			IsOverrideConsignee();

			CustomerID.Text=dr["CustomerID"].ToString();
			CustomerTaxCode.Text=dr["CustomerTaxCode"].ToString();
			if(cbkOverrideConsignee.Checked==false)
			{
				if(CustomerID.Text.Trim() !="")
				{
					CustomerID.Enabled=false;
					CustomerID.BackColor=System.Drawing.ColorTranslator.FromHtml("#eaeaea");

					GoodsDescription.Enabled=true;
					GoodsDescription.BackColor=System.Drawing.Color.White;
				}
				else
				{
					CustomerID.Enabled=true;
					CustomerID.BackColor=System.Drawing.Color.White;

					GoodsDescription.Enabled=true;
					GoodsDescription.BackColor=System.Drawing.Color.White;
				}
			}

			
			consignment_no.Text=dr["consignment_no"].ToString();

			ExporterName.Text=dr["ExporterName"].ToString();
			ExporterAddress1.Text=dr["ExporterAddress1"].ToString();
			ExporterAddress2.Text=dr["ExporterAddress2"].ToString();
			if(dr["ExporterCountry"] != DBNull.Value)
			{
				ExporterCountry.SelectedValue=dr["ExporterCountry"].ToString();	
				ExporterCountry_Desc.Text=dr["ExporterCountry_Desc"].ToString();
			}
			
			NotifyName.Text=dr["NotifyName"].ToString();
			NotifyAddress1.Text=dr["NotifyAddress1"].ToString();
			NotifyAddress2.Text=dr["NotifyAddress2"].ToString();
			if(dr["NotifyCountry"] != DBNull.Value)
			{
				NotifyCountry.SelectedValue=dr["NotifyCountry"].ToString();	
				NotifyCountry_Desc.Text=dr["NotifyCountry_Desc"].ToString();
			}
						
			if(dr["CustomsShed_Location"] != DBNull.Value)
			{
				CustomsShed_Location.SelectedValue=dr["CustomsShed_Location"].ToString().Trim();	
			}

			LocationCustomsShed_Desc.Text=dr["LocationCustomsShed_Desc"].ToString();
			GoodsDescription.Text=dr["GoodsDescription"].ToString();
			Kind_UOM.SelectedValue=dr["Kind_UOM"].ToString();			
			Kind_UOM_Desc.Text=dr["Kind_UOM_Desc"].ToString();

			QuantityOfKind.Text=(dr["QuantityOfKind"] != DBNull.Value?Decimal.Parse(dr["QuantityOfKind"].ToString()).ToString("N0"): "");
			WeightKG.Text=(dr["WeightKG"] != DBNull.Value?Decimal.Parse(dr["WeightKG"].ToString()).ToString("N1"): "");

			if(dr["Nature"] != DBNull.Value)
			{
				Nature.SelectedValue=dr["Nature"].ToString().Trim();	
			}

			Nature_Desc.Text=dr["Nature_Desc"].ToString();

			ConsigneeNameTemp=dr["ConsigneeName"].ToString();
			ConsigneeAddress1Temp=dr["ConsigneeAddress1"].ToString();
			ConsigneeAddress2Temp=dr["ConsigneeAddress2"].ToString();

			ConsigneeName.Text=dr["ConsigneeName"].ToString();
			ConsigneeAddress1.Text=dr["ConsigneeAddress1"].ToString();
			ConsigneeAddress2.Text=dr["ConsigneeAddress2"].ToString();
			ConsigneeZipcode.Text=dr["ConsigneeZipcode"].ToString();
			ConsigneeCountry.Text=dr["ConsigneeState"].ToString();
			TempConsigneeCountry.Text=dr["ConsigneeCountry"].ToString();
			ConsigneeTelephone.Text=dr["ConsigneeTelephone"].ToString();

			if(dr["Imported"] != DBNull.Value && dr["Imported"].ToString().Trim() =="1")
			{
				fsOtherData.Visible=true;
			}
			else
			{
				fsOtherData.Visible=false;
			}

			ImportedAssignedPieces.Text=dr["ImportedAssignedPieces"].ToString();
			ImportedActualGrossWt.Text=dr["ImportedActualGrossWt"].ToString();
			ImportedUnitID.Text=dr["ImportedUnitID"].ToString();
			ImportedOriginDepot.Text=dr["ImportedOriginDepot"].ToString();
			ImportedDestinationDepot.Text=dr["ImportedDestinationDepot"].ToString();
			ImportedDivision.Text=dr["ImportedDivision"].ToString();
			ImportedProduct.Text=dr["ImportedProduct"].ToString();
			ImportedTermsOfPayment.Text=dr["ImportedTermsOfPayment"].ToString();
			ImportedInvValue.Text=dr["ImportedInvValue"].ToString();
			ImportedVolume.Text=dr["ImportedVolume"].ToString();
			ImportedTariffID.Text=dr["ImportedTariffID"].ToString();
			ImportedItemQuantity.Text=dr["ImportedItemQuantity"].ToString();
			ImportedValueIndicator.Text=dr["ImportedValueIndicator"].ToString();
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			string sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			lblError.Text="";
			if(Utility.GetConsNoRegex(consignment_no.Text.Trim())==false)
			{
				lblError.Text = "House AWB Number is invalid.";
				SetInitialFocus(consignment_no);
				return;
			}

			if(consignment_no.Text.Trim() =="")
			{
				lblError.Text="House AWB number is required";
				return;
			}

			if(cbkOverrideConsignee.Checked == false && CustomerID.Text.Trim() =="")
			{
				lblError.Text="Customer ID is required";
				return;
			}

			if(ExporterName.Text.Trim() =="")
			{
				lblError.Text="Exporter name is required";
				return;
			}

			if(ExporterAddress1.Text.Trim() =="")
			{
				lblError.Text="Exporter address is required";
				return;
			}

			if(GoodsDescription.Text.Trim() =="")
			{
				lblError.Text="Goods Description is required";
				return;
			}
			if(cbkOverrideConsignee.Checked)
			{
				if(ConsigneeName.Text.Trim() =="")
				{
					lblError.Text="Consignee details (name and address) are required if override is checked";
					return;
				}
				if(ConsigneeAddress1.Text.Trim() =="")
				{
					lblError.Text="Consignee details (name and address) are required if override is checked";
					return;
				}
			}

			DataTable dtParams = CustomsDAL.MasterAWBDetailParameter();
			System.Data.DataRow drNew = dtParams.NewRow();
			foreach(System.Data.DataColumn col in dtParams.Columns)
			{
				string colName = col.ColumnName;
				System.Web.UI.Control ctrol = this.Page.FindControl(colName);
				if(ctrol != null)
				{
					if(ctrol.GetType()==typeof(TextBox))
					{
						TextBox txt = (TextBox)ctrol;
						if(txt.Text.Trim() != "")
						{
							if(txt.ID=="sender_zipcode" || txt.ID=="recipient_zipcode")
							{
								drNew[colName]=txt.Text.ToUpper().Trim();
							}
							else
							{
								drNew[colName]=txt.Text.Trim();
							}							
						}
						else
						{
							if(colName.ToLower()=="goodsdescription")
							{
								drNew[colName]="";
							}
						}
					}
					else if(ctrol.GetType()==typeof(com.common.util.msTextBox))
					{
						com.common.util.msTextBox txt = (com.common.util.msTextBox)ctrol;
						if(txt.TextMaskType==MaskType.msDate)
						{
							drNew[colName]=DateTime.ParseExact(txt.Text,"dd/MM/yyyy",null).ToString("yyyy-MM-dd");
						}
						else
						{
							if(colName=="WeightKG")
							{
								drNew[colName]=txt.Text.Trim().Replace(",","");
							}
							else
							{
								drNew[colName]=txt.Text.Trim();

							}
						}
						
					}
					else if(ctrol.GetType()==typeof(DropDownList))
					{
						DropDownList ddl = (DropDownList)ctrol;
						drNew[colName]=ddl.SelectedValue;
					}				
				}
			}

			drNew["action"] ="2";
			drNew["enterpriseid"] =enterpriseID;
			drNew["userloggedin"] =userID;
			drNew["MasterAWB_no"] =MAWBNumber.Text;
			drNew["ShipFlightNo"] =ShipFlightNo;
			drNew["consignment_no"] =this.consignment_no.Text;
			if(cbkOverrideConsignee.Checked)
			{
				drNew["OverrideConsigneeDetails"]=1;
				drNew["Consignee_Name"]=ConsigneeName.Text.Trim();
				drNew["Consignee_address1"]=ConsigneeAddress1.Text.Trim();
				drNew["Consignee_address2"]=ConsigneeAddress2.Text.Trim();
			}
			else
			{
				drNew["OverrideConsigneeDetails"]=0;
				drNew["Consignee_Name"]=DBNull.Value;
				drNew["Consignee_address1"]=DBNull.Value;
				drNew["Consignee_address2"]=DBNull.Value;
			}
			drNew["Gross_Mass"] =this.Gross_Mass.Text;
			drNew["Shipping_Marks"] =this.Shipping_Marks.Text;

			dtParams.Rows.Add(drNew);
			DataSet ds = CustomsDAL.Customs_MAWB_Detail(appID,enterpriseID,userID,dtParams);

			System.Data.DataTable dtStatus = ds.Tables[0];
			System.Data.DataTable dtDetail= ds.Tables[1];
			if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
			{				
				if(Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
				{
					if(dtDetail.Rows.Count>0)
					{
						AWBDetailData=dtDetail;
						BindMasterAWBDetail();
					}
				}
				lblError.Text = dtStatus.Rows[0]["ErrorMessage"].ToString();
							
				string sScript = "";				
				sScript += "<script language=javascript>";				
				sScript += "  window.opener.callback();";				
				sScript += "</script>";				
				Response.Write(sScript);
			}
			else
			{
				lblError.Text=(dtStatus.Rows.Count>0?dtStatus.Rows[0]["ErrorMessage"].ToString():"");
			}
		}

		private void ExporterCountry_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ExporterCountry_Desc.Text=CustomJobCompilingDAL.GetCountryName(appID,enterpriseID,ExporterCountry.SelectedValue);
		}

		private void CustomsShed_Location_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DataSet ds = CustomJobCompilingDAL.GetDropDownListDisplayValue(appID,enterpriseID,"CustomsTransitShed",CustomsShed_Location.SelectedValue);			
			LocationCustomsShed_Desc.Text="";			
			if(ds.Tables[0].Rows.Count>0)
			{
				LocationCustomsShed_Desc.Text=ds.Tables[0].Rows[0]["Description"].ToString();
			}			
		}

		private void Kind_UOM_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DataSet ds = CustomJobCompilingDAL.GetDropDownListDisplayValue(appID,enterpriseID,"KindOfPkgASYCUDA",Kind_UOM.SelectedValue);			
			Kind_UOM_Desc.Text="";			
			if(ds.Tables[0].Rows.Count>0)
			{
				Kind_UOM_Desc.Text=ds.Tables[0].Rows[0]["Description"].ToString();
			}			
		}

		private void Nature_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DataSet ds = CustomJobCompilingDAL.GetDropDownListDisplayValue(appID,enterpriseID,"NatureASYCUDA",Nature.SelectedValue);			
			Nature_Desc.Text="";			
			if(ds.Tables[0].Rows.Count>0)
			{
				Nature_Desc.Text=ds.Tables[0].Rows[0]["Description"].ToString();
			}			
		}

		private void cbkSameConsignee_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbkSameConsignee.Checked)
			{
				NotifyName.Text=ConsigneeName.Text;
				NotifyAddress1.Text=ConsigneeAddress1.Text;
				NotifyAddress2.Text=ConsigneeAddress2.Text;
				try
				{
					NotifyCountry.SelectedValue=TempConsigneeCountry.Text;
					NotifyCountry_Desc.Text=CustomJobCompilingDAL.GetCountryName(appID,enterpriseID,NotifyCountry.SelectedValue);
				}
				catch
				{

				}				
				
			}
		}

		private void NotifyCountry_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			NotifyCountry_Desc.Text=CustomJobCompilingDAL.GetCountryName(appID,enterpriseID,NotifyCountry.SelectedValue);
		}

		private void consignment_no_TextChanged(object sender, System.EventArgs e)
		{
			lblError.Text="";
			if(consignment_no.Text.Trim() !="")
			{
				string temp=consignment_no.Text;
				BindControl();
				consignment_no.Text=temp;
				SetInitialFocus(QuantityOfKind);
			}
		}

		private void CustomerID_TextChanged(object sender, System.EventArgs e)
		{
			lblError.Text="";
			if(CustomerID.Text.Trim() != "")
			{
				DataSet ds = SysDataMgrDAL.GetCustomerDetails(appID,enterpriseID,CustomerID.Text.Trim());
				if(ds.Tables[0].Rows.Count>0)
				{
					ConsigneeName.Text=ds.Tables[0].Rows[0]["ConsigneeName"].ToString();
					ConsigneeAddress1.Text=ds.Tables[0].Rows[0]["ConsigneeAddress1"].ToString();
					ConsigneeAddress2.Text=ds.Tables[0].Rows[0]["ConsigneeAddress2"].ToString();
					ConsigneeZipcode.Text=ds.Tables[0].Rows[0]["ConsigneeZipcode"].ToString();
					ConsigneeCountry.Text=ds.Tables[0].Rows[0]["ConsigneeState"].ToString();
					TempConsigneeCountry.Text=ds.Tables[0].Rows[0]["ConsigneeCountry"].ToString();
					ConsigneeTelephone.Text=ds.Tables[0].Rows[0]["ConsigneeTelephone"].ToString();
					CustomerTaxCode.Text=ds.Tables[0].Rows[0]["CustomerTaxCode"].ToString();

					ConsigneeNameTemp=ds.Tables[0].Rows[0]["ConsigneeName"].ToString();
					ConsigneeAddress1Temp=ds.Tables[0].Rows[0]["ConsigneeAddress1"].ToString();
					ConsigneeAddress2Temp=ds.Tables[0].Rows[0]["ConsigneeAddress2"].ToString();

					SetInitialFocus(btnDisplayCustDtls);
				}
				else
				{
					ConsigneeName.Text="";
					ConsigneeAddress1.Text="";
					ConsigneeAddress2.Text="";
					ConsigneeZipcode.Text="";
					ConsigneeCountry.Text="";
					TempConsigneeCountry.Text="";
					ConsigneeTelephone.Text="";
					CustomerTaxCode.Text="";
					lblError.Text="Customer ID is invalid or inactive";
				}
			}
			else
			{
				ConsigneeName.Text="";
				ConsigneeAddress1.Text="";
				ConsigneeAddress2.Text="";
				ConsigneeZipcode.Text="";
				ConsigneeCountry.Text="";
				TempConsigneeCountry.Text="";
				ConsigneeTelephone.Text="";
				CustomerTaxCode.Text="";
			}
		}

		private void cbkOverrideConsignee_CheckedChanged(object sender, System.EventArgs e)
		{
			IsOverrideConsignee();
			if(cbkOverrideConsignee.Checked)
			{
				SetInitialFocus(ConsigneeName);
			}
			else
			{
				if(CustomerID.Enabled==true)
				{
					SetInitialFocus(CustomerID);
				}
				else
				{
					if(ConsigneeName.Enabled==true)
					{
						SetInitialFocus(ConsigneeName);
					}
					else
					{
						SetInitialFocus(cbkOverrideConsignee);
					}
				}
				
				
			}
		}

		private void IsOverrideConsignee()
		{
			if(cbkOverrideConsignee.Checked)
			{
				CustomerID.Text="";
				CustomerID.Enabled=false;
				btnDisplayCustDtls.Enabled=false;
				CustomerID.BackColor=System.Drawing.ColorTranslator.FromHtml("#eaeaea");
				ConsigneeName.Text="";
				ConsigneeAddress1.Text="";
				ConsigneeAddress2.Text="";
				ConsigneeZipcode.Text="";
				ConsigneeCountry.Text="";
				TempConsigneeCountry.Text="";
				ConsigneeTelephone.Text="";
				CustomerTaxCode.Text="";

				lblRegConsigneeName.Visible=true;
				lblRegConsigneeAddress1.Visible=true;
				ConsigneeName.Enabled=true;
				ConsigneeAddress1.Enabled=true;
				ConsigneeAddress2.Enabled=true;
				ConsigneeName.BackColor=System.Drawing.Color.White;
				ConsigneeAddress1.BackColor=System.Drawing.Color.White;
				ConsigneeAddress2.BackColor=System.Drawing.Color.White;
			}
			else
			{
				CustomerID.Text="";
				CustomerID.Enabled=true;
				btnDisplayCustDtls.Enabled=true;
				CustomerID.BackColor=System.Drawing.Color.White;

				lblRegConsigneeName.Visible=false;
				lblRegConsigneeAddress1.Visible=false;
				ConsigneeName.Enabled=false;
				ConsigneeAddress1.Enabled=false;
				ConsigneeAddress2.Enabled=false;
				ConsigneeName.BackColor=System.Drawing.ColorTranslator.FromHtml("#eaeaea");
				ConsigneeAddress1.BackColor=System.Drawing.ColorTranslator.FromHtml("#eaeaea");
				ConsigneeAddress2.BackColor=System.Drawing.ColorTranslator.FromHtml("#eaeaea");

				ConsigneeName.Text="";
				ConsigneeAddress1.Text="";
				ConsigneeAddress2.Text="";
				ConsigneeZipcode.Text="";
				ConsigneeCountry.Text="";
				TempConsigneeCountry.Text="";
				ConsigneeTelephone.Text="";
				CustomerTaxCode.Text="";				
			}
		}

		private void cbkSameShipper_CheckedChanged(object sender, System.EventArgs e)
		{
			if(cbkSameShipper.Checked)
			{			
				ExporterName.Text=(Session["ConsolidatorName"] != null?Session["ConsolidatorName"].ToString() : "");
				ExporterAddress1.Text=(Session["ConsolidatorAddress1"] != null?Session["ConsolidatorAddress1"].ToString() : "");
				ExporterAddress2.Text=(Session["ConsolidatorAddress2"] != null?Session["ConsolidatorAddress2"].ToString() : "");
				ExporterCountry.SelectedValue=(Session["ConsolidatorCountry"] != null?Session["ConsolidatorCountry"].ToString() : "");
				ExporterCountry_Desc.Text=(Session["ConsolidatorCountry_Desc"] != null?Session["ConsolidatorCountry_Desc"].ToString() : "");
				if(ConsigneeName.Enabled)
				{
					SetInitialFocus(ConsigneeName);
				}
				else
				{
					SetInitialFocus(ExporterName);
				}

			}
			else
			{
				SetInitialFocus(ExporterName);
			}
		}

		private void btnDisplayCustDtls_Click(object sender, System.EventArgs e)
		{
			String sUrl = "CustomerPopup.aspx?FORMID=HouseAWBDetailEntry";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openLargerChildParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void ExporterName_TextChanged(object sender, System.EventArgs e)
		{
			if(ExporterName.Text.Trim() != "" && ExporterAddress1.Text.Trim() == "")
			{
				ExporterAddress1.Text=ExporterName.Text.Trim();
			}
		}

		private void ConsigneeName_TextChanged(object sender, System.EventArgs e)
		{
			if(ConsigneeName.Text.Trim() != "" && ConsigneeAddress1.Text.Trim() == "")
			{
				ConsigneeAddress1.Text=ConsigneeName.Text.Trim();
			}
		}
	}
}
