using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using TIESDAL;

namespace TIES.WebUI
{
	public class CustomsTariffs : BasePage
	{

		#region Web Form Variable generated code
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.WebControls.Label lbCustomsTariffs;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Button btQuery;
		protected System.Web.UI.WebControls.Button btExecQuery;
		protected System.Web.UI.WebControls.DataGrid gridTariffs;
		protected System.Web.UI.WebControls.TextBox txtTariffCode;
		protected System.Web.UI.WebControls.Label lbID;
		protected System.Web.UI.WebControls.Label lbTariffCode;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btQuery.Click += new System.EventHandler(this.btQuery_Click);
			this.btExecQuery.Click += new System.EventHandler(this.btExecQuery_Click);
			this.gridTariffs.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.gridTariffs_ItemCommand);
			this.gridTariffs.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.gridTariffs_PageIndexChanged);
			this.gridTariffs.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.gridTariffs_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public string GridCmd
		{
			get
			{
				if(ViewState["GridCmd"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["GridCmd"];
				}
			}
			set
			{
				ViewState["GridCmd"]=value;
			}
		}


		private string appID = string.Empty;
		private string enterpriseID = string.Empty;
		private string userLoggin = string.Empty;

		public DataSet dsTariffs
		{
			get
			{
				if(ViewState["dsTariffs"] == null)
					return new DataSet();
				else
					return (DataSet)ViewState["dsTariffs"];
			}
			set
			{
				ViewState["dsTariffs"] = value;
			} 
		}

		public DataTable dtDutyCalc
		{
			get
			{
				if(ViewState["dtDutyCalc"] == null)
					return new DataTable();
				else
					return (DataTable)ViewState["dtDutyCalc"];
			}
			set
			{
				ViewState["dtDutyCalc"] = value;
			} 
		}

		public DataTable dtPermitReq
		{
			get
			{
				if(ViewState["dtPermitReq"] == null)
					return new DataTable();
				else
					return (DataTable)ViewState["dtPermitReq"];
			}
			set
			{
				ViewState["dtPermitReq"] = value;
			} 
		}

		public DataTable dtExciseCalc
		{
			get
			{
				if(ViewState["dtExciseCalc"] == null)
					return new DataTable();
				else
					return (DataTable)ViewState["dtExciseCalc"];
			}
			set
			{
				ViewState["dtExciseCalc"] = value;
			} 
		}

		public string regEx
		{
			get
			{
				if(ViewState["regEx"] == null)
					return string.Empty;
				else
					return (string)ViewState["regEx"];
			}
			set
			{
				ViewState["regEx"] = value;
			} 
		}

		public int NumberScale
		{
			get
			{
				if(ViewState["NumberScale"] == null)
				{
					return 2;
				}
				else
				{
					return (int)ViewState["NumberScale"];
				}
			}
			set
			{
				ViewState["NumberScale"]=value;
			}
		}
		public string currency_decimal
		{
			get
			{
				if(ViewState["currency_decimal"] == null)
				{
					return "00";
				}
				else
				{
					return (string)ViewState["currency_decimal"];
				}
			}
			set
			{
				ViewState["currency_decimal"]=value;
			}
		}
		private DataTable InitDutyCalc()
		{
			//DataTable dt = new DataTable();
			CustomsTariffsDAL db = new CustomsTariffsDAL(this.appID, this.enterpriseID);

			dtDutyCalc = db.GetCustomsSpecialConfiguration(this.enterpriseID, "duty_calc");

			return dtDutyCalc;
		}

		private DataTable InitPermitReq()
		{
			//DataTable dt = new DataTable();
			CustomsTariffsDAL db = new CustomsTariffsDAL(this.appID, this.enterpriseID);

			dtPermitReq = db.GetCustomsSpecialConfiguration(this.enterpriseID, "permit_required");

			return dtPermitReq;
		}

		private DataTable InitExciseCalc()
		{
			//DataTable dt = new DataTable();
			CustomsTariffsDAL db = new CustomsTariffsDAL(this.appID, this.enterpriseID);

			dtExciseCalc = db.GetCustomsSpecialConfiguration(this.enterpriseID, "excise_calc");

			return dtExciseCalc;
		}

		private string GetDutyCalc(string code)
		{
			string result = string.Empty;
			foreach(DataRow dr in dtDutyCalc.Rows)
				if(dr["CodedValue"].ToString().Equals(code))
				{
					result = dr["DropDownListDisplayValue"].ToString();
					break;
				}
			return result;
		}

		private string GetExciseCalc(string code)
		{
			string result = string.Empty;
			foreach(DataRow dr in dtExciseCalc.Rows)
				if(dr["CodedValue"].ToString().Equals(code))
				{
					result = dr["DropDownListDisplayValue"].ToString();
					break;
				}
			return result;
		}

		private string GetPermitReq(string code)
		{
			string result = string.Empty;
			foreach(DataRow dr in dtPermitReq.Rows)
				if(dr["CodedValue"].ToString().Equals(code))
				{
					result = dr["DropDownListDisplayValue"].ToString();
					break;
				}
			return result;
		}

		public string GetCurrencyDigitsOfEnterpriseConfiguration()
		{
			DataSet dsEprise = SysDataManager1.GetEnterpriseProfile(this.appID, this.enterpriseID);
			DataRow dr = dsEprise.Tables[0].Rows[0];
			
			string tempplate = "00";
			if(dr["currency_decimal"].ToString()!="")
			{
				tempplate="";
				int number_digit = Convert.ToInt32(dr["currency_decimal"].ToString());
				NumberScale = number_digit;
				for (int i = 0 ; i< number_digit ; i++)
				{
					tempplate = tempplate+"0";
				}
			}
			return "#,##0." + tempplate;
		}
		private void InitLoad()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("tariff_code");
			dt.Columns.Add("description1");
			dt.Columns.Add("duty_rate");
			dt.Columns.Add("duty_calc");
			dt.Columns.Add("excise_rate");
			dt.Columns.Add("excise_calc");
			dt.Columns.Add("permit_required");

			dt.AcceptChanges();

			gridTariffs.EditItemIndex = -1;
			gridTariffs.CurrentPageIndex = 0;

			gridTariffs.DataSource = dt;
			gridTariffs.DataBind();
		}
		private void InitGridPage()
		{
			CustomsTariffsDAL db = new CustomsTariffsDAL(appID, enterpriseID);
			gridTariffs.PageSize = int.Parse(db.GetEnterpriseConfigurations(enterpriseID));
		}

		private void InitRegEx()
		{
			CustomsTariffsDAL db = new CustomsTariffsDAL(appID, enterpriseID);
			regEx = db.GetRegEx(enterpriseID);
		}
		private void DisplayGridTariffs()
		{
			try
			{
				DataSet dsResult = new DataSet();
				CustomsTariffsDAL db = new CustomsTariffsDAL(this.appID, this.enterpriseID);
				TIESClasses.CustomsTariffs objInfo = new TIESClasses.CustomsTariffs();
				objInfo.EnterpriseId = this.enterpriseID;
				objInfo.UserLoggedin = this.userLoggin;
				objInfo.Action = "0";
				objInfo.TariffCode = this.txtTariffCode.Text.Trim();
				dsResult = db.ExecCustomsTariffs(objInfo);
				
				dsTariffs = dsResult;

				gridTariffs.DataSource = dsResult.Tables[1];
				gridTariffs.DataBind();

				this.lblError.Text = string.Format("{0} row(s) returned", dsResult.Tables[2].Rows[0][0].ToString());
			}
			catch(Exception ex)
			{
				this.lblError.Text = ex.Message;
			}
		}
		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				System.Text.StringBuilder s = new System.Text.StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}

		private	void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings, Page.Session);
				appID = utility.GetAppID();
				enterpriseID = utility.GetEnterpriseID();
				userLoggin = utility.GetUserID();

				if(!Page.IsPostBack)
				{
					GridCmd=null;
					this.InitGridPage();
					this.InitLoad();
					this.InitRegEx();
					this.GetCurrencyDigitsOfEnterpriseConfiguration();
					currency_decimal =  this.GetCurrencyDigitsOfEnterpriseConfiguration();
					SetInitialFocus(txtTariffCode);
				}
			}
			catch(Exception ex)
			{
				this.lblError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void btQuery_Click(object sender, System.EventArgs e)
		{
			try
			{
				GridCmd=null;
				lblError.Text = string.Empty;
				this.InitLoad();
				this.txtTariffCode.Text = string.Empty;
				SetInitialFocus(txtTariffCode);
			}
			catch(Exception ex)
			{
				this.lblError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void btExecQuery_Click(object sender, System.EventArgs e)
		{
			try
			{
				GridCmd=null;
				lblError.Text = string.Empty;
				//if (!System.Text.RegularExpressions.Regex.IsMatch(txtTariffCode.Text.Trim(), regEx))
				//{
				//	lblError.Text = "Invalid tariff code format.";
				//	return;
				//}
				if (txtTariffCode.Text.Trim().Length < 3)
				{
					lblError.Text = "Enter at least 3 digits";
					return;
				}
				DisplayGridTariffs();
			}
			catch(Exception ex)
			{
				this.lblError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void gridTariffs_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
				{
					DataRowView dr = (DataRowView)e.Item.DataItem;

					Label lbdescription1 = (Label)e.Item.FindControl("lbdescription1");

					string description = dr[3].ToString();
					if(lbdescription1 != null)
						lbdescription1.Text = description;

					Label lbDutyCalc = (Label)e.Item.FindControl("lbDutyCalc");
					lbDutyCalc.Text = (dr[5].ToString().Equals("")) ? string.Empty : GetDutyCalc(dr[5].ToString());

					Label lbExciseCalc = (Label)e.Item.FindControl("lbExciseCalc");
					lbExciseCalc.Text = (dr[7].ToString().Equals("")) ? string.Empty : GetExciseCalc(dr[7].ToString());

					Label lbPermitReq = (Label)e.Item.FindControl("lbPermitReq");
					lbPermitReq.Text = (dr[8].ToString().Equals("")) ? string.Empty : GetPermitReq(dr[8].ToString());

					Label lbDutyRate = (Label)e.Item.FindControl("lbDutyRate");
					
					if(!dr["duty_rate"].ToString().Trim().Equals(""))
						lbDutyRate.Text = Convert.ToDecimal(dr["duty_rate"].ToString()).ToString(currency_decimal);

					Label lbExciseRate = (Label)e.Item.FindControl("lbExciseRate");
					if(!dr["excise_rate"].ToString().Trim().Equals(""))
						lbExciseRate.Text = Convert.ToDecimal(dr["excise_rate"].ToString()).ToString(currency_decimal);
				}
				if((e.Item.ItemType == ListItemType.Footer))
				{
					#region Footer
					DropDownList ddlDutyCalcAdd = (DropDownList)e.Item.FindControl("ddlDutyCalcAdd");
					if (ddlDutyCalcAdd != null)
					{
						ddlDutyCalcAdd.DataSource = InitDutyCalc();
						ddlDutyCalcAdd.DataTextField = "DropDownListDisplayValue";
						ddlDutyCalcAdd.DataValueField = "CodedValue";
						ddlDutyCalcAdd.DataBind();
					}
					DropDownList ddlPermitReqAdd = (DropDownList)e.Item.FindControl("ddlPermitReqAdd");
					if (ddlDutyCalcAdd != null)
					{
						ddlPermitReqAdd.DataSource = InitPermitReq();
						ddlPermitReqAdd.DataTextField = "DropDownListDisplayValue";
						ddlPermitReqAdd.DataValueField = "CodedValue";
						ddlPermitReqAdd.DataBind();
					}
					DropDownList ddlExciseCalcAdd = (DropDownList)e.Item.FindControl("ddlExciseCalcAdd");
					if (ddlExciseCalcAdd != null)
					{
						ddlExciseCalcAdd.DataSource = InitExciseCalc();
						ddlExciseCalcAdd.DataTextField = "DropDownListDisplayValue";
						ddlExciseCalcAdd.DataValueField = "CodedValue";
						ddlExciseCalcAdd.DataBind();
					}

					com.common.util.msTextBox txtDutyRateAdd = (com.common.util.msTextBox)e.Item.FindControl("txtDutyRateAdd");
					if(txtDutyRateAdd != null)				
						txtDutyRateAdd.NumberScale = NumberScale;

					com.common.util.msTextBox txtExciseRateAdd = (com.common.util.msTextBox)e.Item.FindControl("txtExciseRateAdd");
					if(txtExciseRateAdd != null)				
						txtExciseRateAdd.NumberScale = NumberScale;

					if(GridCmd=="ADD_ITEM")
					{
						GridCmd=null;
						TextBox txtTariffCodeAdd = (TextBox)e.Item.FindControl("txtTariffCodeAdd");
						SetInitialFocus(txtTariffCodeAdd);
					}
					#endregion
				}
				if((e.Item.ItemType == ListItemType.EditItem))
				{
					DataRowView dr = (DataRowView)e.Item.DataItem;

					com.common.util.msTextBox txtTariffCodeEdit = (com.common.util.msTextBox)e.Item.FindControl("txtTariffCodeEdit");
					txtTariffCodeEdit.Enabled = false;

					TextBox txtDescEdit = (TextBox)e.Item.FindControl("txtDescEdit");
					TextBox txtAddDescEdit = (TextBox)e.Item.FindControl("txtAddDescEdit");

					string description = dr[3].ToString();
					if(description.IndexOf("AND") > -1)
					{
						txtDescEdit.Text = description.Replace("AND","|").Split('|')[0].Trim();
						txtAddDescEdit.Text = description.Replace("AND","|").Split('|')[1].Trim();
					}
					else
						txtDescEdit.Text = description;

					com.common.util.msTextBox txtDutyRateEdit = (com.common.util.msTextBox)e.Item.FindControl("txtDutyRateEdit");
					if(txtDutyRateEdit != null)
					{
						txtDutyRateEdit.NumberScale = NumberScale;

						if(dr["duty_rate"].ToString().Equals(""))
							txtDutyRateEdit.Text = Convert.ToDecimal("0.00").ToString(currency_decimal);
						else
							txtDutyRateEdit.Text = Convert.ToDecimal(dr["duty_rate"].ToString()).ToString(currency_decimal);
					}

					com.common.util.msTextBox txtExciseRateEdit = (com.common.util.msTextBox)e.Item.FindControl("txtExciseRateEdit");
					if(txtExciseRateEdit != null)
					{
						txtExciseRateEdit.NumberScale = NumberScale;

						if(dr["excise_rate"].ToString().Equals(""))
							txtExciseRateEdit.Text = Convert.ToDecimal("0.00").ToString(currency_decimal);
						else
							txtExciseRateEdit.Text = Convert.ToDecimal(dr["duty_rate"].ToString()).ToString(currency_decimal);
					}

					DropDownList ddlDutyCalcEdit = (DropDownList)e.Item.FindControl("ddlDutyCalcEdit");
					if (ddlDutyCalcEdit != null)
					{
						ddlDutyCalcEdit.DataSource = InitDutyCalc();
						ddlDutyCalcEdit.DataTextField = "DropDownListDisplayValue";
						ddlDutyCalcEdit.DataValueField = "CodedValue";
						ddlDutyCalcEdit.DataBind();

						ddlDutyCalcEdit.SelectedValue = dr[5].ToString();
					}

					DropDownList ddlExciseCalcEdit = (DropDownList)e.Item.FindControl("ddlExciseCalcEdit");
					if (ddlExciseCalcEdit != null)
					{
						ddlExciseCalcEdit.DataSource = InitExciseCalc();
						ddlExciseCalcEdit.DataTextField = "DropDownListDisplayValue";
						ddlExciseCalcEdit.DataValueField = "CodedValue";
						ddlExciseCalcEdit.DataBind();

						ddlExciseCalcEdit.SelectedValue = dr[7].ToString();
					}

					DropDownList ddlPermitReqEdit = (DropDownList)e.Item.FindControl("ddlPermitReqEdit");
					if (ddlPermitReqEdit != null)
					{
						ddlPermitReqEdit.DataSource = InitPermitReq();
						ddlPermitReqEdit.DataTextField = "DropDownListDisplayValue";
						ddlPermitReqEdit.DataValueField = "CodedValue";
						ddlPermitReqEdit.DataBind();

						ddlPermitReqEdit.SelectedValue = dr[8].ToString();
					}
				}
			}
			catch(Exception ex)
			{
				this.lblError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void gridTariffs_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			try
			{
				gridTariffs.CurrentPageIndex = e.NewPageIndex;

				gridTariffs.DataSource = this.dsTariffs.Tables[1];
				gridTariffs.DataBind();
			}
			catch(Exception ex)
			{
				this.lblError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void gridTariffs_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
			{
				string command = e.CommandName;
				GridCmd=e.CommandName;
				lblError.Text = string.Empty;

				if(command.Equals("ADD_ITEM"))
				{
					#region ADD_ITEM
					DataSet dsReturn = new DataSet();

					TextBox txtTariffCodeAdd = (TextBox)e.Item.FindControl("txtTariffCodeAdd");
					TextBox txtDescAdd = (TextBox)e.Item.FindControl("txtDescAdd");
					com.common.util.msTextBox txtDutyRateAdd = (com.common.util.msTextBox)e.Item.FindControl("txtDutyRateAdd");
					DropDownList ddlDutyCalcAdd = (DropDownList)e.Item.FindControl("ddlDutyCalcAdd");
					com.common.util.msTextBox txtExciseRateAdd = (com.common.util.msTextBox)e.Item.FindControl("txtExciseRateAdd");
					DropDownList ddlExciseCalcAdd = (DropDownList)e.Item.FindControl("ddlExciseCalcAdd");
					DropDownList ddlPermitReqAdd = (DropDownList)e.Item.FindControl("ddlPermitReqAdd");

					if (!System.Text.RegularExpressions.Regex.IsMatch(txtTariffCodeAdd.Text.Trim(), regEx))
					{
						lblError.Text = "Invalid tariff code format";
						return;
					}
					if(txtDescAdd.Text.Trim().Equals(""))
					{
						this.lblError.Text = "Description is required";
						return;
					}
					if(txtDutyRateAdd.Text.Trim().Equals(""))
					{
						this.lblError.Text = "Duty Calc is required";
						return;
					}

					if(!(ddlDutyCalcAdd.SelectedIndex > 0))
					{
						this.lblError.Text = "Duty Calc is required";
						return;
					}
					if(!txtExciseRateAdd.Text.Trim().Equals(""))
						if(ddlExciseCalcAdd.SelectedIndex == 0)
						{
							this.lblError.Text = "Excise Calc is required when Excise Rate is specified";
							return;
						}
					if(!(ddlPermitReqAdd.SelectedIndex > 0))
					{
						this.lblError.Text = "Please select Yes or No for Permit Required";
						return;
					}

					string description = txtDescAdd.Text.Trim();

					CustomsTariffsDAL db = new CustomsTariffsDAL(this.appID, this.enterpriseID);
					TIESClasses.CustomsTariffs objInfo = new TIESClasses.CustomsTariffs("1",
						this.enterpriseID,
						this.userLoggin,
						txtTariffCodeAdd.Text.Trim(),
						description,
						txtDutyRateAdd.Text.Trim(),
						ddlDutyCalcAdd.SelectedValue,
						txtExciseRateAdd.Text.Trim(),
						ddlExciseCalcAdd.SelectedValue,
						ddlPermitReqAdd.SelectedValue);
					dsReturn = db.RunCustomsTariffs(objInfo);

					if(dsReturn.Tables[0].Rows[0][0].ToString().Equals("0"))
					{
						this.gridTariffs.CurrentPageIndex = 0;
						this.lblError.Text = dsReturn.Tables[0].Rows[0][1].ToString();
						this.gridTariffs.DataSource = dsReturn.Tables[1];
						this.gridTariffs.DataBind();

						dsTariffs = dsReturn;
					}
					else
						this.lblError.Text = dsReturn.Tables[0].Rows[0][1].ToString();
					#endregion
				}
				else if(command.Equals("EDIT_ITEM"))
				{ 
					#region EDIT_ITEM
					gridTariffs.EditItemIndex = e.Item.ItemIndex; 

					gridTariffs.DataSource = dsTariffs.Tables[1];
					gridTariffs.DataBind();

					#endregion
				} 
				else if(command.Equals("SAVE_ITEM"))
				{
					#region SAVE_ITEM
					DataSet dsReturn = new DataSet();

					com.common.util.msTextBox txtTariffCodeEdit = (com.common.util.msTextBox)e.Item.FindControl("txtTariffCodeEdit");
					TextBox txtDescEdit = (TextBox)e.Item.FindControl("txtDescEdit");					
					com.common.util.msTextBox txtDutyRateEdit = (com.common.util.msTextBox)e.Item.FindControl("txtDutyRateEdit");
					DropDownList ddlDutyCalcEdit = (DropDownList)e.Item.FindControl("ddlDutyCalcEdit");
					com.common.util.msTextBox txtExciseRateEdit = (com.common.util.msTextBox)e.Item.FindControl("txtExciseRateEdit");
					DropDownList ddlExciseCalcEdit = (DropDownList)e.Item.FindControl("ddlExciseCalcEdit");
					DropDownList ddlPermitReqEdit = (DropDownList)e.Item.FindControl("ddlPermitReqEdit");

					if (!System.Text.RegularExpressions.Regex.IsMatch(txtTariffCodeEdit.Text.Trim(), regEx))
					{
						lblError.Text = "Invalid tariff code format";
						return;
					}
					if(txtDescEdit.Text.Trim().Equals(""))
					{
						this.lblError.Text = "Description is required";
						return;
					}
					if(!(ddlDutyCalcEdit.SelectedIndex > 0))
					{
						this.lblError.Text = "Duty Calc is required";
						return;
					}
					if(txtDutyRateEdit.Text.Trim().Equals(""))
					{
						this.lblError.Text = "Duty Calc is required";
						return;
					}
					if(!txtExciseRateEdit.Text.Trim().Equals(""))
						if(ddlExciseCalcEdit.SelectedIndex == 0)
						{
							this.lblError.Text = "Excise Calc is required when Excise Rate is specified";
							return;
						}
					if(!(ddlPermitReqEdit.SelectedIndex > 0))
					{
						this.lblError.Text = "Please select Yes or No for Permit Required";
						return;
					}

					string description = txtDescEdit.Text.Trim();

					CustomsTariffsDAL db = new CustomsTariffsDAL(this.appID, this.enterpriseID);
					TIESClasses.CustomsTariffs objInfo = new TIESClasses.CustomsTariffs("1",
						this.enterpriseID,
						this.userLoggin,
						txtTariffCodeEdit.Text.Trim(),
						description,
						txtDutyRateEdit.Text.Trim(),
						ddlDutyCalcEdit.SelectedValue,
						txtExciseRateEdit.Text.Trim(),
						ddlExciseCalcEdit.SelectedValue,
						ddlPermitReqEdit.SelectedValue);
					dsReturn = db.RunCustomsTariffs(objInfo);

					if(dsReturn.Tables[0].Rows[0][0].ToString().Equals("0"))
					{
						this.gridTariffs.CurrentPageIndex = 0;
						this.gridTariffs.EditItemIndex = -1;

						this.lblError.Text = dsReturn.Tables[0].Rows[0][1].ToString();
						this.gridTariffs.DataSource = dsReturn.Tables[1];
						this.gridTariffs.DataBind();

						dsTariffs = dsReturn;
					}
					else
						this.lblError.Text = dsReturn.Tables[0].Rows[0][1].ToString();
					#endregion
				}
				else if(command.Equals("CANCEL_ITEM"))
				{
					gridTariffs.EditItemIndex = -1;

					gridTariffs.DataSource = dsTariffs.Tables[1];
					gridTariffs.DataBind();
				}
				else if(command.Equals("DELETE_ITEM"))
				{
					DataSet dsReturn = new DataSet();

					Label lbID = (Label)e.Item.FindControl("lbID");

					CustomsTariffsDAL db = new CustomsTariffsDAL(this.appID, this.enterpriseID);
					TIESClasses.CustomsTariffs objInfo = new TIESClasses.CustomsTariffs();
					objInfo.Action = "2";
					objInfo.EnterpriseId = this.enterpriseID;
					objInfo.UserLoggedin = this.userLoggin;
					objInfo.TariffCode = lbID.Text.Trim();

					dsReturn = db.ExecCustomsTariffs(objInfo);

					if(dsReturn.Tables[0].Rows[0][0].ToString().Equals("0"))
					{
						this.gridTariffs.CurrentPageIndex = 0;
						this.lblError.Text = dsReturn.Tables[0].Rows[0][1].ToString();
						this.gridTariffs.DataSource = dsReturn.Tables[1];
						this.gridTariffs.DataBind();

						txtTariffCode.Text = string.Empty;

						dsTariffs = dsReturn;
					}
					else
						this.lblError.Text = dsReturn.Tables[0].Rows[0][1].ToString();
				}
			}
			catch(Exception ex)
			{
				this.lblError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}
	}
}
