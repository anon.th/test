<%@ Page language="c#" Codebehind="RetrieveSWBPackages.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.RetrieveSWBPackages" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>RetrieveSWBPackages</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript">
			function cursor_wait() {
			document.body.style.cursor = 'wait';
			}
			 
			function cursor_clear() {
			document.body.style.cursor = 'default';
			}
	
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="RetrievePickUpPkgs" method="post" runat="server">
			<table align="center" width="100%" height="200">
				<tr align="left">
					<td>
						<asp:label id="lblTitle" style="Z-INDEX: 102; LEFT: 10px; POSITION: relative; TOP: -1px" runat="server"
							CssClass="mainTitleSize" Width="477px" Height="26px">Retrieve SWB Packages</asp:label>
					</td>
				</tr>
				<tr align="left" height="50">
					<td>&nbsp;
						<asp:label id="lblErrorMsg" runat="server" Height="19px" Width="920px" CssClass="errorMsgColor"></asp:label>
					</td>
				</tr>
				<tr valign="middle">
					<td align="center">
						<asp:button id="btnRetrieve" runat="server" Width="250px" Height="23px" CssClass="queryButton"
							Text="Retrieve SWB Packages" CausesValidation="False"></asp:button>
					</td>
				</tr>
				<tr align="left" height="50">
					<td align="center">
						<table width="380" border="0" cellpadding="0" cellspacing="1" bgcolor="#ffffff" Height="60">
							<tr bgcolor="#ddd8c2">
								<td valign="middle" align="center">
									<table width="350" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" Height="30">
										<tr bgcolor="#ddd8c2">
											<td valign="middle">
												<asp:label id="lbl" runat="server" CssClass="tableLabel" Width="350" Height="30">
									This will update the package detail from SWB into existing consignment records 
									in the background. The proccess will propably take less than 15 minutes to 
									complete.</asp:label>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
