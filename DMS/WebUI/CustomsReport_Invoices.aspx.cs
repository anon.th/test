using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TIESDAL;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using System.Text.RegularExpressions;
using System.Text;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CustomsReport_Invoices.
	/// </summary>
	public class CustomsReport_Invoices : BasePage
	{
		String strEnterpriseID=null;
		String strAppID=null;
		String strUserLoggin=null; 
		private string m_strCulture;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnPrintSelected;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtdate;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.RadioButton rbInfoReceived;
		protected System.Web.UI.WebControls.RadioButton rbEntryCompiled;
		protected System.Web.UI.WebControls.RadioButton rbExpectedArrival;
		protected System.Web.UI.WebControls.RadioButton rbInvoiced;
		protected System.Web.UI.WebControls.RadioButton rbManifested;
		protected System.Web.UI.WebControls.RadioButton rbDelivered;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.TextBox txtInvoiceNumberTo;
		protected System.Web.UI.WebControls.TextBox txtInvoiceNumberFrom;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.CheckBox chkSearchOnStatusNotAchieved;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.CheckBox chkShowAllModifiedSinceLastPrinted;
		protected System.Web.UI.WebControls.TextBox txtHouseAwbNumber;
		protected System.Web.UI.WebControls.Label lblHouseAwbNumber;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.TextBox txtCustomsDeclarationNo;
		protected System.Web.UI.WebControls.DataGrid gvInvoiceDetails;
		protected System.Web.UI.WebControls.DropDownList ddlCustomerGroup;
		protected System.Web.UI.WebControls.DropDownList ddlBondedWarehouse;
		protected System.Web.UI.WebControls.DropDownList ddlJobStatus;
		protected System.Web.UI.WebControls.DropDownList ddlEntryType;
		protected System.Web.UI.WebControls.DropDownList ddlCustomsProfile;
		protected System.Web.UI.WebControls.TextBox txtCustomerId;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Button btnCustomerPopup;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
	
		public DataSet dsInvoice
		{
			get
			{
				if(ViewState["dsInvoice"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsInvoice"];
				}
			}
			set
			{
				ViewState["dsInvoice"]=value;
			}
		}
 
		public DataSet dsBondedWarehouse
		{
			get
			{
				if(ViewState["dsBondedWarehouse"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsBondedWarehouse"];
				}
			}
			set
			{
				ViewState["dsBondedWarehouse"]=value;
			}
		}
 
		public DataSet dsJobStatus
		{
			get
			{
				if(ViewState["dsJobStatus"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsJobStatus"];
				}
			}
			set
			{
				ViewState["dsJobStatus"]=value;
			}
		}

		public DataSet dsEntryType
		{
			get
			{
				if(ViewState["dsEntryType"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsEntryType"];
				}
			}
			set
			{
				ViewState["dsEntryType"]=value;
			}
		}

		public DataSet dsCustomsProfile
		{
			get
			{
				if(ViewState["dsCustomsProfile"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsCustomsProfile"];
				}
			}
			set
			{
				ViewState["dsCustomsProfile"]=value;
			}
		}

		public DataSet dsCustomerGroup
		{
			get
			{
				if(ViewState["dsCustomerGroup"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsCustomerGroup"];
				}
			}
			set
			{
				ViewState["dsCustomerGroup"]=value;
			}
		}
 
		public Int32 currency_decimal
		{
			get
			{
				if(ViewState["currency_decimal"] == null)
				{
					return 0;
				}
				else
				{
					return (Int32)ViewState["currency_decimal"];
				}
			}
			set
			{
				ViewState["currency_decimal"]=value;
			}
		}

		public Int32 GridRows
		{
			get
			{
				if(ViewState["GridRows"] == null)
				{
					return 0;
				}
				else
				{
					return (Int32)ViewState["GridRows"];
				}
			}
			set
			{
				ViewState["GridRows"]=value;
			}
		}


		public bool IsGridFocus
		{
			get{ return ( bool)ViewState["IsGridFocus"];}
			set{ViewState["IsGridFocus"]=value;}
		}
 

		private void Page_Load(object sender, System.EventArgs e)
		{
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();
			strUserLoggin = utility.GetUserID();
			m_strCulture = utility.GetUserCulture();

			if(!Page.IsPostBack)
			{ 
				btnPrintSelected.Enabled = false;
				IsGridFocus= false;
				currency_decimal =  GetDigitOfEnterpriseConfiguration();
				DataSet ds  = ReportInvoiceDAL.GetGridRows(strAppID,strEnterpriseID);
				if(ds !=null && ds.Tables[0].Rows.Count > 0)
				{
					GridRows = Convert.ToInt16( ds.Tables[0].Rows[0]["value"].ToString());
				}
				gvInvoiceDetails.PageSize = GridRows;
				CustomsJobStatusDAL custom = new CustomsJobStatusDAL();
				dsBondedWarehouse = CustomJobCompilingDAL.GetDropDownListDisplayValue(strAppID,strEnterpriseID,"BondedWarehouses");
				dsJobStatus = custom.GetJobStatus(strAppID,strEnterpriseID);
				dsEntryType = CustomJobCompilingDAL.GetDropDownListDisplayValue(strAppID,strEnterpriseID, "EntryType" );
				dsCustomsProfile = CustomJobCompilingDAL.GetDropDownListDisplayValue(strAppID,strEnterpriseID, "AssignedProfile" );
				//dsEntryType = GetDropDownListDisplayValue("EntryType","GetCustomsConfiguration");
				//dsCustomsProfile = GetDropDownListDisplayValue("AssignedProfile","GetCustomsConfiguration");
				dsCustomerGroup = custom.GetCustomerGroupings(strAppID,strEnterpriseID); 
 
				ddlBondedWarehouse.DataSource = dsBondedWarehouse;
				ddlBondedWarehouse.DataTextField="DropDownListDisplayValue";
				ddlBondedWarehouse.DataValueField="CodedValue";					
				ddlBondedWarehouse.DataBind(); 

				ddlJobStatus.DataSource = dsJobStatus;
				ddlJobStatus.DataTextField="DropDownListDisplayValue";
				ddlJobStatus.DataValueField="CodedValue";					
				ddlJobStatus.DataBind();

				ddlEntryType.DataSource = dsEntryType;
				ddlEntryType.DataTextField="DropDownListDisplayValue";
				ddlEntryType.DataValueField="CodedValue";					
				ddlEntryType.DataBind();

				ddlCustomsProfile.DataSource = dsCustomsProfile;
				ddlCustomsProfile.DataTextField="DropDownListDisplayValue";
				ddlCustomsProfile.DataValueField="CodedValue";					
				ddlCustomsProfile.DataBind();

				ddlCustomerGroup.DataSource = dsCustomerGroup;
				ddlCustomerGroup.DataTextField="DropDownListDisplayValue";
				ddlCustomerGroup.DataValueField="CodedValue";					
				ddlCustomerGroup.DataBind(); 

				DataTable dtMonths = new DataTable();
				dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
				dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));

				DataRow drNilRow = dtMonths.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtMonths.Rows.Add(drNilRow);

				ArrayList listMonthTxt = Utility.GetCodeValues(strAppID,m_strCulture, "months", CodeValueType.StringValue);
				foreach(SystemCode typeSysCode in listMonthTxt)
				{
					DataRow drEach = dtMonths.NewRow();
					drEach[0] = typeSysCode.Text;
					drEach[1] = typeSysCode.StringValue;
					dtMonths.Rows.Add(drEach);
				}
				DataView dvMonths = new DataView(dtMonths);

				ddMonth.DataSource = dvMonths;
				ddMonth.DataTextField = "Text";
				ddMonth.DataValueField = "StringValue";
				ddMonth.DataBind();	

				SetInitialFocus(txtInvoiceNumberFrom);
			}
			//			if (IsGridFocus)
			//			{
			//			SetInitialFocus(gvInvoiceDetails);
			//			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click_1);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnPrintSelected.Click += new System.EventHandler(this.btnPrintSelected_Click);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.gvInvoiceDetails.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.gvInvoiceDetails_ItemCommand);
			this.gvInvoiceDetails.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.gvInvoiceDetails_ItemDataBound);
			this.gvInvoiceDetails.SelectedIndexChanged += new System.EventHandler(this.gvInvoiceDetails_SelectedIndexChanged);
			this.btnCustomerPopup.Click += new System.EventHandler(this.btnCustomerPopup_Click);
			this.ID = "CustomsReport_Invoices";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnPrintSelected_Click(object sender, System.EventArgs e)
		{
			String  strInvoiceList = GenInvoiceList();
			DataSet m_dsQuery = CustomsDAL.Customs_Invoice_Report(utility.GetAppID(), utility.GetEnterpriseID(), utility.GetUserID(), strInvoiceList);
			
			string errorCode = m_dsQuery.Tables[0].Rows[0]["ErrorCode"].ToString();
			string formId = m_dsQuery.Tables[0].Rows[0]["InvoiceTemplate"].ToString();
			
			if(m_dsQuery !=null && m_dsQuery.Tables[0].Rows.Count>0)
			{
				if(Convert.ToInt32( m_dsQuery.Tables[0].Rows[0]["ErrorCode"]) > 0)
				{
					this.lblErrorMsg.Text = m_dsQuery.Tables[0].Rows[0]["ErrorMessage"].ToString();
				}
				else if(Convert.ToString(m_dsQuery.Tables[0].Rows[0]["InvoiceTemplate"]).Equals(String.Empty))
				{
					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"TEMPLATE_NOT_FOUND",utility.GetUserCulture());;
				}
				else
				{
					lblErrorMsg.Text ="";
					String strUrl = null;
					strUrl = "ReportViewer1.aspx";
					String reportTemplate = m_dsQuery.Tables[0].Rows[0]["InvoiceTemplate"].ToString();
					Session["REPORT_TEMPLATE"] = reportTemplate;
					Session["FORMID"] = "Customs_Invoice";
					Session["SESSION_DS_CUSTOMS_INVOICE"] = m_dsQuery;
					Session["FormatType"] = ".pdf";
					ArrayList paramList = new ArrayList();
					paramList.Add(strUrl);
					String sScript = Utility.GetScript("openParentWindowReport.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);					
					////Add by beer 30-09-2014
					//Utility.RegisterScriptString("<script>UncheckAll()</script>","ClearCheckBox",this.Page);
					btnPrintSelected.Enabled = true;
				}
			}
			
			/**
			if(errorCode == "0")
			{
				String strUrl = null;
				strUrl = "ReportViewer1.aspx";
				String reportTemplate = formId;
				Session["REPORT_TEMPLATE"] = reportTemplate;
				Session["FORMID"] = formId;
				Session["SESSION_DS_CUSTOMS_INVOICE"] = m_dsQuery;
				Session["FormatType"] = ".pdf";
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindow.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
			else
			{
				lblError.Text = m_dsQuery.Tables[0].Rows[0]["ErrorMessage"].ToString();
			}
			**/
		}

		
		public  void CleareScreen( ) 
		{
			IsGridFocus = false;
			rbInfoReceived.Checked = true;
			rbMonth.Checked  = true;
			ddMonth.Enabled = true;

			txtYear.Enabled = true;
			txtPeriod.Enabled = false;
			txtTo.Enabled = false;
			txtdate.Enabled = false; 
			btnPrintSelected.Enabled = false; 

			//			txtYear.BackColor = System.Drawing.Color.White;  
			//			txtPeriod.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
			//			txtTo.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
			//			txtdate.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea"); 

			ddMonth.SelectedIndex = 0;
			lblErrorMsg.Text = "";
			txtYear.Text = "";
			txtPeriod.Text = "";
			txtTo.Text = "";
			txtdate.Text = "";

			txtInvoiceNumberFrom.Text = "";
			txtInvoiceNumberTo.Text = "";
			txtHouseAwbNumber.Text = "";
			txtCustomerId.Text = "";
			ddlCustomerGroup.SelectedIndex = 0;
			ddlBondedWarehouse.SelectedIndex = 0;
			ddlJobStatus.SelectedIndex = 0;
			ddlEntryType.SelectedIndex = 0;
			ddlCustomsProfile.SelectedIndex = 0;
			txtCustomsDeclarationNo.Text = "";
			chkSearchOnStatusNotAchieved.Checked = false;
			chkShowAllModifiedSinceLastPrinted.Checked= false;
			gvInvoiceDetails.DataSource = null;
			gvInvoiceDetails.DataBind();

			SetInitialFocus(txtInvoiceNumberFrom);
		}


		private void btnQry_Click(object sender, System.EventArgs e)
		{
			CleareScreen() ;
		}


		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			try
			{
				lblErrorMsg.Text = "";
				DataTable dtParams = ReportInvoiceDAL.SearchInvoicesParam();
				System.Data.DataRow drNew = dtParams.NewRow();
				drNew["enterpriseid"] = strEnterpriseID; 
				DateTime StartDate = DateTime.MinValue;
				DateTime EndDate = DateTime.MinValue;

				if (rbInfoReceived.Checked)
				{
					drNew["DateType"] = "0";
				}
				else if (rbEntryCompiled.Checked)
				{
					drNew["DateType"] = "1";
				}
				else if (rbExpectedArrival.Checked)
				{
					drNew["DateType"] = "2";
				}
				else if (rbInvoiced.Checked)
				{
					drNew["DateType"] = "3";
				}
				else if (rbManifested.Checked)
				{
					drNew["DateType"] = "4";
				}
				else if (rbDelivered.Checked)
				{
					drNew["DateType"] = "5";
				}
 
				#region Temp		
				//			if (rbMonth.Checked)
				//			{
				//				string strStartDate="";				
				//				if (ddMonth.SelectedIndex != 0)
				//				{
				//					if (txtYear.Text.Trim == "")
				//					{
				//						lblError.Text = "Please Enter year format 'YYYY'";
				//						return;					
				//					} 
				//					else
				//					{
				//						int numberOfDaysInMonth = DateTime.DaysInMonth(Convert.ToInt16(txtYear.Text), Convert.ToInt16(ddMonth.SelectedValue));
				//						DateTime startOfMonth = new DateTime(Convert.ToInt16(txtYear.Text), Convert.ToInt16(ddMonth.SelectedValue), 1);    
				//						DateTime endOfMonth = new DateTime(Convert.ToInt16(txtYear.Text), Convert.ToInt16(ddMonth.SelectedValue), numberOfDaysInMonth);
				//
				//						drNew["StartDate"] = GetDate(startOfMonth.ToString("dd/MM/yyyy")); 
				//						drNew["EndDate"] = GetDate(endOfMonth.ToString("dd/MM/yyyy"));
				//					}
				//				} 
				//				else
				//				{
				//					drNew["StartDate"] = DBNull.Value;
				//					drNew["EndDate"] = DBNull.Value;
				//				}
				//			}
				//			else if (rbPeriod.Checked)
				//			{ 
				//				if(txtPeriod.Text.Trim() != "")
				//				{
				//					StartDate=System.DateTime.ParseExact(txtPeriod.Text.Trim(),"dd/MM/yyyy",null);									
				//				}		
				//				if(txtTo.Text.Trim() != "")
				//				{
				//					EndDate=System.DateTime.ParseExact(txtTo.Text.Trim(),"dd/MM/yyyy",null);
				//				}
				//
				//				if(txtPeriod.Text.Trim() != "" && txtTo.Text.Trim() == "")
				//				{
				//					EndDate=StartDate.AddDays(1);
				//				}
				//
				//				if(EndDate != DateTime.MinValue && StartDate == DateTime.MinValue)
				//				{
				//					lblError.Text="Period from is required";
				//					return;
				//				}
				//
				//								drNew["StartDate"] = GetDate(txtPeriod.Text);
				//								drNew["EndDate"] = GetDate(txtTo.Text);
				//							}
				//							else
				//							{
				//								drNew["StartDate"] = GetDate(txtdate.Text);
				//								drNew["EndDate"] = DBNull.Value;
				//			}
				#endregion
				if(rbMonth.Checked)
				{
					string strStartDate="";
					if(ddMonth.SelectedIndex != 0)
					{		
						if (txtYear.Text.Trim() == "" || !Regex.IsMatch(txtYear.Text, @"^[0-9]{4}$"))
						{
							lblErrorMsg.Text = "Year (format: YYYY) is required when month is selected";
							return;
						}

						if(ddMonth.SelectedItem.Value != null && Convert.ToInt32(ddMonth.SelectedItem.Value) < 10)
							strStartDate="01"+"/0"+ddMonth.SelectedItem.Value+"/"+txtYear.Text;
						else
							strStartDate="01"+"/"+ddMonth.SelectedItem.Value+"/"+txtYear.Text.Trim();
						StartDate=System.DateTime.ParseExact(strStartDate,"dd/MM/yyyy",null);				
						EndDate=StartDate.AddMonths(1).AddDays(-1);	
					}				
				}
				if (rbPeriod.Checked==true)
				{	
					if(txtPeriod.Text.Trim() != "")
					{
						StartDate=System.DateTime.ParseExact(txtPeriod.Text.Trim(),"dd/MM/yyyy",null);									
					}		
					if(txtTo.Text.Trim() != "")
					{
						EndDate=System.DateTime.ParseExact(txtTo.Text.Trim(),"dd/MM/yyyy",null);
					}

					if(txtPeriod.Text.Trim() != "" && txtTo.Text.Trim() == "")
					{
						lblErrorMsg.Text="Period end date is required";
						return;
					}

					if(EndDate != DateTime.MinValue && StartDate == DateTime.MinValue)
					{
						lblErrorMsg.Text="Period from is required";
						return;
					}
					TimeSpan diffPeriod = DateTime.ParseExact(txtTo.Text, "dd/MM/yyyy", null) - DateTime.ParseExact(txtPeriod.Text, "dd/MM/yyyy", null);
					if(diffPeriod.TotalDays < 0)
					{
						lblErrorMsg.Text = "Period end date must be greater than or equal to period start date";
						return;
					}
				}
				if (rbDate.Checked==true)
				{				
					if(txtdate.Text.Trim() != "")
					{
						StartDate=System.DateTime.ParseExact(txtdate.Text.Trim(),"dd/MM/yyyy",null);
						EndDate=StartDate.AddDays(1).AddMinutes(-1);	
					}
			
				}
				if(StartDate != DateTime.MinValue)
				{
					drNew["StartDate"] =StartDate.ToString("yyyy-MM-dd");
				}
				if(EndDate != DateTime.MinValue)
				{
					drNew["EndDate"] =EndDate.ToString("yyyy-MM-dd");
				}

				drNew["Invoice_No"] = txtInvoiceNumberFrom.Text;
				drNew["Invoice_No_To"] = txtInvoiceNumberTo.Text;
				drNew["HouseAWBNo"] = txtHouseAwbNumber.Text;
				drNew["CustomerID"] = txtCustomerId.Text;
				drNew["CustomerGroup"] = ddlCustomerGroup.SelectedValue;
				drNew["BondedWarehouse"] = ddlBondedWarehouse.SelectedValue;
				drNew["JobStatus"] = ddlJobStatus.SelectedValue;
				drNew["userloggedin"] = strUserLoggin;
				if (chkSearchOnStatusNotAchieved.Checked)
				{
					drNew["SearchStatusNotAchieved"] = "1";
				}
				else
				{
					drNew["SearchStatusNotAchieved"] = "0";
				}
			
				drNew["EntryType"] = ddlEntryType.SelectedValue;
				drNew["CustomsProfile"] = ddlCustomsProfile.SelectedValue;
				drNew["CustomsDeclarationNo"] = txtCustomsDeclarationNo.Text;
				if (chkShowAllModifiedSinceLastPrinted.Checked)
				{
					drNew["ShowAllModifiedSincePrint"] = "1";
				}
				else
				{
					drNew["ShowAllModifiedSincePrint"] = "0";
				} 
				//drNew["Debug"] = txtHouseAwbNumber.Text;  
				dtParams.Rows.Add(drNew);

				dsInvoice = ReportInvoiceDAL.Customs_SearchInvoices(strAppID,strEnterpriseID , strUserLoggin, dtParams);
												
				gvInvoiceDetails.DataSource = dsInvoice.Tables[0];
				gvInvoiceDetails.DataBind();
				//SetInitialFocus(gvInvoiceDetails);
  
				//Add by beer 30-09-2014
				lblErrorMsg.Text = dsInvoice.Tables[1].Rows[0][0].ToString() + " invoices returned";
				strScrollPosition = "450";

				//CheckBox cbRows = (CheckBox)gvInvoiceDetails.Controls[0].Controls[gvInvoiceDetails.Controls[0].Controls.Count - 2].FindControl("cbRows");
				//SetInitialFocus(cbRows);
			}
			catch
			{}
		}

		
//		protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
//		{
//			IsGridFocus = true;
//			CheckBox chkSelectAll = sender as CheckBox;
//			btnPrintSelected.Enabled = chkSelectAll.Checked;
//			foreach (DataGridItem row in gvInvoiceDetails.Items) 
//			{
//				CheckBox chkSelect = row.FindControl("chkItem") as CheckBox;
//
//				if (chkSelect != null)
//				{
//					chkSelect.Checked = chkSelectAll.Checked;
//				}
//			}
//			//gvInvoiceDetails.com();
//
//			//SetInitialFocus(gvInvoiceDetails);
//		}


		public  string GetDate(string  strDate)
		{
			string[] tempDate; //DateTime.Parse(txtRateDate.Text).ToString("yyyy-MM-dd").ToString();
			string strResult = "";
			try
			{
				tempDate = strDate.Split('/');
				strResult = tempDate[2]+"-"+tempDate[1]+"-" + tempDate[0];
			}
			catch
			{ 
			}
			return strResult;
		}

//
//		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
//		{
//			if (rbMonth.Checked)
//			{ 
//				txtPeriod.Text = "";
//				txtTo.Text = "";
//				txtdate.Text = "";
//				ddMonth.Enabled = true;
//				txtYear.Enabled = true;
//				txtPeriod.Enabled = false;
//				txtTo.Enabled = false;
//				txtdate.Enabled = false; 
//
//				txtYear.BackColor = System.Drawing.Color.White;  
//				txtPeriod.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
//				txtTo.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
//				txtdate.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea"); 
//			}
//			else if (rbPeriod.Checked)
//			{
//				ddMonth.SelectedIndex = 0;
//				txtYear.Text = ""; 
//				txtdate.Text = "";
//
//				ddMonth.Enabled = false;
//				txtYear.Enabled = false;
//				txtPeriod.Enabled = true;
//				txtTo.Enabled = true;
//				txtdate.Enabled = false;
//
//				txtYear.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
//				txtPeriod.BackColor = System.Drawing.Color.White;  
//				txtTo.BackColor = System.Drawing.Color.White;  
//				txtdate.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea"); 
// 
//			}
//			else
//			{
//				ddMonth.SelectedIndex = 0;
//				txtYear.Text = "";   
//				txtPeriod.Text = "";  
//				txtTo.Text = "";   
//				ddMonth.Enabled = false;
//				txtYear.Enabled = false;
//				txtPeriod.Enabled = false;
//				txtTo.Enabled = false;
//				txtdate.Enabled = true;
//
//				txtYear.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
//				txtPeriod.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
//				txtTo.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
//				txtdate.BackColor = System.Drawing.Color.White;  
//			}
//		}
//

		public  string GenInvoiceList( )
		{
			//Label12.Text = "GenInvoiceList";
			string strResult = "";
			try
			{
				foreach (DataGridItem row in gvInvoiceDetails.Items) 
				{
					//HtmlInputCheckBox chkSelect = (HtmlInputCheckBox)row.FindControl("chkItem");
					//HtmlInputCheckBox heckBox1 =  row.FindControl("CheckBox1") as HtmlInputCheckBox;
					//HtmlInputCheckBox chkSelect =  row.FindControl("cbRows") as HtmlInputCheckBox;

					Label lblInvoiceNo = row.FindControl("lblInvoiceNo") as Label;
					CheckBox chkSelect = row.FindControl("cbRows") as CheckBox;
					 
					if (chkSelect != null)
					{
						if (chkSelect.Checked)
						{
						strResult = strResult + ";"+ lblInvoiceNo.Text;
						} 
					}
					else
					{
					    Label12.Text = Label12.Text + ":" + chkSelect.ToString();
					}
				}
				if (strResult != "")
				{
					if (strResult.StartsWith(";"))
					{
						strResult = strResult.Substring(1,strResult.Length-1);
					}
					else
					{
						strResult = strResult.Substring(0,strResult.Length-1);
					}
				}
				
			}
			catch (Exception ex)
			{
				//Label12.Text = "GenInvoiceList" + ":" +ex.Message;
			}
			//Label12.Text = Label12.Text+ ":" +strResult;
			return strResult;

			
		}


		private void gvInvoiceDetails_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			try
			{
				gvInvoiceDetails.CurrentPageIndex = e.NewPageIndex;
				gvInvoiceDetails.DataSource = dsInvoice;
				gvInvoiceDetails.DataBind();
			}
			catch
			{			
				gvInvoiceDetails.DataSource = null;
				gvInvoiceDetails.DataBind();
			}
		}


		private void gvInvoiceDetails_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			Label lblInvoiceAmount = (Label)e.Item.FindControl("lblInvoiceAmount");
			if (lblInvoiceAmount != null)
			{
				string tempplate = "";
				for (int i = 0 ; i< currency_decimal ; i++)
				{
					tempplate = tempplate+"0";
				}
				if(lblInvoiceAmount.Text != "")
				{
					lblInvoiceAmount.Text =  Convert.ToDecimal(lblInvoiceAmount.Text).ToString("###,###,##0." + tempplate);  //dtAllData.Rows[0]["Duty_Amount"].ToString(); 
				} 
			}
		}


		public int GetDigitOfEnterpriseConfiguration()
		{
			DataSet dsEprise = SysDataManager1.GetEnterpriseProfile(utility.GetAppID(),utility.GetEnterpriseID());
			DataRow dr = dsEprise.Tables[0].Rows[0];
			
			int  number_digit = 3;
			if(dr["currency_decimal"].ToString()!="")
			{
				number_digit = Convert.ToInt32(dr["currency_decimal"].ToString());
			}

			return number_digit;
		}


		protected void chkSelect_CheckedChanged(object sender, EventArgs e)
		{ 
			IsGridFocus = true;
			bool isSelect = false;
			//CheckBox chkSelect new CheckBox;
			foreach (DataGridItem row in gvInvoiceDetails.Items) 
			{
				CheckBox chkSelect = row.FindControl("chkItem") as CheckBox;

				if (chkSelect != null)
				{
					if (chkSelect.Checked )
					{
						//SetInitialFocus(chkSelect);
						isSelect = true;
						break;
					}
				}			
			}
 
			btnPrintSelected.Enabled = isSelect;
			//SetInitialFocus(chkSelect);
		}


		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}


		private DataSet GetDropDownListDisplayValue(String codeID, String sqlFunction)
		{
			DataSet ds = new DataSet();
			ds = CustomJobCompilingDAL.GetDropDownListDisplayValue(utility.GetAppID(),utility.GetEnterpriseID(), codeID, sqlFunction);
			return ds;
		}


		private void btnQry_Click_1(object sender, System.EventArgs e)
		{
		CleareScreen( ) ;
		}


		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;

			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;

			txtdate.Text = null;
			txtdate.Enabled = false;

			SetInitialFocus(ddMonth);
		}


		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			ddMonth.Enabled = false;
			ddMonth.SelectedIndex = 0;
			txtYear.Text = null;
			txtYear.Enabled = false;

			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;

			txtdate.Text = null;
			txtdate.Enabled = false;

			SetInitialFocus(txtPeriod);
		}


		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			ddMonth.Enabled = false;
			ddMonth.SelectedIndex = 0;
			txtYear.Text = null;
			txtYear.Enabled = false;

			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;

			txtdate.Text = null;
			txtdate.Enabled = true;

			SetInitialFocus(txtdate);
		}


		private void gvInvoiceDetails_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			bool isSelect = false;
			//CheckBox chkSelect new CheckBox;
			foreach (DataGridItem row in gvInvoiceDetails.Items) 
			{
				CheckBox chkSelect = row.FindControl("chkItem") as CheckBox;

				if (chkSelect != null)
				{
					if (chkSelect.Checked )
					{
						//SetInitialFocus(chkSelect);
						isSelect = true;
						break;
					}
				}			
			}
 
			btnPrintSelected.Enabled = isSelect;
			//SetInitialFocus(chkSelect);
		}


		private void gvInvoiceDetails_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
		
		}

		private void btnCustomerPopup_Click(object sender, System.EventArgs e)
		{
			String sUrl = "CustomerPopup.aspx?FORMID=CustomsReportInvoices";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
	}
}
