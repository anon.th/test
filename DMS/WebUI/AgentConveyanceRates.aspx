<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="AgentConveyanceRates.aspx.cs" AutoEventWireup="false" Inherits="com.ties.AgentConveyanceRates" smartNavigation="false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>AgentConveyanceRates</title>
		<link href="css/styles.css" rel="stylesheet" rev="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="AgentConveyanceRates" method="post" runat="server">
			<asp:Label id="lblTitle" style="Z-INDEX: 104; LEFT: 24px; POSITION: absolute; TOP: 8px" runat="server" CssClass="mainTitleSize" Height="32px" Width="336px">Agent Zone Rates by Conveyance</asp:Label>
			<asp:Button id="btnInsert" style="Z-INDEX: 114; LEFT: 215px; POSITION: absolute; TOP: 41px" runat="server" Width="80px" CssClass="queryButton" Text="Insert" CausesValidation="False"></asp:Button>
			<asp:button id="btnGoToFirstPage" style="Z-INDEX: 113; LEFT: 600px; POSITION: absolute; TOP: 41px" tabIndex="5" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text="|<"></asp:button>
			<asp:button id="btnPreviousPage" style="Z-INDEX: 106; LEFT: 624px; POSITION: absolute; TOP: 41px" tabIndex="6" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text="<"></asp:button>
			<asp:textbox id="txtCountRec" style="Z-INDEX: 110; LEFT: 648px; POSITION: absolute; TOP: 41px" tabIndex="7" runat="server" Height="19px" Width="24px"></asp:textbox>
			<asp:button id="btnNextPage" style="Z-INDEX: 107; LEFT: 672px; POSITION: absolute; TOP: 41px" tabIndex="8" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text=">"></asp:button>
			<asp:button id="btnGoToLastPage" style="Z-INDEX: 109; LEFT: 696px; POSITION: absolute; TOP: 41px" tabIndex="9" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text=">|"></asp:button>
			<asp:label id="lblAgentName" style="Z-INDEX: 111; LEFT: 384px; POSITION: absolute; TOP: 112px" runat="server" CssClass="tableLabel" Width="100px">Agent Name</asp:label>
			<asp:label id="lblAgentCode" style="Z-INDEX: 108; LEFT: 24px; POSITION: absolute; TOP: 112px" runat="server" CssClass="tableLabel" Width="100px">Agent Code</asp:label>
			<asp:Button id="btnQuery" style="Z-INDEX: 101; LEFT: 22px; POSITION: absolute; TOP: 41px" runat="server" CssClass="queryButton" Width="62px" CausesValidation="False" Text="Query"></asp:Button>
			<asp:Button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 84px; POSITION: absolute; TOP: 41px" runat="server" CssClass="queryButton" Width="130px" CausesValidation="False" Text="Execute Query"></asp:Button>
			<asp:Label id="lblErrorMessage" style="Z-INDEX: 103; LEFT: 24px; POSITION: absolute; TOP: 72px" runat="server" CssClass="errorMsgColor" Width="696px">Error Message</asp:Label>
			<asp:RequiredFieldValidator ID="reqAgentId" Runat="server" BorderWidth="0" Display="None" ControlToValidate="dbCmbAgentId" ErrorMessage="Agent Id "></asp:RequiredFieldValidator>
			<asp:validationsummary id="PageValidationSummary" style="Z-INDEX: 105; LEFT: 606px; POSITION: absolute; TOP: 5px" runat="server" ShowMessageBox="True" ShowSummary="False" DisplayMode="BulletList" HeaderText="Please enter the missing  fields."></asp:validationsummary>
			<TABLE id="tblMain" style="Z-INDEX: 112; LEFT: 16px; WIDTH: 712px; POSITION: absolute; TOP: 144px; HEIGHT: 307px" width="712" runat="server">
				<TR>
					<TD style="HEIGHT: 297px" vAlign="top" colSpan="19">
						<asp:DataGrid id="dgBaseConveyance" runat="server" Width="704px" AllowCustomPaging="True" AllowPaging="True" AutoGenerateColumns="False" HorizontalAlign="Left" OnEditCommand="dgBaseConvey_Edit" OnCancelCommand="dgBaseConvey_Cancel" OnItemDataBound="dgBaseConvey_ItemDataBound" OnDeleteCommand="dgBaseConvey_Delete" OnUpdateCommand="dgBaseConvey_Update" OnPageIndexChanged="dgBaseConvey_PageChange" OnItemCommand="dgBaseConvey_Button">
							<ItemStyle HorizontalAlign="Left" CssClass="gridfield"></ItemStyle>
							<Columns>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
									<HeaderStyle Width="7%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:EditCommandColumn>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
									<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Origin Zone Code" HeaderStyle-Font-Bold="true">
									<HeaderStyle CssClass="gridHeading" Width="20%"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblOZCode" Text='<%#DataBinder.Eval(Container.DataItem,"origin_zone_code")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBox" ID="txtOZCode" Text='<%#DataBinder.Eval(Container.DataItem,"origin_zone_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" >
										</cc1:mstextbox>
										<asp:RequiredFieldValidator ID="ozcValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtOZCode" ErrorMessage="Origin Zone Code "></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton" CommandName="OriginSearch" Visible="true">
									<HeaderStyle CssClass="gridHeading" Width="3%"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Destination Zone Code" HeaderStyle-Font-Bold="true">
									<HeaderStyle CssClass="gridHeading" Width="20%"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblDZCode" Text='<%#DataBinder.Eval(Container.DataItem,"destination_zone_code")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBox" ID="txtDZCode" Text='<%#DataBinder.Eval(Container.DataItem,"destination_zone_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" >
										</cc1:mstextbox>
										<asp:RequiredFieldValidator ID="odcValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtDZCode" ErrorMessage="Destination Zone Code "></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton" CommandName="DestinationSearch" Visible="true">
									<HeaderStyle CssClass="gridHeading" Width="3%"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Conveyance Code" HeaderStyle-Font-Bold="true">
									<HeaderStyle HorizontalAlign="Left" Width="20%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label ID="lblConveyance" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"conveyance_code")%>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBox" ID="txtConveyance" Text='<%#DataBinder.Eval(Container.DataItem,"conveyance_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
										</cc1:mstextbox>
										<asp:RequiredFieldValidator ID="cConveyance" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtConveyance" ErrorMessage="Conveyance Code "></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton" CommandName="ConveyanceSearch" Visible="true">
									<HeaderStyle CssClass="gridHeading" Width="3%"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Start Price">
									<HeaderStyle HorizontalAlign="Left" Width="10%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label ID="lblPrice" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Start_Price","{0:#0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBox" ID="txtStartPrice" Text='<%#DataBinder.Eval(Container.DataItem,"Start_Price")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" >
										</cc1:mstextbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Incremental Price/Kg">
									<HeaderStyle HorizontalAlign="Left" Width="11%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label ID="Label1" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Increment_Price","{0:#0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBox" ID="txtIncrementPrice" Text='<%#DataBinder.Eval(Container.DataItem,"Increment_Price")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" >
										</cc1:mstextbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
						</asp:DataGrid></TD>
					<TD style="HEIGHT: 297px" vAlign="top" align="left" colSpan="1"></TD>
				</TR>
			</TABLE>
			<INPUT type=hidden value="<%=strScrollPosition%>" name="ScrollPosition">
			<dbcombo:dbcombo id="dbCmbAgentName" tabIndex="2" runat="server" AutoPostBack="True" TextBoxColumns="25" ServerMethod="AgentNameServerMethod" TextUpLevelSearchButton="v" ShowDbComboLink="False" ReQueryOnLoad="True" style="Z-INDEX: 115; LEFT: 496px; POSITION: absolute; TOP: 104px" RegistrationKey='<%#System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"]%>'>
			</dbcombo:dbcombo>
			<dbcombo:dbcombo id="dbCmbAgentId" tabIndex="1" runat="server" AutoPostBack="True" TextBoxColumns="20" ServerMethod="AgentIdServerMethod" TextUpLevelSearchButton="v" ShowDbComboLink="False" style="Z-INDEX: 116; LEFT: 128px; POSITION: absolute; TOP: 104px" ReQueryOnLoad="True" RegistrationKey='<%#System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"]%>'>
			</dbcombo:dbcombo>
		</form>
	</body>
</HTML>
