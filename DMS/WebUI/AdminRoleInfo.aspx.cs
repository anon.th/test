using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.RBAC;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.common.applicationpages;
using System.Collections.Generic;

namespace TIES.WebUI
{
    /// <summary>
    /// Summary description for AdminRoleInfo.
    /// </summary>
    public class AdminRoleInfo : BasePage
    {
        protected System.Web.UI.WebControls.Label lblRoleID;
        protected com.common.util.msTextBox txtRoleID;
        protected System.Web.UI.WebControls.Label lblRoleName;
        protected System.Web.UI.WebControls.Label lblRoleDesc;
        protected System.Web.UI.WebControls.Button btnSelectAll;
        protected System.Web.UI.WebControls.Button btnUnSelectAll;
        protected System.Web.UI.WebControls.CheckBox chkGrantsUpd;
        protected System.Web.UI.WebControls.CheckBox chkGrantsIns;
        protected System.Web.UI.WebControls.Label lblInsRec;
        protected System.Web.UI.WebControls.CheckBox chkGrantsDel;
        protected System.Web.UI.WebControls.TreeView funcTree;
        protected System.Web.UI.WebControls.Label lblUpdRec;
        protected System.Web.UI.WebControls.Label lblDelRec;
        protected System.Web.UI.HtmlControls.HtmlTable tblRoleInfo;
        private System.Web.UI.WebControls.TreeNode treeNode;
        private System.Web.UI.WebControls.TreeNode tiesNode;
        ArrayList childList = new ArrayList();
        protected com.common.util.msTextBox txtRoleName;
        private String appID = null;
        private String enterpriseID = null;
        protected System.Web.UI.WebControls.Button btnSave;
        bool isChecked = false;
        protected System.Web.UI.WebControls.CheckBox CheckBox1;
        protected System.Web.UI.WebControls.Button btnQry;
        protected System.Web.UI.WebControls.Button btnExecQry;
        private decimal iDisPlayRole;
        protected System.Web.UI.WebControls.Button btnGoToFirstPage;
        protected System.Web.UI.WebControls.Button btnPreviousPage;
        protected System.Web.UI.WebControls.TextBox txtCountRec;
        protected System.Web.UI.WebControls.Button btnNextPage;
        protected System.Web.UI.WebControls.Button btnGoToLastPage;
        protected System.Web.UI.WebControls.Button btnDelete;
        protected System.Web.UI.WebControls.Button btnInsert;
        protected System.Web.UI.WebControls.Label lblMessages;
        protected System.Web.UI.WebControls.Label lblMainHeader;
        protected System.Web.UI.WebControls.Label reqRoleID;
        protected System.Web.UI.WebControls.Label reqRoleNm;
        protected System.Web.UI.HtmlControls.HtmlGenericControl RoleAdminPanel;
        protected System.Web.UI.WebControls.Button btnToCancel;
        protected System.Web.UI.WebControls.Button btnNotToSave;
        protected System.Web.UI.WebControls.Button btnToSaveChanges;
        protected System.Web.UI.WebControls.Label lblConfirmMsg;
        protected com.common.util.msTextBox txtRoleDesc;
        protected System.Web.UI.HtmlControls.HtmlTable tblPermissions;
        protected System.Web.UI.HtmlControls.HtmlTable tblButtons;
        protected System.Web.UI.HtmlControls.HtmlTable tblAdditionalRoles;
        protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
        protected System.Web.UI.WebControls.Label lblprevillage;
        protected System.Web.UI.WebControls.Label lblRlInfo;
        protected System.Web.UI.WebControls.Label lblAddFnRl;
        protected System.Web.UI.WebControls.Label lblConfirmation;
        ArrayList m_QryList = null;


        private void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            //utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
            appID = utility.GetAppID();
            enterpriseID = utility.GetEnterpriseID();

            if (!Page.IsPostBack)
            {
                ViewState["isTextChange"] = false;
                ViewState["isQueryMode"] = false;
                ViewState["isDirty"] = false;
                ViewState["iPageIndex"] = 0;
                ViewState["iTotalRoles"] = 0;
                ViewState["iCreatedRoleID"] = 0;
                Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete, false, m_moduleAccessRights);
                btnExecQry.Enabled = true;
                Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, false, m_moduleAccessRights);
                btnGoToFirstPage.Enabled = false;
                btnGoToLastPage.Enabled = false;
                btnNextPage.Enabled = false;
                btnPreviousPage.Enabled = false;
                Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                btnQry.Enabled = true;
                Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, false, m_moduleAccessRights);
                Module module = new Module();
                module.ModuleId = "root";
                treeNode = BuildModulesTree(module);
                tiesNode = this.CloneTreeNode(treeNode.ChildNodes[0]);
                funcTree.PathSeparator = '|';
                funcTree.Nodes.AddAt(0, tiesNode);
                funcTree.CollapseAll();
                funcTree.Enabled = false;
                btnSelectAll.Enabled = false;
                btnUnSelectAll.Enabled = false;
                chkGrantsDel.Enabled = false;
                chkGrantsIns.Enabled = false;
                chkGrantsUpd.Enabled = false;
                RoleAdminPanel.Visible = false;
                
                ViewState["NextOperation"] = "None";
                funcTree.Attributes.Add("onclick", "postBackByObject()");//addnew for treenode event
            }
            else
            {
                tiesNode = funcTree.Nodes[0];
            }
        }
        

        public TreeNode BuildModulesTree(Module parentModule)
        {
            TreeNode node = new TreeNode();

            if (parentModule.ModuleName != null)
            {
                //node.Text = Utility.GetLanguageText(ResourceType.ModuleName,parentModule.ModuleName,utility.GetUserCulture());

                String strNodeText = "<font face=" + System.Configuration.ConfigurationSettings.AppSettings["treeFontName"] + " size=" + System.Configuration.ConfigurationSettings.AppSettings["treeFontSize"] + ">";
                strNodeText += Utility.GetLanguageText(ResourceType.ModuleName, parentModule.ModuleName, utility.GetUserCulture());
                strNodeText += "</font>";

                node.Text = strNodeText;
            }

            if (parentModule.ModuleIconImage != null)
            {
                node.ImageUrl = parentModule.ModuleIconImage;
            }

            node.ShowCheckBox = true;
            node.Value = "IDU" + ":" + parentModule.ModuleId;
            String moduleID = parentModule.ModuleId;
            childList = (ArrayList)RBACManager.GetCoreModules(appID, moduleID);
            int cnt = childList.Count;
            foreach (Module child in childList)
            {
                TreeNode childNode = BuildModulesTree(child);
                node.SelectAction = TreeNodeSelectAction.Expand;
                node.ChildNodes.Add(childNode);
            }
            
            return node;
        }

       
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            base.OnInit(e);
            InitializeComponent();

        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
            this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
            this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
            this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
            this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
            this.txtRoleID.TextChanged += new System.EventHandler(this.txtRoleID_TextChanged);
            this.txtRoleName.TextChanged += new System.EventHandler(this.txtRoleName_TextChanged);
            this.txtRoleDesc.TextChanged += new System.EventHandler(this.txtRoleDesc_TextChanged);
            this.funcTree.SelectedNodeChanged += new EventHandler(this.funcTree_SelectedNodeChanged);
            this.funcTree.TreeNodeCheckChanged += new TreeNodeEventHandler(this.funcTree_TreeNodeCheckChanged);
            //this.funcTree.SelectedNodeChanged += new System.Web.UI.WebControls.TreeNodeEventHandler(this.SelectedNodeChange);
            //this.funcTree.Check += new System.Web.UI.WebControls.ClickEventHandler(this.funcTree_SelectedCheckChange);
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            this.btnUnSelectAll.Click += new System.EventHandler(this.btnUnSelectAll_Click);
            this.chkGrantsIns.CheckedChanged += new System.EventHandler(this.chkGrantsIns_CheckedChanged);
            this.chkGrantsUpd.CheckedChanged += new System.EventHandler(this.chkGrantsUpd_CheckedChanged);
            this.chkGrantsDel.CheckedChanged += new System.EventHandler(this.chkGrantsDel_CheckedChanged);
            this.btnToSaveChanges.Click += new System.EventHandler(this.btnToSaveChanges_Click);
            this.btnNotToSave.Click += new System.EventHandler(this.btnNotToSave_Click);
            this.btnToCancel.Click += new System.EventHandler(this.btnToCancel_Click);
            this.Load += new System.EventHandler(this.Page_Load);
        }
        #endregion
        //
        private void CheckAllChildNodes(TreeNode treeNode, bool nodeChecked)
        {
            foreach (TreeNode node in funcTree.Nodes)
            {
                node.Checked = nodeChecked;
                if (funcTree.Nodes.Count > 0)
                {
                    this.CheckAllChildNodes(node, nodeChecked);
                }
            }
        }
        

        private void funcTree_TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            ViewState["isTextChange"] = true;
            //funcTree.Nodes[0].Checked = false;
            String strIndex = e.Node.ValuePath;

            if (funcTree.FindNode(strIndex).ValuePath != funcTree.Nodes[0].ValuePath)
            {
                if ((funcTree.FindNode(strIndex).ChildNodes.Count > 0))//check and uncheck childnode when check parent
                {
                    TreeNodeCollection childNodeCollection = funcTree.FindNode(strIndex).ChildNodes;
                    foreach (TreeNode treeNode in childNodeCollection)
                    {
                        treeNode.Checked = e.Node.Checked;
                    }
                }
                else if (funcTree.FindNode(strIndex).ChildNodes.Count <= 0)//check and uncheck parentnode when check child
                {
                    TreeNode parentNode = funcTree.FindNode(strIndex).Parent;
                    TreeNodeCollection treeNodeCollection = parentNode.ChildNodes;
                    parentNode.Checked = false;
                    foreach (TreeNode treeNode in treeNodeCollection)
                    {
                        if (treeNode.Checked)
                        {
                            parentNode.Checked = true;
                            break;
                        }
                    }
                }
            }

            funcTree.Nodes[0].Checked = false;
        }


        private void SelectAllNodes(TreeNode rootNode, bool btnFlag)
        {

            rootNode.Checked = btnFlag;
            TreeNodeCollection treeNodeCollection = rootNode.ChildNodes;
            foreach (TreeNode node in treeNodeCollection)
            {
                SelectAllNodes(node, btnFlag);
            }
        }

        private void btnDelete_Click(object sender, System.EventArgs e)
        {
            lblMessages.Text = "";
            String strMsg = "";
            btnSelectAll.Enabled = false;
            btnUnSelectAll.Enabled = false;

            if (((bool)ViewState["isTextChange"] == true) && ((bool)ViewState["isQueryMode"] == false))
            {
                lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SAVE_PROMPT", utility.GetUserCulture());
                RoleAdminPanel.Visible = true;
                divMain.Visible = false;
            }

            else
            {
                ViewState["isQueryMode"] = false;
                if (txtRoleID.Text.Trim().Length > 0)
                {
                    Role role = new Role();
                    //Before deleting role from the role master check whether any other user is associated with this role
                    role.RoleID = Convert.ToInt32(txtRoleID.Text.Trim());
                    ArrayList roleUserList = RBACManager.GetAllRoleMemebers(appID, enterpriseID, role);
                    if (roleUserList.Count == 0)
                    {
                        try
                        {
                            RBACManager.DeleteModuleNRole(appID, enterpriseID, Convert.ToInt32(txtRoleID.Text.Trim()));
                        }
                        catch (ApplicationException appException)
                        {
                            String strMessage = appException.Message;
                            if (strMessage.IndexOf("duplicate key") != -1)
                            {
                                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "DUP_KEY_FOUND", utility.GetUserCulture());
                            }
                            else if (strMessage.IndexOf("FK") != -1)
                            {
                                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "FOREIGN_KEY_FOUND", utility.GetUserCulture());
                            }
                            else if (strMessage.IndexOf("child record") != -1)
                            {
                                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "FOREIGN_KEY_FOUND", utility.GetUserCulture());
                            }
                            else
                            {
                                lblMessages.Text = strMessage;
                            }

                            return;
                        }
                        strMsg = Utility.GetLanguageText(ResourceType.UserMessage, "DELETED_SUCCESSFULLY", utility.GetUserCulture());
                    }
                    else
                    {
                        strMsg = Utility.GetLanguageText(ResourceType.UserMessage, "NO_DEL", utility.GetUserCulture());
                    }
                    ArrayList roleArray = (ArrayList)Session["RoleArray"];
                    if ((roleArray != null) && (roleArray.Count > 0))
                    {
                        foreach (Role sesRole in roleArray)
                        {
                            if (sesRole.RoleID == Convert.ToInt32(txtRoleID.Text.Trim()))
                            {
                                roleArray.Remove(sesRole);
                                break;
                            }
                        }
                        Session.Add("RoleArray", roleArray);
                    }

                    ViewState["iTotalRoles"] = roleArray.Count;
                    ViewState["iPageIndex"] = (int)ViewState["iPageIndex"] + 1;
                    if ((int)ViewState["iTotalRoles"] == (int)ViewState["iPageIndex"])
                    {
                        ViewState["iPageIndex"] = (int)ViewState["iPageIndex"] - 1;
                        GetFunctionalRoles((int)ViewState["iPageIndex"]);
                    }
                    else if ((int)ViewState["iTotalRoles"] > (int)ViewState["iPageIndex"])
                    {
                        ViewState["iPageIndex"] = (int)ViewState["iPageIndex"] - 1;
                        GetFunctionalRoles((int)ViewState["iPageIndex"]);
                    }
                    else if ((int)ViewState["iTotalRoles"] < (int)ViewState["iPageIndex"])
                    {
                        ViewState["iPageIndex"] = (int)ViewState["iTotalRoles"] - 1;
                        GetFunctionalRoles((int)ViewState["iPageIndex"]);
                    }
                    else
                    {
                        txtRoleID.Text = "";
                        txtRoleName.Text = "";
                        txtRoleDesc.Text = "";
                        SelectAllNodes(tiesNode, false);
                    }
                }
            }
            lblMessages.Text = strMsg;
            ViewState["isTextChange"] = false;
        }

        private void btnInsert_Click(object sender, System.EventArgs e)
        {
            Insert_Click();
        }
        private void Insert_Click()
        {
            lblMessages.Text = "";
            if ((bool)ViewState["isDirty"] == false)
            {
                if ((bool)ViewState["isTextChange"] == true)
                {
                    lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SAVE_PROMPT", utility.GetUserCulture());
                    RoleAdminPanel.Visible = true;
                    divMain.Visible = false;
                    ViewState["NextOperation"] = "Insert";
                    ViewState["isTextChange"] = false;
                }
                else
                {
                    ViewState["isQueryMode"] = false;
                    funcTree.Enabled = true;
                    chkGrantsDel.Enabled = true;
                    chkGrantsIns.Enabled = true;
                    chkGrantsUpd.Enabled = true;
                    Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, true, m_moduleAccessRights);
                    Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, false, m_moduleAccessRights);
                    txtRoleID.Enabled = false;
                    Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete, false, m_moduleAccessRights);
                    funcTree.Enabled = true;
                    SelectAllNodes(tiesNode, false);
                    txtRoleDesc.Text = "";
                    txtRoleName.Text = "";
                    txtRoleID.Text = "";
                    chkGrantsDel.Checked = false;
                    chkGrantsIns.Checked = false;
                    chkGrantsUpd.Checked = false;
                    btnSelectAll.Enabled = true;
                    btnUnSelectAll.Enabled = true;
                }
            }
            else
            {
                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "CLK_QUERY_INSERT_PROCEED", utility.GetUserCulture());
                Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                ViewState["isDirty"] = false;
            }
        }

        private void btnPreviousPage_Click(object sender, System.EventArgs e)
        {
            MovePreviousDS();
        }
        private void MovePreviousDS()
        {
            lblMessages.Text = "";
            txtRoleID.Enabled = false;
            ViewState["iPageIndex"] = (int)ViewState["iPageIndex"] - 1;
            if ((int)ViewState["iPageIndex"] >= 0)
            {
                GetFunctionalRoles((int)ViewState["iPageIndex"]);
            }
            else
            {
                ViewState["iPageIndex"] = 0;
                GetFunctionalRoles((int)ViewState["iPageIndex"]);
            }
        }

        private void btnNextPage_Click(object sender, System.EventArgs e)
        {
            MoveNextDS();
        }
        private void MoveNextDS()
        {
            lblMessages.Text = "";
            if ((bool)ViewState["isTextChange"] == true)
            {
                lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SAVE_PROMPT", utility.GetUserCulture());
                RoleAdminPanel.Visible = true;
                divMain.Visible = false;
                ViewState["NextOperation"] = "MoveNext";
                ViewState["isTextChange"] = false;
            }
            else
            {
                txtRoleID.Enabled = false;
                ViewState["iPageIndex"] = (int)ViewState["iPageIndex"] + 1;
                if ((int)ViewState["iPageIndex"] == (int)ViewState["iTotalRoles"])
                {
                    ViewState["iPageIndex"] = (int)ViewState["iPageIndex"] - 1;
                    GetFunctionalRoles((int)ViewState["iPageIndex"]);
                }
                else if ((int)ViewState["iPageIndex"] < (int)ViewState["iTotalRoles"])
                {
                    GetFunctionalRoles((int)ViewState["iPageIndex"]);
                }
            }
        }

        private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
        {
            MoveFirstDS();
        }
        private void MoveFirstDS()
        {
            lblMessages.Text = "";
            if ((bool)ViewState["isTextChange"] == true)
            {
                lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SAVE_PROMPT", utility.GetUserCulture());
                RoleAdminPanel.Visible = true;
                divMain.Visible = false;
                ViewState["NextOperation"] = "MoveFirst";
                ViewState["isTextChange"] = false;
            }
            else
            {
                ViewState["iPageIndex"] = 0;
                if ((int)ViewState["iTotalRoles"] > 0)
                {
                    GetFunctionalRoles(0);
                }
            }
        }

        private void btnGoToLastPage_Click(object sender, System.EventArgs e)
        {
            MoveLastDS();
        }
        private void MoveLastDS()
        {
            lblMessages.Text = "";
            if ((bool)ViewState["isTextChange"] == true)
            {
                lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SAVE_PROMPT", utility.GetUserCulture());
                RoleAdminPanel.Visible = true;
                divMain.Visible = false;
                ViewState["NextOperation"] = "MoveLast";
                ViewState["isTextChange"] = false;
            }
            else
            {
                txtRoleID.Enabled = false;

                if ((int)ViewState["iTotalRoles"] > 0)
                {
                    ViewState["iPageIndex"] = (int)ViewState["iTotalRoles"] - 1;
                    GetFunctionalRoles((int)ViewState["iTotalRoles"] - 1);
                }
            }
        }

        private void btnSelectAll_Click(object sender, System.EventArgs e)
        {
            lblMessages.Text = "";
            txtRoleID.Enabled = false;
            SelectAllNodes(tiesNode, true);
        }

        private void btnUnSelectAll_Click(object sender, System.EventArgs e)
        {
            lblMessages.Text = "";
            txtRoleID.Enabled = false;
            SelectAllNodes(tiesNode, false);
        }

        private void UpdateSelectedNodeData(String modRights)
        {
            String strIndex = funcTree.SelectedNode.ValuePath;
            String strSelectedNode = funcTree.FindNode(strIndex).Text;
                    String strNode = strSelectedNode.Substring((strSelectedNode.IndexOf(":") + 1));
                    String strNodeData = modRights + ":" + strNode;
                    funcTree.FindNode(strIndex).Value = strNodeData;
        }

        private void funcTree_SelectedNodeChanged(Object sender, EventArgs e)
        {
            String strParentNodeValue = null;
            String strSelectedTreeNode = null;
            String strIndex = funcTree.SelectedNode.ValuePath;

            TreeNodeCollection nodeCollection = funcTree.SelectedNode.ChildNodes;
            int nodeCnt = nodeCollection.Count;
            if (nodeCnt > 0)
            {
                chkGrantsDel.Checked = false;
                chkGrantsIns.Checked = false;
                chkGrantsUpd.Checked = false;
                chkGrantsDel.Enabled = false;
                chkGrantsIns.Enabled = false;
                chkGrantsUpd.Enabled = false;
            }
            else if (nodeCnt == 0)
            {
                chkGrantsDel.Enabled = true;
                chkGrantsIns.Enabled = true;
                chkGrantsUpd.Enabled = true;
                strSelectedTreeNode = funcTree.FindNode(strIndex).Value;
                if (strIndex.Equals(funcTree.Nodes[0].ValuePath))
                {
                    strParentNodeValue = "root";
                }
                else
                {
                    TreeNode parentNode = funcTree.FindNode(strIndex).Parent;
                    strParentNodeValue = parentNode.Value;
                }

                String strModuleRights = strSelectedTreeNode.Substring(0, strSelectedTreeNode.IndexOf(":"));//bind chkbox when execute
                strModuleRights = strModuleRights.Substring(0, (strModuleRights.Length));

                try
                {
                    if ((strModuleRights.IndexOf("I")) >= 0)
                    {
                        chkGrantsIns.Checked = true;
                    }
                    else
                    {
                        chkGrantsIns.Checked = false;
                    }

                }
                catch (Exception indexOutOfBoundExp)
                {
                    chkGrantsIns.Checked = false;
                    String msg = indexOutOfBoundExp.ToString();
                }

                try
                {
                    if ((strModuleRights.IndexOf("U")) >= 0)
                    {
                        chkGrantsUpd.Checked = true;
                    }
                    else
                    {
                        chkGrantsUpd.Checked = false;
                    }

                }
                catch (Exception indexOutOfBoundUExp)
                {
                    chkGrantsUpd.Checked = false;
                    String msg = indexOutOfBoundUExp.ToString();
                }

                try
                {
                    if ((strModuleRights.IndexOf("D")) >= 0)
                    {
                        chkGrantsDel.Checked = true;
                    }
                    else
                    {
                        chkGrantsDel.Checked = false;
                    }

                }
                catch (Exception indexOutOfBoundDExp)
                {
                    chkGrantsDel.Checked = false;
                    String msg = indexOutOfBoundDExp.ToString();
                }


            }
        }
        private void SelectAllNodes(TreeNode rootNode, decimal roleID)
        {
            ViewState["iCreatedRoleID"] = roleID;
            String strIndex = null;
            String treeNode = null;
            TreeNode parentNode = null;
            String strParentNodeValue = null;
            String strParentID = null, strModRight = null;
            String strChildID = null;
            Role role = new Role();
            role.RoleID = Convert.ToDecimal(ViewState["iCreatedRoleID"]);
            ArrayList arryBatchInsertSql = new ArrayList();

            TreeNodeCollection treeNodeCollection = rootNode.ChildNodes;

            foreach (TreeNode node in treeNodeCollection)
            {
                isChecked = node.Checked;
                if (isChecked)
                {
                    strIndex = node.ValuePath;
                    treeNode = funcTree.FindNode(strIndex).Value;
                    parentNode = funcTree.FindNode(strIndex).Parent;
                    strParentNodeValue = parentNode.Value;
                    strChildID = treeNode.Substring((treeNode.IndexOf(":") + 1));
                    strModRight = treeNode.Substring(0, (treeNode.IndexOf(":")));
                    strParentID = strParentNodeValue.Substring((strParentNodeValue.IndexOf(":") + 1));
                    RBACManager.GetModulesRoleInsertQry(appID, enterpriseID, role, strParentID, strChildID, strModRight, ref m_QryList);
                }
                SelectAllNodes(node, Convert.ToDecimal(ViewState["iCreatedRoleID"]));
            }
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            //Role roleds = new Role();
            //ArrayList arrl = RBACManager.GetAllRoles(appID, enterpriseID, roleds);
            //int ifond = 0;
            //foreach (Role sesRole in arrl)
            //{
            //    if (txtRoleName.Text.ToUpper() == sesRole.RoleName)
            //    {
            //        ifond++;
            //    }
            //}
            //if (ifond < 1)
            //{
            //    lblMessages.Text = "";
            //    SaveUpdateRecord();
            //    Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
            //}
            //else
            //{
            //    lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "DUP_KEY_FOUND", utility.GetUserCulture());
            //}


            lblMessages.Text = "";
            SaveUpdateRecord();
            Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
        }

        private void SaveUpdateRecord()
        {
            m_QryList = new ArrayList();
            bool isCheck = IsAnyNodeChecked(tiesNode);
            String strRootNode = null;
            String strMsg = null;
            lblMessages.Text = "";

            if (isCheck)
            {
                if (txtRoleName.Text.Trim().Length <= 0)
                {
                    if (!utility.GetUserCulture().ToUpper().Equals("EN-US"))
                    {
                        strMsg = Utility.GetLanguageText(ResourceType.ScreenLabel, "Role Name is required field", utility.GetUserCulture());
                    }
                    else
                    {
                        strMsg = "Role Name is required field";
                    }
                }
                else
                {
                    decimal iRoleID = 0;
                    String strRoleName = null;
                    String strRoleDesc = null;
                    String strModRights = null;
                    btnQry.Enabled = true;
                    Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                    txtRoleID.Enabled = false;
                    funcTree.Enabled = true;
                    Role role = new Role();
                    if (txtRoleID.Text.Length <= 0)
                    {
                        role.RoleName = txtRoleName.Text.Trim();
                        role.RoleDescription = txtRoleDesc.Text.Trim();
                        strRootNode = tiesNode.Value;
                        strModRights = strRootNode;
                        strRootNode = strRootNode.Substring((strRootNode.IndexOf(":") + 1));
                        strModRights = strModRights.Substring(0, strModRights.IndexOf(":"));

                        try
                        {
                            iRoleID = RBACManager.GetQryAddModuleNRole(appID, enterpriseID, role, "root", strRootNode, strModRights, ref m_QryList);
                            SelectAllNodes(tiesNode, iRoleID);
                            RBACManager.SaveRecordsInBatch(appID, enterpriseID, ref m_QryList);

                        }
                        catch (ApplicationException appException)
                        {
                            String strMessage = appException.Message;
                            if (strMessage.IndexOf("duplicate key") != -1)
                            {
                                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "DUP_KEY_FOUND", utility.GetUserCulture());
                            }
                            else if (strMessage.IndexOf("FK") != -1)
                            {
                                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "FOREIGN_KEY_FOUND", utility.GetUserCulture());
                            }
                            else
                            {
                                lblMessages.Text = strMessage;
                            }

                            return;
                        }

                        if (iRoleID > 0)
                        {
                            txtRoleID.Text = iRoleID.ToString();
                        }
                        strMsg = Utility.GetLanguageText(ResourceType.UserMessage, "INS_SUCCESSFULLY", utility.GetUserCulture());
                    }
                    else if (txtRoleID.Text.Length > 0)
                    {
                        iRoleID = Convert.ToInt32(txtRoleID.Text.Trim());
                        //call the update method for role_master
                        role.RoleID = iRoleID;
                        role.RoleName = txtRoleName.Text.Trim();
                        role.RoleDescription = txtRoleDesc.Text.Trim();
                        strRootNode = tiesNode.Value;
                        strModRights = strRootNode;
                        strRootNode = strRootNode.Substring((strRootNode.IndexOf(":") + 1));
                        strModRights = strModRights.Substring(0, strModRights.IndexOf(":"));
                        try
                        {
                            RBACManager.GetQryModifyModuleNRole(appID, enterpriseID, iRoleID, role, "root", strRootNode, strModRights, ref m_QryList);
                            //						iRoleID = RBACManager.GetQryAddModuleNRole(appID,enterpriseID,role,"root",strRootNode,strModRights,ref m_QryList);
                            SelectAllNodes(tiesNode, iRoleID);
                            RBACManager.SaveRecordsInBatch(appID, enterpriseID, ref m_QryList);

                        }
                        catch (ApplicationException appException)
                        {
                            String strMessage = appException.Message;
                            if (strMessage.IndexOf("duplicate key") != -1)
                            {
                                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "DUP_KEY_FOUND", utility.GetUserCulture());
                            }
                            else if (strMessage.IndexOf("FK") != -1)
                            {
                                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "FOREIGN_KEY_FOUND", utility.GetUserCulture());
                            }
                            else
                            {
                                lblMessages.Text = strMessage;
                            }

                            return;
                        }

                        ArrayList roleArray = (ArrayList)Session["RoleArray"];
                        strRoleName = txtRoleName.Text.Trim();
                        strRoleDesc = txtRoleDesc.Text.Trim();
                        foreach (Role sesRole in roleArray)
                        {
                            if (sesRole.RoleID == iRoleID)
                            {
                                sesRole.RoleDescription = strRoleDesc;
                                sesRole.RoleName = strRoleName;
                                break;
                            }
                        }
                        strMsg = Utility.GetLanguageText(ResourceType.UserMessage, "UPD_SUCCESSFULLY", utility.GetUserCulture());
                    }
                }
                lblMessages.Text = strMsg;
            }
            else
            {
                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_SELECT_MODULE", utility.GetUserCulture());
            }
            ViewState["isTextChange"] = false;
        }
        private void OnCheckedChange()
        {
            bool isUpd = chkGrantsUpd.Checked;
            bool isDel = chkGrantsDel.Checked;
            bool isIns = chkGrantsIns.Checked;

            String strFlag = "";
            if (isUpd)
                strFlag += "U";
            if (isDel)
                strFlag += "D";
            if (isIns)
                strFlag += "I";
            //UpdateSelectedNodeData(strFlag);
        }

        private void chkGrantsIns_CheckedChanged(object sender, System.EventArgs e)
        {
            OnCheckedChange();
            ViewState["isTextChange"] = true;
        }
        private void chkGrantsUpd_CheckedChanged(object sender, System.EventArgs e)
        {
            OnCheckedChange();
            ViewState["isTextChange"] = true;
        }
        private void chkGrantsDel_CheckedChanged(object sender, System.EventArgs e)
        {
            OnCheckedChange();
            ViewState["isTextChange"] = true;
        }

        private void btnQry_Click(object sender, System.EventArgs e)
        {
            Qry_Click();

        }
        private void Qry_Click()
        {
            lblMessages.Text = "";
            ViewState["isQueryMode"] = true;


            if ((bool)ViewState["isDirty"] == false)
            {
                if ((bool)ViewState["isTextChange"] == true)
                {
                    lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SAVE_PROMPT", utility.GetUserCulture());
                    RoleAdminPanel.Visible = true;
                    divMain.Visible = false;
                    ViewState["NextOperation"] = "Query";
                    ViewState["isTextChange"] = false;
                }
                else
                {
                    lblMessages.Text = "";
                    btnExecQry.Enabled = true;
                    Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, false, m_moduleAccessRights);
                    Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete, false, m_moduleAccessRights);
                    Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                    btnGoToFirstPage.Enabled = false;
                    btnGoToLastPage.Enabled = false;
                    btnNextPage.Enabled = false;
                    btnPreviousPage.Enabled = false;

                    txtRoleID.Enabled = true;
                    funcTree.Enabled = false;
                    SelectAllNodes(tiesNode, false);
                    btnSelectAll.Enabled = false;
                    btnUnSelectAll.Enabled = false;
                    txtRoleDesc.Text = "";
                    txtRoleName.Text = "";
                    txtRoleID.Text = "";
                    chkGrantsDel.Checked = false;
                    chkGrantsIns.Checked = false;
                    chkGrantsUpd.Checked = false;
                }
            }
            else
            {
                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "CLK_QUERY_INSERT_PROCEED", utility.GetUserCulture()); ;
                btnQry.Enabled = true;
                ViewState["isDirty"] = false;
            }
        }

        private void btnExecQry_Click(object sender, System.EventArgs e)
        {
            ExecQry_Click();

        }
        private void ExecQry_Click()
        {
            lblMessages.Text = "";
            if (((bool)ViewState["isTextChange"] == true) && ((bool)ViewState["isQueryMode"] == false))
            {
                lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SAVE_PROMPT", utility.GetUserCulture());
                RoleAdminPanel.Visible = true;
                divMain.Visible = false;
                ViewState["NextOperation"] = "ExecuteQuery";
                ViewState["isTextChange"] = false;
            }
            btnExecQry.Enabled = false;
            btnQry.Enabled = true;
            Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, true, m_moduleAccessRights); ;
            Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete, true, m_moduleAccessRights); ;
            Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
            btnGoToFirstPage.Enabled = true;
            btnGoToLastPage.Enabled = true;
            btnNextPage.Enabled = true;
            btnPreviousPage.Enabled = true;
            btnSelectAll.Enabled = false;
            btnUnSelectAll.Enabled = false;
            chkGrantsDel.Enabled = true;
            chkGrantsIns.Enabled = true;
            chkGrantsUpd.Enabled = true;

            txtRoleID.Enabled = false;
            funcTree.Enabled = true;
            Role role = new Role();

            if ((txtRoleID.Text.Trim().Length) > 0)
            {
                role.RoleID = Convert.ToInt32(txtRoleID.Text.Trim());
            }
            else
            {
                role.RoleID = 0;
            }

            role.RoleName = txtRoleName.Text.Trim();
            role.RoleDescription = txtRoleDesc.Text.Trim();
            ArrayList roleList = (ArrayList)RBACManager.GetAllRoles(appID, enterpriseID, role);
            if ((roleList != null) && (roleList.Count <= 0))
            {
                lblMessages.Text = Utility.GetLanguageText(ResourceType.UserMessage, "NO_RECORDS_FOUND", utility.GetUserCulture());
            }
            else if ((roleList != null) && (roleList.Count > 0))
            {
                lblMessages.Text = "";
                ViewState["iTotalRoles"] = roleList.Count;
                Session.Add("RoleArray", roleList);
                GetFunctionalRoles(0);
                ViewState["iPageIndex"] = 0;
            }
            ViewState["isTextChange"] = false;
        }
        private void GetFunctionalRoles(int indexRole)
        {
            if (indexRole == -1)
                return;
            ArrayList roleArray = (ArrayList)Session["RoleArray"];
            if ((roleArray != null) && (indexRole <= roleArray.Count))
            {
                SelectAllNodes(tiesNode, false);
                Role role = (Role)roleArray[indexRole];
                iDisPlayRole = role.RoleID;
                txtRoleDesc.Text = role.RoleDescription;
                txtRoleName.Text = role.RoleName;
                txtRoleID.Text = iDisPlayRole.ToString();
                SelectAllNodes(tiesNode);

            }
        }

        private void SelectAllNodes(TreeNode rootNode)
        {
            String strParentID = null;
            String strChildID = null;

            TreeNodeCollection treeNodeCollection = rootNode.ChildNodes;

            foreach (TreeNode node in treeNodeCollection)
            {
                String nodeData = node.Value;
                strParentID = nodeData.Substring((nodeData.IndexOf(":") + 1));
                TreeNodeCollection childNodes = node.ChildNodes;
                foreach (TreeNode childNode in childNodes)
                {

                    String strChildNode = childNode.Value;
                    strChildID = strChildNode.Substring((strChildNode.IndexOf(":") + 1));
                    ArrayList selRoleList = new ArrayList();

                    selRoleList = RBACManager.GetAllModules(appID, enterpriseID, strParentID, strChildID, iDisPlayRole);
                    if ((selRoleList != null) && (selRoleList.Count > 0))
                    {
                        node.Checked = true;
                        childNode.Checked = true;
                        foreach (Module mod in selRoleList)
                        {
                            String strChild = mod.ModuleId;
                            String strRights = mod.ModuleRights;
                            childNode.Value = strRights + ":" + strChild;
                        }
                    }
                }
                SelectAllNodes(node);
            }
        }
        private bool IsAnyNodeChecked(TreeNode rootNode)
        {
            bool isCheck = false;

            TreeNodeCollection treeNodeCollection = rootNode.ChildNodes;

            foreach (TreeNode node in treeNodeCollection)
            {
                isCheck = node.Checked;
                if (isCheck)
                {
                    break;
                }
                IsAnyNodeChecked(node);
            }

            return isCheck;
        }

        private void txtRoleName_TextChanged(object sender, System.EventArgs e)
        {
            ViewState["isTextChange"] = true;
            if ((btnExecQry.Enabled == false) && (btnQry.Enabled == true) && (btnInsert.Enabled == true) && (btnSave.Enabled == false) && (btnDelete.Enabled == false))
            {

                ViewState["isTextChange"] = false;
                Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, false, m_moduleAccessRights);
                ViewState["isDirty"] = true;
            }
        }

        private void txtRoleID_TextChanged(object sender, System.EventArgs e)
        {
            ViewState["isTextChange"] = true;
        }

        private void btnToSaveChanges_Click(object sender, System.EventArgs e)
        {
            lblMessages.Text = "";

            SaveUpdateRecord();

            divMain.Visible = true;
            RoleAdminPanel.Visible = false;
        }

        private void btnNotToSave_Click(object sender, System.EventArgs e)
        {
            lblMessages.Text = "";
            divMain.Visible = true;
            RoleAdminPanel.Visible = false;
            ViewState["isTextChange"] = false;
            if ((String)ViewState["NextOperation"] == "Insert")
            {
                Insert_Click();
            }
            else if ((String)ViewState["NextOperation"] == "ExecuteQuery")
            {
                ExecQry_Click();
            }
            else if ((String)ViewState["NextOperation"] == "Query")
            {
                Qry_Click();
            }
            else if ((String)ViewState["NextOperation"] == "MoveFirst")
            {
                MoveFirstDS();
            }
            else if ((String)ViewState["NextOperation"] == "MoveNext")
            {
                MoveNextDS();
            }
            else if ((String)ViewState["NextOperation"] == "MoveLast")
            {
                MoveLastDS();
            }
            else if ((String)ViewState["NextOperation"] == "MovePrevious")
            {
                MovePreviousDS();
            }
        }

        private void btnToCancel_Click(object sender, System.EventArgs e)
        {
            lblMessages.Text = "";
            divMain.Visible = true;
            RoleAdminPanel.Visible = false;

        }

        private void txtRoleDesc_TextChanged(object sender, System.EventArgs e)
        {
            ViewState["isTextChange"] = true;
            if ((btnExecQry.Enabled == false) && (btnQry.Enabled == true) && (btnInsert.Enabled == true) && (btnSave.Enabled == false) && (btnDelete.Enabled == false))
            {

                ViewState["isTextChange"] = false;
                Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, false, m_moduleAccessRights);
                ViewState["isDirty"] = true;
            }
        }
    }
}
