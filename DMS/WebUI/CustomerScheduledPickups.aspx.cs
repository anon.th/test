using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.ties.BAL;
using com.common.applicationpages;
using System.Text;

namespace com.ties
{
	/// <summary>
	/// Summary description for CustomerScheduledPickups.
	/// </summary>
	public class CustomerScheduledPickups : BasePage
	{
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.txtCustomerCode.TextChanged += new System.EventHandler(this.txtCustomerCode_TextChanged);
			this.btnDisplayCustDtls.Click += new System.EventHandler(this.btnDisplayCustDtls_Click);
			this.txtCustomerName.TextChanged += new System.EventHandler(this.txtCustomerName_TextChanged);
			this.txtSendName.TextChanged += new System.EventHandler(this.txtSendName_TextChanged);
			this.btnReference.Click += new System.EventHandler(this.btnReference_Click);
			this.chkPickHolidays.CheckedChanged += new System.EventHandler(this.chkPickHolidays_CheckedChanged);
			this.txtMon1.TextChanged += new System.EventHandler(this.txtMon1_TextChanged);
			this.txtMon2.TextChanged += new System.EventHandler(this.txtMon2_TextChanged);
			this.txtMon3.TextChanged += new System.EventHandler(this.txtMon3_TextChanged);
			this.chkMon.CheckedChanged += new System.EventHandler(this.chkMon_CheckedChanged);
			this.txtTue1.TextChanged += new System.EventHandler(this.txtTue1_TextChanged);
			this.txtTue2.TextChanged += new System.EventHandler(this.txtTue2_TextChanged);
			this.txtTue3.TextChanged += new System.EventHandler(this.txtTue3_TextChanged);
			this.chkTue.CheckedChanged += new System.EventHandler(this.chkTue_CheckedChanged);
			this.txtWed1.TextChanged += new System.EventHandler(this.txtWed1_TextChanged);
			this.txtWed2.TextChanged += new System.EventHandler(this.txtWed2_TextChanged);
			this.txtWed3.TextChanged += new System.EventHandler(this.txtWed3_TextChanged);
			this.chkWed.CheckedChanged += new System.EventHandler(this.chkWed_CheckedChanged);
			this.txtThu1.TextChanged += new System.EventHandler(this.txtThu1_TextChanged);
			this.txtThu2.TextChanged += new System.EventHandler(this.txtThu2_TextChanged);
			this.txtThu3.TextChanged += new System.EventHandler(this.txtThu3_TextChanged);
			this.chkThu.CheckedChanged += new System.EventHandler(this.chkThu_CheckedChanged);
			this.txtFri1.TextChanged += new System.EventHandler(this.txtFri1_TextChanged);
			this.txtFri2.TextChanged += new System.EventHandler(this.txtFri2_TextChanged);
			this.txtFri3.TextChanged += new System.EventHandler(this.txtFri3_TextChanged);
			this.chkFri.CheckedChanged += new System.EventHandler(this.chkFri_CheckedChanged);
			this.txtSat1.TextChanged += new System.EventHandler(this.txtSat1_TextChanged);
			this.txtSat2.TextChanged += new System.EventHandler(this.txtSat2_TextChanged);
			this.txtSat3.TextChanged += new System.EventHandler(this.txtSat3_TextChanged);
			this.chkSat.CheckedChanged += new System.EventHandler(this.chkSat_CheckedChanged);
			this.txtSun1.TextChanged += new System.EventHandler(this.txtSun1_TextChanged);
			this.txtSun2.TextChanged += new System.EventHandler(this.txtSun2_TextChanged);
			this.txtSun3.TextChanged += new System.EventHandler(this.txtSun3_TextChanged);
			this.chkSun.CheckedChanged += new System.EventHandler(this.chkSun_CheckedChanged);
			this.btnToSaveChanges.Click += new System.EventHandler(this.btnToSaveChanges_Click);
			this.btnNotToSave.Click += new System.EventHandler(this.btnNotToSave_Click);
			this.btnToCancel.Click += new System.EventHandler(this.btnToCancel_Click);
			this.btnComfirmDel.Click += new System.EventHandler(this.btnComfirmDel_Click);
			this.btnNoConfirmDel.Click += new System.EventHandler(this.btnNoConfirmDel_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}

		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected System.Web.UI.WebControls.TextBox txtGoToRec;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Button btnDisplayCustDtls;
		protected System.Web.UI.WebControls.Label lblReference;
		protected System.Web.UI.WebControls.Label lblCustomerAccount;
		protected System.Web.UI.WebControls.TextBox txtCustomerName;
		protected System.Web.UI.WebControls.CheckBox chkPickHolidays;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.Label lblSendInfo;
		protected System.Web.UI.WebControls.CheckBox chkMon;
		protected System.Web.UI.WebControls.CheckBox chkWed;
		protected System.Web.UI.WebControls.CheckBox chkTue;
		protected System.Web.UI.WebControls.CheckBox chkThu;
		protected System.Web.UI.WebControls.CheckBox chkFri;
		protected System.Web.UI.WebControls.CheckBox chkSat;
		protected System.Web.UI.WebControls.CheckBox chkSun;
		protected com.common.util.msTextBox txtMon1;
		protected com.common.util.msTextBox txtMon2;
		protected com.common.util.msTextBox txtMon3;
		protected com.common.util.msTextBox txtTue1;
		protected com.common.util.msTextBox txtTue2;
		protected com.common.util.msTextBox txtTue3;
		protected com.common.util.msTextBox txtWed1;
		protected com.common.util.msTextBox txtWed2;
		protected com.common.util.msTextBox txtWed3;
		protected com.common.util.msTextBox txtThu1;
		protected com.common.util.msTextBox txtThu2;
		protected com.common.util.msTextBox txtThu3;
		protected com.common.util.msTextBox txtFri1;
		protected com.common.util.msTextBox txtFri2;
		protected com.common.util.msTextBox txtFri3;
		protected com.common.util.msTextBox txtSat1;
		protected com.common.util.msTextBox txtSat2;
		protected com.common.util.msTextBox txtSat3;
		protected com.common.util.msTextBox txtSun1;
		protected com.common.util.msTextBox txtSun2;
		protected com.common.util.msTextBox txtSun3;
		protected System.Web.UI.WebControls.TextBox txtCustomerCode;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Button btnReference;
		protected System.Web.UI.WebControls.TextBox txtSendName;
		protected System.Web.UI.WebControls.TextBox txtSendAddr1;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected com.common.util.msTextBox txtBookingNo;
		protected System.Web.UI.HtmlControls.HtmlTableRow trMain;
		protected System.Web.UI.HtmlControls.HtmlTableRow trConfirm;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.WebControls.RequiredFieldValidator validCustID;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.Button btnToSaveChanges;
		protected System.Web.UI.WebControls.Button btnNotToSave;
		protected System.Web.UI.WebControls.Button btnToCancel;
		protected System.Web.UI.WebControls.Label lblConfirmation;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.HtmlControls.HtmlTableRow trDelete;
		protected System.Web.UI.WebControls.Button btnComfirmDel;
		protected System.Web.UI.WebControls.Button btnNoConfirmDel;

		#endregion
	
		String appID = null;
		String enterpriseID = null;
		String userID = null;
		String isEnabledHolidaysPickup = null;
		String isEnabledSatPickup = null;
		String isEnabledSunPickup = null;
		static int m_iSetSize = 10;
		protected System.Web.UI.WebControls.Label Label24;
		protected System.Web.UI.WebControls.Label Label25;
		protected System.Web.UI.WebControls.Label Label26;
		protected System.Web.UI.WebControls.Label Label27;
		protected System.Web.UI.WebControls.Label Label28;
		protected System.Web.UI.WebControls.Label Label29;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeMon1;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeTue1;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeWed1;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeThu1;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeFri1;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeSat1;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeSun1;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeMon2;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeTue2;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeWed2;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeThu2;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeFri2;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeSat2;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeSun2;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeMon3;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeTue3;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeWed3;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeThu3;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeFri3;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeSat3;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCodeSun3;
		protected System.Web.UI.WebControls.Label Label10;
		SessionDS m_sdsCustomerSchedPickups = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();
			setDBComboRegistrationKey();
			if (!IsPostBack)
			{
				PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
				validCustID.ErrorMessage = Utility.GetLanguageText(ResourceType.ScreenLabel, "CUSTOMER ACCOUNT", utility.GetUserCulture());
				setHolidaysWeekendPickupStatus();
				trConfirm.Visible = false;
				trDelete.Visible = false;
				ViewState["isTextChanged"] = false;
				ViewState["CSPMode"] = ScreenMode.None;
				ViewState["CSPOperation"] = Operation.None;

				btnGoToFirstPage.Enabled = false;
				btnGoToLastPage.Enabled = false;
				btnNextPage.Enabled = false;
				btnPreviousPage.Enabled = false;
                chkPickHolidays.Enabled = false;



                btnQry.Enabled = true;
				btnExecQry.Enabled = true;
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete, false, m_moduleAccessRights);
				Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, false, m_moduleAccessRights);
				Session["CSP_SESSION"] = m_sdsCustomerSchedPickups;
				ResetForQuery();
				
				//SetInitialFocus();

				Utility ut = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				ut.SetInitialFocus(txtCustomerCode);


			}
			else
			{
				m_sdsCustomerSchedPickups	= (SessionDS)Session["CSP_SESSION"];

				if((isEnabledHolidaysPickup == null) || (isEnabledHolidaysPickup == ""))
				{
					setHolidaysWeekendPickupStatus();
				}
			}

            //if (isEnabledHolidaysPickup == "Y")
            //    chkPickHolidays.Enabled = true;
            //else
            //    chkPickHolidays.Enabled = false;

            if (isEnabledSatPickup == "Y") 
			{
				chkSat.Enabled = true;
				txtSat1.Enabled = true;
				txtSat2.Enabled = true;
				txtSat3.Enabled = true;
				DbComboPathCodeSat1.Enabled=true;
				DbComboPathCodeSat2.Enabled=true;	
				DbComboPathCodeSat3.Enabled=true;
			}
			else 
			{
				chkSat.Enabled = false;
				txtSat1.Enabled = false;
				txtSat2.Enabled = false;
				txtSat3.Enabled = false;
				txtSat1.Text = "";
				txtSat2.Text = "";
				txtSat3.Text = "";
				DbComboPathCodeSat1.Enabled=false;
				DbComboPathCodeSat2.Enabled=false;	
				DbComboPathCodeSat3.Enabled=false;
			}

			if (isEnabledSunPickup == "Y") 
			{
				chkSun.Enabled = true;
				txtSun1.Enabled = true;
				txtSun2.Enabled = true;
				txtSun3.Enabled = true;
				DbComboPathCodeSun1.Enabled=true;
				DbComboPathCodeSun2.Enabled=true;	
				DbComboPathCodeSun3.Enabled=true;
			}
			else 
			{
				chkSun.Enabled = false;
				txtSun1.Enabled = false;
				txtSun2.Enabled = false;
				txtSun3.Enabled = false;
				txtSun1.Text = "";
				txtSun2.Text = "";
				txtSun3.Text = "";
				DbComboPathCodeSun1.Enabled=false;
				DbComboPathCodeSun2.Enabled=false;	
				DbComboPathCodeSun3.Enabled=false;
			}

			setPickupInput();
			SetDbComboServerStates();
			//SetExceptionCodeServerStates();
		}

		
		#region DbComboSetting
		private void SetDbComboServerStates()
		{
			String strDeliveryType="S";
			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
			setDBComboServerState(hash);
			setDBComboServerStateSecretString("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4");
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"' ";
					if(strDelType == "S") 
					{
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct pickup_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
				}				
			}
			
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}

		private void clearDBCombo(Cambro.Web.DbCombo.DbCombo DBComboObj)
		{
			DBComboObj.Text="";
			DBComboObj.Value="";
		}
		private void setDBComboRegistrationKey()
		{

		 DbComboPathCodeMon1.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeTue1.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeWed1.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeThu1.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeFri1.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeSat1.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeSun1.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeMon2.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeTue2.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeWed2.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeThu2.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeFri2.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeSat2.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeSun2.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeMon3.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeTue3.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeWed3.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeThu3.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeFri3.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeSat3.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		 DbComboPathCodeSun3.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
		}
		private void setDBComboServerState(Hashtable hash)
		{
			DbComboPathCodeMon1.ServerState= hash;
			DbComboPathCodeTue1.ServerState= hash;
			DbComboPathCodeWed1.ServerState= hash;
			DbComboPathCodeThu1.ServerState= hash;
			DbComboPathCodeFri1.ServerState= hash;
			DbComboPathCodeSat1.ServerState= hash;
			DbComboPathCodeSun1.ServerState= hash;
			DbComboPathCodeMon2.ServerState= hash;
			DbComboPathCodeTue2.ServerState= hash;
			DbComboPathCodeWed2.ServerState= hash;
			DbComboPathCodeThu2.ServerState= hash;
			DbComboPathCodeFri2.ServerState= hash;
			DbComboPathCodeSat2.ServerState= hash;
			DbComboPathCodeSun2.ServerState= hash;
			DbComboPathCodeMon3.ServerState= hash;
			DbComboPathCodeTue3.ServerState= hash;
			DbComboPathCodeWed3.ServerState= hash;
			DbComboPathCodeThu3.ServerState= hash;
			DbComboPathCodeFri3.ServerState= hash;
			DbComboPathCodeSat3.ServerState= hash;
			DbComboPathCodeSun3.ServerState= hash;
		}
		private void setDBComboServerStateSecretString(string SecretString)
		{

			DbComboPathCodeMon1.ServerStateSecretString = SecretString;
			DbComboPathCodeTue1.ServerStateSecretString = SecretString;
			DbComboPathCodeWed1.ServerStateSecretString = SecretString;
			DbComboPathCodeThu1.ServerStateSecretString = SecretString;
			DbComboPathCodeFri1.ServerStateSecretString = SecretString;
			DbComboPathCodeSat1.ServerStateSecretString = SecretString;
			DbComboPathCodeSun1.ServerStateSecretString = SecretString;
			DbComboPathCodeMon2.ServerStateSecretString = SecretString;
			DbComboPathCodeTue2.ServerStateSecretString = SecretString;
			DbComboPathCodeWed2.ServerStateSecretString = SecretString;
			DbComboPathCodeThu2.ServerStateSecretString = SecretString;
			DbComboPathCodeFri2.ServerStateSecretString = SecretString;
			DbComboPathCodeSat2.ServerStateSecretString = SecretString;
			DbComboPathCodeSun2.ServerStateSecretString = SecretString;
			DbComboPathCodeMon3.ServerStateSecretString = SecretString;
			DbComboPathCodeTue3.ServerStateSecretString = SecretString;
			DbComboPathCodeWed3.ServerStateSecretString = SecretString;
			DbComboPathCodeThu3.ServerStateSecretString = SecretString;
			DbComboPathCodeFri3.ServerStateSecretString = SecretString;
			DbComboPathCodeSat3.ServerStateSecretString = SecretString;
			DbComboPathCodeSun3.ServerStateSecretString = SecretString;
		}

	#endregion

	#region "Private Method"

	private void setHolidaysWeekendPickupStatus()
		{
			DataSet Enterprise = CustomerScheduledPickupsDAL.GetEnterpriseProfile(appID, enterpriseID,0,0).ds;
			
			isEnabledSatPickup = (String)Enterprise.Tables[0].Rows[0]["sat_delivery_avail"];
			isEnabledSunPickup = (String)Enterprise.Tables[0].Rows[0]["sun_delivery_avail"];
			isEnabledHolidaysPickup = (String)Enterprise.Tables[0].Rows[0]["pub_delivery_avail"];
		}


		private void ChangeDSState()
		{
			if(((int)ViewState["CSPMode"] == (int)ScreenMode.Insert) && ((int)ViewState["CSPOperation"] == (int)Operation.None))
			{
				ViewState["CSPOperation"] = Operation.Insert;
			}
			else if(((int)ViewState["CSPMode"] == (int)ScreenMode.Insert) && ((int)ViewState["CSPOperation"] == (int)Operation.Insert))
			{
				ViewState["CSPOperation"] = Operation.Saved;
			}
			else if(((int)ViewState["CSPMode"] == (int)ScreenMode.Insert) && ((int)ViewState["CSPOperation"] == (int)Operation.Saved))
			{
				ViewState["CSPOperation"] = Operation.Update;
			}
			else if(((int)ViewState["CSPMode"] == (int)ScreenMode.Insert) && ((int)ViewState["CSPOperation"] == (int)Operation.Update))
			{
				ViewState["CSPOperation"] = Operation.Saved;
			}
			else if(((int)ViewState["CSPMode"] == (int)ScreenMode.ExecuteQuery) && ((int)ViewState["CSPOperation"] == (int)Operation.None))
			{
				ViewState["CSPOperation"] = Operation.Update;
			}
			else if(((int)ViewState["CSPMode"] == (int)ScreenMode.ExecuteQuery) && ((int)ViewState["CSPOperation"] == (int)Operation.Update))
			{
				ViewState["CSPOperation"] = Operation.None;
			}
		}


		private void GetValuesIntoDS(int index, string getState)
		{
			lblErrorMsg.Text = "";

			if(m_sdsCustomerSchedPickups == null || m_sdsCustomerSchedPickups.ds.Tables[0].Rows.Count == 0)
				ResetForQuery();

			DataRow drEach = m_sdsCustomerSchedPickups.ds.Tables[0].Rows[index];

			if (txtCustomerCode.Text.Trim() != "")
			{
				drEach["custid"] = txtCustomerCode.Text.Trim();
			}

			if(getState == "Query")
			{
				if (txtSendName.Text.Trim() != "")
				{
					drEach["cust_ref_name"] = txtSendName.Text.Trim();
				}
			}
			else
			{
				if (txtSendName.Text.Trim() != "")
				{
					drEach["cust_ref_name"] = txtSendName.Text.Trim();
					drEach["reference"] = "Y";
				}
				else
				{
					drEach["cust_ref_name"] = txtCustomerName.Text.Trim();
					drEach["reference"] = "N";
				}
			}
			
			//isPickups on holidays
			if(chkPickHolidays.Enabled)
			{
				if(chkPickHolidays.Checked)
					drEach["pubholiday"] = "Y";
				else
					drEach["pubholiday"] = "N";
			}
			else
			{
				drEach["pubholiday"] = "N";
			}

			ArrayList al = new ArrayList();
			ArrayList alRoute = new ArrayList();	
			//Monday Group
			al = getRearranged(txtMon1.Text.Trim(), txtMon2.Text.Trim(), txtMon3.Text.Trim());
			txtMon1.Text = "";
			txtMon2.Text = "";
			txtMon3.Text = "";
			alRoute= getRearrangedRouteCode(DbComboPathCodeMon1.Value.Trim(),DbComboPathCodeMon2.Value.Trim(),DbComboPathCodeMon3.Value.Trim());
			clearDBCombo(DbComboPathCodeMon1);
			clearDBCombo(DbComboPathCodeMon2);
			clearDBCombo(DbComboPathCodeMon3);
			for(int i = 0; i <= al.Count - 1; i++)
			{
				if(i == 0) 
				{
					drEach["mon_putime1"] = al[0];
					txtMon1.Text = ((DateTime)al[0]).ToString("HH:mm");
					drEach["mon_routecode1"] =alRoute[0];
					DbComboPathCodeMon1.Text=alRoute[0].ToString();
					DbComboPathCodeMon1.Value=alRoute[0].ToString();
				}
				if(i == 1) 
				{
					drEach["mon_putime2"] = al[1];
					txtMon2.Text = ((DateTime)al[1]).ToString("HH:mm");
					drEach["mon_routecode2"] =alRoute[1];
					DbComboPathCodeMon2.Text=alRoute[1].ToString();
					DbComboPathCodeMon2.Value=alRoute[1].ToString();
				}
				if(i == 2) 
				{
					drEach["mon_putime3"] = al[2];
					txtMon3.Text = ((DateTime)al[2]).ToString("HH:mm");
					drEach["mon_routecode3"] =alRoute[2];
					DbComboPathCodeMon3.Text=alRoute[2].ToString();
					DbComboPathCodeMon3.Value=alRoute[2].ToString();
				}
			}
		
			
			//Tuesday Group
			al = getRearranged(txtTue1.Text.Trim(), txtTue2.Text.Trim(), txtTue3.Text.Trim());
			txtTue1.Text = "";
			txtTue2.Text = "";
			txtTue3.Text = "";
			alRoute= getRearrangedRouteCode(DbComboPathCodeTue1.Value.Trim(),DbComboPathCodeTue2.Value.Trim(),DbComboPathCodeTue3.Value.Trim());
			clearDBCombo(DbComboPathCodeTue1);
			clearDBCombo(DbComboPathCodeTue2);
			clearDBCombo(DbComboPathCodeTue3);
			for(int i = 0; i <= al.Count - 1; i++)
			{
				if(i == 0) 
				{
					drEach["tue_putime1"] = al[0];
					txtTue1.Text = ((DateTime)al[0]).ToString("HH:mm");
					drEach["tue_routecode1"] =alRoute[0];
					DbComboPathCodeTue1.Text=alRoute[0].ToString();
					DbComboPathCodeTue1.Value=alRoute[0].ToString();
				}
				if(i == 1) 
				{
					drEach["tue_putime2"] = al[1];
					txtTue2.Text = ((DateTime)al[1]).ToString("HH:mm");
					drEach["tue_routecode2"] =alRoute[1];
					DbComboPathCodeTue2.Text=alRoute[1].ToString();
					DbComboPathCodeTue2.Value=alRoute[1].ToString();
				}
				if(i == 2) 
				{
					drEach["tue_putime3"] = al[2];
					txtTue3.Text = ((DateTime)al[2]).ToString("HH:mm");
					drEach["tue_routecode3"] =alRoute[2];
					DbComboPathCodeTue3.Text=alRoute[2].ToString();
					DbComboPathCodeTue3.Value=alRoute[2].ToString();
				}
			}

			//Wednesday Group
			al = getRearranged(txtWed1.Text.Trim(), txtWed2.Text.Trim(), txtWed3.Text.Trim());
			txtWed1.Text = "";
			txtWed2.Text = "";
			txtWed3.Text = "";
			alRoute= getRearrangedRouteCode(DbComboPathCodeWed1.Value.Trim(),DbComboPathCodeWed2.Value.Trim(),DbComboPathCodeWed3.Value.Trim());
			clearDBCombo(DbComboPathCodeWed1);
			clearDBCombo(DbComboPathCodeWed2);
			clearDBCombo(DbComboPathCodeWed3);
			for(int i = 0; i <= al.Count - 1; i++)
			{
				if(i == 0) 
				{
					drEach["wed_putime1"] = al[0];
					txtWed1.Text = ((DateTime)al[0]).ToString("HH:mm");
					drEach["wed_routecode1"] =alRoute[0];
					DbComboPathCodeWed1.Text=alRoute[0].ToString();
					DbComboPathCodeWed1.Value=alRoute[0].ToString();
				}
				if(i == 1) 
				{
					drEach["wed_putime2"] = al[1];
					txtWed2.Text = ((DateTime)al[1]).ToString("HH:mm");
					drEach["wed_routecode2"] =alRoute[1];
					DbComboPathCodeWed2.Text=alRoute[1].ToString();
					DbComboPathCodeWed2.Value=alRoute[1].ToString();
				}
				if(i == 2) 
				{
					drEach["wed_putime3"] = al[2];
					txtWed3.Text = ((DateTime)al[2]).ToString("HH:mm");
					drEach["wed_routecode3"] =alRoute[2];
					DbComboPathCodeWed3.Text=alRoute[2].ToString();
					DbComboPathCodeWed3.Value=alRoute[2].ToString();
				}
			}

			//Thuresday Group
			al = getRearranged(txtThu1.Text.Trim(), txtThu2.Text.Trim(), txtThu3.Text.Trim());
			txtThu1.Text = "";
			txtThu2.Text = "";
			txtThu3.Text = "";
			alRoute= getRearrangedRouteCode(DbComboPathCodeThu1.Value.Trim(),DbComboPathCodeThu2.Value.Trim(),DbComboPathCodeThu3.Value.Trim());
			clearDBCombo(DbComboPathCodeThu1);
			clearDBCombo(DbComboPathCodeThu2);
			clearDBCombo(DbComboPathCodeThu3);
			for(int i = 0; i <= al.Count - 1; i++)
			{
				if(i == 0) 
				{
					drEach["thu_putime1"] = al[0];
					txtThu1.Text = ((DateTime)al[0]).ToString("HH:mm");
					drEach["thu_routecode1"] =alRoute[0];
					DbComboPathCodeThu1.Text=alRoute[0].ToString();
					DbComboPathCodeThu1.Value=alRoute[0].ToString();
				}
				if(i == 1) 
				{
					drEach["thu_putime2"] = al[1];
					txtThu2.Text = ((DateTime)al[1]).ToString("HH:mm");
					drEach["thu_routecode2"] =alRoute[1];
					DbComboPathCodeThu2.Text=alRoute[1].ToString();
					DbComboPathCodeThu2.Value=alRoute[1].ToString();
				}
				if(i == 2) 
				{
					drEach["thu_putime3"] = al[2];
					txtThu3.Text = ((DateTime)al[2]).ToString("HH:mm");
					drEach["thu_routecode3"] =alRoute[2];
					DbComboPathCodeThu3.Text=alRoute[2].ToString();
					DbComboPathCodeThu3.Value=alRoute[2].ToString();
				}
			}
			
			//Friday Group
			al = getRearranged(txtFri1.Text.Trim(), txtFri2.Text.Trim(), txtFri3.Text.Trim());
			txtFri1.Text = "";
			txtFri2.Text = "";
			txtFri3.Text = "";
			alRoute= getRearrangedRouteCode(DbComboPathCodeFri1.Value.Trim(),DbComboPathCodeFri2.Value.Trim(),DbComboPathCodeFri3.Value.Trim());
			clearDBCombo(DbComboPathCodeFri1);
			clearDBCombo(DbComboPathCodeFri2);
			clearDBCombo(DbComboPathCodeFri3);
			for(int i = 0; i <= al.Count - 1; i++)
			{
				if(i == 0) 
				{
					drEach["fri_putime1"] = al[0];
					txtFri1.Text = ((DateTime)al[0]).ToString("HH:mm");
					drEach["fri_routecode1"] =alRoute[0];
					DbComboPathCodeFri1.Text=alRoute[0].ToString();
					DbComboPathCodeFri1.Value=alRoute[0].ToString();
				}
				if(i == 1) 
				{
					drEach["fri_putime2"] = al[1];
					txtFri2.Text = ((DateTime)al[1]).ToString("HH:mm");
					drEach["fri_routecode2"] =alRoute[1];
					DbComboPathCodeFri2.Text=alRoute[1].ToString();
					DbComboPathCodeFri2.Value=alRoute[1].ToString();
				}
				if(i == 2) 
				{
					drEach["fri_putime3"] = al[2];
					txtFri3.Text = ((DateTime)al[2]).ToString("HH:mm");
					drEach["fri_routecode3"] =alRoute[2];
					DbComboPathCodeFri3.Text=alRoute[2].ToString();
					DbComboPathCodeFri3.Value=alRoute[2].ToString();
				}
			}
		
			//Saturday Group
			if(chkSat.Enabled)
			{
				al = getRearranged(txtSat1.Text.Trim(), txtSat2.Text.Trim(), txtSat3.Text.Trim());
				txtSat1.Text = "";
				txtSat2.Text = "";
				txtSat3.Text = "";
				alRoute= getRearrangedRouteCode(DbComboPathCodeSat1.Value.Trim(),DbComboPathCodeSat2.Value.Trim(),DbComboPathCodeSat3.Value.Trim());
				clearDBCombo(DbComboPathCodeSat1);
				clearDBCombo(DbComboPathCodeSat2);
				clearDBCombo(DbComboPathCodeSat3);
				for(int i = 0; i <= al.Count - 1; i++)
				{
					if(i == 0) 
					{
						drEach["sat_putime1"] = al[0];
						txtSat1.Text = ((DateTime)al[0]).ToString("HH:mm");
						drEach["sat_routecode1"] =alRoute[0];
						DbComboPathCodeSat1.Text=alRoute[0].ToString();
						DbComboPathCodeSat1.Value=alRoute[0].ToString();
					}
					if(i == 1) 
					{
						drEach["sat_putime2"] = al[1];
						txtSat2.Text = ((DateTime)al[1]).ToString("HH:mm");
						drEach["sat_routecode2"] =alRoute[1];
						DbComboPathCodeSat2.Text=alRoute[1].ToString();
						DbComboPathCodeSat2.Value=alRoute[1].ToString();
					}
					if(i == 2) 
					{
						drEach["sat_putime3"] = al[2];
						txtSat3.Text = ((DateTime)al[2]).ToString("HH:mm");
						drEach["sat_routecode3"] =alRoute[2];
						DbComboPathCodeSat3.Text=alRoute[2].ToString();
						DbComboPathCodeSat3.Value=alRoute[2].ToString();
					}
				}
			}
				
			//Sunday Group
			if(chkSun.Enabled)
			{
				al = getRearranged(txtSun1.Text.Trim(), txtSun2.Text.Trim(), txtSun3.Text.Trim());
				txtSun1.Text = "";
				txtSun2.Text = "";
				txtSun3.Text = "";
				alRoute= getRearrangedRouteCode(DbComboPathCodeSun1.Value.Trim(),DbComboPathCodeSun2.Value.Trim(),DbComboPathCodeSun3.Value.Trim());
				clearDBCombo(DbComboPathCodeSun1);
				clearDBCombo(DbComboPathCodeSun2);
				clearDBCombo(DbComboPathCodeSun3);
				for(int i = 0; i <= al.Count - 1; i++)
				{
					if(i == 0) 
					{
						drEach["sun_putime1"] = al[0];
						txtSun1.Text = ((DateTime)al[0]).ToString("HH:mm");
						drEach["sun_routecode1"] =alRoute[0];
						DbComboPathCodeSun1.Text=alRoute[0].ToString();
						DbComboPathCodeSun1.Value=alRoute[0].ToString();
					}
					if(i == 1) 
					{
						drEach["sun_putime2"] = al[1];
						txtSun2.Text = ((DateTime)al[1]).ToString("HH:mm");
						drEach["sun_routecode2"] =alRoute[1];
						DbComboPathCodeSun2.Text=alRoute[1].ToString();
						DbComboPathCodeSun2.Value=alRoute[1].ToString();
					}
					if(i == 2) 
					{
						drEach["sun_putime3"] = al[2];
						txtSun3.Text = ((DateTime)al[2]).ToString("HH:mm");
						drEach["sun_routecode3"] =alRoute[2];
						DbComboPathCodeSun3.Text=alRoute[2].ToString();
						DbComboPathCodeSun3.Value=alRoute[2].ToString();
					}
				}
			}
			
			//is activeOnMon
			if(chkMon.Enabled)
			{
				if(chkMon.Checked)
					drEach["mon_active"] = "Y";
				else
					drEach["mon_active"] = "N";
			}

			//is activeOnTue
			if(chkTue.Checked)
				drEach["tue_active"] = "Y";
			else
				drEach["tue_active"] = "N";
			
			//is activeOnWed
			if(chkWed.Checked)
				drEach["wed_active"] = "Y";
			else
				drEach["wed_active"] = "N";
			
			//is activeOnThu
			if(chkThu.Checked)
				drEach["thu_active"] = "Y";
			else
				drEach["thu_active"] = "N";
			
			//is activeOnFri
			if(chkFri.Checked)
				drEach["fri_active"] = "Y";
			else
				drEach["fri_active"] = "N";

			//is activeOnSat
			if(chkSat.Enabled)
			{
				if(chkSat.Checked)
					drEach["sat_active"] = "Y";
				else
					drEach["sat_active"] = "N";
			}
			else
			{
				drEach["sat_active"] = "N";
			}
			
		//is activeOnSun
			if(chkSun.Enabled)
			{
				if(chkSun.Checked)
					drEach["sun_active"] = "Y";
				else
					drEach["sun_active"] = "N";
			}
			else
			{
				drEach["sun_active"] = "N";
			}
		}


		private bool SaveUpdateRecord()
		{
			bool isError = false;
			lblErrorMsg.Text = "";

			if(checkValidInput() == false)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_HAS_DAY_IN_CHECKED",utility.GetUserCulture());
				isError = true;
				return isError;
			}


			String CustID = "";
			String CustRef = "";
			if(m_sdsCustomerSchedPickups.ds.Tables[0].Rows.Count > 0) 
			{
				CustID = m_sdsCustomerSchedPickups.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])]["custid"].ToString();
				CustRef = m_sdsCustomerSchedPickups.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])]["cust_ref_name"].ToString();
			}
				
			GetValuesIntoDS(Convert.ToInt32((ViewState["currentPage"])), "Save");
			DataSet dsCustomerSchedPickups = m_sdsCustomerSchedPickups.ds.GetChanges();

			if((int)ViewState["CSPMode"] == (int)ScreenMode.Insert && 
				((int)ViewState["CSPOperation"] == (int)Operation.Insert
				|| (int)ViewState["CSPOperation"] == (int)Operation.Saved))
			{
				try
				{
					CustomerScheduledPickupsDAL.AddCustomerSchedPickups(appID,enterpriseID,
						dsCustomerSchedPickups, utility.GetUserCulture());
					ViewState["isTextChanged"] = false;
					ChangeDSState();
					m_sdsCustomerSchedPickups.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])].AcceptChanges();
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					if(strMsg.IndexOf("duplicate key") != -1 )
					{
						lblErrorMsg.Text = "Duplicate Key cannot save the record";
					}
					else if(strMsg.IndexOf("FK") != -1 )
					{
						lblErrorMsg.Text = "Foreign Key Error cannot save the record";
					}
					else if(strMsg.IndexOf("Duplicate Consignment") != -1 )
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CS_PICK",utility.GetUserCulture());
					}
					else
					{
						lblErrorMsg.Text = strMsg;
					}
					isError = false;
					m_sdsCustomerSchedPickups.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])].RejectChanges();
					return isError;
				}
					
				lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
			}
			else if((int)ViewState["CSPOperation"] == (int)Operation.Update|| (int)ViewState["CSPMode"] == (int)ScreenMode.ExecuteQuery && (int)ViewState["CSPOperation"] == (int)Operation.None)
			{
				try
				{
					CustomerScheduledPickupsDAL.UpdateCustomerSchedPickups(appID,enterpriseID,
						dsCustomerSchedPickups, CustID, CustRef);
					ViewState["isTextChanged"] = false;
					ChangeDSState();
					m_sdsCustomerSchedPickups.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])].AcceptChanges();
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					if(strMsg.IndexOf("duplicate key") != -1 )
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CS_PICK",utility.GetUserCulture());
					}
					else if(strMsg.IndexOf("FK") != -1 )
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CS_PICK",utility.GetUserCulture());
					}
					else
					{
						lblErrorMsg.Text = appException.Message.ToString();
					}
					isError = false;
					m_sdsCustomerSchedPickups.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])].RejectChanges();
					return isError;
				}
				lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
			}

			return isError;
		}


		private void ClearAllFields()
		{
			txtCustomerCode.Text = "";
			txtCustomerName.Text = "";
			txtSendName.Text = "";
			txtSendAddr1.Text = "";

			if(chkPickHolidays.Enabled)
				chkPickHolidays.Checked = false;

			txtMon1.Text = "";
			txtMon2.Text = "";
			txtMon3.Text = "";
			txtTue1.Text = "";
			txtTue2.Text = "";
			txtTue3.Text = "";
			txtWed1.Text = "";
			txtWed2.Text = "";
			txtWed3.Text = "";
			txtThu1.Text = "";
			txtThu2.Text = "";
			txtThu3.Text = "";
			txtFri1.Text = "";
			txtFri2.Text = "";
			txtFri3.Text = "";
			txtSat1.Text = "";
			txtSat2.Text = "";
			txtSat3.Text = "";
			txtSun1.Text = "";
			txtSun2.Text = "";
			txtSun3.Text = "";
			

			chkMon.Checked = false;
			chkTue.Checked = false;
			chkWed.Checked = false;
			chkThu.Checked = false;
			chkFri.Checked = false;

			if(chkSat.Enabled)
				chkSat.Checked = false;
			if(chkSun.Enabled)
				chkSun.Checked = false;


			clearDBCombo(DbComboPathCodeMon1);
			clearDBCombo(DbComboPathCodeMon2);
			clearDBCombo(DbComboPathCodeMon3);
			clearDBCombo(DbComboPathCodeTue1);
			clearDBCombo(DbComboPathCodeTue2);
			clearDBCombo(DbComboPathCodeTue3);
			clearDBCombo(DbComboPathCodeWed1);
			clearDBCombo(DbComboPathCodeWed2);
			clearDBCombo(DbComboPathCodeWed3);
			clearDBCombo(DbComboPathCodeThu1);
			clearDBCombo(DbComboPathCodeThu2);
			clearDBCombo(DbComboPathCodeThu3);
			clearDBCombo(DbComboPathCodeFri1);
			clearDBCombo(DbComboPathCodeFri2);
			clearDBCombo(DbComboPathCodeFri3);
			clearDBCombo(DbComboPathCodeSat1);
			clearDBCombo(DbComboPathCodeSat2);
			clearDBCombo(DbComboPathCodeSat3);	
			clearDBCombo(DbComboPathCodeSun1);
			clearDBCombo(DbComboPathCodeSun2);
			clearDBCombo(DbComboPathCodeSun3);
			setPickupInput();
		}


		private void FetchDSRecSet()
		{
			String tmpStr = ViewState["currentSet"].ToString().Trim();
			int iStartIndex = 0;
			int intIndexOf = tmpStr.IndexOf(".");
	
			if (intIndexOf > 0)
			{
				iStartIndex = Convert.ToInt32(tmpStr.Substring(0, intIndexOf)) * m_iSetSize;
			}
			else
			{
				iStartIndex = Convert.ToInt32(tmpStr) * m_iSetSize;
			}
			
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsCustomerSchedPickups = CustomerScheduledPickupsDAL.GetCustomerSchedPickupsData(appID,enterpriseID,iStartIndex,m_iSetSize,dsQuery);
			Session["CSP_SESSION"] = m_sdsCustomerSchedPickups;
			decimal pgCnt = (m_sdsCustomerSchedPickups.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < Convert.ToDecimal(ViewState["currentSet"]))
			{
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
			}
		}


		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}


		private void DisplayRecords()
		{
			DataRow drEach = m_sdsCustomerSchedPickups.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];

			//custid
			if ((drEach["custid"] != null) && (drEach["custid"].ToString() != ""))
			{
				txtCustomerCode.Text = drEach["custid"].ToString();

				DataSet dsCustData = CustomerScheduledPickupsDAL.GetCustData(appID, enterpriseID,
					txtCustomerCode.Text.Trim());
				if(dsCustData.Tables[0].Rows.Count > 0) 
				{
					txtCustomerName.Text = dsCustData.Tables[0].Rows[0]["cust_name"].ToString();
					txtSendAddr1.Text = dsCustData.Tables[0].Rows[0]["address1"].ToString();
				}
				else
				{
					txtCustomerName.Text = "";
					txtSendAddr1.Text = "";
				}	
			}
			else
			{
				txtCustomerCode.Text = "";
				txtCustomerName.Text = "";
				txtSendAddr1.Text = "";
			}

			
			//reference
			if ((drEach["reference"] != null) && (drEach["reference"].ToString() != ""))
			{
				String reference = drEach["reference"].ToString();

				if(reference == "Y")
				{
					//cust_ref_name
					if ((drEach["cust_ref_name"] != null) && (drEach["cust_ref_name"].ToString() != ""))
					{
						txtSendName.Text = drEach["cust_ref_name"].ToString();

						DataSet dsSender = CustomerScheduledPickupsDAL.GetSenderData(appID, enterpriseID,
							txtSendName.Text.Trim(), txtCustomerCode.Text.Trim(), "C");

						if(dsSender.Tables[0].Rows.Count > 0) 
						{
							txtSendAddr1.Text = dsSender.Tables[0].Rows[0]["address1"].ToString();
						}
						else
						{
							txtSendAddr1.Text = "";
						}	
					}
					else
					{
						txtSendName.Text = "";
						if (txtCustomerCode.Text.Trim() == "")
							txtSendAddr1.Text = "";
					}
				}		
				else
				{
					txtSendName.Text = "";
				}
			}

			//pubholiday
			if(chkPickHolidays.Enabled)
			{
				if ((drEach["pubholiday"] != null) && (drEach["pubholiday"].ToString() != ""))
				{
					if(drEach["pubholiday"].ToString() == "Y")
						chkPickHolidays.Checked = true;
					else
						chkPickHolidays.Checked = false;
				}
			}

			//mon_putime1
			if ((drEach["mon_putime1"] != null) && (drEach["mon_putime1"].ToString() != ""))
			{
				txtMon1.Text = ((DateTime)drEach["mon_putime1"]).ToString("HH:mm");
			}
			else
			{
				txtMon1.Text = "";
			}

			//mon_putime2
			if ((drEach["mon_putime2"] != null) && (drEach["mon_putime2"].ToString() != ""))
			{
				txtMon2.Text = ((DateTime)drEach["mon_putime2"]).ToString("HH:mm");
			}
			else
			{
				txtMon2.Text = "";
			}


			//mon_putime3
			if ((drEach["mon_putime3"] != null) && (drEach["mon_putime3"].ToString() != ""))
			{
				txtMon3.Text = ((DateTime)drEach["mon_putime3"]).ToString("HH:mm");
			}
			else
			{
				txtMon3.Text = "";
			}


			//tue_putime1
			if ((drEach["tue_putime1"] != null) && (drEach["tue_putime1"].ToString() != ""))
			{
				txtTue1.Text = ((DateTime)drEach["tue_putime1"]).ToString("HH:mm");
			}
			else
			{
				txtTue1.Text = "";
			}


			//tue_putime2
			if ((drEach["tue_putime2"] != null) && (drEach["tue_putime2"].ToString() != ""))
			{
				txtTue2.Text = ((DateTime)drEach["tue_putime2"]).ToString("HH:mm");
			}
			else
			{
				txtTue2.Text = "";
			}


			//tue_putime3
			if ((drEach["tue_putime3"] != null) && (drEach["tue_putime3"].ToString() != ""))
			{
				txtTue3.Text = ((DateTime)drEach["tue_putime3"]).ToString("HH:mm");
			}
			else
			{
				txtTue3.Text = "";
			}


			//wed_putime1
			if ((drEach["wed_putime1"] != null) && (drEach["wed_putime1"].ToString() != ""))
			{
				txtWed1.Text = ((DateTime)drEach["wed_putime1"]).ToString("HH:mm");
			}
			else
			{
				txtWed1.Text = "";
			}


			//wed_putime2
			if ((drEach["wed_putime2"] != null) && (drEach["wed_putime2"].ToString() != ""))
			{
				txtWed2.Text = ((DateTime)drEach["wed_putime2"]).ToString("HH:mm");
			}
			else
			{
				txtWed2.Text = "";
			}


			//wed_putime3
			if ((drEach["wed_putime3"] != null) && (drEach["wed_putime3"].ToString() != ""))
			{
				txtWed3.Text = ((DateTime)drEach["wed_putime3"]).ToString("HH:mm");
			}
			else
			{
				txtWed3.Text = "";
			}


			//thu_putime1
			if ((drEach["thu_putime1"] != null) && (drEach["thu_putime1"].ToString() != ""))
			{
				txtThu1.Text = ((DateTime)drEach["thu_putime1"]).ToString("HH:mm");
			}
			else
			{
				txtThu1.Text = "";
			}


			//thu_putime2
			if ((drEach["thu_putime2"] != null) && (drEach["thu_putime2"].ToString() != ""))
			{
				txtThu2.Text = ((DateTime)drEach["thu_putime2"]).ToString("HH:mm");
			}
			else
			{
				txtThu2.Text = "";
			}


			//thu_putime3
			if ((drEach["thu_putime3"] != null) && (drEach["thu_putime3"].ToString() != ""))
			{
				txtThu3.Text = ((DateTime)drEach["thu_putime3"]).ToString("HH:mm");
			}
			else
			{
				txtThu3.Text = "";
			}


			//fri_putime1
			if ((drEach["fri_putime1"] != null) && (drEach["fri_putime1"].ToString() != ""))
			{
				txtFri1.Text = ((DateTime)drEach["fri_putime1"]).ToString("HH:mm");
			}
			else
			{
				txtFri1.Text = "";
			}


			//fri_putime2
			if ((drEach["fri_putime2"] != null) && (drEach["fri_putime2"].ToString() != ""))
			{
				txtFri2.Text = ((DateTime)drEach["fri_putime2"]).ToString("HH:mm");
			}
			else
			{
				txtFri2.Text = "";
			}


			//fri_putime3
			if ((drEach["fri_putime3"] != null) && (drEach["fri_putime3"].ToString() != ""))
			{
				txtFri3.Text = ((DateTime)drEach["fri_putime3"]).ToString("HH:mm");
			}
			else
			{
				txtFri3.Text = "";
			}


			//sat_putime1
			if ((drEach["sat_putime1"] != null) && (drEach["sat_putime1"].ToString() != ""))
			{
				txtSat1.Text = ((DateTime)drEach["sat_putime1"]).ToString("HH:mm");
			}
			else
			{
				txtSat1.Text = "";
			}


			//sat_putime2
			if ((drEach["sat_putime2"] != null) && (drEach["sat_putime2"].ToString() != ""))
			{
				txtSat2.Text = ((DateTime)drEach["sat_putime2"]).ToString("HH:mm");
			}
			else
			{
				txtSat2.Text = "";
			}


			//sat_putime3
			if ((drEach["sat_putime3"] != null) && (drEach["sat_putime3"].ToString() != ""))
			{
				txtSat3.Text = ((DateTime)drEach["sat_putime3"]).ToString("HH:mm");
			}
			else
			{
				txtSat3.Text = "";
			}


			//sun_putime1
			if ((drEach["sun_putime1"] != null) && (drEach["sun_putime1"].ToString() != ""))
			{
				txtSun1.Text = ((DateTime)drEach["sun_putime1"]).ToString("HH:mm");
			}
			else
			{
				txtSun1.Text = "";
			}


			//sun_putime2
			if ((drEach["sun_putime2"] != null) && (drEach["sun_putime2"].ToString() != ""))
			{
				txtSun2.Text = ((DateTime)drEach["sun_putime2"]).ToString("HH:mm");
			}
			else
			{
				txtSun2.Text = "";
			}


			//sun_putime3
			if ((drEach["sun_putime3"] != null) && (drEach["sun_putime3"].ToString() != ""))
			{
				txtSun3.Text = ((DateTime)drEach["sun_putime3"]).ToString("HH:mm");
			}
			else
			{
				txtSun3.Text = "";
			}

			//mon_active
			if ((drEach["mon_active"] != null) && (drEach["mon_active"].ToString() != ""))
			{
				if(drEach["mon_active"].ToString() == "Y")
					chkMon.Checked = true;
				else
					chkMon.Checked = false;
			}
			else
			{
				chkMon.Checked = false;
			}

			//tue_active
			if ((drEach["tue_active"] != null) && (drEach["tue_active"].ToString() != ""))
			{
				if(drEach["tue_active"].ToString() == "Y")
					chkTue.Checked = true;
				else
					chkTue.Checked = false;
			}
			else
			{
				chkTue.Checked = false;
			}

			//wed_active
			if ((drEach["wed_active"] != null) && (drEach["wed_active"].ToString() != ""))
			{
				if(drEach["wed_active"].ToString() == "Y")
					chkWed.Checked = true;
				else
					chkWed.Checked = false;
			}
			else
			{
				chkWed.Checked = false;
			}

			//thu_active
			if ((drEach["thu_active"] != null) && (drEach["thu_active"].ToString() != ""))
			{
				if(drEach["thu_active"].ToString() == "Y")
					chkThu.Checked = true;
				else
					chkThu.Checked = false;
			}
			else
			{
				chkThu.Checked = false;
			}

			//fri_active
			if ((drEach["fri_active"] != null) && (drEach["fri_active"].ToString() != ""))
			{
				if(drEach["fri_active"].ToString() == "Y")
					chkFri.Checked = true;
				else
					chkFri.Checked = false;
			}
			else
			{
				chkFri.Checked = false;
			}

			//sat_active
			if ((drEach["sat_active"] != null) && (drEach["sat_active"].ToString() != ""))
			{
				if(drEach["sat_active"].ToString() == "Y")
					chkSat.Checked = true;
				else
					chkSat.Checked = false;
			}
			else
			{
				chkSat.Checked = false;
			}

			//sun_active
			if ((drEach["sun_active"] != null) && (drEach["sun_active"].ToString() != ""))
			{
				if(drEach["sun_active"].ToString() == "Y")
					chkSun.Checked = true;
				else
					chkSun.Checked = false;
			}
			else
			{
				chkSun.Checked = false;
			}

			//mon_routecode1
			if ((drEach["mon_routecode1"] != null) && (drEach["mon_routecode1"].ToString() != ""))
			{
				DbComboPathCodeMon1.Value =  drEach["mon_routecode1"].ToString();
				DbComboPathCodeMon1.Text =  drEach["mon_routecode1"].ToString();
			}
			else
			{
				DbComboPathCodeMon1.Value = "";
				DbComboPathCodeMon1.Text = "";
			}

			//mon_routecode2
			if ((drEach["mon_routecode2"] != null) && (drEach["mon_routecode2"].ToString() != ""))
			{
				DbComboPathCodeMon2.Value =  drEach["mon_routecode2"].ToString();
				DbComboPathCodeMon2.Text =  drEach["mon_routecode2"].ToString();
			}
			else
			{
				DbComboPathCodeMon2.Value = "";
				DbComboPathCodeMon2.Text = "";
			}


			//mon_routecode3
			if ((drEach["mon_routecode3"] != null) && (drEach["mon_routecode3"].ToString() != ""))
			{
				DbComboPathCodeMon3.Value =  drEach["mon_routecode3"].ToString();
				DbComboPathCodeMon3.Text =  drEach["mon_routecode3"].ToString();
			}
			else
			{
				DbComboPathCodeMon3.Value = "";
				DbComboPathCodeMon3.Text = "";
			}


			//tue_routecode1
			if ((drEach["tue_routecode1"] != null) && (drEach["tue_routecode1"].ToString() != ""))
			{
				DbComboPathCodeTue1.Value =  drEach["tue_routecode1"].ToString();
				DbComboPathCodeTue1.Text =  drEach["tue_routecode1"].ToString();
			}
			else
			{
				DbComboPathCodeTue1.Value = "";
				DbComboPathCodeTue1.Text = "";
			}


			//tue_routecode2
			if ((drEach["tue_routecode2"] != null) && (drEach["tue_routecode2"].ToString() != ""))
			{
				DbComboPathCodeTue2.Value =  drEach["tue_routecode2"].ToString();
				DbComboPathCodeTue2.Text =  drEach["tue_routecode2"].ToString();
			}
			else
			{
				DbComboPathCodeTue2.Value = "";
				DbComboPathCodeTue2.Text = "";
			}


			//tue_routecode3
			if ((drEach["tue_routecode3"] != null) && (drEach["tue_routecode3"].ToString() != ""))
			{
				DbComboPathCodeTue3.Value =  drEach["tue_routecode3"].ToString();
				DbComboPathCodeTue3.Text =  drEach["tue_routecode3"].ToString();
			}
			else
			{
				DbComboPathCodeTue3.Value = "";
				DbComboPathCodeTue3.Text = "";
			}


			//wed_routecode1
			if ((drEach["wed_routecode1"] != null) && (drEach["wed_routecode1"].ToString() != ""))
			{
				DbComboPathCodeWed1.Value =  drEach["wed_routecode1"].ToString();
				DbComboPathCodeWed1.Text =  drEach["wed_routecode1"].ToString();
			}
			else
			{
				DbComboPathCodeWed1.Value = "";
				DbComboPathCodeWed1.Text = "";
			}


			//wed_routecode2
			if ((drEach["wed_routecode2"] != null) && (drEach["wed_routecode2"].ToString() != ""))
			{
				DbComboPathCodeWed2.Value =  drEach["wed_routecode2"].ToString();
				DbComboPathCodeWed2.Text =  drEach["wed_routecode2"].ToString();
			}
			else
			{
				DbComboPathCodeWed2.Value = "";
				DbComboPathCodeWed2.Text = "";
			}


			//wed_routecode3
			if ((drEach["wed_routecode3"] != null) && (drEach["wed_routecode3"].ToString() != ""))
			{
				DbComboPathCodeWed3.Value =  drEach["wed_routecode3"].ToString();
				DbComboPathCodeWed3.Text =  drEach["wed_routecode3"].ToString();
			}
			else
			{
				DbComboPathCodeWed3.Value = "";
				DbComboPathCodeWed3.Text = "";
			}


			//thu_routecode1
			if ((drEach["thu_routecode1"] != null) && (drEach["thu_routecode1"].ToString() != ""))
			{
				DbComboPathCodeThu1.Value =  drEach["thu_routecode1"].ToString();
				DbComboPathCodeThu1.Text =  drEach["thu_routecode1"].ToString();
			}
			else
			{
				DbComboPathCodeThu1.Value = "";
				DbComboPathCodeThu1.Text = "";
			}


			//thu_routecode2
			if ((drEach["thu_routecode2"] != null) && (drEach["thu_routecode2"].ToString() != ""))
			{
				DbComboPathCodeThu2.Value =  drEach["thu_routecode2"].ToString();
				DbComboPathCodeThu2.Text =  drEach["thu_routecode2"].ToString();
			}
			else
			{
				DbComboPathCodeThu2.Value = "";
				DbComboPathCodeThu2.Text = "";
			}


			//thu_routecode3
			if ((drEach["thu_routecode3"] != null) && (drEach["thu_routecode3"].ToString() != ""))
			{
				DbComboPathCodeThu3.Value =  drEach["thu_routecode3"].ToString();
				DbComboPathCodeThu3.Text =  drEach["thu_routecode3"].ToString();
			}
			else
			{
				DbComboPathCodeThu3.Value = "";
				DbComboPathCodeThu3.Text = "";
			}


			//fri_routecode1
			if ((drEach["fri_routecode1"] != null) && (drEach["fri_routecode1"].ToString() != ""))
			{
				DbComboPathCodeFri1.Value =  drEach["fri_routecode1"].ToString();
				DbComboPathCodeFri1.Text =  drEach["fri_routecode1"].ToString();
			}
			else
			{
				
				DbComboPathCodeFri1.Value ="";
				DbComboPathCodeFri1.Text = "";
			}


			//fri_routecode2
			if ((drEach["fri_routecode2"] != null) && (drEach["fri_routecode2"].ToString() != ""))
			{
				DbComboPathCodeFri2.Text =  drEach["fri_routecode2"].ToString();
				DbComboPathCodeFri2.Value =  drEach["fri_routecode2"].ToString();
			}
			else
			{
				DbComboPathCodeFri2.Text = "";
				DbComboPathCodeFri2.Value = "";
			}


			//fri_routecode3
			if ((drEach["fri_routecode3"] != null) && (drEach["fri_routecode3"].ToString() != ""))
			{
				DbComboPathCodeFri3.Text =  drEach["fri_routecode3"].ToString();
				DbComboPathCodeFri3.Value =  drEach["fri_routecode3"].ToString();
			}
			else
			{
				DbComboPathCodeFri3.Text = "";
				DbComboPathCodeFri3.Value = "";
			}


			//sat_routecode1
			if ((drEach["sat_routecode1"] != null) && (drEach["sat_routecode1"].ToString() != ""))
			{
				DbComboPathCodeSat1.Text =  drEach["sat_routecode1"].ToString();
				DbComboPathCodeSat1.Value =  drEach["sat_routecode1"].ToString();
			}
			else
			{
				DbComboPathCodeSat1.Text = "";
				DbComboPathCodeSat1.Value = "";
			}


			//sat_routecode2
			if ((drEach["sat_routecode2"] != null) && (drEach["sat_routecode2"].ToString() != ""))
			{
				DbComboPathCodeSat2.Text =  drEach["sat_routecode2"].ToString();
				DbComboPathCodeSat2.Value =  drEach["sat_routecode2"].ToString();
			}
			else
			{
				DbComboPathCodeSat2.Text = "";
				DbComboPathCodeSat2.Value = "";
			}


			//sat_routecode3
			if ((drEach["sat_routecode3"] != null) && (drEach["sat_routecode3"].ToString() != ""))
			{
				DbComboPathCodeSat3.Text =  drEach["sat_routecode3"].ToString();
				DbComboPathCodeSat3.Value =  drEach["sat_routecode3"].ToString();
			}
			else
			{
				DbComboPathCodeSat3.Text = "";
				DbComboPathCodeSat3.Value = "";
			}


			//sun_routecode1
			if ((drEach["sun_routecode1"] != null) && (drEach["sun_routecode1"].ToString() != ""))
			{
				DbComboPathCodeSun1.Text =  drEach["sun_routecode1"].ToString();
				DbComboPathCodeSun1.Value =  drEach["sun_routecode1"].ToString();
			}
			else
			{
				DbComboPathCodeSun1.Text = "";
				DbComboPathCodeSun1.Value = "";
			}


			//sun_routecode2
			if ((drEach["sun_routecode2"] != null) && (drEach["sun_routecode2"].ToString() != ""))
			{
				DbComboPathCodeSun2.Text =  drEach["sun_routecode2"].ToString();
				DbComboPathCodeSun2.Value =  drEach["sun_routecode2"].ToString();
			}
			else
			{
				DbComboPathCodeSun2.Text = "";
				DbComboPathCodeSun2.Value = "";
			}


			//sun_routecode3
			if ((drEach["sun_routecode3"] != null) && (drEach["sun_routecode3"].ToString() != ""))
			{
				DbComboPathCodeSun3.Text =  drEach["sun_routecode3"].ToString();
				DbComboPathCodeSun3.Value =  drEach["sun_routecode3"].ToString();
			}
			else
			{
				DbComboPathCodeSun3.Text = "";
				DbComboPathCodeSun3.Value = "";
			}

		}


		private void DisplayRecords(string rowsid)
		{
			DataRow drEach = m_sdsCustomerSchedPickups.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];

			switch(rowsid)
			{
				case "mon":
					//mon_putime1
					if ((drEach["mon_putime1"] != null) && (drEach["mon_putime1"].ToString() != ""))
					{
						txtMon1.Text = ((DateTime)drEach["mon_putime1"]).ToString("HH:mm");
					}
					else
					{
						txtMon1.Text = "";
					}

					//mon_putime2
					if ((drEach["mon_putime2"] != null) && (drEach["mon_putime2"].ToString() != ""))
					{
						txtMon2.Text = ((DateTime)drEach["mon_putime2"]).ToString("HH:mm");
					}
					else
					{
						txtMon2.Text = "";
					}

					//mon_putime3
					if ((drEach["mon_putime3"] != null) && (drEach["mon_putime3"].ToString() != ""))
					{
						txtMon3.Text = ((DateTime)drEach["mon_putime3"]).ToString("HH:mm");
					}
					else
					{
						txtMon3.Text = "";
					}

					//mon_routecode1
					if ((drEach["mon_routecode1"] != null) && (drEach["mon_routecode1"].ToString() != ""))
					{
						DbComboPathCodeMon1.Value =  drEach["mon_routecode1"].ToString();
						DbComboPathCodeMon1.Text =  drEach["mon_routecode1"].ToString();
					}
					else
					{
						DbComboPathCodeMon1.Value = "";
						DbComboPathCodeMon1.Text = "";
					}

					//mon_routecode2
					if ((drEach["mon_routecode2"] != null) && (drEach["mon_routecode2"].ToString() != ""))
					{
						DbComboPathCodeMon2.Value =  drEach["mon_routecode2"].ToString();
						DbComboPathCodeMon2.Text =  drEach["mon_routecode2"].ToString();
					}
					else
					{
						DbComboPathCodeMon2.Value = "";
						DbComboPathCodeMon2.Text = "";
					}


					//mon_routecode3
					if ((drEach["mon_routecode3"] != null) && (drEach["mon_routecode3"].ToString() != ""))
					{
						DbComboPathCodeMon3.Value =  drEach["mon_routecode3"].ToString();
						DbComboPathCodeMon3.Text =  drEach["mon_routecode3"].ToString();
					}
					else
					{
						DbComboPathCodeMon3.Value = "";
						DbComboPathCodeMon3.Text = "";
					}
					break;

				case "tue":
					//tue_putime1
					if ((drEach["tue_putime1"] != null) && (drEach["tue_putime1"].ToString() != ""))
					{
						txtTue1.Text = ((DateTime)drEach["tue_putime1"]).ToString("HH:mm");
					}
					else
					{
						txtTue1.Text = "";
					}

					//tue_putime2
					if ((drEach["tue_putime2"] != null) && (drEach["tue_putime2"].ToString() != ""))
					{
						txtTue2.Text = ((DateTime)drEach["tue_putime2"]).ToString("HH:mm");
					}
					else
					{
						txtTue2.Text = "";
					}

					//tue_putime3
					if ((drEach["tue_putime3"] != null) && (drEach["tue_putime3"].ToString() != ""))
					{
						txtTue3.Text = ((DateTime)drEach["tue_putime3"]).ToString("HH:mm");
					}
					else
					{
						txtTue3.Text = "";
					}

					//tue_routecode1
					if ((drEach["tue_routecode1"] != null) && (drEach["tue_routecode1"].ToString() != ""))
					{
						DbComboPathCodeTue1.Value =  drEach["tue_routecode1"].ToString();
						DbComboPathCodeTue1.Text =  drEach["tue_routecode1"].ToString();
					}
					else
					{
						DbComboPathCodeTue1.Value = "";
						DbComboPathCodeTue1.Text = "";
					}

					//tue_routecode2
					if ((drEach["tue_routecode2"] != null) && (drEach["tue_routecode2"].ToString() != ""))
					{
						DbComboPathCodeTue2.Value =  drEach["tue_routecode2"].ToString();
						DbComboPathCodeTue2.Text =  drEach["tue_routecode2"].ToString();
					}
					else
					{
						DbComboPathCodeTue2.Value = "";
						DbComboPathCodeTue2.Text = "";
					}

					//tue_routecode3
					if ((drEach["tue_routecode3"] != null) && (drEach["tue_routecode3"].ToString() != ""))
					{
						DbComboPathCodeTue3.Value =  drEach["tue_routecode3"].ToString();
						DbComboPathCodeTue3.Text =  drEach["tue_routecode3"].ToString();
					}
					else
					{
						DbComboPathCodeTue3.Value = "";
						DbComboPathCodeTue3.Text = "";
					}
					break;
				case "wed":
					//wed_putime1
					if ((drEach["wed_putime1"] != null) && (drEach["wed_putime1"].ToString() != ""))
					{
						txtWed1.Text = ((DateTime)drEach["wed_putime1"]).ToString("HH:mm");
					}
					else
					{
						txtWed1.Text = "";
					}

					//wed_putime2
					if ((drEach["wed_putime2"] != null) && (drEach["wed_putime2"].ToString() != ""))
					{
						txtWed2.Text = ((DateTime)drEach["wed_putime2"]).ToString("HH:mm");
					}
					else
					{
						txtWed2.Text = "";
					}

					//wed_putime3
					if ((drEach["wed_putime3"] != null) && (drEach["wed_putime3"].ToString() != ""))
					{
						txtWed3.Text = ((DateTime)drEach["wed_putime3"]).ToString("HH:mm");
					}
					else
					{
						txtWed3.Text = "";
					}

					//wed_routecode1
					if ((drEach["wed_routecode1"] != null) && (drEach["wed_routecode1"].ToString() != ""))
					{
						DbComboPathCodeWed1.Value =  drEach["wed_routecode1"].ToString();
						DbComboPathCodeWed1.Text =  drEach["wed_routecode1"].ToString();
					}
					else
					{
						DbComboPathCodeWed1.Value = "";
						DbComboPathCodeWed1.Text = "";
					}

					//wed_routecode2
					if ((drEach["wed_routecode2"] != null) && (drEach["wed_routecode2"].ToString() != ""))
					{
						DbComboPathCodeWed2.Value =  drEach["wed_routecode2"].ToString();
						DbComboPathCodeWed2.Text =  drEach["wed_routecode2"].ToString();
					}
					else
					{
						DbComboPathCodeWed2.Value = "";
						DbComboPathCodeWed2.Text = "";
					}

					//wed_routecode3
					if ((drEach["wed_routecode3"] != null) && (drEach["wed_routecode3"].ToString() != ""))
					{
						DbComboPathCodeWed3.Value =  drEach["wed_routecode3"].ToString();
						DbComboPathCodeWed3.Text =  drEach["wed_routecode3"].ToString();
					}
					else
					{
						DbComboPathCodeWed3.Value = "";
						DbComboPathCodeWed3.Text = "";
					}
					break;
				case "thu":
					//thu_putime1
					if ((drEach["thu_putime1"] != null) && (drEach["thu_putime1"].ToString() != ""))
					{
						txtThu1.Text = ((DateTime)drEach["thu_putime1"]).ToString("HH:mm");
					}
					else
					{
						txtThu1.Text = "";
					}
					//thu_putime2
					if ((drEach["thu_putime2"] != null) && (drEach["thu_putime2"].ToString() != ""))
					{
						txtThu2.Text = ((DateTime)drEach["thu_putime2"]).ToString("HH:mm");
					}
					else
					{
						txtThu2.Text = "";
					}

					//thu_putime3
					if ((drEach["thu_putime3"] != null) && (drEach["thu_putime3"].ToString() != ""))
					{
						txtThu3.Text = ((DateTime)drEach["thu_putime3"]).ToString("HH:mm");
					}
					else
					{
						txtThu3.Text = "";
					}

					//thu_routecode1
					if ((drEach["thu_routecode1"] != null) && (drEach["thu_routecode1"].ToString() != ""))
					{
						DbComboPathCodeThu1.Value =  drEach["thu_routecode1"].ToString();
						DbComboPathCodeThu1.Text =  drEach["thu_routecode1"].ToString();
					}
					else
					{
						DbComboPathCodeThu1.Value = "";
						DbComboPathCodeThu1.Text = "";
					}

					//thu_routecode2
					if ((drEach["thu_routecode2"] != null) && (drEach["thu_routecode2"].ToString() != ""))
					{
						DbComboPathCodeThu2.Value =  drEach["thu_routecode2"].ToString();
						DbComboPathCodeThu2.Text =  drEach["thu_routecode2"].ToString();
					}
					else
					{
						DbComboPathCodeThu2.Value = "";
						DbComboPathCodeThu2.Text = "";
					}

					//thu_routecode3
					if ((drEach["thu_routecode3"] != null) && (drEach["thu_routecode3"].ToString() != ""))
					{
						DbComboPathCodeThu3.Value =  drEach["thu_routecode3"].ToString();
						DbComboPathCodeThu3.Text =  drEach["thu_routecode3"].ToString();
					}
					else
					{
						DbComboPathCodeThu3.Value = "";
						DbComboPathCodeThu3.Text = "";
					}
					break;
				case "fri":
					//fri_putime1
					if ((drEach["fri_putime1"] != null) && (drEach["fri_putime1"].ToString() != ""))
					{
						txtFri1.Text = ((DateTime)drEach["fri_putime1"]).ToString("HH:mm");
					}
					else
					{
						txtFri1.Text = "";
					}
					//fri_putime2
					if ((drEach["fri_putime2"] != null) && (drEach["fri_putime2"].ToString() != ""))
					{
						txtFri2.Text = ((DateTime)drEach["fri_putime2"]).ToString("HH:mm");
					}
					else
					{
						txtFri2.Text = "";
					}
					//fri_putime3
					if ((drEach["fri_putime3"] != null) && (drEach["fri_putime3"].ToString() != ""))
					{
						txtFri3.Text = ((DateTime)drEach["fri_putime3"]).ToString("HH:mm");
					}
					else
					{
						txtFri3.Text = "";
					}
					
					//fri_routecode1
					if ((drEach["fri_routecode1"] != null) && (drEach["fri_routecode1"].ToString() != ""))
					{
						DbComboPathCodeFri1.Value =  drEach["fri_routecode1"].ToString();
						DbComboPathCodeFri1.Text =  drEach["fri_routecode1"].ToString();
					}
					else
					{
				
						DbComboPathCodeFri1.Value ="";
						DbComboPathCodeFri1.Text = "";
					}

					//fri_routecode2
					if ((drEach["fri_routecode2"] != null) && (drEach["fri_routecode2"].ToString() != ""))
					{
						DbComboPathCodeFri2.Text =  drEach["fri_routecode2"].ToString();
						DbComboPathCodeFri2.Value =  drEach["fri_routecode2"].ToString();
					}
					else
					{
						DbComboPathCodeFri2.Text = "";
						DbComboPathCodeFri2.Value = "";
					}

					//fri_routecode3
					if ((drEach["fri_routecode3"] != null) && (drEach["fri_routecode3"].ToString() != ""))
					{
						DbComboPathCodeFri3.Text =  drEach["fri_routecode3"].ToString();
						DbComboPathCodeFri3.Value =  drEach["fri_routecode3"].ToString();
					}
					else
					{
						DbComboPathCodeFri3.Text = "";
						DbComboPathCodeFri3.Value = "";
					}
					break;
				case "sat":
					//sat_putime1
					if ((drEach["sat_putime1"] != null) && (drEach["sat_putime1"].ToString() != ""))
					{
						txtSat1.Text = ((DateTime)drEach["sat_putime1"]).ToString("HH:mm");
					}
					else
					{
						txtSat1.Text = "";
					}
					//sat_putime2
					if ((drEach["sat_putime2"] != null) && (drEach["sat_putime2"].ToString() != ""))
					{
						txtSat2.Text = ((DateTime)drEach["sat_putime2"]).ToString("HH:mm");
					}
					else
					{
						txtSat2.Text = "";
					}
					//sat_putime3
					if ((drEach["sat_putime3"] != null) && (drEach["sat_putime3"].ToString() != ""))
					{
						txtSat3.Text = ((DateTime)drEach["sat_putime3"]).ToString("HH:mm");
					}
					else
					{
						txtSat3.Text = "";
					}
					//sat_routecode1
					if ((drEach["sat_routecode1"] != null) && (drEach["sat_routecode1"].ToString() != ""))
					{
						DbComboPathCodeSat1.Text =  drEach["sat_routecode1"].ToString();
						DbComboPathCodeSat1.Value =  drEach["sat_routecode1"].ToString();
					}
					else
					{
						DbComboPathCodeSat1.Text = "";
						DbComboPathCodeSat1.Value = "";
					}

					//sat_routecode2
					if ((drEach["sat_routecode2"] != null) && (drEach["sat_routecode2"].ToString() != ""))
					{
						DbComboPathCodeSat2.Text =  drEach["sat_routecode2"].ToString();
						DbComboPathCodeSat2.Value =  drEach["sat_routecode2"].ToString();
					}
					else
					{
						DbComboPathCodeSat2.Text = "";
						DbComboPathCodeSat2.Value = "";
					}

					//sat_routecode3
					if ((drEach["sat_routecode3"] != null) && (drEach["sat_routecode3"].ToString() != ""))
					{
						DbComboPathCodeSat3.Text =  drEach["sat_routecode3"].ToString();
						DbComboPathCodeSat3.Value =  drEach["sat_routecode3"].ToString();
					}
					else
					{
						DbComboPathCodeSat3.Text = "";
						DbComboPathCodeSat3.Value = "";
					}
					break;
				case "sun":
					//sun_putime1
					if ((drEach["sun_putime1"] != null) && (drEach["sun_putime1"].ToString() != ""))
					{
						txtSun1.Text = ((DateTime)drEach["sun_putime1"]).ToString("HH:mm");
					}
					else
					{
						txtSun1.Text = "";
					}
					//sun_putime2
					if ((drEach["sun_putime2"] != null) && (drEach["sun_putime2"].ToString() != ""))
					{
						txtSun2.Text = ((DateTime)drEach["sun_putime2"]).ToString("HH:mm");
					}
					else
					{
						txtSun2.Text = "";
					}
					//sun_putime3
					if ((drEach["sun_putime3"] != null) && (drEach["sun_putime3"].ToString() != ""))
					{
						txtSun3.Text = ((DateTime)drEach["sun_putime3"]).ToString("HH:mm");
					}
					else
					{
						txtSun3.Text = "";
					}
					//sun_routecode1
					if ((drEach["sun_routecode1"] != null) && (drEach["sun_routecode1"].ToString() != ""))
					{
						DbComboPathCodeSun1.Text =  drEach["sun_routecode1"].ToString();
						DbComboPathCodeSun1.Value =  drEach["sun_routecode1"].ToString();
					}
					else
					{
						DbComboPathCodeSun1.Text = "";
						DbComboPathCodeSun1.Value = "";
					}


					//sun_routecode2
					if ((drEach["sun_routecode2"] != null) && (drEach["sun_routecode2"].ToString() != ""))
					{
						DbComboPathCodeSun2.Text =  drEach["sun_routecode2"].ToString();
						DbComboPathCodeSun2.Value =  drEach["sun_routecode2"].ToString();
					}
					else
					{
						DbComboPathCodeSun2.Text = "";
						DbComboPathCodeSun2.Value = "";
					}


					//sun_routecode3
					if ((drEach["sun_routecode3"] != null) && (drEach["sun_routecode3"].ToString() != ""))
					{
						DbComboPathCodeSun3.Text =  drEach["sun_routecode3"].ToString();
						DbComboPathCodeSun3.Value =  drEach["sun_routecode3"].ToString();
					}
					else
					{
						DbComboPathCodeSun3.Text = "";
						DbComboPathCodeSun3.Value = "";
					}
					break;
				default:
					break;
			}
		}


		private void Qry_Click()
		{
			bool state = false;
			if (ViewState["fromInsert"] != null) 
			{
				state = (bool)ViewState["fromInsert"];
				if(!state)
				{
					if(btnSave.Enabled)
					{
						if (((int)ViewState["CSPOperation"] == (int)Operation.Insert) || ((int)ViewState["CSPOperation"] == (int)Operation.Update))
						{
							lblConfirmation.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
							trMain.Visible = false;
							trConfirm.Visible=true;
							ViewState["NextOperation"] = "Query";
							return;
						}
					}
					else
					{
						ViewState["isTextChanged"] = false;
					}
				}
			}
			else
			{
				if(btnSave.Enabled)
				{
					if (((int)ViewState["CSPOperation"] == (int)Operation.Insert) || ((int)ViewState["CSPOperation"] == (int)Operation.Update))
					{
						lblConfirmation.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
						trMain.Visible = false;
						trConfirm.Visible=true;
						ViewState["NextOperation"] = "Query";
						return;
					}
				}
				else
				{
					ViewState["isTextChanged"] = false;
				}
			}

			btnExecQry.Enabled = true;
			Query();
			ViewState["fromInsert"] = false;
		}


		private void Query()
		{
			if (((bool)ViewState["isTextChanged"] == true)&& ((int)ViewState["CSPMode"]==(int)ScreenMode.ExecuteQuery)) 
			{
				lblConfirmation.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				trMain.Visible = false;
				trConfirm.Visible=true;
				ViewState["CSPMode"] = ScreenMode.Query;
				return;
			}

			ViewState["CSPMode"] = ScreenMode.Query;
			ClearAllFields();
			lblErrorMsg.Text = ""; 
			btnExecQry.Enabled = true;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
			Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, false, m_moduleAccessRights);
			Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete, false, m_moduleAccessRights);
			btnGoToFirstPage.Enabled = false;
			btnNextPage.Enabled = false;
			btnPreviousPage.Enabled = false;
			btnGoToLastPage.Enabled = false;
			txtGoToRec.Text = "";
			ViewState["CSPOperation"] = Operation.None;
			ViewState["isTextChanged"] = false;
			ViewState["currentPage"] = 0;
			clearCustomerScheduledPickupsDs();
		}


		private void ResetForQuery()
		{
			m_sdsCustomerSchedPickups = CustomerScheduledPickupsDAL.GetCustomerSchedPickupsDS();
			Session["CSP_SESSION"] = m_sdsCustomerSchedPickups;
		}


		private void Insert_Click()
		{
			lblErrorMsg.Text = "";
			m_sdsCustomerSchedPickups = CustomerScheduledPickupsDAL.GetCustomerSchedPickupsDS();
			Session["CSP_SESSION"] = m_sdsCustomerSchedPickups;

			if(btnSave.Enabled)
			{
				if (((int)ViewState["CSPOperation"] == (int)Operation.Insert) || ((int)ViewState["CSPOperation"] == (int)Operation.Update))
				{
					lblConfirmation.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
					trMain.Visible = false;
					trConfirm.Visible=true;
					ViewState["NextOperation"]="Insert";
					return;
				}
			}
			else
			{
				ViewState["isTextChanged"] = false;
			}

			Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,true,m_moduleAccessRights);
			btnExecQry.Enabled = false;
			Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete, false, m_moduleAccessRights);
			ClearAllFields();
			ViewState["CSPMode"] = ScreenMode.Insert;
			ViewState["CSPOperation"] = Operation.Insert;
			ViewState["currentPage"] = 0;
			ViewState["isTextChanged"] = false;
		}


		private void ExecQry_Click()
		{
			if (((int)ViewState["CSPOperation"] == (int)Operation.Insert) || ((int)ViewState["CSPOperation"] == (int)Operation.Update))
			{
				lblConfirmation.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				trMain.Visible = false;
				trConfirm.Visible=true;
				ViewState["NextOperation"] = "ExecuteQuery";
				return;
			}		

			ExecuteQueryDS();
		}


		private void ExecuteQueryDS()
		{
			GetValuesIntoDS(0, "Query");
			ViewState["QUERY_DS"] = m_sdsCustomerSchedPickups.ds;
			ViewState["CSPMode"] = ScreenMode.ExecuteQuery;
			ViewState["CSPOperation"] = Operation.None;
			ViewState["currentSet"] = 0;
			ViewState["currentPage"] = 0;
			ViewState["isTextChanged"] = false;
			FetchDSRecSet();
			if(m_sdsCustomerSchedPickups.QueryResultMaxSize == 0)
			{
				lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
				EnableNavigationButtons(false,false,false,false);
				return;
			}
			DisplayRecords();
			btnExecQry.Enabled=false;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			Utility.EnableButton(ref btnDelete, com.common.util.ButtonType.Delete,true,m_moduleAccessRights);
			Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,true,m_moduleAccessRights);
			if(m_sdsCustomerSchedPickups != null && m_sdsCustomerSchedPickups.DataSetRecSize > 0)
			{
				EnableNavigationButtons(false,false,true,true);
			}
		}


		private void MoveLastDS()
		{
			lblErrorMsg.Text = "";
			if(btnSave.Enabled)
			{
				if (((int)ViewState["CSPOperation"] == (int)Operation.Insert) || ((int)ViewState["CSPOperation"] == (int)Operation.Update))
				{
					lblConfirmation.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
					trMain.Visible=false;
					trConfirm.Visible=true;
					return;
				}
			}
			else
			{
				ViewState["isTextChanged"] = false;
				ViewState["CSPOperation"] = Operation.None;
			}

			ViewState["currentSet"] = (m_sdsCustomerSchedPickups.QueryResultMaxSize - 1)/m_iSetSize;	
			FetchDSRecSet();
			ViewState["currentPage"] = m_sdsCustomerSchedPickups.DataSetRecSize - 1;

			if(Convert.ToInt32(ViewState["currentPage"]) < 0)
			{
				ViewState["currentPage"] = m_sdsCustomerSchedPickups.QueryResultMaxSize - 1;
			}
			DisplayRecords();	

			EnableNavigationButtons(true,true,false,false);
		}


		private void MoveFirstDS()
		{
			lblErrorMsg.Text = "";			
			if(btnSave.Enabled)
			{
				if (((int)ViewState["CSPOperation"] == (int)Operation.Insert) || ((int)ViewState["CSPOperation"] == (int)Operation.Update))
				{
					lblConfirmation.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
					trMain.Visible=false;
					trConfirm.Visible=true;
					return;
				}
			}
			else
			{
				ViewState["isTextChanged"] = false;
				ViewState["CSPOperation"] = Operation.None;
			}
			ViewState["currentSet"] = 0;	
			FetchDSRecSet();
			ViewState["currentPage"] = 0;
			DisplayRecords();
			EnableNavigationButtons(false,false,true,true);
		}


		private void MoveNextDS()
		{
			lblErrorMsg.Text = "";
			if(btnSave.Enabled)
			{
				if (((int)ViewState["CSPOperation"] == (int)Operation.Insert) || ((int)ViewState["CSPOperation"] == (int)Operation.Update))
				{
					lblConfirmation.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
					trMain.Visible=false;
					trConfirm.Visible=true;
					return;
				}
			}
			else
			{
				ViewState["isTextChanged"] = false;
				ViewState["CSPOperation"] = Operation.None;
			}
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  < (m_sdsCustomerSchedPickups.DataSetRecSize - 1) )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) + 1;
				DisplayRecords();
			}
			else
			{
				int iTotalRec = (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) + Convert.ToInt32(m_sdsCustomerSchedPickups.DataSetRecSize);
		
				if( iTotalRec ==  m_sdsCustomerSchedPickups.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) + 1;	
				FetchDSRecSet();
				ViewState["currentPage"] = 0;
				DisplayRecords();
			}
			EnableNavigationButtons(true,true,true,true);
		}


		private void MovePreviousDS()
		{
			lblErrorMsg.Text = "";
			if(btnSave.Enabled)
			{
				if (((int)ViewState["CSPOperation"] == (int)Operation.Insert) || ((int)ViewState["CSPOperation"] == (int)Operation.Update))
				{
					lblConfirmation.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
					trMain.Visible=false;
					trConfirm.Visible=true;
					return;
				}
			}
			else
			{
				ViewState["isTextChanged"] = false;
				ViewState["CSPOperation"] = Operation.None;
			}
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  > 0 )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) - 1;
				DisplayRecords();
			}
			else
			{
				if( (Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) - 1;	
				FetchDSRecSet();
				ViewState["currentPage"] = m_sdsCustomerSchedPickups.DataSetRecSize - 1;
				DisplayRecords();
			}
			EnableNavigationButtons(true,true,true,true);
		}


		private void clearCustomerScheduledPickupsDs()
		{
			if (m_sdsCustomerSchedPickups != null && m_sdsCustomerSchedPickups.ds.Tables[0].Rows.Count > 0)
			{
				for(int i = 0; i < m_sdsCustomerSchedPickups.ds.Tables[0].Columns.Count - 1; i++)
				{
					m_sdsCustomerSchedPickups.ds.Tables[0].Rows[0][i] = System.DBNull.Value;
				}
				m_sdsCustomerSchedPickups.ds.Tables[0].AcceptChanges();
				Session["CSP_SESSION"] = m_sdsCustomerSchedPickups;
			}	
		}


		private bool checkValidInput()
		{
			bool state = true;
			if(chkMon.Checked)
			{
				if((txtMon1.Text.Trim() == "") && (txtMon2.Text.Trim() == "") && (txtMon3.Text.Trim() == ""))
					state = false;
			}

			if(chkTue.Checked)
			{
				if((txtTue1.Text.Trim() == "") && (txtTue2.Text.Trim() == "") && (txtTue3.Text.Trim() == ""))
					state = false;
			}

			if(chkWed.Checked)
			{
				if((txtWed1.Text.Trim() == "") && (txtWed2.Text.Trim() == "") && (txtWed3.Text.Trim() == ""))
					state = false;
			}

			if(chkThu.Checked)
			{
				if((txtThu1.Text.Trim() == "") && (txtThu2.Text.Trim() == "") && (txtThu3.Text.Trim() == ""))
					state = false;
			}

			if(chkFri.Checked)
			{
				if((txtFri1.Text.Trim() == "") && (txtFri2.Text.Trim() == "") && (txtFri3.Text.Trim() == ""))
					state = false;
			}

			if(chkSat.Checked)
			{
				if((txtSat1.Text.Trim() == "") && (txtSat2.Text.Trim() == "") && (txtSat3.Text.Trim() == ""))
					state = false;
			}

			if(chkSun.Checked)
			{
				if((txtSun1.Text.Trim() == "") && (txtSun2.Text.Trim() == "") && (txtSun3.Text.Trim() == ""))
					state = false;
			}
			return state;
		}


		private ArrayList getRearranged(String Pick1, String Pick2, String Pick3)
		{
			ArrayList al = new ArrayList();

			if(Pick1 != "")
				al.Add(System.DateTime.ParseExact(Pick1,"HH:mm",null));
			if(Pick2 != "")
				al.Add(System.DateTime.ParseExact(Pick2,"HH:mm",null));
			if(Pick3 != "")
				al.Add(System.DateTime.ParseExact(Pick3,"HH:mm",null));
			al.Sort();

			return al;
		}
		private ArrayList getRearrangedRouteCode(String Pick1, String Pick2, String Pick3)
		{
			ArrayList al = new ArrayList();

			//if(Pick1 != "")
				al.Add(Pick1);
			//if(Pick2 != "")
				al.Add(Pick2);
			//if(Pick3 != "")
				al.Add(Pick3);
			//al.Sort();

			return al;
		}


		private void setPickupInput()
		{
			if(chkMon.Checked)
			{
				txtMon1.Enabled = true;
				txtMon2.Enabled = true;
				txtMon3.Enabled = true;
				DbComboPathCodeMon1.Enabled=true;
				DbComboPathCodeMon2.Enabled=true;	
				DbComboPathCodeMon3.Enabled=true;
			}
			else
			{
				txtMon1.Enabled = false;
				txtMon2.Enabled = false;
				txtMon3.Enabled = false;
				txtMon1.Text = "";
				txtMon2.Text = "";
				txtMon3.Text = "";
				DbComboPathCodeMon1.Enabled=false;
				DbComboPathCodeMon2.Enabled=false;	
				DbComboPathCodeMon3.Enabled=false;
				clearDBCombo(DbComboPathCodeMon1);
				clearDBCombo(DbComboPathCodeMon2);
				clearDBCombo(DbComboPathCodeMon3);
			}

			if(chkTue.Checked)
			{
				txtTue1.Enabled = true;
				txtTue2.Enabled = true;
				txtTue3.Enabled = true;
				DbComboPathCodeTue1.Enabled=true;
				DbComboPathCodeTue2.Enabled=true;	
				DbComboPathCodeTue3.Enabled=true;
			}
			else
			{
				txtTue1.Enabled = false;
				txtTue2.Enabled = false;
				txtTue3.Enabled = false;
				txtTue1.Text = "";
				txtTue2.Text = "";
				txtTue3.Text = "";
				DbComboPathCodeTue1.Enabled=false;
				DbComboPathCodeTue2.Enabled=false;	
				DbComboPathCodeTue3.Enabled=false;
				clearDBCombo(DbComboPathCodeTue1);
				clearDBCombo(DbComboPathCodeTue2);
				clearDBCombo(DbComboPathCodeTue3);
			}

			if(chkWed.Checked)
			{
				txtWed1.Enabled = true;
				txtWed2.Enabled = true;
				txtWed3.Enabled = true;
				DbComboPathCodeWed1.Enabled=true;
				DbComboPathCodeWed2.Enabled=true;	
				DbComboPathCodeWed3.Enabled=true;

			}
			else
			{
				txtWed1.Enabled = false;
				txtWed2.Enabled = false;
				txtWed3.Enabled = false;
				txtWed1.Text = "";
				txtWed2.Text = "";
				txtWed3.Text = "";
				DbComboPathCodeWed1.Enabled=false;
				DbComboPathCodeWed2.Enabled=false;	
				DbComboPathCodeWed3.Enabled=false;
				clearDBCombo(DbComboPathCodeWed1);
				clearDBCombo(DbComboPathCodeWed2);
				clearDBCombo(DbComboPathCodeWed3);
			}

			if(chkThu.Checked)
			{
				txtThu1.Enabled = true;
				txtThu2.Enabled = true;
				txtThu3.Enabled = true;
				DbComboPathCodeThu1.Enabled=true;
				DbComboPathCodeThu2.Enabled=true;	
				DbComboPathCodeThu3.Enabled=true;
			}
			else
			{
				txtThu1.Enabled = false;
				txtThu2.Enabled = false;
				txtThu3.Enabled = false;
				txtThu1.Text = "";
				txtThu2.Text = "";
				txtThu3.Text = "";
				DbComboPathCodeThu1.Enabled=false;
				DbComboPathCodeThu2.Enabled=false;	
				DbComboPathCodeThu3.Enabled=false;
				clearDBCombo(DbComboPathCodeThu1);
				clearDBCombo(DbComboPathCodeThu2);
				clearDBCombo(DbComboPathCodeThu3);
			}

			if(chkFri.Checked)
			{
				txtFri1.Enabled = true;
				txtFri2.Enabled = true;
				txtFri3.Enabled = true;
				DbComboPathCodeFri1.Enabled=true;
				DbComboPathCodeFri2.Enabled=true;	
				DbComboPathCodeFri3.Enabled=true;
			}
			else
			{
				txtFri1.Enabled = false;
				txtFri2.Enabled = false;
				txtFri3.Enabled = false;
				txtFri1.Text = "";
				txtFri2.Text = "";
				txtFri3.Text = "";
				DbComboPathCodeFri1.Enabled=false;
				DbComboPathCodeFri2.Enabled=false;	
				DbComboPathCodeFri3.Enabled=false;
				clearDBCombo(DbComboPathCodeFri1);
				clearDBCombo(DbComboPathCodeFri2);
				clearDBCombo(DbComboPathCodeFri3);
			}

			if(chkSat.Checked)
			{
				txtSat1.Enabled = true;
				txtSat2.Enabled = true;
				txtSat3.Enabled = true;
				DbComboPathCodeSat1.Enabled=true;
				DbComboPathCodeSat2.Enabled=true;	
				DbComboPathCodeSat3.Enabled=true;
			}
			else
			{
				txtSat1.Enabled = false;
				txtSat2.Enabled = false;
				txtSat3.Enabled = false;
				txtSat1.Text = "";
				txtSat2.Text = "";
				txtSat3.Text = "";
				DbComboPathCodeSat1.Enabled=false;
				DbComboPathCodeSat2.Enabled=false;	
				DbComboPathCodeSat3.Enabled=false;
				clearDBCombo(DbComboPathCodeSat1);
				clearDBCombo(DbComboPathCodeSat2);
				clearDBCombo(DbComboPathCodeSat3);
			}

			if(chkSun.Checked)
			{
				txtSun1.Enabled = true;
				txtSun2.Enabled = true;
				txtSun3.Enabled = true;
				DbComboPathCodeSun1.Enabled=true;
				DbComboPathCodeSun2.Enabled=true;	
				DbComboPathCodeSun3.Enabled=true;
			}
			else
			{
				txtSun1.Enabled = false;
				txtSun2.Enabled = false;
				txtSun3.Enabled = false;
				txtSun1.Text = "";
				txtSun2.Text = "";
				txtSun3.Text = "";
				DbComboPathCodeSun1.Enabled=false;
				DbComboPathCodeSun2.Enabled=false;	
				DbComboPathCodeSun3.Enabled=false;
				clearDBCombo(DbComboPathCodeSun1);
				clearDBCombo(DbComboPathCodeSun2);
				clearDBCombo(DbComboPathCodeSun3);
			}
		}


		#endregion"

		#region "Control Event"

		private void btnDisplayCustDtls_Click(object sender, System.EventArgs e)
		{		
			String sUrl = "CustomerPopup.aspx?FORMID="+"CustomerScheduledPickups" +
				"&ReferAddr1=" + txtSendAddr1.Text.Trim();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		private void btnReference_Click(object sender, System.EventArgs e)
		{
			String sUrl = "SndRecipPopup.aspx?FORMID="+"CustomerScheduledPickups"+
				"&SENDRCPTYPE=S"+
				"&SENDERNAME="+txtSendName.Text.Trim()+
				"&CUSTID="+txtCustomerCode.Text.Trim()+
				"&CUSTTYPE=C";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		private void txtMon1_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkMon.Checked == false) && (txtMon1.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtMon1.Text = "";
				return;		
			}
		}


		private void txtMon2_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkMon.Checked == false) && (txtMon2.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtMon2.Text = "";
				return;		
			}
		}


		private void txtMon3_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkMon.Checked == false) && (txtMon3.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtMon3.Text = "";
				return;		
			}
		}


		private void txtTue1_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkTue.Checked == false) && (txtTue1.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtTue1.Text = "";
				return;		
			}
		}


		private void txtTue2_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkTue.Checked == false) && (txtTue2.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtTue2.Text = "";
				return;		
			}
		}


		private void txtTue3_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkTue.Checked == false) && (txtTue3.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtTue3.Text = "";
				return;		
			}
		}


		private void txtWed1_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkWed.Checked == false) && (txtWed1.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtWed1.Text = "";
				return;		
			}
		}


		private void txtWed2_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkWed.Checked == false) && (txtWed2.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtWed2.Text = "";
				return;		
			}
		}


		private void txtWed3_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkWed.Checked == false) && (txtWed3.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtWed3.Text = "";
				return;		
			}
		}


		private void txtThu1_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkThu.Checked == false) && (txtThu1.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtThu1.Text = "";
				return;		
			}
		}


		private void txtThu2_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkThu.Checked == false) && (txtThu2.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtThu2.Text = "";
				return;		
			}
		}


		private void txtThu3_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkThu.Checked == false) && (txtThu3.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtThu3.Text = "";
				return;		
			}
		}


		private void txtFri1_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkFri.Checked == false) && (txtFri1.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtFri1.Text = "";
				return;		
			}
		}


		private void txtFri2_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkFri.Checked == false) && (txtFri2.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtFri2.Text = "";
				return;		
			}
		}


		private void txtFri3_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkFri.Checked == false) && (txtFri3.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtFri3.Text = "";
				return;		
			}
		}


		private void txtSat1_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkSat.Checked == false) && (txtSat1.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtSat1.Text = "";
				return;		
			}

			if((isEnabledSatPickup == null) || (isEnabledSatPickup == ""))
			{
				setHolidaysWeekendPickupStatus();
				if((isEnabledSatPickup.Trim() != "Y") && (txtSat1.Text.Trim() != ""))
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_SAT_DEL",utility.GetUserCulture()); 
					txtSat1.Text = "";
					return;		
				}
			}
		}


		private void txtSat2_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkSat.Checked == false) && (txtSat2.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtSat2.Text = "";
				return;		
			}

			if((isEnabledSatPickup == null) || (isEnabledSatPickup == ""))
			{
				setHolidaysWeekendPickupStatus();
				if((isEnabledSatPickup.Trim() != "Y") && (txtSat2.Text.Trim() != ""))
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_SAT_DEL",utility.GetUserCulture()); 
					txtSat2.Text = "";
					return;		
				}
			}
		}


		private void txtSat3_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkSat.Checked == false) && (txtSat3.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtSat3.Text = "";
				return;		
			}

			if((isEnabledSatPickup == null) || (isEnabledSatPickup == ""))
			{
				setHolidaysWeekendPickupStatus();
				if((isEnabledSatPickup.Trim() != "Y") && (txtSat3.Text.Trim() != ""))
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_SAT_DEL",utility.GetUserCulture()); 
					txtSat3.Text = "";
					return;		
				}
			}
		}


		private void txtSun1_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkSun.Checked == false) && (txtSun1.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtSun1.Text = "";
				return;		
			}

			if((isEnabledSunPickup == null) || (isEnabledSunPickup == ""))
			{
				setHolidaysWeekendPickupStatus();
				if((isEnabledSunPickup.Trim() != "Y") && (txtSun1.Text.Trim() != ""))
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_SUN_DEL",utility.GetUserCulture()); 
					txtSun1.Text = "";
					return;		
				}
			}
		}


		private void txtSun2_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkSun.Checked == false) && (txtSun2.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtSun2.Text = "";
				return;		
			}

			if((isEnabledSunPickup == null) || (isEnabledSunPickup == ""))
			{
				setHolidaysWeekendPickupStatus();
				if((isEnabledSunPickup.Trim() != "Y") && (txtSun2.Text.Trim() != ""))
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_SUN_DEL",utility.GetUserCulture()); 
					txtSun2.Text = "";
					return;		
				}
			}
		}


		private void txtSun3_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";

			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if((chkSun.Checked == false) && (txtSun3.Text.Trim() != ""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_CHECKED",utility.GetUserCulture()); 
				txtSun3.Text = "";
				return;		
			}

			if((isEnabledSunPickup == null) || (isEnabledSunPickup == ""))
			{
				setHolidaysWeekendPickupStatus();
				if((isEnabledSunPickup.Trim() != "Y") && (txtSun3.Text.Trim() != ""))
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CS_PLS_SUN_DEL",utility.GetUserCulture()); 
					txtSun3.Text = "";
					return;		
				}
			}
		}


		private void chkMon_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
			
			setPickupInput();
			if(chkMon.Checked==true)
			{
				this.DisplayRecords("mon");
			}
			
		}


		private void chkTue_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			setPickupInput();

			if(chkTue.Checked==true)
			{
				this.DisplayRecords("tue");
			}
		}


		private void chkWed_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			setPickupInput();

			if(chkWed.Checked==true)
			{
				this.DisplayRecords("wed");
			}
		}


		private void chkThu_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			setPickupInput();
			
			if(chkThu.Checked==true)
			{
				this.DisplayRecords("thu");
			}
		}


		private void chkFri_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			setPickupInput();
			if(chkFri.Checked==true)
			{
				this.DisplayRecords("fri");
			}
		}

		private void chkPickHolidays_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}
		}


		private void chkSat_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			setPickupInput();

			if(chkSat.Checked==true)
			{
				this.DisplayRecords("sat");
			}
		}


		private void chkSun_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			setPickupInput();
			if(chkSun.Checked==true)
			{
				this.DisplayRecords("sun");
			}
		}
		

		private void txtSendName_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if(txtSendName.Text.Trim() != "")
			{
				DataSet dsSender = CustomerScheduledPickupsDAL.GetSenderData(appID, enterpriseID,
					txtSendName.Text.Trim(), txtCustomerCode.Text.Trim(), "C");

				if(dsSender.Tables[0].Rows.Count > 0) 
				{
					txtSendAddr1.Text = dsSender.Tables[0].Rows[0]["address1"].ToString();
				}
				else
				{
					//txtSendName.Text = "";
					if (txtCustomerCode.Text.Trim() == "")
						txtSendAddr1.Text = "";

					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
					return;
				}
			}
			else
			{
				DataSet dsCustData = CustomerScheduledPickupsDAL.GetCustData(appID, enterpriseID,
					txtCustomerCode.Text.Trim());

				if(dsCustData.Tables[0].Rows.Count > 0) 
				{
					txtCustomerName.Text = dsCustData.Tables[0].Rows[0]["cust_name"].ToString();
					if (txtSendName.Text.Trim() == "")
						txtSendAddr1.Text = dsCustData.Tables[0].Rows[0]["address1"].ToString();
				}
				else
				{
					txtCustomerCode.Text = "";
					txtCustomerName.Text = "";
					if (txtSendName.Text.Trim() == "")
						txtSendAddr1.Text = "";

					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
					return;
				}
			}
		}


		private void txtCustomerCode_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if((bool)ViewState["isTextChanged"] == false)
			{
				ChangeDSState();
				ViewState["isTextChanged"] = true;
			}

			if(txtCustomerCode.Text.Trim() != "")
			{
				DataSet dsCustData = CustomerScheduledPickupsDAL.GetCustData(appID, enterpriseID,
					txtCustomerCode.Text.Trim());

				if(dsCustData.Tables[0].Rows.Count > 0) 
				{
					txtCustomerName.Text = dsCustData.Tables[0].Rows[0]["cust_name"].ToString();
					if (txtSendName.Text.Trim() == "")
						txtSendAddr1.Text = dsCustData.Tables[0].Rows[0]["address1"].ToString();
				}
				else
				{
					txtCustomerCode.Text = "";
					txtCustomerName.Text = "";
					if (txtSendName.Text.Trim() == "")
						txtSendAddr1.Text = "";

					lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
					return;
				}
			}
			else
			{
				txtCustomerName.Text = "";
				if (txtSendName.Text.Trim() == "")
					txtSendAddr1.Text = "";
			}
		}


		private void txtCustomerName_TextChanged(object sender, System.EventArgs e)
		{
		
		}


		#region "Main button of function"

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			Qry_Click();
			//Ryuji Add set up focus() on customerCode 
			Utility ut = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			ut.SetInitialFocus(txtCustomerCode);
		}


		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			btnExecQry.Enabled=false;
			ExecQry_Click();
			setPickupInput();

			//Ryuji Add set up focus() on customerCode 
			Utility ut = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			ut.SetInitialFocus(txtCustomerCode);
				
		}


		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			Insert_Click();
		}


		private void btnSave_Click(object sender, System.EventArgs e)
		{
			bool reState = SaveUpdateRecord();
			if (reState == true)
				Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save, false, m_moduleAccessRights);

			ViewState["fromInsert"] = true;
		}
	

		private void btnDelete_Click(object sender, System.EventArgs e)
		{
			trMain.Visible = false;
			trDelete.Visible = true;
		}


		#endregion

		#region "Navigation button of function"

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			MoveFirstDS();
			setPickupInput();
		}


		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			MovePreviousDS();
			setPickupInput();
		}
		

		private void btnNextPage_Click(object sender, System.EventArgs e)
		{
			MoveNextDS();
			setPickupInput();
		}


		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			MoveLastDS();
			setPickupInput();
		}


		#endregion

		#region "Button of comfirmation"

		private void btnToCancel_Click(object sender, System.EventArgs e)
		{
			trMain.Visible = true;
			trConfirm.Visible=false;
		}


		private void btnNotToSave_Click(object sender, System.EventArgs e)
		{

			trMain.Visible = true;
			trConfirm.Visible=false;
			ViewState["CSPOperation"] = Operation.None;
			ViewState["isTextChanged"] = false;

			if((String)ViewState["NextOperation"] == "Insert")
			{
				Insert_Click();
			}
			else if((String)ViewState["NextOperation"] == "ExecuteQuery")
			{
				ExecQry_Click();
			}
			else if((String)ViewState["NextOperation"] == "Query")
			{
				Qry_Click();
			}
			else if((String)ViewState["NextOperation"] == "MoveFirst")
			{
				MoveFirstDS();
			}
			else if((String)ViewState["NextOperation"] == "MoveNext")
			{
				MoveNextDS();
			}
			else if((String)ViewState["NextOperation"] == "MoveLast")
			{
				MoveLastDS();
			}
			else if((String)ViewState["NextOperation"] == "MovePrevious")
			{
				MovePreviousDS();
			}
		}


		private void btnToSaveChanges_Click(object sender, System.EventArgs e)
		{
			trMain.Visible = true;
			trConfirm.Visible=false;

			bool isError = SaveUpdateRecord();

			ViewState["isTextChanged"] = false;
			if(isError == false)
			{
				if((String)ViewState["NextOperation"] == "Insert")
				{
					Insert_Click();
				}
				else if((String)ViewState["NextOperation"] == "ExecuteQuery")
				{
					ExecQry_Click();
				}
				else if((String)ViewState["NextOperation"] == "Query")
				{
					Qry_Click();
				}
				else if((String)ViewState["NextOperation"] == "MoveFirst")
				{
					MoveFirstDS();
				}
				else if((String)ViewState["NextOperation"] == "MoveNext")
				{
					MoveNextDS();
				}
				else if((String)ViewState["NextOperation"] == "MoveLast")
				{
					MoveLastDS();
				}
				else if((String)ViewState["NextOperation"] == "MovePrevious")
				{
					MovePreviousDS();
				}
			}
		}


		private void btnComfirmDel_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			if (((int)ViewState["CSPOperation"] == (int)Operation.Insert) || ((int)ViewState["CSPOperation"] == (int)Operation.Update))
			{
				lblConfirmation.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				trMain.Visible = false;
				trConfirm.Visible=true;
				return;
			}

			DataRow drEach = m_sdsCustomerSchedPickups.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];

			int iRowDeleted = CustomerScheduledPickupsDAL.DeleteCustomerSchedPickups(appID, enterpriseID,
				drEach["custid"].ToString().Trim(), drEach["cust_ref_name"].ToString().Trim());
			lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
			trMain.Visible = true;
			trDelete.Visible = false;
			return;
		}


		private void btnNoConfirmDel_Click(object sender, System.EventArgs e)
		{
			trMain.Visible = true;
			trDelete.Visible = false;
		}


		#endregion


		#endregion

	}
}