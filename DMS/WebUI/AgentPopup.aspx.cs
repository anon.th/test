using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using com.common.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;
namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for AgentPopup.
	/// </summary>
	public class AgentPopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnOk;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Label lblAgentName;
		protected System.Web.UI.WebControls.Label lblAgentID;
		private String appID = null;
		private String enterpriseID = null;
		SessionDS m_sdsAgent =null;
		protected System.Web.UI.WebControls.DataGrid dgAgent;
		protected System.Web.UI.WebControls.Button btnRefresh;
		//Utility utility = null;
		string strFORMID=null;
		protected System.Web.UI.WebControls.TextBox txtAgentName;
		protected System.Web.UI.WebControls.TextBox txtAgentId;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			
			string strCustomer = Request.Params["CustID"];
			strFORMID= Request.Params["FORMID"]; 
			if (!IsPostBack)
			{
				m_sdsAgent = GetEmptyAgent(0); 
				txtAgentId.Text = Request.Params["CUSTID_TEXT"];
				txtAgentName.Text = Request.Params["CUSTNAME_TEXT"];
				BindGrid();
			}
			else
			{
				m_sdsAgent = (SessionDS)ViewState["AGENT_DS"];
			}
			
			//CustErrMsg.Text="";
		}
		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			dgAgent.CurrentPageIndex = e.NewPageIndex;	
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.dgAgent.SelectedIndexChanged += new System.EventHandler(this.dgAgent_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select * from Agent where applicationid='");
			strQry.Append(appID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("'");
			if (txtAgentId.Text.ToString() != null && txtAgentId.Text.ToString() != "")
			{
				strQry.Append(" and agentid like '%");
				strQry.Append(Utility.ReplaceSingleQuote(txtAgentId.Text.ToString()));
				strQry.Append("%'");
			}
			if (txtAgentName.Text.ToString() != null && txtAgentName.Text.ToString() != "")
			{
				strQry.Append(" and agent_name like N'%");
				strQry.Append(Utility.ReplaceSingleQuote(txtAgentName.Text.ToString()));
				strQry.Append("%'");
			}

			String strSQLQuery = strQry.ToString();
			ViewState["SQL_QUERY"] = strSQLQuery;
			dgAgent.CurrentPageIndex = 0;
			ShowCurrentPage();
			
		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
//			sScript += "  window.opener."+strFORMID+".txtCustID.value = '';" ;
//			sScript += "  window.opener."+strFORMID+".txtCustName.value = '';" ;
//			sScript += "  window.opener."+strFORMID+".txtCustAdd1.value = '';" ;
//			sScript += "  window.opener."+strFORMID+".txtCustAdd2.value = '';" ;
//			sScript += "  window.opener."+strFORMID+".txtCustZipCode.value = '';" ;
//			sScript += "  window.opener."+strFORMID+".txtCustStateCode.value = '';" ;
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
		public static SessionDS GetEmptyAgent(int intRows)
		{
			DataTable dtAgent = new DataTable();
			dtAgent.Columns.Add(new DataColumn("agentid", typeof(string)));
			dtAgent.Columns.Add(new DataColumn("agent_name", typeof(string)));
			dtAgent.Columns.Add(new DataColumn("agent_address1", typeof(string)));
			dtAgent.Columns.Add(new DataColumn("agent_address2", typeof(string)));
			dtAgent.Columns.Add(new DataColumn("zipcode", typeof(string)));
			for(int i=0; i < intRows; i++)
			{
				DataRow drEach = dtAgent.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				dtAgent.Rows.Add(drEach);
			}
			DataSet dsAgentIdFields = new DataSet();
			dsAgentIdFields.Tables.Add(dtAgent);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsAgentIdFields;
			sessionDS.DataSetRecSize = dsAgentIdFields.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return sessionDS;
		}

		private void dgAgent_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int iSelIndex = dgAgent.SelectedIndex;
			DataGridItem dgRow = dgAgent.Items[iSelIndex];
			String strCustID = dgRow.Cells[0].Text;
			String strCustName = dgRow.Cells[1].Text;
			String strAddress1 = dgRow.Cells[2].Text;
			String strAddress2 = dgRow.Cells[3].Text;
			String strZipCode = dgRow.Cells[4].Text;
			String strCountry = dgRow.Cells[5].Text;
			String strPODSlipReqrd = dgRow.Cells[6].Text;
			String strTelephone = dgRow.Cells[7].Text;
			String strFax = dgRow.Cells[8].Text;

			//Get the Country
			if(strCountry.Length == 0)
			{
				Enterprise enterprise = new Enterprise();
				enterprise.Populate(appID,enterpriseID);
				strCountry = enterprise.Country;
			}

			//Get the state & display...
			Zipcode zipCode = new Zipcode();
			zipCode.Populate(appID,enterpriseID,strCountry,strZipCode);
			String strState = zipCode.StateName;
			String strRouteCode = zipCode.DeliveryRoute;

			String strIsChecked = "false";
			if(strPODSlipReqrd == "Y")
			{
				strIsChecked = "true";
			}

			if(strCustName == "&nbsp;")
			{
				strCustName = "";
			}
			if(strAddress1 == "&nbsp;")
			{
				strAddress1 = "";
			}
			if(strAddress2 == "&nbsp;")
			{
				strAddress2 = "";
			}

			if(strTelephone == "&nbsp;")
			{
				strTelephone = "";
			}
			if(strFax == "&nbsp;")
			{
				strFax = "";
			}

			String sScript = "";
			sScript += "<script language=javascript>";
				
			if(strFORMID.Equals("DomesticShipment"))
			{
				sScript += "  window.opener.DomesticShipment.txtCustID.value = '" + strCustID + "';";
				sScript += "  window.opener.DomesticShipment.txtCustName.value = '" + strCustName + "';";
				sScript += "  window.opener.DomesticShipment.txtCustAdd1.value = '" + strAddress1 + "';";
				sScript += "  window.opener.DomesticShipment.txtCustAdd2.value = '" + strAddress2 + "';";
				sScript += "  window.opener.DomesticShipment.txtCustZipCode.value = '" + strZipCode + "';";
				sScript += "  window.opener.DomesticShipment.txtCustStateCode.value = '" + strCountry + "';";
				sScript += "  window.opener.DomesticShipment.txtCustCity.value = '" + strState + "';";
				sScript += "  window.opener.DomesticShipment.txtRouteCode.value = '" + strRouteCode + "';";
				sScript += "  window.opener.DomesticShipment.chkshpRtnHrdCpy.checked = " + strIsChecked + ";";
				sScript += "  window.opener.DomesticShipment.txtCustTelephone.value = '" + strTelephone + "';";
				sScript += "  window.opener.DomesticShipment.txtCustFax.value = '" + strFax + "';";
			}
			else if(strFORMID.Equals("BandQuotation"))
			{
				sScript += "  window.opener."+strFORMID+".txtCustID.value = '" + strCustID + "';";
				sScript += "  window.opener."+strFORMID+".txtCustName.value = '" + strCustName + "';";
			}
			else if (strFORMID.Equals("ShipmentTrackingQuery"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = '" + strCustID + "';";
			}
			else if (strFORMID.Equals("ShipmentTrackingQueryRpt"))
			{
				sScript += "window.opener."+strFORMID+".txtPayerCode.value = '" + strCustID + "';";
			}
			else if (strFORMID.Equals("InvoiceGeneration"))
			{
				sScript += "window.opener."+strFORMID+".txtCustomerCode.value = '" +strCustID +"';";
				sScript += "window.opener."+strFORMID+".txtCustomerName.value = '" +strCustName +"';";
			}
			else if (strFORMID.Equals("InvoicePrintingQuery"))
			{
				sScript += "window.opener."+strFORMID+".txtCustomerCode.value = '" +strCustID +"';";
				sScript += "window.opener."+strFORMID+".txtCustomerName.value = '" +strCustName +"';";
			}
			else if (strFORMID.Equals("AgentZone"))
			{
				sScript += "window.opener."+strFORMID+".txtAgentID.value = '" +strCustID +"';";
				sScript += "window.opener."+strFORMID+".txtAgentName.value = '" +strCustName +"';";
			}
			else
			{
				sScript += "  window.opener."+strFORMID+".txtCustID.value = '" + strCustID + "';";
				sScript += "  window.opener."+strFORMID+".txtCustName.value = '" + strCustName + "';";
				sScript += "  window.opener."+strFORMID+".txtCustAddr1.value = '" + strAddress1 + "';";
				sScript += "  window.opener."+strFORMID+".txtCustAdd2.value = '" + strAddress2 + "';";
				sScript += "  window.opener."+strFORMID+".txtCustZipCode.value = '" + strZipCode + "';";
				sScript += "  window.opener."+strFORMID+".Txt_Country.value = '" + strCountry + "';";
				sScript += "  window.opener."+strFORMID+".Txt_StateCode.value = '" + strState + "';";
				sScript += "  window.opener."+strFORMID+".Txt_Telephone.value = '" + strTelephone + "';";
				sScript += "  window.opener."+strFORMID+".Txt_Fax.value = '" + strFax + "';";
			}
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
			Logger.LogDebugInfo("AgentDetail.aspx.cs","IndexChangeof girid","ECD001","");
		}
		
		private void BindGrid()
		{
			dgAgent.VirtualItemCount = System.Convert.ToInt32(m_sdsAgent.QueryResultMaxSize);
			dgAgent.DataSource = m_sdsAgent.ds;
			dgAgent.DataBind();
			ViewState["CUST_DS"] = m_sdsAgent;
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgAgent.CurrentPageIndex * dgAgent.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsAgent = GetAGENTDDS(iStartIndex,dgAgent.PageSize,sqlQuery);
			decimal pgCnt = (m_sdsAgent.QueryResultMaxSize - 1)/dgAgent.PageSize;
			if(pgCnt < dgAgent.CurrentPageIndex)
			{
				dgAgent.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgAgent.SelectedIndex = -1;
			dgAgent.EditItemIndex = -1;

			BindGrid();
			getPageControls(Page);
		}

		private SessionDS GetAGENTDDS(int iCurrent, int iDSRecSize, String strQuery)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("BandIdPopup.aspx.cs","GetBANDIDDS","BD001","DbConnection object is null!!");
				return sessionDS;
			}
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"ZoneCode");
			return  sessionDS;
		}
		
			
	}
}
