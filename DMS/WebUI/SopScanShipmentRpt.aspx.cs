using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;
using Cambro.Web.DbCombo;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for ShipmentTrackingQuery.
	/// </summary>
	public class SopScanShipmentRpt : BasePage
	{
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblConsignmentNo;
		protected System.Web.UI.WebControls.TextBox txtConsignmentNo;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentTracking;
		protected System.Web.UI.WebControls.Label lblZipCode;
		protected com.common.util.msTextBox txtZipCode;
		protected System.Web.UI.WebControls.Button btnZipCode;
		protected System.Web.UI.WebControls.Label lblBookingType;
		//Utility utility = null;
		String m_strAppID=null;
		String m_strEnterpriseID=null;
		String m_strCulture=null;
		protected System.Web.UI.WebControls.TextBox txtPeriod2;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.RadioButton rbEstDate;
		protected System.Web.UI.WebControls.DropDownList ddlPayerType;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCode;
		protected Cambro.Web.DbCombo.DbCombo DbComboOriginDC;
		protected Cambro.Web.DbCombo.DbCombo DbComboDestinationDC;
		protected System.Web.UI.WebControls.TextBox txtBookingType;
		protected System.Web.UI.WebControls.Button btnCancelQry;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label lblShipment;
		protected Cambro.Web.DbCombo.DbCombo DbComboServiceType;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.TextBox txtUserID;
		protected System.Web.UI.WebControls.CheckBox cbMissCon;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.Label Label4;
		protected com.common.util.msTextBox txtDateTo;
		protected System.Web.UI.WebControls.RadioButton rbLongRoute;
		protected System.Web.UI.WebControls.RadioButton rbShortRoute;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipment;
		//private DataView m_dvMonths;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			DbComboServiceType.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			if(!Page.IsPostBack)
			{
				DefaultScreen();
				Session["toRefresh"]=false;
			}
			SetDbComboServerStates();
		}

		private void SetDbComboServerStates()
		{
			String strDeliveryType=null;
			if (rbLongRoute.Checked==true)
				strDeliveryType="L";
			else if (rbShortRoute.Checked==true)
				strDeliveryType="S";
			//else if (rbAirRoute.Checked==true)
			//	strDeliveryType="A";

			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
			DbComboPathCode.ServerState = hash;
			DbComboPathCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"'";
					if(strDelType == "S") 
					{
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct delivery_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
				}				
			}
			
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.rbLongRoute.CheckedChanged += new System.EventHandler(this.rbLongRoute_CheckedChanged);
			this.rbShortRoute.CheckedChanged += new System.EventHandler(this.rbShortRoute_CheckedChanged);
			this.btnZipCode.Click += new System.EventHandler(this.btnZipCode_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";

			DataSet dsSopScan = new DataSet();
			DataTable dtSopScan = CreateEmptyDataTable();
			DataRow dr = dtSopScan.NewRow();

			
			//@consignment_no 		varchar(30) = null,
			//	@user_id				varchar(30) = null,
			//	@service_type			varchar(5) = null,
			//	@delivery_route_code	varchar(12) = null,
			//	@destination_zipcode	varchar(5) = null,
			//	@scanning_datetime		varchar(100) = null

			if(txtConsignmentNo.Text.Trim() != "")
			{
				dr["consignment_no"] = txtConsignmentNo.Text.Trim();
			}

			if(txtUserID.Text.Trim() != "")
			{
				dr["user_id"] =txtUserID.Text.Trim();
			}
			if(DbComboServiceType.Value != "")
			{
				dr["service_type"] = DbComboServiceType.Value;
			}
			if(DbComboPathCode.Value != "")
			{
				if (rbLongRoute.Checked==true)
					dr["line_haul"] =DbComboPathCode.Value;
				else if (rbShortRoute.Checked==true)
					dr["delivery_route_code"] =DbComboPathCode.Value;
				
			}

			if(txtZipCode.Text.Trim() != "")
			{
				dr["destination_zipcode"] = txtZipCode.Text.Trim();
			}
			if(txtDate.Text != "")
			{
				string fromdate = txtDate.Text.Trim();
				string todate = txtDateTo.Text.Trim();

				dr["scanning_datetime"] =fromdate.Substring(6,4) + "-" + fromdate.Substring(3,2) + "-" + fromdate.Substring(0,2)
					+ ";" + todate.Substring(6,4) + "-" + todate.Substring(3,2) + "-" + todate.Substring(0,2);
			}
			if(cbMissCon.Checked)
			{
				dr["missconsignment"] = "1";
			}


			dtSopScan.Rows.Add(dr);
			dsSopScan.Tables.Add(dtSopScan);
			String strUrl = null;
			strUrl = "ReportViewerDataSet.aspx";
			Session["FORMID"] = "sopscanreport";
			Session["SESSION_DS1"] = dsSopScan;
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		
		}
		private DataTable CreateEmptyDataTable()
		{

			//@consignment_no 		varchar(30) = null,
			//	@user_id				varchar(30) = null,
			//	@service_type			varchar(5) = null,
			//	@delivery_route_code	varchar(12) = null,
			//	@destination_zipcode	varchar(5) = null,
			//	@scanning_datetime		varchar(100) = null
			DataTable dtShipment = new DataTable();
			dtShipment.Columns.Add(new DataColumn("consignment_no", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("user_id", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("service_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("delivery_route_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("destination_zipcode", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("scanning_datetime", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("missconsignment", typeof(string)));		
			dtShipment.Columns.Add(new DataColumn("line_haul", typeof(string)));	

			return dtShipment;
		}

		private void OpenWindowpage(String strUrl, String strFeatures)
		{
			
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			//btnExecuteQuery.Enabled = true;
			DefaultScreen();
		}

		private int ValidateData()
		{

			return 1;
		}
		
		private void DefaultScreen()
		{
			txtConsignmentNo.Text = "";
			txtDate.Text = "";
			txtZipCode.Text = "";
			lblErrorMessage.Text = "";
			//Jeab 3 Feb 11
			txtDateTo.Text=""; 
			txtUserID.Text="";  
			DbComboPathCode.Text="";  
			DbComboPathCode.Value=""; 
			DbComboServiceType.Text="";
			DbComboServiceType.Value="";
			rbLongRoute.Checked=true;
			rbShortRoute.Checked=false;
			//rbAirRoute.Checked=false;
			cbMissCon.Checked=false;
			//Jeab 3 Feb 11  =========> End
		}
		private void btnZipCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "ZipCodePopup.aspx?FORMID=ShipmentTrackingQuery"+"&ZIPCODE_CID="+txtZipCode.ClientID+"&ZIPCODE="+txtZipCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboRouteCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"'";
					if(strDelType == "S") 
					{
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct delivery_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
				}				
			}
			
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}

		


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object ServiceCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strCommodityCode="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strCommodityCode"] != null && args.ServerState["strCommodityCode"].ToString().Length > 0)
				{
					strCommodityCode = args.ServerState["strCommodityCode"].ToString();					
				}
			}	
			DataSet dataset = com.ties.classes.DbComboDAL.ServiceCodeQuery (strAppID,strEnterpriseID,args);	
			return dataset;                
		}

		private void rbShortRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";  
			DbComboPathCode.Value=""; 
		}

		private void rbLongRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";  
			DbComboPathCode.Value=""; 
		}

	}
}
