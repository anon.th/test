using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;
namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for DeleteHistoryConfirmPopup.
	/// </summary>
	public class DeleteHistoryConfirmPopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnYes;
		protected System.Web.UI.WebControls.Button btnNo;
		protected System.Web.UI.WebControls.TextBox txtDeleteRemark;
		protected System.Web.UI.WebControls.Label lblValRemark;
		protected System.Web.UI.WebControls.Label lblConfirmQ;
		private String m_strAppID;
		protected System.Web.UI.WebControls.Label lblReason;
		private String m_strEnterpriseID;
		protected System.Web.UI.WebControls.DropDownList DropDownList1;
		private String userID = null;
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			//Gwang on 15Feb08
			userID = utility.GetUserID();
			lblConfirmQ.Text = "Are you sure you want to delete this history?";
			lblReason.Text = "Enter Reason";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
			this.btnNo.Click += new System.EventHandler(this.btnNo_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		/// <summary>
		/// upon clicking yes to update changes to database
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnYes_Click(object sender, System.EventArgs e)
		{
			if(txtDeleteRemark.Text!="")
			{
				UpdateHistory();
				RefreshAndCloseWindow();
			}
			else
			{
				lblValRemark.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_RSN_DEL",utility.GetUserCulture());
			}
			
		}
		/// <summary>
		/// to close current window
		/// </summary>
		private void closeCurrentWindow()
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
		private void RefreshAndCloseWindow()
		{
			Session["toRefresh"] = true;
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "var sURL = unescape(window.opener.location.pathname); ";
			sScript += "  window.opener.location.replace(sURL);" ;
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
		/// <summary>
		/// to update delete history by updating to database
		/// </summary>
		/// Please DON'T DELETE THE methods, RevertStatus, UpdateDeleteHistroy(2 Nos) and UpdateShipmentPODStatus
		private void UpdateHistory()
		{
			String strFormId = Request.Params["FORM_ID"];
			String strConsignmentNo = Request.Params["CONSIGNMENT"];
			if (strFormId == "DispatchRecordDeletion.aspx")
			{
				SessionDS m_sdsDispatchDelete = (SessionDS)Session["SESSION_DS2"];
				String strBookDateTime = Request.Params["BOOKDATE"];
				String strStatusDateTime = Request.Params["STATUSDATE"];
				String strNum = Request.Params["ROWNUM"];
				
				DataRow drBook = m_sdsDispatchDelete.ds.Tables[0].Rows[int.Parse(strNum)+1];
				String strBookNum = drBook["booking_no"].ToString();
				DateTime dt = Convert.ToDateTime(drBook["tracking_datetime"]);
				String strTrackDate = Utility.DateFormat(m_strAppID,m_strEnterpriseID,dt,DTFormat.DateTime);
				String strStatusCheck = drBook["status_code"].ToString();
				if(strBookDateTime == strStatusDateTime)
				{
					//Gets the row above the latest with the previous latest Status 
					//and Exception and update it to database
					DataRow dr = m_sdsDispatchDelete.ds.Tables[0].Rows[int.Parse(strNum)];
					String strBookNo = dr["booking_no"].ToString();
					String strStatus = dr["status_code"].ToString();
					String strException = dr["exception_code"].ToString();
					String strLatestDate = dr["tracking_datetime"].ToString();
					DispatchTrackingMgrDAL.UpdateDeleteHistory(m_strAppID, m_strEnterpriseID, strBookNo, strException, strStatus, strLatestDate);
				}
				DispatchTrackingMgrDAL.UpdateDeleteHistory(m_strAppID, m_strEnterpriseID,strBookNum,strTrackDate,true,txtDeleteRemark.Text,strStatusCheck);
			}
			else if (strFormId == "ShipmentStatusDelete.aspx")
			{
				SessionDS m_sdsShipmentDelete = (SessionDS)Session["SESSION_DS2"];
				String strBookDateTime = Request.Params["BOOKDATE"];
				String strStatusDateTime = Request.Params["STATUSDATE"];
				String strNum = Request.Params["ROWNUM"];
				String strStatusCode=Request.Params["STATUSCODE"];
				//Gwang on 15Feb08
				String strUser = userID;
				
				DataRow drBook_tb1 = m_sdsShipmentDelete.ds.Tables[0].Rows[0];
				DataRow drBook_tb2 = m_sdsShipmentDelete.ds.Tables[1].Rows[int.Parse(strNum)+1];
				String strBookNum = drBook_tb1["booking_no"].ToString();
				int intBookNum = Convert.ToInt32(strBookNum);
				
				
				DateTime dtTrackDate = Convert.ToDateTime(drBook_tb2["tracking_datetime2"]);											
				String strTrackDate = Utility.DateFormat(m_strAppID, m_strEnterpriseID, dtTrackDate, DTFormat.DateVeryLongTime);

				//String strTrackDate = Utility.DateFormat(m_strAppID, m_strEnterpriseID, dtTrackDate, DTFormat.DateLongTime);

				//Gwang on 15Feb08
				//DataSet dsGetLastDate = ShipmentTrackingMgrDAL.GetLatestDateFromShipment(m_strAppID, m_strEnterpriseID, intBookNum);
				//DataRow drLastDate = dsGetLastDate.Tables[0].Rows[0];
				//DateTime dtLastDate=new DateTime(1,1,1);
				//				if(!drLastDate["last_status_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))
				//				{
				//					dtLastDate =Convert.ToDateTime(drLastDate["last_status_datetime"]);
				//				}
				DateTime dtLastDate = DateTime.Now;
				String[] strPODStatusCode =TIESUtility.getPODStatus(m_strAppID, m_strEnterpriseID);
				
				String strPODStatusCodes=null;
				String strPODs=null;
				for(int i=0;i<strPODStatusCode.Length;i++)
				{
					if (i==0 || i==(strPODStatusCode.Length -1))
					{
						strPODStatusCodes+="'"+strPODStatusCode[i]+"'";
						strPODs+=strPODStatusCode[i];
					}
					else
					{
						strPODStatusCodes+="'"+strPODStatusCode[i]+"',";
						strPODs+=strPODStatusCode[i]+",";
					}
				}
				
				String strActDelvryDate=null;
				strActDelvryDate=ShipmentTrackingMgrDAL.GetMinTrackingDate(m_strAppID,m_strEnterpriseID,strBookNum, strTrackDate,strPODStatusCodes,strConsignmentNo);
				//Gwang on 15Feb08
				//int iRows=ShipmentTrackingMgrDAL.UpdateShipmentStatus(m_strAppID,m_strEnterpriseID, strBookNum, dtLastDate, dtTrackDate, true,txtDeleteRemark.Text,strStatusCode,strPODs,strActDelvryDate, strConsignmentNo);
				int iRows= ShipmentTrackingMgrDAL.UpdateShipmentStatus(m_strAppID,m_strEnterpriseID, strBookNum, dtLastDate, dtTrackDate, true,txtDeleteRemark.Text,strStatusCode,strPODs,strActDelvryDate, strConsignmentNo,strUser);
				
								
				//				if(dtTrackDate >= dtLastDate)
				//				{
				//Gets the row above the latest with the previous latest Status and Exception and update it to database										
				//					String strLastDate = Utility.DateFormat(m_strAppID, m_strEnterpriseID, dtLastDate, DTFormat.DateTime);					
				//					ShipmentTrackingMgrDAL.RevertStatus(m_strAppID,m_strEnterpriseID,strBookNum,strLastDate);
				//				}
				//				ShipmentTrackingMgrDAL.UpdateDeleteHistory(m_strAppID, m_strEnterpriseID, strBookNum, strTrackDate, true, txtDeleteRemark.Text);				
				//	ShipmentTrackingMgrDAL.UpdateShipmentPODStatus(m_strAppID, m_strEnterpriseID, strBookNum, strTrackDate);

				//Added By Tom 16/12/2009
				iRows = ShipmentTrackingMgrDAL.UpdateDomesticShipment(m_strAppID,m_strEnterpriseID, strConsignmentNo);
				//End Added By Tom 16/12/2009
			}
		}

		/// <summary>
		/// click no will close current window
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnNo_Click(object sender, System.EventArgs e)
		{
			closeCurrentWindow();
		}
	}
}
