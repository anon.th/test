using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for StatePopup.
	/// </summary>
	public class StatePopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.DataGrid dgStateMaster;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Button btnOk;
		protected System.Web.UI.WebControls.TextBox txtStateCode;
		protected System.Web.UI.WebControls.Label lblStateCode;
		//Utility utility = null;
		String strAppID = "";
		String strEnterpriseID = "";
		protected System.Web.UI.WebControls.Label lblCountry;
		protected System.Web.UI.WebControls.TextBox txtCountry;
		private String strCountryID = null;
		private String strStateID = null;
		private String strFormID = null;
		SessionDS m_sdsStateCode = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();

			strCountryID = Request.Params["COUNTRY"];
			strStateID = Request.Params["STATECODE"];
			strFormID = Request.Params["FORMID"];

			if(!Page.IsPostBack)
			{
				txtCountry.Text = Request.Params["COUNTRY_TEXT"];
				txtStateCode.Text = Request.Params["STATECODE_TEXT"];
				m_sdsStateCode = GetEmptyStateCodeDS(0);
				btnSearch_Click(null,null);
				BindGrid();
			}
			else
			{
				m_sdsStateCode = (SessionDS)ViewState["STATECODE_DS"];
			}
		}

		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			//runs when one of the page control is clicked
			//update the current page from the parameter values
			//dgStateMaster.CurrentPageIndex = e.NewPageIndex;
			//bind data to the grid
			//RefreshData();
			dgStateMaster.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentPage();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select country, state_code, state_name from state where applicationid='");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("'");
			if (txtCountry.Text.ToString() != null && txtCountry.Text.ToString() != "")
			{
				strQry.Append(" and country like '%");
				strQry.Append(Utility.ReplaceSingleQuote(txtCountry.Text.ToString()));
				strQry.Append("%'");
			}
			if (txtStateCode.Text.ToString() != null && txtStateCode.Text.ToString() != "")
			{
				strQry.Append(" and state_code like '%");
				strQry.Append(Utility.ReplaceSingleQuote(txtStateCode.Text.ToString()));
				strQry.Append("%'");
			}

			String strSQLQuery = strQry.ToString();
			//RefreshData();
			ViewState["SQL_QUERY"] = strSQLQuery;
			dgStateMaster.CurrentPageIndex = 0;

			ShowCurrentPage();
		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		public void dgStateMaster_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			int iSelIndex = dgStateMaster.SelectedIndex;
			DataGridItem dgRow = dgStateMaster.Items[iSelIndex];
			String strCountry = dgRow.Cells[0].Text;
			String strStateCode = dgRow.Cells[1].Text;
			
			if(strFormID == "EnterpriseProfile")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strCountryID+".value = \""+strCountry+"\";";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(strFormID == "ShipmentTrackingQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(strFormID == "ShipmentTrackingQuery_New")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(strFormID == "ShipmentTrackingQueryRpt")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(strFormID == "ShipmentExceptionReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(strFormID == "ShipmentPendingPODReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(strFormID == "ServiceFailure")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(strFormID == "RevenueCostReportQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(strFormID == "PODReportQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(strFormID == "ServiceQualityIndicatorQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			//Jeab 14 Jun 2011
			else if(strFormID == "PODEXRptQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			//Jeab 14 Jun 2011  =========> End
			//Jeab 17 Jun 2011
			else if(strFormID == "RevenueBySalesIDRptQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			//Jeab 17 Jun 2011  =========> End

			else if(strFormID == "SalesTerritoryReportQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(strFormID == "AverageDeliveryTimeReportQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			//HC Return Task
			else if(strFormID == "PendingHCReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			//HC Return Task
			//HC Return Task
			else if(strFormID == "ServiceExceptionHCReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			//HC Return Task
			else if(strFormID == "MarginAnalysisReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(strFormID == "RevenueByStateReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(strFormID == "TopNReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(strFormID == "AvgDeliveryTimeReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			// Mohan, 25/12/2002 
			else if(strFormID == "PublicHoliday")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strCountryID+".value = \""+strCountry+"\";";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			// by Ching Nov 30,2007
			else if(strFormID == "ShipmentPendingCOD")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(strFormID == "CODAmountCollectedReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(strFormID == "CODRemittanceReport")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}	
			//PSA# 2009-014
			else if(strFormID == "TotalAverageDeliveryTimeReportQuery")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener."+strFormID+"."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			//PSA# 2009-014
			// End
			else
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener.ZipCode."+strCountryID+".value = \""+strCountry+"\";";
				sScript += "  window.opener.ZipCode."+strStateID+".value = \""+strStateCode+"\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			
		}

		public static SessionDS GetEmptyStateCodeDS(int intRows)
		{
			DataTable dtState = new DataTable();
			dtState.Columns.Add(new DataColumn("country", typeof(string)));
			dtState.Columns.Add(new DataColumn("state_code", typeof(string)));
			dtState.Columns.Add(new DataColumn("state_name", typeof(string)));

			for(int i=0; i < intRows; i++)
			{
				DataRow drEach = dtState.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";

				dtState.Rows.Add(drEach);
			}

			DataSet dsStateFields = new DataSet();
			dsStateFields.Tables.Add(dtState);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsStateFields;
			sessionDS.DataSetRecSize = dsStateFields.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return sessionDS;
		}

		private void BindGrid()
		{
			dgStateMaster.VirtualItemCount = System.Convert.ToInt32(m_sdsStateCode.QueryResultMaxSize);
			dgStateMaster.DataSource = m_sdsStateCode.ds;
			dgStateMaster.DataBind();
			ViewState["STATECODE_DS"] = m_sdsStateCode;

//			if(dgStateMaster.Items.Count == 0)
//				btnSearch_Click(null,null);
			//dgStateMaster.DataSource = dsState;
			//dgStateMaster.DataBind();
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgStateMaster.CurrentPageIndex * dgStateMaster.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsStateCode = GetStateCodeDS(iStartIndex,dgStateMaster.PageSize,sqlQuery);
			decimal pgCnt = (m_sdsStateCode.QueryResultMaxSize - 1)/dgStateMaster.PageSize;
			if(pgCnt < dgStateMaster.CurrentPageIndex)
			{
				dgStateMaster.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgStateMaster.SelectedIndex = -1;
			dgStateMaster.EditItemIndex = -1;

			BindGrid();
			getPageControls(Page);
		}

		private SessionDS GetStateCodeDS(int iCurrent, int iDSRecSize, String strQuery)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("StateCodePopup.aspx.cs","GetStateCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"CountryStateCode");
			return  sessionDS;
		}

	}
}
