using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.common.util;

namespace com.ties
{
	
	/// <summary>
	/// In-line code documentation here...
	/// </summary>

	public enum ScreenMode
	{
		Query, ExecuteQuery, Insert, None
	}

	public enum Operation
	{
		Insert,Update,Delete,Cancel,None,Saved
	}

	public struct ServiceAvailable
	{
		public bool isServiceAvail;
		public String strVASCode;
	}

	public class MainFrame : System.Web.UI.Page
	{
		protected String strWelcomePagePath = null;
		protected String strStyleSheet = null;

		private CoreEnterprise enterprise;
		private Utility utility;

		private void Page_Load(object sender, System.EventArgs e)
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);

			if(!Page.IsPostBack)
			{
				Response.CacheControl = "no-cache";
				Response.AddHeader("Pragma","no-cache");
				Response.Expires = -1;

				enterprise = new CoreEnterprise();
				enterprise.EnterpriseID = utility.GetEnterpriseID();
				ArrayList oneEnterprise = RBACManager.GetCoreEnterprises(utility.GetAppID(),enterprise);
				if (oneEnterprise.Count > 0)
				{
					enterprise = (CoreEnterprise)oneEnterprise[0];
					Session.Add("OneEnterprise" , enterprise);
				}
				else
				{
					Logger.LogTraceError("MainFrame","Page_Load","ERR001","Error getting 'Enterprise' object from RBAC");
					throw new ApplicationException("Enterprise object is null");;
				}
			}
			else
			{
				enterprise = (CoreEnterprise) Session["OneEnterprise"];
			}
			
			String tempStyle = enterprise.StyleSheetURL;
			strStyleSheet = tempStyle;
			String tempWelcome = enterprise.WelcomeURL;
			if(tempWelcome!="")
			{
				strWelcomePagePath = tempWelcome;
			}
			else
			{
				strWelcomePagePath = "defaultWelcome.aspx";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
