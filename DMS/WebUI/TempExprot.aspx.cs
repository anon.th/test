using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.common.applicationpages;
using com.ties.DAL;
using TIES.WebUI ;
using System.IO;
using TIESDAL;
using TIES.WebUI.ConsignmentNote ;

namespace TIES.WebUI
{
	public class TempExprot : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label lblErrorMesssage;
	
		string strappid=null;
		string strenterpriseid=null;
		string strUserLoggin=null;

		public	string pageTitle = "ReportViewer";

		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				String fileName = String.Empty;;
				String strFormid = (String)Session["FORMID"];
				
				if(strFormid == "ExportXMLAWB")
				{
					String XMLText = Session["XMLText"].ToString();

					//fileName = "mawb" + DateTime.Now.ToString("yyyyMMddHHmm");
					
					fileName = (String)Session["FileName"];
					//fileName=@"C:\Inetpub\wwwroot\PRD\"+fileName;
					Response.Clear();
					Response.ContentType = "text/xml";
					Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}", fileName + ".xml"));					
					Response.Write(XMLText);
					Context.Response.End();
				}
				else if(strFormid == "ExportInvoiceAndCheques")
				{
					fileName = (String)Session["FileName"];
					//fileName=@"C:\Inetpub\wwwroot\PRD\"+fileName;
					Response.Clear();
					Response.ContentType = "text/csv";
					//Response.AddHeader("Content-Disposition", "attachment;filename=\"" + @"c:\" + fileName + "\""); 
					Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}", fileName + ".csv"));
					Response.Write((String)Session["CSVData"]);	
					Context.Response.End();
					//Response.Flush();
					//Context.Response.End();
				}
			}
			catch(Exception ex)
			{
				lblErrorMesssage.Text = ex.ToString();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
