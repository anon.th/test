using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.DAL;  
using com.ties.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes; 
using TIES;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for AgentZone.
	/// </summary>
	public class AgentZone : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.DataGrid dgAgentZone;
		protected System.Web.UI.WebControls.Label lblAgentCode;
		protected System.Web.UI.WebControls.Label lblAgentName;
		protected System.Web.UI.WebControls.TextBox txtAgentName;
		protected System.Web.UI.WebControls.Button BtnStatusCode;
		protected System.Web.UI.WebControls.Label lblZipIncludedMessage;
		protected System.Web.UI.WebControls.Label lblAgentZoneMessage;
		protected System.Web.UI.WebControls.Button btnZipInsert;
		protected System.Web.UI.WebControls.DataGrid dgZipcodIncluded;
		protected System.Web.UI.WebControls.Button btnMoveLast;
		protected System.Web.UI.WebControls.Button btnMovenext;
		protected System.Web.UI.WebControls.TextBox Txt_RecCnt;
		protected System.Web.UI.WebControls.Button btnMovePrevious;
		protected System.Web.UI.WebControls.Button btnMovefirst;
		protected System.Web.UI.WebControls.TextBox txtAgentID;
		protected System.Web.UI.WebControls.Button btnAgentName;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnAgentName.Click += new System.EventHandler(this.btnAgentName_Click);
			this.BtnStatusCode.Click += new System.EventHandler(this.BtnStatusCode_Click);
			this.dgAgentZone.SelectedIndexChanged += new System.EventHandler(this.dgAgentZone_SelectedIndexChanged);
			this.btnZipInsert.Click += new System.EventHandler(this.btnZipInsert_Click);
			this.dgZipcodIncluded.SelectedIndexChanged += new System.EventHandler(this.dgZipcodIncluded_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void dgAgentZone_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void dgZipcodIncluded_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void BtnStatusCode_Click(object sender, System.EventArgs e)
		{
			string strCustId =null;
			string strtxtCustId=null;
			if(txtAgentID != null)
			{
				strtxtCustId =txtAgentID.ClientID;
				strCustId = txtAgentID.Text;
			}
						

			String sUrl = "AgentPopup.aspx?FORMID=AgentZone&CUSTID_TEXT="+strCustId+"&BANDCODE="+strtxtCustId;
//			ArrayList paramList = new ArrayList();
//			paramList.Add(sUrl);
//			String sScript = Utility.GetScript("openWindowParam.js",paramList);
//			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
	//testing
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.open('"+sUrl+"');";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnAgentName_Click(object sender, System.EventArgs e)
		{
			string strCustId =null;
			string strtxtCustId=null;
			if(txtAgentID != null)
			{
				strtxtCustId =txtAgentName.ClientID;
				strCustId = txtAgentName.Text;
			}
						

			String sUrl = "AgentPopup.aspx?FORMID=AgentZone&CUSTNAME_TEXT="+strCustId+"&BANDCODE="+strtxtCustId;
			//			ArrayList paramList = new ArrayList();
			//			paramList.Add(sUrl);
			//			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			//			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			//testing
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.open('"+sUrl+"');";
			sScript += "</script>";
			Response.Write(sScript);
			
		}

		private void btnZipInsert_Click(object sender, System.EventArgs e)
		{
			string strCustId =null;
			string strtxtCustId=null;
			String sUrl = "AgentZipCodePopup.aspx?FORMID=AgentZone&CUSTNAME_TEXT="+strCustId+"&BANDCODE="+strtxtCustId;
			//			ArrayList paramList = new ArrayList();
			//			paramList.Add(sUrl);
			//			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			//			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			//testing
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.open('"+sUrl+"');";
			sScript += "</script>";
			Response.Write(sScript);
		
		}

		
	}
}
