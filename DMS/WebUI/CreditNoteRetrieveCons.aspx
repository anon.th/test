<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="CreditNoteRetrieveCons.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CreditNoteRetrieveCons" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CreditNoteRetrieveCons</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="JavaScript">
		<!--
		function SetFocus()
		{
			alert(document.getElementById("hidtxt").value);
		}
	
		// -->
		</script>
	</HEAD>
	<body onload="Form1.elements['hidtxt'].value()!='': alert('xxx'); ?" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<FONT face="Tahoma">
				<asp:label id="lblTitle" style="Z-INDEX: 101; LEFT: 9px; POSITION: absolute; TOP: 16px" runat="server"
					Width="384px" Height="24px" Font-Bold="True" CssClass="mainTitleSize">Copy Invoice to Generate Credit Note</asp:label><asp:label id="lblErrorMsg" style="Z-INDEX: 103; LEFT: 17px; POSITION: absolute; TOP: 48px"
					runat="server" Width="100%" Height="32px" CssClass="errorMsgColor" ForeColor="Red">ErrorMessage</asp:label><asp:label id="lblInvoiceNo" style="Z-INDEX: 104; LEFT: 14px; POSITION: absolute; TOP: 79px"
					runat="server" Height="24px" CssClass="tableLabel">Copied from Invoice No</asp:label></FONT>
			<div id="mainPanel" style="Z-INDEX: 102; LEFT: 1px; WIDTH: 950px; POSITION: relative; TOP: 86px; HEIGHT: 541px"
				MS_POSITIONING="GridLayout" runat="server">
				<table id="mainTable" style="Z-INDEX: 101; LEFT: 0px; POSITION: absolute; TOP: 32px" width="800"
					runat="server">
					<tr height="15">
						<td style="WIDTH: 108px"><asp:button id="btnSave" tabIndex="20" runat="server" Width="62px" CssClass="queryButton" Text="Save"
								Enabled="True"></asp:button></td>
						<td style="WIDTH: 103px"><asp:button id="btnCancel" tabIndex="20" runat="server" Width="62px" CssClass="queryButton"
								Text="Cancel" CausesValidation="False"></asp:button></td>
						<td style="WIDTH: 267px"><asp:label id="lblTotalCreditRow" runat="server" Width="72px" Height="22px" CssClass="tableLabel">Total Credit</asp:label></td>
						<td style="WIDTH: 13px"><asp:textbox id="txtTotalCreditFRG" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 44px"><asp:textbox id="txtTotalCreditINS" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 38px"><asp:textbox id="txtTotalCreditVAS" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 34px"><asp:textbox id="txtTotalCreditESA" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 39px"><asp:textbox id="txtTotalCreditOTH" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 33px"><asp:textbox id="txtTotalCreditPDX" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 35px"><asp:textbox id="txtTotalCreditADJ" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 32px"><asp:textbox id="txtTotalCreditTOT" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td><asp:textbox id="txtTotalCredit" tabIndex="1" runat="server" Width="80px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
					</tr>
					<tr height="15">
						<td style="WIDTH: 108px"></td>
						<td style="WIDTH: 103px"><asp:label id="lblConsignment" runat="server" Width="95px" Height="22px" CssClass="tableLabel">Consignment</asp:label></td>
						<td style="WIDTH: 267px"><asp:label id="lblTotalInvoice" runat="server" Width="82px" Height="22px" CssClass="tableLabel">Total Invoice</asp:label></td>
						<td style="WIDTH: 13px"><asp:textbox id="txtTotalInvoiceFRG" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 44px"><asp:textbox id="txtTotalInvoiceINS" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 38px"><asp:textbox id="txtTotalInvoiceVAS" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 34px"><asp:textbox id="txtTotalInvoiceESA" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 39px"><asp:textbox id="txtTotalInvoiceOTH" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 33px"><asp:textbox id="txtTotalInvoicePDX" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 35px"><asp:textbox id="txtTotalInvoiceADJ" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 32px"><asp:textbox id="txtTotalInvoiceTOT" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 32px"></td>
					</tr>
					<tr height="15">
						<td style="WIDTH: 108px"><asp:button id="btnSearch" tabIndex="20" runat="server" Width="62px" CssClass="queryButton"
								Text="Search" CausesValidation="False"></asp:button></td>
						<td style="WIDTH: 103px"><asp:textbox id="txtConsignment" tabIndex="1" runat="server" Width="88px" CssClass="textField"
								MaxLength="30"></asp:textbox></td>
						<td style="WIDTH: 267px"><asp:label id="lblTotalSelected" runat="server" Height="22px" CssClass="tableLabel">Total Selected</asp:label></td>
						<td style="WIDTH: 13px"><asp:textbox id="txtTotalSelectFRG" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 44px"><asp:textbox id="txtTotalSelectINS" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 38px"><asp:textbox id="txtTotalSelectVAS" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 34px"><asp:textbox id="txtTotalSelectESA" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 39px"><asp:textbox id="txtTotalSelectOTH" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 33px"><asp:textbox id="txtTotalSelectPDX" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 35px"><asp:textbox id="txtTotalSelectADJ" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 32px"><asp:textbox id="txtTotalSelectTOT" tabIndex="1" runat="server" Width="60px" CssClass="textField"
								ReadOnly="True" MaxLength="16"></asp:textbox></td>
						<td style="WIDTH: 32px"></td>
					</tr>
					<tr height="15">
						<td style="WIDTH: 108px"></td>
						<td style="WIDTH: 103px"></td>
						<td style="WIDTH: 267px"><asp:button id="btnAllocate" tabIndex="20" runat="server" Width="79px" CssClass="queryButton"
								Text="Allocate" CausesValidation="False"></asp:button></td>
						<td style="WIDTH: 13px"><cc1:mstextbox id="txtTotalAllocateFRG" Width="60px" CssClass="textFieldRightAlign" MaxLength="9"
								TextMaskString="#,##0.00" NumberScale="2" NumberPrecision="10" NumberMinValue="-99999999" NumberMaxValue="99999999"
								TextMaskType="msNumeric" AutoPostBack="False" Runat="server"></cc1:mstextbox></td>
						<td style="WIDTH: 44px"><cc1:mstextbox id="txtTotalAllocateINS" Width="60px" CssClass="textFieldRightAlign" MaxLength="9"
								TextMaskString="#,##0.00" NumberScale="2" NumberPrecision="10" NumberMinValue="-99999999" NumberMaxValue="99999999"
								TextMaskType="msNumeric" AutoPostBack="False" Runat="server"></cc1:mstextbox></td>
						<td style="WIDTH: 38px"><cc1:mstextbox id="txtTotalAllocateVAS" Width="60px" CssClass="textFieldRightAlign" MaxLength="9"
								TextMaskString="#,##0.00" NumberScale="2" NumberPrecision="10" NumberMinValue="-99999999" NumberMaxValue="99999999"
								TextMaskType="msNumeric" AutoPostBack="False" Runat="server"></cc1:mstextbox></td>
						<td style="WIDTH: 34px"><cc1:mstextbox id="txtTotalAllocateESA" Width="60px" CssClass="textFieldRightAlign" MaxLength="9"
								TextMaskString="#,##0.00" NumberScale="2" NumberPrecision="10" NumberMinValue="-99999999" NumberMaxValue="99999999"
								TextMaskType="msNumeric" AutoPostBack="False" Runat="server"></cc1:mstextbox></td>
						<td style="WIDTH: 39px"><cc1:mstextbox id="txtTotalAllocateOTH" Width="60px" CssClass="textFieldRightAlign" MaxLength="9"
								TextMaskString="#,##0.00" NumberScale="2" NumberPrecision="10" NumberMinValue="-99999999" NumberMaxValue="99999999"
								TextMaskType="msNumeric" AutoPostBack="False" Runat="server"></cc1:mstextbox></td>
						<td style="WIDTH: 33px"><cc1:mstextbox id="txtTotalAllocatePDX" Width="60px" CssClass="textFieldRightAlign" MaxLength="9"
								TextMaskString="#,##0.00" NumberScale="2" NumberPrecision="10" NumberMinValue="-99999999" NumberMaxValue="99999999"
								TextMaskType="msNumeric" AutoPostBack="False" Runat="server"></cc1:mstextbox></td>
						<td style="WIDTH: 35px"><cc1:mstextbox id="txtTotalAllocateADJ" Width="60px" CssClass="textFieldRightAlign" MaxLength="9"
								TextMaskString="#,##0.00" NumberScale="2" NumberPrecision="10" NumberMinValue="-99999999" NumberMaxValue="99999999"
								TextMaskType="msNumeric" AutoPostBack="False" Runat="server"></cc1:mstextbox></td>
						<td style="WIDTH: 32px"><cc1:mstextbox id="txtTotalAllocateTOT" Width="60px" CssClass="textFieldRightAlign" MaxLength="9"
								TextMaskString="#,##0.00" NumberScale="2" NumberPrecision="10" NumberMinValue="-99999999" NumberMaxValue="99999999"
								TextMaskType="msNumeric" AutoPostBack="False" Runat="server"></cc1:mstextbox></td>
						<td style="WIDTH: 32px"></td>
					</tr>
					<tr height="15">
						<td><asp:button id="btnSelectAll" tabIndex="20" runat="server" Width="90px" CssClass="queryButton"
								Text="Select All"></asp:button></td>
						<td></td>
						<td vAlign="middle" align="center"><asp:label id="lblTotal" runat="server" Width="56px" Height="12px" CssClass="tableLabel">Total</asp:label></td>
						<td align="center"><asp:label id="lblFRG" runat="server" Width="42px" Height="14px" CssClass="tableLabel">FRG</asp:label></td>
						<td align="center"><asp:label id="lblINS" runat="server" Width="47px" Height="11px" CssClass="tableLabel">INS</asp:label></td>
						<td align="center"><FONT face="Tahoma"><asp:label id="lblVAS" runat="server" Width="39px" Height="11px" CssClass="tableLabel">VAS</asp:label></FONT></td>
						<td align="center"><FONT face="Tahoma"><asp:label id="lblESA" runat="server" Width="42px" Height="10px" CssClass="tableLabel">ESA</asp:label></FONT></td>
						<td align="center"><asp:label id="lblOTH" runat="server" Width="42px" Height="10px" CssClass="tableLabel">OTH</asp:label></td>
						<td align="center"><asp:label id="lblPDX" runat="server" Width="40px" Height="13px" CssClass="tableLabel">PDX</asp:label></td>
						<td align="center"><FONT face="Tahoma"><asp:label id="lblADJ" runat="server" Width="32px" Height="14px" CssClass="tableLabel">ADJ</asp:label></FONT></td>
						<td vAlign="middle" align="center"><asp:label id="lblTOT" runat="server" Width="41px" Height="12px" CssClass="tableLabel">TOT</asp:label></td>
						<td vAlign="middle" align="center"><asp:label id="lblTotalCreditCol" runat="server" Width="48px" Height="27px" CssClass="tableLabel">Total Credit</asp:label></td>
					</tr>
				</table>
				<asp:datagrid id="dgConsignment" style="Z-INDEX: 102; LEFT: 0px; POSITION: absolute; TOP: 170px"
					runat="server" Width="878px" Height="95px" ShowHeader="False" ItemStyle-Height="20" AutoGenerateColumns="False"
					SelectedItemStyle-CssClass="gridFieldSelected" OnPageIndexChanged="dgConsignment_PageIndexChanged"
					AllowPaging="True">
					<FooterStyle Wrap="False"></FooterStyle>
					<SelectedItemStyle Wrap="False" CssClass="gridFieldSelected"></SelectedItemStyle>
					<EditItemStyle Wrap="False"></EditItemStyle>
					<AlternatingItemStyle Wrap="False"></AlternatingItemStyle>
					<ItemStyle Wrap="False" Height="10px" CssClass="gridField"></ItemStyle>
					<HeaderStyle Wrap="False"></HeaderStyle>
					<Columns>
						<asp:TemplateColumn>
							<HeaderStyle Wrap="False" HorizontalAlign="Center" Width="16px" CssClass="gridBlueHeading"></HeaderStyle>
							<ItemStyle Wrap="False" HorizontalAlign="Center" Width="16px" CssClass="gridField"></ItemStyle>
							<HeaderTemplate>
								<asp:CheckBox CssClass="tableCheckBox" Runat="server" ID="chkAllConsignment" AutoPostBack="True"
									OnCheckedChanged="chkAllConsignment_CheckChanged"></asp:CheckBox>
							</HeaderTemplate>
							<ItemTemplate>
								<asp:CheckBox AutoPostBack="True" CssClass="tableCheckBox" Runat="server" ID="chkConsignment" OnCheckedChanged="chkConsignment_CheckChanged" Checked='<%#Convert.ToString(DataBinder.Eval(Container.DataItem,"Checked"))=="1"%>'>
								</asp:CheckBox>
							</ItemTemplate>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:TemplateColumn>
						<asp:TemplateColumn>
							<HeaderStyle Wrap="False" HorizontalAlign="Center" Width="166px" CssClass="gridBlueHeading"></HeaderStyle>
							<ItemStyle Wrap="False" Width="166px" CssClass="gridField"></ItemStyle>
							<ItemTemplate>
								<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtgConsignment" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' ReadOnly =True>
								</asp:TextBox>
							</ItemTemplate>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="Total">
							<HeaderStyle Wrap="False" HorizontalAlign="Center" Width="85px" CssClass="gridBlueHeading"></HeaderStyle>
							<ItemStyle Wrap="False" Width="85px" CssClass="gridField"></ItemStyle>
							<ItemTemplate>
								<asp:TextBox CssClass="gridTextBoxNumber" runat="server" ID="txtTotal" Text='<%#DataBinder.Eval(Container.DataItem,"invoice_amt",strFormatCurrency)%>' ReadOnly =True>
								</asp:TextBox>
							</ItemTemplate>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="FRG">
							<HeaderStyle Wrap="False" HorizontalAlign="Center" Width="60px" CssClass="gridBlueHeading"></HeaderStyle>
							<ItemStyle Wrap="False" Width="60px" CssClass="gridField"></ItemStyle>
							<ItemTemplate>
								<cc1:mstextbox id=txtFRG CssClass="gridTextBoxNumber" Text='<%#DataBinder.Eval(Container.DataItem,"FRG",strFormatCurrency)%>' MaxLength="9" TextMaskString="#,##0.00" NumberScale="2" NumberPrecision="8" NumberMinValue="-999999" NumberMaxValue="999999" TextMaskType="msNumeric" AutoPostBack="True" Runat="server" OnTextChanged="txtFRG_TextChanged" ToolTip='<%#DataBinder.Eval(Container.DataItem,"tot_freight_charge",strFormatCurrency)%>'>
								</cc1:mstextbox>
							</ItemTemplate>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="INS">
							<HeaderStyle Wrap="False" HorizontalAlign="Center" Width="60px" CssClass="gridBlueHeading"></HeaderStyle>
							<ItemStyle Wrap="False" Width="60px" CssClass="gridField"></ItemStyle>
							<ItemTemplate>
								<cc1:mstextbox ID="txtINS" Runat="server" CssClass="gridTextBoxNumber" TextMaskType="msNumeric" MaxLength="9" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" AutoPostBack="True" OnTextChanged="txtINS_TextChanged" Text='<%#DataBinder.Eval(Container.DataItem,"INS",strFormatCurrency)%>' ToolTip='<%#DataBinder.Eval(Container.DataItem,"insurance_amt",strFormatCurrency)%>' >
								</cc1:mstextbox>
							</ItemTemplate>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="VAS">
							<HeaderStyle Wrap="False" HorizontalAlign="Center" Width="60px" CssClass="gridBlueHeading"></HeaderStyle>
							<ItemStyle Wrap="False" Width="60px" CssClass="gridField"></ItemStyle>
							<ItemTemplate>
								<cc1:mstextbox ID="txtVAS" Runat="server" CssClass="gridTextBoxNumber" TextMaskType="msNumeric" MaxLength="9" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" AutoPostBack="True" OnTextChanged="txtVAS_TextChanged" Text='<%#DataBinder.Eval(Container.DataItem,"VAS",strFormatCurrency)%>' ToolTip='<%#DataBinder.Eval(Container.DataItem,"tot_vas_surcharge",strFormatCurrency)%>'>
								</cc1:mstextbox>
							</ItemTemplate>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="ESA">
							<HeaderStyle Wrap="False" HorizontalAlign="Center" Width="60px" CssClass="gridBlueHeading"></HeaderStyle>
							<ItemStyle Wrap="False" Width="60px" CssClass="gridField"></ItemStyle>
							<ItemTemplate>
								<cc1:mstextbox ID="txtESA" Runat="server" CssClass="gridTextBoxNumber" TextMaskType="msNumeric" MaxLength="9" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" AutoPostBack="True" OnTextChanged="txtESA_TextChanged" Text='<%#DataBinder.Eval(Container.DataItem,"ESA",strFormatCurrency)%>' ToolTip='<%#DataBinder.Eval(Container.DataItem,"esa_surcharge",strFormatCurrency)%>'>
								</cc1:mstextbox>
							</ItemTemplate>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="OTH">
							<HeaderStyle Wrap="False" HorizontalAlign="Center" Width="60px" CssClass="gridBlueHeading"></HeaderStyle>
							<ItemStyle Wrap="False" Width="60px" CssClass="gridField"></ItemStyle>
							<ItemTemplate>
								<cc1:mstextbox ID="txtOTH" Runat="server" CssClass="gridTextBoxNumber" TextMaskType="msNumeric" MaxLength="9" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" AutoPostBack="True" OnTextChanged="txtOTH_TextChanged" Text='<%#DataBinder.Eval(Container.DataItem,"OTH",strFormatCurrency)%>' ToolTip='<%#DataBinder.Eval(Container.DataItem,"other_surcharge",strFormatCurrency)%>'>
								</cc1:mstextbox>
							</ItemTemplate>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="PDX">
							<HeaderStyle Wrap="False" HorizontalAlign="Center" Width="60px" CssClass="gridBlueHeading"></HeaderStyle>
							<ItemStyle Wrap="False" Width="60px" CssClass="gridField"></ItemStyle>
							<ItemTemplate>
								<cc1:mstextbox ID="txtPDX" Runat="server" CssClass="gridTextBoxNumber" TextMaskType="msNumeric" MaxLength="9" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" AutoPostBack="True" OnTextChanged="txtPDX_TextChanged" Text='<%#DataBinder.Eval(Container.DataItem,"PDX",strFormatCurrency)%>' ToolTip='<%#DataBinder.Eval(Container.DataItem,"total_exception",strFormatCurrency)%>'>
								</cc1:mstextbox>
							</ItemTemplate>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="ADJ">
							<HeaderStyle Wrap="False" HorizontalAlign="Center" Width="60px" CssClass="gridBlueHeading"></HeaderStyle>
							<ItemStyle Wrap="False" Width="60px" CssClass="gridField"></ItemStyle>
							<ItemTemplate>
								<cc1:mstextbox ID="txtADJ" Runat="server" CssClass="gridTextBoxNumber" TextMaskType="msNumeric" MaxLength="9" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" AutoPostBack="True" OnTextChanged="txtADJ_TextChanged" Text='<%#DataBinder.Eval(Container.DataItem,"ADJ",strFormatCurrency)%>' ToolTip='<%#DataBinder.Eval(Container.DataItem,"invoice_adj_amount",strFormatCurrency)%>'>
								</cc1:mstextbox>
							</ItemTemplate>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="TOT">
							<HeaderStyle Wrap="False" HorizontalAlign="Center" Width="60px" CssClass="gridBlueHeading"></HeaderStyle>
							<ItemStyle Wrap="False" Width="60px" CssClass="gridField"></ItemStyle>
							<ItemTemplate>
								<cc1:mstextbox ID="txtTOT" Runat="server" CssClass="gridTextBoxNumber" TextMaskType="msNumeric" MaxLength="9" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" AutoPostBack="True" OnTextChanged="txtTOT_TextChanged" Text='<%#DataBinder.Eval(Container.DataItem,"TOT",strFormatCurrency)%>' ToolTip='<%#DataBinder.Eval(Container.DataItem,"invoice_amt",strFormatCurrency)%>'>
								</cc1:mstextbox>
							</ItemTemplate>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:TemplateColumn>
						<asp:TemplateColumn HeaderText="Total Credit">
							<HeaderStyle Wrap="False" HorizontalAlign="Center" Width="80px" CssClass="gridBlueHeading"></HeaderStyle>
							<ItemStyle Wrap="False" Width="80px" CssClass="gridField"></ItemStyle>
							<ItemTemplate>
								<cc1:mstextbox ID="txtgTotalCredit" Runat="server" CssClass="gridTextBoxNumber" AutoPostBack="False" TextMaskType="msNumeric" MaxLength="9" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" Text='<%#DataBinder.Eval(Container.DataItem,"TotalCredit",strFormatCurrency)%>' >
								</cc1:mstextbox>
							</ItemTemplate>
							<FooterStyle Wrap="False"></FooterStyle>
						</asp:TemplateColumn>
					</Columns>
					<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"
						Wrap="False"></PagerStyle>
				</asp:datagrid><INPUT id="hidtxt" style="Z-INDEX: 103; LEFT: 2px; POSITION: absolute; TOP: 484px" type="hidden"
					runat="server">
				<asp:label id="lblRemark" style="Z-INDEX: 104;LEFT: 0px;POSITION: absolute;TOP: 128%" runat="server"
					CssClass="errorMsgColor" Height="22px" Width="752px"> Remark: ADJ/MBG Consignments are highlighted in red.</asp:label></div>
		</form>
	</body>
</HTML>
