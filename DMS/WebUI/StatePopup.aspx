<%@ Page language="c#" Codebehind="StatePopup.aspx.cs" AutoEventWireup="false" Inherits="com.ties.StatePopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>StatePopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="StatePopup" method="post" runat="server">
			<asp:datagrid id="dgStateMaster" style="Z-INDEX: 102; POSITION: absolute; TOP: 96px; LEFT: 45px"
				runat="server" OnPageIndexChanged="Paging" OnSelectedIndexChanged="dgStateMaster_SelectedIndexChanged"
				AllowPaging="True" Width="522px" AutoGenerateColumns="False" BorderColor="#CCCCCC" BackColor="White"
				CellPadding="3" BorderWidth="1px" BorderStyle="None" CssClass="gridHeading" AllowCustomPaging="True">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" CssClass="drilldownFieldSelected"></SelectedItemStyle>
				<ItemStyle CssClass="drilldownField"></ItemStyle>
				<HeaderStyle Width="10%" CssClass="drilldownHeading"></HeaderStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="country" HeaderText="Country"></asp:BoundColumn>
					<asp:BoundColumn DataField="state_code" HeaderText="State Code"></asp:BoundColumn>
					<asp:BoundColumn DataField="state_name" HeaderText="State Name"></asp:BoundColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select"></asp:ButtonColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" ForeColor="#000066"
					BackColor="White" CssClass="normalText"></PagerStyle>
			</asp:datagrid>
			<asp:textbox id="txtCountry" style="Z-INDEX: 107; POSITION: absolute; TOP: 63px; LEFT: 45px"
				runat="server" CssClass="textField" Width="116px" Height="19px"></asp:textbox>
			<asp:label id="lblCountry" style="Z-INDEX: 106; POSITION: absolute; TOP: 38px; LEFT: 45px"
				runat="server" CssClass="tableLabel" Width="100px" Height="16px" Font-Bold="True">Country</asp:label>
			<asp:button id="btnSearch" style="Z-INDEX: 105; POSITION: absolute; TOP: 63px; LEFT: 290px"
				runat="server" Width="78" CssClass="buttonProp" Height="21" Text="Search" CausesValidation="False"
				Font-Bold="True"></asp:button>
			<asp:textbox id="txtStateCode" style="Z-INDEX: 104; POSITION: absolute; TOP: 63px; LEFT: 165px"
				runat="server" Width="116px" CssClass="textField" Height="19px"></asp:textbox>
			<asp:label id="lblStateCode" style="Z-INDEX: 100; POSITION: absolute; TOP: 38px; LEFT: 165px"
				runat="server" Width="100px" CssClass="tableLabel" Height="16px" Font-Bold="True">State Code</asp:label>
			<asp:button id="btnOk" style="Z-INDEX: 101; POSITION: absolute; TOP: 63px; LEFT: 369px" runat="server"
				Width="78px" CssClass="buttonProp" Height="21px" Text="Close" CausesValidation="False" Font-Bold="True"></asp:button>
		</form>
	</body>
</HTML>
