<%@ Page Language="c#" CodeBehind="InvManageSelect.aspx.cs" AutoEventWireup="false" Inherits="com.ties.InvManageSelect" %>

<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Invoice Management Selection</title>
    <link rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
    <meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <!--#INCLUDE FILE="msFormValidations.inc"-->
    <script language="javascript" src="Scripts/settingScrollPosition.js"></script>
    <script language="javascript" src="Scripts/msFormValidation.js"></script>
    <script language="javascript">
		<!--

    var currentAmntPaid = 0;
    var currentBalanceDue = 0;
    var digit = 0;
    function setValue() {
        var s = document.all['inFile'];
        document.all['txtFilePath'].innerText = s.value;
        if (s.value != '')
            document.all['btnImport'].disabled = false;
        else
            document.all['btnImport'].disabled = true;

    }

    function upBrowse() {
        document.all['divBrowse'].style.backgroundImage = 'url(images/btn-browse-up.GIF)';
    }

    function downBrowse() {
        document.all['divBrowse'].style.backgroundImage = 'url(images/btn-browse-down.GIF)';
    }

    function clearValue() {
        if (document.all['txtFilePath'].innerText == '') {
            document.location.reload();
        }
    }
    function validateInput() {
        var frm = document.forms[0];
        if (frm.txtAmntPaid.value != "") {
            var today = new Date();
            var day = today.getDate();
            var month = today.getMonth() + 1;
            var year = today.getFullYear();
            var hours = today.getHours();
            if (day < 10) {
                day = "0" + day;
            }
            if (month < 10) {
                month = "0" + month;
            }
            if (hours < 10) {
                hours = "0" + hours;
            }
            var min = today.getMinutes();
            if (min < 10) {
                min = "0" + min;
            }
            var sec = today.getSeconds();
            if (sec < 10) {
                sec = "0" + sec;
            }
            var time = hours + ":" + min + ":" + sec;
            if ((month < 10) && (month.length == 1)) {
                month = '0' + month;
            }
            if (frm.txtPaidDate.value == '') {
                frm.txtPaidDate.value = day + "/" + month + "/" + year;

            }
            var username = document.getElementById("txtUserName").value;//frm.txtUsername.value;					
            //document.getElementById("lblDispPaymentUpdate").innerHTML = username+" "+day+"/"+month+"/"+year+" "+time;
            //document.getElementById("lblDispPaymentUpdate").style.display="block";
            //document.getElementById("lblPaymentUpd").style.display="block";
            var amntPaid, totalAmnt = 0;
            if (currentAmntPaid == "0") {
                //currentAmntPaid=frm.txtCurrAmntPaid.value;	
                currentAmntPaid = frm.hidtxtCurrAmt.value;
                if (currentAmntPaid.lastIndexOf('.') <= -1) {
                    digit = 0;
                } else {
                    digit = (currentAmntPaid.length - 1) - currentAmntPaid.lastIndexOf('.');
                }
            }
            amntPaid = frm.txtAmntPaid.value;
            totalAmnt = parseFloat(currentAmntPaid.replace(/,/g, '')) + parseFloat(amntPaid);
            frm.txtCurrAmntPaid.value = totalAmnt;
            round(frm.txtCurrAmntPaid, digit);
            frm.txtCurrAmntPaid.value = addCommas(frm.txtCurrAmntPaid.value);
            if (currentBalanceDue == "0") {
                //currentBalanceDue=frm.txtBalanceDue.value;	
                currentBalanceDue = frm.hidtxtBalanceDue.value;
            }
            totalAmnt = parseFloat(currentBalanceDue.replace(/,/g, '')) - parseFloat(amntPaid);
            frm.txtBalanceDue.value = totalAmnt;
            round(frm.txtBalanceDue, digit);
            frm.txtBalanceDue.value = addCommas(frm.txtBalanceDue.value);
            //document.getElementById("lblApplyAmt").style.display="block";
            //document.getElementById("txtApplyAmt").style.display="block";
            //document.getElementById("ddlLinkToCDN").disabled=false;

        } else {
            frm.txtPaidDate.value = "";
            frm.txtPaidDate.innerHTML = "";
            //document.getElementById("lblDispPaymentUpdate").innerHTML="";
            //document.getElementById("lblDispPaymentUpdate").style.display="none";
            //document.getElementById("lblPaymentUpd").style.display="none";
            //document.getElementById("lblApplyAmt").style.display="none";
            //document.getElementById("txtApplyAmt").style.display="none";
            //document.getElementById("ddlLinkToCDN").disabled=true;
        }
    }
    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];

        //x2 = x.length &gt; 1 ? '.' + x[1] : '';
        var x2
        if (x.length = 2) {
            x2 = "." + x[1];
        }
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    //-->
    </script>
</head>
<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
    onload="SetScrollPosition();" ms_positioning="GridLayout">
    <form id="InvManageSelect" method="post" runat="server">
        <asp:TextBox Style="z-index: 108; position: absolute; top: 8px; left: 1208px" ID="txtExcludeCustomerList"
            runat="server" Width="0px"></asp:TextBox>
        <asp:Button Style="z-index: 108; position: absolute; top: 8px; left: 1208px" ID="btnHiddenExport"
            runat="server" Width="0px"></asp:Button>
        <input style="z-index: 108; position: absolute; display: none; top: 48px; left: 144px"
            id="ExecuteCheckBox" type="button" name="execCheckBox" runat="server" causesvalidation="False">
            <div style="z-index: 101; position: absolute; width: 954px; height: 272px; top: 23px; left: 24px"
                id="divInvManageList" runat="server" ms_positioning="GridLayout">
                <table style="z-index: 100; position: absolute; width: 945px; height: 222px; top: 12px; left: 9px"
                    id="Table2" border="0" cellspacing="1" cellpadding="1" width="932">
                    <tbody style="z-index: 1; left: 1px; top: 1px; position: absolute">
                        <tr>
                            <td><font face="Tahoma">
                                <asp:Label ID="lblMainTitle" runat="server" Height="26px" CssClass="mainTitleSize" Width="477px">Invoice Management Listing</asp:Label></font></td>
                        </tr>
                        <tr>
                            <td><font face="Tahoma">
                                <asp:Label ID="lblErrorForCancel" runat="server" Height="30px" CssClass="errorMsgColor"></asp:Label></font></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="queryButton" Width="96px"
                                    Text="Cancel"></asp:Button><asp:Button ID="btnSelectAll" runat="server" CausesValidation="False" CssClass="queryButton"
                                        Width="96px" Text="Select All"></asp:Button><asp:Button ID="btnApproveSelected" runat="server" CssClass="queryButton" Width="154px" Text="Approve Selected"></asp:Button><asp:Button ID="btnCancelSelected" runat="server" CssClass="queryButton" Width="154px" Text="Cancel Selected"></asp:Button><asp:Button ID="btnPrintHeader" runat="server" CssClass="queryButton" Width="154px" Text="Print Header Selected"></asp:Button><asp:Button ID="btnPrintDetail" runat="server" CausesValidation="False" CssClass="queryButton"
                                            Width="153px" Text="Print Detail Selected"></asp:Button><asp:Button ID="btnPrintInvPkg" runat="server" CausesValidation="False" CssClass="queryButton"
                                                Width="120px" Text="Print Detail(Pkg)" Visible="True"></asp:Button><asp:Button ID="btnExport" runat="server" CausesValidation="False" CssClass="queryButton" Width="100px"
                                                    Text="Export" Visible="True"></asp:Button><asp:Button ID="btnSaveImport" runat="server" CausesValidation="False" CssClass="queryButton"
                                                        Width="96px" Text="Save"></asp:Button></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblErrorMsg" runat="server" Height="30px" CssClass="errorMsgColor" Width="556px"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Height="26px" CssClass="mainTitleSize">Status Selected</asp:Label><asp:Label ID="_Label1" runat="server" Height="26px" CssClass="mainTitleSize" Width="2px">:</asp:Label><asp:Label ID="lblShowStatust" runat="server" Height="26px" CssClass="mainTitleSize" Width="275px">[lblShowStatust]</asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DataGrid ID="dgPreview" runat="server" Height="54px" Width="500px" SelectedItemStyle-CssClass="gridFieldSelectedINV"
                                    AutoGenerateColumns="False" ItemStyle-Height="20" HeaderStyle-Height="10px">
                                    <SelectedItemStyle CssClass="gridFieldSelectedINV"></SelectedItemStyle>
                                    <ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
                                    <HeaderStyle Height="10px"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn>
                                            <HeaderStyle Width="20px" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="getCheck_onChecked"></asp:CheckBox>
                                                <%--<INPUT style="Z-INDEX: 0" id="chkSelect" type="checkbox" name="chkSelect" class="queryButton" runat="server" CausesValidation="False">--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="" CancelText="" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:EditCommandColumn>
                                        <asp:TemplateColumn>
                                            <HeaderStyle CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                            <ItemTemplate>
                                                &nbsp;
													<asp:ImageButton CommandName="Select" ID="imgSelect" runat="server" ImageUrl="images/butt-select.gif"></asp:ImageButton>&nbsp;
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="invoice_no" HeaderText="Invoice No.">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="invoice_date" HeaderText="Generation Date" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="invoice_date" HeaderText="Generation Date" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Due_date" HeaderText="Pickup Date" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="payertype_name" HeaderText="Invoice Type">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="payerid" HeaderText="Customer ID">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Adjustment">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label CssClass="gridFieldNum" ID="lblAdj" runat="server" Text='<%#utility.encodeNegValue(DataBinder.Eval(Container.DataItem,"invoice_adj_amount"),(String)ViewState["m_format"])%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Total Invoice Amount">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label CssClass="gridFieldNum" ID="Label5" runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"invoice_amt"))%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Agency Charges">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label CssClass="gridFieldNum" ID="lblAgencyCharges" runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"agency_charges"))%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="invoicestatus_name" HeaderText="Invoice Status">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="first_possible_bpd" HeaderText="1st Possible BPD" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="next_bill_placement_date" HeaderText="Next BPD" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="actual_bill_placement_date" HeaderText="Act BPD" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Amt Paid">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right" CssClass="gridField" Wrap="False"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label CssClass="gridFieldNum" ID="lblAmtPaid" runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"amt_paid"))%>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="payment_date" HeaderText="Paid Date" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="promised_dt" HeaderText="Promise Dt" DataFormatString="{0:dd/MM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="batch_no" HeaderText="Batch Number">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="link_to_invoice" HeaderText="Linked Credit Note">
                                            <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
                                        </asp:BoundColumn>
                                    </Columns>
                                    <PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
                                </asp:DataGrid></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div style="z-index: 103; position: absolute; width: 702px; height: 461px; top: 33px; left: 20px"
                id="divInvoiceManagment" runat="server" ms_positioning="GridLayout">
                <table style="z-index: 101; position: absolute; width: 688px; height: 448px; top: 11px; left: 14px"
                    id="Table1" border="0" cellspacing="1" cellpadding="1" width="670">
                    <tbody>
                        <tr>
                            <td style="height: 9px">
                                <asp:Label ID="lblTitle" runat="server" Height="32px" CssClass="maintitleSize" Width="558px">Invoice Management</asp:Label></td>
                        </tr>
                        <tr>
                            <td style="height: 330px">
                                <asp:Label ID="lblErrorMessage" runat="server" Height="30px" CssClass="errorMsgColor" Width="556px"></asp:Label>
                                <table border="0">
                                    <tbody>
                                        <tr>
                                            <td style="height: 23px">
                                                <asp:Button ID="btnClear" runat="server" CssClass="queryButton" Width="74px" Text="Clear"></asp:Button><asp:Button ID="btnGenerate" runat="server" Height="20px" CssClass="queryButton" Width="155px"
                                                    Text="Execute Query"></asp:Button>&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table style="width: 600px; height: 272px" id="tblShipmentExcepPODRepQry" border="0" width="500"
                                                    runat="server">
                                                    <tbody>
                                                        <tr width="100%">
                                                            <td valign="top" width="100%">
                                                                <fieldset>
                                                                    <legend>
                                                                        <asp:Label ID="Label2" CssClass="tableHeadingFieldset" runat="server">Invoice Management</asp:Label></legend>
                                                                    <table style="width: 482px; height: 233px; top: 2px; left: 1px" id="tblDates" border="0"
                                                                        width="482" align="left" runat="server">
                                                                        <tbody>
                                                                            <tr height="14">
                                                                                <td style="width: 118px">&nbsp;</td>
                                                                                <td style="width: 49px">&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                                <td style="width: 46px">&nbsp;</td>
                                                                                <td>&nbsp;</td>
                                                                            </tr>
                                                                            <tr height="27">
                                                                                <td style="width: 118px">
                                                                                    <asp:Label ID="lblInvoiceDate" runat="server" CssClass="tablelabel" Width="91px">Invoice Date</asp:Label></td>
                                                                                <td style="width: 49px">&nbsp;
																						<asp:Label ID="Label3" runat="server" CssClass="tablelabel" Width="32px">From</asp:Label></td>
                                                                                <td>&nbsp;
																						<cc1:msTextBox ID="txtInvDateFrom" runat="server" CssClass="textField" Width="130px" TextMaskString="99/99/9999"
                                                                                            TextMaskType="msDate" MaxLength="10"></cc1:msTextBox></td>
                                                                                <td>&nbsp;</td>
                                                                                <td style="width: 46px" align="right">
                                                                                    <asp:Label ID="Label4" runat="server" CssClass="tablelabel" Width="19px">To</asp:Label></td>
                                                                                <td>
                                                                                    <cc1:msTextBox ID="txtInvDateto" runat="server" CssClass="textField" Width="130px" TextMaskString="99/99/9999"
                                                                                        TextMaskType="msDate" MaxLength="10" AutoPostBack="True"></cc1:msTextBox></td>
                                                                            </tr>
                                                                            <tr height="27">
                                                                                <td style="width: 118px">
                                                                                    <asp:Label ID="lblInvoiceNo" runat="server" CssClass="tablelabel" Width="91px">Invoice Number</asp:Label></td>
                                                                                <td style="width: 49px" colspan="5">
                                                                                    <asp:TextBox ID="txtInvoiceNo" TabIndex="4" runat="server" CssClass="textField" Width="141px"
                                                                                        MaxLength="20" AutoPostBack="True"></asp:TextBox></FONT></td>
                                                                            </tr>
                                                                            <tr height="27">
                                                                                <td style="width: 118px">
                                                                                    <asp:Label ID="lblDueDate" runat="server" CssClass="tablelabel" Width="91px">Pickup Date</asp:Label></td>
                                                                                <td style="width: 49px">&nbsp;
																						<asp:Label ID="Label10" runat="server" CssClass="tablelabel" Width="32px">From</asp:Label></td>
                                                                                <td>&nbsp;
																						<cc1:msTextBox ID="txtDueDateFrom" runat="server" CssClass="textField" Width="130px" TextMaskString="99/99/9999"
                                                                                            TextMaskType="msDate" MaxLength="10"></cc1:msTextBox></td>
                                                                                <td>&nbsp;</td>
                                                                                <td style="width: 46px" align="right">
                                                                                    <asp:Label ID="Label11" runat="server" CssClass="tablelabel" Width="19px">To</asp:Label></td>
                                                                                <td>
                                                                                    <cc1:msTextBox ID="txtDueDateto" runat="server" CssClass="textField" Width="130px" TextMaskString="99/99/9999"
                                                                                        TextMaskType="msDate" MaxLength="10" AutoPostBack="True"></cc1:msTextBox></td>
                                                                            </tr>
                                                                            <tr height="27">
                                                                                <td style="width: 118px">
                                                                                    <asp:Label ID="lblInvoiceType" runat="server" CssClass="tablelabel" Width="91px">Invoice Type</asp:Label></td>
                                                                                <td style="width: 49px" colspan="5">
                                                                                    <asp:DropDownList ID="ddlInvType" TabIndex="20" runat="server" Height="69px" Width="154px" AutoPostBack="True"
                                                                                        DataValueField="custid" DataTextField="custid">
                                                                                    </asp:DropDownList></td>
                                                                            </tr>
                                                                            <tr height="27">
                                                                                <td style="width: 118px">
                                                                                    <asp:Label ID="lblStatust" runat="server" CssClass="tablelabel" Width="91px"> Status</asp:Label></td>
                                                                                <td style="width: 49px" colspan="5">
                                                                                    <asp:DropDownList ID="ddlInvStatus" TabIndex="20" runat="server" Height="69px" Width="154px" AutoPostBack="True"
                                                                                        DataValueField="custid" DataTextField="custid">
                                                                                    </asp:DropDownList></td>
                                                                            </tr>
                                                                            <tr height="27">
                                                                                <td style="width: 118px">
                                                                                    <asp:Label ID="lblCustID" runat="server" CssClass="tablelabel" Width="91px">Customer ID</asp:Label></td>
                                                                                <td style="width: 49px" colspan="5">
                                                                                    <asp:DropDownList ID="ddbCustomerAcc" TabIndex="20" runat="server" Height="69px" Width="154px" AutoPostBack="True"
                                                                                        DataValueField="custid" DataTextField="custid">
                                                                                    </asp:DropDownList><dbCombo:DbCombo ID="dbCmbAgentId" TabIndex="1" runat="server" Height="17px" Width="140px" AutoPostBack="True"
                                                                                        ReQueryOnLoad="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentIdServerMethod" TextBoxColumns="20"></dbCombo:DbCombo>
                                                                                </td>
                                                                            </tr>
                                                                            <tr height="27">
                                                                                <td style="width: 118px; height: 22px">
                                                                                    <asp:Label ID="lblCustName" runat="server" CssClass="tablelabel" Width="104px">Customer Name</asp:Label></td>
                                                                                <td style="width: 49px; height: 22px" colspan="5">
                                                                                    <asp:TextBox ID="txtCustName" TabIndex="4" runat="server" CssClass="textField" Width="357px"
                                                                                        MaxLength="20" BackColor="Silver"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 118px">
                                                                                    <asp:Label ID="Label6" runat="server" CssClass="tablelabel" Width="118px" Visible="False"> Browse for Import</asp:Label></td>
                                                                                <td style="width: 49px; height: 20px" colspan="4">
                                                                                    <asp:TextBox ID="txtFilePath" TabIndex="4" runat="server" CssClass="textField" Width="250px"
                                                                                        Visible="False" MaxLength="200"></asp:TextBox></td>
                                                                                <td height="20">
                                                                                    <div style="background-image: url(images/btn-browse-up.GIF); width: 70px; display: none; height: 20px"
                                                                                        id="divBrowse" onmouseup="upBrowse();" onmousedown="downBrowse();">
                                                                                        <input onblur="setValue();" style="border-bottom: #88a0c8 1px outset; filter: alpha(opacity: 0); border-left: #88a0c8 1px outset; background-color: #e9edf0; width: 0px; font-family: Arial; color: #003068; font-size: 11px; border-top: #88a0c8 1px outset; border-right: #88a0c8 1px outset; text-decoration: none"
                                                                                            id="inFile" onfocus="setValue();" type="file" name="inFile" runat="server">
                                                                                    </div>
        </input>
        <asp:Button ID="btnImport" runat="server" CssClass="queryButton" Width="60px" Text="Import"
            Visible="False" Enabled="False"></asp:Button></TD></TR>
			<tr>
                <td></td>
            </tr>
        </TBODY></TABLE></FIELDSET> 
			</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR>
			<tr>
                <td>
                    <input value="<%=strScrollPosition%>" type="hidden"
                        name="ScrollPosition"></td>
            </tr>
        </TBODY></TABLE></DIV>
        <asp:Panel Style="z-index: 104; position: absolute; top: 306px; left: 170px" ID="PanelInvoiceCancel"
            runat="server" Height="180px" Width="640px" Visible="False">
            <fieldset style="background-color: #ffff99; width: 609px; height: 177px">
                <legend>
                    <asp:Label ID="lblSendInfo" CssClass="tableHeadingFieldset" runat="server">Invoice Cancel</asp:Label></legend>
                <table style="background-color: #ffff99; width: 608px; height: 127px" id="tblSenderInfo"
                    border="0" cellspacing="0" cellpadding="0" width="608" bgcolor="#ffff99" runat="server">
                    <tr>
                        <td style="width: 40px" width="40"></td>
                        <td style="width: 141px" width="141"></td>
                        <td style="width: 221px" width="221"></td>
                        <td style="width: 15px" width="15"></td>
                        <td width="25%"></td>
                    </tr>
                    <tr>
                        <td style="width: 40px" width="40">&nbsp;
                        </td>
                        <td style="width: 141px" width="141">
                            <asp:Label ID="lblSendAddr1" runat="server" Height="21px" CssClass="tableLabel" Width="155px"
                                BackColor="#FFFF80">Invoice Cancel Type</asp:Label></td>
                        <td style="width: 221px" width="221">
                            <asp:DropDownList ID="ddlInvoiceCancelType" runat="server" Width="149px" AutoPostBack="True"></asp:DropDownList></td>
                        <td style="width: 15px" width="15"></td>
                        <td width="25%"></td>
                    </tr>
                    <tr>
                        <td style="padding-left: 190px" colspan="5" align="left"><font style="text-align: left" face="Tahoma">
                            <asp:Label ID="lblShowCancelInvoice" runat="server" CssClass="tableLabel"></asp:Label></font></td>
                    </tr>
                    <tr height="25">
                        <td style="width: 40px" width="40"></td>
                        <td style="width: 141px" width="141">
                            <asp:Label ID="lblSendNm" runat="server" Height="20px" CssClass="tableLabel" Width="158px"
                                BackColor="#FFFF80">Reason</asp:Label></td>
                        <td style="width: 221px" width="221">
                            <asp:DropDownList ID="ddlInvoiceCancelReason" runat="server" Width="147px" AutoPostBack="True"></asp:DropDownList></td>
                        <td style="width: 15px" width="15"></td>
                        <td width="25%"></td>
                    </tr>
                    <tr>
                        <td style="width: 40px" width="40"></td>
                        <td style="width: 141px" width="141"></td>
                        <td style="width: 221px" width="221"></td>
                        <td style="width: 15px" width="15"></td>
                        <td width="25%"></td>
                    </tr>
                    <tr>
                        <td style="width: 40px" width="40"></td>
                        <td style="width: 141px" width="141"></td>
                        <td style="width: 221px" width="221"><font face="Tahoma">
                            <asp:Button ID="btnOKCancelInvoice" runat="server" CausesValidation="False" CssClass="queryButton"
                                Width="96px" Text="OK"></asp:Button></font>
                            <asp:Button ID="btnSaveCancel" runat="server" CausesValidation="False" CssClass="queryButton"
                                Width="96px" Text="Save"></asp:Button><font face="Tahoma">&nbsp;</font>
                            <asp:Button ID="btnCancelINVRemark" runat="server" CausesValidation="False" CssClass="queryButton"
                                Width="96px" Text="Cancel"></asp:Button></td>
                        <td style="width: 15px" width="15"></td>
                        <td width="25%"></td>
                    </tr>
                </table>
            </fieldset>
        </asp:Panel>
        <div style="z-index: 102; position: relative; width: 1813px; height: 750px; top: 23px; left: 24px"
            id="InvoiceDetails" runat="server" ms_positioning="GridLayout">
            <asp:Button Style="z-index: 102; position: absolute; top: 37px; left: 11px" ID="btnCancelDtl"
                runat="server" CausesValidation="False" CssClass="queryButton" Width="61px" Text="Cancel"></asp:Button>
            <asp:Label Style="z-index: 103; position: absolute; top: 62px; left: 18px" ID="lblErrorMsgDtl"
                runat="server" Height="19px" CssClass="errorMsgColor" Width="537px"></asp:Label>
            <asp:ValidationSummary Style="z-index: 104; position: absolute; top: 62px; left: 14px" ID="PageValidationSummaryDtl"
                runat="server" Height="39px" Width="346px" Visible="True" ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
            <asp:Label Style="z-index: 101; position: absolute; top: 9px; left: 15px" ID="lblMainTitleDtl"
                runat="server" Height="26px" CssClass="mainTitleSize" Width="477px">Invoice Details</asp:Label>
            <table style="z-index: 105; position: absolute; width: 727px; height: 139px; top: 95px; left: 7px"
                id="Table3" border="0" cellspacing="1" width="727" align="left" runat="server">
                <tr height="25">
                    <td style="width: 150px" bgcolor="#008499" valign="middle" width="150" align="left">&nbsp;
							<asp:Label ID="Label_50" runat="server" Height="16px" CssClass="tableLabel" Width="150px" BackColor="#008499"
                                Font-Size="11px" ForeColor="White">Customer ID</asp:Label></td>
                    <td style="width: 9px" valign="top" width="9"></td>
                    <td style="width: 1001px" valign="middle" width="1001">
                        <asp:Label ID="lblCustIDDtl" runat="server" CssClass="tableLabel" Width="143px"></asp:Label></td>
                    <td style="width: 150px" bgcolor="#008499" width="150" align="left">&nbsp;
							<asp:Label ID="Label_6" runat="server" Height="15px" CssClass="tableLabel" Width="150px" BackColor="#008499"
                                Font-Size="11px" ForeColor="White"> Province</asp:Label></td>
                    <td style="width: 8px" width="8"></td>
                    <td style="width: 61px" width="61">
                        <asp:Label ID="lblProvinceDtl" runat="server" CssClass="tableLabel" Width="150px"></asp:Label></td>
                </tr>
                <tr height="25">
                    <td style="width: 150px; height: 25px" bgcolor="#008499" width="96" align="left">&nbsp;
							<asp:Label ID="Label_48" runat="server" Height="15px" CssClass="tableLabel" Width="150px" BackColor="#008499"
                                Font-Size="11px" ForeColor="White">Customer Name</asp:Label></td>
                    <td style="width: 9px; height: 25px" width="9"></td>
                    <td style="width: 1001px; height: 25px" valign="middle" width="1001">
                        <asp:Label ID="lblCustNameDtl" runat="server" CssClass="tableLabel" Width="260px"></asp:Label></td>
                    <td style="width: 150px; height: 25px" bgcolor="#008499" width="150" align="left">&nbsp;
							<asp:Label ID="Label_49" runat="server" Height="14px" CssClass="tableLabel" Width="93px" BackColor="#008499"
                                Font-Size="11px" ForeColor="White"> Telephone</asp:Label></td>
                    <td style="width: 8px; height: 25px" width="8"></td>
                    <td style="width: 61px; height: 25px" width="61">
                        <asp:Label ID="lblTelephoneDtl" runat="server" CssClass="tableLabel" Width="150px"></asp:Label></td>
                </tr>
                <tr height="25">
                    <td style="width: 150px" bgcolor="#008499" valign="middle" width="150" align="left">&nbsp;
							<asp:Label ID="Label_45" runat="server" Height="15px" CssClass="tableLabel" Width="150px" BackColor="#008499"
                                Font-Size="11px" ForeColor="White"> Address 1</asp:Label></td>
                    <td style="width: 9px" width="9"></td>
                    <td style="width: 1001px" width="1001">
                        <asp:Label ID="lblAddress1Dtl" runat="server" CssClass="tableLabel" Width="303px"></asp:Label></td>
                    <td style="width: 150px; height: 25px" bgcolor="#008499" width="148" align="left">&nbsp;
							<asp:Label ID="Label_47" runat="server" Height="13px" CssClass="tableLabel" Width="83px" BackColor="#008499"
                                Font-Size="11px" ForeColor="White">Fax</asp:Label></td>
                    <td style="width: 8px" width="8"></td>
                    <td style="width: 61px" width="61">
                        <asp:Label ID="lblFaxDtl" runat="server" CssClass="tableLabel" Width="150px"></asp:Label></td>
                </tr>
                <tr height="25">
                    <td style="width: 150px" bgcolor="#008499" width="150">&nbsp;
							<asp:Label ID="Label_1" runat="server" Height="16px" CssClass="tableLabel" Width="150px" BackColor="#008499"
                                Font-Size="11px" ForeColor="White">Address 2</asp:Label></td>
                    <td style="width: 9px" width="9"></td>
                    <td style="width: 1001px" width="1001">
                        <asp:Label ID="lblAddress2Dtl" runat="server" CssClass="tableLabel" Width="301px"></asp:Label></td>
                    <td style="width: 150px" bgcolor="#008499" width="148" align="left">&nbsp;
							<asp:Label Style="white-space: nowrap" ID="Label_19" runat="server" Height="13px" CssClass="tableLabel"
                                Width="83px" BackColor="#008499" Font-Size="11px" ForeColor="White">Contact Person</asp:Label></td>
                    <td style="width: 8px" width="8" align="left"></td>
                    <td style="width: 61px" width="61" align="left">
                        <asp:Label ID="lblContactDtl" runat="server" CssClass="tableLabel" Width="300px"></asp:Label></td>
                </tr>
                <tr height="25">
                    <td style="width: 150px" bgcolor="#008499" valign="middle" width="150">&nbsp;
							<asp:Label ID="Label_2" runat="server" Height="16px" CssClass="tableLabel" Width="150px" BackColor="#008499"
                                Font-Size="11px" ForeColor="White">Postal Code</asp:Label></td>
                    <td style="width: 9px" width="9"></td>
                    <td style="width: 1001px" width="1001">
                        <asp:Label ID="lblPostalCodeDtl" runat="server" CssClass="tableLabel" Width="143px"></asp:Label></td>
                    <td style="width: 150px" bgcolor="#008499" width="150">&nbsp;
							<asp:Label ID="Label_46" runat="server" Height="16px" CssClass="tableLabel" Width="150px" BackColor="#008499"
                                Font-Size="11px" ForeColor="White">HC POD Required</asp:Label></td>
                    <td style="width: 8px" width="8" align="left"></td>
                    <td style="width: 61px" width="61" align="left">
                        <asp:Label ID="lblHC_POD_RequiredDtl" runat="server" CssClass="tableLabel" Width="150px" Font-Size="11px"></asp:Label></td>
                </tr>
            </table>
            <table style="z-index: 106; position: absolute; width: 727px; height: 139px; top: 244px; left: 6px"
                id="Table4" border="0" cellspacing="1" width="727" align="left" runat="server">
                <tbody>
                    <tr height="25">
                        <td style="width: 150px" bgcolor="#008499" valign="middle" width="150" align="left">&nbsp;
								<asp:Label Style="white-space: nowrap" ID="Label_3" runat="server" Height="15px" Width="150px"
                                    BackColor="#008499" Font-Size="11px" ForeColor="White">Invoice Number</asp:Label></td>
                        <td style="width: 9px" valign="top" width="9"></td>
                        <td style="width: 1001px" valign="middle" width="1001">
                            <asp:Label ID="lblInvNoDtl" runat="server" CssClass="tableLabel" Width="143px"></asp:Label></td>
                        <td style="width: 150px" bgcolor="#008499" width="150" align="left">&nbsp;
								<asp:Label ID="Label_7" runat="server" Height="15px" CssClass="tableLabel" Width="81px" BackColor="#008499"
                                    Font-Size="11px" ForeColor="White"> Due Date</asp:Label></td>
                        <td style="width: 8px" width="8"></td>
                        <td style="width: 61px" width="61">
                            <asp:Label ID="lblDueDateDtl" runat="server" CssClass="tableLabel" Width="150px"></asp:Label></td>
                    </tr>
                    <tr height="25">
                        <td style="width: 150px" bgcolor="#008499" valign="middle" width="96" align="left">&nbsp;
								<asp:Label ID="Label_9" runat="server" Height="16px" CssClass="tableLabel" Width="150px" BackColor="#008499"
                                    Font-Size="11px" ForeColor="White">Invoice Status</asp:Label></td>
                        <td style="width: 9px" valign="top" width="9"></td>
                        <td style="width: 1001px" valign="middle" width="1001">
                            <asp:Label ID="lblInvStatusDtl" runat="server" CssClass="tableLabel" Width="143px"></asp:Label></td>
                        <td style="width: 150px" bgcolor="#008499" width="150" align="left">&nbsp;
								<asp:Label ID="Label_11" runat="server" Height="14px" CssClass="tableLabel" Width="93px" BackColor="#008499"
                                    Font-Size="11px" ForeColor="White"> Total Amount</asp:Label></td>
                        <td style="width: 8px" width="8"></td>
                        <td style="width: 61px" width="61">
                            <asp:Label ID="lblTotalAmtDtl" runat="server" CssClass="tableLabel" Width="150px"></asp:Label></td>
                    </tr>
                    <tr height="25">
                        <td style="width: 150px; height: 25px" bgcolor="#008499" width="96" align="left">&nbsp;
								<asp:Label ID="Label_13" runat="server" Height="15px" CssClass="tableLabel" Width="150px" BackColor="#008499"
                                    Font-Size="11px" ForeColor="White">Payment Term</asp:Label></td>
                        <td style="width: 9px; height: 25px" width="9"></td>
                        <td style="width: 1001px; height: 25px" valign="middle" width="1001">
                            <asp:Label ID="lblPaymentTermDtl" runat="server" CssClass="tableLabel" Width="260px"></asp:Label></td>
                        <td style="width: 150px; height: 25px" bgcolor="#008499" width="148" align="left">&nbsp;
								<asp:Label ID="Label_15" runat="server" Height="13px" CssClass="tableLabel" Width="150px" BackColor="#008499"
                                    Font-Size="11px" ForeColor="White">Total Adjustments</asp:Label></td>
                        <td style="width: 8px; height: 25px" width="8"></td>
                        <td style="width: 61px; height: 25px" width="61">
                            <asp:Label ID="lblTotalAdjDtl" runat="server" CssClass="tableLabel" Width="150px"></asp:Label></td>
                    </tr>
                    <tr height="25">
                        <td style="width: 150px" bgcolor="#008499" valign="middle" width="96" align="left">&nbsp;
								<asp:Label ID="Label_17" runat="server" Height="15px" CssClass="tableLabel" Width="150px" BackColor="#008499"
                                    Font-Size="11px" ForeColor="White"> Invoice Date</asp:Label></td>
                        <td style="width: 9px" width="9"></td>
                        <td style="width: 1001px" width="1001">
                            <asp:Label ID="lblInvDateDtl" runat="server" CssClass="tableLabel" Width="303px"></asp:Label></td>
                        <td style="width: 150px" bgcolor="#008499" width="148" align="left">&nbsp;
								<asp:Label ID="lblCancel_AppHeadDtl" runat="server" Height="13px" CssClass="tableLabel" Width="150px"
                                    BackColor="#008499" Font-Size="11px" ForeColor="White"></asp:Label></td>
                        <td style="width: 8px" width="8"></td>
                        <td style="width: 61px" width="61">
                            <asp:Label ID="lblCancel_AppTextDtl" runat="server" CssClass="tableLabel" Width="150px"></asp:Label></td>
                    </tr>
                </tbody>
            </table>
            <table style="z-index: 105; position: absolute; top: 406px; left: 5px" width="890">
                <tr>
                    <td style="height: 177px">
                        <asp:DataGrid ID="dgEditDtl" runat="server" Height="95px" SelectedItemStyle-CssClass="gridFieldSelectedINV"
                            AutoGenerateColumns="False" ItemStyle-Height="20" OnUpdateCommand="dgEditDtl_dgUpdate" OnItemDataBound="dgEditDtl_dgBound">
                            <SelectedItemStyle CssClass="gridFieldSelectedINV"></SelectedItemStyle>
                            <ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
                            <Columns>
                                <asp:TemplateColumn>
                                    <HeaderStyle CssClass="gridBlueHeading"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton CommandName="Update" ID="imgUpdate" runat="server" ImageUrl="images/butt-update.gif"></asp:ImageButton>&nbsp;&nbsp;
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Consignment No.">
                                    <HeaderStyle HorizontalAlign="Center" Width="86px" CssClass="gridBlueHeading"></HeaderStyle>
                                    <ItemStyle Wrap="False" CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label Width="86px" CssClass="gridLabel" runat="server" ID="lblConsgnNo"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Type">
                                    <HeaderStyle HorizontalAlign="Center" Width="86px" CssClass="gridBlueHeading"></HeaderStyle>
                                    <ItemStyle Wrap="False" CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:DropDownList Width="86px" CssClass="gridDropDown" ID="ddlAdjType" runat="server"></asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Apply As*">
                                    <HeaderStyle HorizontalAlign="Center" Width="86px" CssClass="gridBlueHeading"></HeaderStyle>
                                    <ItemStyle Wrap="False" CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:DropDownList Width="86px" CssClass="gridDropDown" ID="ddlApplyAs" runat="server"></asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount">
                                    <HeaderStyle HorizontalAlign="Center" Width="100px" CssClass="gridBlueHeading"></HeaderStyle>
                                    <ItemStyle CssClass="gridField"></ItemStyle>
                                    <ItemTemplate>
                                        <cc1:msTextBox CssClass="gridTextBoxNumber" ID="txtAmount" runat="server" MaxLength="9" TextMaskType="msNumeric"
                                            NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2"></cc1:msTextBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Remark">
                                    <HeaderStyle HorizontalAlign="Center" Width="250px" CssClass="gridBlueHeading"></HeaderStyle>
                                    <ItemStyle CssClass="gridField"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:TextBox CssClass="gridTextBox" runat="server" ID="txtRemark"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid></td>
                </tr>
                <tr>
                    <td>
                        <asp:DataGrid ID="dgPreviewDtl" runat="server" Height="95px" Width="1800px" SelectedItemStyle-CssClass="gridFieldSelectedINV"
                            AutoGenerateColumns="False" ItemStyle-Height="20" OnItemDataBound="dgPreviewDtl_ItemDataBound"
                            OnItemCommand="dgPreviewDtl_Button" OnEditCommand="dgPreviewDtl_dgEditDtl">
                            <SelectedItemStyle CssClass="gridFieldSelectedINV"></SelectedItemStyle>
                            <ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
                            <Columns>
                                <asp:ButtonColumn ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridBlueHeading" ButtonType="LinkButton"
                                    Text="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;" CommandName="Edit">
                                    <HeaderStyle Width="10px" CssClass="gridBlueHeading"></HeaderStyle>
                                    <ItemStyle Width="10px" HorizontalAlign="Center"></ItemStyle>
                                </asp:ButtonColumn>
                                <asp:BoundColumn DataField="consignment_no" HeaderText="Consignment No.">
                                    <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle CssClass="gridField" Wrap="False"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="ref_no" HeaderText="Ref No.">
                                    <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle CssClass="gridField" Wrap="False"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="sender_name" HeaderText="Sender">
                                    <HeaderStyle HorizontalAlign="Center" Width="180px" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" CssClass="gridField" Wrap="False"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="sender_zipcode" HeaderText="Origin">
                                    <HeaderStyle Width="42px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle Width="42px" HorizontalAlign="Left" CssClass="gridField" Wrap="False"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="act_pickup_datetime" HeaderText="Pickup" DataFormatString="{0:dd/MM/yyyy HH:mm}">
                                    <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" CssClass="gridField" Wrap="False"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="recipient_name" HeaderText="Recipient">
                                    <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" CssClass="gridField" Wrap="False"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Dest.">
                                    <HeaderStyle Width="42px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle Width="42px" HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label Width="42px" CssClass="gridFieldNum" ID="lblDest" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_zipcode")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="act_delivery_date" HeaderText="Del.D/T" DataFormatString="{0:dd/MM/yyyy HH:mm}">
                                    <HeaderStyle Width="112px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle Width="112px" HorizontalAlign="Left" CssClass="gridField" Wrap="False"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="service_code" HeaderText="Service">
                                    <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" CssClass="gridField" Wrap="False"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Freight">
                                    <HeaderStyle Width="63px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle Width="63px" HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label Width="63px" CssClass="gridFieldNum" ID="lblFreight" runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"tot_freight_charge"))%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Ins.">
                                    <HeaderStyle Width="49px" HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gridBlueHeading"
                                        Wrap="False"></HeaderStyle>
                                    <ItemStyle Width="49px" HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label Width="49px" CssClass="gridFieldNum" ID="lblInsurance" runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"insurance_amt"))%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="VAS">
                                    <HeaderStyle Width="49px" HorizontalAlign="Center" VerticalAlign="Middle" CssClass="gridBlueHeading"
                                        Wrap="False"></HeaderStyle>
                                    <ItemStyle Width="49px" HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label Width="49px" CssClass="gridFieldNum" ID="lblVAS" runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"tot_vas_surcharge"))%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="ESA">
                                    <HeaderStyle Width="49px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle Width="49px" HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label Width="49px" CssClass="gridFieldNum" ID="lblESA" runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"esa_surcharge"))%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Other">
                                    <HeaderStyle Width="49px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle Width="49px" HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label Width="49px" CssClass="gridFieldNum" ID="lblOther" runat="server" Text='<%#utility.encodeNegValue(DataBinder.Eval(Container.DataItem,"other_surcharge"),(String)ViewState["m_format"])%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Except.">
                                    <HeaderStyle Width="49px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle Width="49px" HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label Width="49px" CssClass="gridFieldNum" ID="lblException" runat="server" Text='<%#utility.encodeNegValue(DataBinder.Eval(Container.DataItem,"total_exception"),(String)ViewState["m_format"])%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="MBG">
                                    <HeaderStyle Width="49px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle Width="49px" HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label Width="49px" CssClass="gridFieldNum" ID="lblMBG" runat="server" Text='<%#utility.encodeNegValue(DataBinder.Eval(Container.DataItem,"mbg_amount"),(String)ViewState["m_format"])%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Adj.">
                                    <HeaderStyle Width="49px" HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle Width="49px" HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label Width="49px" CssClass="gridFieldNum" ID="lblAdjustment" runat="server" Text='<%#utility.encodeNegValue(DataBinder.Eval(Container.DataItem,"invoice_adj_amount"),(String)ViewState["m_format"])%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Total">
                                    <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label CssClass="gridFieldNum" ID="lblTOT" runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"invoice_amt"))%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="adjusted_remark" HeaderText="Adjust Remark" Visible="False">
                                    <HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading" Wrap="False"></HeaderStyle>
                                    <ItemStyle CssClass="gridField" Wrap="False"></ItemStyle>
                                </asp:BoundColumn>
                            </Columns>
                            <PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
                        </asp:DataGrid></td>
                </tr>
            </table>
        </div>
        <asp:Panel ID="pnlinvupdate" Width="800" Visible="False" runat="server">
            <table id="table0" border="0" cellspacing="2" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <asp:Label ID="LblHeading" runat="server" Height="30px" CssClass="mainTitleSize" Width="442px">Invoice Payments Update</asp:Label></td>
                    <td>
                        <asp:Button ID="btnOK" runat="server" Height="21" CssClass="buttonProp" Width="85" Text="Save"></asp:Button>&nbsp;
							<asp:Button ID="btnCancelinvoice" runat="server" CssClass="buttonProp" Width="58" Text="Cancel"
                                Height="21"></asp:Button></td>
                </tr>
            </table>
            <table id="table1" border="0" cellspacing="2" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <asp:Label ID="ErrMsg" runat="server" Height="16px" CssClass="errorMsgColor" Width="100%"></asp:Label></td>
                </tr>
            </table>
            <table id="table2" border="0" cellspacing="2" cellpadding="0" width="100%">
                <tr>
                    <td width="20%">
                        <asp:Label ID="Label7" runat="server" Height="16" CssClass="tableLabel" Width="100%">Customer ID</asp:Label></td>
                    <td width="20%">
                        <asp:TextBox ID="txtCustID" runat="server" CssClass="textField" Width="116px" ReadOnly="True"></asp:TextBox></td>
                    <td width="20%">
                        <asp:Label ID="lblInvoiceStatus" runat="server" Height="16" CssClass="tableLabel" Width="100%">Invoice Status</asp:Label></td>
                    <td width="40%"><font face="Tahoma">
                        <asp:Label ID="lblInvDispStat" runat="server" Height="16" CssClass="tableLabel" Width="100px">Status</asp:Label></font></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label8" runat="server" Height="16" CssClass="tableLabel" Width="100%">Customer Name</asp:Label></td>
                    <td colspan="3">
                        <asp:Label ID="lblCustDispName" runat="server" Height="16" CssClass="tableLabel" Width="100%"
                            Font-Bold="True">Cust Name</asp:Label></td>
                </tr>
                <tr>
                    <td><font face="Tahoma">
                        <asp:Label ID="lblInvNo" runat="server" Height="16" CssClass="tableLabel" Width="100%">Invoice Number</asp:Label></font></td>
                    <td><font face="Tahoma">
                        <asp:TextBox ID="txtInvNo" runat="server" CssClass="textField" Width="116px" ReadOnly="True"></asp:TextBox></font></td>
                    <td>
                        <asp:Label ID="lblLastUpdate" runat="server" Height="16" CssClass="tableLabel" Width="100%"
                            Font-Bold="True"> Last Updated By/Date</asp:Label></td>
                    <td>
                        <asp:Label ID="lblDispUpdate" runat="server" Height="16" CssClass="tableLabel" Width="100%"
                            Font-Bold="True">Updated By / Date</asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblPrintedDueDate" runat="server" Height="16" CssClass="tableLabel" Width="100%"
                            Font-Bold="True">Printed Due Date</asp:Label></td>
                    <td>
                        <cc1:msTextBox ID="txtPrintedDueDate" TabIndex="3" runat="server" CssClass="textField" Width="116px"
                            TextMaskString="99/99/9999" TextMaskType="msDate" MaxLength="16" AutoPostBack="True" ReadOnly="True"></cc1:msTextBox></td>
                    <td>
                        <asp:Label ID="lblApproveDate" runat="server" Height="16" CssClass="tableLabel" Width="100%"
                            Font-Bold="True"> Approved By/Date</asp:Label></td>
                    <td>
                        <asp:Label ID="lblDispApprove" runat="server" Height="16" CssClass="tableLabel" Width="100%"
                            Font-Bold="True">Approved By / Date</asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblInternalDueDate" runat="server" Height="16" CssClass="tableLabel" Width="100%"
                            Font-Bold="True">Internal Due Date</asp:Label></td>
                    <td>
                        <cc1:msTextBox ID="txtInternalDueDate" TabIndex="3" runat="server" CssClass="textField" Width="116px"
                            TextMaskString="99/99/9999" TextMaskType="msDate" MaxLength="16" AutoPostBack="True" ReadOnly="True"></cc1:msTextBox></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblActualBPD" runat="server" Height="16" CssClass="tableLabel" Width="100%">Actual BPD</asp:Label></td>
                    <td>
                        <cc1:msTextBox ID="txtActualBPD" TabIndex="3" runat="server" CssClass="textField" Width="116px"
                            TextMaskString="99/99/9999" TextMaskType="msDate" MaxLength="16" AutoPostBack="True" ReadOnly="True"></cc1:msTextBox></td>
                    <td>
                        <asp:Label ID="lblBatchNo" runat="server" Height="16" CssClass="tableLabel" Width="100%">Batch Number</asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtBatchNo" runat="server" CssClass="textField" Width="116px" ReadOnly="True"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblPPD" runat="server" Height="16" CssClass="tableLabel" Width="100%">Promised Payment Date</asp:Label></td>
                    <td>
                        <cc1:msTextBox ID="txtPromisedPaymentDate" TabIndex="3" runat="server" CssClass="textField" Width="116px"
                            TextMaskString="99/99/9999" TextMaskType="msDate" MaxLength="16" AutoPostBack="True" ReadOnly="True"></cc1:msTextBox></td>
                    <td>
                        <asp:Label ID="lblTotalInvAmnt" runat="server" Height="16" CssClass="tableLabel" Width="100%">Total Invoice Amount</asp:Label></td>
                    <td>
                        <cc1:msTextBox ID="txtTotalInvAmnt" Height="21px" CssClass="textField" Text="" Width="116px" runat="server"
                            TextMaskString="#.00" TextMaskType="msNumeric" MaxLength="11" AutoPostBack="false" ReadOnly="True"
                            NumberScale="2" NumberPrecision="10" NumberMinValue="0" NumberMaxValue="99999999"></cc1:msTextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblAmntDate" runat="server" Height="16" CssClass="tableLabel" Width="100%">Amount Paid</asp:Label></td>
                    <td>
                        <cc1:msTextBox ID="txtAmntPaid" CssClass="textField" Text="" Width="116px" runat="server" TextMaskString="#.00"
                            TextMaskType="msNumeric" MaxLength="11" AutoPostBack="false" ReadOnly="True" NumberScale="2"
                            NumberPrecision="10" NumberMinValue="-99999999" NumberMaxValue="99999999" NumberMaxValueCOD="99999999"
                            NumberMinValueCustVAS="-99999999"></cc1:msTextBox></td>
                    <td>
                        <asp:Label ID="lblCurrentAmntPaid" runat="server" Height="16" CssClass="tableLabel" Width="100%">Current Amount Paid</asp:Label></td>
                    <td>
                        <cc1:msTextBox ID="txtCurrAmntPaid" Height="21px" CssClass="textField" Text="" Width="116px" runat="server"
                            TextMaskString="#.00" TextMaskType="msNumeric" MaxLength="11" AutoPostBack="false" ReadOnly="True"
                            NumberScale="2" NumberPrecision="10" NumberMinValue="0" NumberMaxValue="99999999"></cc1:msTextBox>
                        <cc1:msTextBox Style="display: none" ID="hidtxtCurrAmt" Height="21px" CssClass="textField" Text=""
                            Width="0px" runat="server" TextMaskString="#.00" TextMaskType="msNumeric" MaxLength="11" AutoPostBack="false"
                            ReadOnly="True" NumberScale="2" NumberPrecision="10" NumberMinValue="0" NumberMaxValue="99999999"></cc1:msTextBox></td>
                    </TD>
                </tr>
                <tr>
                    <td><font face="Tahoma">
                        <asp:Label ID="lblPaidDate" runat="server" Height="16" CssClass="tableLabel" Width="100%">Paid Date</asp:Label></font></td>
                    <td>
                        <cc1:msTextBox ID="txtPaidDate" TabIndex="3" runat="server" CssClass="textField" Width="116px"
                            TextMaskString="99/99/9999" TextMaskType="msDate" MaxLength="16" AutoPostBack="True" ReadOnly="True"></cc1:msTextBox></td>
                    <td>
                        <asp:Label ID="lblBalanceDue" runat="server" Height="16" CssClass="tableLabel" Width="100%">Balance Due</asp:Label></td>
                    <td>
                        <cc1:msTextBox ID="txtBalanceDue" CssClass="textField" Text="" Width="116px" runat="server" TextMaskString="#.00"
                            TextMaskType="msNumeric" MaxLength="11" AutoPostBack="false" ReadOnly="True" NumberScale="2"
                            NumberPrecision="10" NumberMinValue="0" NumberMaxValue="99999999"></cc1:msTextBox>
                        <cc1:msTextBox Style="display: none" ID="hidtxtBalanceDue" Height="21px" CssClass="textField" Text=""
                            Width="0px" runat="server" TextMaskString="#.00" TextMaskType="msNumeric" MaxLength="11" AutoPostBack="false"
                            ReadOnly="True" NumberScale="2" NumberPrecision="10" NumberMinValue="0" NumberMaxValue="99999999"></cc1:msTextBox></td>
                    </TD>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label ID="lblPaymentUpd" runat="server" Height="16" CssClass="tableLabel" Width="100%"
                            Font-Bold="True">Payment Updated By/Date</asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label9" runat="server" Height="16" CssClass="tableLabel" Width="100%" Visible="False">Link to Credit Note</asp:Label></td>
                    <td>
                        <asp:DropDownList ID="ddlLinkToCDN" TabIndex="13" runat="server" CssClass="textField" Width="116px"
                            Visible="False" Enabled="False">
                        </asp:DropDownList></td>
                    <td>
                        <asp:Label Style="display: none" ID="lblApplyAmt" runat="server" Height="16" CssClass="tableLabel"
                            Width="100%">Applied Amount</asp:Label></td>
                    <td>
                        <cc1:msTextBox Style="display: none" ID="txtApplyAmt" CssClass="textField" Text="" Width="116px"
                            runat="server" TextMaskString="#.00" TextMaskType="msNumeric" MaxLength="11" AutoPostBack="false"
                            ReadOnly="False" NumberScale="2" NumberPrecision="10" NumberMaxValue="99999999" NumberMaxValueCOD="99999999"
                            NumberMinValueCustVAS="0"></cc1:msTextBox></td>
                </tr>
            </table>
            <table id="table3" border="0" cellspacing="2" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <asp:Label ID="Label12" runat="server" Height="30px" CssClass="mainTitleSize" Width="442px">Associated Credit and Debit Notes</asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:DataGrid ID="dgAssociatedCNDN" Width="100%" AutoGenerateColumns="False" ItemStyle-Height="20"
                            runat="server" AllowPaging="True">
                            <ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
                            <Columns>
                                <asp:BoundColumn DataField="CNDN" HeaderText="CR/DB">
                                    <HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="gridHeading"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="CNDN_NO" HeaderText="Note ID">
                                    <HeaderStyle HorizontalAlign="Center" Width="40%" CssClass="gridHeading"></HeaderStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="STAT_TEXT" HeaderText="Status">
                                    <HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="gridHeading"></HeaderStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="total_amnt" HeaderText="Total Amount">
                                    <HeaderStyle HorizontalAlign="Center" Width="20%" CssClass="gridHeading"></HeaderStyle>
                                </asp:BoundColumn>
                            </Columns>
                            <PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
                        </asp:DataGrid>
                        <asp:TextBox Style="display: none" ID="txtUserName" runat="server" ReadOnly="True"></asp:TextBox></td>
                </tr>
                </TR>
            </table>
        </asp:Panel>
    </form>
    </FONT></TR></TBODY></TABLE></FORM>
</body>
</html>
