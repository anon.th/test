using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CrystalDecisions.Web;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource; 
using CrystalDecisions.CrystalReports.Engine;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.common.applicationpages; 
using com.ties.DAL;   
using System.Text;
using TIES.WebUI;
using System.IO;

namespace com.ties
{
	/// <summary>
	/// Summary description for BandQuotationRpt.
	/// </summary>
	public class BandQuotationReport : BasePopupPage
	{
		//Utility utility =null;
		string strappid=null;
		string strenterpriseid=null;
		bool bShowGroupTree=false;		
//		ReportDocument rptSource = null;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.TextBox txtTextToSearch;
		protected System.Web.UI.WebControls.DropDownList ddbZoom;
		protected System.Web.UI.WebControls.Label lblZoom;
		protected System.Web.UI.WebControls.Button btnExport;
		protected System.Web.UI.WebControls.DropDownList ddbExport;
		protected System.Web.UI.WebControls.Button btnMoveLast;
		protected System.Web.UI.WebControls.Button btnMoveNext;
		protected System.Web.UI.WebControls.Button btnMovePrevious;
		protected System.Web.UI.WebControls.Button btnMoveFirst;
		protected System.Web.UI.WebControls.Button btnShowGrpTree;
		protected CrystalDecisions.Web.CrystalReportViewer ReportViewer;
		protected com.common.util.msTextBox txtGoTo;
		protected System.Web.UI.WebControls.Label lblErrorMesssage;
		ReportDocument orpt = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//BandQuotationRpt  orpt = new BandQuotationRpt();
			String strQuatationNo = (String)Session["QUOTATIONNO"];
			String strCustType = (String)Session["CUSTTYPE"]; 
			String strFormid = (String)Session["FORMID"];
			//orpt =  new TIES.WebUI.BandQuotationRpt();
			orpt = new BandQuotationRpt();
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strappid = utility.GetAppID();
			strenterpriseid=utility.GetEnterpriseID();

			if (!IsPostBack)
			{
				LoadExportFormatList();				
				ViewState["ShowGroupTree"]=bShowGroupTree;
			}
			else
			{	
				bShowGroupTree=(bool)ViewState["ShowGroupTree"];
			}
			ReportViewer.DisplayGroupTree=bShowGroupTree;

			if(strFormid=="BandQuotation")
			{
				int i=0;
				DataSet dsBandQuotation = QuotationMgrDAl.CreateBandQuotationReport(strappid,strenterpriseid,strQuatationNo,strCustType ) ;
				BandQuotationds  dsXSDBandQuotation = new BandQuotationds();
				int iRecCnt = dsBandQuotation.Tables[0].Rows.Count;	
				for (i=0;i<iRecCnt;i++)
				{
					DataRow drEach = dsBandQuotation.Tables[0].Rows[i];
					DataRow drXSD =dsXSDBandQuotation.Tables["BandQuotation"].NewRow();
					if((drEach["enterprise_name"]!= null) && (!drEach["enterprise_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["enterprise_name"]= (string)drEach["enterprise_name"];
					}
					if((drEach["quotation_no"]!= null) && (!drEach["quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["quotation_no"]= (string)drEach["quotation_no"];
					}
					if((drEach["quotation_date"]!= null) && (!drEach["quotation_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["quotation_date"]= (DateTime)drEach["quotation_date"];
					}
					if((drEach["quotation_version"]!= null) && (!drEach["quotation_version"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
					  
						drXSD["quotation_version"]= Convert.ToInt32(drEach["quotation_version"]);
					}
					if((drEach["band_code"]!= null) && (!drEach["band_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["band_code"]= (string)drEach["band_code"];
					}
					if(strCustType=="C")
					{
						if((drEach["salesmanid"]!= null) && (!drEach["salesmanid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							drXSD["salesmanid"]= (string)drEach["salesmanid"];
						}
					}
					if((drEach["attention_to_person"]!= null) && (!drEach["attention_to_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["attention_to_person"]= (string)drEach["attention_to_person"];
					}
					if((drEach["copy_to_person"]!= null) && (!drEach["copy_to_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["copy_to_person"]= (string)drEach["copy_to_person"];
					}
					if((drEach["quotation_status"]!= null) && (!drEach["quotation_status"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["quotation_status"]= (string)drEach["quotation_status"];
					}
					if((drEach["e_contact_person"]!= null) && (!drEach["e_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["e_contact_person"]= (string)drEach["e_contact_person"];
					}
					if((drEach["e_contact_telephone"]!= null) && (!drEach["e_contact_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["e_contact_telephone"]= (string)drEach["e_contact_telephone"];
					}
					if((drEach["e_address1"]!= null) && (!drEach["e_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["e_address1"]= (string)drEach["e_address1"];
					}
					if((drEach["e_address2"]!= null) && (!drEach["e_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["e_address2"]= (string)drEach["e_address2"];
					}
					if((drEach["e_country"]!= null) && (!drEach["e_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["e_country"]= (string)drEach["e_country"];
					}
					if((drEach["e_contact_telephone"]!= null) && (!drEach["e_contact_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["e_contact_telephone"]= (string)drEach["e_contact_telephone"];
					}
					if((drEach["e_fax"]!= null) && (!drEach["e_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["e_fax"]= (string)drEach["e_fax"];
					}
					if((drEach["e_zipcode"]!= null) && (!drEach["e_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["e_zipcode"]= (string)drEach["e_zipcode"];
					}
					if((drEach["wt_increment_amt"]!= null) && (!drEach["wt_increment_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["wt_increment_amt"]= (decimal)drEach["wt_increment_amt"];
					}
					if((drEach["c_cust_name"]!= null) && (!drEach["c_cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["c_cust_name"]= (string)drEach["c_cust_name"];
					}
					if((drEach["c_contact_person"]!= null) && (!drEach["c_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["c_contact_person"]= (string)drEach["c_contact_person"];
					}
					if((drEach["c_email"]!= null) && (!drEach["c_email"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["c_email"]= (string)drEach["c_email"];
					}
					if((drEach["c_address1"]!= null) && (!drEach["c_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["c_address1"]= (string)drEach["c_address1"];
					}
					if((drEach["c_address2"]!= null) && (!drEach["c_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["c_address2"]= (string)drEach["c_address2"];
					}
					if((drEach["c_country"]!= null) && (!drEach["c_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["c_country"]= (string)drEach["c_country"];
					}
					if((drEach["c_zipcode"]!= null) && (!drEach["c_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["c_zipcode"]= (string)drEach["c_zipcode"];
					}
					if((drEach["c_telephone"]!= null) && (!drEach["c_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["c_telephone"]= (string)drEach["c_telephone"];
					}
					if((drEach["c_fax"]!= null) && (!drEach["c_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["c_fax"]= (string)drEach["c_fax"];
					}
					if((drEach["service_code"]!= null) && (!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["service_code"]= (string)drEach["service_code"];
					}
					if((drEach["service_description"]!= null) && (!drEach["service_description"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["service_description"]= (string)drEach["service_description"];
					}
					if((drEach["origin_zone_code"]!= null) && (!drEach["origin_zone_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["origin_zone_code"]= (string)drEach["origin_zone_code"];
					}
					//	drXSD["e_state_name"]= (string)drEach["e_state_name"];
					if((drEach["destination_zone_code"]!= null) && (!drEach["destination_zone_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["destination_zone_code"]= (string)drEach["destination_zone_code"];
					}
					if((drEach["start_wt"]!= null) && (!drEach["start_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["start_wt"]= (decimal)drEach["start_wt"];
					}
					if((drEach["end_wt"]!= null) && (!drEach["end_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["end_wt"]= (decimal)drEach["end_wt"];
					}
					if((drEach["start_price"]!= null) && (!drEach["start_price"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["start_price"]= (decimal)drEach["start_price"];
					}
					if((drEach["increment_price"]!= null) && (!drEach["increment_price"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["increment_price"]= (decimal)drEach["increment_price"];
					}
					if((drEach["percent_discount"]!= null) && (!drEach["percent_discount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["percent_discount"]= (decimal)drEach["percent_discount"];
					}
					if((drEach["service_charge_percent"]!= null) && (!drEach["service_charge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["service_charge_percent"]= (decimal)drEach["service_charge_percent"];
					}
					if((drEach["remark"]!= null) && (!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["remark"]= (string)drEach["remark"];
					}
					if((drEach["currency"]!= null) && (!drEach["currency"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["currency"]= (string)drEach["currency"];
					}

					if((drXSD["enterprise_name"]!= null) && (!drXSD["enterprise_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.enterprise_nameColumn.DefaultValue = drXSD["enterprise_name"];
					}
					if((drXSD["quotation_no"]!= null) && (!drXSD["quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.quotation_noColumn.DefaultValue = drXSD["quotation_no"];
					}
					if((drXSD["quotation_date"]!= null) && (!drXSD["quotation_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.quotation_dateColumn.DefaultValue = drXSD["quotation_date"];
					}
					if((drXSD["quotation_version"]!= null) && (!drXSD["quotation_version"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.quotation_versionColumn.DefaultValue = drXSD["quotation_version"];
					}
					if((drXSD["band_code"]!= null) && (!drXSD["band_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.band_codeColumn.DefaultValue = drXSD["band_code"];
					}
					if(strCustType=="C")
					{
						if((drXSD["salesmanid"]!= null) && (!drXSD["salesmanid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							dsXSDBandQuotation.BandQuotation.salesmanidColumn.DefaultValue = drXSD["salesmanid"];
						}
					}
					if((drXSD["attention_to_person"]!= null) && (!drXSD["attention_to_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.attention_to_personColumn.DefaultValue = drXSD["attention_to_person"];
					}
					if((drXSD["copy_to_person"]!= null) && (!drXSD["copy_to_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.copy_to_personColumn.DefaultValue = drXSD["copy_to_person"];
					}
					if((drXSD["quotation_status"]!= null) && (!drXSD["quotation_status"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.quotation_statusColumn.DefaultValue = drXSD["quotation_status"];
					}
					if((drXSD["e_contact_person"]!= null) && (!drXSD["e_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.e_contact_personColumn.DefaultValue = drXSD["e_contact_person"];
					}
					if((drXSD["e_address1"]!= null) && (!drXSD["e_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.e_address1Column.DefaultValue = drXSD["e_address1"];
					}
					if((drXSD["e_address2"]!= null) && (!drXSD["e_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.e_address2Column.DefaultValue = drXSD["e_address2"];
					}
					if((drXSD["c_country"]!= null) && (!drXSD["c_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.c_countryColumn.DefaultValue = drXSD["c_country"];
					}
					if((drXSD["e_zipcode"]!= null) && (!drXSD["e_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.e_zipcodeColumn.DefaultValue = drXSD["e_zipcode"];
					}
					if((drXSD["e_contact_telephone"]!= null) && (!drXSD["e_contact_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.e_contact_telephoneColumn.DefaultValue = drXSD["e_contact_telephone"];
					}
					if((drXSD["e_fax"]!= null) && (!drXSD["e_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.e_faxColumn.DefaultValue = drXSD["e_fax"];
					}
					if((drXSD["wt_increment_amt"]!= null) && (!drXSD["wt_increment_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.wt_increment_amtColumn.DefaultValue = drXSD["wt_increment_amt"];
					}
					if((drXSD["c_cust_name"]!= null) && (!drXSD["c_cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.c_cust_nameColumn.DefaultValue = drXSD["c_cust_name"];
					}
					if((drXSD["c_contact_person"]!= null) && (!drXSD["c_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.c_contact_personColumn.DefaultValue = drXSD["c_contact_person"];
					}
					if((drXSD["c_email"]!= null) && (!drXSD["c_email"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.c_emailColumn.DefaultValue = drXSD["c_email"];
					}
					if((drXSD["c_address1"]!= null) && (!drXSD["c_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.c_address1Column.DefaultValue = drXSD["c_address1"];
					}
					if((drXSD["c_address2"]!= null) && (!drXSD["c_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.c_address2Column.DefaultValue = drXSD["c_address2"];
					}
					if((drXSD["c_country"]!= null) && (!drXSD["c_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.c_countryColumn.DefaultValue = drXSD["c_country"];
					}
					if((drXSD["c_zipcode"]!= null) && (!drXSD["c_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.c_zipcodeColumn.DefaultValue = drXSD["c_zipcode"];
					}
					if((drXSD["c_telephone"]!= null) && (!drXSD["c_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.c_telephoneColumn.DefaultValue = drXSD["c_telephone"];
					}
								
					if((drXSD["c_fax"]!= null) && (!drXSD["c_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.c_faxColumn.DefaultValue = drXSD["c_fax"];
					}
					if((drXSD["service_code"]!= null) && (!drXSD["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.service_codeColumn.DefaultValue = drXSD["service_code"];
					}
					if((drXSD["service_description"]!= null) && (!drXSD["service_description"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.service_descriptionColumn.DefaultValue = drXSD["service_description"];
					}
					if((drXSD["origin_zone_code"]!= null) && (!drXSD["origin_zone_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.origin_zone_codeColumn.DefaultValue = drXSD["origin_zone_code"];
					}
					if((drXSD["destination_zone_code"]!= null) && (!drXSD["destination_zone_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.destination_zone_codeColumn.DefaultValue = drXSD["destination_zone_code"];
					}
					if((drXSD["start_wt"]!= null) && (!drXSD["start_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.start_wtColumn.DefaultValue = drXSD["start_wt"];
					}
					if((drXSD["end_wt"]!= null) && (!drXSD["end_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.end_wtColumn.DefaultValue = drXSD["end_wt"];
					}
					if((drXSD["start_price"]!= null) && (!drXSD["start_price"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.start_priceColumn.DefaultValue = drXSD["start_price"];
					}
					if((drXSD["increment_price"]!= null) && (!drXSD["increment_price"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.increment_priceColumn.DefaultValue = drXSD["increment_price"];
					}
					if((drXSD["percent_discount"]!= null) && (!drXSD["percent_discount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.percent_discountColumn.DefaultValue = drXSD["percent_discount"];
					}
					if((drXSD["service_charge_percent"]!= null) && (!drXSD["service_charge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.service_charge_percentColumn.DefaultValue = drXSD["service_charge_percent"];
					}
					if((drXSD["service_charge_percent"]!= null) && (!drXSD["service_charge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.service_charge_percentColumn.DefaultValue = drXSD["service_charge_percent"];
					}
					if((drXSD["service_charge_percent"]!= null) && (!drXSD["service_charge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.service_charge_percentColumn.DefaultValue = drXSD["service_charge_percent"];
					}
					if((drXSD["remark"]!= null) && (!drXSD["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.remarkColumn.DefaultValue = drXSD["remark"];
					}
					if((drXSD["currency"]!= null) && (!drXSD["currency"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.BandQuotation.currencyColumn.DefaultValue = drXSD["currency"];
					}
					
					//******************
//					if((drXSD["Conveyance_Code"]!= null) && (Utility.IsNotDBNull(drXSD["Conveyance_Code"])))
//					{
//						dsXSDBandQuotation.BandQuotation.currencyColumn.DefaultValue = drXSD["Conveyance_Code"];
//					}
//					if((drXSD["Conveyance_Description"]!= null) && (Utility.IsNotDBNull(drXSD["Conveyance_Description"])))
//					{
//						dsXSDBandQuotation.BandQuotation.currencyColumn.DefaultValue = drXSD["Conveyance_Description"];
//					}
//					if((drXSD["Length"]!= null) && (Utility.IsNotDBNull(drXSD["Length"])))
//					{
//						dsXSDBandQuotation.BandQuotation.currencyColumn.DefaultValue = drXSD["Length"];
//					}
//					if((drXSD["Breadth"]!= null) && (Utility.IsNotDBNull(drXSD["Breadth"])))
//					{
//						dsXSDBandQuotation.BandQuotation.currencyColumn.DefaultValue = drXSD["Breadth"];
//					}
//					if((drXSD["Height"]!= null) && (Utility.IsNotDBNull(drXSD["Height"])))
//					{
//						dsXSDBandQuotation.BandQuotation.currencyColumn.DefaultValue = drXSD["Height"];
//					}
//					if((drXSD["Max_Wt"]!= null) && (Utility.IsNotDBNull(drXSD["Max_Wt"])))
//					{
//						dsXSDBandQuotation.BandQuotation.currencyColumn.DefaultValue = drXSD["Max_Wt"];
//					}
//					if((drXSD["Origin_Zone_Code_C"]!= null) && (Utility.IsNotDBNull(drXSD["Origin_Zone_Code_C"])))
//					{
//						dsXSDBandQuotation.BandQuotation.currencyColumn.DefaultValue = drXSD["Origin_Zone_Code_C"];
//					}
//					if((drXSD["Destination_Zone_Code_C"]!= null) && (Utility.IsNotDBNull(drXSD["Destination_Zone_Code_C"])))
//					{
//						dsXSDBandQuotation.BandQuotation.currencyColumn.DefaultValue = drXSD["Destination_Zone_Code_C"];
//					}
//					if((drXSD["start_price_C"]!= null) && (Utility.IsNotDBNull(drXSD["start_price_C"])))
//					{
//						dsXSDBandQuotation.BandQuotation.currencyColumn.DefaultValue = drXSD["start_price_C"];
//					}
//					if((drXSD["increment_price_C"]!= null) && (Utility.IsNotDBNull(drXSD["increment_price_C"])))
//					{
//						dsXSDBandQuotation.BandQuotation.currencyColumn.DefaultValue = drXSD["increment_price_C"];
//					}
					
					dsXSDBandQuotation.Tables["BandQuotation"].Rows.Add(drXSD);   
									 
				}
				DataSet dsVas = QuotationMgrDAl.GetVas(strappid,strenterpriseid,strQuatationNo,strCustType);
				iRecCnt = dsVas.Tables[0].Rows.Count;
				for (i=0;i<iRecCnt;i++)
				{
					DataRow drEach = dsVas.Tables[0].Rows[i];
					DataRow drXSD =dsXSDBandQuotation.Tables["Vas"].NewRow();
				
					if((drEach["currency"]!= null) && (!drEach["currency"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["currency"]= (string)drEach["currency"];
					}
					if((drEach["vas_description"]!= null) && (!drEach["vas_description"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["vas_descp"]= (string)drEach["vas_description"];
					}
					if((drEach["surcharge"]!= null) && (!drEach["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						drXSD["surcharge"]= (decimal)drEach["surcharge"];
					}
					if((drXSD["vas_descp"]!= null) && (!drXSD["vas_descp"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.Vas.vas_descpColumn.DefaultValue = drXSD["vas_descp"];
					}
					if((drXSD["surcharge"]!= null) && (!drXSD["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dsXSDBandQuotation.Vas.surchargeColumn.DefaultValue = drXSD["surcharge"];

					}
					dsXSDBandQuotation.Tables["Vas"].Rows.Add(drXSD);  
				}
				orpt.SetDataSource(dsXSDBandQuotation); 
				int j = dsXSDBandQuotation.Tables["BandQuotation"].Rows.Count;
				i = dsXSDBandQuotation.Tables["Vas"].Rows.Count; 
				PageMargins margins;

							  
				margins = orpt.PrintOptions.PageMargins;
				int pageht = orpt.PrintOptions.PageContentHeight;
				int pagewidth=orpt.PrintOptions.PageContentWidth;  

				margins.bottomMargin = 200;
				margins.leftMargin = 50;
				margins.rightMargin = 50;
				margins.topMargin= 350;
				orpt.PrintOptions.ApplyPageMargins(margins);   
				ExportOptions expopt = orpt.ExportOptions;
				expopt.FormatOptions = ExportFormatType.PortableDocFormat;  
				ReportViewer.ReportSource = orpt;
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnShowGrpTree.Click += new System.EventHandler(this.btnShowGrpTree_Click);
			this.btnMoveFirst.Click += new System.EventHandler(this.btnMoveFirst_Click);
			this.btnMovePrevious.Click += new System.EventHandler(this.btnMovePrevious_Click);
			this.btnMoveNext.Click += new System.EventHandler(this.btnMoveNext_Click);
			this.btnMoveLast.Click += new System.EventHandler(this.btnMoveLast_Click);
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			this.ddbZoom.SelectedIndexChanged += new System.EventHandler(this.btnZoom_Click);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Export_Click(object sender, System.EventArgs e)
		{
//			ExportOptions ExportOptions = new ExportOptions();
//			DiskFileDestinationOptions DiskFileDstOptions = new DiskFileDestinationOptions();
//
//			String strExportFile =null;
//			
//			strExportFile = "C:\\" + Session.SessionID.ToString() + ".pdf"; 
//			//DiskFileDestinationOptions DiskFileDstOptions  = new DiskFileDestinationOptions();
//
//			DiskFileDstOptions.DiskFileName = strExportFile;
//			ExportOptions = orpt.ExportOptions;
//			ExportOptions.DestinationOptions = DiskFileDstOptions;
//			ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;    
//			ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat; 
//			orpt.Export();
// 
//			Response.ClearContent();
//			Response .ClearHeaders();
//			Response.ContentType= "application/pdf";
//			Response.WriteFile(strExportFile);
//			Response.Flush();
//			Response.Close();
// 
//			System.IO.File.Delete(strExportFile);  
//			
//		
		}
		
		private void btnShowGrpTree_Click(object sender, System.EventArgs e)
		{	
			if (!bShowGroupTree)
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					btnShowGrpTree.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Hide Group Tree", utility.GetUserCulture());
				}
				else
				{
					btnShowGrpTree.Text = "Hide Group Tree";
				}
				bShowGroupTree = true;
				ReportViewer.DisplayGroupTree = bShowGroupTree;				
				ViewState["ShowGroupTree"] = bShowGroupTree;
			}
			else
			{
				if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
				{
					btnShowGrpTree.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Show Group Tree", utility.GetUserCulture());
				}
				else
				{
					btnShowGrpTree.Text = "Show Group Tree";
				}
				bShowGroupTree = false;
				ReportViewer.DisplayGroupTree = bShowGroupTree;				
				ViewState["ShowGroupTree"] = bShowGroupTree;
			}

			/*if (btnShowGrpTree.Text=="Show Group Tree")
			{
				btnShowGrpTree.Text="Hide Group Tree";
				bShowGroupTree=true;
				ReportViewer.DisplayGroupTree=bShowGroupTree;				
				ViewState["ShowGroupTree"]=bShowGroupTree;
			}
			else
			{
				btnShowGrpTree.Text="Show Group Tree";
				bShowGroupTree=false;
				ReportViewer.DisplayGroupTree=bShowGroupTree;				
				ViewState["ShowGroupTree"]=bShowGroupTree;
			}*/
		}
		
		private void btnMoveFirst_Click(object sender, System.EventArgs e)
		{	
			ReportViewer.ShowFirstPage();
		}

		private void btnMovePrevious_Click(object sender, System.EventArgs e)
		{
			if (txtGoTo.Text.Trim() !="")
			{
				ReportViewer.ShowNthPage(System.Convert.ToInt32(txtGoTo.Text)+1);
				txtGoTo.Text="";
			} 
			ReportViewer.ShowPreviousPage();			
		}

		private void btnMoveNext_Click(object sender, System.EventArgs e)
		{
			if (txtGoTo.Text.Trim() !="")
			{
				ReportViewer.ShowNthPage(System.Convert.ToInt32(txtGoTo.Text)-1);
				txtGoTo.Text="";
			} 			
			ReportViewer.ShowNextPage();
		}

		private void btnMoveLast_Click(object sender, System.EventArgs e)
		{			
			ReportViewer.ShowLastPage();	
		}

		public void LoadExportFormatList()
		{
			ddbExport.Items.Clear();			
			ArrayList systemCodes = Utility.GetCodeValues(strappid,utility.GetUserCulture(),"export",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				ddbExport.Items.Add(lstItem);
			}
		}

		private void btnExport_Click(object sender, System.EventArgs e)
		{
			ExportOptions ExportOptions = new ExportOptions();
			DiskFileDestinationOptions DiskFileDstOptions = new DiskFileDestinationOptions();			
			String strExportFile =null;
			
			if (ddbExport.SelectedItem.Value.Trim()=="Formats:")
			{
				return;
			}
			
			string strExportFolder=System.Configuration.ConfigurationSettings.AppSettings["ReportExportFolder"];
			strExportFolder=Server.MapPath(strExportFolder);
			if (! System.IO.Directory.Exists(strExportFolder))
			{
				lblErrorMesssage.Text="Export folder not found. Please create the folder :'"+strExportFolder+"' and export";
				return;
			}

			///************IMPORTANT:Get the Export File directory from Web.config, WRITE Permissions must be given for
			///************"aspnet" user on the Folder read from "ReportExportFolder"
			strExportFile = strExportFolder+"\\"+Session.SessionID.ToString();			
			//			strExportFile = "C:\\" + Session.SessionID.ToString();			
			
			DiskFileDstOptions.DiskFileName = strExportFile;
			ExportOptions = orpt.ExportOptions;			
			ExportOptions.DestinationOptions = DiskFileDstOptions;
			ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;    

			switch (ddbExport.SelectedItem.Value.Trim().ToString())
			{
				case ".pdf":				
					ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;					
					ExportOptions.FormatOptions=new PdfRtfWordFormatOptions();
					break;
				case ".doc":
					ExportOptions.ExportFormatType = ExportFormatType.WordForWindows;					
					ExportOptions.FormatOptions=new PdfRtfWordFormatOptions();
					break;
				case ".rtf":
					ExportOptions.ExportFormatType = ExportFormatType.RichText;					
					ExportOptions.FormatOptions=new PdfRtfWordFormatOptions();
					break;
				case ".xls":
					if (ddbExport.SelectedItem.Text.IndexOf("Data",1)>0)
					{
						ExportOptions.ExportFormatType = ExportFormatType.ExcelRecord;
					}
					else
					{
						ExportOptions.ExportFormatType = ExportFormatType.Excel;
					}					
					ExportOptions.FormatOptions=new ExcelFormatOptions();
					break;
//				case ".rpt":
//					ExportOptions.ExportFormatType = ExportFormatType.CrystalReport;	
//					ExportOptions.FormatOptions=new PdfRtfWordFormatOptions();
//					break;
//				case ".htm":
//					ExportOptions.ExportFormatType = ExportFormatType.HTML40;										
//					HTMLFormatOptions htmlOpt=new HTMLFormatOptions();
//					htmlOpt.HTMLBaseFolderName="C:\\Crystal\\";
//                    htmlOpt.HTMLFileName=strExportFile="C:\\Crystal\\"+ Session.SessionID.ToString() + ddbExport.SelectedItem.Value.Trim();
//					htmlOpt.HTMLHasPageNavigator = true;
//					ExportOptions.FormatOptions=htmlOpt;
//					break;
	
				default:
					ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;					
					ExportOptions.FormatOptions=new PdfRtfWordFormatOptions();
					break;
			}		
			
			orpt.Export(); 
			Response.ClearContent();
			Response.ClearHeaders();	

			switch (ddbExport.SelectedItem.Value.Trim().ToString())
			{
				case ".pdf":
					Response.ContentType = "application/pdf";
					break;
				case ".doc":
				case ".rtf":
					Response.ContentType = "application/msword";
					break;
				case ".xls":
					Response.ContentType = "application/vnd.ms-excel";
					break;
				case ".htm":
					Response.ContentType = "text/html";
					break;
			}

			Response.WriteFile(strExportFile);
			Response.Flush();
			//Response.Close();
 
			//Delete the generated File from the DISK
			System.IO.File.Delete(strExportFile);      
			//To Print 3 Copies
			//orpt.PrintToPrinter(3, true, 1, 1);			
		
		}

		private void btnZoom_Click(object sender, System.EventArgs e)
		{
			if(System.Convert.ToInt32(ddbZoom.SelectedItem.Value)<125)
			{
				ReportViewer.Width=800;
				ReportViewer.Height=1200;
			}
			else if(System.Convert.ToInt32(ddbZoom.SelectedItem.Value)==125)
			{
				ReportViewer.Width=1200;
				ReportViewer.Height=1500;
			}
			else if(System.Convert.ToInt32(ddbZoom.SelectedItem.Value)==150)
			{
				ReportViewer.Width=1300;
				ReportViewer.Height=1850;
			}
			else if(System.Convert.ToInt32(ddbZoom.SelectedItem.Value)==175)
			{
				ReportViewer.Width=1400;
				ReportViewer.Height=2000;
			}
			else if(System.Convert.ToInt32(ddbZoom.SelectedItem.Value)==200)
			{
				ReportViewer.Width=1600;
				ReportViewer.Height=2200;
			}
			ReportViewer.Zoom (System.Convert.ToInt32(ddbZoom.SelectedItem.Value));
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			String strSearchText=txtTextToSearch.Text.Trim();
			if (strSearchText=="")
			{
				return;
			}			
			ReportViewer.SearchForText(strSearchText,SearchDirection.Forward);	
		}
		

		

		
	}
}
