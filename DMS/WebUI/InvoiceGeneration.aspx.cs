using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Threading;
using com.common.util;
using com.ties.DAL;
using com.ties.BAL;
using com.ties.classes;
using com.common.classes;
using com.common.applicationpages;
using TIESDAL;
using System.IO;



namespace com.ties
{
	/// <summary>
	/// Summary description for InvoiceGeneration.
	/// </summary>
	public class InvoiceGeneration : BasePage
	{

		//Utility utility = null;
		private String m_strAppID;
		private String m_strEnterpriseID;
		DataSet dsInvoice = new DataSet();
		protected System.Web.UI.WebControls.Button btnGenerate;
		protected System.Web.UI.WebControls.Button btnClear;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label lblInvoiceFor;
		protected System.Web.UI.WebControls.RadioButton rbCashConsig;
		protected System.Web.UI.WebControls.CheckBox chkShipByCash;
		protected System.Web.UI.WebControls.CheckBox chkShipByCredit;
		protected System.Web.UI.WebControls.RadioButton rbCreditConsig;
		protected System.Web.UI.WebControls.Label lblCustomerCode;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentId;
		protected System.Web.UI.WebControls.Label lblCustomerName;
		protected com.common.util.msTextBox txtCustomerName;
		protected System.Web.UI.WebControls.Label lblInvoiceToDate;
		protected com.common.util.msTextBox txtLastPickupDate;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divInvoiceGen;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentExcepPODRepQry;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.DataGrid dgPreview;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label lblInvoiceType;
		protected System.Web.UI.WebControls.Label Label50;
		protected System.Web.UI.WebControls.Label lblCustID;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label lblProvince;
		protected System.Web.UI.WebControls.Label Label48;
		protected System.Web.UI.WebControls.Label lblCustName;
		protected System.Web.UI.WebControls.Label Label49;
		protected System.Web.UI.WebControls.Label lblTelephone;
		protected System.Web.UI.WebControls.Label Label45;
		protected System.Web.UI.WebControls.Label lblAddress1;
		protected System.Web.UI.WebControls.Label Label47;
		protected System.Web.UI.WebControls.Label lblFax;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblAddress2;
		protected System.Web.UI.WebControls.Label lblHC_POD;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label lblPostalCode;
		protected System.Web.UI.WebControls.Label Label46;
		protected System.Web.UI.WebControls.Label lblHC_POD_Required;
		protected System.Web.UI.WebControls.Button btnSelectAll;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnSelectAllHC;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected com.common.util.msTextBox txtGoToRec;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Label lblNumRec;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divInvoiceGenPreview;
		protected System.Web.UI.HtmlControls.HtmlForm Form1;
		protected System.Web.UI.HtmlControls.HtmlTable Table10;
		//InvoiceCalculationMgrBAL InvoiceMgrBAL = null;
		InvoiceJobMgrBAL m_objInvoiceJobMgrBAL = null;

		private DataSet dsInvoiceHeader,dsInvoiceResult,dsExport;
		private DataSet dsInvoiceQuery;
		private Utility ut;
		static private int m_iSetSize = 50;
		private SessionDS m_sdsInvoiceHeader = null;
		protected System.Web.UI.WebControls.Button btnGenerateInv;
		string strInvType;
		protected System.Web.UI.WebControls.Button btnExport;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentId_Cash;
		protected System.Web.UI.WebControls.RadioButton Radiobutton1;
		protected System.Web.UI.WebControls.RadioButton Radiobutton2;
		protected System.Web.UI.HtmlControls.HtmlTable Table1X;
		protected System.Web.UI.WebControls.Panel Panel1;
		protected com.common.util.msTextBox txtPayerName;
		protected System.Web.UI.WebControls.RadioButton rdtInvoiceConsig;
		protected System.Web.UI.WebControls.RadioButton rbtInvoiceCustomer;
		protected System.Web.UI.WebControls.RadioButton rbtDomesticCon;
		protected System.Web.UI.WebControls.RadioButton rbtExports;
		protected System.Web.UI.WebControls.RadioButton rbtImports;
		protected System.Web.UI.WebControls.Label lbPayerAccount;
		protected System.Web.UI.WebControls.DropDownList dbcPayerAccount;
		protected System.Web.UI.WebControls.Label lb;
		protected System.Web.UI.WebControls.Label lbManifested;
		protected com.common.util.msTextBox txtManFrom;
		protected System.Web.UI.WebControls.Label lbManTo;
		protected com.common.util.msTextBox txtMan;
		protected System.Web.UI.WebControls.RadioButton rbtDomesticCons;
		protected System.Web.UI.WebControls.Label lbPayerName;
		protected com.common.util.msTextBox txtManTo;
		protected System.Web.UI.WebControls.Label lbInvoiNumIs;
		protected System.Web.UI.WebControls.RadioButton rbtConsignmentNum;
		protected System.Web.UI.WebControls.RadioButton rbtAutoGen;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.HtmlControls.HtmlTable tbDomestic;
		protected System.Web.UI.WebControls.Label lbConsInvoiceType;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label lbConsManifestedFrom;
		protected System.Web.UI.WebControls.Label lbConsManifestedToDis;
		protected System.Web.UI.WebControls.Label lbConsManifestedTo;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label lbPayerIDDis;
		protected System.Web.UI.WebControls.Label lbPayerNameDis;
		protected System.Web.UI.WebControls.DataGrid dgPreview2;
		string strInvJobID="";

	
		private void EnterpriseConfigurations()
		{
			InvoiceGenerationPreviewConfigurations conf = new InvoiceGenerationPreviewConfigurations();
			conf = EnterpriseConfigMgrDAL.GetInvoiceGenerationPreviewConfigurations(utility.GetAppID(),utility.GetEnterpriseID());

			//this.dgPreview2.Columns[16].Visible = conf.InsSupportedbyEnterprise;
			//this.dgPreview2.Columns[18].Visible = conf.ESAupportedbyEnterprise;

			if(rbtExports.Checked)
			{
				this.dgPreview2.Columns[4].Visible = false;
				this.dgPreview2.Columns[2].Visible = true;
				this.dgPreview2.Columns[3].Visible = true;
			}
			else
			{
				this.dgPreview2.Columns[4].Visible = true;
				this.dgPreview2.Columns[2].Visible = false;
				this.dgPreview2.Columns[3].Visible = false;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			dbCmbAgentId.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbAgentId_Cash.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];  //Jeab 29 Nov 2011
			//dbcPayerAccount.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			ut = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);

			this.btnGenerate.Attributes.Add("onclick","javascript:document.getElementById('"+ this.btnGenerate.ClientID + "').disabled=true;" + this.GetPostBackEventReference(this.btnGenerate));
			this.btnGenerateInv.Attributes.Add("onclick","javascript:document.getElementById('"+ this.btnGenerateInv.ClientID + "').disabled=true;" + this.GetPostBackEventReference(this.btnGenerateInv));

			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			if (!Page.IsPostBack)
			{		
				DisplayPayerAccount();
				txtManFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
				txtManTo.Text = DateTime.Now.ToString("dd/MM/yyyy");

				if(this.divInvoiceGen.Visible)
					QueryMode();

				if(this.divInvoiceGenPreview.Visible)
				{
					ViewState["CPMode"] = ScreenMode.ExecuteQuery;
					ViewState["CPOperation"] = Operation.None;
					ViewState["IsTextChanged"] = false;
					ViewState["currentPage"] = 0;
					ViewState["currentSet"] = 0;
					
					this.GetRecSet();
					DisplayCurrentPage();
				}
			}
			else
			{
				if(dbcPayerAccount.SelectedItem.Value != "")
				{
					DataSet dsCustData = CustomerScheduledPickupsDAL.GetCustData(utility.GetAppID(), utility.GetEnterpriseID(),dbcPayerAccount.SelectedItem.Value);
					if(dsCustData.Tables[0].Rows.Count > 0) 
					{
						this.txtPayerName.Text = dsCustData.Tables[0].Rows[0]["cust_name"].ToString();
					}
					else
						this.txtPayerName.Text = "";
				}
				if(this.divInvoiceGen.Visible)
				{
					//Jeab 29 Nov 2011
					if(rbCashConsig.Checked  != true)
					{
						if(this.dbCmbAgentId.Text.Trim()!=""&&this.dbCmbAgentId.Text.Trim()!="CASH")
						{
							DataSet dsCustData = CustomerScheduledPickupsDAL.GetCustData(utility.GetAppID(), utility.GetEnterpriseID(),
								this.dbCmbAgentId.Text.Trim());
							if(dsCustData.Tables[0].Rows.Count > 0) 
							{
								this.txtCustomerName.Text = dsCustData.Tables[0].Rows[0]["cust_name"].ToString();
							}
							else
								this.txtCustomerName.Text = "";
						}
						else
							this.txtCustomerName.Text = "";
					}
					else
					{
						if(this.dbCmbAgentId_Cash.Text.Trim()!="")
						{
							DataSet dsCustData = CustomerScheduledPickupsDAL.GetCustData(utility.GetAppID(), utility.GetEnterpriseID(),
								this.dbCmbAgentId_Cash.Text.Trim());
							if(dsCustData.Tables[0].Rows.Count > 0) 
							{
								this.txtCustomerName.Text = dsCustData.Tables[0].Rows[0]["cust_name"].ToString();
							}
							else
								this.txtCustomerName.Text = "";
						}
						else
							this.txtCustomerName.Text = "";
					}
					//Jeab 29 Nov 2011  =========> End
				}

                if (this.divInvoiceGenPreview.Visible)
                {
                    m_sdsInvoiceHeader = (SessionDS)Session["SESSION_DS_Invoice"];
                    dsInvoiceQuery = (DataSet)Session["dsPreviewQUERY"];
                }
            }

			lblErrorMessage.Text = "";

			if(rdtInvoiceConsig.Checked)
			{
				lblInvoiceFor.Visible = false;
				rbCashConsig.Visible = false;
				chkShipByCash.Visible = false;
				chkShipByCredit.Visible = false;
				rbCreditConsig.Visible = false;
				lblCustomerCode.Visible = false;
				dbCmbAgentId.Visible = false;
				dbCmbAgentId_Cash.Visible = false;
				lblCustomerName.Visible = false;
				txtCustomerName.Visible = false;
				lblInvoiceToDate.Visible = false;
				txtLastPickupDate.Visible = false;

				if(rbtDomesticCons.Checked)
				{
					dbcPayerAccount.Enabled = false;
					txtPayerName.Enabled = false;
				}
			}
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object AgentIdServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strAgentName="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{			
				if(args.ServerState["strAgentName"] != null && args.ServerState["strAgentName"].ToString().Length > 0)
				{
					strAgentName = args.ServerState["strAgentName"].ToString();					
				}	
			}
			
			DataSet dataset = DbComboDAL.PayerIDQueryInvoiceGeneration(strAppID,strEnterpriseID,args, "R", strAgentName);
			return dataset;
		}

		private void DisplayPayerAccount()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			String strAgentName="";
			DataSet dataset = DbComboDAL.PayerIDQueryInvoiceGeneration2(strAppID,strEnterpriseID,null, "R", strAgentName);
			
			dbcPayerAccount.DataTextField = "DbComboText";
			dbcPayerAccount.DataValueField = "DbComboValue";
			dbcPayerAccount.DataSource = dataset.Tables[0];
			dbcPayerAccount.DataBind();

		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object PayerIdServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strAgentName="";
			//DataSet dataset = DbComboDAL.PayerIDQueryInvoiceGeneration2(strAppID,strEnterpriseID,args, "R", strAgentName);
			

			//String strAgentName="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{			
				if(args.ServerState["strAgentName"] != null && args.ServerState["strAgentName"].ToString().Length > 0)
				{
					strAgentName = args.ServerState["strAgentName"].ToString();					
				}	
			}
			
			DataSet dataset = DbComboDAL.PayerIDQueryInvoiceGeneration2(strAppID,strEnterpriseID,args, "R", strAgentName);
			return dataset;
		}

		//Jeab 29 Nov 2011
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object AgentIdCashServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strAgentName="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{			
				if(args.ServerState["strAgentName"] != null && args.ServerState["strAgentName"].ToString().Length > 0)
				{
					strAgentName = args.ServerState["strAgentName"].ToString();					
				}	
			}
			
			DataSet dataset = DbComboDAL.PayerIDQueryInvoiceGeneration(strAppID,strEnterpriseID,args, "CASH", strAgentName);
			return dataset;
		}
		//Jeab 29 Nov 2011  =========> End


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			this.rdtInvoiceConsig.CheckedChanged += new System.EventHandler(this.rdtInvoiceConsig_CheckedChanged);
			this.rbtInvoiceCustomer.CheckedChanged += new System.EventHandler(this.rbtInvoiceCustomer_CheckedChanged);
			this.rbtDomesticCons.CheckedChanged += new System.EventHandler(this.rbtDomesticCons_CheckedChanged);
			this.rbtExports.CheckedChanged += new System.EventHandler(this.rbtExports_CheckedChanged);
			this.rbtImports.CheckedChanged += new System.EventHandler(this.rbtImports_CheckedChanged);
			this.txtManFrom.TextChanged += new System.EventHandler(this.txtManFrom_TextChanged);
			this.rbCashConsig.CheckedChanged += new System.EventHandler(this.rbCashConsig_CheckedChanged);
			this.chkShipByCash.CheckedChanged += new System.EventHandler(this.chkShipByCash_CheckedChanged);
			this.rbCreditConsig.CheckedChanged += new System.EventHandler(this.rbCreditConsig_CheckedChanged);
			this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.btnGenerateInv.Click += new System.EventHandler(this.btnGenerateInv_Click);
			this.btnSelectAllHC.Click += new System.EventHandler(this.btnSelectAllHC_Click);
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.txtGoToRec.TextChanged += new System.EventHandler(this.txtGoToRec_TextChanged);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

#region "InvoiceGEN"
		private void btnGenerate_Click(object sender, System.EventArgs e)
		{
			if(rbtInvoiceCustomer.Checked)
			{
				#region Invoice by Customer

				if(this.rbCashConsig.Checked)
					if(!(this.chkShipByCash.Checked) && !(this.chkShipByCredit.Checked))
					{
						this.lblErrorMessage.Text="Please Select Ship By Cash and/ or Ship By Credit.";
						this.btnGenerate.Enabled=true;
						return;
					}

				if(this.rbCreditConsig.Checked)
					if(!(this.dbCmbAgentId.Text.Length>0))
					{
						this.lblErrorMessage.Text="Please Select Customer ID.";
						this.btnGenerate.Enabled=true;
						return;
					}
				//Jeab 29 Nov 2011
				if(this.rbCashConsig.Checked)
					if(!(this.dbCmbAgentId_Cash.Text.Length>0))
					{
						this.lblErrorMessage.Text="Please Select Customer ID.";
						this.btnGenerate.Enabled=true;
						return;
					}
				//Jeab 29 Nov 2011  =========> End

				dsInvoice = GetFilterData();
			
				if(InvoiceGenerationMgrDAL.CheckExistingInvoiceProcess(m_strAppID, m_strEnterpriseID, ut.GetUserID(),dsInvoice , Session.SessionID))
				{
					EnableCustomer();
					this.lblErrorMessage.Text="Invoice Generation is in progress. Please try again later.";
					this.btnGenerate.Enabled=true;
					return;
				}
				string autogen =  "2";
				if(rbtConsignmentNum.Checked)
					autogen = "1";

				if(!InvoiceGenerationMgrDAL.GenTempInvoicePreview(m_strAppID,m_strEnterpriseID,ut.GetUserID(),dsInvoice, Session.SessionID, autogen))
				{
					this.lblErrorMessage.Text="No consignments for generating invoice.";
					this.btnGenerate.Enabled=true;
					return;
				}

				this.btnGenerate.Enabled=false;
				Session["dsPreviewQUERY"] = dsInvoice;
				this.divInvoiceGen.Visible=false;
				this.divInvoiceGenPreview.Visible=true;
	
				m_strAppID = ut.GetAppID();
				m_strEnterpriseID = ut.GetEnterpriseID();
				EnableNavigationButtons(true,true,true,true);
				ViewState["CPMode"] = ScreenMode.ExecuteQuery;
				ViewState["CPOperation"] = Operation.None;
				ViewState["IsTextChanged"] = false;
				ViewState["currentPage"] = 0;
				ViewState["currentSet"] = 0;
				//CREATE DATA INTO TEMP TABLE
				//END CREATE DATA INTO TEMP TABLE
				this.GetRecSet();
				DisplayCurrentPage();

				Table10.Visible = true;
				tbDomestic.Visible = false;
				dgPreview.Visible = true;
				dgPreview2.Visible = false;

				#endregion

				Table10.Visible = true;
				tbDomestic.Visible = false;
				dgPreview.Visible = true;
				dgPreview2.Visible = false;
			}
			else if(rdtInvoiceConsig.Checked)
			{
				if(txtManFrom.Text == "" && txtManTo.Text == "")
				{
					lblErrorMessage.Text = "Please enter a date range";
					return;
				}
				if(txtManFrom.Text == "")
				{
					lblErrorMessage.Text = "Please enter Manifested Date From";
					return;
				}

				if(rbtImports.Checked && dbcPayerAccount.SelectedItem.Text == "")
				{
					lblErrorMessage.Text = "Please select Payer Account";
					return;
				}

				#region Invoice by Consignment

				//Gen Invoice
				dsInvoice = GetFilterData();

				string autogen =  "2";
				if(rbtConsignmentNum.Checked)
					autogen = "1";

				if(!InvoiceGenerationMgrDAL.GenTempInvoicePreview(m_strAppID,m_strEnterpriseID,ut.GetUserID(),dsInvoice, Session.SessionID, autogen))
				{
					this.lblErrorMessage.Text="No consignments for generating invoice.";
					this.btnGenerate.Enabled=true;
					return;
				}
				//End Gen Invoice

				this.btnGenerate.Enabled=false;
				Session["dsPreviewQUERY"] = dsInvoice;
				this.divInvoiceGen.Visible=false;
				this.divInvoiceGenPreview.Visible=true;

				m_strAppID = ut.GetAppID();
				m_strEnterpriseID = ut.GetEnterpriseID();
				EnableNavigationButtons(true,true,true,true);
				ViewState["CPMode"] = ScreenMode.ExecuteQuery;
				ViewState["CPOperation"] = Operation.None;
				ViewState["IsTextChanged"] = false;
				ViewState["currentPage"] = 0;
				ViewState["currentSet"] = 0;

				if(rbtDomesticCons.Checked)
					lbConsInvoiceType.Text = "Domestic";
				else if(rbtExports.Checked)
					lbConsInvoiceType.Text = "Exports";
				else if(rbtImports.Checked)
					lbConsInvoiceType.Text = "Imports/On-forwarding";

				//CLEAR ALL HEADER
				this.lblInvoiceType.Text = "";
				this.lblCustID.Text = "";
				this.lblCustName.Text = "";
				this.lblAddress1.Text = "";
				this.lblAddress2.Text = "";
				this.lblPostalCode.Text = "";
				this.lblProvince.Text = "";
				this.lblTelephone.Text = "";
				this.lblFax.Text = "";
				this.lblHC_POD_Required.Text = "";
				string strCPayment_mode = "";
				//END OF CLEAR ALL HEADER		

				lbConsManifestedFrom.Text = txtManFrom.Text;
				lbConsManifestedTo.Text = txtManTo.Text;
				
				if(rbtImports.Checked == true)
				{
					lbPayerIDDis.Text = dbcPayerAccount.SelectedItem.Value;
					lbPayerNameDis.Text = txtPayerName.Text;
				}
				else
				{
					lbPayerIDDis.Text = "";
					lbPayerNameDis.Text = "";
				}

				this.btnSelectAllHC.Enabled=false;

				string invGenType = "";
				string payerAccount = "";

				if(rbtDomesticCons.Checked)
					invGenType = "D";
				else if(rbtExports.Checked)
					invGenType = "E";
				else if(rbtImports.Checked)
				{
					invGenType = "I";
					payerAccount = dbcPayerAccount.SelectedItem.Value;
				}

				dsInvoiceResult = InvoiceGenerationMgrDAL.PreviewInvoiceHeaderDetails(m_strAppID,m_strEnterpriseID,ut.GetUserID(), Session.SessionID, "1");

				ViewState["No_Of_Pages"] = dsInvoiceResult.Tables[1].Rows[0][0].ToString();
				ViewState["First_Page"] = dsInvoiceResult.Tables[1].Rows[0][1].ToString();
				ViewState["Previous_Page"] = dsInvoiceResult.Tables[1].Rows[0][2].ToString();
				ViewState["Current_Page"] = dsInvoiceResult.Tables[1].Rows[0][3].ToString();
				ViewState["Next_Page"] = dsInvoiceResult.Tables[1].Rows[0][4].ToString();
				ViewState["Last_Page"] = dsInvoiceResult.Tables[1].Rows[0][5].ToString();
				ViewState["Text_Label"] = dsInvoiceResult.Tables[1].Rows[0][6].ToString();

				txtGoToRec.Text = ViewState["Current_Page"].ToString();
				lblNumRec.Text = ViewState["Text_Label"].ToString();

				//BindGrid(dsInvoiceResult, "CREDIT");
				BindGridInvoiceByConsignment(dsInvoiceResult);

				#endregion

				//Label5.Visible = false;
				Table10.Visible = false;
				tbDomestic.Visible = true;
				dgPreview.Visible = false;
				dgPreview2.Visible = true;
			}

		}

		private void btnClear_Click(object sender, System.EventArgs e)
		{
			QueryMode();

			if(!rbtInvoiceCustomer.Checked)
				dbCmbAgentId.Visible = false;
			else
				dbCmbAgentId.Visible = true;

			rbtDomesticCons.Checked = true;
			//dbcPayerAccount.SelectedValue = "";
			dbcPayerAccount.SelectedItem.Value = "";
			//dbcPayerAccount.SelectedItem = "";
			//dbcPayerAccount.SelectedItem = "";
			txtPayerName.Text = "";
			txtManFrom.Text = "";
			txtManTo.Text = "";
			rbtConsignmentNum.Checked = true;

			btnGenerate.Enabled = true;
			txtManFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
			txtManTo.Text = DateTime.Now.ToString("dd/MM/yyyy");
		}

		private void rbCashConsig_CheckedChanged(object sender, System.EventArgs e)
		{
			dbCmbAgentId.Text="";
			dbCmbAgentId.Value="";
			this.lblErrorMessage.Text="";
			this.lblErrorMsg.Text="";
			this.chkShipByCash.Checked=true;
			this.chkShipByCredit.Checked=true;
			//Jeab 23 Nov 2011
//			dbCmbAgentId.EnableViewState =false;
//			this.dbCmbAgentId.Enabled=false;
//			DisableCustomer();
			txtCustomerName.Text = "";
			this.dbCmbAgentId_Cash.Enabled=true;
			dbCmbAgentId_Cash.EnableViewState =true;
			this.txtCustomerName.BackColor = System.Drawing.Color.White;
			this.txtCustomerName.ForeColor = System.Drawing.Color.Black;
			EnableCustomer();
			this.chkShipByCash.Enabled=true;
			this.chkShipByCredit.Enabled=true;
			dbCmbAgentId_Cash.Visible=true;
			dbCmbAgentId_Cash.Width=210;
			dbCmbAgentId.Visible=false;
			dbCmbAgentId.Width =0;
			//Jeab 23 Nov 2011  =========> End
		}

		private void rbCreditConsig_CheckedChanged(object sender, System.EventArgs e)
		{
			this.chkShipByCash.Checked=false;
			this.chkShipByCredit.Checked=false;
			this.lblErrorMessage.Text="";
			this.lblErrorMsg.Text="";
			txtCustomerName.Text = "";
			this.dbCmbAgentId.Enabled=true;
			dbCmbAgentId.EnableViewState =true;
			this.txtCustomerName.BackColor = System.Drawing.Color.White;
			this.txtCustomerName.ForeColor = System.Drawing.Color.Black;
			EnableCustomer();
			//Jeab 21 Dec 2011
			dbCmbAgentId_Cash.Visible=false;
			dbCmbAgentId.Visible=true;
			//Jeab 21 Dec 2011  =========>  End
		}

		private void EnableCustomer()
		{
			this.dbCmbAgentId.Value="";
			this.dbCmbAgentId.Text="";
			//Jeab 21 Dec 2011
			this.dbCmbAgentId_Cash.Value="";
			this.dbCmbAgentId_Cash.Text="";
			//Jeab 21 Dec 2011  =========>  End
			txtCustomerName.Text = null;
			txtCustomerName.Enabled = false;
			this.txtCustomerName.BackColor = System.Drawing.Color.White;
			this.txtCustomerName.ForeColor = System.Drawing.Color.Black;
			this.chkShipByCash.Enabled=false;
			this.chkShipByCredit.Enabled=false;

			lblCustomerCode.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, lblCustomerCode.Text, utility.GetUserCulture());
			lblCustomerName.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, lblCustomerName.Text, utility.GetUserCulture());
		}

		private void DisableCustomer()
		{
			this.dbCmbAgentId.Value="";
			this.dbCmbAgentId.Text="CASH";
			txtCustomerName.Text = "";
			txtCustomerName.BackColor = System.Drawing.Color.White;//System.Drawing.Color.LightGray;
			txtCustomerName.Enabled = false;
			this.chkShipByCash.Enabled=true;
			this.chkShipByCredit.Enabled=true;
		}

		private void QueryMode()
		{
			this.divInvoiceGenPreview.Visible=false;
			//			rbCashConsig.Checked = true;
			//			this.chkShipByCash.Checked=false;
			//			this.chkShipByCredit.Checked=false;
			//			rbCreditConsig.Checked = false;
			//			txtCustomerName.Text = null;
			//			txtCustomerName.Enabled = false;
			rbCashConsig.Checked = false;
			rbCreditConsig.Checked = true;
			this.chkShipByCash.Checked=false;
			this.chkShipByCredit.Checked=false;
			this.chkShipByCash.Enabled=false;
			this.chkShipByCredit.Enabled=false;
			//
			//			txtCustomerName.Text = null;
			//			txtCustomerName.Enabled = false;


			//			this.dbCmbAgentId.Text="";
			//			this.dbCmbAgentId.Enabled=true;
			txtLastPickupDate.Text = null;
			txtLastPickupDate.Enabled = true;
			//DisableCustomer();
			EnableCustomer();
			//Jeab 19 Dec 2011
			dbCmbAgentId.Visible=true;
			dbCmbAgentId.Width=210;
			dbCmbAgentId_Cash.Visible=false;
			dbCmbAgentId_Cash.Width =0;
			//Jeab 19 Dec 2011  =========> End
		}

		private DataSet GetFilterData()
		{
			DataTable dtInvoice = new DataTable();
			dtInvoice.Columns.Add(new DataColumn("Payment_mode", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("Cash_Cust", typeof(bool)));
			dtInvoice.Columns.Add(new DataColumn("Credit_Cust", typeof(bool)));
			dtInvoice.Columns.Add(new DataColumn("Customer_Code", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("Last_Pickup_Date", typeof(DateTime)));
			dtInvoice.Columns.Add(new DataColumn("manifestDTFrom", typeof(DateTime)));
			dtInvoice.Columns.Add(new DataColumn("manifestDTTO", typeof(DateTime)));

			DataRow drInvoice = dtInvoice.NewRow();
			if (rbCashConsig.Checked)
			{
				if(this.chkShipByCash.Checked)
					drInvoice["Cash_Cust"] = true;
				else
					drInvoice["Cash_Cust"] = false;
				if(this.chkShipByCredit.Checked)
					drInvoice["Credit_Cust"] = true;
				else
					drInvoice["Credit_Cust"] = false;

				drInvoice["Payment_mode"] = "C";
				Session["Invoice_Type"] = "CASH";
			}
			else if (rbCreditConsig.Checked)
			{
				drInvoice["Payment_mode"] = "R";
				Session["Invoice_Type"] = "CREDIT";
			}

			if (rbtDomesticCons.Checked)
			{
				drInvoice["Payment_mode"] = "D";
				Session["Invoice_Type"] = "DOMESTIC";
			}
			if (rbtExports.Checked)
			{
				drInvoice["Payment_mode"] = "E";
				Session["Invoice_Type"] = "EXPORT";
			}
			if (rbtImports.Checked)
			{
				drInvoice["Payment_mode"] = "I";
				Session["Invoice_Type"] = "IMPORT";
			}
			
			//Jeab 29 Nov 2011
			if (rbCreditConsig.Checked)
			{
				drInvoice["Customer_Code"] = this.dbCmbAgentId.Text.ToString().Trim();
			}
			else
			{
				drInvoice["Customer_Code"] = this.dbCmbAgentId_Cash.Text.ToString().Trim();
			}
			//Jeab 29 Nov 2011  =========> End

			if (this.txtLastPickupDate.Text.Trim() != "")
				drInvoice["Last_Pickup_Date"] = DateTime.ParseExact(this.txtLastPickupDate.Text+" 23:59:59","dd/MM/yyyy HH:mm:ss",null);
			if(dbcPayerAccount.SelectedItem.Value != "")
				drInvoice["Customer_Code"] = this.dbcPayerAccount.SelectedItem.Value;

			if (this.txtManFrom.Text.Trim() != "")
				drInvoice["manifestDTFrom"] = DateTime.ParseExact(this.txtManFrom.Text+" 00:00:01","dd/MM/yyyy HH:mm:ss",null);
			if (this.txtManTo.Text.Trim() != "")
				drInvoice["manifestDTTO"] = DateTime.ParseExact(this.txtManTo.Text+" 23:59:59","dd/MM/yyyy HH:mm:ss",null);

			dtInvoice.Rows.Add(drInvoice);
			DataSet dsInv = new DataSet();
			dsInv.Tables.Add(dtInvoice);
			return  dsInv;
		}

		private void chkShipByCash_CheckedChanged(object sender, System.EventArgs e)
		{
			if(chkShipByCash.Checked)
			{
				this.dbCmbAgentId.Text="CASH";
				this.dbCmbAgentId.Enabled=false;
			}
		}

#endregion

#region "PREVIEW"
		private void GetRecSet()
		{
			String tmpStr = ViewState["currentSet"].ToString().Trim();
			int iStartIndex = 0;
			int intIndexOf = tmpStr.IndexOf(".");
	
			if (intIndexOf > 0)
			{
				iStartIndex = Convert.ToInt32(tmpStr.Substring(0, intIndexOf)) * m_iSetSize;
			}
			else
			{
				iStartIndex = Convert.ToInt32(tmpStr) * m_iSetSize;
			}

			Session["SESSION_DS_Invoice"] = m_sdsInvoiceHeader;
			decimal pgCnt = (m_sdsInvoiceHeader.QueryResultMaxSize - 1)/m_iSetSize;
			if(pgCnt < Convert.ToInt32(ViewState["currentSet"]))
			{
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
			}

			if(m_sdsInvoiceHeader.QueryResultMaxSize == 0 || m_sdsInvoiceHeader.QueryResultMaxSize == 1)
				EnableNavigationButtons(false,false,false,false);

		}

		public void DisplayCurrentPage()
		{
			
			this.lblErrorMessage.Text="";
			this.lblErrorMsg.Text="";

			this.btnSelectAll.Text="Select All";

			strInvType = (string)Session["Invoice_Type"];

			//BindHeader(strInvType);
			DisplayRecNum();

		}

		public void BindHeader(string strInvType)
		{

			if(m_sdsInvoiceHeader.ds.Tables[0].Rows.Count <= 0) 
			{
				return;
			}

			DataRow drCurrent = m_sdsInvoiceHeader.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
			Session["currentHeader"] = drCurrent;

			//CLEAR ALL HEADER
			this.lblInvoiceType.Text = "";
			this.lblCustID.Text = "";
			this.lblCustName.Text = "";
			this.lblAddress1.Text = "";
			this.lblAddress2.Text = "";
			this.lblPostalCode.Text = "";
			this.lblProvince.Text = "";
			this.lblTelephone.Text = "";
			this.lblFax.Text = "";
			this.lblHC_POD_Required.Text = "";
			string strCPayment_mode = "";
			//END OF CLEAR ALL HEADER
			
			
			if(strInvType=="CASH")
				this.lblInvoiceType.Text = "CASH";
			else
				this.lblInvoiceType.Text = "CREDIT";	
				
			//IN CASE OF INVOICE TYPE = CREDIT

			if(strInvType=="CREDIT")
			{
				if(drCurrent["payerid"]!=null&&(!drCurrent["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					this.lblCustID.Text = drCurrent["payerid"].ToString();
					ViewState["payerID"] = drCurrent["payerid"].ToString();
				}

				if(drCurrent["sender_name"]!=null&&(!drCurrent["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblCustName.Text = drCurrent["sender_name"].ToString();

				if(drCurrent["sender_address1"]!=null&&(!drCurrent["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblAddress1.Text = drCurrent["sender_address1"].ToString();

				if(drCurrent["sender_address2"]!=null&&(!drCurrent["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblAddress2.Text = drCurrent["sender_address2"].ToString();

				if(drCurrent["sender_zipcode"]!=null&&(!drCurrent["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					string strZipCode = drCurrent["sender_zipcode"].ToString();

					this.lblPostalCode.Text = strZipCode;

					Zipcode zipCode = new Zipcode();
					zipCode.Populate(this.m_strAppID,this.m_strEnterpriseID,strZipCode);
					String strStateCode = zipCode.StateCode;

					this.lblProvince.Text = strStateCode;
				}

				if(drCurrent["sender_telephone"]!=null&&(!drCurrent["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblTelephone.Text = drCurrent["sender_telephone"].ToString();

				if(drCurrent["sender_fax"]!=null&&(!drCurrent["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblFax.Text = drCurrent["sender_fax"].ToString();

				if(drCurrent["pod_slip_required"]!=null&&(!drCurrent["pod_slip_required"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if(drCurrent["pod_slip_required"].ToString()=="N")
					{
						this.lblHC_POD_Required.Text = "NO";
						this.btnSelectAllHC.Enabled=false;
					}
					else
					{
						this.lblHC_POD_Required.Text = "YES";
						this.btnSelectAllHC.Enabled=true;
					}
				}

			}
			//END OF INVOICE TYPE = CREDIT

			if(drCurrent["CPayment_mode"]!=null&&(!drCurrent["CPayment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				strCPayment_mode = drCurrent["CPayment_mode"].ToString().Trim().ToString();

			//IN CASE OF INVOICE TYPE == "CASH"
			if(strInvType=="CASH") //&&drCurrent["CPayment_mode"].ToString()=="R")
			{
				if(drCurrent["payerid"]!=null&&(!drCurrent["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if(strCPayment_mode.Equals("C"))
						this.lblCustID.Text = "N/A";
					else
    					this.lblCustID.Text = drCurrent["payerid"].ToString();

					ViewState["payerID"] = drCurrent["payerid"].ToString();
				}

				if(drCurrent["sender_name"]!=null&&(!drCurrent["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblCustName.Text = drCurrent["sender_name"].ToString();

				if(drCurrent["sender_address1"]!=null&&(!drCurrent["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblAddress1.Text = drCurrent["sender_address1"].ToString();

				if(drCurrent["sender_address2"]!=null&&(!drCurrent["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblAddress2.Text = drCurrent["sender_address2"].ToString();

				if(drCurrent["sender_zipcode"]!=null&&(!drCurrent["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					string strZipCode = drCurrent["sender_zipcode"].ToString();

					this.lblPostalCode.Text = strZipCode;

					Zipcode zipCode = new Zipcode();
					zipCode.Populate(this.m_strAppID,this.m_strEnterpriseID,strZipCode);
					String strStateCode = zipCode.StateCode;

					this.lblProvince.Text = strStateCode;
				}

				if(drCurrent["sender_telephone"]!=null&&(!drCurrent["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblTelephone.Text = drCurrent["sender_telephone"].ToString();

				if(drCurrent["sender_fax"]!=null&&(!drCurrent["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					this.lblFax.Text = drCurrent["sender_fax"].ToString();

				this.lblHC_POD_Required.Text = "NO";

				this.btnSelectAllHC.Enabled=false;

			}
			//END OF INVOICE TYPE = CASH
				
			//dsInvoiceResult = InvoiceGenerationMgrDAL.GetInvoiceableShipmentsList(m_strAppID,m_strEnterpriseID,dsInvoiceQuery,this.lblCustID.Text,this.lblCustName.Text);
			dsInvoiceResult = InvoiceGenerationMgrDAL.GetTempInvoiceableShipmentsList(m_strAppID,m_strEnterpriseID,ut.GetUserID(),drCurrent["CPayment_mode"].ToString(),this.lblCustID.Text,this.lblCustName.Text, Session.SessionID);
			
			BindGrid(dsInvoiceResult,drCurrent["CPayment_mode"].ToString());
		
		
		}

		
		public void BindGridInvoiceByConsignment(DataSet dsResult)
		{
			ViewState["dsExport"] = dsResult;
			this.dgPreview2.DataSource = dsResult.Tables[0];
			this.dgPreview2.DataBind(); 

			EnterpriseConfigurations();

			DataSet dsInvoiceQuery = (DataSet)Session["dsPreviewQUERY"];
			string strPayerID = string.Empty;
			if(ViewState["payerID"] != null)
				strPayerID = ViewState["payerID"].ToString();

			string invGenType = "";
			if(rbtDomesticCons.Checked)
				invGenType = "D";
			else if(rbtExports.Checked)
				invGenType = "E";
			else if(rbtImports.Checked)
				invGenType = "I";

			int countCheckBox = 0;
			for(int i = 0; i <= dgPreview2.Items.Count - 1; i++)
			{
				//Check Invoice
				//lbInvoicNo
				//Label lbInvoicNo = (Label)this.dgPreview2.Items[i].FindControl("lbInvoicNo");
				CheckBox Checkbox1 = (CheckBox)this.dgPreview2.Items[i].FindControl("Checkbox1");
				Label lbConsig = (Label)this.dgPreview2.Items[i].FindControl("lbConsig");
				Label lbInvoicNo = (Label)this.dgPreview2.Items[i].FindControl("lbInvoicNo");
				TextBox txtConsig = (TextBox)this.dgPreview2.Items[i].FindControl("txtConsig2");
				Label lbIsInvoiceDuplicated = (Label)this.dgPreview2.Items[i].FindControl("lbIsInvoiceDuplicated");
				//IsInvoiceDuplicated
				
				// Please also restrict any TNT AU on forward invoice generation to 9 digit connotes only for TNT AUSTRALIA and 12 digits for TNT FedEx.
				if ((invGenType == "I" && lbConsig.Text.Length > 9 && strPayerID == "989999") || (invGenType == "I" && lbConsig.Text.Length > 12 && strPayerID == "989888"))
                {	
					ImageButton editIcon = (ImageButton)this.dgPreview2.Items[i].FindControl("imbtEdit");
					editIcon.Visible = false;
					lbConsig.ForeColor = Color.Red;
					Checkbox1.Enabled = false;
					Checkbox1.Checked = false;
				}

				//bool chkInvoice = InvoiceGenerationMgrDAL.CheckInvoice(m_strAppID, m_strEnterpriseID, lbInvoicNo.Text);
				if(lbIsInvoiceDuplicated.Text == "1")
				{
					lbInvoicNo.ForeColor = Color.Red;
					Checkbox1.Enabled = false;
					Checkbox1.Checked = false;
				}
				else
					lbInvoicNo.ForeColor = Color.Gray;
				
				CheckBox chkSelect = (CheckBox)this.dgPreview2.Items[i].FindControl("Checkbox1");
				if(chkSelect.Checked){
					countCheckBox++;
				}	
				chkSelect.Attributes.Add("RowNumber", i.ToString());
			}
			
			// Enable Generate button when there is at least one record CHECKED on InvoicePreview_Detail
			bool checkedInvoicePreview = InvoiceGenerationMgrDAL.IsInvoicePreviewSelected(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),Session.SessionID);
			if(checkedInvoicePreview)
			{
				this.btnGenerateInv.Enabled=true;
				btnSelectAll.Text = "Unselect All";
			}
			else{
				this.btnGenerateInv.Enabled=false;
				btnSelectAll.Text = "Select All";
			}
				
		}

		public void BindGrid(DataSet dsResult,String strCustType)
		{
			ViewState["dsExport"] = dsResult;
			this.dgPreview.DataSource = dsResult;
			this.dgPreview.DataBind(); 

			DataSet dsInvoiceQuery = (DataSet)Session["dsPreviewQUERY"];
			string strPayerID = "";
			if(ViewState["payerID"] != null)
				 strPayerID = ViewState["payerID"].ToString();

			//���������� DDLSC ���
			//DataSet dsDDLSC = InvoiceGenerationMgrDAL.GetInvoiceableShipmentsCustomer(m_strAppID, m_strEnterpriseID,dsInvoiceQuery,this.lblCustID.Text,this.lblCustName.Text);
			DataSet dsDDLSC = InvoiceGenerationMgrDAL.GetTempInvoiceableShipmentsCustomer(m_strAppID, m_strEnterpriseID,ut.GetUserID(),strCustType,this.lblCustID.Text,this.lblCustName.Text,Session.SessionID.ToString());

			//BIND DDL & SAVE BUTTON
			if(strInvType=="CREDIT")
			{//IF EQUALS TO CREDIT , DISABLED DDL & SAVE BUTTON - ELSE ENABLED ALL
				for(int i = 0; i <= dgPreview.Items.Count - 1; i++)
				{
					DropDownList ddlSCShipmentUpdate = (DropDownList)this.dgPreview.Items[i].FindControl("ddlSCShipmentUpdate");
					ddlSCShipmentUpdate.DataSource = dsDDLSC;
					ddlSCShipmentUpdate.DataTextField = "sender_name";
					ddlSCShipmentUpdate.DataValueField = "sender_name";
					ddlSCShipmentUpdate.DataBind();
					ddlSCShipmentUpdate.Enabled=false;

					ImageButton imgSave = (ImageButton)this.dgPreview.Items[i].FindControl("imgSave");
					imgSave.Visible = false;

					//					if(this.lblHC_POD_Required.Text=="NO")
					//					{
					CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");
					//						if(!chkSelect.Checked && (bool)Session["initdgCheck"])
					//							chkSelect.Checked = true;
					//					}
					chkSelect.Attributes.Add("RowNumber", i.ToString());
				}
			}
			if(strInvType=="CASH")
			{
				for(int i = 0; i <= dgPreview.Items.Count - 1; i++)
				{
					DropDownList ddlSCShipmentUpdate = (DropDownList)this.dgPreview.Items[i].FindControl("ddlSCShipmentUpdate");
					ddlSCShipmentUpdate.DataSource = dsDDLSC;
					ddlSCShipmentUpdate.DataTextField = "sender_name";
					ddlSCShipmentUpdate.DataValueField = "sender_name";
					ddlSCShipmentUpdate.DataBind();
					ddlSCShipmentUpdate.Enabled=true;

					ImageButton imgSave = (ImageButton)this.dgPreview.Items[i].FindControl("imgSave");
					imgSave.Visible = true;

					//IF HC POD REQUIRED = NO, CHECK ALL
					//					if(this.lblHC_POD_Required.Text=="NO")
					//					{
					CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");
					//
					//						if(!chkSelect.Checked && (bool)Session["initdgCheck"])
					//							chkSelect.Checked = true;
					//					}
					chkSelect.Attributes.Add("RowNumber", i.ToString());

				}
			}

			//SELECT DROP DOWN LIST SAME AS SENDER_NAME
			for(int i = 0; i <= dgPreview.Items.Count - 1; i++)
			{
				DropDownList ddlSCShipmentUpdate = (DropDownList)this.dgPreview.Items[i].FindControl("ddlSCShipmentUpdate");
				Label dgLabelSenderName = (Label)this.dgPreview.Items[i].FindControl("dgLabelSenderName");
				ImageButton imgSave = (ImageButton)this.dgPreview.Items[i].FindControl("imgSave");

				ddlSCShipmentUpdate.SelectedIndex = ddlSCShipmentUpdate.Items.IndexOf(ddlSCShipmentUpdate.Items.FindByValue(dgLabelSenderName.Text.ToUpper().ToString()));
				if(strCustType=="R")
				{
					ddlSCShipmentUpdate.Enabled=false;
					imgSave.Visible = false;
				}

			}
			//END OF SELECT DDL

			if((TIESDAL.PostDeliveryCOD.GetUserRole(utility.GetAppID(),utility.GetEnterpriseID(),utility.GetUserID(),"ACCT")==0)&&(TIESDAL.PostDeliveryCOD.GetUserRole(utility.GetAppID(),utility.GetEnterpriseID(),utility.GetUserID(),"ACCTMGR")==0))
			{
				this.btnGenerateInv.Enabled=false;
				//����������� ACCT ����Ѿഷ DDL �С����� SAVE ����� ���� GENINV ��������
				for(int i = 0; i <= dgPreview.Items.Count - 1; i++)
				{
					DropDownList ddlSCShipmentUpdate = (DropDownList)this.dgPreview.Items[i].FindControl("ddlSCShipmentUpdate");
					ddlSCShipmentUpdate.Enabled=false;

					ImageButton imgSave = (ImageButton)this.dgPreview.Items[i].FindControl("imgSave");
					imgSave.Visible = false;
				}
			}
			else
			{
				this.btnGenerateInv.Enabled=true;
			}
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			//Response.Redirect("InvoiceGeneration.aspx");
			this.divInvoiceGen.Visible=true;
			this.btnGenerate.Enabled=true;
			this.divInvoiceGenPreview.Visible=false;
			
			
			InvoiceGenerationMgrDAL.DeleteTempInvoicePreview(ut.GetAppID(), ut.GetEnterpriseID(), ut.GetUserID(), null, Session.SessionID); 

			QueryMode();
			//CLEAR ALL TEMP DATA

			//END CLEAR ALL TEMP DATA

			dbCmbAgentId.Visible = false;
			rbtDomesticCons.Checked = true;
			//dbcPayerAccount.SelectedItem = "";
			//dbcPayerAccount.SelectedValue = "";
			txtPayerName.Text = "";
			txtManFrom.Text = "";
			txtManTo.Text = "";
			rbtConsignmentNum.Checked = true;

			if(rbtInvoiceCustomer.Checked)
				dbCmbAgentId.Visible = true;

			txtManFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
			txtManTo.Text = DateTime.Now.ToString("dd/MM/yyyy");
		}

		private void txtGoToRec_TextChanged(object sender, System.EventArgs e)
		{
			ViewState["Current_Page"] = txtGoToRec.Text;
			dsInvoiceResult = InvoiceGenerationMgrDAL.PreviewInvoiceHeaderDetails(m_strAppID,m_strEnterpriseID,ut.GetUserID(), Session.SessionID, ViewState["Current_Page"].ToString());

			ViewState["No_Of_Pages"] = dsInvoiceResult.Tables[1].Rows[0][0].ToString();
			ViewState["First_Page"] = dsInvoiceResult.Tables[1].Rows[0][1].ToString();
			ViewState["Previous_Page"] = dsInvoiceResult.Tables[1].Rows[0][2].ToString();
			ViewState["Current_Page"] = dsInvoiceResult.Tables[1].Rows[0][3].ToString();
			ViewState["Next_Page"] = dsInvoiceResult.Tables[1].Rows[0][4].ToString();
			ViewState["Last_Page"] = dsInvoiceResult.Tables[1].Rows[0][5].ToString();
			ViewState["Text_Label"] = dsInvoiceResult.Tables[1].Rows[0][6].ToString();

			txtGoToRec.Text = ViewState["Current_Page"].ToString();
			lblNumRec.Text = ViewState["Text_Label"].ToString();

			//BindGrid(dsInvoiceResult, "CREDIT");
			BindGridInvoiceByConsignment(dsInvoiceResult);

			if (ViewState["Current_Page"] == ViewState["Previous_Page"])
			{
				EnableNavigationButtons(false,false,true,true);
			} 
			else if (ViewState["Current_Page"] == ViewState["Next_Page"])
			{
				EnableNavigationButtons(true,true,false,false);
			}
			else{
				EnableNavigationButtons(true,true,true,true);
			}

		}

		private void btnNextPage_Click(object sender, System.EventArgs e)
		{
			EnableNavigationButtons(true,true,true,true);
			dsInvoiceResult = InvoiceGenerationMgrDAL.PreviewInvoiceHeaderDetails(m_strAppID,m_strEnterpriseID,ut.GetUserID(), Session.SessionID, ViewState["Next_Page"].ToString());

			ViewState["No_Of_Pages"] = dsInvoiceResult.Tables[1].Rows[0][0].ToString();
			ViewState["First_Page"] = dsInvoiceResult.Tables[1].Rows[0][1].ToString();
			ViewState["Previous_Page"] = dsInvoiceResult.Tables[1].Rows[0][2].ToString();
			ViewState["Current_Page"] = dsInvoiceResult.Tables[1].Rows[0][3].ToString();
			ViewState["Next_Page"] = dsInvoiceResult.Tables[1].Rows[0][4].ToString();
			ViewState["Last_Page"] = dsInvoiceResult.Tables[1].Rows[0][5].ToString();
			ViewState["Text_Label"] = dsInvoiceResult.Tables[1].Rows[0][6].ToString();

			txtGoToRec.Text = ViewState["Current_Page"].ToString();
			lblNumRec.Text = ViewState["Text_Label"].ToString();

			//BindGrid(dsInvoiceResult, "CREDIT");
			BindGridInvoiceByConsignment(dsInvoiceResult);
		}

		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			EnableNavigationButtons(true,true,false,false);
			dsInvoiceResult = InvoiceGenerationMgrDAL.PreviewInvoiceHeaderDetails(m_strAppID,m_strEnterpriseID,ut.GetUserID(), Session.SessionID, ViewState["Last_Page"].ToString());

			ViewState["No_Of_Pages"] = dsInvoiceResult.Tables[1].Rows[0][0].ToString();
			ViewState["First_Page"] = dsInvoiceResult.Tables[1].Rows[0][1].ToString();
			ViewState["Previous_Page"] = dsInvoiceResult.Tables[1].Rows[0][2].ToString();
			ViewState["Current_Page"] = dsInvoiceResult.Tables[1].Rows[0][3].ToString();
			ViewState["Next_Page"] = dsInvoiceResult.Tables[1].Rows[0][4].ToString();
			ViewState["Last_Page"] = dsInvoiceResult.Tables[1].Rows[0][5].ToString();
			ViewState["Text_Label"] = dsInvoiceResult.Tables[1].Rows[0][6].ToString();

			txtGoToRec.Text = ViewState["Current_Page"].ToString();
			lblNumRec.Text = ViewState["Text_Label"].ToString();

			//BindGrid(dsInvoiceResult, "CREDIT");
			BindGridInvoiceByConsignment(dsInvoiceResult);
		}

		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			EnableNavigationButtons(true,true,true,true);
			dsInvoiceResult = InvoiceGenerationMgrDAL.PreviewInvoiceHeaderDetails(m_strAppID,m_strEnterpriseID,ut.GetUserID(), Session.SessionID, ViewState["Previous_Page"].ToString());

			ViewState["No_Of_Pages"] = dsInvoiceResult.Tables[1].Rows[0][0].ToString();
			ViewState["First_Page"] = dsInvoiceResult.Tables[1].Rows[0][1].ToString();
			ViewState["Previous_Page"] = dsInvoiceResult.Tables[1].Rows[0][2].ToString();
			ViewState["Current_Page"] = dsInvoiceResult.Tables[1].Rows[0][3].ToString();
			ViewState["Next_Page"] = dsInvoiceResult.Tables[1].Rows[0][4].ToString();
			ViewState["Last_Page"] = dsInvoiceResult.Tables[1].Rows[0][5].ToString();
			ViewState["Text_Label"] = dsInvoiceResult.Tables[1].Rows[0][6].ToString();

			txtGoToRec.Text = ViewState["Current_Page"].ToString();
			lblNumRec.Text = ViewState["Text_Label"].ToString();

			//BindGrid(dsInvoiceResult, "CREDIT");
			BindGridInvoiceByConsignment(dsInvoiceResult);

		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			EnableNavigationButtons(false,false,true,true);
			dsInvoiceResult = InvoiceGenerationMgrDAL.PreviewInvoiceHeaderDetails(m_strAppID,m_strEnterpriseID,ut.GetUserID(), Session.SessionID, ViewState["First_Page"].ToString());

			ViewState["No_Of_Pages"] = dsInvoiceResult.Tables[1].Rows[0][0].ToString();
			ViewState["First_Page"] = dsInvoiceResult.Tables[1].Rows[0][1].ToString();
			ViewState["Previous_Page"] = dsInvoiceResult.Tables[1].Rows[0][2].ToString();
			ViewState["Current_Page"] = dsInvoiceResult.Tables[1].Rows[0][3].ToString();
			ViewState["Next_Page"] = dsInvoiceResult.Tables[1].Rows[0][4].ToString();
			ViewState["Last_Page"] = dsInvoiceResult.Tables[1].Rows[0][5].ToString();
			ViewState["Text_Label"] = dsInvoiceResult.Tables[1].Rows[0][6].ToString();

			txtGoToRec.Text = ViewState["Current_Page"].ToString();
			lblNumRec.Text = ViewState["Text_Label"].ToString();

			//BindGrid(dsInvoiceResult, "CREDIT");
			BindGridInvoiceByConsignment(dsInvoiceResult);
		}

		private void DisplayRecNum()
		{
			string invGenType = "";
			string payerAccount = "";

			if(rbtDomesticCons.Checked)
				invGenType = "D";
			else if(rbtExports.Checked)
				invGenType = "E";
			else if(rbtImports.Checked)
			{
				invGenType = "I";
				payerAccount = dbcPayerAccount.SelectedItem.Value;
			}

			int iCurrentRec = (Convert.ToInt32(ViewState["currentPage"]) + 1) + (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) ;

			lblNumRec.Text = ""+ iCurrentRec + " of " + m_sdsInvoiceHeader.QueryResultMaxSize + " record(s)";

			//ADDED BY X FEB 22 08
			if(iCurrentRec>=m_sdsInvoiceHeader.QueryResultMaxSize)
			{
				EnableNavigationButtons(true,true,false,false);
				return;
			}
			//END ADDED BY X FEB 22 08
		}

		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}

		private void btnSelectAll_Click(object sender, System.EventArgs e)
		{
			if(rbtInvoiceCustomer.Checked)
			{
				#region InvoiceCustomer
				string strConsigNo=null;

				Button btnSelectAll = (Button)this.FindControl("btnSelectAll");
				if((bool)ViewState["btnSelectAll"])
				{
					ViewState["btnSelectAll"]=false;
					btnSelectAll.Text="Unselect All";
					InvoiceGenerationMgrDAL.UpdateTempCheckedAllConsignment(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),strConsigNo,true,Session.SessionID);
				}
				else
				{
					ViewState["btnSelectAll"]=true;
					btnSelectAll.Text="Select All";
					InvoiceGenerationMgrDAL.UpdateTempCheckedAllConsignment(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),strConsigNo,false,Session.SessionID);
				}
				#endregion
			}
			else
			{	
				if (btnSelectAll.Text=="Select All"){
					InvoiceGenerationMgrDAL.InvoicePreview_SelectAll(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),Session.SessionID,"Y");
				}else{
					InvoiceGenerationMgrDAL.InvoicePreview_SelectAll(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),Session.SessionID,"N");
				}
				
				dsInvoiceResult = InvoiceGenerationMgrDAL.PreviewInvoiceHeaderDetails(m_strAppID,m_strEnterpriseID,ut.GetUserID(), Session.SessionID, txtGoToRec.Text);
				BindGridInvoiceByConsignment(dsInvoiceResult);
			}
		}

		private void btnSelectAllHC_Click(object sender, System.EventArgs e)
		{
			string strConsigNo=null;
			//the shipment_tracking table is searching for the HCRHQ status code, if present, that consignment is selected.
			//GET THE CONSIG_no and search for HCRHQ status in Shipment_tracking
			for(int i = 0; i <= dgPreview.Items.Count - 1; i++)
			{
				CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");

				strConsigNo = this.dgPreview.Items[i].Cells[1].Text;

				if(InvoiceGenerationMgrDAL.checkHCReturned(m_strAppID, m_strEnterpriseID,strConsigNo))
				{
					chkSelect.Checked=true;
					InvoiceGenerationMgrDAL.UpdateTempCheckedConsignment(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),strConsigNo,chkSelect.Checked, Session.SessionID);
				}
				else
				{
					chkSelect.Checked=false;
					InvoiceGenerationMgrDAL.UpdateTempCheckedConsignment(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),strConsigNo,chkSelect.Checked, Session.SessionID);
				}
			}
		}

		protected void dgPreview2_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text = "";
			int rowIndex = e.Item.ItemIndex;
			dgPreview2.EditItemIndex = -1;
			
			string invGenType = "";
			if(rbtDomesticCons.Checked)
				invGenType = "D";
			else if(rbtExports.Checked)
				invGenType = "E";
			else if(rbtImports.Checked)
				invGenType = "I";

			//dsInvoiceResult = InvoiceGenerationMgrDAL.GetTempInvoiceByConsignmentList(m_strAppID,m_strEnterpriseID,ut.GetUserID(), dbcPayerAccount.SelectedItem.Value, Session.SessionID, invGenType);
			dsInvoiceResult = InvoiceGenerationMgrDAL.PreviewInvoiceHeaderDetails(m_strAppID,m_strEnterpriseID,ut.GetUserID(), Session.SessionID, txtGoToRec.Text);

			BindGridInvoiceByConsignment(dsInvoiceResult);
		}

		protected void dgPreview2_Edit(object sender, DataGridCommandEventArgs e)
		{		
			lblErrorMsg.Text = "";
			int iSelIndex = e.Item.ItemIndex;
			string commandName = e.CommandName;

			switch(commandName)
			{
				case "Update" :
					ImageButton imbUpdate = (ImageButton)this.dgPreview2.Items[iSelIndex].FindControl("imbUpdate");
					ImageButton imbtEdit = (ImageButton)this.dgPreview2.Items[iSelIndex].FindControl("imbtEdit");
					ImageButton imbtCancel = (ImageButton)this.dgPreview2.Items[iSelIndex].FindControl("imbtCancel");
					TextBox txtConsig = (TextBox)this.dgPreview2.Items[iSelIndex].FindControl("txtConsig");
					TextBox txtAWBNumber = (TextBox)this.dgPreview2.Items[iSelIndex].FindControl("txtAWBNumber");
					com.common.util.msTextBox txtIntlCharge = (com.common.util.msTextBox)this.dgPreview2.Items[iSelIndex].FindControl("txtIntlCharge");
					Label lbInvoicNo = (Label)this.dgPreview2.Items[iSelIndex].FindControl("lbInvoicNo");

					imbUpdate.Visible = true;
					imbtEdit.Visible = false;
					imbtCancel.Visible = true;
					txtConsig.Enabled = true;
					txtAWBNumber.Enabled = true;
					txtIntlCharge.Enabled = true;
					txtConsig.Visible = true;
					lbInvoicNo.Visible = false;

					break;
			};
			
		}

		protected void dgPreview2_SaveCust(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text = "";
			int iSelIndex = e.Item.ItemIndex;
			string commandName = e.CommandName;

			string invGenType = "";
			if(rbtDomesticCons.Checked)
				invGenType = "D";
			else if(rbtExports.Checked)
				invGenType = "E";
			else if(rbtImports.Checked)
				invGenType = "I";

			if(commandName == "Edit")
			{
				Label lbConsig = (Label)this.dgPreview2.Items[iSelIndex].FindControl("lbConsig");
				Label lbInvoicNo = (Label)this.dgPreview2.Items[iSelIndex].FindControl("lbInvoicNo");
				TextBox txtConsig = (TextBox)this.dgPreview2.Items[iSelIndex].FindControl("txtConsig");
				TextBox txtAWBNumber = (TextBox)this.dgPreview2.Items[iSelIndex].FindControl("txtAWBNumber");
				com.common.util.msTextBox txtIntlCharge = (com.common.util.msTextBox)this.dgPreview2.Items[iSelIndex].FindControl("txtIntlCharge");
				

				InvoiceGenerationMgrDAL.UpdateInvoiceNo(m_strAppID,m_strEnterpriseID, lbConsig.Text, txtConsig.Text, Session.SessionID.ToString(), txtAWBNumber.Text, txtIntlCharge.Text);
				
				//lbInvoicNo.ForeColor = Color.Gray;
				dgPreview2.EditItemIndex = -1;

				//dsInvoiceResult = InvoiceGenerationMgrDAL.GetTempInvoiceByConsignmentList(m_strAppID,m_strEnterpriseID,ut.GetUserID(), dbcPayerAccount.SelectedItem.Value, Session.SessionID, invGenType);
				dsInvoiceResult = InvoiceGenerationMgrDAL.PreviewInvoiceHeaderDetails(m_strAppID,m_strEnterpriseID,ut.GetUserID(), Session.SessionID, txtGoToRec.Text);
				BindGridInvoiceByConsignment(dsInvoiceResult);

				bool chkInvoice = InvoiceGenerationMgrDAL.CheckInvoice(m_strAppID, m_strEnterpriseID, lbInvoicNo.Text);
				if(chkInvoice)
					lbInvoicNo.ForeColor = Color.Red;
				else
					lbInvoicNo.ForeColor = Color.Gray;

			}
			//btnGenerateInv.Enabled = true;
		}

		protected void dgPreview_SaveCust(object sender, DataGridCommandEventArgs e)
		{
			int iSelIndex = e.Item.ItemIndex;

			string strConsigNo = this.dgPreview.Items[iSelIndex].Cells[1].Text;
			DropDownList ddlSCShipmentUpdate = (DropDownList)this.dgPreview.Items[iSelIndex].FindControl("ddlSCShipmentUpdate");
			//੾�� CASH ��ҹ�鹷�����૿���͡ૹ��������Ш��
			string strSenderName = ddlSCShipmentUpdate.SelectedItem.Value.ToString();

			bool bUpdateOK=false;

			bUpdateOK = InvoiceGenerationMgrDAL.UpdateTempSenderName(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),strConsigNo,strSenderName,Session.SessionID.ToString());

			if(bUpdateOK)
				lblErrorMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"MODIFIED_SUCCESSFULLY",utility.GetUserCulture());
			else
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MDY_FAIL",utility.GetUserCulture());


			strInvType = (string)Session["Invoice_Type"];

			BindHeader(strInvType);
			DisplayRecNum();

		}


		protected void dgPreview_CheckChanged(object sender, System.EventArgs e)
		{ 
			int i = 0;
			CheckBox chkSelect =  (CheckBox)sender;
			DataGridItem dgi;
			dgi =(DataGridItem)chkSelect.NamingContainer; 

			i = Convert.ToInt32(chkSelect.Attributes["RowNumber"]);

			string strConsigNo = this.dgPreview.Items[i].Cells[1].Text;
			InvoiceGenerationMgrDAL.UpdateTempCheckedConsignment(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),strConsigNo,chkSelect.Checked, Session.SessionID);

		}

		protected void dgPreview2_CheckChanged(object sender, System.EventArgs e)
		{ 
			int i = 0;
			CheckBox chkSelect =  (CheckBox)sender;
			DataGridItem dgi;
			dgi =(DataGridItem)chkSelect.NamingContainer; 

			i = Convert.ToInt32(chkSelect.Attributes["RowNumber"]);

			if(chkSelect.Checked)
				btnGenerateInv.Enabled = true;

			//string strConsigNo = this.dgPreview.Items[i].Cells[1].Text;
			TextBox txtConsig = (TextBox)this.dgPreview2.Items[i].FindControl("txtConsig2");
			string strConsigNo = txtConsig.Text;
			InvoiceGenerationMgrDAL.UpdateTempCheckedConsignment(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),strConsigNo,chkSelect.Checked, Session.SessionID);
			

			// Enable Generate button when there is at least one record CHECKED on InvoicePreview_Detail
			bool checkedInvoicePreview = InvoiceGenerationMgrDAL.IsInvoicePreviewSelected(utility.GetAppID(),utility.GetEnterpriseID(),ut.GetUserID(),Session.SessionID);
			if(checkedInvoicePreview)
			{
				this.btnGenerateInv.Enabled=true;
				btnSelectAll.Text="Unselect All";
			}
			else{
				this.btnGenerateInv.Enabled=false;
				btnSelectAll.Text="Select All";
			}
				
		}

		private void btnGenerateInv_Click(object sender, System.EventArgs e)
		{
			DataTable dtConsignment = new DataTable();
			DataSet dsConsignment =new DataSet();
			dtConsignment.Columns.Add(new DataColumn("consignment_no", typeof(string)));
			dtConsignment.Columns.Add(new DataColumn("Sender_Name", typeof(string)));	

			//Generate Invoice group by HEADER that consignment has been CHECKED!

			//FIND CHECKED CONSIG BY LOOPING ALL DETAILS

			
			//GET ALL HEADER FOR LOOPING
			DataSet dsHeader  = InvoiceGenerationMgrDAL.GetTempListOfCHECKEDInvoiceHeader(m_strAppID, m_strEnterpriseID,ut.GetUserID(), Session.SessionID);
			if((dsHeader.Tables[0].Rows.Count<=0))
			{
				this.lblErrorMsg.Text="No consignments selected.";
				this.btnGenerateInv.Enabled=true;
				return;
			}

			if(rbtExports.Checked)
			{
				string tempChk = "";
				DataSet dsChk  = InvoiceGenerationMgrDAL.CheckConNotComplete(m_strAppID, m_strEnterpriseID,ut.GetUserID(), Session.SessionID);
				for(int i = 0;i < dsChk.Tables[0].Rows.Count;i++)
				{
					tempChk += dsChk.Tables[0].Rows[i][0].ToString();
					if(i + 1 != dsChk.Tables[0].Rows.Count)
						tempChk += ", ";
				}
				if(tempChk != "")
				{
					lblErrorMsg.Text = "Data entry for consignment " + tempChk + " is not complete so invoicing cannot proceed. ";
					return;
				}
			}

			//GEN INVOICE
			DataSet dsConsignments=new DataSet();
			string selectedCustID = "";
			if (rbCashConsig.Checked == true)
			{
				selectedCustID = this.dbCmbAgentId_Cash.Value;
			}
			else if (rbCreditConsig.Checked == true)
			{
				selectedCustID = this.dbCmbAgentId.Value;
			}

			string strInvJobID = Counter.GetNext(ut.GetAppID(),ut.GetEnterpriseID(),"InvoiceGenerationJobID").ToString();                                           
			InvoiceJobMgrDAL.InsertInvoice_Generation_Task(ut.GetAppID(), ut.GetEnterpriseID(), strInvJobID, DateTime.Now, "P", "Job Awaiting Execution for " + selectedCustID);
			
			this.lblErrorMessage.Text = "Invoice job id: " + strInvJobID + " has been generated." ;
			this.divInvoiceGen.Visible=true;
			this.btnGenerate.Enabled=true;
			this.divInvoiceGenPreview.Visible=false;
			EnableCustomer();


			string sessionID = Session.SessionID;
			int autogen =  2;
			if(rbtConsignmentNum.Checked)
				autogen = 1;

			//InvoiceJobMgrDAL.GenerateInvoices(ut.GetAppID(), ut.GetEnterpriseID(), ut.GetUserID(), Session.SessionID, strInvJobID);
			InvoiceJobMgrDAL objInvJobMgrDAL = new InvoiceJobMgrDAL();
			//objInvJobMgrDAL.GenerateInvoicesCon(ut.GetAppID(), ut.GetEnterpriseID(), ut.GetUserID(), Session.SessionID, strInvJobID, autogen);
			objInvJobMgrDAL.SyncGenerateInvoicesCon(ut.GetAppID(), ut.GetEnterpriseID(), ut.GetUserID(), Session.SessionID, strInvJobID, autogen);
			//objInvJobMgrDAL.SyncGenerateInvoice(ut.GetAppID(), ut.GetEnterpriseID(), ut.GetUserID(), Session.SessionID, strInvJobID);

		}

#endregion

		private void ExportToXls(DataSet ds,String FileName)
		{
			DataTable dt = ds.Tables[0];
			String headerXls = "attachment;filename="+FileName+".xls";
			if (dt  != null)
			{
				Response.Clear();
				Response.AddHeader("Content-Disposition", headerXls);
				Response.Charset = "";
				Response.ContentType = "application/vnd.ms-excel";

				//write columns
				string tab = "";
				foreach (DataColumn dc in dt.Columns)
				{
					Response.Write(tab + dc.ColumnName);
					tab = "\t";
				}
				Response.Write("\n");

				//write rows
				int i;
				foreach (DataRow dr in dt.Rows)
				{
					tab = " ";
					for (i = 0; i < dt.Columns.Count; i++)
					{
						Response.Write(tab + dr[i].ToString());
						tab = "\t";
					}
					Response.Write("\n");
				}
				Response.End();
			}
		}


		private void btnExport_Click(object sender, System.EventArgs e)
		{
			string Res = "";
			lblErrorMsg.Text = "";

			dsExport =  (DataSet)ViewState["dsExport"];
			// Remove column
			string[] columnRemove = {"applicationid","enterpriseid","userid","pod_slip_required","checked","payerid"};
			RemoveColumn(columnRemove);
			// Set Header Column Name
			for (int i = 0;i<dsExport.Tables[0].Columns.Count;i++)
			{
				// Column checked not use
				dsExport.Tables[0].Columns[i].ColumnName = dgPreview.Columns[i+1].HeaderText;
			}
			// Set type column
			string[] ColumnType = new string[dsExport.Tables[0].Columns.Count];
			TIESClasses.CreateXls Xls = new TIESClasses.CreateXls();
			ColumnType[4] = Xls.TypeDate;
			ColumnType[7] = Xls.TypeDate;
			ColumnType[9] = Xls.TypeDecimal;
			ColumnType[10] = Xls.TypeDecimal;
			ColumnType[11] = Xls.TypeDecimal;
			ColumnType[12] = Xls.TypeDecimal;
			ColumnType[13] = Xls.TypeDecimal;
			
			Res = Xls.createXls(dsExport,GetPathExport(),ColumnType);
			
			if (Res == "0")
			{	
				GetDownloadFile(Res);
				
				// Kill Process after save
				Xls.KillProcesses("EXCEL");
			} 
			else
			{
				lblErrorMsg.Text = Res;
			}
		}

		private void RemoveColumn(string[] columnRemoveName)
		{
				foreach (string stringColumn in columnRemoveName)
				{
					if (CheckColumnNameIsExist(stringColumn) == true)
							dsExport.Tables[0].Columns.Remove(stringColumn);
				}
		}

		private bool CheckColumnNameIsExist(string stringColumn)
		{
			bool flag = false;

			foreach (DataColumn dc in dsExport.Tables[0].Columns)
			{
				if (stringColumn.ToLower() == dc.ColumnName.ToLower())
				{
					flag = true;
				} 
			}
			return flag;
		}

		private string  GetPathExport()
		{
			return Server.MapPath("") + "\\Export\\InvoiceGenerationDetails.xls";
		}

		private void GetDownloadFile(string Res)
		{
			if (Res == "0")
			{
				Response.ContentType="application/ms-excel";
				Response.AddHeader( "content-disposition","attachment; filename=InvoiceGenerationDetails.xls");
				FileStream fs = new FileStream(GetPathExport(),FileMode.Open);
				long FileSize;
				FileSize = fs.Length;
				byte[] getContent = new byte[(int)FileSize];
				fs.Read(getContent, 0, (int)fs.Length);
				fs.Close();

				Response.BinaryWrite(getContent);
			} 
		}

		private void rbtInvoiceCustomer_CheckedChanged(object sender, System.EventArgs e)
		{
			if(!rdtInvoiceConsig.Checked)
			{
				rbtDomesticCons.Visible = false;
				rbtExports.Visible = false;
				rbtImports.Visible = false;
				lbPayerAccount.Visible = false;
				dbcPayerAccount.Visible = false;
				lbPayerName.Visible = false;
				txtPayerName.Visible = false;
				lbManifested.Visible = false;
				txtManFrom.Visible = false;
				lbManTo.Visible = false;
				txtManTo.Visible = false;
				rbtConsignmentNum.Visible = false;
				lbInvoiNumIs.Visible = false;
				rbtAutoGen.Visible = false;

				lblInvoiceFor.Visible = true;
				rbCashConsig.Visible = true;
				chkShipByCash.Visible = true;
				chkShipByCredit.Visible = true;
				rbCreditConsig.Visible = true;
				lblCustomerCode.Visible = true;
				dbCmbAgentId.Visible = true;
				lblCustomerName.Visible = true;
				txtCustomerName.Visible = true;
				lblInvoiceToDate.Visible = true;
				txtLastPickupDate.Visible = true;

				rbCreditConsig.Checked = true;
				dbCmbAgentId.Value = "";
				dbCmbAgentId.Text = "";
				txtCustomerName.Text = "";
				txtLastPickupDate.Text = "";

				btnGenerate.Enabled = true;
			}
		}

		private void rdtInvoiceConsig_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rdtInvoiceConsig.Checked)
			{
				rbtDomesticCons.Visible = true;
				rbtExports.Visible = true;
				rbtImports.Visible = true;
				lbPayerAccount.Visible = true;
				dbcPayerAccount.Visible = true;
				lbPayerName.Visible = true;
				txtPayerName.Visible = true;
				lbManifested.Visible = true;
				txtManFrom.Visible = true;
				lbManTo.Visible = true;
				txtManTo.Visible = true;
				rbtConsignmentNum.Visible = true;
				lbInvoiNumIs.Visible = true;
				rbtAutoGen.Visible = true;

				lblInvoiceFor.Visible = false;
				rbCashConsig.Visible = false;
				chkShipByCash.Visible = false;
				chkShipByCredit.Visible = false;
				rbCreditConsig.Visible = false;
				lblCustomerCode.Visible = false;
				dbCmbAgentId.Visible = false;
				lblCustomerName.Visible = false;
				txtCustomerName.Visible = false;
				lblInvoiceToDate.Visible = false;
				txtLastPickupDate.Visible = false;

				rbtDomesticCons.Checked = true;
				//dbcPayerAccount.SelectedItem = "";
				dbcPayerAccount.SelectedItem.Value = "";
				txtPayerName.Text = "";
				txtManFrom.Text = "";
				txtManTo.Text = "";
				rbtConsignmentNum.Checked = true;

				btnGenerate.Enabled = false;
			}
		}

		private void txtManFrom_TextChanged(object sender, System.EventArgs e)
		{
			if(rbtDomesticCons.Checked)
			{
				if(txtManFrom.Text != "")
					btnGenerate.Enabled = true;
				else
					btnGenerate.Enabled = false;
			}
			if(rbtExports.Checked)
			{
				if(txtManFrom.Text != "")
					btnGenerate.Enabled = true;
				else
					btnGenerate.Enabled = false;
			}

			if(rbtImports.Checked)
				btnGenerate.Enabled = true;
		} 

		private void rbtDomesticCons_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbtDomesticCons.Checked)
			{
				btnGenerate.Enabled = true;
				txtManFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
				txtManTo.Text = DateTime.Now.ToString("dd/MM/yyyy");

				dbcPayerAccount.SelectedIndex = 0;
				dbcPayerAccount.Enabled = false;
				txtPayerName.Enabled = false;
			}
		}

		private void rbtExports_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbtExports.Checked)
			{
				btnGenerate.Enabled = true;
				txtManFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
				txtManTo.Text = DateTime.Now.ToString("dd/MM/yyyy");

				dbcPayerAccount.SelectedIndex = 0;
				dbcPayerAccount.Enabled = false;
				txtPayerName.Enabled = false;
			}
		}

		private void rbtImports_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbtImports.Checked)
			{
				btnGenerate.Enabled = true;
				txtManFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
				txtManTo.Text = DateTime.Now.ToString("dd/MM/yyyy");

				dbcPayerAccount.SelectedIndex = 0;
				dbcPayerAccount.Enabled = true;
				txtPayerName.Enabled = false;
			}
		}

	}//class

}//namespace