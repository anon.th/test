<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="BandDiscount.aspx.cs" AutoEventWireup="false" Inherits="com.ties.BandDiscount" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>BandDiscount</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="BandDiscount" method="post" runat="server">
			<asp:validationsummary id="PageValidationSummary" runat="server" HeaderText="Please check the following Invalid/Mandatory field(s):<br><br>" DisplayMode="BulletList" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary>
			<asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 18px; POSITION: absolute; TOP: 58px" runat="server" Text="Query" Width="100px" CssClass="queryButton" CausesValidation="False"></asp:button><asp:button id="btnExecuteQuery" style="Z-INDEX: 103; LEFT: 118px; POSITION: absolute; TOP: 58px" runat="server" Text="Execute Query" Width="100px" Enabled="False" CssClass="queryButton" CausesValidation="False"></asp:button><asp:button id="btnInsert" style="Z-INDEX: 102; LEFT: 218px; POSITION: absolute; TOP: 58px" runat="server" Text="Insert" Width="100px" CssClass="queryButton" CausesValidation="False"></asp:button>&nbsp;
			<asp:datagrid id="dgBandDiscount" style="Z-INDEX: 104; LEFT: 23px; POSITION: absolute; TOP: 141px" ItemStyle-Height="20" runat="server" AutoGenerateColumns="False" AllowPaging="True" AllowCustomPaging="True" OnPageIndexChanged="OnBandDiscount_PageChange" OnCancelCommand="OnCancel_BandDiscount" OnDeleteCommand="OnDelete_BandDiscount" OnEditCommand="OnEdit_BandDiscount" OnUpdateCommand="OnUpdate_BandDiscount" OnItemDataBound="OnItemBound_BandDiscount" Width="600px">
				<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
				<ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="2%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Code" HeaderStyle-Font-Bold="True">
						<HeaderStyle Width="30%" CssClass="gridHeading"></HeaderStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblCode" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"band_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtCode" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"band_code")%>' Runat="server" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="rqBandCode" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtCode" ErrorMessage="Band Code "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Description">
						<HeaderStyle Width="45%" CssClass="gridHeading"></HeaderStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblDesc" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"band_description")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtDesc" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"band_description")%>' Runat="server" MaxLength="200">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Discount">
						<HeaderStyle Width="25%" CssClass="gridHeading"></HeaderStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblPercent" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"percent_discount","{0:n}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtPercent" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"percent_discount","{0:n}")%>' Runat="server" TextMaskType="msNumeric" NumberMaxValue="100" NumberMinValue="0" NumberPrecision="5" NumberScale="2">
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:label id="lblValBandDiscount" style="Z-INDEX: 105; LEFT: 25px; POSITION: absolute; TOP: 92px" runat="server" Width="673px" CssClass="errorMsgColor"></asp:label>
			<asp:Label id="Label1" style="Z-INDEX: 106; LEFT: 28px; POSITION: absolute; TOP: 16px" runat="server" Width="424px" Height="31px" CssClass="mainTitleSize">Band Discount</asp:Label></form>
	</body>
</HTML>
