<%@ Page language="c#" Codebehind="ExportDeliveredConsignment.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.ExportDeliveredConsignment"%>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Export Delivered Consignments</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
		
		function funcDisBack()
		{
			history.go(+1);
		}
		
        function RemoveBadMasterAWBNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^-_/_a-zA-Z0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteMasterAWBNumber(obj) {
            setTimeout(function () {
                RemoveBadMasterAWBNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function ValidateMasterAWBNumber(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[-_/_a-zA-Z0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function validate(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}		
		function cmdFocus()
		{
			document.all('txtPodDate').focus();
			return false;
			
		}
		function openWin()
		{
			var Mywindow = window.open('TempExportDelivery.aspx','','width=1,height=1')
	
		}
		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" onload="cmdFocus()" MS_POSITIONING="GridLayout">
		<form id="ConsignmentStatusPrinting" method="post" runat="server">
			<INPUT style="Z-INDEX: 101; POSITION: absolute; TOP: 680px; LEFT: 16px" type="hidden" name="ScrollPosition">
			<asp:validationsummary style="Z-INDEX: 105; POSITION: absolute; COLOR: red; TOP: 672px; LEFT: 240px" id="Validationsummary1"
				ShowSummary="False" HeaderText="Please enter the missing  fields." ShowMessageBox="True" DisplayMode="BulletList"
				Runat="server"></asp:validationsummary>
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 1008px; HEIGHT: 1248px; TOP: 32px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tr>
						<td colSpan="2" align="left"><asp:label id="Label1" runat="server" Width="421px" Height="27px" CssClass="mainTitleSize">
							Export Delivered Consignments</asp:label></td>
					</tr>
					<tr>
						<td style="HEIGHT: 24px" vAlign="top" colSpan="2" align="left"><asp:button style="Z-INDEX: 0" id="btnQry" runat="server" Width="61px" CssClass="queryButton"
								Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExecQry" runat="server" Width="130px" CssClass="queryButton" Text="Execute Query"
								CausesValidation="False"></asp:button>
                            <asp:button id="btnExportConsign" runat="server" Width="200px" CssClass="queryButton" Text="Export Consignments"
								CausesValidation="False" Enabled="False"></asp:button>
                            <asp:button id="btnExportConsignFTP" runat="server" Width="200px" CssClass="queryButton" Text="Export Consignments (FTP)"
								CausesValidation="False" Enabled="False"></asp:button>
						</td>
					</tr>
					<tr>
						<td vAlign="top" colSpan="2" align="left">
							<TABLE style="Z-INDEX: 0; WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0" width="740"
								runat="server">
								<TR width="100%">
									<TD style="Z-INDEX: 0; HEIGHT: 126px" width="100%">&nbsp;
										<asp:label style="Z-INDEX: 0" id="lblErrorMsg" runat="server" Width="566px" Height="19px" ForeColor="Red"
											Font-Bold="True" Font-Size="X-Small"></asp:label>
										<TABLE style="Z-INDEX: 0" id="Table2" border="0" cellSpacing="1" cellPadding="1">
											<TR id="TrNonCustoms" runat="server">
												<TD style="WIDTH: 199px; HEIGHT: 20px"><FONT face="Tahoma"><asp:label style="Z-INDEX: 0" id="lblPOD" runat="server" Width="100px" CssClass="tableLabel">Actual POD Date:</asp:label></FONT></TD>
												<TD style="WIDTH: 298px; HEIGHT: 20px" width="298"><FONT face="Tahoma"><cc1:mstextbox style="Z-INDEX: 0" id="txtPodDate" tabIndex="10" runat="server" Width="114px" CssClass="textField"
															TextMaskString="99/99/9999" TextMaskType="msDatePOD" MaxLength="12" autofocus="autofocus"></cc1:mstextbox>&nbsp;<asp:label id="lblTo" runat="server" CssClass="tableLabel">To</asp:label>&nbsp;
														<cc1:mstextbox id="txtPodDate2" tabIndex="11" runat="server" Width="114px" CssClass="textField"
															TextMaskString="99/99/9999" TextMaskType="msDatePOD" MaxLength="12"></cc1:mstextbox></FONT></TD>
												<TD style="WIDTH: 145px"><FONT face="Tahoma"></FONT></TD>
												<TD></TD>
											</TR>
											<TR id="TrCustoms" runat="server">
												<TD style="WIDTH: 199px"><asp:label style="Z-INDEX: 0" id="lblOnIn" runat="server" Width="152px" CssClass="tableLabel">On International MAWB:</asp:label></TD>
												<TD style="WIDTH: 298px"><FONT face="Tahoma"><asp:checkbox id="chkOnInterlMAWB" tabIndex="12" runat="server" TextAlign="Left" Checked="True"></asp:checkbox></FONT></TD>
												<TD style="WIDTH: 145px"><FONT face="Tahoma"></FONT></TD>
												<TD></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 199px"><asp:label id="lblInclude" runat="server" Width="168px" CssClass="tableLabel">Include International Docs:</asp:label></TD>
												<TD style="WIDTH: 298px"><asp:checkbox style="Z-INDEX: 0" id="chkIncludeInter" tabIndex="13" runat="server" TextAlign="Left"
														Checked="True"></asp:checkbox></TD>
												<TD style="WIDTH: 145px"></TD>
												<TD></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 199px; HEIGHT: 16px"><asp:label style="Z-INDEX: 0" id="lblShowOnly" runat="server" Width="210px" Height="14px" CssClass="tableLabel">Show Only if not already exported:</asp:label></TD>
												<TD style="WIDTH: 298px; HEIGHT: 16px"><asp:checkbox style="Z-INDEX: 0" id="chkOnIfNot" tabIndex="14" runat="server" TextAlign="Left"
														Checked="True"></asp:checkbox></TD>
												<TD style="WIDTH: 145px; HEIGHT: 16px"></TD>
												<TD style="HEIGHT: 16px" width="450"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 199px; HEIGHT: 21px"><asp:label id="Label9" runat="server" CssClass="tableLabel" style="Z-INDEX: 0"> House AWB Number:</asp:label></TD>
												<TD style="WIDTH: 298px; HEIGHT: 21px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="HouseAWBNo" tabIndex="15" onkeypress="ValidateMasterAWBNumber(event)"
														onpaste="AfterPasteMasterAWBNumber(this)" runat="server" Width="150px" CssClass="textField" MaxLength="30"></asp:textbox></TD>
												<TD style="WIDTH: 145px; HEIGHT: 21px"></TD>
												<TD style="HEIGHT: 21px"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 199px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="Label8" runat="server" CssClass="tableLabel">MAWB Number:</asp:label></TD>
												<TD style="WIDTH: 298px; HEIGHT: 21px"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="MasterAWBNo" tabIndex="16" onkeypress="ValidateMasterAWBNumber(event)"
														onpaste="AfterPasteMasterAWBNumber(this)" runat="server" Width="150px" CssClass="textField" MaxLength="30"></asp:textbox></TD>
												<TD style="WIDTH: 145px; HEIGHT: 21px"></TD>
												<TD style="HEIGHT: 21px"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 199px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="lblServiceType" runat="server" CssClass="tableLabel">Export Format:</asp:label></TD>
												<TD style="WIDTH: 298px; HEIGHT: 21px"><asp:dropdownlist style="Z-INDEX: 0" id="ddlExportFormat" tabIndex="17" runat="server" Width="106px"
														Enabled="True"></asp:dropdownlist></TD>
												<TD style="WIDTH: 145px; HEIGHT: 21px"></TD>
												<TD style="HEIGHT: 21px"></TD>
											</TABLE>
									</TD>
								</TR>
							</TABLE>
							<table width="100%">
								<tr class="gridHeading">
									<td colSpan="6"><STRONG><FONT size="2">Consignment Details</FONT></STRONG></td>
								</tr>
							</table>
							<asp:datagrid style="Z-INDEX: 0" id="dgExportDelivered" runat="server" Width="1000px" SelectedItemStyle-CssClass="gridFieldSelected"
								PageSize="20" AutoGenerateColumns="False" HorizontalAlign="Left" AllowPaging="True">
								<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
								<ItemStyle Height="15px" CssClass="gridField"></ItemStyle>
								<HeaderStyle Font-Bold="True" HorizontalAlign="Center" Height="40px" Width="2%" CssClass="gridHeading"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn>
										<ItemStyle HorizontalAlign="Center" Height="25px" Width="50px"></ItemStyle>
										<ItemTemplate>
											<asp:CheckBox style="Z-INDEX: 0" id=chkNo Runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"Check") %>' AutoPostBack="True">
											</asp:CheckBox>
											<asp:Label style="Z-INDEX: 0" id=lblBookNo Runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"booking_no")%>' Visible="False">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn>
										<HeaderStyle Width="100px"></HeaderStyle>
										<HeaderTemplate>
											<asp:Label ID="lblHeadNumber" Runat="server" Text="Number"></asp:Label>
										</HeaderTemplate>
										<ItemTemplate>
											<center>
												<asp:Label ID="lblConNo" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>'>
												</asp:Label>
											</center>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn>
										<HeaderStyle Width="150px"></HeaderStyle>
										<HeaderTemplate>
											<asp:Label ID="lblHeadPOD" Runat="server" Text="POD Date/Time"></asp:Label>
										</HeaderTemplate>
										<ItemTemplate>
											<center>
												<asp:Label ID="PODDate" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"act_delivery_date","{0:dd/MM/yyyy HH:mm}")%>'>
												</asp:Label>
											</center>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn>
										<HeaderStyle Width="185px"></HeaderStyle>
										<HeaderTemplate>
											<asp:Label ID="Label2" Runat="server" Text="Consignee"></asp:Label>
										</HeaderTemplate>
										<ItemTemplate>
											<center>
												<asp:Label ID="lblConsignee" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ConsigneeName")%>'>
												</asp:Label>
											</center>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn>
										<HeaderStyle Width="100px"></HeaderStyle>
										<HeaderTemplate>
											<asp:Label ID="Label3" Runat="server" Text="Reference No."></asp:Label>
										</HeaderTemplate>
										<ItemTemplate>
											<center>
												<asp:Label ID="lblRefNo" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ref_no")%>'>
												</asp:Label>
											</center>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn>
										<HeaderStyle Width="100px"></HeaderStyle>
										<HeaderTemplate>
											<asp:Label ID="Label4" Runat="server" Text="Delivery DC"></asp:Label>
										</HeaderTemplate>
										<ItemTemplate>
											<center>
												<asp:Label ID="lblDeliveryDC" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"DeliveryDC")%>'>
												</asp:Label>
											</center>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn>
										<HeaderStyle Width="100px"></HeaderStyle>
										<HeaderTemplate>
											<asp:Label ID="Label5" Runat="server" Text="Exported"></asp:Label>
										</HeaderTemplate>
										<ItemTemplate>
											<center>
												<asp:Label ID="lblExportedDT" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ExportedDT","{0:dd/MM/yyyy HH:mm}")%>'>
												</asp:Label>
											</center>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn>
										<HeaderStyle Width="70px"></HeaderStyle>
										<HeaderTemplate>
											<asp:Label ID="Label6" Runat="server" Text="Intl. MAWB"></asp:Label>
										</HeaderTemplate>
										<ItemTemplate>
											<center>
												<asp:Label ID="lblMAWBNo" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MAWBNo")%>'>
												</asp:Label>
											</center>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn>
										<HeaderStyle Width="50px"></HeaderStyle>
										<HeaderTemplate>
											<asp:Label ID="Label7" Runat="server" Text="Pkgs"></asp:Label>
										</HeaderTemplate>
										<ItemTemplate>
											<center>
												<asp:Label ID="lblPkgs" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"tot_pkg")%>'>
												</asp:Label>
											</center>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
							</asp:datagrid></td>
					</tr>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
