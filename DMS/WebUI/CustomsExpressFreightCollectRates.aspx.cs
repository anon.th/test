using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.applicationpages;
using com.common.util;
using com.common.RBAC;
using com.ties.DAL;
using TIESClasses;
using TIESDAL;


namespace TIES.WebUI
{
	public class CustomsExpressFreightCollectRates : BasePage
	{
		#region Web Form Designer generated code
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.WebControls.Label lbHeader;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Label lbZone;
		protected System.Web.UI.WebControls.DropDownList ddlZone;
		protected System.Web.UI.WebControls.Label lbEffectiveDate;
		protected System.Web.UI.WebControls.Label lbWeightFrom;
		protected System.Web.UI.WebControls.Label lbWeightTo;
		protected com.common.util.msTextBox txtWeightFrom;
		protected com.common.util.msTextBox txtWeightTo;
		protected System.Web.UI.WebControls.Label lbCopy;
		protected com.common.util.msTextBox txtCopyDate;
		protected System.Web.UI.WebControls.Button btCopy;
		protected System.Web.UI.WebControls.DataGrid gridExpressRate;
		protected com.common.util.msTextBox txtEffectiveDateAdd;
		protected com.common.util.msTextBox txtEffectiveDate;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btCopy.Click += new System.EventHandler(this.btCopy_Click);
			this.gridExpressRate.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.gridExpressRate_ItemCommand);
			this.gridExpressRate.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.gridExpressRate_PageIndexChanged);
			this.gridExpressRate.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.gridExpressRate_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private string appID = string.Empty;
		private string enterpriseID = string.Empty;
		private string userLoggin = string.Empty;

		public int NumberScale
		{
			get
			{
				if(ViewState["NumberScale"] == null)
				{
					return 2;
				}
				else
				{
					return (int)ViewState["NumberScale"];
				}
			}
			set
			{
				ViewState["NumberScale"]=value;
			}
		}
		public string currency_decimal
		{
			get
			{
				if(ViewState["currency_decimal"] == null)
				{
					return "00";
				}
				else
				{
					return (string)ViewState["currency_decimal"];
				}
			}
			set
			{
				ViewState["currency_decimal"]=value;
			}
		}
		public DataSet dsExpFrCollectRates
		{
			get
			{
				if(ViewState["dsExpFrCollectRates"] == null)
					return new DataSet();
				else
					return (DataSet)ViewState["dsExpFrCollectRates"];
			}
			set
			{
				ViewState["dsExpFrCollectRates"] = value;
			} 
		}

		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				System.Text.StringBuilder s = new System.Text.StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("SetScrollPosition();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}
		private void InitGridPage()
		{
			CustomsExpFrCollectRatesDAL db = new CustomsExpFrCollectRatesDAL(appID, 
				enterpriseID);
			gridExpressRate.PageSize = int.Parse(db.GetEnterpriseConfigurations(enterpriseID));
		}
		private void InitLoad()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("Zone");
			dt.Columns.Add("EffectiveDate");
			dt.Columns.Add("MaxWeight");
			dt.Columns.Add("RateToHub");
			dt.Columns.Add("RateToOthers");

			dt.AcceptChanges();

			gridExpressRate.EditItemIndex = -1;
			gridExpressRate.CurrentPageIndex = 0;

			gridExpressRate.DataSource = dt;
			gridExpressRate.DataBind();
		}

		private DataTable GetZone()
		{
			CustomsExpFrCollectRatesDAL db = new CustomsExpFrCollectRatesDAL(appID, enterpriseID);
			return db.GetCustomsSpecialConfiguration(enterpriseID).Tables[0];
		}

		private string GetCurrencyDigitsOfEnterpriseConfiguration()
		{
			DataSet dsEprise = SysDataManager1.GetEnterpriseProfile(this.appID, this.enterpriseID);
			DataRow dr = dsEprise.Tables[0].Rows[0];
			
			string tempplate = "00";
			if(dr["currency_decimal"].ToString()!="")
			{
				tempplate="";
				int number_digit = Convert.ToInt32(dr["currency_decimal"].ToString());
				NumberScale = number_digit;
				for (int i = 0 ; i< number_digit ; i++)
				{
					tempplate = tempplate+"0";
				}
			}
			return "#,##0." + tempplate;
		}
		private void DisplayddlZone()
		{
			ddlZone.DataSource = GetZone();
			ddlZone.DataTextField = "DropDownListDisplayValue";
			ddlZone.DataValueField = "CodedValue";
			ddlZone.DataBind();
		}
		private void Page_Load(object sender, System.EventArgs e)
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings, Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userLoggin = utility.GetUserID();

			if(!Page.IsPostBack)
			{
				InitLoad();
				InitGridPage();
				DisplayddlZone();
				currency_decimal =  this.GetCurrencyDigitsOfEnterpriseConfiguration();
				SetInitialFocus(ddlZone);
			}
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			ddlZone.SelectedIndex = 0;
			txtEffectiveDate.Text = string.Empty;
			txtWeightFrom.Text = string.Empty;
			txtWeightTo.Text = string.Empty;
			txtCopyDate.Text = string.Empty;
			lblError.Text = string.Empty;
			InitLoad();
			SetInitialFocus(ddlZone);
		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			try
			{
				DataSet dsResult = new DataSet();
				CustomsExpFrCollectRatesDAL db = new CustomsExpFrCollectRatesDAL(appID, enterpriseID);
				TIESClasses.CustomsExpFrCollectRates objInfo = new TIESClasses.CustomsExpFrCollectRates();	
				
				txtCopyDate.Text = string.Empty;

				objInfo.Action = "0";
				objInfo.EnterpriseId = enterpriseID;
				objInfo.UserLoggedin = userLoggin;
				objInfo.WeightFrom = txtWeightFrom.Text.Trim();
				objInfo.WeightTo = txtWeightTo.Text.Trim();
				objInfo.EffectiveDate = !txtEffectiveDate.Text.Equals("") ? DateTime.ParseExact(txtEffectiveDate.Text, "dd/MM/yyyy", null).ToString("yyyy-MM-dd") : string.Empty;
				objInfo.Zone = ddlZone.SelectedValue;

				dsResult = db.ExecCustomsExpFrCollectRates(objInfo);

				if(dsResult.Tables[0].Rows[0][0].ToString().Equals("0"))
				{
					dsExpFrCollectRates = dsResult;
					gridExpressRate.DataSource = dsExpFrCollectRates.Tables[1];
					gridExpressRate.DataBind();
				}

				this.lblError.Text = dsResult.Tables[0].Rows[0][2].ToString();
			}
			catch(Exception ex)
			{
				this.lblError.Text = ex.Message;
			}
		}

		private void gridExpressRate_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			try
			{
				string command = e.CommandName;

				lblError.Text = string.Empty;

				if(command.Equals("ADD_ITEM"))
				{
					#region ADD_ITEM

					DataSet dsResult = new DataSet();
					DropDownList ddlOutFromZoneAdd = (DropDownList)e.Item.FindControl("ddlOutFromZoneAdd");
					com.common.util.msTextBox txtEffectiveDateAdd = (com.common.util.msTextBox)e.Item.FindControl("txtEffectiveDateAdd");
					com.common.util.msTextBox txtMaxWeightAdd = (com.common.util.msTextBox)e.Item.FindControl("txtMaxWeightAdd");
					com.common.util.msTextBox txtInboundToHubAdd = (com.common.util.msTextBox)e.Item.FindControl("txtInboundToHubAdd");
					com.common.util.msTextBox txtInboundToOthersAdd = (com.common.util.msTextBox)e.Item.FindControl("txtInboundToOthersAdd");

					CustomsExpFrCollectRatesDAL db = new CustomsExpFrCollectRatesDAL(appID, enterpriseID);
					TIESClasses.CustomsExpFrCollectRates objInfo = new TIESClasses.CustomsExpFrCollectRates();	

					objInfo.Action = "2";
					objInfo.EnterpriseId = enterpriseID;
					objInfo.UserLoggedin = userLoggin;
					objInfo.Zone = ddlOutFromZoneAdd.SelectedValue;
					objInfo.EffectiveDate = !txtEffectiveDateAdd.Text.Equals("") ? DateTime.ParseExact(txtEffectiveDateAdd.Text, "dd/MM/yyyy", null).ToString("yyyy-MM-dd") : string.Empty;
					objInfo.MaxWeight = txtMaxWeightAdd.Text.Trim();
					objInfo.RateToHub = txtInboundToHubAdd.Text.Trim();
					objInfo.RateToOthers = txtInboundToOthersAdd.Text.Trim();

					dsResult = db.ExecCustomsExpFrCollectRates(objInfo);

					if(dsResult.Tables[0].Rows[0][0].ToString().Equals("0"))
					{
						dsExpFrCollectRates = dsResult;
						gridExpressRate.DataSource = dsExpFrCollectRates.Tables[1];
						gridExpressRate.DataBind();
					}

					this.lblError.Text = dsResult.Tables[0].Rows[0][2].ToString();

					#endregion
				}
				else if(command.Equals("EDIT_ITEM"))
				{ 
					#region EDIT_ITEM
					gridExpressRate.EditItemIndex = e.Item.ItemIndex; 

					gridExpressRate.DataSource = dsExpFrCollectRates.Tables[1];
					gridExpressRate.DataBind();

					#endregion
				} 
				else if(command.Equals("SAVE_ITEM"))
				{
					#region SAVE_ITEM

					DataSet dsResult = new DataSet();
					DropDownList ddlOutFromZoneEdit = (DropDownList)e.Item.FindControl("ddlOutFromZoneEdit");
					com.common.util.msTextBox txtEffectiveDateEdit = (com.common.util.msTextBox)e.Item.FindControl("txtEffectiveDateEdit");
					com.common.util.msTextBox txtMaxWeightEdit = (com.common.util.msTextBox)e.Item.FindControl("txtMaxWeightEdit");
					com.common.util.msTextBox txtInboundToHubEdit = (com.common.util.msTextBox)e.Item.FindControl("txtInboundToHubEdit");
					com.common.util.msTextBox txtInboundToOthersEdit = (com.common.util.msTextBox)e.Item.FindControl("txtInboundToOthersEdit");

					CustomsExpFrCollectRatesDAL db = new CustomsExpFrCollectRatesDAL(appID, enterpriseID);
					TIESClasses.CustomsExpFrCollectRates objInfo = new TIESClasses.CustomsExpFrCollectRates();	

					objInfo.Action = "2";
					objInfo.EnterpriseId = enterpriseID;
					objInfo.UserLoggedin = userLoggin;
					objInfo.Zone = ddlOutFromZoneEdit.SelectedValue;
					objInfo.EffectiveDate = !txtEffectiveDateEdit.Text.Equals("") ? DateTime.ParseExact(txtEffectiveDateEdit.Text, "dd/MM/yyyy", null).ToString("yyyy-MM-dd") : string.Empty;
					objInfo.MaxWeight = txtMaxWeightEdit.Text.Trim();
					objInfo.RateToHub = txtInboundToHubEdit.Text.Trim();
					objInfo.RateToOthers = txtInboundToOthersEdit.Text.Trim();

					dsResult = db.ExecCustomsExpFrCollectRates(objInfo);

					if(dsResult.Tables[0].Rows[0][0].ToString().Equals("0"))
					{
						dsExpFrCollectRates = dsResult;
						gridExpressRate.CurrentPageIndex = 0;
						gridExpressRate.EditItemIndex = -1;

						gridExpressRate.DataSource = dsExpFrCollectRates.Tables[1];
						gridExpressRate.DataBind();
					}

					this.lblError.Text = dsResult.Tables[0].Rows[0][2].ToString();

		
					#endregion
				}
				else if(command.Equals("CANCEL_ITEM"))
				{
					#region CANCEL_ITEM
					gridExpressRate.EditItemIndex = -1;

					gridExpressRate.DataSource = dsExpFrCollectRates.Tables[1];
					gridExpressRate.DataBind();
					#endregion
				}
				else if(command.Equals("DELETE_ITEM"))
				{
					#region DELETE_ITEM

					Label lbZoneItem = (Label)e.Item.FindControl("lbZoneItem");
					Label lbEffectiveDateItem = (Label)e.Item.FindControl("lbEffectiveDateItem");
					Label lbMaxWeight = (Label)e.Item.FindControl("lbMaxWeight");
					Label lbRateToHub = (Label)e.Item.FindControl("lbRateToHub");
					Label lbRateToOthers = (Label)e.Item.FindControl("lbRateToOthers");

					DataSet dsResult = new DataSet();
					CustomsExpFrCollectRatesDAL db = new CustomsExpFrCollectRatesDAL(appID, enterpriseID);
					TIESClasses.CustomsExpFrCollectRates objInfo = new TIESClasses.CustomsExpFrCollectRates();	

					objInfo.Action = "3";
					objInfo.EnterpriseId = enterpriseID;
					objInfo.UserLoggedin = userLoggin;
					objInfo.Zone = lbZoneItem.Text.Trim();
					objInfo.EffectiveDate = !lbEffectiveDateItem.Text.Equals("") ? DateTime.ParseExact(lbEffectiveDateItem.Text, "dd/MM/yyyy", null).ToString("yyyy-MM-dd") : string.Empty;
					objInfo.MaxWeight = lbMaxWeight.Text.Trim();
					objInfo.RateToHub = lbRateToHub.Text.Trim();
					objInfo.RateToOthers = lbRateToOthers.Text.Trim();

					dsResult = db.ExecCustomsExpFrCollectRates(objInfo);

					if(dsResult.Tables[0].Rows[0][0].ToString().Equals("0"))
					{
						dsExpFrCollectRates = dsResult;
						gridExpressRate.CurrentPageIndex = 0;
						gridExpressRate.EditItemIndex = -1;

						gridExpressRate.DataSource = dsExpFrCollectRates.Tables[1];
						gridExpressRate.DataBind();
					}

					this.lblError.Text = dsResult.Tables[0].Rows[0][2].ToString();
					#endregion
				}
			}
			catch(Exception ex)
			{
				this.lblError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void gridExpressRate_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			try
			{
				if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
				{
					#region Item
					DataRowView dr = (DataRowView)e.Item.DataItem;

					Label lbZoneItem = (Label)e.Item.FindControl("lbZoneItem");
					if (lbZoneItem != null)
						lbZoneItem.Text = dr["Zone"].ToString();
					Label lbEffectiveDateItem = (Label)e.Item.FindControl("lbEffectiveDateItem");
					if (lbEffectiveDateItem != null)
						lbEffectiveDateItem.Text = DateTime.Parse(dr["EffectiveDate"].ToString()).ToString("dd/MM/yyyy");
					Label lbMaxWeight = (Label)e.Item.FindControl("lbMaxWeight");
					if (lbMaxWeight != null)
						if(dr["MaxWeight"] != null)
							lbMaxWeight.Text = Convert.ToDecimal(dr["MaxWeight"].ToString()).ToString("#,##0.0");
					Label lbRateToHub = (Label)e.Item.FindControl("lbRateToHub");
					if (lbRateToHub != null)
						lbRateToHub.Text = Convert.ToDecimal(dr["RateToHub"].ToString()).ToString(currency_decimal);
					Label lbRateToOthers = (Label)e.Item.FindControl("lbRateToOthers");
					if (lbRateToOthers != null)
						lbRateToOthers.Text = Convert.ToDecimal(dr["RateToOthers"].ToString()).ToString(currency_decimal);
					#endregion
				}
				if((e.Item.ItemType == ListItemType.EditItem))
				{
					#region EditItem
					DataRowView dr = (DataRowView)e.Item.DataItem;
					DropDownList ddlOutFromZoneEdit = (DropDownList)e.Item.FindControl("ddlOutFromZoneEdit");
					if (ddlOutFromZoneEdit != null)
					{
						ddlOutFromZoneEdit.DataSource = GetZone();
						ddlOutFromZoneEdit.DataTextField = "DropDownListDisplayValue";
						ddlOutFromZoneEdit.DataValueField = "CodedValue";
						ddlOutFromZoneEdit.DataBind();

						ddlOutFromZoneEdit.SelectedValue = dr["Zone"].ToString();

						SetInitialFocus(ddlOutFromZoneEdit);
					}
					com.common.util.msTextBox txtEffectiveDateEdit = (com.common.util.msTextBox)e.Item.FindControl("txtEffectiveDateEdit");
					if (txtEffectiveDateEdit != null)
						if(dr["EffectiveDate"] != null)
							txtEffectiveDateEdit.Text = DateTime.Parse(dr["EffectiveDate"].ToString()).ToString("dd/MM/yyyy");
					com.common.util.msTextBox txtMaxWeightEdit = (com.common.util.msTextBox)e.Item.FindControl("txtMaxWeightEdit");
					if (txtMaxWeightEdit != null)
					{
						if(dr["MaxWeight"] != null)
							txtMaxWeightEdit.Text = Convert.ToDecimal(dr["MaxWeight"].ToString()).ToString("#,##0.0");
					}

					com.common.util.msTextBox txtInboundToHubEdit = (com.common.util.msTextBox)e.Item.FindControl("txtInboundToHubEdit");
					if (txtInboundToHubEdit != null)
					{
						if(dr["RateToHub"] != null)
						{
							txtInboundToHubEdit.Text = Convert.ToDecimal(dr["RateToHub"].ToString()).ToString(currency_decimal);
							txtInboundToHubEdit.NumberScale = NumberScale;
						}	
					}

					com.common.util.msTextBox txtInboundToOthersEdit = (com.common.util.msTextBox)e.Item.FindControl("txtInboundToOthersEdit");
					if (txtInboundToOthersEdit != null)
					{
						if(dr["RateToOthers"] != null)
						{
							txtInboundToOthersEdit.Text = Convert.ToDecimal(dr["RateToOthers"].ToString()).ToString(currency_decimal);
							txtInboundToOthersEdit.NumberScale = NumberScale;
						}
					}

					#endregion
				}
				if((e.Item.ItemType == ListItemType.Footer))
				{
					#region Footer
					DropDownList ddlOutFromZoneAdd = (DropDownList)e.Item.FindControl("ddlOutFromZoneAdd");
					if (ddlOutFromZoneAdd != null)
					{
						ddlOutFromZoneAdd.DataSource = GetZone();
						ddlOutFromZoneAdd.DataTextField = "DropDownListDisplayValue";
						ddlOutFromZoneAdd.DataValueField = "CodedValue";
						ddlOutFromZoneAdd.DataBind();
					}
					com.common.util.msTextBox txtInboundToHubAdd = (com.common.util.msTextBox)e.Item.FindControl("txtInboundToHubAdd");
					if (txtInboundToHubAdd != null)
						txtInboundToHubAdd.NumberScale = NumberScale;
												
					com.common.util.msTextBox txtInboundToOthersAdd = (com.common.util.msTextBox)e.Item.FindControl("txtInboundToOthersAdd");
					if (txtInboundToOthersAdd != null)
						txtInboundToOthersAdd.NumberScale = NumberScale;
					#endregion
				}
			}
			catch(Exception ex)
			{
				this.lblError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void btCopy_Click(object sender, System.EventArgs e)
		{
			try
			{
				DataSet dsResult = new DataSet();
				CustomsExpFrCollectRatesDAL db = new CustomsExpFrCollectRatesDAL(appID, enterpriseID);
				TIESClasses.CustomsExpFrCollectRates objInfo = new TIESClasses.CustomsExpFrCollectRates();	

				objInfo.Action = "1";
				objInfo.EnterpriseId = enterpriseID;
				objInfo.UserLoggedin = userLoggin;
				objInfo.Zone = ddlZone.SelectedValue;
				objInfo.WeightFrom = txtWeightFrom.Text.Trim();
				objInfo.WeightTo = txtWeightTo.Text.Trim();
				objInfo.EffectiveDate = !txtEffectiveDate.Text.Equals("") ? DateTime.ParseExact(txtEffectiveDate.Text, "dd/MM/yyyy", null).ToString("yyyy-MM-dd") : string.Empty;
				objInfo.NewEffectiveDate = !txtCopyDate.Text.Equals("") ? DateTime.ParseExact(txtCopyDate.Text, "dd/MM/yyyy", null).ToString("yyyy-MM-dd") : string.Empty;

				dsResult = db.ExecCustomsExpFrCollectRates(objInfo);

				if(dsResult.Tables[0].Rows[0][0].ToString().Equals("0"))
				{
					dsExpFrCollectRates = dsResult;

					gridExpressRate.CurrentPageIndex = 0;
					gridExpressRate.EditItemIndex = -1;

					gridExpressRate.DataSource = dsExpFrCollectRates.Tables[1];
					gridExpressRate.DataBind();
				}

				this.lblError.Text = dsResult.Tables[0].Rows[0][2].ToString();
			}
			catch(Exception ex)
			{
				this.lblError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void gridExpressRate_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			try
			{
				gridExpressRate.CurrentPageIndex = e.NewPageIndex;

				gridExpressRate.DataSource = dsExpFrCollectRates.Tables[1];
				gridExpressRate.DataBind();
			}
			catch(Exception ex)
			{
				this.lblError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

	}
}
