<%@ Page Language="c#" CodeBehind="TaskTree.aspx.cs" AutoEventWireup="false" Inherits="com.ties.TaskTree" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>TaskTree</title>
    <meta content="True" name="vs_showGrid">
    <link href="<%=strStyleSheet%>" type="text/css" rel="stylesheet" name="style1">
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <script language="javascript">
	
			var x = 250;
			var y = 30;
			function closeframe()
			{
				if (x > 28)
				{
					x -= 5;
					parent.fr_nav_content.cols = x + ",100%";
					setTimeout("closeframe()",1);
				}
				else
				{
					x = 250;
				}
			}
				
			function openframe()
			{
				if (y < 250)
				{
					y += 5;
					parent.fr_nav_content.cols = y + ",100%";
					setTimeout("openframe()",1);
				}
				else
				{
					y = 30;
				}
			}
				
    </script>

    <style type="text/css">
        body{
            background-color:#E9E9CB;
        }

        .TreeViewStyle{
            color:black;
        }

        .TreeViewStyle a:hover {
            color:white;
            background-color: highlight;
            filter: progid:DXImageTransform.Microsoft.Alpha(opacity=60,style=0);
            font-weight: 400;
        }

        .TreeViewSelectStyle{
            color:white;
            background-color: highlight;
            filter: progid:DXImageTransform.Microsoft.Alpha(opacity=60,style=0);
            font-weight: 400;
        }

        .TreeViewMainStyle{
            z-index: 100; 
            position: absolute; 
            top: 19px; 
            left: 10px; 
            width: 300px; 
            height: 95%;
            overflow-y:auto;
        }

    </style>
</head>
<body background="<%=strNaviBgImage%>" ms_positioning="GridLayout">
    <form id="Form1" method="post" runat="server">
        <input onclick="closeframe()" style="z-index: 101; position: absolute; font-size: 10px; top: -1px; left: 0px" type="button" size="13" value="<">
        <input onclick="openframe()" style="z-index: 102; position: absolute; font-size: 10px; top: -1px; left: 13px" type="button" value=">">

        <asp:TreeView ID="TreeView1" runat="server" Target="displayContent" ExpandDepth="1" Indent="0" ImageUrl="images/icons/folder.gif"
            CssClass="TreeViewMainStyle">
            
            <NodeStyle CssClass="TreeViewStyle" NodeSpacing="2px" VerticalPadding="2px" HorizontalPadding="7px" />
            <SelectedNodeStyle CssClass="TreeViewSelectStyle"/>
        </asp:TreeView>
        &nbsp;
    </form>
</body>
</html>
