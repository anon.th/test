<%@ Page language="c#" Codebehind="AgentVAS.aspx.cs" AutoEventWireup="false" Inherits="com.ties.AgentVAS" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Agent VAS</title>
		<LINK rev="stylesheet" href="css/styles.css" rel="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="AgentVAS" method="post" runat="server">
			<asp:label id="lblTitle" style="Z-INDEX: 104; LEFT: 24px; POSITION: absolute; TOP: 8px" runat="server" Width="336px" Height="32px" CssClass="mainTitleSize">Agent VAS</asp:label>
			<dbcombo:dbcombo id="dbCmbAgentName" style="Z-INDEX: 122; LEFT: 488px; POSITION: absolute; TOP: 106px" tabIndex="2" runat="server" Height="17px" Width="182px" ReQueryOnLoad="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentNameServerMethod" TextBoxColumns="25" AutoPostBack="True"></dbcombo:dbcombo>
			<dbcombo:dbcombo id="dbCmbAgentId" style="Z-INDEX: 121; LEFT: 131px; POSITION: absolute; TOP: 105px" tabIndex="1" runat="server" Height="17px" Width="140px" ReQueryOnLoad="True" ShowDbComboLink="False" TextUpLevelSearchButton="v" ServerMethod="AgentIdServerMethod" TextBoxColumns="20" AutoPostBack="True"></dbcombo:dbcombo><asp:requiredfieldvalidator id="reqAgentId" style="Z-INDEX: 120; LEFT: 333px; POSITION: absolute; TOP: 49px" Runat="server" BorderWidth="0" Display="None" ControlToValidate="dbCmbAgentId" ErrorMessage="Agent Id "></asp:requiredfieldvalidator><asp:label id="lblZVASEMessage" style="Z-INDEX: 118; LEFT: 524px; POSITION: absolute; TOP: 402px" runat="server" Width="187px"></asp:label><asp:datagrid id="dgZipcodeVASExcluded" style="Z-INDEX: 117; LEFT: 24px; POSITION: absolute; TOP: 424px" runat="server" Width="710px" Height="40px" AllowCustomPaging="True" PageSize="5" AllowPaging="True" ItemStyle-Height="20" OnPageIndexChanged="dgZipcodeVASExcluded_PageChange" OnUpdateCommand="dgZipcodeVASExcluded_Update" OnDeleteCommand="dgZipcodeVASExcluded_Delete" AutoGenerateColumns="False" OnItemCommand="dgZipcodeVASExcluded_Button">
				<ItemStyle Height="20px"></ItemStyle>
				<Columns>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Postal Code">
						<HeaderStyle Font-Bold="True" Width="30%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblZipcode" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtZipcode" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msUpperAlfaNumericWithUnderscore">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="exValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtZipcode" ErrorMessage="Zipcode is required field"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="State">
						<HeaderStyle Width="30%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblState" Text='<%#DataBinder.Eval(Container.DataItem,"state_name")%>' Runat="server" >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtState" Text='<%#DataBinder.Eval(Container.DataItem,"state_name")%>' Runat="server" ReadOnly="true">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Country">
						<HeaderStyle Width="30%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblCountry" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server" >
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtCountry" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server" ReadOnly="true">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:datagrid id="dgVAS" style="Z-INDEX: 115; LEFT: 22px; POSITION: absolute; TOP: 132px" runat="server" Width="708px" AllowCustomPaging="True" PageSize="5" AllowPaging="True" ItemStyle-Height="20" OnPageIndexChanged="dgVAS_PageChange" OnUpdateCommand="dgVAS_Update" OnDeleteCommand="dgVAS_Delete" AutoGenerateColumns="False" OnItemCommand="dgVAS_Button" SelectedItemStyle-CssClass="gridFieldSelected" OnSelectedIndexChanged="dgVAS_SelectedIndexChanged" OnItemDataBound="dgVAS_Bound" OnCancelCommand="dgVAS_Cancel" OnEditCommand="dgVAS_Edit">
				<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
				<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
				<Columns>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;" CommandName="Select">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="VAS Code">
						<HeaderStyle Font-Bold="True" Width="19%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblVASCode" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtVASCode" Runat="server" Enabled="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>' Readonly=True>
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="scValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtVASCode" ErrorMessage="VAS Code is required field"></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="search">
						<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Center"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="VAS Description">
						<HeaderStyle Width="60%" CssClass="gridHeading"></HeaderStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblVASDescription" Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txtVASDescription" Runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>' MaxLength="200">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Surcharge">
						<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabelNumber" ID="lblSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtSurcharge" Runat="server" Enabled="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>'>
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:button id="btnGoToFirstPage" style="Z-INDEX: 111; LEFT: 608px; POSITION: absolute; TOP: 48px" tabIndex="5" runat="server" Width="24px" CssClass="queryButton" Text="|<" CausesValidation="False"></asp:button><asp:button id="btnPreviousPage" style="Z-INDEX: 105; LEFT: 632px; POSITION: absolute; TOP: 48px" tabIndex="6" runat="server" Width="24px" CssClass="queryButton" Text="<" CausesValidation="False"></asp:button><asp:textbox id="txtCountRec" style="Z-INDEX: 109; LEFT: 656px; POSITION: absolute; TOP: 48px" tabIndex="7" runat="server" Width="24px" Height="19px"></asp:textbox><asp:button id="btnNextPage" style="Z-INDEX: 106; LEFT: 680px; POSITION: absolute; TOP: 48px" tabIndex="8" runat="server" Width="24px" CssClass="queryButton" Text=">" CausesValidation="False"></asp:button><asp:button id="btnGoToLastPage" style="Z-INDEX: 108; LEFT: 704px; POSITION: absolute; TOP: 48px" tabIndex="9" runat="server" Width="24px" CssClass="queryButton" Text=">|" CausesValidation="False"></asp:button><asp:label id="lblAgentName" style="Z-INDEX: 110; LEFT: 386px; POSITION: absolute; TOP: 110px" runat="server" Width="100px" CssClass="tableLabel">Agent Name</asp:label><asp:label id="lblAgentCode" style="Z-INDEX: 107; LEFT: 26px; POSITION: absolute; TOP: 110px" runat="server" Width="100px" CssClass="tableLabel">Agent Code</asp:label><asp:button id="btnQuery" style="Z-INDEX: 100; LEFT: 22px; POSITION: absolute; TOP: 48px" runat="server" Width="62px" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExecQry" style="Z-INDEX: 101; LEFT: 84px; POSITION: absolute; TOP: 48px" runat="server" Width="130px" CssClass="queryButton" Text="Execute Query" CausesValidation="False"></asp:button><asp:label id="lblVASMessage" style="Z-INDEX: 102; LEFT: 28px; POSITION: absolute; TOP: 79px" runat="server" Width="439px" CssClass="errorMsgColor">Error Message</asp:label></TD></TD>&nbsp;</TD>&nbsp;
			<asp:button id="btnInsertVAS" style="Z-INDEX: 112; LEFT: 214px; POSITION: absolute; TOP: 48px" runat="server" Width="57px" CssClass="queryButton" Text="Insert" CausesValidation="False"></asp:button><asp:button id="btnZVASEInsert" style="Z-INDEX: 113; LEFT: 27px; POSITION: absolute; TOP: 396px" runat="server" Width="54px" CssClass="queryButton" Text="Insert" CausesValidation="False"></asp:button><asp:label id="lblEXMessage" style="Z-INDEX: 114; LEFT: 99px; POSITION: absolute; TOP: 400px" runat="server" Width="365px" Height="20px" CssClass="errorMsgColor"></asp:label><asp:validationsummary id="PageValidationSummary" style="Z-INDEX: 103; LEFT: 39px; POSITION: absolute; TOP: 515px" runat="server" Width="460px" Height="70px" HeaderText="Please enter the following mandatory field(s):<br><br>" DisplayMode="BulletList" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary><INPUT 
type=hidden value="<%=strScrollPosition%>" name=ScrollPosition>
			<asp:label id="lblVASETitle" style="Z-INDEX: 119; LEFT: 32px; POSITION: absolute; TOP: 356px" runat="server" CssClass="mainTitleSize">Postal Code Excluded for VAS</asp:label></form>
	</body>
</HTML>
