<%@ Page language="c#" Codebehind="CustomsReport_BondStoreAging.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsReport_BondStoreAging" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CustomsReport_BondStoreAging</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="vs_snapToGrid" content="true">
		<meta name="vs_showGrid" content="true">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
		function funcDisBack(){
			history.go(+1);
		}

        function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }
        function validate(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventdefault) theEvent.preventdefault();
			}
		}
		
		/*function openPopup(link) {
			var html = "<html><head><title></title>";
			html += "</head><body style='margin: 0;'>";
			html += "<iframe height='100%' width='100%' src='" + link +"'></iframe>";
			html += "</body></html>";
			window.top.displayBanner.win_op = window.open("", "_blank", "resizable=1,status=0,toolbar=0,menubar=0");
			window.top.displayBanner.win_op.document.write(html);
		}*/
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onunload="window.top.displayBanner.fnCloseAll(0);">
		<form id="CustomsReport_CheckRequisitions" method="post" runat="server">
			<div style="Z-INDEX: 102; POSITION: relative; WIDTH: 900px; HEIGHT: 1543px; TOP: 34px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tbody>
						<tr>
							<td colSpan="2" align="left"><asp:label id="Label1" runat="server" CssClass="mainTitleSize" Height="27px" Width="421px">
								Bond Store Aging</asp:label></td>
						</tr>
						<tr>
							<td colspan="2">
								<asp:button id="btnQry" runat="server" CssClass="queryButton" CausesValidation="False" Text="Query"></asp:button><asp:button id="btnExecuteQuery" runat="server" CssClass="queryButton" CausesValidation="False"
									Text="Generate Report"></asp:button>
							</td>
						</tr>
						<tr>
							<td style="HEIGHT: 19px" colSpan="4"><asp:label id="lbError" runat="server" Font-Bold="true" Font-Size="X-Small" ForeColor="Red"></asp:label></td>
						</tr>
						<tr>
							<td width="10%">
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label2">As of Date</asp:label>
								<asp:label runat="server" CssClass="tableLabel" ForeColor="Red" ID="Label3">*</asp:label>
							</td>
							<td>
								<cc1:mstextbox id="txtAsOfDate" tabIndex="1" runat="server" CssClass="textField" Width="82px" MaxLength="12"
									TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox>
							</td>
						</tr>
						<tr>
							<td>
								<asp:label style="Z-INDEX: 0" runat="server" CssClass="tableLabel" ID="Label4">Bond Store</asp:label>
								<asp:label runat="server" CssClass="tableLabel" ForeColor="Red" ID="Label5">*</asp:label>
							</td>
							<td>
								<asp:dropdownlist style="Z-INDEX: 0" tabIndex="2" runat="server" AutoPostBack="True" ID="ddlBondStore"
									Width="130px"></asp:dropdownlist>
							</td>
						</tr>
					</tbody>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
