using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.ties.classes;
using com.common.applicationpages;


namespace com.ties
{
	/// <summary>
	/// Summary description for ShipmentTrackingQuery_New.
	/// </summary>
	public class ShipmentTrackingQuery_New : BasePage
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnCancelQry;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.RadioButton rbBookingDate;
		protected System.Web.UI.WebControls.RadioButton rbPickupDate;
		protected System.Web.UI.WebControls.RadioButton rbStatusDate;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.Label lblPayerCustomer;
		protected System.Web.UI.WebControls.Label lblPayerIdCus;
		protected com.common.util.msTextBox txtPayerCode;
		protected System.Web.UI.WebControls.Button btnPayerCode;
		protected System.Web.UI.WebControls.Label lblPayerTypeCus;
		protected System.Web.UI.WebControls.Label lblMasterAcc;
		protected System.Web.UI.WebControls.Label lblRouteType;
		protected System.Web.UI.WebControls.RadioButton rbPickUp;
		protected System.Web.UI.WebControls.RadioButton rbDelivery;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected System.Web.UI.WebControls.Label lblDestination;
		protected System.Web.UI.WebControls.Label lblZipCode;
		protected com.common.util.msTextBox txtZipCode;
		protected System.Web.UI.WebControls.Button btnZipCode;
		protected System.Web.UI.WebControls.Label lblStateCode;
		protected com.common.util.msTextBox txtStateCode;
		protected System.Web.UI.WebControls.Button btnStateCode;
		protected System.Web.UI.WebControls.Label lblShipment;
		protected System.Web.UI.WebControls.Label lblConsignmentNo;
		protected System.Web.UI.WebControls.TextBox txtConsignmentNo;
		protected System.Web.UI.WebControls.Label lblRefNo;
		protected System.Web.UI.WebControls.TextBox txtRefNo;
		protected System.Web.UI.WebControls.Label lblBookingNo;
		protected com.common.util.msTextBox txtBookingNo;
		protected System.Web.UI.WebControls.Label lblBookingType;
		protected System.Web.UI.WebControls.DropDownList Drp_BookingType;
		protected System.Web.UI.WebControls.Label lblStatus;
		protected System.Web.UI.WebControls.Label lblStatusCode;
		protected com.common.util.msTextBox txtStatusCode;
		protected System.Web.UI.WebControls.Button btnStatusCode;
		protected System.Web.UI.WebControls.Label lblExceptionCode;
		protected com.common.util.msTextBox txtExceptionCode;
		protected System.Web.UI.WebControls.Button btnExceptionCode;
		protected System.Web.UI.WebControls.Label lblInvoiceIssued;
		protected System.Web.UI.WebControls.RadioButton rbLastInHistory;
		protected System.Web.UI.WebControls.Label lblShipmentClosed;
		protected System.Web.UI.WebControls.RadioButton rbCheckAllYes;
		protected System.Web.UI.WebControls.RadioButton rbInHistory;
		protected System.Web.UI.WebControls.RadioButton rbMissing;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentTracking;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.HtmlControls.HtmlTable tblPayerType;
		protected System.Web.UI.HtmlControls.HtmlTable tblRouteType;
		protected System.Web.UI.HtmlControls.HtmlTable tblDestination;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipment;
		protected System.Web.UI.WebControls.Label lblStatusIs;
		protected System.Web.UI.WebControls.RadioButton rbInvoiceNo;
		protected System.Web.UI.WebControls.RadioButton rbInvoiceIgnore;
		protected System.Web.UI.WebControls.RadioButton rbCheckAllNo;
		protected System.Web.UI.WebControls.RadioButton rbCheckAllIgnore;
		protected System.Web.UI.WebControls.RadioButton rbInvoiceYes;
		protected com.common.util.msTextBox txtMasterAcc;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.WebControls.ListBox lsbCustType;

		
		public string m_strAppID=null;
		public string m_strEnterpriseID=null;
		public string m_strCulture=null;
		public string m_strUserRole=null;
		public string m_strUserId=null;
		public string ConsignmentNo;

		protected System.Web.UI.WebControls.Label lblServiceType;
		protected System.Web.UI.WebControls.ListBox lsbServiceType;
		protected System.Web.UI.WebControls.DropDownList cboRouteCode;
		protected com.common.util.msTextBox Mstextbox1;
		private DataView m_dvMonths;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			m_strUserId = utility.GetUserID();
			m_strUserRole = Session["UserRole"].ToString();

//			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			if(!Page.IsPostBack)
			{
				DefaultScreen();				
				Session["toRefresh"]=false;
				SetControlFromEnterpriseConfigurations();
				LoadBookingTypeList();
				BindServiceType();
				BindRouteCode();

				if(Request.QueryString["consignment_no"] != null && Request.QueryString["consignment_no"].ToString().Trim() != "")
				{
					txtConsignmentNo.Text=Request.QueryString["consignment_no"].ToString().Trim();
					btnExecuteQuery_Click(null,null);
				}
				if(Request.QueryString["pagemode"] != null && Request.QueryString["pagemode"].ToString().Trim() == "popup")
				{
					btnQuery.Visible=false;
					btnExecuteQuery.Visible=false;
					btnCancelQry.Visible = false;

				}
			}
			//SetDbComboServerStates();
		}

		private DataView CreateMonths(bool showNilOption)
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			if(showNilOption)
			{
				DataRow drNilRow = dtMonths.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtMonths.Rows.Add(drNilRow);
			}
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);
			return dvMonths;
		}
		private void BindMonths(DataView dvMonths)
		{
			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}

		private int LastDayOfMonth(String strMonth, String strYear)
		{
			int intLastDay = 0;
			switch (strMonth)
			{
				case "01":
				case "03":
				case "05":
				case "07":
				case "08":
				case "10":
				case "12":
					intLastDay = 31;
					break;
				case "04":
				case "06":
				case "09":
				case "11":
					intLastDay = 30;
					break;
				case "02":
					intLastDay = 28;
					break;
			}
			return intLastDay;
		}

		private void DefaultScreen()
		{
			rbBookingDate.Checked = true;
			rbPickupDate.Checked = false;
			rbStatusDate.Checked = false;
			rbMonth.Checked = true;
			rbPeriod.Checked = false;
			rbDate.Checked = false;
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;

			txtPayerCode.Text = null;
			txtMasterAcc.Text = null;
			ClearPayerType();

			rbPickUp.Checked = true;
			rbDelivery.Checked = false;
//			DbComboPathCode.Text="";
//			DbComboPathCode.Value="";
			ClearServiceType();

			txtZipCode.Text = null;
			txtStateCode.Text = null;

			txtConsignmentNo.Text = null;
			txtRefNo.Text = null;
			txtBookingNo.Text = null;
			Drp_BookingType.Enabled = true;

			txtStateCode.Text = null;
			txtExceptionCode.Text = null;
			rbInvoiceYes.Checked = false;
			rbInvoiceNo.Checked = false;
			rbInvoiceIgnore.Checked = true;
			rbCheckAllYes.Checked = false;
			rbCheckAllNo.Checked = false;
			rbCheckAllIgnore.Checked = true;
			rbLastInHistory.Checked = true;
			rbInHistory.Checked = false;
			rbMissing.Checked = false;
			
			m_dvMonths = CreateMonths(true);
			BindMonths(m_dvMonths);
			lblErrorMessage.Text = "";	    

		}

		private void SetControlFromEnterpriseConfigurations()
		{
			QueryShipmentTrackingConfigurations conf = TIESDAL.EnterpriseConfigMgrDAL.GetQueryShipmentTrackingConfigurations(m_strAppID,m_strEnterpriseID);
			
			// Master Account
			if(conf.CreditControlInScope)
			{
				txtMasterAcc.Enabled= true;
				txtMasterAcc.Text= null;
			}
			else
			{
				txtMasterAcc.Enabled= false;
				txtMasterAcc.Text= null;
				lblMasterAcc.Enabled = false;
			}

			//Invoice Issued
			if(conf.InvoicingInScope)
			{
				rbInvoiceYes.Enabled = true;
				rbInvoiceYes.Checked = false;

				rbInvoiceNo.Enabled = true;
				rbInvoiceNo.Checked = false;

				rbInvoiceIgnore.Enabled = true;
				rbInvoiceIgnore.Checked = true;
			}
			else
			{
				rbInvoiceYes.Enabled = false;
				rbInvoiceYes.Checked = false;

				rbInvoiceNo.Enabled = false;
				rbInvoiceNo.Checked = false;

				rbInvoiceIgnore.Enabled = false;
				rbInvoiceIgnore.Checked = true;

				lblInvoiceIssued.Enabled = false;
			}

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.btnPayerCode.Click += new System.EventHandler(this.btnPayerCode_Click);
			this.btnZipCode.Click += new System.EventHandler(this.btnZipCode_Click);
			this.btnStateCode.Click += new System.EventHandler(this.btnStateCode_Click);
			this.btnStatusCode.Click += new System.EventHandler(this.btnStatusCode_Click);
			this.btnExceptionCode.Click += new System.EventHandler(this.btnExceptionCode_Click);
			this.ID = "ShipmentTrackingQuery";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			//			String strDelType = "L";			
			String strWhereClause=null;
			//			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			//			{
			//				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
			//				{
			//					strDelType = args.ServerState["strDeliveryType"].ToString();
			//					if(strDelType == "S") 
			//					{
			//						strWhereClause=" and Delivery_Type in ('"+Utility.ReplaceSingleQuote(strDelType)+"','W') ";
			//						strWhereClause=strWhereClause+"and path_code in ( ";
			//						strWhereClause=strWhereClause+"select distinct delivery_route ";
			//						strWhereClause=strWhereClause+"from Zipcode ";
			//						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
			//						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
			//					}
			//					else
			//						strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"'";
			//				}				
			//			}
			
			string strDelType_S = "S";
			string strDelType_W = "W";
			strWhereClause=" and Delivery_Type in ('"+Utility.ReplaceSingleQuote(strDelType_S)+"', '"+Utility.ReplaceSingleQuote(strDelType_W)+"')";
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}

		private string validatePeriod()
		{			
			if (rbPeriod.Checked && txtPeriod.Text != "")
			{
				DateTime rEndDate;
				DateTime rStartDate = DateTime.ParseExact(txtPeriod.Text ,"dd/MM/yyyy",null);		
				if(txtTo.Text != "")
				{
					rEndDate = DateTime.ParseExact(txtTo.Text+" 23:59" ,"dd/MM/yyyy HH:mm",null);
				} 
				else
				{
					DateTime time = DateTime.Now; 
					string format = "dd/MM/yyyy"; 
					rEndDate = DateTime.ParseExact(time.ToString(format)+" 23:59" ,"dd/MM/yyyy HH:mm",null);
				}

				if(m_strUserRole.ToUpper().IndexOf("CSSU") != -1)
				{
					DateTime maxDate = rStartDate.AddYears(1);
					if(rEndDate <= maxDate)
					{
						return "";
					}
					else
					{
						return "E1";
					}
				}
				else
				{
					DateTime maxDate = rStartDate.AddDays(31);
					if(rEndDate <= maxDate)
					{
						return "";
					}
					else
					{
						return "E2";
					}
				}
			}
			else
			{
				return "";
			}
		}

		private bool ValidateYear()
		{
			if (txtYear.Text != "" )
			{
				if (Convert.ToInt32(txtYear.Text) < 2000 || Convert.ToInt32(txtYear.Text) > 2099)
				{
					//lblErrorMessage.Text = "Enter year between 2000 to 2099";
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
					return false;
				}
			}
			return true;
		}

		private bool ValidateDate_Shipment()
		{
			if(txtConsignmentNo.Text.Trim() == "" && txtRefNo.Text.Trim() == "" && txtBookingNo.Text =="")
			{
				if(rbMonth.Checked == true && ValidateMonth() == false)
				{
					return false;
				}
				else if(rbPeriod.Checked == true && txtPeriod.Text == "")
				{
					return false;
				}
				else if(rbDate.Checked == true && txtDate.Text == "" )
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				return true;
			}
		}
		private bool ValidateMonth()
		{
			try
			{
				if(rbMonth.Checked == true)
				{
					if(ddMonth.SelectedValue != "" && txtYear.Text != "" ) 
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return true;
				}
			}
			catch
			{
				return false;
			}
		}
		private bool ValidateFormatDate()
		{
			try
			{
				if(rbPeriod.Checked == true)
				{
					string strPeriod = txtPeriod.Text;
					string strTo = txtTo.Text;

					if(txtPeriod.Text != "")
					{
						
						string[] arrPeriod = strPeriod.Split('/');
						int intPeriod_Date = Convert.ToInt16(arrPeriod[0]) ; 
						int intPeriod_Month = Convert.ToInt16(arrPeriod[1]);

						if(intPeriod_Date < 1)
						{
							return false;
						}
						
						if(intPeriod_Date > MaxDateOfMonth(intPeriod_Month))
						{
							return false;
						}

						if(intPeriod_Month < 1 && intPeriod_Month > 12)
						{
							return false;
						}
					}
					
				
					if(txtTo.Text != "")
					{
						string[] arrTo = strTo.Split('/');					
						int intTo_Date = Convert.ToInt16(arrTo[0]); 
						int intTo_Month = Convert.ToInt16(arrTo[1]);					

						if(intTo_Date < 1)
						{
							return false;
						}

						if(intTo_Date > MaxDateOfMonth(intTo_Month))
						{
							return false;
						}

						if(intTo_Month < 1 && intTo_Month > 12)
						{
							return false;
						}					
					}				

					return true;
				}
				else if(rbDate.Checked == true)
				{
					string strDate = txtDate.Text;

					if(txtDate.Text != "" )
					{
						string[] arrDate = strDate.Split('/');
						int intDate_Date = Convert.ToInt16(arrDate[0]); 
						int intDate_Month = Convert.ToInt16(arrDate[1]);

						if(intDate_Date < 1)
						{
							return false;
						}

						if(intDate_Date > MaxDateOfMonth(intDate_Month))
						{
							return false;
						}

						if(intDate_Month < 1 && intDate_Month > 12)
						{
							return false;
						}
					}			

					return true;
				}	
				else
				{
					return true;
				}
			}
			catch
			{
				return false;
			}
		}
		private bool ValidateNoDateFuture()
		{
			if(rbMonth.Checked == true)
			{
				if(ddMonth.SelectedValue != "" && txtYear.Text != "" )
				{
					if(Convert.ToInt32(txtYear.Text) > DateTime.Now.Year)
					{
						return false;
					}
					else
					{
						if((Convert.ToInt32(txtYear.Text) == DateTime.Now.Year) && (Convert.ToInt32(ddMonth.SelectedValue) > DateTime.Now.Month))
						{
							return false;
						}
					}
				}
			}
			else if(rbPeriod.Checked == true)
			{
				if (txtPeriod.Text != "")
				{
					DateTime rStartDate = DateTime.ParseExact(txtPeriod.Text ,"dd/MM/yyyy",null);
					if(rStartDate > DateTime.Now )
					{
						return false;
					}
				}
			}
			else if(rbDate.Checked == true)
			{
				if (txtDate.Text != "")
				{
					DateTime rDate = DateTime.ParseExact(txtDate.Text ,"dd/MM/yyyy",null);
					if(rDate > DateTime.Now )
					{
						return false;
					}
				}
			}
			else
			{
				return true;
			}
			return true;
		}
		private string ValidateDataPopup()
		{
			bool flgCustID = ShipmentTrackingMgrDAL.IsExistCustomer(m_strAppID,m_strEnterpriseID,txtPayerCode.Text.Trim());
			bool flgZipCode = ShipmentTrackingMgrDAL.IsExistZipCode(m_strAppID,m_strEnterpriseID,txtZipCode.Text.Trim());
			bool flgStateCode = ShipmentTrackingMgrDAL.IsExistState(m_strAppID,m_strEnterpriseID,txtStateCode.Text.Trim());
			bool flgStatusCode = ShipmentTrackingMgrDAL.IsExistStatus(m_strAppID,m_strEnterpriseID,txtStatusCode.Text.Trim(), m_strUserId);
			bool flgExceoptionCode = ShipmentTrackingMgrDAL.IsExistException(m_strAppID,m_strEnterpriseID,txtExceptionCode.Text.Trim());

			if(flgCustID == false)
			{
				return "Payer Code is invalid";
			}

			if(flgZipCode == false)
			{
				return "Postal Code is invalid";
			}

			if(flgStateCode == false)
			{
				return "State Code is invalid";
			}

			if(flgStatusCode == false)
			{
				return "Status Code is invalid";
			}

			if(flgExceoptionCode == false)
			{
				return "Exception Code is invalid";
			}
			return "";
		}
		private int MaxDateOfMonth(int iMonth)
		{
			int amtDate = 0;
			switch (iMonth)
			{
				case 1:
					amtDate = 31; 
					break;
				case 2:
					amtDate = 28;
					break;
				case 3:
					amtDate = 31; 
					break;
				case 4:
					amtDate = 30;
					break;
				case 5:
					amtDate = 31; 
					break;
				case 6:
					amtDate = 30;
					break;
				case 7:
					amtDate = 31; 
					break;
				case 8:
					amtDate = 31;
					break;
				case 9:
					amtDate = 30; 
					break;
				case 10:
					amtDate = 31;
					break;
				case 11:
					amtDate = 30; 
					break;
				case 12:
					amtDate = 31;
					break;
			}
			return amtDate;
		}
		public DataSet GetShipmentQueryData()
		{
			DataSet dsShipment = new DataSet();
			DataTable dtShipment = CreateEmptyDataTable();
			DataRow dr = dtShipment.NewRow(); //16 columns
			
			//Enterprise
			dr[0] = m_strEnterpriseID;
			//UserId
			dr[1] = m_strUserId;
			//Start Date
			dr[2] = GetStartDate();
			//End Date
			dr[3] = GetEndDate();
			//Date Type
			dr[4] = GetDatetype();
			// PayerId
			dr[5] = txtPayerCode.Text;
			// Payer Type
			if(GetPayerType() != ""){dr[6] = GetPayerType();} 
			else {dr[6] = null;}
			// MasterAccount
			if(txtMasterAcc.Text != ""){dr[7] = txtMasterAcc.Text;} 
			else {dr[7] = null;}
			// Route Type
			dr[8] = GetRoutetype();
			// RouteCode
			if(cboRouteCode.SelectedValue != ""){dr[9] = cboRouteCode.SelectedValue;} 
			else {dr[9] = null;}
			// ZipCode
			if(txtZipCode.Text != ""){dr[10] = txtZipCode.Text;} 
			else {dr[10] = null;}
			// StateCode
			if(txtStateCode.Text != ""){dr[11] = txtStateCode.Text;} 
			else {dr[11] = null;}
			// Consignment No
			if(txtConsignmentNo.Text.Trim() != ""){dr[12] = txtConsignmentNo.Text.Trim();} 
			else {dr[12] = null;}
			// booking No
			if(txtBookingNo.Text != ""){dr[13] = txtBookingNo.Text;} 
			else {dr[13] = null;}
			// ref No
			if(txtRefNo.Text.Trim() != ""){dr[14] = txtRefNo.Text.Trim();} 
			else {dr[14] = null;}
			// Booking Type
			if(Drp_BookingType.SelectedValue != "0"){dr[15] = Drp_BookingType.SelectedValue;} 
			else {dr[15] = null;}
			// Status Code
			if(txtStatusCode.Text != ""){dr[16] = txtStatusCode.Text;} 
			else {dr[16] = null;}
			// exceptionCode 
			if(txtExceptionCode.Text != ""){dr[17] = txtExceptionCode.Text;} 
			else {dr[17] = null;}
			// Status type 
			dr[18] = GetStatustype();
			// Invoiced 
			dr[19] = GetInvoiced();
			//Closed
			dr[20] = GetClosed();
			//Service Type
			dr[21] = GetServiceType();

			dtShipment.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipment);
			return dsShipment;
		}

		public DataTable CreateEmptyDataTable()
		{
			DataTable dtShipment = new DataTable();
			dtShipment.Columns.Add(new DataColumn("enterpriseid", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("userid", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("Startdate", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("Enddate", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("DateType", typeof(int)));
			dtShipment.Columns.Add(new DataColumn("PayerID", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("PayerType", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("master_account", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("RouteType", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("RouteCode", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("Recipient_zipcode", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("destination_state_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("consignment_no", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("booking_no", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("ref_no", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("Booking_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("status_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("exception_code",typeof(string)));
			dtShipment.Columns.Add(new DataColumn("status_type",typeof(int)));
			dtShipment.Columns.Add(new DataColumn("invoiced",typeof(int)));
			dtShipment.Columns.Add(new DataColumn("closed",typeof(int)));
			dtShipment.Columns.Add(new DataColumn("service_type",typeof(string)));
			return dtShipment;
		}

		private void SetDbComboServerStates()
		{
			//			String strDeliveryType=null;
			//			if (rbLongRoute.Checked==true)
			//				strDeliveryType="L";
			//			else if (rbShortRoute.Checked==true)
			//				strDeliveryType="S";
			//			else if (rbAirRoute.Checked==true)
			//				strDeliveryType="A";

			//			Hashtable hash = new Hashtable();
			//			hash.Add("strDeliveryType", strDeliveryType);
			//			DbComboPathCode.ServerState = hash;
  			//DbComboPathCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}

		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}

		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			ddMonth.Enabled = false;
			ddMonth.SelectedValue = "";
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}

		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			ddMonth.Enabled = false;
			ddMonth.SelectedValue = "";
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = true;
		}

		private void btnPayerCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "CustomerPopup.aspx?FORMID="+"ShipmentTrackingQuery_New"+"&CUSTID_TEXT="+txtPayerCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnZipCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "ZipCodePopup.aspx?FORMID=ShipmentTrackingQuery_New"+"&ZIPCODE_CID="+txtZipCode.ClientID+"&ZIPCODE="+txtZipCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openPostalWindow.js",paramList); //Thosapol.y  Modify (26/06/2013)
			//String sScript = Utility.GetScript("openParentWindow.js",paramList); //Thosapol.y  Comment (26/06/2013)
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnStateCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "StatePopup.aspx?FORMID=ShipmentTrackingQuery_New"+"&STATECODE="+txtStateCode.ClientID+"&STATECODE_TEXT="+txtStateCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnStatusCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "StatusCodePopup.aspx?FORMID=ShipmentTrackingQuery_New"+"&STATUSCODE="+txtStatusCode.Text+"&EXCEPTCODE="+txtExceptionCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnExceptionCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "ExceptionCodePopup.aspx?FORMID=ShipmentTrackingQuery_New"+"&EXCEPTIONCODE="+txtExceptionCode.Text.Trim().ToString()+"&STATUSCODE="+txtStatusCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
		
		public void LoadBookingTypeList()
		{
			ListItem defItem = new ListItem();
			defItem.Text = "";
			defItem.Value = "0";
			Drp_BookingType.Items.Add(defItem);
			ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"booking_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				Drp_BookingType.Items.Add(lstItem);
			}

			if (Drp_BookingType.Items.Count > 0)
				Drp_BookingType.SelectedIndex = 0;
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			lblErrorMessage.Visible = false;	
			if(ValidateFormatDate() == false)
			{
				lblErrorMessage.Visible = true;	
				lblErrorMessage.Text = "Date format is invalid.";
			}
			else if(validatePeriod() != "")
			{
				if(validatePeriod() == "E1")
				{
					lblErrorMessage.Visible = true;	
					lblErrorMessage.Text = "Date range limit exceeded � allowed period is one year.";
				}
				else if(validatePeriod() == "E2")
				{
					lblErrorMessage.Visible = true;	
					lblErrorMessage.Text = "Date range limit exceeded � allowed period is one month.";
				}
			}
			else if(ValidateYear() == false)
			{
				lblErrorMessage.Visible = true;	
				lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
			}
			else if(ValidateDate_Shipment() == false)
			{
				lblErrorMessage.Visible = true;	
				lblErrorMessage.Text = "Date range is required because you did not enter Consignment No, Customer Ref# or Booking No.";
			}
			else if(ValidateNoDateFuture() == false)
			{
				lblErrorMessage.Visible = true;	
				lblErrorMessage.Text = "Invalid date - must be in the past.";
			}
			else if(ValidateDataPopup() != "" )
			{
				lblErrorMessage.Visible = true;	
				lblErrorMessage.Text = ValidateDataPopup();
			}
			else if(validatePeriod() == "" && ValidateYear() == true)
			{
				DataSet dsShipment = GetShipmentQueryData();

				String strUrl = null;
				SessionDS sessionds = new SessionDS();
				sessionds = ShipmentTrackingMgrDAL.QueryShipmentTracking_New(dsShipment, m_strAppID, m_strEnterpriseID, 0, 50);

				strUrl = "ShipmentTracking.aspx?FORMID=TRUE_CHECK";
				//String strFeatures = "'height=500,width=900,left=30,top=30,location=no,menubar=no,scrollbars=yes,resizable=yes,status=yes,titlebar=no,toolbar=no'";
				Session["SESSION_DS1"] = sessionds;
				Session["QUERY_DS"] = dsShipment;
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindow.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);				
				
			}
		}
		private void BindServiceType()
		{
			DataSet lsService = ShipmentTrackingMgrDAL.GetListServiceType(m_strAppID,m_strEnterpriseID);

			DataTable dt = lsService.Tables[0];

			lsbServiceType.DataTextField	= dt.Columns[0].ColumnName;
			lsbServiceType.DataValueField = dt.Columns[0].ColumnName;
			lsbServiceType.DataSource = dt;
			lsbServiceType.DataBind();

		}

		private void BindRouteCode()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();				
			String strWhereClause=null;			
			string strDelType_S = "S";
			string strDelType_W = "W";
			strWhereClause=" and Delivery_Type in ('"+Utility.ReplaceSingleQuote(strDelType_S)+"', '"+Utility.ReplaceSingleQuote(strDelType_W)+"')";
			DataSet dataset = ShipmentTrackingMgrDAL.GetRouteCode(strAppID,strEnterpriseID);
			
			cboRouteCode.DataSource = dataset;
			cboRouteCode.DataTextField = "ComboText";
			cboRouteCode.DataValueField = "ComboText";
			cboRouteCode.DataBind();

		}

		private string GetStartDate()
		{
			if(rbMonth.Checked == true)
			{
				string tmpMonth = ddMonth.SelectedValue;
				string tmpYear = txtYear.Text;
				if(tmpMonth != "" && tmpYear != "")
				{
					if(tmpMonth.Length == 1){tmpMonth = "0" + tmpMonth;}
					return tmpYear + "-" + tmpMonth;
				}
				else
				{
					return null;
				}
			}
			else if(rbPeriod.Checked == true)
			{
				if(txtPeriod.Text != "" && txtTo.Text != "") 
				{
					return reformatDate(txtPeriod.Text);
				}
				else if(txtPeriod.Text != "" && txtTo.Text == "")
				{
					return reformatDate(txtPeriod.Text);
				}
				else 
				{
					return null;
				}				
			}
			else if(rbDate.Checked == true)
			{
				if(txtDate.Text != "") 
				{
					return reformatDate(txtDate.Text);
				}
				else
				{
					return null;				
				}
			}
			else
			{
				return null;
			}
		}

		private string GetEndDate()
		{
			if(rbMonth.Checked == true)
			{				
				return null;
			}
			else if(rbPeriod.Checked == true)
			{
				if(txtPeriod.Text != "" && txtTo.Text != "") 
				{
					return reformatDate(txtTo.Text);
				}
				else if(txtPeriod.Text != "" && txtTo.Text == "")
				{
					DateTime time = DateTime.Now;              
					string format = "yyyy-MM-dd";   
					return time.ToString(format);
				}
				else 
				{
					return null;
				}				
			}
			else if(rbDate.Checked == true)
			{
				return null;
			}
			else
			{
				return null;
			}
		}

		private int GetDatetype()
		{
			if(rbBookingDate.Checked == true && GetDatetype_Empty() == false)
			{
				return 1;
			}
			else if(rbPickupDate.Checked == true && GetDatetype_Empty() == false)
			{
				return 2;
			}
			else if(rbStatusDate.Checked == true && GetDatetype_Empty() == false)
			{
				return 3;
			}
			else
			{
				return 0;
			}
		}
		private int GetRoutetype()
		{
			if(cboRouteCode.SelectedValue == "")
			{
				return 0;
			}
			else if(rbPickUp.Checked == true)
			{
				return 1;
			}
			else if(rbDelivery.Checked == true)
			{
				return 2;
			}
			else
			{
				return 0;
			}
		}
		private int GetStatustype()
		{
			if(txtStatusCode.Text == "" && txtExceptionCode.Text == "" )
			{
				return 0;
			}
			else
			{
				if(rbLastInHistory.Checked == true)
				{
					return 1;
				}
				else if(rbInHistory.Checked == true)
				{
					return 2;
				}
				else if(rbMissing.Checked == true)
				{
					return 3;
				}
				else
				{
					return 0;
				}
			}
			
		}
		private int GetInvoiced()
		{
			if(rbInvoiceYes.Checked == true)
			{
				return 1;
			}
			else if(rbInvoiceNo.Checked == true)
			{
				return 2;
			}
			else if(rbInvoiceIgnore.Checked == true)
			{
				return 0;
			}
			else
			{
				return 0;
			}
		}
		private int GetClosed()
		{
			if(rbCheckAllYes.Checked == true)
			{
				return 1;
			}
			else if(rbCheckAllNo.Checked == true)
			{
				return 2;
			}
			else if(rbCheckAllIgnore.Checked == true)
			{
				return 0;
			}
			else
			{
				return 0;
			}
		}
		private bool GetDatetype_Empty()
		{
			if(rbMonth.Checked == true && (ddMonth.SelectedValue == "" || txtYear.Text == ""))
			{
				return true;
			}
			else if(rbPeriod.Checked == true && txtPeriod.Text == "")
			{
				return true;
			}
			else if(rbDate.Checked == true && txtDate.Text == "")
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		private string GetPayerType()
		{
			string strData = string.Empty;
//			for(int i=lsbCustType.Items.Count-1;i>=0;i--)
			for(int i=0;i<=lsbCustType.Items.Count-1;i++)
			{
				if (lsbCustType.Items[i].Selected==true)
				{
					if(strData == "")
					{
						strData = lsbCustType.Items[i].Value;
					}
					else
					{
						strData = strData + ',' + lsbCustType.Items[i].Value;
					}					
				}
			}	
			return strData;
		}

		private string GetServiceType()
		{
			string strData = string.Empty;
			//			for(int i=lsbCustType.Items.Count-1;i>=0;i--)
			for(int i=0;i<=lsbServiceType.Items.Count-1;i++)
			{
				if (lsbServiceType.Items[i].Selected==true)
				{
					if(strData == "")
					{
						strData = lsbServiceType.Items[i].Value;
					}
					else
					{
						strData = strData + ',' + lsbServiceType.Items[i].Value;
					}					
				}
			}	
			return strData;
		}

		private string reformatDate(string sDate)
		{
			try
			{
				if(sDate != "")
				{
					string[] arrDate = sDate.Split('/');

					if(arrDate.Length == 3)
					{
						string sDay = arrDate[0];
						string sMonth = arrDate[1];
						string sYear = arrDate[2];
					
						return sYear + "-" + sMonth + "-" + sDay ;
					}
					else
					{
						return "";
					}
				}
				else
				{
					return "";
				}				
			}
			catch
			{
				return "";
			}
		}

		private void ClearPayerType()
		{
			for(int i=0;i<=lsbCustType.Items.Count-1;i++)
			{
				if (lsbCustType.Items[i].Selected==true)
				{
					lsbCustType.Items[i].Selected=false;					
				}
			}	
		}

		private void ClearServiceType()
		{
			for(int i=0;i<=lsbServiceType.Items.Count-1;i++)
			{
				if (lsbServiceType.Items[i].Selected==true)
				{
					lsbServiceType.Items[i].Selected=false;					
				}
			}	
		}


	}
}
