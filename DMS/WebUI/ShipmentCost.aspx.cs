using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Threading;
using com.ties.DAL;
using com.ties.BAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for ShipmentCost.
	/// </summary>
	public class ShipmentCost : BasePage
	{
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Label lblMessage;
		protected System.Web.UI.WebControls.Label lblMCAJob;
		protected System.Web.UI.WebControls.DataGrid dgMCAJob;
		protected System.Web.UI.WebControls.DataGrid dgCostCodes;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentCost;
		protected System.Web.UI.WebControls.Button btnExecQry;

		DataView m_dvMonthOptions = null;
		DataView m_dvJobStatusOptions = null;

		
		SessionDS m_sdsMCAJob = null;
		DataSet m_dsCostCode = null;

		protected System.Web.UI.WebControls.Button btnAllocateMonthlyCost;
		protected System.Web.UI.WebControls.Button btnReAllocateMonthlyCost;
		protected System.Web.UI.WebControls.Panel YearMonthInputPanel;
		protected System.Web.UI.WebControls.Label lblInputYear;
		protected System.Web.UI.WebControls.Label lblSelectMonth;
		protected System.Web.UI.WebControls.Button btnOk;
		protected com.common.util.msTextBox txtAccYear;
		protected System.Web.UI.WebControls.DropDownList ddlAccMonth;
		protected System.Web.UI.WebControls.ValidationSummary shipmentCostValidation;
		protected System.Web.UI.WebControls.RequiredFieldValidator validateYear;
		protected System.Web.UI.WebControls.Label lblAstrik;
		protected System.Web.UI.WebControls.Button btnStopThread;

		//Utility utility = null;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.TextBox TextBox1;
		protected System.Web.UI.WebControls.Button btnYes;
		protected System.Web.UI.WebControls.Button btnNo;
		protected System.Web.UI.WebControls.Label lblPanelMessage;
		MCAJobMgrBAL m_objMCAJobMgrBAL = null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			ShowButtonColumns(true);
			loadErrorMsg();
			if(!Page.IsPostBack)
			{
				ViewState["MCAJobOperation"] = Operation.None;
				ResetScreenForQuery();
			}
			else
			{
				m_sdsMCAJob = (SessionDS)Session["SESSION_DS1"];
				//- m_sdsExceptionCode = (SessionDS)Session["SESSION_DS2"];
								
				lblMessage.Text = "";
				int iMode = (int) ViewState["MCAJobMode"];
				switch(iMode)
				{
					case (int) ScreenMode.Query:
						LoadComboLists(true);
						break;
					default:
						LoadComboLists(false);
						break;
				}
			}
		}

		private void loadErrorMsg()
		{
			if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
			{
				validateYear.ErrorMessage = Utility.GetLanguageText(ResourceType.UserMessage, "ACC_YEAR_REQ", utility.GetUserCulture());
			}
			else
			{
				validateYear.ErrorMessage = "Account Year is required field.";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnAllocateMonthlyCost.Click += new System.EventHandler(this.btnAllocateMonthlyCost_Click);
			this.btnReAllocateMonthlyCost.Click += new System.EventHandler(this.btnReAllocateMonthlyCost_Click);
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.btnNo.Click += new System.EventHandler(this.btnNo_Click);
			this.btnStopThread.Click += new System.EventHandler(this.btnStopThread_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		private void btnQry_Click(object sender, System.EventArgs e)
		{
			ResetScreenForQuery();
		}


		private void ResetScreenForQuery()
		{
			ShowYearMonthInputPanel(false);
			ShowGridPanel(true);
			LoadComboLists(true);
			BindComboLists();
			txtAccYear.Text = "";
			lblPanelMessage.Text = "";

			m_sdsMCAJob = MCAJobMgrDAL.GetEmptyMCAJobDS(1);
			
			ViewState["MCAJobMode"] = ScreenMode.Query;

			Session["SESSION_DS1"] = m_sdsMCAJob;
			dgMCAJob.EditItemIndex = 0;
			dgMCAJob.CurrentPageIndex = 0;
			BindMCAJobGrid();
			btnExecQry.Enabled = true;
			//- btnInsert.Enabled = true;
			lblMessage.Text = "";

			ShowButtonColumns(false);
			
			ResetDetailsGrid();
		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			ShowYearMonthInputPanel(false);
			ShowGridPanel(true);
			LoadComboLists(false);
			FillMCAJobDataRow( dgMCAJob.Items[0],0);
			ViewState["QUERY_DS"] = m_sdsMCAJob.ds;
			dgMCAJob.CurrentPageIndex = 0;
			ShowCurrentMCAJobPage(-1);
			btnExecQry.Enabled = false;
			//- btnInsert.Enabled = true;
			lblMessage.Text = "";
			ViewState["MCAJobMode"] = ScreenMode.ExecuteQuery;
			getPageControls(Page);
		}

		private void btnAllocateMonthlyCost_Click(object sender, System.EventArgs e)
		{
			LoadComboLists(true);
			BindComboLists();
			txtAccYear.Text = "";
			lblPanelMessage.Text = "";
			
			ShowOkCancelButtons(true);

			btnCancel.Enabled = true;

			ShowYearMonthInputPanel(true);
			ShowGridPanel(false);
			ViewState["SHOW_PANEL_FLAG"] = true;
		}

		private void btnReAllocateMonthlyCost_Click(object sender, System.EventArgs e)
		{
		
		}


		private void btnOk_Click(object sender, System.EventArgs e)
		{
			if (ValidateYearNMonthInput() == false)
				return;

			if ((bool)ViewState["SHOW_PANEL_FLAG"] == false)
			{
				RefreshMCAJobGrid();
			}
			else
			{
				btnCancel.Enabled = false;

				
				ArrayList paramValues = null;
				String strMessage = null;


				int iAccountYear = Convert.ToInt32(txtAccYear.Text);
				short iAccountMonth = Convert.ToInt16(ddlAccMonth.SelectedItem.Value);
			

				int iMCARecordCount = MonthlyCostAccountMgrDAL.GetMCARecordCount(utility.GetAppID(),utility.GetEnterpriseID(),iAccountYear,iAccountMonth);

				if(iMCARecordCount <= 0)
				{

					strMessage = Utility.GetLanguageText(ResourceType.UserMessage,"NO_COST_CODES_FOR_YEARMONTH",utility.GetUserCulture());
										
					lblPanelMessage.Text = strMessage;
					
					ViewState["CurrentJobID"] = -1;
					ViewState["SHOW_PANEL_FLAG"] = false;

					return;
				}

				int iShipmentRecordCount = MCAJobMgrDAL.GetShipmentRecordCount(utility.GetAppID(),utility.GetEnterpriseID(),iAccountYear,iAccountMonth);

				if(iShipmentRecordCount <= 0)
				{
					strMessage = Utility.GetLanguageText(ResourceType.UserMessage,"NO_SHIPMENTS_FOR_YEARMONTH",utility.GetUserCulture());
					lblPanelMessage.Text = strMessage;
					
					ViewState["CurrentJobID"] = -1;
					ViewState["SHOW_PANEL_FLAG"] = false;

					return;
				}

				
				//Check for already existing JobID
				decimal iMCARecordJobID = MCAJobMgrDAL.GetCostAllocationTaskJobID(utility.GetAppID(),utility.GetEnterpriseID(),iAccountYear,iAccountMonth);

				if(iMCARecordJobID >= 0)
				{
					ShowOkCancelButtons(false);

					paramValues = new ArrayList();

					paramValues.Add(""+iMCARecordJobID);

					strMessage = Utility.GetLanguageText(ResourceType.UserMessage,"JOB_ALREADY_EXISTS",utility.GetUserCulture(),paramValues);
										
					lblPanelMessage.Text = strMessage;
					
					ViewState["CurrentJobID"] = iMCARecordJobID;

					return;
				}
				



				m_objMCAJobMgrBAL = new MCAJobMgrBAL();
				
				JobStatusInfo jobStatusInfo = m_objMCAJobMgrBAL.AllocateMCAJob(utility.GetAppID(),utility.GetEnterpriseID(),iAccountYear,iAccountMonth, -1L, this.Context);
				ViewState["CurrentJobID"] = jobStatusInfo.iJobID;

				//String strMessage = "The job with jobId : <#> already in progress. The job started at <#> and present remark is <#>.";

				paramValues = new ArrayList();

				paramValues.Add(""+jobStatusInfo.iJobID);
				paramValues.Add(jobStatusInfo.dtStartDateTime.ToString("dd/MM/yyyy HH:mm"));
				paramValues.Add(jobStatusInfo.strRemark);

				strMessage = Utility.GetLanguageText(ResourceType.UserMessage,jobStatusInfo.strJobStatusMessageID,utility.GetUserCulture(),paramValues);
				//Replace the parameter with actual value.
				lblPanelMessage.Text = strMessage;
				ViewState["SHOW_PANEL_FLAG"] = false;
			}
		}

		private void RefreshMCAJobGrid()
		{
			decimal iJobID = Convert.ToDecimal(ViewState["CurrentJobID"]);

			SessionDS sdsQuery = MCAJobMgrDAL.GetEmptyMCAJobDS(1);
			ShowYearMonthInputPanel(false);
			ShowGridPanel(true);
			LoadComboLists(false);
			
			if(iJobID != -1)
			{
				DataRow drEach = sdsQuery.ds.Tables[0].Rows[0];
				drEach["jobid"] = iJobID;
			}

			ViewState["QUERY_DS"] = sdsQuery.ds;
			dgMCAJob.CurrentPageIndex = 0;
			ShowCurrentMCAJobPage(0);
			btnExecQry.Enabled = false;
			lblMessage.Text = "";
			ViewState["MCAJobMode"] = ScreenMode.ExecuteQuery;
			ShowCurrentMCAPage();
		}
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			lblPanelMessage.Text = "";
			ShowYearMonthInputPanel(false);
			ShowGridPanel(true);
		}


		private void ShowYearMonthInputPanel(bool bShow)
		{
			YearMonthInputPanel.Visible = bShow;
		}

		private void ShowGridPanel(bool bShow)
		{
			dgMCAJob.Visible = bShow;
			dgCostCodes.Visible = bShow;
		}

		public void dgMCAJob_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Label lblJobID = (Label)dgMCAJob.SelectedItem.FindControl("lblJobID");
			decimal iJobID = -1;

			if(lblJobID != null && lblJobID.Text.Length > 0)
			{
				iJobID = Convert.ToDecimal(lblJobID.Text);
			}
			ViewState["CurrentJobID"] = iJobID;
			ShowCurrentMCAPage();
		}


		public void dgMCAJob_Delete(object sender, DataGridCommandEventArgs e)
		{
			Label lblJobID = (Label)e.Item.FindControl("lblJobID");

			decimal iJobID = -1;

			if(lblJobID != null && lblJobID.Text.Length > 0)
			{
				iJobID = Convert.ToInt64(lblJobID.Text);
			}

			try
			{
				MCAJobMgrDAL.DeleteMCAJobDS(utility.GetAppID(),utility.GetEnterpriseID(),iJobID);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblMessage.Text = "Error Deleting MCA Job";
					
				}

				return;
			}

			if((int)ViewState["MCAJobMode"] == (int)ScreenMode.Insert)
			{
				m_sdsMCAJob.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsMCAJob.QueryResultMaxSize--;
				dgMCAJob.CurrentPageIndex = 0;

				if((int) ViewState["MCAJobOperation"] == (int)Operation.Insert && dgMCAJob.EditItemIndex > 0)
				{
					m_sdsMCAJob.ds.Tables[0].Rows.RemoveAt(dgMCAJob.EditItemIndex-1);
					m_sdsMCAJob.QueryResultMaxSize--;
				}

				dgMCAJob.EditItemIndex = -1;
				dgMCAJob.SelectedIndex = -1;
				BindMCAJobGrid();
				ResetDetailsGrid();
			}
			else
			{
				ShowCurrentMCAJobPage(-1);
			}

			Logger.LogDebugInfo("ShipmentCost","dgMCAJob_Delete","INF004","Deleted row in ShipmentCost table..");
			ViewState["MCAJobOperation"] = Operation.None;
			//- btnInsert.Enabled = true;
		}

		private void BindMCAJobGrid()
		{
			dgMCAJob.VirtualItemCount = System.Convert.ToInt32(m_sdsMCAJob.QueryResultMaxSize);			
			dgMCAJob.DataSource = m_sdsMCAJob.ds;
			dgMCAJob.DataBind();
			Session["SESSION_DS1"] = m_sdsMCAJob;
		}

		private void AddRowInSCGrid()
		{
			MCAJobMgrDAL.AddNewRowInMCAJobDS(m_sdsMCAJob);
			Session["SESSION_DS1"] = m_sdsMCAJob;
		}

		protected void dgMCAJob_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsMCAJob.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}


			DropDownList ddlStatus = (DropDownList)e.Item.FindControl("ddlStatus");			
			if(ddlStatus != null)
			{
				ddlStatus.DataSource = LoadJobStatusOptions();
				ddlStatus.DataTextField = "Text";
				ddlStatus.DataValueField = "StringValue";
				ddlStatus.DataBind();
				if((drSelected["job_status"]!= null) && (!drSelected["job_status"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{				
					String strJobStatus = (String)drSelected["job_status"];
					ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(strJobStatus));  //.FindByValue("N"));
					ddlStatus.Enabled = false;
					//- Logger.LogDebugInfo("StatusException","dgMCAJob_Bound","INF006","Selecting Shipment Update......"+e.Item.ItemIndex+" Selected Index is : "+ddlStatus.SelectedIndex+"   "+strShipmentUpdate);
				}
			}

			DropDownList ddlMonth = (DropDownList)e.Item.FindControl("ddlMonth");			
			if(ddlMonth != null)
			{
				ddlMonth.DataSource = LoadMonthOptions();
				ddlMonth.DataTextField = "Text";
				ddlMonth.DataValueField = "NumValue";
				ddlMonth.DataBind();
				if((drSelected["account_month"]!= null) && (!drSelected["account_month"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strMonth = "";
					int iMonth = Convert.ToInt32(drSelected["account_month"]);
					if(iMonth < 10)
					{
						strMonth += "0";
					}
					strMonth += iMonth;

					ddlMonth.SelectedIndex = ddlMonth.Items.IndexOf(ddlMonth.Items.FindByValue(strMonth));
					ddlMonth.Enabled = false;
					//- Logger.LogDebugInfo("StatusException","dgMCAJob_Bound","INF008","Selecting Invoiceable......"+e.Item.ItemIndex+" Selected Index is  : "+ddlMonth.SelectedIndex+"   "+strInvoiceable);
				}
			}


		}

		protected void dgMCAJob_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgMCAJob.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentMCAJobPage(-1);
			Logger.LogDebugInfo("StatusException","dgMCAJob_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		private void ShowCurrentMCAJobPage(int iSelectedIndex)
		{
			int iStartIndex = dgMCAJob.CurrentPageIndex * dgMCAJob.PageSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsMCAJob = MCAJobMgrDAL.GetMCAJobDS(utility.GetAppID(),utility.GetEnterpriseID(),iStartIndex,dgMCAJob.PageSize,dsQuery);
			decimal pgCnt = (m_sdsMCAJob.QueryResultMaxSize - 1)/dgMCAJob.PageSize; 
			if(pgCnt < dgMCAJob.CurrentPageIndex)
			{
				dgMCAJob.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgMCAJob.SelectedIndex = iSelectedIndex;
			dgMCAJob.EditItemIndex = -1;
			lblMessage.Text = "";

			BindMCAJobGrid();
			ResetDetailsGrid();
		}

		private void ShowCurrentMCAPage()
		{
			decimal iJobID = System.Convert.ToDecimal(ViewState["CurrentJobID"]);
			if(iJobID == -1)
			{
				return;
			}

			m_dsCostCode = MonthlyCostAccountMgrDAL.GetCostCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),iJobID);
			dgCostCodes.EditItemIndex = -1;
			BindCostCodesGrid();
		}


		private void BindCostCodesGrid()
		{
			dgCostCodes.DataSource = m_dsCostCode;
			dgCostCodes.DataBind();
			Session["SESSION_DS2"] = m_dsCostCode;
		}


		/// <summary>
		/// Other methods..
		/// </summary>
		/// <param name="bNilValue"></param>
		protected ICollection LoadMonthOptions()
		{
			Logger.LogDebugInfo("StatusException","LoadMonthOptions","INF001","Loading MBG Options");
			return m_dvMonthOptions;
		}

		private DataView GetMonthOptions(bool showNilOption) 
		{
			DataTable dtMonthOptions = new DataTable();
 
			dtMonthOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonthOptions.Columns.Add(new DataColumn("NumValue", typeof(string)));

			ArrayList monthOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"months",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtMonthOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtMonthOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode monthSysCode in monthOptionArray)
			{
				DataRow drEach = dtMonthOptions.NewRow();
				drEach[0] = monthSysCode.Text;
				drEach[1] = monthSysCode.StringValue;
				dtMonthOptions.Rows.Add(drEach);
			}

			DataView dvMonthOptions = new DataView(dtMonthOptions);
			return dvMonthOptions;
		}

		protected ICollection LoadJobStatusOptions()
		{
			//Logger.LogDebugInfo("StatusException","LoadCloseStatusOptions","INF001","Loading Close Status Options");
			return m_dvJobStatusOptions;
		}

		private DataView GetJobStatusOptions(bool showNilOption) 
		{
			DataTable dtJobStatusOptions = new DataTable();
 
			dtJobStatusOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtJobStatusOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList jobStatusOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"job_status_type",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtJobStatusOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtJobStatusOptions.Rows.Add(drNilRow);
			}
			if(jobStatusOptionArray != null)
			{
				foreach(SystemCode jobStatusSysCode in jobStatusOptionArray)
				{
					DataRow drEach = dtJobStatusOptions.NewRow();
					drEach[0] = jobStatusSysCode.Text;
					drEach[1] = jobStatusSysCode.StringValue;
					dtJobStatusOptions.Rows.Add(drEach);
				}
			}
			DataView dvJobStatusOptions = new DataView(dtJobStatusOptions);
			return dvJobStatusOptions;
		}

		private void LoadComboLists(bool bNilValue)
		{
			m_dvJobStatusOptions = GetJobStatusOptions(bNilValue);
			m_dvMonthOptions = GetMonthOptions(bNilValue);
		}

		private void ShowButtonColumns(bool bShow)
		{
			dgMCAJob.Columns[0].Visible = bShow;
			dgMCAJob.Columns[1].Visible = bShow;

		}

		private void FillMCAJobDataRow(DataGridItem item, int drIndex)
		{
			DataRow drCurrent = m_sdsMCAJob.ds.Tables[0].Rows[drIndex];

			msTextBox txtJobID = (msTextBox)item.FindControl("txtJobID");
			if(txtJobID != null && txtJobID.Text.Length > 0)
			{
				drCurrent["jobid"] = txtJobID.Text;
			}
			else
			{
				drCurrent["jobid"] = System.DBNull.Value;
			}

			msTextBox txtYear = (msTextBox)item.FindControl("txtYear");
			if(txtYear != null && txtYear.Text.Length > 0)
			{
				drCurrent["account_year"] = txtYear.Text;
			}
			else
			{
				drCurrent["account_year"] = System.DBNull.Value;
			}


			DropDownList ddlMonth = (DropDownList)item.FindControl("ddlMonth");
			if(ddlMonth != null && ddlMonth.SelectedItem.Value.Length > 0)
			{
				drCurrent["account_month"] = ddlMonth.SelectedItem.Value;
			}
			else
			{
				drCurrent["account_month"] = System.DBNull.Value;
			}


			DropDownList ddlStatus = (DropDownList)item.FindControl("ddlStatus");
			if(ddlStatus != null && ddlStatus.SelectedItem.Value.Length > 0)
			{
				drCurrent["job_status"] = ddlStatus.SelectedItem.Value;
			}
			else
			{
				drCurrent["job_status"] = System.DBNull.Value;
			}

			msTextBox txtStartDateTime = (msTextBox)item.FindControl("txtStartDateTime");
			if(txtStartDateTime != null && txtStartDateTime.Text.Length > 0)
			{
				
				drCurrent["start_datetime"] = System.DateTime.ParseExact(txtStartDateTime.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			}
			else
			{
				drCurrent["start_datetime"] = System.DBNull.Value;
			}

			msTextBox txtEndDateTime = (msTextBox)item.FindControl("txtEndDateTime");
			if(txtEndDateTime != null && txtEndDateTime.Text.Length > 0)
			{
				drCurrent["end_datetime"] = System.DateTime.ParseExact(txtEndDateTime.Text.Trim(),"dd/MM/yyyy HH:mm",null);
			}
			else
			{
				drCurrent["end_datetime"] = System.DBNull.Value;
			}

			TextBox txtRemarks = (TextBox)item.FindControl("txtRemarks");
			if(txtRemarks != null && txtRemarks.Text.Length > 0)
			{
				drCurrent["remark"] = txtRemarks.Text;
			}
			else
			{
				drCurrent["remark"] = System.DBNull.Value;
			}
		}

		private void ResetDetailsGrid()
		{
			m_dsCostCode = MonthlyCostAccountMgrDAL.GetEmptyCostCodeDS();
			Session["SESSION_DS2"] = m_dsCostCode;
			BindCostCodesGrid();
		}

		private void BindComboLists()
		{
			ddlAccMonth.DataSource = (ICollection)m_dvMonthOptions;
			ddlAccMonth.DataTextField = "Text";
			ddlAccMonth.DataValueField = "NumValue";
			ddlAccMonth.DataBind();
		}

		private void btnStopThread_Click(object sender, System.EventArgs e)
		{
			Hashtable appThreads = (Hashtable)this.Context.Application["APP_THREADS"];

			if(appThreads != null)
			{
				Thread mcaJobThread  = (Thread)appThreads[Utility.GetJobName(utility.GetAppID(),utility.GetEnterpriseID(),JobType.MCAJob)];

				if(mcaJobThread != null && mcaJobThread.IsAlive)
				{
					String strThreadName = mcaJobThread.Name;
					mcaJobThread.Abort();
				}
			}
		}

		private void btnYes_Click(object sender, System.EventArgs e)
		{
			if(ValidateYearNMonthInput() == false)
				return;


			btnCancel.Enabled = false;

				
			ArrayList paramValues = null;
			String strMessage = null;


			int iAccountYear = Convert.ToInt32(txtAccYear.Text);
			short iAccountMonth = Convert.ToInt16(ddlAccMonth.SelectedItem.Value);
			
			m_objMCAJobMgrBAL = new MCAJobMgrBAL();

			decimal iJobID = Convert.ToDecimal(ViewState["CurrentJobID"]);
				
			JobStatusInfo jobStatusInfo = m_objMCAJobMgrBAL.AllocateMCAJob(utility.GetAppID(),utility.GetEnterpriseID(),iAccountYear,iAccountMonth, iJobID, this.Context);
			ViewState["CurrentJobID"] = jobStatusInfo.iJobID;

			//String strMessage = "The job with jobId : <#> already in progress. The job started at <#> and present remark is <#>.";

			paramValues = new ArrayList();

			paramValues.Add(""+jobStatusInfo.iJobID);
			paramValues.Add(jobStatusInfo.dtStartDateTime.ToString("dd/MM/yyyy HH:mm"));
			paramValues.Add(jobStatusInfo.strRemark);

			strMessage = Utility.GetLanguageText(ResourceType.UserMessage,jobStatusInfo.strJobStatusMessageID,utility.GetUserCulture(),paramValues);
			//Replace the parameter with actual value.
			lblPanelMessage.Text = strMessage;
			ViewState["SHOW_PANEL_FLAG"] = false;

			ShowOkCancelButtons(true);
		}

		private void btnNo_Click(object sender, System.EventArgs e)
		{
			ShowYearMonthInputPanel(false);
			ShowGridPanel(true);
			ShowOkCancelButtons(true);
		}

		private void ShowOkCancelButtons(bool bShow)
		{
			btnCancel.Visible = bShow;
			btnOk.Visible = bShow;
			btnYes.Visible = !(bShow);
			btnNo.Visible = !(bShow);
			
		}

		private bool ValidateYearNMonthInput()
		{
			bool bYearValid = false;
			bool bMonthValid = false;
			lblMessage.Text = "";

			/*if(!utility.GetUserCulture().ToUpper().Equals("EN-US"))
			{
				validateYear.ErrorMessage = Utility.GetLanguageText(ResourceType.UserMessage, "ACC_YEAR_REQ", utility.GetUserCulture());
			}
			else
			{
				validateYear.ErrorMessage = "Account Year is required field.";
			}*/

			validateYear.Validate();

			if(!validateYear.IsValid)
			{
				lblPanelMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ACC_YEAR_REQ", utility.GetUserCulture());
				return false;
			}
			else
			{
				lblPanelMessage.Text = "";
				bYearValid = true;
			}

			String strAccountMonth = ddlAccMonth.SelectedItem.Value;

			if(strAccountMonth == null || (strAccountMonth != null && strAccountMonth.Length <= 0))
			{
				lblPanelMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ACC_MONTH_REQ", utility.GetUserCulture());// "Account Month is required field.";
				return false;
			}
			else
			{
				lblPanelMessage.Text = "";
				bMonthValid = true;
			}
			return (bYearValid && bMonthValid);
		}
	}
}
