using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;
namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for BandIdPopup.
	/// </summary>
	public class BandIdPopup : BasePopupPage
	{
		//Utility utility =null;
		String strappid =null;
		String strenterpriseid =null;
		SessionDS m_sdsBandDisc =null;
		String strFORMID = null;
		String strBandId=null;
		protected System.Web.UI.WebControls.Button btnOk;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.TextBox txtBandId;
		protected System.Web.UI.WebControls.DataGrid dgBandId;
		protected System.Web.UI.WebControls.Label lblBanId;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strappid = utility.GetAppID();
			strenterpriseid = utility.GetEnterpriseID();

			strBandId = Request.Params["BANDID"];
			strFORMID = Request.Params["FORMID"]; 
			if(!Page.IsPostBack )
			{
				m_sdsBandDisc = GetEmptyBandId(0); 
				txtBandId.Text = Request.Params["BANDCODE_TEXT"];
				BindGrid();
			}
			else
			{
				m_sdsBandDisc = (SessionDS)ViewState["BANDID_DS"];
			}
  		}
		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			dgBandId.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentPage();
		}
		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select band_code,percent_discount from   Band_Discount where applicationid='");
			strQry.Append(strappid);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strenterpriseid);
			strQry.Append("'");
			if (txtBandId.Text.ToString() != null && txtBandId.Text.ToString() != "")
			{
				strQry.Append(" and band_code like '%");
				strQry.Append(Utility.ReplaceSingleQuote(txtBandId.Text.ToString()));
				strQry.Append("%'");
			}

			String strSQLQuery = strQry.ToString();
			ViewState["SQL_QUERY"] = strSQLQuery;
			dgBandId.CurrentPageIndex = 0;
			ShowCurrentPage();
		}
		private void btnOk_Click(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		public void dgBandId_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			int iSelIndex = dgBandId.SelectedIndex;
			DataGridItem dgRow = dgBandId.Items[iSelIndex];
			String strBandId = dgRow.Cells[0].Text;
			String strPctDisc = dgRow.Cells[1].Text;
			if(strBandId == "&nbsp;")
			{
				strBandId = "";
			}
			if(strPctDisc == "&nbsp;")
			{
				strPctDisc = "";
			}
			
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.opener."+strFORMID+".txtbandId.value = '"+strBandId.Trim() +"';" ;
			sScript += "  window.opener."+strFORMID+".txtPctDisc.value = '"+strPctDisc.Trim() +"';" ;
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		public static SessionDS GetEmptyBandId(int intRows)
		{
			DataTable dtBand = new DataTable();
			dtBand.Columns.Add(new DataColumn("band_code", typeof(string)));
			dtBand.Columns.Add(new DataColumn("percent_discount", typeof(decimal)));
			for(int i=0; i < intRows; i++)
			{
				DataRow drEach = dtBand.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				dtBand.Rows.Add(drEach);
			}

			DataSet dsBandIdFields = new DataSet();
			dsBandIdFields.Tables.Add(dtBand);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsBandIdFields;
			sessionDS.DataSetRecSize = dsBandIdFields.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return sessionDS;
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.dgBandId.SelectedIndexChanged += new System.EventHandler(this.dgBandId_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void BindGrid()
		{
			dgBandId.VirtualItemCount = System.Convert.ToInt32(m_sdsBandDisc.QueryResultMaxSize);
			dgBandId.DataSource = m_sdsBandDisc.ds;
			dgBandId.DataBind();
			ViewState["BANDID_DS"] = m_sdsBandDisc;
		}

		private void ShowCurrentPage()
		{
			int iStartIndex = dgBandId.CurrentPageIndex * dgBandId.PageSize;
			String sqlQuery = (String)ViewState["SQL_QUERY"];
			m_sdsBandDisc = GetBANDIDDS(iStartIndex,dgBandId.PageSize,sqlQuery);
			decimal pgCnt = (m_sdsBandDisc.QueryResultMaxSize - 1)/dgBandId.PageSize;
			if(pgCnt < dgBandId.CurrentPageIndex)
			{
				dgBandId.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgBandId.SelectedIndex = -1;
			dgBandId.EditItemIndex = -1;

			BindGrid();
			getPageControls(Page);
		}

		private SessionDS GetBANDIDDS(int iCurrent, int iDSRecSize, String strQuery)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strappid,strenterpriseid);
			if(dbCon == null)
			{
				Logger.LogTraceError("BandIdPopup.aspx.cs","GetBANDIDDS","BD001","DbConnection object is null!!");
				return sessionDS;
			}
			sessionDS = dbCon.ExecuteQuery(strQuery,iCurrent,iDSRecSize,"ZoneCode");
			return  sessionDS;
		}

	}
}
