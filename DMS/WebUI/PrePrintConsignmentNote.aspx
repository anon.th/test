<%@ Page language="c#" Codebehind="PrePrintConsignmentNote.aspx.cs" AutoEventWireup="false" Inherits="com.ties.PrePrintConsignmentNote" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PrePrintConsignmentNote</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<FONT face="Tahoma">
				<TABLE id="Table2" style="Z-INDEX: 102; LEFT: 16px; WIDTH: 424px; TOP: 8px; HEIGHT: 56px"
					cellSpacing="1" cellPadding="1" width="424" border="0">
					<TR>
						<TD style="WIDTH: 76px"><asp:button id="Add" runat="server" Height="30px" Text="Query" Width="64px" CssClass="queryButton"></asp:button></TD>
						<TD style="WIDTH: 77px"><asp:button id="Edit" runat="server" Height="28px" Text="Insert" Width="64px" CssClass="queryButton"
								DESIGNTIMEDRAGDROP="1052"></asp:button></TD>
						<TD style="WIDTH: 65px"><asp:button id="Save" runat="server" Height="28px" Text="Save" Width="59px" CssClass="queryButton"></asp:button></TD>
						<TD style="WIDTH: 76px"><asp:button id="Search" runat="server" Height="28px" Text="Search" Width="64px" CssClass="queryButton"></asp:button><FONT face="Tahoma"><asp:panel id="Panel1" style="Z-INDEX: 101; LEFT: 0px; POSITION: absolute; TOP: 0px" runat="server"
									Height="0px" Width="0px" BackColor="#FFE0C0" Visible="False" BorderColor="Gainsboro" BorderStyle="Outset">
									<TABLE class="tableLabel" id="Table5" style="HEIGHT: 0.01pt" cellSpacing="1" cellPadding="1"
										width="1" border="0">
										<TR>
											<TD style="WIDTH: 195px; HEIGHT: 43px">
												<asp:Label id="Label32" runat="server" Width="136px" DESIGNTIMEDRAGDROP="2407">Consignment No.</asp:Label></TD>
											<TD style="WIDTH: 173px; HEIGHT: 43px">
												<asp:TextBox id="txtConsearch" runat="server"></asp:TextBox></TD>
											<TD style="WIDTH: 391px; HEIGHT: 43px">
												<asp:Button id="btsearchcon" runat="server" CssClass="queryButton" Width="72px" Text="Search"
													Height="28px"></asp:Button>
												<asp:Label id="Label34" runat="server" Width="224px"></asp:Label>
												<asp:Button id="btclose" runat="server" CssClass="queryButton" Width="58px" Text="Close" Height="28px"></asp:Button></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 195px" width="195" colSpan="1" rowSpan="1">
												<asp:Label id="Label33" runat="server" Width="136px">Reference No.</asp:Label></TD>
											<TD style="WIDTH: 173px" width="173" colSpan="1" rowSpan="1">
												<asp:TextBox id="txtrefsearch" runat="server"></asp:TextBox></TD>
											<TD style="WIDTH: 391px" width="391" colSpan="1" rowSpan="1"><FONT face="Tahoma"></FONT></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 724px" colSpan="3">
												<asp:datagrid id="dgSearch" runat="server" Width="720px" Height="0px" AutoGenerateColumns="False"
													SelectedItemStyle-CssClass="gridFieldSelected" ItemStyle-Height="20" HeaderStyle-CssClass="gridBlueHeading">
													<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
													<ItemStyle Height="0px" CssClass="gridField"></ItemStyle>
													<HeaderStyle Height="0px" CssClass="gridBlueHeading"></HeaderStyle>
													<Columns>
														<asp:TemplateColumn HeaderText="Select">
															<ItemTemplate>
																<asp:Button runat="server" Text="Select" CommandName="Delete" CausesValidation="false"></asp:Button>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Consignment No">
															<ItemStyle Width="20%"></ItemStyle>
															<ItemTemplate>
																<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Consignment_No") %>'>
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Consignment_No") %>'>
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Payer ID">
															<ItemTemplate>
																<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PayerID") %>'>
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PayerID") %>'>
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Telephone No" HeaderStyle-Width="20%">
															<ItemTemplate>
																<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Telephone") %>'>
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Telephone") %>'>
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="address">
															<ItemStyle Width="50%"></ItemStyle>
															<ItemTemplate>
																<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.address1") %>'>
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.address1") %>'>
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Reference Name">
															<ItemStyle Width="50%"></ItemStyle>
															<ItemTemplate>
																<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.reference_name") %>' ID="Label2">
																</asp:Label>
															</ItemTemplate>
															<EditItemTemplate>
																<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.reference_name") %>' ID="Textbox1">
																</asp:TextBox>
															</EditItemTemplate>
														</asp:TemplateColumn>
													</Columns>
												</asp:datagrid></TD>
										</TR>
										<TR>
											<TD style="WIDTH: 724px" colSpan="3"><FONT face="Tahoma"></FONT></TD>
										</TR>
									</TABLE>
								</asp:panel></FONT></TD>
						<TD style="WIDTH: 81px"><asp:button id="Delete" runat="server" Height="28px" Text="Delete" CssClass="queryButton"></asp:button></TD>
						<TD><asp:button id="Print" runat="server" Height="28px" Text="Print" Width="57px" CssClass="queryButton"></asp:button></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 93px" colSpan="6"><FONT face="Tahoma"><asp:label id="lbMessage" runat="server" Width="416px" ForeColor="Red" Font-Size="X-Small"></asp:label></FONT></TD>
					</TR>
				</TABLE>
				<fieldset style="WIDTH: 935px; HEIGHT: 88px">
					<TABLE class="tableLabel" id="Table1" style="Z-INDEX: 101; LEFT: 16px; TOP: 40px; HEIGHT: 80px"
						cellSpacing="1" cellPadding="1" width="100%" border="0">
						<TR>
							<TD style="HEIGHT: 25px" width="14%" colSpan="1" rowSpan="1"><FONT face="Tahoma"><asp:label id="Label2" runat="server" Width="112px" Font-Bold="True">Today's Date :</asp:label></FONT></TD>
							<TD style="HEIGHT: 25px" width="20%" colSpan="1" rowSpan="1"><asp:label id="lbDate" runat="server" Width="126px" DESIGNTIMEDRAGDROP="96"></asp:label></TD>
							<TD style="HEIGHT: 25px" width="15%" colSpan="1" rowSpan="1"><asp:label id="Label10" runat="server" Width="136px" DESIGNTIMEDRAGDROP="144" Font-Bold="True">User Name :</asp:label></TD>
							<TD style="HEIGHT: 25px" width="45%"><asp:label id="lbUserName" runat="server" DESIGNTIMEDRAGDROP="226"></asp:label></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 138px"><asp:label id="Label3" runat="server" Width="112px" DESIGNTIMEDRAGDROP="67" Font-Bold="True">User ID :</asp:label></TD>
							<TD style="WIDTH: 68px"><asp:label id="lbUserID" runat="server" Width="126px" DESIGNTIMEDRAGDROP="142"></asp:label></TD>
							<TD style="WIDTH: 96px"><asp:label id="Label11" runat="server" Width="136px" DESIGNTIMEDRAGDROP="145" Font-Bold="True">Customer Name :</asp:label></TD>
							<TD style="WIDTH: 142px"><asp:label id="lbCustomerName" runat="server" Width="384px"></asp:label></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 138px"><asp:label id="Label4" runat="server" Width="104px" DESIGNTIMEDRAGDROP="3005" Font-Bold="True">Customer ID :</asp:label></TD>
							<TD style="WIDTH: 68px"><asp:label id="lbCustomerID" runat="server" Width="126px"></asp:label></TD>
							<TD style="WIDTH: 96px"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 142px"><FONT face="Tahoma"></FONT></TD>
						</TR>
					</TABLE>
				</fieldset>
				<TABLE class="tableLabel" id="Table10" style="Z-INDEX: 101; LEFT: 16px; WIDTH: 936px; TOP: 40px; HEIGHT: 28px"
					cellSpacing="1" cellPadding="1" width="100%" border="0">
					<TR>
						<TD style="HEIGHT: 25px" width="15%" colSpan="1" rowSpan="1"><FONT face="Tahoma"><asp:label id="Label9" runat="server" Width="136px" Font-Bold="True">Consignment No. :</asp:label></FONT></TD>
						<TD style="HEIGHT: 25px" width="100%"><asp:textbox id="txtConsignmentNo" runat="server" Height="22px" Width="328px" Enabled="False"></asp:textbox></TD>
					</TR>
				</TABLE>
				<fieldset style="WIDTH: 937px; HEIGHT: 610px">
					<TABLE class="tableLabel" id="Table3" cellSpacing="1" cellPadding="1" width="927" border="0">
						<TR>
							<TD style="HEIGHT: 25px" vAlign="top" width="16%" colSpan="1" rowSpan="1"><asp:label id="Label20" runat="server" Width="128px" DESIGNTIMEDRAGDROP="1097" Font-Bold="True">Contact Person :</asp:label></TD>
							<TD style="WIDTH: 293px; HEIGHT: 25px" width="293" colSpan="1" rowSpan="1"><FONT face="Tahoma"><asp:textbox id="txtContactPerson" runat="server" Height="22px" Width="328px" DESIGNTIMEDRAGDROP="2760"></asp:textbox></FONT></TD>
							<TD style="HEIGHT: 25px" vAlign="top" width="17%"><FONT face="Tahoma"></FONT></TD>
							<TD style="HEIGHT: 25px" width="35%"><FONT face="Tahoma"></FONT></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 161px; HEIGHT: 13px" vAlign="top" width="161"><asp:label id="Label6" runat="server" Width="136px" Font-Bold="True">Customer Ref No. :</asp:label></TD>
							<TD style="WIDTH: 293px; HEIGHT: 13px"><asp:textbox id="txtCustomerref" runat="server" Height="22px" Width="328px"></asp:textbox></TD>
							<TD style="HEIGHT: 13px" vAlign="top" width="15%"></TD>
							<TD style="WIDTH: 142px; HEIGHT: 13px"></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 161px; HEIGHT: 17px" vAlign="top" width="161"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 293px; HEIGHT: 17px"></TD>
							<TD style="HEIGHT: 17px" vAlign="top" width="15%"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 142px; HEIGHT: 17px"></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 161px; HEIGHT: 25px" vAlign="top"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 293px; HEIGHT: 25px"><asp:label id="Label16" runat="server" Width="184px" Font-Bold="True">Company Name :</asp:label><asp:textbox id="txtCompanyName" runat="server" Height="22px" Width="320px"></asp:textbox></TD>
							<TD style="WIDTH: 113px; HEIGHT: 25px" vAlign="bottom"><asp:button id="btsearch" runat="server" Text="Search" CssClass="queryButton"></asp:button></TD>
							<TD style="WIDTH: 142px; HEIGHT: 25px"></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 161px; HEIGHT: 25px" vAlign="top"></TD>
							<TD style="WIDTH: 293px; HEIGHT: 25px"><asp:label id="Label1" runat="server" Width="192px" Font-Bold="True">Telephone :</asp:label><asp:textbox id="txtTelephone" runat="server" Height="22px" Width="184px"></asp:textbox></TD>
							<TD style="WIDTH: 113px; HEIGHT: 25px" vAlign="top"></TD>
							<TD style="WIDTH: 142px; HEIGHT: 25px"></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 161px; HEIGHT: 165px" vAlign="top"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 293px; HEIGHT: 165px"><asp:listbox id="lsCompany" runat="server" Height="203px" Width="328px" DESIGNTIMEDRAGDROP="181"
									AutoPostBack="True"></asp:listbox></TD>
							<TD style="WIDTH: 113px; HEIGHT: 165px" vAlign="top"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 142px; HEIGHT: 165px" vAlign="top"><FONT face="Tahoma"></FONT></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 161px; HEIGHT: 23px"><asp:label id="Label14" runat="server" Width="128px" Font-Bold="True">Customer Zone :</asp:label></TD>
							<TD style="WIDTH: 293px; HEIGHT: 23px"><asp:label id="lbCustZone" runat="server" Width="296px"></asp:label></TD>
							<TD style="WIDTH: 113px; HEIGHT: 23px"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 142px; HEIGHT: 23px"></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 161px; HEIGHT: 23px"><asp:label id="Label21" runat="server" Width="120px" Font-Bold="True">Address :</asp:label></TD>
							<TD style="WIDTH: 293px; HEIGHT: 23px"><asp:textbox id="txtAddress" runat="server" Height="48px" Width="328px" Enabled="False" TextMode="MultiLine"
									MaxLength="255"></asp:textbox></TD>
							<TD style="WIDTH: 113px; HEIGHT: 23px"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 142px; HEIGHT: 23px"></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 164px; HEIGHT: 48px"><asp:label id="Label12" runat="server" Width="128px" Font-Bold="True">Special Instruction :</asp:label></TD>
							<TD style="WIDTH: 293px; HEIGHT: 48px"><asp:textbox id="txtspecial" runat="server" Height="48px" Width="328px" TextMode="MultiLine"
									MaxLength="255"></asp:textbox></TD>
							<TD style="WIDTH: 113px; HEIGHT: 48px"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 142px; HEIGHT: 48px"><FONT face="Tahoma"></FONT></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 164px; HEIGHT: 3px"></TD>
							<TD style="WIDTH: 293px; HEIGHT: 3px"><asp:checkbox id="chkHC" runat="server" Text="HC Return"></asp:checkbox><asp:checkbox id="chkInv" runat="server" Text="Invoice Return"></asp:checkbox></TD>
							<TD style="WIDTH: 113px; HEIGHT: 3px"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 142px; HEIGHT: 3px"></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 164px; HEIGHT: 108px" vAlign="top"><asp:label id="Label5" runat="server" Width="128px" DESIGNTIMEDRAGDROP="229" Font-Bold="True">Service Type :</asp:label></TD>
							<TD style="WIDTH: 293px; HEIGHT: 108px"><asp:listbox id="lsService" runat="server" Height="129px" Width="324px" AutoPostBack="True"></asp:listbox></TD>
							<TD style="WIDTH: 113px; HEIGHT: 108px"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 142px; HEIGHT: 108px"></TD>
						</TR>
						<TR>
						</TR>
						<TR>
						</TR>
						<TR>
						</TR>
						<TR>
						</TR>
						<TR>
						</TR>
					</TABLE>
					<TABLE class="tableLabel" id="Table6" cellSpacing="1" cellPadding="1" width="100%" border="0">
						<TR>
							<TD style="WIDTH: 138px" width="138" colSpan="1" rowSpan="1"><asp:label id="Label15" runat="server" Font-Bold="True">Declare Value :</asp:label></TD>
							<TD width="18%"><asp:textbox id="txtDeclareVal" runat="server" Height="22px" Width="128px">0</asp:textbox></TD>
							<TD width="15%"><asp:label id="Label7" runat="server" Font-Bold="True">COD Amount :</asp:label></TD>
							<TD><asp:textbox id="txtCODAmount" runat="server" Height="22px" Width="128px">0</asp:textbox></TD>
						</TR>
					</TABLE>
				</fieldset>
				<fieldset style="WIDTH: 936px; HEIGHT: 293px">
					<TABLE class="tableLabel" id="Table4" style="Z-INDEX: 104; LEFT: 16px; TOP: 816px; HEIGHT: 138px"
						cellSpacing="1" cellPadding="1" width="100%" border="0">
						<TR>
							<TD style="WIDTH: 51px; HEIGHT: 25px" width="51"><asp:label id="Label28" runat="server" Width="80px" DESIGNTIMEDRAGDROP="23" Font-Bold="True">Box Size :</asp:label></TD>
							<TD style="WIDTH: 97px; HEIGHT: 25px"><asp:dropdownlist id="dlsizebox" runat="server" Height="22px" Width="136px" AutoPostBack="True"></asp:dropdownlist></TD>
							<TD style="WIDTH: 25px; HEIGHT: 25px"><asp:label id="Label17" runat="server" Font-Bold="True">Charge :</asp:label></TD>
							<TD style="WIDTH: 104px; HEIGHT: 25px" colSpan="2"><asp:textbox id="txtCharge" runat="server" Height="22px" Width="96px" DESIGNTIMEDRAGDROP="1068"
									Enabled="False"></asp:textbox></TD>
							<TD style="WIDTH: 142px; HEIGHT: 25px"><asp:button id="btadd" runat="server" Text="Add" Width="96px" CssClass="queryButton"></asp:button></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 51px; HEIGHT: 25px" width="51"><FONT face="Tahoma"></FONT></TD>
							<TD style="WIDTH: 98px; HEIGHT: 25px" colSpan="5"><FONT face="Tahoma"><asp:textbox id="txtBoxDesc" runat="server" Height="22px" Width="136px" Enabled="False"></asp:textbox></FONT></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 51px; HEIGHT: 25px" width="51"><asp:label id="Label24" runat="server" Width="80px" DESIGNTIMEDRAGDROP="67" Font-Bold="True">Max Kilos :</asp:label></TD>
							<TD style="WIDTH: 97px; HEIGHT: 25px"><asp:textbox id="txtMaxKg" runat="server" Height="22px" Width="136px" DESIGNTIMEDRAGDROP="299"
									Enabled="False"></asp:textbox></TD>
							<TD style="WIDTH: 25px; HEIGHT: 25px"><asp:label id="Label22" runat="server" DESIGNTIMEDRAGDROP="281" Font-Bold="True">Kilos :</asp:label></TD>
							<TD style="WIDTH: 66px; HEIGHT: 25px"><asp:textbox id="txtKG" runat="server" Height="22px" Width="96px" DESIGNTIMEDRAGDROP="301"></asp:textbox></TD>
							<TD style="WIDTH: 4px; HEIGHT: 25px"><asp:label id="Label26" runat="server" Font-Bold="True">QTY :</asp:label></TD>
							<TD style="WIDTH: 142px; HEIGHT: 25px"><asp:textbox id="txtQty" runat="server" Height="22px" Width="94px"></asp:textbox></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 51px; HEIGHT: 18px"></TD>
							<TD style="WIDTH: 97px; HEIGHT: 18px"></TD>
							<TD style="WIDTH: 25px; HEIGHT: 18px"></TD>
							<TD style="WIDTH: 66px; HEIGHT: 18px"></TD>
							<TD style="WIDTH: 4px; HEIGHT: 18px"></TD>
							<TD style="WIDTH: 142px; HEIGHT: 18px"><FONT face="Tahoma"></FONT></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 104px" vAlign="top" colSpan="6"><asp:datagrid id="dgAdd" runat="server" Height="0px" Width="728px" HeaderStyle-CssClass="gridBlueHeading"
									ItemStyle-Height="20" SelectedItemStyle-CssClass="gridFieldSelected" AutoGenerateColumns="False">
									<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
									<ItemStyle Height="0px" CssClass="gridField"></ItemStyle>
									<HeaderStyle Height="0px" CssClass="gridBlueHeading"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn HeaderText="Service Code">
											<ItemTemplate>
												<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ServiceCode") %>' ID="Label8" NAME="Label8">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ServiceCode") %>' ID="Textbox1" NAME="Textbox1">
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Box Size">
											<ItemTemplate>
												<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BoxSize") %>' ID="Label19" NAME="Label19">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BoxSize") %>' ID="Textbox2" NAME="Textbox2">
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Description Box">
											<ItemTemplate>
												<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DescriptionBox") %>' ID="Label23" NAME="Label23">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DescriptionBox") %>' ID="Textbox3" NAME="Textbox3">
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Charge">
											<ItemTemplate>
												<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Charge") %>' ID="Label25" NAME="Label25">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Charge") %>' ID="Textbox4" NAME="Textbox4">
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="KG">
											<ItemTemplate>
												<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.KG") %>' ID="Label27" NAME="Label27">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.KG") %>' ID="Textbox5" NAME="Textbox5">
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="QTY">
											<ItemTemplate>
												<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTY") %>' ID="Label29" NAME="Label29">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QTY") %>' ID="Textbox6" NAME="Textbox6">
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Total">
											<ItemTemplate>
												<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Total") %>' ID="Label30" NAME="Label30">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Total") %>' ID="Textbox7" NAME="Textbox7">
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:ButtonColumn Text="Delete" HeaderImageUrl="images/butt-delete.gif" ButtonType="PushButton" HeaderText="Delete"
											CommandName="Delete" ItemStyle-HorizontalAlign="Center"></asp:ButtonColumn>
									</Columns>
								</asp:datagrid></TD>
						</TR>
						<TR>
							<TD style="WIDTH: 104px" vAlign="top" colSpan="6"><FONT face="Tahoma"></FONT></TD>
						</TR>
					</TABLE>
				</fieldset>
				&nbsp; </FONT>
		</form>
	</body>
</HTML>
