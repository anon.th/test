using System;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using TIESDAL;
using TIESClasses;

using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;

using com.common.applicationpages;
using com.ties;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for MaintainIMEI_PhoneNumber.
	/// </summary>
	public class MaintainIMEI_PhoneNumber : BasePage
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblValPhoneIMEI;
		protected System.Web.UI.WebControls.DataGrid dgPhoneIMEI;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.TextBox txtPhoneNoGrid;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
	
		//private SessionDS m_dsPhoneIMEI;
		private String m_strAppID;
		private String m_strEnterpriseID;
		protected System.Web.UI.WebControls.TextBox txtPhoneNo;
		protected System.Web.UI.WebControls.TextBox txtIMEI;
		private String m_strCulture;
		public string deactive_date_warning
		{
			get{ return (ViewState["deactive_date_warning"]==null)? "": (String)ViewState["deactive_date_warning"];}
			set{ViewState["deactive_date_warning"]=value;}
		}
		public string m_strPhoneNo
		{
			get{ return (ViewState["m_strPhoneNo"]==null)? "": (String)ViewState["m_strPhoneNo"];}
			set{ViewState["m_strPhoneNo"]=value;}
		}
		public string m_strRemark
		{
			get{ return (ViewState["m_strRemark"]==null)? "": (String)ViewState["m_strRemark"];}
			set{ViewState["m_strRemark"]=value;}
		}
		public string m_strIMEI
		{
			get{ return (ViewState["m_strIMEI"]==null)? "": (String)ViewState["m_strIMEI"];}
			set{ViewState["m_strIMEI"]=value;}
		}
		public string m_strDeactivated
		{
			get{ return (ViewState["m_strDeactivated"]==null)? "": (String)ViewState["m_strDeactivated"];}
			set{ViewState["m_strDeactivated"]=value;}
		}
		public int DSMode
		{
			get
			{
				if(ViewState["iDSMode"]==null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["iDSMode"];
				}
			}
			set
			{
				ViewState["iDSMode"]=value;
			}
		}

		public int DSOperation
		{
			get
			{
				if(ViewState["iDSOperation"]==null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["iDSOperation"];
				}
			}
			set
			{
				ViewState["iDSOperation"]=value;
			}
		}

		public int Index
		{
			get
			{
				if(ViewState["index"]==null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["index"];
				}
			}
			set
			{
				ViewState["index"]=value;
			}
		}
		
		public SessionDS m_dsPhoneIMEI
		{
			get{ return (ViewState["m_dsPhoneIMEI"]==null)? null: (SessionDS)ViewState["m_dsPhoneIMEI"];}
			set{ViewState["m_dsPhoneIMEI"]=value;}
		}

		public SessionDS m_dsPhoneIMEI2
		{
			get{ return (ViewState["m_dsPhoneIMEI2"]==null)? null: (SessionDS)ViewState["m_dsPhoneIMEI2"];}
			set{ViewState["m_dsPhoneIMEI2"]=value;}
		}
		public int GridSize
		{
			get
			{
				if(ViewState["GridSize"]==null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["GridSize"];
				}
			}
			set
			{
				ViewState["GridSize"]=value;
			}
		}
		

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			if(!Page.IsPostBack)
			{			
				dgPhoneIMEI.EditItemIndex = -1;
				DSMode = (int)ScreenMode.None;
				DSOperation = (int)Operation.None;
				clearscreen();

				com.ties.classes.MaintainPhonesIMEIsConfigurations MaintainPhonesIMEIsConfig = EnterpriseConfigMgrDAL.GetMaintainPhonesIMEIsConfigurations(m_strAppID,m_strEnterpriseID);
				GridSize = MaintainPhonesIMEIsConfig.GridRows;
				dgPhoneIMEI.PageSize = GridSize ;
				deactive_date_warning = Utility.GetLanguageText(ResourceType.UserMessage,"DEACTIVATEED_DATE_INVALID",utility.GetUserCulture());	
			
			}
			else
			{
				
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.dgPhoneIMEI.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.PhoneIMEI_ItemCommand);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		
		protected void dgPhoneIMEI_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgPhoneIMEI.CurrentPageIndex = e.NewPageIndex;
			dgPhoneIMEI.EditItemIndex =-1;
			BindPhoneIMEI();
		}
		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{    lblValPhoneIMEI.Text = "";
			dgPhoneIMEI.EditItemIndex = -1;
			btnExecuteQuery.Enabled = false;
			QueryPhoneIMEI();
			BindPhoneIMEI();
		}

		
		private void PhoneIMEI_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			this.lblValPhoneIMEI.Text="";
			string cmd = e.CommandName;
			int rowIndex = e.Item.ItemIndex;
			if(cmd=="ADD_ITEM")
			{	
				dgPhoneIMEI.EditItemIndex = -1;
				if(!checkEmptyFields(e.Item.ItemIndex,e))
				{
					TextBox txtPhoneNoGrid = (TextBox)e.Item.FindControl("txtFooterPhone");
					TextBox txtIMEIGrid = (TextBox)e.Item.FindControl("txtFooterIMEI");
					msTextBox txtDeactivated = (msTextBox)e.Item.FindControl("txtFooterDeactivated");
					TextBox txtRemarkFooter = (TextBox)e.Item.FindControl("txtRemarkFooter");
			
					string strPhone = txtPhoneNoGrid.Text.Trim();
					string strIMEI = txtIMEIGrid.Text.Trim();
					string strDeactivate = txtDeactivated.Text.Trim();
					string strRemark = txtRemarkFooter.Text.Trim();
					try
					{
						//validate IMEI
						SessionDS s_ds = MaintainIMEIDAL.ValidateIMEI(m_strAppID,m_strEnterpriseID, strIMEI);
						if(s_ds !=null && s_ds.ds.Tables[0].Rows[0]["ErrorCode"] !=null)
						{
							if( Convert.ToInt32( s_ds.ds.Tables[0].Rows[0]["ErrorCode"]) != 0)
							{
								lblValPhoneIMEI.Text = s_ds.ds.Tables[0].Rows[0]["ErrorMessage"].ToString();
								TextBox txtIM = (TextBox)e.Item.FindControl("txtFooterIMEI");
								if(txtIM !=null)
								{
									SetFocus(txtIM.ClientID.ToString());
								}
								return;	
							}
						}

						//check dup
						string strMessage = MaintainIMEIDAL.checkDupKey(m_strAppID,m_strEnterpriseID, strPhone,strIMEI,m_strCulture);
						if(strMessage !="")
						{
							
							String[] p = strMessage.Split(':');
							if(p[0].ToString() =="2")
							{
								TextBox txtPhone = (TextBox)e.Item.FindControl("txtFooterPhone");
								if(txtPhone !=null)
								{
									SetFocus(txtPhone.ClientID.ToString());
								}
								
							}
							else if(p[0].ToString() =="3")
							{
								TextBox txtIM = (TextBox)e.Item.FindControl("txtFooterIMEI");
								if(txtIM !=null)
								{
									SetFocus(txtIM.ClientID.ToString());
								}
							}
							else if (p[0].ToString() =="1")
							{
								TextBox txtPhone = (TextBox)e.Item.FindControl("txtFooterPhone");
								if(txtPhone !=null)
								{
									SetFocus(txtPhone.ClientID.ToString());
								}
							}
							lblValPhoneIMEI.Text = p[1].ToString() ;
							
						}
						else
						{
							bool  idAdd = MaintainIMEIDAL.InsertMobilePhoneNoIMEI(m_strAppID,m_strEnterpriseID,strPhone, strIMEI, strDeactivate, strRemark);
							if(idAdd)
							{
								lblValPhoneIMEI.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());	
								QueryPhoneIMEI(strPhone , strIMEI);
								BindPhoneIMEI();
							}
						}
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
					
						return;				
					}
					catch(Exception err)
					{
						String msg = err.ToString();
						lblValPhoneIMEI.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
					}
				}
				

			}
			else if(cmd=="EDIT_ITEM")
			{
				OnEdit_PhoneIMEI(source, e);
				
			}
			else if(cmd=="DELETE_ITEM")
			{
				OnDelete_PhoneIMEI(source, e);
			}
			else if(cmd=="SAVE_ITEM")
			{
				OnUpdate_PhoneIMEI(source, e);

			}
			else if(cmd=="CANCEL_ITEM")
			{
				OnCancel_PhoneIMEI(source, e);
			}
		}

		protected void OnEdit_PhoneIMEI(object sender, DataGridCommandEventArgs e)
		{		
			lblValPhoneIMEI.Text = "";
			int rowIndex = e.Item.ItemIndex;
			Index = rowIndex;

			if(dgPhoneIMEI.EditItemIndex >0)
			{
				TextBox txtPhone = (TextBox)dgPhoneIMEI.Items[rowIndex].FindControl("txtPhoneNoGrid");				
				if(txtPhone !=null && txtPhone.Text=="")	
				{
					m_strPhoneNo = txtPhone.Text;
					m_dsPhoneIMEI.ds.Tables[0].Rows.RemoveAt(dgPhoneIMEI.EditItemIndex);
				}

				TextBox txtIMEI = (TextBox)dgPhoneIMEI.Items[rowIndex].FindControl("txtIMEIGrid");				
				if(txtIMEI !=null && txtIMEI.Text=="")	
				{
					m_strIMEI = txtIMEI.Text;
					m_dsPhoneIMEI.ds.Tables[0].Rows.RemoveAt(dgPhoneIMEI.EditItemIndex);
				}

				msTextBox txtDeactivated = (msTextBox)dgPhoneIMEI.Items[rowIndex].FindControl("txtDeactivated");				
				if(txtDeactivated !=null && txtDeactivated.Text=="")	
				{
					m_strDeactivated = txtDeactivated.Text;
					m_dsPhoneIMEI.ds.Tables[0].Rows.RemoveAt(dgPhoneIMEI.EditItemIndex);
				}

				TextBox txtRemarkEdit = (TextBox)dgPhoneIMEI.Items[rowIndex].FindControl("txtRemarkEdit");				
				if(txtIMEI !=null && txtIMEI.Text=="")	
				{
					m_strIMEI = txtRemarkEdit.Text;
					m_dsPhoneIMEI.ds.Tables[0].Rows.RemoveAt(dgPhoneIMEI.EditItemIndex);
				}
			}

			Label lblPhone = (Label)dgPhoneIMEI.Items[rowIndex].FindControl("lblPhoneNo");	
			if(lblPhone !=null)
			{
				m_strPhoneNo= lblPhone.Text;
			}
			Label lblIMEI = (Label)dgPhoneIMEI.Items[rowIndex].FindControl("lblIMEI");	
			if(lblIMEI !=null)
			{
				m_strIMEI = lblIMEI.Text;
			}
			
			Label lblDate = (Label)dgPhoneIMEI.Items[rowIndex].FindControl("lblDate");	
			if(lblDate !=null)
			{
				m_strDeactivated = lblDate.Text;
			}
			Label lblRemark = (Label)dgPhoneIMEI.Items[rowIndex].FindControl("lblRemark");	
			if(lblRemark !=null)
			{
				m_strRemark = lblRemark.Text;
			}

			dgPhoneIMEI.EditItemIndex = rowIndex;
			BindPhoneIMEI();

			TextBox txtDateActive = (TextBox)dgPhoneIMEI.Items[rowIndex].FindControl("txtDeactivated");				
			if(txtDateActive != null )	
			{
				SetFocus(txtDateActive.ClientID.ToString());
			}
			m_dsPhoneIMEI2 = m_dsPhoneIMEI;
	
		}
		
		
		protected void OnCancel_PhoneIMEI(object sender, DataGridCommandEventArgs e)
		{		
			lblValPhoneIMEI.Text = "";
			dgPhoneIMEI.EditItemIndex = -1;
			//m_dsPhoneIMEI = (SessionDS)Session["SESSION_DS1"];
			int rowIndex = e.Item.ItemIndex;
			if( DSMode == (int)ScreenMode.Insert)
			{
				m_dsPhoneIMEI.ds.Tables[0].Rows.RemoveAt(rowIndex);
			}
			enableDeleteColumn(true);
			BindPhoneIMEI();
	
		}
	
		protected void OnUpdate_PhoneIMEI(object sender, DataGridCommandEventArgs e)
		{		
			int rowIndex = e.Item.ItemIndex;
			if(!checkEmptyFieldsEdit(rowIndex, e))
			{
				updateLatest(rowIndex);
				DSOperation = (int)Operation.Update;
				string key ="";
			
				string phone = m_strPhoneNo;
				string imei = m_strIMEI;	
				key = m_strAppID + m_strEnterpriseID + phone + imei;
			
				DataSet dsPhoneIMEI = m_dsPhoneIMEI.ds.GetChanges();
				SessionDS sdsPhoneIMEI = new SessionDS();
				sdsPhoneIMEI.ds = dsPhoneIMEI;

				string oldDeactivated = m_strDeactivated;
				string oldRemark = m_strRemark;
				string newDeactivate = sdsPhoneIMEI.ds.Tables[0].Rows[0][4].ToString();
				string newRemark = sdsPhoneIMEI.ds.Tables[0].Rows[0][5].ToString();
				if(oldDeactivated.Trim() != newDeactivate.Trim() || oldRemark.Trim() != newRemark.Trim())
				{
					try
					{
						bool isUpdate =	MaintainIMEIDAL.ModifyMobilePhoneNoIMEI(m_strAppID,m_strEnterpriseID, sdsPhoneIMEI,key );
						if(isUpdate)
						{
							lblValPhoneIMEI.Text=Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());	
							QueryPhoneIMEI(phone , imei);
						}
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;

						if(strMsg.IndexOf("duplicate key") != -1  || strMsg.IndexOf("unique") != -1)
						{
							lblValPhoneIMEI.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
						} 
						//return;	
						m_dsPhoneIMEI = m_dsPhoneIMEI2;
					}
					catch(Exception err)
					{
						m_dsPhoneIMEI = m_dsPhoneIMEI2;
						String msg = err.ToString();
						lblValPhoneIMEI.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
					}			
				
				}
				else
				{
					m_dsPhoneIMEI = m_dsPhoneIMEI2;
				}

				DSOperation = (int)Operation.None;
				dgPhoneIMEI.EditItemIndex = -1;
				BindPhoneIMEI();	
			}
		}

		protected void OnDelete_PhoneIMEI(object sender, DataGridCommandEventArgs e)
		{	
			if (dgPhoneIMEI.EditItemIndex == e.Item.ItemIndex)
			{
				return;
			}
			
				int rowNum = e.Item.ItemIndex;				
				Delete(rowNum);								
				//showCurrentPage();
				
				if(dgPhoneIMEI.EditItemIndex < -1)
				{
					m_dsPhoneIMEI.ds.Tables[0].Rows.RemoveAt(dgPhoneIMEI.EditItemIndex);
				}	
				dgPhoneIMEI.EditItemIndex = -1;
				BindPhoneIMEI();
			

		}

		
		private void QueryPhoneIMEI()
		{
			m_dsPhoneIMEI =	MaintainIMEIDAL.GetMobilePhoneNoIMEIDS(m_strAppID,m_strEnterpriseID, txtPhoneNo.Text.Trim(),txtIMEI.Text.Trim());
			if(m_dsPhoneIMEI.ds.Tables[0].Rows.Count <= 0)
			{
				lblValPhoneIMEI.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			}
		}

		private void QueryPhoneIMEI(string strPhone, string strIMEI)
		{
			m_dsPhoneIMEI =	MaintainIMEIDAL.GetMobilePhoneNoIMEIDS(m_strAppID,m_strEnterpriseID, strPhone,strIMEI);
			if(m_dsPhoneIMEI.ds.Tables[0].Rows.Count <= 0)
			{
				lblValPhoneIMEI.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			}
		}

		private void BindPhoneIMEI()
		{
			dgPhoneIMEI.DataSource = m_dsPhoneIMEI.ds;
			int iStartIndex = dgPhoneIMEI.CurrentPageIndex * dgPhoneIMEI.PageSize;
			int pgCnt = (Convert.ToInt32(m_dsPhoneIMEI.ds.Tables[0].Rows.Count - 1))/dgPhoneIMEI.PageSize;
			if(pgCnt < dgPhoneIMEI.CurrentPageIndex)
			{
				dgPhoneIMEI.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}		
			
			dgPhoneIMEI.DataBind();
		}

		private void updateLatest(int rowIndex)
		{
			TextBox txtPhoneNoGrid = (TextBox)dgPhoneIMEI.Items[rowIndex].FindControl("txtPhoneNoGrid");
			TextBox txtIMEIGrid = (TextBox)dgPhoneIMEI.Items[rowIndex].FindControl("txtIMEIGrid");
			msTextBox txtDeactivated = (msTextBox)dgPhoneIMEI.Items[rowIndex].FindControl("txtDeactivated");
			TextBox txtRemarkEdit = (TextBox)dgPhoneIMEI.Items[rowIndex].FindControl("txtRemarkEdit");
			
			DataRow dr = m_dsPhoneIMEI.ds.Tables[0].Rows[rowIndex];
			dr[0] = m_strAppID;
			dr[1] = m_strEnterpriseID;
			dr[2] = txtPhoneNoGrid.Text.Trim();
			dr[3] = txtIMEIGrid.Text.Trim();
			dr[4] = txtDeactivated.Text.Trim();
			dr[5] = txtRemarkEdit.Text.Trim();
		}

		
		private bool checkEmptyFields(int rowIndex,DataGridCommandEventArgs e)
		{			
			TextBox txtPhoneNoGrid = (TextBox)e.Item.FindControl("txtFooterPhone");
			TextBox txtIMEIGrid = (TextBox)e.Item.FindControl("txtFooterIMEI");

			if(txtPhoneNoGrid !=null && txtIMEIGrid!=null )
			{

				if(txtPhoneNoGrid.Text !="" &&  txtIMEIGrid.Text !="")
				{
					return false;
				}
				else if (txtPhoneNoGrid.Text =="")
				{
					lblValPhoneIMEI.Text =Utility.GetLanguageText(ResourceType.UserMessage,"REQUIRED_PHONE",utility.GetUserCulture());
					SetFocus(txtPhoneNoGrid.ClientID.ToString());
				}
				else if( txtIMEIGrid.Text  =="")
				{
					lblValPhoneIMEI.Text =Utility.GetLanguageText(ResourceType.UserMessage,"REQUIRED_IMEI",utility.GetUserCulture());
					SetFocus(txtIMEIGrid.ClientID.ToString());
				}
			}
			return true;
		}

		private bool checkEmptyFieldsEdit(int rowIndex,DataGridCommandEventArgs e)
		{			
			TextBox txtPhoneNoGrid = (TextBox)e.Item.FindControl("txtPhoneNoGrid");
			TextBox txtIMEIGrid = (TextBox)e.Item.FindControl("txtIMEIGrid");

			if(txtPhoneNoGrid !=null && txtIMEIGrid!=null )
			{

				if(txtPhoneNoGrid.Text !="" &&  txtIMEIGrid.Text !="")
				{
					return false;
				}
				else if (txtPhoneNoGrid.Text =="")
				{
					//lblValPhoneIMEI.Text ="Phone Number is required.";
					lblValPhoneIMEI.Text =Utility.GetLanguageText(ResourceType.UserMessage,"REQUIRED_PHONE",utility.GetUserCulture());
					SetFocus(txtPhoneNoGrid.ClientID.ToString());
				}
				else if( txtIMEIGrid.Text  =="")
				{
					//lblValPhoneIMEI.Text ="IMEI is required.";
					lblValPhoneIMEI.Text =Utility.GetLanguageText(ResourceType.UserMessage,"REQUIRED_IMEI",utility.GetUserCulture());
					SetFocus(txtIMEIGrid.ClientID.ToString());
				}
			}
			return true;
		}

		private void Delete(int rowIndex)
		{
			String strPhone = "";
			String strIMEI = "";
			Label lblPhone = (Label)dgPhoneIMEI.Items[rowIndex].FindControl("lblPhoneNo");				
			if(lblPhone !=null )	
			{
				strPhone = lblPhone.Text;
			}

			Label lblIMEI = (Label)dgPhoneIMEI.Items[rowIndex].FindControl("lblIMEI");				
			if(lblIMEI !=null )	
			{
				strIMEI = lblIMEI.Text;
			}

			try
			{
				bool  idDelete = MaintainIMEIDAL.DeleteMobilePhoneNoIMEI(m_strAppID,m_strEnterpriseID,strPhone, strIMEI);
				DataRow[] rows;
				rows = m_dsPhoneIMEI.ds.Tables[0].Select("phoneNo = '"+strPhone+"'"); // UserName is Column Name
				foreach(DataRow r in rows)
				{
					r.Delete();
				}
				m_dsPhoneIMEI.ds.Tables[0].AcceptChanges();
				//m_dsPhoneIMEI = m_dsPhoneIMEI ;
				lblValPhoneIMEI.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY_NO_POINT",utility.GetUserCulture());	
			}
			catch(Exception err)
			{
				String strMsg=err.Message;
				if(strMsg.IndexOf("FK") != -1)
					lblValPhoneIMEI.Text =Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_COMMODITY_TRANS",utility.GetUserCulture());
				else if(strMsg.IndexOf("child record found") != -1)
					lblValPhoneIMEI.Text=Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_COMMODITY_TRANS",utility.GetUserCulture());
				else 
					lblValPhoneIMEI.Text=strMsg;

				return;
			}
			
		}

		private void enableDeleteColumn(bool toEnable)
		{
			dgPhoneIMEI.Columns[1].Visible=toEnable;//Delete column
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			btnExecuteQuery.Enabled =true;
			clearscreen();
		}

		private void clearscreen()
		{
			lblValPhoneIMEI.Text = "";
			txtPhoneNo.Text ="";
			txtIMEI.Text="";
			SetFocus(txtPhoneNo.ClientID.ToString());
			btnExecuteQuery.Enabled=true;
			
			DataTable dt = new DataTable();
			dt.Columns.Add("applicationid");
			dt.Columns.Add("enterpriseid");
			dt.Columns.Add("PhoneNo");
			dt.Columns.Add("IMEI");
			dt.Columns.Add("DeactivatedDate");
			dt.Columns.Add("Remark");
			
			dt.AcceptChanges();
			
			dgPhoneIMEI.DataSource=dt;

			int iStartIndex = dgPhoneIMEI.CurrentPageIndex * dgPhoneIMEI.PageSize;
			int pgCnt = (Convert.ToInt32(dt.Rows.Count - 1))/dgPhoneIMEI.PageSize;
			if(pgCnt < dgPhoneIMEI.CurrentPageIndex)
			{
				dgPhoneIMEI.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}			
			
			dgPhoneIMEI.DataBind();
			
		}
		private void showCurrentPage()
		{
			
//			int iStartIndex = dgPhoneIMEI.CurrentPageIndex * dgPhoneIMEI.PageSize;
//			
//			m_dsPhoneIMEI = SysDataManager1.QueryCommodity(m_sdsQueryCommodity,m_strAppID,m_strEnterpriseID, iStartIndex,dgCommodity.PageSize);
//			int pgCnt = (Convert.ToInt32(m_sdsCommodity.QueryResultMaxSize - 1))/dgCommodity.PageSize;
//			if(pgCnt < dgCommodity.CurrentPageIndex)
//			{
//				dgCommodity.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
//			}
//			Session["SESSION_DS1"] = m_sdsCommodity;
			
		}
		private DataTable newTable()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("applicationid");
			dt.Columns.Add("enterpriseid");
			dt.Columns.Add("PhoneNo");
			dt.Columns.Add("IMEI");
			dt.Columns.Add("DeactivatedDate");

			//DataRow drNew = dt.NewRow();
			return dt;
		}

		private void SetFocus(string idControl)
		{
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("<script language = 'javascript'>");
			strBuilder.Append("document.forms.PhoneIMEI." + idControl + ".focus();");
			strBuilder.Append("document.forms.PhoneIMEI." + idControl + ".select();");

			strBuilder.Append("</script>");
			RegisterStartupScript("Focus",strBuilder.ToString());
		}
	}
}
