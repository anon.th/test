using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.applicationpages;
using com.common.util;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for AccPacExportReconciliation.
	/// </summary>
	public class AccPacExportReconciliation : BasePage
	{
		private String m_strAppID;
		private String m_strEnterpriseID;
		private Utility ut;
		private DataSet m_dsQuery=null;

		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button GenerateReport;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentTracking;
		protected System.Web.UI.WebControls.Button btnGenerateReport;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.Label lbPeriodFrom;
		protected com.common.util.msTextBox txtDateStart;
		protected System.Web.UI.WebControls.Label lbPeriodEnd;
		protected com.common.util.msTextBox txtDateEnd;
		protected System.Web.UI.WebControls.Label lbExportedBy;
		protected System.Web.UI.WebControls.DataGrid dgExportAccPac;
		protected System.Web.UI.WebControls.TextBox txtExportedBy;
		protected System.Web.UI.HtmlControls.HtmlInputButton ExecuteQuery;
		protected System.Web.UI.HtmlControls.HtmlInputButton btnExecute;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			// Put user code to initialize the page here
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			lblErrorMessage.Text = "";
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnGenerateReport.Click += new System.EventHandler(this.btnGenerateReport_Click);
			this.ExecuteQuery.ServerClick += new System.EventHandler(this.ExecuteQuery_ServerClick);
			this.dgExportAccPac.SelectedIndexChanged += new System.EventHandler(this.dgExportAccPac_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		protected void getCheck_onChecked(object obj,EventArgs e)
		{
			bool Check = false;
			foreach(DataGridItem dgItem in this.dgExportAccPac.Items)
			{
				CheckBox getCheck = (CheckBox)dgItem.Cells[0].Controls[1];
	
				if(getCheck.Checked == true)
				{
					btnGenerateReport.Enabled = true;
					Check = true;
				}
			}
			btnGenerateReport.Enabled = Check;
		}

		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private bool ValidateValues()
		{
			bool iCheck=false;

			if(txtDateStart.Text == "" && txtDateEnd.Text == "" && txtExportedBy.Text == "")
			{					
				lblErrorMessage.Text="Please enter the criterias to search.";
				return iCheck=true;
			}

//			if((txtDateStart.Text!="")&&(txtDateEnd.Text==""))
//			{					
//				lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_END_DT",utility.GetUserCulture());
//				return iCheck=true;
//			}
			if((txtDateStart.Text=="")&&(txtDateEnd.Text!=""))
			{
				lblErrorMessage.Text="Please enter the period date range.";
				//lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_START_DT",utility.GetUserCulture());					
				return iCheck=true;			
			}
			else
			{
				return iCheck=false;
			}

			return iCheck;
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			txtDateStart.Text = "";
			txtDateEnd.Text = "";
			txtExportedBy.Text = "";
			//btnExecute.Enabled = true;
			btnExecute.Disabled = false;
			btnGenerateReport.Enabled = false;
			dgExportAccPac.DataSource = null;
			dgExportAccPac.DataBind();
		}

		private void btnExecute_Click(object sender, System.EventArgs e)
		{
			if(!ValidateValues())
			{
				string tmpDateEnd = "";
				if((txtDateStart.Text!="")&&(txtDateEnd.Text==""))
					tmpDateEnd = DateTime.Now.ToString("dd/MM/yyyy");
				else
					tmpDateEnd = txtDateEnd.Text.Trim();
				DataSet ds = new DataSet();
				ds = AccPacExportDAL.GetInvoicesMatching(m_strAppID, m_strEnterpriseID, txtDateStart.Text.Trim(), txtDateEnd.Text.Trim(), txtExportedBy.Text.Trim());
				dgExportAccPac.DataSource = ds.Tables[0];
				dgExportAccPac.DataBind();
				btnGenerateReport.Enabled = false;
				//btnExecute.Enabled = false;
				btnExecute.Disabled = true;
			}
		}

		private void btnGenerateReport_Click(object sender, System.EventArgs e)
		{
			string freightNo = "";
			lblErrorMessage.Text = "";
			
			for(int i=0;i<dgExportAccPac.Items.Count;i++)
			{
				CheckBox chkNo = (CheckBox)this.dgExportAccPac.Items[i].FindControl("chkNo");
				Label lblConsignee = (Label)this.dgExportAccPac.Items[i].FindControl("lbInvoiceNo");

				if(chkNo.Checked)
					freightNo += ", " + lblConsignee.Text;
			}
			freightNo = freightNo.Substring(1, freightNo.Length - 1);

			String strUrl = null;
			strUrl = "ReportViewerDataSet.aspx";
			Session["FORMID"] = "AccPacExport";
			Session["SESSION_DS1"] = m_dsQuery;
			Session["SESSION_Param"] = freightNo;
			OpenWindowpage(strUrl);
			
		}

		private void dgExportAccPac_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void ExecuteQuery_ServerClick(object sender, System.EventArgs e)
		{
			if(!ValidateValues())
			{
				string tmpDateEnd = "";
				if((txtDateStart.Text!="")&&(txtDateEnd.Text==""))
					tmpDateEnd = DateTime.Now.ToString("dd/MM/yyyy");
				else
					tmpDateEnd = txtDateEnd.Text.Trim();
				DataSet ds = new DataSet();
				ds = AccPacExportDAL.GetInvoicesMatching(m_strAppID, m_strEnterpriseID, txtDateStart.Text.Trim(), txtDateEnd.Text.Trim(), txtExportedBy.Text.Trim());
				dgExportAccPac.DataSource = ds.Tables[0];
				dgExportAccPac.DataBind();
				btnGenerateReport.Enabled = false;
				//btnExecute.Enabled = false;
				btnExecute.Disabled = true;
			}
		}
	}
}
