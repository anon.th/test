using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;
namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for ExceptionCodePopup.
	/// </summary>
	public class ExceptionCodePopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.TextBox txtStatusSrch;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		//protected System.Web.UI.WebControls.TextBox txtDescSrch;
		protected System.Web.UI.WebControls.DataGrid m_dgExceptionCode;
		//Utility utility = null;
		private String appID = null;
		private String enterpriseID = null;
		private String strStatusCode = null;
		private String strExceptionCode = null;
		protected System.Web.UI.WebControls.Label Label5;
		private SessionDS m_sdsExceptionCode = null;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.DropDownList ddlMbgSrch;		
		protected System.Web.UI.WebControls.DropDownList ddlCloseSrch;
		protected System.Web.UI.WebControls.DropDownList ddlInvoiceSrch;
		protected System.Web.UI.WebControls.TextBox txtExceptionSrch;
		
		String sStatusTextID=null;
		String sExceptionTextID=null;
		String sExcepDescTextID=null;
		
		//private DataView m_dvStatusTypeOptions;
		private DataView m_dvMBGOptions;
		private DataView m_dvStatusCloseOptions;
		private DataView m_dvInvoiceableOptions;
		private String m_strAppID, m_strEnterpriseID, m_strCulture;
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.WebControls.Label Label7;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			if(!Page.IsPostBack)
			{
				txtExceptionSrch.Text = Request.Params["EXCEPTIONCODE"];
				txtStatusSrch.Text = Request.Params["STATUSCODE"];
				//ClientIDs of Controls

				sStatusTextID=Request.Params["STATUSCODE_CID"];
				sExceptionTextID=Request.Params["EXCEPTIONCODE_CID"];
				sExcepDescTextID=Request.Params["EXDESCRIPTION_CID"];
				ViewState["STATUSCODE_CID"]=sStatusTextID;
				ViewState["EXCEPTIONCODE_CID"]=sExceptionTextID;
				ViewState["EXDESCRIPTION_CID"]=sExcepDescTextID;

				LoadComboLists();
				BindComboLists();				
				RefreshData();
				BindExceptionCode();
				getPageControls(Page);
				ViewState["EXCEPT_DS"] = m_sdsExceptionCode;
			}
			else
			{
				m_sdsExceptionCode = (SessionDS)ViewState["STATUS_DS"];
				sStatusTextID=(String)ViewState["STATUSCODE_CID"];
				sExceptionTextID=(String)ViewState["EXCEPTIONCODE_CID"];
				sExcepDescTextID=(String)ViewState["EXDESCRIPTION_CID"];
			}
		}
		private void RefreshData()
		{
			DbConnection dbConApp = null;
			//strStatusCode = Request.Params["STATUSCODE"];			
			strStatusCode=txtStatusSrch.Text;

			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("ExceptionCodePopup.aspx.cs","RefreshData","ExceptionCodePopupl001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select e.status_code, e.exception_code, e.exception_description, e.mbg, e.status_close, e.invoiceable,s.status_description from Exception_Code e,status_code s where e.applicationid='");
			strQry.Append(appID);
			strQry.Append("' and e.enterpriseid = '");
			strQry.Append(enterpriseID);			
			strQry.Append("' and e.active_st = 'Y'");	//Jeab 22 Mar 2012
			
			if(strStatusCode!="")
			{
				strQry.Append(" and e.status_code like '%");
				strQry.Append(Utility.ReplaceSingleQuote(strStatusCode)+"%'");
			}
			
			if(txtExceptionSrch.Text.ToString()!="" )
			{
				strQry.Append(" and e.exception_code like '%");
				strQry.Append(Utility.ReplaceSingleQuote(txtExceptionSrch.Text.ToString())+"%'");
			}
			
			if(ddlMbgSrch.SelectedItem.Value != "")
			{
				strQry.Append(" and e.mbg like '%");
				strQry.Append(ddlMbgSrch.SelectedItem.Value+"%'");
			}
			if(ddlCloseSrch.SelectedItem.Value != "")
			{
				strQry.Append(" and e.status_close like '%");
				strQry.Append(ddlCloseSrch.SelectedItem.Value+"%'");
			}
			if(ddlInvoiceSrch.SelectedItem.Value != "")
			{
				strQry.Append(" and e.invoiceable like '%");
				strQry.Append(ddlInvoiceSrch.SelectedItem.Value+"%'");
			}

			strQry.Append(" and s.applicationid = e.applicationid");
			strQry.Append(" and s.enterpriseid = e.enterpriseid");
			strQry.Append(" and s.status_code = e.status_code");

			String strQuery = strQry.ToString();

			IDbCommand idbCom = dbConApp.CreateCommand(strQuery, CommandType.Text);
			try
			{
				int iStartIndex = m_dgExceptionCode.CurrentPageIndex * m_dgExceptionCode.PageSize;
				m_sdsExceptionCode = dbConApp.ExecuteQuery(idbCom, iStartIndex,m_dgExceptionCode.PageSize,"Status_Code");
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("ExceptionCodePopup.aspx.cs","RefreshData","Statel002","Error in the query String");
				throw appExpection;
			}
			//ViewState["STATUS_DS"] = m_sdsStatusCode;
		}
		private void BindExceptionCode()
		{
			m_dgExceptionCode.VirtualItemCount = System.Convert.ToInt32(m_sdsExceptionCode.QueryResultMaxSize);
			m_dgExceptionCode.DataSource = m_sdsExceptionCode.ds;
			m_dgExceptionCode.DataBind();
			ViewState["STATUS_DS"] = m_sdsExceptionCode;
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.m_dgExceptionCode.SelectedIndexChanged += new System.EventHandler(this.m_dgExceptionCode_SelectedIndexChanged);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		public void OnPaging_Dispatch(Object sender, DataGridPageChangedEventArgs e)
		{
			m_dgExceptionCode.CurrentPageIndex = e.NewPageIndex;
			RefreshData();
			BindExceptionCode();
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			m_dgExceptionCode.CurrentPageIndex=0;
			RefreshData();
			BindExceptionCode();
			getPageControls(Page);
		}

		private void m_dgExceptionCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			String strFormID = Request.Params["FORMID"];
			int iSelIndex = m_dgExceptionCode.SelectedIndex;
			DataGridItem dgRow = m_dgExceptionCode.Items[iSelIndex];
			strStatusCode = dgRow.Cells[0].Text;
			String strStatusDescription = dgRow.Cells[2].Text;
			if(strStatusDescription == "&nbsp;")
			{
				strStatusDescription = "";
			}
			String strExcpDescription = dgRow.Cells[3].Text;
			if(strExcpDescription == "&nbsp;")
			{
				strExcpDescription = "";
			}
			DataRow dr = m_sdsExceptionCode.ds.Tables[0].Rows[iSelIndex];
			strExceptionCode = dr["exception_code"].ToString();
			windowClose(strFormID, "txtStatus", "txtException",strStatusDescription,strExcpDescription);
			
		}
		private void windowClose(String formID, String strStatusTextBoxID, String strExceptTextBoxID,String strStatusDescription,String strExcpDescription)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			if(formID.Equals("ShipmentUpdate") || (formID.Equals("ShipmentUpdateRoute")))
			{
				sScript += "  window.opener."+formID+".txtStatusCode.value = \""+strStatusCode+"\";";
				sScript += "  window.opener."+formID+".txtExceptioncode.value = \""+strExceptionCode+"\";";
				sScript += "  window.opener."+formID+".txtStatusDesc.value = \""+strStatusDescription+"\";";
				sScript += "  window.opener."+formID+".txtExceptionDesc.value = \""+strExcpDescription+"\";";
			}
			else if(formID.Equals("ShipmentTrackingQuery"))
			{
				sScript += "  window.opener."+formID+".txtStatusCode.value = \""+strStatusCode+"\";";
				sScript += "  window.opener."+formID+".txtExceptionCode.value = \""+strExceptionCode+"\";";
			}
			else if(formID.Equals("ShipmentTrackingQuery_New"))
			{
				sScript += "  window.opener."+formID+".txtStatusCode.value = \""+strStatusCode+"\";";
				sScript += "  window.opener."+formID+".txtExceptionCode.value = \""+strExceptionCode+"\";";
			}
			else if(formID.Equals("ShipmentTrackingQueryRpt"))
			{
				sScript += "  window.opener."+formID+".txtStatusCode.value = \""+strStatusCode+"\";";
				sScript += "  window.opener."+formID+".txtExceptionCode.value = \""+strExceptionCode+"\";";
			}
			else if(formID.Equals("ShipmentExceptionReport"))
			{
				sScript += "  window.opener."+formID+".txtStatus.value = \""+strStatusCode+"\";"; ;
				sScript += "  window.opener."+formID+".txtException.value = \""+strExceptionCode+"\";";
			}
			else if (formID.Equals("CustomerProfile"))
			{
				sScript += "  window.opener."+formID+"."+sExceptionTextID+".value = \""+strExceptionCode+"\";";
				sScript += "  window.opener."+formID+"."+sExcepDescTextID+".value = \""+strExcpDescription+"\";";
			}
			else if (formID.Equals("AgentProfile"))
			{
				sScript += "  window.opener."+formID+"."+sExceptionTextID+".value = \""+strExceptionCode+"\";";
				sScript += "  window.opener."+formID+"."+sExcepDescTextID+".value = \""+strExcpDescription+"\";";
			}
			else if(formID.Equals("DispatchTracking"))
			{
				sScript += "  window.opener."+formID+"."+sStatusTextID+".value = \""+strStatusCode+"\";";
				sScript += "  window.opener."+formID+"."+sExceptionTextID+".value = \""+strExceptionCode+"\";";
			}
			else if(formID.Equals("AgentStatusCost"))
			{
				//sScript += "  window.opener."+formID+"."+sStatusTextID+".value = \""+strStatusCode+"\";";
				sScript += "  window.opener."+formID+"."+sExceptionTextID+".value = \""+strExceptionCode+"\";";
				sScript += "  window.opener."+formID+"."+sExcepDescTextID+".value = \""+strExcpDescription+"\";";
			}
			else 
			{
				sScript += "  window.opener."+formID+".txtStatus.value		=  \""+strStatusCode+"\";";
				sScript += "  window.opener."+formID+".txtException.value	= \""+strExceptionCode+"\";";
			}

			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
			
			
		}
		
		private DataView GetStatusTypeOptions(bool showNilOption)
		{
			DataTable dtStatusTypeOptions = new DataTable();
			
			dtStatusTypeOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtStatusTypeOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList statusTypeOptionArray = Utility.GetCodeValues(m_strAppID,m_strCulture,"status_type",CodeValueType.StringValue);


			if(showNilOption)
			{
				DataRow drNilRow = dtStatusTypeOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtStatusTypeOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode statusTypeSysCode in statusTypeOptionArray)
			{
				DataRow drEach = dtStatusTypeOptions.NewRow();
				drEach[0] = statusTypeSysCode.Text;
				drEach[1] = statusTypeSysCode.StringValue;
				dtStatusTypeOptions.Rows.Add(drEach);
			}

			DataView dvStatusTypeOptions = new DataView(dtStatusTypeOptions);
			return dvStatusTypeOptions;

		}

		private DataView GetStatusCloseOptions(bool showNilOption)
		{
			DataTable dtStatusCloseOptions = new DataTable();
			
			dtStatusCloseOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtStatusCloseOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList statusCloseOptionArray = Utility.GetCodeValues(m_strAppID,m_strCulture,"close_status",CodeValueType.StringValue);


			if(showNilOption)
			{
				DataRow drNilRow = dtStatusCloseOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtStatusCloseOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode statusCloseSysCode in statusCloseOptionArray)
			{
				DataRow drEach = dtStatusCloseOptions.NewRow();
				drEach[0] = statusCloseSysCode.Text;
				drEach[1] = statusCloseSysCode.StringValue;
				dtStatusCloseOptions.Rows.Add(drEach);
			}

			DataView dvStatusCloseOptions = new DataView(dtStatusCloseOptions);
			return dvStatusCloseOptions;

		}


		private DataView GetInvoiceableOptions(bool showNilOption)
		{
			DataTable dtInvoiceableOptions = new DataTable();
			
			dtInvoiceableOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtInvoiceableOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList invoiceableOptionArray = Utility.GetCodeValues(m_strAppID,m_strCulture,"invoiceable",CodeValueType.StringValue);


			if(showNilOption)
			{
				DataRow drNilRow = dtInvoiceableOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtInvoiceableOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode invoiceableSysCode in invoiceableOptionArray)
			{
				DataRow drEach = dtInvoiceableOptions.NewRow();
				drEach[0] = invoiceableSysCode.Text;
				drEach[1] = invoiceableSysCode.StringValue;
				dtInvoiceableOptions.Rows.Add(drEach);
			}

			DataView dvInvoiceableOptions = new DataView(dtInvoiceableOptions);
			return dvInvoiceableOptions;

		}

		private void LoadComboLists()
		{
			//m_dvStatusTypeOptions = GetStatusTypeOptions(true);			
			m_dvStatusCloseOptions = GetStatusCloseOptions(true);
			m_dvInvoiceableOptions = GetInvoiceableOptions(true);
			m_dvMBGOptions = GetMBGOptions(true);
		}

		private void BindComboLists()
		{
			
			ddlCloseSrch.DataSource = (ICollection)m_dvStatusCloseOptions;
			ddlCloseSrch.DataTextField = "Text";
			ddlCloseSrch.DataValueField = "StringValue";
			ddlCloseSrch.DataBind();

			ddlInvoiceSrch.DataSource = (ICollection)m_dvInvoiceableOptions;
			ddlInvoiceSrch.DataTextField = "Text";
			ddlInvoiceSrch.DataValueField = "StringValue";
			ddlInvoiceSrch.DataBind();	

			ddlMbgSrch.DataSource = (ICollection)m_dvMBGOptions;
			ddlMbgSrch.DataTextField = "Text";
			ddlMbgSrch.DataValueField = "StringValue";
			ddlMbgSrch.DataBind();
		}

		private DataView GetMBGOptions(bool showNilOption)
		{
			DataTable dtMBGOptions = new DataTable();
			
			dtMBGOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMBGOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

						ArrayList mbgOptionArray = Utility.GetCodeValues(m_strAppID,m_strCulture,"mbg",CodeValueType.StringValue);


			if(showNilOption)
			{
				DataRow drNilRow = dtMBGOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtMBGOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode mbgSysCode in mbgOptionArray)
			{
				DataRow drEach = dtMBGOptions.NewRow();
				drEach[0] = mbgSysCode.Text;
				drEach[1] = mbgSysCode.StringValue;
				dtMBGOptions.Rows.Add(drEach);
			}

			DataView dvMBGOptions = new DataView(dtMBGOptions);
			return dvMBGOptions;

		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

	}

}
