<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<%@ Page language="c#" Codebehind="StatusCodePopup1.aspx.cs" AutoEventWireup="false" Inherits="com.ties.StatusCodePopup1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>StatusCodePopup1</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="StatusCodePopup1" method="post" runat="server">
			<asp:datagrid id="m_dgStatusCode" style="Z-INDEX: 101; LEFT: 9px; POSITION: absolute; TOP: 208px" runat="server" Width="700px" AutoGenerateColumns="False" OnPageIndexChanged="OnPaging_Dispatch" AllowCustomPaging="True" AllowPaging="True" BorderColor="#CCCCCC" BackColor="White" CellPadding="3" BorderWidth="1px" BorderStyle="None" CssClass="gridHeading">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
				<ItemStyle CssClass="popupGridField"></ItemStyle>
				<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:BoundColumn ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%" DataField="status_code" HeaderText="Status Code"></asp:BoundColumn>
					<asp:BoundColumn ItemStyle-HorizontalAlign="Left" ItemStyle-Width="40%" DataField="status_description" HeaderText="Status Description"></asp:BoundColumn>
					<asp:BoundColumn ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%" DataField="status_close" HeaderText="Status Close"></asp:BoundColumn>
					<asp:BoundColumn ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%" DataField="invoiceable" HeaderText="Invoiceable"></asp:BoundColumn>
					<asp:ButtonColumn ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%" Text="Select" CommandName="Select"></asp:ButtonColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Left" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
			</asp:datagrid><asp:label id="lblMainTitle" style="Z-INDEX: 102; LEFT: 15px; POSITION: absolute; TOP: 15px" runat="server" Width="382px" CssClass="mainTitleSize" Height="36px">Status Code</asp:label>
			<TABLE id="Table1" style="Z-INDEX: 103; LEFT: 8px; WIDTH: 501px; POSITION: absolute; TOP: 67px; HEIGHT: 114px" cellSpacing="1" cellPadding="1" width="501" border="0">
				<TR>
					<TD><asp:label id="lblStatusCode" runat="server" CssClass="tableLabel">Status Code</asp:label></TD>
					<TD><cc1:msTextBox id="txtStatusSrch" runat="server" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore" Enabled="True"></cc1:msTextBox></TD>
					<TD><asp:button id="btnSearch" runat="server" Text="Search" CssClass="buttonProp"></asp:button></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblStatusDescription" runat="server" CssClass="tableLabel">Status Description</asp:label></TD>
					<TD><asp:textbox id="txtDescSrch" runat="server"></asp:textbox></TD>
					<TD>
						<asp:button id="btnClose" runat="server" CssClass="queryButton" Width="67" Text="Close"></asp:button></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblStatusClose" runat="server" CssClass="tableLabel">Status Close</asp:label></TD>
					<TD><asp:dropdownlist id="ddlCloseSrch" runat="server"></asp:dropdownlist></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD><asp:label id="lblInvoiceable" runat="server" CssClass="tableLabel">Invoiceable</asp:label></TD>
					<TD><asp:dropdownlist id="ddlInvoiceSrch" runat="server"></asp:dropdownlist></TD>
					<TD></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
