using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;
using Cambro.Web.DbCombo;
using com.common.RBAC;
using com.ties.classes;
using System.Text;
using com.common.DAL;  //Jeab 13 Jul 2011

namespace com.ties
{
	/// <summary>
	/// Summary description for RevenueBySalesIDRptQuery.
	/// </summary>
	public class RevenueBySalesIDRptQuery : BasePage
	{
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.RadioButton rbBookingDate;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.Label lblPayerType;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label lblPayerCode;
		protected System.Web.UI.WebControls.TextBox txtPayerCode;
		protected System.Web.UI.WebControls.Button btnPayerCode;
		protected System.Web.UI.WebControls.Label lblRouteType;
		protected System.Web.UI.WebControls.RadioButton rbLongRoute;
		protected System.Web.UI.WebControls.RadioButton rbShortRoute;
		protected System.Web.UI.WebControls.RadioButton rbAirRoute;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected Cambro.Web.DbCombo.DbCombo DbComboPathCode;
		protected System.Web.UI.WebControls.Label Label2;
		protected Cambro.Web.DbCombo.DbCombo DbComboOriginDC;
		protected System.Web.UI.WebControls.Label Label3;
		protected Cambro.Web.DbCombo.DbCombo DbComboDestinationDC;
		protected System.Web.UI.WebControls.Label lblDestination;
		protected System.Web.UI.WebControls.Label lblZipCode;
		protected com.common.util.msTextBox txtZipCode;
		protected System.Web.UI.WebControls.Button btnZipCode;
		protected System.Web.UI.WebControls.Label lblStateCode;
		protected com.common.util.msTextBox txtStateCode;
		protected System.Web.UI.WebControls.Button btnStateCode;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.HtmlControls.HtmlTable tblPayerType;
		protected System.Web.UI.HtmlControls.HtmlTable tblRouteType;
		protected System.Web.UI.WebControls.ListBox lsbCustType;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected Cambro.Web.DbCombo.DbCombo DbSalesman;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.WebControls.Label lblServiceCode;
		protected Cambro.Web.DbCombo.DbCombo dbCmbServiceCode;
		protected System.Web.UI.WebControls.RadioButton rbPickUpDate;
		protected System.Web.UI.HtmlControls.HtmlTable tblInternal;
		protected System.Web.UI.WebControls.Label Label7;
		protected Cambro.Web.DbCombo.DbCombo DbComboDiscountBand;
		protected System.Web.UI.WebControls.Label Label9;
		protected Cambro.Web.DbCombo.DbCombo DbComboMasterAccount;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.HtmlControls.HtmlTable Table3;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label16;
		protected com.common.util.msTextBox txtRouteZipcode;
		protected com.common.util.msTextBox txtRouteStateCode;
		protected System.Web.UI.WebControls.Button btnRouteZipCode;
		protected System.Web.UI.WebControls.Button btnRouteStateCode;
		protected Cambro.Web.DbCombo.DbCombo dbComboRoutePath;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.HtmlControls.HtmlTable Table4;
		protected System.Web.UI.WebControls.RadioButtonList rbRptType;
		protected System.Web.UI.HtmlControls.HtmlTable tblDestination;

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.btnRouteZipCode.Click += new System.EventHandler(this.btnRouteZipCode_Click);
			this.btnRouteStateCode.Click += new System.EventHandler(this.btnRouteStateCode_Click);
			this.btnPayerCode.Click += new System.EventHandler(this.btnPayerCode_Click);
			this.rbLongRoute.CheckedChanged += new System.EventHandler(this.rbLongRoute_CheckedChanged);
			this.rbShortRoute.CheckedChanged += new System.EventHandler(this.rbShortRoute_CheckedChanged);
			this.rbAirRoute.CheckedChanged += new System.EventHandler(this.rbAirRoute_CheckedChanged);
			this.btnZipCode.Click += new System.EventHandler(this.btnZipCode_Click);
			this.btnStateCode.Click += new System.EventHandler(this.btnStateCode_Click);
			this.ID = "RevenueBySalesIDRptQuery";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		String m_strAppID=null;
		String m_strEnterpriseID=null;
		String m_strCulture=null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboOriginDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboDestinationDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbSalesman.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbServiceCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboDiscountBand.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			DbComboMasterAccount.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbComboRoutePath.RegistrationKey =System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			com.common.classes.User user = RBACManager.GetUser(m_strAppID, m_strEnterpriseID, (String)Session["userID"]);
			ViewState["usertype"] = user.UserType;
			ViewState["PayerID"] = user.PayerID;
			
			tblInternal.Visible = true;

			if(!Page.IsPostBack)
			{
				DefaultScreen();
				ddlmonths();
				LoadCustomerTypeList();
				Session["toRefresh"]=false;
			}

			SetDbComboServerStates();
			SetPickupDbComboServerStates();
		}


		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
//			if(this.DbComboDiscountBand.Text.Trim().Length>0 && this.DbComboMasterAccount.Text.Trim().Length>0)
//			{
//				lblErrorMessage.Text="Please select either Discount Band or Master Account";
//				return;
//			}

			bool icheck =  ValidateValues();
			if(icheck == false)
			{
				String strModuleID = Request.Params["MODID"];
				DataSet dsShipment = GetShipmentQueryData();

				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				
				if (rbRptType.SelectedValue == "Detail")
				{
					Session["FORMID"] = "RevenueBySalesID";
				}
				else
				{
					Session["FORMID"] = "RevenueBySalesIDSummary";
				}
				
				Session["SESSION_DS1"] = dsShipment;
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindow.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
		}


		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}


		#region "Page Functions"

		private DataTable CreateEmptyDataTable()
		{
			DataTable dtShipment = new DataTable();

			#region "Dates"
			dtShipment.Columns.Add(new DataColumn("tran_date", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
			dtShipment.Columns.Add(new DataColumn("end_date", typeof(DateTime)));
			#endregion
			
			#region "Payer Type"
			dtShipment.Columns.Add(new DataColumn("payer_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payer_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("discount_band",typeof(string)));
			dtShipment.Columns.Add(new DataColumn("master_account",typeof(string)));
			dtShipment.Columns.Add(new DataColumn("cust_new",typeof(string)));  //Jeab 13 Jul 2011
			dtShipment.Columns.Add(new DataColumn("cust_new2",typeof(string)));  //Jeab 18 Jul 2011
			dtShipment.Columns.Add(new DataColumn("cust_new3",typeof(string)));  //Jeab 18 Jul 2011
			dtShipment.Columns.Add(new DataColumn("cust_new4",typeof(string)));  //Jeab 21 Jul 2011
			dtShipment.Columns.Add(new DataColumn("cust_new5",typeof(string)));  //Jeab 21 Jul 2011
			dtShipment.Columns.Add(new DataColumn("cust_new6",typeof(string)));  //Jeab 21 Jul 2011
			dtShipment.Columns.Add(new DataColumn("cust_new7",typeof(string)));  //Jeab 21 Jul 2011
			dtShipment.Columns.Add(new DataColumn("cust_new8",typeof(string)));  //Jeab 21 Jul 2011
			#endregion
		
			#region "Route / DC Selection"
			dtShipment.Columns.Add(new DataColumn("route_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("route_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("origin_dc", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("destination_dc", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("delPath_origin_dc", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("delPath_destination_dc", typeof(string)));
			#endregion

			#region "Destination"
			dtShipment.Columns.Add(new DataColumn("zip_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("state_code", typeof(string)));
			#endregion

			#region "Sale"
			dtShipment.Columns.Add(new DataColumn("sale_id", typeof(string)));
			#endregion

			#region "Service Type"
			dtShipment.Columns.Add(new DataColumn("service_type", typeof(string)));
			#endregion

			#region "pickup"
			dtShipment.Columns.Add(new DataColumn("picup_route_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("pickup_zip_code", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("pickup_state_code", typeof(string)));
			#endregion

			return dtShipment;
		}


		private DataSet GetShipmentQueryData()
		{
			DataSet dsShipment = new DataSet();
			DataTable dtShipment = CreateEmptyDataTable();
			DataRow dr = dtShipment.NewRow(); 
			
			#region "Dates"
			
				if (rbBookingDate.Checked) 
				{	// Booking Date Selected
					dr["tran_date"] = "B";
				}
				if (rbPickUpDate.Checked) 
				{	// Estimated Delivery Date Selected
					dr["tran_date"] = "S";
				}
			
				string strMonth =null;
				if (rbMonth.Checked) 
				{	// Month Selected
					strMonth = ddMonth.SelectedItem.Value;
					if (strMonth != "" && txtYear.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact ("01/"+strMonth.ToString()+"/"+txtYear.Text+" 00:00:01", "dd/MM/yyyy HH:mm:ss", null);
						int intLastDay = 0;
						intLastDay = LastDayOfMonth(strMonth, txtYear.Text);
						dr["end_date"] = DateTime.ParseExact (intLastDay.ToString()+"/"+strMonth.ToString()+"/"+txtYear.Text+" 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
					}
				}
				if (rbPeriod.Checked) 
				{	// Period Selected
					if (txtPeriod.Text != ""  && txtTo.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact(txtPeriod.Text+" 00:00:01","dd/MM/yyyy HH:mm:ss",null);
						dr["end_date"] = DateTime.ParseExact(txtTo.Text+" 23:59:59" ,"dd/MM/yyyy HH:mm:ss",null);
					}
				}
				if (rbDate.Checked) 
				{	// Date Selected
					if (txtDate.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact(txtDate.Text+" 00:00:01", "dd/MM/yyyy HH:mm:ss",null);
						dr["end_date"] = DateTime.ParseExact(txtDate.Text+" 23:59:59", "dd/MM/yyyy HH:mm:ss",null);
					}
				}
			
			
			
			#endregion

			#region "Payer Type"

			string strCustPayerType = "";	
				for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
				{
					if(lsbCustType.Items[i].Selected == true)
					{
						strCustPayerType += "\"" + lsbCustType.Items[i].Value + "\",";
					}
				}

				if (strCustPayerType != "") 

					dr["payer_type"] = "(" + strCustPayerType.Substring(0,strCustPayerType.Length - 1) + ")";
				else
					dr["payer_type"] = System.DBNull.Value;
	
				if ((String)ViewState["usertype"]  == "C")

					dr["payer_code"] = (String)ViewState["PayerID"];
				else
					dr["payer_code"] = txtPayerCode.Text.Trim();

			dr["discount_band"] = DbComboDiscountBand.Text;

			dr["Master_account"] = DbComboMasterAccount.Text;

			dr["cust_new"] = GetNewCust(m_strAppID,m_strEnterpriseID,dr["start_date"].ToString(),dr["end_date"].ToString())  ; //Jeab 13 Jul 2011
			dr["cust_new2"] = Session["CustList2"]; //Jeab 18 Jul 2011
			dr["cust_new3"] = Session["CustList3"]; //Jeab 18 Jul 2011
			dr["cust_new4"] = Session["CustList4"]; //Jeab 21 Jul 2011
			dr["cust_new5"] = Session["CustList5"]; //Jeab 21 Jul 2011
			dr["cust_new6"] = Session["CustList6"]; //Jeab 21 Jul 2011
			dr["cust_new7"] = Session["CustList7"]; //Jeab 21 Jul 2011
			dr["cust_new8"] = Session["CustList8"]; //Jeab 21 Jul 2011

			#endregion
			
			#region "Route / DC Selection"
			if (rbLongRoute.Checked) 
			{
				dr["route_type"] = "L";
			}
			if (rbShortRoute.Checked) 
			{
				dr["route_type"] = "S";
			}
			if (rbAirRoute.Checked) 
			{
				dr["route_type"] = "A";
			}

			dr["route_code"] = DbComboPathCode.Value;
			dr["origin_dc"] = DbComboOriginDC.Value;
			dr["destination_dc"] = DbComboDestinationDC.Value;
			
			if (rbLongRoute.Checked || rbAirRoute.Checked)
			{
				com.ties.classes.DeliveryPath delPath = new com.ties.classes.DeliveryPath();
				delPath.Populate(m_strAppID, m_strEnterpriseID, DbComboPathCode.Value);

				dr["delPath_origin_dc"] = delPath.OriginStation;
				dr["delPath_destination_dc"] = delPath.DestinationStation;
			}
			else
			{
				dr["delPath_origin_dc"] = "";
				dr["delPath_destination_dc"] = "";
			}
			#endregion

			#region "Destination"
		
				dr["zip_code"] = txtZipCode.Text.Trim();
				dr["state_code"] = txtStateCode.Text.Trim();
			
			#endregion

			#region "Sale"

				dr["sale_id"] = DbSalesman.Value;
			
			#endregion

			#region "Service Type"
			dr["service_type"] = dbCmbServiceCode.Value;
			#endregion

			#region "pickup"
			dr["picup_route_code"] = dbComboRoutePath.Value;
			dr["pickup_zip_code"] = txtRouteZipcode.Text.ToString().Trim();
			dr["pickup_state_code"] =txtRouteStateCode.Text.ToString().Trim();
			#endregion

			dtShipment.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipment);
			return dsShipment;
		}


		private void DefaultScreen()
		{
			#region "Dates"

			rbBookingDate.Checked = true;
			rbPickUpDate.Checked = false;
			rbMonth.Checked = true;
			rbPeriod.Checked = false;
			rbDate.Checked = false;
			
			ddMonth.Enabled = true;
			ddMonth.SelectedIndex = -1;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;

			#endregion

			#region "Payer Type"

			lsbCustType.SelectedIndex = -1;
			txtPayerCode.Text = null;
			DbComboDiscountBand.Text = "";
			DbComboDiscountBand.Value = "";

			if (lsbCustType.Enabled == false) 
				lsbCustType.Enabled = true;
			
			if (txtPayerCode.Enabled == false)
				txtPayerCode.Enabled = true;

			if (btnPayerCode.Enabled == false)
				btnPayerCode.Enabled = true;

			if (DbComboDiscountBand.Enabled == false)
				DbComboDiscountBand.Enabled = true;

			#endregion

			#region "Route / DC Selection"

			DbComboPathCode.Text="";
			DbComboPathCode.Value="";

			DbComboOriginDC.Text="";
			DbComboOriginDC.Value="";

			DbComboDestinationDC.Text="";
			DbComboDestinationDC.Value="";

			#endregion

			#region "Destination"

			txtZipCode.Text = null;
			txtStateCode.Text = null;

			#endregion
			
			#region "Sale"
			DbSalesman.Text="";
			DbSalesman.Value="";
			#endregion

			#region "Service Type"
			dbCmbServiceCode.Text="";
			dbCmbServiceCode.Value="";
			DbComboMasterAccount.Text="";
			DbComboMasterAccount.Value="";
			#endregion

			#region "pickup"
			dbComboRoutePath.Text="";
			dbComboRoutePath.Value="";
			txtRouteStateCode.Text="";
			txtRouteZipcode.Text="";
		
			#endregion

			lblErrorMessage.Text = "";

			DbComboDiscountBand.Text="";
			DbComboDiscountBand.Value="";

			DbComboMasterAccount.Text="";
			DbComboMasterAccount.Value="";
			
			rbRptType.SelectedIndex=0;

		}


		private bool ValidateValues()
		{
			bool iCheck=false;
	
				if((ddMonth.SelectedIndex==0)&&(txtYear.Text=="")&&(txtPeriod.Text=="")&&(txtTo.Text=="")&&(txtDate.Text==""))
				{				
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVL",utility.GetUserCulture());
					return iCheck=true;
			
				}
				if(rbMonth.Checked == true)
				{
					if((ddMonth.SelectedIndex>0) &&(txtYear.Text==""))
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
						return iCheck=true;
					}
					else if((ddMonth.SelectedIndex==0) &&(txtYear.Text!=""))
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
						return iCheck=true;
					}
					else
					{
						return iCheck=false;
					}
				}
				if(rbPeriod.Checked == true )
				{
					if((txtPeriod.Text!="")&&(txtTo.Text==""))
					{					
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_END_DT",utility.GetUserCulture());
						return iCheck=true;
					}
					else if((txtPeriod.Text=="")&&(txtTo.Text!=""))
					{
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_START_DT",utility.GetUserCulture());					
						return iCheck=true;			
					}
					else
					{
						return iCheck=false;
					}
				}
				if(rbDate.Checked == true )
				{
					if(txtDate.Text=="")
					{					
						lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT",utility.GetUserCulture());	
						return iCheck=true;				
					}
				}			

				return iCheck;		
			
		}

		private int ValidateData()
		{
			if (txtYear.Text != "" )
			{
				if (Convert.ToInt32(txtYear.Text) < 2000 || Convert.ToInt32(txtYear.Text) > 2099)
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
					return -1;
				}
			}
			return 1;
		}
		

		private int LastDayOfMonth(String strMonth, String strYear)
		{
			int intLastDay = 0;
			switch (strMonth)
			{
				case "01":
				case "03":
				case "05":
				case "07":
				case "08":
				case "10":
				case "12":
					intLastDay = 31;
					break;
				case "04":
				case "06":
				case "09":
				case "11":
					intLastDay = 30;
					break;
				case "02":
					//intLastDay = 28;
					//by sittichai 18/02/2008
					DateTime dt = System.DateTime.ParseExact("01/02/"+strYear,"dd/MM/yyyy",null);
					DateTime firstDayOfThisMonth = dt.Subtract(TimeSpan.FromDays(dt.Day - 1));
					DateTime firstDayOfNextMonth = firstDayOfThisMonth.AddMonths(1);
					DateTime lastDayOfThisMonth = firstDayOfNextMonth.Subtract(TimeSpan.FromDays(1)) ;

					intLastDay = Int32.Parse(lastDayOfThisMonth.Day.ToString());
					break;
			}
			return intLastDay;
		}


		private void OpenWindowpage(String strUrl, String strFeatures)
		{
			
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion

		#region "Dates Part : Controls"

		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = true;
		}		

		#endregion

		#region "Payer Type Part : Controls

		private void btnPayerCode_Click(object sender, System.EventArgs e)
		{
			string strCustPayerType = "";
			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
			{
				if(lsbCustType.Items[i].Selected == true)
				{
					strCustPayerType += lsbCustType.Items[i].Value + ",";
				}
			}
			if((strCustPayerType != null) && (strCustPayerType.Trim().Length > 0))
				strCustPayerType = strCustPayerType.Substring(0, strCustPayerType.Length - 1);

			String sUrl = "CustomerPopup.aspx?FORMID="+"RevenueBySalesIDRptQuery"+
				"&CustType="+strCustPayerType.ToString()+
				"&SalesID="+DbSalesman.Value;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion

		#region "Route / DC Selection : Controls"

		private void rbLongRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}


		private void rbShortRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";	
		}


		private void rbAirRoute_CheckedChanged(object sender, System.EventArgs e)
		{
			DbComboPathCode.Text="";
			DbComboPathCode.Value="";
		}


		#endregion

		#region "Destination : Controls"

		private void btnZipCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "ZipCodePopup.aspx?FORMID=RevenueBySalesIDRptQuery"+"&ZIPCODE_CID="+txtZipCode.ClientID+"&ZIPCODE="+txtZipCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		private void btnStateCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "StatePopup.aspx?FORMID=RevenueBySalesIDRptQuery"+"&STATECODE="+txtStateCode.ClientID+"&STATECODE_TEXT="+txtStateCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnRouteZipCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "ZipCodePopup.aspx?FORMID=RevenueBySalesIDRptQuery"+"&ZIPCODE_CID="+txtRouteZipcode.ClientID+"&ZIPCODE="+txtRouteZipcode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

		}

		private void btnRouteStateCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "StatePopup.aspx?FORMID=RevenueBySalesIDRptQuery"+"&STATECODE="+txtRouteStateCode.ClientID+"&STATECODE_TEXT="+txtRouteStateCode.Text.Trim().ToString();
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

		}
		
		#endregion

		#region "Dates Part : DropDownList"

		private void ddlmonths()
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);

			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}

		#endregion

		#region "Payer Type Part : DropDownList"

		public void LoadCustomerTypeList()
		{
			ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"customer_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				lsbCustType.Items.Add(lstItem);
			}
		}

		#endregion

		#region "Route / DC Selection : DropDownList"

		private void SetDbComboServerStates()
		{
			String strDeliveryType=null;
			if (rbLongRoute.Checked==true)
				strDeliveryType="L";
			else if (rbShortRoute.Checked==true)
				strDeliveryType="S";
			else if (rbAirRoute.Checked==true)
				strDeliveryType="A";

			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
			DbComboPathCode.ServerState = hash;
			DbComboPathCode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"'";
					if(strDelType == "S") 
					{
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct delivery_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
				}				
			}
			
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboDistributionCenterSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,args);	
			return dataset;
		}

	

		#endregion

		#region "Sale : DropDownList"

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object SalesmanQuery(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.SalesmanQuery(strAppID,strEnterpriseID,args);	
			return dataset;
		}

		#endregion
		
		#region "Service Type : DropDownList"

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object ServiceCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			String strServiceCode="";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strServiceCode"] != null && args.ServerState["strServiceCode"].ToString().Length > 0)
				{
					strServiceCode = args.ServerState["strServiceCode"].ToString();					
				}
			}	
			DataSet dataset = com.ties.classes.DbComboDAL.ServiceCodeQuery (strAppID,strEnterpriseID,args);	
			return dataset;                
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DiscountBandServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.DiscountBandQuery(strAppID,strEnterpriseID,args);
			return dataset;
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object MasterAccountServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.MasterAccountQuery(strAppID,strEnterpriseID,args);
			return dataset;
		}


		#endregion	

		#region "Pickup Route / DC Selection : DropDownList"

		private void SetPickupDbComboServerStates()
		{
			String strDeliveryType="S";
			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
			dbComboRoutePath.ServerState = hash;
			dbComboRoutePath.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPickupPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"' ";
					if(strDelType == "S") 
					{
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct pickup_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
				}				
			}
			
			DataSet dataset = com.ties.classes.DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPickupDistributionCenterSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,args);	
			return dataset;
		}


		#endregion


		//Jeab 13 Jul 2011
		public  String GetNewCust(String strAppID, String strEnterpriseID, string strDateFrom , String strDateTo)
		{
			DbConnection dbCon= null;
			DataSet dsNewCust =null;
			IDbCommand dbcmd=null;
			String strNewCust = "";
			String strNewCust2 = "";
			String strNewCust3 = "";
			String strNewCust4 = "";
			String strNewCust5 = "";
			String strNewCust6 = "";
			String strNewCust7 = "";
			String strNewCust8 = "";

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("RevenueBySalesIDRptQuery.aspx","GetNewCust.","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry.Append("select  custid from v_Cust_FirstContact ");
			strQry.Append(" where firstContactDT >= '" + strDateFrom + "'  ");
			strQry.Append(" and firstContactDT <= '" + strDateTo + "'  ");
			
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				dsNewCust = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RevenueBySalesIDRptQuery.aspx","GetNewCust","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			
			int CustQty = 0;
			CustQty = dsNewCust.Tables[0].Rows.Count;

			if( CustQty > 0)
			{
				int i = 0;
				int j=25;
				int m =50;
				//Jeab 21 Jul 2011
				int m2 =75;  
				int m3 =100;
				int m4 =125;
				int m5 =150;
				int m6 =175;
				int m7 =200;

//				for(i = 0;i<dsNewCust.Tables[0].Rows.Count;i++)
//				{	
//					DataRow drNewCust = dsNewCust.Tables[0].Rows[i];									
//					
//						strNewCust += "\"" +  drNewCust["custid"].ToString() + "\",";
//							
//				}
//				strNewCust = "(" + strNewCust.Substring(0,strNewCust.Length - 1) + ")";
				if(CustQty <= j)				
					j=CustQty;
			
				for(i = 0;i<j;i++)
				{	
					DataRow drNewCust = dsNewCust.Tables[0].Rows[i];							
					strNewCust += "\"" +  drNewCust["custid"].ToString() + "\",";							
				}
				if (CustQty <= 25)
				{
					Session["CustList"]  =  strNewCust.Substring(0,strNewCust.Length - 1) ;					
				}
				else
				{
					Session["CustList"] =  strNewCust.Substring(0,strNewCust.Length) ;					
					if (CustQty <= m)
						m = CustQty;

					for(i = 26;i<m;i++)
					{	
						DataRow drNewCust2 = dsNewCust.Tables[0].Rows[i];								
						strNewCust2 += "\"" +  drNewCust2["custid"].ToString() + "\",";							
					}
					if (CustQty <=50)
					{
						Session["CustList2"] =  strNewCust2.Substring(0,strNewCust2.Length - 1) ;
					}
					else
					{
						Session["CustList2"] =  strNewCust2.Substring(0,strNewCust2.Length) ;
						if (CustQty <= m2)
							m = CustQty;

						for(i = 51;i<m2;i++)
						{	
							DataRow drNewCust3 = dsNewCust.Tables[0].Rows[i];							
							strNewCust3 += "\"" +  drNewCust3["custid"].ToString() + "\",";							
						}
						if(CustQty <=75)
						{
							Session["CustList3"] =  strNewCust3.Substring(0,strNewCust3.Length - 1) ;
						}
						else
						{
							Session["CustList3"] =  strNewCust3.Substring(0,strNewCust3.Length) ;
							if(CustQty <=m3)
								m3= CustQty;
				
							for(i=76;i<m3;i++)
							{
								DataRow drNewCust4 = dsNewCust.Tables[0].Rows[i];							
								strNewCust4 += "\"" +  drNewCust4["custid"].ToString() + "\",";
							}
							if(CustQty <= 100)  //Jeab 21 Jul 2011
							{
								Session["CustList4"] =  strNewCust4.Substring(0,strNewCust4.Length - 1) ;
							}
							else
							{
								Session["CustList4"] =  strNewCust4.Substring(0,strNewCust4.Length) ;
								if (CustQty <= m4)
									m4 = CustQty;
					
								for(i=101;i<m4;i++)
								{
									DataRow drNewCust5 = dsNewCust.Tables[0].Rows[i];							
									strNewCust5 += "\"" +  drNewCust5["custid"].ToString() + "\",";
								}
								if(CustQty <= 125)
								{
									Session["CustList5"] =  strNewCust5.Substring(0,strNewCust5.Length - 1) ;
								}
								else
								{
									Session["CustList5"] =  strNewCust5.Substring(0,strNewCust5.Length) ;
									if (CustQty <= m5)
										m5 = CustQty;

									for(i=126;i<m5;i++)
									{
										DataRow drNewCust6 = dsNewCust.Tables[0].Rows[i];							
										strNewCust6 += "\"" +  drNewCust6["custid"].ToString() + "\",";
									}
									if (CustQty <=150)
									{
										Session["CustList6"] =  strNewCust6.Substring(0,strNewCust6.Length - 1) ;
									}
									else
									{
										Session["CustList6"] =  strNewCust6.Substring(0,strNewCust6.Length) ;
										if (CustQty <= m6)
											m6 = CustQty;

										for(i=151;i<m6;i++)
										{
											DataRow drNewCust7 = dsNewCust.Tables[0].Rows[i];							
											strNewCust7 += "\"" +  drNewCust7["custid"].ToString() + "\",";
										}
										if (CustQty <= 175)
										{
											Session["CustList7"] =  strNewCust7.Substring(0,strNewCust7.Length - 1) ;
										}
										else
										{
											Session["CustList7"] =  strNewCust7.Substring(0,strNewCust7.Length) ;
											if (CustQty <= m7)
												m7 = CustQty;

											for (i=176;i<m7;i++)
											{
												DataRow drNewCust8 = dsNewCust.Tables[0].Rows[i];							
												strNewCust8 += "\"" +  drNewCust8["custid"].ToString() + "\",";
											}
											Session["CustList8"] =  strNewCust8.Substring(0,strNewCust8.Length) ;
										}
									}
								}
							}

						}
					}
				}
			}

			return strNewCust;
		}
		//Jeab 13 Jul 2011  =========> End

	}
}