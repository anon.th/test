<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="Quick_POD.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.Quick_POD" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<TITLE>Scanning Workbench Emulator</TITLE> 
		<!--
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
 -->
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css">
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/msFormValidation.js"></script>
		<script language="javascript">	
		
		function makeUppercase(objId)
		{
			document.getElementById(objId).value = document.getElementById(objId).value.toUpperCase();
		}
		
		function SetScrollPosition_page()
		{
			window.scroll(0,document.forms[0].ScrollPosition.value);
			var sHostname = window.location.hostname;
			var sPort = window.location.port;
			
			document.getElementById('hidHost').value = sHostname; 
			document.getElementById('hidPort').value = sPort; 
			//alert(document.getElementById('hidHost').value);			
		}
		
		function setValue()
		{
			var s = document.all['inFile'];
			document.all['txtFilePath'].innerText = s.value;
			if(s.value != '') 
				document.all['btnImport'].disabled = false;
			else
				document.all['btnImport'].disabled = true;
		}
	
		function upBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-up.GIF)';
		}
		
		function downBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-down.GIF)';
		}		
		
		function clearValue()
		{
			if(document.all['txtFilePath'].innerText == '') {
				document.location.reload();
			}
		}

		function IsNumeric(sText)
		{
			//alert(sText);
			var ValidChars = "0123456789.";
			var IsNumber=true;
			var Char;
				
			for (i = 0; i < sText.length ; i++) 
				{ 
				Char = sText.charAt(i); 
				if (ValidChars.indexOf(Char) == -1) 
					{
					//alert(Char);
					IsNumber = false;
					break;
					}
				}
			return IsNumber;	
		}
		
		function TxtConNoFocus()
		{
			document.getElementById("txtConNo").value = '';
			document.getElementById("txtConNo").focus();
		}

		function KeyDownHandler()
		{
			//debugger;
			// process only the Enter key
			var key;
			if(window.event)  key = window.event.keyCode;     //IE
			else  key = e.which;     //firefox
			if (key == 13)
			{
				// cancel the default submit
				//event.returnValue=false;
				//event.cancel = true;
				// submit the form by programmatically clicking the specified button
	            
				document.getElementById("btnExecQry").click();
				document.getElementById("txtConNo").focus();
            
				/*
				if(document.getElementById("btnInsert").disabled==false)
				{
					//btn = "btnInsert";
					
					document.getElementById("btnExecQry").click();
					document.getElementById("txtConNo").focus();
				}
				else if(document.getElementById("btnUpdate").disabled==false)
				{
					//btn = "btnUpdate";
					document.getElementById("btnUpdate").click();
					document.getElementById("txtConNo").focus();
				}
				*/          
			}
		}
    
		function  suppressNonEng(e,obj)
		{	//debugger;
			var key;
			if(window.event)  key = window.event.keyCode;     //IE
			else  key = e.which;     //firefox
				
			if(key >128)  return false;
			else  return UpperMask(obj);//return true;*/
		}		
	 
		function suppressOnBluer(e)
		{	//alert("");
			//e.value = Check2FontChar2LastChar(e);
			
			var s= e.value;
			e.value = '';
			for(i=0;i<s.length;i++)
			{     if (s.charCodeAt(i)<=128)
					{
						e.value += s.charAt(i);
					}			
			}
			if(s.length!=e.value.length)
			{
			//alert('Other than english character are not supported')
				return false;
			}//debugger;
			//var hddConNo = document.getElementById("hddConNo");
			__doPostBack('hddConNo','');
		}	
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition_page();" MS_POSITIONING="GridLayout">
		<form id="Quick_POD" method="post" runat="server">
			<input style="DISPLAY: none" id="hddConNo" type="button" name="hddCon" runat="server">
			<P><FONT face="Tahoma"></FONT>&nbsp;</P>
			<P><asp:label style="Z-INDEX: 101; POSITION: absolute; TOP: 23px; LEFT: 14px" id="Header1" Width="490"
					CssClass="mainTitleSize" Runat="server">Quick POD</asp:label></P>
			<P></P>
			<TABLE style="Z-INDEX: 106; POSITION: absolute; WIDTH: 896px; HEIGHT: 49px; TOP: 56px; LEFT: 16px"
				id="Table10">
				<TR>
					<TD vAlign="bottom"><asp:button style="Z-INDEX: 0" id="btnQry" runat="server" Width="61px" CssClass="queryButton"
							CausesValidation="False" Text="Query"></asp:button><asp:button style="Z-INDEX: 0" id="btnExecQry" runat="server" Width="130px" CssClass="queryButton"
							CausesValidation="False" Text="Execute Query"></asp:button><asp:button style="Z-INDEX: 0" id="btnSave" runat="server" Width="86px" CssClass="queryButton"
							CausesValidation="False" Text="Save"></asp:button></TD>
					<TD vAlign="bottom">&nbsp;
					</TD>
					<TD vAlign="bottom">&nbsp;
					</TD>
				</TR>
				<TR>
					<TD colSpan="4">&nbsp;
						<asp:label style="Z-INDEX: 0" id="lblErrrMsg" runat="server" Width="720" CssClass="errorMsgColor"
							Height="2"></asp:label></TD>
				</TR>
			</TABLE>
			<P></P>
			<INPUT style="Z-INDEX: 103; POSITION: absolute; TOP: 432px; LEFT: 760px" type="hidden"
				name="ScrollPosition">&nbsp;&nbsp;
			<asp:validationsummary style="Z-INDEX: 104; POSITION: absolute; TOP: 16px; LEFT: 552px" id="reqSummary"
				Runat="server" DisplayMode="BulletList" ShowMessageBox="True" HeaderText="Please enter the missing  fields."
				ShowSummary="False"></asp:validationsummary>
			<FIELDSET style="Z-INDEX: 105; POSITION: absolute; WIDTH: 824px; HEIGHT: 490px; TOP: 120px; LEFT: 16px"
				id="fsCreateSIP" runat="server">
				<TABLE id="Table1" cellSpacing="0" cellPadding="0">
					<TR>
						<TD vAlign="top">
							<TABLE style="WIDTH: 600px" id="Table8">
								<TBODY class="tableLabel">
									<TR>
										<TD style="WIDTH: 4px; HEIGHT: 23px"></TD>
										<TD style="WIDTH: 148px; HEIGHT: 23px"><asp:label id="lblConNo" runat="server" CssClass="tableLabel" style="Z-INDEX: 0">Consignment 
																		Number:</asp:label></TD>
										<TD style="WIDTH: 448px; HEIGHT: 23px"><cc1:mstextbox style="Z-INDEX: 0" id="txtConNo" runat="server" Width="172px" CssClass="tableTextbox"
												MaxLength="32" AutoPostBack="True"></cc1:mstextbox></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD style="WIDTH: 148px" class="tableLabel"><asp:label id="lblOriginDC" runat="server" CssClass="tableLabel"> Origin DC:</asp:label></TD>
										<TD style="WIDTH: 448px"><asp:dropdownlist id="ddlOriginDC" runat="server" Width="106px" AutoPostBack="True" tabIndex="1"></asp:dropdownlist><asp:textbox id="tbTempText" Runat="server" Visible="False"></asp:textbox></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD style="WIDTH: 148px" class="tableLabel"><asp:label id="lblDestPostCode" runat="server" CssClass="tableLabel">Destination Postal Code:</asp:label></TD>
										<TD style="WIDTH: 448px"><cc1:mstextbox style="Z-INDEX: 0" id="txtDestPostCode" runat="server" Width="50px" CssClass="tableTextbox"
												MaxLength="10" AutoPostBack="True" tabIndex="2"></cc1:mstextbox><asp:label id="lblProvince" runat="server" CssClass="tableLabel"></asp:label><asp:label id="lbltmpDestDC" runat="server" Visible="False"></asp:label></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px; HEIGHT: 21px"></TD>
										<TD style="WIDTH: 148px; HEIGHT: 21px" class="tableLabel"><asp:label id="lblServiceType" runat="server" CssClass="tableLabel">Service Type:</asp:label></TD>
										<TD style="WIDTH: 448px; HEIGHT: 21px"><asp:dropdownlist id="ddlServiceType" runat="server" Width="106px" tabIndex="3"></asp:dropdownlist></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD style="WIDTH: 148px" class="tableLabel"><asp:label id="lblNumOfPackages" runat="server" CssClass="tableLabel">Number of Packages:</asp:label></TD>
										<TD style="WIDTH: 448px"><cc1:mstextbox id="txtPkgNum" runat="server" Width="66px" CssClass="tableTextbox" TextMaskType="msNumeric"
												NumberMinValue="1" NumberPrecision="3" NumberMaxValue="9999" tabIndex="4"></cc1:mstextbox></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD style="WIDTH: 148px" class="tableLabel"></TD>
										<TD style="WIDTH: 448px"></TD>
									</TR>
									<tr>
										<TD style="WIDTH: 4px">&nbsp;</TD>
										<TD style="WIDTH: 100%" colSpan="2">
											<fieldset><LEGEND><asp:label id="lblPOD_Detail" runat="server" Width="80px" CssClass="tableHeadingFieldset" Height="16px">POD Details</asp:label></LEGEND>
												<table style="WIDTH: 100%">
													<tr>
														<td style="WIDTH: 148px"><asp:label style="Z-INDEX: 0" id="lblActual_Datetine" runat="server" CssClass="tableLabel">Actual D/T:</asp:label></td>
														<td style="WIDTH: 134px"><cc1:mstextbox id="txtActual" runat="server" Width="140px" CssClass="textField" MaxLength="16"
																TextMaskType="msDateTime" TextMaskString="99/99/9999 99:99" tabIndex="5"></cc1:mstextbox></td>
														<td style="WIDTH: 8px"></td>
														<td style="WIDTH: 100px"><asp:label style="Z-INDEX: 0" id="lblConsignee" runat="server" CssClass="tableLabel">Consignee:</asp:label></td>
														<td style="WIDTH: 172px"><asp:textbox style="Z-INDEX: 0" id="txtConsignee" runat="server" Width="140px" CssClass="tableTextbox"
																MaxLength="20" tabIndex="6"></asp:textbox></td>
													</tr>
													<tr>
														<td style="WIDTH: 148px; HEIGHT: 21px"><asp:label style="Z-INDEX: 0" id="lblRemark" runat="server" CssClass="tableLabel">Remark:</asp:label></td>
														<td style="WIDTH: 134px" colSpan="4"><asp:textbox id="txtRemark" runat="server" Width="422px" Height="82px" TextMode="MultiLine" tabIndex="7"></asp:textbox></td>
													</tr>
												</table>
											</fieldset>
										</TD>
									</tr>
								</TBODY>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
				<TABLE style="WIDTH: 784px; HEIGHT: 144px" id="Table2">
					<TR class="tableLabel">
						<TD style="HEIGHT: 14px" noWrap><asp:label id="lblColConsignment" runat="server" Width="120px" CssClass="tableLabel">Consignment</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:label style="Z-INDEX: 0" id="lblColService" runat="server" Width="56px" CssClass="tableLabel">Service</asp:label>&nbsp;&nbsp;<asp:label style="Z-INDEX: 0" id="lblColDestination" runat="server" Width="26px" CssClass="tableLabel">Destination</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;<asp:label style="Z-INDEX: 0" id="lblColPkgs" runat="server" CssClass="tableLabel">Pkgs</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:label style="Z-INDEX: 0" id="lblColPOD" runat="server" CssClass="tableLabel">POD</asp:label>&nbsp;<asp:label style="Z-INDEX: 0" id="lblColDataTime" runat="server" CssClass="tableLabel">Date/Time</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:label style="Z-INDEX: 0" id="Label1" runat="server" CssClass="tableLabel">Origin DC</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:label style="Z-INDEX: 0" id="lblColUser" runat="server" CssClass="tableLabel">Consignee</asp:label></TD>
					</TR>
					<TR>
						<TD><asp:listbox style="FONT-FAMILY: 'Courier New', Courier, monospace" id="lbSIPCon" runat="server"
								Width="776px" Height="120px" AutoPostBack="True"></asp:listbox></TD>
					</TR>
				</TABLE>
			</FIELDSET>
			<INPUT id="hidHost" type="hidden" name="hhost" runat="server"> <INPUT id="hidPort" type="hidden" name="hport" runat="server">
		</form>
	</body>
</HTML>
