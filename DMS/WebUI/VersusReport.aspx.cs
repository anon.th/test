using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.ties.classes;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for VersusReport.
	/// </summary>
	public class VersusReport : BasePage 
	{
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected com.common.util.msTextBox txtPeriod;
		protected System.Web.UI.WebControls.Label lblTo;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Button btnGenerate;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.HtmlControls.HtmlTable table1;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label8;
		protected com.common.util.msTextBox txtDesStatus1;
		protected com.common.util.msTextBox txtDesStatus2;
		protected com.common.util.msTextBox txtDesStatus3;
		protected com.common.util.msTextBox txtDesStatus4;
		protected com.common.util.msTextBox txtDesStatus5;
		protected System.Web.UI.WebControls.RadioButtonList rblHistoryType;
		protected Cambro.Web.DbCombo.DbCombo dbCmbStatusCode1;
		protected Cambro.Web.DbCombo.DbCombo dbCmbStatusCode2;
		protected Cambro.Web.DbCombo.DbCombo dbCmbStatusCode3;
		protected Cambro.Web.DbCombo.DbCombo dbCmbStatusCode4;
		protected Cambro.Web.DbCombo.DbCombo dbCmbStatusCode5;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.HtmlControls.HtmlTable Table3;
		protected System.Web.UI.WebControls.Label Label10;
		protected Cambro.Web.DbCombo.DbCombo DbcmbRoutecode;
		protected System.Web.UI.WebControls.RadioButtonList rdlDeliveryType;
		protected com.common.util.msTextBox txtDesStatus6;
		protected Cambro.Web.DbCombo.DbCombo dbCmbStatusCode6;
		protected System.Web.UI.WebControls.Label Label11;
		protected com.common.util.msTextBox txtDesStatus7;
		protected Cambro.Web.DbCombo.DbCombo dbCmbStatusCode7;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.CheckBox chkMissFirstStatus;

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.rdlDeliveryType.SelectedIndexChanged += new System.EventHandler(this.rdlDeliveryType_SelectedIndexChanged);
			this.dbCmbStatusCode1.SelectedItemChanged += new System.EventHandler(this.dbCmbStatusCode1_SelectedItemChanged);
			this.dbCmbStatusCode2.SelectedItemChanged += new System.EventHandler(this.dbCmbStatusCode2_SelectedItemChanged);
			this.dbCmbStatusCode3.SelectedItemChanged += new System.EventHandler(this.dbCmbStatusCode3_SelectedItemChanged);
			this.dbCmbStatusCode4.SelectedItemChanged += new System.EventHandler(this.dbCmbStatusCode4_SelectedItemChanged);
			this.dbCmbStatusCode5.SelectedItemChanged += new System.EventHandler(this.dbCmbStatusCode5_SelectedItemChanged);
			this.dbCmbStatusCode6.SelectedItemChanged += new System.EventHandler(this.dbCmbStatusCode6_SelectedItemChanged);
			this.dbCmbStatusCode7.SelectedItemChanged += new System.EventHandler(this.dbCmbStatusCode7_SelectedItemChanged);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.rblHistoryType.SelectedIndexChanged += new System.EventHandler(this.rblHistoryType_SelectedIndexChanged);
			this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}

		#endregion

		private String m_strEnterpriseID;
		private String m_History;
		private String m_strAppID;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			m_strAppID = System.Configuration.ConfigurationSettings.AppSettings["applicationID"];//utility.GetAppID();
			m_strEnterpriseID = System.Configuration.ConfigurationSettings.AppSettings["enterpriseID"];//utility.GetEnterpriseID();

			Session["history"] = rblHistoryType.SelectedItem.Value.ToString();
	
			if(!IsPostBack)
			{
				DefaultScreen();	
			}
			SetComboProperties();
		}


		private void btnGenerate_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			bool icheck =  ValidateValues();

			if(icheck==false)
			{
				setParamDataSet();
				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				OpenWindowpage(strUrl);
			}
		}


		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}


		private void DefaultScreen()
		{
			rbMonth.Checked = true;
			rbPeriod.Checked = false;
			rbDate.Checked = false;
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
			ddMonth.SelectedIndex=0; 
			LoadMonths();
			Clearddl();
			lblErrorMessage.Text = "";

		}


		private void LoadMonths()
		{
			ddMonth.Items.Clear();
			ListItem defItem = new ListItem();
			defItem.Text = "";
			defItem.Value = "0";
			ddMonth.Items.Add(defItem);
			ArrayList systemCodes = Utility.GetCodeValues(m_strAppID  ,"en-US","months",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				ddMonth.Items.Add(lstItem);
			}
				
		}


		private void Clearddl()
		{
			dbCmbStatusCode1.Text="";dbCmbStatusCode1.Value="";txtDesStatus1.Text="";
			dbCmbStatusCode2.Text="";dbCmbStatusCode2.Value="";txtDesStatus2.Text="";
			dbCmbStatusCode3.Text="";dbCmbStatusCode3.Value="";txtDesStatus3.Text="";
			dbCmbStatusCode4.Text="";dbCmbStatusCode4.Value="";txtDesStatus4.Text="";
			dbCmbStatusCode5.Text="";dbCmbStatusCode5.Value="";txtDesStatus5.Text="";
			dbCmbStatusCode6.Text="";dbCmbStatusCode6.Value="";txtDesStatus6.Text="";
			dbCmbStatusCode7.Text="";dbCmbStatusCode7.Value="";txtDesStatus7.Text="";
			
			DbcmbRoutecode.Text="";DbcmbRoutecode.Value="";

			dbCmbStatusCode2.Enabled=false;txtDesStatus2.Enabled=false;
			dbCmbStatusCode3.Enabled=false;txtDesStatus3.Enabled=false;
			dbCmbStatusCode4.Enabled=false;txtDesStatus4.Enabled=false;
			dbCmbStatusCode5.Enabled=false;txtDesStatus5.Enabled=false;
			dbCmbStatusCode6.Enabled=false;txtDesStatus6.Enabled=false;
			dbCmbStatusCode7.Enabled=false;txtDesStatus7.Enabled=false;

		}


		private bool ValidateValues()
		{
			bool iCheck=false;
			if((ddMonth.SelectedIndex==0)&&(txtYear.Text=="")&&(txtPeriod.Text=="")&&(txtTo.Text=="")&&(txtDate.Text==""))
			{
				lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVLY",utility.GetUserCulture());
				return iCheck=true;
			
			}
			if(rbMonth.Checked == true)
			{
				if((ddMonth.SelectedIndex>0) &&(txtYear.Text==""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((ddMonth.SelectedIndex==0) &&(txtYear.Text!=""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbPeriod.Checked == true )
			{
				if((txtPeriod.Text!="")&&(txtTo.Text==""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_END_DT",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((txtPeriod.Text=="")&&(txtTo.Text!=""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_START_DT",utility.GetUserCulture());			
					return iCheck=true;
			
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbDate.Checked == true )
			{
				if(txtDate.Text=="")
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT",utility.GetUserCulture());	
					return iCheck=true;
				
				}
			}
			

			return iCheck;
			
		}


		private void BindStatusCode(DropDownList ddl)
		{
			
			ddl.DataSource = Utility.GetStatusCode(m_strAppID,m_strEnterpriseID, "");
			ddl.DataTextField = "status_code";
			ddl.DataValueField = "status_code";
			ddl.DataBind();	
			ddl.SelectedItem.Text= "";
		}


		private void  setParamDataSet()
		{
			DataSet m_dsQuery =  new DataSet();
			DataTable dtParam = new DataTable();

			dtParam.Columns.Add("ApplicationID",typeof(string));		//1
			dtParam.Columns.Add("EnterpriseID",typeof(string));			//2
			dtParam.Columns.Add("Type",typeof(string));					//3
			dtParam.Columns.Add("FromDate",typeof(DateTime));			//4
			dtParam.Columns.Add("ToDate",typeof(DateTime));				//5
			dtParam.Columns.Add("Status1",typeof(string));				//6
			dtParam.Columns.Add("Status2",typeof(string));				//7
			dtParam.Columns.Add("Status3",typeof(string));				//8
			dtParam.Columns.Add("Status4",typeof(string));				//9
			dtParam.Columns.Add("Status5",typeof(string));				//10
			dtParam.Columns.Add("Status6",typeof(string));				//11
			dtParam.Columns.Add("Status7",typeof(string));				//12
			dtParam.Columns.Add("RouteType",typeof(string));			//13
			dtParam.Columns.Add("RouteCode",typeof(string));			//14
			dtParam.Columns.Add("PathOriginDC", typeof(string));		//15
			dtParam.Columns.Add("PathDestinationDC", typeof(string));	//16
			dtParam.Columns.Add("Flag",typeof(string));					//17
		
			DataRow drEach = dtParam.NewRow();

			drEach["ApplicationID"] = utility.GetAppID();
			drEach["EnterpriseID"] = utility.GetEnterpriseID();
			drEach["Type"] = rblHistoryType.SelectedItem.Value;
			
			String strStartDate = null;
			DateTime dtStartDate = DateTime.Now;
			DateTime dtEndDate = DateTime.Now;

			if((ddMonth.SelectedIndex==0) && (txtYear.Text=="")&&(txtPeriod.Text=="")&&(txtTo.Text=="")&&(txtDate.Text==""))
			{
				lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVL",utility.GetUserCulture());
				return;
			}
			
			if (rbMonth.Checked==true)
			{					
				if(ddMonth.SelectedItem.Value != null && Convert.ToInt32(ddMonth.SelectedItem.Value) < 10)
					strStartDate="01"+"/0"+ddMonth.SelectedItem.Value+"/"+txtYear.Text.Trim();
				else
					strStartDate="01"+"/"+ddMonth.SelectedItem.Value+"/"+txtYear.Text.Trim();
				dtStartDate=System.DateTime.ParseExact(strStartDate,"dd/MM/yyyy",null);				
				dtEndDate=dtStartDate.AddMonths(1).AddDays(-1);				
			}
			if (rbPeriod.Checked==true)
			{
				dtStartDate=System.DateTime.ParseExact(txtPeriod.Text.Trim(),"dd/MM/yyyy",null);									
				dtEndDate=System.DateTime.ParseExact(txtTo.Text.Trim(),"dd/MM/yyyy",null);
			}
			if (rbDate.Checked==true)
			{			
				dtStartDate=System.DateTime.ParseExact(txtDate.Text.Trim(),"dd/MM/yyyy",null);
				dtEndDate=dtStartDate.AddDays(1).AddMinutes(-1);				
			}
			drEach["FromDate"] = dtStartDate;
			drEach["ToDate"] = dtEndDate;
			
			String strStatusCode1 = "";
			String strStatusCode2 = "";
			String strStatusCode3 = "";
			String strStatusCode4 = "";
			String strStatusCode5 = "";
			String strStatusCode6 = "";
			String strStatusCode7 = "";

			strStatusCode1 = dbCmbStatusCode1.Value;
			strStatusCode2 = dbCmbStatusCode2.Value;
			strStatusCode3 = dbCmbStatusCode3.Value;
			strStatusCode4 = dbCmbStatusCode4.Value;
			strStatusCode5 = dbCmbStatusCode5.Value;
			strStatusCode6 = dbCmbStatusCode6.Value;
			strStatusCode7 = dbCmbStatusCode7.Value;

			if (Utility.IsNotDBNull(strStatusCode1))
				drEach["Status1"] = strStatusCode1;
			if (Utility.IsNotDBNull(strStatusCode2))
				drEach["Status2"] = strStatusCode2;
			if (Utility.IsNotDBNull(strStatusCode3))
				drEach["Status3"] = strStatusCode3;
			if (Utility.IsNotDBNull(strStatusCode4))
				drEach["Status4"] = strStatusCode4;
			if (Utility.IsNotDBNull(strStatusCode5))
				drEach["Status5"] = strStatusCode5;
			if (Utility.IsNotDBNull(strStatusCode6))
				drEach["Status6"] = strStatusCode6;
			if (Utility.IsNotDBNull(strStatusCode7))
				drEach["Status7"] = strStatusCode7;


			drEach["RouteType"] = rdlDeliveryType.SelectedItem.Value;
			drEach["RouteCode"] = DbcmbRoutecode.Value;
			
			if ((rdlDeliveryType.SelectedItem.Value == "L") ||
				(rdlDeliveryType.SelectedItem.Value == "A")) 
			{
				com.ties.classes.DeliveryPath delPath = new com.ties.classes.DeliveryPath();
				delPath.Populate(m_strAppID, m_strEnterpriseID, DbcmbRoutecode.Value);

				drEach["PathOriginDC"] = delPath.OriginStation;
				drEach["PathDestinationDC"] = delPath.DestinationStation;
			}
			else
			{
				drEach["PathOriginDC"] = "";
				drEach["PathDestinationDC"] = "";
			}

			String strMissFirstStatus = null;
			
			if(chkMissFirstStatus.Checked)
				strMissFirstStatus = "Y";
			else
				strMissFirstStatus = "N";

			if (Utility.IsNotDBNull(strMissFirstStatus))
				drEach["Flag"] = strMissFirstStatus;

			m_dsQuery.Tables.Add(dtParam) ; 
			m_dsQuery.Tables[0].Rows.Add(drEach);
			Session["SESSION_DS1"] = m_dsQuery;
			Session["FORMID"]="VERSUS REPORT"; 
		}		


		private void SetComboProperties()
		{
			dbCmbStatusCode1.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];	
			dbCmbStatusCode2.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];			
			dbCmbStatusCode3.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbStatusCode4.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbStatusCode5.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbStatusCode6.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbStatusCode7.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			
			SetHistoryServerStates();
			SetDeliverytypeRoutecode();
		}

		
		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		
		#region "Controls"

		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			txtDate.Enabled = true;
			txtPeriod.Enabled = false;
			txtTo.Enabled = false;
			txtPeriod.Text ="";
			txtTo.Text ="";
			ddMonth.SelectedIndex =0;
			txtYear.Text="";
			ddMonth.Enabled = false;
			txtYear.Enabled =false;
		}


		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			txtPeriod.Enabled = true;
			txtTo.Enabled = true;
			
			ddMonth.Enabled = false; 
			txtYear.Enabled = false;
			ddMonth.SelectedIndex=0;
			txtYear.Text ="";
		}


		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true; 
			txtYear.Enabled = true; 
			txtPeriod.Text ="";
			txtTo.Text="";
			txtDate.Text ="";
		}


		private void rblHistoryType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			m_History = rblHistoryType.SelectedItem.Value.ToString();
			Clearddl();
		}


		#endregion

		#region "Cambro"

		private void SetHistoryServerStates()
		{
			String strhistory=null;
			strhistory = rblHistoryType.SelectedItem.Value.ToString();
			
			Hashtable hash = new Hashtable();
			hash.Add("strhistory", strhistory);
			
			dbCmbStatusCode1.ServerState = hash;
			dbCmbStatusCode1.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4";
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object StatusCodeServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			String historyType="";
			String strStatusCode="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm qrs;q4"))
			{
				if(args.ServerState["strStatusCode"] != null && args.ServerState["strStatusCode"].ToString().Length > 0)
				{
					strStatusCode = args.ServerState["strStatusCode"].ToString();					
				}
				if(args.ServerState["strhistory"] != null && args.ServerState["strhistory"].ToString().Length > 0)
				{
					historyType = args.ServerState["strhistory"].ToString();					
				}
			}	
			DataSet dataset = DbComboDAL.VersusStatusCodeQuery(strAppID,strEnterpriseID,args,historyType,"");	
			return dataset;                
		}


		private void SetDeliverytypeRoutecode()
		{
			String strDeliveryType=null,strHistoryType=null;
			strDeliveryType = rdlDeliveryType.SelectedItem.Value.ToString();
			strHistoryType = rblHistoryType.SelectedItem.Value.ToString();
			
			Hashtable hash = new Hashtable();
			hash.Add("strDeliveryType", strDeliveryType);
			hash.Add("strHistoryType", strHistoryType);
		
			DbcmbRoutecode.ServerState = hash;
			DbcmbRoutecode.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}


		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object DbComboPathCodeSelect(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			String strDelType = "L";			
			String strWhereClause=null;
			String strHistoryType = "";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{
				if(args.ServerState["strDeliveryType"] != null && args.ServerState["strDeliveryType"].ToString().Length > 0)
				{
					strDelType = args.ServerState["strDeliveryType"].ToString();
					strHistoryType = args.ServerState["strHistoryType"].ToString();
					strWhereClause=" and Delivery_Type='"+Utility.ReplaceSingleQuote(strDelType)+"' ";
					if(strDelType == "S" && strHistoryType == "D")
					{
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct delivery_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
					else if(strDelType == "S" && strHistoryType == "P")
					{
						
						strWhereClause=strWhereClause+"and path_code in ( ";
						strWhereClause=strWhereClause+"select distinct pickup_route ";
						strWhereClause=strWhereClause+"from Zipcode ";
						strWhereClause=strWhereClause+"where applicationid = '"+strAppID+"' ";
						strWhereClause=strWhereClause+"and enterpriseid = '"+ strEnterpriseID +"') ";
					}
				}				
			}
			
			DataSet dataset = DbComboDAL.PathCodeQuery(strAppID,strEnterpriseID,args, strWhereClause);	
			return dataset;
		}


		#endregion

		#region "dbCmb Status Code"

		private void dbCmbStatusCode1_SelectedItemChanged(object sender, System.EventArgs e)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet ds = DbComboDAL.VersusStatusCodeQuery(strAppID,strEnterpriseID,rblHistoryType.SelectedItem.Value.ToString(),dbCmbStatusCode1.Value.ToString());	

			if(dbCmbStatusCode1.Value.ToString() !="")
			{
				txtDesStatus1.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString();
			}
			else
			{
				txtDesStatus1.Text = "";
			}
			dbCmbStatusCode2.Enabled = true;
			dbCmbStatusCode3.Enabled = false;
			dbCmbStatusCode4.Enabled = false;
			dbCmbStatusCode5.Enabled = false;
			dbCmbStatusCode6.Enabled = false;
			dbCmbStatusCode7.Enabled = false;
		}


		private void dbCmbStatusCode2_SelectedItemChanged(object sender, System.EventArgs e)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet ds = DbComboDAL.VersusStatusCodeQuery(strAppID,strEnterpriseID,rblHistoryType.SelectedItem.Value.ToString(),dbCmbStatusCode2.Value.ToString());	

			if(dbCmbStatusCode2.Value.ToString() !="")
			{
				txtDesStatus2.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString();
			}
			else
			{
				txtDesStatus2.Text = "";
			}
			dbCmbStatusCode3.Enabled = true;
			dbCmbStatusCode4.Enabled = false;
			dbCmbStatusCode5.Enabled = false;
			dbCmbStatusCode6.Enabled = false;
			dbCmbStatusCode7.Enabled = false;
		}


		private void dbCmbStatusCode3_SelectedItemChanged(object sender, System.EventArgs e)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet ds = DbComboDAL.VersusStatusCodeQuery(strAppID,strEnterpriseID,rblHistoryType.SelectedItem.Value.ToString(),dbCmbStatusCode3.Value.ToString());	

			if(dbCmbStatusCode3.Value.ToString() !="")
			{
				txtDesStatus3.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString();
			}
			else
			{
				txtDesStatus3.Text = "";
			}
			dbCmbStatusCode4.Enabled = true;
			dbCmbStatusCode5.Enabled = false;
			dbCmbStatusCode6.Enabled = false;
			dbCmbStatusCode7.Enabled = false;
		}


		private void dbCmbStatusCode4_SelectedItemChanged(object sender, System.EventArgs e)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet ds = DbComboDAL.VersusStatusCodeQuery(strAppID,strEnterpriseID,rblHistoryType.SelectedItem.Value.ToString(),dbCmbStatusCode4.Value.ToString());	

			if(dbCmbStatusCode4.Text !="")
			{
				txtDesStatus4.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString();
			}
			else
			{
				txtDesStatus4.Text = "";
			}
			dbCmbStatusCode5.Enabled = true;
			dbCmbStatusCode6.Enabled = false;
			dbCmbStatusCode7.Enabled = false;
		}


		private void dbCmbStatusCode5_SelectedItemChanged(object sender, System.EventArgs e)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet ds = DbComboDAL.VersusStatusCodeQuery(strAppID,strEnterpriseID,rblHistoryType.SelectedItem.Value.ToString(),dbCmbStatusCode5.Value.ToString());	

			if(dbCmbStatusCode5.Value.ToString() !="")
			{
				txtDesStatus5.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString();
			}
			else
			{
				txtDesStatus5.Text = "";
			}
			
			dbCmbStatusCode6.Enabled = true;
			dbCmbStatusCode7.Enabled = false;
		}


		private void dbCmbStatusCode6_SelectedItemChanged(object sender, System.EventArgs e)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet ds = DbComboDAL.VersusStatusCodeQuery(strAppID,strEnterpriseID,rblHistoryType.SelectedItem.Value.ToString(),dbCmbStatusCode6.Value.ToString());	

			if(dbCmbStatusCode5.Value.ToString() !="")
			{
				txtDesStatus6.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString();
			}
			else
			{
				txtDesStatus6.Text = "";
			}
			
			dbCmbStatusCode7.Enabled = true;
		}


		private void dbCmbStatusCode7_SelectedItemChanged(object sender, System.EventArgs e)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet ds = DbComboDAL.VersusStatusCodeQuery(strAppID,strEnterpriseID,rblHistoryType.SelectedItem.Value.ToString(),dbCmbStatusCode7.Value.ToString());	

			if(dbCmbStatusCode7.Value.ToString() !="")
			{
				txtDesStatus7.Text = ds.Tables[0].Rows[0].ItemArray[1].ToString();
			}
			else
			{
				txtDesStatus7.Text = "";
			}
			
		}


		#endregion

		private void rdlDeliveryType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DbcmbRoutecode.Text="";
			DbcmbRoutecode.Value="";
		}

	}
}