using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for AssignSenderLocation.
	/// </summary>
	public class AssignSenderLocation : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.ValidationSummary Validationsummary1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnDelete;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.DropDownList Drp_Location;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.DataGrid dgLodgments;
		protected System.Web.UI.WebControls.DataGrid Datagrid1;
		protected System.Web.UI.WebControls.Button btnHidReport;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			BindGrid();
			lblErrorMsg.Text = "* Minimum Copies is number of con note copies plus one per pkg";
			lblErrorMsg.Visible = true;
			dgLodgments.Items[2].Cells[0].Text = null;
			dgLodgments.Items[2].Cells[1].Text = null;
			dgLodgments.Items[2].Cells[5].Text = null;

			

			DataTable dt1 = new DataTable();

			dt1.Columns.Add("Name",typeof(String));

			DropDownList ddl = new DropDownList();
			dgLodgments.Items[0].FindControl("DropDownList2").Visible = false;
			dgLodgments.Items[1].FindControl("DropDownList1").Visible = false;
			// Put user code to initialize the page here
		}
		private void BindGrid()
		{	
			DataSet ds = new DataSet();
			DataTable dt = new DataTable();

			dt.Columns.Add("Master",typeof(String));
			dt.Columns.Add("Account",typeof(String));
			dt.Columns.Add("Username",typeof(String));
			dt.Columns.Add("Minimum Copies",typeof(String));
			dt.Columns.Add("Sender Name",typeof(String));
			dt.Columns.Add("Sender Address",typeof(String));
			dt.Columns.Add("Postal Code",typeof(String));
			//			DataColumn[] dc
			//				={
			//					 new DataColumn("Master",Type(ofString)),
			//					 new DataColumn("Account",String),
			//					 new DataColumn("Username",String),
			//					 new DataColumn("Minimun Copies",String),
			//					 new DataColumn("Sender Name",String),
			//					 new DataColumn("Sender Address",String),
			//					 new DataColumn("Postal Code",String)
			//			 };
			//
			//			dt.Columns.Add(dc);
			
			DataRow dr = dt.NewRow();
			dr["Master"] = "true";
			dr["Account"] = "20650";
			dr["Username"] = "BankSP";
			dr["Minimum Copies"] = "1";
			dr["Sender Name"] = "BILLING";
			dr["Sender Address"] = "PO BOX 173";
			dr["Postal Code"] = "POM";
			dt.Rows.Add(dr);

			dr = dt.NewRow();
			dr["Master"] = "false";
			dr["Account"] = "BSP01";
			dr["Username"] = "BankSPLAE";
			dr["Minimum Copies"] = "1";
			dr["Sender Name"] = "BANKSPLAE";
			dr["Sender Address"] = "PO BOX 2123";
			dr["Postal Code"] = "LAE";
			dt.Rows.Add(dr);

			dr = dt.NewRow();
			dr["Master"] = "false";
			dr["Account"] = "";
			dr["Username"] = "";
			dr["Minimum Copies"] = "";
			dr["Sender Name"] = "";
			dr["Sender Address"] = "";
			dr["Postal Code"] = "";
			dt.Rows.Add(dr);

			

			this.dgLodgments.DataSource = dt;
			this.dgLodgments.DataBind();

//			
//			DataTable dt2 = new DataTable();
//
//			dt2.Columns.Add("Master",typeof(Boolean));
//			dt2.Columns.Add("Account",typeof(String));
//			dt2.Columns.Add("Username",typeof(String));
//			dt2.Columns.Add("Minimum Copies",typeof(String));
//			dt2.Columns.Add("Sender Name",typeof(String));
//			dt2.Columns.Add("Sender Address",typeof(String));
//			dt2.Columns.Add("Postal Code",typeof(String));
//
//			DataRow dr2 = dt2.NewRow();
//			dr2["Master"] = false;
//			dr2["Account"] = "";
//			dr2["Username"] = "";
//			dr2["Minimum Copies"] = "";
//			dr2["Sender Name"] = "";
//			dr2["Sender Address"] = "";
//			dr2["Postal Code"] = "";
//			dt2.Rows.Add(dr2);
//			
//			this.Datagrid1.DataSource = dt2;
//			this.Datagrid1.DataBind();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
