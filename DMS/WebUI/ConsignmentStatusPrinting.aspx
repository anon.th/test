<%@ Page language="c#" Codebehind="ConsignmentStatusPrinting.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.ConsignmentStatusPrinting" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Consignment Status Printing</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
		function funcDisBack()
		{
			history.go(+1);
		}

        function RemoveBadMasterAWBNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^-_/_a-zA-Z0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        function AfterPasteMasterAWBNumber(obj) {
            setTimeout(function () {
                RemoveBadMasterAWBNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function ValidateMasterAWBNumber(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[-_/_a-zA-Z0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}
		
		function RemoveBadNonNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
        
        function AfterPasteNonNumber(obj) {
            setTimeout(function () {
                RemoveBadNonNumber(obj.value, obj);
            }, 1); //or 4
        }
        
        function validate(evt) {
			var theEvent = evt || window.event;
			var key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode( key );
			var regex = /[0-9]/;
			if( !regex.test(key) ) {
				theEvent.returnValue = false;
				if(theEvent.preventDefault) theEvent.preventDefault();
			}
		}		
		</script>
</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="ConsignmentStatusPrinting" method="post" runat="server">
			<INPUT style="Z-INDEX: 101; POSITION: absolute; TOP: 680px; LEFT: 16px" type="hidden" name="ScrollPosition">
			<asp:validationsummary style="Z-INDEX: 105; POSITION: absolute; COLOR: red; TOP: 672px; LEFT: 240px" id="Validationsummary1"
				ShowSummary="False" HeaderText="Please enter the missing  fields." ShowMessageBox="True" DisplayMode="BulletList"
				Runat="server"></asp:validationsummary>
			<div style="Z-INDEX: 102; POSITION: relative; HEIGHT: 1248px; TOP: 32px; LEFT: 24px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tr>
						<td colSpan="2" align="left"><asp:label id="Label1" runat="server" Width="421px" Height="27px" CssClass="mainTitleSize">
							Consignment Status/Printing</asp:label></td>
					</tr>
					<tr>
						<td style="HEIGHT: 24px" vAlign="top" colSpan="2" align="left">
							<asp:button id="btnQry" runat="server" Width="61px" CssClass="queryButton" Text="Query" CausesValidation="False"
								style="Z-INDEX: 0"></asp:button>
							<asp:button id="btnExecQry" runat="server" Width="130px" CssClass="queryButton" Text="Execute Query"
								CausesValidation="False"></asp:button>
							<asp:button id="btnPrintConsNote" runat="server" Width="150px" CssClass="queryButton" Text="Print Con Notes"
								CausesValidation="False" Enabled="False"></asp:button>
							<asp:button id="btnPrintShipList" runat="server" Width="150px" CssClass="queryButton" Text="Print Shipping List"
								CausesValidation="False" Enabled="False"></asp:button>
							<asp:button id="btnReprintConsNote" runat="server" Width="150px" CssClass="queryButton" Text="Reprint Con Note"
								CausesValidation="False" Enabled="False"></asp:button>
							<asp:button id="btnReprintConsNoteLabel" runat="server" Width="192px" CssClass="queryButton"
								Text="Reprint Con Notes Label" CausesValidation="False" Enabled="False"></asp:button>
						</td>
					</tr>
					<tr>
						<td vAlign="top" colSpan="2" align="left">
							<TABLE style="Z-INDEX: 112; WIDTH: 740px" id="Table1" border="0" width="730" runat="server">
								<TR width="100%">
									<TD style="Z-INDEX: 0" width="100%">
										<fieldset><legend><asp:label id="lblSearch" runat="server" CssClass="tableHeadingFieldset">Search Criteria</asp:label></legend><br>
											<table border="0" cellSpacing="1" cellPadding="1">
												<TBODY>
													<TR id="TrNonCustoms" runat="server">
														<TD style="WIDTH: 107px; HEIGHT: 20px"><FONT face="Tahoma"><asp:label style="Z-INDEX: 0" id="Label2" runat="server" Width="100px" CssClass="tableLabel">Customer ID</asp:label></FONT></TD>
														<TD width="285" style="HEIGHT: 20px"><FONT face="Tahoma"><asp:dropdownlist id="ddlCustomer" tabIndex="1" runat="server" Width="106px" CssClass="textField"
																	Enabled="False" AutoPostBack="True"></asp:dropdownlist></FONT></TD>
													</TR>
													<TR id="TrCustoms" runat="server">
														<TD style="WIDTH: 107px"><asp:label style="Z-INDEX: 0" id="Label3" runat="server" Width="100px" CssClass="tableLabel">Customer ID</asp:label></TD>
														<TD width="285"><FONT face="Tahoma"><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtCustID" tabIndex="1" runat="server"
																	Width="150px" CssClass="textField" Enabled="False" MaxLength="100">20650</asp:textbox></FONT></TD>
														<td><asp:label style="Z-INDEX: 0" id="Label4" runat="server" Width="130" CssClass="tableLabel">Master AWB No.</asp:label></td>
														<td><asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="MasterAWBNo" tabIndex="1" onkeypress="ValidateMasterAWBNumber(event)"
																onpaste="AfterPasteMasterAWBNumber(this)" runat="server" Width="150px" CssClass="textField"
																MaxLength="30"></asp:textbox></td>
													</TR>
													<tr>
														<td style="WIDTH: 107px"><asp:label id="lblSenderName" runat="server" Width="100px" CssClass="tableLabel">Sender Name</asp:label></td>
														<td width="285"><asp:dropdownlist id="ddlSenderName" tabIndex="2" runat="server" Width="106px" CssClass="textField"
																Enabled="False">
																<asp:ListItem Value="-1" Selected="True">BSPLae</asp:ListItem>
															</asp:dropdownlist></td>
														<td>
															<asp:label style="Z-INDEX: 0" id="lblJobEntryNo" runat="server" CssClass="tableLabel" Width="130">Job Entry No. </asp:label></td>
														<td>
															<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="JobEntryNo" tabIndex="15" onkeypress="validate(event)"
																onpaste="AfterPasteNonNumber(this)" runat="server" CssClass="textField" Width="150px" MaxLength="10"></asp:textbox></td>
													</tr>
													<tr>
														<td style="WIDTH: 107px"><asp:label id="lblConsNo" runat="server" Width="100" CssClass="tableLabel">Consignment No.</asp:label></td>
														<td width="285"><cc1:mstextbox style="TEXT-TRANSFORM: uppercase" id="txtConsNo" tabIndex="3" runat="server" CssClass="textField"
																AutoPostBack="False" MaxLength="14" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></td>
														<td><asp:label id="lblStatus" runat="server" Width="130" CssClass="tableLabel">Status</asp:label></td>
														<td width="450"><asp:dropdownlist id="ddlStatus" tabIndex="4" runat="server" Width="125px" CssClass="textField" Enabled="True"></asp:dropdownlist></td>
													</tr>
													<tr>
														<td style="WIDTH: 107px"><asp:label id="lblShipList" runat="server" CssClass="tableLabel">Shipping List</asp:label></td>
														<td><asp:textbox id="txtShipList" tabIndex="5" runat="server" Width="185px" CssClass="textField"
																MaxLength="50"></asp:textbox></td>
														<td><asp:label id="lblRecipPhoneNo" runat="server" CssClass="tableLabel">Recipient Phone No.</asp:label></td>
														<td><asp:textbox style="TEXT-TRANSFORM: uppercase" id="txtRecipPhoneNo" tabIndex="6" runat="server"
																CssClass="textField" MaxLength="12"></asp:textbox></td>
													</tr>
													<tr>
														<td style="WIDTH: 107px"><asp:label id="lblRef" runat="server" CssClass="tableLabel">Reference No.</asp:label></td>
														<td><asp:textbox style="TEXT-TRANSFORM: uppercase" id="txtRef" tabIndex="7" runat="server" CssClass="textField"
																MaxLength="16"></asp:textbox></td>
														<td><asp:label id="lblRecipPostCode" runat="server" CssClass="tableLabel">Recipient Postal Code</asp:label></td>
														<td><asp:textbox style="TEXT-TRANSFORM: uppercase" id="txtRecipPostCode" tabIndex="8" runat="server"
																CssClass="textField" MaxLength="12"></asp:textbox></td>
													</tr>
													<tr>
														<td style="WIDTH: 107px"><asp:label id="lblServiceType" runat="server" CssClass="tableLabel">Service Type</asp:label></td>
														<td><asp:dropdownlist id="ddlServiceType" tabIndex="9" runat="server" Width="106px" Enabled="True"></asp:dropdownlist><FONT face="Tahoma">&nbsp;</FONT></td>
														<td><asp:label id="lblDateFrom" runat="server" Width="100" CssClass="tableLabel">Date From</asp:label></td>
														<td><cc1:mstextbox id="txtDateFrom" tabIndex="10" runat="server" Width="114px" CssClass="textField"
																MaxLength="12" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;<asp:label id="lblTo" runat="server" CssClass="tableLabel">To</asp:label>&nbsp;
															<cc1:mstextbox id="txtDateTo" tabIndex="11" runat="server" Width="114px" CssClass="textField" MaxLength="12"
																TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></td>
													</tr>
												</TBODY>
											</table>
										</fieldset>
									</TD>
								</TR>
								<tr>
									<td colSpan="2"><asp:label style="Z-INDEX: 0" id="lblErrorMsg" runat="server" Width="566px" Height="19px" Font-Size="X-Small"
											Font-Bold="True" ForeColor="Red"></asp:label></td>
								</tr>
								<tr>
									<td colSpan="2"><asp:datagrid style="Z-INDEX: 0" id="dgConsignmentStatus" runat="server" SelectedItemStyle-CssClass="gridFieldSelected"
											PageSize="25" Width="1000px" AutoGenerateColumns="False" HorizontalAlign="Left" AllowPaging="True">
											<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
											<ItemStyle Height="15px" CssClass="gridField"></ItemStyle>
											<HeaderStyle Font-Bold="True" HorizontalAlign="Center" Height="40px" Width="2%" CssClass="gridHeading"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn>
													<ItemStyle HorizontalAlign="Center" Height="25px" Width="50px"></ItemStyle>
													<ItemTemplate>
														<asp:CheckBox Runat="server" ID="chkConNo" CrsID='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' Checked='<%# DataBinder.Eval(Container.DataItem,"Check").ToString().Equals("true") %>'>
														</asp:CheckBox>
													</ItemTemplate>
													<EditItemTemplate>
														<asp:Label></asp:Label>
													</EditItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Consignment No">
													<HeaderStyle Width="130px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:LinkButton ID="lnkConsignment_no" runat="server" Visible="True" CausesValidation="false" CommandName="previewConsignment_no" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>'>
														</asp:LinkButton>
														<input type="hidden" id="hProcessStep" runat="server" value='<%#DataBinder.Eval(Container.DataItem,"ProcessStep")%>'/>
														<input type="hidden" id="hBooking_No" runat="server" value='<%#DataBinder.Eval(Container.DataItem,"Booking_No")%>' NAME="hBooking_No"/>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="sender_name" HeaderText="Sender Name">
													<HeaderStyle Width="150px"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="last_status" HeaderText="Status">
													<HeaderStyle Width="100px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="ShippingList_no" HeaderText="Shipping List">
													<HeaderStyle Width="185px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="recipient_telephone" HeaderText="Recipient Phone">
													<HeaderStyle Width="100px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="recipient_name" HeaderText="Recipient Name">
													<HeaderStyle Width="130px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="recipient_zipcode" HeaderText="Postal Code">
													<HeaderStyle Width="100px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="service_code" HeaderText="Service">
													<HeaderStyle Width="70px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="pkg_qty" HeaderText="Pkgs">
													<HeaderStyle Width="50px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="ref_no" HeaderText="Ref#">
													<HeaderStyle Width="120px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="MAWBNumber" HeaderText="MAWB No.">
													<HeaderStyle Width="100px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="JobEntryNo" HeaderText="Job Entry No.">
													<HeaderStyle Width="160px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
											</Columns>
											<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
										</asp:datagrid></td>
								</tr>
							</TABLE>
						</td>
					</tr>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
