using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.ties.classes;
using com.common.applicationpages;


namespace com.ties
{
	/// <summary>
	/// Summary description for AgentVAS.
	/// </summary>
	public class AgentVAS : BasePage
	{
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected System.Web.UI.WebControls.TextBox txtCountRec;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.WebControls.Label lblAgentName;
		protected System.Web.UI.WebControls.Label lblAgentCode;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Label lblVASMessage;		
		SessionDS m_sdsAgent = null;
		SessionDS m_sdsVAS = null;
		SessionDS m_sdsZipcodeVASExcluded = null;
		protected System.Web.UI.WebControls.Button btnInsertVAS;
		//private static int m_SetSize=10;
		static private int m_iSetSize = 4;
		protected System.Web.UI.WebControls.Label lblZVASEMessage;
		protected System.Web.UI.WebControls.DataGrid dgZipcodeVASExcluded;
		protected System.Web.UI.WebControls.Button btnZVASEInsert;
		protected System.Web.UI.WebControls.Label lblEXMessage;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.WebControls.Label lblVASETitle;
		protected System.Web.UI.WebControls.RequiredFieldValidator reqAgentId;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentId;
		protected Cambro.Web.DbCombo.DbCombo dbCmbAgentName;
		protected System.Web.UI.WebControls.DataGrid dgVAS;       
		
		private void Page_Load(object sender, System.EventArgs e)
		{
			dbCmbAgentId.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			dbCmbAgentName.RegistrationKey=System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];		
		
			if(!Page.IsPostBack)
			{
				ViewState["AgentOperation"] = Operation.None;
				ViewState["AgentMode"]		= ScreenMode.None;
				ViewState["MovePrevious"]=false;
				ViewState["MoveNext"]=false;
				ViewState["MoveFirst"]=false;
				ViewState["MoveLast"]=false;
				ResetScreenForQuery();
				ResetDetailsVASGrid();
				ResetDetailsZipcodeGrid();
			}
			else
			{
				m_sdsAgent				= (SessionDS)Session["SESSION_DS3"];
				m_sdsVAS				= (SessionDS)Session["SESSION_DS1"];
				m_sdsZipcodeVASExcluded	= (SessionDS)Session["SESSION_DS2"];
				if(Session["VASZipCode"] != null)
				{
					Session["VASZipCode"]=null;
					ShowCurrentZVASEPage();
					ViewState["ZVASEMode"]		= ScreenMode.ExecuteQuery;
				}

			}
			SetAgentIDServerStates();
			SetAgentNameServerStates();
			PageValidationSummary.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnInsertVAS.Click += new System.EventHandler(this.btnInsertVAS_Click);
			this.btnZVASEInsert.Click += new System.EventHandler(this.btnZVASEInsert_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion		

		private void SetAgentNameServerStates()
		{						
			String strAgentID=dbCmbAgentId.Value;			
			Hashtable hash = new Hashtable();		
			if (strAgentID !="")
			{
				hash.Add("strAgentID", strAgentID);
			}			
			dbCmbAgentName.ServerState = hash;
			dbCmbAgentName.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";
		}

		private void SetAgentIDServerStates()
		{			
			String strAgentName=dbCmbAgentName.Value;			
			Hashtable hash = new Hashtable();			
			if (strAgentName !="")
			{
				hash.Add("strAgentName", strAgentName);
			}						

			dbCmbAgentId.ServerState = hash;
			dbCmbAgentId.ServerStateSecretString = "kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4";			
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			ResetScreenForQuery();
		}
		
		private void InsertAgent(int iRowIndex)
		{
			m_sdsAgent = AgentProfileMgrDAL.GetEmptyAgentDS(1);		
			DataRow drEach =  m_sdsAgent.ds.Tables[0].Rows[0];	
			drEach["agentid"]	= dbCmbAgentId.Text; 
			drEach["agent_name"]= dbCmbAgentName.Text;
			Session["DS3"]= m_sdsAgent;
		}
		private void GetAgentRecSet()
		{
			int iStartIndex = Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsAgent = AgentProfileMgrDAL.GetAgentDS(utility.GetAppID(),utility.GetEnterpriseID(),iStartIndex,m_iSetSize,dsQuery);
			Session["SESSION_DS3"] = m_sdsAgent;
			ViewState["QryRes"] =  m_sdsAgent.QueryResultMaxSize; 			
			int pgCnt = (Convert.ToInt32(m_sdsAgent.QueryResultMaxSize) - 1)/m_iSetSize;
			if(pgCnt < Convert.ToDecimal(ViewState["currentSet"]))
			{
				ViewState["currentSet"] = System.Convert.ToInt32(pgCnt);
			}
		}
		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			
			InsertAgent(0);
			ViewState["QUERY_DS"] = m_sdsAgent.ds;
			ViewState["currentPage"] = 0;
			ViewState["currentSet"] = 0;
			GetAgentRecSet();			
			if ((decimal)ViewState["QryRes"]!=0)
			{
				DisplayCurrentPage();
				//VAS
				ResetDetailsVASGridData();
				FillVASDataRow(dgVAS.Items[0],0);
				ViewState["QUERY_DSV"] = m_sdsVAS.ds;
				dgVAS.CurrentPageIndex = 0;
				ShowCurrentVASPage();
				btnExecQry.Enabled = false;
				//		Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				lblVASMessage.Text = "";
				ViewState["VASMode"] = ScreenMode.ExecuteQuery;
				ViewState["AgentMode"] = ScreenMode.ExecuteQuery;
				btnGoToFirstPage.Enabled = true;
				btnGoToLastPage.Enabled=true;
				btnPreviousPage.Enabled =true;
				btnNextPage.Enabled=true; 
				btnInsertVAS.Enabled=true;
				EnablingDBCombo(false);
			}
			else
			{
				lblVASMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			}
		}   
		private void DisplayCurrentPage()
		{
			DataRow drCurrent = m_sdsAgent.ds.Tables[0].Rows[Convert.ToInt32(ViewState["currentPage"])];
						
			if(!drCurrent["agentid"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dbCmbAgentId.Text		= drCurrent["agentid"].ToString();
				dbCmbAgentId.Value		= drCurrent["agentid"].ToString();
				ViewState["AgentID"]	= drCurrent["agentid"].ToString();
			}
			if(!drCurrent["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dbCmbAgentName.Text		= drCurrent["agent_name"].ToString();
				dbCmbAgentName.Value	= drCurrent["agent_name"].ToString();
			}
		}
		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			btnExecQry.Enabled = false;
			lblVASMessage.Text = "";
		}
	
		private void btnNextPage_Click(object sender, System.EventArgs e)
		{
			btnInsertVAS.Enabled=true;
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  < (m_sdsAgent.DataSetRecSize - 1) )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) + 1;
				DisplayCurrentPage();
				ShowCurrentVASPage();
			}
			else
			{
				decimal iTotalRec = (Convert.ToInt32(ViewState["currentSet"]) * m_iSetSize) + m_sdsAgent.DataSetRecSize;
				if( iTotalRec ==  m_sdsAgent.QueryResultMaxSize)
				{
					EnableNavigationButtons(true,true,false,false);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) + 1;	
				ViewState["currentPage"] = 0;
				DisplayCurrentPage();
				ShowCurrentVASPage();
			}
			EnableNavigationButtons(true,true,true,true);
			SetAgentIDServerStates();
			SetAgentNameServerStates();
			EnablingDBCombo(false);
		}
		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			btnInsertVAS.Enabled=true;
			ViewState["currentSet"] = Convert.ToInt32((m_sdsAgent.QueryResultMaxSize - 1))/m_iSetSize;	
			GetAgentRecSet();
			ViewState["currentPage"] = m_sdsAgent.DataSetRecSize - 1;
			DisplayCurrentPage();
			ShowCurrentVASPage();
			EnableNavigationButtons(true,true,false,false);
			SetAgentIDServerStates();
			SetAgentNameServerStates();
			EnablingDBCombo(false);
		}

		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			btnInsertVAS.Enabled=true;
			int iCurrentPage = Convert.ToInt32(ViewState["currentPage"]);

			if(Convert.ToInt32(ViewState["currentPage"])  > 0 )
			{
				ViewState["currentPage"] = Convert.ToInt32(ViewState["currentPage"]) - 1;
				DisplayCurrentPage();	
				ShowCurrentVASPage();
			}
			else
			{
				if( (Convert.ToInt32(ViewState["currentSet"]) - 1) < 0)
				{
					EnableNavigationButtons(false,false,true,true);
					return;
				}
				
				ViewState["currentSet"] = Convert.ToInt32(ViewState["currentSet"]) - 1;	
				GetAgentRecSet();
				ViewState["currentPage"] = m_sdsAgent.DataSetRecSize - 1;
				DisplayCurrentPage();
				ShowCurrentVASPage();
			}
			EnableNavigationButtons(true,true,true,true);
			SetAgentIDServerStates();
			SetAgentNameServerStates();
			EnablingDBCombo(false);
		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			btnInsertVAS.Enabled=true;
			ViewState["currentSet"] = 0;	
			GetAgentRecSet();
			ViewState["currentPage"] = 0;
			DisplayCurrentPage();
			ShowCurrentVASPage();
			EnableNavigationButtons(false,false,true,true);			
			SetAgentIDServerStates();
			SetAgentNameServerStates();
			EnablingDBCombo(false);

		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			ResetScreenForQuery();			
			ResetDetailsVASGrid();
			ResetDetailsVASGridData();
			ResetDetailsZipcodeGrid();
			btnInsertVAS.Enabled=true;

		}

		private void EnableNavigationButtons(bool bMoveFirst, bool bMoveNext,bool bEnableMovePrevious, bool bMoveLast)
		{
			btnGoToFirstPage.Enabled	= bMoveFirst;
			btnPreviousPage.Enabled		= bMoveNext;
			btnNextPage.Enabled			= bEnableMovePrevious;
			btnGoToLastPage.Enabled		= bMoveLast;
		}

		private void ResetScreenForQuery()
		{
			
			lblVASMessage.Text = "";
			dbCmbAgentId.Text="";
			dbCmbAgentName.Text="";
			dbCmbAgentId.Value="";
			dbCmbAgentName.Value="";
			btnExecQry.Enabled=true;
			m_sdsAgent = AgentProfileMgrDAL.GetEmptyAgentDS(1);
			Session["SESSION_DS3"] = m_sdsAgent;		
			ViewState["AgentOperation"] = Operation.None;
			ViewState["AgentMode"]		= ScreenMode.Query;
			ViewState["VASOperation"]	= Operation.None;
			ViewState["VASMode"]		= ScreenMode.Query;
			ViewState["ZVASEMode"]		= ScreenMode.None;
			ViewState["ZVASEOperation"] = Operation.None;
			EnableNavigationButtons(false,false,false,false);
			m_sdsZipcodeVASExcluded = AgentProfileMgrDAL.GetEmptyZipcodeServiceExcludedDS(1);
			Session["SESSION_DS2"]	= m_sdsZipcodeVASExcluded;
			//	Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);			
			EnablingDBCombo(true);
			EnableNavigationButtons(false,false,false,false);
			ResetDetailsZipcodeGrid();	
		
		}
		private void EnablingDBCombo(bool val)
		{
			dbCmbAgentId.Enabled=val;
			dbCmbAgentName.Enabled=val;
		}
		[Cambro.Web.DbCombo.ResultsMethod(true)]
		public static object AgentNameServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
	
			String strAgentID= "";

			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{				
				if (args.ServerState["strAgentID"] != null && args.ServerState["strAgentID"].ToString().Length > 0 )
				{
					strAgentID = (args.ServerState["strAgentID"].ToString());
				}
			}	

			DataSet dataset = DbComboDAL.PayerNameQuery(strAppID,strEnterpriseID,args, "A", strAgentID);	
			return dataset;                
		}
		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]		
		public static object AgentIdServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
						
			String strAgentName="";
			if(args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
			{			
				if(args.ServerState["strAgentName"] != null && args.ServerState["strAgentName"].ToString().Length > 0)
				{
					strAgentName = args.ServerState["strAgentName"].ToString();					
				}	
			}
			
			DataSet dataset = DbComboDAL.PayerIDQuery(strAppID,strEnterpriseID,args, "A", strAgentName);	
			return dataset;
		}

		#region Code for VAS
		/// <summary>
		/// VAS
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnInsertVAS_Click(object sender, System.EventArgs e)
		{
			btnInsertVAS.Enabled=false;
			if( m_sdsVAS.ds.Tables[0].Rows.Count >= dgVAS.PageSize)
			{
				ResetDetailsVASGridData();
			}

			ViewState["VASMode"] = ScreenMode.Insert;
			ViewState["VASOperation"] = Operation.Insert;
			AddRowInVASGrid();
			dgVAS.EditItemIndex = m_sdsVAS.ds.Tables[0].Rows.Count - 1;
			BindVASGrid();
			getPageControls(Page);
		}
		
		public void dgVAS_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Label lblVASCode = (Label)dgVAS.SelectedItem.FindControl("lblVASCode");
			msTextBox txtVASCode = (msTextBox)dgVAS.SelectedItem.FindControl("txtVASCode");
			String strVASCode = null;

			if(lblVASCode != null)
			{
				strVASCode = lblVASCode.Text;
			}

			if(txtVASCode != null)
			{
				strVASCode = txtVASCode.Text;
			}
			if((int)ViewState["VASMode"] != (int)ScreenMode.Insert || (int)ViewState["VASOperation"] != (int)Operation.Insert)
			{
			
				ViewState["CurrentVAS"] = strVASCode;
				dgZipcodeVASExcluded.CurrentPageIndex = 0;

				Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_SelectedIndexChanged","INF004","updating data grid..."+dgVAS.SelectedIndex+"  : "+strVASCode);
				ShowCurrentZVASEPage();

				ViewState["ZVASEMode"] = ScreenMode.ExecuteQuery;

				if((int)ViewState["VASMode"] != (int)ScreenMode.Insert && (int)ViewState["VASOperation"] != (int)Operation.Insert)
				{
					//	Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				}

				if((int)ViewState["VASMode"] == (int)ScreenMode.Insert && dgVAS.EditItemIndex == dgVAS.SelectedIndex && (int)ViewState["VASOperation"] == (int)Operation.Insert)
				{
					btnZVASEInsert.Enabled = false;
				}
				else
				{
					btnZVASEInsert.Enabled = true;
				}
			}
			lblVASMessage.Text	= "";
			lblEXMessage.Text	= "";
		}

		protected void dgVAS_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int)ViewState["VASMode"] == (int)ScreenMode.Insert && (int) ViewState["VASOperation"] == (int)Operation.Insert && dgVAS.EditItemIndex > 0)
			{
				m_sdsVAS.ds.Tables[0].Rows.RemoveAt(dgVAS.EditItemIndex);
				m_sdsVAS.QueryResultMaxSize--;
				dgVAS.CurrentPageIndex = 0;
			}
			dgVAS.EditItemIndex = e.Item.ItemIndex;
			ViewState["VASOperation"] = Operation.Update;
			BindVASGrid();
			lblVASMessage.Text = "";
			Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_Edit","INF004","updating data grid...");			
		}
		
		public void dgVAS_Update(object sender, DataGridCommandEventArgs e)
		{
			//String strAgentID=(String)ViewState["AgentID"];
			String strAgentID = dbCmbAgentId.Value;
			FillVASDataRow(e.Item,e.Item.ItemIndex);
			int iOperation = (int)ViewState["VASOperation"];
			switch(iOperation)
			{

				case (int)Operation.Update:

					DataSet dsToUpdate = m_sdsVAS.ds.GetChanges();
					AgentProfileMgrDAL.ModifyAgentVASCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,dsToUpdate);
					lblVASMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
					//	Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
					m_sdsVAS.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_OnUpdate","INF004","update in modify mode..");
					btnInsertVAS.Enabled=true;
					break;
				case (int)Operation.Insert:
					DataSet dsToInsert = m_sdsVAS.ds.GetChanges();
					try
					{
						AgentProfileMgrDAL.AddAgentVASCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,dsToInsert);
						lblVASMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						//		Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
						btnInsertVAS.Enabled=true;									
									
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblVASMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_VAS",utility.GetUserCulture());
							return;
						}
						if(strMsg.IndexOf("unique constraint ") != -1 )
						{
							lblVASMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_VAS",utility.GetUserCulture());
							return;
						}
						
					}
					m_sdsVAS.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_OnUpdate","INF004","update in Insert mode");
					break;
			}
			dgVAS.EditItemIndex = -1;
			ViewState["VASOperation"] = Operation.None;
			lblZVASEMessage.Text="";
			BindVASGrid();
		}

		protected void dgVAS_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgVAS.EditItemIndex = -1;

			if((int)ViewState["VASMode"] == (int)ScreenMode.Insert && (int)ViewState["VASOperation"] == (int)Operation.Insert)
			{
				m_sdsVAS.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsVAS.QueryResultMaxSize--;
				dgVAS.CurrentPageIndex = 0;
			}
			ViewState["VASOperation"] = Operation.None;
			BindVASGrid();
			//		Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			lblVASMessage.Text = "";
			Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_Cancel","INF004","updating data grid...");			
		}

		public void dgVAS_Delete(object sender, DataGridCommandEventArgs e)
		{
			String strAgentID=(String)ViewState["AgentID"];
			msTextBox txtVASCode = (msTextBox)e.Item.FindControl("txtVASCode");
			Label lblVASCode = (Label)e.Item.FindControl("lblVASCode");
			String strVASCode = null;
			if(txtVASCode != null)
			{
				strVASCode = txtVASCode.Text;
			}

			if(lblVASCode != null)
			{
				strVASCode = lblVASCode.Text;
			}

			try
			{
				AgentProfileMgrDAL.DeleteAgentVASCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,strVASCode);
				lblVASMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblVASMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_VAS_TRANS",utility.GetUserCulture());
				}
				if(strMsg.IndexOf("child record") != -1)
				{
					lblVASMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_VAS_TRANS",utility.GetUserCulture());
				}
				return;

			}

			if((int)ViewState["VASMode"] == (int)ScreenMode.Insert)
			{
				m_sdsVAS.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsVAS.QueryResultMaxSize--;
				dgVAS.CurrentPageIndex = 0;

				if((int) ViewState["VASOperation"] == (int)Operation.Insert && dgVAS.EditItemIndex > 0)
				{
					m_sdsVAS.ds.Tables[0].Rows.RemoveAt(dgVAS.EditItemIndex-1);
					m_sdsVAS.QueryResultMaxSize--;
				}
				dgVAS.EditItemIndex = -1;
				dgVAS.SelectedIndex = -1;
				BindVASGrid();
				ResetDetailsZipcodeGrid();
			}
			else
			{
				ShowCurrentVASPage();
			}
			Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_Delete","INF004","Deleted row in VAS_Code table..");
			ViewState["VASOperation"] = Operation.None;
			lblEXMessage.Text="";
			//	Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
		}

		private void BindVASGrid()
		{
			dgVAS.VirtualItemCount = System.Convert.ToInt32(m_sdsVAS.QueryResultMaxSize);
			dgVAS.DataSource = m_sdsVAS.ds;
			dgVAS.DataBind();
			Session["SESSION_DS1"] = m_sdsVAS;
		}

		private void AddRowInVASGrid()
		{
			AgentProfileMgrDAL.AddNewRowInAgentVASCodeDS(m_sdsVAS);
			Session["SESSION_DS1"] = m_sdsVAS;
		}

		protected void dgVAS_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsVAS.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			msTextBox txtVASCode = (msTextBox)e.Item.FindControl("txtVASCode");


			int iOperation = (int) ViewState["VASOperation"];
			if(txtVASCode != null && iOperation == (int)Operation.Update)
			{
				txtVASCode.Enabled = false;
				e.Item.Cells[3].Enabled=false;
			}

			SetAgentIDServerStates();
			SetAgentNameServerStates();

		}

		protected void dgVAS_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgVAS.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentVASPage();
			Logger.LogDebugInfo("VASZipcodeExcluded","dgVAS_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}
		
		public void dgVAS_Button(object sender, DataGridCommandEventArgs e)
		{
			
			String strCmdNm = e.CommandName;
			if(strCmdNm.Equals("search"))
			{
				int iOperation = (int) ViewState["VASOperation"];
				if(dgVAS.EditItemIndex  != e.Item.ItemIndex)
				{
					return;
				}
				if(iOperation == (int)Operation.Update)
				{
					return;
				}
				msTextBox txtVASCode = (msTextBox)e.Item.FindControl("txtVASCode");
				TextBox txtVASDescription = (TextBox)e.Item.FindControl("txtVASDescription");
				msTextBox txtSurcharge = (msTextBox)e.Item.FindControl("txtSurcharge");
				
				String strVASClientID = null;
				String strVASClientDesc = null;
				String strVASDescriptionClientID = null;
				String strSurchargeClientID = null;

				String strVASCode = null;
			
				if(txtVASCode != null)
				{
					strVASClientID = txtVASCode.ClientID;
					strVASCode = txtVASCode.Text;
				}

				if(txtVASDescription != null)
				{
					strVASDescriptionClientID = txtVASDescription.ClientID;
					strVASClientDesc=txtVASDescription.Text;
				}			
				if(txtSurcharge != null)
				{
					strSurchargeClientID = txtSurcharge.ClientID;
				}
				//String sUrl = "ZonePopup.aspx?FORMID=AgentVAS&ZONECODE_TEXT="+strVASCode+"&ZONECODE="+strVASClientID;
				String sUrl = "VASPopup.aspx?FORMID=AgentVAS&VASID="+strVASClientID+"&VASDESC="+strVASDescriptionClientID+"&VASSURCHARGE="+strSurchargeClientID+"&VASCODE="+strVASCode+"&VASDESCRIPTION="+strVASClientDesc;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);	
			}
		
		}
		private void ShowCurrentVASPage()
		{
			String strAgentID=(String)ViewState["AgentID"];
			int iStartIndex = dgVAS.CurrentPageIndex * dgVAS.PageSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DSV"];
			m_sdsVAS = AgentProfileMgrDAL.GetAgentVASCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,iStartIndex,dgVAS.PageSize,dsQuery);
			int pgCnt = (Convert.ToInt32(m_sdsVAS.QueryResultMaxSize) - 1)/dgVAS.PageSize;
			if(pgCnt < dgVAS.CurrentPageIndex)
			{
					dgVAS.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgVAS.SelectedIndex = -1;
			dgVAS.EditItemIndex = -1;
			lblVASMessage.Text	= "";
			lblEXMessage.Text	= "";
			BindVASGrid();
			ResetDetailsZipcodeGrid();
		}

		private void FillVASDataRow(DataGridItem item, int drIndex)
		{

			msTextBox txtVASCode = (msTextBox)item.FindControl("txtVASCode");
			if(txtVASCode != null)
			{
				DataRow drCurrent = m_sdsVAS.ds.Tables[0].Rows[drIndex];
				drCurrent["vas_code"] = txtVASCode.Text;
			}

			TextBox txtVASDescription = (TextBox)item.FindControl("txtVASDescription");
			if(txtVASDescription != null)
			{
				DataRow drCurrent = m_sdsVAS.ds.Tables[0].Rows[drIndex];
				drCurrent["vas_description"] = txtVASDescription.Text;
			}

			msTextBox txtSurcharge = (msTextBox)item.FindControl("txtSurcharge");
			if(txtSurcharge != null )
			{
				DataRow drCurrent = m_sdsVAS.ds.Tables[0].Rows[drIndex];
				if(txtSurcharge.Text == "")
				{
					drCurrent["surcharge"] = System.DBNull.Value;
				}
				else
				{
					
					drCurrent["surcharge"] = txtSurcharge.Text;
				}
			}

		}

		private void ResetDetailsVASGrid()
		{
			m_sdsVAS = AgentProfileMgrDAL.GetEmptyVASCodeDS(0);			// 11/11/2002 
			Session["SESSION_DS1"]	= m_sdsVAS;		
			dgVAS.EditItemIndex = 0;
			dgVAS.CurrentPageIndex = 0;
			BindVASGrid();
		}
		private void ResetDetailsVASGridData()
		{
			m_sdsVAS = AgentProfileMgrDAL.GetEmptyVASCodeDS(1);			// 11/11/2002 
			Session["SESSION_DS1"]	= m_sdsVAS;		
			dgVAS.EditItemIndex = 0;
			dgVAS.CurrentPageIndex = 0;
			BindVASGrid();
		}

		#endregion
		#region Code for VAS excluded zipcode
		/// ZVASE Grid Methos..
		/// 
	
		private void btnZVASEInsert_Click(object sender, System.EventArgs e)
		{
			lblVASMessage.Text="";
			lblZVASEMessage.Text="";
			lblEXMessage.Text="";
			String strAgentID	= (String)ViewState["AgentID"];
			String strVASCode	= (String)ViewState["CurrentVAS"];				
			String sUrl = "AgentZipCodePopup.aspx?FORMID=AgentVAS&VASCODE="+strVASCode+"&AGENTID="+strAgentID;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		
		}

		protected void dgZipcodeVASExcluded_Edit(object sender, DataGridCommandEventArgs e)
		{
			dgZipcodeVASExcluded.EditItemIndex = e.Item.ItemIndex;
			ViewState["ZVASEOperation"] = Operation.Update;
			BindZVASEGrid();
			lblZVASEMessage.Text = "";
			Logger.LogDebugInfo("VASZipcodeExcluded","dgZipcodeVASExcluded_Edit","INF004","updating data grid...");			
		}

		public void dgZipcodeVASExcluded_Update(object sender, DataGridCommandEventArgs e)
		{
			FillZVASEDataRow(e.Item,e.Item.ItemIndex);

			int iOperation = (int)ViewState["ZVASEOperation"];

			String strVASCode = (String)ViewState["CurrentVAS"];
			switch(iOperation)
			{

				case (int)Operation.Insert:

					DataSet dsToInsert = m_sdsZipcodeVASExcluded.ds.GetChanges();
					try
					{
						SysDataMgrDAL.AddZipcodeVASExcludedDS(utility.GetAppID(),utility.GetEnterpriseID(),strVASCode,dsToInsert);
						lblZVASEMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						btnZVASEInsert.Enabled = true;
						
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblZVASEMessage.Text = "Duplicate Zipcode Code is not allowed.";
						} 
						else if(strMsg.IndexOf("FOREIGN KEY") != -1 || strMsg.IndexOf("parent key") != -1)
						{
							lblZVASEMessage.Text = "Zipcode not found. Please create Zipcode in Zipcode Master.";
						}
						else if(strMsg.IndexOf("unique") != -1 )
						{
							lblZVASEMessage.Text = "Duplicate Zipcode Code is not allowed.";
						} 
						else if(strMsg.IndexOf("APPDB.SYS_C002182") != -1 )
						{
							//violated - parent key not found //Zipcode not present in parent
							lblZVASEMessage.Text = "Zipcode not found. Please create Zipcode in Zipcode Master.";
						}
						else
						{
							lblZVASEMessage.Text = strMsg;
						}
						return;
						
					}

					m_sdsZipcodeVASExcluded.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
					Logger.LogDebugInfo("VASZipcodeExcluded","dgZipcodeVASExcluded_OnUpdate","INF004","update in Insert mode");
					break;

			}
			dgZipcodeVASExcluded.EditItemIndex = -1;
			ViewState["ZVASEOperation"] = Operation.None;
			BindZVASEGrid();		
			lblVASMessage.Text = "";
		}

		protected void dgZipcodeVASExcluded_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgZipcodeVASExcluded.EditItemIndex = -1;
			if((int)ViewState["ZVASEMode"] == (int)ScreenMode.Insert && (int)ViewState["ZVASEOperation"] == (int)Operation.Insert)
			{
				m_sdsZipcodeVASExcluded.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsZipcodeVASExcluded.QueryResultMaxSize--;
				dgZipcodeVASExcluded.CurrentPageIndex = 0;
			}

			btnZVASEInsert.Enabled = true;
		
			lblZVASEMessage.Text = "";

			ViewState["ZVASEOperation"] = Operation.None;
			BindZVASEGrid();
			Logger.LogDebugInfo("VASZipcodeExcluded","dgZipcodeVASExcluded_Cancel","INF004","updating data grid...");			
		}

		protected void dgZipcodeVASExcluded_Delete(object sender, DataGridCommandEventArgs e)
		{
			String strVASCode = (String)ViewState["CurrentVAS"];
			String strAgentID	= (String)ViewState["AgentID"];
			msTextBox txtZipcode = (msTextBox)e.Item.FindControl("txtZipcode");
			Label lblZipcode = (Label)e.Item.FindControl("lblZipcode");
			String strZipcode = null;
			if(txtZipcode != null)
			{
				strZipcode = txtZipcode.Text;
			}

			if(lblZipcode != null)
			{
				strZipcode = lblZipcode.Text;
			}			
			try
			{
				AgentProfileMgrDAL.DeleteAgentZipcodeVASExcludedDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,strVASCode,strZipcode);				
				lblEXMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());

			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblEXMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
				}
				if(strMsg.IndexOf("child record") != -1)
				{
					lblEXMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
				}
				return;

			}
			if((int)ViewState["ZVASEMode"] == (int)ScreenMode.Insert)
			{
				m_sdsZipcodeVASExcluded.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsZipcodeVASExcluded.QueryResultMaxSize--;
				dgZipcodeVASExcluded.CurrentPageIndex = 0;

				if((int) ViewState["ZVASEOperation"] == (int)Operation.Insert && dgZipcodeVASExcluded.EditItemIndex > 0)
				{
					m_sdsZipcodeVASExcluded.ds.Tables[0].Rows.RemoveAt(dgZipcodeVASExcluded.EditItemIndex-1);
					m_sdsZipcodeVASExcluded.QueryResultMaxSize--;
				}
				dgZipcodeVASExcluded.EditItemIndex = -1;
				BindZVASEGrid();
			}
			else
			{
				m_sdsZipcodeVASExcluded.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				BindZVASEGrid();				
			}
			ShowCurrentZVASEPage();

			Logger.LogDebugInfo("VASZipcodeExcluded","dgZipcodeVASExcluded_Delete","INF004","Deleted row in Zipcode_VAS_Excluded table..");

			ViewState["ZVASEOperation"] = Operation.None;
			btnZVASEInsert.Enabled = true;
			lblVASMessage.Text = "";
		}

		private void BindZVASEGrid()
		{
			dgZipcodeVASExcluded.VirtualItemCount = System.Convert.ToInt32(m_sdsZipcodeVASExcluded.QueryResultMaxSize);
			dgZipcodeVASExcluded.DataSource = m_sdsZipcodeVASExcluded.ds;
			dgZipcodeVASExcluded.DataBind();
			Session["SESSION_DS2"] = m_sdsZipcodeVASExcluded;
		}

		private void AddRowInZVASEGrid()
		{
			SysDataMgrDAL.AddNewRowInZipcodeVASExcludedDS(m_sdsZipcodeVASExcluded);
			Session["SESSION_DS2"] = m_sdsZipcodeVASExcluded;
		}

		public void dgZipcodeVASExcluded_Button(object sender, DataGridCommandEventArgs e)
		{
			String strCmdNm = e.CommandName;
			if(strCmdNm.Equals("search"))
			{
				TextBox txtZipcode = (TextBox)e.Item.FindControl("txtZipcode");
				TextBox txtState = (TextBox)e.Item.FindControl("txtState");
				TextBox txtCountry = (TextBox)e.Item.FindControl("txtCountry");

				String strZipcodeClientID = null;
				String strStateClientID = null;
				String strCountryClientID = null;

				String strZipcode = null;
				
				if(txtZipcode != null)
				{
					strZipcodeClientID = txtZipcode.ClientID;
					strZipcode = txtZipcode.Text;
				}

				if(txtState != null)
				{
					strStateClientID = txtState.ClientID;
				}
				
				if(txtCountry != null)
				{
					strCountryClientID = txtCountry.ClientID;
				}



				String sUrl = "ZipcodePopup.aspx?FORMID=VASZipcodeExcluded&ZIPCODE="+strZipcode+"&ZIPCODE_CID="+strZipcodeClientID+"&STATE_CID="+strStateClientID+"&COUNTRY_CID="+strCountryClientID;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);	
			}
			
		}

		protected void dgZipcodeVASExcluded_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsZipcodeVASExcluded.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}
			msTextBox txtZipcode = (msTextBox)e.Item.FindControl("txtZipcode");
			
			int iOperation = (int) ViewState["ZVASEOperation"];
			if(txtZipcode != null && iOperation == (int)Operation.Update)
			{
				txtZipcode.Enabled = true;
			}

			if(e.Item.ItemType == ListItemType.EditItem)
			{
				e.Item.Cells[0].Enabled = true;
				e.Item.Cells[3].Enabled = true;
			}
			else
			{
				e.Item.Cells[0].Enabled = true;
				e.Item.Cells[3].Enabled = true;
			}
		}

		protected void dgZipcodeVASExcluded_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgZipcodeVASExcluded.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentZVASEPage();
			Logger.LogDebugInfo("VASZipcodeExcluded","dgZipcodeVASExcluded_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		private void ShowCurrentZVASEPage()
		{
			int iStartIndex = dgZipcodeVASExcluded.CurrentPageIndex * dgZipcodeVASExcluded.PageSize;
			String strVASCode = (String)ViewState["CurrentVAS"];
			String strAgentID	= (String)ViewState["AgentID"];
			m_sdsZipcodeVASExcluded = AgentProfileMgrDAL.GetAgentZipcodeVASExcludedDS(utility.GetAppID(),utility.GetEnterpriseID(),strAgentID,strVASCode,iStartIndex,dgZipcodeVASExcluded.PageSize);

			int pgCnt = (Convert.ToInt32(m_sdsZipcodeVASExcluded.QueryResultMaxSize) - 1)/dgZipcodeVASExcluded.PageSize;
			if(pgCnt < dgZipcodeVASExcluded.CurrentPageIndex)
			{
					dgZipcodeVASExcluded.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgZipcodeVASExcluded.EditItemIndex = -1;
			BindZVASEGrid();
			lblZVASEMessage.Text = "";
		}
		
		private void FillZVASEDataRow(DataGridItem item, int drIndex)
		{
			msTextBox txtZipcode = (msTextBox)item.FindControl("txtZipcode");
			if(txtZipcode != null)
			{
				DataRow drCurrent = m_sdsZipcodeVASExcluded.ds.Tables[0].Rows[drIndex];
				drCurrent["zipcode"] = txtZipcode.Text;
			}

			TextBox txtState = (TextBox)item.FindControl("txtState");
			if(txtState != null)
			{
				DataRow drCurrent = m_sdsZipcodeVASExcluded.ds.Tables[0].Rows[drIndex];
				drCurrent["state_name"] = txtState.Text;
			}

			TextBox txtCountry = (TextBox)item.FindControl("txtCountry");
			if(txtCountry != null)
			{
				DataRow drCurrent = m_sdsZipcodeVASExcluded.ds.Tables[0].Rows[drIndex];
				drCurrent["country"] = txtCountry.Text;
			}

		}

		private void ResetDetailsZipcodeGrid()
		{
			dgZipcodeVASExcluded.CurrentPageIndex = 0;
			btnZVASEInsert.Enabled = false;			
			m_sdsZipcodeVASExcluded = SysDataMgrDAL.GetEmptyZipcodeVASExcludedDS(0);
			Session["SESSION_DS2"] = m_sdsZipcodeVASExcluded;
			lblZVASEMessage.Text = "";
			BindZVASEGrid();
		}

		private void dgZipcodeVASExcluded_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		#endregion

		private void dbCmbAgentId_SelectedItemChanged(object sender, System.EventArgs e)
		{
			SetAgentNameServerStates();
		}

		private void dbCmbAgentName_SelectedItemChanged(object sender, System.EventArgs e)
		{
			SetAgentIDServerStates();
		}
	}
}
