using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.ties.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for VersusReport_New.
	/// </summary>
	public class VersusReportDate : BasePage
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.RadioButton rbLastStatusDate;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtYear;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.WebControls.Label lblRoute;
		protected System.Web.UI.WebControls.RadioButton rbPickUp;
		protected System.Web.UI.WebControls.RadioButton rbDelivery;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected System.Web.UI.WebControls.DropDownList cboRouteCode;
		protected System.Web.UI.WebControls.Label lblServiceType;
		protected System.Web.UI.WebControls.ListBox lsbServiceType;
		protected System.Web.UI.WebControls.Label lblExceptionSelection;
		protected System.Web.UI.HtmlControls.HtmlTable Table2;
		protected System.Web.UI.WebControls.DropDownList cboOrigin;
		protected System.Web.UI.WebControls.Label lblDestinationDC;
		protected System.Web.UI.WebControls.DropDownList cboDestinationDC;
		protected System.Web.UI.WebControls.RadioButton rbHasAllStatus;
		protected System.Web.UI.WebControls.ListBox lsbHasAllStatus_Left;
		protected System.Web.UI.WebControls.ListBox lsbHasAllStatus_Right;
		protected System.Web.UI.WebControls.ListBox lsbHasSelectStatus_Left;
		protected System.Web.UI.WebControls.Button btnToRight;
		protected System.Web.UI.WebControls.Button btnToLeft;
		protected System.Web.UI.WebControls.Button btnToUp;
		protected System.Web.UI.WebControls.Button btnToDown;
		protected System.Web.UI.WebControls.ListBox lsbHasSelectStatus_Right;
		protected System.Web.UI.WebControls.ListBox lsbMissingStatus;
		protected System.Web.UI.WebControls.RadioButton rbHasSelectStatus;
		protected System.Web.UI.WebControls.RadioButton rbMissingStatus;
		protected System.Web.UI.WebControls.Label lblOriginDC;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
	
		string m_strAppID=null;
		string m_strEnterpriseID=null;
		string m_strCulture=null;
		string m_strUserRole=null;
		string m_strUserId=null;
		private DataView m_dvMonths;

		private void Page_Load(object sender, System.EventArgs e)
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			m_strUserId = utility.GetUserID();
			m_strUserRole = Session["UserRole"].ToString();

			// Put user code to initialize the page here
			if(!Page.IsPostBack)
			{
				BindRouteCode();
				m_dvMonths = CreateMonths(true);
				BindMonths(m_dvMonths);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ID = "VersusReportDate";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void BindRouteCode()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();				
			String strWhereClause=null;			
			string strDelType_S = "S";
			string strDelType_W = "W";
			strWhereClause=" and Delivery_Type in ('"+Utility.ReplaceSingleQuote(strDelType_S)+"', '"+Utility.ReplaceSingleQuote(strDelType_W)+"')";
			DataSet dataset = ShipmentTrackingMgrDAL.GetRouteCode(strAppID,strEnterpriseID);
			
			cboRouteCode.DataSource = dataset;
			cboRouteCode.DataTextField = "ComboText";
			cboRouteCode.DataValueField = "ComboText";
			cboRouteCode.DataBind();

		}


		private DataView CreateMonths(bool showNilOption)
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			if(showNilOption)
			{
				DataRow drNilRow = dtMonths.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtMonths.Rows.Add(drNilRow);
			}
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);
			return dvMonths;
		}
		private void BindMonths(DataView dvMonths)
		{
			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}

	}
}
