<%@ Page language="c#" Codebehind="ConsignmentsWithoutPkgWeights.aspx.cs" SmartNavigation="True" 
	AutoEventWireup="false" Inherits="TIES.WebUI.ConsignmentsWithoutPkgWeights" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Consignments Without Package Weights</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" src="Scripts/WorkProgress.js"></script>
		<script language="javascript">
			function SetInitialFocus(name)
			{			
				var contrl = document.getElementById(name);
				contrl.focus();
			}
			
			function UpperMask( toField )
			{
				var lcNewChar = String.fromCharCode(window.event.keyCode);
				var llRetVal = true;
				window.event.keyCode = lcNewChar.toUpperCase().charCodeAt(0);
				return llRetVal;
			}
			
			function Confirm_Print() {
				var confirm_value = document.createElement("INPUT");
				confirm_value.type = "hidden";
				confirm_value.name = "confirm_value";
				if (confirm("This manifest may contain information that is not up to date.\nDo you wish to proceed?")) {
					confirm_value.value = "Yes";
				} else {
					confirm_value.value = "No";
				}
				document.forms[0].appendChild(confirm_value);
				document.getElementById("btnHidReport").click();
			}
		
		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="ConsignmentsWithoutPkgWeights" method="post" runat="server">
			<INPUT style="Z-INDEX: 101; POSITION: absolute; TOP: 680px; LEFT: 16px" type="hidden" name="ScrollPosition">
			<asp:validationsummary style="Z-INDEX: 105; POSITION: absolute; COLOR: red; TOP: 672px; LEFT: 240px" id="Validationsummary1"
				ShowSummary="False" HeaderText="Please enter the missing  fields." ShowMessageBox="True" DisplayMode="BulletList"
				Runat="server"></asp:validationsummary>
			<div style="Z-INDEX: 102; POSITION: relative; HEIGHT: 850px; TOP: 16px; LEFT: 14px" id="divMain"
				MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tr>
						<td colSpan="2" align="left"><asp:label id="Label1" runat="server" Width="421px" Height="27px" CssClass="mainTitleSize">
								Consignments Without Package Weights</asp:label></td>
					</tr>
					<tr>
						<td style="HEIGHT: 24px" vAlign="top" colSpan="2" align="left"><asp:button id="btnQry" runat="server" Width="61px" CssClass="queryButton" Text="Query" CausesValidation="False"></asp:button><asp:button id="btnExecQry" runat="server" Width="130px" CssClass="queryButton" Text="Execute Query"
								CausesValidation="False"></asp:button><asp:button style="DISPLAY: none; VISIBILITY: hidden" id="btnDelete" runat="server" Width="62px"
								CssClass="queryButton" Text="Delete" CausesValidation="False"></asp:button></td>
					</tr>
					<tr>
						<td colSpan="2"><asp:label id="lblErrorMsg" runat="server" Width="566px" Height="19px" CssClass="errorMsgColor"></asp:label></td>
					</tr>
					<tr>
						<td vAlign="top" colSpan="2" align="left">
							<table border="0" cellSpacing="1" cellPadding="1">
								<tr>
									<td><asp:label id="lbl1" runat="server" Width="110px" CssClass="tableLabel">Date of Initial SIP:
										</asp:label></td>
									<td><cc1:mstextbox id="Txt_BatchDate" tabIndex="1" Runat="server" Width="106px" CssClass="textField"
											Text="" MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999" Enabled="True"></cc1:mstextbox></td>
									<td><asp:label id="Label3" runat="server" Width="55px" CssClass="tableLabel">Location:
										</asp:label></td>
									<td colSpan="3"><asp:dropdownlist id="Drp_Location" runat="server" CssClass="textField" AutoPostBack="False"></asp:dropdownlist></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:datagrid style="Z-INDEX: 0" id="dgLodgments" runat="server" Width="800px" HorizontalAlign="Left"
								AutoGenerateColumns="False" PageSize="25" SelectedItemStyle-CssClass="gridFieldSelected" AllowPaging="True">
								<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
								<ItemStyle Height="15px" CssClass="gridField"></ItemStyle>
								<HeaderStyle Font-Bold="True" HorizontalAlign="Center" Width="6%" CssClass="gridHeading"></HeaderStyle>
								<Columns>
									<asp:BoundColumn DataField="Consignment No" HeaderText="Consignment No">
										<HeaderStyle Width="100px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="No. of Packages" HeaderText="No. of Packages">
										<HeaderStyle Width="80px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Created Date" HeaderText="Created Date">
										<HeaderStyle Width="150px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Origin DC" HeaderText="Origin DC">
										<HeaderStyle Width="60px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Destination Zipcode" HeaderText="Destination Postal Code">
										<HeaderStyle Width="100px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Service Code" HeaderText="Service Code">
										<HeaderStyle Width="80px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Missing MPS No." HeaderText="Missing MPS No.">
										<HeaderStyle Width="230px"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left"></ItemStyle>
									</asp:BoundColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
							</asp:datagrid></td>
					</tr>
				</TABLE>
			</div>
			&nbsp; <INPUT 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition> <INPUT type="hidden" name="hdnRefresh">
			<asp:button style="Z-INDEX: 105; POSITION: absolute; DISPLAY: none; TOP: 1064px; LEFT: 672px"
				id="btnHidReport" runat="server" Text=".."></asp:button></form>
	</body>
</HTML>
