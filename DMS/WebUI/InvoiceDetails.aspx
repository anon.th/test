<%@ Page language="c#" Codebehind="InvoiceDetails.aspx.cs" AutoEventWireup="false" Inherits="com.ties.InvoiceDetails" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>InvoiceDetails</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<META content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<META content="C#" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"> <!--#INCLUDE FILE="msFormValidations.inc"-->
		<SCRIPT language="javascript" src="Scripts/settingScrollPosition.js"></SCRIPT>
	</HEAD>
	<BODY onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<FORM id="InvoiceGenerationPreview" method="post" runat="server">
			<div id="InvoiceDetails" style="Z-INDEX: 100; LEFT: 24px; WIDTH: 954px; POSITION: relative; TOP: 23px; HEIGHT: 272px" runat="server" ms_positioning="GridLayout"><asp:button id="btnCancelDtl" style="Z-INDEX: 102; LEFT: 11px; POSITION: absolute; TOP: 37px" runat="server" Text="Cancel" CausesValidation="False" Width="61px" CssClass="queryButton"></asp:button><asp:label id="lblErrorMsgDtl" style="Z-INDEX: 103; LEFT: 18px; POSITION: absolute; TOP: 62px" runat="server" Width="537px" CssClass="errorMsgColor" Height="19px"></asp:label><asp:validationsummary id="PageValidationSummaryDtl" style="Z-INDEX: 104; LEFT: 14px; POSITION: absolute; TOP: 62px" runat="server" Width="346px" Height="39px" ShowMessageBox="True" ShowSummary="False" Visible="True"></asp:validationsummary><asp:label id="lblMainTitleDtl" style="Z-INDEX: 101; LEFT: 15px; POSITION: absolute; TOP: 9px" runat="server" Width="477px" CssClass="mainTitleSize" Height="26px">Invoice Details</asp:label>
				<TABLE id="Table1" style="Z-INDEX: 105; LEFT: 7px; WIDTH: 727px; POSITION: absolute; TOP: 95px; HEIGHT: 139px" cellSpacing="1" width="727" align="left" border="0" runat="server">
					<TR height="25">
						<TD style="WIDTH: 150px" vAlign="center" align="left" width="150" bgColor="#008499">&nbsp;
							<asp:label id="Label_50" runat="server" Width="150px" CssClass="tableLabel" Height="16px" ForeColor="White" BackColor="#008499" Font-Size="11px">Customer ID</asp:label></TD>
						<TD style="WIDTH: 9px" vAlign="top" width="9"></TD>
						<TD style="WIDTH: 1001px" vAlign="center" width="1001"><asp:label id="lblCustIDDtl" runat="server" Width="143px" CssClass="tableLabel"></asp:label></TD>
						<TD style="WIDTH: 150px" align="left" width="150" bgColor="#008499">&nbsp;
							<asp:label id="Label_6" runat="server" Width="150px" CssClass="tableLabel" Height="15px" ForeColor="White" BackColor="#008499" Font-Size="11px"> Province</asp:label></TD>
						<TD style="WIDTH: 8px" width="8"></TD>
						<TD style="WIDTH: 61px" width="61"><asp:label id="lblProvinceDtl" runat="server" Width="150px" CssClass="tableLabel"></asp:label></TD>
					</TR>
					<TR height="25">
						<TD style="WIDTH: 150px; HEIGHT: 25px" align="left" width="96" bgColor="#008499">&nbsp;
							<asp:label id="Label_48" runat="server" Width="150px" CssClass="tableLabel" Height="15px" ForeColor="White" BackColor="#008499" Font-Size="11px">Customer Name</asp:label></TD>
						<TD style="WIDTH: 9px; HEIGHT: 25px" width="9"></TD>
						<TD style="WIDTH: 1001px; HEIGHT: 25px" vAlign="center" width="1001"><asp:label id="lblCustNameDtl" runat="server" Width="260px" CssClass="tableLabel"></asp:label></TD>
						<TD style="WIDTH: 150px; HEIGHT: 25px" align="left" width="150" bgColor="#008499">&nbsp;
							<asp:label id="Label_49" runat="server" Width="93px" CssClass="tableLabel" Height="14px" ForeColor="White" BackColor="#008499" Font-Size="11px"> Telephone</asp:label></TD>
						<TD style="WIDTH: 8px; HEIGHT: 25px" width="8"></TD>
						<TD style="WIDTH: 61px; HEIGHT: 25px" width="61"><asp:label id="lblTelephoneDtl" runat="server" Width="150px" CssClass="tableLabel"></asp:label></TD>
					</TR>
					<TR height="25">
						<TD style="WIDTH: 150px" vAlign="center" align="left" width="150" bgColor="#008499">&nbsp;
							<asp:label id="Label_45" runat="server" Width="150px" CssClass="tableLabel" Height="15px" ForeColor="White" BackColor="#008499" Font-Size="11px"> Address 1</asp:label></TD>
						<TD style="WIDTH: 9px" width="9"></TD>
						<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblAddress1Dtl" runat="server" Width="303px" CssClass="tableLabel"></asp:label></TD>
						<TD style="WIDTH: 150px; HEIGHT: 25px" align="left" width="148" bgColor="#008499">&nbsp;
							<asp:label id="Label_47" runat="server" Width="83px" CssClass="tableLabel" Height="13px" ForeColor="White" BackColor="#008499" Font-Size="11px">Fax</asp:label></TD>
						<TD style="WIDTH: 8px" width="8"></TD>
						<TD style="WIDTH: 61px" width="61"><asp:label id="lblFaxDtl" runat="server" Width="150px" CssClass="tableLabel"></asp:label></TD>
					</TR>
					<TR height="25">
						<TD style="WIDTH: 150px" width="150" bgColor="#008499">&nbsp;
							<asp:label id="Label_1" runat="server" Width="150px" CssClass="tableLabel" Height="16px" ForeColor="White" BackColor="#008499" Font-Size="11px">Address 2</asp:label></TD>
						<TD style="WIDTH: 9px" width="9"></TD>
						<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblAddress2Dtl" runat="server" Width="301px" CssClass="tableLabel"></asp:label></TD>
						<TD style="WIDTH: 150px" align="left" width="148" bgColor="#008499">&nbsp;
							<asp:label id="Label_19" style="WHITE-SPACE: nowrap" runat="server" Width="83px" CssClass="tableLabel" Height="13px" ForeColor="White" BackColor="#008499" Font-Size="11px">Contact Person</asp:label></TD>
						<TD style="WIDTH: 8px" align="left" width="8"></TD>
						<TD style="WIDTH: 61px" align="left" width="61"><asp:label id="lblContactDtl" runat="server" Width="150px" CssClass="tableLabel"></asp:label></TD>
					</TR>
					<TR height="25">
						<TD style="WIDTH: 150px" vAlign="center" width="150" bgColor="#008499">&nbsp;
							<asp:label id="Label_2" runat="server" Width="150px" CssClass="tableLabel" Height="16px" ForeColor="White" BackColor="#008499" Font-Size="11px">Postal Code</asp:label></TD>
						<TD style="WIDTH: 9px" width="9"></TD>
						<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblPostalCodeDtl" runat="server" Width="143px" CssClass="tableLabel"></asp:label></TD>
						<TD style="WIDTH: 150px" width="150" bgColor="#008499">&nbsp;
							<asp:label id="Label_46" runat="server" Width="150px" CssClass="tableLabel" Height="16px" ForeColor="White" BackColor="#008499" Font-Size="11px">HC POD Required</asp:label></TD>
						<TD style="WIDTH: 8px" align="left" width="8"></TD>
						<TD style="WIDTH: 61px" align="left" width="61"><asp:label id="lblHC_POD_RequiredDtl" runat="server" Width="150px" CssClass="tableLabel" Font-Size="11px"></asp:label></TD>
					</TR>
				</TABLE>
				<TABLE id="Table2" style="Z-INDEX: 106; LEFT: 6px; WIDTH: 727px; POSITION: absolute; TOP: 244px; HEIGHT: 139px" cellSpacing="1" width="727" align="left" border="0" runat="server">
					<TBODY>
						<TR height="25">
							<TD style="WIDTH: 150px" vAlign="center" align="left" width="150" bgColor="#008499">&nbsp;
								<asp:label id="Label_3" style="WHITE-SPACE: nowrap" runat="server" Width="150px" Height="15px" ForeColor="White" BackColor="#008499" Font-Size="11px">Invoice Number</asp:label></TD>
							<TD style="WIDTH: 9px" vAlign="top" width="9"></TD>
							<TD style="WIDTH: 1001px" vAlign="center" width="1001"><asp:label id="lblInvNoDtl" runat="server" Width="143px" CssClass="tableLabel"></asp:label></TD>
							<TD style="WIDTH: 150px" align="left" width="150" bgColor="#008499">&nbsp;
								<asp:label id="Label_7" runat="server" Width="81px" CssClass="tableLabel" Height="15px" ForeColor="White" BackColor="#008499" Font-Size="11px"> Due Date</asp:label></TD>
							<TD style="WIDTH: 8px" width="8"></TD>
							<TD style="WIDTH: 61px" width="61"><asp:label id="lblDueDateDtl" runat="server" Width="150px" CssClass="tableLabel"></asp:label></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 150px" vAlign="center" align="left" width="96" bgColor="#008499">&nbsp;
								<asp:label id="Label_9" runat="server" Width="150px" CssClass="tableLabel" Height="16px" ForeColor="White" BackColor="#008499" Font-Size="11px">Invoice Status</asp:label></TD>
							<TD style="WIDTH: 9px" vAlign="top" width="9"></TD>
							<TD style="WIDTH: 1001px" vAlign="center" width="1001"><asp:label id="lblInvStatusDtl" runat="server" Width="143px" CssClass="tableLabel"></asp:label></TD>
							<TD style="WIDTH: 150px" align="left" width="150" bgColor="#008499">&nbsp;
								<asp:label id="Label_11" runat="server" Width="93px" CssClass="tableLabel" Height="14px" ForeColor="White" BackColor="#008499" Font-Size="11px"> Total Amount</asp:label></TD>
							<TD style="WIDTH: 8px" width="8"></TD>
							<TD style="WIDTH: 61px" width="61"><asp:label id="lblTotalAmtDtl" runat="server" Width="150px" CssClass="tableLabel"></asp:label></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 150px; HEIGHT: 25px" align="left" width="96" bgColor="#008499">&nbsp;
								<asp:label id="Label_13" runat="server" Width="150px" CssClass="tableLabel" Height="15px" ForeColor="White" BackColor="#008499" Font-Size="11px">Payment Term</asp:label></TD>
							<TD style="WIDTH: 9px; HEIGHT: 25px" width="9"></TD>
							<TD style="WIDTH: 1001px; HEIGHT: 25px" vAlign="center" width="1001"><asp:label id="lblPaymentTermDtl" runat="server" Width="260px" CssClass="tableLabel"></asp:label></TD>
							<TD style="WIDTH: 150px; HEIGHT: 25px" align="left" width="148" bgColor="#008499">&nbsp;
								<asp:label id="Label_15" runat="server" Width="150px" CssClass="tableLabel" Height="13px" ForeColor="White" BackColor="#008499" Font-Size="11px">Total Adjustments</asp:label></TD>
							<TD style="WIDTH: 8px; HEIGHT: 25px" width="8"></TD>
							<TD style="WIDTH: 61px; HEIGHT: 25px" width="61"><asp:label id="lblTotalAdjDtl" runat="server" Width="150px" CssClass="tableLabel"></asp:label></TD>
						</TR>
						<TR height="25">
							<TD style="WIDTH: 150px" vAlign="center" align="left" width="96" bgColor="#008499">&nbsp;
								<asp:label id="Label_17" runat="server" Width="150px" CssClass="tableLabel" Height="15px" ForeColor="White" BackColor="#008499" Font-Size="11px"> Invoice Date</asp:label></TD>
							<TD style="WIDTH: 9px" width="9"></TD>
							<TD style="WIDTH: 1001px" width="1001"><asp:label id="lblInvDateDtl" runat="server" Width="303px" CssClass="tableLabel"></asp:label></TD>
							<TD style="WIDTH: 150px" align="left" width="148" bgColor="#008499">&nbsp;
								<asp:label id="lblCancel_AppHeadDtl" runat="server" Width="150px" CssClass="tableLabel" Height="13px" ForeColor="White" BackColor="#008499" Font-Size="11px"></asp:label>
							</TD>
							<TD style="WIDTH: 8px" width="8"></TD>
							<TD style="WIDTH: 61px" width="61"><asp:label id="lblCancel_AppTextDtl" runat="server" Width="150px" CssClass="tableLabel"></asp:label></TD>
						</TR>
					</TBODY>
				</TABLE>
				<table style="Z-INDEX: 105; LEFT: 5px; POSITION: absolute; TOP: 406px" width="890">
					<tr>
						<td>
							<asp:datagrid id="dgPreviewDtl" runat="server" Height="95px" OnItemDataBound="dgPreviewDtl_ItemDataBound" SelectedItemStyle-CssClass="gridFieldSelected" OnEditCommand="dgPreviewDtl_dgEditDtl" OnItemCommand="dgPreviewDtl_Button" AutoGenerateColumns="False" ItemStyle-Height="20">
								<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
								<ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
								<Columns>
									<asp:ButtonColumn ItemStyle-CssClass="gridField" HeaderStyle-CssClass="gridBlueHeading" ButtonType="LinkButton" Text="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Search' &gt;" CommandName="Edit"></asp:ButtonColumn>
									<asp:BoundColumn DataField="consignment_no" HeaderText="Consignment No.">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="ref_no" HeaderText="Ref No.">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="sender_name" HeaderText="Sender">
										<HeaderStyle HorizontalAlign="Center" Width="180px" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="sender_zipcode" HeaderText="Origin">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="act_pickup_datetime" HeaderText="Pickup" DataFormatString="{0:dd/MM/yyyy HH:mm}">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="recipient_name" HeaderText="Recipient">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="recipient_zipcode" HeaderText="Destination">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="act_delivery_date" HeaderText="Delivery" DataFormatString="{0:dd/MM/yyyy HH:mm}">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left" CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="service_code" HeaderText="Service">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Freight">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridFieldNumber" ID="lblFreight" Runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"tot_freight_charge"))%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>								
									<asp:TemplateColumn HeaderText="Insurance">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridFieldNumber" ID="lblInsurance" Runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"insurance_amt"))%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>									
									<asp:TemplateColumn HeaderText="VAS">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridFieldNumber" ID="lblVAS" Runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"tot_vas_surcharge"))%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>		
										<asp:TemplateColumn HeaderText="ESA">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridFieldNumber" ID="lblESA" Runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"esa_surcharge"))%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>	
									<asp:TemplateColumn HeaderText="Exception">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridFieldNumber" ID="lblException" Runat="server" Text='<%#utility.encodeNegValue(DataBinder.Eval(Container.DataItem,"total_exception"),(String)ViewState["m_format"])%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Adjustment">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridFieldNumber" ID="lblAdjustment" Runat="server" Text='<%#utility.encodeNegValue(DataBinder.Eval(Container.DataItem,"invoice_adj_amount"),(String)ViewState["m_format"])%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Other">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridFieldNumber" ID="lblOther" Runat="server" Text='<%#utility.encodeNegValue(DataBinder.Eval(Container.DataItem,"other_surcharge"),(String)ViewState["m_format"])%>' >
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="MBG">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridFieldNumber" ID="lblMBG" Runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"mbg_amount"))%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridFieldNumber" ID="lblTOT" Runat="server" Text='<%#String.Format((String)ViewState["m_format"],DataBinder.Eval(Container.DataItem,"invoice_amt"))%>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="adjusted_remark" HeaderText="Adjust Remark" Visible="False">
										<HeaderStyle HorizontalAlign="Center" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField" Wrap="False"></ItemStyle>
									</asp:BoundColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
							</asp:datagrid></td>
					</tr>
					<tr height="20">
					</tr>
					<tr>
						<td><asp:datagrid id="dgEditDtl" runat="server" Height="95px" OnItemDataBound="dgEditDtl_dgBound" SelectedItemStyle-CssClass="gridFieldSelected" AutoGenerateColumns="False" ItemStyle-Height="20" OnUpdateCommand="dgEditDtl_dgUpdate">
								<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
								<ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
								<Columns>
									<asp:TemplateColumn>
										<HeaderStyle CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:ImageButton CommandName="Update" id="imgUpdate" runat="server" ImageUrl="images/butt-update.gif"></asp:ImageButton>&nbsp;&nbsp;
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Consignment No.">
										<HeaderStyle HorizontalAlign="Center" Width="86px" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label Width="86px" CssClass="gridLabel" runat="server" ID="lblConsgnNo"></asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Type">
										<HeaderStyle HorizontalAlign="Center" Width="86px" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList Width="86px" CssClass="gridDropDown" ID="ddlAdjType" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Apply As*">
										<HeaderStyle HorizontalAlign="Center" Width="86px" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle Wrap="False" CssClass="gridField" VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:DropDownList Width="86px" CssClass="gridDropDown" ID="ddlApplyAs" Runat="server"></asp:DropDownList>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Amount">
										<HeaderStyle HorizontalAlign="Center" Width="100px" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtAmount" Runat="server" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2"></cc1:mstextbox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Remark">
										<HeaderStyle HorizontalAlign="Center" Width="250px" CssClass="gridBlueHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField"></ItemStyle>
										<ItemTemplate>
											<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtRemark"></asp:TextBox>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid></td>
					</tr>
				</table>
			</div>
		</FORM>
	</BODY>
</HTML>
