<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>

<%@ Page Language="c#" CodeBehind="PODSummary.aspx.cs" AutoEventWireup="false" Inherits="com.ties.PODSummary" SmartNavigation="False" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>POD Summary</title>
    <link href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <!--#INCLUDE FILE="msFormValidations.inc"-->
    <script language="javascript" src="Scripts/settingScrollPosition.js"></script>
    <script language="javascript">
        function makeUppercase(objId) {
            document.getElementById(objId).value = document.getElementById(objId).value.toUpperCase();
        }
    </script>
</head>
<body onunload="window.top.displayBanner.fnCloseAll(0);" ms_positioning="GridLayout">
    <form id="PODSummary" method="post" runat="server">
        <asp:Button Style="z-index: 102; position: absolute; top: 53px; left: 16px" ID="btnQuery" TabIndex="100"
            runat="server" Text="Query" Width="64px" CssClass="queryButton"></asp:Button>
        &nbsp;&nbsp;
			<asp:Label Style="z-index: 104; position: absolute; top: 80px; left: 16px" ID="lblErrorMessage"
                runat="server" Width="616px" CssClass="errorMsgColor" Height="30px">Place holder for err msg</asp:Label>
        <asp:Label Style="z-index: 103; position: absolute; top: 16px; left: 15px" ID="lblTitle" runat="server"
            Width="558px" CssClass="maintitleSize" Height="32px">POD Summary</asp:Label>
        <asp:Button Style="z-index: 101; position: absolute; top: 53px; left: 81px" ID="btnGenerate"
            runat="server" Text="Generate" Width="82px" CssClass="queryButton"></asp:Button>
        <table style="z-index: 104; position: absolute; width: 883px; height: 337px; top: 102px; left: 13px"
            id="tblShipmentTracking" border="0" width="883" runat="server">
            <tr>
                <td style="width: 464px; height: 137px" valign="top">
                    <fieldset style="width: 450px; height: 129px">
                        <legend>
                            <asp:Label ID="lblDates" runat="server" Width="178px" CssClass="tableHeadingFieldset" Font-Bold="True">Enter Manifest Year</asp:Label></legend>
                        <table style="width: 360px; height: 91px" id="tblDates" border="0" cellspacing="0" cellpadding="0"
                            align="left" runat="server">
                            <tr>
                                <td></td>
                                <td colspan="3">&nbsp;
                                        <asp:Label ID="lblYear" runat="server" Width="30px" CssClass="tableLabel" Height="22px">Year</asp:Label>
                                    &nbsp;
										<asp:TextBox ID="txtfromYears" runat="server" Width="83px" MaxLength="4" CssClass="textField"></asp:TextBox>
                                    &nbsp;
										&nbsp;
										<asp:TextBox ID="txtToYears" runat="server" Width="83px" MaxLength="4" CssClass="textField"></asp:TextBox>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td style="height: 137px" valign="top">
                    <fieldset style="width: 320px; height: 105px">
                        <legend>
                            <asp:Label ID="lblPayerType" runat="server" Width="79px" CssClass="tableHeadingFieldset" Font-Bold="True">Payer Type</asp:Label></legend>
                        <table style="width: 300px; height: 73px" id="tblPayerType" border="0" cellspacing="0"
                            cellpadding="0" align="left" runat="server">
                            <tr>
                                <td></td>
                                <td height="1" colspan="2">&nbsp;</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="tableLabel" valign="top" colspan="2">&nbsp;
										<asp:Label ID="Label4" runat="server" Width="87px" CssClass="tableLabel" Height="22px">Payer Type</asp:Label></td>
                                <td>
                                    <asp:ListBox ID="lsbCustType" runat="server" Width="137px" Height="61px" SelectionMode="Multiple"></asp:ListBox></td>
                                <td></td>
                            </tr>
                            <tr height="33">
                                <td bgcolor="blue"></td>
                                <td class="tableLabel" valign="top" colspan="2">&nbsp;
										<asp:Label ID="lblPayerCode" runat="server" Width="79px" CssClass="tableLabel" Height="22px">Payer Code</asp:Label>&nbsp;&nbsp;</td>
                                <td>
                                    <asp:TextBox ID="txtPayerCode" runat="server" Width="148px" CssClass="textField"></asp:TextBox>&nbsp;&nbsp;
										<asp:Button ID="btnPayerCode" runat="server" Text="..." CssClass="searchButton" CausesValidation="False"></asp:Button></td>
                                <td></td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
