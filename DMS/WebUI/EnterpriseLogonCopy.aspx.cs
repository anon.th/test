using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using com.common.RBAC;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using TIES; //Add By Lin 29/4/2010
using com.ties.DAL;    //Add By Lin 29/4/2010
using iTextSharp.text.xml;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.Text; 
using System.Net;
//using System.Collections;
using iTextSharp.text.html.simpleparser;

//namespace TIES.WebUI
namespace com.ties
{
	/// <summary>
	/// Summary description for EnterpriseLogon.
	/// </summary>
	public class EnterpriseLogonCopy : System.Web.UI.Page 
	{
		protected System.Web.UI.WebControls.RequiredFieldValidator Requiredfieldvalidator1;
		protected System.Web.UI.WebControls.RequiredFieldValidator Requiredfieldvalidator2;
		protected System.Web.UI.WebControls.RequiredFieldValidator Requiredfieldvalidator3;
		protected System.Web.UI.WebControls.Label Label1;
		protected com.common.util.msTextBox txtUserID;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox txtUserPswd;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.TextBox txtEnterpriseID;
		protected System.Web.UI.WebControls.Label lblErrMsg;
		protected System.Web.UI.WebControls.Button btnSubmit;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.HtmlControls.HtmlGenericControl div_export;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.HtmlControls.HtmlTable tblData;
		Utility utility = null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
//			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
//			if(!Page.IsPostBack)
//			{
//				Response.CacheControl = "no-cache";
//				Response.AddHeader("Pragma","no-cache");
//				Response.Expires = -1;
//				if(lblErrMsg.Text == "")
//				{
//					if (Request.Params["errMsg"] != null)
//					{
//						//lblErrMsg.Text=Request.Params["errMsg"].ToString();
//						if (Request.Params["errMsg"].ToString().Equals("TIMEOUT"))
//						{
//							if (Request.Params["culture"] != null)
//							{
//								lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SESSION_TIMEOUT", Request.Params["culture"].ToString());
//							}
//							else
//							{
//								lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SESSION_TIMEOUT", utility.GetUserCulture());
//							}
//						}
//						else
//						{
//							if (Request.Params["culture"] != null)
//							{
//								lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SESSION_LOGOUT", Request.Params["culture"].ToString());
//							}
//							else
//							{
//								lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "SESSION_LOGOUT", utility.GetUserCulture());
//							}
//						}
//					}
//				}
//			}
			
			btnCancel_Click(null,null);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnSubmit_Click(object sender, System.EventArgs e)
		{
			ArrayList userRolesList = null;
			bool isValid = RBACManager.AuthenticateUser(utility.GetAppID(),txtEnterpriseID.Text.Trim(),txtUserID.Text.Trim(),txtUserPswd.Text.Trim());
			DataSet dsCon = new DataSet();  //Add By Lin 29/4/2010 
			DataTable dtCon = new DataTable(); //Add By Lin 29/4/2010
			DataRow drCon;  //Add By Lin 29/4/2010
			DataSet dsParam = new DataSet();  //Add By Lin 29/4/2010 
			DataTable dtParam = new DataTable(); //Add By Lin 29/4/2010
			string ParamName = "";

			if (isValid)
			{
				lblErrMsg.Text = "";

				com.common.classes.User user = RBACManager.GetUser(utility.GetAppID(),txtEnterpriseID.Text.Trim(),txtUserID.Text.Trim());

				user.UserID = txtUserID.Text.Trim();
				userRolesList = RBACManager.GetAllRoles(utility.GetAppID(),txtEnterpriseID.Text.Trim(),user);
				
						
				String strRoles = null;
				foreach (Role role in userRolesList)
				{
					strRoles += role.RoleName;
					strRoles += ";";
				}
			
				Session["userID"] = user.UserID;
				Session["applicationID"] = System.Configuration.ConfigurationSettings.AppSettings["applicationID"];
				Session["enterpriseID"] = txtEnterpriseID.Text.Trim();
				Session["userCulture"] = user.UserCulture;
				Session["UserName"] = user.UserName; //Add By Lin 28/4/2010
				Session["PayerID"] = user.PayerID; //Add By Lin 28/4/2010
				Session["UserLocation"] = user.UserLocation;  //Add By Lin 28/4/2010

				//start Add By Lin 29/4/2010
				if (user.PayerID.ToString() != "")
				{
					// Get Customer Data
					dsCon = ConsignmentNoteDAL.GetInitConData(utility.GetAppID(), utility.GetEnterpriseID(), user.PayerID);     
					dtCon = dsCon.Tables[0]; 
					if (dtCon != null && dtCon.Rows.Count > 0)
					{
						drCon = dtCon.Rows[0];
						Session["PayerID"] = drCon["custid"] + "";
						Session["CustID"] = drCon["custid"] + "";
						Session["CustName"] = drCon["cust_name"] + "";
						Session["Remark2"] = drCon["Remark2"] + "";
						Session["SenderZipcode"] = drCon["zipcode"] + "";
					}

					#region Edit by Sompote 2010-08-06
					// Get Parameter ConsignmentNote
					try
					{
						dsParam = ConsignmentNoteDAL.GetParameterConsignment(utility.GetAppID(), utility.GetEnterpriseID(), user.PayerID);
						dtParam = dsParam.Tables[0];
						if (dtParam != null && dtParam.Rows.Count > 0)
						{
							foreach (DataRow drParam in dtParam.Rows)
							{
								ParamName = "";
								ParamName = drParam["ParamName"] + "";
							
								if (ParamName == "Sender_Contact_Name") {Session["SenderContactName"] = drParam["ParamValue"] + "";}
								if (ParamName == "Sender_Telephone") {Session["SenderTelephone"] = drParam["ParamValue"] + "";}
								if (ParamName == "Default_Service_Type") {Session["DefaultServiceType"] = drParam["ParamValue"] + "";}
								if (ParamName == "Special_Instruction") {Session["SpecialInstruction"] = drParam["ParamValue"] + "";}
							}
						}
					}
					catch(Exception ex)
					{
						//String msg = "This user is unavailable in this version.";
						//lblErrMsg.Text = msg; 
					}
//					dsParam = ConsignmentNoteDAL.GetParameterConsignment(utility.GetAppID(), utility.GetEnterpriseID(), user.PayerID);
//					dtParam = dsParam.Tables[0];
//					if (dtParam != null && dtParam.Rows.Count > 0)
//					{
//						foreach (DataRow drParam in dtParam.Rows)
//						{
//							ParamName = "";
//							ParamName = drParam["ParamName"] + "";
//							
//							if (ParamName == "Sender_Contact_Name") {Session["SenderContactName"] = drParam["ParamValue"] + "";}
//							if (ParamName == "Sender_Telephone") {Session["SenderTelephone"] = drParam["ParamValue"] + "";}
//							if (ParamName == "Default_Service_Type") {Session["DefaultServiceType"] = drParam["ParamValue"] + "";}
//							if (ParamName == "Special_Instruction") {Session["SpecialInstruction"] = drParam["ParamValue"] + "";}
//						}
//					}
					#endregion
				}
				//end Add By Lin 29/4/2010

				FormsAuthenticationTicket formsAuthenticationTicket = null;
				//Context.User.Identity.Name = user.UserID;


				try
				{
					formsAuthenticationTicket = new FormsAuthenticationTicket(1,Context.User.Identity.Name,DateTime.Now,DateTime.Now.AddHours(1), false, strRoles);
					//Encrypt the ticket
					String strCookie  = FormsAuthentication.Encrypt(formsAuthenticationTicket);

					//Send the cookie to the client
					String strCookieName = System.Configuration.ConfigurationSettings.AppSettings["appcookiename"];
					Response.Cookies[strCookieName].Value = strCookie;
					Response.Cookies[strCookieName].Path = "/";
					Response.Cookies[strCookieName].Expires = DateTime.Now.AddMinutes(1);

					FormsAuthentication.SetAuthCookie(user.UserID,false);

					//Response.Redirect("LaunchPage.aspx");
					
					Response.Redirect("MainFrame.aspx");
				}
				catch(Exception exp)
				{
					Console.WriteLine("ERRRRor "+ exp.Message.ToString());
				}
			}
			else
			{
				//String msg = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_LOGIN",utility.GetUserCulture());
				String msg = "Please enter User ID/Password/Enterprise ID to login";
				lblErrMsg.Text = msg; 
			}
			
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
//			String strHtml;
//			MemoryStream memStream = new MemoryStream();
//			StringWriter strWriter = new StringWriter();
//			Server.Execute("TestReport.aspx", strWriter);
//			strHtml = strWriter.ToString();
//			strWriter.Flush();
//			strWriter.Close();
//			string strFileShortName = "test" + DateTime.Now.Ticks + ".pdf";
//			string strFileName = HttpContext.Current.Server.MapPath("reports\\" + strFileShortName);
//			iTextSharp.text.Document docWorkingDocument = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4.Rotate(), 1, 1, 0, 0);
//			StringReader srdDocToString = null;
//			try
//			{
//				PdfWriter pdfWrite;
//				pdfWrite = PdfWriter.GetInstance(docWorkingDocument, new FileStream(strFileName, FileMode.Create));
//				srdDocToString = new StringReader(strHtml);
//				docWorkingDocument.Open();
//				XMLWorkerHelper.GetInstance().ParseXHtml(pdfWrite, docWorkingDocument, srdDocToString);
//			}
//			catch(Exception ex)
//					   {
//						   Response.Write(ex.Message);
//						   }
//	 finally
// {
// if(docWorkingDocument != null)
// {
//	 //strWriter.Flush();
//	docWorkingDocument.Close();
//					   }
// if(srdDocToString != null)
//							  {
//	srdDocToString.Close();
//	 }
// }
//
// Page.RegisterClientScriptBlock(this.GetType(), "myscript", "window.open('reports/" + strFileShortName + "','_blank','location=0,menubar=0,status=0,titlebar=0,toolbar=0');", true);




//			StringBuilder sb = new StringBuilder();
//			div_export.RenderControl(new HtmlTextWriter(new StringWriter(sb)));
//			string s = sb.ToString();
//			string contents = s;
//			Document document = new Document();
//				PdfWriter writer = PdfWriter.GetInstance(document, Response.OutputStream);
//				document.Open();
//				try
//				{
//					StringReader sr = new StringReader(contents);
//					XMLWorkerHelper.GetInstance().ParseXHtml( writer, document, sr ); 
//					document.Close();
//					Response.ContentType = "application/pdf";
//					Response.AddHeader("content-disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMdd") + ".pdf");
//					System.Web.HttpContext.Current.Response.Write(document);
//					Response.Flush();
//					Response.End();
//				}
//				catch (Exception)
//				{
//					//throw;
//				}

//			StringBuilder sb = new StringBuilder();
//			div_export.RenderControl(new HtmlTextWriter(new StringWriter(sb)));
//			string s = sb.ToString();
//
//			Document pdf = new Document(PageSize.LETTER);
//			PdfWriter writer = PdfWriter.GetInstance(pdf, 
//				new FileStream(Request.PhysicalApplicationPath + "~1.pdf", FileMode.Create));
//			pdf.Open();
//
//			//This action leads directly to printer dialogue
//			PdfAction jAction = PdfAction.JavaScript("this.print(true);\r", writer);
//			writer.AddJavaScript(jAction);
//
//			pdf.Add(new Paragraph(s));
//			pdf.Close();

			//StreamPdf


			StringBuilder sb = new StringBuilder();
			div_export.RenderControl(new HtmlTextWriter(new StringWriter(sb)));
			string s = sb.ToString();
			//HtmlTextWriter(new StringWriter(sb));
			//createPDF(s);
			

//
//			Response.ContentType = "application/pdf";
//			Response.AddHeader("content-disposition", "attachment;filename=Panel.pdf");
//			Response.Cache.SetCacheability(HttpCacheability.NoCache);
//			StringWriter sw = new StringWriter();
//			HtmlTextWriter hw = new HtmlTextWriter(sw);
//			div_export.RenderControl(hw);
//			StringReader sr = new StringReader(sw.ToString());
//			Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
//			HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
//			PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
//			pdfDoc.Open();
//			htmlparser.Parse(sr);
//			pdfDoc.Close();
//			Response.Write(pdfDoc);
//			Response.End();

			CreatePDFDocument(s);
			//Render();
			//ShowPdf("LovLove.pdf");
		}

		private MemoryStream createPDF(string html)
		{
			MemoryStream msOutput = new MemoryStream();
			TextReader reader = new StringReader(html);

			// step 1: creation of a document-object
			Document document = new Document(PageSize.A4, 30, 30, 30, 30);            

			// step 2:
			// we create a writer that listens to the document
			// and directs a XML-stream to a file
			PdfWriter writer = PdfWriter.GetInstance(document, msOutput);

			// step 3: we create a worker parse the document
			HTMLWorker worker = new HTMLWorker(document);

			// step 4: we open document and start the worker on the document
			document.Open();
			worker.StartDocument();

			// step 5: parse the html into the document
			worker.Parse(reader);

			// step 6: close the document and the worker
			worker.EndDocument();
			worker.Close();
			document.Close();
//
			return msOutput;
		}
//
//		protected void StreamPdf(Stream pdfSource)
//		{
//			var outputStream = new MemoryStream();
//			var pdfReader = new PdfReader(pdfSource);
//			var pdfStamper = new PdfStamper(pdfReader, outputStream);
//			//Add the auto-print javascript
//			var writer = pdfStamper.Writer;
//			writer.AddJavaScript(GetAutoPrintJs());
//			pdfStamper.Close();
//			var content = outputStream.ToArray();
//			outputStream.Close();
//			Response.ContentType = "application/pdf";
//			Response.BinaryWrite(content);
//			Response.End();
//			outputStream.Close();
//			outputStream.Dispose();
//		}



//		protected override void Render(HtmlTextWriter writer)
//		{
//			MemoryStream mem = new MemoryStream();
//			StreamWriter twr = new StreamWriter(mem);
//			HtmlTextWriter myWriter = new HtmlTextWriter(twr);
//			base.Render(myWriter);
//			myWriter.Flush();
//			myWriter.Close();
//			StreamReader strmRdr = new StreamReader(mem);
//			strmRdr.BaseStream.Position = 0;
//			string pageContent = strmRdr.ReadToEnd();
//			strmRdr.Close();
//			mem.Close();
//			writer.Write(pageContent);
			//CreatePDFDocument(pageContent);
//		}
		public  void CreatePDFDocument(string strHtml)
		{
			try
			{
				//string strFileName = HttpContext.Current.Server.MapPath("test.pdf");
				// step 1: creation of a document-object
				Document document = new Document();
				// step 2:
				// we create a writer that listens to the document
				PdfWriter.GetInstance(document, new FileStream(Request.PhysicalApplicationPath + "test~1.pdf", FileMode.Create));
				StringReader se = new StringReader(strHtml);
				HTMLWorker obj = new HTMLWorker(document);
				document.Open();
				obj.Parse(se);
				document.Close();
				ShowPdf(Request.PhysicalApplicationPath + "test~1.pdf");
			}
			catch(Exception e)
			{
				string err = e.Message;
//				obj.Close();
//				document.Close();
			}
		}
		public void ShowPdf(string strFileName)
		{
//			Response.ClearContent();
//			Response.ClearHeaders();
//			Response.AddHeader("Content-Disposition", "attachment;filename=" + strFileName);
//			Response.ContentType = "application/pdf";
//			Response.WriteFile(strFileName);
//			Response.Flush();
//			Response.Clear();
			
			Response.ClearContent();
			Response.ClearHeaders();
			Response.ContentType = "application/pdf";
			Response.AddHeader("Content-Disposition", String.Format("inline; filename={0}.pdf", Path.GetFileName(strFileName)));

			Response.WriteFile(strFileName);
			Response.Flush();
			Response.End();
		}


	}
}
