﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.common.applicationpages;
using System.Text;
using System.Configuration;
using System.Data.OleDb;
using System.IO;
using System.Globalization;
using ClosedXML.Excel;

namespace com.ties
{
    /// <summary>
    /// Summary description for BaseZoneRates.
    /// </summary>
    public class BaseZoneRates : BasePage
    {
        protected System.Web.UI.WebControls.Button btnQuery;
        protected System.Web.UI.WebControls.Button btnExecuteQuery;
        protected System.Web.UI.WebControls.Button btnInsert;
        protected System.Web.UI.WebControls.Label lblTitle;
        protected System.Web.UI.WebControls.DataGrid dgBZRates;
        protected System.Web.UI.WebControls.Label lblErrorMessage;

        //Utility utility = null;
        private String m_strAppID;
        private String m_strEnterpriseID;
        protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
        //DataSet m_dsBZRates = null;
        SessionDS m_sdsBZRates = null;
        protected System.Web.UI.WebControls.Button btnImport_Show;
        protected System.Web.UI.WebControls.Button btnImport_Cancel;
        protected System.Web.UI.WebControls.Button btnImport;
        protected System.Web.UI.WebControls.Button btnNewImport;
        protected System.Web.UI.WebControls.Button btnImport_Save;
        protected System.Web.UI.WebControls.Button btnDisplayCustDtls;
        protected System.Web.UI.WebControls.TextBox txtFilePath;
        protected System.Web.UI.HtmlControls.HtmlInputFile inFile;
        protected System.Web.UI.HtmlControls.HtmlTable Table1;
        protected System.Web.UI.HtmlControls.HtmlTable xxx;
        protected System.Web.UI.HtmlControls.HtmlTableRow trQuery;
        protected System.Web.UI.HtmlControls.HtmlTableRow trImport;
        protected System.Web.UI.WebControls.TextBox txtRow;
        protected System.Web.UI.WebControls.TextBox txtEndRow;
        protected System.Web.UI.WebControls.TextBox txtBasicRate;
        protected System.Web.UI.WebControls.TextBox txtColumn;
        protected System.Web.UI.WebControls.TextBox txtValuable;
        protected System.Web.UI.WebControls.TextBox txtHuman;
        protected System.Web.UI.WebControls.TextBox txtLive;
        protected System.Web.UI.WebControls.TextBox txtExpress;
        protected System.Web.UI.WebControls.TextBox txtCustID;
        protected System.Web.UI.WebControls.TextBox txtCustName;
        protected com.common.util.msTextBox txtEffDate;
        protected System.Web.UI.WebControls.TextBox txtSheet;
        protected System.Web.UI.UpdateProgress UpdateProgress1;

        string strRetMsg = null;

        private void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            //utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
            m_strAppID = utility.GetAppID();
            m_strEnterpriseID = utility.GetEnterpriseID();

            if (!Page.IsPostBack)
            {
                ViewState["Operation"] = Operation.None;
                ViewState["Mode"] = ScreenMode.None;
                lblErrorMessage.Text = "";
                dgBZRates.CurrentPageIndex = 0;
                dgBZRates.Columns[0].Visible = false;
                dgBZRates.Columns[1].Visible = false;
                dgBZRates.Columns[2].Visible = false;

                Session["dsBaseRates_Import"] = null;


                this.btnExecuteQuery.Enabled = false;
                EnableZoneUpload(false);

                QueryMode();
            }
            else
                m_sdsBZRates = (SessionDS)Session["SESSION_DS1"];

            lblErrorMessage.Text = "";
            ValidationSummary1.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            base.OnInit(e);
            InitializeComponent();

        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            this.btnImport_Show.Click += new System.EventHandler(this.btnImport_Show_Click);
            this.btnImport_Cancel.Click += new System.EventHandler(this.btnImport_Cancel_Click);
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            this.btnNewImport.Click += new System.EventHandler(this.btnNewImport_Click);
            this.btnImport_Save.Click += new System.EventHandler(this.btnImport_Save_Click);
            this.Load += new System.EventHandler(this.Page_Load);
            // this.btnDisplayCustDtls.Click += new System.EventHandler(this.btnDisplayCustDtls_Click);

        }
        #endregion

        //Add by Panas
        public bool ckEffectiveDate(DateTime effDate)
        {
            DateTime effStart = System.DateTime.Now;
            ViewState["effStart"] = effStart;
            DateTime effEnd = System.DateTime.Now;
            bool ckDate = false;

            DataSet effDS = new DataSet();
            effDS = SysDataMgrDAL.GetEndRoundEffectiveDate(utility.GetAppID(), utility.GetEnterpriseID(), effStart.ToString("dd/MM/yyyy"));
            DataTable effDT = new DataTable();
            effDT = effDS.Tables[0];
            foreach (DataRow dr in effDT.Rows)
            {
                effEnd = Convert.ToDateTime(dr["endEffDate"].ToString());
                ViewState["effEnd"] = effEnd;
            }
            if (effDate.Date >= effStart.Date && effDate.Date <= effEnd.Date)
            {
                ckDate = true;
            }
            else
            {
                ckDate = false;
            }

            return ckDate;
        }

        protected void dgBZRates_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            Logger.LogDebugInfo("BaseZoneRates", "dgBZRates_SelectedIndexChanged", "INF004", "updating data grid...");
        }

        protected void dgBZRates_Update(object sender, DataGridCommandEventArgs e)
        {
            lblErrorMessage.Text = "";
            //IN THE IMPORT MODE
            if (this.btnImport_Save.Visible == true)
            {
                //verify data


                //update data into dsBaseRates_Import session
                DataSet dsBaseRates_Import = (DataSet)Session["dsBaseRates_Import"];

                msTextBox txtOZCode = (msTextBox)e.Item.FindControl("txtOZCode");
                msTextBox txtDZCode = (msTextBox)e.Item.FindControl("txtDZCode");
                msTextBox txtStartWt = (msTextBox)e.Item.FindControl("txtStartWt");
                msTextBox txtEndWt = (msTextBox)e.Item.FindControl("txtEndWt");
                msTextBox txtSPrice = (msTextBox)e.Item.FindControl("txtSPrice");
                msTextBox txtIPrice = (msTextBox)e.Item.FindControl("txtIPrice");
                msTextBox txtServiceCode = (msTextBox)e.Item.FindControl("txtServiceCode");
                msTextBox txtEffectiveDate = (msTextBox)e.Item.FindControl("txtEffectiveDate");


                string strOZCode = txtOZCode.Text.ToString();
                string strDZCode = txtDZCode.Text.ToString();
                decimal decStartWt = 0;
                decimal decEndWt = 0;
                decimal decSPrice = 0;
                decimal decIPrice = 0;
                string strServiceCode = txtServiceCode.Text.ToString();
                string strEffectiveDate = txtEffectiveDate.Text.ToString();

                if ((txtStartWt.Text != null) && (txtStartWt.Text != ""))
                    decStartWt = Convert.ToDecimal(txtStartWt.Text.ToString());
                if ((txtEndWt.Text != null) && (txtEndWt.Text != ""))
                    decEndWt = Convert.ToDecimal(txtEndWt.Text.ToString());
                if ((txtSPrice.Text != null) && (txtSPrice.Text != ""))
                    decSPrice = Convert.ToDecimal(txtSPrice.Text.ToString());
                if ((txtIPrice.Text != null) && (txtIPrice.Text != ""))
                    decIPrice = Convert.ToDecimal(txtIPrice.Text.ToString());

                try

                {
                    DataRow dr = dsBaseRates_Import.Tables[0].Rows[e.Item.ItemIndex];
                    dr[0] = strOZCode;
                    dr[1] = strDZCode;
                    dr[2] = decStartWt;
                    dr[3] = decEndWt;
                    dr[4] = decSPrice;
                    dr[5] = decIPrice;
                    dr[6] = strServiceCode;
                    dr[7] = DateTime.ParseExact(strEffectiveDate, "dd/MM/yyyy", null);
                }
                catch (Exception ex)
                {
                    lblErrorMessage.Text = ex.Message;
                }
                this.dgBZRates.EditItemIndex = -1;
                BindGrid();

                dgBZRates.Columns[0].Visible = true;
                dgBZRates.Columns[1].Visible = true;
                dgBZRates.Columns[2].Visible = true;

                string statusButton = "0";

                foreach (DataGridItem dgItem in dgBZRates.Items)
                {
                    DataSet dsZone = new DataSet();

                    //bool duplicate=false;

                    ImageButton imgSelect = (ImageButton)dgItem.Cells[0].Controls[1];
                    Label lblOZCode = (Label)dgItem.FindControl("lblOZCode");
                    Label lblDZCode = (Label)dgItem.FindControl("lblDZCode");
                    Label lblServiceCode = (Label)dgItem.FindControl("lblServiceCode");
                    Label lblStartWt = (Label)dgItem.FindControl("lblStartWt");
                    Label lblEndWt = (Label)dgItem.FindControl("lblEndWt");
                    Label lblSPrice = (Label)dgItem.FindControl("lblSPrice");
                    Label lblIPrice = (Label)dgItem.FindControl("lblIPrice");
                    Label lblEffectivedate = (Label)dgItem.FindControl("lblEffectivedate");

                    string strOZ = "1";
                    string strDZ = "1";
                    string strSC = "1";
                    string strSwt = "1"; // ÁÕ¤èÒà»ç¹ 0 ä´é
                    string strEwt = "1";
                    string strSprice = "1";
                    string strIprice = "1";// ÁÕ¤èÒà»ç¹ 0 ä´é
                    string strEffDate = "1";

                    if (lblOZCode != null)
                    {
                        if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblOZCode.Text.ToString(), "Zone"))
                        {
                            strOZ = "0";
                        }
                        else if (lblOZCode.Text == "")
                        {
                            strOZ = "0";
                        }
                    }
                    if (lblDZCode != null)
                    {
                        if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblDZCode.Text.ToString(), "Zone"))
                        {
                            strDZ = "0";
                        }
                        else if (lblDZCode.Text == "")
                        {
                            strDZ = "0";
                        }
                    }
                    if (lblServiceCode != null)
                    {
                        if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblServiceCode.Text.ToString(), "ServiceType"))
                        {
                            strSC = "0";
                        }
                        else if (lblServiceCode.Text == "")
                        {
                            strSC = "0";
                        }
                    }

                    if (lblStartWt != null && lblEndWt != null)
                    {
                        if (Utility.isNumeric(lblStartWt.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float) && Utility.isNumeric(lblEndWt.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                        {
                            if (Convert.ToDouble(lblStartWt.Text) < 0)
                                strSwt = "0";
                            //							lblStartWt.Text.
                            if (Convert.ToDouble(lblStartWt.Text) >= Convert.ToDouble(lblEndWt.Text))
                                strEwt = "0";
                        }
                        else
                        {
                            strSwt = "0";
                            strEwt = "0";
                        }
                    }
                    if (lblSPrice != null)
                    {
                        if (Utility.isNumeric(lblSPrice.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                        {
                            if (Convert.ToDouble(lblSPrice.Text) <= 0)
                                strSprice = "0";
                        }
                        else
                        {
                            strSprice = "0";
                        }
                    }
                    if (lblIPrice != null)
                    {
                        if (Utility.isNumeric(lblIPrice.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                        {
                            if (Convert.ToDouble(lblIPrice.Text) < 0)
                                strIprice = "0";
                        }
                        else
                        {
                            strIprice = "0";
                        }
                    }
                    if (lblEffectivedate != null)
                    {
                        object objEffectiveDate = System.DBNull.Value;
                        objEffectiveDate = DateTime.ParseExact(lblEffectivedate.Text.ToString().Trim(), "dd/MM/yyyy", null);

                        if (!ckEffectiveDate(Convert.ToDateTime(objEffectiveDate)))
                            strEffDate = "0";
                    }

                    if (strOZ.Equals("0") || strDZ.Equals("0") || strSC.Equals("0") || strSwt.Equals("0") || strEwt.Equals("0") || strSprice.Equals("0") || strIprice.Equals("0") || strEffDate.Equals("0"))
                    {
                        imgSelect.ImageUrl = "images/butt-red.gif";
                        string strErrorToolTip = "";

                        if (strOZ.Equals("0"))
                            strErrorToolTip += "- Invalid Original Zode code\n";
                        if (strDZ.Equals("0"))
                            strErrorToolTip += "- Invalid Destination Zode code\n";
                        if (strSC.Equals("0"))
                            strErrorToolTip += "- Invalid Service Type\n";
                        if (strSwt.Equals("0"))
                            strErrorToolTip += "- Invalid Start Wt.\n";
                        if (strEwt.Equals("0"))
                            strErrorToolTip += "- Invalid End Wt.\n";
                        if (strSprice.Equals("0"))
                            strErrorToolTip += "- Invalid Start Price\n";
                        if (strIprice.Equals("0"))
                            strErrorToolTip += "- Invalid Increment Price\n";
                        if (strEffDate.Equals("0"))
                            strErrorToolTip += "- Invalid Effective Date\n";

                        //						if(strErrorToolTip.Length>0)
                        //							strErrorToolTip.Replace("  ","\n ");

                        imgSelect.ToolTip = strErrorToolTip;
                        statusButton = "1";
                    }
                    else
                    {
                        DataSet dsChangedRow = SysDataManager2.GetEmptyBZRatesDS().ds;
                        try
                        {

                            if (dsChangedRow.Tables[0].Rows.Count > 0)
                            {
                                dsChangedRow.Tables[0].Rows.RemoveAt(0);
                            }
                            DataRow dr = dsChangedRow.Tables[0].NewRow();

                            if (txtOZCode != null)
                                dr[0] = lblOZCode.Text;
                            if (txtDZCode != null)
                                dr[1] = lblDZCode.Text;
                            if (txtStartWt != null)
                                dr[2] = lblStartWt.Text;
                            if (txtEndWt != null)
                                dr[3] = lblEndWt.Text;
                            if (txtSPrice != null)
                                dr[4] = lblSPrice.Text;
                            if (txtIPrice != null)
                                dr[5] = lblIPrice.Text;
                            if (txtServiceCode != null)
                                dr[6] = lblServiceCode.Text;

                            if (strEffectiveDate != "")
                            {
                                dr[7] = DateTime.ParseExact(lblEffectivedate.Text, "dd/MM/yyyy", null);
                            }
                            else
                            {
                                dr[7] = System.DBNull.Value;
                            }

                            dsChangedRow.Tables[0].Rows.Add(dr);

                        }
                        catch (Exception ex)
                        {
                            lblErrorMessage.Text = ex.Message;
                        }

                        DataSet dsBZRates = SysDataManager2.GetBZRatesDS(dsChangedRow, utility.GetAppID(), utility.GetEnterpriseID());

                        //(utility.GetAppID(),utility.GetEnterpriseID(),
                        //			lblOZCode.Text,lblDZCode.Text,Convert.ToInt16(lblStartWt.Text),Convert.ToInt16(lblEndWt.Text),Convert.ToInt16(lblSPrice.Text),Convert.ToInt16(lblIPrice.Text),lblServiceCode.Text,lblEffectivedate.Text);
                        if (dsBZRates.Tables[0].Rows.Count > 0)
                        {
                            imgSelect.ToolTip = "Duplicate Data";
                            imgSelect.ImageUrl = "images/butt-red.gif";
                            statusButton = "1";
                        }
                        else
                            imgSelect.ImageUrl = "images/butt-select.gif";
                    }

                    if (statusButton.Equals("1"))
                        this.btnImport_Save.Enabled = false;
                    else
                    {
                        this.btnImport_Save.Enabled = true;
                        this.btnImport_Save.Attributes.Add("onclick", "javascript:showSaveProgress();");
                    }   
                }

                return;
            }

            //END IN THE IMPORT MODE
            //if (btnExecuteQuery.Visible == false)
            else
            {

                int iRowsAffected = 0;
                int intChkRange = 1;
                int rowIndex = e.Item.ItemIndex;
                GetChangedRow(e.Item, e.Item.ItemIndex);
                int iOperation = (int)ViewState["Operation"];
                //int iMode = (int)ViewState["Mode"];

                try
                {
                    msTextBox txtOZCode = (msTextBox)e.Item.FindControl("txtOZCode");
                    msTextBox txtDZCode = (msTextBox)e.Item.FindControl("txtDZCode");
                    msTextBox txtStartWt = (msTextBox)e.Item.FindControl("txtStartWt");
                    msTextBox txtEndWt = (msTextBox)e.Item.FindControl("txtEndWt");
                    msTextBox txtSPrice = (msTextBox)e.Item.FindControl("txtSPrice");
                    msTextBox txtIPrice = (msTextBox)e.Item.FindControl("txtIPrice");
                    msTextBox txtServiceCode = (msTextBox)e.Item.FindControl("txtServiceCode");
                    msTextBox txtEffectiveDate = (msTextBox)e.Item.FindControl("txtEffectiveDate");



                    if (Convert.ToDouble(txtStartWt.Text) >= Convert.ToDouble(txtEndWt.Text))
                    {
                        lblErrorMessage.Text = "Start Weight must be less than End Wieght.";
                        return;
                    }

                    if (Convert.ToDouble(txtStartWt.Text) < 0)
                    {
                        lblErrorMessage.Text = "Start Weight must not be less than zero.";
                        return;
                    }

                    if (Convert.ToDouble(txtSPrice.Text) <= 0)
                    {
                        lblErrorMessage.Text = "Start Price must be more than zero.";
                        return;
                    }

                    if (Convert.ToDouble(txtIPrice.Text) < 0)
                    {
                        lblErrorMessage.Text = "Increment Price must not be less than zero.";
                        return;
                    }

                    object objEffectiveDate = System.DBNull.Value;
                    if (txtEffectiveDate != null)
                    {
                        objEffectiveDate = DateTime.ParseExact(txtEffectiveDate.Text.ToString().Trim(), "dd/MM/yyyy", null);
                    }

                    System.IFormatProvider format = new System.Globalization.CultureInfo("en-US");


                    if (!ckEffectiveDate(Convert.ToDateTime(objEffectiveDate)))
                    {
                        DateTime dStart = (DateTime)ViewState["effStart"];
                        DateTime dEnd = (DateTime)ViewState["effEnd"];
                        lblErrorMessage.Text = "Effective date must between " + dStart.ToString("dd/MM/yyyy") + " and " + dEnd.ToString("dd/MM/yyyy") + " ";
                        return;
                    }

                    if (iOperation == (int)Operation.Insert)
                    {

                        iRowsAffected = SysDataManager2.InsertBZRates(m_sdsBZRates.ds, rowIndex, m_strAppID, m_strEnterpriseID);

                        if (iRowsAffected > 0)
                        {
                            ArrayList paramValues = new ArrayList();
                            paramValues.Add("" + iRowsAffected);
                            lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ROWS_INS", utility.GetUserCulture(), paramValues);

                            QueryAllData();
                        }
                        else
                        {
                            lblErrorMessage.Text = "Cannot to insert.";
                            return;
                        }

                    }
                    else
                    {
                        //						DataSet dsChangedRow = m_sdsBZRates.ds.GetChanges();
                        //						iRowsAffected = SysDataManager2.UpdateBZRates(dsChangedRow, m_strAppID, m_strEnterpriseID);
                        //						ArrayList paramValues = new ArrayList();
                        //						paramValues.Add(""+iRowsAffected);						
                        //						lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_UPD",utility.GetUserCulture(),paramValues);
                        //						m_sdsBZRates.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();



                        DataSet dsChangedRow = SysDataManager2.GetEmptyBZRatesDS().ds;

                        if (dsChangedRow.Tables[0].Rows.Count > 0)
                        {
                            dsChangedRow.Tables[0].Rows.RemoveAt(0);
                        }
                        DataRow dr = dsChangedRow.Tables[0].NewRow();

                        if (txtOZCode != null)
                            dr[0] = txtOZCode.Text;
                        if (txtDZCode != null)
                            dr[1] = txtDZCode.Text;
                        if (txtStartWt != null)
                            dr[2] = txtStartWt.Text;
                        if (txtEndWt != null)
                            dr[3] = txtEndWt.Text;
                        if (txtSPrice != null)
                            dr[4] = txtSPrice.Text;
                        if (txtIPrice != null)
                            dr[5] = txtIPrice.Text;
                        if (txtServiceCode != null)
                            dr[6] = txtServiceCode.Text;
                        if (txtEffectiveDate != null)
                            dr[7] = DateTime.ParseExact(txtEffectiveDate.Text.ToString().Trim(), "dd/MM/yyyy", null);

                        Response.Write(dr[0].ToString() + "," + dr[1].ToString() + "," + dr[2].ToString() + "," + dr[3].ToString() + "," + dr[4].ToString() + "," + dr[5].ToString() + "," + dr[6].ToString() + "," + dr[7].ToString());

                        dsChangedRow.Tables[0].Rows.Add(dr);

                        iRowsAffected = SysDataManager2.InsertBZRates(dsChangedRow, 0, m_strAppID, m_strEnterpriseID);

                        if (iRowsAffected > 0)
                        {
                            ArrayList paramValues = new ArrayList();
                            paramValues.Add("" + iRowsAffected);
                            lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ROWS_INS", utility.GetUserCulture(), paramValues);

                            QueryAllData();
                        }
                        else
                        {
                            lblErrorMessage.Text = "Cannot to update.";
                            return;
                        }
                    }
                }
                catch (ApplicationException appException)
                {
                    String strMsg = appException.Message;
                    strMsg = appException.InnerException.Message;
                    strMsg = appException.InnerException.InnerException.Message;
                    if (strMsg.IndexOf("duplicate key") != -1 || strMsg.IndexOf("PRIMARY") != -1)
                        lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "DUP_KEY_FOUND", utility.GetUserCulture());
                    if (strMsg.IndexOf("unique") != -1)
                        lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "DUP_KEY_FOUND", utility.GetUserCulture());
                    else if (strMsg.IndexOf("FOREIGN KEY") != -1)
                        lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "NO_ZIP_MSTR", utility.GetUserCulture());
                    else if (strMsg.IndexOf("parent key not found") != -1)
                    {
                        lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "NO_ZIP_MSTR", utility.GetUserCulture());
                    }
                    return;
                }

                if (iRowsAffected == 0)
                {
                    return;
                }
                Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                ViewState["Operation"] = Operation.None;

                if (intChkRange >= 0)
                {
                    dgBZRates.EditItemIndex = -1;
                }

                BindGrid();
                Logger.LogDebugInfo("BaseZoneRates", "dgBZRates_Update", "INF004", "updating data grid...");
            }
        }

        protected void QueryAllData()
        {
            //			DataSet dsAll = (DataSet)Session["QUERY_DS"];
            //			m_sdsBZRates = (SessionDS)SysDataManager2.GetBZRatesDS(dsAll, 0,utility.GetAppID(), utility.GetEnterpriseID());
            //			Session["dsCustZones"] = dsAll;
            //			dgBZRates.EditItemIndex = -1;
            //			BindGrid();
            //			Session["SESSION_DS1"] = m_sdsBZRates;
            //			btnExecuteQuery.Enabled = false;
            //
            //			dgBZRates.Columns[0].Visible = false;  
            //			dgBZRates.Columns[1].Visible = true;  
            //			dgBZRates.Columns[2].Visible = true; 


            int iStartRow = dgBZRates.CurrentPageIndex * dgBZRates.PageSize;
            DataSet dsQuery = new DataSet();
            dsQuery = (DataSet)SysDataManager2.GetEmptyBZRatesDS().ds;
            //			if(dsQuery.Tables[0].Rows.Count > 0)
            //			{
            //				Response.Write("<script>alert('"+dsQuery.Tables[0].Rows.Count.ToString()+"');</script>");
            //				return;
            //			}
            m_sdsBZRates = SysDataManager2.GetBZRatesDS(utility.GetAppID(), utility.GetEnterpriseID(), iStartRow, dgBZRates.PageSize, dsQuery);
            int iPageCnt = Convert.ToInt32((m_sdsBZRates.QueryResultMaxSize - 1)) / dgBZRates.PageSize;
            if (iPageCnt < dgBZRates.CurrentPageIndex)
            {
                dgBZRates.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
            }
            dgBZRates.SelectedIndex = -1;
            dgBZRates.EditItemIndex = -1;

            BindGrid();
            Session["SESSION_DS1"] = m_sdsBZRates;
        }

        protected void dgBZRates_Cancel(object sender, DataGridCommandEventArgs e)
        {

            lblErrorMessage.Text = "";
            int rowIndex = e.Item.ItemIndex;

            if (this.btnImport_Save.Visible == true)
            {
                dgBZRates.EditItemIndex = -1;
                BindGrid();

                dgBZRates.Columns[0].Visible = true;
                dgBZRates.Columns[1].Visible = true;
                dgBZRates.Columns[2].Visible = true;

                foreach (DataGridItem dgItem in dgBZRates.Items)
                {
                    DataSet dsZone = new DataSet();

                    ImageButton imgSelect = (ImageButton)dgItem.Cells[0].Controls[1];
                    Label lblOZCode = (Label)dgItem.FindControl("lblOZCode");
                    Label lblDZCode = (Label)dgItem.FindControl("lblDZCode");
                    Label lblServiceCode = (Label)dgItem.FindControl("lblServiceCode");
                    Label lblStartWt = (Label)dgItem.FindControl("lblStartWt");
                    Label lblEndWt = (Label)dgItem.FindControl("lblEndWt");
                    Label lblSPrice = (Label)dgItem.FindControl("lblSPrice");
                    Label lblIPrice = (Label)dgItem.FindControl("lblIPrice");
                    Label lblEffectivedate = (Label)dgItem.FindControl("lblEffectivedate");

                    string strOZ = "1";
                    string strDZ = "1";
                    string strSC = "1";
                    string strSwt = "1"; // ÁÕ¤èÒà»ç¹ 0 ä´é
                    string strEwt = "1";
                    string strSprice = "1";
                    string strIprice = "1";// ÁÕ¤èÒà»ç¹ 0 ä´é
                    string strEffDate = "1";

                    string statusButton = "0";

                    if (lblOZCode != null)
                    {
                        if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblOZCode.Text.ToString(), "Zone"))
                        {
                            strOZ = "0";
                        }
                        else if (lblOZCode.Text == "")
                        {
                            strOZ = "0";
                        }
                    }
                    if (lblDZCode != null)
                    {
                        if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblDZCode.Text.ToString(), "Zone"))
                        {
                            strDZ = "0";
                        }
                        else if (lblDZCode.Text == "")
                        {
                            strDZ = "0";
                        }
                    }
                    if (lblServiceCode != null)
                    {
                        if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblServiceCode.Text.ToString(), "ServiceType"))
                        {
                            strSC = "0";
                        }
                        else if (lblServiceCode.Text == "")
                        {
                            strSC = "0";
                        }
                    }

                    if (lblStartWt != null && lblEndWt != null)
                    {
                        if (Utility.isNumeric(lblStartWt.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float) && Utility.isNumeric(lblEndWt.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                        {
                            if (Convert.ToDouble(lblStartWt.Text) < 0)
                                strSwt = "0";
                            //							lblStartWt.Text.
                            if (Convert.ToDouble(lblStartWt.Text) >= Convert.ToDouble(lblEndWt.Text))
                                strEwt = "0";
                        }
                        else
                        {
                            strSwt = "0";
                            strEwt = "0";
                        }
                    }
                    if (lblSPrice != null)
                    {
                        if (Utility.isNumeric(lblSPrice.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                        {
                            if (Convert.ToDouble(lblSPrice.Text) <= 0)
                                strSprice = "0";
                        }
                        else
                        {
                            strSprice = "0";
                        }
                    }
                    if (lblIPrice != null)
                    {
                        if (Utility.isNumeric(lblIPrice.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                        {
                            if (Convert.ToDouble(lblIPrice.Text) < 0)
                                strIprice = "0";
                        }
                        else
                        {
                            strIprice = "0";
                        }
                    }
                    if (lblEffectivedate != null)
                    {
                        object objEffectiveDate = System.DBNull.Value;
                        objEffectiveDate = DateTime.ParseExact(lblEffectivedate.Text.ToString().Trim(), "dd/MM/yyyy", null);

                        if (!ckEffectiveDate(Convert.ToDateTime(objEffectiveDate)))
                            strEffDate = "0";
                    }

                    if (strOZ.Equals("0") || strDZ.Equals("0") || strSC.Equals("0") || strSwt.Equals("0") || strEwt.Equals("0") || strSprice.Equals("0") || strIprice.Equals("0") || strEffDate.Equals("0"))
                    {
                        imgSelect.ImageUrl = "images/butt-red.gif";
                        string strErrorToolTip = "";

                        if (strOZ.Equals("0"))
                            strErrorToolTip += "- Invalid Original Zode code\n";
                        if (strDZ.Equals("0"))
                            strErrorToolTip += "- Invalid Destination Zode code\n";
                        if (strSC.Equals("0"))
                            strErrorToolTip += "- Invalid Service Type\n";
                        if (strSwt.Equals("0"))
                            strErrorToolTip += "- Invalid Start Wt.\n";
                        if (strEwt.Equals("0"))
                            strErrorToolTip += "- Invalid End Wt.\n";
                        if (strSprice.Equals("0"))
                            strErrorToolTip += "- Invalid Start Price\n";
                        if (strIprice.Equals("0"))
                            strErrorToolTip += "- Invalid Increment Price\n";
                        if (strEffDate.Equals("0"))
                            strErrorToolTip += "- Invalid Effective Date\n";

                        //						if(strErrorToolTip.Length>0)
                        //							strErrorToolTip.Replace("  ","\n ");

                        imgSelect.ToolTip = strErrorToolTip;

                        statusButton = "1";
                    }
                    else
                    {
                        imgSelect.ImageUrl = "images/butt-select.gif";
                    }
                    if (statusButton.Equals("1"))
                        this.btnImport_Save.Enabled = false;
                    else
                    {
                        this.btnImport_Save.Enabled = true;
                        this.btnImport_Save.Attributes.Add("onclick", "javascript:showSaveProgress();");
                    }
                }
                return;
            }

            if ((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int)ViewState["Operation"] == (int)Operation.Insert)
            {
                Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
                m_sdsBZRates.ds.Tables[0].Rows.RemoveAt(rowIndex);
            }
            else
            {
                QueryAllData();
            }
            ViewState["Operation"] = Operation.None;
            dgBZRates.EditItemIndex = -1;
            BindGrid();
            Logger.LogDebugInfo("BaseZoneRates", "dgBZRates_Edit", "INF004", "updating data grid...");
        }

        protected void dgBZRates_Edit(object sender, DataGridCommandEventArgs e)
        {
            if (this.btnImport_Save.Visible == true)
            {
                DataSet dsBaseRates_Import = (DataSet)Session["dsBaseRates_Import"];

                //dsBaseRates_Import.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
                //m_sdsBZRates.QueryResultMaxSize--;
                dgBZRates.CurrentPageIndex = 0;

                dgBZRates.EditItemIndex = e.Item.ItemIndex;
                ViewState["Operation"] = Operation.Update;
                Session["dsBaseRates_Import"] = dsBaseRates_Import;
                BindGrid();

                dgBZRates.Columns[0].Visible = false;
                dgBZRates.Columns[2].Visible = false;
                return;
            }

            if ((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int)ViewState["Operation"] == (int)Operation.Insert && dgBZRates.EditItemIndex > 0)
            {
                m_sdsBZRates.ds.Tables[0].Rows.RemoveAt(dgBZRates.EditItemIndex);
                m_sdsBZRates.QueryResultMaxSize--;
                dgBZRates.CurrentPageIndex = 0;
            }
            dgBZRates.EditItemIndex = e.Item.ItemIndex;
            ViewState["RowIndexUpdate"] = e.Item.ItemIndex;
            ViewState["Operation"] = Operation.Update;
            BindGrid();
            Logger.LogDebugInfo("BaseZoneRates", "dgBZRates_Edit", "INF004", "updating data grid...");
        }

        protected void dgBZRates_Delete(object sender, DataGridCommandEventArgs e)
        {
            if (this.btnImport.Visible == true)
            {
                DataSet dsBaseRate = new DataSet();
                dsBaseRate = (DataSet)Session["dsBaseRates_Import"];

                dsBaseRate.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
                dsBaseRate.AcceptChanges();

                Session["dsBaseRates_Import"] = dsBaseRate;
                //dgCustZones.EditItemIndex = -1;
                this.BindGrid();

                dgBZRates.Columns[0].Visible = true;
                dgBZRates.Columns[1].Visible = true;
                dgBZRates.Columns[2].Visible = true;

                string statusButton = "0";

                foreach (DataGridItem dgItem in dgBZRates.Items)
                {
                    DataSet dsZone = new DataSet();

                    ImageButton imgSelect = (ImageButton)dgItem.Cells[0].Controls[1];
                    Label lblOZCode = (Label)dgItem.FindControl("lblOZCode");
                    Label lblDZCode = (Label)dgItem.FindControl("lblDZCode");
                    Label lblServiceCode = (Label)dgItem.FindControl("lblServiceCode");
                    Label lblStartWt = (Label)dgItem.FindControl("lblStartWt");
                    Label lblEndWt = (Label)dgItem.FindControl("lblEndWt");
                    Label lblSPrice = (Label)dgItem.FindControl("lblSPrice");
                    Label lblIPrice = (Label)dgItem.FindControl("lblIPrice");
                    Label lblEffectivedate = (Label)dgItem.FindControl("lblEffectivedate");

                    string strOZ = "1";
                    string strDZ = "1";
                    string strSC = "1";
                    string strSwt = "1"; // ÁÕ¤èÒà»ç¹ 0 ä´é
                    string strEwt = "1";
                    string strSprice = "1";
                    string strIprice = "1";// ÁÕ¤èÒà»ç¹ 0 ä´é
                    string strEffDate = "1";

                    if (lblOZCode != null)
                    {
                        if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblOZCode.Text.ToString(), "Zone"))
                        {
                            strOZ = "0";
                        }
                        else if (lblOZCode.Text == "")
                        {
                            strOZ = "0";
                        }
                    }
                    if (lblDZCode != null)
                    {
                        if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblDZCode.Text.ToString(), "Zone"))
                        {
                            strDZ = "0";
                        }
                        else if (lblDZCode.Text == "")
                        {
                            strDZ = "0";
                        }
                    }
                    if (lblServiceCode != null)
                    {
                        if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblServiceCode.Text.ToString(), "ServiceType"))
                        {
                            strSC = "0";
                        }
                        else if (lblServiceCode.Text == "")
                        {
                            strSC = "0";
                        }
                    }

                    if (lblStartWt != null && lblEndWt != null)
                    {
                        if (Utility.isNumeric(lblStartWt.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float) && Utility.isNumeric(lblEndWt.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                        {
                            if (Convert.ToDouble(lblStartWt.Text) < 0)
                                strSwt = "0";
                            //							lblStartWt.Text.
                            if (Convert.ToDouble(lblStartWt.Text) >= Convert.ToDouble(lblEndWt.Text))
                                strEwt = "0";
                        }
                        else
                        {
                            strSwt = "0";
                            strEwt = "0";
                        }
                    }
                    if (lblSPrice != null)
                    {
                        if (Utility.isNumeric(lblSPrice.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                        {
                            if (Convert.ToDouble(lblSPrice.Text) <= 0)
                                strSprice = "0";
                        }
                        else
                        {
                            strSprice = "0";
                        }
                    }
                    if (lblIPrice != null)
                    {
                        if (Utility.isNumeric(lblIPrice.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                        {
                            if (Convert.ToDouble(lblIPrice.Text) < 0)
                                strIprice = "0";
                        }
                        else
                        {
                            strIprice = "0";
                        }
                    }
                    if (lblEffectivedate != null)
                    {
                        object objEffectiveDate = System.DBNull.Value;
                        objEffectiveDate = DateTime.ParseExact(lblEffectivedate.Text.ToString().Trim(), "dd/MM/yyyy", null);

                        if (!ckEffectiveDate(Convert.ToDateTime(objEffectiveDate)))
                            strEffDate = "0";
                    }

                    if (strOZ.Equals("0") || strDZ.Equals("0") || strSC.Equals("0") || strSwt.Equals("0") || strEwt.Equals("0") || strSprice.Equals("0") || strIprice.Equals("0") || strEffDate.Equals("0"))
                    {
                        imgSelect.ImageUrl = "images/butt-red.gif";
                        string strErrorToolTip = "";

                        if (strOZ.Equals("0"))
                            strErrorToolTip += "- Invalid Original Zode code\n";
                        if (strDZ.Equals("0"))
                            strErrorToolTip += "- Invalid Destination Zode code\n";
                        if (strSC.Equals("0"))
                            strErrorToolTip += "- Invalid Service Type\n";
                        if (strSwt.Equals("0"))
                            strErrorToolTip += "- Invalid Start Wt.\n";
                        if (strEwt.Equals("0"))
                            strErrorToolTip += "- Invalid End Wt.\n";
                        if (strSprice.Equals("0"))
                            strErrorToolTip += "- Invalid Start Price\n";
                        if (strIprice.Equals("0"))
                            strErrorToolTip += "- Invalid Increment Price\n";
                        if (strEffDate.Equals("0"))
                            strErrorToolTip += "- Invalid Effective Date\n";

                        //						if(strErrorToolTip.Length>0)
                        //							strErrorToolTip.Replace("  ","\n ");

                        imgSelect.ToolTip = strErrorToolTip;

                        statusButton = "1";
                    }
                    else
                    {
                        DataSet dsChangedRow = SysDataManager2.GetEmptyBZRatesDS().ds;
                        try
                        {

                            if (dsChangedRow.Tables[0].Rows.Count > 0)
                            {
                                dsChangedRow.Tables[0].Rows.RemoveAt(0);
                            }
                            DataRow dr = dsChangedRow.Tables[0].NewRow();

                            dr[0] = lblOZCode.Text;
                            dr[1] = lblDZCode.Text;
                            dr[2] = lblStartWt.Text;
                            dr[3] = lblEndWt.Text;
                            dr[4] = lblSPrice.Text;
                            dr[5] = lblIPrice.Text;
                            dr[6] = lblServiceCode.Text;
                            dr[7] = DateTime.ParseExact(lblEffectivedate.Text, "dd/MM/yyyy", null);

                            dsChangedRow.Tables[0].Rows.Add(dr);

                        }
                        catch (Exception ex)
                        {
                            lblErrorMessage.Text = ex.Message;
                        }

                        DataSet dsBZRates = SysDataManager2.GetBZRatesDS(dsChangedRow, utility.GetAppID(), utility.GetEnterpriseID());

                        if (dsBZRates.Tables[0].Rows.Count > 0)
                        {
                            imgSelect.ToolTip = "Duplicate Data";
                            imgSelect.ImageUrl = "images/butt-red.gif";
                            statusButton = "1";
                        }
                        else
                            imgSelect.ImageUrl = "images/butt-select.gif";
                    }

                    if (statusButton.Equals("1"))
                        this.btnImport_Save.Enabled = false;
                    else
                    {
                        this.btnImport_Save.Enabled = true;
                        this.btnImport_Save.Attributes.Add("onclick", "javascript:showSaveProgress();");
                    }
                }
                return;
            }
            if (!btnExecuteQuery.Enabled)
            {
                if ((int)ViewState["Operation"] == (int)Operation.Update)
                {
                    dgBZRates.EditItemIndex = -1;
                }
                BindGrid();
                int rowIndex = e.Item.ItemIndex;
                m_sdsBZRates = (SessionDS)Session["SESSION_DS1"];

                try
                {
                    // delete from table
                    int iRowsDeleted = SysDataManager2.DeleteBZRates(m_sdsBZRates.ds, rowIndex, m_strAppID, m_strEnterpriseID);
                    ArrayList paramValues = new ArrayList();
                    paramValues.Add("" + iRowsDeleted);
                    lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ROWS_DLD", utility.GetUserCulture(), paramValues);
                }
                catch (ApplicationException appException)
                {
                    String strMsg = appException.Message;
                    if (strMsg.IndexOf("FK") != -1)
                    {
                        lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ERR_DEL_ZONE_TRANS", utility.GetUserCulture());
                    }
                    if (strMsg.IndexOf("child record") != -1)
                    {
                        lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ERR_DEL_ZONE_TRANS", utility.GetUserCulture());
                    }
                    else
                    {
                        lblErrorMessage.Text = strMsg;
                    }

                    Logger.LogTraceError("BaseZoneRates.aspx.cs", "dgBZRates_Delete", "RBAC003", appException.Message.ToString());
                    return;
                }

                // remove the data from grid
                if ((int)ViewState["Mode"] == (int)ScreenMode.Insert)
                    m_sdsBZRates.ds.Tables[0].Rows.RemoveAt(rowIndex);
                else
                {
                    RetreiveSelectedPage();
                }
                ViewState["Operation"] = Operation.None;
                //Make the row in non-edit Mode
                EditRow(false);
                BindGrid();
                Logger.LogDebugInfo("BaseZoneRates", "dgBZRates_Delete", "INF004", "updating data grid...");
            }
        }

        protected void dgBZRates_Bound(object sender, DataGridItemEventArgs e)
        {
            //if (this.btnImport.Visible == false)
            if (this.btnNewImport.Visible == false)
            {
                if (e.Item.ItemIndex == -1)
                {
                    return;
                }

                DataRow drSelected = m_sdsBZRates.ds.Tables[0].Rows[e.Item.ItemIndex];
                if (drSelected.RowState == DataRowState.Deleted)
                    return;

                if ((int)ViewState["Operation"] != (int)Operation.Insert && (!btnExecuteQuery.Enabled))
                {
                    msTextBox txtOZCode = (msTextBox)e.Item.FindControl("txtOZCode");
                    msTextBox txtDZCode = (msTextBox)e.Item.FindControl("txtDZCode");
                    msTextBox txtStartWt = (msTextBox)e.Item.FindControl("txtStartWt");
                    msTextBox txtEndWt = (msTextBox)e.Item.FindControl("txtEndWt");
                    msTextBox txtServiceCode = (msTextBox)e.Item.FindControl("txtServiceCode");

                    if (txtOZCode != null && txtDZCode != null)
                    {
                        //					txtOZCode.Enabled = false;
                        //					txtDZCode.Enabled = false;
                        //					txtStartWt.Enabled = false;
                        //					txtEndWt.Enabled = false;
                        //					txtServiceCode.Enabled = false;
                    }
                }
                if ((int)ViewState["Mode"] == (int)ScreenMode.Insert && (int)ViewState["Operation"] == (int)Operation.Insert)
                {
                    e.Item.Cells[1].Enabled = true;
                    e.Item.Cells[2].Enabled = false;
                }
            }
        }

        protected void dgBZRates_PageChange(object sender, DataGridPageChangedEventArgs e)
        {
            //RetreiveSelectedPage();
            //Logger.LogDebugInfo("BaseZoneRates","dgBZRates_PageChange","INF009","On Page Change "+e.NewPageIndex);

            dgBZRates.Columns[1].Visible = true;
            dgBZRates.Columns[2].Visible = true;

            if (this.btnImport_Save.Visible == true)
            {
                dgBZRates.CurrentPageIndex = e.NewPageIndex;
                dgBZRates.EditItemIndex = -1;
                this.BindGrid();
                this.dgBZRates.AllowCustomPaging = false;
                dgBZRates.Columns[0].Visible = true;
                foreach (DataGridItem dgItem in dgBZRates.Items)
                {
                    DataSet dsZone = new DataSet();

                    ImageButton imgSelect = (ImageButton)dgItem.Cells[0].Controls[1];
                    Label lblOZCode = (Label)dgItem.FindControl("lblOZCode");
                    Label lblDZCode = (Label)dgItem.FindControl("lblDZCode");
                    Label lblServiceCode = (Label)dgItem.FindControl("lblServiceCode");
                    Label lblStartWt = (Label)dgItem.FindControl("lblStartWt");
                    Label lblEndWt = (Label)dgItem.FindControl("lblEndWt");
                    Label lblSPrice = (Label)dgItem.FindControl("lblSPrice");
                    Label lblIPrice = (Label)dgItem.FindControl("lblIPrice");
                    Label lblEffectivedate = (Label)dgItem.FindControl("lblEffectivedate");

                    string strOZ = "1";
                    string strDZ = "1";
                    string strSC = "1";
                    string strSwt = "1"; // ÁÕ¤èÒà»ç¹ 0 ä´é
                    string strEwt = "1";
                    string strSprice = "1";
                    string strIprice = "1";// ÁÕ¤èÒà»ç¹ 0 ä´é
                    string strEffDate = "1";


                    string statusButton = "0";

                    if (lblOZCode != null)
                    {
                        if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblOZCode.Text.ToString(), "Zone"))
                        {
                            strOZ = "0";
                        }
                        else if (lblOZCode.Text == "")
                        {
                            strOZ = "0";
                        }
                    }
                    if (lblDZCode != null)
                    {
                        if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblDZCode.Text.ToString(), "Zone"))
                        {
                            strDZ = "0";
                        }
                        else if (lblDZCode.Text == "")
                        {
                            strDZ = "0";
                        }
                    }
                    if (lblServiceCode != null)
                    {
                        if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblServiceCode.Text.ToString(), "ServiceType"))
                        {
                            strSC = "0";
                        }
                        else if (lblServiceCode.Text == "")
                        {
                            strSC = "0";
                        }
                    }

                    if (lblStartWt != null && lblEndWt != null)
                    {
                        if (Utility.isNumeric(lblStartWt.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float) && Utility.isNumeric(lblEndWt.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                        {
                            if (Convert.ToDouble(lblStartWt.Text) < 0)
                                strSwt = "0";
                            //							lblStartWt.Text.
                            if (Convert.ToDouble(lblStartWt.Text) >= Convert.ToDouble(lblEndWt.Text))
                                strEwt = "0";
                        }
                        else
                        {
                            strSwt = "0";
                            strEwt = "0";
                        }
                    }
                    if (lblSPrice != null)
                    {
                        if (Utility.isNumeric(lblSPrice.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                        {
                            if (Convert.ToDouble(lblSPrice.Text) <= 0)
                                strSprice = "0";
                        }
                        else
                        {
                            strSprice = "0";
                        }
                    }
                    if (lblIPrice != null)
                    {
                        if (Utility.isNumeric(lblIPrice.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                        {
                            if (Convert.ToDouble(lblIPrice.Text) < 0)
                                strIprice = "0";
                        }
                        else
                        {
                            strIprice = "0";
                        }
                    }
                    if (lblEffectivedate != null)
                    {
                        object objEffectiveDate = System.DBNull.Value;
                        //Response.Write("<script>alert('"+lblEffectivedate.Text.ToString().Trim()+"');</script>");
                        objEffectiveDate = DateTime.ParseExact(lblEffectivedate.Text.ToString().Trim(), "dd/MM/yyyy", null);

                        if (!ckEffectiveDate(Convert.ToDateTime(objEffectiveDate)))
                            strEffDate = "0";
                    }

                    if (strOZ.Equals("0") || strDZ.Equals("0") || strSC.Equals("0") || strSwt.Equals("0") || strEwt.Equals("0") || strSprice.Equals("0") || strIprice.Equals("0") || strEffDate.Equals("0"))
                    {
                        imgSelect.ImageUrl = "images/butt-red.gif";
                        string strErrorToolTip = "";

                        if (strOZ.Equals("0"))
                            strErrorToolTip += "- Invalid Original Zode code \n";
                        if (strDZ.Equals("0"))
                            strErrorToolTip += "- Invalid Destination Zode code \n";
                        if (strSC.Equals("0"))
                            strErrorToolTip += "- Invalid Service Type \n";
                        if (strSwt.Equals("0"))
                            strErrorToolTip += "- Invalid Start Wt. \n";
                        if (strEwt.Equals("0"))
                            strErrorToolTip += "- Invalid End Wt.\n";
                        if (strSprice.Equals("0"))
                            strErrorToolTip += "- Invalid Start Price\n";
                        if (strIprice.Equals("0"))
                            strErrorToolTip += "- Invalid Increment Price\n";
                        if (strEffDate.Equals("0"))
                            strErrorToolTip += "- Invalid Effective Date\n";

                        //						if(strErrorToolTip.Length>0)
                        //							strErrorToolTip.Replace("  ","\n ");

                        imgSelect.ToolTip = strErrorToolTip;

                        statusButton = "1";
                    }
                    else
                    {
                        //new
                        DataSet dsChangedRow = SysDataManager2.GetEmptyBZRatesDS().ds;
                        try
                        {

                            if (dsChangedRow.Tables[0].Rows.Count > 0)
                            {
                                dsChangedRow.Tables[0].Rows.RemoveAt(0);
                            }
                            DataRow dr = dsChangedRow.Tables[0].NewRow();

                            if (lblOZCode != null)
                                dr[0] = lblOZCode.Text;
                            if (lblDZCode != null)
                                dr[1] = lblDZCode.Text;
                            if (lblStartWt != null)
                                dr[2] = lblStartWt.Text;
                            if (lblEndWt != null)
                                dr[3] = lblEndWt.Text;
                            if (lblSPrice != null)
                                dr[4] = lblSPrice.Text;
                            if (lblIPrice != null)
                                dr[5] = lblIPrice.Text;
                            if (lblServiceCode != null)
                                dr[6] = lblServiceCode.Text;

                            if (lblEffectivedate.Text != "")
                            {
                                dr[7] = DateTime.ParseExact(lblEffectivedate.Text, "dd/MM/yyyy", null);
                            }
                            else
                            {
                                dr[7] = System.DBNull.Value;
                            }

                            dsChangedRow.Tables[0].Rows.Add(dr);

                        }
                        catch (Exception ex)
                        {
                            lblErrorMessage.Text = ex.Message;
                        }

                        DataSet dsBZRates = SysDataManager2.GetBZRatesDS(dsChangedRow, utility.GetAppID(), utility.GetEnterpriseID());

                        //(utility.GetAppID(),utility.GetEnterpriseID(),
                        //			lblOZCode.Text,lblDZCode.Text,Convert.ToInt16(lblStartWt.Text),Convert.ToInt16(lblEndWt.Text),Convert.ToInt16(lblSPrice.Text),Convert.ToInt16(lblIPrice.Text),lblServiceCode.Text,lblEffectivedate.Text);
                        if (dsBZRates.Tables[0].Rows.Count > 0)
                        {
                            imgSelect.ToolTip = "Duplicate Data";
                            imgSelect.ImageUrl = "images/butt-red.gif";
                            statusButton = "1";
                        }
                        else
                            imgSelect.ImageUrl = "images/butt-select.gif";
                        //end new
                    }
                    if (statusButton.Equals("1"))
                        this.btnImport_Save.Enabled = false;
                    else
                    {
                        this.btnImport_Save.Enabled = true;
                        this.btnImport_Save.Attributes.Add("onclick", "javascript:showSaveProgress();");
                    }
                }
            }
            else
            {

                dgBZRates.CurrentPageIndex = e.NewPageIndex;
                dgBZRates.VirtualItemCount = System.Convert.ToInt32(m_sdsBZRates.QueryResultMaxSize);
                this.dgBZRates.Columns[0].Visible = false;
                RetreiveSelectedPage();
                this.dgBZRates.AllowCustomPaging = true;
                Logger.LogDebugInfo("BaseZoneRates", "dgBZRates_PageChange", "INF009", "On Page Change " + e.NewPageIndex);
            }
            //return;
        }

        private void btnQuery_Click(object sender, System.EventArgs e)
        {
            lblErrorMessage.Text = "";
            dgBZRates.CurrentPageIndex = 0;
            dgBZRates.Columns[0].Visible = false;
            dgBZRates.Columns[1].Visible = false;
            dgBZRates.Columns[2].Visible = false;
            QueryMode();
        }

        private void btnExecuteQuery_Click(object sender, System.EventArgs e)
        {
            //Since Query mode is always one row in edit mode, the rowindex is set to zero
            if (this.btnImport_Save.Visible != true)
            {
                this.dgBZRates.AllowCustomPaging = true;
            }
            else
            {
                this.dgBZRates.AllowCustomPaging = false;
            }
            int rowIndex = 0;
            GetChangedRow(dgBZRates.Items[0], rowIndex);
            Session["QUERY_DS"] = m_sdsBZRates.ds;
            RetreiveSelectedPage();
            //			this.BindGrid();
            lblErrorMessage.Text = "";
            btnExecuteQuery.Enabled = false;
            //set all records to non-edit mode after execute query
            EditRow(false);
            dgBZRates.Columns[0].Visible = false;
            dgBZRates.Columns[1].Visible = true;
            dgBZRates.Columns[2].Visible = true;
        }

        private void btnInsert_Click(object sender, System.EventArgs e)
        {
            //clear the rows from the datagrid
            if (btnExecuteQuery.Enabled == true)
                btnExecuteQuery.Enabled = false;
            if ((int)ViewState["Mode"] != (int)ScreenMode.Insert || m_sdsBZRates.ds.Tables[0].Rows.Count >= dgBZRates.PageSize)
                m_sdsBZRates = SysDataManager2.GetEmptyBZRatesDS();
            else
            {
                AddRow();
            }
            Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, false, m_moduleAccessRights);
            EditRow(false);
            ViewState["Mode"] = ScreenMode.Insert;
            ViewState["Operation"] = Operation.Insert;
            dgBZRates.Columns[0].Visible = false;
            dgBZRates.Columns[1].Visible = true;
            dgBZRates.Columns[2].Visible = true;


            dgBZRates.EditItemIndex = m_sdsBZRates.ds.Tables[0].Rows.Count - 1;

            dgBZRates.CurrentPageIndex = 0;
            BindGrid();
            Logger.LogDebugInfo("BaseZoneRates", "btnInsert_Click", "INF003", "Data Grid Items count" + dgBZRates.Items.Count);
            getPageControls(Page);
        }

        public void dgBZRates_Button(object sender, DataGridCommandEventArgs e)
        {
            int iOperation = (int)ViewState["Operation"];
            if ((iOperation == (int)Operation.Insert) || (iOperation == (int)Operation.Update) || (btnExecuteQuery.Enabled))
            {
                String strCmdNm = e.CommandName;
                String strtxtZoneCode = null;
                String strZoneCode = null;
                if (strCmdNm.Equals("OriginSearch") || strCmdNm.Equals("DestinationSearch"))
                {
                    if (strCmdNm.Equals("OriginSearch"))
                    {
                        msTextBox txtOZCode = (msTextBox)e.Item.FindControl("txtOZCode");
                        if (txtOZCode != null)
                        {
                            strtxtZoneCode = txtOZCode.ClientID;
                            strZoneCode = txtOZCode.Text;
                        }
                    }
                    else if (strCmdNm.Equals("DestinationSearch"))
                    {
                        msTextBox txtDZCode = (msTextBox)e.Item.FindControl("txtDZCode");
                        if (txtDZCode != null)
                        {
                            strtxtZoneCode = txtDZCode.ClientID;
                            strZoneCode = txtDZCode.Text;
                        }
                    }

                    if(strtxtZoneCode != null)
                    {
                        String sUrl = "ZonePopup.aspx?FORMID=BaseZoneRates&ZONECODE_TEXT=" + strZoneCode + "&ZONECODE=" + strtxtZoneCode;
                        ArrayList paramList = new ArrayList();
                        paramList.Add(sUrl);
                        String sScript = Utility.GetScript("openWindowParam.js", paramList);
                        Utility.RegisterScriptString(sScript, "ShowPopupScript", this.Page);
                    } 
                }
                else if (strCmdNm.Equals("ServiceCodeSearch"))
                {
                    msTextBox txtservice_code = (msTextBox)e.Item.FindControl("txtServiceCode");
                    if (txtservice_code != null)
                    {
                        String sUrl = "ServiceCodePopup.aspx?FORMID=BaseZoneRates&CODEID=" + txtservice_code.ClientID;
                        ArrayList paramList = new ArrayList();
                        paramList.Add(sUrl);

                        String sScript = Utility.GetScript("openWindowParam.js", paramList);
                        Utility.RegisterScriptString(sScript, "ShowPopupScript", this.Page);
                    }
                }
            }
        }

        private void BindGrid()
        {
            if (this.btnImport_Save.Visible == true)
            {//SysDataManager2.GetEmptyBZRatesDS();
                DataSet dsBaseRates_Import;
                if (Session["dsBaseRates_Import"] != null)
                {
                    //Response.Write("<script>alert('¡ÃÕê´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´´');</script>");
                    dsBaseRates_Import = (DataSet)Session["dsBaseRates_Import"];
                }
                else
                    dsBaseRates_Import = SysDataManager2.GetEmptyBZRatesDS().ds;

                //Response.Write("<script>alert('"+dsBaseRates_Import.Tables[0].Rows.Count.ToString()+"');</script>");
                //dgBZRates.VirtualItemCount = System.Convert.ToInt32(dsBaseRates_Import.Tables[0].Rows.Count);
                dgBZRates.DataSource = dsBaseRates_Import;
                dgBZRates.DataBind();
            }
            else
            {
                //Response.Write("<script>alert('¡ÃÕê´´´´´´´´´´´´´´´´´´´´´´´´´ Execute');</script>");
                dgBZRates.VirtualItemCount = System.Convert.ToInt32(m_sdsBZRates.QueryResultMaxSize);
                //dgBZRates.CurrentPageIndex = 0;
                dgBZRates.DataSource = m_sdsBZRates.ds;
                dgBZRates.DataBind();
                Session["SESSION_DS1"] = m_sdsBZRates;
            }
        }

        private void EditRow(bool bItemIndex)
        {
            foreach (DataGridItem item in dgBZRates.Items)
            {
                if (bItemIndex)
                    dgBZRates.EditItemIndex = item.ItemIndex;
                else
                    dgBZRates.EditItemIndex = -1;
            }
            dgBZRates.DataBind();
        }

        private void QueryMode()
        {
            //Set Query Mode
            ViewState["Mode"] = ScreenMode.None;
            ViewState["Operation"] = Operation.None;
            //Set Button Enable Properties
            btnQuery.Enabled = true;
            btnExecuteQuery.Enabled = true;
            btnImport_Save.Enabled = false;

            Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
            //inset an empty row for query 
            m_sdsBZRates = SysDataManager2.GetEmptyBZRatesDS();
            m_sdsBZRates.DataSetRecSize = 1;
            Session["SESSION_DS1"] = m_sdsBZRates;
            dgBZRates.CurrentPageIndex = 0;
            BindGrid();
            //Make the row in Edit Mode
            EditRow(true);
            dgBZRates.Columns[0].Visible = false;
            dgBZRates.Columns[1].Visible = false;
            dgBZRates.Columns[2].Visible = false;


            lblErrorMessage.Text = "";

            txtFilePath.Text = "";
            txtCustID.Text = "";
            txtCustName.Text = "";
            txtEffDate.Text = "";
            txtBasicRate.Text = "";
            txtValuable.Text = "";
            txtHuman.Text = "";
            txtLive.Text = "";
            txtExpress.Text = "";
            txtExpress.Text = "";
            txtColumn.Text = "";
            txtRow.Text = "";
            txtEndRow.Text = "";
            txtSheet.Text = "";
        }

        private void AddRow()
        {
            SysDataManager2.AddNewRowInBZRatesDS(ref m_sdsBZRates);
            BindGrid();
        }

        private int ChkWtRange()
        {
            int intChkWtRet = 0;
            intChkWtRet = SysDataManager2.CheckWtInBZRatesDS(m_sdsBZRates.ds, m_strAppID, m_strEnterpriseID);
            switch (intChkWtRet)
            {
                case -1:
                    strRetMsg = "End Weight should be greater than Start Weight";
                    break;
                case -2:
                    strRetMsg = "End Weight should be greater than Start Weight";
                    break;
                default:
                    strRetMsg = null;
                    break;
            }
            return intChkWtRet;
        }

        private void GetChangedRow(DataGridItem item, int rowIndex)
        {
            msTextBox txtOZCode = (msTextBox)item.FindControl("txtOZCode");
            msTextBox txtDZCode = (msTextBox)item.FindControl("txtDZCode");
            msTextBox txtStartWt = (msTextBox)item.FindControl("txtStartWt");
            msTextBox txtEndWt = (msTextBox)item.FindControl("txtEndWt");
            msTextBox txtSPrice = (msTextBox)item.FindControl("txtSPrice");
            msTextBox txtIPrice = (msTextBox)item.FindControl("txtIPrice");
            msTextBox txtServiceCode = (msTextBox)item.FindControl("txtServiceCode");
            msTextBox txtEffectiveDate = (msTextBox)item.FindControl("txtEffectiveDate");



            string strOZCode = txtOZCode.Text.ToString();
            string strDZCode = txtDZCode.Text.ToString();
            decimal decStartWt = 0;
            decimal decEndWt = 0;
            decimal decSPrice = 0;
            decimal decIPrice = 0;
            string strServiceCode = txtServiceCode.Text.ToString();
            string strEffectiveDate = txtEffectiveDate.Text.ToString();

            if ((txtStartWt.Text != null) && (txtStartWt.Text != ""))
                decStartWt = Convert.ToDecimal(txtStartWt.Text.ToString());
            if ((txtEndWt.Text != null) && (txtEndWt.Text != ""))
                decEndWt = Convert.ToDecimal(txtEndWt.Text.ToString());
            if ((txtSPrice.Text != null) && (txtSPrice.Text != ""))
                decSPrice = Convert.ToDecimal(txtSPrice.Text.ToString());
            if ((txtIPrice.Text != null) && (txtIPrice.Text != ""))
                decIPrice = Convert.ToDecimal(txtIPrice.Text.ToString());

            try
            {
                DataRow dr = m_sdsBZRates.ds.Tables[0].Rows[rowIndex];
                dr[0] = strOZCode;
                dr[1] = strDZCode;
                dr[2] = decStartWt;
                dr[3] = decEndWt;
                dr[4] = decSPrice;
                dr[5] = decIPrice;
                dr[6] = strServiceCode;
                if (strEffectiveDate != "")
                {
                    dr[7] = DateTime.ParseExact(strEffectiveDate, "dd/MM/yyyy", null);
                }
                else
                {
                    dr[7] = System.DBNull.Value;
                }
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = ex.Message;
            }
        }

        private void RetreiveSelectedPage()
        {
            int iStartRow = dgBZRates.CurrentPageIndex * dgBZRates.PageSize;
            DataSet dsQuery = (DataSet)Session["QUERY_DS"];
            m_sdsBZRates = SysDataManager2.GetBZRatesDS(utility.GetAppID(), utility.GetEnterpriseID(), iStartRow, dgBZRates.PageSize, dsQuery);
            int iPageCnt = Convert.ToInt32((m_sdsBZRates.QueryResultMaxSize - 1)) / dgBZRates.PageSize;
            if (iPageCnt < dgBZRates.CurrentPageIndex)
            {
                dgBZRates.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
            }
            dgBZRates.SelectedIndex = -1;
            dgBZRates.EditItemIndex = -1;

            BindGrid();
            Session["SESSION_DS1"] = m_sdsBZRates;
        }

        private void EnableZoneUpload(bool b)
        {
            Session["dsBaseRates_Import"] = null;
            this.btnImport_Cancel.Visible = b;
            this.btnImport_Save.Visible = b;
            this.btnImport_Cancel.Visible = b;
            this.btnImport.Visible = b;
            this.txtFilePath.Visible = b;
            //Add by Panas 18/11/2008
            this.trImport.Visible = b;
            

            this.btnQuery.Visible = !b;
            this.btnExecuteQuery.Visible = !b;
            this.btnInsert.Visible = !b;
            this.btnImport_Show.Visible = !b;
            //Add by Panas 18/11/2008
            this.trQuery.Visible = !b;

            this.txtFilePath.Text = "";

            m_sdsBZRates = SysDataManager2.GetEmptyBZRatesDS();
            m_sdsBZRates.DataSetRecSize = 1;
            Session["SESSION_DS1"] = m_sdsBZRates;
            BindGrid();
            //Make the row in Edit Mode
            EditRow(false);
            dgBZRates.Columns[0].Visible = false;
            dgBZRates.Columns[1].Visible = false;

            this.btnImport_Cancel.Attributes.Add("onclick", "javascript:showSaveProgress();");
        }

        private void btnImport_Show_Click(object sender, System.EventArgs e)
        {
            EnableZoneUpload(true);
        }

        private void btnImport_Cancel_Click(object sender, System.EventArgs e)
        {
            if (this.btnImport_Save.Visible != true)
            {
                this.dgBZRates.AllowCustomPaging = true;
            }
            else
            {
                this.dgBZRates.AllowCustomPaging = false;
            }

            ViewState["Operation"] = Operation.None;
            ViewState["Mode"] = ScreenMode.None;
            lblErrorMessage.Text = "";
            dgBZRates.CurrentPageIndex = 0;
            dgBZRates.Columns[0].Visible = false;
            dgBZRates.Columns[1].Visible = false;
            dgBZRates.Columns[2].Visible = false;

            Session["dsBaseRates_Import"] = null;


            this.btnExecuteQuery.Enabled = false;
            EnableZoneUpload(false);

            QueryMode();
        }

        private void btnImport_Click(object sender, System.EventArgs e)
        {

            try
            {
                if (this.btnImport_Save.Visible != true)
                {
                    this.dgBZRates.AllowCustomPaging = true;
                }
                else
                {
                    this.dgBZRates.AllowCustomPaging = false;
                }

                string statusButton = "0";
                if (uploadFiletoServer())
                {
                    //Cleare existing data
                    //ImportConsignmentsDAL.ClearTempDataByUserId(appID, enterpriseID, userID);

                    //Get data from Excel WorkSheet
                    getExcelFiles();

                    DataSet ds = (DataSet)Session["dsBaseRates_Import"];

                    //					ds = EliminateDuplicateRecords(ds);
                    //
                    //					ds = ValidateCustZones(ds);

                    dgBZRates.VirtualItemCount = System.Convert.ToInt32(ds.Tables[0].Rows.Count);
                    this.dgBZRates.DataSource = ds;
                    this.dgBZRates.EditItemIndex = -1;
                    this.dgBZRates.DataBind();


                    dgBZRates.Columns[0].Visible = true;
                    dgBZRates.Columns[1].Visible = true;
                    dgBZRates.Columns[2].Visible = true;

                    foreach (DataGridItem dgItem in dgBZRates.Items)
                    {
                        DataSet dsZone = new DataSet();

                        ImageButton imgSelect = (ImageButton)dgItem.Cells[0].Controls[1];
                        Label lblOZCode = (Label)dgItem.FindControl("lblOZCode");
                        Label lblDZCode = (Label)dgItem.FindControl("lblDZCode");
                        Label lblServiceCode = (Label)dgItem.FindControl("lblServiceCode");
                        Label lblStartWt = (Label)dgItem.FindControl("lblStartWt");
                        Label lblEndWt = (Label)dgItem.FindControl("lblEndWt");
                        Label lblSPrice = (Label)dgItem.FindControl("lblSPrice");
                        Label lblIPrice = (Label)dgItem.FindControl("lblIPrice");
                        Label lblEffectivedate = (Label)dgItem.FindControl("lblEffectivedate");

                        string strOZ = "1";
                        string strDZ = "1";
                        string strSC = "1";
                        string strSwt = "1"; // ÁÕ¤èÒà»ç¹ 0 ä´é
                        string strEwt = "1";
                        string strSprice = "1";
                        string strIprice = "1";// ÁÕ¤èÒà»ç¹ 0 ä´é
                        string strEffDate = "1";



                        if (lblOZCode != null)
                        {
                            if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblOZCode.Text.ToString(), "Zone"))
                            {
                                strOZ = "0";
                            }
                            else if (lblOZCode.Text == "")
                            {
                                strOZ = "0";
                            }
                        }
                        if (lblDZCode != null)
                        {
                            if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblDZCode.Text.ToString(), "Zone"))
                            {
                                strDZ = "0";
                            }
                            else if (lblDZCode.Text == "")
                            {
                                strDZ = "0";
                            }
                        }
                        if (lblServiceCode != null)
                        {
                            if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblServiceCode.Text.ToString(), "ServiceType"))
                            {
                                strSC = "0";
                            }
                            else if (lblServiceCode.Text == "")
                            {
                                strSC = "0";
                            }
                        }

                        if (lblStartWt != null && lblEndWt != null)
                        {
                            if (Utility.isNumeric(lblStartWt.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float) && Utility.isNumeric(lblEndWt.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                            {
                                if (Convert.ToDouble(lblStartWt.Text) < 0)
                                    strSwt = "0";
                                //							lblStartWt.Text.
                                if (Convert.ToDouble(lblStartWt.Text) >= Convert.ToDouble(lblEndWt.Text))
                                    strEwt = "0";
                            }
                            else
                            {
                                strSwt = "0";
                                strEwt = "0";
                            }
                        }
                        if (lblSPrice != null)
                        {
                            if (Utility.isNumeric(lblSPrice.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                            {
                                if (Convert.ToDouble(lblSPrice.Text) <= 0)
                                    strSprice = "0";
                            }
                            else
                            {
                                strSprice = "0";
                            }
                        }
                        if (lblIPrice != null)
                        {
                            if (Utility.isNumeric(lblIPrice.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                            {
                                if (Convert.ToDouble(lblIPrice.Text) < 0)
                                    strIprice = "0";
                            }
                            else
                            {
                                strIprice = "0";
                            }
                        }
                        if (lblEffectivedate != null)
                        {
                            object objEffectiveDate = System.DBNull.Value;
                            objEffectiveDate = DateTime.ParseExact(lblEffectivedate.Text.ToString().Trim(), "dd/MM/yyyy", null);

                            if (!ckEffectiveDate(Convert.ToDateTime(objEffectiveDate)))
                                strEffDate = "0";
                        }

                        if (strOZ.Equals("0") || strDZ.Equals("0") || strSC.Equals("0") || strSwt.Equals("0") || strEwt.Equals("0") || strSprice.Equals("0") || strIprice.Equals("0") || strEffDate.Equals("0"))
                        {
                            imgSelect.ImageUrl = "images/butt-red.gif";
                            string strErrorToolTip = "";

                            if (strOZ.Equals("0"))
                                strErrorToolTip += "- Invalid Original Zode code \n";
                            if (strDZ.Equals("0"))
                                strErrorToolTip += "- Invalid Destination Zode code\n";
                            if (strSC.Equals("0"))
                                strErrorToolTip += "- Invalid Service Type\n";
                            if (strSwt.Equals("0"))
                                strErrorToolTip += "- Invalid Start Wt.\n";
                            if (strEwt.Equals("0"))
                                strErrorToolTip += "- Invalid End Wt.\n";
                            if (strSprice.Equals("0"))
                                strErrorToolTip += "- Invalid Start Price\n";
                            if (strIprice.Equals("0"))
                                strErrorToolTip += "- Invalid Increment Price\n";
                            if (strEffDate.Equals("0"))
                                strErrorToolTip += "- Invalid Effective Date\n";

                            //							if(strErrorToolTip.Length>0)
                            //								strErrorToolTip.Replace("  ","\n ");

                            imgSelect.ToolTip = strErrorToolTip;

                            statusButton = "1";
                        }
                        else
                        {
                            //new
                            DataSet dsChangedRow = SysDataManager2.GetEmptyBZRatesDS().ds;
                            try
                            {

                                if (dsChangedRow.Tables[0].Rows.Count > 0)
                                {
                                    dsChangedRow.Tables[0].Rows.RemoveAt(0);
                                }
                                DataRow dr = dsChangedRow.Tables[0].NewRow();

                                if (lblOZCode != null)
                                    dr[0] = lblOZCode.Text;
                                if (lblDZCode != null)
                                    dr[1] = lblDZCode.Text;
                                if (lblStartWt != null)
                                    dr[2] = lblStartWt.Text;
                                if (lblEndWt != null)
                                    dr[3] = lblEndWt.Text;
                                if (lblSPrice != null)
                                    dr[4] = lblSPrice.Text;
                                if (lblIPrice != null)
                                    dr[5] = lblIPrice.Text;
                                if (lblServiceCode != null)
                                    dr[6] = lblServiceCode.Text;

                                if (lblEffectivedate.Text != "")
                                {
                                    dr[7] = DateTime.ParseExact(lblEffectivedate.Text, "dd/MM/yyyy", null);
                                }
                                else
                                {
                                    dr[7] = System.DBNull.Value;
                                }

                                dsChangedRow.Tables[0].Rows.Add(dr);

                            }
                            catch (Exception ex)
                            {
                                lblErrorMessage.Text = ex.Message;
                            }

                            DataSet dsBZRates = SysDataManager2.GetBZRatesDS(dsChangedRow, utility.GetAppID(), utility.GetEnterpriseID());

                            //(utility.GetAppID(),utility.GetEnterpriseID(),
                            //			lblOZCode.Text,lblDZCode.Text,Convert.ToInt16(lblStartWt.Text),Convert.ToInt16(lblEndWt.Text),Convert.ToInt16(lblSPrice.Text),Convert.ToInt16(lblIPrice.Text),lblServiceCode.Text,lblEffectivedate.Text);
                            if (dsBZRates.Tables[0].Rows.Count > 0)
                            {
                                imgSelect.ToolTip = "Duplicate Data";
                                imgSelect.ImageUrl = "images/butt-red.gif";
                                statusButton = "1";
                            }
                            else
                                imgSelect.ImageUrl = "images/butt-select.gif";
                            //end new
                        }

                    }


                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (statusButton.Equals("1"))
                            this.btnImport_Save.Enabled = false;
                        else
                        {
                            this.btnImport_Save.Enabled = true;
                            this.btnImport_Save.Attributes.Add("onclick", "javascript:showSaveProgress();");
                        }
                    }
                    Session["dsBaseRates_Import"] = ds;
                }
            }
            catch (Exception ex)
            {

                this.lblErrorMessage.Text = ex.Message.ToString();
                txtFilePath.Text = "";
            }

        }

        private bool uploadFiletoServer()
        {
            bool status = true;

            if ((inFile.PostedFile != null) && (inFile.PostedFile.ContentLength > 0))
            {
                string extension = System.IO.Path.GetExtension(inFile.PostedFile.FileName);

                if (extension.ToUpper() == ".XLS" || extension.ToUpper() == ".XLSX")
                {
                    string fn = System.IO.Path.GetFileName(inFile.PostedFile.FileName);
                    string path = Server.MapPath("Excel") + "\\";
                    string pathFn = Server.MapPath("Excel") + "\\" + this.User.ToString() + "_" + fn;

                    ViewState["FileName"] = pathFn;

                    //txtFilePath.Text = pathFn;

                    try
                    {
                        if (System.IO.Directory.Exists(path) == false)
                            System.IO.Directory.CreateDirectory(path);

                        if (System.IO.File.Exists(pathFn))
                            System.IO.File.Delete(pathFn);

                        inFile.PostedFile.SaveAs(pathFn);
                        //txtFilePath.Text = "";

                        this.lblErrorMessage.Text = "";
                    }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Error during upload file to the server", null);
                    }
                }
                else
                {
                    this.lblErrorMessage.Text = "Not a valid Excel Workbook.";
                    txtFilePath.Text = "";
                    status = false;
                }
            }
            else
            {
                lblErrorMessage.Text = "Please select a file to upload.";
                status = false;
            }

            return status;
        }

        private void getExcelFiles()
        {
            int i = 0;

            try
            {
                String connString = ConfigurationSettings.AppSettings["ExcelConn"];
                connString = connString.Replace("(DestinationFile)", (String)ViewState["FileName"]);

                int recCouter = 1;

                StringBuilder strBuilder = new StringBuilder();
                strBuilder.Append("SELECT origin_zone_code,destination_zone_code,service_code,start_wt,end_wt,start_price,increment_price,effective_date ");
                //HC Return Task
                strBuilder.Append("FROM [Base_Rates$] ");
                OleDbDataAdapter daBaseRates = new OleDbDataAdapter(strBuilder.ToString(), connString);

                DataSet dsExcel = new DataSet();
                daBaseRates.Fill(dsExcel);

                DataSet dsBaseRates = new DataSet();
                dsBaseRates = SysDataManager2.GetEmptyBZRatesDS().ds;
                if (dsBaseRates.Tables[0].Rows.Count == 1)
                {
                    dsBaseRates.Tables[0].Rows[0].Delete();
                    dsBaseRates.AcceptChanges();
                }

                foreach (DataRow dr in dsExcel.Tables[0].Rows)
                {
                    if ((dr["origin_zone_code"].ToString().Trim() != "") ||
                        (dr["destination_zone_code"].ToString().Trim() != "") ||
                        (dr["service_code"].ToString().Trim() != "") ||
                        (dr["start_wt"].ToString().Trim() != "") ||
                        (dr["end_wt"].ToString().Trim() != "") ||
                        (dr["start_price"].ToString().Trim() != "") ||
                        (dr["increment_price"].ToString().Trim() != "") ||
                        (dr["effective_date"].ToString().Trim() != ""))
                    {
                        DataRow tmpDr = dsBaseRates.Tables[0].NewRow();

                        //tmpDr["recordid"] = recCouter.ToString();
                        recCouter++;

                        //						tmpDr["custid"] = Convert.ToString(dr["custid"]);
                        tmpDr["origin_zone_code"] = Convert.ToString(dr["origin_zone_code"]);
                        tmpDr["destination_zone_code"] = Convert.ToString(dr["destination_zone_code"]);
                        tmpDr["service_code"] = Convert.ToString(dr["service_code"]);
                        //tmpDr["start_wt"] = Convert.ToString(dr["start_wt"]);
                        if (dr["end_wt"].ToString() != "")
                        {
                            tmpDr["start_wt"] = Convert.ToString(dr["start_wt"]);
                        }
                        else
                        {
                            tmpDr["start_wt"] = "0";
                        }
                        tmpDr["end_wt"] = Convert.ToString(dr["end_wt"]);
                        tmpDr["start_price"] = Convert.ToString(dr["start_price"]);
                        //tmpDr["increment_price"] = Convert.ToString(dr["increment_price"]);
                        if (dr["increment_price"].ToString() != "")
                        {
                            tmpDr["increment_price"] = Convert.ToString(dr["increment_price"]);
                        }
                        else
                        {
                            tmpDr["increment_price"] = "0";
                        }
                        tmpDr["effective_date"] = Convert.ToString(dr["effective_date"]);

                        dsBaseRates.Tables[0].Rows.Add(tmpDr);
                        dsBaseRates.AcceptChanges();
                    }
                }

                dsExcel.Dispose();
                dsExcel = null;

                i++;
                recCouter = 1;
                Session["dsBaseRatesTable_Import"] = dsBaseRates;
                Session["dsBaseRates_Import"] = dsBaseRates;
            }
            catch (Exception ex)
            {
                String strMsg = ex.Message;

                if (strMsg.IndexOf("'Consignments$' is not a valid name") != -1)
                {
                    throw new ApplicationException("Excel Workbook must contain two named sheets: Consignments and Packages.", null);
                }
                else if (strMsg.IndexOf("'Packages$' is not a valid name") != -1)
                {
                    throw new ApplicationException("Excel Workbook must contain two named sheets: Consignments and Packages.", null);
                }
                else if (strMsg.IndexOf("No value given for one or more required parameters") != -1)
                {
                    if (i == 0)
                        throw new ApplicationException("Consignments worksheet has invalid columns", null);
                    else
                        throw new ApplicationException("Packages worksheet has invalid columns", null);
                }
                else
                {
                    throw ex;
                }
            }
        }

        private void btnImport_Save_Click(object sender, System.EventArgs e)
        {
            this.btnImport_Save.Enabled = false;

            String strCustID = (String)ViewState["currentCustomer"];
            DataSet dsBaseRates_Import = (DataSet)Session["dsBaseRates_Import"];
            DataSet dsBaseRates = new DataSet();

            for (int i = 0; i < dsBaseRates_Import.Tables[0].Rows.Count; i++)
                SysDataManager2.InsertBZRates(dsBaseRates_Import.Tables[0].Rows[i], utility.GetAppID(), utility.GetEnterpriseID());

            EnableZoneUpload(false);
            this.QueryMode();
            this.btnQuery_Click(this, new System.EventArgs());
        }

  
        #region Add Function New Import by Kat [2019-05-21]

        private void btnNewImport_Click(object sender, System.EventArgs e)
        {
            try
            {
                //ValidationSummary1.va();

                if (this.btnImport_Save.Visible != true)
                    this.dgBZRates.AllowCustomPaging = true;
                else
                    this.dgBZRates.AllowCustomPaging = false;

                string statusButton = "0";
                bool flgDup = false;

                if (uploadFiletoServer()) //ค่อยเอาออก
                {
                    //Get data from Excel WorkSheet
                    GetExcelFilesNew();

                    DataSet ds = (DataSet)Session["dsBaseRates_Import"];

                    if (ds != null)
                    {
                        dgBZRates.VirtualItemCount = System.Convert.ToInt32(ds.Tables[0].Rows.Count);
                        this.dgBZRates.DataSource = ds;
                        this.dgBZRates.EditItemIndex = -1;
                        this.dgBZRates.DataBind();


                        dgBZRates.Columns[0].Visible = true;
                        dgBZRates.Columns[1].Visible = true;
                        dgBZRates.Columns[2].Visible = true;

                        foreach (DataGridItem dgItem in dgBZRates.Items)
                        {
                            DataSet dsZone = new DataSet();

                            ImageButton imgSelect = (ImageButton)dgItem.Cells[0].Controls[1];
                            Label lblOZCode = (Label)dgItem.FindControl("lblOZCode");
                            Label lblDZCode = (Label)dgItem.FindControl("lblDZCode");
                            Label lblServiceCode = (Label)dgItem.FindControl("lblServiceCode");
                            Label lblStartWt = (Label)dgItem.FindControl("lblStartWt");
                            Label lblEndWt = (Label)dgItem.FindControl("lblEndWt");
                            Label lblSPrice = (Label)dgItem.FindControl("lblSPrice");
                            Label lblIPrice = (Label)dgItem.FindControl("lblIPrice");
                            Label lblEffectivedate = (Label)dgItem.FindControl("lblEffectivedate");

                            string strOZ = "1";
                            string strDZ = "1";
                            string strSC = "1";
                            string strSwt = "1"; // ÁÕ¤èÒà»ç¹ 0 ä´é
                            string strEwt = "1";
                            string strSprice = "1";
                            string strIprice = "1";// ÁÕ¤èÒà»ç¹ 0 ä´é
                            string strEffDate = "1";



                            if (lblOZCode != null)
                            {
                                if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblOZCode.Text.ToString(), "Zone"))
                                    strOZ = "0";
                                else if (lblOZCode.Text == "")
                                    strOZ = "0";
                            }

                            if (lblDZCode != null)
                            {
                                if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblDZCode.Text.ToString(), "Zone"))
                                    strDZ = "0";
                                else if (lblDZCode.Text == "")
                                    strDZ = "0";
                            }

                            if (lblServiceCode != null)
                            {
                                if (!SysDataManager2.ckZoneOrServiceInSystem(utility.GetEnterpriseID(), utility.GetAppID(), lblServiceCode.Text.ToString(), "ServiceType"))
                                    strSC = "0";
                                else if (lblServiceCode.Text == "")
                                    strSC = "0";
                            }

                            if (lblStartWt != null && lblEndWt != null)
                            {
                                if (Utility.isNumeric(lblStartWt.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float) && Utility.isNumeric(lblEndWt.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                                {
                                    if (Convert.ToDouble(lblStartWt.Text) < 0)
                                        strSwt = "0";
                                    //							lblStartWt.Text.
                                    if (Convert.ToDouble(lblStartWt.Text) >= Convert.ToDouble(lblEndWt.Text))
                                        strEwt = "0";
                                }
                                else
                                {
                                    strSwt = "0";
                                    strEwt = "0";
                                }
                            }

                            if (lblSPrice != null)
                            {
                                if (Utility.isNumeric(lblSPrice.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                                {
                                    if (Convert.ToDouble(lblSPrice.Text) <= 0)
                                        strSprice = "0";
                                }
                                else
                                    strSprice = "0";
                            }

                            if (lblIPrice != null)
                            {
                                if (Utility.isNumeric(lblIPrice.Text.ToString().Trim().Replace(",", ""), System.Globalization.NumberStyles.Float))
                                {
                                    if (Convert.ToDouble(lblIPrice.Text) < 0)
                                        strIprice = "0";
                                }
                                else
                                    strIprice = "0";
                            }

                            if (lblEffectivedate != null)
                            {
                                object objEffectiveDate = System.DBNull.Value;
                                objEffectiveDate = DateTime.ParseExact(lblEffectivedate.Text.ToString().Trim(), "dd/MM/yyyy", null);

                                if (!ckEffectiveDate(Convert.ToDateTime(objEffectiveDate)))
                                    strEffDate = "0";
                            }

                            if (strOZ.Equals("0") || strDZ.Equals("0") || strSC.Equals("0") || strSwt.Equals("0") || strEwt.Equals("0") || strSprice.Equals("0") || strIprice.Equals("0") || strEffDate.Equals("0"))
                            {
                                imgSelect.ImageUrl = "images/butt-red.gif";
                                string strErrorToolTip = "";

                                if (strOZ.Equals("0"))
                                    strErrorToolTip += "- Invalid Original Zode code \n";
                                if (strDZ.Equals("0"))
                                    strErrorToolTip += "- Invalid Destination Zode code\n";
                                if (strSC.Equals("0"))
                                    strErrorToolTip += "- Invalid Service Type\n";
                                if (strSwt.Equals("0"))
                                    strErrorToolTip += "- Invalid Start Wt.\n";
                                if (strEwt.Equals("0"))
                                    strErrorToolTip += "- Invalid End Wt.\n";
                                if (strSprice.Equals("0"))
                                    strErrorToolTip += "- Invalid Start Price\n";
                                if (strIprice.Equals("0"))
                                    strErrorToolTip += "- Invalid Increment Price\n";
                                if (strEffDate.Equals("0"))
                                    strErrorToolTip += "- Invalid Effective Date\n";

                                imgSelect.ToolTip = strErrorToolTip;

                                statusButton = "1";
                            }
                            else
                            {
                                //new
                                DataSet dsChangedRow = SysDataManager2.GetEmptyBZRatesDS().ds;
                                try
                                {
                                    if (dsChangedRow.Tables[0].Rows.Count > 0)
                                        dsChangedRow.Tables[0].Rows.RemoveAt(0);

                                    DataRow dr = dsChangedRow.Tables[0].NewRow();

                                    if (lblOZCode != null)
                                        dr[0] = lblOZCode.Text;
                                    if (lblDZCode != null)
                                        dr[1] = lblDZCode.Text;
                                    if (lblStartWt != null)
                                        dr[2] = lblStartWt.Text;
                                    if (lblEndWt != null)
                                        dr[3] = lblEndWt.Text;
                                    if (lblSPrice != null)
                                        dr[4] = lblSPrice.Text;
                                    if (lblIPrice != null)
                                        dr[5] = lblIPrice.Text;
                                    if (lblServiceCode != null)
                                        dr[6] = lblServiceCode.Text;

                                    if (lblEffectivedate.Text != "")
                                        dr[7] = DateTime.ParseExact(lblEffectivedate.Text, "dd/MM/yyyy", null);
                                    else
                                        dr[7] = System.DBNull.Value;

                                    dsChangedRow.Tables[0].Rows.Add(dr);
                                }
                                catch (Exception ex)
                                {
                                    lblErrorMessage.Text = ex.Message;
                                }

                                DataSet dsBZRates = SysDataManager2.GetBZRatesDS(dsChangedRow, utility.GetAppID(), utility.GetEnterpriseID());

                                if (dsBZRates.Tables[0].Rows.Count > 0)
                                {
                                    imgSelect.ToolTip = "Duplicate Data";
                                    imgSelect.ImageUrl = "images/butt-red.gif";
                                    statusButton = "1";

                                    if (flgDup == false)
                                        flgDup = true;
                                }
                                else
                                    imgSelect.ImageUrl = "images/butt-select.gif";
                            }
                        }

                        if (flgDup == true)
                            Response.Write("<script>alert('Duplicate Data');</script>");

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            if (statusButton.Equals("1"))
                                this.btnImport_Save.Enabled = false;
                            else
                            {
                                this.btnImport_Save.Enabled = true;
                                this.btnImport_Save.Attributes.Add("onclick", "javascript:showSaveProgress();");
                            }
                                
                        }
                        Session["dsBaseRates_Import"] = ds;
                    }
                    //else
                    //    this.lblErrorMessage.Text = "Can't find zone of customer : '" + txtCustID.Text + "'";
                }
            }
            catch (Exception ex)
            {
                this.lblErrorMessage.Text = ex.Message.ToString();
                txtFilePath.Text = "";
            }
        }

        private void GetExcelFilesNew()
        {
            try
            {
                DataSet dsBaseRates = new DataSet();
                dsBaseRates = SysDataManager2.GetEmptyBZRatesDS().ds;
                if (dsBaseRates.Tables[0].Rows.Count == 1)
                {
                    dsBaseRates.Tables[0].Rows[0].Delete();
                    dsBaseRates.AcceptChanges();
                }

                string strErr = "";
                String fullFilePath = (String)ViewState["FileName"];
                using (var excelWorkbook = new XLWorkbook(fullFilePath))
                {
                    var nonEmptyDataRows = excelWorkbook.Worksheet(int.Parse(txtSheet.Text)).RowsUsed();

                    string originZone = "";
                    double Price = 0;
                    int s = 0;
                    int col = int.Parse(txtColumn.Text);
                    int row = int.Parse(txtRow.Text);
                    int endRow = int.Parse(txtEndRow.Text);
                    int vcRate = int.Parse(txtValuable.Text);
                    int hrRate = int.Parse(txtHuman.Text);
                    int laRate = int.Parse(txtLive.Text);
                    int cRate = int.Parse(txtValuable.Text);
                    int expRate = int.Parse(txtExpress.Text);
                    DataSet cusDS = GetCustomerZonesDS();

                    if (cusDS.Tables[0].Rows.Count > 0)
                    {
                        strErr = ValidateExcel(nonEmptyDataRows, cusDS);

                        if (strErr != "")
                            lblErrorMessage.Text = "The city " + strErr + "' is not found !!!";
                        else if (strErr == "NoData")
                            lblErrorMessage.Text = "Can't find data in excel file !!!";
                        else if(row == endRow)
                            lblErrorMessage.Text = "Can't find destination city";
                        else
                        {
                            Session["dsBaseRatesTable_Import"] = dsBaseRates;
                            Session["dsBaseRates_Import"] = dsBaseRates;

                            foreach (var dataRowOrigin in nonEmptyDataRows)
                            {
                                //for row number check
                                if (dataRowOrigin.RowNumber() >= row && dataRowOrigin.RowNumber() <= endRow)
                                {
                                    int o = 0;

                                    foreach (var dataRowDesc in nonEmptyDataRows)
                                    {
                                        if (dataRowDesc.RowNumber() >= row + s && dataRowDesc.RowNumber() <= endRow)
                                        {
                                            if (dataRowDesc.RowNumber() == row + s)
                                            {
                                                originZone = dataRowDesc.Cell(col + o + s).Value.ToString();

                                                DataRow[] foundRow = cusDS.Tables[0].Select("zipcode = '" + originZone + "' and zipcode <> ''");

                                                if(foundRow.Length > 0)
                                                    originZone = foundRow[0].ItemArray[1].ToString();
                                            }
                                            else
                                            {
                                                if (double.TryParse(dataRowDesc.Cell(col + s).Value.ToString(), out Price))
                                                {
                                                    string DescZone = "";

                                                    DataRow[] foundRow = cusDS.Tables[0].Select("zipcode = '" + dataRowDesc.Cell(col + o + s).Value + "'");
                                                    DescZone = foundRow[0].ItemArray[1].ToString();

                                                    #region GEN (General Cargo)
                                                    DataRow tmpDr = dsBaseRates.Tables[0].NewRow();

                                                    tmpDr["origin_zone_code"] = originZone;
                                                    tmpDr["destination_zone_code"] = DescZone;
                                                    tmpDr["service_code"] = "GEN";
                                                    tmpDr["start_wt"] = "0.00";
                                                    tmpDr["end_wt"] = "9999.00";
                                                    tmpDr["start_price"] = txtBasicRate.Text;
                                                    tmpDr["increment_price"] = Math.Round(Price, 2);
                                                    tmpDr["effective_date"] = DateTime.ParseExact(txtEffDate.Text.Trim(), "dd/MM/yyyy", null);

                                                    dsBaseRates.Tables[0].Rows.Add(tmpDr);
                                                    #endregion

                                                    #region VC (Valuable Cargo)
                                                    tmpDr = dsBaseRates.Tables[0].NewRow();

                                                    tmpDr["origin_zone_code"] = originZone;
                                                    tmpDr["destination_zone_code"] = DescZone;
                                                    tmpDr["service_code"] = "VC";
                                                    tmpDr["start_wt"] = "0.00";
                                                    tmpDr["end_wt"] = "9999.00";
                                                    tmpDr["start_price"] = txtBasicRate.Text;
                                                    tmpDr["increment_price"] = Math.Round((Price * vcRate) / 100, 2);
                                                    tmpDr["effective_date"] = DateTime.ParseExact(txtEffDate.Text.Trim(), "dd/MM/yyyy", null);

                                                    dsBaseRates.Tables[0].Rows.Add(tmpDr);
                                                    #endregion

                                                    #region HRRate (Human Remain)
                                                    tmpDr = dsBaseRates.Tables[0].NewRow();

                                                    tmpDr["origin_zone_code"] = originZone;
                                                    tmpDr["destination_zone_code"] = DescZone;
                                                    tmpDr["service_code"] = "HR";
                                                    tmpDr["start_wt"] = "0.00";
                                                    tmpDr["end_wt"] = "9999.00";
                                                    tmpDr["start_price"] = txtBasicRate.Text;
                                                    tmpDr["increment_price"] = Math.Round((Price * hrRate) / 100, 2);
                                                    tmpDr["effective_date"] = DateTime.ParseExact(txtEffDate.Text.Trim(), "dd/MM/yyyy", null);

                                                    dsBaseRates.Tables[0].Rows.Add(tmpDr);
                                                    #endregion

                                                    #region LA (Live Animal)
                                                    tmpDr = dsBaseRates.Tables[0].NewRow();

                                                    tmpDr["origin_zone_code"] = originZone;
                                                    tmpDr["destination_zone_code"] = DescZone;
                                                    tmpDr["service_code"] = "LA";
                                                    tmpDr["start_wt"] = "0.00";
                                                    tmpDr["end_wt"] = "9999.00";
                                                    tmpDr["start_price"] = txtBasicRate.Text;
                                                    tmpDr["increment_price"] = Math.Round((Price * laRate) / 100, 2);
                                                    tmpDr["effective_date"] = DateTime.ParseExact(txtEffDate.Text.Trim(), "dd/MM/yyyy", null);

                                                    dsBaseRates.Tables[0].Rows.Add(tmpDr);
                                                    #endregion

                                                    #region EXP (Express Cargo)
                                                    tmpDr = dsBaseRates.Tables[0].NewRow();

                                                    tmpDr["origin_zone_code"] = originZone;
                                                    tmpDr["destination_zone_code"] = DescZone;
                                                    tmpDr["service_code"] = "EXP";
                                                    tmpDr["start_wt"] = "0.00";
                                                    tmpDr["end_wt"] = "9999.00";
                                                    tmpDr["start_price"] = txtBasicRate.Text;
                                                    tmpDr["increment_price"] = Math.Round((Price * expRate) / 100, 2);
                                                    tmpDr["effective_date"] = DateTime.ParseExact(txtEffDate.Text.Trim(), "dd/MM/yyyy", null);

                                                    dsBaseRates.Tables[0].Rows.Add(tmpDr);
                                                    #endregion

                                                    dsBaseRates.AcceptChanges();
                                                }
                                            }

                                            o++;
                                            if (dataRowDesc.RowNumber() == endRow)
                                                break;
                                        }
                                    }

                                    s++;

                                    if (dataRowOrigin.RowNumber() == endRow)
                                        break;
                                }
                            }
                        }
                    }
                    else
                        lblErrorMessage.Text = "Can't find zone of customer : '" + txtCustID.Text + "' !!!";
                }
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = "Can't find Sheet : " + txtSheet.Text;
            }
        }

        private DataSet GetCustomerZonesDS()
        {
            return SysDataManager2.GetCustomerZonesDS(m_strAppID, m_strEnterpriseID, txtCustID.Text);
        }

        private string ValidateExcel(IXLRows nonEmptyDataRows, DataSet cusDS)
        {
            int row = int.Parse(txtRow.Text);
            int endRow = int.Parse(txtEndRow.Text);
            int o = 0;
            int col = int.Parse(txtColumn.Text);
            string zone = "";
            string msg = "";
            bool flgData = false;

            foreach (var dr in nonEmptyDataRows)
            {
                if (dr.RowNumber() >= row && dr.RowNumber() <= endRow)
                {
                    zone = dr.Cell(col + o).Value.ToString();

                    DataRow[] foundRow = cusDS.Tables[0].Select("zipcode = '" + zone + "'");

                    if (foundRow.Length == 0 || zone == "")
                        msg += msg == "" ? "'" + zone : " ," + zone;

                    o++;

                    flgData = true;
                }
            }

            if (flgData == true)
                return msg;
            else 
                return "NoData";
        }
        #endregion
    }
}
