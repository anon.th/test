using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.common.applicationpages;


namespace com.ties
{
	/// <summary>
	/// Summary description for BaseConveyanceRates.
	/// </summary>
	public class BaseConveyanceRates : com.common.applicationpages.BasePage
	{
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.ValidationSummary Pagevalid;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.DataGrid dgBaseConveyance;
		protected System.Web.UI.WebControls.Button btnQuery;
	
		//		Utility utility=null;
		string m_strAppID=null;
		string m_strEnterpriseID=null;
		SessionDS m_sdsConveyance = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			if(!IsPostBack)
			{
				QueryMode();					  
			}
			else
			{
				m_sdsConveyance = (SessionDS) Session["SESSION_DS1"];
			}
			Pagevalid.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public void BindConveyanceGrid()
		{
			dgBaseConveyance.VirtualItemCount = System.Convert.ToInt32(m_sdsConveyance.QueryResultMaxSize);
			dgBaseConveyance.DataSource = m_sdsConveyance.ds;
			dgBaseConveyance.DataBind(); 
			Session["SESSION_DS1"] = m_sdsConveyance;
		}

		public void QueryMode()
		{
			ViewState["ConvOperation"]=Operation.None;
			ViewState["ConvMode"]=ScreenMode.Query;
			 	
			btnQuery.Enabled=true;
			btnExecuteQuery.Enabled=true;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);

			m_sdsConveyance = SysDataMgrDAL.GetEmptyBaseConvey() ;  

			m_sdsConveyance.DataSetRecSize = 1;
			AddRow(); 
			Session["SESSION_DS1"] = m_sdsConveyance;
			BindConveyanceGrid();
			lblErrorMessage.Text="";
			
			EditHRow(true);
			dgBaseConveyance.Columns[0].Visible=false;
			dgBaseConveyance.Columns[1].Visible=false;
		}

		private void AddRow()
		{
			SysDataMgrDAL.AddRowInBaseConveyDS(ref m_sdsConveyance);
			BindConveyanceGrid();
		}

		private void EditHRow(bool bItemIndex)
		{
			foreach (DataGridItem item in dgBaseConveyance.Items)
			{ 
				if (bItemIndex) 
					dgBaseConveyance.EditItemIndex = item.ItemIndex; 
				else 
					dgBaseConveyance.EditItemIndex = -1; 
			}
			dgBaseConveyance.DataBind();
		}

		private void GetChangedRow(DataGridItem item, int rowIndex)
		{
			msTextBox txtOZCode = (msTextBox)item.FindControl("txtOZCode");
			msTextBox txtDZCode = (msTextBox)item.FindControl("txtDZCode");
			msTextBox txtConveyance = (msTextBox)item.FindControl("txtConveyance");
			msTextBox txtStartPrice = (msTextBox)item.FindControl("txtStartPrice");				
			msTextBox txtIncrementPrice = (msTextBox)item.FindControl("txtIncrementPrice");	
			
			string strOZCode = txtOZCode.Text.ToString();
			string strDZCode = txtDZCode.Text.ToString();
			string strConveyance = txtConveyance.Text.ToString();
			decimal dStartPrice = Convert.ToDecimal(txtStartPrice.Text);
			decimal dIncrementPrice = Convert.ToDecimal(txtIncrementPrice.Text);
			
									
			DataRow dr = m_sdsConveyance.ds.Tables[0].Rows[rowIndex];
			dr[0] = strOZCode;
			dr[1] = strDZCode;
			dr[2] = strConveyance;
			dr[3] = dStartPrice;
			dr[4] = dIncrementPrice;
			
			Session["SESSION_DS1"] = m_sdsConveyance;
			
		}

		protected void  dgBaseConvey_Edit(object sender, DataGridCommandEventArgs e)
		{		
			lblErrorMessage.Text = "";
			if((int)ViewState["ConvMode"] == (int)ScreenMode.Insert && (int) ViewState["ConvOperation"] == (int)Operation.Insert &&  dgBaseConveyance.EditItemIndex > 0)
			{
				m_sdsConveyance.ds.Tables[0].Rows.RemoveAt(dgBaseConveyance.EditItemIndex);		
				dgBaseConveyance.CurrentPageIndex = 0;
			}
			dgBaseConveyance.EditItemIndex = e.Item.ItemIndex;
			ViewState["ConvOperation"] = Operation.Update;
			BindConveyanceGrid();
		}
		/*---*/
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			dgBaseConveyance.CurrentPageIndex = 0;
			QueryMode();
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{			
			lblErrorMessage.Text = "";
			if (btnExecuteQuery.Enabled == true)
				btnExecuteQuery.Enabled = false;

			if((int)ViewState["ConvMode"] != (int)ScreenMode.Insert || m_sdsConveyance.ds.Tables[0].Rows.Count >= dgBaseConveyance.PageSize)
			{
				m_sdsConveyance = SysDataMgrDAL.GetEmptyBaseConvey();
				dgBaseConveyance.EditItemIndex = m_sdsConveyance.ds.Tables[0].Rows.Count - 1;
				dgBaseConveyance.CurrentPageIndex = 0;
				AddRow();				
			}
			else
			{
				AddRow();
			}
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,false,m_moduleAccessRights);
			EditHRow(false);
			ViewState["ConvMode"] = ScreenMode.Insert;
			ViewState["ConvOperation"] = Operation.Insert;
			dgBaseConveyance.Columns[0].Visible=true;
			dgBaseConveyance.Columns[1].Visible=true;
			dgBaseConveyance.EditItemIndex = m_sdsConveyance.ds.Tables[0].Rows.Count - 1;
			dgBaseConveyance.CurrentPageIndex = 0;
			BindConveyanceGrid();
			getPageControls(Page);
		}
				
		public void dgBaseConvey_Button(object sender, DataGridCommandEventArgs e)
		{
			int iOperation = (int)ViewState["ConvOperation"];
			int iMode =(int)ViewState["ConvMode"];
			String strCmdNm = e.CommandName;
			String strtxtZoneCode = null; 
			String strZoneCode = null;
			String strtxtConveyanceCode=null;
			String strConveyanceCode=null;
			String sUrl=null;
			String sScript=null;
			ArrayList paramList=null;
		
			if (strCmdNm.Equals("Update") || strCmdNm.Equals("Edit") || strCmdNm.Equals("Delete") || strCmdNm.Equals("Cancel"))
			{
				return;
			}
			if (iMode==(int)ScreenMode.ExecuteQuery)
			{
				return;
			}
			if (strCmdNm.Equals("OriginSearch") )
			{
				msTextBox txtOZCode = (msTextBox)e.Item.FindControl("txtOZCode");
				if(txtOZCode != null)
				{
					strtxtZoneCode = txtOZCode.ClientID;
					strZoneCode = txtOZCode.Text;
				}
			}
			else if (strCmdNm.Equals("DestinationSearch") )
			{
				msTextBox txtDZCode = (msTextBox)e.Item.FindControl("txtDZCode");
				if(txtDZCode != null)
				{
					strtxtZoneCode = txtDZCode.ClientID;
					strZoneCode = txtDZCode.Text;
				}
			}
			else if (strCmdNm.Equals("ConveyanceSearch"))
			{
				msTextBox txtConveyance = (msTextBox)e.Item.FindControl("txtConveyance");
				if(txtConveyance != null)
				{
					strtxtConveyanceCode = txtConveyance.ClientID;
					strConveyanceCode = txtConveyance.Text;
				}
			
				sUrl = "ConveyancePopup.aspx?FORMID=BaseConveyanceRates&CONVEYANCECODE_TEXT="+strtxtConveyanceCode+"&CONVEYANCECODE="+strConveyanceCode;
				paramList = new ArrayList();
				paramList.Add(sUrl);
				sScript = Utility.GetScript("openWindowParam.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}

			sUrl = "ZonePopup.aspx?FORMID=BaseConveyanceRates&ZONECODE_TEXT="+strZoneCode+"&ZONECODE="+strtxtZoneCode;
			paramList = new ArrayList();
			paramList.Add(sUrl);
			sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
						
		}

		protected void dgBaseConvey_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
				return;

			DataRow drSelected = m_sdsConveyance.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
				return;
			
			if ((int)ViewState["ConvOperation"] != (int)Operation.Insert && (!btnExecuteQuery.Enabled))
			{
				msTextBox txtOZCode = (msTextBox)e.Item.FindControl("txtOZCode");
				msTextBox txtDZCode = (msTextBox)e.Item.FindControl("txtDZCode");
				msTextBox txtConveyance = (msTextBox)e.Item.FindControl("txtConveyance");

				if (txtOZCode !=null)
					txtOZCode.Enabled=false;
				if (txtDZCode !=null)
					txtDZCode.Enabled=false;
				if(txtConveyance != null ) 
					txtConveyance.Enabled = false;
			}

			if ((int)ViewState["ConvMode"] == (int)ScreenMode.Insert && (int)ViewState["ConvOperation"] == (int)Operation.Insert)
				e.Item.Cells[1].Enabled=false;

			if((int)ViewState["ConvMode"]==(int)ScreenMode.Insert)
				e.Item.Cells[1].Enabled = false;	
			else
				e.Item.Cells[1].Enabled = true;
		}

		protected void dgBaseConvey_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			int rowIndex = e.Item.ItemIndex;
			if ((int)ViewState["ConvMode"] == (int)ScreenMode.Insert && (int)ViewState["ConvOperation"] == (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				m_sdsConveyance.ds.Tables[0].Rows.RemoveAt(rowIndex);				
			}
			ViewState["ConvOperation"] = Operation.None;
			dgBaseConveyance.EditItemIndex = -1;
			BindConveyanceGrid();			
		}

		protected void dgBaseConvey_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			lblErrorMessage.Text = "";
			dgBaseConveyance.CurrentPageIndex = e.NewPageIndex;
			dgBaseConveyance.SelectedIndex = -1;
			dgBaseConveyance.EditItemIndex = -1;
			RetreiveSelectedPage();
		}

		private void RetreiveSelectedPage()
		{
			int iStartRow = dgBaseConveyance.CurrentPageIndex * dgBaseConveyance.PageSize;
			DataSet dsQuery = (DataSet)Session["QUERY_DS"];
			
			m_sdsConveyance = SysDataMgrDAL.GetBaseConveyDS(utility.GetAppID(), utility.GetEnterpriseID(), iStartRow, dgBaseConveyance.PageSize, dsQuery);
			int iPageCnt = Convert.ToInt32((m_sdsConveyance.QueryResultMaxSize - 1))/dgBaseConveyance.PageSize;
			if(iPageCnt < dgBaseConveyance .CurrentPageIndex)
			{
				dgBaseConveyance.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			BindConveyanceGrid();
			Session["SESSION_DS1"] = m_sdsConveyance;
		}

		protected void dgBaseConvey_Delete(object sender, DataGridCommandEventArgs e)
		{
			if (btnExecuteQuery.Enabled)
				return;

			if ((int)ViewState["ConvOperation"] == (int)Operation.Update)
			{
				dgBaseConveyance.EditItemIndex = -1;
			}
			BindConveyanceGrid();

			int rowIndex = e.Item.ItemIndex;
			m_sdsConveyance  = (SessionDS)Session["SESSION_DS1"];
			DataRow dr =m_sdsConveyance.ds.Tables[0].Rows[rowIndex];
			
			String strOZCode=(string) dr[0];
			String strDZCode=(string) dr[1];
			String strConveyance =(string) dr[2];

			if (strConveyance==null || strConveyance =="")
				return;
			if (strOZCode==null || strOZCode =="")
				return;
			if (strDZCode==null || strDZCode =="")
				return;
			
			try
			{
				// delete from table
				
				int iRowsDeleted = SysDataMgrDAL.DeleteBaseConvey(m_strAppID, m_strEnterpriseID, strOZCode, strDZCode, strConveyance);
				ArrayList paramValues = new ArrayList();
				paramValues.Add(iRowsDeleted.ToString());
				lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture(),paramValues);
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_COST",utility.GetUserCulture());
				if(strMsg.ToLower().IndexOf("child record found") != -1 )
					lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());
				else
				{
					lblErrorMessage.Text = strMsg;
				}

				Logger.LogTraceError("BaseConveyance","dgBaseConvey_Delete","RBAC003",appException.Message.ToString());
				return;
			}

			if((int)ViewState["ConvMode"] == (int)ScreenMode.Insert)
				m_sdsConveyance.ds.Tables[0].Rows.RemoveAt(rowIndex);
			else
			{
				RetreiveSelectedPage();
			}
			ViewState["ConvOperation"] = Operation.None;
			//Make the row in non-edit Mode
			EditHRow(false);
			BindConveyanceGrid();
			Logger.LogDebugInfo("BaseConveyance","dgBaseConvey_Delete","INF004","updating data grid...");			
			
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			btnExecuteQuery.Enabled = false;
			ViewState["ConvMode"]=ScreenMode.ExecuteQuery; 
			//Since Query mode is always one row in edit mode, the rowindex is set to zero
			int rowIndex = 0; 
			GetChangedRow(dgBaseConveyance.Items[0], rowIndex);
			Session["QUERY_DS"] = m_sdsConveyance.ds;
			RetreiveSelectedPage();
			if(m_sdsConveyance.ds.Tables[0].Rows.Count==0)
			{
				lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
				dgBaseConveyance.EditItemIndex = -1; 
				return;
			}
			//set all records to non-edit mode after execute query
			EditHRow(false);
			dgBaseConveyance.Columns[0].Visible=true;
			dgBaseConveyance.Columns[1].Visible=true;
		}

		protected void dgBaseConvey_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			if (btnExecuteQuery.Enabled)
			{
				return;
			}
			int iRowsAffected = 0;
			int rowIndex = e.Item.ItemIndex;
			GetChangedRow(e.Item, e.Item.ItemIndex);
		
			int iOperation = (int)ViewState["ConvOperation"];
			try
			{
				if (iOperation == (int)Operation.Insert)
				{
					iRowsAffected = SysDataMgrDAL.InsertBaseConvey(m_sdsConveyance.ds, rowIndex, m_strAppID, m_strEnterpriseID);
					ArrayList paramValues = new ArrayList();
					paramValues.Add(iRowsAffected.ToString());
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_INS",utility.GetUserCulture(),paramValues);;
				}
				else
				{
					DataSet dsChangedRow = m_sdsConveyance.ds.GetChanges();
					iRowsAffected = SysDataMgrDAL.UpdateBaseConvey(dsChangedRow,m_strAppID, m_strEnterpriseID);
					ArrayList paramValues = new ArrayList();
					paramValues.Add(iRowsAffected.ToString());
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ROWS_UPD",utility.GetUserCulture(),paramValues);;
					m_sdsConveyance.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
				}
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				strMsg = appException.InnerException.Message;
				strMsg = appException.InnerException.InnerException.Message;
				if(strMsg.IndexOf("PRIMARY KEY") != -1)
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_DUPLICATE",utility.GetUserCulture());
				else if(strMsg.IndexOf("duplicate key") != -1 )
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_DUPLICATE",utility.GetUserCulture());
				else if(strMsg.IndexOf("unique") != -1 )
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_DUPLICATE",utility.GetUserCulture());
				}
				Logger.LogTraceError("BaseConveyanceRates","dgBaseConvey_Update","RBAC003",appException.Message.ToString());
				return;
			}

			if (iRowsAffected == 0)
			{
				return;
			}
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			ViewState["ConvOperation"] = Operation.None;
			dgBaseConveyance.EditItemIndex = -1;
			BindConveyanceGrid();
			Logger.LogDebugInfo("BaseConveyanceRates","dgBaseConvey_Update","INF004","updating data grid...");			
			
		}

	}
}
