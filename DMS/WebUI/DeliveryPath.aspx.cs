using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for DeliveryPath.
	/// </summary>
	//public class DeliveryPath : com.common.applicationpages.BasePage
	public class DeliveryPath : BasePage
	{
		protected System.Web.UI.WebControls.Label lblMainheading;
		protected com.common.util.msTextBox txtLnHlCst;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.DataGrid dgDelvPath;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		//string m_strAppID = null;
		//string m_strEnterpriseID = null;
		private string m_strAppID = null;
		private string m_strEnterpriseID = null;

		//Utility utility = null;
		protected System.Web.UI.WebControls.ValidationSummary PageValid;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		SessionDS m_sdsDlvPath = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			String userCulTure = utility.GetUserCulture();
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			if(!IsPostBack)
			{
				DataSet profileDS = SysDataManager1.GetEnterpriseProfile(m_strAppID, m_strEnterpriseID);

				if(profileDS.Tables[0].Rows.Count > 0)
				{
					int currency_decimal = (int) profileDS.Tables[0].Rows[0]["currency_decimal"];
					ViewState["m_format"] = "{0:F" + currency_decimal.ToString() + "}";
				}
				
				
				QueryMode();	  
			}
			else
			{
				m_sdsDlvPath = (SessionDS) Session["SESSION_DS1"];
			}

		}

		public void QueryMode()
		{
			ViewState["DPOperation"]=Operation.None;
			ViewState["DPMode"]=ScreenMode.Query;
			ViewState["EditMode"]=false;
  	
			btnQuery.Enabled=true;
			btnExecuteQuery.Enabled=true;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);

			m_sdsDlvPath = SysDataMgrDAL.GetEmptyDlvPathCode();  

			m_sdsDlvPath.DataSetRecSize = 1;
			AddRow(); 
			Session["SESSION_DS1"] = m_sdsDlvPath;
			BindDPGrid();
			LoadDeliveryPath("delivery_type");

			EditHRow(true);
			dgDelvPath.Columns[0].Visible=false;
			dgDelvPath.Columns[1].Visible=false;
		}

		public void BindDPGrid()
		{
			dgDelvPath.VirtualItemCount = System.Convert.ToInt32(m_sdsDlvPath.QueryResultMaxSize);
			dgDelvPath.DataSource = m_sdsDlvPath.ds;
			dgDelvPath.DataBind(); 
			Session["SESSION_DS1"] = m_sdsDlvPath;
		}

		private void AddRow()
		{
			SysDataMgrDAL.AddNewRowInDeliveryPathCodeDS(ref m_sdsDlvPath);
			//BindDPGrid();
		}

		private void EditHRow(bool bItemIndex)
		{
			foreach (DataGridItem item in dgDelvPath.Items)
			{ 
				if (bItemIndex) 
				{
					dgDelvPath.EditItemIndex = item.ItemIndex; }
				else 
				{
					dgDelvPath.EditItemIndex = -1; }
			}
			dgDelvPath.DataBind();
		}

		protected void dgDelvPath_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";

			if (!btnExecuteQuery.Enabled)
			{
				int iRowsAffected = 0;
				int rowIndex = e.Item.ItemIndex;

				if (!GetChangedRowOfUpdate(e.Item, e.Item.ItemIndex))
				{
					//int iMode = (int)ViewState["Mode"];
					int iOperation = (int)ViewState["DPOperation"];
					try
					{
						if (iOperation == (int)Operation.Insert)
						{
							iRowsAffected = (SysDataMgrDAL.InsertDeliveryPathCode(m_sdsDlvPath.ds, rowIndex, m_strAppID, m_strEnterpriseID))-1;
							//lblErrorMessage.Text = iRowsAffected + " row(s) inserted.";
							ArrayList paramValues = new ArrayList();
							paramValues.Add(""+iRowsAffected);	
							lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ROWS_INS", utility.GetUserCulture(),paramValues);
						}
						else
						{
							DataSet dsChangedRow = m_sdsDlvPath.ds.GetChanges();
							iRowsAffected = (SysDataMgrDAL.UpdateDeliveryPathCode(dsChangedRow,m_strAppID, m_strEnterpriseID))-1;
							//lblErrorMessage.Text = iRowsAffected + " row(s) updated.";
							ArrayList paramValues = new ArrayList();
							paramValues.Add(""+iRowsAffected);
							lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ROWS_UPD", utility.GetUserCulture(),paramValues);
							m_sdsDlvPath.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
						}
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("PRIMARY KEY") != -1)
						{
							lblErrorMessage.Text = "Duplication of Delivery Path is Found"; 
						}
						else if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblErrorMessage.Text = "Duplicate Path Code are not allowed. Please try again.";
							
						}
						else if(strMsg.IndexOf("APPDB.SYS_C001759") != -1 )
						{
							lblErrorMessage.Text = "Duplicate Path Code are not allowed. Please try again.";
							
						}
						else if(strMsg.IndexOf("CREATE RULE ")!= -1)
						{
							lblErrorMessage.Text = "Please enter delivery_type";
							return;
						}
						else
						{
							lblErrorMessage.Text = strMsg;
						}

						Logger.LogTraceError("DeliveryPathCode.aspx.cs","dgState_Update","RBAC003",appException.Message.ToString());
						return;
					}

					if (iRowsAffected == 0)
					{
						return;
					}
					Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
					ViewState["DPOperation"] = Operation.None;
					dgDelvPath.EditItemIndex = -1;
					BindDPGrid();
					Logger.LogDebugInfo("State","dgDelvPath_Update","INF004","updating data grid...");			
				}
			}
		}

		private bool GetChangedRowOfUpdate(DataGridItem item, int rowIndex)
		{
			bool isError = false;
			decimal decLnHlCst=0 ;
			msTextBox txtPathCode = (msTextBox)item.FindControl("txtPathCode");
			TextBox txtPathDescp = (TextBox)item.FindControl("txtPathDescp");
			DropDownList DrpDlvType = (DropDownList)item.FindControl("DrpDlvType");
			msTextBox txtLnHlCst = (msTextBox)item.FindControl("txtLnHlCst");
			TextBox txtOrgStat = (TextBox)item.FindControl("txtOrgStat");
			TextBox txtDestStat = (TextBox)item.FindControl("txtDestStat");
			TextBox txtFlightVehicle = (TextBox)item.FindControl("txtFlightVehicle");
			TextBox txtAWBDriver = (TextBox)item.FindControl("txtAWBDriver");
			msTextBox txtApproxDepart = (msTextBox)item.FindControl("txtApproxDepart");
			DropDownList DrpActive = (DropDownList)item.FindControl("DrpActive");
				
			if (txtOrgStat.Text.Trim() != "")
			{
				if(!SysDataMgrDAL.IsCodeIsExistInDC(m_strAppID,m_strEnterpriseID,txtOrgStat.Text.Trim()))
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_DEL_ORIG_CODE_EXIT_IN_DC",utility.GetUserCulture());
					isError = true;
				}
			}
			
			if(!isError)
			{
				if (txtDestStat.Text.Trim() != "")
				{
					if(!SysDataMgrDAL.IsCodeIsExistInDC(m_strAppID,m_strEnterpriseID,txtDestStat.Text.Trim()))
					{
						lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_DEL_DEST_CODE_EXIT_IN_DC",utility.GetUserCulture());
						isError = true;
					}
				}	
			}

			if(!isError)
			{
				if ((txtOrgStat.Text.Trim() != "") && (txtDestStat.Text.Trim() != ""))
				{
					if(((DrpDlvType.SelectedItem.Value == "L") || 
						(DrpDlvType.SelectedItem.Value == "A"))
						&& (txtOrgStat.Text.Trim().ToUpper() == txtDestStat.Text.Trim().ToUpper()))
					{
						lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ORIG_CODE_AND_DEST_CODE_CANNOT_SAME",utility.GetUserCulture());
						isError = true;
					}
				}	
			}

			if(!isError)
			{
				if ((txtDestStat.Text.Trim() != "") &&(DrpDlvType.SelectedItem.Value == "S"))
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DEST_CODE_MUST_BE_BLANK_FOR_SHORT",utility.GetUserCulture());
					isError = true;
				}	
			}
			if(!isError)
			{
				if ((txtDestStat.Text.Trim() != "") &&(DrpDlvType.SelectedItem.Value == "W"))
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DEST_CODE_MUST_BE_BLANK_FOR_CURRENT",utility.GetUserCulture());
					isError = true;
				}	
			}

			if(!isError)
			{	
				string strPathCode = txtPathCode.Text.ToString();
				string strPathDescp = txtPathDescp.Text.ToString();
				string strActive = DrpActive.SelectedItem.Value;
				string strDlvType = DrpDlvType.SelectedItem.Value;
				if(strActive=="Y")
				{
					if (strDlvType=="Air")
					{
						strDlvType="A";
					}
					else if (strDlvType=="Short Haul")
					{
						strDlvType="S";
					}
					else if (strDlvType=="Long Haul")
					{
						strDlvType="L";
					}
				}
				else
				{
					if (strDlvType=="A")
					{
						strDlvType="1";
					}
					else if (strDlvType=="S")
					{
						strDlvType="2";
					}
					else if (strDlvType=="L")
					{
						strDlvType="3";
					}
				}
				
			
				if ((txtLnHlCst.Text !="")&&(txtLnHlCst.Text !=null))
				{
					decLnHlCst = System.Convert.ToDecimal(txtLnHlCst.Text);
				}
				string strOrgStat = txtOrgStat.Text.ToString();
				string strDestStat = txtDestStat.Text.ToString();
				string strFlight = txtFlightVehicle.Text.ToString();
				string strAWB = txtAWBDriver.Text.ToString();

				DataRow dr = m_sdsDlvPath.ds.Tables[0].Rows[rowIndex];
				dr[0] = strPathCode;
				dr[1] = strPathDescp;
				dr[2] = strDlvType;
				dr[3] = decLnHlCst;
				dr[4]=strOrgStat;
				dr[5]=strDestStat;
				dr[6] = strFlight;
				dr[7]=strAWB;

				if (txtApproxDepart !=null)
				{
					if (txtApproxDepart.Text.Trim() != "")
					{
						dr[8]= txtApproxDepart.Text;
					}
					else
					{
						dr[8]=System.DBNull.Value;
					}
				}
				dr[9]=strActive;

				Session["SESSION_DS1"] = m_sdsDlvPath;
			}
			return isError;
		}


		private void GetChangedRow(DataGridItem item, int rowIndex)
		{
			decimal decLnHlCst=0 ;
			msTextBox txtPathCode = (msTextBox)item.FindControl("txtPathCode");
			TextBox txtPathDescp = (TextBox)item.FindControl("txtPathDescp");
			DropDownList DrpDlvType = (DropDownList)item.FindControl("DrpDlvType");
			msTextBox txtLnHlCst = (msTextBox)item.FindControl("txtLnHlCst");
			TextBox txtOrgStat = (TextBox)item.FindControl("txtOrgStat");
			TextBox txtDestStat = (TextBox)item.FindControl("txtDestStat");
			TextBox txtFlightVehicle = (TextBox)item.FindControl("txtFlightVehicle");
			TextBox txtAWBDriver = (TextBox)item.FindControl("txtAWBDriver");
			msTextBox txtApproxDepart = (msTextBox)item.FindControl("txtApproxDepart");
			DropDownList DrpActive = (DropDownList)item.FindControl("DrpActive");

			string strPathCode = txtPathCode.Text.ToString();
			string strPathDescp = txtPathDescp.Text.ToString();
			string strDlvType = DrpDlvType.SelectedItem.Value;
			string strActive = DrpActive.SelectedItem.Value;
			if (strDlvType=="Air")
			{
				strDlvType="A";
			}
			else if (strDlvType=="Short Haul")
			{
				strDlvType="S";
			}
			else if (strDlvType=="Long Haul")
			{
				strDlvType="L";
			}

			
			if ((txtLnHlCst.Text !="")&&(txtLnHlCst.Text !=null))
			{
				decLnHlCst = System.Convert.ToDecimal(txtLnHlCst.Text);
			}
			string strOrgStat = txtOrgStat.Text.ToString();
			string strDestStat = txtDestStat.Text.ToString();
			string strFlight = txtFlightVehicle.Text.ToString();
			string strAWB = txtAWBDriver.Text.ToString();

			DataRow dr = m_sdsDlvPath.ds.Tables[0].Rows[rowIndex];
			dr[0] = strPathCode;
			dr[1] = strPathDescp;
			dr[2] = strDlvType;
			dr[3] = decLnHlCst;
			dr[4]=strOrgStat;
			dr[5]=strDestStat;
			dr[6] = strFlight;
			dr[7]=strAWB;

			if (txtApproxDepart !=null)
			{
				if (txtApproxDepart.Text.Trim() != "")
				{
					dr[8]= txtApproxDepart.Text;
				}
				else
				{
					dr[8]=System.DBNull.Value;
				}
			}
			dr[9]= strActive;

			Session["SESSION_DS1"] = m_sdsDlvPath;
		}

		protected void  dgDelvPath_Edit(object sender, DataGridCommandEventArgs e)
		{
			RequiredFieldValidator rvDestStat = (RequiredFieldValidator)e.Item.FindControl("rvDestStat");
			ViewState["EditRow"] = e.Item.ItemIndex;

			lblErrorMessage.Text="";
			LoadDeliveryPath("delivery_type"); 
			if((int)ViewState["DPMode"] == (int)ScreenMode.Insert && (int) ViewState["DPOperation"] == (int)Operation.Insert && dgDelvPath.EditItemIndex > 0)
			{
				m_sdsDlvPath.ds.Tables[0].Rows.RemoveAt(dgDelvPath.EditItemIndex);
				dgDelvPath.CurrentPageIndex = 0;
			}
			dgDelvPath.EditItemIndex = e.Item.ItemIndex;
			ViewState["DPOperation"] = Operation.Update;
			BindDPGrid();
			getPageControls(Page);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}

		private void InitializeComponent()
		{
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			if (btnExecuteQuery.Enabled == true)
			{
				btnExecuteQuery.Enabled = false;
			}
			if((int)ViewState["DPMode"] != (int)ScreenMode.Insert || m_sdsDlvPath.ds.Tables[0].Rows.Count >= dgDelvPath.PageSize)
			{
				m_sdsDlvPath = SysDataMgrDAL.GetEmptyDlvPathCode();
				AddRow();
				
			}
			else
			{
				AddRow();
			}
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,false,m_moduleAccessRights);
			EditHRow(false);
			ViewState["DPMode"] = ScreenMode.Insert;
			ViewState["DPOperation"] = Operation.Insert;
			dgDelvPath.Columns[0].Visible=true;
			dgDelvPath.Columns[1].Visible=true;
			dgDelvPath.EditItemIndex = m_sdsDlvPath.ds.Tables[0].Rows.Count - 1;
			dgDelvPath.CurrentPageIndex = 0;
			BindDPGrid();
			getPageControls(Page);
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			dgDelvPath.CurrentPageIndex = 0;
			QueryMode();
		}
		protected void dgDelvPath_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			String strType=null;
			String strActive = null;
			DropDownList DrpDlvType = (DropDownList)e.Item.FindControl("DrpDlvType");
			DropDownList DrpActive = (DropDownList)e.Item.FindControl("DrpActive");
			//int i=0;
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsDlvPath.ds.Tables[0].Rows[e.Item.ItemIndex];
			
			   
			if (DrpDlvType!=null)
			{
				strType = (String)drSelected["delivery_type"];
				DrpDlvType.SelectedIndex = DrpDlvType.Items.IndexOf(DrpDlvType.Items.FindByValue(strType));	

			}
			if (DrpActive!=null)
			{
				strActive = DrpActive.SelectedItem.Value;
				DrpActive.SelectedIndex = DrpActive.Items.IndexOf(DrpActive.Items.FindByValue(strActive));
			}

            if (drSelected.RowState == DataRowState.Deleted)
            {
                return;
            }

            if ((int)ViewState["DPOperation"] != (int)Operation.Insert && (!btnExecuteQuery.Enabled))
            {
                msTextBox txtPathCode = (msTextBox)e.Item.FindControl("txtPathCode");

                if (txtPathCode != null)
                {
                    txtPathCode.Enabled = false;

                    RequiredFieldValidator rvDestStat = (RequiredFieldValidator)e.Item.FindControl("rvDestStat");
                    if (strType != "")
                    {
                        if (strType == "S" || strType == "W")
                        {
                            rvDestStat.Enabled = false;
                        }
                        else
                        {
                            rvDestStat.Enabled = true;
                        }
                    }
                }
            }

            //if ((int)ViewState["DPMode"] == (int)ScreenMode.Insert && (int)ViewState["DPOperation"] == (int)Operation.Insert)
            //{
            //    e.Item.Cells[1].Enabled = false;

            //    TextBox txtDestStat = (TextBox)e.Item.FindControl("txtDestStat");
            //    if (txtDestStat != null)
            //    {
            //        RequiredFieldValidator rvDestStat = (RequiredFieldValidator)e.Item.FindControl("rvDestStat");
            //        if (strType == "")
            //        {
            //            rvDestStat.Enabled = false;
            //        }
            //    }
            //}
            //if ((int)ViewState["DPMode"] == (int)ScreenMode.Insert)
            //{
            //    e.Item.Cells[1].Enabled = false;
            //}
            //else
            //{
            //    e.Item.Cells[1].Enabled = true;

            //}
            string codeID = null;
			if(strActive == "N")
				codeID = "delivery_type_Unused";
			else
				codeID = "delivery_type";

			DataSet dsDlvType = LoadDeliveryPath(codeID);
			if (DrpDlvType!=null)
			{
				int cnt = dsDlvType.Tables[0].Rows.Count;  
				DrpDlvType.DataSource = dsDlvType;
				DrpDlvType.DataTextField = "DeliveryPathValue"; 
				DrpDlvType.DataValueField="DeliveryPathText";
				DrpDlvType.DataBind();
				strType = (string)drSelected[2];
				strActive = drSelected[9].ToString();
				
				if(strType == "1")
					strType = "A";
				else if(strType == "2")
					strType = "S";
				else if(strType == "3")
					strType = "L";

				if (strType.Trim() != "") 
				{
					DrpDlvType.SelectedIndex = DrpDlvType.Items.IndexOf(DrpDlvType.Items.FindByValue(strType)); 
				}
				else
				{
					DrpDlvType.SelectedIndex = DrpDlvType.Items.IndexOf(DrpDlvType.Items.FindByValue("S")); 
				}
				if (strActive != "")
				{
					DrpActive.SelectedIndex = DrpActive.Items.IndexOf(DrpActive.Items.FindByValue(strActive));
				}
				else
				{
					DrpActive.SelectedIndex = DrpActive.Items.IndexOf(DrpActive.Items.FindByValue("Y"));
				}
			}

			Label lblLnHlCst = (Label)e.Item.FindControl("lblLnHlCst");

			if (lblLnHlCst != null && drSelected["line_haul_cost"] != System.DBNull.Value) 
			{
				lblLnHlCst.Text = String.Format((String)ViewState["m_format"], drSelected["line_haul_cost"]);
			}

			msTextBox txtLnHlCst = (msTextBox)e.Item.FindControl("txtLnHlCst");
			if (txtLnHlCst != null && drSelected["line_haul_cost"] != System.DBNull.Value) 
			{
				txtLnHlCst.Text = String.Format((String)ViewState["m_format"], drSelected["line_haul_cost"]);
			}

			Label lblOrgStat = (Label)e.Item.FindControl("lblOrgStat");

			if (lblOrgStat != null && drSelected["origin_station"] != System.DBNull.Value) 
			{
				lblOrgStat.Text = drSelected["origin_station"].ToString().ToUpper();
			}

			TextBox txtOrgStat = (TextBox)e.Item.FindControl("txtOrgStat");
			if (txtOrgStat != null && drSelected["origin_station"] != System.DBNull.Value) 
			{
				txtOrgStat.Text = drSelected["origin_station"].ToString().ToUpper();
			}

			Label lblDestStation = (Label)e.Item.FindControl("lblDestStation");

			if (lblDestStation != null && drSelected["destination_station"] != System.DBNull.Value) 
			{
				lblDestStation.Text = drSelected["destination_station"].ToString().ToUpper();
			}

			TextBox txtDestStatx = (TextBox)e.Item.FindControl("txtDestStat");
			if (txtDestStatx!= null && drSelected["destination_station"] != System.DBNull.Value) 
			{
				txtDestStatx.Text = drSelected["destination_station"].ToString().ToUpper();
			}
		}
		protected void dgDelvPath_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			int rowIndex = e.Item.ItemIndex;
			if ((int)ViewState["DPMode"] == (int)ScreenMode.Insert && (int)ViewState["DPOperation"] == (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				m_sdsDlvPath.ds.Tables[0].Rows.RemoveAt(rowIndex);
				
			}
			ViewState["DPOperation"] = Operation.None;
			dgDelvPath.EditItemIndex = -1;
			BindDPGrid();
			
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			btnExecuteQuery.Enabled = false;
			ViewState["DPMode"]=ScreenMode.ExecuteQuery; 
			//Since Query mode is always one row in edit mode, the rowindex is set to zero
			int rowIndex = 0; 
			GetChangedRow(dgDelvPath.Items[0], rowIndex);
			Session["QUERY_DS"] = m_sdsDlvPath.ds;
			DataRow dr = m_sdsDlvPath.ds.Tables[0].Rows[0];
			string strActive =(String)dr[9];
			string codeID = null;
			if(strActive == "N")
				codeID = "delivery_type_Unused";
			else
				codeID = "delivery_type";

			LoadDeliveryPath(codeID);
			RetreiveSelectedPage();
			if(m_sdsDlvPath.ds.Tables[0].Rows.Count==0)
			{
				lblErrorMessage.Text = "No Matching records found..";
				dgDelvPath.EditItemIndex = -1; 
				return;
			}
			//set all records to non-edit mode after execute query
			EditHRow(false);
			dgDelvPath.Columns[0].Visible=true;
			dgDelvPath.Columns[1].Visible=true;
			
		
		}
		private void RetreiveSelectedPage()
		{
			int iStartRow = dgDelvPath.CurrentPageIndex * dgDelvPath.PageSize;
			DataSet dsQuery = (DataSet)Session["QUERY_DS"];
			m_sdsDlvPath = SysDataMgrDAL.GetDeliveryPathCodeDS(utility.GetAppID(), utility.GetEnterpriseID(), iStartRow, dgDelvPath.PageSize, dsQuery);
			int iPageCnt = Convert.ToInt32((m_sdsDlvPath.QueryResultMaxSize - 1))/dgDelvPath.PageSize;
			if(iPageCnt < dgDelvPath.CurrentPageIndex)
			{
				dgDelvPath.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			
			dgDelvPath.SelectedIndex=-1;
			dgDelvPath.EditItemIndex=-1;

			BindDPGrid();
			Session["SESSION_DS1"] = m_sdsDlvPath;
		}
		
		protected void dgDelvPath_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgDelvPath.CurrentPageIndex = e.NewPageIndex;
			RetreiveSelectedPage();
			Logger.LogDebugInfo("State","dgDelvPath_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		protected void dgDelvPath_Delete(object sender, DataGridCommandEventArgs e)
		{
			if (!btnExecuteQuery.Enabled)
			{
				if ((int)ViewState["DPOperation"] == (int)Operation.Update)
				{
					dgDelvPath.EditItemIndex = -1;
				}
				BindDPGrid ();

				int rowIndex = e.Item.ItemIndex;
				m_sdsDlvPath  = (SessionDS)Session["SESSION_DS1"];

				try
				{
					DataRow dr = m_sdsDlvPath.ds.Tables[0].Rows[rowIndex];
					string strPathCode = (string)dr[0];

					if (SysDataMgrDAL.IsPathCodeIsExistManifest(m_strAppID, m_strEnterpriseID,strPathCode))
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"CANNOT_DEL_MANIFEST",utility.GetUserCulture());
						return;
					}
					
					// delete from table
					int iRowsDeleted = (SysDataMgrDAL.DeleteDeliveryPathCode(m_sdsDlvPath.ds, rowIndex, m_strAppID, m_strEnterpriseID))-1;
                    if (iRowsDeleted < 0)
                        iRowsDeleted = 0;

                    lblErrorMessage.Text = iRowsDeleted + " row(s) deleted.";
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					if(strMsg.IndexOf("FK") != -1)
					{
						//lblErrorMessage.Text = "Error Deleting Path Code. Path Code being used in some transaction(s)";
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_PATH_TRANS",utility.GetUserCulture());
					}
					else if(strMsg.ToLower().IndexOf("child record found") != -1 )
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_PATH_TRANS",utility.GetUserCulture());
							
					}			
					else
					{
						lblErrorMessage.Text = strMsg;
					}

					Logger.LogTraceError("DeliveryPath.aspx.cs","dgState_Delete","RBAC003",appException.Message.ToString());
					return;
				}
				if((int)ViewState["DPMode"] == (int)ScreenMode.Insert)
				{
					m_sdsDlvPath.ds.Tables[0].Rows.RemoveAt(rowIndex);
				}
				else
				{
					RetreiveSelectedPage();
				}
				ViewState["DPOperation"] = Operation.None;
				//Make the row in non-edit Mode
				EditHRow(false);
				BindDPGrid();
				Logger.LogDebugInfo("DeliveryPath","dgDelvPath_Delete","INF004","updating data grid...");			
			}
		}

		public DataSet  LoadDeliveryPath(string codeID)
		{
			
			DataSet dsDlvType = new DataSet();
			DataTable dtDlvType = new DataTable();
			dtDlvType.Columns.Add("DeliveryPathText",typeof(string));
			dtDlvType.Columns.Add("DeliveryPathValue",typeof(string));
 
   			ArrayList systemCodes = Utility.GetCodeValues(m_strAppID ,utility.GetUserCulture(),codeID,CodeValueType.StringValue);
			DataRow dr; 
//			DataRow dr =  dtDlvType.NewRow();
//			dr[1] = System.DBNull.Value;
//			dr[0] = System.DBNull.Value;
//			dtDlvType.Rows.Add(dr); 
			if(systemCodes != null)
			{
				foreach(SystemCode sysCode in systemCodes)
				{
					dr = dtDlvType.NewRow();
					dr[1] = sysCode.Text;
					dr[0] = sysCode.StringValue;
					dtDlvType.Rows.Add(dr); 
			
				}
			}
			dsDlvType.Tables.Add(dtDlvType);
			int cnt = dsDlvType.Tables [0].Rows.Count; 
			return dsDlvType;
		}

		public void chkDrpDlvType(object sender, System.EventArgs e)
		{
			if ((int)ViewState["DPOperation"] != (int)Operation.None)
			{
//				DropDownList DrpDlvType = (DropDownList)sender;				
//				RequiredFieldValidator rvDestStat = null;
				DropDownList DrpDlvType = null;
				RequiredFieldValidator rvDestStat = null;

//				for (int i=0; i<dgDelvPath.Items.Count; i++)
//				{
					//					if ((DropDownList)dgDelvPath.Items[i].Cells[4].Controls[1] == sender)
					//						rvDestStat	= (RequiredFieldValidator)dgDelvPath.Items[i].FindControl("rvDestStat");
				if((int)ViewState["DPOperation"] == (int)Operation.Insert)
				{
					DrpDlvType = (DropDownList)dgDelvPath.Items[0].Cells[4].FindControl("DrpDlvType");
					rvDestStat	= (RequiredFieldValidator)dgDelvPath.Items[0].FindControl("rvDestStat");
				}
				else
				{
					DrpDlvType = (DropDownList)dgDelvPath.Items[(int)ViewState["EditRow"]].Cells[4].FindControl("DrpDlvType");
					rvDestStat	= (RequiredFieldValidator)dgDelvPath.Items[(int)ViewState["EditRow"]].FindControl("rvDestStat");
				}
//				}

				if (DrpDlvType.SelectedItem.Value == "S" || DrpDlvType.SelectedItem.Value == "W")
				{
					rvDestStat.Enabled = false;
				}
				else
				{
					rvDestStat.Enabled = true;
				}
			}
		}
	}
}
		

