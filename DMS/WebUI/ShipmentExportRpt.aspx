<%@ Page language="c#" Codebehind="ShipmentExportRpt.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ShipmentExportRpt" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>COD Amount Collected Report</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="GridLayout">
		<form id="CODAmountCollectedReport" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 20px; POSITION: absolute; TOP: 40px" runat="server"
				Text="Query" CssClass="queryButton" Width="64px"></asp:button><asp:label id="lblErrorMessage" style="Z-INDEX: 105; LEFT: 24px; POSITION: absolute; TOP: 69px"
				runat="server" CssClass="errorMsgColor" Width="700px" Height="17px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnExecuteQuery" style="Z-INDEX: 102; LEFT: 90px; POSITION: absolute; TOP: 40px"
				runat="server" Text="Generate" CssClass="queryButton" Width="123px"></asp:button>
			<TABLE id="tblShipmentTracking" style="Z-INDEX: 104; LEFT: 21px; WIDTH: 792px; POSITION: absolute; TOP: 90px; HEIGHT: 208px"
				width="792" border="0" runat="server">
				<TR>
					<TD style="WIDTH: 393px; HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 392px; HEIGHT: 144px"><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="144px" Font-Bold="True">Shipment Manifest Date</asp:label></legend>
							<TABLE id="tblDates" style="WIDTH: 368px; HEIGHT: 119px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<td style="WIDTH: 9px; HEIGHT: 14px"></td>
									<TD colSpan="3" style="WIDTH: 412px; HEIGHT: 14px">&nbsp;&nbsp;&nbsp;</TD>
								</TR>
								<TR>
									<td style="WIDTH: 9px; HEIGHT: 34px"></td>
									<TD colSpan="3" style="WIDTH: 412px; HEIGHT: 34px">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" Text="Month" CssClass="tableRadioButton" Width="73px"
											Height="21px" AutoPostBack="True" GroupName="EnterDate" Checked="True"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Width="88px" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" CssClass="tableLabel" Width="30px" Height="15px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" MaxLength="5" TextMaskType="msNumeric"
											NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
								</TR>
								<TR>
									<td style="WIDTH: 9px; HEIGHT: 31px"></td>
									<TD style="WIDTH: 412px; HEIGHT: 31px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" Text="Period" CssClass="tableRadioButton" Width="62px"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
								</TR>
								<TR>
									<td style="WIDTH: 9px; HEIGHT: 45px"></td>
									<TD colSpan="3" style="WIDTH: 412px; HEIGHT: 45px">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" Text="Date" CssClass="tableRadioButton" Width="74px"
											Height="22px" AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
								</TR>
							</TABLE>
						</fieldset>
						<P>&nbsp;</P>
						<P><FONT face="Tahoma"></FONT>&nbsp;</P>
						<P><FONT face="Tahoma"></FONT>&nbsp;</P>
						<P><FONT face="Tahoma"></FONT>&nbsp;</P>
						<P><FONT face="Tahoma"></FONT>&nbsp;</P>
					</TD>
					<TD style="WIDTH: 683px; HEIGHT: 137px" vAlign="top">
						<FIELDSET style="WIDTH: 384px; HEIGHT: 144px"><LEGEND>
								<asp:label id="lblPayerType" runat="server" Width="72px" CssClass="tableHeadingFieldset" Font-Bold="True">Payer Type</asp:label></LEGEND>
							<TABLE id="tblPayerType" style="WIDTH: 300px; HEIGHT: 73px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<TD></TD>
									<TD style="WIDTH: 110px" colSpan="2" height="1">&nbsp;</TD>
									<TD></TD>
									<TD style="WIDTH: 1px"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD class="tableLabel" style="WIDTH: 110px" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="lblPayType" runat="server" Width="98px" CssClass="tableLabel" Height="22px">Payer Type</asp:label></TD>
									<TD>
										<asp:listbox id="lsbCustType" runat="server" Width="137px" Height="61px" SelectionMode="Multiple"></asp:listbox></TD>
									<TD style="WIDTH: 1px"></TD>
								</TR>
								<TR height="33">
									<TD bgColor="blue"></TD>
									<TD class="tableLabel" style="WIDTH: 110px" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="lblPayCode" runat="server" Width="79px" CssClass="tableLabel" Height="22px">Payer Code</asp:label>&nbsp;&nbsp;</TD>
									<TD>
										<asp:textbox id="txtPayerCode" runat="server" Width="139px" CssClass="textField"></asp:textbox>&nbsp;&nbsp;
										<asp:button id="btnPayerCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<TD style="WIDTH: 1px"></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
			</TABLE>
			<asp:label id="Label1" style="Z-INDEX: 103; LEFT: 20px; POSITION: absolute; TOP: 4px" runat="server"
				CssClass="maintitleSize" Width="484px" Height="27px"> Shipment Export Report</asp:label></TR></TABLE></TR></TABLE><input 
style="Z-INDEX: 106; LEFT: 222px; POSITION: absolute; TOP: 35px" type=hidden 
value="<%=strScrollPosition%>" name=ScrollPosition>
		</form>
	</body>
</HTML>
