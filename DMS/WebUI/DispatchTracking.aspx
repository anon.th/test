<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="DispatchTracking.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.DispatchTracking" EnableEventValidation="False" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DispatchTracking</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<BODY onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();">
		<form id="DispatchTracking" method="post" runat="server">
			<asp:button style="Z-INDEX: 103; POSITION: absolute; TOP: 50px; LEFT: 211px" id="btnOutstanding"
				runat="server" CssClass="queryButton" Text="Outstanding Dispatch"></asp:button><asp:button style="Z-INDEX: 102; POSITION: absolute; TOP: 50px; LEFT: 82px" id="btnExecuteQuery"
				runat="server" CssClass="queryButton" Text="Execute Query" Width="128px"></asp:button><asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 50px; LEFT: 19px" id="btnQuery" runat="server"
				CssClass="queryButton" Text="Query" Width="62px"></asp:button>&nbsp;
			<TABLE style="Z-INDEX: 104; POSITION: absolute; WIDTH: 303px; HEIGHT: 80px; TOP: 89px; LEFT: 20px"
				id="Table1" border="0" cellSpacing="1" cellPadding="1" width="303" runat="server">
				<TR>
					<TD style="WIDTH: 118px" class="tableLabel"><asp:label id="lblDispatchType" runat="server" Width="112px">Dispatch Type</asp:label></TD>
					<TD><asp:dropdownlist id="ddDispatchType" runat="server" Width="150px"></asp:dropdownlist></TD>
					<TD></TD>
					<TD style="WIDTH: 65px"></TD>
					<TD style="WIDTH: 166px"></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 118px" class="tableLabel"><asp:label id="lblBookNo" runat="server" Width="112px">Booking No</asp:label></TD>
					<TD><cc1:mstextbox id="txtBookNo" runat="server" CssClass="textField" Width="150px" NumberPrecision="10"
							NumberMaxValue="2147483647" Height="18px" TextMaskType="msNumeric" MaxLength="10"></cc1:mstextbox></TD>
					<TD></TD>
					<TD style="WIDTH: 65px"><FONT face="Tahoma"></FONT></TD>
					<TD style="WIDTH: 166px"><FONT face="Tahoma"></FONT></TD>
					<TD><FONT face="Tahoma"></FONT></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 118px" class="tableLabel"><asp:label id="lblSenderName" runat="server" Width="112px">Sender Name</asp:label></TD>
					<TD><asp:textbox id="txtSender" runat="server" CssClass="textfield" Width="150px"></asp:textbox></TD>
					<TD class="tableLabel"><FONT face="Tahoma"></FONT></TD>
					<TD style="WIDTH: 80px" class="tableLabel"><FONT face="Tahoma">
							<P align="center"><asp:label id="Label2" runat="server" Width="56px">   Payer ID</asp:label></P>
						</FONT>
					</TD>
					<TD style="WIDTH: 166px"><FONT face="Tahoma"><asp:textbox id="txtPayerID" runat="server" CssClass="textfield" Width="136px"></asp:textbox></FONT></TD>
					<TD><FONT face="Tahoma"><asp:button id="btnPayerID" runat="server" CssClass="searchButton" Text="..." Width="23px"></asp:button></FONT></TD>
				</TR>
			</TABLE>
			<fieldset style="Z-INDEX: 105; POSITION: absolute; WIDTH: 632px; HEIGHT: 56px; TOP: 240px; LEFT: 15px"><legend><asp:label id="lblBookDate" runat="server" CssClass="tableHeadingFieldset" Width="90px">Booking Date</asp:label></legend>
				<TABLE id="Table2" border="0" cellSpacing="0" cellPadding="0" width="622" runat="server">
					<TR>
						<td></td>
						<TD class="tableLabel" colSpan="3">&nbsp;
							<asp:label id="lblBookFromDate" runat="server" Width="111px">From Date</asp:label><cc1:mstextbox id="txtBookFromDate" runat="server" CssClass="textfield" Width="75px" TextMaskType="msDate"
								MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="lblBookToDate" runat="server" Width="99px">To Date</asp:label>&nbsp;&nbsp;
							<cc1:mstextbox id="txtBookToDate" runat="server" CssClass="textfield" Width="75px" TextMaskType="msDate"
								MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
						<td></td>
					</TR>
				</TABLE>
			</fieldset>
			<fieldset style="Z-INDEX: 107; POSITION: absolute; WIDTH: 632px; HEIGHT: 56px; TOP: 296px; LEFT: 14px"><legend><asp:label id="lblPickDate" runat="server" CssClass="tableHeadingFieldset" Width="90px">Picking Date</asp:label></legend>
				<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="621" runat="server">
					<TR>
						<td></td>
						<td class="tableLabel" colSpan="3">&nbsp;
							<asp:label id="lblPickFromDate" runat="server" Width="111px">From Date</asp:label><cc1:mstextbox id="txtPickFromDate" runat="server" CssClass="textfield" Width="75px" TextMaskType="msDate"
								MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="lblPickToDate" runat="server">To Date</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
							&nbsp;
							<cc1:mstextbox id="txtPickToDate" runat="server" CssClass="textfield" Width="75px" TextMaskType="msDate"
								MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></td>
						<td></td>
					</TR>
				</TABLE>
			</fieldset>
			<asp:label style="Z-INDEX: 108; POSITION: absolute; TOP: 10px; LEFT: 24px" id="Label1" runat="server"
				CssClass="maintitleSize" Width="365px" Height="27px">Dispatch Tracking</asp:label><input 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition>&nbsp;&nbsp;
			<fieldset style="Z-INDEX: 105; POSITION: absolute; WIDTH: 632px; HEIGHT: 72px; TOP: 168px; LEFT: 15px"><LEGEND><asp:label id="lblStatus" runat="server" CssClass="tableHeadingFieldset" Width="40px" Font-Bold="True">Status</asp:label></LEGEND>
				<TABLE style="WIDTH: 617px; HEIGHT: 44px" id="Table3" border="0" cellSpacing="0" cellPadding="0"
					runat="server">
					<TR>
						<TD bgColor="blue"></TD>
						<TD class="tableLabel" colSpan="3">&nbsp;
							<asp:checkbox id="chkboxOutStanding" runat="server" Text="Get all outstanding Dispatch" AutoPostBack="True"></asp:checkbox></TD>
						<TD bgColor="blue"></TD>
					</TR>
					<TR>
						<TD bgColor="blue"></TD>
						<TD class="tableLabel" colSpan="3">&nbsp;
							<asp:label id="lblStatusCode" runat="server" Width="111px">Status Code</asp:label><asp:textbox id="txtStatus" runat="server" CssClass="textfield" Width="121px"></asp:textbox><asp:button id="btnStatusSrch" runat="server" CssClass="searchButton" Text="..." Width="23px"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="lblExceptionCode" runat="server" Width="109px">Exception Code</asp:label>&nbsp;
							<asp:textbox id="txtException" runat="server" CssClass="textfield" Width="136px"></asp:textbox><asp:button id="btnExceptionSrch" runat="server" CssClass="searchButton" Text="..." Width="23px"></asp:button></TD>
						<TD bgColor="blue"></TD>
					</TR>
				</TABLE>
			</fieldset>
			<P></P>
			<P><FONT face="Tahoma"></FONT>&nbsp;</P>
			<P><FONT face="Tahoma"></FONT>&nbsp;</P>
			<P><FONT face="Tahoma"></FONT>&nbsp;</P>
			<P><FONT face="Tahoma"></FONT>&nbsp;</P>
			<P><FONT face="Tahoma"></FONT>&nbsp;</P>
			<P><FONT face="Tahoma"></FONT>&nbsp;</P>
			<P><FONT face="Tahoma"></FONT>&nbsp;</P>
			<P><FONT face="Tahoma"></FONT>&nbsp;</P>
			<P><FONT face="Tahoma"></FONT>&nbsp;</P>
			<P></P>
			<!--<FIELDSET style="WIDTH: 640px; HEIGHT: 72px; LEFT: 14px"><LEGEND>-->
			<FIELDSET style="Z-INDEX: 109; POSITION: absolute; WIDTH: 632px; HEIGHT: 72px; TOP: 352px; LEFT: 14px"><LEGEND><asp:label id="lblRouteType" runat="server" CssClass="tableHeadingFieldset" Width="160px" Font-Bold="True">Pickup Route / DC Selection</asp:label></LEGEND>
				<TABLE style="WIDTH: 406px; HEIGHT: 8px" id="tblRouteType" border="0" cellSpacing="0" cellPadding="0"
					align="left" runat="server">
					<TR>
						<TD style="WIDTH: 105px; HEIGHT: 26px" class="tableLabel"><FONT face="Tahoma">&nbsp;&nbsp;
							</FONT>
							<asp:label id="lblPickupRoute" runat="server" Width="112px">Pickup Route</asp:label></TD>
						<TD style="HEIGHT: 26px"><FONT face="Tahoma"><asp:textbox id="txtRouteCode" runat="server" CssClass="textfield" Width="150px"></asp:textbox><asp:button id="btnRouteCode" runat="server" CssClass="searchButton" Text="..." Width="23px"></asp:button></FONT></TD>
						<TD style="HEIGHT: 26px"></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 105px; HEIGHT: 30px" class="tableLabel">&nbsp;<FONT face="Tahoma">&nbsp;
							</FONT>
							<asp:label id="lblODC" runat="server" Width="168px">Origin Distribution Center</asp:label></TD>
						<TD style="HEIGHT: 30px"><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" CssClass="tableLabel" Width="120px" AutoPostBack="True"
								Runat="server" TextBoxColumns="18" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbComboDistributionCenterSelect"></DBCOMBO:DBCOMBO></TD>
						<TD style="HEIGHT: 30px"></TD>
					</TR>
				</TABLE>
			</FIELDSET>
			<P></P>
			<P></P>
			<P><FONT face="Tahoma"></FONT></P>
		</form>
	</BODY>
</HTML>
