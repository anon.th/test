<%@ Page language="c#" Codebehind="PostDeliveryCOD.aspx.cs" AutoEventWireup="false" Inherits="com.ties.PostDeliveryCOD" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PostDeliveryCOD</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="PostDeliveryCOD" method="post" runat="server">
			<P><asp:label id="lblMainTitle" style="Z-INDEX: 105; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server" CssClass="mainTitleSize" Width="477px" Height="26px">Post Delivery COD Details Capture</asp:label></P>
			<DIV id="divMain" style="Z-INDEX: 101; LEFT: 2px; WIDTH: 838px; POSITION: relative; TOP: 23px; HEIGHT: 1749px" MS_POSITIONING="GridLayout" runat="server">
				<TABLE id="MainTable" style="Z-INDEX: 101; LEFT: 0px; WIDTH: 726px; POSITION: absolute; TOP: 5px; HEIGHT: 1611px" width="726" border="0" runat="server">
					<TR vAlign="top">
						<TD vAlign="top" width="738"><asp:button id="btnQry" runat="server" CssClass="queryButton" Width="61px" CausesValidation="False" Text="Query"></asp:button><asp:button id="btnExecQry" runat="server" CssClass="queryButton" Width="130px" CausesValidation="False" Text="Execute Query"></asp:button><asp:button id="btnSave" runat="server" CssClass="queryButton" Width="66px" Text="Save"></asp:button><FONT face="Tahoma">&nbsp;
								<DIV style="DISPLAY: inline; WIDTH: 333px; HEIGHT: 19px" ms_positioning="FlowLayout"></DIV>
							</FONT>
							<asp:button id="btnGoToFirstPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text="|<"></asp:button><asp:button id="btnPreviousPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text="<"></asp:button>
							<cc1:mstextbox id="txtGoToRec" tabIndex="8" runat="server" CssClass="textFieldRightAlign" Width="28px" Enabled="True" MaxLength="5" TextMaskType="msNumeric" NumberScale="0" NumberPrecision="8" NumberMinValue="1" NumberMaxValue="999999" AutoPostBack="True" Wrap="False"></cc1:mstextbox>
							<asp:button id="btnNextPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text=">"></asp:button><asp:button id="btnGoToLastPage" runat="server" CssClass="queryButton" Width="24px" CausesValidation="False" Text=">|"></asp:button><FONT face="Tahoma"><asp:label id="lblErrorMsg" runat="server" CssClass="errorMsgColor" Width="537px" Height="19px"></asp:label><FONT face="Tahoma"><asp:label id="lblNumRec" runat="server" CssClass="RecMsg" Width="187px" Height="19px"></asp:label><asp:validationsummary id="PageValidationSummary" runat="server" Width="346px" Height="39px" Visible="True" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary></FONT></FONT></TD>
					</TR>
					<TR>
						<TD width="738" style="HEIGHT: 115px">
							<TABLE id="TblBookDtl" style="WIDTH: 737px; HEIGHT: 105px" width="737" border="0" runat="server">
								<TR height="25">
									<TD width="3%"></TD>
									<TD style="WIDTH: 120px" width="120"><asp:label id="lblConsgmtNo" runat="server" CssClass="tableLabel" Width="113px" Height="22px">Consignment No</asp:label></TD>
									<td style="WIDTH: 156px" width="156"><FONT face="Tahoma"><asp:textbox id="txtConsigNo" style="TEXT-TRANSFORM: uppercase" tabIndex="4" runat="server" CssClass="textField" Width="141px" MaxLength="20"></asp:textbox></FONT></td>
									<td style="WIDTH: 102px" width="102"></td>
									<TD style="WIDTH: 191px" width="191"><asp:label id="lblRefNo" runat="server" CssClass="tableLabel" Width="147px" Height="22px">Customer Reference No</asp:label></TD>
									<td style="WIDTH: 146px" width="146"><FONT face="Tahoma"><asp:textbox id="txtCustRefNo" style="TEXT-TRANSFORM: uppercase" tabIndex="5" runat="server" CssClass="textField" Width="173px" MaxLength="20" AutoPostBack="True"></asp:textbox></FONT></td>
								</TR>
								<tr height="20">
									<td width="3%"></td>
									<td style="WIDTH: 120px" width="120"><FONT face="Tahoma"><asp:label id="lblCustID" runat="server" CssClass="tableLabel" Width="95px" Height="22px">Customer ID</asp:label></FONT></td>
									<td style="WIDTH: 249px" width="249" colSpan="2"><FONT face="Tahoma"><asp:textbox id="txtCustID" style="TEXT-TRANSFORM: uppercase" tabIndex="15" runat="server" CssClass="textField" Width="122px" MaxLength="20" AutoPostBack="True"></asp:textbox><asp:button id="btnDisplayCustDtls" tabIndex="16" runat="server" CssClass="searchButton" Width="21px" Height="19px" CausesValidation="False" Text="..."></asp:button></FONT></td>
									<TD style="WIDTH: 191px" width="191"></TD>
									<td style="WIDTH: 146px" width="146"></td>
								</tr>
								<tr height="25">
									<td width="3%" style="HEIGHT: 25px"></td>
									<td style="WIDTH: 120px; HEIGHT: 25px" width="120"><asp:label id="lblRouteCode" runat="server" CssClass="tableLabel" Width="139px" Height="22px"> Destination DC</asp:label></td>
									<TD style="WIDTH: 156px; HEIGHT: 25px" width="156"><FONT face="Tahoma"><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True" ServerMethod="DbComboDistributionCenterSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" " TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></FONT></TD>
									<td style="WIDTH: 102px; HEIGHT: 25px" width="102"></td>
									<TD style="WIDTH: 191px; HEIGHT: 25px" width="191"><asp:label id="lblDelManifest" runat="server" CssClass="tableLabel" Width="130px" Height="22px">Delivery Route Code</asp:label></TD>
									<td width="55%" colSpan="2" style="HEIGHT: 25px"><FONT face="Tahoma"><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True" ServerMethod="DbComboPathCodeSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" " TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></FONT></td>
								</tr>
								<tr height="25">
									<td width="3%"></td>
									<td style="WIDTH: 120px" width="120"><asp:label id="lblStatusCode" runat="server" CssClass="tableLabel" Width="136px" Height="15px">Actual POD Date From</asp:label></td>
									<td style="WIDTH: 156px" width="156"><FONT face="Tahoma"><cc1:mstextbox id="txtAct_pod_From" tabIndex="19" runat="server" CssClass="textField" Width="100%" MaxLength="16" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></FONT></td>
									<td style="WIDTH: 102px" width="102"><FONT face="Tahoma"><asp:label id="lblExcepCode" runat="server" CssClass="tableLabel" Width="14px" Height="14px">To</asp:label></FONT></td>
									<TD style="WIDTH: 191px" width="191"><FONT face="Tahoma"><cc1:mstextbox id="txtAct_Pod_to" tabIndex="19" runat="server" CssClass="textField" Width="100%" MaxLength="16" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></FONT></TD>
									<td width="55%" colSpan="2"><asp:textbox id="txtAct_pod_FromX" style="TEXT-TRANSFORM: uppercase" runat="server" CssClass="textField" Width="48px" Visible="False" MaxLength="10"></asp:textbox><asp:textbox id="txtAct_Pod_toX" style="TEXT-TRANSFORM: uppercase" runat="server" CssClass="textfield" Width="40px" Visible="False" MaxLength="10"></asp:textbox></td>
								</tr>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 164px" vAlign="top" width="738"><FONT face="Tahoma">
								<FIELDSET style="WIDTH: 737px; HEIGHT: 163px"><LEGEND><asp:label id="Label52" CssClass="tableHeadingFieldset" Runat="server">Shipment Identification</asp:label></LEGEND>
									<TABLE id="Table10" style="WIDTH: 727px; HEIGHT: 139px" width="727" align="left" border="0" runat="server" cellSpacing="0">
										<TR height="25">
											<TD style="WIDTH: 111px" align="right" width="111"><asp:label id="Label5" runat="server" CssClass="tableLabelRight" Width="110px" Height="22px" Font-Size="11px">Consignment No</asp:label></TD>
											<TD style="WIDTH: 174px" vAlign="top" width="174"></TD>
											<TD style="WIDTH: 174px" vAlign="top" width="174"><asp:label id="lblSI_consigNo" runat="server" CssClass="tableLabel" Width="143px"></asp:label></TD>
											<TD style="WIDTH: 117px" align="right" width="117"><asp:label id="Label6" runat="server" CssClass="tableLabelRight" Width="147px" Height="15px" Font-Size="11px">Customer Reference No</asp:label></TD>
											<TD style="WIDTH: 130px" width="130"></TD>
											<TD style="WIDTH: 130px" width="130"><asp:label id="lblSI_CusRef" runat="server" CssClass="tableLabel" Width="133px"></asp:label></TD>
											<TD vAlign="top" width="15%"></TD>
										</TR>
										<TR height="25">
											<TD style="WIDTH: 111px" align="right" width="111"><asp:label id="Label50" runat="server" CssClass="tableLabelRight" Width="107px" Height="22px" Font-Size="11px">Booking D/T</asp:label></TD>
											<TD style="WIDTH: 174px" vAlign="top" width="174"></TD>
											<TD style="WIDTH: 174px" vAlign="top" width="174"><asp:label id="lblSI_BookingDT" runat="server" CssClass="tableLabel" Width="143px"></asp:label></TD>
											<TD style="WIDTH: 117px" align="right" width="117"><asp:label id="Label49" runat="server" CssClass="tableLabelRight" Width="142px" Height="14px" Font-Size="11px">Total Package</asp:label></TD>
											<TD style="WIDTH: 130px" width="130"></TD>
											<TD style="WIDTH: 130px" width="130"><asp:label id="lblSI_TotPackage" runat="server" CssClass="tableLabel" Width="133px"></asp:label></TD>
											<TD width="15%"></TD>
										</TR>
										<TR height="25">
											<TD style="WIDTH: 111px; HEIGHT: 25px" align="right" width="111"><asp:label id="Label48" runat="server" CssClass="tableLabelRight" Width="107px" Height="15px" Font-Size="11px">Pickup D/T</asp:label></TD>
											<TD style="WIDTH: 174px; HEIGHT: 25px" width="174"></TD>
											<TD style="WIDTH: 174px; HEIGHT: 25px" width="174"><asp:label id="lblSI_PickupDT" runat="server" CssClass="tableLabel" Width="145px"></asp:label></TD>
											<TD style="WIDTH: 117px; HEIGHT: 25px" align="right" width="117"><asp:label id="Label47" tabIndex="100" runat="server" CssClass="tableLabelRight" Width="144px" Height="13px" Font-Size="11px">Delivery Route Code</asp:label></TD>
											<TD style="WIDTH: 130px; HEIGHT: 25px" width="130"></TD>
											<TD style="WIDTH: 130px; HEIGHT: 25px" width="130"><asp:label id="lblSI_DelivRoute" runat="server" CssClass="tableLabel" Width="133px"></asp:label></TD>
											<TD style="HEIGHT: 25px" width="15%"></TD>
										</TR>
										<TR height="25">
											<TD style="WIDTH: 111px" align="right" width="111"><asp:label id="Label45" runat="server" CssClass="tableLabelRight" Width="107px" Height="13px" Font-Size="11px">Actual POD D/T</asp:label></TD>
											<TD style="WIDTH: 174px" width="174"></TD>
											<TD style="WIDTH: 174px" width="174"><asp:label id="lblSI_ActualPOD" runat="server" CssClass="tableLabel" Width="145px"></asp:label></TD>
											<TD align="right" width="15%"><asp:label id="Label46" tabIndex="100" runat="server" CssClass="tableLabelRight" Width="139px" Height="16px" Font-Size="11px">Service Type</asp:label></TD>
											<TD style="WIDTH: 130px" width="130"></TD>
											<TD style="WIDTH: 130px" width="130"><asp:label id="lblSI_ServiceType" runat="server" CssClass="tableLabel" Width="133px"></asp:label></TD>
											<TD width="10%">&nbsp;</TD>
										</TR>
										<TR height="25">
											<TD style="WIDTH: 111px" width="111"></TD>
											<TD style="WIDTH: 174px" width="174"></TD>
											<TD style="WIDTH: 174px" width="174"></TD>
											<TD width="15%"></TD>
											<TD style="WIDTH: 130px" align="right" width="130"></TD>
											<TD style="WIDTH: 130px" align="right" width="130"><asp:label id="Label23" runat="server" CssClass="tableLabelRight" Width="139px" Font-Size="11px">COD Amount VAS</asp:label></TD>
											<TD align="right" width="10%"><asp:label id="lblSI_CODamVAS" runat="server" CssClass="tableLabelRight" Width="128px" Font-Size="11px"></asp:label></TD>
										</TR>
										<TR height="25">
											<TD style="WIDTH: 111px" width="111"></TD>
											<TD style="WIDTH: 174px" width="174"></TD>
											<TD style="WIDTH: 174px" width="174"></TD>
											<TD style="WIDTH: 117px" width="117"></TD>
											<TD style="WIDTH: 130px" align="right" width="130"></TD>
											<TD style="WIDTH: 130px" align="right" width="130"><asp:label id="Label24" runat="server" CssClass="tableLabelRight" Width="137px" Font-Size="11px">COD Amount</asp:label></TD>
											<TD align="right" width="10%"><asp:label id="lblSI_CODam" runat="server" CssClass="tableLabelRight" Width="127px" Font-Size="11px"></asp:label></TD>
										</TR>
									</TABLE>
								</FIELDSET>
							</FONT>
						</TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 617px" vAlign="top" width="738"><FONT face="Tahoma"><FONT face="Tahoma">
									<FIELDSET style="WIDTH: 737px; HEIGHT: 470px">
										<P><LEGEND><asp:label id="Label62" CssClass="tableHeadingFieldset" Runat="server" Font-Size="10pt">Post Delivery Data Entry</asp:label></LEGEND></P>
										<FIELDSET style="WIDTH: 737px; HEIGHT: 227px"><LEGEND><asp:label id="Label63" CssClass="tableHeadingFieldset" Runat="server">COD Amount Collected</asp:label></LEGEND>
											<TABLE id="Table7" style="WIDTH: 729px; HEIGHT: 208px" width="729" align="left" border="0" runat="server">
												<TBODY>
													<TR height="25">
														<TD style="WIDTH: 63px" align="right" width="63"><asp:label id="Label61" runat="server" CssClass="tableLabelRight" Width="161px" Height="14px" Font-Size="11px">COD Amount Collected D/T</asp:label></TD>
														<TD style="WIDTH: 177px" width="177"><cc1:mstextbox id="txtCODAmountCollectedDT" tabIndex="19" runat="server" CssClass="textField" Width="100%" MaxLength="16" TextMaskString="99/99/9999 99:99" TextMaskType="msDateTime"></cc1:mstextbox></TD>
														<TD style="WIDTH: 236px" align="right" width="236"><asp:label id="Label59" runat="server" CssClass="tableLabelRight" Width="216px" Height="15px" Font-Size="11px">NET COD Amount Collected (Cash)</asp:label></TD>
														<TD width="15%"><cc1:mstextbox id="txtNETCODAmountCollected_Cash" tabIndex="25" runat="server" CssClass="textFieldRightAlign" Width="100%" Enabled="True" MaxLength="10" TextMaskType="msNumericCOD" NumberMaxValueCOD="9999999.99" NumberScale="2" NumberPrecision="9" NumberMinValue="0" AutoPostBack="True"></cc1:mstextbox></TD>
													</TR>
													<TR height="25">
														<TD style="WIDTH: 63px" align="right" width="63"><asp:label id="Label57" runat="server" CssClass="tableLabelRight" Width="155px" Height="15px" Font-Size="11px">COD Refused</asp:label></TD>
														<TD style="WIDTH: 177px" width="177"><asp:dropdownlist id="ddlCODRefused" runat="server" AutoPostBack="True"></asp:dropdownlist></TD>
														<TD style="WIDTH: 236px" align="right" width="236"><asp:label id="Label55" runat="server" CssClass="tableLabelRight" Width="214px" Height="15px" Font-Size="11px">NET COD Amount Collected (Check)</asp:label></TD>
														<TD width="15%"><cc1:mstextbox id="txtNETCODAmountCollected_Check" tabIndex="25" runat="server" CssClass="textFieldRightAlign" Width="100%" Enabled="True" MaxLength="10" TextMaskType="msNumericCOD" NumberMaxValueCOD="9999999.99" NumberScale="2" NumberPrecision="9" NumberMinValue="0" AutoPostBack="True"></cc1:mstextbox></TD>
													</TR>
													<TR height="25">
														<TD style="WIDTH: 63px" width="63"><asp:textbox id="txtCODAmountCollectedDTX" runat="server" CssClass="textFieldRightAlign" Visible="False"></asp:textbox></TD>
														<TD style="WIDTH: 177px" width="177"><asp:textbox id="txtNETCODAmountCollected_CashX" runat="server" CssClass="textFieldRightAlign" Width="155px" Visible="False"></asp:textbox></TD>
														<TD style="WIDTH: 236px" align="right" width="236"><asp:label id="Label36" tabIndex="100" runat="server" CssClass="tableLabelRight" Width="214px" Height="15px" Font-Size="11px">NET COD Amount Collected (Other)</asp:label></TD>
														<TD width="15%"><cc1:mstextbox id="txtNETCODAmountCollected_Other" tabIndex="25" runat="server" CssClass="textFieldRightAlign" Width="100%" Enabled="True" MaxLength="10" TextMaskType="msNumericCOD" NumberMaxValueCOD="9999999.99" NumberScale="2" NumberPrecision="9" NumberMinValue="0" AutoPostBack="True"></cc1:mstextbox></TD>
													</TR>
													<TR height="25">
														<TD style="WIDTH: 63px" width="63"></TD>
														<TD style="WIDTH: 177px" width="177"><asp:textbox id="txtNETCODAmountCollected_CheckX" runat="server" CssClass="textFieldRightAlign" Width="155px" Visible="False"></asp:textbox></TD>
														<TD style="WIDTH: 236px" align="right" width="236"><asp:label id="Label32" tabIndex="100" runat="server" CssClass="tableLabelRight" Width="192px" Height="15px" Font-Size="11px">Withholding Tax Amount</asp:label></TD>
														<TD width="10%"><cc1:mstextbox id="txtWithholdingTaxAmount" tabIndex="25" runat="server" CssClass="textFieldRightAlign" Width="100%" Enabled="True" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2" AutoPostBack="True"></cc1:mstextbox></TD>
													</TR>
													<TR height="25">
														<TD style="WIDTH: 63px" width="63"></TD>
														<TD style="WIDTH: 177px" width="177"><asp:textbox id="txtNETCODAmountCollected_OtherX" runat="server" CssClass="textFieldRightAlign" Width="155px" Visible="False"></asp:textbox></TD>
														<TD style="WIDTH: 236px" align="right" width="236"><asp:label id="Label30" runat="server" CssClass="tableLabelRight" Width="143px" Font-Size="11px">Total COD Amount</asp:label></TD>
														<TD align="right" width="10%"><asp:label id="lblTotalCODAmount" runat="server" CssClass="tableLabelRight" Width="134px" Font-Size="11px"></asp:label></TD>
													</TR>
													<TR height="25">
														<TD style="WIDTH: 63px" width="63"></TD>
														<TD style="WIDTH: 177px" width="177"><asp:textbox id="txtWithholdingTaxAmountX" runat="server" CssClass="textFieldRightAlign" Width="155px" Visible="False"></asp:textbox></TD>
														<TD style="WIDTH: 236px" align="right" width="236"><asp:label id="Label28" runat="server" CssClass="tableLabelRight" Width="139px" Font-Size="11px">NET COD Amount</asp:label></TD>
														<TD align="right" width="10%"><asp:label id="txtNetCODAmount" runat="server" CssClass="tableLabelRight" Width="130px" Font-Size="11px"></asp:label></TD>
													</TR>
													<TR>
														<TD vAlign="top" align="right" width="10%" colSpan="4"><asp:label id="Label33" runat="server" CssClass="tableLabelRight" Width="52px" Height="16px" Font-Size="11px">Remarks</asp:label>&nbsp;
															<asp:textbox id="txtCODRemark" runat="server" CssClass="textField" Width="482px" Height="21px" MaxLength="80"></asp:textbox></TD>
													</TR>
													<TR>
														<TD align="right" width="10%"><asp:label id="Label34" runat="server" CssClass="tableLabelRight" Width="133px" Font-Size="11px">Last User Updated</asp:label></TD>
														<TD style="WIDTH: 177px" align="left" width="177"><asp:label id="lblCODLastUserUpdated" runat="server" CssClass="tableLabel" Width="130px" Font-Size="11px" Font-Bold="True"></asp:label></TD>
														<TD style="WIDTH: 236px" align="right" width="236"><asp:label id="Label53" runat="server" CssClass="tableLabelRight" Width="133px" Font-Size="11px">Last Updated D/T</asp:label></TD>
														<TD align="right" width="10%"><asp:label id="lblCODLastUpdateDT" runat="server" CssClass="tableLabelRight" Width="133px" Font-Size="11px" Font-Bold="True"></asp:label></TD>
													</TR>
												</TBODY>
											</TABLE>
										</FIELDSET>
										<FIELDSET style="WIDTH: 740px; HEIGHT: 153px"><LEGEND><asp:label id="Label86" CssClass="tableHeadingFieldset" Height="15px" Runat="server">COD Verification</asp:label></LEGEND>
											<TABLE id="Table8" style="WIDTH: 728px; HEIGHT: 133px" width="728" align="left" border="0" runat="server">
												<TR height="25">
													<TD style="WIDTH: 206px" align="right" width="206"></TD>
													<TD style="WIDTH: 218px" width="218"><asp:textbox id="txtNETCODAmountVerifiedDT_CashX" runat="server" CssClass="textFieldRightAlign" Width="155px" Visible="False"></asp:textbox></TD>
													<TD style="WIDTH: 255px" align="right" width="255"><asp:label id="Label83" runat="server" CssClass="tableLabelRight" Width="223px" Height="12px" Font-Size="11px"> COD Amount Verified D/T (Cash)</asp:label></TD>
													<TD align="right" width="15%"><cc1:mstextbox id="txtNETCODAmountVerifiedDT_Cash" tabIndex="19" runat="server" CssClass="textFieldRightAlign" Width="100%" MaxLength="16" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></TD>
												</TR>
												<TR height="25">
													<TD style="WIDTH: 206px" align="right" width="206"></TD>
													<TD style="WIDTH: 218px" width="218"><asp:textbox id="txtNETCODAmountVerifiedDT_CheckX" runat="server" CssClass="textFieldRightAlign" Width="155px" Visible="False"></asp:textbox></TD>
													<TD style="WIDTH: 255px" align="right" width="255"><asp:label id="Label79" runat="server" CssClass="tableLabelRight" Width="226px" Height="12px" Font-Size="11px"> COD Amount Verified D/T (Check)</asp:label></TD>
													<TD align="right" width="15%"><cc1:mstextbox id="txtNETCODAmountVerifiedDT_Check" tabIndex="19" runat="server" CssClass="textFieldRightAlign" Width="100%" MaxLength="16" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></TD>
												</TR>
												<TR height="25">
													<TD style="WIDTH: 206px" width="206"></TD>
													<TD style="WIDTH: 218px" width="218"><asp:textbox id="txtNETCODAmountVerifiedDT_OtherX" runat="server" CssClass="textFieldRightAlign" Width="155px" Visible="False"></asp:textbox></TD>
													<TD style="WIDTH: 255px" align="right" width="255"><asp:label id="Label77" tabIndex="100" runat="server" CssClass="tableLabelRight" Width="229px" Height="12px" Font-Size="11px"> COD Amount Verified D/T (Other)</asp:label></TD>
													<TD align="right" width="15%"><cc1:mstextbox id="txtNETCODAmountVerifiedDT_Other" tabIndex="19" runat="server" CssClass="textFieldRightAlign" Width="100%" MaxLength="16" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></TD>
												</TR>
												<TR>
													<TD style="HEIGHT: 29px" align="right" width="10%" colSpan="4"><asp:label id="Label69" runat="server" CssClass="tableLabelRight" Width="52px" Height="16px" Font-Size="11px">Remarks</asp:label>&nbsp;
														<asp:textbox id="txtCODVerificationRemark" runat="server" CssClass="textField" Width="482px" MaxLength="80"></asp:textbox></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 206px" align="right" width="206"><asp:label id="Label68" runat="server" CssClass="tableLabelRight" Width="133px" Font-Size="11px">Last User Updated</asp:label></TD>
													<TD style="WIDTH: 218px" align="left" width="218"><asp:label id="lblCODVLUser" runat="server" CssClass="tableLabel" Width="190px" Font-Size="11px" Font-Bold="True"></asp:label></TD>
													<TD style="WIDTH: 255px" align="right" width="255"><asp:label id="Label66" runat="server" CssClass="tableLabelRight" Width="133px" Font-Size="11px">Last Updated D/T</asp:label></TD>
													<TD align="right" width="10%"><asp:label id="lblCODVLUpdateDT" runat="server" CssClass="tableLabelRight" Width="151px" Font-Size="11px" Font-Bold="True"></asp:label></TD>
												</TR>
											</TABLE>
										</FIELDSET>
										<FIELDSET style="WIDTH: 739px; HEIGHT: 185px"><LEGEND><asp:label id="Label108" CssClass="tableHeadingFieldset" Height="15px" Runat="server">COD Remittance to Customer</asp:label></LEGEND>
											<TABLE id="Table11" style="WIDTH: 733px; HEIGHT: 168px" width="733" align="left" border="0" runat="server">
												<TR height="25">
													<TD style="WIDTH: 63px" align="right" width="63"></TD>
													<TD style="WIDTH: 188px" width="188" colSpan="1"><asp:textbox id="txtCODRemittanceToCustomerDT_CashX" runat="server" CssClass="textFieldRightAlign" Width="155px" Visible="False"></asp:textbox></TD>
													<TD style="WIDTH: 176px" align="right" width="176" colSpan="1"><asp:label id="Label105" runat="server" CssClass="tableLabelRight" Width="247px" Height="16px" Font-Size="11px">COD Remittance to Customer D/T (Cash)</asp:label></TD>
													<TD align="right" width="15%"><cc1:mstextbox id="txtCODRemittanceToCustomerDT_Cash" tabIndex="19" runat="server" CssClass="textFieldRightAlign" Width="100%" MaxLength="16" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></TD>
												</TR>
												<TR height="25">
													<TD style="WIDTH: 63px" align="right" width="63"></TD>
													<TD style="WIDTH: 188px" width="188"><asp:textbox id="txtCODRemittanceToCustomerDT_CheckX" runat="server" CssClass="textFieldRightAlign" Width="155px" Visible="False"></asp:textbox></TD>
													<TD style="WIDTH: 176px" align="right" width="176"><asp:label id="Label101" runat="server" CssClass="tableLabelRight" Width="248px" Height="16px" Font-Size="11px">COD Remittance to Customer D/T (Check)</asp:label></TD>
													<TD align="right" width="15%"><cc1:mstextbox id="txtCODRemittanceToCustomerDT_Check" tabIndex="19" runat="server" CssClass="textFieldRightAlign" Width="100%" MaxLength="16" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></TD>
												</TR>
												<TR height="25">
													<TD style="WIDTH: 63px" width="63"></TD>
													<TD style="WIDTH: 188px" width="188"><asp:textbox id="txtCODRemittanceToCustomerDT_OtherX" runat="server" CssClass="textFieldRightAlign" Width="155px" Visible="False"></asp:textbox></TD>
													<TD style="WIDTH: 176px" align="right" width="176"><asp:label id="Label99" tabIndex="100" runat="server" CssClass="tableLabelRight" Width="246px" Height="16px" Font-Size="11px">COD Remittance to Customer D/T (Other)</asp:label></TD>
													<TD align="right" width="15%"><cc1:mstextbox id="txtCODRemittanceToCustomerDT_Other" tabIndex="19" runat="server" CssClass="textFieldRightAlign" Width="100%" MaxLength="16" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></TD>
												</TR>
												<TR height="25">
													<TD style="WIDTH: 63px" width="63"></TD>
													<TD style="WIDTH: 188px" width="188"></TD>
													<TD style="WIDTH: 176px" align="right" width="176"><asp:label id="Label97" tabIndex="100" runat="server" CssClass="tableLabelRight" Width="244px" Height="16px" Font-Size="11px">COD Remittance Advance to customer</asp:label></TD>
													<TD align="right" width="10%"><cc1:mstextbox id="txtCODRemittanceAdvanceToCustomer" tabIndex="20" runat="server" CssClass="textFieldRightAlign" Width="100%" Enabled="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2" AutoPostBack="false"></cc1:mstextbox></TD>
												</TR>
												<TR>
													<TD align="right" width="10%" colSpan="4"><asp:label id="Label91" runat="server" CssClass="tableLabelRight" Width="52px" Height="14px" Font-Size="11px">Remarks</asp:label>&nbsp;
														<asp:textbox id="txtCODRemittanceRemark" runat="server" CssClass="textField" Width="482px" MaxLength="80"></asp:textbox></TD>
												</TR>
												<TR>
													<TD align="right" width="10%"><asp:label id="Label90" runat="server" CssClass="tableLabelRight" Width="133px" Font-Size="11px">Last User Updated</asp:label></TD>
													<TD style="WIDTH: 188px" align="left" width="188"><asp:label id="lblCODRLUpdateUser" runat="server" CssClass="tableLabel" Width="130px" Font-Size="11px" Font-Bold="True"></asp:label></TD>
													<TD style="WIDTH: 176px" align="right" width="176" colSpan="1"><asp:label id="Label88" runat="server" CssClass="tableLabelRight" Width="245px" Font-Size="11px">Last Updated D/T</asp:label></TD>
													<TD align="right" width="10%"><asp:label id="lblCODRLUpdateDT" runat="server" CssClass="tableLabelRight" Width="122px" Font-Size="11px" Font-Bold="True"></asp:label></TD>
												</TR>
											</TABLE>
										</FIELDSET>
									</FIELDSET>
								</FONT></FONT>
						</TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 617px" vAlign="top" width="738"><FONT face="Tahoma">
								<FIELDSET style="WIDTH: 749px; HEIGHT: 618px">
									<P><LEGEND><asp:label id="Label39" CssClass="tableHeadingFieldset" Height="15px" Runat="server" Font-Size="10pt">Shipment Details</asp:label></LEGEND></P>
									<FIELDSET style="WIDTH: 744px; HEIGHT: 200px"><LEGEND><asp:label id="lblCustInfo" CssClass="tableHeadingFieldset" Runat="server">Customer Information</asp:label></LEGEND>
										<TABLE id="Table5" style="WIDTH: 732px; HEIGHT: 185px" width="732" align="left" border="0" runat="server">
											<TR height="25">
												<TD style="WIDTH: 3px" width="3"></TD>
												<TD style="WIDTH: 73px" width="73" vAlign="top"><asp:label id="lblCustType" runat="server" CssClass="tableLabel" Width="108px" Height="22px">Customer Type</asp:label></TD>
												<TD style="WIDTH: 201px" width="201"><FONT face="Tahoma"><asp:label id="lblCI_CustType" runat="server" CssClass="tableLabel" Width="108px" Height="22px"></asp:label></FONT></TD>
												<TD style="WIDTH: 60px" width="60" colSpan="2"></TD>
												<TD width="105" style="WIDTH: 105px"></TD>
												<TD width="10%"></TD>
											</TR>
											<TR height="25">
												<TD style="WIDTH: 3px" width="3"></TD>
												<TD style="WIDTH: 73px" width="73"><FONT face="Tahoma"><asp:label id="Label9" runat="server" CssClass="tableLabel" Width="108px" Height="22px">Customer ID</asp:label></FONT></TD>
												<TD style="WIDTH: 302px" width="302" colSpan="2"><asp:label id="lblCI_CustID" runat="server" CssClass="tableLabel" Width="108px" Height="22px"></asp:label></TD>
												<TD width="51" style="WIDTH: 51px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
												<TD width="105" style="WIDTH: 105px"></TD>
												<TD width="10%"></TD>
											</TR>
											<TR height="25">
												<TD style="WIDTH: 3px" width="3"></TD>
												<TD style="WIDTH: 73px" width="73"><asp:label id="lblName" runat="server" CssClass="tableLabel" Width="108px" Height="22px">Name</asp:label></TD>
												<TD style="WIDTH: 302px" width="302" colSpan="3"><asp:label id="lblCI_Name" runat="server" CssClass="tableLabel" Width="292px" Height="22px"></asp:label></TD>
												<TD width="105" vAlign="top" style="WIDTH: 105px"><asp:label id="lblPaymode" runat="server" CssClass="tableLabel" Width="98px" Height="22px">Payment Mode</asp:label></TD>
												<TD width="15%" vAlign="top"><asp:label id="lblCI_PaymentMode" runat="server" CssClass="tableLabel" Width="178px" Height="22px"></asp:label></TD>
											</TR>
											<TR height="25">
												<TD style="WIDTH: 3px; HEIGHT: 18px" width="3"></TD>
												<TD style="WIDTH: 73px; HEIGHT: 18px" width="73" vAlign="top"><asp:label id="lblAddress" runat="server" CssClass="tableLabel" Width="108px" Height="22px">Address</asp:label></TD>
												<TD style="WIDTH: 302px; HEIGHT: 18px" width="302" colSpan="3"><asp:label id="lblCI_Addr1" runat="server" CssClass="tableLabel" Width="307px" Height="22px"></asp:label></TD>
												<TD width="105" vAlign="top" style="WIDTH: 105px; HEIGHT: 18px"><asp:label id="lblTelphone" tabIndex="100" runat="server" CssClass="tableLabel" Width="56px" Height="22px">Telephone</asp:label></TD>
												<TD width="10%" vAlign="top" style="HEIGHT: 18px"><asp:label id="lblCI_Tel" runat="server" CssClass="tableLabel" Width="108px" Height="22px"></asp:label></TD>
											</TR>
											<TR height="25">
												<TD style="WIDTH: 3px" width="3"></TD>
												<TD style="WIDTH: 73px" width="73"></TD>
												<TD style="WIDTH: 302px" width="302" colSpan="3" vAlign="top"><asp:label id="lblCI_Addr2" runat="server" CssClass="tableLabel" Width="307px" Height="22px"></asp:label></TD>
												<TD width="105" vAlign="top" style="WIDTH: 105px"><asp:label id="lblFax" tabIndex="100" runat="server" CssClass="tableLabel" Width="94px" Height="22px">Fax</asp:label></TD>
												<TD width="10%" vAlign="top"><asp:label id="lblCI_Fax" runat="server" CssClass="tableLabel" Width="108px" Height="22px"></asp:label></TD>
											</TR>
											<TR height="25">
												<TD style="WIDTH: 3px" width="3"></TD>
												<TD style="WIDTH: 73px" width="73" vAlign="top"><asp:label id="lblZip" runat="server" CssClass="tableLabel" Width="108px" Height="14px">Postal Code</asp:label></TD>
												<TD style="WIDTH: 319px" width="319" colSpan="3"><asp:label id="lblCI_PostalCode" runat="server" CssClass="tableLabel" Width="108px" Height="22px"></asp:label><BR>
													<asp:label id="lblCI_Province" runat="server" CssClass="tableLabel" Width="132px" Height="22px"></asp:label><asp:label id="lblCI_Country" runat="server" CssClass="tableLabel" Width="108px" Height="22px"></asp:label></TD>
												<TD style="WIDTH: 105px" width="105"></TD>
												<TD width="15%"></TD>
											</TR>
										</TABLE>
									</FIELDSET>
									<BR>
									<FIELDSET style="WIDTH: 744px; HEIGHT: 152px"><LEGEND><asp:label id="lblSendInfo" CssClass="tableHeadingFieldset" Runat="server">Sender Information</asp:label></LEGEND>
										<TABLE id="tblSenderInfo" style="WIDTH: 733px" width="733" border="0" runat="server">
											<TR height="25">
												<TD style="WIDTH: 6px" width="6"></TD>
												<TD style="WIDTH: 88px" width="88" vAlign="top"><asp:label id="lblSendNm" runat="server" CssClass="tableLabel" Width="103px" Height="20px">Name</asp:label></TD>
												<TD style="WIDTH: 241px" width="241" colSpan="2"><asp:label id="lblSDI_Name" runat="server" CssClass="tableLabel" Width="287px" Height="22px"></asp:label></TD>
												<TD style="WIDTH: 50px" width="50" colSpan="1"></TD>
												<TD style="WIDTH: 112px" width="112" vAlign="top"><asp:label id="lblSendConPer" runat="server" CssClass="tableLabel" Width="95px">Contact Person</asp:label></TD>
												<TD width="25%" vAlign="top"><asp:label id="lblSDI_ContactPerson" runat="server" CssClass="tableLabel" Width="179px" Height="22px"></asp:label></TD>
											</TR>
											<TR height="25">
												<TD style="WIDTH: 6px; HEIGHT: 18px" width="6"></TD>
												<TD style="WIDTH: 88px; HEIGHT: 18px" width="88" vAlign="top"><asp:label id="lblSendAddr1" runat="server" CssClass="tableLabel" Width="103px" Height="21px">Address</asp:label></TD>
												<TD style="WIDTH: 344px; HEIGHT: 18px" width="344" colSpan="3"><asp:label id="lblSDI_Addr1" runat="server" CssClass="tableLabel" Width="313px" Height="23px"></asp:label></TD>
												<TD style="WIDTH: 112px; HEIGHT: 18px" width="112" vAlign="top"><asp:label id="lblSendTel" tabIndex="100" runat="server" CssClass="tableLabel" Width="87px">Telephone</asp:label></TD>
												<TD width="25%" vAlign="top" style="HEIGHT: 18px"><asp:label id="lblSDI_Tel" runat="server" CssClass="tableLabel" Width="180px" Height="22px"></asp:label></TD>
											</TR>
											<TR height="25">
												<TD style="WIDTH: 6px" width="6"></TD>
												<TD style="WIDTH: 88px" width="88"></TD>
												<TD style="WIDTH: 344px" width="344" colSpan="3" vAlign="top"><asp:label id="lblSDI_Addr2" runat="server" CssClass="tableLabel" Width="311px" Height="22px"></asp:label></TD>
												<TD style="WIDTH: 112px" width="112" vAlign="top"><asp:label id="lblSendFax" tabIndex="100" runat="server" CssClass="tableLabel" Width="88px">Fax</asp:label></TD>
												<TD width="25%" vAlign="top"><asp:label id="lblSDI_Fax" runat="server" CssClass="tableLabel" Width="108px" Height="22px"></asp:label></TD>
											</TR>
											<TR height="25">
												<TD style="WIDTH: 6px" width="6"></TD>
												<TD style="WIDTH: 88px" width="88" vAlign="top"><asp:label id="lblSendZip" runat="server" CssClass="tableLabel" Width="103px" Height="21px">Postal Code</asp:label></TD>
												<TD style="WIDTH: 431px" width="431" colSpan="4"><asp:label id="lblSDI_PostalCode" runat="server" CssClass="tableLabel" Width="98px" Height="22px"></asp:label><BR>
													<asp:label id="lblSDI_Province" runat="server" CssClass="tableLabel" Width="134px" Height="22px"></asp:label><asp:label id="lblSDI_Country" runat="server" CssClass="tableLabel" Width="108px" Height="22px"></asp:label></TD>
												<TD style="WIDTH: 27px" width="27">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
											</TR>
											<TR height="25">
												<TD style="WIDTH: 6px" width="6"></TD>
												<TD style="WIDTH: 88px" width="88" vAlign="top"><asp:label id="lblSendCuttOffTime" runat="server" CssClass="tableLabel" Width="103px" Height="21px">Cut-off Time</asp:label></TD>
												<TD style="WIDTH: 241px" width="241" colSpan="2"><FONT face="Tahoma"><asp:label id="lblSDI_CutOffTime" runat="server" CssClass="tableLabel" Width="108px" Height="22px"></asp:label></FONT></TD>
												<TD style="WIDTH: 50px" width="50"></TD>
												<TD style="WIDTH: 112px" width="112"></TD>
												<TD width="25%"></TD>
											</TR>
										</TABLE>
									</FIELDSET>
									<BR>
									<FIELDSET style="WIDTH: 743px; HEIGHT: 151px"><LEGEND><asp:label id="lblRecInfo" CssClass="tableHeadingFieldset" Runat="server">Recipient Information</asp:label></LEGEND>
										<TABLE id="tblRecepInfo" style="WIDTH: 733px" width="733" border="0" runat="server">
											<TR>
												<TD style="WIDTH: 2px" width="2">&nbsp;</TD>
												<TD style="WIDTH: 102px" width="102" vAlign="top"><asp:label id="lblRecipTelephone" tabIndex="83" runat="server" CssClass="tableLabel" Width="88px">Telephone</asp:label></TD>
												<TD style="WIDTH: 290px" width="290">
													<%-- //Phase2 - J03 --%>
													<%-- //Phase2 - J03 --%>
													<asp:label id="RI_Tel" runat="server" CssClass="tableLabel" Width="150px" Height="22px"></asp:label></TD>
												<TD style="WIDTH: 31px" width="31">&nbsp;
												</TD>
												<TD style="WIDTH: 104px" width="104"></TD>
												<TD width="25%"></TD>
											</TR>
											<TR height="25">
												<TD style="WIDTH: 2px" width="2"></TD>
												<TD style="WIDTH: 102px" width="102"><asp:label id="lblRecipNm" tabIndex="100" runat="server" CssClass="tableLabel" Width="85px" Height="20px">Name</asp:label></TD>
												<TD style="WIDTH: 290px" width="290"><asp:label id="RI_Name" runat="server" CssClass="tableLabel" Width="269px" Height="22px"></asp:label></TD>
												<TD style="WIDTH: 31px" width="31"></TD>
												<TD style="WIDTH: 104px" width="104" vAlign="top"><asp:label id="lblRecpContPer" tabIndex="100" runat="server" CssClass="tableLabel" Width="105px">Contact Person</asp:label></TD>
												<TD width="25%" vAlign="top"><asp:label id="RI_ContactPerson" runat="server" CssClass="tableLabel" Width="179px" Height="22px"></asp:label></TD>
											</TR>
											<TR height="25">
												<TD style="WIDTH: 2px; HEIGHT: 19px" width="2"></TD>
												<TD style="WIDTH: 102px; HEIGHT: 19px" width="102" vAlign="top"><asp:label id="lblRecipAddr1" tabIndex="100" runat="server" CssClass="tableLabel" Width="81px" Height="21px">Address</asp:label></TD>
												<TD style="WIDTH: 290px; HEIGHT: 19px" width="290" colSpan="2"><asp:label id="RI_Addr1" runat="server" CssClass="tableLabel" Width="318px" Height="22px"></asp:label></TD>
												<TD style="WIDTH: 104px; HEIGHT: 19px" width="104"></TD>
												<TD width="25%" style="HEIGHT: 19px"></TD>
											</TR>
											<TR height="25">
												<TD style="WIDTH: 2px" width="2"></TD>
												<TD style="WIDTH: 102px" width="102"></TD>
												<TD style="WIDTH: 290px" width="290" vAlign="top" colSpan="2"><asp:label id="RI_Addr2" runat="server" CssClass="tableLabel" Width="311px" Height="22px"></asp:label></TD>
												<TD style="WIDTH: 104px" width="104" vAlign="top"><asp:label id="lblRecipFax" tabIndex="85" runat="server" CssClass="tableLabel" Width="105px">Fax</asp:label></TD>
												<TD width="25%"><asp:label id="RI_Fax" runat="server" CssClass="tableLabel" Width="141px" Height="22px"></asp:label></TD>
											</TR>
											<TR height="25">
												<TD style="WIDTH: 2px" width="2"></TD>
												<TD style="WIDTH: 102px" width="102" vAlign="top"><asp:label id="lblRecipZip" tabIndex="100" runat="server" CssClass="tableLabel" Width="84px" Height="21px">Postal Code</asp:label></TD>
												<TD style="WIDTH: 429px" width="429" colSpan="3"><asp:label id="RI_PostalCode" runat="server" CssClass="tableLabel" Width="85px" Height="22px"></asp:label><BR>
													<asp:label id="RI_Province" runat="server" CssClass="tableLabel" Width="135px" Height="22px"></asp:label><asp:label id="RI_Country" runat="server" CssClass="tableLabel" Width="137px" Height="22px"></asp:label></TD>
												<TD style="WIDTH: 74px" width="74"></TD>
											</TR>
										</TABLE>
									</FIELDSET>
								</FIELDSET>
							</FONT>
						</TD>
					</TR>
					<tr>
						<td style="WIDTH: 738px" align="left" width="738">
						</td>
					</tr>
					<%--  HC Return Task --%>
					<%--  HC Return Task --%>
					<%--  HC Return Task --%>
					<%--  HC Return Task --%>
					<TR vAlign="top">
						<TD style="WIDTH: 738px" align="left" width="738"></TD>
					</TR>
				</TABLE>
			</DIV>
			<DIV id="divTotalCODAmountCollected" style="Z-INDEX: 80; LEFT: 70px; WIDTH: 711px; POSITION: relative; TOP: 46px; HEIGHT: 310px; relative: absolute" MS_POSITIONING="GridLayout" runat="server">&nbsp;
				<TABLE id="Table1" style="Z-INDEX: 106; LEFT: 24px; WIDTH: 659px; POSITION: absolute; TOP: 20px; HEIGHT: 231px" runat="server">
					<TR>
						<TD>
							<P align="center"><asp:label id="lblConfirmMsg" runat="server" Width="440px" Font-Bold="True">Total COD Amount Collected (including W/H) does not equal the COD Amount continue?</asp:label></P>
							<P>
							<P align="center"><asp:label id="Label3" runat="server" Height="12px">Yes : Save the record and ignore the difference</asp:label><FONT face="Tahoma"><BR>
									<asp:label id="Label4" runat="server">No : Return to screen and retain the keyed amounts</asp:label><BR>
								</FONT><FONT face="Tahoma">
									<asp:label id="Label7" runat="server">Cancel : 0 the keyed amounts</asp:label><BR>
									<BR>
								</FONT>
								<asp:button id="btnDivTotalYes" runat="server" CssClass="queryButton" Text="Yes"></asp:button><asp:button id="btnDivTotalNo" runat="server" CssClass="queryButton" CausesValidation="False" Text="No"></asp:button><asp:button id="btnDivTotalCancel" runat="server" CssClass="queryButton" CausesValidation="False" Text="Cancel"></asp:button></P>
						</TD>
					</TR>
				</TABLE>
			</DIV>
			<DIV id="divCODREMITTANCE_DT" style="Z-INDEX: 103; LEFT: 7px; WIDTH: 658px; POSITION: relative; TOP: 46px; HEIGHT: 187px" MS_POSITIONING="GridLayout" runat="server">
				<TABLE id="Table2" style="Z-INDEX: 105; LEFT: 43px; WIDTH: 558px; POSITION: absolute; TOP: 16px; HEIGHT: 171px" runat="server">
					<TR>
						<TD>
							<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:label id="lblCustIdCngMsg" runat="server" Width="343px" Height="14px" Font-Bold="True">COD Remittance D/T is more than 10 days in the future from the actual POD date, continue?</asp:label></P>
							<P>
							<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:label id="Label1" runat="server" Width="440px" Height="21px">Yes : Save the record and ignore the keyed COD Remittance D/T</asp:label><BR>
								<FONT face="Tahoma">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</FONT>
								<asp:label id="Label2" runat="server" Width="487px" Height="25px">No : Return to the screen and retain the keyed COD Remittance D/T</asp:label></P>
							<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:button id="btnDivCODRemitYes" runat="server" CssClass="queryButton" CausesValidation="False" Text="Yes"></asp:button>&nbsp;<asp:button id="btnDivCODRemitNo" runat="server" CssClass="queryButton" CausesValidation="False" Text=" No "></asp:button></P>
						</TD>
					</TR>
				</TABLE>
			</DIV>
			<DIV id="divCODRefused" style="Z-INDEX: 103; LEFT: 6px; WIDTH: 661px; POSITION: relative; TOP: 46px; HEIGHT: 200px" MS_POSITIONING="GridLayout" runat="server">
				<TABLE id="Table3" style="Z-INDEX: 105; LEFT: 43px; WIDTH: 576px; POSITION: absolute; TOP: 10px; HEIGHT: 171px" runat="server">
					<TR>
						<TD>
							<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:label id="Label8" runat="server" Width="278px" Height="14px" Font-Bold="True"> Remove COD Surcharge? (Yes / No)</asp:label></P>
							<P>
							<P>&nbsp;&nbsp;<FONT face="Tahoma">&nbsp; </FONT>&nbsp;
								<asp:label id="Label10" runat="server" Width="517px" Height="21px"> Yes : save and remove the original COD Surcharge on the domestic shipment</asp:label><BR>
								<FONT face="Tahoma">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</FONT>
								<asp:label id="Label11" runat="server" Width="500px" Height="25px">No : save and retain the original COD Surcharge on the domestic shipment</asp:label></P>
							<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:button id="btnDivCODRefusedYes" runat="server" CssClass="queryButton" Text="Yes" CausesValidation="False"></asp:button>&nbsp;<asp:button id="btnDivCODRfusedNo" runat="server" CssClass="queryButton" Text=" No " CausesValidation="False"></asp:button></P>
						</TD>
					</TR>
				</TABLE>
			</DIV>
			<DIV id="divInvoiced" style="Z-INDEX: 106; LEFT: 5px; WIDTH: 661px; POSITION: relative; TOP: 46px; HEIGHT: 187px" MS_POSITIONING="GridLayout" runat="server">
				<TABLE id="Table4" style="Z-INDEX: 105; LEFT: 43px; WIDTH: 582px; POSITION: absolute; TOP: 16px; HEIGHT: 171px" runat="server">
					<TR>
						<TD>
							<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<BR>
								<FONT face="Tahoma">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</FONT>
								<asp:label id="Label12" runat="server" Height="25px" Width="451px" Font-Bold="True">System cannot remove COD Surcharge from Consignment because it has already been invoiced.</asp:label></P>
							<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<asp:button id="btnInvoicedOK" runat="server" CssClass="queryButton" Text="OK" CausesValidation="False" Width="47px"></asp:button></P>
						</TD>
					</TR>
				</TABLE>
			</DIV>
		</form>
	</body>
</HTML>
