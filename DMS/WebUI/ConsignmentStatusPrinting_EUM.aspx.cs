using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TIESDAL;
using TIESClasses;

using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;

using com.common.applicationpages;
using com.ties;
using System.Text;
using System.Globalization;
using System.IO;


namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for ConsignmentStatusPrinting_EUM.
	/// </summary>
	public class ConsignmentStatusPrinting_EUM : BasePage
	{
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.DataGrid dgConsignmentStatus;
		protected System.Web.UI.WebControls.ValidationSummary Validationsummary1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnPrintConsNote;
		protected System.Web.UI.WebControls.Button btnPrintShipList;
		protected System.Web.UI.WebControls.Button btnReprintConsNote;
		protected System.Web.UI.WebControls.Label lblSearch;
		protected System.Web.UI.WebControls.Label lblSenderName;
		protected System.Web.UI.WebControls.DropDownList ddlSenderName;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
	
			
		private string appID = null;
		private string enterpriseID = null;
		protected System.Web.UI.WebControls.Label lblShipList;
		protected System.Web.UI.WebControls.TextBox txtShipList;
		protected System.Web.UI.WebControls.Label lblDateFrom;
		protected com.common.util.msTextBox txtDateTo;
		protected System.Web.UI.WebControls.Label lblTo;
		protected com.common.util.msTextBox txtDateFrom;
		protected System.Web.UI.WebControls.Label lblStatus;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.DropDownList ddlTransporter;
		protected com.common.util.msTextBox Mstextbox1;
		private string userID = null;

		public string ExcSenderName
		{
			get
			{
				if(ViewState["ExcSenderName"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["ExcSenderName"];
				}
			}
			set
			{
				ViewState["ExcSenderName"]=value;
			}
		}

		public string ExcConsignment_no
		{
			get
			{
				if(ViewState["ExcConsignment_no"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["ExcConsignment_no"];
				}
			}
			set
			{
				ViewState["ExcConsignment_no"]=value;
			}
		}

		public int ExcStatus_id
		{
			get
			{
				if(ViewState["ExcStatus_id"]==null)
				{
					return -1;
				}
				else
				{
					return (int)ViewState["ExcStatus_id"];
				}
			}
			set
			{
				ViewState["ExcStatus_id"]=value;
			}
		}

		public string ExcShippingList_No
		{
			get
			{
				if(ViewState["ExcShippingList_No"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["ExcShippingList_No"];
				}
			}
			set
			{
				ViewState["ExcShippingList_No"]=value;
			}
		}

		public string ExcRecipient_telephone
		{
			get
			{
				if(ViewState["ExcRecipient_telephone"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["ExcRecipient_telephone"];
				}
			}
			set
			{
				ViewState["ExcRecipient_telephone"]=value;
			}
		}

		public string ExcRef_no
		{
			get
			{
				if(ViewState["ExcRef_no"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["ExcRef_no"];
				}
			}
			set
			{
				ViewState["ExcRef_no"]=value;
			}
		}

		public string ExcRecipient_zipcode
		{
			get
			{
				if(ViewState["ExcRecipient_zipcode"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["ExcRecipient_zipcode"];
				}
			}
			set
			{
				ViewState["ExcRecipient_zipcode"]=value;
			}
		}

		public string ExcService_code
		{
			get
			{
				if(ViewState["ExcService_code"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["ExcService_code"];
				}
			}
			set
			{
				ViewState["ExcService_code"]=value;
			}
		}

		public DateTime ExcDatefrom 
		{
			get
			{
				if(ViewState["ExcDatefrom"]==null)
				{
					return DateTime.MinValue;
				}
				else
				{
					return (DateTime)ViewState["ExcDatefrom"];
				}
			}
			set
			{
				ViewState["ExcDatefrom"]=value;
			}
		}

		public DateTime ExcDateto
		{
			get
			{
				if(ViewState["ExcDateto"]==null)
				{
					return DateTime.MinValue;
				}
				else
				{
					return (DateTime)ViewState["ExcDateto"];
				}
			}
			set
			{
				ViewState["ExcDateto"]=value;
			}
		}


		public string ConsList
		{
			get
			{
				if(ViewState["ConsList"]==null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["ConsList"];
				}
			}
			set
			{
				ViewState["ConsList"]=value;
			}
		}

		public CustomerAccount UserCustomerAccount
		{
			get
			{
				if(ViewState["UserCustomerAccount"]==null)
				{
					return null;
				}
				else
				{
					return (CustomerAccount)ViewState["UserCustomerAccount"];
				}
			}
			set
			{
				ViewState["UserCustomerAccount"]=value;
			}
		}

		public com.ties.classes.ConsignmentStatusPrintingConfigurations EnterpriseConfig
		{
			get
			{
				if(ViewState["EnterpriseConfigurations"]==null)
				{
					ViewState["EnterpriseConfigurations"]=new com.ties.classes.ConsignmentStatusPrintingConfigurations();
					return null;
				}
				return (com.ties.classes.ConsignmentStatusPrintingConfigurations)ViewState["EnterpriseConfigurations"];
			}
			set
			{
				ViewState["EnterpriseConfigurations"]=value;
			}
		}


		private void Page_Load(object sender, System.EventArgs e)
		{
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();

			// Put user code to initialize the page here	
			if(Page.IsPostBack ==false)
			{				
				
				//txtRecipPhoneNo.Attributes.Add("onkeypress","if (window.event.keyCode >= 48 && window.event.keyCode <= 57) return ture; else return false;");
				//txtShipList.Attributes.Add("onkeypress","if (window.event.keyCode >= 48 && window.event.keyCode <= 57) return ture; else return false;");
				clearscreen();
				BindControl();
			}
		}

		private void BindGrid()
		{				
			string strAppID = this.appID;
			string strEnterpriseID = this.enterpriseID;
			string userloggedin =this.userID;

			string SenderName =ExcSenderName;			
			string consignment_no = ExcConsignment_no;
			int status_id =ExcStatus_id;
			string ShippingList_No =ExcShippingList_No;
			string recipient_telephone=ExcRecipient_telephone;
			string ref_no =ExcRef_no;
			string recipient_zipcode =ExcRecipient_zipcode; 
			string service_code =ExcService_code;
			DateTime datefrom =ExcDatefrom; 
			DateTime dateto = ExcDateto;

			DataSet dsResult = CustomerConsignmentDAL.CSS_ConsignmentStatus(strAppID,strEnterpriseID,userloggedin,SenderName,
				consignment_no,status_id,
				ShippingList_No,recipient_telephone,ref_no,
				recipient_zipcode,service_code,
				datefrom,dateto);
			System.Data.DataTable dtStatus = dsResult.Tables[0];
			System.Data.DataTable dtList = dsResult.Tables[1];
			dtList.Columns.Add("Check",typeof(bool));

			this.dgConsignmentStatus.PageSize=this.EnterpriseConfig.ConsignmentStatusGrid;
			this.dgConsignmentStatus.DataSource = dtList;
			this.dgConsignmentStatus.DataBind();

			lblErrorMsg.Text="";
			if(dtStatus.Rows.Count>0 && dtStatus.Rows[0]["ErrorCode"].ToString() != "0")
			{
				lblErrorMsg.Text=dtStatus.Rows[0]["ErrorMessage"].ToString();
			}
		}

		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnPrintConsNote.Click += new System.EventHandler(this.btnPrintConsNote_Click);
			this.btnPrintShipList.Click += new System.EventHandler(this.btnPrintShipList_Click);
			this.btnReprintConsNote.Click += new System.EventHandler(this.btnReprintConsNote_Click);
			this.dgConsignmentStatus.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgConsignmentStatus_ItemCommand);
			this.dgConsignmentStatus.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgConsignmentStatus_PageIndexChanged);
			this.dgConsignmentStatus.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgConsignmentStatus_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			//change enabled status of buttons
			btnQry.Enabled = true;
			btnExecQry.Enabled = false;
			btnPrintConsNote.Enabled = true;
			btnPrintShipList.Enabled = true;
			btnReprintConsNote.Enabled = true;
			
			//Binding Data
			this.ExcSenderName ="";
			if(this.ddlSenderName.Items.Count>0 && this.ddlSenderName.SelectedValue != "")
			{
				this.ExcSenderName = ddlSenderName.SelectedValue;
			}

			this.ExcConsignment_no = "";
//			if(txtConsNo.Text.Trim() != "")
//			{
//				this.ExcConsignment_no=txtConsNo.Text.Trim();
//			}
//			this.ExcStatus_id =-1;
//			if(ddlStatus.Items.Count>0 && ddlStatus.SelectedValue != "")
//			{
//				try
//				{
//					this.ExcStatus_id= int.Parse(ddlStatus.SelectedValue);
//				}
//				catch
//				{
//					this.ExcStatus_id=-1;
//				}
//			}
			this.ExcShippingList_No ="";
			if(txtShipList.Text.Trim() !="")
			{
				this.ExcShippingList_No =txtShipList.Text.Trim();
			}
			this.ExcRecipient_telephone="";
//			if(txtRecipPhoneNo.Text.Trim() != "")
//			{
//				this.ExcRecipient_telephone=txtRecipPhoneNo.Text;
//			}
//			this.ExcRef_no ="";
//			if(txtRef.Text.Trim() != "")
//			{
//				this.ExcRef_no=txtRef.Text.Trim();
//			}
//
//			this.ExcRecipient_zipcode =""; 
//			if(txtRecipPostCode.Text.Trim() != "")
//			{
//				this.ExcRecipient_zipcode=txtRecipPostCode.Text.Trim();
//			}
//			this.ExcService_code ="";
//			if(ddlServiceType.Items.Count>0 && ddlServiceType.SelectedValue != "")
//			{
//				this.ExcService_code =ddlServiceType.SelectedValue;
//			}
			this.ExcDatefrom =DateTime.MinValue; 
			if(txtDateFrom.Text.Trim() != "")
			{
				try
				{
					this.ExcDatefrom  = DateTime.ParseExact(txtDateFrom.Text.Trim(),"dd/MM/yyyy",null);
				}
				catch
				{
					this.ExcDatefrom =DateTime.MinValue;
				}
			}
			this.ExcDateto  = DateTime.MinValue;
			if(txtDateTo.Text.Trim() != "")
			{
				try
				{
					this.ExcDateto  = DateTime.ParseExact(txtDateTo.Text.Trim(),"dd/MM/yyyy",null);
				}
				catch
				{
					this.ExcDateto =DateTime.MinValue;
				}
			}

			if(this.ExcDateto != DateTime.MinValue && this.ExcDatefrom ==DateTime.MinValue)
			{
				lblErrorMsg.Text="Please enter From Date if To Date entered.";
				return;
			}

			if(this.ExcDateto != DateTime.MinValue && this.ExcDatefrom != DateTime.MinValue)
			{
				if(this.ExcDateto.Date < this.ExcDatefrom.Date)
				{
					lblErrorMsg.Text="To Date should be greater than or equal to From Date.";
					return;
				}

				if(ExcDateto.Date > DateTime.Now.Date)
				{
					lblErrorMsg.Text="Dates should not be in the future.";
					return;
				}
				
				if(ExcDatefrom.Date > DateTime.Now.Date)
				{
					lblErrorMsg.Text="Dates should not be in the future.";
					return;
				}
			}

			ViewState["BindStatus"] = "Execute";
			this.dgConsignmentStatus.CurrentPageIndex=0;
			BindGrid();
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			clearscreen();
			BindControl();
		}

		private void clearscreen()
		{
			ExcSenderName="";			
			ExcConsignment_no="";
			ExcStatus_id=-1;
			ExcShippingList_No="";
			ExcRecipient_telephone="";
			ExcRef_no="";
			ExcRecipient_zipcode=""; 
			ExcService_code="";
			ExcDatefrom=DateTime.MinValue; 
			ExcDateto=DateTime.MinValue;

			btnQry.Enabled = true;
			btnExecQry.Enabled = false;
			btnPrintConsNote.Enabled = false;
			btnPrintShipList.Enabled = false;
			btnReprintConsNote.Enabled = false;

			ddlSenderName.Enabled=false;
			ddlSenderName.Items.Clear();
//			txtConsNo.Text="";
			txtShipList.Text="";
//			txtRef.Text="";
//			ddlServiceType.Items.Clear();
//			ddlStatus.Items.Clear();
//			txtRecipPhoneNo.Text="";
//			txtRecipPostCode.Text="";
			txtDateFrom.Text="";
			txtDateTo.Text="";

			lblErrorMsg.Text="";

			ConsList="";

			UserCustomerAccount = CustomerConsignmentDAL.GetCustomerAccount(appID,enterpriseID,userID);
			this.EnterpriseConfig = EnterpriseConfigMgrDAL.GetConsignmentStatusPrintingConfigurations(appID,enterpriseID);	

			if(UserCustomerAccount.IsEnterpriseUser==false)
			{
				DataSet dsRef = CustomerConsignmentDAL.GetReferenceDS(appID,enterpriseID,this.userID, UserCustomerAccount.Payerid).ds;
				
				if(dsRef != null && dsRef.Tables["ReferenceTable"] != null)
				{
					this.ddlSenderName.DataSource=dsRef.Tables["ReferenceTable"];
					this.ddlSenderName.DataTextField="snd_rec_name";
					this.ddlSenderName.DataValueField="snd_rec_name";
					this.ddlSenderName.DataBind();											
				}
			}


			if(UserCustomerAccount.IsEnterpriseUser)
			{
				btnExecQry.Enabled = false;
				ddlSenderName.Items.Clear();
			} 
			else if(UserCustomerAccount.IsCustomerUser ==true && UserCustomerAccount.IsMasterCustomerUser==true)
			{
				ddlSenderName.Enabled=true;
			}
			else if(UserCustomerAccount.IsCustomerUser ==true && UserCustomerAccount.IsMasterCustomerUser==false)
			{				
				ddlSenderName.Enabled=false;
				if(UserCustomerAccount.DefaultSender != "")
				{
					try
					{
						this.ddlSenderName.SelectedValue=UserCustomerAccount.DefaultSender;
					}
					catch
					{

					}
				}		
			}

			DataSet dsResult = CustomerConsignmentDAL.CSS_ConsignmentStatus(this.appID,this.enterpriseID,"-1","-1",null,-1,null,null,null,null,null,DateTime.MinValue,DateTime.MinValue);
			System.Data.DataTable dtList = dsResult.Tables[1];
			dtList.Columns.Add("Check",typeof(bool));
			this.dgConsignmentStatus.CurrentPageIndex=0;
			this.dgConsignmentStatus.PageSize=this.EnterpriseConfig.ConsignmentStatusGrid;
			this.dgConsignmentStatus.DataSource = dtList;
			this.dgConsignmentStatus.DataBind();
		}

		private void BindControl()
		{			
			if(UserCustomerAccount.IsEnterpriseUser)
			{
				btnExecQry.Enabled = false;
			} 
			else
			{
				btnExecQry.Enabled=true;
			}

			DataSet ds = SysDataMgrDAL.GetServiceCodeDS(this.appID,this.enterpriseID).ds;			
//			ddlServiceType.DataSource=ds;
//			ddlServiceType.DataTextField="service_code";
//			ddlServiceType.DataValueField="service_code";
//			ddlServiceType.DataBind();
//			ddlServiceType.Items.Insert(0,new ListItem("",""));


			DataSet dsStatus = CustomerConsignmentDAL.GetCustomerStatus(this.appID,this.enterpriseID);
//			ddlStatus.DataSource= dsStatus;
//			ddlStatus.DataTextField="DisplayStatus";
//			ddlStatus.DataValueField="StatusID";
//			ddlStatus.DataBind();
//			ddlStatus.Items.Insert(0,new ListItem("",""));

		}

		private void btnPrintConsNote_Click(object sender, System.EventArgs e)
		{
			ConsList="";			
			string cons="";
			foreach(DataGridItem item in dgConsignmentStatus.Items)
			{
				CheckBox chkConNo = (CheckBox)item.FindControl("chkConNo");
				if(chkConNo != null && chkConNo.Checked==true)
				{
					string id = chkConNo.Attributes["CrsID"].ToString();
					if(cons.Length>0)
					{
						cons+=";";
					}
					cons+=id;
				}
			}
			ConsList=cons;
			if(this.ConsList.Length>0)
			{
				string strConsignmentNo = this.ConsList;
				DataSet dsConsignments = CustomerConsignmentDAL.CSS_PrintConsNotes(appID,enterpriseID,userID,strConsignmentNo);
				if(dsConsignments !=null && dsConsignments.Tables[0].Rows.Count>0)
				{
					if(Convert.ToInt32( dsConsignments.Tables[0].Rows[0]["ErrorCode"]) > 0)
					{
						this.lblErrorMsg.Text = dsConsignments.Tables[0].Rows[0]["ErrorMessage"].ToString();
					}
					else if(dsConsignments.Tables[0].Rows[0]["ReportTemplate"].ToString() =="")
					{
						lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"TEMPLATE_NOT_FOUND",utility.GetUserCulture());;
					}
					else
					{
						lblErrorMsg.Text ="";
					}
				}
				if(lblErrorMsg.Text =="")
				{
					String strUrl = null;
					strUrl = "ReportViewer1.aspx";
					String reportTemplate = dsConsignments.Tables[0].Rows[0]["ReportTemplate"].ToString() ;
					Session["REPORT_TEMPLATE"] = reportTemplate;
					Session["FORMID"] = "PrintConsNotes";
					Session["SESSION_DS_PRINTCONSNOTES"] = dsConsignments;
					Session["FormatType"] = ".pdf";
					ArrayList paramList = new ArrayList();
					paramList.Add(strUrl);
					String sScript = Utility.GetScript("openParentWindow.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

					this.dgConsignmentStatus.CurrentPageIndex=0;
					BindGrid();
				}
			}
			
		}

		private void dgConsignmentStatus_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{
			dgConsignmentStatus.CurrentPageIndex = e.NewPageIndex;
			dgConsignmentStatus.SelectedIndex = -1;
			dgConsignmentStatus.EditItemIndex = -1;
			BindGrid();		
		}

		private void dgConsignmentStatus_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			if(e.CommandName.ToLower()=="previewconsignment_no")
			{
				LinkButton lnkConsignment_no = (LinkButton)e.Item.FindControl("lnkConsignment_no");
				System.Web.UI.HtmlControls.HtmlInputHidden hBooking_No = (System.Web.UI.HtmlControls.HtmlInputHidden)e.Item.FindControl("hBooking_No");
				if(lnkConsignment_no != null)
				{
					Session["toRefresh"]=false;
					string cons_no = lnkConsignment_no.Text;
					string strBookNo= hBooking_No.Value;
					string strUrl = "ShipmentStatus.aspx?Type=CONSGNO&Number="+cons_no+"&PlayerId=&Err=F";

					ArrayList paramList = new ArrayList();
					paramList.Add(strUrl);
					String sScript = Utility.GetScript("openParentWindow.js",paramList);
					Utility.RegisterScriptString(sScript,"DomesticShipmentDisplayPopupScript",this.Page);
				}
			}
			else if(e.CommandName.ToLower()=="updateconsignment_no")
			{
				LinkButton lnkConsignment_no = (LinkButton)e.Item.FindControl("lnkConsignment_no");
				System.Web.UI.HtmlControls.HtmlInputHidden hBooking_No = (System.Web.UI.HtmlControls.HtmlInputHidden)e.Item.FindControl("hBooking_No");
				if(lnkConsignment_no != null)
				{
					string cons_no = lnkConsignment_no.Text;
					Response.Redirect("CreateUpdateConsignment.aspx?MODID=02Create/Update Consignment&PARENTMODID=06Customer Self Service&consignment_no="+cons_no);
				}

			}
		}

		public void changed(object sender, System.EventArgs e)
		{
			string id = string.Empty;
			string lst = ConsList;
			if (((CheckBox)sender).Checked)
			{
				id = ((CheckBox)sender).Attributes["CrsID"].ToString();
				if(lst != "")
				{
					lst+=";";
				}
				lst+=id;
				//arrCourse.Add(id);
			}
			else
			{
				id = ((CheckBox)sender).Attributes["CrsID"].ToString();
				//arrCourse.Remove(id);
				lst.Replace(id,"");
				lst.Replace(";;",";");
				if(lst.Length==1 && lst==";")
				{
					lst="";
				}
			}
			ConsList=lst;
		}

		private void btnPrintShipList_Click(object sender, System.EventArgs e)
		{
			try
			{
				ConsList="";			
				string cons="";
				foreach(DataGridItem item in dgConsignmentStatus.Items)
				{
					CheckBox chkConNo = (CheckBox)item.FindControl("chkConNo");
					if(chkConNo != null && chkConNo.Checked==true)
					{
						string id = chkConNo.Attributes["CrsID"].ToString();
						if(cons.Length>0)
						{
							cons+=";";
						}
						cons+=id;
					}
				}
				ConsList=cons;

				if(this.ConsList.Length>0)
				{
					string strConsignmentNo = this.ConsList;
					DataSet dsShipping = CustomerConsignmentDAL.CSS_PrintShippingList(appID,enterpriseID,userID,strConsignmentNo);
					if(dsShipping !=null && dsShipping.Tables[0].Rows.Count>0)
					{
						if(Convert.ToInt32( dsShipping.Tables[0].Rows[0]["ErrorCode"]) > 0)
						{
							this.lblErrorMsg.Text = dsShipping.Tables[0].Rows[0]["ErrorMessage"].ToString();
						}
						else
						{
							lblErrorMsg.Text ="";
						}
					}
					if(lblErrorMsg.Text =="")
					{
						String strUrl = null;
						strUrl = "ReportViewer1.aspx";
						Session["FORMID"] = "PrintShipping";
						Session["SESSION_DS1_ShipmentList"] = dsShipping;
						Session["FormatType"] = ".pdf";
						ArrayList paramList = new ArrayList();
						paramList.Add(strUrl);
						String sScript = Utility.GetScript("openParentWindow.js",paramList);
						Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

						this.dgConsignmentStatus.CurrentPageIndex=0;
						BindGrid();
					}
				}
			}
			catch(Exception ex)
			{
				lblErrorMsg.Text =ex.Message;
			}

		}

		private void btnReprintConsNote_Click(object sender, System.EventArgs e)
		{
			ConsList="";			
			string cons="";
			foreach(DataGridItem item in dgConsignmentStatus.Items)
			{
				CheckBox chkConNo = (CheckBox)item.FindControl("chkConNo");
				if(chkConNo != null && chkConNo.Checked==true)
				{
					string id = chkConNo.Attributes["CrsID"].ToString();
					if(cons.Length>0)
					{
						cons+=";";
					}
					cons+=id;
				}
			}
			ConsList=cons;

			if(this.ConsList.Length>0)
			{
				string strConsignmentNo = this.ConsList;
				DataSet dsConsignments = CustomerConsignmentDAL.CSS_ReprintConsNote(appID,enterpriseID,userID,strConsignmentNo,0,-1);
				if(dsConsignments !=null && dsConsignments.Tables[0].Rows.Count>0)
				{
					if(Convert.ToInt32( dsConsignments.Tables[0].Rows[0]["ErrorCode"]) > 0)
					{
						this.lblErrorMsg.Text = dsConsignments.Tables[0].Rows[0]["ErrorMessage"].ToString();
					}
					else if(dsConsignments.Tables[0].Rows[0]["ReportTemplate"].ToString() =="")
					{
						lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"TEMPLATE_NOT_FOUND",utility.GetUserCulture());;
					}
					else
					{
						lblErrorMsg.Text ="";
					}
				}
				if(lblErrorMsg.Text =="")
				{
					String strUrl = null;
					strUrl = "ReportViewer1.aspx";
					String reportTemplate = dsConsignments.Tables[0].Rows[0]["ReportTemplate"].ToString() ;
					Session["REPORT_TEMPLATE"] = reportTemplate;
					Session["FORMID"] = "PrintConsNotes";
					Session["SESSION_DS_PRINTCONSNOTES"] = dsConsignments;
					Session["FormatType"] = ".pdf";
					ArrayList paramList = new ArrayList();
					paramList.Add(strUrl);
					String sScript = Utility.GetScript("openParentWindow.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);


					this.dgConsignmentStatus.CurrentPageIndex=0;
					BindGrid();
				}
			}
		}

		private void dgConsignmentStatus_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if((e.Item.ItemType == ListItemType.Item) || 
				(e.Item.ItemType == ListItemType.AlternatingItem))
			{
				LinkButton lnkConsignment_no = (LinkButton)e.Item.FindControl("lnkConsignment_no");
				if(lnkConsignment_no != null)
				{
					int status = int.Parse(DataBinder.Eval(e.Item.DataItem,"ProcessStep").ToString());
					if(status==4)
					{
						lnkConsignment_no.CommandName="previewConsignment_no";
					}
					else
					{
						lnkConsignment_no.CommandName="updateConsignment_no";
					}
					
				}
			} 		
		}
	}
}
