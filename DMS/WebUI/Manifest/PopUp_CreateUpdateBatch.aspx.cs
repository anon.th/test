using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;
using com.ties.DAL;
using com.common.RBAC;

namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for PopUp_CreateUpdateBatch.
	/// </summary>
	public class PopUp_CreateUpdateBatch : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label lblCreateOrUpdateBatch;
		protected System.Web.UI.WebControls.RadioButton rdbCreateNewBatch;
		protected System.Web.UI.WebControls.RadioButton rdbUpdateExistingBatch;
		protected System.Web.UI.WebControls.Label lblBatchID;
		protected System.Web.UI.WebControls.Label lblTruckID;
		protected System.Web.UI.WebControls.Label lblBatchDate;
		protected System.Web.UI.WebControls.Label lblBatchType;
		protected System.Web.UI.WebControls.Label lblBatchNumber;
		protected System.Web.UI.WebControls.TextBox txtBatchType;
		protected com.common.util.msTextBox txtBatchDate;
		protected System.Web.UI.WebControls.DropDownList ddlBatchID;
		protected System.Web.UI.WebControls.Button btnTruckIDSrch;
		protected System.Web.UI.WebControls.Button btnOK;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected com.common.util.msTextBox txtBatchNo;
		protected System.Web.UI.WebControls.DropDownList ddlTruckID;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		string statuscode;
		String m_strAppID=null;
		String m_strEnterpriseID=null;
		protected System.Web.UI.WebControls.Label lblErrrMsg;
		protected System.Web.UI.WebControls.TextBox txtStatusCode;
		protected System.Web.UI.WebControls.TextBox txtMode;
		protected System.Web.UI.WebControls.TextBox txtBatchNoFromPopup;
		protected System.Web.UI.WebControls.TextBox hidBatchNo;
		protected System.Web.UI.WebControls.TextBox hidTruckID;
		protected System.Web.UI.WebControls.TextBox hidBatchDate;
		protected System.Web.UI.WebControls.TextBox hidBatchType;
		protected System.Web.UI.WebControls.TextBox hidBatchID;
		protected System.Web.UI.WebControls.Button btnPopup;
		string strFormID;
		protected System.Web.UI.WebControls.Button btnConfirmUpdate;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlTableRow trServiceType;
		protected System.Web.UI.WebControls.DropDownList ddlServiceType;
		string location;
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//btnOK.Attributes.Add("OnClick","return validate();");
			getSessionTimeOut(true);
			btnPopup.Attributes.Add("OnClick","return popup();");
			strFormID= Request.Params["strFormID"].ToString();
			statuscode = Request.Params["status"].ToString();
			location = Request.Params["usrloc"].ToString();
			txtStatusCode.Text = statuscode;
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();

			if(!Page.IsPostBack)
			{
				ControlChk();
				LoadBatch();
				LoadTruckAll();
				Session["Mode"]=null;
				if(statuscode=="SOP" || statuscode=="CLS" )
				{
					rdbCreateNewBatch.Enabled=true;
					if(statuscode=="CLS")
					{
						ddlTruckID.SelectedValue="";
						ddlTruckID.Enabled=false;
					}
				}
				else
				{
					rdbCreateNewBatch.Enabled=false;
					rdbCreateNewBatch.Checked=false;
					rdbUpdateExistingBatch.Checked=true;

					EnableOnClickUpdateExistingBatch();
				}
				ddlBatchID.Enabled=false;
				ddlTruckID.Enabled=false;
				txtBatchDate.Enabled =false;
				txtBatchType.Enabled =false;
				setFocusCtrl(txtBatchNo.ClientID);

				LoadServiceType();
			}
		}

        private void LoadServiceType()
        {
            // Thosapol Yennam 20/12/2013 Modify
            //ddlServiceType.DataSource = com.ties.classes.DbComboDAL.GetAllServiceCode (m_strAppID,m_strEnterpriseID);	
            //ddlServiceType.DataBind();

            DataSet ds = com.ties.classes.DbComboDAL.GetAllServiceCode(m_strAppID, m_strEnterpriseID);

            if ((ds.Tables.Count > 0) && (ds.Tables[0] != null))
            {
                this.ddlServiceType.DataSource = ds.Tables[0];
                this.ddlServiceType.DataBind();

                ListItem defItem = new ListItem();
                defItem.Text = "";
                defItem.Value = "";

                this.ddlServiceType.Items.Insert(0, (defItem));
            }
            else
            {
                this.ddlServiceType.Items.Clear();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConfirmUpdate.Click += new System.EventHandler(this.btnConfirmUpdate_Click);
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.rdbCreateNewBatch.CheckedChanged += new System.EventHandler(this.rdbCreateNewBatch_CheckedChanged);
            this.ddlBatchID.SelectedIndexChanged += new System.EventHandler(this.ddlBatchID_SelectedIndexChanged);
            this.btnTruckIDSrch.Click += new System.EventHandler(this.btnTruckIDSrch_Click);
            this.txtBatchType.TextChanged += new System.EventHandler(this.txtBatchType_TextChanged);
            this.rdbUpdateExistingBatch.CheckedChanged += new System.EventHandler(this.rdbUpdateExistingBatch_CheckedChanged);
            this.btnPopup.Click += new System.EventHandler(this.btnPopup_Click);
            this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public void LoadBatch()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

            DataSet dataset = com.ties.classes.DbComboDAL.BatchIDQuery(strAppID, strEnterpriseID, location, statuscode);

            if ((dataset.Tables.Count > 0) && (dataset.Tables[0] != null))
            {
                this.ddlBatchID.DataSource = dataset.Tables[0];
                this.ddlBatchID.DataTextField = dataset.Tables[0].Columns["path_code"].ToString();
                this.ddlBatchID.DataValueField = dataset.Tables[0].Columns["value"].ToString();
                this.ddlBatchID.DataBind();

                ListItem defItem = new ListItem();
                defItem.Text = "";
                defItem.Value = "";

                this.ddlBatchID.Items.Insert(0, (defItem));
            }
            else
            {
                this.ddlBatchID.Items.Clear();
            }
        }

        public void LoadTruckAll()
        {
            Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings, HttpContext.Current.Session);
            String strAppID = util.GetAppID();
            String strEnterpriseID = util.GetEnterpriseID();

            //DataSet dataset = com.ties.classes.DbComboDAL.TruckIDQuery(strAppID,strEnterpriseID,"Y","Null",0);	
            DataSet dataset = com.ties.classes.DbComboDAL.TruckIDQuery(strAppID, strEnterpriseID, "N", ddlBatchID.SelectedItem.Text, 0);

            if ((dataset.Tables.Count > 0) && (dataset.Tables[0] != null))
            {
                this.ddlTruckID.DataSource = dataset.Tables[0];
                this.ddlTruckID.DataTextField = dataset.Tables[0].Columns["truckid"].ToString();
                this.ddlTruckID.DataValueField = dataset.Tables[0].Columns["truckid"].ToString();
                this.ddlTruckID.DataBind();

                ListItem defItem = new ListItem();
                defItem.Text = "";
                defItem.Value = "";

                this.ddlTruckID.Items.Insert(0, (defItem));
            }
            else
            {
                this.ddlTruckID.Items.Clear();
            }
        }
        public void LoadTruck()
        {
            Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings, HttpContext.Current.Session);
            String strAppID = util.GetAppID();
            String strEnterpriseID = util.GetEnterpriseID();

            DataSet dataset = com.ties.classes.DbComboDAL.TruckIDQuery(strAppID, strEnterpriseID, "N", ddlBatchID.SelectedItem.Text, 0);

            if ((dataset.Tables.Count > 0) && (dataset.Tables[0] != null))
            {
                this.ddlTruckID.DataSource = dataset.Tables[0];
                this.ddlTruckID.DataTextField = dataset.Tables[0].Columns["truckid"].ToString();
                this.ddlTruckID.DataValueField = dataset.Tables[0].Columns["truckid"].ToString();
                this.ddlTruckID.DataBind();

                ListItem defItem = new ListItem();
                defItem.Text = "";
                defItem.Value = "";

                this.ddlTruckID.Items.Insert(0, (defItem));
            }
            else
            {
                LoadTruckAll();
            }
        }

        public void ControlChk()
        {
            if (rdbCreateNewBatch.Checked)
            {
                GetCurrentDate();
                EnableOnClickCreateNewBatch();
            }
            else
            {
                txtBatchDate.Text = "";
                EnableOnClickUpdateExistingBatch();
            }
        }

		private void rdbCreateNewBatch_CheckedChanged(object sender, System.EventArgs e)
		{
			GetCurrentDate();
			if(rdbCreateNewBatch.Checked)
			{
				txtBatchNo.Text="";
			}
			EnableOnClickCreateNewBatch();
		}

		private void rdbUpdateExistingBatch_CheckedChanged(object sender, System.EventArgs e)
		{
			txtBatchDate.Text =""; 
			EnableOnClickUpdateExistingBatch();
			if(rdbUpdateExistingBatch.Checked)
			{
				ddlBatchID.SelectedValue="";
				ddlTruckID.SelectedValue="";
				txtBatchDate.Text="";
				txtBatchType.Text="";
			}
			setFocusCtrl(txtBatchNo.ClientID);
		}

		private void ddlBatchID_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			DataSet dataset = com.ties.classes.DbComboDAL.BatchTypeQuery(strAppID,strEnterpriseID,ddlBatchID.SelectedItem.Text.ToString() );	
			//this.ddlTruckID.DataSource = dataset;
			 
			string path_des = ddlBatchID.SelectedValue.ToString();
			LoadTruck();
			if (dataset.Tables[0].Rows.Count>0)
			{
				string del_type = dataset.Tables[0].Rows[0]["delivery_type"].ToString()  ;
				string batch_type="";

				if(del_type.ToUpper()=="W")
				{
					batch_type ="Current Location";
					ddlTruckID.SelectedIndex=0;
					ddlTruckID.Enabled=false;
					btnTruckIDSrch.Enabled=false;
				}
				else if (del_type.ToUpper()=="S")
				{
					batch_type ="Delivery";
					ddlTruckID.Enabled=true;
					btnTruckIDSrch.Enabled=true;
				}
				else if (del_type.ToUpper()=="L")
				{
					batch_type ="Linehaul";
					ddlTruckID.Enabled=true;
					btnTruckIDSrch.Enabled=true;
				}
				else if (del_type.ToUpper()=="A")
				{
					batch_type ="Air";
					ddlTruckID.Enabled=true;
					btnTruckIDSrch.Enabled=true;
				}
				txtBatchType.Text =batch_type;

				if(txtBatchType.Text == "Air")
					trServiceType.Style["display"] = "block";
				else
					trServiceType.Style["display"] = "none";
			}
			else
			{
				txtBatchType.Text ="";
				ddlTruckID.Enabled=true;
				btnTruckIDSrch.Enabled=true;

			}
			if(statuscode=="CLS")
			{
				ddlTruckID.SelectedValue="";
				ddlTruckID.Enabled=false;
			}
		}
		private void GetCurrentDate()
		{
			txtBatchDate.Text = DateTime.Now.ToString("dd/MM/yyyy")  ; 
		}

		private void EnableOnClickCreateNewBatch()
		{
			txtBatchNo.Enabled   = false;
			txtBatchNo.BackColor = Color.Silver; 
			ddlBatchID.Enabled = true;
			ddlBatchID.BackColor = Color.White;
			ddlTruckID.Enabled = true;
			ddlTruckID.BackColor = Color.White;
			txtBatchDate.Enabled = true;
			txtBatchDate.BackColor = Color.White ;
			txtBatchType.Enabled = true;
			txtBatchType.BackColor = Color.White ; 
			btnTruckIDSrch.Enabled = true;

			if(statuscode=="CLS")
			{
				//ddlTruckID.SelectedValue="";
				ddlTruckID.Enabled=false;
				btnTruckIDSrch.Enabled=false;
			}
		}

		private void EnableOnClickUpdateExistingBatch()
		{
			txtBatchNo.Enabled    = true;
			txtBatchNo.BackColor = Color.White ; 
			ddlBatchID.Enabled = false;
			ddlBatchID.BackColor = Color.Silver;
			ddlTruckID.Enabled = false;
			ddlTruckID.BackColor = Color.Silver;
			txtBatchDate.Enabled = false;
			txtBatchDate.BackColor = Color.Silver;
			txtBatchType.Enabled = false;
			txtBatchType.BackColor = Color.Silver;
			btnTruckIDSrch.Enabled = false;
			

		}
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openLargerChildParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
		private void btnTruckIDSrch_Click(object sender, System.EventArgs e)
		{
			LoadTruckAll();
			OpenWindowpage("Vehicles.aspx?FORMID=PopUp_CreateUpdateBatch&BatchID=" + ddlBatchID.SelectedItem.Text);
			//OpenWindowpage("PopUp_Arrival_Departure.aspx?FORMID=BatchManifest_ControlPanel&AppID="+m_strAppID+"&EnterpriseID="+m_strEnterpriseID+"&btnname=Arrival&UserID="+""+"&Location="+ddlTruckID.SelectedItem.Text+"&BatchNo="+(string)ViewState["batch_no"]+"&RecIsCom="+(string)ViewState["RecIsCom"] +"  ");

		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			if(ValidateCtrl())
			{
				if(rdbUpdateExistingBatch.Checked==true)
				{
					string strlocation = Request.Params["usrloc"].ToString();
					string strOrigin="";
					string strDes="";
					DataSet dest =BatchManifest.GetDestinationAllType(m_strAppID,m_strEnterpriseID,(string)ViewState["batch_id"]);
					//,(string)ViewState["batch_type"]
					if (dest.Tables[0].Rows.Count > 0 )
					{
						strOrigin = dest.Tables[0].Rows[0]["origin_station"].ToString(); 
					}
					if (dest.Tables[0].Rows.Count > 0 )
					{
						strDes = dest.Tables[0].Rows[0]["destination_station"].ToString(); 
					}
				
					if(ViewState["batch_type"].ToString()=="S")
					{
						DataSet Arr =BatchManifest.GetBatchDetail_ShippingOriginLoad(m_strAppID,m_strEnterpriseID,txtBatchNo.Text.Trim(),strlocation);
						if (Arr.Tables[0].Rows.Count > 0 )
						{
							//1
							if(Arr.Tables[0].Rows[0]["Arr_DT"]==System.DBNull.Value && (statuscode.ToUpper() == "SIP"||statuscode.ToUpper() == "UNSIP"))
							{
								lblErrrMsg.Text="Status code not allowed prior to arrival.";
								return;
							}
							//Case �á
							if(Arr.Tables[0].Rows[0]["dep_DT"]!=System.DBNull.Value && Arr.Tables[0].Rows[0]["Arr_DT"]==System.DBNull.Value && (statuscode.ToUpper() == "SOP"||statuscode.ToUpper() == "UNSOP"))
							{
								string script = "<script language='javascript'> ";
								script += "ConfirmUpdate();";
								script += "</script>";
								Page.RegisterStartupScript("Popup_Click",script);
								return;
							}
							//Case �á
							if(Arr.Tables[0].Rows[0]["Arr_DT"]!=System.DBNull.Value && (statuscode.ToUpper() == "SOP"||statuscode.ToUpper() == "UNSOP"))
							{
								lblErrrMsg.Text="Status code not allowed after arrival.";
								return;
							}

							//2
							//							if(Arr.Tables[0].Rows[0]["Dep_DT"]!=System.DBNull.Value && (statuscode.ToUpper() == "SOP"||statuscode.ToUpper() == "UNSOP"))
							//							{
							//								string script = "<script language='javascript'> ";
							//								script += "ConfirmUpdate();";
							//								script += "</script>";
							//								Page.RegisterStartupScript("Popup_Click",script);
							//								return;
							//							}
						}
					}
					else if(ViewState["batch_type"].ToString()=="L"||ViewState["batch_type"].ToString()=="A")
					{
						DataSet Arr =BatchManifest.GetBatchDetail_ShippingOriginLoad(m_strAppID,m_strEnterpriseID,txtBatchNo.Text.Trim(),strlocation);
						
						//3
						if(strOrigin==strlocation && (statuscode.ToUpper() == "SIP"||statuscode.ToUpper() == "UNSIP"))
						{
							lblErrrMsg.Text="Status code not allowed at the origin DC.";
							return;
						}
						if(strOrigin!=strlocation)
						{
							//								if (Arr.Tables[0].Rows.Count > 0 )
							//								{
							//									//4
							//									if(Arr.Tables[0].Rows[0]["Dep_DT"]!=System.DBNull.Value && (statuscode.ToUpper() == "SIP"||statuscode.ToUpper() == "UNSIP"))
							//									{
							//										string script = "<script language='javascript'> ";
							//										script += "ConfirmUpdate();";
							//										script += "</script>";
							//										Page.RegisterStartupScript("Popup_Click",script);
							//										return;
							//									}
							//								}

							//5
							DataSet Arr_DC =BatchManifest.GetBatchDetail_ShippingTerminationLoad(m_strAppID,m_strEnterpriseID,txtBatchNo.Text.Trim(),strlocation);
							if (Arr_DC.Tables[0].Rows.Count > 0 )
							{
								if(Arr_DC.Tables[0].Rows[0]["Arr_DT"]==System.DBNull.Value && (statuscode.ToUpper() == "SIP"||statuscode.ToUpper() == "UNSIP"))
								{
									lblErrrMsg.Text="Status code not allowed prior to arrival.";
									return;
								}
							}
							else
							{
								if(statuscode.ToUpper() == "SIP"||statuscode.ToUpper() == "UNSIP")
								{
									lblErrrMsg.Text="Status code not allowed prior to arrival.";
									return;
								}
							}
						}
						//6
						if(strlocation==strDes && (statuscode.ToUpper() == "SOP"||statuscode.ToUpper() == "UNSOP"))
						{
							lblErrrMsg.Text="Status code not allowed at terminal DC.";
							return;
								
						}
						if(strlocation!=strDes)
						{
							if (Arr.Tables[0].Rows.Count > 0 )
							{
								//7
								if(Arr.Tables[0].Rows[0]["Dep_DT"]!=System.DBNull.Value && Arr.Tables[0].Rows[0]["Arr_DT"] ==System.DBNull.Value &&(statuscode.ToUpper() == "SOP"||statuscode.ToUpper() == "UNSOP"))
								{
									string script = "<script language='javascript'> ";
									script += "ConfirmUpdate();";
									script += "</script>";
									Page.RegisterStartupScript("Popup_Click",script);
									return;
								}
								//Case �á
								if(Arr.Tables[0].Rows[0]["Dep_DT"]!=System.DBNull.Value && Arr.Tables[0].Rows[0]["Arr_DT"]!=System.DBNull.Value && (statuscode.ToUpper() == "SOP"||statuscode.ToUpper() == "UNSOP"))
								{
									lblErrrMsg.Text="Status code not allowed after arrival.";
									return;
								}
							}
							
						}
						
					}
				}

				string script1 = "<script language='javascript'> ";
				script1 += "document.getElementById('btnPopup').click();";
				script1 += "</script>";
				Page.RegisterStartupScript("Popup_Click",script1);
			}


		}
		private void btnPopup_Click(object sender, System.EventArgs e)
		{
			InsertUpdateData();
			Session["Mode"]=null;
		}

		private string GenerateBatchNo()
		{
			string del_type = ddlBatchID.SelectedValue.ToString();
			//string del_type = txtBatchType.Text.ToString();
			string[] deltype = del_type.Split('|');
			string batch_type ="";
			int batchno;
			string result;
			
			if(deltype[0].ToUpper()=="W")
			{
				batch_type ="Batch_CL";
			}
			else if (deltype[0].ToUpper()=="S")
			{
				batch_type ="Batch_DL";
			}
			else if (deltype[0].ToUpper()=="L")
			{
				batch_type ="Batch_LH";
			}
			else if (deltype[0].ToUpper()=="A")
			{
				batch_type ="Batch_AR";
			}
			batchno=(int)Counter.GetNext(m_strAppID,m_strEnterpriseID,batch_type);
			result = batchno.ToString();
			batchno = batchno%7;
			result += batchno;
			return result;
		}
		private bool ValidateCtrl()
		{
			bool result = true;
			lblErrrMsg.Text="";
			if(rdbCreateNewBatch.Checked)
			{
				if(ddlBatchID.SelectedValue=="")
				{
					lblErrrMsg.Text ="Select Batch ID.";
					result=false;
					return result;
				}
				else if(ddlTruckID.SelectedValue=="" && statuscode.ToUpper()!="CLS" && ddlTruckID.Enabled==true)
				{
					lblErrrMsg.Text ="Select Truck ID.";
					result=false;
					return result;
				}
				else if(txtBatchDate.Text =="")
				{
					lblErrrMsg.Text ="Insert Batch date";
					result=false;
					return result;
				
				}
				else if(txtBatchType.Text=="")
				{
					lblErrrMsg.Text ="Insert Batch Type.";
					result=false;
					return result;
				}
				if((statuscode.ToUpper() == "CLS" || statuscode.ToUpper()=="UNCLS") && (txtBatchType.Text!="Current Location"))
				{
					if(strFormID!="ShipmentUpdate")
					{
						lblErrrMsg.Text ="Invalid batch type for selected status code.";
						result=false;
						return result;
					}
					else
					{
						lblErrrMsg.Text ="Status code selected is only applicable for a current location batch.";
						result=false;
						return result;
					}
				}
				if(statuscode.ToUpper() == "SIP" || statuscode.ToUpper()=="SOP" || statuscode.ToUpper() == "UNSIP" || statuscode.ToUpper()=="UNSOP") 
				{
					if(txtBatchType.Text!="Delivery" && txtBatchType.Text!="Linehaul" && txtBatchType.Text!="Air" )
					{
						if(strFormID!="ShipmentUpdate")
						{

							lblErrrMsg.Text ="Invalid batch type for selected status code.";
							result=false;
							return result;
						}
						else
						{
							lblErrrMsg.Text ="Invalid status code selected for current location batch.";
							result=false;
							return result;
						}
					}
				}
				DateTime cNow = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);
				DateTime BatchDate = DateTime.ParseExact(txtBatchDate.Text.ToString(), "dd/MM/yyyy", null);
				BatchDate = new DateTime(BatchDate.Year,BatchDate.Month,BatchDate.Day);
				System.TimeSpan Diff =cNow.Subtract(BatchDate);
				//
				if((Diff.Days!=0) &&(Diff.Days!=-1))
				{

					lblErrrMsg.Text ="Invalid batch date- the date of the batch may not be in the past or future date later than tomorrow.";
					result=false;
					return result;

				}
				if(statuscode.ToUpper()=="SOP" && ddlTruckID.SelectedValue.ToString()=="" && ddlTruckID.Enabled==true)
				{
					lblErrrMsg.Text ="Truck ID is required for this batch.";
					result=false;
					return result;
				}
				if(statuscode.ToUpper()=="CLS" && ddlTruckID.SelectedValue.ToString()!=""&& ddlTruckID.Enabled==true)
				{
					lblErrrMsg.Text ="Truck ID must be blank for this batch.";
					result=false;
					return result;
				}
				ViewState["batch_id"]=ddlBatchID.SelectedItem.Text;

			}
			else//Update batch
			{
				if(txtBatchNo.Text=="")
				{
					lblErrrMsg.Text ="Insert Batch no.";
					result=false;
					return result;
				}
				string BatchNo;
				if(Session["Batch_No"]!=null)
				{
					BatchNo = (string)Session["Batch_No"];

				}
				else
				{
					BatchNo = txtBatchNo.Text.ToString();
				}

				DataSet ds = new DataSet();
				ds= SWB_Emulator.SWB_Batch_Manifest_Read(m_strAppID,m_strEnterpriseID,BatchNo);
				if(ds.Tables[0].Rows.Count>0)
				{
					//if()
					DateTime BatchDate  = Convert.ToDateTime(ds.Tables[0].Rows[0]["Batch_date"]);
					DateTime cBatchDate = new DateTime(BatchDate.Year,BatchDate.Month,BatchDate.Day);
					DataSet dsDayToRead = new DataSet();
					dsDayToRead = BatchManifest.GetDayToRead(utility.GetAppID(),utility.GetEnterpriseID());
					if (dsDayToRead.Tables[0].Rows.Count>0)
					{
						DateTime cNow = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);
						System.TimeSpan Diff =cNow.Subtract(cBatchDate);
						if(Diff.Days > int.Parse(dsDayToRead.Tables[0].Rows[0]["code_str_value"].ToString()) )
						{
							lblErrrMsg.Text ="Selected batch number has expired.";
							result=false;
							return result;
						}
					}
					if(((statuscode.ToUpper() == "CLS" || statuscode.ToUpper()=="UNCLS"))&& ds.Tables[0].Rows[0]["Batch_type"].ToString()!="W")
					{
						
						if(strFormID!="ShipmentUpdate")
						{
							lblErrrMsg.Text ="Invalid batch type for selected status code.";
							result=false;
							return result;
						}
						else
						{
							lblErrrMsg.Text ="Status code selected is only applicable for a current location batch.";
							result=false;
							return result;
						}

					}
					else if (((statuscode.ToUpper() == "SIP" || statuscode.ToUpper()=="UNSIP")||(statuscode.ToUpper() == "SOP" || statuscode.ToUpper()=="UNSOP"))
						&& ((ds.Tables[0].Rows[0]["Batch_type"].ToString()!="S")&&(ds.Tables[0].Rows[0]["Batch_type"].ToString()!="L")&&(ds.Tables[0].Rows[0]["Batch_type"].ToString()!="A"))						)
					
					{
						if(strFormID!="ShipmentUpdate")
						{

							lblErrrMsg.Text ="Invalid batch type for selected status code.";
							result=false;
							return result;
						}
						else
						{
							lblErrrMsg.Text ="Invalid status code selected for a current location batch.";
							result=false;
							return result;
						}

					}

					Session["Mode"]="Update";
					txtMode.Text="Update";
					hidBatchNo.Text =ds.Tables[0].Rows[0]["Batch_No"].ToString();
					hidBatchID.Text = ds.Tables[0].Rows[0]["Batch_ID"].ToString();
					hidTruckID.Text =ds.Tables[0].Rows[0]["Truck_ID"].ToString();
					DateTime DTBatchDate = (DateTime)ds.Tables[0].Rows[0]["Batch_Date"];
					hidBatchDate.Text = DTBatchDate.ToString("dd/MM/yyyy");
					ViewState["batch_type"]=ds.Tables[0].Rows[0]["Batch_Type"].ToString().ToUpper();
					ViewState["batch_id"]=ds.Tables[0].Rows[0]["Batch_ID"].ToString();
				
					if(ds.Tables[0].Rows[0]["Batch_Type"].ToString().ToUpper()=="W")
					{
						hidBatchType.Text="Current Location";
					}
					else if (ds.Tables[0].Rows[0]["Batch_Type"].ToString().ToUpper()=="S")
					{
						hidBatchType.Text="Delivery";
					}
					else if (ds.Tables[0].Rows[0]["Batch_Type"].ToString().ToUpper()=="A")
					{
						hidBatchType.Text="Air";
					}
					else//L
					{
						hidBatchType.Text="Linehaul";
					}
					if(ds.Tables[0].Rows[0]["Batch_Status"]!=null)
					{
						if(ds.Tables[0].Rows[0]["Batch_Status"].ToString()=="C")
						{
							lblErrrMsg.Text="Cannot update to closed batch.";
							return false;;
						}
					}

					if(ds.Tables[0].Rows[0]["Batch_type"].ToString()=="S"||ds.Tables[0].Rows[0]["Batch_type"].ToString()=="W")
					{
						DataSet desOrigin =BatchManifest.GetDestinationAllType(utility.GetAppID(),utility.GetEnterpriseID(),ds.Tables[0].Rows[0]["Batch_ID"].ToString());
						string strOriginDC="";
						string strlocation = Request.Params["usrloc"].ToString();
						if (desOrigin.Tables[0].Rows.Count > 0 )
						{
							strOriginDC = desOrigin.Tables[0].Rows[0]["origin_station"].ToString(); 
						}
						if(strOriginDC.Trim()!=strlocation.Trim())
						{
							lblErrrMsg.Text="Update not allowed to this batch from this location.";
							result=false;
							return result;
						}
					}

				}
				else
				{
					String sScript = "";
					sScript += "<script language=javascript> ";
					sScript += "  alert('Batch number : "+txtBatchNo.Text.ToString()+" does not exist. Operation cancelled.')";
					
					sScript += "</script>";
					Response.Write(sScript);
					result=false;
					return result;
				}
			}
			return result;
		}
		private void InsertUpdateData()
		{
			com.common.classes.User user =  RBACManager.GetUser(m_strAppID,m_strEnterpriseID, (String)Session["userID"]);
			string strlocation = Request.Params["usrloc"].ToString();

			DataSet ds = new DataSet();
			if(Session["Mode"]==null || (rdbCreateNewBatch.Checked && Session["Mode"].ToString()!="Update" ) || Session["Mode"].ToString()=="Create")
			{
				string del_type = ddlBatchID.SelectedValue.ToString();
				//string del_type = txtBatchType.Text.ToString();
				string[] deltype = del_type.Split('|');
				
				string Batch_No = GenerateBatchNo();
				DateTime BatchDate = DateTime.ParseExact(txtBatchDate.Text.ToString(), "dd/MM/yyyy", null);

				if(trServiceType.Style["display"]== "block")
				{
					ds=	 SWB_Emulator.SWB_Batch_Manifest_Insert(m_strAppID,m_strEnterpriseID,Batch_No.ToString(),ddlTruckID.SelectedValue.ToString(),
						ddlBatchID.SelectedItem.Text.ToString(),BatchDate,deltype[0].ToUpper(),user.UserID.ToString(),"1", ddlServiceType.SelectedValue);
				}
				else
				{
					ds=	 SWB_Emulator.SWB_Batch_Manifest_Insert(m_strAppID,m_strEnterpriseID,Batch_No.ToString(),ddlTruckID.SelectedValue.ToString(),
						ddlBatchID.SelectedItem.Text.ToString(),BatchDate,deltype[0].ToUpper(),user.UserID.ToString(),"1", "");
				}
			}
			else// if(Session["Mode"].ToString()=="Update" )
			{
				string BatchNo;
				if(Session["Mode"].ToString()=="Update")
				{
					if(Session["Batch_No"]!=null)
					{
						BatchNo = (string)Session["Batch_No"];

					}
					else
					{
						BatchNo = txtBatchNo.Text.ToString();
					}
				}
				else
				{
					BatchNo = txtBatchNo.Text.ToString();
				}
				
				Boolean First=false;
				DataSet dest =BatchManifest.GetDestinationAllType(m_strAppID,m_strEnterpriseID,(string)ViewState["batch_id"]);
				//,(string)ViewState["batch_type"]
				if(dest.Tables[0].Rows.Count>0)
				{
					string strOrigin = dest.Tables[0].Rows[0]["origin_station"].ToString(); 
					
					if(strlocation==strOrigin)
					{
						ds= SWB_Emulator.SWB_Batch_Manifest_Update(m_strAppID,m_strEnterpriseID,BatchNo,user.UserID.ToString(),"1");
						First=true;
					}
				}
				if(First==false)
				{
					DataSet Arr_DC =BatchManifest.GetBatchDetail_ShippingTerminationLoad(m_strAppID,m_strEnterpriseID,BatchNo,strlocation);
					if (Arr_DC.Tables[0].Rows.Count > 0 )
					{
						if(Arr_DC.Tables[0].Rows[0]["Arr_DT"]!=System.DBNull.Value)
						{
							ds= SWB_Emulator.SWB_Batch_Manifest_Update(m_strAppID,m_strEnterpriseID,BatchNo,user.UserID.ToString(),"1");
						}
					}
					else
					{
						//No Update
						ds= SWB_Emulator.SWB_Batch_Manifest_Update(m_strAppID,m_strEnterpriseID,BatchNo,user.UserID.ToString(),"");
					}
				}
			} 

			if(ds.Tables.Count>0)
			{
				if(ds.Tables[0].Rows.Count>0)
				{
					Session["Mode"] = null;
					Session["Batch_No"] =null;
					strFormID= Request.Params["strFormID"];
					string strBATCH_NO_CID = Request.Params["BATCH_NO_CID"];
					string strBATCH_DATE_CID = Request.Params["BATCH_DATE_CID"];
					string strTRUCK_ID_CID = Request.Params["TRUCK_ID_CID"];
					string strBATCH_ID_CID = Request.Params["BATCH_ID_CID"];
					string strBATCH_TYPE_CID = Request.Params["BATCH_TYPE_CID"];
					string strSEVICE_TYPE_CID = Request.Params["SEVICE_TYPE_CID"];
					string strtrSEVICE_TYPE_CID = Request.Params["trSEVICE_TYPE_CID"];

					string strBatchNo = (string)ds.Tables[0].Rows[0]["Batch_No"].ToString();
					DateTime DTBatchDate = (DateTime)ds.Tables[0].Rows[0]["Batch_Date"];
					string strTruckID = (string)ds.Tables[0].Rows[0]["Truck_ID"].ToString();
					string strBatchID = (string)ds.Tables[0].Rows[0]["Batch_ID"].ToString();
					string strBatchType = (string)ds.Tables[0].Rows[0]["Batch_Type"].ToString();
					string strServiceType = "";
					if(ds.Tables[0].Rows[0]["ServiceType"] != null)
						strServiceType = (string)ds.Tables[0].Rows[0]["ServiceType"].ToString();
					string strBatchDate =DTBatchDate.ToString("dd/MM/yyyy");
					if(ds.Tables[0].Rows[0]["Batch_Type"].ToString().ToUpper()=="W")
					{
						strBatchType="Current Location";
					}
					else if (ds.Tables[0].Rows[0]["Batch_Type"].ToString().ToUpper()=="S")
					{
						strBatchType="Delivery";
					}
					else if (ds.Tables[0].Rows[0]["Batch_Type"].ToString().ToUpper()=="A")
					{
						strBatchType="Air";
					}
					else//L
					{
						strBatchType="Linehaul";
					}
					Session["CreateBatchState"]="Scans Complete";
					Session["BatchNo"]=strBatchNo;
					Session["BatchID"]=strBatchID;
					String sScript = "";
					sScript += "<script language=javascript> ";
					if(strFormID!="ShipmentUpdate")
					{
						sScript += "  window.opener.document.getElementById('"+strBATCH_NO_CID+"').innerHTML = \""+strBatchNo+"\";";
						sScript += "  window.opener.document.getElementById('"+strBATCH_DATE_CID+"').innerHTML = \""+strBatchDate+"\";";
						sScript += "  window.opener.document.getElementById('"+strTRUCK_ID_CID+"').innerHTML = \""+strTruckID+"\";";
						sScript += "  window.opener.document.getElementById('"+strBATCH_ID_CID+"').innerHTML = \""+strBatchID+"\";";
						sScript += "  window.opener.document.getElementById('"+strBATCH_TYPE_CID+"').innerHTML = \""+strBatchType+"\";";
						if(strBatchType == "Air")
						{
							sScript += "  window.opener.document.getElementById('"+strSEVICE_TYPE_CID+"').innerHTML = \""+strServiceType+"\";";
							sScript += "  window.opener.document.getElementById('"+strtrSEVICE_TYPE_CID+"').style.display = 'block';";
						}
						else
						{
							sScript += "  window.opener.document.getElementById('"+strtrSEVICE_TYPE_CID+"').style.display = 'none';";
						}
						sScript += "  window.opener.document.getElementById('btnScanComplete').value = \"Scans Complete\";";
						sScript += "  window.opener.document.getElementById('txtBatch_No').value = \""+ strBatchNo +"\";";
						sScript += "  window.opener.document.getElementById('ddlStatusCode').disabled =true;";
						sScript += "  window.opener.document.getElementById('txtLastestScan').focus();";
					}
					else
					{
						//Session["BatchNo"]=strBatchNo;
						strBatchNo="Batch: " + strBatchNo+" selected";
						sScript += "  window.opener.document.getElementById('btnSubmitHidden').click();";
					}
					//document.getElementById("txtLastestScan").focus();
					sScript += "  window.close();";
				
					sScript += "</script>";
					Response.Write(sScript);


				}
				else
				{
					if(rdbUpdateExistingBatch.Checked && txtBatchNo.Text.ToString()!="")
					{
						String sScript = "";
						sScript += "<script language=javascript> ";
						sScript += "  alert('Batch number : "+txtBatchNo.Text.ToString()+" does not exist. Operation cancelled.')";
					
						sScript += "</script>";
						Response.Write(sScript);
					}
				}
			}
		}
		//		private void txtBatchNo_TextChanged(object sender, System.EventArgs e)
		//		{
		//			lblErrrMsg.Text="";
		//			string batchNo =txtBatchNo.Text;
		//			if(batchNo!="" && rdbUpdateExistingBatch.Checked)
		//			{
		//
		//				DataSet dsUpd = new DataSet();
		//				dsUpd= SWB_Emulator.SWB_Batch_Manifest_Read(m_strAppID,m_strEnterpriseID,batchNo);
		//				if(dsUpd.Tables[0].Rows.Count>0)
		//				{
		//					Session["Mode"]="Update";
		//					txtMode.Text="Update";
		//					hidBatchNo.Text =dsUpd.Tables[0].Rows[0]["Batch_No"].ToString();
		//					hidBatchID.Text = dsUpd.Tables[0].Rows[0]["Batch_ID"].ToString();
		//					hidTruckID.Text =dsUpd.Tables[0].Rows[0]["Truck_ID"].ToString();
		//					DateTime DTBatchDate = (DateTime)dsUpd.Tables[0].Rows[0]["Batch_Date"];
		//					hidBatchDate.Text = DTBatchDate.ToString("dd/MM/yyyy");
		//					if(dsUpd.Tables[0].Rows[0]["Batch_Type"].ToString().ToUpper()=="W")
		//					{
		//						hidBatchType.Text="Current Location";
		//					}
		//					else if (dsUpd.Tables[0].Rows[0]["Batch_Type"].ToString().ToUpper()=="S")
		//					{
		//						hidBatchType.Text="Delivery";
		//					}
		//					else if (dsUpd.Tables[0].Rows[0]["Batch_Type"].ToString().ToUpper()=="A")
		//					{
		//						hidBatchType.Text="Air";
		//					}
		//					else//L
		//					{
		//						hidBatchType.Text="Linehaul";
		//					}
		//				
		//				}
		//				else
		//				{
		//					if(rdbUpdateExistingBatch.Checked && txtBatchNo.Text.ToString()!="")
		//					{
		//						String sScript = "";
		//						sScript += "<script language=javascript> ";
		//						sScript += "  alert('Batch number : "+txtBatchNo.Text.ToString()+" does not exist. Operation cancelled.')";
		//			
		//						sScript += "</script>";
		//						Response.Write(sScript);
		//					}
		//				}
		//
		//			}
		//
		//		}
		private void setFocusCtrl(string ctrlName)

		{
			string script="<script langauge='javacript'>";
			script+= "document.all['"+ ctrlName +"'].focus();";
			script+= "</script>";
			Page.RegisterStartupScript("setFocus",script);
		
		}

		private void btnConfirmUpdate_Click(object sender, System.EventArgs e)
		{
			string strlocation = Request.Params["usrloc"].ToString();
			int i = BatchManifest.ModifyDep_DTTONull( m_strAppID,m_strEnterpriseID,txtBatchNo.Text.Trim(),strlocation);
			InsertUpdateData();
			Session["Mode"]=null;
		}


		private void txtBatchType_TextChanged(object sender, System.EventArgs e)
		{
			if(txtBatchType.Text == "Air")
				trServiceType.Style["display"] = "block";
			else
				trServiceType.Style["display"] = "none";
		}


	}
}
