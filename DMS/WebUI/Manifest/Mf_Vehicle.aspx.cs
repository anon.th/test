using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for Mf_Vehicle.
	/// </summary>
	public class Mf_Vehicle : BasePage
	{
		protected System.Web.UI.WebControls.Label lblMainheading;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.DataGrid dgVehicle;

		private string m_strAppID = null;
		private string m_strEnterpriseID = null;
		SessionDS m_sdsVehicle = null;
	

		private void Page_Load(object sender, System.EventArgs e)
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings, Page.Session);

			String userCulTure = utility.GetUserCulture();
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();

			if(!IsPostBack)
			{
				DataSet profileDS = SysDataManager1.GetEnterpriseProfile(m_strAppID, m_strEnterpriseID);

				if(profileDS.Tables[0].Rows.Count > 0)
				{
					int currency_decimal = (int) profileDS.Tables[0].Rows[0]["currency_decimal"];
					ViewState["m_format"] = "{0:F" + currency_decimal.ToString() + "}";
				}				
				
				QueryMode();	  
			}
			else
			{
				m_sdsVehicle = (SessionDS)Session["SESSION_DS_Vehicle"];
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		public void QueryMode()
		{
			ViewState["DPOperation"] = Operation.None;
			ViewState["DPMode"] = ScreenMode.Query;
			ViewState["EditMode"] = false;
  	
			btnQuery.Enabled = true;
			btnExecuteQuery.Enabled = true;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);

			m_sdsVehicle = Vehicle.GetEmptyVehicle();  

			m_sdsVehicle.DataSetRecSize = 1;
			AddRow(); 
			Session["SESSION_DS_Vehicle"] = m_sdsVehicle;

			BindDataGrid();

			EditHRow(true);
		}

		
				
		public DataSet  LoadVehicleType(string codeID)
		{
			DataSet dsVehType = new DataSet();
			DataTable dtVehType = new DataTable();
			DataRow dr;

			dtVehType.Columns.Add("VehTypeText", typeof(string));
			dtVehType.Columns.Add("VehTypeValue", typeof(string));
 
			ArrayList systemCodes = Utility.GetCodeValues(m_strAppID, utility.GetUserCulture(), codeID, CodeValueType.StringValue);
			 
			if(systemCodes != null)
			{
				dr = dtVehType.NewRow();
				dr[1] = "";
				dr[0] = "";
				dtVehType.Rows.Add(dr); 

				foreach(SystemCode sysCode in systemCodes)
				{
					dr = dtVehType.NewRow();
					dr[1] = sysCode.Text;
					dr[0] = sysCode.StringValue;
					dtVehType.Rows.Add(dr); 			
				}
			}
			dsVehType.Tables.Add(dtVehType);
			int cnt = dsVehType.Tables [0].Rows.Count; 
			return dsVehType;
		}

	
		public DataSet  LoadVehicleColor(string codeID)
		{
			DataSet dsVehColor = new DataSet();
			DataTable dtVehColor = new DataTable();
			DataRow dr;

			dtVehColor.Columns.Add("VehColorText", typeof(string));
			dtVehColor.Columns.Add("VehColorValue", typeof(string));
 
			ArrayList systemCodes = Utility.GetCodeValues(m_strAppID, utility.GetUserCulture(), codeID, CodeValueType.StringValue);
			 
			if(systemCodes != null)
			{
				foreach(SystemCode sysCode in systemCodes)
				{
					dr = dtVehColor.NewRow();
					dr[1] = sysCode.Text;
					dr[0] = sysCode.StringValue;
					dtVehColor.Rows.Add(dr); 			
				}
			}
			dsVehColor.Tables.Add(dtVehColor);
			int cnt = dsVehColor.Tables [0].Rows.Count; 
			return dsVehColor;
		}

	
		private void AddRow()
		{
			Vehicle.AddNewRowVehicleCodeDS(ref m_sdsVehicle);
		}

		
		private void EditHRow(bool bItemIndex)
		{
			foreach (DataGridItem item in dgVehicle.Items)
			{ 
				if (bItemIndex) 
					dgVehicle.EditItemIndex = item.ItemIndex;
				else 
					dgVehicle.EditItemIndex = -1;
			}
			dgVehicle.DataBind();
		}

		
		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			if (btnExecuteQuery.Enabled == true)
			{
				btnExecuteQuery.Enabled = false;
			}
			if((int)ViewState["DPMode"] != (int)ScreenMode.Insert || m_sdsVehicle.ds.Tables[0].Rows.Count >= dgVehicle.PageSize)
			{
				m_sdsVehicle = Vehicle.GetEmptyVehicle();
				AddRow();				
			}
			else
			{
				AddRow();
			}
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,false,m_moduleAccessRights);
			EditHRow(false);
			ViewState["DPMode"] = ScreenMode.Insert;
			ViewState["DPOperation"] = Operation.Insert;
			dgVehicle.Columns[0].Visible=true;
			dgVehicle.Columns[1].Visible=true;
			dgVehicle.EditItemIndex = m_sdsVehicle.ds.Tables[0].Rows.Count - 1;
			dgVehicle.CurrentPageIndex = 0;
			BindDataGrid();
			getPageControls(Page);
		}

		
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			dgVehicle.CurrentPageIndex = 0;
			QueryMode();
		}

		
		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			btnExecuteQuery.Enabled = false;
			ViewState["DPMode"]=ScreenMode.ExecuteQuery; 

			//Since Query mode is always one row in edit mode, the row index is set to zero
			int rowIndex = 0; 
			GetChangedRow(dgVehicle.Items[0], rowIndex);
			Session["QUERY_DS"] = m_sdsVehicle.ds;
			DataRow dr = m_sdsVehicle.ds.Tables[0].Rows[0];

			RetreiveSelectedPage();
			if(m_sdsVehicle.ds.Tables[0].Rows.Count==0)
			{
				lblErrorMessage.Text = "No Matching records found..";
				dgVehicle.EditItemIndex = -1; 
				return;
			}
			//set all records to non-edit mode after execute query
			EditHRow(false);
			dgVehicle.Columns[0].Visible = true;
			dgVehicle.Columns[1].Visible = true;		
		}
		
		
		private void RetreiveSelectedPage()
		{
			int iStartRow = dgVehicle.CurrentPageIndex * dgVehicle.PageSize;

			DataSet dsQuery = (DataSet)Session["QUERY_DS"];

			m_sdsVehicle = Vehicle.GetVehicleCodeDS(utility.GetAppID(), utility.GetEnterpriseID(), iStartRow, dgVehicle.PageSize, dsQuery);

			int iPageCnt = Convert.ToInt32((m_sdsVehicle.QueryResultMaxSize - 1))/dgVehicle.PageSize;

			if(iPageCnt < dgVehicle.CurrentPageIndex)
			{
				dgVehicle.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			
			dgVehicle.SelectedIndex = -1;
			dgVehicle.EditItemIndex = -1;

			BindDataGrid();
			Session["SESSION_DS_Vehicle"] = m_sdsVehicle;
		}

		
		private void GetChangedRowOfUpdate(DataGridItem item, int rowIndex)
		{
			msTextBox txtTruckId = (msTextBox)item.FindControl("txtTruckId");
			TextBox txtDescription = (TextBox)item.FindControl("txtDescription");
			TextBox txtLicensePlate = (TextBox)item.FindControl("txtLicensePlate");			
			DropDownList ddlVehType = (DropDownList)item.FindControl("ddlVehType");
			DropDownList ddlYear = (DropDownList)item.FindControl("ddlYear");
			TextBox txtBrand = (TextBox)item.FindControl("txtBrand");
			TextBox txtModel = (TextBox)item.FindControl("txtModel");
			DropDownList ddlColor = (DropDownList)item.FindControl("ddlColor");
			TextBox txtOwner = (TextBox)item.FindControl("txtOwner");
			TextBox txtTypeOfLease = (TextBox)item.FindControl("txtTypeOfLease");
			msTextBox txtLeaseStartDate = (msTextBox)item.FindControl("txtLeaseStartDate");
			msTextBox txtLeaseEndDate = (msTextBox)item.FindControl("txtLeaseEndDate");
			msTextBox txtMonthlyCost = (msTextBox)item.FindControl("txtMonthlyCost");
			DropDownList ddlBaseLocation = (DropDownList)item.FindControl("ddlBaseLocation");
			msTextBox txtTareWeight = (msTextBox)item.FindControl("txtTareWeight");
			msTextBox txtGrossWeight = (msTextBox)item.FindControl("txtGrossWeight");
			msTextBox txtMaxCBM = (msTextBox)item.FindControl("txtMaxCBM");
			TextBox txtRemark = (TextBox)item.FindControl("txtRemark");
			msTextBox txtRegistrationDate = (msTextBox)item.FindControl("txtRegistrationDate");
			msTextBox txtInitInServiceDate = (msTextBox)item.FindControl("txtInitInServiceDate");
			msTextBox txtDisposalDate = (msTextBox)item.FindControl("txtDisposalDate");
			msTextBox txtDateOutOfService = (msTextBox)item.FindControl("txtDateOutOfService");
			msTextBox txtDateInService = (msTextBox)item.FindControl("txtDateInService");			
					
			DataRow dr = m_sdsVehicle.ds.Tables[0].Rows[rowIndex];

			dr["truckid"] = txtTruckId.Text.ToString();
			dr["description"] = txtDescription.Text.ToString();
			dr["licenseplate"] = txtLicensePlate.Text.ToString();
			dr["type"] = ddlVehType.SelectedItem.Value;
			dr["year"] = ddlYear.SelectedItem.Value;
			dr["brand"] = txtBrand.Text.ToString();
			dr["model"] = txtModel.Text.ToString();
			dr["color"] = ddlColor.SelectedItem.Value;
			dr["owner"] = txtOwner.Text.ToString();
			dr["type_of_lease"] = txtTypeOfLease.Text.ToString();

			if (txtMonthlyCost.Text != "")
                dr["monthly_cost"] = decimal.Parse(txtMonthlyCost.Text);
			else
				dr["monthly_cost"] = decimal.Parse("0");

			dr["base_location"] = ddlBaseLocation.SelectedItem.Value;

			if (txtTareWeight.Text != "")
                dr["tare_weight"] = Convert.ToInt32(txtTareWeight.Text);
			else
				dr["tare_weight"] = Convert.ToInt32("0");

			if (txtGrossWeight.Text != "")
                dr["gross_weight"] = Convert.ToInt32(txtGrossWeight.Text);
			else
				dr["gross_weight"] = Convert.ToInt32("0");

			if (txtMaxCBM.Text != "")
                dr["max_cbm"] = Convert.ToInt32(txtMaxCBM.Text);
			else
				dr["max_cbm"] = Convert.ToInt32("0");

			dr["remark"] = txtRemark.Text.ToString();

			if (txtLeaseStartDate.Text != "")
				dr["lease_start_date"] = DateTime.ParseExact(txtLeaseStartDate.Text, "dd/MM/yyyy", null);
			else
				dr["lease_start_date"] = System.DBNull.Value;

			if (txtLeaseEndDate.Text != "")
				dr["lease_end_date"] = DateTime.ParseExact(txtLeaseEndDate.Text, "dd/MM/yyyy", null);
			else
				dr["lease_end_date"] = System.DBNull.Value;

			if (txtRegistrationDate.Text != "")
				dr["registration_date"] = DateTime.ParseExact(txtRegistrationDate.Text, "dd/MM/yyyy", null);
			else
				dr["registration_date"] = System.DBNull.Value;

			if (txtInitInServiceDate.Text != "")
				dr["initial_in_service_date"] = DateTime.ParseExact(txtInitInServiceDate.Text, "dd/MM/yyyy", null);
			else
				dr["initial_in_service_date"] = System.DBNull.Value;

			if (txtDisposalDate.Text != "")
				dr["disposal_date"] = DateTime.ParseExact(txtDisposalDate.Text, "dd/MM/yyyy", null);
			else
				dr["disposal_date"] = System.DBNull.Value;

			if (txtDateOutOfService.Text != "")
				dr["date_out_of_service"] = DateTime.ParseExact(txtDateOutOfService.Text, "dd/MM/yyyy", null);
			else
				dr["date_out_of_service"] = System.DBNull.Value;

			if (txtDateInService.Text != "")
				dr["date_in_service"] = DateTime.ParseExact(txtDateInService.Text, "dd/MM/yyyy", null);
			else
				dr["date_in_service"] = System.DBNull.Value;

			Session["SESSION_DS_Vehicle"] = m_sdsVehicle;
		}


		private void GetChangedRow(DataGridItem item, int rowIndex)
		{
			msTextBox txtTruckId = (msTextBox)item.FindControl("txtTruckId");
			TextBox txtDescription = (TextBox)item.FindControl("txtDescription");
			TextBox txtLicensePlate = (TextBox)item.FindControl("txtLicensePlate");			
			DropDownList ddlVehType = (DropDownList)item.FindControl("ddlVehType");
			DropDownList ddlYear = (DropDownList)item.FindControl("ddlYear");
			TextBox txtBrand = (TextBox)item.FindControl("txtBrand");
			TextBox txtModel = (TextBox)item.FindControl("txtModel");
			DropDownList ddlColor = (DropDownList)item.FindControl("ddlColor");
			TextBox txtOwner = (TextBox)item.FindControl("txtOwner");
			TextBox txtTypeOfLease = (TextBox)item.FindControl("txtTypeOfLease");
			msTextBox txtLeaseStartDate = (msTextBox)item.FindControl("txtLeaseStartDate");
			msTextBox txtLeaseEndDate = (msTextBox)item.FindControl("txtLeaseEndDate");
			msTextBox txtMonthlyCost = (msTextBox)item.FindControl("txtMonthlyCost");
			DropDownList ddlBaseLocation = (DropDownList)item.FindControl("ddlBaseLocation");
			msTextBox txtTareWeight = (msTextBox)item.FindControl("txtTareWeight");
			msTextBox txtGrossWeight = (msTextBox)item.FindControl("txtGrossWeight");
			msTextBox txtMaxCBM = (msTextBox)item.FindControl("txtMaxCBM");
			TextBox txtRemark = (TextBox)item.FindControl("txtRemark");
			msTextBox txtRegistrationDate = (msTextBox)item.FindControl("txtRegistrationDate");
			msTextBox txtInitInServiceDate = (msTextBox)item.FindControl("txtInitInServiceDate");
			msTextBox txtDisposalDate = (msTextBox)item.FindControl("txtDisposalDate");
			msTextBox txtDateOutOfService = (msTextBox)item.FindControl("txtDateOutOfService");
			msTextBox txtDateInService = (msTextBox)item.FindControl("txtDateInService");			
					
			DataRow dr = m_sdsVehicle.ds.Tables[0].Rows[rowIndex];

			dr["truckid"] = txtTruckId.Text.ToString();
			dr["description"] = txtDescription.Text.ToString();
			dr["licenseplate"] = txtLicensePlate.Text.ToString();
			dr["type"] = ddlVehType.SelectedItem.Value;
			dr["year"] = ddlYear.SelectedItem.Value;
			dr["brand"] = txtBrand.Text.ToString();
			dr["model"] = txtModel.Text.ToString();
			dr["color"] = ddlColor.SelectedItem.Value;
			dr["owner"] = txtOwner.Text.ToString();
			dr["type_of_lease"] = txtTypeOfLease.Text.ToString();

			if (txtMonthlyCost.Text != "")
                dr["monthly_cost"] = decimal.Parse(txtMonthlyCost.Text);
			else
				dr["monthly_cost"] = decimal.Parse("0");

			dr["base_location"] = ddlBaseLocation.SelectedItem.Value;

			if (txtTareWeight.Text != "")
                dr["tare_weight"] = Convert.ToInt32(txtTareWeight.Text);
			else
				dr["tare_weight"] = Convert.ToInt32("0");

			if (txtGrossWeight.Text != "")
                dr["gross_weight"] = Convert.ToInt32(txtGrossWeight.Text);
			else
				dr["gross_weight"] = Convert.ToInt32("0");

			if (txtMaxCBM.Text != "")
                dr["max_cbm"] = Convert.ToInt32(txtMaxCBM.Text);
			else
				dr["max_cbm"] = Convert.ToInt32("0");

			dr["remark"] = txtRemark.Text.ToString();

			if (txtLeaseStartDate.Text != "")
				dr["lease_start_date"] = DateTime.ParseExact(txtLeaseStartDate.Text, "dd/MM/yyyy", null);
			else
				dr["lease_start_date"] = System.DBNull.Value;

			if (txtLeaseEndDate.Text != "")
				dr["lease_end_date"] = DateTime.ParseExact(txtLeaseEndDate.Text, "dd/MM/yyyy", null);
			else
				dr["lease_end_date"] = System.DBNull.Value;

			if (txtRegistrationDate.Text != "")
				dr["registration_date"] = DateTime.ParseExact(txtRegistrationDate.Text, "dd/MM/yyyy", null);
			else
				dr["registration_date"] = System.DBNull.Value;

			if (txtInitInServiceDate.Text != "")
				dr["initial_in_service_date"] = DateTime.ParseExact(txtInitInServiceDate.Text, "dd/MM/yyyy", null);
			else
				dr["initial_in_service_date"] = System.DBNull.Value;

			if (txtDisposalDate.Text != "")
				dr["disposal_date"] = DateTime.ParseExact(txtDisposalDate.Text, "dd/MM/yyyy", null);
			else
				dr["disposal_date"] = System.DBNull.Value;

			if (txtDateOutOfService.Text != "")
				dr["date_out_of_service"] = DateTime.ParseExact(txtDateOutOfService.Text, "dd/MM/yyyy", null);
			else
				dr["date_out_of_service"] = System.DBNull.Value;

			if (txtDateInService.Text != "")
				dr["date_in_service"] = DateTime.ParseExact(txtDateInService.Text, "dd/MM/yyyy", null);
			else
				dr["date_in_service"] = System.DBNull.Value;

			Session["SESSION_DS_Vehicle"] = m_sdsVehicle;
		}

					
		public void chkDdlVehType(object sender, System.EventArgs e)
		{
			if ((int)ViewState["DPOperation"] != (int)Operation.None)
			{
				DropDownList ddlVehType = null;
				RequiredFieldValidator rvVehType = null;

				if((int)ViewState["DPOperation"] == (int)Operation.Insert)
				{
					ddlVehType = (DropDownList)dgVehicle.Items[0].Cells[4].FindControl("ddlVehType");
					rvVehType	= (RequiredFieldValidator)dgVehicle.Items[0].FindControl("rvVehType");
				}
				else
				{
					ddlVehType = (DropDownList)dgVehicle.Items[(int)ViewState["EditRow"]].Cells[4].FindControl("ddlVehType");
					rvVehType	= (RequiredFieldValidator)dgVehicle.Items[(int)ViewState["EditRow"]].FindControl("rvVehType");
				}
			}
		}
	

		public void chkDdlColor(object sender, System.EventArgs e)
		{
			if ((int)ViewState["DPOperation"] != (int)Operation.None)
			{
				DropDownList ddlColor = null;
				RequiredFieldValidator rvColor = null;

				if((int)ViewState["DPOperation"] == (int)Operation.Insert)
				{
					ddlColor = (DropDownList)dgVehicle.Items[0].Cells[8].FindControl("ddlColor");
					rvColor	= (RequiredFieldValidator)dgVehicle.Items[0].FindControl("rvColor");
				}
				else
				{
					ddlColor = (DropDownList)dgVehicle.Items[(int)ViewState["EditRow"]].Cells[8].FindControl("ddlColor");
					rvColor	= (RequiredFieldValidator)dgVehicle.Items[(int)ViewState["EditRow"]].FindControl("rvColor");
				}
			}
		}
	
		
		public void BindDataGrid()
		{
			dgVehicle.VirtualItemCount = System.Convert.ToInt32(m_sdsVehicle.QueryResultMaxSize);
			dgVehicle.DataSource = m_sdsVehicle.ds;
			dgVehicle.DataBind(); 
			Session["SESSION_DS_Vehicle"] = m_sdsVehicle;
		}

		
		protected void dgVehicle_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";

			if (!btnExecuteQuery.Enabled)
			{
				int iRowsAffected = 0;
				int rowIndex = e.Item.ItemIndex;

				GetChangedRowOfUpdate(e.Item, e.Item.ItemIndex);

				DataRow drSelected = m_sdsVehicle.ds.Tables[0].Rows[e.Item.ItemIndex];

				//== initial_in_service_date
				TextBox txtInitInServiceDate = (TextBox)e.Item.FindControl("txtInitInServiceDate");
				if (txtInitInServiceDate != null && drSelected["initial_in_service_date"] != System.DBNull.Value) 
					txtInitInServiceDate.Text = DataBinder.Eval(e.Item.DataItem,"initial_in_service_date","{0:dd/MM/yyyy}").ToString();

				//== disposal_date
				TextBox txtDisposalDate = (TextBox)e.Item.FindControl("txtDisposalDate");
				if (txtDisposalDate != null && drSelected["disposal_date"] != System.DBNull.Value) 
					txtDisposalDate.Text = DataBinder.Eval(e.Item.DataItem,"disposal_date","{0:dd/MM/yyyy}").ToString();

				//== date_out_of_service
				TextBox txtDateOutOfService = (TextBox)e.Item.FindControl("txtDateOutOfService");
				if (txtDateOutOfService != null && drSelected["date_out_of_service"] != System.DBNull.Value) 
					txtDateOutOfService.Text = DataBinder.Eval(e.Item.DataItem,"date_out_of_service","{0:dd/MM/yyyy}").ToString();

				//== date_in_service
				TextBox txtDateInService = (TextBox)e.Item.FindControl("txtDateInService");
				if (txtDateInService != null && drSelected["date_in_service"] != System.DBNull.Value) 
					txtDateInService.Text = DataBinder.Eval(e.Item.DataItem,"date_in_service","{0:dd/MM/yyyy}").ToString();


				//// Check Rule DateTime
				///1) Disposal Date > Initial In Service Date
				if (IsRuleDateTime(txtInitInServiceDate.Text, txtDisposalDate.Text) == false)
				{
					lblErrorMessage.Text = "Disposal Date must greater than Initial In Service Date";
					return;
				}
				///2) Out of Service Date > Initial In Service Date
				if (IsRuleDateTime(txtInitInServiceDate.Text, txtDateOutOfService.Text) == false)
				{
					lblErrorMessage.Text = "Out of Service Date must greater than Initial In Service Date";
					return;
				}
				///3) Disposal Date > Out of Service Date
				if (IsRuleDateTime(txtDateOutOfService.Text, txtDisposalDate.Text) == false)
				{
					lblErrorMessage.Text = "Disposal Date must greater than Out of Service Date";
					return;
				}
				///4) In Service Date > Out of Service Date
				if (IsRuleDateTime(txtDateOutOfService.Text, txtDateInService.Text) == false)
				{
					lblErrorMessage.Text ="Out of Service Date must greater than In Service Date";
					return;
				}
				///5) In Service Date > Initial In Service Date
				if (IsRuleDateTime(txtInitInServiceDate.Text,txtDateInService.Text) == false)
				{
					lblErrorMessage.Text ="Initial In Service Date must greater than In Service Date";
					return;
				}
				///6) Disposal Date >= In Service Date
				if (IsRuleDateTime(txtDateInService.Text, txtDisposalDate.Text) == false)
				{
					lblErrorMessage.Text ="Disposal Date must greater than In Service Date";
					return;
				}

				/// Process insert & update
				{
					int iOperation = (int)ViewState["DPOperation"];

					try
					{	// INSERT
						if (iOperation == (int)Operation.Insert)
						{
							iRowsAffected = Vehicle.InsertVehicle(m_sdsVehicle.ds, rowIndex, m_strAppID, m_strEnterpriseID);

							ArrayList paramValues = new ArrayList();
							paramValues.Add("" + iRowsAffected);	
							lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ROWS_INS", utility.GetUserCulture(), paramValues);
						}
						else
						{	// UPDATE
							DataSet dsChangedRow = m_sdsVehicle.ds.GetChanges();
							iRowsAffected = Vehicle.UpdateVehicle(dsChangedRow, m_strAppID, m_strEnterpriseID);

							ArrayList paramValues = new ArrayList();
							paramValues.Add("" + iRowsAffected);
							lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ROWS_UPD", utility.GetUserCulture(), paramValues);
								
							m_sdsVehicle.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
						}
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;

						if(strMsg.IndexOf("PRIMARY KEY") != -1)
						{
							lblErrorMessage.Text = "Duplication of Truck ID is Found"; 
						}
//						else if(strMsg.IndexOf("duplicate key") != -1 )
//						{
//							lblErrorMessage.Text = "Duplicate Path Code are not allowed. Please try again.";							
//						}
						else
						{
							lblErrorMessage.Text = strMsg;
						}

						Logger.LogTraceError("Mf_Vehicle.aspx.cs", "dgVehicle_Update", "RBAC003", appException.Message.ToString());
						return;
					}

					if (iRowsAffected == 0)
					{
						return;
					}
					Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
					ViewState["DPOperation"] = Operation.None;
					dgVehicle.EditItemIndex = -1;
					BindDataGrid();
					Logger.LogDebugInfo("Vehicle","dgVehicle_Update","INF004","updating data grid...");			
				}
			}
		}

		
		protected void dgVehicle_Edit(object sender, DataGridCommandEventArgs e)
		{
			ViewState["EditRow"] = e.Item.ItemIndex;

			lblErrorMessage.Text="";

			LoadVehicleType("vehicle_type");

			LoadVehicleColor("vehicle_color");

			if((int)ViewState["DPMode"] == (int)ScreenMode.Insert && (int) ViewState["DPOperation"] == (int)Operation.Insert && dgVehicle.EditItemIndex > 0)
			{
				m_sdsVehicle.ds.Tables[0].Rows.RemoveAt(dgVehicle.EditItemIndex);
				dgVehicle.CurrentPageIndex = 0;
			}

			dgVehicle.EditItemIndex = e.Item.ItemIndex;
			ViewState["DPOperation"] = Operation.Update;
			BindDataGrid();
			getPageControls(Page);
		}

		
		protected void dgVehicle_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			DataSet dsDestCode = null;
			DropDownList ddlBaseLocation = (DropDownList)e.Item.FindControl("ddlBaseLocation");
			DropDownList ddlVehType = (DropDownList)e.Item.FindControl("ddlVehType");
			DropDownList ddlYear = (DropDownList)e.Item.FindControl("ddlYear");
			DropDownList ddlColor = (DropDownList)e.Item.FindControl("ddlColor");
			string strType = null;
			string strColor = null;

			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsVehicle.ds.Tables[0].Rows[e.Item.ItemIndex];
			
			if (ddlBaseLocation != null)
			{
				if (drSelected["base_location"] != System.DBNull.Value)
				{
					string strBaseLocation = (string)drSelected["base_location"];
					ddlBaseLocation.SelectedIndex = ddlBaseLocation.Items.IndexOf(ddlBaseLocation.Items.FindByValue((string)drSelected["base_location"]));
				}
			}

			if (ddlVehType != null)
			{
				if (drSelected["type"] != System.DBNull.Value) 
				{
					strType = (string)drSelected["type"];
					ddlVehType.SelectedIndex = ddlVehType.Items.IndexOf(ddlVehType.Items.FindByValue((string)drSelected["type"]));
				}
			}

			if (ddlYear != null)
			{
				if (drSelected["year"] != System.DBNull.Value)
				{
					ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByValue((string)drSelected["year"]));
				}
			}

			if (ddlColor != null)
			{
				if (drSelected["color"] != System.DBNull.Value) 
				{
					strColor = (string)drSelected["color"];
					ddlColor.SelectedIndex = ddlColor.Items.IndexOf(ddlColor.Items.FindByValue((string)drSelected["color"]));
				}
			}

			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}
			
			//== TruckId
			if ((int)ViewState["DPOperation"] != (int)Operation.Insert && (!btnExecuteQuery.Enabled))
			{
				msTextBox txtTruckId = (msTextBox)e.Item.FindControl("txtTruckId");
				
				if(txtTruckId != null ) 
				{
					txtTruckId.Enabled = false;
					RequiredFieldValidator rvTruckId = (RequiredFieldValidator)e.Item.FindControl("rvTruckId");
				}
			}

			//== Description
			if ((int)ViewState["DPMode"] == (int)ScreenMode.Insert && (int)ViewState["DPOperation"] == (int)Operation.Insert)
			{
				e.Item.Cells[1].Enabled=false;

				TextBox txtDescription = (TextBox)e.Item.FindControl("txtDescription");
				if(txtDescription != null ) 
				{
					RequiredFieldValidator rvDescription = (RequiredFieldValidator)e.Item.FindControl("rvDescription");
				}
			}

			//== LicensePlate
			Label lblLicensePlate = (Label)e.Item.FindControl("lblLicensePlate");
			if (lblLicensePlate != null && drSelected["LicensePlate"] != System.DBNull.Value)
				lblLicensePlate.Text = String.Format((String)ViewState["m_format"], drSelected["LicensePlate"]);

			TextBox txtLicensePlate = (TextBox)e.Item.FindControl("txtLicensePlate");
			if (txtLicensePlate != null && drSelected["LicensePlate"] != System.DBNull.Value) 
				txtLicensePlate.Text = String.Format((String)ViewState["m_format"], drSelected["LicensePlate"]);

			//== Type
			Label lblVehType = (Label)e.Item.FindControl("lblVehType");
			if (lblVehType != null && drSelected["type"] != System.DBNull.Value) 
				lblVehType.Text = String.Format((String)ViewState["m_format"], drSelected["type"]);

			string codeID = "vehicle_type";

			DataSet dsVehicleType = LoadVehicleType(codeID);
			if (ddlVehType != null)
			{
				int cnt = dsVehicleType.Tables[0].Rows.Count;  
				ddlVehType.DataSource = dsVehicleType;
				ddlVehType.DataTextField = "VehTypeValue"; 
				ddlVehType.DataValueField = "VehTypeText";
				ddlVehType.DataBind();

				ddlVehType.SelectedIndex = ddlVehType.Items.IndexOf(ddlVehType.Items.FindByValue(strType)); 
			}	

			//== Year
			Label lblYear = (Label)e.Item.FindControl("lblYear");
			if (lblYear != null && drSelected["year"] != System.DBNull.Value) 
				lblYear.Text = String.Format((String)ViewState["m_format"], drSelected["year"]);

			if (ddlYear != null)
			{		
				ListItem lsYearEmp = new ListItem();
				lsYearEmp.Text = "";
				lsYearEmp.Value = "";
				ddlYear.Items.Add(lsYearEmp);

				for (int i=2001; i<=2020; i++)
				{
					ListItem lsYear = new ListItem();
					lsYear.Text = i.ToString();
					lsYear.Value = i.ToString();
					ddlYear.Items.Add(lsYear);
				}

				if (drSelected["year"] != System.DBNull.Value)
					ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByValue((string)drSelected["year"]));
			}

			msTextBox txtYear = (msTextBox)e.Item.FindControl("txtYear");
			if (txtYear != null && drSelected["year"] != System.DBNull.Value) 
				txtYear.Text = String.Format((String)ViewState["m_format"], drSelected["year"]);

			//== Brand
			Label lblBrand = (Label)e.Item.FindControl("lblBrand");
			if (lblBrand != null && drSelected["brand"] != System.DBNull.Value) 
				lblBrand.Text = String.Format((String)ViewState["m_format"], drSelected["brand"]);

			TextBox txtBrand = (TextBox)e.Item.FindControl("txtBrand");
			if (txtBrand != null && drSelected["brand"] != System.DBNull.Value) 
				txtBrand.Text = String.Format((String)ViewState["m_format"], drSelected["brand"]);

			//== Model
			Label lblModel = (Label)e.Item.FindControl("lblModel");
			if (lblModel != null && drSelected["model"] != System.DBNull.Value) 
				lblModel.Text = String.Format((String)ViewState["m_format"], drSelected["model"]);

			TextBox txtModel = (TextBox)e.Item.FindControl("txtModel");
			if (txtModel != null && drSelected["model"] != System.DBNull.Value) 
				txtModel.Text = String.Format((String)ViewState["m_format"], drSelected["model"]);

			//== Color
			Label lblColor = (Label)e.Item.FindControl("lblColor");
			if (lblColor != null && drSelected["color"] != System.DBNull.Value) 
				lblColor.Text = String.Format((String)ViewState["m_format"], drSelected["Color"]);

			codeID = "vehicle_color";

			DataSet dsVehicleColor = LoadVehicleColor(codeID);
			if (ddlColor != null)
			{
				int cnt = dsVehicleColor.Tables[0].Rows.Count;  
				ddlColor.DataSource = dsVehicleColor;
				ddlColor.DataTextField = "VehColorValue"; 
				ddlColor.DataValueField = "VehColorText";
				ddlColor.DataBind();

				ddlColor.SelectedIndex = ddlColor.Items.IndexOf(ddlColor.Items.FindByValue(strColor)); 
			}	

			//== Owner
			Label lblOwner = (Label)e.Item.FindControl("lblOwner");
			if (lblOwner != null && drSelected["owner"] != System.DBNull.Value) 
				lblOwner.Text = String.Format((String)ViewState["m_format"], drSelected["owner"]);

			TextBox txtOwner = (TextBox)e.Item.FindControl("txtOwner");
			if (txtOwner != null && drSelected["owner"] != System.DBNull.Value) 
				txtOwner.Text = String.Format((String)ViewState["m_format"], drSelected["owner"]);

			//== TypeOfLease
			Label lblTypeOfLease = (Label)e.Item.FindControl("lblTypeOfLease");
			if (lblTypeOfLease != null && drSelected["type_of_lease"] != System.DBNull.Value) 
				lblTypeOfLease.Text = String.Format((String)ViewState["m_format"], drSelected["type_of_lease"]);

			TextBox txtTypeOfLease = (TextBox)e.Item.FindControl("txtTypeOfLease");
			if (txtTypeOfLease != null && drSelected["type_of_lease"] != System.DBNull.Value) 
				txtTypeOfLease.Text = String.Format((String)ViewState["m_format"], drSelected["type_of_lease"]);

			//== MonthlyCost
			Label lblMonthlyCost = (Label)e.Item.FindControl("lblMonthlyCost");
			if (lblMonthlyCost != null && drSelected["monthly_cost"] != System.DBNull.Value) 
				lblMonthlyCost.Text = DataBinder.Eval(e.Item.DataItem,"monthly_cost","{0:#,##0.00}").ToString();	//String.Format((String)ViewState["m_format"], drSelected["monthly_cost"]);

			msTextBox txtMonthlyCost = (msTextBox)e.Item.FindControl("txtMonthlyCost");
			if (txtMonthlyCost != null && drSelected["monthly_cost"] != System.DBNull.Value) 
				txtMonthlyCost.Text = DataBinder.Eval(e.Item.DataItem,"monthly_cost","{0:###0.00}").ToString();	//String.Format((String)ViewState["m_format"], drSelected["monthly_cost"]);

			//== BaseLocation
			dsDestCode = Vehicle.GetDistributionCenter(m_strAppID, m_strEnterpriseID);
			if (ddlBaseLocation != null)
			{
				int cnt = dsDestCode.Tables[0].Rows.Count;  
				ddlBaseLocation.DataSource = dsDestCode.Tables[0];
				ddlBaseLocation.DataTextField = dsDestCode.Tables[0].Columns["origin_code"].ColumnName.ToString(); 
				ddlBaseLocation.DataValueField = dsDestCode.Tables[0].Columns["origin_code"].ColumnName.ToString();
				ddlBaseLocation.DataBind();
				
				if (drSelected["base_location"] != System.DBNull.Value)
					ddlBaseLocation.SelectedIndex = ddlBaseLocation.Items.IndexOf(ddlBaseLocation.Items.FindByValue((string)drSelected["base_location"]));
			}

			//== TareWeight
			Label lblTareWeight = (Label)e.Item.FindControl("lblTareWeight");
			if (lblTareWeight != null && drSelected["tare_weight"] != System.DBNull.Value) 
				lblTareWeight.Text = DataBinder.Eval(e.Item.DataItem,"tare_weight","{0:#,##0}").ToString();

			msTextBox txtTareWeight = (msTextBox)e.Item.FindControl("txtTareWeight");
			if (txtTareWeight != null && drSelected["tare_weight"] != System.DBNull.Value) 
				txtTareWeight.Text = DataBinder.Eval(e.Item.DataItem,"tare_weight","{0:###0}").ToString();

			//== GrossWeight
			Label lblGrossWeight = (Label)e.Item.FindControl("lblGrossWeight");
			if (lblGrossWeight != null && drSelected["gross_weight"] != System.DBNull.Value) 
				lblGrossWeight.Text = DataBinder.Eval(e.Item.DataItem,"gross_weight","{0:#,##0}").ToString();

			msTextBox txtGrossWeight = (msTextBox)e.Item.FindControl("txtGrossWeight");
			if (txtGrossWeight != null && drSelected["gross_weight"] != System.DBNull.Value) 
				txtGrossWeight.Text = DataBinder.Eval(e.Item.DataItem,"gross_weight","{0:###0}").ToString();

			//== MaxCBM
			Label lblMaxCBM = (Label)e.Item.FindControl("lblMaxCBM");
			if (lblMaxCBM != null && drSelected["max_cbm"] != System.DBNull.Value) 
				lblMaxCBM.Text = DataBinder.Eval(e.Item.DataItem,"max_cbm","{0:#,##0}").ToString();

			msTextBox txtMaxCBM = (msTextBox)e.Item.FindControl("txtMaxCBM");
			if (txtMaxCBM != null && drSelected["max_cbm"] != System.DBNull.Value) 
				txtMaxCBM.Text = DataBinder.Eval(e.Item.DataItem,"max_cbm","{0:###0}").ToString();

			//== Remark
			Label lblRemark = (Label)e.Item.FindControl("lblRemark");
			if (lblRemark != null && drSelected["remark"] != System.DBNull.Value) 
				lblRemark.Text = String.Format((String)ViewState["m_format"], drSelected["remark"]);

			TextBox txtRemark = (TextBox)e.Item.FindControl("txtRemark");
			if (txtRemark != null && drSelected["remark"] != System.DBNull.Value) 
				txtRemark.Text = String.Format((String)ViewState["m_format"], drSelected["remark"]);
			
			//== LeaseStartDate
			Label lblLeaseStartDate = (Label)e.Item.FindControl("lblLeaseStartDate");
			if (lblLeaseStartDate != null && drSelected["lease_start_date"] != System.DBNull.Value) 
				lblLeaseStartDate.Text = DataBinder.Eval(e.Item.DataItem,"lease_start_date","{0:dd/MM/yyyy}").ToString();

			TextBox txtLeaseStartDate = (TextBox)e.Item.FindControl("txtLeaseStartDate");
			if (txtLeaseStartDate != null && drSelected["lease_start_date"] != System.DBNull.Value) 
				txtLeaseStartDate.Text = DataBinder.Eval(e.Item.DataItem,"lease_start_date","{0:dd/MM/yyyy}").ToString();

			//== LeaseEndDate
			Label lblLeaseEndDate = (Label)e.Item.FindControl("lblLeaseEndDate");
			if (lblLeaseEndDate != null && drSelected["lease_end_date"] != System.DBNull.Value) 
				lblLeaseEndDate.Text = DataBinder.Eval(e.Item.DataItem,"lease_end_date","{0:dd/MM/yyyy}").ToString();

			TextBox txtLeaseEndDate = (TextBox)e.Item.FindControl("txtLeaseEndDate");
			if (txtLeaseEndDate != null && drSelected["lease_end_date"] != System.DBNull.Value) 
				txtLeaseEndDate.Text = DataBinder.Eval(e.Item.DataItem,"lease_end_date","{0:dd/MM/yyyy}").ToString();

			//== RegistrationDate
			Label lblRegistrationDate = (Label)e.Item.FindControl("lblRegistrationDate");
			if (lblRegistrationDate != null && drSelected["registration_date"] != System.DBNull.Value) 
				lblRegistrationDate.Text = DataBinder.Eval(e.Item.DataItem,"registration_date","{0:dd/MM/yyyy}").ToString();

			TextBox txtRegistrationDate = (TextBox)e.Item.FindControl("txtRegistrationDate");
			if (txtRegistrationDate != null && drSelected["registration_date"] != System.DBNull.Value) 
				txtRegistrationDate.Text = DataBinder.Eval(e.Item.DataItem,"registration_date","{0:dd/MM/yyyy}").ToString();

			//== initial_in_service_date
			Label lblInitInServiceDate = (Label)e.Item.FindControl("lblInitInServiceDate");
			if (lblInitInServiceDate != null && drSelected["initial_in_service_date"] != System.DBNull.Value) 
				lblInitInServiceDate.Text = DataBinder.Eval(e.Item.DataItem,"initial_in_service_date","{0:dd/MM/yyyy}").ToString();

			TextBox txtInitInServiceDate = (TextBox)e.Item.FindControl("txtInitInServiceDate");
			if (txtInitInServiceDate != null && drSelected["initial_in_service_date"] != System.DBNull.Value) 
				txtInitInServiceDate.Text = DataBinder.Eval(e.Item.DataItem,"initial_in_service_date","{0:dd/MM/yyyy}").ToString();

			//== disposal_date
			Label lblDisposalDate = (Label)e.Item.FindControl("lblDisposalDate");
			if (lblDisposalDate != null && drSelected["disposal_date"] != System.DBNull.Value) 
				lblDisposalDate.Text = DataBinder.Eval(e.Item.DataItem,"disposal_date","{0:dd/MM/yyyy}").ToString();

			TextBox txtDisposalDate = (TextBox)e.Item.FindControl("txtDisposalDate");
			if (txtDisposalDate != null && drSelected["disposal_date"] != System.DBNull.Value) 
				txtDisposalDate.Text = DataBinder.Eval(e.Item.DataItem,"disposal_date","{0:dd/MM/yyyy}").ToString();

			//== date_out_of_service
			Label lblDateOutOfService = (Label)e.Item.FindControl("lblDateOutOfService");
			if (lblDateOutOfService != null && drSelected["date_out_of_service"] != System.DBNull.Value) 
				lblDateOutOfService.Text = DataBinder.Eval(e.Item.DataItem,"date_out_of_service","{0:dd/MM/yyyy}").ToString();

			TextBox txtDateOutOfService = (TextBox)e.Item.FindControl("txtDateOutOfService");
			if (txtDateOutOfService != null && drSelected["date_out_of_service"] != System.DBNull.Value) 
				txtDateOutOfService.Text = DataBinder.Eval(e.Item.DataItem,"date_out_of_service","{0:dd/MM/yyyy}").ToString();

			//== date_in_service
			Label lblDateInService = (Label)e.Item.FindControl("lblDateInService");
			if (lblDateInService != null && drSelected["date_in_service"] != System.DBNull.Value) 
				lblDateInService.Text = DataBinder.Eval(e.Item.DataItem,"date_in_service","{0:dd/MM/yyyy}").ToString();

			TextBox txtDateInService = (TextBox)e.Item.FindControl("txtDateInService");
			if (txtDateInService != null && drSelected["date_in_service"] != System.DBNull.Value) 
				txtDateInService.Text = DataBinder.Eval(e.Item.DataItem,"date_in_service","{0:dd/MM/yyyy}").ToString();
		}
		
		
		protected void dgVehicle_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			int rowIndex = e.Item.ItemIndex;
			if ((int)ViewState["DPMode"] == (int)ScreenMode.Insert && (int)ViewState["DPOperation"] == (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				m_sdsVehicle.ds.Tables[0].Rows.RemoveAt(rowIndex);
			}

			ViewState["DPOperation"] = Operation.None;
			dgVehicle.EditItemIndex = -1;
			BindDataGrid();			
		}

		
		protected void dgVehicle_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgVehicle.CurrentPageIndex = e.NewPageIndex;
			RetreiveSelectedPage();
			Logger.LogDebugInfo("Vehicle", "dgVehicle_PageChange", "INF009", "On Page Change " + e.NewPageIndex);
		}

		
		protected void dgVehicle_Delete(object sender, DataGridCommandEventArgs e)
		{
			if (!btnExecuteQuery.Enabled)
			{
				if ((int)ViewState["DPOperation"] == (int)Operation.Update)
				{
					dgVehicle.EditItemIndex = -1;
				}
				BindDataGrid ();

				int rowIndex = e.Item.ItemIndex;
				m_sdsVehicle  = (SessionDS)Session["SESSION_DS_Vehicle"];

				try
				{
					DataRow dr = m_sdsVehicle.ds.Tables[0].Rows[rowIndex];
					string strTruckId = (string)dr["truckid"];

					//--Check before delete
					if ( Vehicle.IsTruckIdIsExistManifest(m_strAppID, m_strEnterpriseID, strTruckId))
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage, "CANNOT_DEL_MANIFEST_VEHICLE", utility.GetUserCulture());
						return;
					}
					
					//--DELETE
					int iRowsDeleted = Vehicle.DeleteVehicle(m_sdsVehicle.ds, rowIndex, m_strAppID, m_strEnterpriseID);
					lblErrorMessage.Text = iRowsDeleted + " row(s) deleted.";
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					if(strMsg.IndexOf("FK") != -1)
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage, "ERR_DEL_PATH_TRANS", utility.GetUserCulture());
					}
					if(strMsg.ToLower().IndexOf("child record found") != -1 )
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage, "ERR_DEL_PATH_TRANS", utility.GetUserCulture());							
					}			
					else
					{
						lblErrorMessage.Text = strMsg;
					}

					Logger.LogTraceError("MF_VEHICLE", "dgVehicle_Delete", "RBAC003", appException.Message.ToString());

					return;
				}
				if((int)ViewState["DPMode"] == (int)ScreenMode.Insert)
				{
					m_sdsVehicle.ds.Tables[0].Rows.RemoveAt(rowIndex);
				}
				else
				{
					RetreiveSelectedPage();
				}

				ViewState["DPOperation"] = Operation.None;
				//Make the row in non-edit Mode
				EditHRow(false);
				BindDataGrid();
				Logger.LogDebugInfo("Vehicle", "dgVehicle_Delete", "INF004", "updating data grid...");			
			}
		}

	
		private bool IsRuleDateTime(string start_date, string end_date)
		{
			bool result = true;
			if ( start_date!="" && end_date !="")
			{							 
				string d = start_date.Substring(0,2);
				string M = start_date.Substring(3,2);
				string y = start_date.Substring(6,4);
				string t = "00:00";	////start_date.Substring(11,5);

				string d1 = end_date.Substring(0,2);
				string M1 = end_date.Substring(3,2);
				string y1 = end_date.Substring(6,4);
				string t1 = "00:00";	////end_date.Substring(11,5);
				string d_start = M+"/"+d+"/"+y+" "+t+":"+"0";
				string d_end = M1+"/"+d1+"/"+y1+" "+t1+":"+"0";
				if (Convert.ToDateTime(d_end) <= Convert.ToDateTime(d_start))
				{
					result = false;
				}
				else
				{
					result = true;
				}
			}

			return result;
		}

	}
}
