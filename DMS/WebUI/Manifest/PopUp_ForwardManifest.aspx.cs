using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;
using com.ties.DAL;
using com.common.RBAC;
using CrystalDecisions.Web; 
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using CrystalDecisions.CrystalReports.Engine;


namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for PopUp_CreateUpdateBatch.
	/// </summary>
	public class PopUp_ForwardManifest : BasePopupPage
	{
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.TextBox txtEmailContent;
		protected System.Web.UI.WebControls.Label Header1;
		protected System.Web.UI.WebControls.Label lblEmailContent;
		protected System.Web.UI.WebControls.Label lblDCAddress;
		protected System.Web.UI.WebControls.Button btnSendEmail;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.CheckBoxList ckboxDCEMail;
		string strAppID=null;
		string strEnterpriseID=null;
		string strFORMID=null;
		string strUserId=null;
		string strbatchType=null;
		string strLocation=null;
		string strBatchNo=null;
		ReportDocument rptSource = null;
		string strExportFolder=null;
		com.common.classes.User user = null;
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			getSessionTimeOut(true);
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();
			strUserId = utility.GetUserID();  
			strFORMID = (String)Session["FORMID"]; 

			btnSendEmail.Attributes.Add("onmousedown", "return validateForm(this);");
			
			if(!Page.IsPostBack)
			{	
				if(getEmailUser()==false)
				{
					return;
				}

				if (Request.QueryString["BatchNo"] !=null)
				{						
					strBatchNo= Request.QueryString["BatchNo"].ToString().Trim();
					getDCEmail(strBatchNo);
				}

				
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSendEmail.Click += new System.EventHandler(this.btnSendEmail_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.ckboxDCEMail.SelectedIndexChanged += new System.EventHandler(this.ckboxDCEMail_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		
		private bool getEmailUser()
		{
			try
			{
				user = com.common.RBAC.RBACManager.GetUser(utility.GetAppID(), utility.GetEnterpriseID(), utility.GetUserID());
				if (user.UserEmail =="")
				{
					btnSendEmail.Enabled = false;
					lblError.Text = "Your email has not assigned to this user login." ;
					return false;
				}
			return true;
			}
			catch(Exception ex)
			{
				lblError.Text = ex.Message ;
				//Page.RegisterStartupScript("disable", "<script>disableListItems('" + ckboxDCEMail.ClientID + "','"+ tempIndex +"');</script>");
				return false;
			}
			
		}
		private void getDCEmail(string strBatchNo)
		{
			DataSet ds=null;
			ds = BatchManifest.GetDCEmailAddress(utility,strBatchNo);
			if (ds.Tables[0].Rows.Count>0)
			{
				ckboxDCEMail.DataTextField="DCDispaly";
				ckboxDCEMail.DataValueField="EmailAddress";
				ckboxDCEMail.DataSource=ds.Tables[0];
				ckboxDCEMail.DataBind();
				String tempIndex="";
				for(int i=0; i<ckboxDCEMail.Items.Count; i++)
				{
					if(ckboxDCEMail.Items[i].Value.ToString().Trim()!="")
					{
						ckboxDCEMail.Items[i].Selected=true;
					}
					else
					{
						tempIndex= tempIndex + i.ToString() + ",";
					}
				}
				if(tempIndex!="")
				{
					Page.RegisterStartupScript("disable", "<script>disableListItems('" + ckboxDCEMail.ClientID + "','"+ tempIndex +"');</script>");
				}
			}
			else
			{
				btnSendEmail.Enabled=false;
			}
		}


		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnSendEmail_Click(object sender, System.EventArgs e)
		{

			bool b = CreatePDFfile();
			if (b==false)
			{
				return;
			}
			
			string strBatchNo="";
			strBatchNo= Request.QueryString["BatchNo"].ToString().Trim();
			string strEmailContent=Utility.ReplaceSingleQuote(txtEmailContent.Text.Trim());
			if(strEmailContent.Trim()=="")
			{
				strEmailContent="!@#$%";
			}
			
			string strEmail="";
			for(int i=0; i<ckboxDCEMail.Items.Count; i++)
			{
				if(ckboxDCEMail.Items[i].Selected)
				{
					 strEmail += ckboxDCEMail.Items[i].Value + ";" ;
				}
			}
			if(strEmail.Trim()!="")
			{
				if(strEmail.Substring(strEmail.Length-1,1)==";")
				{
					strEmail=strEmail.Substring(0,strEmail.Length-1);
				}
			}

			//SendMail 
			if (user==null)
				getEmailUser();
			string body = txtEmailContent.Text.Trim() + "\n\n\n\n" + "Please don't reply to the email." ;	
			string from =utility.GetEmailNameSend() + "<" + utility.GetEmailSend() + ">";
			string mailTo = user.UserEmail;
			string ccTo = strEmail;
			string subject = utility.GetEmailSubject();
			string smtpServer = utility.GetSmtpServer();


			string strErr =	utility.SendEmail(body,from,mailTo,ccTo,subject,smtpServer,strExportFolder);
				
			if (strErr == "")
			{
				bool IsUpdate=false;						
				IsUpdate = Convert.ToBoolean(BatchManifest.ForwardManifest(utility,user,strBatchNo,strEmailContent,strEmail));
				if(IsUpdate)
				{
					String sScript = "";
					sScript += "<script language=javascript>";
					sScript += "  window.close();";
					sScript += "</script>";
					Response.Write(sScript);

				}
				else
				{
					lblError.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_UPD",utility.GetUserCulture());
				}
				
			}
			else
			{
				lblError.Text = strErr;
			}
			

		}

		private void ckboxDCEMail_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			foreach(ListItem chk in ckboxDCEMail.Items)
			{
				if(chk.Selected==true)
				{
					btnSendEmail.Enabled=true;
					break;
				}
				else
				{
					btnSendEmail.Enabled=false;
				}
			}
			String tempIndex="";
			for(int i=0; i<ckboxDCEMail.Items.Count; i++)
			{
				if(ckboxDCEMail.Items[i].Value.ToString().Trim()!="")
				{
					//ckboxDCEMail.Items[i].Selected=true;
				}
				else
				{
					tempIndex= tempIndex + i.ToString() + ",";
				}
			}
			if(tempIndex!="")
			{
				Page.RegisterStartupScript("disable", "<script>disableListItems('" + ckboxDCEMail.ClientID + "','"+ tempIndex +"');</script>");
			}

			 
		}

	//Create file PDF for send mail  by R 13/6/2013

		private bool CreatePDFfile()
		{
			
			strBatchNo= Request.QueryString["BatchNo"].ToString().Trim();
			strbatchType= Request.QueryString["Batch_type"].ToString().Trim();
			strLocation= Request.QueryString["Location"].ToString().Trim();
			if(strBatchNo == null || strbatchType == null)
			{
				lblError.Text = "Can not create pdf file.";
				return false;
			}
		
			try
			{
				SetReportInstance();
				writeFile();
				lblError.Text = "Create PDF files complete. Sending email.";
				return true;
			}
			catch(Exception ex)
			{
				lblError.Text = ex.Message;
				return false;
			}
		
		}
		private void writeFile()
		{
			strBatchNo= Request.QueryString["BatchNo"].ToString().Trim();
			ExportOptions exportOpts = rptSource.ExportOptions;
			exportOpts.ExportFormatType = ExportFormatType.PortableDocFormat;
			exportOpts.ExportDestinationType = ExportDestinationType.DiskFile;
			exportOpts.DestinationOptions = new DiskFileDestinationOptions();
			strExportFolder=System.Configuration.ConfigurationSettings.AppSettings["InvoiceExportPath"];
			strExportFolder = strExportFolder + strBatchNo + ".pdf";
			((DiskFileDestinationOptions)rptSource.ExportOptions.DestinationOptions).DiskFileName = strExportFolder;
			rptSource.Export();
		}
		private void SetReportInstance()
		{
			strbatchType= Request.QueryString["Batch_type"].ToString().Trim();
			if(strbatchType=="S" || strbatchType=="W")
			{
				rptSource = new DeliveryManifestReport_MF();
				ViewState["ReportName"]="DeliveryManifestReport_MF";
			}
			else if(strbatchType=="L" || strbatchType=="A")
			{
				rptSource = new DeliveryManifestReportLH_Draft();
				ViewState["ReportName"]="DeliveryManifestReportLH_Draft";
			}
			SetDeliveryParams();
			SetLogonInfo();
		}
		private void SetDeliveryParams()
		{
			string strDeliveryType= Request.QueryString["Batch_type"].ToString();
			string strBatch_no= Request.QueryString["BatchNo"].ToString();
			string strLocation= Request.QueryString["Location"].ToString();


			ParameterFieldDefinitions paramFldDefs ;
			ParameterValues paramVals = new ParameterValues() ;
			ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
			paramFldDefs = rptSource.DataDefinition.ParameterFields;

			if(strDeliveryType=="S" || strDeliveryType=="W")
			{
				foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
				{
					switch(paramFldDef.ParameterFieldName)
					{
						case "prmDeliveryType":
							paramDVal.Value = strDeliveryType;
							break;
						case "prmBatchNo":
							paramDVal.Value =  strBatch_no; 
							break;
						case "prmApplicationID":
							paramDVal.Value = strAppID ; 
							break;
						case "prmEnterpriseID":
							paramDVal.Value = strEnterpriseID ; 
							break;
						case "prmUserID":
							paramDVal.Value = strUserId; 
							break;
						default:
							continue;
					}
					paramVals = paramFldDef.CurrentValues;
					paramVals.Add(paramDVal);
					paramFldDef.ApplyCurrentValues(paramVals);
				}
			}

			else
			{
				DataSet dsCheckReport = BatchManifest.GetBatchDetail_Shipping_BMCP(strAppID,strEnterpriseID, strBatch_no);
				if(dsCheckReport.Tables[0].Rows.Count>0)
				{
					bool hasFlag=false;
					for(int i=0;i<=dsCheckReport.Tables[0].Rows.Count-1;i++)
					{
						if(dsCheckReport.Tables[0].Rows[i]["Dep_DC"].ToString()==strLocation)
						{
							hasFlag=true;
							break;
						}
					}
					if(hasFlag==true)
					{
						foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
						{
							switch(paramFldDef.ParameterFieldName)
							{
								case "prmDeliveryType":
									paramDVal.Value = strDeliveryType;
									break;
								case "prmBatchNo":
									paramDVal.Value =  strBatch_no; 
									break;
								case "prmApplicationID":
									paramDVal.Value = strAppID ; 
									break;
								case "prmEnterpriseID":
									paramDVal.Value = strEnterpriseID ; 
									break;
								case "prmUserID":
									paramDVal.Value = strUserId; 
									break;
								case "prmLocation":
									paramDVal.Value = strLocation; 
									break;
								default:
									continue;
							}
							paramVals = paramFldDef.CurrentValues;
							paramVals.Add(paramDVal);
							paramFldDef.ApplyCurrentValues(paramVals);
						}
					}
					else
					{
						rptSource = new DeliveryManifestReportLH_Draft_Web();
						ViewState["ReportName"]="DeliveryManifestReportLH_Draft_Web";
						paramFldDefs = rptSource.DataDefinition.ParameterFields;
						string strMainLocation="";
						//getDC ����ش
						DataSet dsCheckDC= BatchManifest.GetBatchDetail_Shipping(strAppID,strEnterpriseID, strBatch_no);
						if(dsCheckDC.Tables[0].Rows.Count>0)
						{
							strMainLocation=dsCheckDC.Tables[0].Rows[0]["Dep_DC"].ToString().Trim();
						}
						
						foreach(ParameterFieldDefinition paramFldDef in paramFldDefs)
						{
							switch(paramFldDef.ParameterFieldName)
							{
								case "prmDeliveryType":
									paramDVal.Value = strDeliveryType;
									break;
								case "prmBatchNo":
									paramDVal.Value =  strBatch_no; 
									break;
								case "prmApplicationID":
									paramDVal.Value = strAppID ; 
									break;
								case "prmEnterpriseID":
									paramDVal.Value = strEnterpriseID ; 
									break;
								case "prmUserID":
									paramDVal.Value = strUserId; 
									break;
								case "prmLocation":
									paramDVal.Value = strLocation; 
									break;
								case "prmMainLocation":
									paramDVal.Value = strMainLocation; 
									break;
								default:
									continue;
							}
							paramVals = paramFldDef.CurrentValues;
							paramVals.Add(paramDVal);
							paramFldDef.ApplyCurrentValues(paramVals);
						}
					}
				}
			}
		}

		private void SetLogonInfo()
		{
			//create a structure object to store connection details
			ConnectionDetails conDet = new ConnectionDetails();

			//get connection details from dbcon manager
			conDet = DbConnectionManager.GetConnectionDetails(strAppID, strEnterpriseID);

			TableLogOnInfo MyLogin; 
			//get the no of tables in the main report and 
			//assign connection details to all
			int intTabCnt = rptSource.Database.Tables.Count, i=0;
			for (i=0; i<intTabCnt; i++)
			{
				MyLogin = rptSource.Database.Tables[i].LogOnInfo;
				if (Utility.IsNotDBNull(conDet.DatabaseName))
					MyLogin.ConnectionInfo.DatabaseName = conDet.DatabaseName;
				MyLogin.ConnectionInfo.ServerName = conDet.ServerName;
				MyLogin.ConnectionInfo.UserID = conDet.UserID;
				MyLogin.ConnectionInfo.Password = conDet.Password;
				rptSource.Database.Tables[i].ApplyLogOnInfo(MyLogin);
				
				if(ViewState["ReportName"]!=null)
				{
					if(ViewState["ReportName"].ToString()=="DeliveryManifestReport_MF" && rptSource.Database.Tables[i].Name.IndexOf("v_DeliveryManifestByRoute")>=0)
					{
						rptSource.Database.Tables[i].Location = rptSource.Database.Tables[i].Location.Substring(rptSource.Database.Tables[i].Location.LastIndexOf(".")+1);
					}
					else if(ViewState["ReportName"].ToString()=="DeliveryManifestReportLH_Draft" && rptSource.Database.Tables[i].Name.IndexOf("v_DeliveryManifestByRoute")>=0)
					{
						rptSource.Database.Tables[i].Location = rptSource.Database.Tables[i].Location.Substring(rptSource.Database.Tables[i].Location.LastIndexOf(".")+1);
					}
					else if(ViewState["ReportName"].ToString()=="DeliveryManifestReportLH_Draft_Web" && rptSource.Database.Tables[i].Name.IndexOf("v_DeliveryManifestByRoute")>=0)
					{
						rptSource.Database.Tables[i].Location = rptSource.Database.Tables[i].Location.Substring(rptSource.Database.Tables[i].Location.LastIndexOf(".")+1);
					}
					else if(ViewState["ReportName"].ToString()=="DairyLodgments")
					{
						
					}
					else
					{
						rptSource.Database.Tables[i].Location = rptSource.Database.Tables[i].Name.ToUpper();
					}
				}
				else
				{
					rptSource.Database.Tables[i].Location = rptSource.Database.Tables[i].Name.ToUpper();
				}
				

				
				
				  
			}

			//get subreports from the main report
			Sections crSections; //new Sections();
			ReportObjects crRepObjects; //new ReportObjects();
			SubreportObject crSubRepObj; //new SubreportObject();
			ReportDocument crSubRep; //new ReportDocument();

			crSections = rptSource.ReportDefinition.Sections;
			int intSecCnt = rptSource.ReportDefinition.Sections.Count;
			for (i=0; i<intSecCnt; i++)
			{
				crRepObjects = crSections[i].ReportObjects;
				int intSubCnt = crSections[i].ReportObjects.Count;
				for (int j=0; j<intSubCnt; j++)
				{
					if (crRepObjects[j].Kind == ReportObjectKind.SubreportObject)
					{
						crSubRepObj = (SubreportObject)crRepObjects[j];
						crSubRep = crSubRepObj.OpenSubreport(crSubRepObj.SubreportName);
						//get the no of tables in the sub report and 
						//assign connection details to all
						int intSubTabCnt = crSubRep.Database.Tables.Count;
						for (int k=0; k<intSubTabCnt; k++)
						{
							MyLogin = crSubRep.Database.Tables[k].LogOnInfo;
							if (Utility.IsNotDBNull(conDet.DatabaseName))
								MyLogin.ConnectionInfo.DatabaseName = conDet.DatabaseName;
							MyLogin.ConnectionInfo.ServerName = conDet.ServerName;
							MyLogin.ConnectionInfo.UserID = conDet.UserID;
							MyLogin.ConnectionInfo.Password = conDet.Password;
							crSubRep.Database.Tables[k].ApplyLogOnInfo(MyLogin);
							crSubRep.Database.Tables[k].Location = crSubRep.Database.Tables[k].Name.ToUpper();
							if(ViewState["ReportName"]!=null)
							{
								if(ViewState["ReportName"].ToString()=="DeliveryManifestReportLH_Draft" && crSubRep.Database.Tables[k].Name.IndexOf("v_DeliveryManifestByRoute")>=0)
								{
									crSubRep.Database.Tables[k].Location = crSubRep.Database.Tables[k].Location.Substring(crSubRep.Database.Tables[k].Location.LastIndexOf(".")+1);
								}
								else if(ViewState["ReportName"].ToString()=="DeliveryManifestReportLH_Draft_Web" && crSubRep.Database.Tables[k].Name.IndexOf("v_DeliveryManifestByRoute")>=0)
								{
									crSubRep.Database.Tables[k].Location = crSubRep.Database.Tables[k].Location.Substring(crSubRep.Database.Tables[k].Location.LastIndexOf(".")+1);
								}
								else
								{
									crSubRep.Database.Tables[k].Location = crSubRep.Database.Tables[k].Name.ToUpper();
								}
							}
							else
							{
								crSubRep.Database.Tables[k].Location = crSubRep.Database.Tables[k].Name.ToUpper();
							}
						}
					}
				}
			}
		}

	}
}