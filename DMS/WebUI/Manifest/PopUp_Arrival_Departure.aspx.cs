
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;
using com.ties.DAL;
using com.common.RBAC;
using com.ties.classes;

namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for PopUp_Arrival_Departure.
	/// </summary>
	public class PopUp_Arrival_Departure : BasePopupPage//System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblBatchNumber;
		protected System.Web.UI.WebControls.Label lblBatchDate;
		protected System.Web.UI.WebControls.Label lblTruckID;
		protected System.Web.UI.WebControls.Label lblBatchID;
		protected System.Web.UI.WebControls.Label lblBatchType;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label lblBatchNoDisplay;
		protected System.Web.UI.WebControls.Label lblLocDisplay;
		protected System.Web.UI.WebControls.Label lblTruckIDDisplay;
		protected System.Web.UI.WebControls.Label lblBatchIDDisplay;
		protected System.Web.UI.WebControls.Label lblBatchTypeDisplay;
		protected System.Web.UI.WebControls.Label lblUserDisplay;
		protected System.Web.UI.WebControls.Label lblBatchDateDisplay;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label lblCreateOrUpdateBatch;
		protected System.Web.UI.WebControls.RadioButton rdbDeparture;
		protected System.Web.UI.WebControls.DropDownList ddlDepartureDC;
		protected com.common.util.msTextBox txtActualDateDT;
		protected System.Web.UI.WebControls.RadioButton rdbArrival;
		protected com.common.util.msTextBox txtArrivalDate;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label8;
		protected com.common.util.msTextBox txtActualDateAR;
		protected System.Web.UI.WebControls.DropDownList ddlArrivalDC;
		protected System.Web.UI.WebControls.Button btnRevCom;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label Header1;
		private String m_strAppID, m_strEnterpriseID, m_strCulture,btnName,userid,location,batch_no,reciscom,DriverID_;
		protected System.Web.UI.WebControls.Label lblValISector;
		protected System.Web.UI.WebControls.TextBox hdnbox;
		protected System.Web.UI.WebControls.TextBox txtmsg;
		protected System.Web.UI.WebControls.Button btnEnableRevCom;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.DropDownList ddlAirTruckID;
		protected System.Web.UI.WebControls.Button btnAirTruckSearch;
		protected com.common.util.msTextBox txtAirDepDT;
		protected System.Web.UI.WebControls.Panel pnlAir;
		protected System.Web.UI.WebControls.TextBox txtAWBNo;
		protected com.common.util.msTextBox txtFightNo;
		protected System.Web.UI.WebControls.TextBox txtValueActDT;
		protected System.Web.UI.WebControls.TextBox txtmsgActDT;
		protected System.Web.UI.WebControls.Button btnCancelChangeActual;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label lblServiceType;
		protected System.Web.UI.HtmlControls.HtmlTableRow trServiceType;
		protected System.Web.UI.WebControls.TextBox txtDriverID;
		protected System.Web.UI.WebControls.TextBox txtAirDriverID;
		protected ArrayList userRoleArray;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			getSessionTimeOut(true);
			Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID =  Request.QueryString["AppID"] ;
			m_strEnterpriseID =  Request.QueryString["EnterpriseID"] ;
			m_strCulture =utility.GetUserCulture();
			btnName =  Request.QueryString["btnname"] ;
			userid =  Request.QueryString["UserID"] ;
			location =  Request.QueryString["Location"] ;
			batch_no =  Request.QueryString["BatchNo"] ;
			reciscom = Request.QueryString["RecIsCom"]; 
			btnRevCom.Attributes.Add("Onclick", "javascript:return confirm(document.getElementById('txtmsg').value)");
			//btnEnableRevCom.Attributes.Add("Onclick", "javascript:return confirm(document.getElementById('txtmsgActDT').value)");
			//txtActualDateDT.Attributes.Add("onkeyDown", "KeyDownExe();");
			btnSave.Attributes.Add("Onclick", "javascript:return ChkChangeActual();");
			//ChkChangeActual()
			//	txtActualDateAR.Attributes.Add("onchange", "EnableRevCom();");
			
			if(!Page.IsPostBack)
			{
				GetInformation(btnName);
				if (btnName=="Arrival")
				{
					txtmsg.Text ="Receiving for batch:" + batch_no +" Truck ID: " + lblTruckIDDisplay.Text + " " + lblBatchIDDisplay.Text + " " + lblBatchDateDisplay.Text + " " + lblBatchTypeDisplay.Text +" at " + ddlArrivalDC.SelectedItem.Text +" is complete. Is this correct?";
					
				}
				//				else if (btnName=="Departure")
				//				{
				//					
				//				}
				txtmsgActDT.Text ="Changing the departure date/time will not change the date recorded on the existing TKL/DVL records. Proceed?"; 
				GetDeparture();
				EnableControl(btnName,false);
			
			
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.hdnbox.TextChanged += new System.EventHandler(this.hdnbox_TextChanged);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.ddlDepartureDC.SelectedIndexChanged += new System.EventHandler(this.ddlDepartureDC_SelectedIndexChanged);
			this.ddlArrivalDC.SelectedIndexChanged += new System.EventHandler(this.ddlArrivalDC_SelectedIndexChanged);
			this.btnRevCom.Click += new System.EventHandler(this.btnRevCom_Click);
			this.btnAirTruckSearch.Click += new System.EventHandler(this.btnAirTruckSearch_Click);
			this.btnEnableRevCom.Click += new System.EventHandler(this.btnEnableRevCom_Click);
			this.btnCancelChangeActual.Click += new System.EventHandler(this.btnCancelChangeActual_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void EnableControl(string type,bool valueEnable)
		{
			lblValISector.Text ="";
			if (type == "Departure")
			{
				rdbDeparture.Checked = !valueEnable;
				rdbArrival.Enabled = valueEnable;
				txtActualDateAR.Enabled =valueEnable;
				ddlArrivalDC.Enabled = valueEnable;
				ddlAirTruckID.Enabled = valueEnable;
				btnAirTruckSearch.Enabled = valueEnable;

				txtAirDriverID.Enabled = valueEnable;
//				ddlAirDriverID.Enabled = valueEnable;
//				btnAirDriverSearch.Enabled = valueEnable;
				if (getRole()!="OPSSU")
				{
					ddlDepartureDC.Enabled = false;
				}
				btnRevCom.Enabled = false;
			}
			else 
			{
				rdbArrival.Checked = !valueEnable;
				rdbDeparture.Enabled = valueEnable;
				txtActualDateDT.Enabled  = valueEnable;
				ddlDepartureDC.Enabled = valueEnable;

				txtDriverID.Enabled  = valueEnable;
//				ddlDriverID.Enabled  = valueEnable;
//				btnDriverIDSrch.Enabled = valueEnable; 
				txtArrivalDate.Enabled = valueEnable;
				txtFightNo.Enabled = valueEnable;
				txtAirDepDT.Enabled = valueEnable;
				txtAWBNo.Enabled = valueEnable;
				if (getRole()!="OPSSU")
				{
					ddlArrivalDC.Enabled = false;
				}
				
				if ((string)Session["batch_type"] =="L")
				{
					DataSet cRecL = BatchManifest.GetEnableRecForBatchTypeL(m_strAppID,m_strEnterpriseID,batch_no,ddlArrivalDC.SelectedItem.Text);
					if (cRecL.Tables[0].Rows.Count > 0)
					{
						if (cRecL.Tables[0].Rows[0]["cbatch_no"].ToString() =="0")
							btnRevCom.Enabled = false;
						else
							btnRevCom.Enabled = true;
					}
				}
				else 
				{
					if (reciscom =="Y")
						btnRevCom.Enabled = false;
					else
						btnRevCom.Enabled = true;
				}
			}
		}
		private string getRole()
		{
			User user = new User();
			user.UserID = userid;
			userRoleArray = com.common.RBAC.RBACManager.GetAllRoles(m_strAppID, m_strEnterpriseID, user);
			Role role;

			for(int i=0;i<userRoleArray.Count;i++)
			{
				role = (Role)userRoleArray[i];				
				if (role.RoleName.ToUpper().Trim() == "OPSSU") return "OPSSU";
			}
			return " ";
		}
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void GetInformation(string type)
		{
			DataSet con = BatchManifest.GetDataArrivalDeparture( m_strAppID,m_strEnterpriseID,m_strCulture,batch_no);
			if (con.Tables[0].Rows[0]["batch_type"].ToString()== "W")
				btnRevCom.Enabled = false;
//			else
//				btnRevCom.Enabled = true;
			
			string strServiceType = "";
			if (con.Tables[0].Rows[0]["batch_type"].ToString()== "A")
			{
				pnlAir.Visible =true;
				trServiceType.Style["display"] = "block";

				if (con.Tables[0].Rows[0]["ServiceType"] == null || con.Tables[0].Rows[0]["ServiceType"].ToString() == "")
					strServiceType ="-";
				else
					strServiceType = con.Tables[0].Rows[0]["ServiceType"].ToString();
			}
			else
			{
				pnlAir.Visible =false;
				trServiceType.Style["display"] = "none";
			}

			

			Session["batch_type"] =con.Tables[0].Rows[0]["batch_type"].ToString();
			if (con.Tables[0].Rows.Count > 0 )
			{
				lblBatchNoDisplay.Text = batch_no;
				lblBatchDateDisplay.Text = String.Format("{0:dd/MM/yyyy}",con.Tables[0].Rows[0]["batch_date"]);  
				lblTruckIDDisplay.Text = con.Tables[0].Rows[0]["truck_id"].ToString(); 
				lblBatchIDDisplay.Text = con.Tables[0].Rows[0]["batch_id"].ToString();
				ViewState["batch_id"]=lblBatchIDDisplay.Text;
				lblBatchTypeDisplay.Text = con.Tables[0].Rows[0]["code_text"].ToString(); 
				lblUserDisplay.Text = userid;
				lblLocDisplay.Text = location;
				lblServiceType.Text = strServiceType;
			}
			if (type == "Departure")
			{
				LoadDepartureDC();
				ddlDepartureDC.SelectedItem.Text = location; 
				//LoadDriverID();
			}
			else
			{
				LoadDepartureDC();
				//LoadDriverID();
				LoadArrivalDC(); 
				ddlArrivalDC.SelectedItem.Text =location;
				LoadAirTruck();
//				LoadAirDriverID(); 
				DataSet dsArr = BatchManifest.GetBatchDetail_ShippingTerminationLoad(m_strAppID,m_strEnterpriseID,batch_no,ddlArrivalDC.SelectedItem.Text);
				txtActualDateAR.Text =String.Format("{0:dd/MM/yyyy HH:mm}",DateTime.Now);
				
				if(dsArr.Tables[0].Rows.Count>0 && dsArr.Tables[0].Rows[0]["Arr_DT"].ToString()!="")
				{
					txtActualDateAR.Text = String.Format("{0:dd/MM/yyyy HH:mm}",dsArr.Tables[0].Rows[0]["Arr_DT"]);

					if(dsArr.Tables[0].Rows[0]["Arr_Driver_ID"] != null)
						txtAirDriverID.Text = dsArr.Tables[0].Rows[0]["Arr_Driver_ID"].ToString();
//					if(ddlAirDriverID.Items.FindByValue(dsArr.Tables[0].Rows[0]["Arr_Driver_ID"].ToString())!=null)
//					{
//						ddlAirDriverID.SelectedValue = dsArr.Tables[0].Rows[0]["Arr_Driver_ID"].ToString();
//					}
					if(ddlAirTruckID.Items.FindByText( dsArr.Tables[0].Rows[0]["Arr_Truck_ID"].ToString())!=null)
					{
						ddlAirTruckID.SelectedValue = dsArr.Tables[0].Rows[0]["Arr_Truck_ID"].ToString();
					}
				}

			}
			

		}
		private void GetDeparture()
		{
			DataSet con = BatchManifest.GetBatchDetail_Shipping( m_strAppID,m_strEnterpriseID,batch_no);
			if ((string)Session["batch_type"] != "L")
			{
				if (con.Tables[0].Rows.Count > 0 )
				{
					int i,k = 0;
					bool chkLocation =false;
					for(i=0;i<con.Tables[0].Rows.Count;i++)
					{
						if(location==con.Tables[0].Rows[i]["Dep_DC"].ToString())
						{
							ddlDepartureDC.SelectedValue = con.Tables[0].Rows[i]["Dep_DC"].ToString();
							chkLocation=true;
							break;
						}
					}
					if(chkLocation)
					{
						if (String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Dep_DT"]) =="")
						{
							txtActualDateDT.Text = DateTime.Now.AddMinutes(-1).ToString("dd/MM/yyyy HH:mm"); 
							//txtActualDateDT.ForeColor = System.Drawing.Color.Blue; //Thosapol.y Modify (28/06/2013)
							txtValueActDT.Text = "";
						}
						else
						{
							txtActualDateDT.Text = String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Dep_DT"]);
							txtValueActDT.Text = String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Dep_DT"]);
						}
						for (k=i;k>=0;k--)
						{
							if(con.Tables[0].Rows[k]["Dep_Driver_ID"].ToString()!="")
							{
								break;
							}
						}
						if(k<0)
						{
							k=0;
						}
//						DataSet driverid = BatchManifest.GetCHKDDLDriverID(m_strAppID,m_strEnterpriseID,con.Tables[0].Rows[k]["Dep_Driver_ID"].ToString(),location) ;
//						if (driverid.Tables[0].Rows.Count >0  )
//						{
//							if (driverid.Tables[0].Rows[0]["cemp_id"].ToString() =="0" )
//							{
//								txtDriverID.Text ="";
//								//ddlDriverID.SelectedItem.Text =""; 
//								ViewState["DriverID_"] ="";
//							}
//							else
//							{
//								txtDriverID.Text = con.Tables[0].Rows[k]["Dep_Driver_ID"].ToString();
//								//ddlDriverID.SelectedValue =con.Tables[0].Rows[k]["Dep_Driver_ID"].ToString();
//								ViewState["DriverID_"] =con.Tables[0].Rows[k]["Dep_Driver_ID"].ToString();
//							}
//						}
						txtDriverID.Text = con.Tables[0].Rows[k]["Dep_Driver_ID"].ToString();
						ViewState["DriverID_"] =con.Tables[0].Rows[k]["Dep_Driver_ID"].ToString();


						txtArrivalDate.Text =String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Dep_Est_Arr_DT"]);
						ViewState["txtArrivalDate"] = txtArrivalDate.Text;
						txtFightNo.Text = con.Tables[0].Rows[i]["Dep_Flight_No"].ToString();
						txtAWBNo.Text = con.Tables[0].Rows[i]["Dep_AWB_No"].ToString(); 
						txtAirDepDT.Text = String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Dep_Flight_DT"]);
						ViewState["txtAirDepDT"] = txtAirDepDT.Text;
					}
					else // Select Another location
					{
						//txtActualDateDT.Text ="";
						txtActualDateDT.Text = DateTime.Now.AddMinutes(-1).ToString("dd/MM/yyyy HH:mm"); 
						txtValueActDT.Text = "";
						txtDriverID.Text ="";
						txtArrivalDate.Text = "";
						txtFightNo.Text = "";
						txtAWBNo.Text = ""; 
						txtAirDepDT.Text = "";

						ViewState["DriverID_"] ="";
						ViewState["txtArrivalDate"] ="";
						ViewState["txtAirDepDT"] ="";
						if(btnName=="Arrival")
						{
							////
							DataSet conArr = BatchManifest.GetBatchDetail_Shipping_OnArrival( m_strAppID,m_strEnterpriseID,batch_no);
							if(conArr.Tables[0].Rows.Count>0)
							{
								txtActualDateDT.Text = String.Format("{0:dd/MM/yyyy HH:mm}",conArr.Tables[0].Rows[0]["Dep_DT"]);
								ddlDepartureDC.SelectedValue = conArr.Tables[0].Rows[0]["Dep_DC"].ToString();

								DataSet driverid = BatchManifest.GetCHKDDLDriverID(m_strAppID,m_strEnterpriseID,conArr.Tables[0].Rows[0]["Dep_Driver_ID"].ToString(),location) ;
								if (driverid.Tables[0].Rows.Count >0  )
								{
									if (driverid.Tables[0].Rows[0]["cemp_id"].ToString() =="0" )
									{
										txtDriverID.Text =""; 
										ViewState["DriverID_"] ="";
									}
									else
									{
										txtDriverID.Text = conArr.Tables[0].Rows[0]["Dep_Driver_ID"].ToString();
										ViewState["DriverID_"] =conArr.Tables[0].Rows[0]["Dep_Driver_ID"].ToString();
									}
								}
								txtArrivalDate.Text =String.Format("{0:dd/MM/yyyy HH:mm}",conArr.Tables[0].Rows[0]["Dep_Est_Arr_DT"]);
								ViewState["txtArrivalDate"] = txtArrivalDate.Text;
								txtFightNo.Text = conArr.Tables[0].Rows[0]["Dep_Flight_No"].ToString();
								txtAWBNo.Text = conArr.Tables[0].Rows[0]["Dep_AWB_No"].ToString(); 
								txtAirDepDT.Text = String.Format("{0:dd/MM/yyyy HH:mm}",conArr.Tables[0].Rows[0]["Dep_Flight_DT"]);
								ViewState["txtAirDepDT"] = txtAirDepDT.Text;
						
							}

						}
					}
				}
				else
				{
					if(ddlDepartureDC.Items.FindByValue(location)!=null)
					{
						ddlDepartureDC.SelectedValue =location;
					}
					else
					{
						ddlDepartureDC.SelectedValue ="";
					}
					//txtActualDateDT.Text ="";
					txtActualDateDT.Text = DateTime.Now.AddMinutes(-1).ToString("dd/MM/yyyy HH:mm"); 

					txtDriverID.Text ="";
					txtArrivalDate.Text = "";
					txtFightNo.Text = "";
					txtAWBNo.Text = ""; 
					txtAirDepDT.Text = "";
				}
			}
			else //batch tpye = L
			{
				if(btnName=="Departure")
				{
					DataSet conL = BatchManifest.GetBatchDetailForBatchType_L_OnDeparture(m_strAppID,m_strEnterpriseID,batch_no,location);
					if (conL.Tables[0].Rows.Count > 0  )
					{
						DataSet driverid = BatchManifest.GetCHKDDLDriverIDforbatchtypeL_A(m_strAppID,m_strEnterpriseID,conL.Tables[0].Rows[0]["Dep_Driver_ID"].ToString(),location,lblBatchIDDisplay.Text ) ;
						if (driverid.Tables[0].Rows.Count >0  )
						{
							if (driverid.Tables[0].Rows[0]["cemp_id"].ToString() =="0" )
							{
								txtDriverID.Text =""; 
								ViewState["DriverID_"] ="";
							}
							else
							{
								txtDriverID.Text =conL.Tables[0].Rows[0]["Dep_Driver_ID"].ToString();
								ViewState["DriverID_"] =conL.Tables[0].Rows[0]["Dep_Driver_ID"].ToString();
							}
						}
						if (String.Format("{0:dd/MM/yyyy HH:mm}",conL.Tables[0].Rows[0]["Dep_DT"])!="")
						{
							txtActualDateDT.Text = String.Format("{0:dd/MM/yyyy HH:mm}",conL.Tables[0].Rows[0]["Dep_DT"]);
							txtValueActDT.Text = String.Format("{0:dd/MM/yyyy HH:mm}",conL.Tables[0].Rows[0]["Dep_DT"]);
						}
						else
						{
							txtActualDateDT.Text = DateTime.Now.AddMinutes(-1).ToString("dd/MM/yyyy HH:mm"); 
							txtValueActDT.Text = "";
						}

						ddlDepartureDC.SelectedValue =conL.Tables[0].Rows[0]["Dep_dc"].ToString();
						ViewState["LDep_DC"] = conL.Tables[0].Rows[0]["Dep_dc"].ToString();
						txtArrivalDate.Text =String.Format("{0:dd/MM/yyyy HH:mm}",conL.Tables[0].Rows[0]["Dep_Est_Arr_DT"]);
					}
					else
					{
						txtActualDateDT.Text = DateTime.Now.AddMinutes(-1).ToString("dd/MM/yyyy HH:mm"); 
						txtValueActDT.Text = "";
						conL = BatchManifest.GetBatchDetailForBatchType_L_OnArrival(m_strAppID,m_strEnterpriseID,batch_no);
						if (conL.Tables[0].Rows.Count > 0  )
						{
							//DataSet driverid = BatchManifest.GetCHKDDLDriverID(m_strAppID,m_strEnterpriseID,conL.Tables[0].Rows[0]["Dep_Driver_ID"].ToString(),location) ;
							DataSet driverid = BatchManifest.GetCHKDDLDriverIDforbatchtypeL_A(m_strAppID,m_strEnterpriseID,conL.Tables[0].Rows[0]["Dep_Driver_ID"].ToString(),location,lblBatchIDDisplay.Text ) ;

							if (driverid.Tables[0].Rows.Count >0  )
							{
								if (driverid.Tables[0].Rows[0]["cemp_id"].ToString() =="0" )
								{
									txtDriverID.Text =""; 
									ViewState["DriverID_"] ="";
								}
								else
								{
									txtDriverID.Text =conL.Tables[0].Rows[0]["Dep_Driver_ID"].ToString();
									ViewState["DriverID_"] =conL.Tables[0].Rows[0]["Dep_Driver_ID"].ToString();
								}
							}
						}
					}
				}
				else
				{
					DataSet conL = BatchManifest.GetBatchDetailForBatchType_L_OnArrival(m_strAppID,m_strEnterpriseID,batch_no);
					if (conL.Tables[0].Rows.Count > 0  )
					{
						//DataSet driverid = BatchManifest.GetCHKDDLDriverID(m_strAppID,m_strEnterpriseID,conL.Tables[0].Rows[0]["Dep_Driver_ID"].ToString(),location) ;
						DataSet driverid = BatchManifest.GetCHKDDLDriverIDforbatchtypeL_A(m_strAppID,m_strEnterpriseID,conL.Tables[0].Rows[0]["Dep_Driver_ID"].ToString(),location,lblBatchIDDisplay.Text ) ;

						if (driverid.Tables[0].Rows.Count >0  )
						{
							if (driverid.Tables[0].Rows[0]["cemp_id"].ToString() =="0" )
							{
								txtDriverID.Text =""; 
								ViewState["DriverID_"] ="";
							}
							else
							{
								txtDriverID.Text =conL.Tables[0].Rows[0]["Dep_Driver_ID"].ToString();
								ViewState["DriverID_"] =conL.Tables[0].Rows[0]["Dep_Driver_ID"].ToString();
							}
						}
						txtActualDateDT.Text = String.Format("{0:dd/MM/yyyy HH:mm}",conL.Tables[0].Rows[0]["Dep_DT"]);
						txtValueActDT.Text = String.Format("{0:dd/MM/yyyy HH:mm}",conL.Tables[0].Rows[0]["Dep_DT"]);
						ddlDepartureDC.SelectedValue =conL.Tables[0].Rows[0]["Dep_dc"].ToString();
						ViewState["LDep_DC"] = conL.Tables[0].Rows[0]["Dep_dc"].ToString();
						txtArrivalDate.Text =String.Format("{0:dd/MM/yyyy HH:mm}",conL.Tables[0].Rows[0]["Dep_Est_Arr_DT"]);
					}

				}
			}
		}
		public void LoadDepartureDC()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet dataset = com.ties.classes.DbComboDAL.UserLocationQuery(strAppID,strEnterpriseID);	
			this.ddlDepartureDC.DataSource = dataset;

			ddlDepartureDC.DataTextField = dataset.Tables[0].Columns["origin_code"].ToString() ;
			ddlDepartureDC.DataValueField = dataset.Tables[0].Columns["origin_code"].ToString() ;
			this.ddlDepartureDC.DataBind();
			ListItem defItem = new ListItem();
			defItem.Text = " ";
			
			defItem.Value = "";
			ddlDepartureDC.Items.Insert(0,(defItem));
		}

		public void LoadArrivalDC()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet dataset = com.ties.classes.DbComboDAL.UserLocationQuery(strAppID,strEnterpriseID);	
			this.ddlArrivalDC.DataSource = dataset;

			ddlArrivalDC.DataTextField = dataset.Tables[0].Columns["origin_code"].ToString() ;
			ddlArrivalDC.DataValueField = dataset.Tables[0].Columns["origin_code"].ToString() ;
			this.ddlArrivalDC.DataBind();
			ListItem defItem = new ListItem();
			defItem.Text = " ";
			
			defItem.Value = "";
			ddlArrivalDC.Items.Insert(0,(defItem));
		}


//		public void LoadDriverID()
//		{
//			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
//			String strAppID = util.GetAppID();
//			String strEnterpriseID = util.GetEnterpriseID();
//
//			DataSet dataset = com.ties.classes.DbComboDAL.DriverIDQuery(strAppID,strEnterpriseID,"N",Convert.ToInt32(batch_no),ddlDepartureDC.SelectedItem.Text,"N");	
//			this.ddlDriverID.DataSource = dataset;
//
//			ddlDriverID.DataTextField = dataset.Tables[0].Columns["emp_display"].ToString() ;
//			ddlDriverID.DataValueField = dataset.Tables[0].Columns["emp_id"].ToString() ;
//			this.ddlDriverID.DataBind();
//			ListItem defItem = new ListItem();
//			defItem.Text = " ";
//			
//			defItem.Value = "";
//			ddlDriverID.Items.Insert(0,(defItem));
//		}

//		public void LoadDriverIDAll()
//		{
//			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
//			String strAppID = util.GetAppID();
//			String strEnterpriseID = util.GetEnterpriseID();
//
//			DataSet dataset = com.ties.classes.DbComboDAL.DriverIDQuery(strAppID,strEnterpriseID,"Y",0,"Null","N");	
//			this.ddlDriverID.DataSource = dataset;
//
//			ddlDriverID.DataTextField = dataset.Tables[0].Columns["emp_display"].ToString() ;
//			ddlDriverID.DataValueField = dataset.Tables[0].Columns["emp_id"].ToString() ;
//			this.ddlDriverID.DataBind();
//			ListItem defItem = new ListItem();
//			defItem.Text = " ";
//			
//			defItem.Value = "";
//			ddlDriverID.Items.Insert(0,(defItem));
//		}

//		public void LoadAirDriverID()
//		{
//			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
//			String strAppID = util.GetAppID();
//			String strEnterpriseID = util.GetEnterpriseID();
//
//			DataSet dataset = com.ties.classes.DbComboDAL.DriverIDQuery(strAppID,strEnterpriseID,"N",Convert.ToInt32(batch_no),ddlArrivalDC.SelectedItem.Text ,"Y");	
//			this.ddlAirDriverID.DataSource = dataset;
//
//			ddlAirDriverID.DataTextField = dataset.Tables[0].Columns["emp_display"].ToString() ;
//			ddlAirDriverID.DataValueField = dataset.Tables[0].Columns["emp_id"].ToString() ;
//			this.ddlAirDriverID.DataBind();
//			ListItem defItem = new ListItem();
//			defItem.Text = " ";
//			
//			defItem.Value = "";
//			ddlAirDriverID.Items.Insert(0,(defItem));
//		}
//
//		public void LoadAirDriverIDAll()
//		{
//			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
//			String strAppID = util.GetAppID();
//			String strEnterpriseID = util.GetEnterpriseID();
//
//			DataSet dataset = com.ties.classes.DbComboDAL.DriverIDQuery(strAppID,strEnterpriseID,"Y",0,"Null","N");	
//			this.ddlAirDriverID.DataSource = dataset;
//
//			ddlAirDriverID.DataTextField = dataset.Tables[0].Columns["emp_display"].ToString() ;
//			ddlAirDriverID.DataValueField = dataset.Tables[0].Columns["emp_id"].ToString() ;
//			this.ddlAirDriverID.DataBind();
//			ListItem defItem = new ListItem();
//			defItem.Text = " ";
//			
//			defItem.Value = "";
//			ddlAirDriverID.Items.Insert(0,(defItem));
//		}

//		public void LoadDriverIDforbatchtypeAir()
//		{
//			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
//			String strAppID = util.GetAppID();
//			String strEnterpriseID = util.GetEnterpriseID();
//
//			DataSet dataset = com.ties.classes.DbComboDAL.DriverIDforbatchtypeSQuery(strAppID,strEnterpriseID,location);	
//			this.ddlAirDriverID.DataSource = dataset;
//
//			ddlAirDriverID.DataTextField = dataset.Tables[0].Columns["emp_display"].ToString() ;
//			ddlAirDriverID.DataValueField = dataset.Tables[0].Columns["emp_id"].ToString() ;
//			this.ddlAirDriverID.DataBind();
//			ListItem defItem = new ListItem();
//			defItem.Text = " ";
//			
//			defItem.Value = "";
//			ddlAirDriverID.Items.Insert(0,(defItem));
//		}

		public void LoadAirTruck()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			
			DataSet dataset = com.ties.classes.DbComboDAL.TruckIDQuery(strAppID,strEnterpriseID,"N",null,Convert.ToInt32(lblBatchNoDisplay.Text) );	
			this.ddlAirTruckID.DataSource = dataset;

			ddlAirTruckID.DataTextField = dataset.Tables[0].Columns["truckid"].ToString() ;
			ddlAirTruckID.DataValueField = dataset.Tables[0].Columns["truckid"].ToString() ;
			this.ddlAirTruckID.DataBind();
			ListItem defItem = new ListItem();
			defItem.Text = "";
			
			defItem.Value = "";
			ddlAirTruckID.Items.Insert(0,(defItem));
		}
		public void LoadTruck()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet dataset = com.ties.classes.DbComboDAL.TruckIDQuery(strAppID,strEnterpriseID,"Y","Null",0);	
			this.ddlAirTruckID.DataSource = dataset;

			ddlAirTruckID.DataTextField = dataset.Tables[0].Columns["truckid"].ToString() ;
			ddlAirTruckID.DataValueField = dataset.Tables[0].Columns["truckid"].ToString() ;
			this.ddlAirTruckID.DataBind();
			ListItem defItem = new ListItem();
			defItem.Text = "";
			
			defItem.Value = "";
			ddlAirTruckID.Items.Insert(0,(defItem));
		}

//		public void LoadAirTruckforbatchtypeA()
//		{
//			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
//			String strAppID = util.GetAppID();
//			String strEnterpriseID = util.GetEnterpriseID();
//
//			DataSet dataset = com.ties.classes.DbComboDAL.TruckIDforbatchtypeAQuery(strAppID,strEnterpriseID,location);	
//			this.ddlAirTruckID.DataSource = dataset;
//
//			ddlAirTruckID.DataTextField = dataset.Tables[0].Columns["truckid"].ToString() ;
//			ddlAirTruckID.DataValueField = dataset.Tables[0].Columns["truckid"].ToString() ;
//			this.ddlAirTruckID.DataBind();
//			ListItem defItem = new ListItem();
//			defItem.Text = "";
//			
//			defItem.Value = "";
//			ddlAirTruckID.Items.Insert(0,(defItem));
//		}
		private void btnSave_Click(object sender, System.EventArgs e)
		{
			CheckRawScan(batch_no,ddlDepartureDC.SelectedItem.Text);
			string airdate="";
			string tacdt = "";
			//string tardt ="";

			// Thosapol Yennam (04/09/2013) get configuration is DeparturesArrivals
			string ConfigDeparturesArrivals = BatchManifest.IsConfigDeparturesArrivals(m_strAppID,m_strEnterpriseID);
			
			DataSet conac = BatchManifest.GetTracking_Datetime(m_strAppID,m_strEnterpriseID,batch_no,ddlDepartureDC.SelectedItem.Text );
			if (conac.Tables[0].Rows.Count > 0)
			{
				tacdt =  String.Format("{0:dd/MM/yyyy HH:mm}",conac.Tables[0].Rows[0]["tracking_datetime"]);
				
			}
			else
			{
				tacdt = null;
			}

			
			DataSet conRev = BatchManifest.GetCountConsignment(m_strAppID,m_strEnterpriseID,"SOP",batch_no,ddlDepartureDC.SelectedItem.Text );
			//string c_con ;
			if (conRev.Tables[0].Rows.Count > 0 )
			{
				ViewState["c_con"] = conRev.Tables[0].Rows[0]["c_count"].ToString(); 
			}
			else
			{
				ViewState["c_con"] = "0";
			}

			if (btnName =="Departure")
			{
				if (tacdt==null)
				{
					if (txtActualDateDT.Text !="")
					{
						if (Session["batch_type"].ToString() =="A" )
						{
							string strFightNo = "";
							if(txtFightNo.Text.Length >= 2)
								strFightNo = txtFightNo.Text.Substring(0,2);
							
							string strAWBNo = "";
							if(txtAWBNo.Text.Length >= 2)
								strAWBNo = txtAWBNo.Text.Substring(0,2);

							if (txtActualDateDT.Text =="")
							{
								lblValISector.Text ="Enter a valid datetime for the departure.";
							}
//							else if (ddlDriverID.SelectedIndex  ==0 )
//							{
//								lblValISector.Text ="Select Driver ID from the list.";
//							}
							else if (txtArrivalDate.Text =="")
							{
								lblValISector.Text ="Enter a valid datetime for the estimated arrival.";
							}
							else if (CheckInputDatetime(txtActualDateDT.Text,"") =="0")
							{
								lblValISector.Text ="Actual departure date/time must be in the past and within the last 24 hours.";
							}
							else if (CheckInputDatetime(txtArrivalDate.Text,"est") =="0" && ConfigDeparturesArrivals == "N" ) // Thosapol Yennam (04/09/2013) Add check config DeparturesArrivals
							{
								lblValISector.Text ="Estimated arrival date/time must be in the future and within 24 hours.";
							}
							else if ((string)ViewState["CheckRawScan"]=="N")
							{
								lblValISector.Text ="No manifest exists for this departure DC";
							}
							else if (CheckEstDateTime(txtActualDateDT.Text,txtArrivalDate.Text) =="0" && ConfigDeparturesArrivals == "N" ) // Thosapol Yennam (04/09/2013) Add check config DeparturesArrivals
							{
								lblValISector.Text ="Estimated arrival date/time must greater than Actual departure date/time";
							}
							else if (txtFightNo.Text == "" )
							{
								lblValISector.Text ="Flight Number is required.";
							}
							else if(strFightNo!= "" && (strFightNo != "PX" && (strFightNo != "CG") && (strFightNo != "TA") ))
							{
								lblValISector.Text ="Flight Number Format Incorrect.";
							}
							else if(strAWBNo != "" && (strAWBNo != "PX" && (strAWBNo != "CG") && (strAWBNo != "TA")))
							{
								lblValISector.Text ="AWB Number Format Incorrect.";
							}
							else if (txtAirDepDT.Text=="")
							{
								lblValISector.Text ="Enter a valid date and time for the Flight's Departure.";
							}
							else if (CheckEstDateTime(txtActualDateDT.Text,txtAirDepDT.Text ) =="0" && ConfigDeparturesArrivals == "N" ) // Thosapol Yennam (04/09/2013) Add check config DeparturesArrivals
							{
								lblValISector.Text ="Actual Departure D/T must be less than the Flight Departure D/T.";
							}
							else if (CheckAirDateTime(txtActualDateDT.Text , txtArrivalDate.Text,txtAirDepDT.Text) =="0" && ConfigDeparturesArrivals == "N" ) // Thosapol Yennam (04/09/2013) Add check config DeparturesArrivals
							{
								lblValISector.Text ="Flight Departure D/T must be less than the Estimated Arrival D/T.";
							}
							else 
							{
								int result = BatchManifest.UpdateArrivalDeparture( m_strAppID,m_strEnterpriseID,batch_no,"Departure"
									,ddlDepartureDC.SelectedItem.Text ,txtActualDateDT.Text,txtDriverID.Text.Trim() 
									,txtArrivalDate.Text ,"","",txtFightNo.Text,txtAWBNo.Text,txtAirDepDT.Text ,"",""
									,lblUserDisplay.Text ,lblLocDisplay.Text );
								if (result==1)
								{
									lblValISector.Text = "Status code applied to "+ (string)ViewState["c_con"] +" consignment(s).";
									Session["CountTKL"] = lblValISector.Text;
								}

								String sScript = "";
								sScript += "<script language=javascript>";
								//							if(this.("BatchManifest_ControlPanel"))
								//							{
								sScript += "  window.opener.BatchManifest_ControlPanel.btnShowTKLHide.click();" ;
								//							}

								//String sScript = "";
								//sScript += "<script language=javascript>";
								sScript += "  window.close();";
								sScript += "</script>";
								Response.Write(sScript);
							}
						}
						else if (Session["batch_type"].ToString() =="L" )
						{
							DataSet conarrdt = BatchManifest.GetArrDTforTypeL(m_strAppID,m_strEnterpriseID,batch_no,ddlDepartureDC.SelectedItem.Text);
							string arrdt  ="";
							if (conarrdt.Tables[0].Rows.Count > 0)
							{
								arrdt =  String.Format("{0:dd/MM/yyyy HH:mm}",conarrdt.Tables[0].Rows[0]["arr_dt"]);
			
							}
							else
							{
								arrdt = null;
							}
							if (Convert.ToDateTime(ConDatetime(txtActualDateDT.Text)) <= Convert.ToDateTime(ConDatetime(arrdt)))
							{
								lblValISector.Text ="Departure date/time must be later than arrival date/time.";
							}
							else
							{

								if (txtActualDateDT.Text =="")
								{
									lblValISector.Text ="Enter a valid datetime for the departure.";
								}
//								else if (ddlDriverID.SelectedIndex  ==0 )
//								{
//									lblValISector.Text ="Select Driver ID from the list.";
//								}
								else if (txtArrivalDate.Text =="")
								{
									lblValISector.Text ="Enter a valid datetime for the estimated arrival.";
								}
								else if (CheckInputDatetime(txtActualDateDT.Text,"") =="0")
								{
									lblValISector.Text ="Actual departure date/time must be in the past and within the last 24 hours.";
								}
					
								else if (CheckInputDatetime(txtArrivalDate.Text,"est") =="0")
								{
									lblValISector.Text ="Estimated arrival date/time must be in the future and within  24 hours.";
								}
								else if ((string)ViewState["CheckRawScan"]=="N")
								{
									lblValISector.Text ="No manifest exists for this departure DC";
								}
								else if (CheckEstDateTime(txtActualDateDT.Text,txtArrivalDate.Text) =="0")
								{
									lblValISector.Text ="Estimated arrival date/time must greater than Actual departure date/time";

								}
								else
								{
									int result = BatchManifest.UpdateArrivalDeparture( m_strAppID,m_strEnterpriseID,batch_no,"Departure"
										,ddlDepartureDC.SelectedItem.Text ,txtActualDateDT.Text,txtDriverID.Text.Trim() 
										,txtArrivalDate.Text ,"","",txtFightNo.Text,txtAWBNo.Text,"null" ,"","" 
										,lblUserDisplay.Text ,lblLocDisplay.Text );
									if (result==1)
									{
										lblValISector.Text = "Status code applied to "+ (string)ViewState["c_con"] +" consignment(s). Please wait 5 - 10 minutes to view shipment tracking history.";
										Session["CountTKL"] = lblValISector.Text;
									}

									String sScript = "";
									sScript += "<script language=javascript>";
									sScript += "  window.opener.BatchManifest_ControlPanel.btnShowTKLHide.click();" ;
									//sScript += "<script language=javascript>";
									sScript += "  window.close();";
									sScript += "</script>";
									Response.Write(sScript);
								}
							}
					
						}
						else if (Session["batch_type"].ToString() =="S" || Session["batch_type"].ToString() =="W")
						{
							if (txtActualDateDT.Text =="")
							{
								lblValISector.Text ="Enter a valid datetime for the departure.";
							}
//							else if (ddlDriverID.SelectedIndex  ==0 )
//							{
//								lblValISector.Text ="Select Driver ID from the list.";
//							}
							else if (CheckInputDatetime(txtActualDateDT.Text,"") =="0")
							{
								lblValISector.Text ="Actual departure date/time must be in the past and within the last 24 hours.";
							}
							else if (CheckInputDatetime(txtArrivalDate.Text,"est") =="0")
							{
								lblValISector.Text ="Estimated arrival date/time must be in the future and within 24 hours.";
							}
							else if ((string)ViewState["CheckRawScan"]=="N")
							{
								lblValISector.Text ="No manifest exists for this departure DC";
							}
							else if (CheckEstDateTime(txtActualDateDT.Text,txtArrivalDate.Text) =="0")
							{
								lblValISector.Text ="Estimated arrival date/time must greater than Actual departure date/time";

							}
							else
							{
								if (txtArrivalDate.Text =="")
									airdate = "N";
								else
									airdate = txtArrivalDate.Text;

								int result = BatchManifest.UpdateArrivalDeparture( m_strAppID,m_strEnterpriseID,batch_no,"Departure"
									,ddlDepartureDC.SelectedItem.Text ,txtActualDateDT.Text,txtDriverID.Text.Trim()
									,airdate,"","",txtFightNo.Text,txtAWBNo.Text,"null" ,"",""
									,lblUserDisplay.Text ,lblLocDisplay.Text );
								if (result==1)
								{
									lblValISector.Text = "Status code applied to "+ (string)ViewState["c_con"] +" consignment(s). Please wait 5 - 10 minutes to view shipment tracking history.";
									Session["CountTKL"] = lblValISector.Text;
								}

								String sScript = "";
								sScript += "<script language=javascript>";
								sScript += "  window.opener.BatchManifest_ControlPanel.btnShowTKLHide.click();" ;
								//sScript += "<script language=javascript>";
								sScript += "  window.close();";
								sScript += "</script>";
								Response.Write(sScript);
							}
						}
					}
					else  //actual datetime=""
					{
						if (Session["batch_type"].ToString() =="A" )
						{
							if ((string)ViewState["CheckRawScan"]=="N")
							{
								lblValISector.Text ="No manifest exists for this departure DC";
							}
							else 
							{
								int result = BatchManifest.UpdateArrivalDeparture( m_strAppID,m_strEnterpriseID,batch_no,"Departure"
									,ddlDepartureDC.SelectedItem.Text ,null,null 
									,null ,"","",null,null,null ,"",""
									,lblUserDisplay.Text ,lblLocDisplay.Text );
								if (result==1)
								{
									lblValISector.Text = "Status code applied to "+ (string)ViewState["c_con"] +" consignment(s). Please wait 5 - 10 minutes to view shipment tracking history.";
									Session["CountTKL"] = lblValISector.Text;
								}

								String sScript = "";
								sScript += "<script language=javascript>";
								//							if(this.("BatchManifest_ControlPanel"))
								//							{
								sScript += "  window.opener.BatchManifest_ControlPanel.btnShowTKLHide.click();" ;
								//							}

								//String sScript = "";
								//sScript += "<script language=javascript>";
								sScript += "  window.close();";
								sScript += "</script>";
								Response.Write(sScript);
							}
						}
						else if (Session["batch_type"].ToString() =="L" )
						{
							if ((string)ViewState["CheckRawScan"]=="N")
							{
								lblValISector.Text ="No manifest exists for this departure DC";
							}
							else
							{
								int result = BatchManifest.UpdateArrivalDeparture( m_strAppID,m_strEnterpriseID,batch_no,"Departure"
									,ddlDepartureDC.SelectedItem.Text ,null,null 
									,null ,"","",null,null,"null" ,"","" 
									,lblUserDisplay.Text ,lblLocDisplay.Text );
								if (result==1)
								{
									lblValISector.Text = "Status code applied to "+ (string)ViewState["c_con"] +" consignment(s). Please wait 5 - 10 minutes to view shipment tracking history.";
									Session["CountTKL"] = lblValISector.Text;
								}

								String sScript = "";
								sScript += "<script language=javascript>";
								sScript += "  window.opener.BatchManifest_ControlPanel.btnShowTKLHide.click();" ;
								//sScript += "<script language=javascript>";
								sScript += "  window.close();";
								sScript += "</script>";
								Response.Write(sScript);
							}
					
						}
						else if (Session["batch_type"].ToString() =="S" || Session["batch_type"].ToString() =="W")
						{
							if ((string)ViewState["CheckRawScan"]=="N")
							{
								lblValISector.Text ="No manifest exists for this departure DC";
							}
							else
							{
								if (txtArrivalDate.Text =="")
									airdate = "N";
								else
									airdate = txtArrivalDate.Text;

								int result = BatchManifest.UpdateArrivalDeparture( m_strAppID,m_strEnterpriseID,batch_no,"Departure"
									,ddlDepartureDC.SelectedItem.Text ,null,null
									,airdate,"","",null,null,"null" ,"",""
									,lblUserDisplay.Text ,lblLocDisplay.Text );
								if (result==1)
								{
									lblValISector.Text = "Status code applied to "+ (string)ViewState["c_con"] +" consignment(s). Please wait 5 - 10 minutes to view shipment tracking history.";
									Session["CountTKL"] = lblValISector.Text;
								}

								String sScript = "";
								sScript += "<script language=javascript>";
								sScript += "  window.opener.BatchManifest_ControlPanel.btnShowTKLHide.click();" ;
								//sScript += "<script language=javascript>";
								sScript += "  window.close();";
								sScript += "</script>";
								Response.Write(sScript);
							}
						}
					}
				
				}
				else
				{	
					if (txtActualDateDT.Text !="")
					{
						if (Convert.ToDateTime(ConDatetime(txtActualDateDT.Text)) < Convert.ToDateTime(ConDatetime(tacdt)))
						{
							lblValISector.Text ="The batch departure date/time cannot be earlier than " + tacdt +" (time of last scan to this batch)." ;
						}
						else
						{
							string strFightNo = "";
							if(txtFightNo.Text.Length >=2)
								strFightNo = txtFightNo.Text.Substring(0,2);
							
							string strAWBNo = "";
							if(txtAWBNo.Text.Length >=2)
								strAWBNo = txtAWBNo.Text.Substring(0,2);

							if (Session["batch_type"].ToString() =="A" )
							{
								if (txtActualDateDT.Text =="")
								{
									lblValISector.Text ="Enter a valid datetime for the departure.";
								}
//								else if (ddlDriverID.SelectedIndex  ==0 )
//								{
//									lblValISector.Text ="Select Driver ID from the list.";
//								}
								else if (txtArrivalDate.Text =="")
								{
									lblValISector.Text ="Enter a valid datetime for the estimated arrival.";
								}
								else if (CheckInputDatetime(txtActualDateDT.Text,"") =="0")
								{
									lblValISector.Text ="Actual departure date/time must be in the past and within the last 24 hours.";
								}
								else if (CheckInputDatetime(txtArrivalDate.Text,"est") =="0" && ConfigDeparturesArrivals == "N" ) // Thosapol Yennam (04/09/2013) Add check config DeparturesArrivals
								{
									lblValISector.Text ="Estimated arrival date/time must be in the future and within 24 hours.";
								}
								else if ((string)ViewState["CheckRawScan"]=="N")
								{
									lblValISector.Text ="No manifest exists for this departure DC";
								}
								else if (CheckEstDateTime(txtActualDateDT.Text,txtArrivalDate.Text) =="0" && ConfigDeparturesArrivals == "N" ) // Thosapol Yennam (04/09/2013) Add check config DeparturesArrivals
								{
									lblValISector.Text ="Estimated arrival date/time must greater than Actual departure date/time";
								}
								else if (txtFightNo.Text == "" )
								{
									lblValISector.Text ="Flight Number is required.";
								}
								else if(strFightNo != "" && (strFightNo != "PX" && (strFightNo != "CG") && (strFightNo != "TA")))
								{
									lblValISector.Text ="Flight Number Format Incorrect.";
								}
								else if(strAWBNo != "" && (strAWBNo != "PX" && (strAWBNo != "CG") && (strAWBNo != "TA")))
								{
									lblValISector.Text ="AWB Number Format Incorrect.";
								}
								else if (txtAirDepDT.Text=="")
								{
									lblValISector.Text ="Enter a valid date and time for the Flight's Departure.";
								}
								else if (CheckEstDateTime(txtActualDateDT.Text,txtAirDepDT.Text ) =="0" && ConfigDeparturesArrivals == "N" ) // Thosapol Yennam (04/09/2013) Add check config DeparturesArrivals
								{
									lblValISector.Text ="Actual Departure D/T must be less than the Flight Departure D/T.";
								}
								else if (CheckAirDateTime(txtActualDateDT.Text , txtArrivalDate.Text,txtAirDepDT.Text) =="0" && ConfigDeparturesArrivals == "N" ) // Thosapol Yennam (04/09/2013) Add check config DeparturesArrivals
								{
									lblValISector.Text ="Flight Departure D/T must be less than the Estimated Arrival D/T.";
								}
								else 
								{
									int result = BatchManifest.UpdateArrivalDeparture( m_strAppID,m_strEnterpriseID,batch_no,"Departure"
										,ddlDepartureDC.SelectedItem.Text ,txtActualDateDT.Text,txtDriverID.Text.Trim()
										,txtArrivalDate.Text ,"","",txtFightNo.Text,txtAWBNo.Text,txtAirDepDT.Text ,"",""
										,lblUserDisplay.Text ,lblLocDisplay.Text );
									if (result==1)
									{
										lblValISector.Text = "Status code applied to "+ (string)ViewState["c_con"] +" consignment(s). Please wait 5 - 10 minutes to view shipment tracking history.";
										Session["CountTKL"] = lblValISector.Text;
									}

									String sScript = "";
									sScript += "<script language=javascript>";
									//							if(this.("BatchManifest_ControlPanel"))
									//							{
									sScript += "  window.opener.BatchManifest_ControlPanel.btnShowTKLHide.click();" ;
									//							}

									//String sScript = "";
									//sScript += "<script language=javascript>";
									sScript += "  window.close();";
									sScript += "</script>";
									Response.Write(sScript);
								}
							}
							else if (Session["batch_type"].ToString() =="L" )
							{
								if (txtActualDateDT.Text =="")
								{
									lblValISector.Text ="Enter a valid datetime for the departure.";
								}
//								else if (ddlDriverID.SelectedIndex  ==0 )
//								{
//									lblValISector.Text ="Select Driver ID from the list.";
//								}
								else if (txtArrivalDate.Text =="")
								{
									lblValISector.Text ="Enter a valid datetime for the estimated arrival.";
								}
								else if (CheckInputDatetime(txtActualDateDT.Text,"") =="0")
								{
									lblValISector.Text ="Actual departure date/time must be in the past and within the last 24 hours.";
								}
					
								else if (CheckInputDatetime(txtArrivalDate.Text,"est") =="0")
								{
									lblValISector.Text ="Estimated arrival date/time must be in the future and within  24 hours.";
								}
								else if ((string)ViewState["CheckRawScan"]=="N")
								{
									lblValISector.Text ="No manifest exists for this departure DC";
								}
								else if (CheckEstDateTime(txtActualDateDT.Text,txtArrivalDate.Text) =="0")
								{
									lblValISector.Text ="Estimated arrival date/time must greater than Actual departure date/time";

								}
								else
								{
									int result = BatchManifest.UpdateArrivalDeparture( m_strAppID,m_strEnterpriseID,batch_no,"Departure"
										,ddlDepartureDC.SelectedItem.Text ,txtActualDateDT.Text,txtDriverID.Text.Trim() 
										,txtArrivalDate.Text ,"","",txtFightNo.Text,txtAWBNo.Text,"null" ,"","" 
										,lblUserDisplay.Text ,lblLocDisplay.Text );
									if (result==1)
									{
										lblValISector.Text = "Status code applied to "+ (string)ViewState["c_con"] +" consignment(s). Please wait 5 - 10 minutes to view shipment tracking history.";
										Session["CountTKL"] = lblValISector.Text;
									}

									String sScript = "";
									sScript += "<script language=javascript>";
									sScript += "  window.opener.BatchManifest_ControlPanel.btnShowTKLHide.click();" ;
									//sScript += "<script language=javascript>";
									sScript += "  window.close();";
									sScript += "</script>";
									Response.Write(sScript);
								}
					
							}
							else if (Session["batch_type"].ToString() =="S" || Session["batch_type"].ToString() =="W")
							{
								if (txtActualDateDT.Text =="")
								{
									lblValISector.Text ="Enter a valid datetime for the departure.";
								}
//								else if (ddlDriverID.SelectedIndex  ==0 )
//								{
//									lblValISector.Text ="Select Driver ID from the list.";
//								}
								else if (CheckInputDatetime(txtActualDateDT.Text,"") =="0")
								{
									lblValISector.Text ="Actual departure date/time must be in the past and within the last 24 hours.";
								}
								else if (CheckInputDatetime(txtArrivalDate.Text,"est") =="0")
								{
									lblValISector.Text ="Estimated arrival date/time must be in the future and within 24 hours.";
								}
								else if ((string)ViewState["CheckRawScan"]=="N")
								{
									lblValISector.Text ="No manifest exists for this departure DC";
								}
								else if (CheckEstDateTime(txtActualDateDT.Text,txtArrivalDate.Text) =="0")
								{
									lblValISector.Text ="Estimated arrival date/time must greater than Actual departure date/time";

								}
								else
								{
									if (txtArrivalDate.Text =="")
										airdate = "N";
									else
										airdate = txtArrivalDate.Text;

									int result = BatchManifest.UpdateArrivalDeparture( m_strAppID,m_strEnterpriseID,batch_no,"Departure"
										,ddlDepartureDC.SelectedItem.Text ,txtActualDateDT.Text,txtDriverID.Text.Trim()
										,airdate,"","",txtFightNo.Text,txtAWBNo.Text,"null" ,"",""
										,lblUserDisplay.Text ,lblLocDisplay.Text );
									if (result==1)
									{
										lblValISector.Text = "Status code applied to "+ (string)ViewState["c_con"] +" consignment(s). Please wait 5 - 10 minutes to view shipment tracking history.";
										Session["CountTKL"] = lblValISector.Text;
									}

									String sScript = "";
									sScript += "<script language=javascript>";
									sScript += "  window.opener.BatchManifest_ControlPanel.btnShowTKLHide.click();" ;
									//sScript += "<script language=javascript>";
									sScript += "  window.close();";
									sScript += "</script>";
									Response.Write(sScript);
								}
							}
						}
					}
					else //actual datetime =""
					{
						if (Session["batch_type"].ToString() =="A" )
						{

							if ((string)ViewState["CheckRawScan"]=="N")
							{
								lblValISector.Text ="No manifest exists for this departure DC";
							}
							else 
							{
								// Thosapol.y > Comment (28/06/2013)
								//int result = BatchManifest.UpdateArrivalDeparture( m_strAppID,m_strEnterpriseID,batch_no,"Departure"
								//	,ddlDepartureDC.SelectedItem.Text ,null,null 
								//	,null ,"","",null,null,null ,"",""
								//	,lblUserDisplay.Text ,lblLocDisplay.Text );

								// Thosapol.y > Modify (28/06/2013)
								int result = BatchManifest.UpdateArrivalDeparture( m_strAppID,m_strEnterpriseID,batch_no,"Departure"
									,ddlDepartureDC.SelectedItem.Text ,null,txtDriverID.Text.Trim() 
									,txtArrivalDate.Text ,"","",txtFightNo.Text,txtAWBNo.Text,txtAirDepDT.Text ,"",""
									,lblUserDisplay.Text ,lblLocDisplay.Text );
								if (result==1)
								{
									lblValISector.Text = "Status code applied to "+ (string)ViewState["c_con"] +" consignment(s). Please wait 5 - 10 minutes to view shipment tracking history.";
									Session["CountTKL"] = lblValISector.Text;
								}

								String sScript = "";
								sScript += "<script language=javascript>";
								sScript += "  window.opener.BatchManifest_ControlPanel.btnShowTKLHide.click();" ;
								sScript += "  window.close();";
								sScript += "</script>";
								Response.Write(sScript);
							}
						}
						else if (Session["batch_type"].ToString() =="L" )
						{
							if ((string)ViewState["CheckRawScan"]=="N")
							{
								lblValISector.Text ="No manifest exists for this departure DC";
							}
							else
							{
								int result = BatchManifest.UpdateArrivalDeparture( m_strAppID,m_strEnterpriseID,batch_no,"Departure"
									,ddlDepartureDC.SelectedItem.Text ,null,null 
									,null ,"","",null,null,"null" ,"","" 
									,lblUserDisplay.Text ,lblLocDisplay.Text );
								if (result==1)
								{
									lblValISector.Text = "Status code applied to "+ (string)ViewState["c_con"] +" consignment(s). Please wait 5 - 10 minutes to view shipment tracking history.";
									Session["CountTKL"] = lblValISector.Text;
								}

								String sScript = "";
								sScript += "<script language=javascript>";
								sScript += "  window.opener.BatchManifest_ControlPanel.btnShowTKLHide.click();" ;
								sScript += "  window.close();";
								sScript += "</script>";
								Response.Write(sScript);
							}
					
						}
						else if (Session["batch_type"].ToString() =="S" || Session["batch_type"].ToString() =="W")
						{
							if ((string)ViewState["CheckRawScan"]=="N")
							{
								lblValISector.Text ="No manifest exists for this departure DC";
							}
							else
							{
								if (txtArrivalDate.Text =="")
									airdate = "N";
								else
									airdate = txtArrivalDate.Text;

								int result = BatchManifest.UpdateArrivalDeparture( m_strAppID,m_strEnterpriseID,batch_no,"Departure"
									,ddlDepartureDC.SelectedItem.Text ,null,null
									,airdate,"","",null,null,"null" ,"",""
									,lblUserDisplay.Text ,lblLocDisplay.Text );
								if (result==1)
								{
									lblValISector.Text = "Status code applied to "+ (string)ViewState["c_con"] +" consignment(s). Please wait 5 - 10 minutes to view shipment tracking history.";
									Session["CountTKL"] = lblValISector.Text;
								}

								String sScript = "";
								sScript += "<script language=javascript>";
								sScript += "  window.opener.BatchManifest_ControlPanel.btnShowTKLHide.click();" ;
								sScript += "  window.close();";
								sScript += "</script>";
								Response.Write(sScript);
							}
						}
					}
				}
			}
			else // Arrival
			{
				DataSet conArr = BatchManifest.GetArrival_Datetime(m_strAppID,m_strEnterpriseID,batch_no,ddlArrivalDC.SelectedItem.Text );
				DataSet dest =BatchManifest.GetDestinationStation(m_strAppID,m_strEnterpriseID,(string)ViewState["batch_id"],(string)Session["batch_type"]);
				DataSet depdc =BatchManifest.GetDepdcforArrdc(m_strAppID,m_strEnterpriseID,batch_no,ddlArrivalDC.SelectedItem.Text );
				string tardt="";
				string depdcforarrdc="";
				if (conArr.Tables[0].Rows.Count > 0)
				{
					tardt = String.Format("{0:dd/MM/yyyy HH:mm}",conArr.Tables[0].Rows[0]["tracking_datetime"]);
				}
				else
				{
					tardt = null;
				}

				if (depdc.Tables[0].Rows.Count > 0  )
				{
					depdcforarrdc =depdc.Tables[0].Rows[0]["dep_dc"].ToString(); 
				}
				else
				{
					depdcforarrdc ="";
				}
				if (tardt==null)
				{
					if (txtActualDateAR.Text !="")
					{
						if (Convert.ToDateTime(ConDatetime(txtActualDateAR.Text)) < Convert.ToDateTime(ConDatetime(txtActualDateDT.Text)))
						{
							lblValISector.Text ="Arrival date/time cannot be earlier than Departure date/time." ;
						}
						else
						{
							if (Session["batch_type"].ToString() =="L"  )
							{
								if (txtActualDateAR.Text =="")
								{
									lblValISector.Text ="Enter a valid datetime for the Actual Arrival.";
								}
								else if (CheckInputDatetime(txtActualDateAR.Text,"") =="0")
								{
									lblValISector.Text ="Actual Arrival date/time must be in the past and within the last 24 hours.";
								}
								else if (ddlArrivalDC.SelectedItem.Text ==""  )
								{
									lblValISector.Text ="Select Arrival DC from the list.";
								}
								else
								{
									int result = BatchManifest.UpdateArrivalDeparture(m_strAppID,m_strEnterpriseID,batch_no
										,"Arrival",ViewState["LDep_DC"].ToString()  ,"","","" 
										,ddlArrivalDC.SelectedItem.Text ,txtActualDateAR.Text
										,"","",""
										,ddlAirTruckID.SelectedItem.Text,txtAirDriverID.Text.Trim(),lblUserDisplay.Text ,lblLocDisplay.Text);
									String sScript = "";
									sScript += "<script language=javascript>";
									sScript += "  window.close();";
									sScript += "</script>";
									Response.Write(sScript);
								}
							}
							else if (Session["batch_type"].ToString() =="A")
							{
								if (dest.Tables[0].Rows[0]["destination_station"].ToString() != ddlArrivalDC.SelectedItem.Text)
								{
									lblValISector.Text ="Arrival for batch must be at destination DC for this batch.";
								}
								else
								{
									if (txtActualDateAR.Text =="")
									{
										lblValISector.Text ="Enter a valid datetime for the Actual Arrival.";
									}
									else if (CheckInputDatetime(txtActualDateAR.Text,"") =="0")
									{
										lblValISector.Text ="Actual Arrival date/time must be in the past and within the last 24 hours.";
									}
									else if (ddlArrivalDC.SelectedItem.Text ==""  )
									{
										lblValISector.Text ="Select Arrival DC from the list.";
									}
									else if (CheckEstDateTime((string)ViewState["txtAirDepDT"],txtActualDateAR.Text) =="0")
									{
										lblValISector.Text ="Actual Arrival date/time must greater than departure date/time";
									}
									else if (ddlAirTruckID.SelectedItem.Text =="" )
									{
										lblValISector.Text ="Select Arriving Truck ID from the list.";
									}
//									else if (ddlAirDriverID.SelectedItem.Text =="" )
//									{
//										lblValISector.Text ="Select Driver ID from the list.";
//									}
									else
									{
										int result = BatchManifest.UpdateArrivalDeparture(m_strAppID,m_strEnterpriseID,batch_no
											,"Arrival","" ,"","","" 
											,ddlArrivalDC.SelectedItem.Text ,txtActualDateAR.Text
											,"","",""
											,ddlAirTruckID.SelectedItem.Text,txtAirDriverID.Text.Trim() ,lblUserDisplay.Text ,lblLocDisplay.Text);
										String sScript = "";
										sScript += "<script language=javascript>";
										sScript += "  window.close();";
										sScript += "</script>";
										Response.Write(sScript);
									}
								}
							}
							else if (Session["batch_type"].ToString() =="S" && (string)ViewState["txtArrivalDate"]=="")
							{
								if (txtActualDateAR.Text =="")
								{
									lblValISector.Text ="Enter a valid datetime for the Actual Arrival.";
								}
								else if (CheckInputDatetime(txtActualDateAR.Text,"") =="0")
								{
									lblValISector.Text ="Actual Arrival date/time must be in the past and within the last 24 hours.";
								}
								else if (ddlArrivalDC.SelectedItem.Text ==""  )
								{
									lblValISector.Text ="Select Arrival DC from the list.";
								}
								else
								{
									int result = BatchManifest.UpdateArrivalDeparture(m_strAppID,m_strEnterpriseID,batch_no
										,"Arrival","" ,"","","" 
										,ddlArrivalDC.SelectedItem.Text ,txtActualDateAR.Text
										,"","",""
										,ddlAirTruckID.SelectedItem.Text,txtAirDriverID.Text.Trim(),lblUserDisplay.Text ,lblLocDisplay.Text);
									String sScript = "";
									sScript += "<script language=javascript>";
									sScript += "  window.close();";
									sScript += "</script>";
									Response.Write(sScript);
								}
							}
							else if (Session["batch_type"].ToString() =="S" && (string)ViewState["txtArrivalDate"]!="")
							{
								if (txtActualDateAR.Text =="")
								{
									lblValISector.Text ="Enter a valid datetime for the Actual Arrival.";
								}
								else if (CheckInputDatetime(txtActualDateAR.Text,"") =="0")
								{
									lblValISector.Text ="Actual Arrival date/time must be in the past and within the last 24 hours.";
								}
								else if (ddlArrivalDC.SelectedItem.Text ==""  )
								{
									lblValISector.Text ="Select Arrival DC from the list.";
								}
								else
								{
									int result = BatchManifest.UpdateArrivalDeparture(m_strAppID,m_strEnterpriseID,batch_no
										,"Arrival","" ,"","","" 
										,ddlArrivalDC.SelectedItem.Text ,txtActualDateAR.Text
										,"","",""
										,ddlAirTruckID.SelectedItem.Text,txtAirDriverID.Text.Trim(),lblUserDisplay.Text ,lblLocDisplay.Text);
									String sScript = "";
									sScript += "<script language=javascript>";
									sScript += "  window.close();";
									sScript += "</script>";
									Response.Write(sScript);
								}
							}
						}
					}
					else //txtActualDateAR.Text ==""
					{
						if (Session["batch_type"].ToString() =="L")
						{
							if (depdcforarrdc == ddlArrivalDC.SelectedItem.Text && ddlArrivalDC.SelectedItem.Text !=  dest.Tables[0].Rows[0]["destination_station"].ToString())
							{
								lblValISector.Text ="Cannot cancel this arrival record.";
							}
							else
							{
								int result = BatchManifest.UpdateArrivalDeparture(m_strAppID,m_strEnterpriseID,batch_no
									,"Arrival",ViewState["LDep_DC"].ToString()  ,"","","" 
									,"" ,null
									,"","",""
									,ddlAirTruckID.SelectedItem.Text,txtAirDriverID.Text.Trim(),lblUserDisplay.Text ,lblLocDisplay.Text);
								String sScript = "";
								sScript += "<script language=javascript>";
								sScript += "  window.close();";
								sScript += "</script>";
								Response.Write(sScript);
							}
						}
						else if(Session["batch_type"].ToString() =="S")
						{
							int result = BatchManifest.UpdateArrivalDeparture(m_strAppID,m_strEnterpriseID,batch_no
								,"Arrival","" ,"","","" 
								,"" ,null
								,"","",""
								,ddlAirTruckID.SelectedItem.Text,txtAirDriverID.Text.Trim(),lblUserDisplay.Text ,lblLocDisplay.Text);
							String sScript = "";
							sScript += "<script language=javascript>";
							sScript += "  window.close();";
							sScript += "</script>";
							Response.Write(sScript);
						}
						else if (Session["batch_type"].ToString() =="A")
						{
							int result = BatchManifest.UpdateArrivalDeparture(m_strAppID,m_strEnterpriseID,batch_no
								,"Arrival","" ,"","","" 
								,"" ,null
								,"","",""
								,ddlAirTruckID.SelectedItem.Text,txtAirDriverID.Text.Trim() ,lblUserDisplay.Text ,lblLocDisplay.Text);
							String sScript = "";
							sScript += "<script language=javascript>";
							sScript += "  window.close();";
							sScript += "</script>";
							Response.Write(sScript);
						}
					}
				}
				else //tardt != ""
				{
					if (txtActualDateAR.Text =="")
					{
						lblValISector.Text ="Enter a valid datetime for the Actual Arrival.";
					}
					else
					{
						if (Convert.ToDateTime(ConDatetime(txtActualDateAR.Text)) > Convert.ToDateTime(ConDatetime(tardt)))
						{
							lblValISector.Text ="Arrival date/time cannot be later than " + tardt +" (time of first SIP scan to this batch)." ;
						}
						else
						{
							if (Session["batch_type"].ToString() =="L"  )
							{
								if (txtActualDateAR.Text =="")
								{
									lblValISector.Text ="Enter a valid datetime for the Actual Arrival.";
								}
								else if (CheckInputDatetime(txtActualDateAR.Text,"") =="0")
								{
									lblValISector.Text ="Actual Arrival date/time must be in the past and within the last 24 hours.";
								}
								else if (ddlArrivalDC.SelectedItem.Text ==""  )
								{
									lblValISector.Text ="Select Arrival DC from the list.";
								}
								else
								{
									int result = BatchManifest.UpdateArrivalDeparture(m_strAppID,m_strEnterpriseID,batch_no
										,"Arrival",ViewState["LDep_DC"].ToString()  ,"","","" 
										,ddlArrivalDC.SelectedItem.Text ,txtActualDateAR.Text
										,"","",""
										,ddlAirTruckID.SelectedItem.Text,txtAirDriverID.Text.Trim(),lblUserDisplay.Text ,lblLocDisplay.Text);
								
									DataSet dsCheck = BatchManifest.GetRowScan(m_strAppID,m_strEnterpriseID,batch_no,ddlArrivalDC.SelectedValue.ToString().Trim());
									if(dsCheck!=null)
									{
										if(dsCheck.Tables[0].Rows.Count>0)
										{
											SWB_Emulator.SWB_Batch_Manifest_Update(m_strAppID,m_strEnterpriseID,batch_no,lblUserDisplay.Text,"0");
										}
									}

									String sScript = "";
									sScript += "<script language=javascript>";
									sScript += "  window.close();";
									sScript += "</script>";
									Response.Write(sScript);
								}
							}
							else if (Session["batch_type"].ToString() =="A")
							{
								if (txtActualDateAR.Text =="")
								{
									lblValISector.Text ="Enter a valid datetime for the Actual Arrival.";
								}
								else if (CheckInputDatetime(txtActualDateAR.Text,"") =="0")
								{
									lblValISector.Text ="Actual Arrival date/time must be in the past and within the last 24 hours.";
								}
								else if (ddlArrivalDC.SelectedItem.Text ==""  )
								{
									lblValISector.Text ="Select Arrival DC from the list.";
								}
								else if (CheckEstDateTime((string)ViewState["txtAirDepDT"],txtActualDateAR.Text) =="0")
								{
									lblValISector.Text ="Actual Arrival date/time must greater than departure date/time";
								}
								else if (ddlAirTruckID.SelectedItem.Text =="" )
								{
									lblValISector.Text ="Select Arriving Truck ID from the list.";
								}
//								else if (ddlAirDriverID.SelectedItem.Text =="" )
//								{
//									lblValISector.Text ="Select Driver ID from the list.";
//								}
								else
								{
									int result = BatchManifest.UpdateArrivalDeparture(m_strAppID,m_strEnterpriseID,batch_no
										,"Arrival","" ,"","","" 
										,ddlArrivalDC.SelectedItem.Text ,txtActualDateAR.Text
										,"","",""
										,ddlAirTruckID.SelectedItem.Text,txtAirDriverID.Text.Trim(),lblUserDisplay.Text ,lblLocDisplay.Text);
									String sScript = "";
									sScript += "<script language=javascript>";
									sScript += "  window.close();";
									sScript += "</script>";
									Response.Write(sScript);
								}
							}
							else if (Session["batch_type"].ToString() =="S" && (string)ViewState["txtArrivalDate"]=="")
							{
								if (txtActualDateAR.Text =="")
								{
									lblValISector.Text ="Enter a valid datetime for the Actual Arrival.";
								}
								else if (CheckInputDatetime(txtActualDateAR.Text,"") =="0")
								{
									lblValISector.Text ="Actual Arrival date/time must be in the past and within the last 24 hours.";
								}
								else if (ddlArrivalDC.SelectedItem.Text ==""  )
								{
									lblValISector.Text ="Select Arrival DC from the list.";
								}
								else
								{
									int result = BatchManifest.UpdateArrivalDeparture(m_strAppID,m_strEnterpriseID,batch_no
										,"Arrival","" ,"","","" 
										,ddlArrivalDC.SelectedItem.Text ,txtActualDateAR.Text
										,"","",""
										,ddlAirTruckID.SelectedItem.Text,txtAirDriverID.Text.Trim(),lblUserDisplay.Text ,lblLocDisplay.Text);
								
									DataSet dsCheck = BatchManifest.GetRowScan(m_strAppID,m_strEnterpriseID,batch_no,ddlArrivalDC.SelectedValue.ToString().Trim());
									if(dsCheck!=null)
									{
										if(dsCheck.Tables[0].Rows.Count>0)
										{
											SWB_Emulator.SWB_Batch_Manifest_Update(m_strAppID,m_strEnterpriseID,batch_no,lblUserDisplay.Text,"0");
										}
									}
									String sScript = "";
									sScript += "<script language=javascript>";
									sScript += "  window.close();";
									sScript += "</script>";
									Response.Write(sScript);
								}
							}
							else if (Session["batch_type"].ToString() =="S" && (string)ViewState["txtArrivalDate"]!="")
							{
								if (txtActualDateAR.Text =="")
								{
									lblValISector.Text ="Enter a valid datetime for the Actual Arrival.";
								}
								else if (CheckInputDatetime(txtActualDateAR.Text,"") =="0")
								{
									lblValISector.Text ="Actual Arrival date/time must be in the past and within the last 24 hours.";
								}
								else if (ddlArrivalDC.SelectedItem.Text ==""  )
								{
									lblValISector.Text ="Select Arrival DC from the list.";
								}
								else
								{
									int result = BatchManifest.UpdateArrivalDeparture(m_strAppID,m_strEnterpriseID,batch_no
										,"Arrival","" ,"","","" 
										,ddlArrivalDC.SelectedItem.Text ,txtActualDateAR.Text
										,"","",""
										,ddlAirTruckID.SelectedItem.Text,txtAirDriverID.Text.Trim(),lblUserDisplay.Text ,lblLocDisplay.Text);
								
									DataSet dsCheck = BatchManifest.GetRowScan(m_strAppID,m_strEnterpriseID,batch_no,ddlArrivalDC.SelectedValue.ToString().Trim());
									if(dsCheck!=null)
									{
										if(dsCheck.Tables[0].Rows.Count>0)
										{
											SWB_Emulator.SWB_Batch_Manifest_Update(m_strAppID,m_strEnterpriseID,batch_no,lblUserDisplay.Text,"0");
										}
									}

									String sScript = "";
									sScript += "<script language=javascript>";
									sScript += "  window.close();";
									sScript += "</script>";
									Response.Write(sScript);
								}
							}
						}
					}
				}

			}
			
		
		
	}
		
		private string CheckInputDatetime(string date_time,string est)
		{
			string result ="";
			if (date_time !="")
			{
				string d = date_time.Substring(0,2);
				string M = date_time.Substring(3,2);
				string y = date_time.Substring(6,4);
				string H = date_time.Substring(11,2);
				string m = date_time.Substring(14,2);
				string t = date_time.Substring(11,5);
				string d_new = M+"/"+d+"/"+y+" "+t+":"+"0";
				TimeSpan dt = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy HH:mm:s")).Subtract(Convert.ToDateTime(d_new)) ;
//
				if (est == "est")
				{
					if (Convert.ToDateTime(d_new) < Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy HH:mm:s")))
					{
						result = "0";
					}
					else if (Convert.ToDateTime(d_new) > Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy HH:mm:s")))
					{
						if (Convert.ToDateTime(d_new) > DateTime.Now.AddHours(24) )
							result = "0";
						else
							result = "1";
					}
				}
				else
				{
					if (Convert.ToDateTime(d_new) < Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy HH:mm:s")))
					{
						if (Convert.ToDateTime(d_new) < DateTime.Now.AddHours(-24))
							result = "0";
					}
					else if (Convert.ToDateTime(d_new) >= Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy HH:mm:s")))
					{
						result = "0";
					}
				
						
//						if (Convert.ToDateTime(d_new) > DateTime.Now.AddHours(24) )
//							result = "0";
//						else
//							result = "1";
//						
//					}
				}
			}
			return result;
		}

		private string CheckEstDateTime(string actual_date,string est_date)
		{
			string result ="";
			if ( actual_date!="" && est_date !="")
			{							 
				string d = actual_date.Substring(0,2);
				string M = actual_date.Substring(3,2);
				string y = actual_date.Substring(6,4);
				string H = actual_date.Substring(11,2);
				string m = actual_date.Substring(14,2);
				string t = actual_date.Substring(11,5);

				string d1 = est_date.Substring(0,2);
				string M1 = est_date.Substring(3,2);
				string y1 = est_date.Substring(6,4);
				string H1= est_date.Substring(11,2);
				string m1 = est_date.Substring(14,2);
				string t1 = est_date.Substring(11,5);
				string d_actual = M+"/"+d+"/"+y+" "+t+":"+"0";
				string d_est = M1+"/"+d1+"/"+y1+" "+t1+":"+"0";
				if (Convert.ToDateTime(d_est) <= Convert.ToDateTime(d_actual))
				{
					result = "0";
				}
				else
				{
					result = "1";
				}
			}

			return result;
		}
		private string ConDatetime(string date_time)
		{
			string result ="";
			if (date_time !="")
			{
				string d = date_time.Substring(0,2);
				string M = date_time.Substring(3,2);
				string y = date_time.Substring(6,4);
				string H = date_time.Substring(11,2);
				string m = date_time.Substring(14,2);
				string t = date_time.Substring(11,5);
				string d_new = M+"/"+d+"/"+y+" "+t+":"+"0";
				result = d_new;
			}
			return result;
		}
		private string CheckAirDateTime(string actual_date,string est_date,string air_date)
		{
			string result ="";
			if ( actual_date!="" && est_date !="" && air_date !="")
			{							 
				string d = actual_date.Substring(0,2);
				string M = actual_date.Substring(3,2);
				string y = actual_date.Substring(6,4);
				string H = actual_date.Substring(11,2);
				string m = actual_date.Substring(14,2);
				string t = actual_date.Substring(11,5);

				string d1 = est_date.Substring(0,2);
				string M1 = est_date.Substring(3,2);
				string y1 = est_date.Substring(6,4);
				string H1= est_date.Substring(11,2);
				string m1 = est_date.Substring(14,2);
				string t1 = est_date.Substring(11,5);

				string d2 = air_date.Substring(0,2);
				string M2 = air_date.Substring(3,2);
				string y2 = air_date.Substring(6,4);
				string H2= air_date.Substring(11,2);
				string m2 = air_date.Substring(14,2);
				string t2 = air_date.Substring(11,5);

				string d_actual = M+"/"+d+"/"+y+" "+t+":"+"0";
				string d_est = M1+"/"+d1+"/"+y1+" "+t1+":"+"0";
				string d_air = M2+"/"+d2+"/"+y2+" "+t2+":"+"0";
				if (Convert.ToDateTime(d_actual) < Convert.ToDateTime(d_air) && Convert.ToDateTime(d_air) < Convert.ToDateTime(d_est))
				{
					result = "1";
				}
				else
				{
					result = "0";
				}
			}

			return result;
		}

//		private string CheckAirDateTime(string actual_date,string air_date)
//		{
//			string result ="";
//			if ( actual_date!="" && est_date !="")
//			{							 
//				string d = actual_date.Substring(0,2);
//				string M = actual_date.Substring(3,2);
//				string y = actual_date.Substring(6,4);
//				string H = actual_date.Substring(11,2);
//				string m = actual_date.Substring(14,2);
//				string t = actual_date.Substring(11,5);
//
//				string d1 = air_date.Substring(0,2);
//				string M1 = air_date.Substring(3,2);
//				string y1 = air_date.Substring(6,4);
//				string H1= air_date.Substring(11,2);
//				string m1 = air_date.Substring(14,2);
//				string t1 = air_date.Substring(11,5);
//				string d_actual = M+"/"+d+"/"+y+" "+t+":"+"0";
//				string d_est = M1+"/"+d1+"/"+y1+" "+t1+":"+"0";
//				if (Convert.ToDateTime(d_est) <= Convert.ToDateTime(d_actual))
//				{
//					result = "0";
//				}
//				else
//				{
//					result = "1";
//				}
//			}
//
//			return result;
//		}

		private void CheckRawScan(string batch_no,string location)
		{
//			DataSet cRowScan = BatchManifest.GetRowScan( m_strAppID,m_strEnterpriseID,batch_no,location);
//
//			if (cRowScan.Tables[0].Rows.Count > 0)
//			{
//				if (cRowScan.Tables[0].Rows[0]["cbatch_no"].ToString() =="0" )
//					ViewState["CheckRawScan"] = "N";
//				else
					ViewState["CheckRawScan"] = "Y";
//			}
		}

		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openLargerChildParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
		private void btnDriverIDSrch_Click(object sender, System.EventArgs e)
		{
			//LoadDriverIDAll();
			OpenWindowpage("Staff.aspx?FORMID=PopUp_Arrival_Departure");
		}

		private void btnRevCom_Click(object sender, System.EventArgs e)
		{
			DataSet con = BatchManifest.GetCountConsignment(m_strAppID,m_strEnterpriseID,"SOP",batch_no,ddlArrivalDC.SelectedItem.Text);
			string c_con ;
			if (con.Tables[0].Rows.Count > 0 )
			{
				c_con = con.Tables[0].Rows[0]["c_count"].ToString(); 
			}
			else
			{
				c_con = "0";
			}
			DataSet conArr = BatchManifest.GetArrival_Datetime(m_strAppID,m_strEnterpriseID,batch_no,ddlArrivalDC.SelectedItem.Text );
			string tardt="";
			if (conArr.Tables[0].Rows.Count > 0)
			{
				tardt = String.Format("{0:dd/MM/yyyy HH:mm}",conArr.Tables[0].Rows[0]["tracking_datetime"]);
			}
			else
			{
				tardt = null;
			}
				
			if (tardt!=null)
			{
				if (txtActualDateAR.Text !="")
				{
					if (Convert.ToDateTime(ConDatetime(txtActualDateAR.Text)) > Convert.ToDateTime(ConDatetime(tardt)))
					{
						lblValISector.Text ="Arrival date/time cannot be later than " + tardt +" (time of first SIP scan to this batch). Please correct the arrival date/time before processing Receiving is Complete" ;
					}
					else
					{
						int result = BatchManifest.InsertReceivingComplete( m_strAppID,m_strEnterpriseID,batch_no,ddlArrivalDC.SelectedItem.Text,txtActualDateAR.Text,lblTruckIDDisplay.Text,lblBatchIDDisplay.Text,(string)ViewState["DriverID_"],lblUserDisplay.Text,lblLocDisplay.Text,(string)Session["batch_type"]);
						if (result==1)
						{
							lblValISector.Text = "Status code applied to "+ c_con +" consignment(s). Please wait 5 - 10 minutes to view shipment tracking history.";
						}
					}
				}
			}
			else
			{
				int result = BatchManifest.InsertReceivingComplete( m_strAppID,m_strEnterpriseID,batch_no,ddlArrivalDC.SelectedItem.Text,txtActualDateAR.Text,lblTruckIDDisplay.Text,lblBatchIDDisplay.Text,(string)ViewState["DriverID_"],lblUserDisplay.Text,lblLocDisplay.Text,(string)Session["batch_type"]);
				if (result==1)
				{
					lblValISector.Text = "Status code applied to "+ c_con +" consignment(s). Please wait 5 - 10 minutes to view shipment tracking history.";
				}
			}
					
			




		}

		private void hdnbox_TextChanged(object sender, System.EventArgs e)
		{
			string DriverID ="";
			if (hdnbox.Text[0] =='Y') // if the value return from javascript is yes then update
			{
//				if (ddlDriverID.SelectedIndex == -1)
//					DriverID ="";
//				else
//					DriverID = ddlDriverID.SelectedItem.Text ; 
				DriverID = txtDriverID.Text.Trim();
				int result = BatchManifest.InsertReceivingComplete( m_strAppID,m_strEnterpriseID,batch_no,ddlArrivalDC.SelectedItem.Text,txtActualDateAR.Text,lblTruckIDDisplay.Text,lblBatchIDDisplay.Text,DriverID,lblUserDisplay.Text,lblLocDisplay.Text,(string)Session["batch_type"]);

			}
			else
			{

			}
		}

		private void txtActualDateAR_TextChanged(object sender, System.EventArgs e)
		{
			btnRevCom.Enabled = true;
		}

		private void btnEnableRevCom_Click(object sender, System.EventArgs e)
		{
			int result = BatchManifest.UpdateArrivalDeparture( m_strAppID,m_strEnterpriseID,batch_no,"Departure"
				,ddlDepartureDC.SelectedItem.Text ,txtActualDateDT.Text,txtDriverID.Text.Trim() 
				,txtArrivalDate.Text ,"","",txtFightNo.Text,txtAWBNo.Text,txtAirDepDT.Text ,"",""
				,lblUserDisplay.Text ,lblLocDisplay.Text );
		}

		private void btnAirTruckSearch_Click(object sender, System.EventArgs e)
		{
			LoadTruck();
			OpenWindowpage("Vehicles.aspx?FORMID=PopUp_Arrival_Departure&AirTruck=ddlAirTruckID");
		}

		private void btnAirDriverSearch_Click(object sender, System.EventArgs e)
		{
//			LoadAirDriverIDAll(); 
			OpenWindowpage("Staff.aspx?FORMID=PopUp_Arrival_Departure&AirDriver=ddlAirDriverID");
		}

		private void txtActualDateDT_TextChanged(object sender, System.EventArgs e)
		{
			if (txtValueActDT.Text != "" )
			{
				if (txtValueActDT.Text != txtActualDateDT.Text )
				{
					
				}
			}
			else
			{
			}

		}

		private void btnCancelChangeActual_Click(object sender, System.EventArgs e)
		{
			txtActualDateDT.Text  = txtValueActDT.Text;
		}

		private void ddlDepartureDC_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//LoadDriverID();
		}

		private void ddlArrivalDC_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			LoadAirTruck();
//			LoadAirDriverID(); 
		}
	}
}
