using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;
using com.ties.DAL;
using com.common.RBAC;

namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for PopUp_CreateUpdateBatch_Show.
	/// </summary>
	public class PopUp_CreateUpdateBatch_Show : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Button btnOk;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.RadioButtonList rdoBatchList;
		protected System.Web.UI.WebControls.Label Label1;
		String m_strAppID=null;
		String m_strEnterpriseID=null;
		protected System.Web.UI.WebControls.TextBox txtResult;
		protected System.Web.UI.WebControls.TextBox txtBatchID;
		protected System.Web.UI.WebControls.TextBox txtTruckID;
		protected System.Web.UI.WebControls.TextBox txtBatchType;
		protected System.Web.UI.WebControls.TextBox txtBatchNo;
		protected System.Web.UI.WebControls.TextBox txtBatchDate;

		private void Page_Load(object sender, System.EventArgs e)
		{
			getSessionTimeOut(true);
			btnCancel.Attributes.Add("onclick","setCancel();");
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			string batchId =Request.Params["batchId"];
			string truckId = Request.Params["truckId"];
			string batchDate = Request.Params["batchDate"];
		
			if(!Page.IsPostBack)
			{
				if(Session["Mode"]==null)
				{
				Session["Mode"]=Request.Params["resultTarget"];
				}
			Response.CacheControl="no-cache";
			DataSet ds = new DataSet();
			ds= SWB_Emulator.SWB_Batch_Manifest_Read(m_strAppID,m_strEnterpriseID,batchId,batchDate,truckId);
				
			if(ds.Tables[0].Rows.Count>0 && Session["Mode"].ToString()!="Update")
			{
				rdoBatchList.DataSource=ds;
				rdoBatchList.DataTextField="text";
				rdoBatchList.DataValueField="batch_no";
				rdoBatchList.DataBind();
				ViewState["ds"] = ds;
				
			}
			else
			{
				CloseWindow();
					
			}
				}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.rdoBatchList.SelectedIndexChanged += new System.EventHandler(this.rdoBatchList_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void rdoBatchList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			btnOk.Text ="Update";
			Session["Mode"]="Update";
			Session["Batch_No"]= rdoBatchList.SelectedValue;
			txtResult.Text= "Update";
			DataView dv = new DataView();
			DataSet ds = new DataSet();
			ds=(DataSet)ViewState["ds"];
			dv.Table = ds.Tables[0];
			dv.RowFilter="Batch_No='"+rdoBatchList.SelectedValue.ToString()+"'";
			if(dv.Count>0)
			{
				txtBatchNo.Text =rdoBatchList.SelectedValue;
				txtBatchID.Text = dv[0]["Batch_ID"].ToString();
				txtTruckID.Text =dv[0]["Truck_ID"].ToString();
				txtBatchDate.Text =dv[0]["Batch_Date"].ToString();
				//txtBatchType.Text =dv[0]["Batch_Type"].ToString();
				if(dv[0]["Batch_Type"].ToString().ToUpper()=="W")
				{
					txtBatchType.Text="Current Location";
				}
				else if (dv[0]["Batch_Type"].ToString().ToUpper()=="S")
				{
					txtBatchType.Text="Delivery";
				}
				else if (dv[0]["Batch_Type"].ToString().ToUpper()=="A")
				{
					txtBatchType.Text="Air";
				}
				else//L
				{
					txtBatchType.Text="Linehaul";
				}
			
			}

		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Session["Mode"]=null;	
			txtResult.Text = "Cancel";

			CloseWindow();
		}
		private void CloseWindow()
		{
			//Session["Mode"]=null;	
			String sScript = "";
			sScript += "<script language=javascript>";
			//sScript += "  document.getElementById('txtResult').value='Cancel';";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			if (btnOk.Text.ToString()=="Create")
			{
				Session["Mode"]="Create";
				txtResult.Text = "Create";
				CloseWindow();
			}
			else
			{
				Session["Mode"]="Update";
				Session["Batch_No"]= rdoBatchList.SelectedValue;
				txtResult.Text = "Update";
				CloseWindow();

			}

			
		}
	}
}
