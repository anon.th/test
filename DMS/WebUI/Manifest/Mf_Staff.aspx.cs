using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.ties.DAL;
using com.common.classes;
using com.common.applicationpages;
using System.Globalization;


namespace com.ties
{
	/// <summary>
	/// Summary description for Mf_Staff.
	/// </summary>
	public class Mf_Staff : BasePage
	{
		protected System.Web.UI.WebControls.Label lblMainheading;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;	

		private string m_strAppID = null;
		private string m_strEnterpriseID = null;
		protected System.Web.UI.WebControls.DataGrid dgStaff;

//		protected System.Web.UI.WebControls.ValidationSummary PageValid;
		SessionDS m_sdsStaff = null;
		Utility utility;


		private void Page_Load(object sender, System.EventArgs e)
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);

			String userCulTure = utility.GetUserCulture();
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();

			if(!IsPostBack)
			{
				DataSet profileDS = SysDataManager1.GetEnterpriseProfile(m_strAppID, m_strEnterpriseID);

				if(profileDS.Tables[0].Rows.Count > 0)
				{
					int currency_decimal = (int) profileDS.Tables[0].Rows[0]["currency_decimal"];
					ViewState["m_format"] = "{0:F" + currency_decimal.ToString() + "}";
				}				
				
				QueryMode();	  
			}
			else
			{
				m_sdsStaff = (SessionDS)Session["SESSION_DS_Staff"];
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		public void QueryMode()
		{
			ViewState["DPOperation"] = Operation.None;
			ViewState["DPMode"] = ScreenMode.Query;
			ViewState["EditMode"] = false;
  	
			btnQuery.Enabled = true;
			btnExecuteQuery.Enabled = true;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);

			m_sdsStaff = Staff.GetEmptyStaff();  

			m_sdsStaff.DataSetRecSize = 1;
			AddRow(); 
			Session["SESSION_DS_Staff"] = m_sdsStaff;

			BindDataGrid();

			LoadEmpType("employee_type");

			EditHRow(true);
//			dgStaff.Columns[0].Visible=false;
//			dgStaff.Columns[1].Visible=false;
		}

		
		public DataSet  LoadEmpType(string codeID)
		{
			
			DataSet dsEmpType = new DataSet();
			DataTable dtEmpType = new DataTable();
			DataRow dr;

			dtEmpType.Columns.Add("EmpTypeText",typeof(string));
			dtEmpType.Columns.Add("EmpTypeValue",typeof(string));
 
			ArrayList systemCodes = Utility.GetCodeValues(m_strAppID ,utility.GetUserCulture(),codeID,CodeValueType.StringValue);
			 
			dr = dtEmpType.NewRow();
			dr[1] = "";
			dr[0] = "";
			dtEmpType.Rows.Add(dr);

			if(systemCodes != null)
			{
				foreach(SystemCode sysCode in systemCodes)
				{
					dr = dtEmpType.NewRow();
					dr[1] = sysCode.Text;
					dr[0] = sysCode.StringValue;
					dtEmpType.Rows.Add(dr); 			
				}
			}
			dsEmpType.Tables.Add(dtEmpType);
			int cnt = dsEmpType.Tables [0].Rows.Count; 
			return dsEmpType;
		}

		
		private void AddRow()
		{
			Staff.AddNewRowStaffCodeDS(ref m_sdsStaff);
		}

		
		private void EditHRow(bool bItemIndex)
		{
			foreach (DataGridItem item in dgStaff.Items)
			{ 
				if (bItemIndex) 
				{
					dgStaff.EditItemIndex = item.ItemIndex; }
				else 
				{
					dgStaff.EditItemIndex = -1; }
			}
			dgStaff.DataBind();
		}

		
		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			if (btnExecuteQuery.Enabled == true)
			{
				btnExecuteQuery.Enabled = false;
			}
			if((int)ViewState["DPMode"] != (int)ScreenMode.Insert || m_sdsStaff.ds.Tables[0].Rows.Count >= dgStaff.PageSize)
			{
				m_sdsStaff = Staff.GetEmptyStaff();
				AddRow();				
			}
			else
			{
				AddRow();
			}
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,false,m_moduleAccessRights);
			EditHRow(false);
			ViewState["DPMode"] = ScreenMode.Insert;
			ViewState["DPOperation"] = Operation.Insert;
			dgStaff.Columns[0].Visible=true;
			dgStaff.Columns[1].Visible=true;
			dgStaff.EditItemIndex = m_sdsStaff.ds.Tables[0].Rows.Count - 1;
			dgStaff.CurrentPageIndex = 0;
			BindDataGrid();
			getPageControls(Page);
		}

		
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			dgStaff.CurrentPageIndex = 0;
			QueryMode();
		}
		
		
		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			btnExecuteQuery.Enabled = false;
			ViewState["DPMode"]=ScreenMode.ExecuteQuery; 

			//Since Query mode is always one row in edit mode, the rowindex is set to zero
			int rowIndex = 0; 
			GetChangedRow(dgStaff.Items[0], rowIndex);
			Session["QUERY_DS"] = m_sdsStaff.ds;
			DataRow dr = m_sdsStaff.ds.Tables[0].Rows[0];

			string codeID = null;

			codeID = "employee_type";

			LoadEmpType(codeID);
			RetreiveSelectedPage();
			if(m_sdsStaff.ds.Tables[0].Rows.Count==0)
			{
				lblErrorMessage.Text = "No Matching records found..";
				dgStaff.EditItemIndex = -1; 
				return;
			}
			//set all records to non-edit mode after execute query
			EditHRow(false);
			dgStaff.Columns[0].Visible = true;
			dgStaff.Columns[1].Visible = true;		
		}
		
		
		private void RetreiveSelectedPage()
		{
			int iStartRow = dgStaff.CurrentPageIndex * dgStaff.PageSize;

			DataSet dsQuery = (DataSet)Session["QUERY_DS"];

			m_sdsStaff = Staff.GetStaffCodeDS(utility.GetAppID(), utility.GetEnterpriseID(), iStartRow, dgStaff.PageSize, dsQuery);

			int iPageCnt = Convert.ToInt32((m_sdsStaff.QueryResultMaxSize - 1))/dgStaff.PageSize;

			if(iPageCnt < dgStaff.CurrentPageIndex)
			{
				dgStaff.CurrentPageIndex = System.Convert.ToInt32(iPageCnt);
			}
			
			dgStaff.SelectedIndex = -1;
			dgStaff.EditItemIndex = -1;

			BindDataGrid();
			Session["SESSION_DS_Staff"] = m_sdsStaff;
		}

		
		private void GetChangedRowOfUpdate(DataGridItem item, int rowIndex)
		{
			msTextBox txtEmpId = (msTextBox)item.FindControl("txtEmpId");
			TextBox txtEmpNa = (TextBox)item.FindControl("txtEmpNa");
			DropDownList ddlEmpType = (DropDownList)item.FindControl("ddlEmpType");
			msTextBox txtMobileNo = (msTextBox)item.FindControl("txtMobileNo");
			DropDownList ddlBaseLocation = (DropDownList)item.FindControl("ddlBaseLocation");
			msTextBox txtInitInServiceDate = (msTextBox)item.FindControl("txtInitInServiceDate");
			msTextBox txtSeparationDate = (msTextBox)item.FindControl("txtSeparationDate");
			msTextBox txtOutOfServiceDate = (msTextBox)item.FindControl("txtOutOfServiceDate");
			msTextBox txtInServiceDate = (msTextBox)item.FindControl("txtInServiceDate");			
			
			DataRow dr = m_sdsStaff.ds.Tables[0].Rows[rowIndex];

			dr["emp_id"] = txtEmpId.Text.ToString();
			dr["emp_name"] = txtEmpNa.Text.ToString();
			dr["emp_type"] = ddlEmpType.SelectedItem.Value;
			dr["mobileno"] = txtMobileNo.Text.ToString();
			dr["base_location"] = ddlBaseLocation.SelectedItem.Value;

			if (txtInitInServiceDate.Text != "")
				dr["initial_in_service_date"] = DateTime.ParseExact(txtInitInServiceDate.Text, "dd/MM/yyyy", null);
			else
				dr["initial_in_service_date"] = System.DBNull.Value;

			if (txtSeparationDate.Text != "")
				dr["separation_date"] = DateTime.ParseExact(txtSeparationDate.Text, "dd/MM/yyyy", null);
			else
				dr["separation_date"] = System.DBNull.Value;

			if (txtOutOfServiceDate.Text != "")
				dr["out_of_service_date"] = DateTime.ParseExact(txtOutOfServiceDate.Text, "dd/MM/yyyy", null);
			else
				dr["out_of_service_date"] = System.DBNull.Value;

			if (txtInServiceDate.Text != "")
				dr["in_service_date"] = DateTime.ParseExact(txtInServiceDate.Text, "dd/MM/yyyy", null);
			else
				dr["in_service_date"] = System.DBNull.Value;

			Session["SESSION_DS_Staff"] = m_sdsStaff;
		}


		private void GetChangedRow(DataGridItem item, int rowIndex)
		{
			msTextBox txtEmpId = (msTextBox)item.FindControl("txtEmpId");
			TextBox txtEmpNa = (TextBox)item.FindControl("txtEmpNa");
			DropDownList ddlEmpType = (DropDownList)item.FindControl("ddlEmpType");
			msTextBox txtMobileNo = (msTextBox)item.FindControl("txtMobileNo");
			DropDownList ddlBaseLocation = (DropDownList)item.FindControl("ddlBaseLocation");
			msTextBox txtInitInServiceDate = (msTextBox)item.FindControl("txtInitInServiceDate");
			msTextBox txtSeparationDate = (msTextBox)item.FindControl("txtSeparationDate");
			msTextBox txtOutOfServiceDate = (msTextBox)item.FindControl("txtOutOfServiceDate");
			msTextBox txtInServiceDate = (msTextBox)item.FindControl("txtInServiceDate");

			DataRow dr = m_sdsStaff.ds.Tables[0].Rows[rowIndex];
			dr["emp_id"] = txtEmpId.Text.ToString();
			dr["emp_name"] = txtEmpNa.Text.ToString();
			dr["emp_type"] = ddlEmpType.SelectedItem.Value;
			dr["mobileno"] = txtMobileNo.Text.ToString();
			dr["base_location"] = ddlBaseLocation.SelectedItem.Value;
			if (txtInitInServiceDate.Text != "")
				dr["initial_in_service_date"] = DateTime.ParseExact(txtInitInServiceDate.Text, "dd/MM/yyyy", null);
			else
				dr["initial_in_service_date"] = System.DBNull.Value;

			if (txtSeparationDate.Text != "")
				dr["separation_date"] = DateTime.ParseExact(txtSeparationDate.Text, "dd/MM/yyyy", null);
			else
				dr["separation_date"] = System.DBNull.Value;

			if (txtOutOfServiceDate.Text != "")
				dr["out_of_service_date"] = DateTime.ParseExact(txtOutOfServiceDate.Text, "dd/MM/yyyy", null);
			else
				dr["out_of_service_date"] = System.DBNull.Value;

			if (txtInServiceDate.Text != "")
				dr["in_service_date"] = DateTime.ParseExact(txtInServiceDate.Text, "dd/MM/yyyy", null);
			else
				dr["in_service_date"] = System.DBNull.Value;

//			if (txtApproxDepart !=null)
//			{
//				if (txtApproxDepart.Text.Trim() != "")
//				{
//					dr[8]= txtApproxDepart.Text;
//				}
//				else
//				{
//					dr[8]=System.DBNull.Value;
//				}
//			}

			Session["SESSION_DS_Staff"] = m_sdsStaff;
		}

		
		public void chkDdlEmpType(object sender, System.EventArgs e)
		{
			if ((int)ViewState["DPOperation"] != (int)Operation.None)
			{
				DropDownList ddlEmpType = null;
				RequiredFieldValidator rvEmpType = null;

				if((int)ViewState["DPOperation"] == (int)Operation.Insert)
				{
					ddlEmpType = (DropDownList)dgStaff.Items[0].Cells[4].FindControl("ddlEmpType");
					rvEmpType	= (RequiredFieldValidator)dgStaff.Items[0].FindControl("rvEmpType");
				}
				else
				{
					ddlEmpType = (DropDownList)dgStaff.Items[(int)ViewState["EditRow"]].Cells[4].FindControl("ddlEmpType");
					rvEmpType	= (RequiredFieldValidator)dgStaff.Items[(int)ViewState["EditRow"]].FindControl("rvEmpType");
				}
			}
		}
	

		public void BindDataGrid()
		{
			dgStaff.VirtualItemCount = System.Convert.ToInt32(m_sdsStaff.QueryResultMaxSize);
			dgStaff.DataSource = m_sdsStaff.ds;
			dgStaff.DataBind(); 
			Session["SESSION_DS_Staff"] = m_sdsStaff;
		}

		
		protected void dgStaff_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";

			if (!btnExecuteQuery.Enabled)
			{
				int iRowsAffected = 0;
				int rowIndex = e.Item.ItemIndex;

				GetChangedRowOfUpdate(e.Item, e.Item.ItemIndex);

				DataRow drSelected = m_sdsStaff.ds.Tables[0].Rows[e.Item.ItemIndex];

				//== initial in service date
				TextBox txtInitInServiceDate = (TextBox)e.Item.FindControl("txtInitInServiceDate");
				if (txtInitInServiceDate != null && drSelected["initial_in_service_date"] != System.DBNull.Value) 
					txtInitInServiceDate.Text = DataBinder.Eval(e.Item.DataItem,"initial_in_service_date","{0:dd/MM/yyyy}").ToString();

				//== separation date
				TextBox txtSeparationDate = (TextBox)e.Item.FindControl("txtSeparationDate");
				if (txtSeparationDate != null && drSelected["separation_date"] != System.DBNull.Value) 
					txtSeparationDate.Text = DataBinder.Eval(e.Item.DataItem,"separation_date","{0:dd/MM/yyyy}").ToString();

				//== out of service date
				TextBox txtOutOfServiceDate = (TextBox)e.Item.FindControl("txtOutOfServiceDate");
				if (txtOutOfServiceDate != null && drSelected["out_of_service_date"] != System.DBNull.Value) 
					txtOutOfServiceDate.Text = DataBinder.Eval(e.Item.DataItem,"out_of_service_date","{0:dd/MM/yyyy}").ToString();

				//// Check Rule DateTime
				///1) [Initial In Service Date] < [Separation Date]
				if (IsRuleDateTime(txtInitInServiceDate.Text, txtSeparationDate.Text) == false)
				{
					lblErrorMessage.Text = "Separation Date must greater than Initial In Service Date";
					return;
				}
				///2) [Initial In Service Date] < [Out of Service Date]
				if (IsRuleDateTime(txtInitInServiceDate.Text, txtOutOfServiceDate.Text) == false)
				{
					lblErrorMessage.Text = "Out of Service Date must greater than Initial In Service Date";
					return;
				}
				///3) [Out of Service Date] < [Separation Date]
				if (IsRuleDateTime(txtOutOfServiceDate.Text, txtSeparationDate.Text) == false)
				{
					lblErrorMessage.Text = "Separation Date must greater than Out of Service Date";
					return;
				}

				//== mobile no
				TextBox txtMobileNo = (TextBox)e.Item.FindControl("txtMobileNo");
				if (txtMobileNo != null && drSelected["mobileno"] != System.DBNull.Value) 
					txtMobileNo.Text = drSelected["mobileno"].ToString();

				double Num;
				bool isNum = double.TryParse(txtMobileNo.Text.Trim(), NumberStyles.Integer, CultureInfo.CurrentCulture, out Num);
				if (!isNum)
				{
					lblErrorMessage.Text = "Invalid Mobile No.";
					return;
				}


				/// Process insert & update
				{
					int iOperation = (int)ViewState["DPOperation"];
					try
					{	// INSERRT
						if (iOperation == (int)Operation.Insert)
						{
							iRowsAffected = Staff.InsertStaff(m_sdsStaff.ds, rowIndex, m_strAppID, m_strEnterpriseID);

							ArrayList paramValues = new ArrayList();
							paramValues.Add("" + iRowsAffected);	
							lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ROWS_INS", utility.GetUserCulture(), paramValues);
						}
						else
						{	// UPDATE
							DataSet dsChangedRow = m_sdsStaff.ds.GetChanges();
							iRowsAffected = Staff.UpdateStaff(dsChangedRow, m_strAppID, m_strEnterpriseID);

							ArrayList paramValues = new ArrayList();
							paramValues.Add("" + iRowsAffected);
							lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ROWS_UPD", utility.GetUserCulture(), paramValues);
							
							m_sdsStaff.ds.Tables[0].Rows[e.Item.ItemIndex].AcceptChanges();
						}
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("PRIMARY KEY") != -1)
						{
							lblErrorMessage.Text = "Duplication of Employee ID is Found"; 
						}
						else
						{
							lblErrorMessage.Text = strMsg;
						}

						Logger.LogTraceError("DeliveryPathCode.aspx.cs","dgState_Update","RBAC003",appException.Message.ToString());
						return;
					}

					if (iRowsAffected == 0)
					{
						return;
					}
					Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
					ViewState["DPOperation"] = Operation.None;
					dgStaff.EditItemIndex = -1;
					BindDataGrid();
					Logger.LogDebugInfo("State","dgStaff_Update","INF004","updating data grid...");			
				}
			}
		}

		
		protected void dgStaff_Edit(object sender, DataGridCommandEventArgs e)
		{
			ViewState["EditRow"] = e.Item.ItemIndex;

			lblErrorMessage.Text="";

			LoadEmpType("employee_type"); 

			if((int)ViewState["DPMode"] == (int)ScreenMode.Insert && (int) ViewState["DPOperation"] == (int)Operation.Insert && dgStaff.EditItemIndex > 0)
			{
				m_sdsStaff.ds.Tables[0].Rows.RemoveAt(dgStaff.EditItemIndex);
				dgStaff.CurrentPageIndex = 0;
			}
			dgStaff.EditItemIndex = e.Item.ItemIndex;
			ViewState["DPOperation"] = Operation.Update;
			BindDataGrid();
			getPageControls(Page);
		}

		
		protected void dgStaff_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			DataSet dsDestCode = null;
			DropDownList ddlEmpType = (DropDownList)e.Item.FindControl("ddlEmpType");
			DropDownList ddlBaseLocation = (DropDownList)e.Item.FindControl("ddlBaseLocation");
			string strType = null;

			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsStaff.ds.Tables[0].Rows[e.Item.ItemIndex];
			   
			if (ddlEmpType != null)
			{
				strType = (string)drSelected["emp_type"];
				ddlEmpType.SelectedIndex = ddlEmpType.Items.IndexOf(ddlEmpType.Items.FindByValue((string)drSelected["emp_type"]));
			}
			
			if (ddlBaseLocation != null)
			{
				if (drSelected["base_location"] != System.DBNull.Value)
				{
					string strBaseLocation = (string)drSelected["base_location"];
					ddlBaseLocation.SelectedIndex = ddlBaseLocation.Items.IndexOf(ddlBaseLocation.Items.FindByValue((string)drSelected["base_location"]));
				}
			}

			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}
			
			//== EmpId
			if ((int)ViewState["DPOperation"] != (int)Operation.Insert && (!btnExecuteQuery.Enabled))
			{
				msTextBox txtEmpId = (msTextBox)e.Item.FindControl("txtEmpId");
				
				if(txtEmpId != null ) 
				{
					txtEmpId.Enabled = false;

					RequiredFieldValidator rvEmpId = (RequiredFieldValidator)e.Item.FindControl("rvEmpId");
				}
			}

			//== EmpNa
			if ((int)ViewState["DPMode"] == (int)ScreenMode.Insert && (int)ViewState["DPOperation"] == (int)Operation.Insert)
			{
				e.Item.Cells[1].Enabled = false;

				TextBox txtEmpNa = (TextBox)e.Item.FindControl("txtEmpNa");

				if(txtEmpNa != null ) 
				{
					RequiredFieldValidator rvEmpName = (RequiredFieldValidator)e.Item.FindControl("rvEmpName");
				}
			}

			string codeID = "employee_type";

			DataSet dsEmpType = LoadEmpType(codeID);
			if (ddlEmpType!=null)
			{
				int cnt = dsEmpType.Tables[0].Rows.Count;  
				ddlEmpType.DataSource = dsEmpType;
				ddlEmpType.DataTextField = "EmpTypeValue"; 
				ddlEmpType.DataValueField = "EmpTypeText";
				ddlEmpType.DataBind();

				ddlEmpType.SelectedIndex = ddlEmpType.Items.IndexOf(ddlEmpType.Items.FindByValue(strType)); 
			}		


			//== MobileNo
			Label lblMobileNo = (Label)e.Item.FindControl("lblMobileNo");
			if (lblMobileNo != null && drSelected["mobileno"] != System.DBNull.Value && drSelected["mobileno"].ToString().Trim() != "") 
			{
				string mobileno = drSelected["mobileno"].ToString().Replace(" ","");
				lblMobileNo.Text = String.Format("{0:(0##) ###-####}", Convert.ToInt64(mobileno));
			}
			

			msTextBox txtMobileNo = (msTextBox)e.Item.FindControl("txtMobileNo");
			if (txtMobileNo != null && drSelected["mobileno"] != System.DBNull.Value && drSelected["mobileno"].ToString().Trim() != "") 
			{
				string mobileno = drSelected["mobileno"].ToString().Replace(" ","");
				txtMobileNo.Text = String.Format("{0:##########}", Convert.ToInt64(mobileno));
			}
			


			//== ddlBaseLocation
			dsDestCode = Staff.GetDistributionCenter(m_strAppID, m_strEnterpriseID);

			if (ddlBaseLocation != null)
			{
				int cnt = dsDestCode.Tables[0].Rows.Count;  
				ddlBaseLocation.DataSource = dsDestCode.Tables[0];
				ddlBaseLocation.DataTextField = dsDestCode.Tables[0].Columns["origin_code"].ColumnName.ToString(); 
				ddlBaseLocation.DataValueField = dsDestCode.Tables[0].Columns["origin_code"].ColumnName.ToString();
				ddlBaseLocation.DataBind();
				
				if (drSelected["base_location"] != System.DBNull.Value)
                    ddlBaseLocation.SelectedIndex = ddlBaseLocation.Items.IndexOf(ddlBaseLocation.Items.FindByValue((string)drSelected["base_location"]));
			}
		}
		
		
		protected void dgStaff_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMessage.Text = "";
			int rowIndex = e.Item.ItemIndex;
			if ((int)ViewState["DPMode"] == (int)ScreenMode.Insert && (int)ViewState["DPOperation"] == (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				m_sdsStaff.ds.Tables[0].Rows.RemoveAt(rowIndex);
				
			}
			ViewState["DPOperation"] = Operation.None;
			dgStaff.EditItemIndex = -1;
			BindDataGrid();
			
		}

		
		protected void dgStaff_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgStaff.CurrentPageIndex = e.NewPageIndex;
			RetreiveSelectedPage();
			Logger.LogDebugInfo("State","dgStaff_PageChange","INF009","On Page Change "+e.NewPageIndex);
		}

		
		protected void dgStaff_Delete(object sender, DataGridCommandEventArgs e)
		{
			if (!btnExecuteQuery.Enabled)
			{
				if ((int)ViewState["DPOperation"] == (int)Operation.Update)
				{
					dgStaff.EditItemIndex = -1;
				}
				BindDataGrid ();

				int rowIndex = e.Item.ItemIndex;
				m_sdsStaff  = (SessionDS)Session["SESSION_DS_Staff"];

				try
				{
					DataRow dr = m_sdsStaff.ds.Tables[0].Rows[rowIndex];
					string strEmpId = (string)dr["emp_id"];

					//--Check before delete
					if (Staff.IsEmpIdIsExistManifest(m_strAppID, m_strEnterpriseID, strEmpId))
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage, "CANNOT_DEL_MANIFEST_STAFF", utility.GetUserCulture());
						return;
					}
					
					//--DELETE
					int iRowsDeleted = Staff.DeleteStaff(m_sdsStaff.ds, rowIndex, m_strAppID, m_strEnterpriseID);
					lblErrorMessage.Text = iRowsDeleted + " row(s) deleted.";
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					if(strMsg.IndexOf("FK") != -1)
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_PATH_TRANS",utility.GetUserCulture());
					}
					if(strMsg.ToLower().IndexOf("child record found") != -1 )
					{
						lblErrorMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_PATH_TRANS",utility.GetUserCulture());							
					}			
					else
					{
						lblErrorMessage.Text = strMsg;
					}

					Logger.LogTraceError("MF_STAFF", "dgState_Delete", "RBAC003", appException.Message.ToString());

					return;
				}
				if((int)ViewState["DPMode"] == (int)ScreenMode.Insert)
				{
					m_sdsStaff.ds.Tables[0].Rows.RemoveAt(rowIndex);
				}
				else
				{
					RetreiveSelectedPage();
				}

				ViewState["DPOperation"] = Operation.None;
				//Make the row in non-edit Mode
				EditHRow(false);
				BindDataGrid();
				Logger.LogDebugInfo("Staff", "dgStaff_Delete", "INF004", "updating data grid...");			
			}
		}		
	
	
		private bool IsRuleDateTime(string min_date, string max_date)
		{
			bool result = true;
			if ( min_date!="" && max_date !="")
			{							 
				string d = min_date.Substring(0,2);
				string M = min_date.Substring(3,2);
				string y = min_date.Substring(6,4);
				string t = "00:00";	////min_date.Substring(11,5);

				string d1 = max_date.Substring(0,2);
				string M1 = max_date.Substring(3,2);
				string y1 = max_date.Substring(6,4);
				string t1 = "00:00";	////max_date.Substring(11,5);
				string d_min = M+"/"+d+"/"+y+" "+t+":"+"0";
				string d_max = M1+"/"+d1+"/"+y1+" "+t1+":"+"0";
				if (Convert.ToDateTime(d_max) <= Convert.ToDateTime(d_min))
				{
					result = false;
				}
				else
				{
					result = true;
				}
			}

			return result;
		}
	}
}
