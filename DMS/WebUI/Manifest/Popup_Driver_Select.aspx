<%@ Page language="c#" Codebehind="Popup_Driver_Select.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.Popup_Driver_Select" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Popup_Driver_Select</title>
		<LINK rel="stylesheet" type="text/css" href="../css/Styles.css">
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:Label id="lblLocation" style="Z-INDEX: 106; LEFT: 48px; POSITION: absolute; TOP: 80px"
				runat="server" CssClass="tableLabel">Location</asp:Label>
			<asp:datagrid id="dgDriver" style="Z-INDEX: 109; LEFT: 32px; POSITION: absolute; TOP: 120px" runat="server"
				Width="522px" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" AllowCustomPaging="True"
				OnItemDataBound="dgShipUpdate_Bound" SelectedItemStyle-CssClass="gridFieldSelected">
				<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
				<ItemStyle CssClass="drilldownField"></ItemStyle>
				<HeaderStyle CssClass="gridHeading"></HeaderStyle>
				<Columns>
					<asp:TemplateColumn HeaderText="Driver ID">
						<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lbDriverID" Text='<%#DataBinder.Eval(Container.DataItem,"serial_no")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txDriverID" Text='<%#DataBinder.Eval(Container.DataItem,"serial_no")%>' Runat="server" Enabled="True" MaxLength="12">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Name">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lbName" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_zipcode")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txName" Text='<%#DataBinder.Eval(Container.DataItem,"recipient_zipcode")%>' Runat="server">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Location">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle VerticalAlign="Middle"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lbLocation" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' Runat="server" Enabled="True">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<asp:TextBox CssClass="gridTextBox" ID="txLocation" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' Runat="server">
							</asp:TextBox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select">
						<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
					</asp:ButtonColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
			</asp:datagrid>
			<asp:Button id="btnClose" style="Z-INDEX: 108; LEFT: 464px; POSITION: absolute; TOP: 80px" runat="server"
				CssClass="buttonProp" Width="84px" Text="Close"></asp:Button>
			<asp:Label id="lblName" style="Z-INDEX: 105; LEFT: 48px; POSITION: absolute; TOP: 56px" runat="server"
				CssClass="tableLabel">Name</asp:Label>
			<asp:TextBox id="txtName" style="Z-INDEX: 102; LEFT: 136px; POSITION: absolute; TOP: 56px" runat="server"
				CssClass="textField" Width="144px"></asp:TextBox>
			<asp:TextBox id="txtDriverID" style="Z-INDEX: 101; LEFT: 136px; POSITION: absolute; TOP: 32px"
				runat="server" CssClass="textField" Width="144px"></asp:TextBox>
			<asp:DropDownList id="DropDownList1" style="Z-INDEX: 103; LEFT: 136px; POSITION: absolute; TOP: 80px"
				runat="server" CssClass="ddlLocation" Width="144px"></asp:DropDownList>
			<asp:Label id="lblDriverID" style="Z-INDEX: 104; LEFT: 48px; POSITION: absolute; TOP: 32px"
				runat="server" CssClass="tableLabel">Driver ID</asp:Label>
			<asp:Button id="btnSearch" style="Z-INDEX: 107; LEFT: 376px; POSITION: absolute; TOP: 80px"
				runat="server" CssClass="buttonProp" Width="84px" Text="Search"></asp:Button>
		</form>
	</body>
</HTML>
