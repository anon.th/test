<%@ Page language="c#" Codebehind="PopUp_WarningConsignment.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.PopUp_WarningConsignment"  %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Warning: Consignment Does not Exists</title>
		<base target="_self">
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../css/Styles.css">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript">
			var strA='0';
			function ReadPassedData()
			{
				var obj = window.dialogArguments;
				document.getElementById('txtPackage').focus();
			}
			
			function ConfirmGen(strLimit,strInput)
			{
				if (confirm("The system cannot auto-generate more than "+ strLimit +" status codes due to performance limitations.\nTotal packages for this consignment will be recorded as "+strInput+".\nDo you wish to proceed?")==true)
					document.getElementById("btnConfirm").click();
				else
					return false;
			}
			
			function DoUnload()
			{//debugger;
				var obj = window.dialogArguments;
				var a = strA;
				//alert(a);
				if(a=='0')
				{
					obj.returnvalue='Close';
				}
				else if(a=='1')
				{
					obj.returnvalue='ClickDDlNo';
				}
				else
				{
					obj.returnvalue='ClickDDlYes';
				}
				
			}
			window.onbeforeunload = function(){//debugger;
				var obj = window.dialogArguments;
				var a = strA;
				//alert(a);
				if(a=='0')
				{
					obj.returnvalue='Close';
				}
				else if(a=='1')
				{
					obj.returnvalue='ClickDDlNo';
				}
				else
				{
					obj.returnvalue='ClickDDlYes';
				}
			};

		</script>
	</HEAD>
	<body onload="ReadPassedData();" onunload="DoUnload();" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 8px" id="Table1" cellSpacing="1"
				cellPadding="1">
				<TR>
					<TD><asp:label id="Label1" runat="server" CssClass="tableLabel" Width="416px">The consignment number does not exist. If you are sure that you want to apply the  selected status code to this consignment, you must provide the total number of packages.</asp:label></TD>
					<TD></TD>
					<TD vAlign="bottom">
						<table>
								<tr>
									<td style="HEIGHT: 31px"><asp:button id="btnOk" runat="server" CssClass="queryButton" Width="101px" Text="OK"></asp:button></td>
								</tr>
								<tr>
									<td><asp:button id="btnCancel" runat="server" CssClass="queryButton" Width="101px" Text="Cancel"></asp:button></td>
									</tr>
							</tbody>
						</table>
					</TD>
				</TR>
			<TR>
				<TD>
					<table style="Z-INDEX: 0">
						<tr>
							<td style="WIDTH: 227px"><asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel"> Total Packages in this consignment:</asp:label><FONT face="Tahoma">&nbsp;</FONT>
							</td>
							<td><cc1:mstextbox id="txtPackage" CssClass="gridTextBox" Width="88px" NumberScale="1" NumberPrecision="3"
									NumberMinValue="1" NumberMaxValue="999" Enabled="True" TextMaskType="msNumeric" MaxLength="3"
									Runat="server"></cc1:mstextbox></td>
						</tr>
					</table>
				</TD>
				<TD></TD>
				<TD></TD>
			</tr>
			<TR>
				<TD>
					<table style="WIDTH: 334px; HEIGHT: 24px">
						<tr>
							<td style="WIDTH: 226px"><asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel"> Create status for all packages:</asp:label><FONT face="Tahoma">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</FONT>
							</td>
							<td><asp:dropdownlist style="Z-INDEX: 0" id="ddlStatus" tabIndex="1" runat="server" CssClass="textField"
									Width="96px" AutoPostBack="True" Height="19px">
									<asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
									<asp:ListItem Value="0">No</asp:ListItem>
								</asp:dropdownlist></FONT></td>
						</tr>
					</table>
				</TD>
				<TD></TD>
				<TD>
					<asp:button style="Z-INDEX: 0;display:none" id="btnConfirm" runat="server" Width="101px" CssClass="queryButton"
						Text="OK"></asp:button></TD>
			</TR>
			<TR>
				<TD></TD>
				<TD></TD>
				<TD></TD>
			</TR>
			<TR>
				<TD></TD>
				<TD></TD>
				<TD></TD>
			</TR>
			</TABLE>
		</form>
	</body>
</HTML>
