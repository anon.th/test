<%@ Page language="c#" Codebehind="BatchManifest_ControlPanel.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.BatchManifest_ControlPanel" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>BatchManifest_ControlPanel</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="../css/Styles.css">
		<!--#INCLUDE FILE="../msFormValidations.inc"-->
		<script language="javascript" src="../Scripts/settingScrollPosition.js"></script>
		<script language="javascript">
		 function KeyDownExe()
			{
				if (event.keyCode == 9 || event.keyCode == 13)
				{
					document.getElementById("btnExcuteQuery").click();
					//document.getElementById("txtConNo").focus();
				}
			}
			function confirm_report()
			{
				if (confirm("This manifest may contain information that is not up to date.\nDo you wish to proceed?")==true){
					document.getElementById("btnHidReport").click();
				}else {
					return false;
				}
			}
				
			function confirm_manifest2()
			{
				if (confirm("This manifest is already begin created. Do you wish to proceed?")==true)
					document.getElementById("btnmsgm2").click();
				else
					return false;
			}
			function confirm_manifest1()
			{
				if (confirm("Scanning is in progress for this batch. Do you wish to proceed anyway (the manifest created may not be accurate)?")==true)
					document.getElementById("btnmsgm2").click();
				else
					return false;
			}
			function ConfirmScannedPkgs()
			{
				
				if (confirm("Manifest Scanned Pkgs not allowed because Departure record has been created for this location.\nWould you like to cancel the departure record?")==true)
					document.getElementById("btnmsgm3").click();
				else
					return false;
			}
			
		</script>
	    <style type="text/css">
            .auto-style1 {
                font-family: Verdana, Arial, Helvetica, sans-serif;
                font-size: 11px;
                font-weight: bold;
                color: #FFFFFF;
                background-color: #008499;
                border-top-color: #FFFFFF;
                border-right-color: #336666;
                border-bottom-color: #336666;
                border-left-color: #FFFFFF;
                }
        </style>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="BatchManifest_ControlPanel" method="post" runat="server">
			<P><FONT face="Tahoma">
					<asp:button style="Z-INDEX: 104; POSITION: absolute; TOP: 40px; LEFT: 16px" id="btnQuery" runat="server"
						CssClass="queryButton" Width="101px" Text="Query"></asp:button>
					<asp:button style="Z-INDEX: 103; POSITION: absolute; TOP: 40px; LEFT: 118px" id="btnExcuteQuery"
						runat="server" CssClass="queryButton" Width="140px" Text="Execute Query"></asp:button>
				</FONT>
			</P>
			<P>
				<asp:label style="Z-INDEX: 101; POSITION: absolute; TOP: 16px; LEFT: 14px" id="Header1" CssClass="mainTitleSize"
					Width="490" Runat="server" Height="16px">Batch Manifests Control Panel</asp:label>
			</P>
			<P>
				<asp:label style="Z-INDEX: 111; POSITION: absolute; TOP: 432px; LEFT: 424px" id="lblTotal"
					runat="server" CssClass="tableLabel">Total</asp:label>
			</P>
			<INPUT style="Z-INDEX: 102; POSITION: absolute; TOP: 712px; LEFT: 16px" type="hidden" name="ScrollPosition">
			<asp:button id="btnShowReportExcel" style="Z-INDEX: 148; POSITION: absolute; TOP: 40px; LEFT: 502px"
				runat="server" Text="Show Manifest/DR (Excel)" Width="192px" CssClass="queryButton"></asp:button>
			<asp:button style="Z-INDEX: 147; POSITION: absolute; DISPLAY: none; TOP: 24px; LEFT: 630px"
				id="btnmsgm3" runat="server" Text="Button"></asp:button>
			<asp:button style="Z-INDEX: 145; POSITION: absolute; TOP: 16px; LEFT: 640px" id="btnmsgm1" runat="server"
				Width="0px" Text="Button"></asp:button>
			<asp:textbox style="Z-INDEX: 143; POSITION: absolute; TOP: 16px; LEFT: 560px" id="txtmsgm1" runat="server"
				Width="0px"></asp:textbox>
			<asp:textbox style="Z-INDEX: 142; POSITION: absolute; TOP: 16px; LEFT: 536px" id="txtmsgm2" runat="server"
				Width="0px"></asp:textbox>
			<asp:label style="Z-INDEX: 140; POSITION: absolute; TOP: 448px; LEFT: 640px" id="Label2" runat="server"
				CssClass="tableLabel">User</asp:label>
			<asp:label style="Z-INDEX: 138; POSITION: absolute; TOP: 448px; LEFT: 592px" id="lblcolStatus"
				runat="server" CssClass="tableLabel">Status</asp:label>
			<asp:label style="Z-INDEX: 133; POSITION: absolute; TOP: 72px; LEFT: 16px" id="lblValISector"
				runat="server" CssClass="errorMsgColor" Width="600px" Height="24px"></asp:label>
			<asp:label style="Z-INDEX: 105; POSITION: absolute; TOP: 680px; LEFT: 16px" id="lblErrrMsg"
				CssClass="errorMsgColor" Width="720px" Runat="server" HEIGHT="2px"></asp:label>
			<asp:validationsummary style="Z-INDEX: 106; POSITION: absolute; COLOR: red; TOP: 672px; LEFT: 240px" id="PageValidationSummary"
				Runat="server" DisplayMode="BulletList" ShowMessageBox="True" HeaderText="Please enter the missing  fields."
				ShowSummary="False"></asp:validationsummary>
			<asp:label style="Z-INDEX: 121; POSITION: absolute; TOP: 448px; LEFT: 536px" id="lblColWeight"
				runat="server" CssClass="tableLabel">Weight</asp:label>
			<asp:label style="Z-INDEX: 132; POSITION: absolute; TOP: 568px; LEFT: 600px" id="lblColPkgs04"
				runat="server" CssClass="tableLabel">#Pkgs</asp:label>
			<asp:label style="Z-INDEX: 131; POSITION: absolute; TOP: 568px; LEFT: 552px" id="lblColCons04"
				runat="server" CssClass="tableLabel">#Cons</asp:label>
			<asp:label style="Z-INDEX: 130; POSITION: absolute; TOP: 568px; LEFT: 448px" id="lblColArrivalDT"
				runat="server" CssClass="tableLabel">Arrival D/T</asp:label>
			<asp:label style="Z-INDEX: 129; POSITION: absolute; TOP: 568px; LEFT: 400px" id="lblColDC02"
				runat="server" CssClass="tableLabel">DC</asp:label>
			<asp:label style="Z-INDEX: 128; POSITION: absolute; TOP: 568px; LEFT: 352px" id="lblColPkgs03"
				runat="server" CssClass="tableLabel">#Pkgs</asp:label>
			<asp:label style="Z-INDEX: 127; POSITION: absolute; TOP: 568px; LEFT: 304px" id="lblColCons03"
				runat="server" CssClass="tableLabel">#Cons</asp:label>
			<asp:label style="Z-INDEX: 126; POSITION: absolute; TOP: 568px; LEFT: 192px" id="lblColDepartureDT"
				runat="server" CssClass="tableLabel">Departure D/T</asp:label>
			<asp:label style="Z-INDEX: 125; POSITION: absolute; TOP: 568px; LEFT: 144px" id="lblColDC01"
				runat="server" CssClass="tableLabel">DC</asp:label>
			<asp:label style="Z-INDEX: 124; POSITION: absolute; TOP: 568px; LEFT: 24px" id="lblColUpdatedDT"
				runat="server" CssClass="tableLabel">Updated D/T</asp:label>
			<asp:label style="Z-INDEX: 123; POSITION: absolute; TOP: 552px; LEFT: 552px" id="lblColReceived"
				runat="server" CssClass="tableLabel">---Received---</asp:label>
			<asp:label style="Z-INDEX: 122; POSITION: absolute; TOP: 552px; LEFT: 328px" id="lblColShipped"
				runat="server" CssClass="tableLabel">---Shipped---</asp:label>
			<asp:label style="Z-INDEX: 120; POSITION: absolute; TOP: 448px; LEFT: 472px" id="lblColOnTruck02"
				runat="server" CssClass="tableLabel">On Batch</asp:label>
			<asp:label style="Z-INDEX: 119; POSITION: absolute; TOP: 448px; LEFT: 424px" id="lblColPkgs"
				runat="server" CssClass="tableLabel">#Pkgs</asp:label>
			<asp:label style="Z-INDEX: 118; POSITION: absolute; TOP: 448px; LEFT: 368px" id="lblColOnTruck01"
				runat="server" CssClass="tableLabel">On Batch </asp:label>
			<asp:label style="Z-INDEX: 117; POSITION: absolute; TOP: 448px; LEFT: 328px" id="lblColType"
				runat="server" CssClass="tableLabel">Type</asp:label>
			<asp:label style="Z-INDEX: 116; POSITION: absolute; TOP: 448px; LEFT: 256px" id="lblColBatchDate"
				runat="server" CssClass="tableLabel">Batch Date</asp:label>
			<asp:label style="Z-INDEX: 115; POSITION: absolute; TOP: 448px; LEFT: 176px" id="lblColBatchID"
				runat="server" CssClass="tableLabel">Batch ID</asp:label>
			<asp:label style="Z-INDEX: 114; POSITION: absolute; TOP: 448px; LEFT: 104px" id="lblColTruckID02"
				runat="server" CssClass="tableLabel">Truck ID</asp:label>
			<asp:label style="Z-INDEX: 113; POSITION: absolute; TOP: 448px; LEFT: 24px" id="lblColBatchNo"
				runat="server" CssClass="tableLabel">Batch No.</asp:label>
			<asp:label style="Z-INDEX: 112; POSITION: absolute; TOP: 432px; LEFT: 472px" id="lblPkgs" runat="server"
				CssClass="tableLabel">#Pkgs</asp:label>
			<asp:label style="Z-INDEX: 110; POSITION: absolute; TOP: 432px; LEFT: 368px" id="lblCons" runat="server"
				CssClass="tableLabel">#Cons</asp:label>
			<asp:button style="Z-INDEX: 107; POSITION: absolute; TOP: 40px; LEFT: 259px" id="btnRefresh"
				runat="server" CssClass="queryButton" Width="103px" Text="Refresh"></asp:button>
			<table style="Z-INDEX: 108; POSITION: absolute; WIDTH: 48.31%; HEIGHT: 55px; TOP: 376px; LEFT: 16px">
				<TR class="tableLabel">
					<TD colSpan="4"><asp:label id="lblHeaderTruck" runat="server" CssClass="tableLabel"> Loading for Selected Batch(es)</asp:label></TD>
				</TR>
				<TR class="tableLabel">
					<TD style="WIDTH: 106px; HEIGHT: 17px" class="style4"><asp:label id="lblColTruckID01" runat="server" CssClass="tableLabel">Truck ID</asp:label></TD>
					<TD style="HEIGHT: 17px" class="style5"><asp:label id="lblGrossWt" runat="server" CssClass="tableLabel">Gross Wt.</asp:label></TD>
					<TD style="HEIGHT: 17px" class="style5"><asp:label id="lblTareWt" runat="server" CssClass="tableLabel">Tare Wt.</asp:label></TD>
					<TD style="WIDTH: 97px; HEIGHT: 17px" class="style5"><asp:label id="lblLoadedWt" runat="server" CssClass="tableLabel">Loaded Wt.</asp:label></TD>
				</TR>
				<TR class="tableLabel">
					<TD style="WIDTH: 106px" class="style4"><asp:label id="lblColTruckID01Display" runat="server" CssClass="tableLabel"></asp:label></TD>
					<TD class="style5"><asp:label id="lblGrossWtDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
					<TD class="style5"><asp:label id="lblTareWtDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
					<TD style="WIDTH: 97px" class="style5"><asp:label id="lblLoadedWtDisplay" runat="server" CssClass="tableLabel"></asp:label></TD>
				</TR>
			</table>
			<table style="Z-INDEX: 109; POSITION: absolute; TOP: 104px; LEFT: 16px">
				<TR class="tableLabel">
					<TD style="WIDTH: 120px"><asp:label id="lblBatchNo" runat="server" CssClass="tableLabel">Batch Number:</asp:label></TD>
					<TD><cc1:mstextbox id="txtBatchNo" runat="server" CssClass="tableTextbox" Width="106px" AutoPostBack="True"
							NumberMaxValueCOD="9999999999" NumberPrecision="10" TextMaskType="msNumericCOD"></cc1:mstextbox></TD>
					<TD rowSpan="8">
						<table style="Z-INDEX: 0; WIDTH: 22.72%; HEIGHT: 166px">
							<TR class="tableLabel">
								<TD style="HEIGHT: 2px" colSpan="2">&nbsp;</TD>
								<TD style="HEIGHT: 4px" noWrap><STRONG>
										<asp:label id="lblSteps" runat="server" CssClass="tableLabel" Font-Bold="True">Steps:</asp:label></STRONG></TD>
							</TR>
							<TR class="tableLabel">
								<TD style="HEIGHT: 17px">
									<asp:button id="btnOnArrival" runat="server" Text="On Arrival" Width="178px" CssClass="queryButton"></asp:button></TD>
								<TD style="HEIGHT: 17px" class="style5">&nbsp;</TD>
								<TD style="HEIGHT: 17px" noWrap>
									<asp:label id="lblStep1" runat="server" CssClass="tableLabel">1) Enter Arrival Record</asp:label></TD>
							</TR>
							<TR class="tableLabel">
								<TD style="WIDTH: 106px; HEIGHT: 26px" class="style4">
									<asp:button style="Z-INDEX: 0" id="btnManifestScannedPkgs" runat="server" Text="Manifest Scanned Pkgs."
										Width="178px" CssClass="queryButton"></asp:button></TD>
								<TD style="HEIGHT: 26px" class="style5">&nbsp;</TD>
								<TD style="HEIGHT: 24px" noWrap>
									<asp:label id="lblStep2" runat="server" CssClass="tableLabel">2) Create Manifest</asp:label></TD>
							</TR>
							<TR class="tableLabel">
								<TD style="WIDTH: 106px; HEIGHT: 25px" class="style4">
									<asp:button id="btnViewVariances" runat="server" Text="View Variances" Width="178px" CssClass="queryButton"></asp:button></TD>
								<TD style="HEIGHT: 25px" class="style5"></TD>
								<TD style="HEIGHT: 25px" noWrap>
									<asp:label id="lblStep3" runat="server" CssClass="tableLabel">3) Check for Variances</asp:label></TD>
							</TR>
							<TR class="tableLabel">
								<TD style="WIDTH: 106px" class="style4">
									<asp:button id="btnReceivingIsComplete" runat="server" Text="Receiving is Complete" Width="178px"
										CssClass="queryButton"></asp:button></TD>
								<TD class="style5">&nbsp;</TD>
								<TD noWrap>
									<asp:label id="lblStep4" runat="server" CssClass="tableLabel">4) After Variances Resolved</asp:label></TD>
							</TR>
							<TR class="tableLabel">
								<TD style="WIDTH: 106px" class="style4">
									<asp:button id="btnOnDeparture" runat="server" Text="On Departure" Width="178px" CssClass="queryButton"></asp:button></TD>
								<TD class="style5">&nbsp;</TD>
								<TD noWrap>
									<asp:label id="lblStep5" runat="server" CssClass="tableLabel">5) Enter Departure Record</asp:label></TD>
							</TR>
							<TR class="tableLabel">
								<TD style="WIDTH: 106px" class="style4">
									<asp:button id="btnForwardManifest" runat="server" Text="Forward Manifest" Width="178px" CssClass="queryButton"></asp:button></TD>
								<TD class="style5">&nbsp;</TD>
								<TD noWrap>
									<asp:label id="lblStep6" runat="server" CssClass="tableLabel">6) Forward Final Manifest</asp:label></TD>
							</TR>
						</table>
					</TD>
				</TR>
				<TR class="tableLabel">
					<TD style="WIDTH: 120px"><asp:label id="lblOr" runat="server" CssClass="tableLabel">or</asp:label></TD>
					<TD>&nbsp;</TD>
				</TR>
				<TR class="tableLabel">
					<TD style="WIDTH: 120px; HEIGHT: 15px"><asp:label id="lblTruckID" runat="server" CssClass="tableLabel">Truck ID:</asp:label></TD>
					<TD style="HEIGHT: 15px"><asp:dropdownlist id="ddlTruckID" runat="server" CssClass="textField" Width="106px"></asp:dropdownlist><asp:button id="btnTruckIDSrch" runat="server" CssClass="searchButton" Text="..."></asp:button></TD>
				</TR>
				<TR class="tableLabel">
					<TD style="WIDTH: 120px; HEIGHT: 10px"><asp:label id="lblBatchID" runat="server" CssClass="tableLabel">Batch ID:</asp:label></TD>
					<TD style="HEIGHT: 10px"><asp:dropdownlist id="ddlBatchID" runat="server" CssClass="textField" Width="106px"></asp:dropdownlist></TD>
				</TR>
				<TR class="tableLabel">
					<TD style="WIDTH: 120px"><asp:label id="lblBatchDate" runat="server" CssClass="tableLabel">Batch Date:</asp:label></TD>
					<TD><FONT face="Tahoma"><cc1:mstextbox id="txtBatchDate" runat="server" CssClass="textField" Width="106px" TextMaskType="msDate"
								TextMaskString="99/99/9999" MaxLength="10"></cc1:mstextbox></FONT></TD>
				</TR>
				<TR class="tableLabel">
					<TD style="WIDTH: 120px; HEIGHT: 14px"><asp:label id="lblBatchType" runat="server" CssClass="tableLabel">Batch Type:</asp:label></TD>
					<TD style="HEIGHT: 14px"><asp:dropdownlist id="ddlBatchType" runat="server" CssClass="textField" Width="147px"></asp:dropdownlist></TD>
				</TR>
				<TR class="tableLabel">
					<TD style="WIDTH: 120px; HEIGHT: 2px"><asp:label id="lblCurrentLocation" runat="server" CssClass="tableLabel">Current Location:</asp:label></TD>
					<TD style="HEIGHT: 2px"><asp:dropdownlist id="ddlCurrentLocation" runat="server" CssClass="textField" Width="106px" AutoPostBack="True"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 120px; HEIGHT: 2px"></TD>
					<TD style="HEIGHT: 2px"></TD>
				</TR>
				<TR class="tableLabel">
					<TD colSpan="3"><asp:label id="Label1" runat="server" CssClass="tableLabel">Use Current Location to filter by Origin/Termination<br>
						of the batch&nbsp;</asp:label><FONT face="Tahoma">&nbsp;</FONT>
						<asp:radiobutton id="rdbOrigin" runat="server" CssClass="tablelabel" Text="Origin" AutoPostBack="True"
							GroupName="FilterLocation"></asp:radiobutton>
						<FONT face="Tahoma">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</FONT>
						<asp:radiobutton id="rdbTermination" runat="server" CssClass="tablelabel" Text="Termination" AutoPostBack="True"
							GroupName="FilterLocation"></asp:radiobutton>
						<FONT face="Tahoma">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</FONT>
						<asp:radiobutton id="rdbIgnore" runat="server" CssClass="tablelabel" Text="Ignore" AutoPostBack="True"
							GroupName="FilterLocation" Checked="True"></asp:radiobutton>
						<FONT face="Tahoma">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</FONT>
						<asp:radiobutton id="rdbBoth" runat="server" CssClass="tablelabel" Text="Both" AutoPostBack="True"
							GroupName="FilterLocation"></asp:radiobutton>
					</TD>
				</TR>
				<TR class="tableLabel">
					<TD class="style4" colSpan="3"><asp:label id="Label3" runat="server" CssClass="tableLabel">Show only :</asp:label><FONT face="Tahoma">&nbsp;</FONT>
						<asp:radiobutton id="rdbOpenBatch" runat="server" CssClass="tablelabel" Text="Open Batches" AutoPostBack="True"
							GroupName="FilterOpenBatch"></asp:radiobutton><FONT face="Tahoma">&nbsp;</FONT>
						<asp:radiobutton id="rdbCloseBatch" runat="server" CssClass="tablelabel" Text="Closed Batches" AutoPostBack="True"
							GroupName="FilterOpenBatch"></asp:radiobutton><FONT face="Tahoma">&nbsp;</FONT>
						<asp:radiobutton id="rdbIgnorebatch" runat="server" CssClass="tablelabel" Text="Ignore" AutoPostBack="True"
							GroupName="FilterOpenBatch" Checked="True"></asp:radiobutton></TD>
				</TR>
				<TR>
					<TD class="style4" colSpan="2"><asp:label id="Label4" runat="server" CssClass="tableLabel">Driver ID :</asp:label><FONT face="Tahoma">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
							&nbsp; </FONT>
						<asp:dropdownlist id="ddlDriverID" runat="server" CssClass="textField" Width="152px"></asp:dropdownlist><asp:button id="btnDriverSearch" runat="server" CssClass="searchButton" Text="..."></asp:button></TD>
					<TD class="style4"></TD>
				</TR>
			</table>
			<TABLE style="Z-INDEX: 134; POSITION: absolute; WIDTH: 808px; HEIGHT: 103px; TOP: 464px; LEFT: 16px"
				id="Table1" border="0" cellSpacing="1" cellPadding="1" width="808">
				<TR>
					<TD>
						<DIV style="WIDTH: 750px; HEIGHT: 80px; OVERFLOW: auto" align="left"><asp:listbox id="lbBatch" runat="server" Width="672px" Height="74px" AutoPostBack="True" Font-Names="Courier New"
								SelectionMode="Multiple"></asp:listbox></DIV>
					</TD>
				</TR>
			</TABLE>
			<TABLE style="Z-INDEX: 135; POSITION: absolute; WIDTH: 304px; HEIGHT: 10px; TOP: 592px; LEFT: 16px"
				id="Table2" border="0" cellSpacing="1" cellPadding="1" width="304">
				<TR>
					<TD>
						<DIV style="WIDTH: 640px; HEIGHT: 80px; OVERFLOW: auto" align="left"><asp:listbox id="lbShipping" runat="server" Width="640px" Height="73px" Font-Names="Courier New"></asp:listbox></DIV>
					</TD>
				</TR>
			</TABLE>
			<FONT face="Tahoma">
				<asp:textbox style="Z-INDEX: 136; POSITION: absolute; TOP: 280px; LEFT: 544px" id="txtmsg" runat="server"
					Width="0px"></asp:textbox><asp:textbox style="Z-INDEX: 137; POSITION: absolute; TOP: 288px; LEFT: 576px" id="txterrmsg"
					runat="server" Width="0px"></asp:textbox><asp:textbox style="Z-INDEX: 139; POSITION: absolute; TOP: 296px; LEFT: 632px" id="txtmsgRe"
					runat="server" Width="0px"></asp:textbox><asp:button style="Z-INDEX: 141; POSITION: absolute; TOP: 40px; LEFT: 364px" id="btnShowReport"
					runat="server" CssClass="auto-style1" Width="136px" Text="Show Manifest/DR"></asp:button><asp:button style="Z-INDEX: 144; POSITION: absolute; TOP: 48px; LEFT: 664px" id="btnmsgm2" runat="server"
					Width="0px" Text="Button"></asp:button><asp:button style="Z-INDEX: 146; POSITION: absolute; TOP: 72px; LEFT: 576px" id="btnShowTKLHide"
					runat="server" Width="0px" Text="Button"></asp:button>
				<asp:Button style="Z-INDEX: 149; POSITION: absolute; TOP: 672px; LEFT: 752px; display:none;" id="btnHidReport"
					runat="server" Text="."></asp:Button></FONT></form>
	</body>
</HTML>
