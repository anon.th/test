using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.applicationpages;
using com.common.util;
using com.ties.DAL;
using com.common.RBAC;
namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for SWB_ScanPkgs.
	/// </summary>
	public class SWB_ScanPkgs : BasePage //: System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label lblErrrMsg;
		protected System.Web.UI.WebControls.Label lblLastestScan;
		protected System.Web.UI.WebControls.Label lblScannerID;
		protected System.Web.UI.WebControls.Label lblScanDateTime;
		protected System.Web.UI.WebControls.Label lblUserID;
		protected System.Web.UI.WebControls.Label lblUserIDDisplay;
		protected System.Web.UI.WebControls.Label lblLocation;
		protected System.Web.UI.WebControls.Label lblLocationDisplay;
		protected System.Web.UI.WebControls.Label lblSelectedStatusCode;
		protected System.Web.UI.WebControls.Label lblBatchSelected;
		protected System.Web.UI.WebControls.Label lblBatchSelectedDisplay;
		protected System.Web.UI.WebControls.Label lblBatchNo;
		protected System.Web.UI.WebControls.Label lblBatchNoDisplay;
		protected System.Web.UI.WebControls.Label lblBatchDate;
		protected System.Web.UI.WebControls.Label lblBatchDateDisplay;
		protected System.Web.UI.WebControls.Label lblTruckID;
		protected System.Web.UI.WebControls.Label lblTruckIDDisplay;
		protected System.Web.UI.WebControls.Label lblBatchID;
		protected System.Web.UI.WebControls.Label lblBatchIDDisplay;
		protected System.Web.UI.WebControls.Label lblBatchType;
		protected System.Web.UI.WebControls.Label lblBatchTypeDisplay;
		protected System.Web.UI.WebControls.Label lblScanningPackages01;
		protected System.Web.UI.WebControls.Label lblScanningPackages02;
		protected System.Web.UI.WebControls.Label lblProcessedToDMS;
		protected System.Web.UI.WebControls.Label lblCurrentScanResult;
		protected System.Web.UI.WebControls.Label lblSaveInDB;
		protected System.Web.UI.WebControls.Label lblSaveInDBDisplay;
		protected System.Web.UI.WebControls.Button btnScanComplete;
		protected System.Web.UI.WebControls.TextBox txtLastestScan;
		protected System.Web.UI.WebControls.TextBox txtScanerID;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.DropDownList ddlStatusCode;
		protected System.Web.UI.WebControls.ValidationSummary reqSummary;
		protected System.Web.UI.WebControls.Label lblColPackageScan;
		protected System.Web.UI.WebControls.Label lblColScan;
		protected System.Web.UI.WebControls.Label lblColDT;
		protected System.Web.UI.WebControls.Label lblColID;
		protected System.Web.UI.WebControls.Label lblColDuplicate;
		protected System.Web.UI.WebControls.Label Header1;
		String m_strAppID=null;
		String m_strEnterpriseID=null;
		protected System.Web.UI.WebControls.TextBox hidAppID;
		protected System.Web.UI.WebControls.TextBox hidEnterpriseID;
		protected System.Web.UI.WebControls.Label lblColPackageScan2;
		protected System.Web.UI.WebControls.Label lblColScan2;
		protected System.Web.UI.WebControls.Label lblColDT2;
		protected System.Web.UI.WebControls.Label lblColID2;
		protected System.Web.UI.WebControls.ListBox lbScanningPackages;
		protected System.Web.UI.WebControls.Label lblScanCount;
		protected System.Web.UI.WebControls.Label lblScanCountDisplay;
		protected System.Web.UI.WebControls.TextBox txtBatch_No;
		protected com.common.util.msTextBox txtBeginScanDT;
		protected com.common.util.msTextBox txtEndScanDT;
		protected System.Web.UI.WebControls.TextBox txtAjaxState;
		protected System.Web.UI.WebControls.Button btnResume;
		protected System.Web.UI.WebControls.TextBox txtProcesstoDMS;
		protected System.Web.UI.WebControls.Button BtnCallCodeBehind;
		protected System.Web.UI.WebControls.Button btnClientEvent;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblServiceType;
		protected System.Web.UI.HtmlControls.HtmlTableRow trServiceType;
		
		private bool BatchNoScan
		{
			get
			{
				if(ViewState["BatchNoScan"] == null)
				{
					return false;
				}
				else
				{
					return (bool)ViewState["BatchNoScan"];
				}
			}
			set
			{
				ViewState["BatchNoScan"]=value;
			}
		}

		String m_strCulture=null;
		private void Page_Load(object sender, System.EventArgs e)
		{
			btnResume.Attributes.Add("OnClick","doCallAjax('ADD');return false;");
			getSessionTimeOut(true);
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			if (!Page.IsPostBack)
			{
				Session["CreateBatchState"]=null;
				com.common.classes.User user =  RBACManager.GetUser(m_strAppID,m_strEnterpriseID, (String)Session["userID"]);
				hidAppID.Text = m_strAppID;
				hidEnterpriseID.Text=m_strEnterpriseID;
				lblUserIDDisplay.Text = user.UserID;
				lblLocationDisplay.Text = user.UserLocation;
				BindStatusCode();
			}
		}
		private void BindStatusCode()
		{
			Session["Status_Code"] =  SysDataMgrDAL.GetStatusCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),"SWB_ScanPkgs",utility.GetUserID());//SWB_Emulator.GetStatusCodeDS(m_strAppID,m_strEnterpriseID);
			ddlStatusCode.DataSource= (DataSet)Session["Status_Code"];
			ddlStatusCode.DataTextField="status_code";
			ddlStatusCode.DataValueField="status_code";
			ddlStatusCode.DataBind();
			//ddlStatusCode.Items.Insert(0," ");
			ddlStatusCode.SelectedValue="SOP";
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnScanComplete.Click += new System.EventHandler(this.btnScanComplete_Click);
			this.BtnCallCodeBehind.Click += new System.EventHandler(this.BtnCallCodeBehind_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnScanComplete_Click(object sender, System.EventArgs e)
		{
			if(BatchNoScan==false)
			{
				if(Session["CreateBatchState"]!=null && Session["CreateBatchState"].ToString()=="Scans Complete")
				{
					string concount =SWB_Emulator.SWB_Scan_Complete_Read(m_strAppID,m_strEnterpriseID,txtBeginScanDT.Text ,txtEndScanDT.Text,txtBatch_No.Text,lblUserIDDisplay.Text,lblLocationDisplay.Text).ToString();
					lblErrrMsg.Text="Batch#"+txtBatch_No.Text+" and "+ concount.ToString()+ " Cons Updated.";
					btnScanComplete.Text= Session["CreateBatchState"].ToString();			
					SWB_Emulator.SWB_Batch_Manifest_Update(m_strAppID,m_strEnterpriseID,Session["BatchNo"].ToString(),(String)Session["userID"],"0");
					Session["CreateBatchState"]=null;
					txtBatch_No.Text="";
					txtBeginScanDT.Text="";
					txtEndScanDT.Text="";
				}
				else
				{
					btnScanComplete.Text="Select Batch";
				}


				if(btnScanComplete.Text=="Select Batch")
				{
					lblErrrMsg.Text = "";
			
					string statusCode = ddlStatusCode.SelectedValue.ToString();
					String sUrl = "PopUp_CreateUpdateBatch.aspx?strFormID=SWB_ScanPkgs";
					sUrl +="&status="+ statusCode;
					sUrl += "&BATCH_NO_CID="+ lblBatchNoDisplay.ClientID;
					sUrl += "&BATCH_DATE_CID="+ lblBatchDateDisplay.ClientID;
					sUrl += "&TRUCK_ID_CID="+ lblTruckIDDisplay.ClientID;
					sUrl += "&BATCH_ID_CID="+ lblBatchIDDisplay.ClientID;
					sUrl += "&BATCH_TYPE_CID="+ lblBatchTypeDisplay.ClientID;
					sUrl += "&SEVICE_TYPE_CID="+ lblServiceType.ClientID;
					sUrl += "&trSEVICE_TYPE_CID="+ trServiceType.ClientID;
					sUrl += "&usrloc="+ lblLocationDisplay.Text;

					   
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScriptPopupSetSize("OpenSetSize.js",paramList,400,500);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
				else
				{
					if(lblScanCountDisplay.Text!= lblSaveInDBDisplay.Text)
					{
						lblErrrMsg.Text ="Not all scans have been processed to the database. Please try again.";
					
					}
					else
					{
						Session["CreateBatchState"]=null;
						//lbProcessedToDMS.Items.Clear();
						txtProcesstoDMS.Text="";
						lbScanningPackages.Items.Clear();
						btnScanComplete.Text="Select Batch";
						ddlStatusCode.Enabled=true;
					}
				}
			}
			else
			{
				BatchNoScanComplete();
			}

			
		}

		public static bool IsNumeric(string stringToTest)
		{
			try
			{
				int result = int.Parse(stringToTest);
				return true;
			}
			catch
			{
				return false;
			}
		}

		private void BatchNoScanComplete()
		{
			string [] lines = txtProcesstoDMS.Text.Split('\n');
			string msgUpdate="";
			foreach(string line in lines)
			{
				string [] inlines=line.Split(' ');
				string batchno = inlines[0];
				if(inlines.Length > 4 && IsNumeric(inlines[1])==false)
				{
					string beginScanDT= "";
					for(int idx = 4;idx<inlines.Length;idx++)
					{
						if(inlines[idx].Trim() != "")
						{
							if(beginScanDT.Length ==0)
							{
								beginScanDT = inlines[idx]+ " ";
							}
							else
							{
								beginScanDT+=inlines[idx];
								break;
							}
						}
					}
					string concount =SWB_Emulator.SWB_Scan_Complete_Read(m_strAppID,m_strEnterpriseID,beginScanDT 
						,DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
						,batchno,lblUserIDDisplay.Text,lblLocationDisplay.Text).ToString();

					SWB_Emulator.SWB_Batch_Manifest_Update(m_strAppID,m_strEnterpriseID,batchno,lblUserIDDisplay.Text,"0");

					if(msgUpdate.Length>0)
					{
						msgUpdate+=", ";
					}
					msgUpdate+="Batch#"+batchno+" and "+ concount + " Cons Updated.";
				}
			}
			lblErrrMsg.Text=msgUpdate;
			lblBatchNoDisplay.Text="";
			lblBatchDateDisplay.Text="";
			lblTruckIDDisplay.Text="";
			lblBatchIDDisplay.Text="";
			lblBatchTypeDisplay.Text="";
			Session["CreateBatchState"]=null;
			//lbProcessedToDMS.Items.Clear();
			txtProcesstoDMS.Text="";
			lbScanningPackages.Items.Clear();
			btnScanComplete.Text="Select Batch";
			ddlStatusCode.Enabled=true;
			BatchNoScan=false;
		}

		private void BtnCallCodeBehind_Click(object sender, System.EventArgs e)
		{
			lblErrrMsg.Text = "";
			string BatchNo = txtLastestScan.Text.Trim().Replace("*","").Replace("$","");
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);			
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			com.common.classes.User user =  RBACManager.GetUser(m_strAppID,m_strEnterpriseID, (string)Session["userID"]);
			DataSet ds = new DataSet();
			ds= SWB_Emulator.SWB_Batch_Manifest_Read(m_strAppID,m_strEnterpriseID,BatchNo);
			if(ValidateBatchNo(BatchNo,ds) == true)
			{
				string strlocation = lblLocationDisplay.Text;
				int i = BatchManifest.ModifyDep_DTTONull(m_strAppID,m_strEnterpriseID,BatchNo,strlocation);				
				bool First=false;
				DataSet dest =BatchManifest.GetDestinationAllType(m_strAppID,m_strEnterpriseID, ds.Tables[0].Rows[0]["Batch_ID"].ToString());
				//,(string)ViewState["batch_type"]
				if(dest.Tables[0].Rows.Count>0)
				{
					string strOrigin = dest.Tables[0].Rows[0]["origin_station"].ToString(); 
					
					if(strlocation==strOrigin)
					{
						ds= SWB_Emulator.SWB_Batch_Manifest_Update(m_strAppID,m_strEnterpriseID,BatchNo,user.UserID.ToString(),"1");
						First=true;
					}
				}
				if(First==false)
				{
					DataSet Arr_DC =BatchManifest.GetBatchDetail_ShippingTerminationLoad(m_strAppID,m_strEnterpriseID,BatchNo,strlocation);
					if (Arr_DC.Tables[0].Rows.Count > 0 )
					{
						if(Arr_DC.Tables[0].Rows[0]["Arr_DT"]!=System.DBNull.Value)
						{
							ds= SWB_Emulator.SWB_Batch_Manifest_Update(m_strAppID,m_strEnterpriseID,BatchNo,user.UserID.ToString(),"1");
						}
					}
					else
					{
						//No Update
						ds= SWB_Emulator.SWB_Batch_Manifest_Update(m_strAppID,m_strEnterpriseID,BatchNo,user.UserID.ToString(),"");
					}
				}

				if(ds.Tables[0].Rows.Count>0)
				{
					string strBatchNo = (string)ds.Tables[0].Rows[0]["Batch_No"].ToString();
					DateTime DTBatchDate = (DateTime)ds.Tables[0].Rows[0]["Batch_Date"];
					string strTruckID = (string)ds.Tables[0].Rows[0]["Truck_ID"].ToString();
					string strBatchID = (string)ds.Tables[0].Rows[0]["Batch_ID"].ToString();
					string strBatchType = (string)ds.Tables[0].Rows[0]["Batch_Type"].ToString();
					string strServiceType = "";
					if(ds.Tables[0].Rows[0]["ServiceType"] != null)
						strServiceType = (string)ds.Tables[0].Rows[0]["ServiceType"].ToString();
					string strBatchDate =DTBatchDate.ToString("dd/MM/yyyy");
					string strDes = "";
					if(ViewState["destination_station"] != null)
					{
						strDes = ViewState["destination_station"].ToString();
					}
					//string strServiceType = ds.Tables[0].Rows[0]["ServiceType"].ToString(); 
					string strNow = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
					if(ds.Tables[0].Rows[0]["Batch_Type"].ToString().ToUpper()=="W")
					{
						strBatchType="Current Location";
					}
					else if (ds.Tables[0].Rows[0]["Batch_Type"].ToString().ToUpper()=="S")
					{
						strBatchType="Delivery";
					}
					else if (ds.Tables[0].Rows[0]["Batch_Type"].ToString().ToUpper()=="A")
					{
						strBatchType="Air";
					}
					else//L
					{
						strBatchType="Linehaul";
					}
					Session["CreateBatchState"]="Scans Complete";
					Session["BatchNo"]=strBatchNo;
					Session["BatchID"]=strBatchID;

					lblBatchNoDisplay.Text=strBatchNo;
					lblBatchDateDisplay.Text=strBatchDate;
					lblTruckIDDisplay.Text=strTruckID;
					lblBatchIDDisplay.Text=strBatchID;
					lblBatchTypeDisplay.Text=strBatchType;
					if(strBatchType == "Air")
					{
						trServiceType.Style["display"] = "block";
						lblServiceType.Text = strServiceType;
					}
					else
					{
						trServiceType.Style["display"] = "none";
						lblServiceType.Text = "";
					}
					btnScanComplete.Text="Scans Complete";
					txtBatch_No.Text=strBatchNo;
					ddlStatusCode.Enabled=false;	
					string outputBatchNo=strBatchNo+ " " + strDes+" " + strServiceType;
					string newline = outputBatchNo+JsGenSpace(32-outputBatchNo.Length);
					newline+=strNow +JsGenSpace(22-strNow.Length)+lblUserIDDisplay.Text + "    " +"CB\n";
					string oldline = txtProcesstoDMS.Text;
					txtProcesstoDMS.Text = newline + oldline;
//					string outputBatchNo=strBatchNo;
//					string newline = outputBatchNo+JsGenSpace(32-outputBatchNo.Length);
//					newline+=strNow +JsGenSpace(22-strNow.Length)+lblUserIDDisplay.Tex;
//					//string oldline = txtProcesstoDMS.Text;
//					lbScanningPackages.Items.Add(newline);
					BatchNoScan=true;
				}

			}
			txtLastestScan.Text="";
			txtScanerID.Text="";
			txtDate.Text="";	

			String sScript = "<script language='javascript'>LastestScanFocus();</script>";
			Utility.RegisterScriptString(sScript,"LastestScanFocus",this.Page);
		}

		private string JsGenSpace(int cnt)
		{
			string space="";
			for (int i=0;i<=cnt;i++)
			{
				space+=' ';
			}
			return space;
		}

		private bool ValidateBatchNo(string BatchNo, DataSet dsBatch)
		{
			bool IsValidate = true;	
			if(dsBatch.Tables[0].Rows.Count>0)
			{
				string statuscode = ddlStatusCode.SelectedValue;
				string strlocation = lblLocationDisplay.Text.Trim();
				string strOrigin="";
				string strDes="";
				string Batch_Type = "";
				if(dsBatch.Tables[0].Rows[0]["Batch_Status"] != null)
				{
					Batch_Type = dsBatch.Tables[0].Rows[0]["Batch_Type"].ToString().ToUpper();
				}
				DateTime BatchDate  = Convert.ToDateTime(dsBatch.Tables[0].Rows[0]["Batch_date"]);
				DateTime cBatchDate = new DateTime(BatchDate.Year,BatchDate.Month,BatchDate.Day);
				DataSet dsDayToRead = new DataSet();
				dsDayToRead = BatchManifest.GetDayToRead(m_strAppID,m_strEnterpriseID);
				//************ Validate batch number  expired ************ 
				if (dsDayToRead.Tables[0].Rows.Count>0)
				{
					DateTime cNow = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);
					System.TimeSpan Diff =cNow.Subtract(cBatchDate);
					if(Diff.Days > int.Parse(dsDayToRead.Tables[0].Rows[0]["code_str_value"].ToString()) )
					{
						lblErrrMsg.Text ="Selected batch number has expired.";
						return false;
					}
				}
				//*********************************************************
				//************ Validate selected status code **************
				if(((statuscode.ToUpper() == "CLS" || statuscode.ToUpper()=="UNCLS"))
					&& dsBatch.Tables[0].Rows[0]["Batch_type"].ToString()!="W")
				{
					lblErrrMsg.Text ="Invalid batch type for selected status code.";
					return false;
				}
				else if (((statuscode.ToUpper() == "SIP" || statuscode.ToUpper()=="UNSIP")||(statuscode.ToUpper() == "SOP" || statuscode.ToUpper()=="UNSOP"))
					&& ((Batch_Type!="S")&&(Batch_Type!="L")&&(Batch_Type !="A")))
				{
					lblErrrMsg.Text ="Invalid batch type for selected status code.";
					return false;
				}				
				//*********************************************************
				//************ Validate Batch Type ************************
				if(Batch_Type == "C")
				{
					lblErrrMsg.Text="Cannot update to closed batch.";
					return false;;
				}
				if(Batch_Type == "S"||Batch_Type == "W")
				{
					DataSet desOrigin =BatchManifest.GetDestinationAllType(m_strAppID,m_strEnterpriseID,  dsBatch.Tables[0].Rows[0]["Batch_ID"].ToString());
					string strOriginDC="";
					if (desOrigin.Tables[0].Rows.Count > 0 )
					{
						strOriginDC = desOrigin.Tables[0].Rows[0]["origin_station"].ToString(); 
					}
					if(strOriginDC.Trim()!=strlocation.Trim())
					{
						lblErrrMsg.Text="Update not allowed to this batch from this location.";
						return false;
					}
				}
				//*********************************************************

				DataSet dest =BatchManifest.GetDestinationAllType(m_strAppID, m_strEnterpriseID, dsBatch.Tables[0].Rows[0]["Batch_ID"].ToString());
				if (dest.Tables[0].Rows.Count > 0 )
				{
					strOrigin = dest.Tables[0].Rows[0]["origin_station"].ToString(); 
				}
				if (dest.Tables[0].Rows.Count > 0 )
				{
					strDes = dest.Tables[0].Rows[0]["destination_station"].ToString(); 
				}
				ViewState["destination_station"] = strDes;				
				if(Batch_Type == "S")
				{														
					DataSet Arr =BatchManifest.GetBatchDetail_ShippingOriginLoad(m_strAppID,m_strEnterpriseID,BatchNo,strlocation);
					if (Arr.Tables[0].Rows.Count > 0 )
					{
						if(Arr.Tables[0].Rows[0]["Arr_DT"]==System.DBNull.Value 
							&& (statuscode.ToUpper() == "SIP"||statuscode.ToUpper() == "UNSIP"))
						{
							lblErrrMsg.Text="Status code not allowed prior to arrival.";
							return false;
						}
						if(Arr.Tables[0].Rows[0]["dep_DT"]!=System.DBNull.Value && Arr.Tables[0].Rows[0]["Arr_DT"]==System.DBNull.Value 
							&& (statuscode.ToUpper() == "SOP"||statuscode.ToUpper() == "UNSOP"))
						{
							return true;
						}
						if(Arr.Tables[0].Rows[0]["Arr_DT"]!=System.DBNull.Value 
							&& (statuscode.ToUpper() == "SOP"||statuscode.ToUpper() == "UNSOP"))
						{
							lblErrrMsg.Text="Status code not allowed after arrival.";
							return false;
						}
					}
					else
					{
						IsValidate=true;
					}
				}
				else if(Batch_Type=="L" || Batch_Type=="A")
				{
					DataSet Arr =BatchManifest.GetBatchDetail_ShippingOriginLoad(m_strAppID,m_strEnterpriseID,BatchNo,strlocation);

					if(strOrigin==strlocation && (statuscode.ToUpper() == "SIP"||statuscode.ToUpper() == "UNSIP"))
					{
						lblErrrMsg.Text="Status code not allowed at the origin DC.";
						return false;
					}
					if(strOrigin!=strlocation)
					{
						DataSet Arr_DC =BatchManifest.GetBatchDetail_ShippingTerminationLoad(m_strAppID,m_strEnterpriseID,BatchNo,strlocation);
						if (Arr_DC.Tables[0].Rows.Count > 0 )
						{
							if(Arr_DC.Tables[0].Rows[0]["Arr_DT"]==System.DBNull.Value 
								&& (statuscode.ToUpper() == "SIP"||statuscode.ToUpper() == "UNSIP"))
							{
								lblErrrMsg.Text="Status code not allowed prior to arrival.";
								return false;
							}
						}
						else
						{
							if(statuscode.ToUpper() == "SIP"||statuscode.ToUpper() == "UNSIP")
							{
								lblErrrMsg.Text="Status code not allowed prior to arrival.";
								return false;
							}
						}
					}

					if(strlocation==strDes && (statuscode.ToUpper() == "SOP"||statuscode.ToUpper() == "UNSOP"))
					{
						lblErrrMsg.Text="Status code not allowed at terminal DC.";
						return false;								
					}

					if(strlocation!=strDes)
					{
						if (Arr.Tables[0].Rows.Count > 0 )
						{
							if(Arr.Tables[0].Rows[0]["Dep_DT"]!=System.DBNull.Value && Arr.Tables[0].Rows[0]["Arr_DT"] ==System.DBNull.Value 
								&&(statuscode.ToUpper() == "SOP"||statuscode.ToUpper() == "UNSOP"))
							{
								return true;
							}
							if(Arr.Tables[0].Rows[0]["Dep_DT"]!=System.DBNull.Value && Arr.Tables[0].Rows[0]["Arr_DT"]!=System.DBNull.Value 
								&& (statuscode.ToUpper() == "SOP"||statuscode.ToUpper() == "UNSOP"))
							{
								lblErrrMsg.Text="Status code not allowed after arrival.";
								return false;
							}
						}							
					}

				}
			}
			else
			{
				lblErrrMsg.Text = "Batch number : " + BatchNo + " does not exist. Operation cancelled.";
				return false;
			}
		

			return IsValidate;
		}
	}
}
