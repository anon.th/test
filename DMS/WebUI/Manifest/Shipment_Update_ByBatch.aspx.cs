
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;
using com.ties.DAL;
using com.common.RBAC;
using com.ties.classes;

namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for PopUp_ViewVariances.
	/// </summary>
	public class Shipment_Update_ByBatch :BasePage
	{
		protected System.Web.UI.WebControls.Label lblBatchTypeDisplay;
		protected System.Web.UI.WebControls.Label lblBatchType;
		protected System.Web.UI.WebControls.Label lblBatchIDDisplay;
		protected System.Web.UI.WebControls.Label lblBatchID;
		protected System.Web.UI.WebControls.Label lblTruckIDDisplay;
		protected System.Web.UI.WebControls.Label lblTruckID;
		protected System.Web.UI.WebControls.Label lblBatchDateDisplay;
		protected System.Web.UI.WebControls.Label lblBatchDate;
		protected System.Web.UI.WebControls.Label lblBatchNumber;
		protected System.Web.UI.WebControls.RadioButtonList RadioButtonList1;
		protected System.Web.UI.WebControls.Label Header1;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExcuteQuery;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Label lblUserDis;
		protected System.Web.UI.WebControls.Label lblUser;
		protected System.Web.UI.WebControls.Label lblLocationDis;
		protected System.Web.UI.WebControls.Label lblApplyStatus;
		protected com.common.util.msTextBox txtBatchNo;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label lblStatusDateDis;
		protected System.Web.UI.WebControls.DropDownList ddlStatus;
		protected com.common.util.msTextBox txtStatusDateTime;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label lblRemark;
		protected System.Web.UI.WebControls.TextBox txtRemark;
		protected System.Web.UI.WebControls.TextBox txtPersonChange;
		protected System.Web.UI.WebControls.Label lblConsignee;
		protected System.Web.UI.WebControls.TextBox txtConsignee;
		protected System.Web.UI.WebControls.DropDownList ddlCurrentLocation;
		protected System.Web.UI.WebControls.Button btnSubmitHidden;
		protected System.Web.UI.WebControls.Label lblError;



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnExcuteQuery.Click += new System.EventHandler(this.btnExcuteQuery_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.ddlStatus.SelectedIndexChanged += new System.EventHandler(this.ddlStatus_SelectedIndexChanged);
			this.btnSubmitHidden.Click += new System.EventHandler(this.btnSubmitHidden_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			getSessionTimeOut(true);
			txtBatchNo.Attributes.Add("onkeyDown", "KeyDownHandler();");
			ddlStatus.Attributes.Add("OnChange","document.all['"+ txtPersonChange.ClientID +"'].focus();");
			txtPersonChange.Attributes.Add("Onblur","document.all['"+ txtRemark.ClientID +"'].focus();");
			txtRemark.Attributes.Add("onkeyDown","if (event.keyCode == 13)document.all['"+ btnSave.ClientID +"'].click();");
			btnSave.Attributes.Add("OnClick","this.disabled = true;__doPostBack('btnSave','')");

			if(!Page.IsPostBack)
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				com.common.classes.User user = com.common.RBAC.RBACManager.GetUser(utility.GetAppID(), utility.GetEnterpriseID(), utility.GetUserID());
				lblUser.Text=user.UserID;
				LoadUserLocation();
				ddlCurrentLocation.SelectedValue=user.UserLocation;
				GetStatus();
				setFocusCtrl(txtBatchNo.ClientID);
				if (getRole("OPSSU")!="OPSSU")
				{
					
					ddlCurrentLocation.Enabled = false; 
					
				}
			}
		}

		private string getRole(String strRole)
		{
			ArrayList userRoleArray;
			User user = new User();
			user.UserID = utility.GetUserID();
			userRoleArray = com.common.RBAC.RBACManager.GetAllRoles(utility.GetAppID(), utility.GetEnterpriseID(), user);
			Role role;

			for(int i=0;i<userRoleArray.Count;i++)
			{
				role = (Role)userRoleArray[i];				
				if (role.RoleName.ToUpper().Trim() == strRole) return strRole;
			}
			return " ";
		}

		public void LoadUserLocation()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet dataset = com.ties.classes.DbComboDAL.UserLocationQuery(strAppID,strEnterpriseID);	
			this.ddlCurrentLocation.DataSource = dataset;

			ddlCurrentLocation.DataTextField = dataset.Tables[0].Columns["origin_code"].ToString() ;
			ddlCurrentLocation.DataValueField = dataset.Tables[0].Columns["origin_code"].ToString() ;
			this.ddlCurrentLocation.DataBind();
		}

		private void GetStatus()
		{
			DataSet ds = SysDataMgrDAL.GetStatusCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),"ShipmentUpdate_ByBatch",utility.GetUserID());

			if (ds.Tables[0].Rows.Count > 0 )
			{
				ddlStatus.DataSource=ds.Tables[0];
				ddlStatus.DataTextField="status_code";
				ddlStatus.DataValueField="status_code";
				ddlStatus.DataBind();
				ddlStatus.Items.Insert(0,new ListItem("",""));
			}
		}


		private void GetInformation()
		{
			DataSet ds = BatchManifest.GetDataArrivalDeparture( utility.GetAppID(),utility.GetEnterpriseID(),utility.GetUserCulture(),txtBatchNo.Text.Trim());
			if (ds.Tables[0].Rows.Count > 0 )
			{
				ViewState["BatchDate"] =ds.Tables[0].Rows[0]["batch_date"];  
				lblBatchDateDisplay.Text = String.Format("{0:dd/MM/yyyy}",ds.Tables[0].Rows[0]["batch_date"]);  
				lblTruckIDDisplay.Text = ds.Tables[0].Rows[0]["truck_id"].ToString(); 
				lblBatchIDDisplay.Text = ds.Tables[0].Rows[0]["batch_id"].ToString();
				ViewState["batch_type"]=ds.Tables[0].Rows[0]["batch_type"].ToString();
				lblBatchTypeDisplay.Text = ds.Tables[0].Rows[0]["code_text"].ToString(); 
				if(ds.Tables[0].Rows[0]["Batch_Status"]!=null)
				{
					if(ds.Tables[0].Rows[0]["Batch_Status"].ToString()=="C")
					{
						lblError.Text="Cannot update to closed batch.";
						btnSave.Enabled=false;
					}
				}

				if(!ds.Tables[0].Rows[0]["batch_type"].ToString().Equals("S")) 
				{
					ddlStatus.Items.Remove(ddlStatus.Items.FindByValue("POD"));
				}
				setFocusCtrl(ddlStatus.ClientID);
			}
			else
			{
				lblError.Text ="No matches found.";
				btnSave.Enabled=false;
				//setFocusCtrl(txtBatchNo.ClientID);

				string script="<script langauge='javacript'>";
				script+= "document.getElementById('"+ txtBatchNo.ClientID +"').focus();";
				script+= "document.getElementById('"+ txtBatchNo.ClientID +"').select();";
				script+= "</script>";
				Page.RegisterStartupScript("SelectText",script);
			}
		}

		
		private void btnExcuteQuery_Click(object sender, System.EventArgs e)
		{
			if (txtBatchNo.Text =="")
			{
				lblError.Text ="Please enter Batch Number."; 
				return;
			}
			
			btnExcuteQuery.Enabled=false;
			btnSave.Enabled=true;
			GetInformation();
			//txtBatchNo.ReadOnly=true;
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			btnExcuteQuery.Enabled=true;
			btnSave.Enabled=false;
			lblBatchDateDisplay.Text="";
			lblTruckIDDisplay.Text="";
			lblBatchIDDisplay.Text="";
			lblBatchTypeDisplay.Text="";
			ddlStatus.SelectedValue ="" ;;
			txtStatusDateTime.Text="";
			txtPersonChange.Text="";
			txtRemark.Text="";
			txtBatchNo.ReadOnly=false;
			lblError.Text="";
			txtBatchNo.Text="";
			setFocusCtrl(txtBatchNo.ClientID);
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			if(ddlStatus.SelectedValue.ToString().Trim().Equals(""))
			{
				lblError.Text="Please select the status code to apply.";
				return;
			}
			else
			{
				if(ddlStatus.SelectedValue.ToString().Trim().Equals("POD")&& txtConsignee.Text.Trim()=="")
				{
					lblError.Text="Consignee Name is a required entry for a POD status.";
					return;
				}
			}
			if(txtStatusDateTime.Text.Trim().Equals(""))
			{
				lblError.Text="Please enter a status date/time.";
				return;
			}

			if(ddlStatus.SelectedValue.ToString().Trim()=="POD")
			{
				DataSet dest =BatchManifest.GetDestinationAllType(utility.GetAppID(),utility.GetEnterpriseID(),lblBatchIDDisplay.Text.Trim());
				if(dest.Tables[0].Rows.Count>0)
				{
					string strOrigin = dest.Tables[0].Rows[0]["origin_station"].ToString(); 
					if(ddlCurrentLocation.SelectedValue.ToString().Trim()!=strOrigin)
					{
						lblError.Text="Update to batch not allowed for this location.";
						return;
					}
				}
			}

			DateTime bkgDateTime= DateTime.Now;
			DateTime statusDateTime =DateTime.ParseExact(txtStatusDateTime.Text,"dd/MM/yyyy HH:mm",null);
			System.TimeSpan StatusdateDiff;
			StatusdateDiff = bkgDateTime.Subtract(statusDateTime);
			if(StatusdateDiff.Minutes<0)
			{
				//lblError.Text="Please enter a status date/time that is less than  the current date/time.";
				lblError.Text="Status Date/Time must be less than  the current system date/time.";

				return;
			}
			if(txtPersonChange.Text.Trim().Equals(""))
			{
				lblError.Text="Please enter the person in charge.";
				return;
			}
			if(ViewState["BatchDate"]!=null)
			{
				DateTime BatchDate  = Convert.ToDateTime(ViewState["BatchDate"]);
				DateTime cBatchDate = new DateTime(BatchDate.Year,BatchDate.Month,BatchDate.Day);
				DataSet dsDayToRead = new DataSet();
				dsDayToRead = BatchManifest.GetDayToRead(utility.GetAppID(),utility.GetEnterpriseID());
				if (dsDayToRead.Tables[0].Rows.Count>0)
				{
					DateTime cNow = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);
					//System.TimeSpan Diff = DateTime.Now.Subtract(Convert.ToDateTime(ViewState["BatchDate"]));
					System.TimeSpan Diff =cNow.Subtract(cBatchDate);
					if(Diff.Days > int.Parse(dsDayToRead.Tables[0].Rows[0]["code_str_value"].ToString()) )
					{
						lblError.Text ="Selected batch number has expired.";
						
						return ;
					}
				}
				
				DateTime cStatusDate =  new DateTime(statusDateTime.Year,statusDateTime.Month,statusDateTime.Day);
				System.TimeSpan dateDiff =cStatusDate.Subtract(cBatchDate);
				if(dateDiff.Days < 0 )
				{
					lblError.Text ="Status Date/Time may not be earlier than the Batch Date.";
						
					return ;
				}
			}


			if(ddlStatus.SelectedValue.ToString().Trim()=="SIP")
			{
				if(ViewState["batch_type"]!=null)
				{
					if(ViewState["batch_type"].ToString()=="L")
					{
												DataSet dest =BatchManifest.GetDestinationAllType(utility.GetAppID(),utility.GetEnterpriseID(),lblBatchIDDisplay.Text.ToString().Trim());
												string strOrigin="";
						string strlocation=ddlCurrentLocation.SelectedValue.ToString().Trim();
												if(dest.Tables[0].Rows.Count>0)
												{
													strOrigin = dest.Tables[0].Rows[0]["origin_station"].ToString(); 
												}
												if(strlocation == strOrigin)
												{
													lblError.Text="Status code not allowed at the origin DC.";
													return;
												}

												DataSet dep_dc =BatchManifest.GetBatchDetail_ShippingOriginLoad(utility.GetAppID(),utility.GetEnterpriseID(),txtBatchNo.Text.Trim(),strlocation);
												if (dep_dc.Tables[0].Rows.Count > 0 )
												{
													if(dep_dc.Tables[0].Rows[0]["Dep_DT"]!=System.DBNull.Value)
													{
														lblError.Text="Status code not allowed after departure.";
														return;
													}
												}

												DataSet Arr_DC =BatchManifest.GetBatchDetail_ShippingTerminationLoad(utility.GetAppID(),utility.GetEnterpriseID(),txtBatchNo.Text.Trim(),strlocation);						
												if (Arr_DC.Tables[0].Rows.Count > 0 )
												{
													if(Arr_DC.Tables[0].Rows[0]["Arr_DT"]==System.DBNull.Value)
													{
														lblError.Text="Status code not allowed prior to arrival.";
														return;
													}
												}
												else
												{
													lblError.Text="Status code not allowed prior to arrival.";
													return;
												}

						ArrayList paramList = new ArrayList();
						paramList.Add("PopUp_SIPByBatch.aspx?BatchNo="+ txtBatchNo.Text.Trim()+"&CurrentLocation="+ddlCurrentLocation.SelectedItem.Text.Trim()+"&BatchID="+lblBatchIDDisplay.Text.Trim()+"&PersonInchange="+txtPersonChange.Text.Trim() +"");
						String sScript = Utility.GetScriptPopupSetSize("OpenSetSize.js",paramList,250,450);
						Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
						return;
					}
					else
					{
						lblError.Text="Status code not valid for this batch type.";
						return;
					}
				}
			}
			else
			{
				if(txtRemark.Text.Trim().Equals(""))
				{
					lblError.Text="Please enter remarks.";
					return;
				}
			}
			string strRemark = txtRemark.Text.Trim();
			if(strRemark.Length>=200)
			{
				strRemark = strRemark.Substring(0,180);
			}



			int intRoweffect = 0;
			try
			{
				//intRoweffect = ShipmentUpdateMgrDAL.MF_Shipment_Update(utility.GetAppID(),utility.GetEnterpriseID(),txtBatchNo.Text.Trim(),ddlStatus.SelectedValue.ToString(),statusDateTime,Utility.ReplaceSingleQuote(strRemark),lblUser.Text,ddlCurrentLocation.SelectedValue.ToString().Trim(),Utility.ReplaceSingleQuote(txtPersonChange.Text.Trim()),Utility.ReplaceSingleQuote(txtConsignee.Text.Trim()));
				intRoweffect = ShipmentUpdateMgrDAL.MF_Shipment_Update_by_Batch(utility.GetAppID(),utility.GetEnterpriseID(),txtBatchNo.Text.Trim(),ddlStatus.SelectedValue.ToString(),statusDateTime,Utility.ReplaceSingleQuote(strRemark),lblUser.Text,ddlCurrentLocation.SelectedValue.ToString().Trim(),Utility.ReplaceSingleQuote(txtPersonChange.Text.Trim()),Utility.ReplaceSingleQuote(txtConsignee.Text.Trim()));
				if(intRoweffect==-1)
				{
					//lblError.Text = "POD update not allowed because some consignments have POD";
					lblError.Text = "This status code cannot be applied to all consignments on this batch, so it has been applied to none.";
					return;
				}
				else if(intRoweffect==-2)
				{
					lblError.Text = "No consignment on this batch no.";
					return;
				}
			}
			catch(Exception ex)
			{
				lblError.Text = "Duplicate records. Please change status date/time.";
				return;
			}
			if(intRoweffect>0)
			{
				ClearScreen();
				lblError.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture()) + " "+intRoweffect.ToString() + " Consignment updated.";
				setFocusCtrl(txtBatchNo.ClientID);
			}
			else
			{
				lblError.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_UPD",utility.GetUserCulture());
			}
		}

		private void ddlStatus_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!ddlStatus.SelectedValue.ToString().Trim().Equals(""))
			{
				txtStatusDateTime.Text=DateTime.Now.ToString("dd/MM/yyyy HH:mm");
				setFocusCtrl(txtPersonChange.ClientID);
				if(ddlStatus.SelectedValue.ToString().Trim().Equals("POD"))
				{
					txtConsignee.Text="";
					txtConsignee.Visible=true;
					lblConsignee.Visible=true;
					txtPersonChange.Attributes.Add("Onblur","document.all['"+ txtConsignee.ClientID +"'].focus();");
					txtConsignee.Attributes.Add("Onblur","document.all['"+ txtRemark.ClientID +"'].focus();");
				}
				else
				{
					if(ddlStatus.SelectedValue.ToString().Trim().Equals("SIP"))
					{
						txtRemark.Enabled=false;
						txtStatusDateTime.Enabled=false;
					}
					else
					{
						txtRemark.Enabled=true;
						txtStatusDateTime.Enabled=true;
					}
					txtConsignee.Visible=false;
					lblConsignee.Visible=false;
					txtPersonChange.Attributes.Add("Onblur","document.all['"+ txtRemark.ClientID +"'].focus();");
				}
			}
			else
			{
				txtStatusDateTime.Text="";
				txtConsignee.Visible=false;
				lblConsignee.Visible=false;
				txtPersonChange.Attributes.Add("Onblur","document.all['"+ txtRemark.ClientID +"'].focus();");
			}
		}
		private void setFocusCtrl(string ctrlName)

		{
			string script="<script langauge='javacript'>";
			script+= "document.all['"+ ctrlName +"'].focus();";
			script+= "</script>";
			Page.RegisterStartupScript("setFocus",script);
		
		}
		private void ClearScreen()
		{
			btnExcuteQuery.Enabled=true;
			btnSave.Enabled=false;
			lblBatchDateDisplay.Text="";
			lblTruckIDDisplay.Text="";
			lblBatchIDDisplay.Text="";
			lblBatchTypeDisplay.Text="";
			GetStatus();
			ddlStatus.SelectedValue ="" ;
			txtStatusDateTime.Text="";
			txtPersonChange.Text="";
			txtRemark.Text="";
			txtBatchNo.ReadOnly=false;
			lblError.Text="";
			txtBatchNo.Text="";
			txtConsignee.Text="";
			setFocusCtrl(txtBatchNo.ClientID);
		}

		private void btnSubmitHidden_Click(object sender, System.EventArgs e)
		{
			if (Session["SIPbyBatchRowEffect"]!=null)
			{
				int intRoweffect=Convert.ToInt32(Session["SIPbyBatchRowEffect"]);
				if(intRoweffect>0)
				{
					ClearScreen();
					lblError.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture()) + " "+intRoweffect.ToString() + " Consignment updated.";
					setFocusCtrl(txtBatchNo.ClientID);
				}
				else
				{
					lblError.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_UPD",utility.GetUserCulture());
				}
				Session.Remove("SIPbyBatchRowEffect");

			}
		}
	}
}
