using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;
using com.ties.DAL;
using com.common.RBAC;
using com.ties.classes;
using System.Globalization;

namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for BatchManifest_ControlPanel.
	/// </summary>
	public class BatchManifest_ControlPanel : BasePage//System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnExcuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnRefresh;
		protected System.Web.UI.WebControls.Label lblSteps;
		protected System.Web.UI.WebControls.Button btnOnArrival;
		protected System.Web.UI.WebControls.Label lblStep1;
		protected System.Web.UI.WebControls.Button btnManifestScannedPkgs;
		protected System.Web.UI.WebControls.Label lblStep2;
		protected System.Web.UI.WebControls.Button btnViewVariances;
		protected System.Web.UI.WebControls.Label lblStep3;
		protected System.Web.UI.WebControls.Button btnReceivingIsComplete;
		protected System.Web.UI.WebControls.Label lblStep4;
		protected System.Web.UI.WebControls.Button btnOnDeparture;
		protected System.Web.UI.WebControls.Label lblStep5;
		protected System.Web.UI.WebControls.Button btnForwardManifest;
		protected System.Web.UI.WebControls.Label lblStep6;
		protected System.Web.UI.WebControls.Label lblColTruckID01;
		protected System.Web.UI.WebControls.Label lblGrossWt;
		protected System.Web.UI.WebControls.Label lblTareWt;
		protected System.Web.UI.WebControls.Label lblLoadedWt;
		protected System.Web.UI.WebControls.Label lblColTruckID01Display;
		protected System.Web.UI.WebControls.Label lblGrossWtDisplay;
		protected System.Web.UI.WebControls.Label lblTareWtDisplay;
		protected System.Web.UI.WebControls.Label lblLoadedWtDisplay;
		protected System.Web.UI.WebControls.Label lblBatchNo;
		protected com.common.util.msTextBox txtBatchNo;
		protected System.Web.UI.WebControls.Label lblOr;
		protected System.Web.UI.WebControls.Label lblTruckID;
		protected System.Web.UI.WebControls.Label lblBatchID;
		protected System.Web.UI.WebControls.DropDownList ddlBatchID;
		protected System.Web.UI.WebControls.Label lblBatchDate;
		protected com.common.util.msTextBox txtBatchDate;
		protected System.Web.UI.WebControls.Label lblBatchType;
		protected System.Web.UI.WebControls.DropDownList ddlBatchType;
		protected System.Web.UI.WebControls.Label lblCurrentLocation;
		protected System.Web.UI.WebControls.RadioButton rdbOrigin;
		protected System.Web.UI.WebControls.RadioButton rdbTermination;
		protected System.Web.UI.WebControls.Label lblCons;
		protected System.Web.UI.WebControls.Label lblTotal;
		protected System.Web.UI.WebControls.Label lblPkgs;
		protected System.Web.UI.WebControls.Label lblColBatchNo;
		protected System.Web.UI.WebControls.Label lblColTruckID02;
		protected System.Web.UI.WebControls.Label lblColBatchID;
		protected System.Web.UI.WebControls.Label lblColBatchDate;
		protected System.Web.UI.WebControls.Label lblColType;
		protected System.Web.UI.WebControls.Label lblColOnTruck01;
		protected System.Web.UI.WebControls.Label lblColPkgs;
		protected System.Web.UI.WebControls.Label lblColOnTruck02;
		protected System.Web.UI.WebControls.Label lblColWeight;
		protected System.Web.UI.WebControls.ListBox lbBatch;
		protected System.Web.UI.WebControls.Label lblColShipped;
		protected System.Web.UI.WebControls.Label lblColReceived;
		protected System.Web.UI.WebControls.Label lblColUpdatedDT;
		protected System.Web.UI.WebControls.Label lblColDC01;
		protected System.Web.UI.WebControls.Label lblColDepartureDT;
		protected System.Web.UI.WebControls.Label lblColCons03;
		protected System.Web.UI.WebControls.Label lblColPkgs03;
		protected System.Web.UI.WebControls.Label lblColDC02;
		protected System.Web.UI.WebControls.Label lblColArrivalDT;
		protected System.Web.UI.WebControls.Label lblColCons04;
		protected System.Web.UI.WebControls.Label lblColPkgs04;
		protected System.Web.UI.WebControls.ListBox lbShipping;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.WebControls.Label lblErrrMsg;
		protected System.Web.UI.WebControls.Label lblHeaderTruck;
		protected System.Web.UI.WebControls.DropDownList ddlTruckID;
		protected System.Web.UI.WebControls.DropDownList ddlCurrentLocation;
		protected System.Web.UI.WebControls.Label Header1;
		private DataView m_dvBatchTypeOptions;
		private String m_strAppID, m_strEnterpriseID, m_strCulture;
		protected System.Web.UI.WebControls.Label lblValISector;
		protected ArrayList userRoleArray;
		String UserLocation = null;
		String userID = null;
        DataSet m_dsQuery = null;


        protected  string reloadPage = @"
		<script language='javascript'>
		location.reload();
		<" + "/script" + ">";
		protected System.Web.UI.WebControls.TextBox txtmsg;
		protected System.Web.UI.WebControls.TextBox txterrmsg;
		protected System.Web.UI.WebControls.TextBox txtmsgRe;
		protected System.Web.UI.WebControls.Button btnShowReport;
		protected System.Web.UI.WebControls.Button btnTruckIDSrch;
		protected System.Web.UI.WebControls.Label lblcolStatus;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox txtmsgm2;
		protected System.Web.UI.WebControls.TextBox txtmsgm1;
		protected System.Web.UI.WebControls.Button btnmsgm2;
		protected System.Web.UI.WebControls.Button btnShowTKLHide;
		protected System.Web.UI.WebControls.RadioButton rdbCloseBatch;
		protected System.Web.UI.WebControls.RadioButton rdbIgnorebatch;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.RadioButton rdbOpenBatch;
		protected System.Web.UI.WebControls.DropDownList ddlDriverID;
		protected System.Web.UI.WebControls.Button btnDriverSearch;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Button btnmsgm3;
		protected System.Web.UI.WebControls.Button btnmsgm1;
		protected System.Web.UI.WebControls.Button btnShowReportExcel;		
		protected System.Web.UI.WebControls.RadioButton rdbIgnore;
		protected System.Web.UI.WebControls.RadioButton rdbBoth;
		protected System.Web.UI.WebControls.Button btnHidReport;

		protected  string closeWindow = @"
		<script language='javascript'>
		window.close();
		<" + "/script" + ">";
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			getSessionTimeOut(true);
			Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			userID = Session["userID"].ToString()  ; 
			Session["batch_type"] = "";
			txtBatchNo.Attributes.Add("onkeyDown", "KeyDownExe();");
			//btnManifestScannedPkgs.Attributes.Add("Onclick", "javascript:return confirm(document.getElementById('txtmsg').value)");
			//btnManifestScannedPkgs.Attributes.Add("Onclick","javascript:return confirm('You are delete?')");
			btnReceivingIsComplete.Attributes.Add("Onclick", "javascript:return confirm(document.getElementById('txtmsgRe').value)");
			if(!Page.IsPostBack)
			{
				LoadComboLists();
				BindComboLists();
				LoadBatch();
				LoadTruck();
				LoadDriverID();
				LoadUserLocation();
				SetInitialFocus(txtBatchNo);
				getUser();
				EnableControls(false);
				if (getRole("OPSSU")!="OPSSU")
				{					
					ddlCurrentLocation.Enabled = false; 					
				}
				if (getRole("OPSSR")!="OPSSR")
				{					
					btnShowReport.Visible = false; 
					btnShowReportExcel.Visible = false; 					
				}
				ClearCriteria();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExcuteQuery.Click += new System.EventHandler(this.btnExcuteQuery_Click);
			this.btnShowReportExcel.Click += new System.EventHandler(this.btnShowReportExcel_Click);
			this.btnmsgm3.Click += new System.EventHandler(this.btnmsgm3_Click);
			this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
			this.btnOnArrival.Click += new System.EventHandler(this.btnOnArrival_Click);
			this.btnManifestScannedPkgs.Click += new System.EventHandler(this.btnManifestScannedPkgs_Click);
			this.btnViewVariances.Click += new System.EventHandler(this.btnViewVariances_Click);
			this.btnReceivingIsComplete.Click += new System.EventHandler(this.btnReceivingIsComplete_Click);
			this.btnOnDeparture.Click += new System.EventHandler(this.btnOnDeparture_Click);
			this.btnForwardManifest.Click += new System.EventHandler(this.btnForwardManifest_Click);
			this.btnTruckIDSrch.Click += new System.EventHandler(this.btnTruckIDSrch_Click);
			this.ddlCurrentLocation.SelectedIndexChanged += new System.EventHandler(this.ddlCurrentLocation_SelectedIndexChanged);
			this.rdbOrigin.CheckedChanged += new System.EventHandler(this.rdbOrigin_CheckedChanged);
			this.rdbTermination.CheckedChanged += new System.EventHandler(this.rdbTermination_CheckedChanged);
			this.rdbIgnore.CheckedChanged += new System.EventHandler(this.rdbIgnore_CheckedChanged);
			this.rdbBoth.CheckedChanged += new System.EventHandler(this.rdbBoth_CheckedChanged_1);
			this.btnDriverSearch.Click += new System.EventHandler(this.btnDriverSearch_Click);
			this.lbBatch.SelectedIndexChanged += new System.EventHandler(this.lbBatch_SelectedIndexChanged);
			this.btnShowReport.Click += new System.EventHandler(this.btnShowReport_Click);
			this.btnmsgm2.Click += new System.EventHandler(this.btnmsgm2_Click);
			this.btnShowTKLHide.Click += new System.EventHandler(this.btnShowTKLHide_Click);
			this.btnHidReport.Click += new System.EventHandler(this.btnHidReport_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.UniqueID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		} 

		public void LoadBatch()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet dataset = com.ties.classes.DbComboDAL.BatchIDQuery(strAppID,strEnterpriseID);	
			this.ddlBatchID.DataSource = dataset;

			ddlBatchID.DataTextField = dataset.Tables[0].Columns["path_code"].ToString() ;
			ddlBatchID.DataValueField = dataset.Tables[0].Columns["path_code"].ToString() ;
			this.ddlBatchID.DataBind();
			ListItem defItem = new ListItem();
			defItem.Text = "";
			
			defItem.Value = "";
			ddlBatchID.Items.Insert(0,(defItem));
		}

		public void LoadTruck()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet dataset = com.ties.classes.DbComboDAL.TruckIDQuery(strAppID,strEnterpriseID,"Y","Null",0);	
			this.ddlTruckID.DataSource = dataset;

			ddlTruckID.DataTextField = dataset.Tables[0].Columns["truckid"].ToString() ;
			ddlTruckID.DataValueField = dataset.Tables[0].Columns["truckid"].ToString() ;
			this.ddlTruckID.DataBind();
			ListItem defItem = new ListItem();
			defItem.Text = "";
			
			defItem.Value = "";
			ddlTruckID.Items.Insert(0,(defItem));
		}

		public void LoadDriverID()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet dataset = com.ties.classes.DbComboDAL.DriverIDQuery(strAppID,strEnterpriseID,"Y",0,"Null","N");	
			this.ddlDriverID.DataSource = dataset;

			ddlDriverID.DataTextField = dataset.Tables[0].Columns["emp_display"].ToString() ;
			ddlDriverID.DataValueField = dataset.Tables[0].Columns["emp_id"].ToString() ;
			this.ddlDriverID.DataBind();
			ListItem defItem = new ListItem();
			defItem.Text = " ";
			
			defItem.Value = "";
			ddlDriverID.Items.Insert(0,(defItem));
		}

		public void LoadUserLocation()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();

			DataSet dataset = com.ties.classes.DbComboDAL.UserLocationQuery(strAppID,strEnterpriseID);	
			this.ddlCurrentLocation.DataSource = dataset;

			ddlCurrentLocation.DataTextField = dataset.Tables[0].Columns["origin_code"].ToString() ;
			ddlCurrentLocation.DataValueField = dataset.Tables[0].Columns["origin_code"].ToString() ;
			this.ddlCurrentLocation.DataBind();
			//			ListItem defItem = new ListItem();
			//			defItem.Text = " ";
			//			
			//			defItem.Value = "";
			//			ddlCurrentLocation.Items.Insert(0,(defItem));
		}
		private void LoadComboLists()
		{
			m_dvBatchTypeOptions = GetBatchTypeOptions(true);
		}
		private void BindComboLists()
		{
			ddlBatchType.DataSource = (ICollection)m_dvBatchTypeOptions;
			ddlBatchType.DataTextField = "Text";
			ddlBatchType.DataValueField = "StringValue";
			ddlBatchType.DataBind();
		}

		private DataView GetBatchTypeOptions(bool showNilOption)
		{
			DataTable dtBatchTypeOptions = new DataTable();
			
			dtBatchTypeOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtBatchTypeOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList BatchTypeOptionArray = Utility.GetCodeValues(m_strAppID,m_strCulture,"delivery_type",CodeValueType.StringValue);
			if(showNilOption)
			{
				DataRow drNilRow = dtBatchTypeOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtBatchTypeOptions.Rows.Add(drNilRow);
			}
			foreach(SystemCode BatchTypeSysCode in BatchTypeOptionArray)
			{
				DataRow drEach = dtBatchTypeOptions.NewRow();
				drEach[0] = BatchTypeSysCode.Text;
				drEach[1] = BatchTypeSysCode.StringValue;
				dtBatchTypeOptions.Rows.Add(drEach);
			}

			DataView dvBatchTypeOptions = new DataView(dtBatchTypeOptions);
			return dvBatchTypeOptions;

		}

		private void btnExcuteQuery_Click(object sender, System.EventArgs e)
		{
			try
			{
				lblColTruckID01Display.Text="";
				lblGrossWtDisplay.Text="";
				lblTareWtDisplay.Text="";
				lblLoadedWtDisplay.Text="";
				lbShipping.Items.Clear();  
				EnableControlsOnClickBtnQuery(false);
				ViewState["statusreport"]=null;
				lblValISector.Text = "";
				if (txtBatchNo.Text =="" && ddlTruckID.SelectedItem.Text =="" && ddlBatchID.SelectedItem.Text =="" && txtBatchDate.Text == "" && ddlBatchType.SelectedItem.Text =="")
				{
					lblValISector.Text ="Please enter selection criteria."; 
				}
				else if (txtBatchNo.Text !="" && (ddlTruckID.SelectedItem.Text !="" || ddlBatchID.SelectedItem.Text !="" || txtBatchDate.Text != "" || ddlBatchType.SelectedItem.Text !=""))
				{
					lblValISector.Text ="Please enter either Batch Number or the alternative selection criteria."; 
				}
				else
				{
					lblValISector.Text = "";
					//btnExcuteQuery.Enabled =false; 
					GetBatchList();
				}
				rdbIgnore.Enabled =false;
				rdbOrigin.Enabled =false;
				rdbTermination.Enabled =false;
				rdbBoth.Enabled=false;
 
				rdbOpenBatch.Enabled =false;
				rdbCloseBatch.Enabled =false;
				rdbIgnorebatch.Enabled =false; 

				ddlDriverID.Enabled = false; 
				btnDriverSearch.Enabled =false; 
			}
			catch(Exception ex)
			{
				this.lblErrrMsg.Text = "Please wait up to 5 minutes while the batch is processing.";
			}

		}
		private string getRole(String strRole)
		{
			User user = new User();
			user.UserID = userID;
			userRoleArray = com.common.RBAC.RBACManager.GetAllRoles(m_strAppID, m_strEnterpriseID, user);
			Role role;

			for(int i=0;i<userRoleArray.Count;i++)
			{
				role = (Role)userRoleArray[i];				
				if (role.RoleName.ToUpper().Trim() == strRole) return strRole;
			}
			return " ";
		}


		private void getUser()
		{
			string loc = com.common.RBAC.RBACManager.GetUser(m_strAppID,m_strEnterpriseID, userID).UserLocation.ToString();
			ddlCurrentLocation.SelectedValue = loc; 
			ViewState["loc"] = loc;
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			ClearCriteria();
			SetInitialFocus(txtBatchNo); 
			btnExcuteQuery.Enabled =true; 
			EnableControlsOnClickBtnQuery(false);
			lbBatch.Items.Clear();  
			lbShipping.Items.Clear(); 
		}

		private void ClearCriteria()
		{
			txtBatchNo.Text ="";
			lblValISector.Text ="";
			ddlTruckID.SelectedValue ="" ;
			ddlBatchID.SelectedValue ="";
			ddlDriverID.Enabled =true;
			ddlDriverID.SelectedValue =""; 
			btnDriverSearch.Enabled =true; 
			txtBatchDate.Text ="";
			ddlBatchType.SelectedValue =""; 

			if (getRole("OPSSU")=="OPSSU")
			{					
				rdbIgnore.Checked = true ;
				rdbBoth.Checked=false;
				rdbOrigin.Enabled = true;
				rdbTermination.Enabled =true; 
				rdbIgnore.Enabled =true;
				rdbBoth.Enabled = true;				
			}
			else
			{
				rdbIgnore.Checked = false;
				rdbOrigin.Enabled = true;
				rdbTermination.Enabled =true; 
				rdbIgnore.Enabled = false;
				rdbBoth.Enabled = true;	
				rdbBoth.Checked = true;
			}


			rdbIgnorebatch.Checked = true;
			rdbIgnorebatch.Enabled =true;
			rdbOpenBatch.Enabled = true;
			rdbTermination.Enabled =true; 
			rdbCloseBatch.Enabled =true;

			lblColTruckID01Display.Text ="";
			lblGrossWtDisplay.Text = ""; 
			lblTareWtDisplay.Text = "";
			lblLoadedWtDisplay.Text = "" ;
		}

		private void GetBatchList()
		{
			try
			{
				Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				string chkloc ="";
				string chkOpen = "";
				if (rdbOrigin.Checked )
					chkloc ="1";
				else if (rdbTermination.Checked )
					chkloc ="2";
				else if (rdbIgnore.Checked )
					chkloc ="3";
				else if (rdbBoth.Checked )
					chkloc ="4";

				if (rdbOpenBatch.Checked )
					chkOpen ="1";
				else if (rdbCloseBatch.Checked )
					chkOpen ="2";
				else if (rdbIgnorebatch.Checked )
					chkOpen ="3";

				DataSet con = BatchManifest.GetBatchList( m_strAppID,m_strEnterpriseID,txtBatchNo.Text,ddlTruckID.SelectedValue,ddlBatchID.SelectedValue,txtBatchDate.Text ,ddlBatchType.SelectedValue,ddlCurrentLocation.SelectedItem.Text,chkloc,chkOpen,ddlDriverID.SelectedValue);
				string data ="";
				if (con.Tables[0].Rows.Count >0)
				{
					lbBatch.Items.Clear();
				
					for(int i=0; i < con.Tables[0].Rows.Count; i++)
					{
						data  = con.Tables[0].Rows[i]["Batch_No"].ToString();
						data += utility.generateHTMLSpace(12-con.Tables[0].Rows[i]["Batch_No"].ToString().Length);
						data += con.Tables[0].Rows[i]["Truck_ID"].ToString();
						data += utility.generateHTMLSpace(10-con.Tables[0].Rows[i]["Truck_ID"].ToString().Length);
						data += con.Tables[0].Rows[i]["Batch_ID"].ToString();
						data += utility.generateHTMLSpace(11-con.Tables[0].Rows[i]["Batch_ID"].ToString().Length);
						data += String.Format("{0:dd/MM/yyyy}",con.Tables[0].Rows[i]["Batch_Date"]);
						data += utility.generateHTMLSpace(12-String.Format("{0:dd/MM/yyyy}",con.Tables[0].Rows[i]["Batch_Date"]).Length);
						data += con.Tables[0].Rows[i]["Batch_Type"].ToString();
						data += utility.generateHTMLSpace(7-con.Tables[0].Rows[i]["Batch_Type"].ToString().Length);
						data += con.Tables[0].Rows[i]["cons_on_truck"].ToString();
						data += utility.generateHTMLSpace(7-con.Tables[0].Rows[i]["cons_on_truck"].ToString().Length);
						data += con.Tables[0].Rows[i]["total_pkg"].ToString();
						data += utility.generateHTMLSpace(7-con.Tables[0].Rows[i]["total_pkg"].ToString().Length);
						data += con.Tables[0].Rows[i]["pkg_on_track"].ToString();
						data += utility.generateHTMLSpace(7-con.Tables[0].Rows[i]["pkg_on_track"].ToString().Length);
						data += con.Tables[0].Rows[i]["weight"].ToString();
						data += utility.generateHTMLSpace(8-con.Tables[0].Rows[i]["weight"].ToString().Length);
						data += con.Tables[0].Rows[i]["Status"].ToString();
						data += utility.generateHTMLSpace(7-con.Tables[0].Rows[i]["Status"].ToString().Length);
						data += con.Tables[0].Rows[i]["Status_UpdatedBy"].ToString();

						if (lbBatch.Items.Count.ToString()=="0")
						{	
							lbBatch.Items.Insert(lbBatch.Items.Count,data);
						}
						else
						{
							ListItem li = new ListItem(data);
							if (!lbBatch.Items.Contains(li))
							{
								lbBatch.Items.Insert(lbBatch.Items.Count,data);
							 
							}
						}
					}
				}
				else
				{
					lblValISector.Text ="No matches found.";
				}
			}
			catch(Exception ex)
			{
				this.lblErrrMsg.Text = "Please wait up to 5 minutes before process this batch.";
			}

		}

		private void lbBatch_SelectedIndexChanged(object sender, System.EventArgs e)
		{

			//			string space = System.Web.HttpUtility.HtmlDecode("&nbsp;");
			//
			//			string ndata = lbBatch.SelectedValue.Replace(space,","); 
			//			string[] data = ndata.Split(','); 
			//  
			//			string batch_no = data[0];
			//			string chkloc ="";
			//			string chkOpen="";
			//			if (rdbOrigin.Checked )
			//				chkloc ="1";
			//			else if (rdbTermination.Checked )
			//				chkloc ="2";
			//			else if (rdbIgnore.Checked )
			//				chkloc ="3";
			//
			//			if (rdbOpenBatch.Checked )
			//				chkOpen ="1";
			//			else if (rdbCloseBatch.Checked )
			//				chkOpen ="2";
			//			else if (rdbIgnorebatch.Checked )
			//				chkOpen ="3";
			//
			//			//GetBatchDetails
			//			DataSet con = BatchManifest.GetBatchList( m_strAppID,m_strEnterpriseID,batch_no,"","","" ,"",ddlCurrentLocation.SelectedItem.Text,chkloc,chkOpen,ddlDriverID.SelectedValue );
			//			Session["con"] = con;
			//			string truckid ="";
			//			string weight ="";
			//			int TareWt =0;
			//
			//			DataSet Scan =BatchManifest.GetEnableScanPkg(m_strAppID,m_strEnterpriseID,(string)ViewState["batch_no"]);
			//			if (Scan.Tables[0].Rows.Count > 0 )
			//				ViewState["EnableScannedPkgs"] = Scan.Tables[0].Rows[0]["cbatch_no"].ToString(); 
			//
			//			//Batch Expiration
			//			if (DateTime.Parse(con.Tables[0].Rows[0]["batch_date"].ToString())  < DateTime.Parse(result.Tables[0].Rows[0]["daytoread"].ToString()))
			//				ViewState["EnableScanPkg"] = "N";
			//			else
			//				ViewState["EnableScanPkg"] = "Y";
			//
			//			//GetPkg Scanned on this Batch
			//			DataSet cRowScan = BatchManifest.GetRowScan( m_strAppID,m_strEnterpriseID,batch_no,ddlCurrentLocation.SelectedItem.Text);
			//			ViewState["EnableOnDepart"] = "N";
			//			if (cRowScan.Tables[0].Rows.Count > 0)
			//			{
			//				if (cRowScan.Tables[0].Rows[0]["cbatch_no"].ToString() =="0" )
			//					ViewState["EnableOnDepart"] = "N";
			//				else
			//					ViewState["EnableOnDepart"] = "Y";
			//			}
			//			if (con.Tables[0].Rows.Count > 0 )
			//			{
			//				truckid = con.Tables[0].Rows[0]["truck_id"].ToString();
			//				weight = con.Tables[0].Rows[0]["weight"].ToString();
			//				ViewState["batch_no"] = batch_no;
			//				Session["batch_type"] = con.Tables[0].Rows[0]["batch_type"].ToString();
			//				ViewState["batch_type_"] = con.Tables[0].Rows[0]["batch_type"].ToString();
			//				ViewState["batch_id"] = con.Tables[0].Rows[0]["batch_id"].ToString();
			//				ViewState["truck_id"] = con.Tables[0].Rows[0]["truck_id"].ToString();
			//
			//				ViewState["statusreport"] = con.Tables[0].Rows[0]["Status_code"].ToString();
			//
			//				if (weight == "")
			//					weight = "0";
			//
			//			}
			//
			//			DataSet truck = BatchManifest.GetBatchDetail_TruckLoad( m_strAppID,m_strEnterpriseID,truckid );
			//			if (truck.Tables[0].Rows.Count >0  )
			//			{
			//				if (lblTareWtDisplay.Text =="" )
			//					TareWt = 0;
			//				else
			//					TareWt = Convert.ToInt32(truck.Tables[0].Rows[0]["Tare_Weight"].ToString());
			//
			//				lblColTruckID01Display.Text = truck.Tables[0].Rows[0]["truckid"].ToString();
			//				lblGrossWtDisplay.Text = truck.Tables[0].Rows[0]["Gross_Weight"].ToString(); 
			//				lblTareWtDisplay.Text = truck.Tables[0].Rows[0]["Tare_Weight"].ToString();
			//				lblLoadedWtDisplay.Text = Convert.ToString((TareWt + Convert.ToInt32(weight))) ;
			//				
			//				lbShipping.Items.Clear();  
			//				GetShippingDetail();
			//				EnableControls(true);
			//				ScannedPkgs();
			//			}
			//			else
			//			{
			//				lblColTruckID01Display.Text ="";
			//				lblGrossWtDisplay.Text = ""; 
			//				lblTareWtDisplay.Text = "";
			//				lblLoadedWtDisplay.Text = "" ;
			//				lbShipping.Items.Clear();  
			//				GetShippingDetail();
			//				EnableControls(true);
			//				ScannedPkgs();
			//			}
			//
			//			if(lbBatch.SelectedIndex!=-1)
			//			{
			//				int sumTareWt = 0;
			//				int sumGrossWt = 0;
			//				int sumLoadedWt =0;
			//				int i =0;
			//				foreach(ListItem lb in lbBatch.Items)
			//				{
			//					if(lb.Selected)
			//					{
			//						//����ա������ space �͹ bind lbBatch ��ͧ���������¹�����ǹ�����¹Ф�Ѻ ����鹨����ҼԴ 
			//						string sbatchno=lb.Value.Substring(0,lb.Value.IndexOf(space)+1).Trim();
			//						string struckid=lb.Value.Substring(12,lb.Value.IndexOf(space)+1).Trim();
			//						string sbatchtype=lb.Value.Substring(45,1).Trim();
			//						string sweight = lb.Value.Substring(72,lb.Value.IndexOf(space)+1).Trim();
			//						string tmpVal = struckid+"/"+sbatchtype;
			//
			//						
			//						if(ViewState["tmpVal"]==null)
			//							ViewState["tmpVal"]= tmpVal;
			//
			//						if(ViewState["tmpVal"].ToString()== tmpVal)
			//						{
			//							DataSet sumtruck = BatchManifest.GetBatchDetail_TruckLoad( m_strAppID,m_strEnterpriseID,struckid );
			//
			//							if (truck.Tables[0].Rows.Count >0  )
			//							{
			//								if (lblTareWtDisplay.Text =="" )
			//									TareWt = 0;
			//								else
			//									TareWt = Convert.ToInt32(truck.Tables[0].Rows[0]["Tare_Weight"].ToString());
			//								
			//								if (sweight=="")
			//									sweight="0";
			//
			//								sumTareWt += Convert.ToInt32(sumtruck.Tables[0].Rows[0]["Tare_Weight"]);
			//								sumGrossWt += Convert.ToInt32(sumtruck.Tables[0].Rows[0]["Gross_Weight"]);
			//								sumLoadedWt +=  Convert.ToInt32((TareWt + Convert.ToInt32(sweight)));
			//
			//
			//								lblColTruckID01Display.Text = sumtruck.Tables[0].Rows[0]["truckid"].ToString();
			//				
			//								lbShipping.Items.Clear();  
			//								GetShippingDetail();
			//								EnableControls(true);
			//								ScannedPkgs();
			//								i++;
			//							}
			//						}	
			//						else
			//							lb.Selected=false;
			//					}
			//			
			//				}
			//				if (i>1)
			//				{
			//					lblGrossWtDisplay.Text = sumGrossWt.ToString();
			//					lblTareWtDisplay.Text = sumTareWt.ToString();
			//					lblLoadedWtDisplay.Text = Convert.ToString(sumLoadedWt) ;
			//				}
			//				
			//				ViewState["tmpVal"]=null;
			//
			//			}
			//			if (con.Tables[0].Rows.Count >0  )
			//			{
			//				DateTime BatchDate  = Convert.ToDateTime(con.Tables[0].Rows[0]["Batch_date"]);
			//				DateTime cBatchDate = new DateTime(BatchDate.Year,BatchDate.Month,BatchDate.Day);
			//			
			//				DataSet dsDayToRead = new DataSet();
			//				dsDayToRead = BatchManifest.GetDayToRead(utility.GetAppID(),utility.GetEnterpriseID());
			//				if (dsDayToRead.Tables[0].Rows.Count>0)
			//				{
			//					DateTime cNow = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day);
			//					System.TimeSpan Diff =cNow.Subtract(cBatchDate);
			//					if(Diff.Days > int.Parse(dsDayToRead.Tables[0].Rows[0]["code_str_value"].ToString()) )
			//					{
			//						btnOnArrival.Enabled=false;
			//						btnOnDeparture.Enabled=false;
			//						btnReceivingIsComplete.Enabled=false;
			//					}
			//					else
			//					{
			//						btnOnArrival.Enabled=true;
			//						btnOnDeparture.Enabled=true;
			//						btnReceivingIsComplete.Enabled=true;
			//					}
			//				} 
			//			}

			string position = this.lbBatch.SelectedIndex.ToString();
			string scrollScript="" ;
			scrollScript += "<script language='javascript'>";
			scrollScript += "document.forms[0].lbBatch.selectedIndex = " +
				position + ";";
			scrollScript += "<" + "/" + "script>";

			Page.RegisterStartupScript("", scrollScript);

			btnRefresh_Click(null,null);
		}

		private void EnableControlsOnClickBtnQuery(bool EnableValue)
		{
			btnRefresh.Enabled = EnableValue;
			btnOnArrival.Enabled = EnableValue;
			btnManifestScannedPkgs.Enabled  = EnableValue;
			btnViewVariances.Enabled  = EnableValue;
			btnReceivingIsComplete.Enabled  = EnableValue;
			btnOnDeparture.Enabled  = EnableValue;
			btnForwardManifest.Enabled  = EnableValue;
			btnShowReport.Enabled  = EnableValue;
			btnShowReportExcel.Enabled  = EnableValue;
		}
		private void EnableControls(bool EnableValue)
		{
			btnRefresh.Enabled = EnableValue;
			if ((string)ViewState["EnableOnArr"]=="N")
				btnOnArrival.Enabled = !EnableValue;
			else
				btnOnArrival.Enabled = EnableValue;
			//			if ((string)ViewState["EnableScannedPkgs"]=="0")
			//				btnManifestScannedPkgs.Enabled  = !EnableValue;
			//			else 
			if ((string)ViewState["EnableScanPkg"] == "N")
				btnManifestScannedPkgs.Enabled  = !EnableValue;
			else
				btnManifestScannedPkgs.Enabled  = EnableValue;

			btnViewVariances.Enabled  = EnableValue;
			btnShowReport.Enabled  = EnableValue;
			btnShowReportExcel.Enabled  = EnableValue;

			if (Session["batch_type"].ToString() =="W" )
				btnReceivingIsComplete.Enabled  = !EnableValue;
			else if ((string)ViewState["EnableScanPkg"] == "N")
				btnReceivingIsComplete.Enabled  = !EnableValue;
			else if ((string)ViewState["EnableReCom"] =="N")
				btnReceivingIsComplete.Enabled  = !EnableValue;
			else
				btnReceivingIsComplete.Enabled  = EnableValue;

			if (Session["batch_type"].ToString() =="W" )
				btnOnDeparture.Enabled  = !EnableValue;
			else if ((string)ViewState["EnableOnDepart"] =="N")
				btnOnDeparture.Enabled  = !EnableValue;
			else if ((string)ViewState["EnableOnDepForDestStation"]=="N")
				btnOnDeparture.Enabled  = !EnableValue;
			else
				btnOnDeparture.Enabled  = EnableValue;

			if (Session["batch_type"].ToString() =="W" || Session["batch_type"].ToString() =="S")
				btnForwardManifest.Enabled  = !EnableValue;
			else if ((string)ViewState["EnableScannedPkgs"]=="0")
				btnForwardManifest.Enabled  = !EnableValue;
			else 
				btnForwardManifest.Enabled  = EnableValue;
		}

		private void GetShippingDetail()
		{
			ViewState["ConfirmScannedPkgs"]=null;
			ViewState["Arr_DT"]=null;

			Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			DataSet con = null;
			con = BatchManifest.GetBatchDetail_Shipping_BMCP( m_strAppID,m_strEnterpriseID, (string)ViewState["batch_no"]);

			string data ="";
			if (Session["batch_type"].ToString() !="L")
			{
				if (con.Tables[0].Rows.Count >0)
				{
					for(int i=con.Tables[0].Rows.Count-1; i >=0; i--)
					{
						ViewState["RecIsCom"]=con.Tables[0].Rows[i]["Recv_Is_Complete"].ToString();
						ViewState["arr_dc"] =con.Tables[0].Rows[i]["Arr_DC"].ToString();
					}
					for(int i=0; i <con.Tables[0].Rows.Count; i++)
					{
						data  = String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Updated_DT"]);
						data += utility.generateHTMLSpace(17-String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Updated_DT"]).Length);
						data += con.Tables[0].Rows[i]["Dep_DC"].ToString();
						data += utility.generateHTMLSpace(6-con.Tables[0].Rows[i]["Dep_DC"].ToString().Length);
						data += String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Dep_DT"]);
						data += utility.generateHTMLSpace(20-String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Dep_DT"]).Length);
						data += con.Tables[0].Rows[i]["Con_SOP_Scan_Count"].ToString();
						data += utility.generateHTMLSpace(5-con.Tables[0].Rows[i]["Con_SOP_Scan_Count"].ToString().Length);
						data += con.Tables[0].Rows[i]["Pkg_SOP_Scan_Count"].ToString();
						data += utility.generateHTMLSpace(5-con.Tables[0].Rows[i]["Pkg_SOP_Scan_Count"].ToString().Length);
						data += con.Tables[0].Rows[i]["Arr_DC"].ToString();
						data += utility.generateHTMLSpace(6-con.Tables[0].Rows[i]["Arr_DC"].ToString().Length);
						data += String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Arr_DT"]);
						data += utility.generateHTMLSpace(20-String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Arr_DT"]).Length);
						data += con.Tables[0].Rows[i]["Con_SIP_Scan_Count"].ToString();
						data += utility.generateHTMLSpace(5-con.Tables[0].Rows[i]["Con_SIP_Scan_Count"].ToString().Length);
						data += con.Tables[0].Rows[i]["Pkg_SIP_Scan_Count"].ToString();
						//data += utility.generateHTMLSpace(9-con.Tables[0].Rows[i]["Pkg_SIP_Scan_Count"].ToString().Length);
						lbShipping.Items.Insert(lbShipping.Items.Count,data);
					}
			
				}
			}
			else // Batch Type = L
			{
				if (con.Tables[0].Rows.Count >0)
				{
					for(int i=con.Tables[0].Rows.Count-1; i >=0; i--)
					{
						ViewState["RecIsCom"]=con.Tables[0].Rows[i]["Recv_Is_Complete"].ToString();
						ViewState["arr_dc"] =con.Tables[0].Rows[i]["Arr_DC"].ToString();
					}
					for(int i=0; i <con.Tables[0].Rows.Count; i++)
					{
						data  = String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Updated_DT"]);
						data += utility.generateHTMLSpace(17-String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Updated_DT"]).Length);
						data += con.Tables[0].Rows[i]["Dep_DC"].ToString();
						data += utility.generateHTMLSpace(6-con.Tables[0].Rows[i]["Dep_DC"].ToString().Length);
						data += String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Dep_DT"]);
						data += utility.generateHTMLSpace(20-String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Dep_DT"]).Length);
						data += con.Tables[0].Rows[i]["Con_SOP_Scan_Count"].ToString();
						data += utility.generateHTMLSpace(5-con.Tables[0].Rows[i]["Con_SOP_Scan_Count"].ToString().Length);
						data += con.Tables[0].Rows[i]["Pkg_SOP_Scan_Count"].ToString();
						data += utility.generateHTMLSpace(5-con.Tables[0].Rows[i]["Pkg_SOP_Scan_Count"].ToString().Length);
						data += con.Tables[0].Rows[i]["Arr_DC"].ToString();
						data += utility.generateHTMLSpace(6-con.Tables[0].Rows[i]["Arr_DC"].ToString().Length);
						data += String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Arr_DT"]);
						data += utility.generateHTMLSpace(20-String.Format("{0:dd/MM/yyyy HH:mm}",con.Tables[0].Rows[i]["Arr_DT"]).Length);
						data += con.Tables[0].Rows[i]["Con_SIP_Scan_Count"].ToString();
						data += utility.generateHTMLSpace(5-con.Tables[0].Rows[i]["Con_SIP_Scan_Count"].ToString().Length);
						data += con.Tables[0].Rows[i]["Pkg_SIP_Scan_Count"].ToString();
						lbShipping.Items.Insert(lbShipping.Items.Count,data);
					}
			
				}
			}
		}

		private void btnRefresh_Click(object sender, System.EventArgs e)
		{
			try
			{
				this.btnOnArrival.Enabled = true;
				this.btnReceivingIsComplete.Enabled = true;
				this.btnOnDeparture.Enabled = true;
				this.btnForwardManifest.Enabled = true;
				this.btnManifestScannedPkgs.Enabled = true;
				this.btnViewVariances.Enabled = true;
				this.btnShowReport.Enabled = true;
				this.btnShowReportExcel.Enabled = true;
				this.btnRefresh.Enabled=true;

				int iBatchSelected = -1;
				if(this.lbBatch.SelectedIndex>-1)
					iBatchSelected = this.lbBatch.SelectedIndex;

				string space = System.Web.HttpUtility.HtmlDecode("&nbsp;");

				string ndata = lbBatch.SelectedValue.Replace(space,","); 
				string[] data = ndata.Split(','); 
				string batch_no = data[0];
				string chkloc ="";
				string chkOpen="";
				if (rdbOrigin.Checked )
					chkloc ="1";
				else if (rdbTermination.Checked )
					chkloc ="2";
				else if (rdbIgnore.Checked )
					chkloc ="3";
				else if (rdbBoth.Checked )
					chkloc ="4";

				if (rdbOpenBatch.Checked )
					chkOpen ="1";
				else if (rdbCloseBatch.Checked )
					chkOpen ="2";
				else if (rdbIgnorebatch.Checked )
					chkOpen ="3";

				//Departure button Enable/Disable
				DataSet cRowScan = BatchManifest.GetRowScan( m_strAppID,m_strEnterpriseID,batch_no,ddlCurrentLocation.SelectedItem.Text);
				ViewState["EnableOnDepart"] = "N";
				if (cRowScan.Tables[0].Rows.Count > 0)
				{
					if (cRowScan.Tables[0].Rows[0]["cbatch_no"].ToString() =="0" )
						ViewState["EnableOnDepart"] = "N";
					else
						ViewState["EnableOnDepart"] = "Y";
				}
				DataSet depdtisnull = BatchManifest.GetDepDTISNULL(m_strAppID,m_strEnterpriseID,batch_no);
				if (depdtisnull.Tables[0].Rows.Count > 0  )
				{
					if (depdtisnull.Tables[0].Rows[0]["cbatch_no"].ToString() !="0" )
						ViewState["EnableDepart"] = "N";
					else
						ViewState["EnableDepart"] = "Y";
				}
				DataSet con = BatchManifest.GetBatchList( m_strAppID,m_strEnterpriseID,batch_no,"","","" ,"",ddlCurrentLocation.SelectedItem.Text,chkloc,chkOpen,ddlDriverID.SelectedValue);
				Session["con"] = con;
				string truckid ="";
				string weight ="";
				decimal TareWt =0;

				if (con.Tables[0].Rows.Count > 0 )
				{
					truckid = con.Tables[0].Rows[0]["truck_id"].ToString();
					weight = con.Tables[0].Rows[0]["weight"].ToString();
					ViewState["batch_no"] = batch_no;
					Session["batch_type"] = con.Tables[0].Rows[0]["batch_type"].ToString();
					ViewState["batch_type_"] = con.Tables[0].Rows[0]["batch_type"].ToString();
					ViewState["batch_id"] = con.Tables[0].Rows[0]["batch_id"].ToString();
					ViewState["truck_id"] = con.Tables[0].Rows[0]["truck_id"].ToString();
					//Status DIRTY, IPGRS, READY for report warining
					ViewState["statusreport"] = con.Tables[0].Rows[0]["Status_code"].ToString();
					if (weight == "")
						weight = "0";

					//Expire or not
					ViewState["EnableScanPkg"] = "N";
					DataSet result = BatchManifest.GetDayToRead(m_strAppID,m_strEnterpriseID); 
					if (DateTime.Parse(con.Tables[0].Rows[0]["batch_date"].ToString())  < DateTime.Parse(result.Tables[0].Rows[0]["daytoread"].ToString()))
						ViewState["EnableScanPkg"] = "N";
					else
						ViewState["EnableScanPkg"] = "Y";
				
					DataSet desOrigin =BatchManifest.GetDestinationAllType(m_strAppID,m_strEnterpriseID,con.Tables[0].Rows[0]["Batch_ID"].ToString());
					if (desOrigin.Tables[0].Rows.Count > 0 )
					{
						ViewState["destination_station"] = desOrigin.Tables[0].Rows[0]["destination_station"].ToString(); 
						ViewState["origin_station"] = desOrigin.Tables[0].Rows[0]["origin_station"].ToString(); 
					}

					DataSet Scan =BatchManifest.GetEnableScanPkg(m_strAppID,m_strEnterpriseID,(string)ViewState["batch_no"]);
					if (Scan.Tables[0].Rows.Count > 0 )
						ViewState["EnableScannedPkgs"] = Scan.Tables[0].Rows[0]["cbatch_no"].ToString(); 


					//Get Departure/Arrival Records
					DataSet DepArr = BatchManifest.GetBatchDetail_Shipping_BMCP( m_strAppID,m_strEnterpriseID, (string)ViewState["batch_no"]);
					string strCurrentLocDepDT=null;
					string strCurrentLocArrDT=null;
					string strOriginDepDT=null;
					string strDestinationArrDT=null;
					string strArrivalDTonCurrentLocationDepature=null;
					string strBatchOriginStation=null;
					string strBatchDestinationStation=null;
					bool bCurrentLocInDepDCs = false;
					bool bCurrentLocInArrDCs = false;

					strBatchOriginStation = ViewState["origin_station"].ToString().ToUpper();
					strBatchDestinationStation = ViewState["destination_station"].ToString().ToUpper();

					//Retrieve Dep & Arr 
					if(DepArr.Tables[0].Rows.Count>0)
					{
						for(int i=DepArr.Tables[0].Rows.Count-1; i >=0; i--)
						{
							if(DepArr.Tables[0].Rows[i]["Dep_DC"].ToString()==ddlCurrentLocation.SelectedValue.ToString().Trim())
							{
								bCurrentLocInDepDCs = true;
								if(DepArr.Tables[0].Rows[i]["Dep_DT"]!=System.DBNull.Value)
								{
									strCurrentLocDepDT = DepArr.Tables[0].Rows[i]["Dep_DT"].ToString();
									if(DepArr.Tables[0].Rows[i]["Arr_DT"]!=System.DBNull.Value)
										strArrivalDTonCurrentLocationDepature = DepArr.Tables[0].Rows[i]["Arr_DT"].ToString();
								}
							}

							if(DepArr.Tables[0].Rows[i]["Arr_DC"].ToString()==ddlCurrentLocation.SelectedValue.ToString().Trim())
							{
								bCurrentLocInArrDCs = true;
								if(DepArr.Tables[0].Rows[i]["Arr_DT"]!=System.DBNull.Value)
									strCurrentLocArrDT = DepArr.Tables[0].Rows[i]["Arr_DT"].ToString();
							}

							if(DepArr.Tables[0].Rows[i]["Dep_DC"].ToString()==ViewState["origin_station"].ToString().ToUpper())
								if(DepArr.Tables[0].Rows[i]["Dep_DT"]!=System.DBNull.Value)
									strOriginDepDT = DepArr.Tables[0].Rows[i]["Dep_DT"].ToString();	

							if(DepArr.Tables[0].Rows[i]["Arr_DC"].ToString()==ViewState["destination_station"].ToString().ToUpper())
								if(DepArr.Tables[0].Rows[i]["Arr_DT"]!=System.DBNull.Value)
									strDestinationArrDT = DepArr.Tables[0].Rows[i]["Arr_DT"].ToString();	
						}

					}


					//Case Batch_Status <>'N' or Batch Date is expired
					if(con.Tables[0].Rows[0]["Batch_Status"].ToString()!="N" || ViewState["EnableScanPkg"].ToString()=="N")
					{
						this.btnOnArrival.Enabled = false;
						this.btnManifestScannedPkgs.Enabled = false;
						this.btnReceivingIsComplete.Enabled = false;
						this.btnOnDeparture.Enabled = false;
						this.btnForwardManifest.Enabled = false;
						if(!bCurrentLocInDepDCs && !bCurrentLocInArrDCs && (ViewState["origin_station"].ToString().ToUpper()!=ddlCurrentLocation.SelectedValue.ToString()))
						{
							this.btnViewVariances.Enabled = false;
							this.btnShowReport.Enabled = false;
							this.btnShowReportExcel.Enabled = false;
						}

					}
					else
					{
						if(con.Tables[0].Rows[0]["Batch_Type"].ToString()=="W")
						{
							this.btnOnArrival.Enabled = false;
							this.btnReceivingIsComplete.Enabled = false;
							this.btnOnDeparture.Enabled = false;
							this.btnForwardManifest.Enabled = false;

							if(this.ddlCurrentLocation.SelectedValue.ToUpper()!=ViewState["origin_station"].ToString().ToUpper())
							{
								this.btnManifestScannedPkgs.Enabled = false;
								this.btnViewVariances.Enabled = false;
								this.btnShowReport.Enabled = false;
								this.btnShowReportExcel.Enabled = false;
							}
						}


						if(con.Tables[0].Rows[0]["Batch_Type"].ToString()=="S")
						{
							this.btnForwardManifest.Enabled = false;

							if(this.ddlCurrentLocation.SelectedValue.ToUpper()!=ViewState["origin_station"].ToString().ToUpper())
							{
								this.btnOnArrival.Enabled = false;
								this.btnManifestScannedPkgs.Enabled = false;
								this.btnReceivingIsComplete.Enabled = false;
								this.btnViewVariances.Enabled = false;
								this.btnShowReport.Enabled = false;
								this.btnShowReportExcel.Enabled = false;
								this.btnOnDeparture.Enabled = false;
							}
							else
							{
								if(strCurrentLocDepDT==null)
								{
									this.btnOnArrival.Enabled = false;
									this.btnReceivingIsComplete.Enabled = false;
								}
								else if(strCurrentLocDepDT!=null&strCurrentLocArrDT!=null)
								{
									this.btnOnDeparture.Enabled = false;
								}
								else if(strCurrentLocArrDT==null)
									this.btnReceivingIsComplete.Enabled = false;
							}
						}

                
						if(con.Tables[0].Rows[0]["Batch_Type"].ToString()=="A")
						{
							if(this.ddlCurrentLocation.SelectedValue.ToUpper()==strBatchOriginStation)
							{

								this.btnOnArrival.Enabled = false;
								this.btnReceivingIsComplete.Enabled = false;

								if(strOriginDepDT!=null&&strDestinationArrDT!=null)
								{
									this.btnOnDeparture.Enabled = false;
									this.btnManifestScannedPkgs.Enabled = false;
								}
							}
							else
							{
								if (ViewState["EnableDepart"].ToString() =="N" )
								{
									this.btnOnArrival.Enabled = false;
								}
								else
								{
									this.btnOnArrival.Enabled = true;
								}
							}

							if(this.ddlCurrentLocation.SelectedValue.ToUpper()==strBatchDestinationStation)
							{
								this.btnOnDeparture.Enabled = false;
								this.btnForwardManifest.Enabled = false;
							}
							else if(this.ddlCurrentLocation.SelectedValue.ToUpper()!=strBatchDestinationStation&&this.ddlCurrentLocation.SelectedValue.ToUpper()!=strBatchOriginStation)
							{
								this.btnViewVariances.Enabled = false;
								this.btnShowReport.Enabled = false;
								this.btnShowReportExcel.Enabled = false;
								this.btnOnArrival.Enabled = false;
								this.btnManifestScannedPkgs.Enabled = false;
								this.btnReceivingIsComplete.Enabled = false;
								this.btnOnDeparture.Enabled = false;
								this.btnForwardManifest.Enabled = false;
							}

						}

						if(con.Tables[0].Rows[0]["Batch_Type"].ToString()=="L")
						{
							if(this.ddlCurrentLocation.SelectedValue.ToUpper()==strBatchOriginStation)
							{
								this.btnOnArrival.Enabled = false;
								this.btnReceivingIsComplete.Enabled = false;

								if(strOriginDepDT!=null&&strArrivalDTonCurrentLocationDepature!=null)
								{
									this.btnOnDeparture.Enabled = false;
									this.btnManifestScannedPkgs.Enabled = false;
								}
							}
							else
							{
								if (ViewState["EnableDepart"].ToString() =="N" )
								{
									this.btnOnArrival.Enabled = false;
								}
								else
								{
									this.btnOnArrival.Enabled = true;
								}
							}

							if(this.ddlCurrentLocation.SelectedValue.ToUpper()==strBatchDestinationStation)
							{
								this.btnOnDeparture.Enabled = false;
								this.btnForwardManifest.Enabled = false;
								if(strCurrentLocArrDT==null)
									this.btnReceivingIsComplete.Enabled = false;
							}
							else if(this.ddlCurrentLocation.SelectedValue.ToUpper()!=strBatchDestinationStation&&this.ddlCurrentLocation.SelectedValue.ToUpper()!=strBatchOriginStation)
							{
								if(strCurrentLocArrDT!=null&&strCurrentLocDepDT!=null&&strArrivalDTonCurrentLocationDepature!=null)
								{
									this.btnOnArrival.Enabled = false;
									this.btnManifestScannedPkgs.Enabled = false;
									this.btnReceivingIsComplete.Enabled = false;
									this.btnOnDeparture.Enabled = false;
									this.btnForwardManifest.Enabled = false;
								}
								else if(strCurrentLocArrDT==null)
								{
									this.btnManifestScannedPkgs.Enabled = false;
									this.btnReceivingIsComplete.Enabled = false;
									this.btnOnDeparture.Enabled = false;
									this.btnForwardManifest.Enabled = false;
								}
								else if(strCurrentLocArrDT!=null&&strCurrentLocDepDT==null)
									this.btnForwardManifest.Enabled = false;
							}
						}

					}
					//If Batch Closed, close buttons as much as possible
					if(con.Tables[0].Rows[0]["Batch_Status"].ToString()=="C")
					{
						btnOnArrival.Enabled = false;
						btnManifestScannedPkgs.Enabled  = false;
						btnViewVariances.Enabled  = true;
						btnShowReport.Enabled  = true;
						btnShowReportExcel.Enabled  = true;
						btnReceivingIsComplete.Enabled  = false;
						btnOnDeparture.Enabled  = false;
						btnForwardManifest.Enabled  = false;
					}

				}
			

				//Get Truckload as usual
				DataSet truck = BatchManifest.GetBatchDetail_TruckLoad( m_strAppID,m_strEnterpriseID,truckid );
				if (truck.Tables[0].Rows.Count >0  )
				{
					lblTareWtDisplay.Text = truck.Tables[0].Rows[0]["Tare_Weight"].ToString();
					if (lblTareWtDisplay.Text =="" )
						TareWt = 0;
					else
						TareWt = Convert.ToInt32(truck.Tables[0].Rows[0]["Tare_Weight"].ToString());

					lblColTruckID01Display.Text = truck.Tables[0].Rows[0]["truckid"].ToString();
					lblGrossWtDisplay.Text = truck.Tables[0].Rows[0]["Gross_Weight"].ToString(); 
			
					lblLoadedWtDisplay.Text = Convert.ToString((TareWt + Convert.ToDecimal(weight))) ;
				
					lbShipping.Items.Clear();  
					GetShippingDetail();
					//EnableControls(true);
					ScannedPkgs();
				}
				else
				{
					lblColTruckID01Display.Text ="";
					lblGrossWtDisplay.Text = ""; 
					lblTareWtDisplay.Text = "";
					lblLoadedWtDisplay.Text = "" ;
					lbShipping.Items.Clear();  
					GetShippingDetail();
					//EnableControls(true);
					ScannedPkgs();
				}

				lbShipping.Items.Clear();   
				GetShippingDetail();

				lblValISector.Text = "";
				if (txtBatchNo.Text =="" && ddlTruckID.SelectedItem.Text =="" && ddlBatchID.SelectedItem.Text =="" && txtBatchDate.Text == "" && ddlBatchType.SelectedItem.Text =="")
					lblValISector.Text ="Please enter selection criteria."; 
				else if (txtBatchNo.Text !="" && (ddlTruckID.SelectedItem.Text !="" || ddlBatchID.SelectedItem.Text !="" || txtBatchDate.Text != "" || ddlBatchType.SelectedItem.Text !=""))
					lblValISector.Text ="Please enter either Batch Number or the alternative selection criteria."; 
				else
				{
					lblValISector.Text = "";
					GetBatchList();
				}

				rdbIgnore.Enabled =false;
				rdbOrigin.Enabled =false;
				rdbTermination.Enabled =false;
 
				rdbOpenBatch.Enabled =false;
				rdbCloseBatch.Enabled =false;
				rdbIgnorebatch.Enabled =false; 

				ddlDriverID.Enabled = false; 
				btnDriverSearch.Enabled =false;
				/// --------------


				if(iBatchSelected>-1)
					this.lbBatch.SelectedIndex = iBatchSelected;
			}
			catch(Exception ex)
			{
				this.lblErrrMsg.Text = "Please wait up to 5 minutes before process this batch.";
			}

		}

		private void UpdateDT()
		{
			int i = BatchManifest.ModifyUpdateDT( m_strAppID,m_strEnterpriseID, (string)ViewState["batch_no"]);
		}
		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

		}
		private void btnOnDeparture_Click(object sender, System.EventArgs e)
		{	
			//OpenWindowpage("PopUp_Arrival_Departure.aspx?FORMID=BatchManifest_ControlPanel&AppID="+m_strAppID+"&EnterpriseID="+m_strEnterpriseID+"&btnname=Departure&UserID="+userID+"&Location="+ddlCurrentLocation.SelectedItem.Text+"&BatchNo="+(string)ViewState["batch_no"]+"  ");
			//ViewState["batch_no"]=null;
			if (ViewState["batch_no"] ==null || (string)ViewState["batch_no"] =="")
			{
				string script="<script langauge='javacript'>";
				script+= "alert('Please select batch no again.')";
				script+= "</script>";
				Page.RegisterStartupScript("setFocus",script);

			}
			else
			{
				ArrayList paramList = new ArrayList();
				paramList.Add("PopUp_Arrival_Departure.aspx?FORMID=BatchManifest_ControlPanel&AppID="+m_strAppID+"&EnterpriseID="+m_strEnterpriseID+"&btnname=Departure&UserID="+userID+"&Location="+ddlCurrentLocation.SelectedItem.Text+"&BatchNo="+(string)ViewState["batch_no"]+"  ");
				String sScript = Utility.GetScriptPopupSetSize("OpenSetSize.js",paramList,400,900);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}

		}

		private void btnOnArrival_Click(object sender, System.EventArgs e)
		{
			//OpenWindowpage("PopUp_Arrival_Departure.aspx?FORMID=BatchManifest_ControlPanel&AppID="+m_strAppID+"&EnterpriseID="+m_strEnterpriseID+"&btnname=Arrival&UserID="+userID+"&Location="+ddlCurrentLocation.SelectedItem.Text+"&BatchNo="+(string)ViewState["batch_no"]+"&RecIsCom="+(string)ViewState["RecIsCom"] +"  ");
			if(ViewState["batch_type_"]!=null)
			{
				if(ViewState["batch_type_"].ToString().Trim()=="S")
				{
					DataSet desOrigin =BatchManifest.GetDestinationAllType(m_strAppID,m_strEnterpriseID,(string)ViewState["batch_id"]);
					string strOriginDC="";
					if (desOrigin.Tables[0].Rows.Count > 0 )
					{
						strOriginDC = desOrigin.Tables[0].Rows[0]["origin_station"].ToString(); 
					}
					if(strOriginDC.Trim()!=ddlCurrentLocation.SelectedValue.ToString().Trim())
					{
						lblErrrMsg.Text="Delivery batch may not arrive at a different location than departure.";
						return;
					}
				}
			}

			if (ViewState["batch_no"] ==null || (string)ViewState["batch_no"] =="")
			{
				string script="<script langauge='javacript'>";
				script+= "alert('Please select batch no again.')";
				script+= "</script>";
				Page.RegisterStartupScript("setFocus",script);

			}
			else
			{
				ArrayList paramList = new ArrayList();
				paramList.Add("PopUp_Arrival_Departure.aspx?FORMID=BatchManifest_ControlPanel&AppID="+m_strAppID+"&EnterpriseID="+m_strEnterpriseID+"&btnname=Arrival&UserID="+userID+"&Location="+ddlCurrentLocation.SelectedItem.Text+"&BatchNo="+(string)ViewState["batch_no"]+"&RecIsCom="+(string)ViewState["RecIsCom"] +"  ");
				String sScript = Utility.GetScriptPopupSetSize("OpenSetSize.js",paramList,400,900);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}

		}

		private void btnViewVariances_Click(object sender, System.EventArgs e)
		{
			if (ViewState["batch_no"] ==null || (string)ViewState["batch_no"] =="")
			{
				string script="<script langauge='javacript'>";
				script+= "alert('Please select batch no again.')";
				script+= "</script>";
				Page.RegisterStartupScript("setFocus",script);

			}
			else
			{
				OpenWindowpage("PopUp_ViewVariances.aspx?FORMID=BatchManifest_ControlPanel&AppID="+m_strAppID+"&EnterpriseID="+m_strEnterpriseID+"&UserID="+userID+"&Location="+ddlCurrentLocation.SelectedItem.Text+"&BatchNo="+(string)ViewState["batch_no"]+"  ");
			}
		}

		private void ScannedPkgs()
		{
			DataSet dsbatch =  (DataSet)Session["con"];
			DataSet con = BatchManifest.GetDataArrivalDeparture( m_strAppID,m_strEnterpriseID,m_strCulture,(string)ViewState["batch_no"]);
			string batchno="";
			string truckid ="";
			string batchid ="";
			string batchdate ="";
			string batchtypename ="";
			if (dsbatch.Tables[0].Rows.Count  > 0  )
			{
				batchno = dsbatch.Tables[0].Rows[0]["batch_no"].ToString();
				truckid = dsbatch.Tables[0].Rows[0]["truck_id"].ToString();
				batchid = dsbatch.Tables[0].Rows[0]["batch_id"].ToString();
				batchdate = String.Format("{0:dd/MM/yyyy}",dsbatch.Tables[0].Rows[0]["batch_date"]);
			}
			else
			{
				batchno ="";
				truckid ="";
				batchid ="";
				batchdate ="";
			}
			if (con.Tables[0].Rows.Count > 0)
			{
				batchtypename = con.Tables[0].Rows[0]["code_text"].ToString();
			}
			txtmsg.Text ="Manifest for batch:" + batchno +" Truck ID "+ truckid +" "+ batchid +" "+ batchdate + " "+ batchtypename +" Will be updated in the background. Is this correct?";
			string m ="Manifest for batch:" + batchno +" Truck ID "+ truckid +" "+ batchid +" "+ batchdate + " "+ batchtypename +" Will be updated in the background. Is this correct?";;
			string loc = ddlCurrentLocation.SelectedValue;//(string)ViewState["loc"];
			
			DataSet conL = BatchManifest.GetBatchDetailForBatchType_L_OnArrival(m_strAppID,m_strEnterpriseID,(string)ViewState["batch_no"]);
			if (conL.Tables[0].Rows.Count >0)
			{
				ViewState["arr_dc_L"] = conL.Tables[0].Rows[0]["arr_dc"].ToString();
			}
			//			if ((string)Session["batch_type"] !="L")
			txtmsgRe.Text ="Receiving for batch:" + batchno +" Truck ID: " + truckid + " " + batchid + " " + batchdate + " " + batchtypename +" at " +  ddlCurrentLocation.SelectedItem.Text +" is complete. Is this correct?";
			//			else
			//				txtmsgRe.Text ="Receiving for batch:" + batchno +" Truck ID: " + truckid + " " + batchid + " " + batchdate + " " + batchtypename +" at " + (string)ViewState["arr_dc_L"] +" is complete. Is this correct?";
			if(ViewState["batch_type_"].ToString().Trim()=="W" || ViewState["batch_type_"].ToString().Trim()=="S")
			{
				DataSet desOrigin =BatchManifest.GetDestinationAllType(m_strAppID,m_strEnterpriseID,(string)ViewState["batch_id"]);
				string strOriginDC="";
				if (desOrigin.Tables[0].Rows.Count > 0 )
				{
					strOriginDC = desOrigin.Tables[0].Rows[0]["origin_station"].ToString(); 
				}
				if(strOriginDC.Trim()!=ddlCurrentLocation.SelectedValue.ToString().Trim())
				{
					btnManifestScannedPkgs.Attributes.Add("Onclick", "javascript:alert('Operation not allowed for this location.');");
					return;
				}
			}
			btnManifestScannedPkgs.Attributes.Add("Onclick", "javascript:return confirm(document.getElementById('txtmsg').value)");
		}
		private void btnManifestScannedPkgs_Click(object sender, System.EventArgs e)
		{
			if(ViewState["origin_station"]!=null)
			{
				if(ViewState["batch_type_"].ToString().Trim()=="A" || ViewState["batch_type_"].ToString().Trim()=="L"||ViewState["batch_type_"].ToString().Trim()=="S")
				{
					if(ViewState["origin_station"].ToString().Trim()!=ddlCurrentLocation.SelectedValue.ToString().Trim())
					{
						if(ViewState["Arr_DT"]!=null)
						{
							if(ViewState["Arr_DT"].ToString()=="")
							{
								String sScript = "";
								sScript += "<script language=javascript> ";
								sScript += "  alert('Manifest Scanned Pkgs not allowed because Arrival record has not been recorded.')";
								sScript += "</script>";
								Page.RegisterStartupScript("alertScan",sScript);
								return;
							}
						}
					}
				}
			}

			if(ViewState["statusreport"]!=null)
			{
				if(ViewState["statusreport"].ToString().Trim().Equals("2"))
				{
					String sScript = "";
					sScript += "<script language=javascript> ";
					sScript += "  confirm_manifest2();";
					sScript += "</script>";
					Page.RegisterStartupScript("confirmmanifest2",sScript);
					return;
				}
				if(ViewState["statusreport"].ToString().Trim().Equals("1"))
				{
					String sScript = "";
					sScript += "<script language=javascript> ";
					sScript += "  confirm_manifest1();";
					sScript += "</script>";
					Page.RegisterStartupScript("confirmmanifest1",sScript);
					return;
				}
			}

			//			if(ViewState["ConfirmScannedPkgs"]!=null)
			//			{
			//				if(ViewState["ConfirmScannedPkgs"].ToString().Trim().Equals("Y"))
			//				{
			//					String sScript = "";
			//					sScript += "<script language=javascript> ";
			//					sScript += "  ConfirmScannedPkgs();";
			//					sScript += "</script>";
			//					Page.RegisterStartupScript("ConfirmScannedPkgs",sScript);
			//					ViewState["ConfirmScannedPkgs"]=null;
			//					return;
			//				}
			//			}
			UpdateScannedPkgs();
		}

		private void UpdateScannedPkgs()
		{
			int result = BatchManifest.UpdateScannedPkgs(m_strAppID,m_strEnterpriseID,userID.ToUpper(),(string)ddlCurrentLocation.SelectedValue, (string)ViewState["batch_no"]); 
			DataSet ds = SWB_Emulator.SWB_Batch_Manifest_Update(m_strAppID,m_strEnterpriseID,(string)ViewState["batch_no"],userID.ToUpper(),"2");
			txterrmsg.Text  ="Batch number :" + (string)ViewState["batch_no"] +" is already being updated. Operation Cancelled.";
			if (result == 1 && getRole("OPSSR")!="OPSSR")
			{
				String sScript = "";
				sScript += "<script language=javascript> ";
				sScript += "  alert('Your Manifest will be created as a background process. You will receive it in email shortly.')";
				sScript += "</script>";
				Response.Write(sScript);
				Page.RegisterClientScriptBlock("closeWindow",closeWindow);

			}
			else
			{
				String sScript = "";
				sScript += "<script language=javascript> ";
				sScript += "  alert(document.getElementById('txterrmsg').value)";
				sScript += "</script>";
				Response.Write(sScript);
				Page.RegisterClientScriptBlock("closeWindow",closeWindow);
			}
		}

		private void btnForwardManifest_Click(object sender, System.EventArgs e)
		{
			//OpenWindowpage("PopUp_ForwardManifest.aspx?BatchNo="+(string)ViewState["batch_no"]+"");
			if (ViewState["batch_no"] ==null || (string)ViewState["batch_no"] =="")
			{
				string script="<script langauge='javacript'>";
				script+= "alert('Please select batch no again.')";
				script+= "</script>";
				Page.RegisterStartupScript("setFocus",script);

			}
			else
			{
				//Batch_no=" + ViewState["batch_no"].ToString().Trim()+ "&Batch_type=" + strbatchType + "&Location=" + ddlCurrentLocation.SelectedValue.ToString().Trim() + "&Doc_Type=pdf
				String strbatchType =ViewState["batch_type_"].ToString();
				ArrayList paramList = new ArrayList();
				paramList.Add("PopUp_ForwardManifest.aspx?BatchNo="+(string)ViewState["batch_no"]+"&Batch_type=" + strbatchType + "&Location=" + ddlCurrentLocation.SelectedValue.ToString().Trim() + "&Doc_Type=pdf");
				String sScript = Utility.GetScriptPopupSetSize("OpenSetSize.js",paramList,250,600);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}

		}

		private void btnReceivingIsComplete_Click(object sender, System.EventArgs e)
		{
			//int result = BatchManifest.UpdateReComplete(m_strAppID,m_strEnterpriseID, (string)ViewState["batch_no"]); 
			DataSet con = BatchManifest.GetCountConsignment(m_strAppID,m_strEnterpriseID,"SIP",(string)ViewState["batch_no"],ddlCurrentLocation.SelectedItem.Text);
			string c_con ;
			if (con.Tables[0].Rows.Count > 0 )
			{
				c_con = con.Tables[0].Rows[0]["c_count"].ToString(); 
			}
			else
			{
				c_con = "0";
			}
			DataSet conArr = BatchManifest.GetArrival_Datetime(m_strAppID,m_strEnterpriseID,(string)ViewState["batch_no"],ddlCurrentLocation.SelectedItem.Text );
			DataSet conDCArr = BatchManifest.GetArrival_DatetimeDepArr(m_strAppID,m_strEnterpriseID,(string)ViewState["batch_no"],ddlCurrentLocation.SelectedItem.Text );

			string tardt="";
			string tardcdt ="";
			if (conArr.Tables[0].Rows.Count > 0)
			{
				tardt = String.Format("{0:dd/MM/yyyy HH:mm}",conArr.Tables[0].Rows[0]["tracking_datetime"]);
			}
			else
			{
				tardt = null;
			}

			if (conDCArr.Tables[0].Rows.Count > 0)
			{
				tardcdt = String.Format("{0:dd/MM/yyyy HH:mm}",conDCArr.Tables[0].Rows[0]["arr_dt"]);
			}
			else
			{
				tardcdt = null;
			}
				
				
			if (tardt!=null)
			{
		
				if (Convert.ToDateTime(ConDatetime(tardcdt)) > Convert.ToDateTime(ConDatetime(tardt)))
				{
					lblValISector.Text ="Arrival date/time cannot be later than " + tardt +" (time of first SIP scan to this batch). Please correct the arrival date/time before processing Receiving is Complete." ;
				}
				else
				{
					int result = BatchManifest.InsertReceivingComplete( m_strAppID,m_strEnterpriseID,(string)ViewState["batch_no"],"N","N",(string)ViewState["truck_id"],(string)ViewState["batch_id"],"N",userID,ddlCurrentLocation.SelectedItem.Text  ,ViewState["batch_type_"].ToString() );
					if (result==1)
					{
						lblValISector.Text = "Status code applied to "+ c_con +" consignment(s). Please wait 5 - 10 minutes to view shipment tracking history.";
					}
				}
			}
			else
			{
				int result = BatchManifest.InsertReceivingComplete( m_strAppID,m_strEnterpriseID,(string)ViewState["batch_no"],"N","N",(string)ViewState["truck_id"],(string)ViewState["batch_id"],"N",userID,ddlCurrentLocation.SelectedItem.Text  ,ViewState["batch_type_"].ToString() );
				if (result==1)
				{
					lblValISector.Text = "Status code applied to "+ c_con +" consignment(s). Please wait 5 - 10 minutes to view shipment tracking history.";
				}
			}
			

		}
		private string ConDatetime(string date_time)
		{
			string result ="";
			if (date_time !="")
			{
				string d = date_time.Substring(0,2);
				string M = date_time.Substring(3,2);
				string y = date_time.Substring(6,4);
				string H = date_time.Substring(11,2);
				string m = date_time.Substring(14,2);
				string t = date_time.Substring(11,5);
				string d_new = M+"/"+d+"/"+y+" "+t+":"+"0";
				result = d_new;
			}
			return result;
		}
		private void txtBatchNo_TextChanged(object sender, System.EventArgs e)
		{
			btnExcuteQuery_Click(this,null); 
		}

		private void btnShowReport_Click(object sender, System.EventArgs e)
		{
            if (ViewState["statusreport"] != null)
            {
                if (ViewState["statusreport"].ToString().Trim().Equals("0") || ViewState["statusreport"].ToString().Trim().Equals("1") || ViewState["statusreport"].ToString().Trim().Equals("2"))
                {
                    String sScript = "";
                    sScript += "<script language=javascript> ";
                    sScript += "  confirm_report();";
                    sScript += "</script>";
                    Page.RegisterStartupScript("confirmreport", sScript);
                    return;
                }
            }
            showreport();
		}

		private void btnTruckIDSrch_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("Vehicles.aspx?FORMID=BatchManifest_ControlPanel");

		}

		private void showreport_for_image()
		{
			if(ViewState["batch_no"]!= null)
			{
                if (ViewState["batch_type_"] != null)
                {
                    if (lbShipping.Items.Count == 0 && ViewState["batch_type_"].ToString() != "W")
                    {
                        lblValISector.Text = "No data available for the selected batch";
                        return;
                    }
                    String strbatchType = ViewState["batch_type_"].ToString();
                    //	Session["FORMID"]="BatchManifestControlPanel_Delivery"; 
                    //	String strUrl = null;
                    //	strUrl = "../ReportViewer.aspx?Batch_no=" + ViewState["batch_no"].ToString().Trim()+ "&Batch_type=" + strbatchType + "&Location=" + ddlCurrentLocation.SelectedValue.ToString().Trim() + "&Doc_Type=pdf" ;
                    //	ArrayList paramList = new ArrayList();
                    //	paramList.Add(strUrl);
                    //	String sScript = Utility.GetScript("openParentWindow.js",paramList);
                    //	Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
                    //}
                    //
                    string space = System.Web.HttpUtility.HtmlDecode("&nbsp;");

                    string ndata = lbBatch.SelectedValue.Replace(space, ",");
                    string[] data = ndata.Split(',');
                    string batch_no = data[0];
                    //
                    string sBatch_No = batch_no;
                    //string sTruck_ID = ddlTruckID.SelectedValue;
                    //string sBatch_ID = ddlBatchID.SelectedValue;
                    //string sBatch_Date = txtBatchDate.Text;
                    //string sBatch_Type = ddlBatchType.SelectedValue;
                    //string sCurrLoc = ddlCurrentLocation.SelectedItem.Text;
                    //string sDriver_ID = ddlDriverID.SelectedValue;
                    m_dsQuery = BatchManifest.GetManifestDataToReport(utility.GetAppID(), utility.GetEnterpriseID(), sBatch_No);//, sTruck_ID, sBatch_ID, sBatch_Date, sBatch_Type, sCurrLoc, sDriver_ID
                    String strUrl = null;
                    //strUrl = "ReportViewer1.aspx";
                    strUrl = "../ReportViewer1.aspx?Batch_no=" + ViewState["batch_no"].ToString().Trim() + "&Batch_type=" + strbatchType + "&Location=" + ddlCurrentLocation.SelectedValue.ToString().Trim() + "&Doc_Type=pdf";
                    //strUrl = "../ReportViewer.aspx?Batch_no=" + ViewState["batch_no"].ToString().Trim() + "&Batch_type=" + strbatchType + "&Location=" + ddlCurrentLocation.SelectedValue.ToString().Trim() + "&Doc_Type=pdf";
                    Session["REPORT_TEMPLATE"] = "DeleiveryManifestReport_MF";
                    Session["FORMID"] = "Deliver_Manifest";//add to reportviewer1
                    Session["SESSION_MF_Deli"] = m_dsQuery;
                    Session["FormatType"] = ".pdf";
                    ArrayList paramList = new ArrayList();
                    paramList.Add(strUrl);
                    String sScript = Utility.GetScript("openParentWindowReport.js", paramList);
                    Utility.RegisterScriptString(sScript, "ShowPopupScript", this.Page);
                }

            }
		}

        private void showreport()
        {
            if (ViewState["batch_no"] != null)
            {
                if (ViewState["batch_type_"] != null)
                {
                    if (lbShipping.Items.Count == 0 && ViewState["batch_type_"].ToString() != "W")
                    {
                        lblValISector.Text = "No data available for the selected batch";
                        return;
                    }
                    String strbatchType = ViewState["batch_type_"].ToString();
                    Session["FORMID"] = "BatchManifestControlPanel_Delivery";
                    String strUrl = null;
                    strUrl = "../ReportViewer.aspx?Batch_no=" + ViewState["batch_no"].ToString().Trim() + "&Batch_type=" + strbatchType + "&Location=" + ddlCurrentLocation.SelectedValue.ToString().Trim() + "&Doc_Type=pdf";
                    ArrayList paramList = new ArrayList();
                    paramList.Add(strUrl);
                    String sScript = Utility.GetScript("openParentWindow.js", paramList);
                    Utility.RegisterScriptString(sScript, "ShowPopupScript", this.Page);
                }

            }
        }

        private void btnHidReport_Click(object sender, System.EventArgs e)
		{
			showreport();
		}

		private void rdbOrigin_CheckedChanged(object sender, System.EventArgs e)
		{
			rdbIgnore.Enabled  =false;
			rdbTermination.Enabled  =false; 
			rdbBoth.Enabled=false;
		}

		private void rdbTermination_CheckedChanged(object sender, System.EventArgs e)
		{
			rdbIgnore.Enabled  =false;
			rdbOrigin.Enabled  =false;
			rdbBoth.Enabled=false;
		}

		private void rdbIgnore_CheckedChanged(object sender, System.EventArgs e)
		{
			rdbTermination.Enabled  =false;
			rdbOrigin.Enabled =false; 
			rdbBoth.Enabled=false;
		}

		private void rdbBoth_CheckedChanged_1(object sender, System.EventArgs e)
		{
			rdbIgnore.Enabled  =false;
			rdbTermination.Enabled  =false;
			rdbOrigin.Enabled =false; 
		}

		private void btnmsgm2_Click(object sender, System.EventArgs e)
		{
			//			if(ViewState["ConfirmScannedPkgs"]!=null)
			//			{
			//				if(ViewState["ConfirmScannedPkgs"].ToString().Trim().Equals("Y"))
			//				{
			//					String sScript = "";
			//					sScript += "<script language=javascript> ";
			//					sScript += "  ConfirmScannedPkgs();";
			//					sScript += "</script>";
			//					Page.RegisterStartupScript("ConfirmScannedPkgs",sScript);
			//					ViewState["ConfirmScannedPkgs"]=null;
			//					return;
			//				}
			//			}
			UpdateScannedPkgs();
			//			int result = BatchManifest.UpdateScannedPkgs(m_strAppID,m_strEnterpriseID,userID.ToUpper(),(string)ddlCurrentLocation.SelectedValue, (string)ViewState["batch_no"]); 
			//			txterrmsg.Text  ="Batch number :" + (string)ViewState["batch_no"] +" is already being updated. Operation Cancelled.";
			//			if (result == 1)
			//			{
			//				String sScript = "";
			//				sScript += "<script language=javascript> ";
			//				sScript += "  alert('Your Manifest will be created as a background process. You will receive it in email shortly.')";
			//				sScript += "</script>";
			//				Response.Write(sScript);
			//				DataSet ds = SWB_Emulator.SWB_Batch_Manifest_Update(m_strAppID,m_strEnterpriseID,(string)ViewState["batch_no"],userID.ToUpper(),"2");
			//				Page.RegisterClientScriptBlock("closeWindow",closeWindow);
			//
			//			}
			//			else
			//			{
			//				String sScript = "";
			//				sScript += "<script language=javascript> ";
			//				sScript += "  alert(document.getElementById('txterrmsg').value)";
			//				sScript += "</script>";
			//				Response.Write(sScript);
			//				Page.RegisterClientScriptBlock("closeWindow",closeWindow);
			//			}
			
		}

		private void btnmsgm1_Click(object sender, System.EventArgs e)
		{
			//			if(ViewState["ConfirmScannedPkgs"]!=null)
			//			{
			//				if(ViewState["ConfirmScannedPkgs"].ToString().Trim().Equals("Y"))
			//				{
			//					String sScript = "";
			//					sScript += "<script language=javascript> ";
			//					sScript += "  ConfirmScannedPkgs();";
			//					sScript += "</script>";
			//					Page.RegisterStartupScript("ConfirmScannedPkgs",sScript);
			//					ViewState["ConfirmScannedPkgs"]=null;
			//					return;
			//				}
			//			}
			UpdateScannedPkgs();
			//			int result = BatchManifest.UpdateScannedPkgs(m_strAppID,m_strEnterpriseID,userID.ToUpper(),(string)ddlCurrentLocation.SelectedValue, (string)ViewState["batch_no"]); 
			//			txterrmsg.Text  ="Batch number :" + (string)ViewState["batch_no"] +" is already being updated. Operation Cancelled.";
			//			if (result == 1)
			//			{
			//				String sScript = "";
			//				sScript += "<script language=javascript> ";
			//				sScript += "  alert('Your Manifest will be created as a background process. You will receive it in email shortly.')";
			//				sScript += "</script>";
			//				Response.Write(sScript);
			//				DataSet ds = SWB_Emulator.SWB_Batch_Manifest_Update(m_strAppID,m_strEnterpriseID,(string)ViewState["batch_no"],userID.ToUpper(),"2");
			//				Page.RegisterClientScriptBlock("closeWindow",closeWindow);
			//
			//			}
			//			else
			//			{
			//				String sScript = "";
			//				sScript += "<script language=javascript> ";
			//				sScript += "  alert(document.getElementById('txterrmsg').value)";
			//				sScript += "</script>";
			//				Response.Write(sScript);
			//				Page.RegisterClientScriptBlock("closeWindow",closeWindow);
			//			}
			
		}

		private void btnShowTKLHide_Click(object sender, System.EventArgs e)
		{
			lblValISector.Text =(string)Session["CountTKL"];
		}

		private void btnDriverSearch_Click(object sender, System.EventArgs e)
		{
			OpenWindowpage("Staff.aspx?FORMID=BatchManifest_ControlPanel&BMCPDriver=ddlDriverID");
		}

		private void btnmsgm3_Click(object sender, System.EventArgs e)
		{
			int i = BatchManifest.ModifyDep_DTTONull( m_strAppID,m_strEnterpriseID, (string)ViewState["batch_no"],ddlCurrentLocation.SelectedValue.ToString().Trim());
			if(i>0)
			{
				UpdateScannedPkgs();
			}
			
		}

		private void ddlCurrentLocation_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			btnRefresh_Click(this,null);
			if(ViewState["batch_type_"]!=null)
			{
				if(ViewState["batch_type_"].ToString().Trim()=="W" || ViewState["batch_type_"].ToString().Trim()=="S")
				{
					DataSet desOrigin =BatchManifest.GetDestinationAllType(m_strAppID,m_strEnterpriseID,(string)ViewState["batch_id"]);
					string strOriginDC="";
					if (desOrigin.Tables[0].Rows.Count > 0 )
					{
						strOriginDC = desOrigin.Tables[0].Rows[0]["origin_station"].ToString(); 
					}
					if(strOriginDC.Trim()!=ddlCurrentLocation.SelectedValue.ToString().Trim())
					{
						btnManifestScannedPkgs.Attributes.Add("Onclick", "javascript:alert('Operation not allowed for this location.');");
						return;
					}
				}
				btnManifestScannedPkgs.Attributes.Add("Onclick", "javascript:return confirm(document.getElementById('txtmsg').value)");
				
			}
		}

		private void btnShowReportExcel_Click(object sender, System.EventArgs e)
		{
			if(ViewState["statusreport"]!=null)
			{
				if(ViewState["statusreport"].ToString().Trim().Equals("0") || ViewState["statusreport"].ToString().Trim().Equals("1")||ViewState["statusreport"].ToString().Trim().Equals("2"))
				{
					String sScript = "";
					sScript += "<script language=javascript> ";
					sScript += "  confirm_report();";
					sScript += "</script>";
					Page.RegisterStartupScript("confirmreport",sScript);
					return;
				}
			}
			showreportinExcel();
		}

		private void showreportinExcel()
		{
			if(ViewState["batch_no"]!= null)
			{
				if(ViewState["batch_type_"]!=null) 
				{
					if(lbShipping.Items.Count==0 && ViewState["batch_type_"].ToString()!="W")
					{
						lblValISector.Text="No data available for the selected batch";
						return;
					}
					String strbatchType =ViewState["batch_type_"].ToString();
					Session["FORMID"]="BatchManifestControlPanel_Delivery"; 
					String strUrl = null;
					strUrl = "../ReportViewer.aspx?Batch_no=" + ViewState["batch_no"].ToString().Trim()+ "&Batch_type=" + strbatchType + "&Location=" + ddlCurrentLocation.SelectedValue.ToString().Trim() + "&Doc_Type=xls" ;
					ArrayList paramList = new ArrayList();
					paramList.Add(strUrl);
					String sScript = Utility.GetScript("openParentWindow.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
				
			}
		}




	}
}
