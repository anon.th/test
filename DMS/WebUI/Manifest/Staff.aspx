<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="Staff.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Staff" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Staff</title>
		<LINK href="../css/Styles.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<FONT face="Tahoma">
				<asp:label id="lblMainTitle" style="Z-INDEX: 101; LEFT: 32px; POSITION: absolute; TOP: 8px"
					runat="server" Width="112px" CssClass="mainTitleSize" Height="26px">Staff</asp:label><asp:datagrid id="dgStaff" style="Z-INDEX: 102; LEFT: 32px; POSITION: absolute; TOP: 144px" runat="server"
					AllowPaging="True" AllowCustomPaging="True" AutoGenerateColumns="False" HorizontalAlign="Center">
					<ItemStyle CssClass="popupGridField"></ItemStyle>
					<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
					<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
					<Columns>
						<asp:BoundColumn DataField="emp_id" HeaderText="Driver ID">
							<HeaderStyle Width="100px"></HeaderStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Emp_Name" HeaderText="Name">
							<HeaderStyle Width="150px"></HeaderStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Base_Location" HeaderText="Location">
							<HeaderStyle Width="100px"></HeaderStyle>
						</asp:BoundColumn>
						<asp:ButtonColumn Text="Select" CommandName="Select">
							<HeaderStyle Width="50px"></HeaderStyle>
						</asp:ButtonColumn>
					</Columns>
					<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
				</asp:datagrid></FONT>
			<TABLE id="Table1" style="Z-INDEX: 103; LEFT: 32px; WIDTH: 448px; POSITION: absolute; TOP: 48px; HEIGHT: 56px"
				cellSpacing="1" cellPadding="1" width="448" border="0">
				<TR>
					<TD style="WIDTH: 102px"><asp:label id="lblDriverID" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Driver ID</asp:label></TD>
					<TD style="WIDTH: 160px"><FONT face="Tahoma"><asp:textbox id="txtdriverid" tabIndex="1" runat="server" Width="152px" CssClass="textField"
								MaxLength="100"></asp:textbox></FONT></TD>
					<TD><FONT face="Tahoma"></FONT></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 102px"><asp:label id="lblName" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Name</asp:label></TD>
					<TD style="WIDTH: 160px"><asp:textbox id="txtName" tabIndex="1" runat="server" Width="152px" CssClass="textField" MaxLength="100"></asp:textbox></TD>
					<TD><FONT face="Tahoma"></FONT></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 102px"><asp:label id="lblLocation" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Location</asp:label></TD>
					<TD style="WIDTH: 160px"><FONT face="Tahoma"><asp:dropdownlist id="ddlLocation" runat="server" CssClass="textField"></asp:dropdownlist></FONT></TD>
					<TD><asp:button id="btnSearch" tabIndex="4" runat="server" Width="62px" CssClass="queryButton" Height="22px"
							CausesValidation="False" Text="Search"></asp:button><FONT face="Tahoma">&nbsp;
							<asp:button id="btnClose" tabIndex="5" runat="server" Width="62px" CssClass="queryButton" Height="22px"
								CausesValidation="False" Text="Close"></asp:button></FONT></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
