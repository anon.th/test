using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using System.Text;
using com.common.applicationpages;
using com.ties.DAL;
using com.common.RBAC;

namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for PopUp_CreateUpdateBatch_Show.
	/// </summary>
	public class PopUp_WarningConsignment : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label1;
		String m_strAppID=null;
		String m_strEnterpriseID=null;
		String m_strUserID=null;
		String m_strCulture=null;
		protected System.Web.UI.WebControls.Button btnOk;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected com.common.util.msTextBox txtPackage;
		protected System.Web.UI.WebControls.TextBox txtHid;
		protected System.Web.UI.WebControls.Button btnConfirm;
		protected System.Web.UI.WebControls.DropDownList ddlStatus;

		private void Page_Load(object sender, System.EventArgs e)
		{
			getSessionTimeOut(true);
						// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strUserID=utility.GetUserID();
			m_strCulture="en-US";
			if(!Page.IsPostBack)
			{
				Response.CacheControl="no-cache";
//				string script="<script langauge='javacript'>";
//				script+= "document.all['"+ txtPackage.ClientID +"'].focus();";
//				script+= "</script>";
//				Page.RegisterStartupScript("setFocus",script);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += " window.close();";
			sScript += "</script>";
			Page.Response.Write(sScript);
		
		}
		private void CloseWindowDDlNo()
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += " strA='1';window.close();";
			sScript += "</script>";
			Page.RegisterStartupScript("O",sScript);
		}
		private void CloseWindowDDlYes()
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += " strA='2';window.close();";
			sScript += "</script>";
			Page.RegisterStartupScript("O",sScript);
		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			string strconno =Request.Params["consingment_no"];
			string strstatus = Request.Params["status"];
			if(txtPackage.Text=="")
			{
				string script="<script langauge='javacript'>";
				script+= "alert('Please enter total packages.');";
				script+= "</script>";
				Page.RegisterStartupScript("warning",script);
				return;
			}

			

			if (ddlStatus.SelectedValue.ToString()!="0")
			{
				if(Session["dtTemp"]!=null)
				{
					DataTable dt = (DataTable)Session["dtTemp"];
					if(Convert.ToInt32(txtPackage.Text.Trim())>0)
					{
						DataSet ds = BatchManifest.getManifestStatus(m_strAppID,m_strEnterpriseID,"Manifest_Max_Create_Pkg",m_strCulture);
						if(ds.Tables[0].Rows.Count>0)
						{
							string strLimitPacakge =ds.Tables[0].Rows[0]["code_str_value"].ToString();
							if(Convert.ToInt32(txtPackage.Text.Trim())>Convert.ToInt32(strLimitPacakge))
							{
								string script="<script langauge='javacript'>";
								script+= "ConfirmGen('"+strLimitPacakge+"','"+txtPackage.Text.Trim()+"');";
								script+= "</script>";
								Page.RegisterStartupScript("warning",script);
								return;
							}
						}

						com.common.classes.User user =  RBACManager.GetUser(m_strAppID,m_strEnterpriseID,m_strUserID);
						int result = SWB_Emulator.SWB_SIP_Insert(m_strAppID,m_strEnterpriseID,strconno,"XXXXX",
							txtPackage.Text.Trim(),"XXX",user.UserLocation,m_strUserID,"21");

						String CnsgBkgNo = dt.Rows[dt.Rows.Count-1]["CnsgBkgNo"].ToString();
						DateTime tracking_datetime = (DateTime)dt.Rows[dt.Rows.Count-1]["tracking_datetime"];
						String person_incharge = dt.Rows[dt.Rows.Count-1]["person_incharge"].ToString();
						String remarks = dt.Rows[dt.Rows.Count-1]["remarks"].ToString();
						dt.Rows[dt.Rows.Count-1]["pkg_no"]="1";
						for(int i=2;i<= Convert.ToInt32(txtPackage.Text.Trim());i++ )
						{
							DataRow drNew = dt.NewRow();
							drNew["CnsgBkgNo"]=CnsgBkgNo;
							drNew["tracking_datetime"]=tracking_datetime;
							drNew["person_incharge"]=person_incharge;
							drNew["remarks"]=remarks;
							drNew["pkg_no"]=i.ToString();
							dt.Rows.Add(drNew);
						}
						dt.AcceptChanges();
						Session["dtTemp"]=dt;
						CloseWindowDDlYes();
					}
				}
				//				BatchManifest.InsertToStategingNoexist(m_strAppID,m_strEnterpriseID,strconno,strstatus,m_strUserID);
			}
			else
			{
				com.common.classes.User user =  RBACManager.GetUser(m_strAppID,m_strEnterpriseID,m_strUserID);
				int result = SWB_Emulator.SWB_SIP_Insert(m_strAppID,m_strEnterpriseID,strconno,"XXXXX",
					txtPackage.Text.Trim(),"XXX",user.UserLocation,m_strUserID,"21");

				CloseWindowDDlNo();
			}

		}

		private void btnConfirm_Click(object sender, System.EventArgs e)
		{
			if(Session["dtTemp"]!=null)
			{
				DataTable dt = (DataTable)Session["dtTemp"];
				string strconno =Request.Params["consingment_no"];

				com.common.classes.User user =  RBACManager.GetUser(m_strAppID,m_strEnterpriseID,m_strUserID);
				int result = SWB_Emulator.SWB_SIP_Insert(m_strAppID,m_strEnterpriseID,strconno,"XXXXX",
					txtPackage.Text.Trim(),"XXX",user.UserLocation,m_strUserID,"21");

				String CnsgBkgNo = dt.Rows[dt.Rows.Count-1]["CnsgBkgNo"].ToString();
				DateTime tracking_datetime = (DateTime)dt.Rows[dt.Rows.Count-1]["tracking_datetime"];
				String person_incharge = dt.Rows[dt.Rows.Count-1]["person_incharge"].ToString();
				String remarks = dt.Rows[dt.Rows.Count-1]["remarks"].ToString();
				dt.Rows[dt.Rows.Count-1]["pkg_no"]="1";
				DataSet ds = BatchManifest.getManifestStatus(m_strAppID,m_strEnterpriseID,"Manifest_Max_Create_Pkg",m_strCulture);
				string strLimitPacakge="";
				if(ds.Tables[0].Rows.Count>0)
				{
					strLimitPacakge =ds.Tables[0].Rows[0]["code_str_value"].ToString();
				}
				for(int i=2;i<= Convert.ToInt32(strLimitPacakge);i++ )
				{
					DataRow drNew = dt.NewRow();
					drNew["CnsgBkgNo"]=CnsgBkgNo;
					drNew["tracking_datetime"]=tracking_datetime;
					drNew["person_incharge"]=person_incharge;
					drNew["remarks"]=remarks;
					drNew["pkg_no"]=i.ToString();
					dt.Rows.Add(drNew);
				}
				dt.AcceptChanges();
				Session["dtTemp"]=dt;
				CloseWindowDDlYes();
			}
		}
	}
}
