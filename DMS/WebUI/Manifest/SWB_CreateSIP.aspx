<%@ Page Language="c#" CodeBehind="SWB_CreateSIP.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.Manifest.SWB_CreateSIP" %>

<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Scanning Workbench Emulator</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="../css/Styles.css">
    <!--#INCLUDE FILE="../msFormValidations.inc"-->
    <script type="text/javascript" src="../Scripts/JScript_TextControl.js"></script>
    <script type="text/javascript" src="../Scripts/settingScrollPosition.js"></script>
    <script type="text/javascript">

        function SetScrollPosition_page() {
            window.scroll(0, document.forms[0].ScrollPosition.value);
            var sHostname = window.location.hostname;
            var sPort = window.location.port;

            document.getElementById('hidHost').value = sHostname;
            document.getElementById('hidPort').value = sPort;
            //alert(document.getElementById('hidHost').value);

        }

        function setValue() {
            var s = document.all['inFile'];
            document.all['txtFilePath'].innerText = s.value;
            if (s.value != '')
                document.all['btnImport'].disabled = false;
            else
                document.all['btnImport'].disabled = true;
        }

        function upBrowse() {
            document.all['divBrowse'].style.backgroundImage = 'url(../images/btn-browse-up.GIF)';
        }

        function downBrowse() {
            document.all['divBrowse'].style.backgroundImage = 'url(../images/btn-browse-down.GIF)';
        }

        function clearValue() {
            if (document.all['txtFilePath'].innerText == '') {
                document.location.reload();
            }
        }

        function IsNumeric(sText) {
            var ValidChars = "0123456789.";
            var IsNumber = true;
            var Char;


            for (i = 0; i < sText.length && IsNumber == true; i++) {
                Char = sText.charAt(i);
                if (ValidChars.indexOf(Char) == -1) {
                    IsNumber = false;
                }
            }
            return IsNumber;

        }

        function TxtConNoFocus() {
            document.getElementById("txtConNo").value = '';
            document.getElementById("txtConNo").focus();
        }

        function KeyDownHandler() {
            //debugger;
            // process only the Enter key
            var key;
            if (window.event) key = window.event.keyCode;     //IE
            else key = e.which;     //firefox
            if (key == 13) {
                // cancel the default submit
                //event.returnValue=false;
                //event.cancel = true;
                // submit the form by programmatically clicking the specified button
                if (document.getElementById("btnInsert").disabled == false) {
                    //btn = "btnInsert";

                    document.getElementById("btnInsert_Print").click();
                    document.getElementById("txtConNo").focus();
                }
                else if (document.getElementById("btnUpdate").disabled == false) {
                    //btn = "btnUpdate";
                    document.getElementById("btnUpdate").click();
                    document.getElementById("txtConNo").focus();
                }

            }
        }

        function suppressNonEng(e, obj) {//debugger;
            var key;
            if (window.event) key = window.event.keyCode;     //IE
            else key = e.which;     //firefox

            if (key > 128) return false;
            else return UpperMask(obj);//return true;*/
        }

        function suppressOnBluer(e) {//alert("OnBlur");
            //e.value = Check2FontChar2LastChar(e);

            var s = e.value;
            e.value = '';
            for (i = 0; i < s.length; i++) {
                if (s.charCodeAt(i) <= 128) {
                    e.value += s.charAt(i);
                }
            }

            if (s.length != e.value.length) {
                //alert('Other than english character are not supported')
                return false;
            }//debugger;
            //var hddConNo = document.getElementById("hddConNo");

            __doPostBack('hddConNo', '');
            return true;
        }

        function WebForm_TextBoxKeyHandler(e){}
    </script>
</head>
<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
    onload="SetScrollPosition_page();" ms_positioning="GridLayout">
    <form id="SWB_CreateSIP" method="post" runat="server">
        <input style="display: none" id="hddConNo" type="button" name="hddCon" runat="server">
        <p><font face="Tahoma"></font>&nbsp;</p>
        <p>
            <asp:Label Style="z-index: 101; position: absolute; top: 23px; left: 14px" ID="Header1" runat="server"
                CssClass="mainTitleSize" Width="490">Scanning Workbench Emulator - Create SIP</asp:Label>
        </p>
        <p></p>
        <asp:Panel ID="pnlImport" runat="server">
            <fieldset style="z-index: 105; position: absolute; width: 953px; height: 48px; top: 88px; left: 16px"
                id="fsImport" runat="server">
                <legend>
                    <asp:Label ID="lblImportSIPs" runat="server" Width="80px" CssClass="tableHeadingFieldset" Height="16px">Import 
								SIPs</asp:Label></legend>
                <table style="width: 896px; height: 49px" id="Table10">
                    <tr>
                        <td>&nbsp;</td>
                        <td valign="bottom">
                            <div style="background-image: url(../images/btn-browse-up.GIF); width: 70px; display: inline; height: 20px"
                                id="divBrowse" onmouseup="upBrowse();" onmousedown="downBrowse();">
                                <input onblur="setValue();" style="filter: alpha(opacity: 0); border-left: #88a0c8 1px outset; background-color: #e9edf0; width: 20px; font-family: Arial; color: #003068; font-size: 11px; border-top: #88a0c8 1px outset; border-right: #88a0c8 1px outset; text-decoration: none"
                                    id="inFile" onfocus="setValue();" type="file" name="inFile" runat="server">
                            </div>
                        </td>
                        <td valign="bottom">&nbsp;
								<asp:TextBox ID="txtFilePath" runat="server" Width="658px" CssClass="textField"></asp:TextBox></td>
                        <td valign="bottom">&nbsp;
								<asp:Button ID="btnImport" runat="server" Width="104px" CssClass="queryButton" Text="Import"></asp:Button></td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                </table>
            </fieldset>
        </asp:Panel>
        <p></p>
        <asp:Label Style="z-index: 102; position: absolute; top: 56px; left: 16px" ID="lblErrrMsg"
            runat="server" CssClass="errorMsgColor" Width="720" Height="2"></asp:Label>
        <input style="z-index: 103; position: absolute; top: 432px; left: 760px" type="hidden"
            name="ScrollPosition">
        &nbsp;&nbsp;
			<asp:ValidationSummary Style="z-index: 104; position: absolute; top: 16px; left: 552px" ID="reqSummary"
                runat="server" ShowSummary="False" HeaderText="Please enter the missing  fields." ShowMessageBox="True"
                DisplayMode="BulletList"></asp:ValidationSummary>
        <fieldset style="z-index: 106; position: absolute; width: 921px; height: 256px; top: 176px; left: 16px"
            id="fsCreateSIP" runat="server">
            <legend>
                <asp:Label ID="lblCreateSIP" runat="server" CssClass="tableHeadingFieldset" Width="68px" Height="18px">Create SIP</asp:Label></legend>
            <table id="Table1" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top">
                        <table style="width: 344px" id="Table8">
                            <tbody class="tableLabel">
                                <tr>
                                    <td style="width: 4px; height: 26px"></td>
                                    <td style="width: 148px; height: 26px">
                                        <asp:Label ID="lblConNo" runat="server" CssClass="tableLabel">Consignment Number:</asp:Label>
                                    </td>
                                    <td style="width: 134px; height: 26px">
                                        <cc1:msTextBox ID="txtConNo" runat="server" CssClass="tableTextbox" Width="172px" AutoPostBack="True" MaxLength="32" Style="z-index: 0" DESIGNTIMEDRAGDROP="150"></cc1:msTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 4px">&nbsp;</td>
                                    <td style="width: 148px" class="tableLabel">
                                        <asp:Label ID="lblDestPostCode" runat="server" CssClass="tableLabel">Destination Postal Code:</asp:Label></td>
                                    <td style="width: 134px">
                                        <cc1:msTextBox ID="txtDestPostCode" runat="server" CssClass="tableTextbox" Width="120px" AutoPostBack="True" MaxLength="10"></cc1:msTextBox><asp:Label ID="lblProvince" runat="server" CssClass="tableLabel"></asp:Label><asp:Label ID="lbltmpDestDC" runat="server" Visible="False"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 4px"></td>
                                    <td style="width: 148px" class="tableLabel">
                                        <asp:Label ID="lblServiceType" runat="server" CssClass="tableLabel">Service Type:</asp:Label></td>
                                    <td style="width: 134px">
                                        <asp:DropDownList ID="ddlServiceType" runat="server" Width="106px"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td style="width: 4px">&nbsp;</td>
                                    <td style="width: 148px" class="tableLabel">
                                        <asp:Label ID="lblNumOfPackages" runat="server" CssClass="tableLabel">Number of Packages:</asp:Label></td>
                                    <td style="width: 134px">
                                        <cc1:msTextBox ID="txtPkgNum" runat="server" CssClass="tableTextbox" Width="66px" NumberMaxValue="9999"
                                            NumberPrecision="3" NumberMinValue="1" TextMaskType="msNumeric"></cc1:msTextBox></td>
                                </tr>
                                <tr>
                                    <td style="width: 4px">&nbsp;</td>
                                    <td class="style1" colspan="2">
                                        <asp:Button ID="btnClear" runat="server" CssClass="queryButton" Width="86px" Text="Clear" CausesValidation="False"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<asp:Button ID="btnInsert" runat="server" CssClass="queryButton" Width="80px" Text="Insert"
                                                Enabled="False"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<asp:Button ID="btnUpdate" runat="server" CssClass="queryButton" Width="94px" Text="Update"
                                                Enabled="False"></asp:Button></td>
                                </tr>
                                <tr>
                                    <td style="width: 4px; height: 22px">&nbsp;</td>
                                    <td class="style1" colspan="2" style="height: 22px">
                                        <asp:Button ID="btnInsert_Print" runat="server" CssClass="queryButton" Width="136px" Text="Insert &amp; Print"
                                            CausesValidation="False" Enabled="False"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
											&nbsp;
											<asp:Button ID="btnPrintSelected" runat="server" CssClass="queryButton" Width="136px" Text="Print Selected"
                                                Enabled="False"></asp:Button></td>
                                </tr>
                                <tr>
                                    <td style="width: 4px">&nbsp;</td>
                                    <td style="width: 148px" class="style2">
                                        <asp:Label ID="lblSipCount" runat="server" CssClass="tableLabel">SIP Count:</asp:Label></td>
                                    <td style="width: 134px">
                                        <asp:Label ID="lblSipCountDisplay" runat="server" CssClass="tableLabel"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 4px">&nbsp;</td>
                                    <td style="width: 148px" class="style2">
                                        <asp:Label ID="lblSubmitDateTime" runat="server" CssClass="tableLabel">Submit Date/Time:</asp:Label></td>
                                    <td style="width: 134px">
                                        <asp:Label ID="lblSubmitDateTimeDisplay" runat="server" CssClass="tableLabel"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 4px"></td>
                                    <td style="width: 148px" class="style2">
                                        <asp:Label ID="lblUser" runat="server" CssClass="tableLabel">User:</asp:Label></td>
                                    <td style="width: 134px">
                                        <asp:Label ID="lblUserDisplay" runat="server" CssClass="tableLabel"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 4px">&nbsp;</td>
                                    <td style="width: 148px" class="style2">
                                        <asp:Label ID="lblLocation" runat="server" CssClass="tableLabel">Location:</asp:Label></td>
                                    <td style="width: 134px">
                                        <asp:Label ID="lblLocationDisplay" runat="server" CssClass="tableLabel"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td style="width: 4px">&nbsp;</td>
                                    <td style="width: 148px" class="style2">&nbsp;</td>
                                    <td style="width: 134px">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="15">&nbsp;</td>
                    <td valign="top">
                        <table style="width: 632px; height: 231px" id="Table2">
                            <tr class="tableLabel">
                                <td style="height: 15px" nowrap>
                                    <asp:Label ID="lblColConsignment" runat="server" CssClass="tableLabel" Width="120px">Consignment</asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:Label ID="lblColService" runat="server" CssClass="tableLabel" Width="56px">Service</asp:Label><asp:Label ID="lblColDestination" runat="server" CssClass="tableLabel" Width="26px">Destination</asp:Label>&nbsp;&nbsp;&nbsp;
										<asp:Label ID="lblColPkgs" runat="server" CssClass="tableLabel">Pkgs</asp:Label>&nbsp;&nbsp;&nbsp;
										<asp:Label ID="lblColSIP" runat="server" CssClass="tableLabel">SIP</asp:Label>&nbsp;<asp:Label ID="lblColDataTime" runat="server" CssClass="tableLabel">Date/Time</asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:Label ID="lblColUser" runat="server" CssClass="tableLabel">User</asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<asp:Label ID="lblColLoc" runat="server" CssClass="tableLabel" Width="4px">Loc.</asp:Label>&nbsp;&nbsp;&nbsp;
										<asp:Label ID="lblColDestDC" runat="server" CssClass="tableLabel">Dest. DC</asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ListBox Style="font-family: 'Courier New', Courier, monospace" ID="lbSIPCon" runat="server"
                                        Width="616px" Height="208px" AutoPostBack="True"></asp:ListBox></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </fieldset>
        <input type="hidden" id="hidHost" runat="server" name="hhost">
        <input type="hidden" id="hidPort" runat="server" name="hport">
    </form>
</body>
</html>
