<%@ Page language="c#" Codebehind="CustomsExpressFreightCollectRates.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.CustomsExpressFreightCollectRates" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="vs_snapToGrid" content="true">
		<meta name="vs_showGrid" content="true">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
			function AfterPasteDateEdit(obj) {
				after(obj);
				var strDate = obj.value;
				obj.value = strDate.toString().trim();
			}
			function RemoveBadPaseNumberwhithDot(strTemp, obj) {
				strTemp = strTemp.replace(/[^.0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			
			function AfterPasteNumberwhithDot(obj) {
				setTimeout(function () {
					RemoveBadPaseNumberwhithDot(obj.value, obj);
				}, 1); 
			}
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<div style="Z-INDEX: 0; POSITION: relative; WIDTH: 1007px; HEIGHT: 1248px" id="divMain"
				MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<TBODY>
						<tr>
							<td colSpan="2" align="left"><asp:label id="lbHeader" runat="server" CssClass="mainTitleSize" Height="27px" Width="421px">Customs Express Freight Collect Rates</asp:label></td>
						</tr>
						<tr>
							<td style="HEIGHT: 26px" vAlign="top" colSpan="2" align="left"><asp:button id="btnQry" runat="server" CssClass="queryButton" CausesValidation="False" Text="Query"></asp:button><label style="WIDTH: 5px"></label><asp:button id="btnExecQry" runat="server" CssClass="queryButton" CausesValidation="False" Text="Execute Query"></asp:button><label style="WIDTH: 5px"></label></td>
						</tr>
						<tr>
							<td style="HEIGHT: 19px" colSpan="6"><asp:label style="Z-INDEX: 0" id="lblError" runat="server" Font-Size="X-Small" Font-Bold="True"
									ForeColor="Red"></asp:label></td>
						</tr>
						<tr>
							<td>
								<fieldset style="WIDTH: 65%"><legend style="COLOR: black">Search Filter
									</legend>
									<table>
										<tr>
											<td><asp:label style="Z-INDEX: 0" id="lbZone" runat="server" CssClass="tableLabel" Width="40px">Zone</asp:label></td>
											<td><asp:dropdownlist style="Z-INDEX: 0" id="ddlZone" tabIndex="1" runat="server" Width="84px" AutoPostBack="True"></asp:dropdownlist></td>
											<td><asp:label style="Z-INDEX: 0" id="lbEffectiveDate" runat="server" CssClass="tableLabel" Width="88px">Effective Date</asp:label></td>
											<td><cc1:mstextbox id="txtEffectiveDate" runat="server" CssClass="textField" Width="82" TextMaskType="msDate"
													MaxLength="10" TextMaskString="99/99/9999" tabIndex="2"></cc1:mstextbox></td>
											<td><asp:label style="Z-INDEX: 0" id="lbWeightFrom" runat="server" CssClass="tableLabel" Height="14px"
													Width="80px">Weight From</asp:label></td>
											<td><cc1:mstextbox style="Z-INDEX: 0" id="txtWeightFrom" tabIndex="3" onpaste="AfterPasteNonNumber(this)"
													runat="server" CssClass="textFieldRightAlign" Width="65px" TextMaskType="msNumericCOD" MaxLength="7"
													NumberPrecision="5" NumberMinValue="0" NumberMaxValueCOD="9999.99" NumberScale="1"></cc1:mstextbox></td>
											<td><asp:label style="Z-INDEX: 0" id="lbWeightTo" runat="server" CssClass="tableLabel" Height="14px"
													Width="64px">Weight To</asp:label></td>
											<td><cc1:mstextbox style="Z-INDEX: 0" id="txtWeightTo" tabIndex="4" onpaste="AfterPasteNonNumber(this)"
													runat="server" CssClass="textFieldRightAlign" Width="65px" TextMaskType="msNumericCOD" MaxLength="7"
													NumberPrecision="5" NumberMinValue="0" NumberMaxValueCOD="9999.99" NumberScale="1"></cc1:mstextbox></td>
										</tr>
									</table>
								</fieldset>
							</td>
						</tr>
						<tr>
							<td>
								<fieldset style="WIDTH: 50%"><legend style="COLOR: black">Copy Rates
									</legend>
									<table>
										<tr>
											<td><asp:label style="Z-INDEX: 0" id="lbCopy" runat="server" CssClass="tableLabel" Width="320px">Use search results to copy rates to new Effective Date</asp:label></td>
											<td><cc1:mstextbox id="txtCopyDate" runat="server" CssClass="textField" Width="82" TextMaskType="msDate"
													MaxLength="10" TextMaskString="99/99/9999" tabIndex="5"></cc1:mstextbox></td>
											<td><asp:button id="btCopy" runat="server" CssClass="queryButton" CausesValidation="False" Text="Copy"
													tabIndex="6"></asp:button></td>
										</tr>
									</table>
								</fieldset>
							</td>
						</tr>
						<tr>
							<td><asp:datagrid style="Z-INDEX: 0" id="gridExpressRate" runat="server" Width="70%" AutoGenerateColumns="False"
									ShowFooter="True" AlternatingItemStyle-CssClass="gridField" ItemStyle-CssClass="gridField"
									HeaderStyle-CssClass="gridHeading" CellPadding="0" CellSpacing="1" BorderWidth="0px" FooterStyle-CssClass="gridHeading"
									AllowPaging="True" PageSize="25">
									<FooterStyle CssClass="gridHeading"></FooterStyle>
									<AlternatingItemStyle CssClass="gridField"></AlternatingItemStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn>
											<HeaderStyle Width="8%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:LinkButton id="btnEditItem" Runat="server" Text="<img src='images/butt-edit.gif' alt='edit' border=0>"
													CommandName="EDIT_ITEM" />
												<asp:LinkButton id="btnDeleteItem" Runat="server" Text="<img src='images/butt-delete.gif' alt='delete' border=0>"
													CommandName="DELETE_ITEM" />
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Center"></FooterStyle>
											<FooterTemplate>
												<asp:LinkButton id="btnAddItem" Runat="server" Text="<img src='images/butt-add.gif' alt='add' border=0>"
													CommandName="ADD_ITEM" tabIndex="13" />
											</FooterTemplate>
											<EditItemTemplate>
												<asp:LinkButton id="btnSaveItem" Runat="server" Text="<img src='images/butt-update.gif' alt='save' border=0>"
													CommandName="SAVE_ITEM" tabIndex="19" />
												<asp:LinkButton id="btnCancelItem" Runat="server" Text="<img src='images/butt-red.gif' alt='cancel' border=0>"
													CommandName="CANCEL_ITEM" />
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Outbound From Zone">
											<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lbZoneItem" Runat="server"></asp:Label>
												<%--# DataBinder.Eval(Container.DataItem, "Zone") --%>
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Center"></FooterStyle>
											<FooterTemplate>
												<asp:DropDownList ID="ddlOutFromZoneAdd" Runat="server" Width="100%" tabIndex="8"></asp:DropDownList>
											</FooterTemplate>
											<EditItemTemplate>
												<asp:DropDownList ID="ddlOutFromZoneEdit" Runat="server" Width="100%" tabIndex="14"></asp:DropDownList>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Effective Date">
											<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lbEffectiveDateItem" Runat="server"></asp:Label>
												<%--# DateTime.Parse(DataBinder.Eval(Container.DataItem, "EffectiveDate").ToString()).ToString("dd/MM/yyyy") --%>
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Center"></FooterStyle>
											<FooterTemplate>
												<cc1:mstextbox id="txtEffectiveDateAdd" runat="server" Width="100%" CssClass="textField" TextMaskType="msDate"
													MaxLength="10" TextMaskString="99/99/9999" tabIndex="9"></cc1:mstextbox>
											</FooterTemplate>
											<EditItemTemplate>
												<cc1:mstextbox id="txtEffectiveDateEdit" runat="server" Width="100%" CssClass="textField" TextMaskType="msDate"
													MaxLength="10" TextMaskString="99/99/9999" tabIndex="15"></cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Max Weight">
											<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center"></ItemStyle>
											<ItemTemplate>
												<asp:Label ID="lbMaxWeight" Runat="server"></asp:Label>
												<%--# DataBinder.Eval(Container.DataItem, "MaxWeight") --%>
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Center"></FooterStyle>
											<FooterTemplate>
												<cc1:mstextbox style="Z-INDEX: 0" id="txtMaxWeightAdd" tabIndex="10" onpaste="AfterPasteNonNumber(this)"
													runat="server" CssClass="textFieldRightAlign" Width="100%" MaxLength="7" TextMaskType="msNumericCOD"
													NumberPrecision="5" NumberMinValue="0" NumberMaxValueCOD="9999.99" NumberScale="1"></cc1:mstextbox>
											</FooterTemplate>
											<EditItemTemplate>
												<cc1:mstextbox style="Z-INDEX: 0" id="txtMaxWeightEdit" onpaste="AfterPasteNonNumber(this)" runat="server"
													CssClass="textFieldRightAlign" Width="100%" MaxLength="7" TextMaskType="msNumericCOD" NumberPrecision="5"
													NumberMinValue="0" NumberMaxValueCOD="9999.99" tabIndex="16" NumberScale="1"></cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Inbound to Hub">
											<HeaderStyle HorizontalAlign="Right" Width="10%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<%--# DataBinder.Eval(Container.DataItem, "RateToHub") --%>
												<asp:Label ID="lbRateToHub" Runat="server"></asp:Label>
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Right"></FooterStyle>
											<FooterTemplate>
												<cc1:mstextbox style="Z-INDEX: 0" id="txtInboundToHubAdd" tabIndex="11" onpaste="AfterPasteNonNumber(this)"
													runat="server" CssClass="textFieldRightAlign" Width="100%" MaxLength="10" TextMaskType="msNumericCOD"
													NumberPrecision="10" NumberMinValue="0" NumberMaxValueCOD="999999.9999" NumberScale="4"></cc1:mstextbox>
											</FooterTemplate>
											<EditItemTemplate>
												<cc1:mstextbox style="Z-INDEX: 0" id="txtInboundToHubEdit" onpaste="AfterPasteNonNumber(this)"
													runat="server" CssClass="textFieldRightAlign" Width="100%" MaxLength="10" TextMaskType="msNumericCOD"
													NumberPrecision="10" NumberMinValue="0" NumberMaxValueCOD="999999.9999" NumberScale="4" tabIndex="17"></cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Inbound to Others">
											<HeaderStyle HorizontalAlign="Right" Width="7%"></HeaderStyle>
											<ItemStyle HorizontalAlign="Right"></ItemStyle>
											<ItemTemplate>
												<%--# DataBinder.Eval(Container.DataItem, "RateToOthers") --%>
												<asp:Label ID="lbRateToOthers" Runat="server"></asp:Label>
											</ItemTemplate>
											<FooterStyle HorizontalAlign="Right"></FooterStyle>
											<FooterTemplate>
												<cc1:mstextbox style="Z-INDEX: 0" id="txtInboundToOthersAdd" tabIndex="12" onpaste="AfterPasteNonNumber(this)"
													runat="server" CssClass="textFieldRightAlign" Width="100%" MaxLength="10" TextMaskType="msNumericCOD"
													NumberPrecision="10" NumberMinValue="0" NumberMaxValueCOD="999999.9999" NumberScale="4"></cc1:mstextbox>
											</FooterTemplate>
											<EditItemTemplate>
												<cc1:mstextbox style="Z-INDEX: 0" id="txtInboundToOthersEdit" onpaste="AfterPasteNonNumber(this)"
													runat="server" CssClass="textFieldRightAlign" Width="100%" MaxLength="10" TextMaskType="msNumericCOD"
													NumberPrecision="10" NumberMinValue="0" NumberMaxValueCOD="999999.9999" tabIndex="18" NumberScale="4"></cc1:mstextbox>
											</EditItemTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
								</asp:datagrid></td>
						</tr>
					</TBODY>
				</TABLE>
			</div>
			<INPUT 
value="<%=strScrollPosition%>" type=hidden name=ScrollPosition> <INPUT type="hidden" name="hdnRefresh">
		</form>
	</body>
</HTML>
