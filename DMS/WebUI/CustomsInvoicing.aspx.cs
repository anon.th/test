using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.applicationpages;
using TIESClasses;
using TIESDAL;
using com.ties;
using com.ties.DAL;
using com.ties.classes;
using com.common.util;
using System.Text;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CustomsInvoicing.
	/// </summary>
	public class CustomsInvoicing : BasePage
	{
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.Button btnPrintInvoice;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Label Label14;
		protected System.Web.UI.WebControls.Label Label15;
		protected System.Web.UI.WebControls.TextBox Textbox21;
		protected System.Web.UI.WebControls.Label Label18;
		protected System.Web.UI.WebControls.Label Label19;
		protected System.Web.UI.WebControls.Label Label21;
		protected System.Web.UI.WebControls.Label Label22;
		protected System.Web.UI.WebControls.Label Label23;
		protected System.Web.UI.WebControls.Label Label24;
		protected System.Web.UI.WebControls.Label Label25;
		protected System.Web.UI.WebControls.Label Label26;
		protected System.Web.UI.WebControls.Label Label27;
		protected System.Web.UI.WebControls.TextBox Invoice_No;
		protected System.Web.UI.WebControls.TextBox EntryType;
		protected System.Web.UI.WebControls.TextBox consignment_no;
		protected System.Web.UI.WebControls.TextBox MasterAWBNumber;
		protected com.common.util.msTextBox InvoiceDate;
		protected System.Web.UI.WebControls.TextBox CustomerID;
		protected System.Web.UI.WebControls.TextBox CustomerTaxCode;
		protected System.Web.UI.WebControls.TextBox CustomerName;
		protected System.Web.UI.WebControls.TextBox Customer_address1;
		protected System.Web.UI.WebControls.TextBox Customer_address2;
		protected System.Web.UI.WebControls.TextBox Customer_zipcode;
		protected System.Web.UI.WebControls.TextBox Customer_state;
		protected System.Web.UI.WebControls.TextBox Customer_telephone;
		protected System.Web.UI.WebControls.TextBox Customer_fax;
		protected System.Web.UI.WebControls.TextBox Delivery_zipcode;
		protected System.Web.UI.WebControls.TextBox Delivery_state;
		protected System.Web.UI.WebControls.TextBox Delivery_telephone;
		protected System.Web.UI.WebControls.TextBox Delivery_fax;
		protected System.Web.UI.WebControls.TextBox DeliveryDate;
		protected System.Web.UI.WebControls.TextBox ShipFlightNo;
		protected System.Web.UI.WebControls.TextBox ActualArrivalDate;
		protected System.Web.UI.WebControls.TextBox AgencyFees;
		protected System.Web.UI.WebControls.TextBox Disbursements;
		protected System.Web.UI.WebControls.TextBox TotalInvoice;
		protected System.Web.UI.WebControls.TextBox PreparedBy;
		protected System.Web.UI.WebControls.TextBox PreparedDT;
		protected System.Web.UI.WebControls.TextBox ModifiedBy;
		protected System.Web.UI.WebControls.TextBox ModifiedDT;
		protected System.Web.UI.WebControls.TextBox PrintedBy;
		protected System.Web.UI.WebControls.TextBox PrintedDT;
		protected System.Web.UI.WebControls.TextBox ExportedBy;
		protected System.Web.UI.WebControls.TextBox ExportedDT;
		protected System.Web.UI.WebControls.TextBox Delivery_Name;
		protected System.Web.UI.WebControls.TextBox Delivery_address1;
		protected System.Web.UI.WebControls.TextBox Delivery_address2;
		protected System.Web.UI.WebControls.TextBox Desc_OthCharges1;
		protected System.Web.UI.WebControls.TextBox Desc_OthCharges2;
		protected System.Web.UI.WebControls.TextBox Desc_OthCharges3;
		protected System.Web.UI.WebControls.TextBox Desc_OthCharges4;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
	
		private string userID = null;
		private string appID = null;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGrd;
		protected System.Web.UI.WebControls.DataGrid GrdInvoiceDetails;
		protected System.Web.UI.WebControls.Button btnClientEvent;
		protected System.Web.UI.WebControls.Label Label20;
		protected System.Web.UI.WebControls.TextBox From_City;
		protected System.Web.UI.WebControls.TextBox From_CountryCode;
		protected System.Web.UI.WebControls.TextBox From_CountryName;
		protected System.Web.UI.WebControls.Label Label28;
		protected System.Web.UI.WebControls.TextBox DeclarationFormPages;
		protected System.Web.UI.WebControls.Label Label17;
		protected System.Web.UI.WebControls.TextBox FolioNumber;
		protected System.Web.UI.WebControls.Label Label16;
		protected System.Web.UI.WebControls.Label Label29;
		protected System.Web.UI.WebControls.Label Label30;
		protected System.Web.UI.WebControls.Label Label31;
		protected System.Web.UI.WebControls.TextBox AssessmentNumber;
		protected System.Web.UI.WebControls.TextBox AssessmentAmount;
		protected System.Web.UI.WebControls.TextBox AssessmentDate;
		protected com.common.util.msTextBox TotalWeight;
		protected System.Web.UI.WebControls.Button btnExecQryHidden;
		protected System.Web.UI.WebControls.Button btnUnfinalize;
		protected System.Web.UI.WebControls.Label Label32;
		protected com.common.util.msTextBox ChargeableWeight;
        protected System.Web.UI.WebControls.TextBox Packages;
        protected System.Web.UI.WebControls.TextBox Description1;
        protected System.Web.UI.WebControls.TextBox Description2;
        protected System.Web.UI.WebControls.TextBox Description3;
        protected System.Web.UI.WebControls.TextBox Description4;
        protected System.Web.UI.WebControls.TextBox Description5;
        protected System.Web.UI.WebControls.TextBox Description6;
        private string enterpriseID = null;
        private bool IsEnabledPrintInvoice;

        public int DSMode
		{
			get
			{
				if(ViewState["iDSMode"]==null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["iDSMode"];
				}
			}
			set
			{
				ViewState["iDSMode"]=value;
			}
		}

		public int DSOperation
		{
			get
			{
				if(ViewState["iDSOperation"]==null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["iDSOperation"];
				}
			}
			set
			{
				ViewState["iDSOperation"]=value;
			}
		}


		public string currency_decimal
		{
			get
			{
				if(ViewState["currency_decimal"] == null)
				{
					return "00";
				}
				else
				{
					return (string)ViewState["currency_decimal"];
				}
			}
			set
			{
				ViewState["currency_decimal"]=value;
			}
		}


		public int NumberScale
		{
			get
			{
				if(ViewState["NumberScale"] == null)
				{
					return 2;
				}
				else
				{
					return (int)ViewState["NumberScale"];
				}
			}
			set
			{
				ViewState["NumberScale"]=value;
			}
		}


		public bool IsUnfinalizeInvoice
		{
			get
			{
				if(ViewState["IsUnfinalizeInvoice"] == null)
				{
					return false;
				}
				else
				{
					return (bool)ViewState["IsUnfinalizeInvoice"];
				}
			}
			set
			{
				ViewState["IsUnfinalizeInvoice"]=value;
			}
		}


		public DataTable InvoiceDetailsDT
		{
			get
			{
				if(ViewState["InvoiceDetailsDT"]==null)
				{
					return null;
				}
				else
				{
					return (DataTable)ViewState["InvoiceDetailsDT"];
				}
			}
			set
			{
				ViewState["InvoiceDetailsDT"]=value;
			}
		}


		private void Page_Load(object sender, System.EventArgs e)
		{
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();
			if(this.IsPostBack==false)
			{
				currency_decimal =  this.GetCurrencyDigitsOfEnterpriseConfiguration();	
				IsUnfinalizeInvoice= SysDataMgrDAL.IsUserAssignedRoles(appID,enterpriseID,userID,"CUSTOMSSU");

				clearscreen();
				SetInitialFocus(Invoice_No);

				if(Request.QueryString["jobentryno"] != null && Request.QueryString["jobentryno"].ToString().Trim() != "")
				{
					Invoice_No.Text=Request.QueryString["jobentryno"].ToString().Trim();
					ExecQry();
				}
            }
        }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnPrintInvoice.Click += new System.EventHandler(this.btnPrintInvoice_Click);
			this.btnUnfinalize.Click += new System.EventHandler(this.btnUnfinalize_Click);
			this.btnExecQryHidden.Click += new System.EventHandler(this.btnExecQryHidden_Click);
			this.Delivery_zipcode.TextChanged += new System.EventHandler(this.Delivery_zipcode_TextChanged);
			this.GrdInvoiceDetails.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.GrdInvoiceDetails_ItemCommand);
			this.GrdInvoiceDetails.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.GrdInvoiceDetails_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void clearscreen()
		{
			lblError.Text="";

			Invoice_No.Text="";
			InvoiceDate.Text="";
			Invoice_No.Enabled=true;
			Desc_OthCharges1.Text="";
			Desc_OthCharges2.Text="";
			Desc_OthCharges3.Text="";
			Desc_OthCharges4.Text="";
			Delivery_Name.Text="";
			Delivery_address1.Text="";
			Delivery_address2.Text="";
			FolioNumber.Text="";
			DeclarationFormPages.Text="";

			consignment_no.Text="";
			MasterAWBNumber.Text="";
			EntryType.Text="";

			CustomerID.Text="";
			CustomerTaxCode.Text="";
			CustomerName.Text="";
			Customer_address1.Text="";
			Customer_address2.Text="";
			Customer_zipcode.Text="";
			Customer_state.Text="";
			Customer_telephone.Text="";
			Customer_fax.Text="";

			Delivery_zipcode.Text="";
			Delivery_state.Text="";
			Delivery_telephone.Text="";
			Delivery_fax.Text="";
			DeliveryDate.Text="";

			ShipFlightNo.Text="";
			ActualArrivalDate.Text="";
			From_City.Text="";
			From_CountryCode.Text="";
			From_CountryName.Text="";
            Packages.Text = "";
            Description1.Text = "";
            Description2.Text = "";
            Description3.Text = "";
            Description4.Text = "";
            Description5.Text = "";
            Description6.Text = "";

            AgencyFees.Text="";
			Disbursements.Text="";
			TotalInvoice.Text="";

			PreparedBy.Text="";
			PreparedDT.Text="";
			ModifiedBy.Text="";
			ModifiedDT.Text="";
			PrintedBy.Text="";
			PrintedDT.Text="";
			ExportedBy.Text="";
			ExportedDT.Text="";

			TotalWeight.Text="";
			AssessmentNumber.Text="";
			AssessmentDate.Text="";
			AssessmentAmount.Text="";
			ChargeableWeight.Text="";

			InvoiceDetailsDT=null;
			InvoiceDetailsDT=CustomsDAL.InvoiceDetailDataTable();
			this.GrdInvoiceDetails.EditItemIndex=-1;
			BindGrdInvoice();

			btnQuery.Enabled=true;
			btnExecuteQuery.Enabled=true;

			btnSave.Enabled=false;
			btnSave.Visible=true;

            btnPrintInvoice.Enabled = false;

            btnUnfinalize.Enabled=false;
			btnUnfinalize.Visible=IsUnfinalizeInvoice;
	
			DeclarationFormPages.Enabled=true;
			DeclarationFormPages.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");

			DSMode = (int)ScreenMode.None;
			DSOperation = (int)Operation.None;
		}


		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			clearscreen();
			SetInitialFocus(Invoice_No);
		}


		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			ExecQry();
			if(lblError.Text.Trim()=="")
			{
				SetInitialFocus(InvoiceDate);
			}

		}


		private void ExecQry()
		{
			DataTable dtParams = CustomsDAL.InvoiceParameter();
			System.Data.DataRow drNew = dtParams.NewRow();
			drNew["action"] ="0";
			drNew["enterpriseid"] =enterpriseID;
			drNew["userloggedin"] =userID;
			if(Invoice_No.Text.Trim() !="")
			{
				drNew["Invoice_No"] =Invoice_No.Text.Trim();
			}
			else
			{
				drNew["Invoice_No"] = DBNull.Value;
			}
					
			dtParams.Rows.Add(drNew);
			DataSet ds = CustomsDAL.Customs_Invoice(appID,enterpriseID,userID,dtParams);
			System.Data.DataTable dtStatus = ds.Tables[0];
			System.Data.DataTable dtInvoice= ds.Tables[1];			
			if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
			{
				clearscreen();
				BindInvoice(dtInvoice);
				InvoiceDetailsDT = ds.Tables[2];
				GrdInvoiceDetails.EditItemIndex=-1;
				BindGrdInvoice();

				btnSave.Enabled=(ExportedDT.Text.Trim() =="" ? true:false);

                IsEnabledPrintInvoice = SysDataMgrDAL.IsUserAssignedRoles(appID, enterpriseID, userID, "PRINTAGENCYINV");
                btnPrintInvoice.Enabled = IsEnabledPrintInvoice;

                btnExecuteQuery.Enabled=false;
				Invoice_No.Enabled=false;
				//SetInitialFocus(InvoiceDate);
				DSMode = (int)ScreenMode.ExecuteQuery;
				DSOperation = (int)Operation.None;			
			}				
			else
			{
				SetInitialFocus(Invoice_No);
				lblError.Text = dtStatus.Rows[0]["ErrorMessage"].ToString();
			}
		}


		private void BindInvoice(DataTable dt)
		{
			if(dt.Rows.Count<=0)
			{
				return;
			}
			DataRow dr = dt.Rows[0];

			Invoice_No.Text=dr["Invoice_No"].ToString();
			InvoiceDate.Text=(dr["InvoiceDate"] != DBNull.Value ? DateTime.Parse(dr["InvoiceDate"].ToString()).ToString("dd/MM/yyyy") :"");
			Desc_OthCharges1.Text=dr["Desc_OthCharges1"].ToString();
			Desc_OthCharges2.Text=dr["Desc_OthCharges2"].ToString();
			Desc_OthCharges3.Text=dr["Desc_OthCharges3"].ToString();
			Desc_OthCharges4.Text=dr["Desc_OthCharges4"].ToString();
			Delivery_Name.Text=dr["Delivery_Name"].ToString();
			Delivery_address1.Text=dr["Delivery_address1"].ToString();
			Delivery_address2.Text=dr["Delivery_address2"].ToString();
			FolioNumber.Text=dr["FolioNumber"].ToString();
			DeclarationFormPages.Text=dr["DeclarationFormPages"].ToString();

			consignment_no.Text=dr["consignment_no"].ToString();
			MasterAWBNumber.Text=dr["MasterAWBNumber"].ToString();
			EntryType.Text=dr["EntryType"].ToString();

			CustomerID.Text=dr["CustomerID"].ToString();
			CustomerTaxCode.Text=dr["CustomerTaxCode"].ToString();
			CustomerName.Text=dr["CustomerName"].ToString();
			Customer_address1.Text=dr["Customer_address1"].ToString();
			Customer_address2.Text=dr["Customer_address2"].ToString();
			Customer_zipcode.Text=dr["Customer_zipcode"].ToString();
			Customer_state.Text=dr["Customer_state"].ToString();
			Customer_telephone.Text=dr["Customer_telephone"].ToString();
			Customer_fax.Text=dr["Customer_fax"].ToString();

			Delivery_zipcode.Text=dr["Delivery_zipcode"].ToString();
			Delivery_state.Text=dr["Delivery_state"].ToString();
			Delivery_telephone.Text=dr["Delivery_telephone"].ToString();
			Delivery_fax.Text=dr["Delivery_fax"].ToString();
			DeliveryDate.Text=(dr["DeliveryDate"] != DBNull.Value ? DateTime.Parse(dr["DeliveryDate"].ToString()).ToString("dd/MM/yyyy") :"");

			ShipFlightNo.Text=dr["ShipFlightNo"].ToString();
			ActualArrivalDate.Text=(dr["ActualArrivalDate"] != DBNull.Value ? DateTime.Parse(dr["ActualArrivalDate"].ToString()).ToString("dd/MM/yyyy") :"");
			From_City.Text=dr["From_City"].ToString();
			From_CountryCode.Text=dr["From_CountryCode"].ToString();
			From_CountryName.Text=dr["From_CountryName"].ToString();
            Packages.Text = dr["Packages"].ToString();
            Description1.Text = dr["Description1"].ToString();
            Description2.Text = dr["Description2"].ToString();
            Description3.Text = dr["Description3"].ToString();
            Description4.Text = dr["Description4"].ToString();
            Description5.Text = dr["Description5"].ToString();
            Description6.Text = dr["Description6"].ToString();

            AgencyFees.Text=(dr["AgencyFees"] != DBNull.Value ? Convert.ToDecimal(dr["AgencyFees"]).ToString(currency_decimal) :"");
			Disbursements.Text=(dr["Disbursements"] != DBNull.Value ? Convert.ToDecimal(dr["Disbursements"]).ToString(currency_decimal) :"");
			TotalInvoice.Text=(dr["TotalInvoice"] != DBNull.Value ? Convert.ToDecimal(dr["TotalInvoice"]).ToString(currency_decimal) :"");

			PreparedBy.Text=dr["PreparedBy"].ToString();
			PreparedDT.Text=(dr["PreparedDT"] != DBNull.Value ? DateTime.Parse(dr["PreparedDT"].ToString()).ToString("dd/MM/yyyy HH:mm:ss") :"");
			ModifiedBy.Text=dr["ModifiedBy"].ToString();
			ModifiedDT.Text=(dr["ModifiedDT"] != DBNull.Value ? DateTime.Parse(dr["ModifiedDT"].ToString()).ToString("dd/MM/yyyy HH:mm:ss") :"");
			PrintedBy.Text=dr["PrintedBy"].ToString();
			PrintedDT.Text=(dr["PrintedDT"] != DBNull.Value ? DateTime.Parse(dr["PrintedDT"].ToString()).ToString("dd/MM/yyyy HH:mm:ss") :"");
			ExportedBy.Text=dr["ExportedBy"].ToString();
			ExportedDT.Text=(dr["ExportedDT"] != DBNull.Value ? DateTime.Parse(dr["ExportedDT"].ToString()).ToString("dd/MM/yyyy HH:mm:ss") :"");

			if(IsUnfinalizeInvoice)
			{
				btnUnfinalize.Enabled=(dr["ExportedDT"] != DBNull.Value ? true:false);
			}

			DeclarationFormPages.Enabled=true;
			DeclarationFormPages.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
			if(EntryType.Text.Trim().ToUpper()!="FORMAL")
			{
				DeclarationFormPages.Enabled=false;
				DeclarationFormPages.BackColor = System.Drawing.ColorTranslator.FromHtml("#eaeaea");
			}

			TotalWeight.Text=(dr["TotalWeight"] != DBNull.Value ? Convert.ToDecimal(dr["TotalWeight"]).ToString("N1") :"");
			ChargeableWeight.Text=(dr["ChargeableWeight"] != DBNull.Value ? Convert.ToDecimal(dr["ChargeableWeight"]).ToString("N1") :"");
			AssessmentNumber.Text=dr["CustomsDeclarationNo"].ToString();;
			AssessmentDate.Text=(dr["DateOfAssessment"] != DBNull.Value ? DateTime.Parse(dr["DateOfAssessment"].ToString()).ToString("dd/MM/yyyy") :"");
			AssessmentAmount.Text=(dr["AssessmentAmount"] != DBNull.Value ? Convert.ToDecimal(dr["AssessmentAmount"]).ToString("#,##0.00") :"");
			
		}


		private void BindGrdInvoice()
		{
			if(Request.QueryString["pagemode"] != null && Request.QueryString["pagemode"].ToString().Trim() == "popup")
			{
				GrdInvoiceDetails.Columns[0].Visible=false;
			}

			GrdInvoiceDetails.ShowFooter=false;
			if(InvoiceDetailsDT != null && InvoiceDetailsDT.Rows.Count<=0)
			{
				GrdInvoiceDetails.ShowFooter = true;
			}
			GrdInvoiceDetails.Columns[0].Visible=(ExportedDT.Text.Trim() =="" ? true:false);
			GrdInvoiceDetails.DataSource=InvoiceDetailsDT;
			GrdInvoiceDetails.DataBind();
		}


		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			ExecQry();
		}


		private void btnSave_Click(object sender, System.EventArgs e)
		{
			if(Invoice_No.Text.Trim()=="")
			{
				lblError.Text="Invoice Number is required";
				return;
			}
			if(InvoiceDate.Text.Trim()=="")
			{
				lblError.Text="Invoice date is required";
				return;
			}
			try
			{
				DateTime.ParseExact(InvoiceDate.Text,"dd/MM/yyyy",null).ToString("yyyy-MM-dd");
			}
			catch
			{
				lblError.Text="Invoice date is invalid";
				return;
			}
			if(Delivery_zipcode.Text.Trim()=="")
			{
				lblError.Text="Not saved - recipient zipcode is required";
				return;
			}
			if(DeclarationFormPages.Text.Trim()=="" && EntryType.Text.ToLower() =="formal")
			{
				lblError.Text="Declaration form pages is required";
				return;
			}


			try
			{
				if(int.Parse(DeclarationFormPages.Text.Trim())<=0 && EntryType.Text.ToLower() =="formal")
				{
					lblError.Text="Declaration form pages must be > 0 for a formal entry";
					return;
				}
			}
			catch
			{

			}

			try
			{
				if(TotalWeight.Text.Trim()=="")
				{
					lblError.Text="Weight must be greater than zero";
					return;
				}

				if(int.Parse(TotalWeight.Text.Trim()) > 9999.99)
				{
					lblError.Text="Weight must be <= 9999.99";
					return;
				}
			}
			catch
			{

			}
			DataTable dtParams = CustomsDAL.InvoiceParameter();
			System.Data.DataRow drNew = dtParams.NewRow();
			foreach(System.Data.DataColumn col in dtParams.Columns)
			{
				string colName = col.ColumnName;
				System.Web.UI.Control ctrol = this.Page.FindControl(colName);
				if(ctrol != null)
				{
					if(ctrol.GetType()==typeof(TextBox))
					{
						TextBox txt = (TextBox)ctrol;
						if(txt.Text.Trim() != "")
						{
							if(txt.ID=="sender_zipcode" || txt.ID=="recipient_zipcode" || txt.ID.ToLower()=="delivery_zipcode")
							{
								drNew[colName]=txt.Text.ToUpper().Trim();
							}
							else
							{
								drNew[colName]=txt.Text.Trim();
							}							
						}						
					}
					else if(ctrol.GetType()==typeof(com.common.util.msTextBox))
					{
						com.common.util.msTextBox txt = (com.common.util.msTextBox)ctrol;
						if(txt.TextMaskType==MaskType.msDate)
						{
							drNew[colName]=DateTime.ParseExact(txt.Text,"dd/MM/yyyy",null).ToString("yyyy-MM-dd");
						}
						else if(txt.TextMaskType==MaskType.msNumericCOD)
						{							
							try
							{
								drNew[colName]=Decimal.Parse(txt.Text.Trim());
							}
							catch
							{
								drNew[colName] = DBNull.Value;
							}
							
						}						
						else
						{
							if(colName=="TotalWeight" || colName=="ChargeableWeight" )
							{
								drNew[colName]=txt.Text.Trim().Replace(",","");
							}
							else
							{
								drNew[colName]=txt.Text.Trim();
							}
						}
						
					}
					else if(ctrol.GetType()==typeof(DropDownList))
					{
						DropDownList ddl = (DropDownList)ctrol;
						drNew[colName]=ddl.SelectedValue;
					}				
				}
			}
			drNew["action"] ="1";
			drNew["enterpriseid"] =enterpriseID;
			drNew["userloggedin"] =userID;
			drNew["Invoice_No"] =Invoice_No.Text.Trim();			
			dtParams.Rows.Add(drNew);

			DataSet ds = CustomsDAL.Customs_Invoice(appID,enterpriseID,userID,dtParams);
			System.Data.DataTable dtStatus = ds.Tables[0];			
			if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
			{				
				ExecQry();
				lblError.Text = "Invoice saved";
			}
			else
			{
				lblError.Text=(dtStatus.Rows.Count>0?dtStatus.Rows[0]["ErrorMessage"].ToString():"");
			}
		}


		private void btnPrintInvoice_Click(object sender, System.EventArgs e)
		{
			DataSet m_dsQuery = CustomsDAL.Customs_Invoice_Report(appID, enterpriseID, userID, Invoice_No.Text);
			
			string errorCode = m_dsQuery.Tables[0].Rows[0]["ErrorCode"].ToString();
			string formId = m_dsQuery.Tables[0].Rows[0]["InvoiceTemplate"].ToString();
			
			if(m_dsQuery !=null && m_dsQuery.Tables[0].Rows.Count>0)
			{
				if(Convert.ToInt32( m_dsQuery.Tables[0].Rows[0]["ErrorCode"]) > 0)
				{
					lblError.Text = m_dsQuery.Tables[0].Rows[0]["ErrorMessage"].ToString();
				}
				else if(Convert.ToString(m_dsQuery.Tables[0].Rows[0]["InvoiceTemplate"]).Equals(String.Empty))
				{
					lblError.Text =Utility.GetLanguageText(ResourceType.UserMessage,"TEMPLATE_NOT_FOUND",utility.GetUserCulture());;
				}
				else
				{
					ExecQry();
					lblError.Text ="";
					String strUrl = null;
					strUrl = "ReportViewer1.aspx";
					String reportTemplate = m_dsQuery.Tables[0].Rows[0]["InvoiceTemplate"].ToString();
					Session["REPORT_TEMPLATE"] = reportTemplate;
					Session["FORMID"] = "Customs_Invoice";
					Session["SESSION_DS_CUSTOMS_INVOICE"] = m_dsQuery;
                    Session["SESSION_DS_CUSTOMS_INVOICE_NO"] = m_dsQuery.Tables[1].Rows[0]["Invoice_Number"].ToString();
                    Session["FormatType"] = ".pdf";
					ArrayList paramList = new ArrayList();
					paramList.Add(strUrl);
					String sScript = Utility.GetScript("openParentWindowReport.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);					
				}
			}
		}


		private void GrdInvoiceDetails_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			this.lblError.Text="";
			string cmd = e.CommandName;
			if(cmd=="EDIT_ITEM")
			{
                //aw_Khatawut comment 2019-06-18 : DMSUpgrade 2.4.1) After searching Job Status, there should be able to update data on the search result
                //if(Request.QueryString["pagemode"] != null && Request.QueryString["pagemode"].ToString().Trim() == "popup")
                //{
                //	return;
                //}
                GrdInvoiceDetails.EditItemIndex=e.Item.ItemIndex;
				this.BindGrdInvoice();
			}
			else if(cmd=="SAVE_ITEM")
			{
				Label lblConsignment_no =  (Label)e.Item.FindControl("lblConsignment_no");
				Label lblPayerid =  (Label)e.Item.FindControl("lblPayerid");
				Label lblInvoice_No =  (Label)e.Item.FindControl("lblInvoice_No");
				TextBox txtCode =  (TextBox)e.Item.FindControl("txtCode");
				com.common.util.msTextBox txtOverrideAmount =  (com.common.util.msTextBox)e.Item.FindControl("txtOverrideAmount");

				DataTable dtParams = CustomsDAL.InvoiceDetailParameter();
				System.Data.DataRow drNew = dtParams.NewRow();

				drNew["enterpriseid"] =enterpriseID;
				drNew["userloggedin"] =userID;
				drNew["consignment_no"] =lblConsignment_no.Text.Trim();		
				drNew["payerid"] =lblPayerid.Text.Trim();	
				drNew["Invoice_No"] =lblInvoice_No.Text.Trim();	
				drNew["ChargeCode"] =txtCode.Text.Trim();	
				drNew["OverrideAmount"] =txtOverrideAmount.Text.Trim();	
				dtParams.Rows.Add(drNew);

				DataSet ds = CustomsDAL.Customs_InvoiceDetails(appID,enterpriseID,userID,dtParams);
				System.Data.DataTable dtStatus = ds.Tables[0];			
				if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
				{				
					dtParams = CustomsDAL.InvoiceParameter();
					drNew = dtParams.NewRow();
					drNew["action"] ="0";
					drNew["enterpriseid"] =enterpriseID;
					drNew["userloggedin"] =userID;
					if(Invoice_No.Text.Trim() !="")
					{
						drNew["Invoice_No"] =Invoice_No.Text.Trim();
					}
					else
					{
						drNew["Invoice_No"] = DBNull.Value;
					}					
					dtParams.Rows.Add(drNew);
					ds = CustomsDAL.Customs_Invoice(appID,enterpriseID,userID,dtParams);	
					if(dtStatus.Rows.Count>0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
					{
						InvoiceDetailsDT = ds.Tables[2];
						GrdInvoiceDetails.EditItemIndex=-1;
						BindGrdInvoice();
					}
					lblError.Text = dtStatus.Rows[0]["ErrorMessage"].ToString();
				}
				else
				{
					lblError.Text=(dtStatus.Rows.Count>0?dtStatus.Rows[0]["ErrorMessage"].ToString():"");
				}
			}
			else if(cmd=="CANCEL_ITEM")
			{
				GrdInvoiceDetails.EditItemIndex=-1;
				this.BindGrdInvoice();
			}
		}


		private void GrdInvoiceDetails_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.EditItem)
			{
				DataRowView dr = (DataRowView)e.Item.DataItem;
				Label txtCalculatedAmount = (Label)e.Item.FindControl("txtCalculatedAmount");
				com.common.util.msTextBox txtOverrideAmount = (com.common.util.msTextBox)e.Item.FindControl("txtOverrideAmount");
				if(txtCalculatedAmount != null)
				{
					if(dr["CalculatedAmount"] != DBNull.Value && dr["CalculatedAmount"].ToString() != "")
					{
						txtCalculatedAmount.Text= Convert.ToDecimal(dr["CalculatedAmount"]).ToString(currency_decimal); 
					}						
				}

				if(txtOverrideAmount != null)
				{
					if(dr["OverrideAmount"] != DBNull.Value && dr["OverrideAmount"].ToString() != "")
					{
						txtOverrideAmount.Text= Convert.ToDecimal(dr["OverrideAmount"]).ToString(currency_decimal); 
					}						
					txtOverrideAmount.NumberScale=NumberScale;
					SetInitialFocus(txtOverrideAmount);
				}
			}

			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				DataRowView dr = (DataRowView)e.Item.DataItem;
				if(dr["CalculatedAmount"] != DBNull.Value && dr["CalculatedAmount"].ToString() != "")
				{
					Label lblCalculated = (Label)e.Item.FindControl("lblCalculated");
					try
					{
						lblCalculated.Text = Convert.ToDecimal(dr["CalculatedAmount"]).ToString(currency_decimal); 
					}
					catch
					{
						lblCalculated.Text = dr["OverrideAmount"].ToString();
					}	
				}

				if(dr["OverrideAmount"] != DBNull.Value && dr["OverrideAmount"].ToString() != "")
				{
					Label lblOverride = (Label)e.Item.FindControl("lblOverride");
					try
					{
						lblOverride.Text = Convert.ToDecimal(dr["OverrideAmount"]).ToString(currency_decimal); 
					}
					catch
					{
						lblOverride.Text = dr["OverrideAmount"].ToString();
					}
					
				}

				if(dr["InvoiceAmount"] != DBNull.Value && dr["InvoiceAmount"].ToString() != "")
				{
					Label lblInvoice = (Label)e.Item.FindControl("lblInvoice");
					try
					{
						lblInvoice.Text = Convert.ToDecimal(dr["InvoiceAmount"]).ToString(currency_decimal); 
					}
					catch
					{
						lblInvoice.Text = dr["InvoiceAmount"].ToString();
					}
					
				}
			}
		}


		public string GetCurrencyDigitsOfEnterpriseConfiguration()
		{
			DataSet dsEprise = SysDataManager1.GetEnterpriseProfile(utility.GetAppID(),utility.GetEnterpriseID());
			DataRow dr = dsEprise.Tables[0].Rows[0];
			
			string tempplate = "00";
			if(dr["currency_decimal"].ToString()!="")
			{
				tempplate="";
				int number_digit = Convert.ToInt32(dr["currency_decimal"].ToString());
				NumberScale=number_digit;
				for (int i = 0 ; i< number_digit ; i++)
				{
					tempplate = tempplate+"0";
				}
			}
			return "#,##0." + tempplate;
		}


		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				System.Text.StringBuilder s = new System.Text.StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("SetScrollPosition();\n");
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}

		private void btnExecQryHidden_Click(object sender, System.EventArgs e)
		{
			btnExecuteQuery_Click(null,null);
		}

		private void btnUnfinalize_Click(object sender, System.EventArgs e)
		{
			DataSet ds = CustomsDAL.Customs_UnfinalizeInvoice(appID,enterpriseID,userID,Invoice_No.Text.Trim());
			System.Data.DataTable dtStatus = ds.Tables[0];			
			if(dtStatus.Rows.Count >0 && Convert.ToInt32(dtStatus.Rows[0]["ErrorCode"].ToString()) == 0)
			{
				ExecQry();
			}
			lblError.Text=dtStatus.Rows[0]["ErrorMessage"].ToString();
		}

		private void Delivery_zipcode_TextChanged(object sender, System.EventArgs e)
		{
			if(Delivery_zipcode.Text.Trim() != "")
			{
				Delivery_state.Text=GetStateName(Delivery_zipcode.Text);
				SetInitialFocus(Delivery_zipcode);
			}
			else
			{
				Delivery_state.Text="";
				SetInitialFocus(Delivery_zipcode);				
			}
		}

		private string GetStateName(string zipcode)
		{
			string state_name="";
			System.Data.DataSet ds = CustomerConsignmentDAL.GetZipcode(this.appID,this.enterpriseID,zipcode);
			if(ds.Tables["Zipcode"].Rows.Count>0 && ds.Tables["Zipcode"].Rows[0]["state_name"] != DBNull.Value)
			{
				state_name = ds.Tables["Zipcode"].Rows[0]["state_name"].ToString();
			}
			
			return state_name;
		}


	}
}
