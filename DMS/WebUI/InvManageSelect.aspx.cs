using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using com.common.RBAC;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.common.applicationpages;
using com.ties.classes;
using com.ties.DAL;
using com.ties.BAL;
using com.ties;
using System.IO;
using System.Data.SqlClient;
using System.Diagnostics;

using System.Runtime.InteropServices;
using System.Net;
using System.Configuration;
using System.Threading;
using System.Data.OleDb;
using System.Text;
using TIESDAL;

//using System.IO;

namespace com.ties
{
    /// <summary>
    /// Summary description for InvManageSelect.
    /// </summary>
    public class InvManageSelect : BasePage
    {
        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtExcludeCustomerList.TextChanged += new System.EventHandler(this.txtExcludeCustomerList_TextChanged);
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            this.btnApproveSelected.Click += new System.EventHandler(this.btnApproveSelected_Click);
            this.btnCancelSelected.Click += new System.EventHandler(this.btnCancelSelected_Click);
            this.btnPrintHeader.Click += new System.EventHandler(this.btnPrintHeader_Click);
            this.btnPrintDetail.Click += new System.EventHandler(this.btnPrintDetail_Click);
            this.btnPrintInvPkg.Click += new System.EventHandler(this.btnPrintInvPkg_Click);
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            this.btnSaveImport.Click += new System.EventHandler(this.btnSaveImport_Click);
            this.dgPreview.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgPreview_ItemCommand);
            this.dgPreview.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.dgPreview_EditCommand);
            this.dgPreview.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgPreview_ItemDataBound);
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            this.txtInvDateto.TextChanged += new System.EventHandler(this.txtInvDateto_TextChanged);
            this.txtInvoiceNo.TextChanged += new System.EventHandler(this.txtInvoiceNo_TextChanged);
            this.txtDueDateto.TextChanged += new System.EventHandler(this.txtDueDateto_TextChanged);
            this.ddlInvType.SelectedIndexChanged += new System.EventHandler(this.ddlInvType_SelectedIndexChanged);
            this.ddlInvStatus.SelectedIndexChanged += new System.EventHandler(this.ddlInvStatus_SelectedIndexChanged);
            this.ddbCustomerAcc.SelectedIndexChanged += new System.EventHandler(this.ddbCustomerAcc_SelectedIndexChanged_1);
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            this.btnOKCancelInvoice.Click += new System.EventHandler(this.btnOKCancelInvoice_Click);
            this.btnSaveCancel.Click += new System.EventHandler(this.btnSaveCancel_Click);
            this.btnCancelINVRemark.Click += new System.EventHandler(this.btnCancel_Click);
            this.btnCancelDtl.Click += new System.EventHandler(this.btnCancelDtl_Click);
            this.dgEditDtl.SelectedIndexChanged += new System.EventHandler(this.dgEditDtl_SelectedIndexChanged);
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            this.btnCancelinvoice.Click += new System.EventHandler(this.btnCancelinvoice_Click);
            this.dgAssociatedCNDN.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgAssociatedCNDN_PageIndexChanged);
            this.dgAssociatedCNDN.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgAssociatedCNDN_ItemDataBound);
            this.ExecuteCheckBox.ServerClick += new System.EventHandler(this.ExecuteCheckBox_ServerClick);
            this.Load += new System.EventHandler(this.Page_Load);

        }


        protected System.Web.UI.WebControls.Button btnGenerate;
        protected System.Web.UI.WebControls.Button btnClear;
        protected System.Web.UI.WebControls.Label lblErrorMessage;
        protected System.Web.UI.WebControls.Label Label2;
        protected System.Web.UI.WebControls.Label lblTitle;
        protected System.Web.UI.HtmlControls.HtmlTable tblShipmentExcepPODRepQry;
        protected System.Web.UI.WebControls.Label Label3;
        protected System.Web.UI.WebControls.Label Label4;
        protected System.Web.UI.WebControls.Label Label10;
        protected System.Web.UI.WebControls.Label Label11;
        protected System.Web.UI.WebControls.DropDownList ddbCustomerAcc;
        protected System.Web.UI.HtmlControls.HtmlTable tblDates;
        #endregion

        #region InvoiceDetails Member

        protected System.Web.UI.HtmlControls.HtmlTable Table2;
        protected System.Web.UI.HtmlControls.HtmlTable Table1;
        protected Cambro.Web.DbCombo.DbCombo dbCmbAgentId;

        string strFileName = null;
        String strRemark = null;
        decimal temp_tot_freight;
        decimal temp_insurance_amt;
        decimal temp_other_surcharge;
        decimal temp_esa_surcharge;
        decimal temp_total_exception;
        decimal temp_mbg_amount;
        decimal temp_old_invoice_adj;
        decimal temp_new_invoice_adj;
        decimal temp_tot_vas_surcharge;
        decimal temp_old_total;
        decimal temp_new_total;
        DataSet dsInvoice = null;

        #endregion

        #region Control & Variable Define

        protected System.Web.UI.WebControls.DropDownList ddlInvType;
        protected System.Web.UI.WebControls.DropDownList ddlInvStatus;
        protected System.Web.UI.WebControls.TextBox txtCustName;
        protected System.Web.UI.WebControls.Label lblInvoiceDate;
        protected com.common.util.msTextBox txtInvDateFrom;
        protected System.Web.UI.WebControls.Label lblInvoiceNo;
        protected System.Web.UI.WebControls.Label lblDueDate;
        protected System.Web.UI.WebControls.Label lblInvoiceType;
        protected System.Web.UI.WebControls.Label lblCustID;
        protected System.Web.UI.WebControls.Label lblCustName;
        protected System.Web.UI.WebControls.Label lblStatust;
        protected com.common.util.msTextBox txtInvDateto;
        protected System.Web.UI.WebControls.TextBox txtInvoiceNo;
        protected com.common.util.msTextBox txtDueDateFrom;
        protected com.common.util.msTextBox txtDueDateto;
        protected System.Web.UI.WebControls.Label lblMainTitle;
        protected System.Web.UI.WebControls.Button btnPrintDetail;
        protected System.Web.UI.WebControls.Button btnPrintHeader;
        protected System.Web.UI.WebControls.Button btnCancelSelected;
        protected System.Web.UI.WebControls.Button btnApproveSelected;
        protected System.Web.UI.WebControls.Button btnSelectAll;
        protected System.Web.UI.WebControls.Label lblShowStatust;
        protected System.Web.UI.WebControls.Label Label1;
        protected System.Web.UI.WebControls.DataGrid dgPreview;
        protected System.Web.UI.HtmlControls.HtmlGenericControl divInvoiceManagment;
        protected System.Web.UI.HtmlControls.HtmlGenericControl divInvManageList;
        protected System.Web.UI.HtmlControls.HtmlGenericControl divInvoiceDetails;

        private String m_strUserID = null;
        private DataSet m_sdsInvoiceQuery = null, dsExport = null;
        private string appID;
        private string enterpriseID;
        private string m_strCulture;
        protected System.Web.UI.WebControls.Label lblErrorMsg;
        protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
        protected System.Web.UI.WebControls.Label Label5;
        protected System.Web.UI.WebControls.Label Label50;
        protected System.Web.UI.WebControls.Label lblProvince;
        protected System.Web.UI.WebControls.Label Label48;
        protected System.Web.UI.WebControls.Label Label49;
        protected System.Web.UI.WebControls.Label lblTelephone;
        protected System.Web.UI.WebControls.Label Label45;
        protected System.Web.UI.WebControls.Label lblAddress1;
        protected System.Web.UI.WebControls.Label Label47;
        protected System.Web.UI.WebControls.Label lblFax;
        protected System.Web.UI.WebControls.Label Label9;
        protected System.Web.UI.WebControls.Label lblAddress2;
        protected System.Web.UI.WebControls.Label Label19;
        protected System.Web.UI.WebControls.Label lblContact;
        protected System.Web.UI.WebControls.Label Label12;
        protected System.Web.UI.WebControls.Label lblPostalCode;
        protected System.Web.UI.WebControls.Label Label46;
        protected System.Web.UI.WebControls.Label lblHC_POD_Required;
        protected System.Web.UI.WebControls.Label Label13;
        protected System.Web.UI.WebControls.Label lblInvNo;
        protected System.Web.UI.WebControls.Label Label14;
        protected System.Web.UI.WebControls.Label Label15;
        protected System.Web.UI.WebControls.Label Label16;
        protected System.Web.UI.WebControls.Label lblInvStatus;
        protected System.Web.UI.WebControls.Label Label17;
        protected System.Web.UI.WebControls.Label lblTotalAmt;
        protected System.Web.UI.WebControls.Label Label18;
        protected System.Web.UI.WebControls.Label lblPaymentTerm;
        protected System.Web.UI.WebControls.Label Label20;
        protected System.Web.UI.WebControls.Label lblTotalAdj;
        protected System.Web.UI.WebControls.Label Label21;
        protected System.Web.UI.WebControls.Label lblInvDate;
        protected System.Web.UI.WebControls.Label lblCancel_AppHead;
        protected System.Web.UI.WebControls.Label lblCancel_AppText;
        protected System.Web.UI.WebControls.DataGrid Datagrid1;
        protected System.Web.UI.WebControls.DataGrid dgEdit;
        protected string strInvNo;
        protected double i, sum;
        protected System.Web.UI.WebControls.Label _Label1;
        protected System.Web.UI.WebControls.Button btnCancel;
        protected System.Web.UI.WebControls.Button btnExport;
        protected System.Web.UI.WebControls.Label Label6;
        protected System.Web.UI.WebControls.Panel Panel1;
        protected System.Web.UI.WebControls.Label lblErrorForCancel;
        protected System.Web.UI.WebControls.TextBox txtFilePath;
        protected System.Web.UI.WebControls.Button btnImport;
        protected System.Web.UI.HtmlControls.HtmlInputFile inFile;
        protected System.Web.UI.WebControls.Label lblSendInfo;
        protected System.Web.UI.WebControls.Label lblSendAddr1;
        protected System.Web.UI.WebControls.DropDownList ddlInvoiceCancelType;
        protected System.Web.UI.WebControls.Label lblShowCancelInvoice;
        protected System.Web.UI.WebControls.Label lblSendNm;
        protected System.Web.UI.WebControls.DropDownList ddlInvoiceCancelReason;
        protected System.Web.UI.WebControls.Button btnOKCancelInvoice;
        protected System.Web.UI.WebControls.Button btnSaveCancel;
        protected System.Web.UI.WebControls.Button btnCancelINVRemark;
        protected System.Web.UI.WebControls.Panel PanelInvoiceCancel;
        protected System.Web.UI.HtmlControls.HtmlTable tblSenderInfo;
        protected System.Web.UI.WebControls.Button btnSaveImport;
        protected System.Web.UI.WebControls.Button btnPrintInvPkg;
        protected System.Web.UI.WebControls.Button btnCancelDtl;
        protected System.Web.UI.WebControls.Label lblErrorMsgDtl;
        protected System.Web.UI.WebControls.ValidationSummary PageValidationSummaryDtl;
        protected System.Web.UI.WebControls.Label lblMainTitleDtl;
        protected System.Web.UI.WebControls.Label Label_50;
        protected System.Web.UI.WebControls.Label lblCustIDDtl;
        protected System.Web.UI.WebControls.Label Label_6;
        protected System.Web.UI.WebControls.Label lblProvinceDtl;
        protected System.Web.UI.WebControls.Label Label_48;
        protected System.Web.UI.WebControls.Label lblCustNameDtl;
        protected System.Web.UI.WebControls.Label Label_49;
        protected System.Web.UI.WebControls.Label lblTelephoneDtl;
        protected System.Web.UI.WebControls.Label Label_45;
        protected System.Web.UI.WebControls.Label lblAddress1Dtl;
        protected System.Web.UI.WebControls.Label Label_47;
        protected System.Web.UI.WebControls.Label lblFaxDtl;
        protected System.Web.UI.WebControls.Label Label_1;
        protected System.Web.UI.WebControls.Label lblAddress2Dtl;
        protected System.Web.UI.WebControls.Label Label_19;
        protected System.Web.UI.WebControls.Label lblContactDtl;
        protected System.Web.UI.WebControls.Label Label_2;
        protected System.Web.UI.WebControls.Label lblPostalCodeDtl;
        protected System.Web.UI.WebControls.Label Label_46;
        protected System.Web.UI.WebControls.Label lblHC_POD_RequiredDtl;
        protected System.Web.UI.WebControls.Label Label_3;
        protected System.Web.UI.WebControls.Label lblInvNoDtl;
        protected System.Web.UI.WebControls.Label Label_7;
        protected System.Web.UI.WebControls.Label lblDueDateDtl;
        protected System.Web.UI.WebControls.Label Label_9;
        protected System.Web.UI.WebControls.Label lblInvStatusDtl;
        protected System.Web.UI.WebControls.Label Label_11;
        protected System.Web.UI.WebControls.Label lblTotalAmtDtl;
        protected System.Web.UI.WebControls.Label Label_13;
        protected System.Web.UI.WebControls.Label lblPaymentTermDtl;
        protected System.Web.UI.WebControls.Label Label_15;
        protected System.Web.UI.WebControls.Label lblTotalAdjDtl;
        protected System.Web.UI.WebControls.Label Label_17;
        protected System.Web.UI.WebControls.Label lblInvDateDtl;
        protected System.Web.UI.WebControls.Label lblCancel_AppHeadDtl;
        protected System.Web.UI.WebControls.Label lblCancel_AppTextDtl;
        protected System.Web.UI.WebControls.DataGrid dgEditDtl;
        protected System.Web.UI.WebControls.DataGrid dgPreviewDtl;
        protected System.Web.UI.HtmlControls.HtmlGenericControl InvoiceDetails;
        protected System.Web.UI.HtmlControls.HtmlTable Table3;
        protected System.Web.UI.HtmlControls.HtmlTable Table4;
        protected System.Web.UI.WebControls.Label LblHeading;
        protected System.Web.UI.WebControls.Button btnOK;
        protected System.Web.UI.WebControls.Label ErrMsg;
        protected System.Web.UI.WebControls.Label Label7;
        protected System.Web.UI.WebControls.TextBox txtCustID;
        protected System.Web.UI.WebControls.Label lblInvoiceStatus;
        protected System.Web.UI.WebControls.Label lblInvDispStat;
        protected System.Web.UI.WebControls.Label Label8;
        protected System.Web.UI.WebControls.Label lblCustDispName;
        protected System.Web.UI.WebControls.TextBox txtInvNo;
        protected System.Web.UI.WebControls.Label lblLastUpdate;
        protected System.Web.UI.WebControls.Label lblDispUpdate;
        protected System.Web.UI.WebControls.Label lblPrintedDueDate;
        protected com.common.util.msTextBox txtPrintedDueDate;
        protected System.Web.UI.WebControls.Label lblApproveDate;
        protected System.Web.UI.WebControls.Label lblDispApprove;
        protected System.Web.UI.WebControls.Label lblInternalDueDate;
        protected com.common.util.msTextBox txtInternalDueDate;
        protected System.Web.UI.WebControls.Label lblActualBPD;
        protected com.common.util.msTextBox txtActualBPD;
        protected System.Web.UI.WebControls.Label lblBatchNo;
        protected System.Web.UI.WebControls.TextBox txtBatchNo;
        protected System.Web.UI.WebControls.Label lblPPD;
        protected com.common.util.msTextBox txtPromisedPaymentDate;
        protected System.Web.UI.WebControls.Label lblTotalInvAmnt;
        protected com.common.util.msTextBox txtTotalInvAmnt;
        protected System.Web.UI.WebControls.Label lblAmntDate;
        protected com.common.util.msTextBox txtAmntPaid;
        protected System.Web.UI.WebControls.Label lblCurrentAmntPaid;
        protected com.common.util.msTextBox txtCurrAmntPaid;
        protected System.Web.UI.WebControls.Label lblPaidDate;
        protected com.common.util.msTextBox txtPaidDate;
        protected System.Web.UI.WebControls.Label lblBalanceDue;
        protected com.common.util.msTextBox txtBalanceDue;
        protected System.Web.UI.WebControls.Label lblPaymentUpd;
        protected System.Web.UI.WebControls.Label lblDispPaymentUpdate;
        protected System.Web.UI.WebControls.DropDownList ddlLinkToCDN;
        protected System.Web.UI.WebControls.Label lblApplyAmt;
        protected com.common.util.msTextBox txtApplyAmt;
        protected System.Web.UI.WebControls.DataGrid dgAssociatedCNDN;
        protected System.Web.UI.WebControls.TextBox txtUserName;
        protected System.Web.UI.WebControls.Panel pnlinvupdate;
        protected System.Web.UI.HtmlControls.HtmlInputButton ExecuteCheckBox;
        protected bool boolCheck;

        private String InvNo = "";
        protected int currency_decimal;

        private String CustId = "";
        private String entID = "", culture = "";
        private DataSet dsASCNDN;
        protected System.Web.UI.WebControls.Button btnCancelinvoice;
        protected com.common.util.msTextBox hidtxtCurrAmt;
        protected com.common.util.msTextBox hidtxtBalanceDue;
        protected System.Web.UI.WebControls.TextBox txtExcludeCustomerList;
        protected System.Web.UI.WebControls.Button btnHiddenExport;
        protected String strFormatCurrency;

        #endregion

        #region Properties

        public String ConsgnNo
        {
            get { return strInvNo; }
            set { strInvNo = value; }
        }

        public String UserRole
        {
            get { return ViewState["UserRole"].ToString(); }
            set { ViewState["UserRole"] = value; }
        }

        public int CountSeletct
        {
            get
            {
                if (ViewState["CountSeletct"] == null)
                    ViewState["CountSeletct"] = 0;
                return (int)ViewState["CountSeletct"];
            }
            set
            {
                ViewState["CountSeletct"] = value;
            }
        }

        #endregion

        private void EnterpriseConfigurations()
        {
            InvoiceManagementListingConfigurations conf = new InvoiceManagementListingConfigurations();
            conf = EnterpriseConfigMgrDAL.GetInvoiceManagementListingConfigurations(utility.GetAppID(), utility.GetEnterpriseID());

            btnApproveSelected.Visible = conf.InvoiceManagementApproveSelected;
            btnPrintHeader.Visible = conf.InvoiceManagementPrintHeaderSelected;
            btnPrintDetail.Visible = conf.InvoiceManagementPrintDetailSelected;
            btnPrintInvPkg.Visible = conf.InvoiceManagementPrintDetailPkgSelected;

            dgPreview.Columns[1].Visible = conf.ShowAdvancedInvoicing;
            dgPreview.Columns[2].Visible = conf.ShowAdvancedInvoicing;
            dgPreview.Columns[9].Visible = conf.ShowAdvancedInvoicing;
            dgPreview.Columns[13].Visible = conf.ShowAdvancedInvoicing;
            dgPreview.Columns[14].Visible = conf.ShowAdvancedInvoicing;
            dgPreview.Columns[15].Visible = conf.ShowAdvancedInvoicing;
            dgPreview.Columns[16].Visible = conf.ShowAdvancedInvoicing;
            dgPreview.Columns[17].Visible = conf.ShowAdvancedInvoicing;
            dgPreview.Columns[18].Visible = conf.ShowAdvancedInvoicing;
            dgPreview.Columns[19].Visible = conf.ShowAdvancedInvoicing;
            dgPreview.Columns[20].Visible = conf.ShowAdvancedInvoicing;
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings, Page.Session);
            EnterpriseConfigurations();
            appID = utility.GetAppID();
            enterpriseID = utility.GetEnterpriseID();
            m_strUserID = utility.GetUserID();
            m_strCulture = utility.GetUserCulture();
            ddbCustomerAcc.Visible = true;
            dbCmbAgentId.Visible = false;

            dbCmbAgentId.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

            this.btnCancelINVRemark.Attributes.Add("onclick", "javascript:document.getElementById('" + this.ddlInvoiceCancelType.ClientID + "').disabled=true;" + "javascript:document.getElementById('" + this.ddlInvoiceCancelReason.ClientID + "').disabled=true;" + "javascript:document.getElementById('" + this.btnCancelINVRemark.ClientID + "').disabled=true;" + "javascript:document.getElementById('" + this.btnSaveCancel.ClientID + "').disabled=true;" + this.GetPostBackEventReference(this.btnCancelINVRemark));
            this.btnSaveCancel.Attributes.Add("onclick", "javascript:document.getElementById('" + this.ddlInvoiceCancelType.ClientID + "').disabled=true;" + "javascript:document.getElementById('" + this.ddlInvoiceCancelReason.ClientID + "').disabled=true;" + "javascript:document.getElementById('" + this.btnCancelINVRemark.ClientID + "').disabled=true;" + "javascript:document.getElementById('" + this.btnSaveCancel.ClientID + "').disabled=true;" + this.GetPostBackEventReference(this.btnSaveCancel));

            if (!IsPostBack)
            {
                DataSet profileDS = SysDataManager1.GetEnterpriseProfile(utility.GetAppID(), utility.GetEnterpriseID());

                this.btnOK.Attributes.Add("onclick", "javascript:document.getElementById('" + this.btnOK.ClientID + "').disabled=true;" + this.GetPostBackEventReference(this.btnOK));
                //this.btnGenerateInv.Attributes.Add("onclick","javascript:document.getElementById('"+ this.btnGenerateInv.ClientID + "').disabled=true;" + this.GetPostBackEventReference(this.btnGenerateInv));
                //Call function for rounding in TIESUtility 
                if (profileDS.Tables[0].Rows.Count > 0)
                {
                    int currency_decimal = (int)profileDS.Tables[0].Rows[0]["currency_decimal"];
                    ViewState["m_format"] = "{0:N" + currency_decimal.ToString() + "}";

                    if (profileDS.Tables[0].Rows[0]["wt_rounding_method"] != System.DBNull.Value)
                    {

                        ViewState["wt_rounding_method"] = Convert.ToInt32(profileDS.Tables[0].Rows[0]["wt_rounding_method"]);
                    }

                    if (profileDS.Tables[0].Rows[0]["wt_increment_amt"] != System.DBNull.Value)
                    {
                        ViewState["wt_increment_amt"] = Convert.ToDecimal(profileDS.Tables[0].Rows[0]["wt_increment_amt"]);
                    }
                }

                if (TIESDAL.PostDeliveryCOD.GetUserRoleID(appID, enterpriseID, m_strUserID) == 0)
                {
                    this.UserRole = "ACCTMGR";
                }
                else
                {
                    if (TIESDAL.PostDeliveryCOD.GetUserRole(appID, enterpriseID, m_strUserID, "ACCT") == 1)
                    {
                        this.UserRole = "ACCT";
                    }
                    else if (TIESDAL.PostDeliveryCOD.GetUserRole(appID, enterpriseID, m_strUserID, "ACCTMGR") == 1)
                    {
                        this.UserRole = "ACCTMGR";
                    }
                    else
                    {
                        this.UserRole = "";
                    }
                }
                this.btnApproveSelected.Enabled = false;
                //this.btnCancelSelected.Enabled = false; 

                ActiveDIVSearch();
                LoadCustomerAccount();
                LoadInvoiceType();
                LoadStatus();
            }
            else
            {
                if (this.PanelInvoiceCancel.Visible == true)
                {
                    this.btnCancel.Enabled = false;
                    this.btnApproveSelected.Enabled = false;
                    this.btnSelectAll.Enabled = false;
                    //this.btnCancelSelected.Enabled = false;
                    this.btnPrintHeader.Enabled = false;
                    this.btnPrintDetail.Enabled = false;

                    DataTable dtInvChecked = (DataTable)ViewState["dtInvChecked"];
                    for (int j = 0; j < dtInvChecked.Rows.Count; j++)
                    {
                        for (int i = 0; i < this.dgPreview.Items.Count; i++)
                        {
                            ImageButton ib = (ImageButton)this.dgPreview.Items[i].FindControl("imgSelect");
                            ib.Visible = false;

                            if (dgPreview.Items[i].Cells[3].Text == dtInvChecked.Rows[j][0].ToString())
                            {
                                CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");
                                chkSelect.Checked = true;
                            }
                        }
                    }

                    return;
                }
                //SetRowsColor(dgPreview);
                if (this.divInvoiceManagment.Visible == true)
                {
                    BindCustListToTextName();
                }
            }


        }


        #region "Selection Listing"

        public void LoadCustomerAccount()
        {
            Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings, HttpContext.Current.Session);
            String strAppID = util.GetAppID();
            String strEnterpriseID = util.GetEnterpriseID();

            string temInvDateFrom = "";
            if (txtInvDateFrom.Text != "")
            {
                DateTime dtInvDateFrom = DateTime.ParseExact(txtInvDateFrom.Text, "dd/MM/yyyy", null);
                temInvDateFrom = dtInvDateFrom.ToString("yyyy-MM-dd");
            }
            string temInvDateto = "";
            if (txtInvDateto.Text != "")
            {
                DateTime dtInvDateto = DateTime.ParseExact(txtInvDateto.Text, "dd/MM/yyyy", null);
                temInvDateto = dtInvDateto.ToString("yyyy-MM-dd");
            }

            string temDueDateFrom = "";
            if (txtDueDateFrom.Text != "")
            {
                DateTime dtDueDateFrom = DateTime.ParseExact(txtDueDateFrom.Text, "dd/MM/yyyy", null);
                temDueDateFrom = dtDueDateFrom.ToString("yyyy-MM-dd");
            }
            string temDueDateto = "";
            if (txtDueDateto.Text != "")
            {
                DateTime dtDueDateto = DateTime.ParseExact(txtDueDateto.Text, "dd/MM/yyyy", null);
                temDueDateto = dtDueDateto.ToString("yyyy-MM-dd");
            }
            string InvType = ddlInvType.SelectedValue;
            string InvStatus = (ddlInvStatus.SelectedValue == "" ? "N" : ddlInvStatus.SelectedValue);

            DataSet dataset = com.ties.classes.DbComboDAL.CustomerAccountQueryInv(strAppID, strEnterpriseID, temInvDateFrom, temInvDateto, txtInvoiceNo.Text, temDueDateFrom, temDueDateto, InvType, InvStatus);
            this.ddbCustomerAcc.DataTextField = "custid";
            this.ddbCustomerAcc.DataValueField = "custid";
            this.ddbCustomerAcc.DataSource = dataset.Tables[0];
            this.ddbCustomerAcc.DataBind();

            int test = dataset.Tables[0].Rows.Count;

            //ListItem defItem = new ListItem();

            //ddbCustomerAcc.Items.Insert(0,(defItem));
        }


        public void LoadInvoiceType()
        {
            DataTable dtInvType = new DataTable();
            dtInvType.Columns.Add(new DataColumn("Text", typeof(string)));
            dtInvType.Columns.Add(new DataColumn("StringValue", typeof(string)));

            DataRow drFirstRow = dtInvType.NewRow();
            dtInvType.Rows.Add(drFirstRow);

            ArrayList listMonthTxt = Utility.GetCodeValues(appID, m_strCulture, "payment_mode", CodeValueType.StringValue);
            foreach (SystemCode typeSysCode in listMonthTxt)
            {
                DataRow drEach = dtInvType.NewRow();
                drEach[0] = typeSysCode.Text;
                drEach[1] = typeSysCode.StringValue;
                dtInvType.Rows.Add(drEach);
            }
            DataView dvInvType = new DataView(dtInvType);

            ddlInvType.DataSource = dvInvType;
            ddlInvType.DataTextField = "Text";
            ddlInvType.DataValueField = "StringValue";
            ddlInvType.DataBind();
        }


        public void LoadStatus()
        {
            DataTable dtInvStatus = new DataTable();
            dtInvStatus.Columns.Add(new DataColumn("Text", typeof(string)));
            dtInvStatus.Columns.Add(new DataColumn("StringValue", typeof(string)));

            DataRow drNilRow = dtInvStatus.NewRow();
            drNilRow[0] = "";
            drNilRow[1] = "";
            dtInvStatus.Rows.Add(drNilRow);

            ArrayList listMonthTxt = Utility.GetCodeValuesForInvoiceStatus(appID, m_strCulture, "invoice_status", CodeValueType.StringValue);
            foreach (SystemCode typeSysCode in listMonthTxt)
            {
                DataRow drEach = dtInvStatus.NewRow();
                drEach[0] = typeSysCode.Text;
                drEach[1] = typeSysCode.StringValue;
                dtInvStatus.Rows.Add(drEach);
            }
            DataView dvInvStatus = new DataView(dtInvStatus);

            ddlInvStatus.DataSource = dvInvStatus;
            ddlInvStatus.DataTextField = "Text";
            ddlInvStatus.DataValueField = "StringValue";
            ddlInvStatus.DataBind();
            ddlInvStatus.Items.Remove("");
            //			ddlInvStatus.Items.FindByText("NOT YET APPROVED").Selected = true;
            ddlInvStatus.Items.FindByValue("N").Selected = true;
        }


        #endregion

        #region Event in Control

        private void ddbCustomerAcc_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            Customer customer = new Customer();
            customer.Populate(appID, enterpriseID, ddbCustomerAcc.SelectedItem.Text);
            txtCustName.Text = customer.CustomerName;
        }

        private void btnGenerate_Click(object sender, System.EventArgs e)
        {
            lblErrorMessage.Text = "";
            if (Session["InvoiceForImport"] == null)
            {
                bool icheck = ValidateValues();
                if (icheck == false)
                {
                    DataSet dsInvoice = GetFilterData();
                    ViewState["dsQueryInvoice"] = dsInvoice;
                    BindGridPriview();
                    ActiveDIVGrid();
                    btnCancelSelected.Enabled = false;
                    btnExport.Enabled = false;

                    if (ddlInvStatus.SelectedItem.Value == "A")
                        btnExport.Enabled = false;
                }

            }
            else
            {
                BindGridPreviewForImport();
                ActiveDIVGrid();
                ViewState["InvoiceImport"] = (DataTable)Session["InvoiceForImport"];
                DataSet dsInvoice = GetFilterData();
                ViewState["PreviewGrid"] = dsInvoice;
                Session["InvoiceForImport"] = null;
            }
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            this.lblErrorMsg.Text = "";
            ClearGrid();
            ClearSearchControl();
            ActiveDIVSearch();
            PanelInvoiceCancel.Visible = false;
            dgPreview.Enabled = true;
            btnCancel.Enabled = true;
            btnSelectAll.Enabled = true;
        }
        private void btnClear_Click(object sender, System.EventArgs e)
        {
            this.lblErrorMsg.Text = "";
            lblErrorMessage.Text = "";
            ClearSearchControl();
            LoadCustomerAccount();
        }
        private void btnSelectAll_Click(object sender, System.EventArgs e)
        {
            this.lblErrorMsg.Text = "";

            // A: Approved, C: Cancelled, N: Not yet approved.
            if (ddlInvStatus.SelectedItem.Value == "A")
            {
                btnCancelSelected.Enabled = true;
                btnExport.Enabled = false;
                CheckAll(0);
            }
            else if (ddlInvStatus.SelectedItem.Value != "C")
            {
                btnCancelSelected.Enabled = true;
                btnExport.Enabled = true;
                // Limit maximum checked as 500
                CheckAll(500);
            }
        }

        private void btnApproveSelected_Click(object sender, System.EventArgs e)
        {
            this.lblErrorMsg.Text = "";
            utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings, Page.Session);

            double dblAdj = 0;

            DataTable dtSelect = new DataTable();
            dtSelect.Columns.Add(new DataColumn("invoice_no", typeof(string)));
            dtSelect.Columns.Add(new DataColumn("approved_by", typeof(string)));
            dtSelect.Columns.Add(new DataColumn("approved_date", typeof(string)));

            DataRow drSelect;

            for (int i = 0; i <= dgPreview.Items.Count - 1; i++)
            {
                CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");
                Label lblAdj = (Label)this.dgPreview.Items[i].FindControl("lblAdj");
                if ((lblAdj != null) && (!lblAdj.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblAdj.Text.Trim() != ""))
                {
                    dblAdj = Convert.ToDouble(utility.decodeNegValue(lblAdj.Text));
                }
                else
                {
                    dblAdj = Convert.ToDouble(0);
                }
                if (chkSelect.Checked)
                {
                    if (dblAdj != 0 && UserRole.Equals("ACCT"))
                    {
                        return;
                    }
                    else
                    {
                        drSelect = dtSelect.NewRow();
                        drSelect["invoice_no"] = dgPreview.Items[i].Cells[3].Text;
                        drSelect["approved_by"] = m_strUserID;
                        drSelect["approved_date"] = System.DateTime.Now.ToString();
                        dtSelect.Rows.Add(drSelect);
                    }
                }
            }

            DataSet dsSelect = new DataSet();
            dsSelect.Tables.Add(dtSelect);

            int getReturn;

            getReturn = InvoiceManagementMgrDAL.ApproveInvoice(appID, enterpriseID, dsSelect);

            if (getReturn > 0)
            {
                ClearGrid();
                BindGridPriview();
            }
        }
        private void btnCancelSelected_Click(object sender, System.EventArgs e)
        {
            //Test Transaction
            //CreditDebitMgrDAL.UpdateTestMain();return;

            //			Page.RegisterStartupScript("showcancel","<script>alert('BBBBBB');</script>");
            //			return;

            this.lblErrorMsg.Text = "";
            utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings, Page.Session);

            DataTable dtSelect = new DataTable();
            dtSelect.Columns.Add(new DataColumn("invoice_no", typeof(string)));
            dtSelect.Columns.Add(new DataColumn("cancelled_by", typeof(string)));
            dtSelect.Columns.Add(new DataColumn("cancel_date", typeof(string)));

            DataSet dsSelect = new DataSet();

            DataSet dsShipment = DomesticShipmentManager.GetEmptyDomesticShipDS();

            DataRow drSelect;
            bool hasCancelINV = false;

            for (int i = 0; i <= dgPreview.Items.Count - 1; i++)
            {
                CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");

                if (chkSelect.Checked)
                {
                    drSelect = dtSelect.NewRow();
                    drSelect["invoice_no"] = dgPreview.Items[i].Cells[3].Text;
                    drSelect["cancelled_by"] = m_strUserID;
                    drSelect["cancel_date"] = System.DateTime.Now.ToString();
                    dtSelect.Rows.Add(drSelect);
                    hasCancelINV = true;
                }

            }

            if (!hasCancelINV)
                return;

            dsSelect.Tables.Add(dtSelect);

            //			int getReturn=0;

            ViewState["dsSelect"] = dsSelect;
            ViewState["dsShipment"] = dsShipment;
            //BY X AUG 11 08
            CanCelRemark(); //show Area of Remark and CC + Reason given.
                            //END By X AUG 11 08
                            //int getReturn = InvoiceManagementMgrDAL.CancelInvoice(appID,enterpriseID,dsSelect,dsShipment);
                            //New Method 18/1/2010				


            //			if(getReturn > 0)
            //			{
            //				ClearGrid();
            //				BindGridPriview();
            //			}
        }

        private int CanCelRemark()
        {
            //this.PanelInvoiceCancel.Style["Top"] = this.dgPreview.Style["Height"];
            ViewState["btnAppriveSelected"] = this.btnApproveSelected.Enabled;
            ViewState["btnSelectAll"] = this.btnSelectAll.Enabled;
            ViewState["btnCancelSelected"] = this.btnCancelSelected.Enabled;
            ViewState["btnPrintHeader"] = this.btnPrintHeader.Enabled;
            ViewState["btnPrintDetail"] = this.btnPrintDetail.Enabled;

            this.btnCancel.Enabled = false;
            this.btnApproveSelected.Enabled = false;
            this.btnSelectAll.Enabled = false;
            this.btnCancelSelected.Enabled = false;
            this.btnPrintHeader.Enabled = false;
            this.btnPrintDetail.Enabled = false;

            //keep dg preview checking status
            DataTable dtInvChecked = new DataTable();
            dtInvChecked.Columns.Add("invoice_no");
            for (int i = 0; i < this.dgPreview.Items.Count; i++)
            {
                ImageButton ib = (ImageButton)this.dgPreview.Items[i].FindControl("imgSelect");
                ib.Visible = false;
                CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");
                if (chkSelect != null && chkSelect.Checked == true)
                {
                    DataRow drNew = dtInvChecked.NewRow();
                    drNew["invoice_no"] = dgPreview.Items[i].Cells[3].Text;
                    dtInvChecked.Rows.Add(drNew);
                }
            }

            ViewState["dtInvChecked"] = dtInvChecked;

            this.dgPreview.Enabled = false;
            this.PanelInvoiceCancel.Visible = true; ManageShowPopup(true);
            ArrayList systemCodes = Utility.GetCodeValues(this.appID.ToString(), utility.GetUserCulture(), "inv_cancel_code", CodeValueType.StringValue);
            //GetCodeValues

            //			ListItem defItem = new ListItem();
            //			defItem.Text = "";
            //			defItem.Value = "0";
            //			ddbUserType.Items.Add(defItem);
            this.ddlInvoiceCancelType.Items.Clear();
            foreach (SystemCode sysCode in systemCodes)
            {
                ListItem lstItem = new ListItem();
                lstItem.Text = sysCode.StringValue;//Text;
                lstItem.Value = sysCode.StringValue;
                this.ddlInvoiceCancelType.Items.Add(lstItem);
            }

            ArrayList systemCodes2 = Utility.GetCodeValues(this.appID.ToString(), utility.GetUserCulture(), "inv_cancel_reason", CodeValueType.StringValue);
            this.ddlInvoiceCancelReason.Items.Clear();
            foreach (SystemCode sysCode in systemCodes2)
            {
                if (sysCode.StringValue.Substring(0, 2).ToString() == "CC")
                {
                    ListItem lstItem = new ListItem();
                    lstItem.Text = sysCode.StringValue; //Text;
                    lstItem.Value = sysCode.StringValue;
                    this.ddlInvoiceCancelReason.Items.Add(lstItem);
                }
            }


            return 1;
        }

        private void btnPrintHeader_Click(object sender, System.EventArgs e)
        {
            //by sittichai 27/03/2008
            lblErrorMessage.Text = "";
            bool icheck = ValidateValues();

            bool foundChecked = false;
            for (int i = 0; i <= dgPreview.Items.Count - 1; i++)
            {
                CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");

                if (chkSelect.Checked)
                {
                    foundChecked = true;
                    break;
                }
            }

            if (foundChecked == false)
            {
                //lblErr.txt = ""
                return;
            }

            if (icheck == false)
            {
                String strModuleID = Request.Params["MODID"];
                DataSet dsInvoiceRP = GetInvoiceQueryData();

                //				//Added by GwanG on 21May08
                //				for(int i=0;i <= dsInvoiceRP.Tables[0].Rows.Count - 1; i++)
                //				{
                //					int irow = InvoiceManagementMgrDAL.UpdateInvsumFreight(appID,enterpriseID,dsInvoiceRP.Tables[0].Rows[i]["invoice_no"].ToString());
                //				}

                String strUrl = null;
                strUrl = "ReportViewer.aspx";
                Session["FORMID"] = "InvoiceHeaderReportQuery";
                Session["SESSION_DS1"] = dsInvoiceRP;
                ArrayList paramList = new ArrayList();
                paramList.Add(strUrl);
                String sScript = Utility.GetScript("openParentWindow.js", paramList);
                Utility.RegisterScriptString(sScript, "ShowPopupScript", this.Page);
            }

        }

        private void btnPrintDetail_Click(object sender, System.EventArgs e)
        {
            //by sittichai 27/03/2008
            lblErrorMessage.Text = "";
            bool icheck = ValidateValues();

            bool foundChecked = false;
            for (int i = 0; i <= dgPreview.Items.Count - 1; i++)
            {
                CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");

                if (chkSelect.Checked)
                {
                    foundChecked = true;
                    break;
                }
            }

            if (foundChecked == false)
            {
                //lblErr.txt = ""
                return;
            }

            if (icheck == false)
            {
                String strModuleID = Request.Params["MODID"];
                DataSet dsInvoiceRP = GetInvoiceQueryData();

                String strUrl = null;
                strUrl = "ReportViewer.aspx";
                Session["FORMID"] = "InvoiceDetailReportQuery";
                Session["SESSION_DS1"] = dsInvoiceRP;
                ArrayList paramList = new ArrayList();
                paramList.Add(strUrl);
                String sScript = Utility.GetScript("openParentWindow.js", paramList);
                Utility.RegisterScriptString(sScript, "ShowPopupScript", this.Page);
            }
        }

        private void btnCancelDtl_Click(object sender, System.EventArgs e)
        {


            BindGridPriview();
            this.ActiveDIVGrid();

            foreach (DataGridItem dgItem in dgPreview.Items)
            {
                ImageButton btSelect = (ImageButton)dgItem.Cells[2].Controls[1];

                if (btSelect != null)
                {
                    //btSelect
                }
            }

        }
        private void dgPreview_ItemCommand(object sender, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                strInvNo = e.Item.Cells[3].Text;
                ViewState["strInvNo"] = strInvNo;
                BindingHeader();
                BindingDetail();
                this.dgEditDtl.Visible = false;
                this.lblErrorMsgDtl.Text = "";

                ActiveDIVInvDetail();
            }
        }

        //		protected void chkSelect_onChecked(object obj,EventArgs e)
        //		{
        //			CheckBox getCheck = (CheckBox)dgItem.Cells[0].Controls[1];
        //		}

        protected void getCheck_onChecked(object obj, EventArgs e)
        {
            i = 0;
            sum = 0;
            boolCheck = false;

            int countCheck = 0;
            foreach (DataGridItem dgItem in this.dgPreview.Items)
            {
                CheckBox getCheck = (CheckBox)dgItem.Cells[0].Controls[1];
                if (getCheck.Checked == true)
                    countCheck++;

                if (countCheck > 500) { CountSeletct = 500; break; }
            }
            if (countCheck < 500)
            {
                foreach (DataGridItem dgItem in this.dgPreview.Items)
                {
                    CheckBox getCheck = (CheckBox)dgItem.Cells[0].Controls[1];
                    getCheck.Enabled = true;
                }
                CountSeletct = countCheck;
            }
            else
            {
                foreach (DataGridItem dgItem in this.dgPreview.Items)
                {
                    CheckBox getCheck = (CheckBox)dgItem.Cells[0].Controls[1];
                    if (getCheck.Checked)
                        getCheck.Enabled = true;
                    else
                        getCheck.Enabled = false;
                }
            }

            if (this.ddlInvStatus.SelectedItem.Value == "N")
            {
                int countChecked = 0;
                foreach (DataGridItem dgItem in this.dgPreview.Items)
                {
                    CheckBox getCheck = (CheckBox)dgItem.Cells[0].Controls[1];

                    if (getCheck.Checked == true)
                    {
                        //						if(countChecked + 1 > 500)
                        //						{
                        //							getCheck.Checked = false;
                        //							//return;
                        //						}
                        countChecked++;
                        //CountSeletct++;
                        boolCheck = true;
                        sum = 1;
                        break;

                        Label lblAdj = (Label)dgItem.FindControl("lblAdj");
                        if ((lblAdj != null) && (!lblAdj.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblAdj.Text.Trim() != ""))
                        {
                            i = Convert.ToDouble(utility.decodeNegValue(lblAdj.Text));
                        }
                        else
                        {
                            i = Convert.ToDouble(0);
                        }
                        sum += i;
                    }
                    //					else
                    //						CountSeletct--;
                }
                //lblErrorMsg.Text = countChecked.ToString();
                lblErrorMsg.Text = CountSeletct.ToString() + " Selected";

                if (sum != 0)
                {
                    if (UserRole.Equals("ACCT"))
                    {
                        this.btnApproveSelected.Enabled = false;
                        this.btnCancelSelected.Enabled = false;
                    }
                    else if (UserRole.Equals("ACCTMGR"))
                    {
                        this.btnApproveSelected.Enabled = true;
                        this.btnCancelSelected.Enabled = true;
                    }

                }
                else if (sum == 0 && boolCheck == true)
                {

                    if (UserRole.Equals("ACCTMGR"))
                    {
                        this.btnApproveSelected.Enabled = true;
                        this.btnCancelSelected.Enabled = true;
                    }
                    else if (UserRole.Equals("ACCT"))
                    {
                        this.btnApproveSelected.Enabled = true;
                        this.btnCancelSelected.Enabled = false;
                    }
                }
                else if (sum == 0 && boolCheck == false)
                {
                    this.btnApproveSelected.Enabled = false;
                    this.btnCancelSelected.Enabled = false;
                }


            }
            else if (this.ddlInvStatus.SelectedItem.Value == "A" || this.ddlInvStatus.SelectedItem.Value == "L")
            {
                foreach (DataGridItem dgItem in this.dgPreview.Items)
                {
                    CheckBox getCheck = (CheckBox)dgItem.Cells[0].Controls[1];

                    if (getCheck.Checked == true)
                    {
                        string strInvoiceNo = dgItem.Cells[3].Text;
                        if (this.UserRole.Equals("ACCTMGR"))
                        {
                            this.btnApproveSelected.Enabled = false;
                            //Check if it PLACED "L" and no partially paid + No CN/DN attatched.
                            /*
							if(InvoiceManagementMgrDAL.IsPlacedInvoiceCanCancelled(this.appID,this.enterpriseID,strInvoiceNo))
								this.btnCancelSelected.Enabled = true;
							else
								this.btnCancelSelected.Enabled = false;
							*/
                        }
                        else
                        {
                            this.btnApproveSelected.Enabled = false;
                            btnCancelSelected.Enabled = true;
                            //this.btnCancelSelected.Enabled = false;  Test By Tung
                        }
                        return;
                    }
                    else
                    {
                        this.btnApproveSelected.Enabled = false;
                        //this.btnCancelSelected.Enabled = false;
                    }
                }
            }
            else
            {
                this.btnApproveSelected.Enabled = false;
                this.btnCancelSelected.Enabled = false;
            }
            if (boolCheck)
            {
                btnCancelSelected.Enabled = true;
                btnExport.Enabled = true;
            }
            else
            {
                btnCancelSelected.Enabled = false;
                btnExport.Enabled = false;
            }
            //lblErrorMessage.Text = "Test";
            //if(
            //CountSeletct
        }

        private void dgPreview_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            //SetRowsColor(dgPreview);

            //			string strCode = e.Item.Cells[11].Text;
            //					
            //			string strVal = InvoiceManagementMgrDAL.GetValueByCodeText(appID,m_strCulture,"invoice_status",strCode,CodeValueType.StringValue);
            //			if(strVal == "A")
            //			{
            //				e.Item.BackColor = Color.FromName("#9ACC00");
            //			}
            //			if(strVal == "C")
            //			{
            //				e.Item.BackColor = Color.FromName("#FF9A00");
            //			}
            //			if(strVal == "N")
            //			{
            //				e.Item.BackColor = Color.FromName("#FFFF9A");
            //			}
            //			if(strVal == "L")
            //			{
            //				e.Item.BackColor = Color.FromName("#9ACC00");
            //			}
            //			if(strVal == "D")
            //			{
            //				e.Item.BackColor = Color.FromName("#9ACC00");
            //			}

        }


        #endregion

        #region InvoiceDetail Method

        private void BindingHeader()
        {
            DateTime dtFrom = DateTime.MinValue;
            DateTime dtTo = DateTime.MaxValue;
            strInvNo = (String)ViewState["strInvNo"];
            DataSet dsInv = InvoiceManagementMgrDAL.GetInvoiceByInvNo(appID, enterpriseID, strInvNo);//GetInvoice(appID,enterpriseID,dtFrom,dtTo,strInvNo,dtFrom,dtTo,null,null,null);
            Session["SESSION_DSINV"] = dsInv;

            if (dsInv.Tables[0].Rows.Count > 0)
            {
                DataRow drEach = dsInv.Tables[0].Rows[0];
                if ((drEach["payerid"] != null) && (drEach["payerid"].ToString() != ""))
                {
                    if (drEach["payerid"].ToString().Trim() == "N/A")
                    {
                        this.lblCustIDDtl.Text = "CASH";
                    }
                    else
                    {
                        this.lblCustIDDtl.Text = drEach["payerid"].ToString();
                    }
                }

                if ((drEach["payer_name"] != null) && (drEach["payer_name"].ToString() != ""))
                {
                    this.lblCustNameDtl.Text = drEach["payer_name"].ToString();
                }

                if ((drEach["payer_address1"] != null) && (drEach["payer_address1"].ToString() != ""))
                {
                    this.lblAddress1Dtl.Text = drEach["payer_address1"].ToString();
                }

                if ((drEach["payer_address2"] != null) && (drEach["payer_address2"].ToString() != ""))
                {
                    this.lblAddress2Dtl.Text = drEach["payer_address2"].ToString();
                }

                if ((drEach["payer_zipcode"] != null) && (drEach["payer_zipcode"].ToString() != ""))
                {
                    this.lblPostalCodeDtl.Text = drEach["payer_zipcode"].ToString();
                }

                if ((drEach["state_code"] != null) && (drEach["state_code"].ToString() != ""))
                {
                    this.lblProvinceDtl.Text = drEach["state_code"].ToString();
                }

                if ((drEach["payer_telephone"] != null) && (drEach["payer_telephone"].ToString() != ""))
                {
                    this.lblTelephoneDtl.Text = drEach["payer_telephone"].ToString();
                }

                if ((drEach["payer_fax"] != null) && (drEach["payer_fax"].ToString() != ""))
                {
                    this.lblFaxDtl.Text = drEach["payer_fax"].ToString();
                }

                if ((drEach["contact_person"] != null) && (drEach["contact_person"].ToString() != ""))
                {
                    this.lblContactDtl.Text = drEach["contact_person"].ToString();
                }

                if ((drEach["pod_slip_required"] != null) && (drEach["pod_slip_required"].ToString() != ""))
                {
                    String podStatus = drEach["pod_slip_required"].ToString();
                    this.lblHC_POD_RequiredDtl.Text = InvoiceManagementMgrDAL.GetCodeTextByValue(appID, m_strCulture, "pod_slip_required", podStatus, CodeValueType.StringValue);
                }

                if ((drEach["invoice_no"] != null) && (drEach["invoice_no"].ToString() != ""))
                {
                    this.lblInvNoDtl.Text = drEach["invoice_no"].ToString();
                }

                if ((drEach["invoice_status"] != null) && (drEach["invoice_status"].ToString() != ""))
                {
                    String invStatus = drEach["invoice_status"].ToString();
                    ViewState["INVSTATE"] = invStatus;
                    this.lblInvStatusDtl.Text = InvoiceManagementMgrDAL.GetCodeTextByValue(appID, m_strCulture, "invoice_status", invStatus, CodeValueType.StringValue);
                    switch (invStatus)
                    {
                        case "A":
                            this.lblCancel_AppHeadDtl.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Approve Date", m_strCulture);
                            if (!drEach["approved_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
                            {
                                DateTime dtApprove = (DateTime)drEach["approved_date"];
                                this.lblCancel_AppTextDtl.Text = dtApprove.ToString("dd/MM/yyyy ");
                            }

                            break;

                        case "C":
                            this.lblCancel_AppHeadDtl.Text = Utility.GetLanguageText(ResourceType.ScreenLabel, "Cancel Date", m_strCulture);
                            if (!drEach["cancel_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
                            {
                                DateTime dtCancel = (DateTime)drEach["cancel_date"];
                                this.lblCancel_AppTextDtl.Text = dtCancel.ToString("dd/MM/yyyy ");
                            }
                            break;
                    }
                }

                if ((drEach["payment_mode"] != null) && (drEach["payment_mode"].ToString() != ""))
                {
                    String strPaymentMode = drEach["payment_mode"].ToString();
                    switch (strPaymentMode)
                    {
                        case "C"://Cash
                            this.lblPaymentTermDtl.Text = "NOT APPLICABLE";
                            break;
                        case "R"://Creadit
                            if ((drEach["credit_term"] != null) && drEach["credit_term"].ToString() != "")
                            {
                                int creditTerm = Convert.ToInt16(drEach["credit_term"]);
                                if (creditTerm == 0)
                                {
                                    this.lblPaymentTermDtl.Text = "IMMEDIATE";
                                }
                                else if (creditTerm > 0)
                                {
                                    this.lblPaymentTermDtl.Text = "NET " + creditTerm.ToString() + " DAYS";
                                }
                                else
                                {
                                    this.lblPaymentTermDtl.Text = "";
                                }

                            }
                            else
                            {
                                this.lblPaymentTermDtl.Text = "NONE SPECIFIED";
                            }

                            break;
                    }

                }

                if ((drEach["invoice_date"] != null) && (drEach["invoice_date"].ToString() != ""))
                {
                    DateTime dtInvoice = (DateTime)drEach["invoice_date"];
                    this.lblInvDateDtl.Text = dtInvoice.ToString("dd/MM/yyyy ");
                }

                if ((drEach["due_date"] != null) && (drEach["due_date"].ToString() != ""))
                {
                    DateTime dtDueDate = (DateTime)drEach["due_date"];
                    this.lblDueDateDtl.Text = dtDueDate.ToString("dd/MM/yyyy ");
                }

                if ((drEach["invoice_amt"] != null) && (drEach["invoice_amt"].ToString() != ""))
                {
                    this.lblTotalAmtDtl.Text = utility.encodeNegValue(drEach["invoice_amt"].ToString(), (String)ViewState["m_format"]);
                }

                if ((drEach["invoice_adj_amount"] != null) && (drEach["invoice_adj_amount"].ToString() != ""))
                {
                    this.lblTotalAdjDtl.Text = utility.encodeNegValue(drEach["invoice_adj_amount"].ToString(), (String)ViewState["m_format"]);
                }

            }
        }


        private void BindingDetail()
        {
            strInvNo = (String)ViewState["strInvNo"];
            DataSet dsInvDtl = InvoiceManagementMgrDAL.GetInvoiceDetail(appID, enterpriseID, strInvNo);
            Session["SESSION_DSINVDTL"] = dsInvDtl;
            dgPreviewDtl.DataSource = dsInvDtl;
            dgPreviewDtl.DataBind();
        }


        public void dgPreviewDtl_dgEditDtl(object sender, DataGridCommandEventArgs e)
        {
            //gridFieldSelected
            int col = dgPreviewDtl.Columns.Count;
            for (int i = 0; i < col; i++)
            {
                e.Item.CssClass = "gridFieldSelectedINV";
                e.Item.Cells[i].BackColor = Color.FromName("#ffcc00");
                //e.Item.Style.Add("background-color","#ffcc00");
            }

            //Consgn_No
            if (!e.Item.Cells[0].Text.GetType().Equals(System.Type.GetType("System.DBNull")) && e.Item.Cells[0].Text.Trim() != "" && e.Item.Cells[0].Text.Trim() != "&nbsp;")
            {
                ViewState["strConsgnNo"] = e.Item.Cells[1].Text;
            }
            //insurance_surcharge,other_surch_amount,tot_vas_surcharge,esa_surcharge,total_exception,invoice_adj_amount,other_surcharge,mbg_amount,invoice_amt

            Label lblFreight = (Label)e.Item.FindControl("lblFreight");
            if ((lblFreight != null) && (!lblFreight.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblFreight.Text.Trim() != ""))
            {
                ViewState["temp_tot_freight"] = Convert.ToDecimal(lblFreight.Text);
            }
            else
            {
                ViewState["temp_tot_freight"] = Convert.ToDecimal(0);
            }

            //insruance_amt
            Label lblInsurance = (Label)e.Item.FindControl("lblInsurance");
            if ((lblInsurance != null) && (!lblInsurance.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblInsurance.Text.Trim() != ""))
            {
                ViewState["temp_insurance_amt"] = Convert.ToDecimal(lblInsurance.Text);
            }
            else
            {
                ViewState["temp_insurance_amt"] = Convert.ToDecimal(0);
            }

            //tot_vas_surcharge
            Label lblVAS = (Label)e.Item.FindControl("lblVAS");
            if ((lblVAS != null) && (!lblVAS.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblVAS.Text.Trim() != ""))
            {
                ViewState["temp_tot_vas_surcharge"] = Convert.ToDecimal(lblVAS.Text);
            }
            else
            {
                ViewState["temp_tot_vas_surcharge"] = Convert.ToDecimal(0);
            }

            //esa_surcharge
            Label lblESA = (Label)e.Item.FindControl("lblESA");
            if ((lblESA != null) && (!lblESA.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblESA.Text.Trim() != ""))
            {

                ViewState["temp_esa_surcharge"] = Convert.ToDecimal(lblESA.Text);
            }
            else
            {
                ViewState["temp_esa_surcharge"] = Convert.ToDecimal(0);
            }

            //total_exception
            Label lblException = (Label)e.Item.FindControl("lblException");
            if ((lblException != null) && (!lblException.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblException.Text.Trim() != ""))
            {
                ViewState["temp_total_exception"] = Convert.ToDecimal(utility.decodeNegValue(lblException.Text));
            }
            else
            {
                ViewState["temp_total_exception"] = Convert.ToDecimal(0);
            }

            //invoice_adj_amount
            Label lblAdjustment = (Label)e.Item.FindControl("lblAdjustment");
            if ((lblAdjustment != null) && (!lblAdjustment.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblAdjustment.Text.Trim() != ""))
            {
                ViewState["temp_old_invoice_adj"] = Convert.ToDecimal(utility.decodeNegValue(lblAdjustment.Text));
                ViewState["InvAdjExist"] = true;
            }
            else
            {
                ViewState["temp_old_invoice_adj"] = Convert.ToDecimal(0);
                ViewState["InvAdjExist"] = false;
            }

            //other_surcharge
            Label lblOther = (Label)e.Item.FindControl("lblOther");
            if ((lblOther != null) && (!lblOther.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblOther.Text.Trim() != ""))
            {
                ViewState["temp_other_surcharge"] = Convert.ToDecimal(utility.decodeNegValue(lblOther.Text));
            }
            else
            {
                ViewState["temp_other_surcharge"] = Convert.ToDecimal(0);
            }

            //mbg_amount
            Label lblMBG = (Label)e.Item.FindControl("lblMBG");
            if ((lblMBG != null) && (!lblMBG.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblMBG.Text.Trim() != ""))
            {
                ViewState["temp_mbg_amount"] = Convert.ToDecimal(utility.decodeNegValue(lblMBG.Text));
            }
            else
            {
                ViewState["temp_mbg_amount"] = Convert.ToDecimal(0);
            }

            //temp_old_total 
            Label lblTOT = (Label)e.Item.FindControl("lblTOT");
            if ((lblTOT != null) && (!lblTOT.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblTOT.Text.Trim() != ""))
            {
                ViewState["temp_old_total"] = Convert.ToDecimal(lblTOT.Text);
            }
            else
            {
                ViewState["temp_old_total"] = Convert.ToDecimal(0);
            }

            if (!e.Item.Cells[19].Text.GetType().Equals(System.Type.GetType("System.DBNull")) && e.Item.Cells[19].Text.Trim() != "" && e.Item.Cells[19].Text.Trim() != "&nbsp;")
            {
                ViewState["temp_adjusted_remark"] = e.Item.Cells[19].Text;
            }
            else
            {
                ViewState["temp_adjusted_remark"] = "";
            }

            this.dgEditDtl.Visible = true;
            //dgEditDtl.DataBind();
            int iSelIndex = e.Item.ItemIndex;
            DataGridItem dgRow = dgPreviewDtl.Items[iSelIndex];
            ViewState["strConsgnNo"] = dgRow.Cells[1].Text;

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            DataRow dr = dt.NewRow();

            dt.Columns.Add(new DataColumn("consignment_no", typeof(string)));

            dr["consignment_no"] = ViewState["strConsgnNo"];
            dt.Rows.Add(dr);
            ds.Tables.Add(dt);

            dgEditDtl.DataSource = ds;
            dgEditDtl.DataBind();
        }


        public void dgEditDtl_dgUpdate(object sender, DataGridCommandEventArgs e)
        {
            decimal temp_new_invoice_adj = 0;
            decimal sumAmtInvDtl = 0;
            sumAmtInvDtl = SumAmountInvDtl();
            TextBox txtRemark = (TextBox)e.Item.FindControl("txtRemark");
            msTextBox txtAmount = (msTextBox)e.Item.FindControl("txtAmount");

            if (txtRemark.Text.Trim() == "" && txtAmount.Text.Trim() == "")
            {
                //UPDATE To Invoice_Detail Table
                ViewState["temp_new_total"] = sumAmtInvDtl;
                DataSet dsNewInvDtl = InvoiceManagementMgrDAL.GetEmptyInvoiceDetail();
                DataRow drRecShipment = dsNewInvDtl.Tables[0].Rows[0];
                drRecShipment["invoice_no"] = ViewState["strInvNo"];
                drRecShipment["consignment_no"] = ViewState["strConsgnNo"];
                drRecShipment["adjusted_by"] = m_strUserID;
                drRecShipment["adjusted_date"] = Utility.DateFormat(appID, enterpriseID, System.DateTime.Now, DTFormat.DateLongTime);
                drRecShipment["invoice_adj_amount"] = System.DBNull.Value;
                drRecShipment["invoice_amt"] = (decimal)ViewState["temp_new_total"];
                drRecShipment["adjusted_remark"] = strRemark;

                try
                {
                    int iRow = InvoiceManagementMgrDAL.AdjustInvoice(appID, enterpriseID, dsNewInvDtl);
                    lblErrorMsgDtl.Text = Utility.GetLanguageText(ResourceType.UserMessage, "INS_SUCCESSFULLY", m_strCulture);
                    BindingHeader();
                    BindingDetail();

                    dgEditDtl.Visible = false;

                }
                catch (ApplicationException appException)
                {
                    String strMsg = appException.Message;
                    lblErrorMsgDtl.Text = strMsg;
                    ViewState["ErrDtl"] = true;
                    this.dgEditDtl.Visible = true;
                    return;

                }
                return;
            }
            else if (txtAmount.Text.Trim() == "0")
            {
                //UPDATE To Invoice_Detail Table
                ViewState["temp_new_total"] = sumAmtInvDtl;
                DataSet dsNewInvDtl = InvoiceManagementMgrDAL.GetEmptyInvoiceDetail();
                DataRow drRecShipment = dsNewInvDtl.Tables[0].Rows[0];
                drRecShipment["invoice_no"] = ViewState["strInvNo"];
                drRecShipment["consignment_no"] = ViewState["strConsgnNo"];
                drRecShipment["adjusted_by"] = m_strUserID;
                drRecShipment["adjusted_date"] = Utility.DateFormat(appID, enterpriseID, System.DateTime.Now, DTFormat.DateLongTime);
                drRecShipment["invoice_adj_amount"] = 0;
                drRecShipment["invoice_amt"] = (decimal)ViewState["temp_new_total"];
                drRecShipment["adjusted_remark"] = strRemark;

                try
                {
                    int iRow = InvoiceManagementMgrDAL.AdjustInvoice(appID, enterpriseID, dsNewInvDtl);
                    lblErrorMsgDtl.Text = Utility.GetLanguageText(ResourceType.UserMessage, "INS_SUCCESSFULLY", m_strCulture);
                    BindingHeader();
                    BindingDetail();

                    dgEditDtl.Visible = false;

                }
                catch (ApplicationException appException)
                {
                    String strMsg = appException.Message;
                    lblErrorMsgDtl.Text = strMsg;
                    ViewState["ErrDtl"] = true;
                    this.dgEditDtl.Visible = true;
                    return;

                }
                return;
            }

            else
            {
                if (txtRemark.Text.Trim() == "" || txtRemark.Text == null)
                {
                    lblErrorMsgDtl.Text = Utility.GetLanguageText(ResourceType.UserMessage, "INVDTL_REMARK", m_strCulture);
                    ViewState["ErrDtl"] = true;
                    this.dgEditDtl.Visible = true;
                    return;
                }
                else
                {
                    strRemark = txtRemark.Text;
                }

                if ((txtAmount.Text.Trim() == "") || (txtAmount.Text == null))
                {
                    lblErrorMsgDtl.Text = Utility.GetLanguageText(ResourceType.UserMessage, "INVDTL_AMOUNT", m_strCulture);
                    ViewState["ErrDtl"] = true;
                    this.dgEditDtl.Visible = true;
                    return;
                }
                else
                {

                    DropDownList ddlTempApplyAs = (DropDownList)e.Item.FindControl("ddlApplyAs");
                    if (ddlTempApplyAs != null)
                    {
                        if (ddlTempApplyAs.SelectedItem.Value == "A")
                        {
                            temp_new_invoice_adj = Convert.ToDecimal(txtAmount.Text);
                        }

                        else if (ddlTempApplyAs.SelectedItem.Value == "P")
                        {
                            decimal tempval;
                            //temp_new_invoice_adj =TIESUtility.EnterpriseRounding(Convert.ToDecimal(drCurrent["cod_surcharge_amt"]),(int)ViewState["wt_rounding_method"],(decimal)ViewState["wt_increment_amt"]
                            tempval = (Convert.ToDecimal(txtAmount.Text) * sumAmtInvDtl) / 100;
                            temp_new_invoice_adj = TIESUtility.EnterpriseRounding(tempval, (int)ViewState["wt_rounding_method"], (decimal)ViewState["wt_increment_amt"]);
                        }
                    }

                    DropDownList ddlTempAdjType = (DropDownList)e.Item.FindControl("ddlAdjType");
                    if (ddlTempAdjType != null)
                    {
                        if (ddlTempAdjType.SelectedItem.Value == "C")
                        {
                            ViewState["temp_new_invoice_adj"] = -temp_new_invoice_adj;
                        }
                        else if (ddlTempAdjType.SelectedItem.Value == "D")
                        {
                            ViewState["temp_new_invoice_adj"] = Convert.ToDecimal(temp_new_invoice_adj);
                        }
                    }

                    if (ddlTempAdjType.SelectedItem.Value == "C")
                    {
                        //Check temp_new_invoice_adj have to less than or equal Amount
                        if (temp_new_invoice_adj > sumAmtInvDtl)
                        {
                            lblErrorMsgDtl.Text = Utility.GetLanguageText(ResourceType.UserMessage, "INVDTL_AMTLESSZERO", m_strCulture);
                            ViewState["ErrDtl"] = true;
                            this.dgEditDtl.Visible = true;
                            return;
                        }
                    }


                }


                //UPDATE To Invoice_Detail Table
                ViewState["temp_new_total"] = (decimal)ViewState["temp_new_invoice_adj"] + sumAmtInvDtl;
                DataSet dsNewInvDtl = InvoiceManagementMgrDAL.GetEmptyInvoiceDetail();
                DataRow drRecShipment = dsNewInvDtl.Tables[0].Rows[0];
                drRecShipment["invoice_no"] = ViewState["strInvNo"];
                drRecShipment["consignment_no"] = ViewState["strConsgnNo"];
                drRecShipment["adjusted_by"] = m_strUserID;
                drRecShipment["adjusted_date"] = Utility.DateFormat(appID, enterpriseID, System.DateTime.Now, DTFormat.DateLongTime);
                drRecShipment["invoice_adj_amount"] = (decimal)ViewState["temp_new_invoice_adj"];
                drRecShipment["invoice_amt"] = (decimal)ViewState["temp_new_total"];
                drRecShipment["adjusted_remark"] = strRemark;

                try
                {
                    int iRow = InvoiceManagementMgrDAL.AdjustInvoice(appID, enterpriseID, dsNewInvDtl);
                    lblErrorMsgDtl.Text = Utility.GetLanguageText(ResourceType.UserMessage, "INS_SUCCESSFULLY", m_strCulture);
                    BindingHeader();
                    BindingDetail();

                    dgEditDtl.Visible = false;
                }
                catch (ApplicationException appException)
                {
                    String strMsg = appException.Message;
                    lblErrorMsgDtl.Text = strMsg;
                    ViewState["ErrDtl"] = true;
                    this.dgEditDtl.Visible = true;
                    return;

                }
                return;

            }
        }


        public void dgEditDtl_dgBound(object sender, DataGridItemEventArgs e)
        {

            if (e.Item.ItemIndex == -1)
            {
                return;
            }


            Label consignment_no = (Label)e.Item.FindControl("lblConsgnNo");
            if (consignment_no != null)
            {
                consignment_no.Text = (String)ViewState["strConsgnNo"];
            }

            com.common.util.msTextBox txtAmount = (com.common.util.msTextBox)e.Item.FindControl("txtAmount");
            if ((bool)ViewState["InvAdjExist"])
            {
                txtAmount.Text = Convert.ToString((Math.Abs((decimal)ViewState["temp_old_invoice_adj"])));
            }


            TextBox txtRemark = (TextBox)e.Item.FindControl("txtRemark");
            if ((bool)ViewState["InvAdjExist"])
            {
                txtRemark.Text = ViewState["temp_adjusted_remark"].ToString();
            }

            DropDownList ddlAdjType = (DropDownList)e.Item.FindControl("ddlAdjType");
            if (ddlAdjType != null)
            {
                DataTable dtAdjType = new DataTable();
                dtAdjType.Columns.Add(new DataColumn("Text", typeof(string)));
                dtAdjType.Columns.Add(new DataColumn("StringValue", typeof(string)));

                ArrayList listAdjType = Utility.GetCodeValues(utility.GetAppID(), m_strCulture, "adjustment_type", CodeValueType.StringValue);
                foreach (SystemCode typeSysCode in listAdjType)
                {
                    DataRow drEach = dtAdjType.NewRow();
                    drEach[0] = typeSysCode.Text;
                    drEach[1] = typeSysCode.StringValue;
                    dtAdjType.Rows.Add(drEach);
                }
                DataView dvAdjType = new DataView(dtAdjType);

                ddlAdjType.DataSource = dvAdjType;
                ddlAdjType.DataTextField = "Text";
                ddlAdjType.DataValueField = "StringValue";
                ddlAdjType.DataBind();
                if ((bool)ViewState["InvAdjExist"])
                {
                    if ((decimal)ViewState["temp_old_invoice_adj"] < 0)
                    {
                        ddlAdjType.ClearSelection();
                        ddlAdjType.Items.FindByValue("C").Selected = true;
                    }
                    else
                    {
                        ddlAdjType.ClearSelection();
                        ddlAdjType.Items.FindByValue("D").Selected = true;
                    }
                }
                else
                {
                    ddlAdjType.Items.FindByValue("C").Selected = true;
                }

            }

            DropDownList ddlApplyAs = (DropDownList)e.Item.FindControl("ddlApplyAs");
            if (ddlApplyAs != null)
            {
                DataTable dtApplyAs = new DataTable();
                dtApplyAs.Columns.Add(new DataColumn("Text", typeof(string)));
                dtApplyAs.Columns.Add(new DataColumn("StringValue", typeof(string)));

                ArrayList listApplyAs = Utility.GetCodeValues(utility.GetAppID(), m_strCulture, "apply_as_type", CodeValueType.StringValue);
                foreach (SystemCode typeSysCode in listApplyAs)
                {
                    DataRow drEach = dtApplyAs.NewRow();
                    drEach[0] = typeSysCode.Text;
                    drEach[1] = typeSysCode.StringValue;
                    dtApplyAs.Rows.Add(drEach);
                }
                DataView dvApplyAs = new DataView(dtApplyAs);
                ddlApplyAs.DataSource = dvApplyAs;
                ddlApplyAs.DataTextField = "Text";
                ddlApplyAs.DataValueField = "StringValue";
                ddlApplyAs.DataBind();
                ddlApplyAs.ClearSelection();
                ddlApplyAs.Items.FindByValue("A").Selected = true;

            }

        }

        public void dgEditDtl_Button(object sender, DataGridCommandEventArgs e)
        {

        }


        public void dgPreviewDtl_Button(object sender, DataGridCommandEventArgs e)
        {

        }

        public void dgPreviewDtl_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            e.Item.Cells[7].Attributes.Add("width", "27px");
            e.Item.Cells[10].Attributes.Add("width", "38px");
            e.Item.Cells[11].Attributes.Add("width", "27px");

            if (e.Item.ItemIndex == -1)
            {
                return;
            }

            String invStatus = "";
            if (Utility.IsNotDBNull(ViewState["INVSTATE"]))
            {
                invStatus = (string)ViewState["INVSTATE"];
            }

            //			if(this.UserRole == "ACCT" || invStatus == "N" )
            //			{
            //				dgPreviewDtl.Columns[0].Visible = true;
            //			}
            //
            //			if(this.UserRole == "ACCTMGR" || invStatus != "N")
            //			{
            //				dgPreviewDtl.Columns[0].Visible = false;	
            //			}

            if (invStatus == "N")
            {
                dgPreviewDtl.Columns[0].Visible = true;
            }

            else if (invStatus != "N")
            {
                dgPreviewDtl.Columns[0].Visible = false;
            }


        }




        private decimal SumAmountInvDtl()
        {
            decimal tempSum = Convert.ToDecimal(0);
            tempSum += Convert.ToDecimal(ViewState["temp_tot_freight"]);
            tempSum += Convert.ToDecimal(ViewState["temp_insurance_amt"]);
            tempSum += Convert.ToDecimal(ViewState["temp_other_surcharge"]);
            tempSum += Convert.ToDecimal(ViewState["temp_esa_surcharge"]);
            tempSum += Convert.ToDecimal(ViewState["temp_total_exception"]);
            tempSum += Convert.ToDecimal(ViewState["temp_mbg_amount"]);
            tempSum += Convert.ToDecimal(ViewState["temp_tot_vas_surcharge"]);

            return tempSum;

        }







        #endregion

        #region Custom Method

        private void movePage()
        {
            Response.Redirect("InvManageList.aspx?PARENTMODID=07Account&MODID=13InvManageList");
        }

        private void BindCustListToTextName()
        {
            if (this.dbCmbAgentId.Text.Trim() != "" && this.dbCmbAgentId.Text.Trim() != "CASH")
            {
                DataSet dsCustData = CustomerScheduledPickupsDAL.GetCustData(utility.GetAppID(), utility.GetEnterpriseID(),
                    this.dbCmbAgentId.Text.Trim());
                if (dsCustData.Tables[0].Rows.Count > 0)
                {
                    this.txtCustName.Text = dsCustData.Tables[0].Rows[0]["cust_name"].ToString();
                }
                else
                    this.txtCustName.Text = "";
            }
            else
            {
                this.txtCustName.Text = "";
            }
        }

        private DataSet GetFilterData()
        {
            DataTable dtInvoiceDetail = new DataTable();
            dtInvoiceDetail.Columns.Add(new DataColumn("invoice_no", typeof(string)));
            dtInvoiceDetail.Columns.Add(new DataColumn("invdate_from", typeof(DateTime)));
            dtInvoiceDetail.Columns.Add(new DataColumn("invdate_to", typeof(DateTime)));
            dtInvoiceDetail.Columns.Add(new DataColumn("duedate_from", typeof(DateTime)));
            dtInvoiceDetail.Columns.Add(new DataColumn("duedate_to", typeof(DateTime)));
            dtInvoiceDetail.Columns.Add(new DataColumn("invoice_type", typeof(string)));
            dtInvoiceDetail.Columns.Add(new DataColumn("customer_id", typeof(string)));
            dtInvoiceDetail.Columns.Add(new DataColumn("customer_name", typeof(string)));
            dtInvoiceDetail.Columns.Add(new DataColumn("status", typeof(string)));

            DataRow drEach = dtInvoiceDetail.NewRow();

            if (txtInvDateFrom.Text != "" && txtInvDateto.Text != "")
            {
                drEach["invdate_from"] = DateTime.ParseExact(txtInvDateFrom.Text + " 00:00:01", "dd/MM/yyyy HH:mm:ss", null);
                drEach["invdate_to"] = DateTime.ParseExact(txtInvDateto.Text + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
            }
            else if (txtInvDateFrom.Text != "" && txtInvDateto.Text == "")
            {
                drEach["invdate_from"] = DateTime.ParseExact(txtInvDateFrom.Text + " 00:00:01", "dd/MM/yyyy HH:mm:ss", null);
                //drEach["invdate_to"] = DateTime.ParseExact(txtInvDateFrom.Text+" 23:00:00","dd/MM/yyyy HH:mm:ss",null);
                drEach["invdate_to"] = DateTime.Now;
            }
            else if (txtInvDateFrom.Text == "" && txtInvDateto.Text != "")
            {
                drEach["invdate_from"] = DateTime.MinValue;
                drEach["invdate_to"] = DateTime.ParseExact(txtInvDateto.Text + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
            }
            else
            {
                drEach["invdate_from"] = System.DBNull.Value;
                drEach["invdate_to"] = System.DBNull.Value;
            }

            if (txtDueDateFrom.Text != "" && txtDueDateto.Text != "")
            {
                drEach["duedate_from"] = DateTime.ParseExact(txtDueDateFrom.Text + " 00:00:01", "dd/MM/yyyy HH:mm:ss", null);
                drEach["duedate_to"] = DateTime.ParseExact(txtDueDateto.Text + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);

            }
            else if (txtDueDateFrom.Text != "" && txtDueDateto.Text == "")
            {
                drEach["duedate_from"] = DateTime.ParseExact(txtDueDateFrom.Text + " 00:00:01", "dd/MM/yyyy HH:mm:ss", null);
                drEach["duedate_to"] = DateTime.ParseExact(txtDueDateFrom.Text + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
            }
            else if (txtDueDateFrom.Text == "" && txtDueDateto.Text != "")
            {
                drEach["duedate_from"] = DateTime.MinValue;
                drEach["duedate_to"] = DateTime.ParseExact(txtDueDateto.Text + " 23:59:59", "dd/MM/yyyy HH:mm:ss", null);
            }
            else
            {
                drEach["duedate_from"] = System.DBNull.Value;
                drEach["duedate_to"] = System.DBNull.Value;
            }

            string strInvType = "";
            strInvType = ddlInvType.SelectedItem.Value;
            if (strInvType != "")
            {
                drEach["invoice_type"] = strInvType;

            }
            else
            {
                drEach["invoice_type"] = System.DBNull.Value;
            }

            string strCustomerID = "";
            //			strCustomerID = ddbCustomerAcc.SelectedItem.Value;
            //			strCustomerID = dbCmbAgentId.Value;
            strCustomerID = ddbCustomerAcc.SelectedValue;
            if (strCustomerID != "")
            {
                drEach["customer_id"] = strCustomerID;
            }
            else
            {
                drEach["customer_id"] = System.DBNull.Value;
            }

            string strCustomerName = "";
            strCustomerName = this.txtCustName.Text;
            if (strCustomerName != "")
            {
                drEach["customer_name"] = strCustomerName;
            }
            else
            {
                drEach["customer_name"] = System.DBNull.Value;
            }

            string strStatus = "";
            strStatus = ddlInvStatus.SelectedItem.Value;
            //			if (lblInvDispStat.Text!="" && lblInvDispStat.Text!="Status" && ddlInvStatus.Items.FindByText(lblInvDispStat.Text).Value != null )
            //			{
            //				
            //				strStatus = ddlInvStatus.Items.FindByText(lblInvDispStat.Text).Value ;
            //			}
            //			else
            if (strStatus != "")
            {
                drEach["status"] = strStatus;
            }
            else
            {
                drEach["status"] = System.DBNull.Value;
            }

            if (txtInvoiceNo.Text != "")
            {
                drEach["invoice_no"] = txtInvoiceNo.Text.Trim();
            }
            //			else if (txtInvNo.Text!="")
            //			{
            //				drEach["invoice_no"] = txtInvNo.Text.Trim();
            //			}
            else
            {
                drEach["invoice_no"] = System.DBNull.Value;

            }

            dtInvoiceDetail.Rows.Add(drEach);
            DataSet dsInvoiceDetail = new DataSet();
            dtInvoiceDetail.TableName = "InvPreview";
            dsInvoiceDetail.Tables.Add(dtInvoiceDetail);
            return dsInvoiceDetail;

        }

        private bool ValidateValues()
        {
            bool iCheck = false;
            if ((txtInvDateFrom.Text != "") && (txtInvDateto.Text != ""))
            {
                if (DateTime.ParseExact(txtInvDateFrom.Text, "dd/MM/yyyy", null) > DateTime.ParseExact(txtInvDateto.Text, "dd/MM/yyyy", null))
                {
                    lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "DT_FROM_GT_TO", utility.GetUserCulture());
                    iCheck = true;
                }
            }


            if ((txtDueDateFrom.Text != "") && (txtDueDateto.Text != ""))
            {
                if (DateTime.ParseExact(txtDueDateFrom.Text, "dd/MM/yyyy", null) > DateTime.ParseExact(txtDueDateto.Text, "dd/MM/yyyy", null))
                {
                    lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "DT_FROM_GT_TO", utility.GetUserCulture());
                    iCheck = true;
                }
            }

            return iCheck;
        }

        private void BindGridPriview()
        {
            DataSet dsPreview = (DataSet)ViewState["dsQueryInvoice"];
            DataRow drPreview = dsPreview.Tables[0].Rows[0];
            DataTable dtPreview = dsPreview.Copy().Tables["InvPreview"].Copy();

            string strInvNo = "";
            DateTime strInvDateFrom;
            DateTime strInvDateTo;
            DateTime strDueDateFrom;
            DateTime strDueDateTo;
            string strInvType = "";
            string strCustID = "";
            string strCustName = "";
            string strStatus = "";

            if (drPreview["invoice_no"] != null && drPreview["invoice_no"].ToString() != "")
            {
                strInvNo = (string)drPreview["invoice_no"].ToString();
            }
            else
            {
                strInvNo = "";
            }

            if (drPreview["invdate_from"] != null && drPreview["invdate_from"].ToString() != "")
            {
                strInvDateFrom = DateTime.Parse(drPreview["invdate_from"].ToString());
            }
            else
            {
                strInvDateFrom = DateTime.MinValue;
            }
            if (drPreview["invdate_to"] != null && drPreview["invdate_to"].ToString() != "")
            {
                strInvDateTo = DateTime.Parse(drPreview["invdate_to"].ToString());
            }
            else if (drPreview["invdate_from"] != null && drPreview["invdate_from"].ToString() != "")
            {
                strInvDateTo = DateTime.Now;
                //strInvDateTo = DateTime.MinValue;
                //strInvDateTo = DateTime.Parse(drPreview["invdate_from"].ToString());
            }
            else
            {
                strInvDateTo = DateTime.MinValue;
            }

            if (drPreview["duedate_from"] != null && drPreview["duedate_from"].ToString() != "")
            {
                strDueDateFrom = DateTime.Parse(drPreview["duedate_from"].ToString());
            }
            else
            {
                strDueDateFrom = DateTime.MinValue;
            }

            if (drPreview["duedate_to"] != null && drPreview["duedate_to"].ToString() != "")
            {
                strDueDateTo = DateTime.Parse(drPreview["duedate_to"].ToString());
            }
            else if (drPreview["duedate_from"] != null && drPreview["duedate_from"].ToString() != "")
            {
                strDueDateTo = DateTime.Parse(drPreview["duedate_from"].ToString());
                //strDueDateTo = DateTime.MinValue;
            }
            else
            {
                strDueDateTo = DateTime.MinValue;
            }

            if (drPreview["invoice_type"] != null && drPreview["invoice_type"].ToString() != "")
            {
                strInvType = (string)drPreview["invoice_type"].ToString();
            }
            else
            {
                strInvType = "";
            }

            if (drPreview["customer_id"] != null && drPreview["customer_id"].ToString() != "")
            {
                strCustID = (string)drPreview["customer_id"].ToString();
            }
            else
            {
                strCustID = "";
            }

            if (drPreview["customer_name"] != null && drPreview["customer_name"].ToString() != "")
            {
                strCustName = (string)drPreview["customer_name"].ToString();
            }
            else
            {
                strCustName = "";
            }

            if (drPreview["status"] != null && drPreview["status"].ToString() != "")
            {
                strStatus = (string)drPreview["status"].ToString();
            }
            else
            {
                strStatus = "";
            }




            m_sdsInvoiceQuery = InvoiceManagementMgrDAL.GetInvoice(appID, enterpriseID, this.m_strCulture, strInvDateFrom, strInvDateTo, strInvNo, strDueDateFrom, strDueDateTo, strInvType, strCustID, strStatus, "InvoiceManageSelect");
            //			m_sdsInvoiceQuery.Tables[0].Columns.Add(new DataColumn("batch_no", typeof(string)));
            //			m_sdsInvoiceQuery.Tables[0].Columns.Add(new DataColumn("next_bill_placement_date", typeof(DateTime)));
            //			m_sdsInvoiceQuery.Tables[0].Columns.Add(new DataColumn("amt_paid", typeof(decimal)));
            //			m_sdsInvoiceQuery.Tables[0].Columns.Add(new DataColumn("pay_date", typeof(DateTime)));
            //			m_sdsInvoiceQuery.Tables[0].Columns.Add(new DataColumn("first_possible_bpd", typeof(DateTime)));
            //			m_sdsInvoiceQuery.Tables[0].Columns.Add(new DataColumn("promised_dt", typeof(DateTime)));
            //			m_sdsInvoiceQuery.Tables[0].Columns.Add(new DataColumn("linked_credit_note", typeof(string)));
            ViewState["PreviewGrid"] = m_sdsInvoiceQuery;
            this.dgPreview.DataSource = m_sdsInvoiceQuery.Tables[0];
            this.dgPreview.DataBind();
            //SetRowsColor(dgPreview);
            //Added By GwanG 09May08
            //			if(this.ddlInvStatus.SelectedItem.Value == "N")		
            //			{
            //				//"Created Date";
            //				dgPreview.Columns[3].Visible = false;
            //				dgPreview.Columns[4].Visible = true;
            //			}
            //			else if(this.ddlInvStatus.SelectedItem.Value == "A")
            //			{
            //				//"Invoice Date";
            //				dgPreview.Columns[4].Visible = false;
            //				dgPreview.Columns[3].Visible = true;
            //			}
            //			else 
            //			{
            //"Invoice Date";
            dgPreview.Columns[4].Visible = false;
            dgPreview.Columns[3].Visible = true;
            //			}

            this.lblShowStatust.Text = this.ddlInvStatus.SelectedItem.Text;
            this.btnSaveImport.Visible = false;
        }

        private void BindGridPreviewForImport()
        {
            this.dgPreview.DataSource = (DataTable)Session["InvoiceForImport"];
            this.dgPreview.DataBind();
            this.lblShowStatust.Text = "NOT APPLICABLE";
            this.btnSaveImport.Visible = true;
        }

        private void ActiveDIVSearch()
        {
            divInvoiceManagment.Visible = true;
            divInvManageList.Visible = false;
            InvoiceDetails.Visible = false;
        }

        private void ActiveDIVGrid()
        {
            divInvoiceManagment.Visible = false;
            divInvManageList.Visible = true;
            InvoiceDetails.Visible = false;
        }

        private void ActiveDIVInvDetail()
        {
            divInvoiceManagment.Visible = false;
            divInvManageList.Visible = false;
            InvoiceDetails.Visible = true;
        }

        private void ClearGrid()
        {
            DataSet ds = new DataSet();
            ds = (DataSet)ViewState["PreviewGrid"];
            ds.Clear();
            this.dgPreview.DataSource = ds;
            this.dgPreview.DataBind();
        }

        private void CheckAll(int maxSelectedCount)
        {
            //int selectedCount = 1;
            CountSeletct = 0;
            foreach (DataGridItem dgItem in this.dgPreview.Items)
            {
                CheckBox getCheck = (CheckBox)dgItem.Cells[0].Controls[1];
                Label lblAdj = (Label)dgItem.FindControl("lblAdj");

                double i;

                if ((lblAdj != null) && (!lblAdj.Text.GetType().Equals(System.Type.GetType("System.DBNull"))) && (lblAdj.Text.Trim() != ""))
                {
                    i = Convert.ToDouble(utility.decodeNegValue(lblAdj.Text));
                }
                else
                {
                    i = Convert.ToDouble(0);
                }

                getCheck.Checked = true;
                if (getCheck.Checked == true)
                {
                    if (CountSeletct <= maxSelectedCount)
                        CountSeletct++;
                    if (this.UserRole.Equals("ACCT") && i != 0)
                    {
                        this.btnApproveSelected.Enabled = false;
                    }
                }
                //				if (maxSelectedCount > 0 && selectedCount > maxSelectedCount) break;
                //				selectedCount++;
                if (maxSelectedCount > 0 && CountSeletct > maxSelectedCount)
                {
                    getCheck.Enabled = false;
                    getCheck.Checked = false;
                    //break;
                }
            }
            if (CountSeletct > maxSelectedCount)
                CountSeletct -= 1;
            if (CountSeletct > 0)
                lblErrorMsg.Text = CountSeletct.ToString() + " Selected";
        }

        private void ClearSearchControl()
        {
            this.txtInvDateFrom.Text = "";
            this.txtInvDateto.Text = "";
            this.txtDueDateFrom.Text = "";
            this.txtDueDateto.Text = "";
            this.txtInvoiceNo.Text = "";
            this.txtCustName.Text = "";
            this.dbCmbAgentId.Text = "";
            this.dbCmbAgentId.Value = "";
            this.ddbCustomerAcc.SelectedIndex = 0;
            this.ddlInvType.SelectedIndex = 0;
            this.ddlInvStatus.SelectedIndex = 0;
            this.txtFilePath.Text = "";
        }

        private void SetRowsColor(DataGrid dg)
        {

            foreach (DataGridItem dbItem in dg.Items)
            {
                int i;
                String strCode;
                String strVal;

                Label lblAmtPaid = (Label)dbItem.Cells[15].FindControl("lblAmtPaid");
                if (lblAmtPaid != null)
                {
                    if (lblAmtPaid.Text.Length > 0)
                    {
                        if (Convert.ToDouble(lblAmtPaid.Text) < 0.00)
                        {
                            //							dbItem.Cells[15].BackColor = Color.Red;
                            //							dbItem.BackColor = Color.Red;
                            lblAmtPaid.ForeColor = Color.Red;
                        }
                    }
                }

                /*
				for(i=0;i<dg.Columns.Count;i++)
				{
					//Modified by GwanG on 15May08
					strCode = dbItem.Cells[11].Text;
					
					strVal = InvoiceManagementMgrDAL.GetValueByCodeText(appID,m_strCulture,"invoice_status",strCode,CodeValueType.StringValue);
					if(strVal == "A")
					{
						dbItem.Cells[i].BackColor = Color.FromName("#9ACC00");
					}
					if(strVal == "C")
					{
						dbItem.Cells[i].BackColor = Color.FromName("#FF9A00");
					}
					if(strVal == "N")
					{
						dbItem.Cells[i].BackColor = Color.FromName("#FFFF9A");
					}
					if(strVal == "L")
					{
						dbItem.Cells[i].BackColor = Color.FromName("#9ACC00");
					}
					if(strVal == "D")
					{
						dbItem.Cells[i].BackColor = Color.FromName("#9ACC00");
					}

//					if(dbItem.Cells[9].Text == "APPROVED")
//					{
//						dbItem.Cells[i].BackColor = Color.FromName("#9ACC00");
//					}
//					if(dbItem.Cells[9].Text == "CANCELLED")
//					{
//						dbItem.Cells[i].BackColor = Color.FromName("#FF9A00");
//					}
//					if(dbItem.Cells[9].Text == "NOT YET APPROVED")
//					{
//						dbItem.Cells[i].BackColor = Color.FromName("#FFFF9A");
//					}

					
				}
				*/
            }
        }

        private void ManageShowPopup(bool IsNoramlMode)
        {

            btnSaveCancel.Visible = IsNoramlMode;
            btnCancelINVRemark.Visible = IsNoramlMode;
            lblSendAddr1.Visible = IsNoramlMode;
            lblSendNm.Visible = IsNoramlMode;
            ddlInvoiceCancelType.Visible = IsNoramlMode;
            ddlInvoiceCancelReason.Visible = IsNoramlMode;

            lblShowCancelInvoice.Visible = !IsNoramlMode;
            btnOKCancelInvoice.Visible = !IsNoramlMode;
        }


        #endregion

        #region Reports Function



        //by sittichai 27/03/2008
        private DataTable CreateEmptyDataTable()
        {
            DataTable dtInvoiceRP = new DataTable();

            #region "Dates"
            //			dtInvoiceRP.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
            //			dtInvoiceRP.Columns.Add(new DataColumn("end_date", typeof(DateTime)));
            #endregion

            #region "Invoice"
            dtInvoiceRP.Columns.Add(new DataColumn("invoice_no", typeof(string)));
            dtInvoiceRP.Columns.Add(new DataColumn("payerid", typeof(string)));
            #endregion

            return dtInvoiceRP;

        }

        private DataSet GetInvoiceQueryData()
        {

            DataSet dsInvoiceRP = new DataSet();
            DataTable dtInvoiceRP = CreateEmptyDataTable();
            DataRow dr;

            for (int i = 0; i <= dgPreview.Items.Count - 1; i++)
            {
                CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");

                if (chkSelect.Checked)
                {
                    dr = dtInvoiceRP.NewRow();
                    dr["invoice_no"] = dgPreview.Items[i].Cells[3].Text;
                    dr["payerid"] = dgPreview.Items[i].Cells[8].Text;
                    dtInvoiceRP.Rows.Add(dr);
                }
            }

            dsInvoiceRP.Tables.Add(dtInvoiceRP);
            return dsInvoiceRP;

        }

        #endregion


        [Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
        public static object AgentIdServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
        {
            Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings, HttpContext.Current.Session);
            String strAppID = util.GetAppID();
            String strEnterpriseID = util.GetEnterpriseID();

            String strAgentName = "";
            if (args.ServerState != null && args.ServerState.Authenticate("kdfsjgnskajh[apq2-4uhg465654435879mnbfdm cxs;q4"))
            {
                if (args.ServerState["strAgentName"] != null && args.ServerState["strAgentName"].ToString().Length > 0)
                {
                    strAgentName = args.ServerState["strAgentName"].ToString();
                }
            }

            DataSet dataset = DbComboDAL.PayerIDQuery(strAppID, strEnterpriseID, args, "BC", strAgentName);
            return dataset;
        }

        private void ddlInvoiceCancelType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ArrayList systemCodes2 = Utility.GetCodeValues(this.appID.ToString(), utility.GetUserCulture(), "inv_cancel_reason", CodeValueType.StringValue);
            this.ddlInvoiceCancelReason.Items.Clear();
            foreach (SystemCode sysCode in systemCodes2)
            {
                if (this.ddlInvoiceCancelType.SelectedIndex == 0)
                {
                    if (sysCode.StringValue.Substring(0, 2).ToString() == "CC")
                    {
                        ListItem lstItem = new ListItem();
                        lstItem.Text = sysCode.StringValue; //Text;
                        lstItem.Value = sysCode.StringValue;
                        this.ddlInvoiceCancelReason.Items.Add(lstItem);
                    }
                }
                else
                {
                    if (sysCode.StringValue.Substring(0, 2).ToString() == "CN")
                    {
                        ListItem lstItem = new ListItem();
                        lstItem.Text = sysCode.StringValue; //Text;
                        lstItem.Value = sysCode.StringValue;
                        this.ddlInvoiceCancelReason.Items.Add(lstItem);
                    }
                }

            }
        }

        private void btnSaveCancel_Click(object sender, System.EventArgs e)
        {

            DataSet dsSelect = (DataSet)ViewState["dsSelect"];

            //Show Message	
            ManageShowPopup(false);
            lblShowCancelInvoice.Text = string.Empty;

            if (lblShowCancelInvoice.Text.Trim() == "")
                btnOKCancelInvoice_Click(null, null);
            else
            {
                ManageShowPopup(false);
                //string Formatshow ="Invoice no<br>{0}<br> has already CN/DN related and also will be cancelled.";
                string Formatshow = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_CANCEL_Invoice no", utility.GetUserCulture());
                lblShowCancelInvoice.Text = String.Format(Formatshow, lblShowCancelInvoice.Text);
            }

        }

        private void btnCancelINVRemark_Click(object sender, System.EventArgs e)
        {
            this.dgPreview.Enabled = true;

            for (int i = 0; i < this.dgPreview.Items.Count; i++)
            {
                ImageButton ib = (ImageButton)this.dgPreview.Items[i].FindControl("imgSelect");
                ib.Visible = true;
            }

            this.PanelInvoiceCancel.Visible = false;
            this.btnCancel.Enabled = true;
            this.btnApproveSelected.Enabled = (bool)ViewState["btnAppriveSelected"];
            this.btnSelectAll.Enabled = (bool)ViewState["btnSelectAll"];
            this.btnCancelSelected.Enabled = (bool)ViewState["btnCancelSelected"];
            this.btnPrintHeader.Enabled = (bool)ViewState["btnPrintHeader"];
            this.btnPrintDetail.Enabled = (bool)ViewState["btnPrintDetail"];
        }

        private void ddlInvoiceCancelReason_SelectedIndexChanged(object sender, System.EventArgs e)
        {

        }


        #region "2009-026 by GwanG on 02Apr09"
        private void ExportToXls(DataSet ds, String FileName)
        {
            DataTable dt = ds.Tables[0];
            String headerXls = "attachment;filename=" + FileName + ".xls";
            if (dt != null)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", headerXls);
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-excel";

                //write columns
                string tab = "";
                foreach (DataColumn dc in dt.Columns)
                {
                    Response.Write(tab + dc.ColumnName);
                    tab = "\t";
                }
                Response.Write("\n");

                //write rows
                int i;
                foreach (DataRow dr in dt.Rows)
                {
                    tab = " ";
                    for (i = 0; i < dt.Columns.Count; i++)
                    {
                        Response.Write(tab + dr[i].ToString());
                        tab = "\t";
                    }
                    Response.Write("\n");
                }
                Response.End();
            }
        }

        private void ExportCSVFile(DataTable dt, string fileName)
        {
            StringBuilder sb = new StringBuilder();
            int count2 = 0;

            foreach (DataRow row in dt.Rows)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    sb.Append(row[i].ToString() + ",");
                    if (count2 == 16)
                    {
                        sb.Append("\r\n");
                        count2 = 0;
                    }
                    else
                        count2++;
                }
                count2 = 0;
            }
            string strActualFile = GetPathExport() + fileName + ".csv";
            string InvoiceExportPath = (string)System.Configuration.ConfigurationSettings.AppSettings["InvoiceExportPath"];
            string file_path = InvoiceExportPath + fileName + ".csv";
            using (StreamWriter w = File.AppendText(file_path))
            {
                w.Write(sb.ToString());
                w.Close();
            }
            GetDownloadFile("0", file_path);

        }

        private string GetVirtualPath(string physicalPath)
        {
            if (!physicalPath.StartsWith(HttpContext.Current.Request.PhysicalApplicationPath))
            {
                throw new InvalidOperationException(String.Format("Physical path is not within the application root : physicalFilePath {0}  PhysicalApplicationPath {1}", physicalPath, HttpContext.Current.Request.PhysicalApplicationPath));
            }

            return "../" + physicalPath.Substring(HttpContext.Current.Request.PhysicalApplicationPath.Length).Replace("\\", "/");
        }

        private void GetDownloadFile(string Res, string FullPath)
        {
            if (Res == "0")
            {
                ArrayList paramList = new ArrayList();
                paramList.Add(this.GetVirtualPath(FullPath));
                String sScript = Utility.GetScript("openWindowParam.js", paramList);
                Utility.RegisterScriptString(sScript, "ShowPopupScript", this.Page);

            }
        }

        private string DistinctCustomer(string list, string data)
        {
            string[] listData = list.Split(',');
            if (listData.Length > 0)
            {
                for (int i = 0; i < listData.Length; i++)
                {
                    if (listData[i] == data)
                    {
                        return "";
                    }
                }
            }
            return data;
        }

        private void btnExport_Click(object sender, System.EventArgs e)
        {
            txtExcludeCustomerList.Visible = true;
            txtExcludeCustomerList.Text = "Y";
            DataSet dsInvoiceNumberList = GetInvoiceQueryData();
            string userID = utility.GetUserID();

            lblErrorMsg.Text = "";

            string invoice_list = "";
            string customer_list = "";
            string customerID = "";
            for (int i = 0; i < dsInvoiceNumberList.Tables[0].Rows.Count; i++)
            {
                customerID = "";
                string strInvoiceNumber = dsInvoiceNumberList.Tables[0].Rows[i]["invoice_no"].ToString();
                invoice_list += strInvoiceNumber;
                if (i != dsInvoiceNumberList.Tables[0].Rows.Count - 1)
                    invoice_list += ",";

                customerID = DistinctCustomer(customer_list, dsInvoiceNumberList.Tables[0].Rows[i]["payerid"].ToString());

                customer_list += customerID;
                if (customerID != "")
                {
                    if (i != dsInvoiceNumberList.Tables[0].Rows.Count - 1)
                        customer_list += ",";
                }
            }
            //result = InvoiceManagementMgrDAL.GetCSVExport(this.appID, this.enterpriseID, invoice_list, m_strUserID);

            String sUrl = "InvoiceCustomerPopup.aspx?FORMID=InvManageSelect&TARGET=" + txtExcludeCustomerList.ClientID + "&CUSTOMER_LIST=" + customer_list;
            ArrayList paramList = new ArrayList();
            paramList.Add(sUrl);
            String sScript = Utility.GetScript("openParentWindowCustomSize.js", paramList, 300, 300);
            //String sScript = Utility.GetScript("openWindowParam.js",paramList);
            Utility.RegisterScriptString(sScript, "ShowPopupScript", this.Page);
        }

        private bool CheckColumnNameIsExist(string stringColumn)
        {
            bool flag = false;

            foreach (DataColumn dc in dsExport.Tables[0].Columns)
            {
                if (stringColumn.ToLower() == dc.ColumnName.ToLower())
                {
                    flag = true;
                }
            }
            return flag;
        }


        private string GetPathExport()
        {
            //return Server.MapPath("") + "\\Export\\";
            return (string)System.Configuration.ConfigurationSettings.AppSettings["InvoiceSharedPath"];
        }

        #endregion

        private void btnOKCancelInvoice_Click(object sender, System.EventArgs e)
        {
            this.dgPreview.Enabled = true;
            this.PanelInvoiceCancel.Visible = false;
            ManageShowPopup(true);

            int getReturn = 0;

            DataSet dsSelect = (DataSet)ViewState["dsSelect"];
            DataSet dsShipment = (DataSet)ViewState["dsShipment"];
            //OldCode
            //getReturn = InvoiceManagementMgrDAL.CancelInvoice(appID,enterpriseID,dsSelect,dsShipment,this.ddlInvoiceCancelType.SelectedItem.Value.ToString(),this.ddlInvoiceCancelReason.SelectedItem.Value.ToString());
            // 18/1/2010

            //Lop For 1 invoice
            foreach (DataRow drInvoice in dsSelect.Tables[0].Rows)
            {
                DataSet dsInvoice = new DataSet();
                DataTable dtTemp = dsSelect.Tables[0].Clone();
                dtTemp.Rows.Add(drInvoice.ItemArray);
                dsInvoice.Tables.Add(dtTemp);

                getReturn = InvoiceManagementMgrDAL.CancelInvoiceCreditDebit(appID
                    , enterpriseID
                    , m_strUserID
                    , drInvoice["invoice_no"].ToString()
                    , dsInvoice
                    , dsShipment
                    , this.ddlInvoiceCancelType.SelectedValue
                    , this.ddlInvoiceCancelReason.SelectedValue);
                if (getReturn == -100)
                {
                    //					string jsScript = "Cancelling this invoice ({0})  will also cancel Credit and/or Debit Notes <br>";
                    //					jsScript += "approved in the prior accounting period.  You must cancel those Credit and/or <br>";
                    //					jsScript += "Debit Notes first and then cancel this invoice.<br><br>";	
                    string jsScript =
                    Utility.GetLanguageText(ResourceType.UserMessage, "MSG_CANCEL_Cancelling_this", utility.GetUserCulture());

                    lblErrorForCancel.Text = string.Format(jsScript, drInvoice["invoice_no"].ToString());
                    return;
                }
            }

            //			if(getReturn > 0)
            //			{
            ClearGrid();
            BindGridPriview();
            //}

            this.btnCancel.Enabled = true;
            this.btnApproveSelected.Enabled = (bool)ViewState["btnAppriveSelected"];
            this.btnSelectAll.Enabled = (bool)ViewState["btnSelectAll"];
            this.btnCancelSelected.Enabled = (bool)ViewState["btnCancelSelected"];
            this.btnPrintHeader.Enabled = (bool)ViewState["btnPrintHeader"];
            this.btnPrintDetail.Enabled = (bool)ViewState["btnPrintDetail"];
            btnCancelSelected.Enabled = false;
            btnExport.Enabled = false;
        }

        private void btnImport_Click(object sender, System.EventArgs e)
        {
            UploadFiletoServer();
            if (GetExcelFiles())
            {
                getInvoiceDetailByImport();
                DisplayCurrentPage();
                setDGAssociatedCNDN();
                btnGenerate_Click(null, null);
                divInvManageList.Visible = true;
                pnlinvupdate.Visible = false;
                lblInvDispStat.Text = "Status";
            }


        }
        private bool UploadFiletoServer()
        {
            bool status = true;
            String userID = utility.GetUserID();
            if ((inFile.PostedFile != null) && (inFile.PostedFile.ContentLength > 0))
            {
                string extension = System.IO.Path.GetExtension(inFile.PostedFile.FileName);

                if (extension.ToUpper() == ".XLS")
                {
                    string fn = System.IO.Path.GetFileName(inFile.PostedFile.FileName);
                    string path = Server.MapPath("Excel") + "\\";
                    string pathFn = Server.MapPath("Excel") + "\\" + userID + "_" + fn;

                    ViewState["FileName"] = pathFn;

                    try
                    {
                        if (System.IO.Directory.Exists(path) == false)
                            System.IO.Directory.CreateDirectory(path);

                        if (System.IO.File.Exists(pathFn))
                            System.IO.File.Delete(pathFn);

                        inFile.PostedFile.SaveAs(pathFn);

                        lblErrorMsg.Text = "";
                    }
                    catch (Exception ex)
                    {
                        lblErrorMessage.Text = ex.Message;
                    }
                }
                else
                {
                    lblErrorMessage.Text = "Not a valid Excel Workbook.";
                    txtFilePath.Text = "";
                    status = false;
                }
            }
            else
            {
                lblErrorMessage.Text = "Please select a file to upload.";
                status = false;
            }

            return status;
        }

        private bool GetExcelFiles()
        {
            bool pass = false;
            try
            {
                String connString = ConfigurationSettings.AppSettings["ExcelConn"];
                connString = connString.Replace("(DestinationFile)", (String)ViewState["FileName"]);

                int recCouter = 1;

                StringBuilder strBuilder = new StringBuilder();
                strBuilder.Append("SELECT Invoice_Number, Batch_No, Payment_Date, Amount_Paid ");
                strBuilder.Append("FROM [Sheet1$] order by Invoice_Number, Payment_Date");
                OleDbDataAdapter daInvoice = new OleDbDataAdapter(strBuilder.ToString(), connString);

                DataSet dsExcelInvoice = new DataSet();
                DataSet dsInvoiceForValidate = null;
                DataSet dsInvoiceForImport = null;
                daInvoice.Fill(dsExcelInvoice);

                dsInvoiceForValidate = InvoiceManagementMgrDAL.GetEmptyImportValidation();
                dsInvoiceForImport = InvoiceManagementMgrDAL.GetEmptyInvoiceTmp();

                if (dsInvoiceForValidate.Tables[0].Rows.Count == 1)
                {
                    dsInvoiceForValidate.Tables[0].Rows[0].Delete();
                    dsInvoiceForValidate.AcceptChanges();
                }
                if (dsInvoiceForImport.Tables[0].Rows.Count == 1)
                {
                    dsInvoiceForImport.Tables[0].Rows[0].Delete();
                    dsInvoiceForImport.AcceptChanges();
                }
                DataSet dsExcelTmp = dsExcelInvoice.Clone();
                foreach (DataRow drTmpExcel in dsExcelInvoice.Tables[0].Rows)
                {
                    DataRow drTmpExcel2 = dsExcelTmp.Tables[0].NewRow();
                    if (!(drTmpExcel["Invoice_Number"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                    {
                        String invoice = (String)drTmpExcel["Invoice_Number"];
                        DataRow[] drExcel = dsExcelTmp.Tables[0].Select("Invoice_Number='" + invoice + "'");
                        int numRow = dsExcelTmp.Tables[0].Rows.Count;
                        if (drExcel.Length > 0)
                        {
                            foreach (DataRow r in drExcel)
                            {
                                drTmpExcel2["Amount_Paid"] = (double)r["Amount_Paid"] + (double)drTmpExcel["Amount_Paid"];
                                drTmpExcel2["Invoice_Number"] = drTmpExcel["Invoice_Number"];
                                drTmpExcel2["Batch_No"] = drTmpExcel["Batch_No"];
                                drTmpExcel2["Payment_Date"] = drTmpExcel["Payment_Date"];
                            }
                            dsExcelTmp.Tables[0].Rows[numRow - 1].Delete();
                        }
                        else
                        {
                            drTmpExcel2["Amount_Paid"] = drTmpExcel["Amount_Paid"];
                            drTmpExcel2["Invoice_Number"] = drTmpExcel["Invoice_Number"];
                            drTmpExcel2["Batch_No"] = drTmpExcel["Batch_No"];
                            drTmpExcel2["Payment_Date"] = drTmpExcel["Payment_Date"];
                        }


                        dsExcelTmp.Tables[0].Rows.Add(drTmpExcel2);
                        dsExcelTmp.AcceptChanges();
                    }
                }
                dsExcelInvoice = dsExcelTmp;
                foreach (DataRow dr in dsExcelInvoice.Tables[0].Rows)
                {
                    if ((dr["Invoice_Number"].ToString().Trim() != "") ||
                        (dr["Batch_No"].ToString().Trim() != "") ||
                        (dr["Payment_Date"].ToString().Trim() != "") ||
                        (dr["Amount_Paid"].ToString().Trim() != ""))
                    {
                        //						DataRow tmpDr = dsInvoice.Tables[0].NewRow();
                        String[] errMsg = new string[20];
                        String[] tmpDate = null;
                        int i = 0;
                        String payDate = null;

                        //						tmpDr["recordid"] = recCouter.ToString();
                        //						recCouter++;

                        try
                        {
                            DateTime.ParseExact(dr["Payment_Date"].ToString(), "dd/MM/yyyy", null);
                            System.TimeSpan dateDiff = DateTime.ParseExact(dr["Payment_Date"].ToString(), "dd/MM/yyyy", null).Subtract(DateTime.Now);
                            payDate = dr["Payment_Date"].ToString();
                            if (dateDiff.Days > 90)
                            {
                                errMsg[i] = "Future payment dates must be within 90 days";
                                i++;
                            }

                            if (dateDiff.Days <= 90 && dateDiff.Days > 0)
                            {
                                if (Convert.ToDecimal(((double)dr["Amount_Paid"]).ToString("0.00")) < 0)
                                {
                                    errMsg[i] = "Future payments may not be negative";
                                    i++;
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            if (e.Message == "The DateTime represented by the string is out of range.")
                            {
                                tmpDate = (dr["Payment_Date"].ToString()).Split('/');
                                if (Convert.ToInt32(tmpDate[1]) < 1 || Convert.ToInt32(tmpDate[1]) > 12)
                                {
                                    errMsg[i] = "Invalid date (month)";
                                    i++;
                                }
                                else
                                {
                                    DateTime first = new DateTime(Convert.ToInt32(tmpDate[2]), Convert.ToInt32(tmpDate[1]), 1);
                                    DateTime last = new DateTime(Convert.ToInt32(tmpDate[2]), Convert.ToInt32(tmpDate[1]) + 1, 1).AddDays(-1);
                                    if (Convert.ToInt32(tmpDate[0]) > last.Day || Convert.ToInt32(tmpDate[0]) < first.Day)
                                    {
                                        errMsg[i] = "Invalid date (day)";
                                        i++;
                                    }
                                }
                            }
                            else
                            {
                                if (dr["Payment_Date"] == System.DBNull.Value)
                                {
                                    errMsg[i] = "Invalid date - null and blank not accepted";
                                    i++;
                                }
                                else
                                {
                                    errMsg[i] = "Invalid date";
                                    i++;
                                }
                            }
                            return false;
                        }

                        try
                        {
                            if (Convert.ToDecimal(((double)dr["Amount_Paid"]).ToString("0.00")) < 0)
                            {

                            }
                        }
                        catch (Exception e)
                        {
                            errMsg[i] = "Invalid amount";
                            i++;
                        }
                        DateTime dtInvoiceDate = System.DateTime.Now;
                        DateTime dtDueDate = System.DateTime.Now;
                        DateTime dtActBillPlacementDate = System.DateTime.MinValue;
                        DateTime dtFirstBillPlacementDate = System.DateTime.MinValue;
                        DateTime dtNextBillPlacementDate = System.DateTime.MinValue;
                        String strCreditNo = null;
                        String strInvoiceStatus = null;
                        decimal invoiceAjustAmount = 0;
                        String strCustID = null;
                        decimal invoiceAmount = 0;

                        DataSet dsTmpInvoice = InvoiceManagementMgrDAL.GetInvoiceByInvNo(appID, enterpriseID, dr["Invoice_Number"].ToString(), "R");
                        if (dsTmpInvoice.Tables[0].Rows.Count < 1)
                        {
                            errMsg[i] = "Invoice does not exist";
                            i++;
                        }
                        else
                        {
                            foreach (DataRow drEach in dsTmpInvoice.Tables[0].Rows)
                            {

                                if ((drEach["actual_bill_placement_date"] != System.DBNull.Value || drEach["actual_bill_placement_date"].ToString() != "") && payDate != null)
                                {
                                    if (DateTime.ParseExact(payDate, "dd/MM/yyyy", null) < Convert.ToDateTime(drEach["actual_bill_placement_date"]))
                                    {
                                        errMsg[i] = "Payment date earlier than placement date";
                                        i++;
                                    }
                                    else
                                    {
                                        dtActBillPlacementDate = Convert.ToDateTime(drEach["actual_bill_placement_date"]);
                                    }
                                }
                                if (drEach["invoice_status"].ToString() != "L" && drEach["invoice_status"].ToString() != "D")
                                {
                                    errMsg[i] = "Invoice must be in PLACED status";
                                    i++;
                                }
                                if (drEach["invoice_status"] != System.DBNull.Value || drEach["invoice_status"].ToString() != "")
                                {
                                    if (drEach["invoice_status"].ToString() != "L" || drEach["invoice_status"].ToString() != "D")
                                    {
                                        //										errMsg[i] = "Invoice must be in PLACED status";
                                        //										i++;
                                        strInvoiceStatus = (String)drEach["invoice_status"];
                                    }
                                    else if (drEach["invoice_status"].ToString() == "L")
                                    {
                                        strInvoiceStatus = "PLACED";
                                    }
                                    else if (drEach["invoice_status"].ToString() == "D")
                                    {
                                        strInvoiceStatus = "PAID";
                                    }
                                }

                                if (drEach["invoice_date"] != System.DBNull.Value || drEach["invoice_date"].ToString() != "")
                                {
                                    dtInvoiceDate = (DateTime)drEach["invoice_date"];
                                }
                                if (drEach["due_date"] != System.DBNull.Value || drEach["due_date"].ToString() != "")
                                {
                                    dtDueDate = (DateTime)drEach["due_date"];
                                }

                                if (drEach["payerid"] != System.DBNull.Value || drEach["payerid"].ToString() != "")
                                {
                                    strCustID = (String)drEach["payerid"];
                                }
                                if (drEach["invoice_adj_amount"] != System.DBNull.Value || drEach["invoice_adj_amount"].ToString() != "")
                                {
                                    invoiceAjustAmount = (decimal)drEach["invoice_adj_amount"];
                                }
                                if (drEach["invoice_amt"] != System.DBNull.Value || drEach["invoice_amt"].ToString() != "")
                                {
                                    invoiceAmount = (decimal)drEach["invoice_amt"];
                                }
                                if (drEach["first_bill_placement_date"] != System.DBNull.Value || drEach["first_bill_placement_date"].ToString() != "")
                                {
                                    dtFirstBillPlacementDate = Convert.ToDateTime(drEach["first_bill_placement_date"]);
                                }
                                if (drEach["next_bill_placement_date"] != System.DBNull.Value || drEach["next_bill_placement_date"].ToString() != "")
                                {
                                    dtNextBillPlacementDate = Convert.ToDateTime(drEach["next_bill_placement_date"]);
                                }
                                if (drEach["credit_no"] != System.DBNull.Value || drEach["credit_no"].ToString() != "")
                                {
                                    strCreditNo = (String)drEach["credit_no"];
                                }
                            }
                            decimal tmpAmountPaid = 0;
                            decimal tmpDebitAmount = 0;
                            decimal tmpCreditAmount = 0;
                            decimal tmpLinkCreditAmount = 0;
                            decimal balancedDue = 0;
                            DataSet dsInvoicePayment = InvoiceManagementMgrDAL.GetInvoice_Payment(appID, enterpriseID, dr["Invoice_Number"].ToString());
                            if (dsInvoicePayment.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow drPayment in dsInvoicePayment.Tables[0].Rows)
                                {
                                    tmpAmountPaid = (decimal)drPayment["amount_paid"];
                                }
                            }
                            DataSet dsDebitAmount = InvoiceManagementMgrDAL.GetDebit_Amount(appID, enterpriseID, dr["Invoice_Number"].ToString());
                            if (dsDebitAmount.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow drDebit in dsDebitAmount.Tables[0].Rows)
                                {
                                    tmpDebitAmount = (decimal)drDebit["debit_amount"];
                                }
                            }
                            DataSet dsCreditAmount = InvoiceManagementMgrDAL.GetCredit_Amount(appID, enterpriseID, dr["Invoice_Number"].ToString());
                            if (dsCreditAmount.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow drCredit in dsCreditAmount.Tables[0].Rows)
                                {
                                    tmpCreditAmount = (decimal)drCredit["credit_amount"];
                                }
                            }
                            DataSet dsLinkedCreditAmount = InvoiceManagementMgrDAL.GetLinkedCredit_Amount(appID, enterpriseID, dr["Invoice_Number"].ToString());
                            if (dsLinkedCreditAmount.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow drLink in dsLinkedCreditAmount.Tables[0].Rows)
                                {
                                    tmpLinkCreditAmount = (decimal)drLink["credit_amount"];
                                }
                            }
                            if (tmpAmountPaid + Convert.ToDecimal(((double)dr["Amount_Paid"]).ToString("0.00")) < 0)
                            {
                                errMsg[i] = "Negative amount exceeds paid amount";
                                i++;
                            }
                            balancedDue = (invoiceAmount + tmpDebitAmount) - (tmpCreditAmount + tmpLinkCreditAmount + tmpAmountPaid);
                            if (Convert.ToDecimal(((double)dr["Amount_Paid"]).ToString("0.00")) > balancedDue)
                            {
                                errMsg[i] = "Payment amount exceeds balance due";
                                i++;
                            }
                        }
                        if (i > 0)
                        {
                            for (int k = 0; k < i; k++)
                            {
                                DataRow tmpDr = dsInvoiceForValidate.Tables[0].NewRow();
                                tmpDr["recordid"] = recCouter.ToString();
                                recCouter++;
                                tmpDr["Invoice_Number"] = Convert.ToString(dr["Invoice_Number"]);
                                tmpDr["Batch_No"] = Convert.ToString(dr["Batch_No"]);
                                tmpDr["Payment_Date"] = Convert.ToString(dr["Payment_Date"]);
                                if (!(dr["Amount_Paid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                                {
                                    tmpDr["Amount_Paid"] = ((double)dr["Amount_Paid"]).ToString("0.00");
                                }
                                tmpDr["error"] = errMsg[k];

                                dsInvoiceForValidate.Tables[0].Rows.Add(tmpDr);
                                dsInvoiceForValidate.AcceptChanges();
                            }
                        }
                        else
                        {
                            DataRow tmpDr = dsInvoiceForImport.Tables[0].NewRow();
                            tmpDr["invoice_no"] = Convert.ToString(dr["Invoice_Number"]);
                            tmpDr["batch_no"] = Convert.ToString(dr["Batch_No"]);
                            tmpDr["payment_date"] = DateTime.ParseExact(dr["Payment_Date"].ToString(), "dd/MM/yyyy", null);
                            tmpDr["amt_paid"] = Convert.ToDecimal(((double)dr["Amount_Paid"]).ToString("0.00"));
                            tmpDr["invoice_date"] = dtInvoiceDate;
                            tmpDr["due_date"] = dtDueDate;
                            tmpDr["invoicestatus_name"] = strInvoiceStatus;
                            tmpDr["payerid"] = strCustID;
                            tmpDr["invoice_adj_amount"] = invoiceAjustAmount;
                            tmpDr["invoice_amt"] = invoiceAmount;
                            tmpDr["payertype_name"] = "Credit";
                            if (dtActBillPlacementDate.Equals(System.DateTime.MinValue))
                                tmpDr["actual_bill_placement_date"] = System.DBNull.Value;
                            else
                                tmpDr["actual_bill_placement_date"] = dtActBillPlacementDate;

                            if (dtFirstBillPlacementDate.Equals(System.DateTime.MinValue))
                                tmpDr["first_possible_bpd"] = System.DBNull.Value;
                            else
                                tmpDr["first_possible_bpd"] = dtFirstBillPlacementDate;

                            if (dtNextBillPlacementDate.Equals(System.DateTime.MinValue))
                                tmpDr["next_bill_placement_date"] = System.DBNull.Value;
                            else
                                tmpDr["next_bill_placement_date"] = dtNextBillPlacementDate;

                            tmpDr["link_to_invoice"] = strCreditNo;

                            dsInvoiceForImport.Tables[0].Rows.Add(tmpDr);
                            dsInvoiceForImport.AcceptChanges();

                            DataRow[] drInvoicePayment = dsInvoiceForImport.Tables[0].Select("invoice_no='" + Convert.ToString(dr["Invoice_Number"]) + "'");
                            InvoiceManagementMgrDAL.AddInvoice_Payment(utility.GetAppID(), utility.GetEnterpriseID(), drInvoicePayment);

                            // for Check New Status
                            DataSet dstmpInv = InvoiceManagementMgrDAL.GetInvoiceByInvNo(appID, enterpriseID, dr["Invoice_Number"].ToString(), "R");
                            if (dstmpInv.Tables[0].Rows[0]["invoice_status"] != System.DBNull.Value || dstmpInv.Tables[0].Rows[0]["invoice_status"].ToString() != "")
                            {
                                strInvoiceStatus = (String)dstmpInv.Tables[0].Rows[0]["invoice_status"];
                                if (strInvoiceStatus == "L")
                                {
                                    strInvoiceStatus = "PLACED";
                                }
                                else if (strInvoiceStatus == "D")
                                {
                                    strInvoiceStatus = "PAID";
                                }

                            }
                            int itmpInv = 0;
                            foreach (DataRow drtmpInv in dsInvoiceForImport.Tables[0].Rows)
                            {
                                dsInvoiceForImport.Tables[0].Rows[i]["invoicestatus_name"] = strInvoiceStatus;
                                itmpInv++;
                            }
                            dsInvoiceForImport.AcceptChanges();
                            // end Check New Status


                        }
                    }
                }

                dsExcelInvoice.Dispose();
                dsExcelInvoice = null;

                if (dsInvoiceForValidate.Tables[0].Rows.Count > 0)
                {
                    Session["InvoiceValidate"] = dsInvoiceForValidate.Tables[0];
                    String sUrl = "InvoiceImportValidationResult.aspx";
                    ArrayList paramList = new ArrayList();
                    paramList.Add(sUrl);
                    String sScript = Utility.GetScript("openParentWindow.js", paramList);
                    Utility.RegisterScriptString(sScript, "ShowPopupScript", this.Page);
                    //				Session["dsInvoice"] = dsInvoice;
                    return false;
                }
                else
                {
                    Session["InvoiceForImport"] = dsInvoiceForImport.Tables[0];
                    return true;
                }
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = ex.Message;
                return false;
            }
        }

        private void btnSaveImport_Click(object sender, System.EventArgs e)
        {
            DataSet dsInvoiceNumberList = GetInvoiceQueryData();
            String invoiceNumber = null;
            if (dsInvoiceNumberList.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsInvoiceNumberList.Tables[0].Rows)
                {
                    invoiceNumber = (String)dr["invoice_no"];
                    DataTable dtInvoicePayment = (DataTable)ViewState["InvoiceImport"];
                    DataRow[] drInvoicePayment = dtInvoicePayment.Select("invoice_no='" + invoiceNumber + "'");
                    InvoiceManagementMgrDAL.AddInvoice_Payment(utility.GetAppID(), utility.GetEnterpriseID(), drInvoicePayment);
                    this.btnSaveImport.Visible = false;
                }
            }
        }

        private void dgPreview_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            try
            {
                String strCode = e.Item.Cells[11].Text;
                String strVal = InvoiceManagementMgrDAL.GetValueByCodeText(appID, m_strCulture, "invoice_status", strCode, CodeValueType.StringValue);
                strCode = e.Item.Cells[7].Text;
                String strType = InvoiceManagementMgrDAL.GetValueByCodeText(appID, m_strCulture, "payment_mode", strCode, CodeValueType.StringValue);
                if (strVal != "C" && strType == "R")
                {
                    string strCustId = "";
                    string strInvNo = "";
                    strInvNo = e.Item.Cells[3].Text;
                    strCustId = e.Item.Cells[8].Text;
                    DataSet profileDS = SysDataManager1.GetEnterpriseProfile(utility.GetAppID(), utility.GetEnterpriseID());

                    entID = utility.GetEnterpriseID();
                    appID = utility.GetAppID();
                    culture = utility.GetUserCulture();
                    if (profileDS.Tables[0].Rows.Count > 0)
                    {
                        currency_decimal = (int)profileDS.Tables[0].Rows[0]["currency_decimal"];
                        ViewState["m_format"] = "{0:n" + currency_decimal.ToString() + "}";
                        strFormatCurrency = ViewState["m_format"].ToString();
                    }
                    CustId = strCustId;
                    InvNo = strInvNo;
                    getInvoiceDetail();
                    ViewState["CUSTID_TEXT"] = CustId;
                    ViewState["INVOICE_ID"] = InvNo;
                    DisplayCurrentPage();
                    setDGAssociatedCNDN();
                    getPageControls(Page);
                    ErrMsg.Text = "";
                    pnlinvupdate.Visible = true;
                    divInvManageList.Visible = false;



                    //					String sUrl = "InvoicePaymentsUpdate.aspx?CUSTID_TEXT="+strCustId+"&INVOICE_ID="+strInvNo;
                    //					ArrayList paramList = new ArrayList();
                    //					paramList.Add(sUrl);
                    //					String sScript = Utility.GetScript("openParentWindow.js",paramList);
                    //					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
                }
            }
            catch
            {
            }
        }

        private void getInvoiceDetail()
        {
            try
            {
                DataSet InvDS = InvoiceManagementMgrDAL.GetInvoiceByInvNo(appID, entID, InvNo);
                Decimal dCAmntPaid = InvoicePaymentUpdateMgrDAL.getTotalCurrentAmntPaid(appID, entID, InvNo);
                Session["InvDS"] = InvDS;
                Session["dCAmntPaid"] = dCAmntPaid;
            }
            catch
            {
            }
        }
        private void getInvoiceDetailByImport()
        {
            try
            {
                String[] InvNo = new string[300];
                int i = 0;
                if (Session["InvoiceForImport"] != null)
                {
                    DataTable dtInvoice = (DataTable)Session["InvoiceForImport"];
                    foreach (DataRow dr in dtInvoice.Rows)
                    {
                        InvNo[i] = (String)dr["invoice_no"];
                        i++;
                    }
                }

                DataSet InvDS = InvoiceManagementMgrDAL.GetInvoice(appID, entID, InvNo);
                Decimal dCAmntPaid = InvoicePaymentUpdateMgrDAL.getTotalCurrentAmntPaid(appID, entID, InvNo[0]);
                Session["InvDS"] = InvDS;
                Session["dCAmntPaid"] = dCAmntPaid;
            }
            catch
            {

            }
        }
        private void DisplayCurrentPage()
        {
            try
            {
                if (Session["InvDS"] != null)
                {
                    txtUserName.Text = utility.GetUserID();
                    Decimal dInvAmnt, dDN, dCN, dBalanceDue, dCAmntPaid = 0;
                    DataSet InvDS = (DataSet)Session["InvDS"];
                    dCAmntPaid = (Decimal)Session["dCAmntPaid"];
                    setInputFormat();
                    setInputMode(InvDS.Tables[0].Rows[0]["invoice_status"].ToString().ToUpper());
                    txtCustID.Text = InvDS.Tables[0].Rows[0]["payerid"].ToString();
                    lblCustDispName.Text = InvDS.Tables[0].Rows[0]["payer_name"].ToString();
                    txtInvNo.Text = InvDS.Tables[0].Rows[0]["invoice_no"].ToString().ToUpper();
                    DataSet dsInvStat = InvoiceManagementMgrDAL.GetInvoiceStatus(appID, entID, InvDS.Tables[0].Rows[0]["invoice_status"].ToString().ToUpper(), culture);
                    lblInvDispStat.Text = dsInvStat.Tables[0].Rows[0]["code_text"].ToString().ToUpper();

                    //txtBatchNo.TabIndex = InvDS.Tables[0].Rows[0]["invoice_status"].ToString().ToUpper();
                    txtTotalInvAmnt.Text = String.Format(strFormatCurrency, InvDS.Tables[0].Rows[0]["invoice_amt"]);
                    dInvAmnt = Convert.ToDecimal(InvDS.Tables[0].Rows[0]["invoice_amt"]);
                    dDN = CreditDebitMgrDAL.getTotalDebitNote(appID, entID, InvDS.Tables[0].Rows[0]["invoice_no"].ToString().ToUpper());
                    dCN = CreditDebitMgrDAL.getTotalCreditNote(appID, entID, InvDS.Tables[0].Rows[0]["invoice_no"].ToString().ToUpper());
                    dBalanceDue = dInvAmnt + dDN - (dCN + dCAmntPaid);
                    Session["dBalanceDue"] = dBalanceDue;

                    txtBalanceDue.Text = String.Format(strFormatCurrency, dBalanceDue);
                    txtCurrAmntPaid.Text = String.Format(strFormatCurrency, dCAmntPaid);
                    hidtxtCurrAmt.Text = String.Format(strFormatCurrency, dCAmntPaid);
                    hidtxtBalanceDue.Text = String.Format(strFormatCurrency, dBalanceDue);
                    string strUpdatedBy = "";
                    if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["updated_by"]) && Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["updated_date"]))
                    {
                        strUpdatedBy = InvDS.Tables[0].Rows[0]["updated_by"].ToString().ToUpper();
                        lblDispUpdate.Text = InvDS.Tables[0].Rows[0]["updated_by"].ToString().ToUpper() + " " + Convert.ToDateTime(InvDS.Tables[0].Rows[0]["updated_date"]).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        lblDispUpdate.Text = "";
                    }

                    if (dCAmntPaid > 0)
                    {
                        string strLastPayment = "";
                        DataSet dsLastPayment = InvoiceManagementMgrDAL.GetInvoice_Payment(this.appID, this.enterpriseID, InvDS.Tables[0].Rows[0]["invoice_no"].ToString().ToUpper());
                        if (dsLastPayment.Tables[0].Rows.Count > 0)
                        {
                            strLastPayment = Convert.ToDateTime(dsLastPayment.Tables[0].Rows[0]["payment_date"]).ToString("dd/MM/yyyy");
                        }

                        this.lblPaymentUpd.Visible = true;
                        this.lblPaymentUpd.Text = "Payment Updated By/Date: " + strUpdatedBy + "  " + strLastPayment;
                    }
                    else
                    {
                        this.lblPaymentUpd.Visible = false;
                    }


                    if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["approved_by"]) && Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["approved_date"]))
                    {
                        lblDispApprove.Text = InvDS.Tables[0].Rows[0]["approved_by"].ToString().ToUpper() + " " + Convert.ToDateTime(InvDS.Tables[0].Rows[0]["approved_date"]).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        lblDispApprove.Text = "";
                    }
                    if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["printed_due_date"]))
                    {
                        txtPrintedDueDate.Text = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["printed_due_date"]).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        txtPrintedDueDate.Text = "";
                    }
                    if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["internal_due_date"]))
                    {
                        txtInternalDueDate.Text = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["internal_due_date"]).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        txtInternalDueDate.Text = "";
                    }
                    if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["actual_bill_placement_date"]))
                    {
                        txtActualBPD.Text = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["actual_bill_placement_date"]).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        txtActualBPD.Text = "";
                    }
                    if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["promised_payment_date"]))
                    {
                        txtPromisedPaymentDate.Text = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["promised_payment_date"]).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        txtPromisedPaymentDate.Text = "";
                    }
                    //lblLastUpdate.Text = (String)InvDS.Tables[0].Rows[0]["update_by"]+" "+InvDS.Tables[0].Rows[0]["update_date"];
                    //lblDispApprove.Text = (String)InvDS.Tables[0].Rows[0]["approved_by"]+" "+System.DateTime.ParseExact((String)InvDS.Tables[0].Rows[0]["approved_date"],"dd/MM/yyyy",null);;

                }
            }
            catch
            {
            }
        }
        private void setInputFormat()
        {
            try
            {
                //currency
                txtAmntPaid.Text = txtAmntPaid.Text.Replace(",", "");
                txtAmntPaid.Attributes.Add("onblur", "round(this," + currency_decimal + ");validateInput();");
                txtAmntPaid.Attributes.Add("onTextChanged", "round(this," + currency_decimal + ");validateInput();");
                txtAmntPaid.NumberScale = currency_decimal;
                txtAmntPaid.MaxLength = 8 + currency_decimal + 1;
                txtAmntPaid.NumberPrecision = 8 + currency_decimal;

                txtTotalInvAmnt.Text = txtAmntPaid.Text.Replace(",", "");
                //txtTotalInvAmnt.Attributes.Add("onblur","round(this,"+currency_decimal+")");
                txtTotalInvAmnt.NumberScale = currency_decimal;
                txtTotalInvAmnt.MaxLength = 8 + currency_decimal + 1;
                txtTotalInvAmnt.NumberPrecision = 8 + currency_decimal;
                //txtTotalInvAmnt.Attributes.Remove("onkeypress");


                txtBalanceDue.Text = txtAmntPaid.Text.Replace(",", "");
                //txtBalanceDue.Attributes.Add("onblur","round(this,"+currency_decimal+")");
                txtBalanceDue.NumberScale = currency_decimal;
                txtBalanceDue.MaxLength = 8 + currency_decimal + 1;
                txtBalanceDue.NumberPrecision = 8 + currency_decimal;
                //txtBalanceDue.Attributes.Remove("onkeypress");

                txtCurrAmntPaid.Text = txtAmntPaid.Text.Replace(",", "");
                //txtCurrAmntPaid.Attributes.Add("onblur","round(this,"+currency_decimal+")");
                txtCurrAmntPaid.NumberScale = currency_decimal;
                txtCurrAmntPaid.MaxLength = 8 + currency_decimal + 1;
                txtCurrAmntPaid.NumberPrecision = 8 + currency_decimal;


                hidtxtCurrAmt.Text = txtAmntPaid.Text.Replace(",", "");
                //txtCurrAmntPaid.Attributes.Add("onblur","round(this,"+currency_decimal+")");
                hidtxtCurrAmt.NumberScale = currency_decimal;
                hidtxtCurrAmt.MaxLength = 8 + currency_decimal + 1;
                hidtxtCurrAmt.NumberPrecision = 8 + currency_decimal;
                //end currency

                hidtxtBalanceDue.Text = txtAmntPaid.Text.Replace(",", "");
                //txtBalanceDue.Attributes.Add("onblur","round(this,"+currency_decimal+")");
                hidtxtBalanceDue.NumberScale = currency_decimal;
                hidtxtBalanceDue.MaxLength = 8 + currency_decimal + 1;
                hidtxtBalanceDue.NumberPrecision = 8 + currency_decimal;
                //txtBalanceDue.Attributes.Remove("onkeypress");
            }
            catch
            {
            }
        }
        private void setInputMode(String strMode)
        {
            switch (strMode)
            {
                case "A": // approve
                    txtActualBPD.ReadOnly = false;
                    txtAmntPaid.ReadOnly = true;
                    txtAmntPaid.Attributes.Remove("onblur");
                    txtBalanceDue.ReadOnly = true;
                    txtBatchNo.ReadOnly = true;
                    txtCurrAmntPaid.ReadOnly = true;
                    txtCustID.ReadOnly = true;
                    txtInternalDueDate.ReadOnly = false;
                    txtInvNo.ReadOnly = true;
                    txtPaidDate.ReadOnly = true;
                    txtPrintedDueDate.ReadOnly = true;
                    txtPromisedPaymentDate.ReadOnly = false;
                    txtTotalInvAmnt.ReadOnly = true;
                    btnOK.Enabled = true;
                    break;
                case "L": // placed
                    txtActualBPD.ReadOnly = false;
                    txtAmntPaid.ReadOnly = false;
                    txtBalanceDue.ReadOnly = true;
                    txtBatchNo.ReadOnly = false;
                    txtCurrAmntPaid.ReadOnly = true;
                    txtCustID.ReadOnly = true;
                    txtInternalDueDate.ReadOnly = true;
                    txtInvNo.ReadOnly = true;
                    txtPaidDate.ReadOnly = false;
                    txtPrintedDueDate.ReadOnly = true;
                    txtPromisedPaymentDate.ReadOnly = false;
                    txtTotalInvAmnt.ReadOnly = true;
                    btnOK.Enabled = true;
                    break;
                case "N": // not yet approve
                    txtActualBPD.ReadOnly = true;
                    txtAmntPaid.ReadOnly = true;
                    txtAmntPaid.Attributes.Remove("onblur");
                    txtBalanceDue.ReadOnly = true;
                    txtBatchNo.ReadOnly = true;
                    txtCurrAmntPaid.ReadOnly = true;
                    txtCustID.ReadOnly = true;
                    txtInternalDueDate.ReadOnly = false;
                    txtInvNo.ReadOnly = true;
                    txtPaidDate.ReadOnly = true;
                    txtPrintedDueDate.ReadOnly = true;
                    txtPromisedPaymentDate.ReadOnly = true;
                    txtTotalInvAmnt.ReadOnly = true;
                    btnOK.Enabled = true;
                    break;
                default:
                    txtActualBPD.ReadOnly = true;
                    txtAmntPaid.ReadOnly = false;
                    //txtAmntPaid.Attributes.Remove("onblur");
                    txtBalanceDue.ReadOnly = true;
                    txtBatchNo.ReadOnly = true;
                    txtCurrAmntPaid.ReadOnly = true;
                    txtCustID.ReadOnly = true;
                    txtInternalDueDate.ReadOnly = true;
                    txtInvNo.ReadOnly = true;
                    txtPaidDate.ReadOnly = false;
                    txtPrintedDueDate.ReadOnly = true;
                    txtPromisedPaymentDate.ReadOnly = true;
                    txtTotalInvAmnt.ReadOnly = true;
                    btnOK.Enabled = true;
                    break;
            }
        }
        private void setDGAssociatedCNDN()
        {
            try
            {
                dsASCNDN = InvoicePaymentUpdateMgrDAL.getAssociatedCNDN(appID, entID, InvNo, culture);
                dgAssociatedCNDN.DataSource = dsASCNDN;
                Session["dsASCNDN"] = dsASCNDN;
                dgAssociatedCNDN.DataBind();
            }
            catch
            {
            }
        }

        private void dgAssociatedCNDN_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            try
            {
                dgAssociatedCNDN.CurrentPageIndex = e.NewPageIndex;
                dgAssociatedCNDN.DataSource = dsASCNDN;
                dgAssociatedCNDN.DataBind();
            }
            catch
            {
            }
        }

        private void dgAssociatedCNDN_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
        {
            int page, index, pagesize, page_index = 0;
            page = dgAssociatedCNDN.CurrentPageIndex;
            index = e.Item.ItemIndex;
            pagesize = dgAssociatedCNDN.PageSize;
            page_index = index + (page * pagesize);
            if (e.Item.ItemIndex > -1)
            {
                e.Item.Cells[3].Text = String.Format(strFormatCurrency, dsASCNDN.Tables[0].Rows[page_index]["total_amnt"]);
            }
        }

        private void btnOK_Click(object sender, System.EventArgs e)
        {
            ErrMsg.Text = "";
            try
            {
                if (validateForm())
                {
                    DataSet InvDS = (DataSet)Session["InvDS"];
                    Decimal dCAmntPaid = 0;

                    if (Session["dCAmntPaid"] != null)
                    {
                        dCAmntPaid = (Decimal)Session["dCAmntPaid"];
                    }
                    Decimal dDN = CreditDebitMgrDAL.getTotalDebitNote(appID, entID, InvDS.Tables[0].Rows[0]["invoice_no"].ToString().ToUpper());
                    Decimal dCN = CreditDebitMgrDAL.getTotalCreditNote(appID, entID, InvDS.Tables[0].Rows[0]["invoice_no"].ToString().ToUpper());
                    Decimal dInvAmnt = 0;
                    if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["invoice_amt"]))
                    {
                        dInvAmnt = Convert.ToDecimal(InvDS.Tables[0].Rows[0]["invoice_amt"]);
                    }

                    String Stat = InvDS.Tables[0].Rows[0]["invoice_status"].ToString().Trim().ToUpper();
                    Decimal dBalanceDue = 0;
                    DataTable dtParam = new DataTable();
                    dtParam.Columns.Add("invoice_no");//
                    dtParam.Columns.Add("invoice_status");//
                    dtParam.Columns.Add("amount_paid");//
                    dtParam.Columns.Add("batch_no");//
                    dtParam.Columns.Add("user");//
                    dtParam.Columns.Add("promised_payment_date");//
                    dtParam.Columns.Add("paid_date");//
                    dtParam.Columns.Add("internal_due_date");
                    dtParam.Columns.Add("actual_bill_placement_date");
                    DataRow drData = dtParam.NewRow();

                    drData["invoice_no"] = InvDS.Tables[0].Rows[0]["invoice_no"].ToString().ToUpper();
                    drData["user"] = utility.GetUserID();
                    if (txtBatchNo.Text.Trim() != "")
                    {
                        drData["batch_no"] = txtBatchNo.Text.Trim();
                    }
                    else
                    {
                        drData["batch_no"] = DBNull.Value;
                    }
                    if (txtInternalDueDate.Text.Trim() != "")
                    {
                        drData["internal_due_date"] = DateTime.ParseExact(txtInternalDueDate.Text.Trim(), "dd/MM/yyyy", null);
                    }
                    else
                    {
                        drData["internal_due_date"] = DBNull.Value;
                    }
                    if (txtActualBPD.Text.Trim() != "")
                    {
                        drData["actual_bill_placement_date"] = DateTime.ParseExact(txtActualBPD.Text.Trim(), "dd/MM/yyyy", null);
                    }
                    else
                    {
                        drData["actual_bill_placement_date"] = DBNull.Value;
                    }

                    if (txtAmntPaid.Text.Trim() != "")
                    {
                        drData["amount_paid"] = Convert.ToDecimal(txtAmntPaid.Text.Trim());
                        dBalanceDue = dInvAmnt + dDN - (dCN + dCAmntPaid + Convert.ToDecimal(txtAmntPaid.Text.Trim()));
                    }
                    else
                    {
                        drData["amount_paid"] = DBNull.Value;
                        dBalanceDue = dInvAmnt + dDN - (dCN + dCAmntPaid);
                    }
                    if (Stat == "L")
                    {
                        int day = 0;
                        if (txtPaidDate.Text != "")
                        {
                            DateTime dtPayment = DateTime.ParseExact(txtPaidDate.Text.Trim(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentUICulture);
                            DateTime dtToday = DateTime.Now;
                            TimeSpan span = dtPayment - dtToday;
                            day = (int)span.TotalDays;
                        }
                        //						if(day>0)
                        //						{							
                        //							drData["invoice_status"]=Stat;														
                        //						}
                        //						else
                        //						{
                        if (dBalanceDue > 0)
                        {
                            drData["invoice_status"] = Stat;

                        }
                        else
                        {
                            drData["invoice_status"] = "D";
                        }
                        //						}

                    }
                    else if (Stat == "A")
                    {
                        if (txtActualBPD.Text.Trim() != "")
                        {
                            drData["invoice_status"] = "L";
                        }
                        else
                        {
                            drData["invoice_status"] = Stat;
                        }
                    }
                    else
                    {
                        drData["invoice_status"] = Stat;
                    }

                    if (txtPromisedPaymentDate.Text.Trim() != "")
                    {
                        drData["promised_payment_date"] = DateTime.ParseExact(txtPromisedPaymentDate.Text.Trim(), "dd/MM/yyyy", null);
                    }
                    else
                    {
                        drData["promised_payment_date"] = DBNull.Value;
                    }

                    if (txtPaidDate.Text.Trim() != "")
                    {
                        drData["paid_date"] = DateTime.ParseExact(txtPaidDate.Text.Trim(), "dd/MM/yyyy", null);
                    }
                    else
                    {
                        drData["paid_date"] = DBNull.Value;
                    }
                    dtParam.Rows.Add(drData);
                    int pass = InvoicePaymentUpdateMgrDAL.updateInvoice(appID, enterpriseID, dtParam);
                    txtAmntPaid.Text = "";
                    if (pass == 0)
                    {
                        InvNo = drData["invoice_no"].ToString();
                        txtAmntPaid.Text = "";
                        txtPaidDate.Text = "";
                        txtBatchNo.Text = "";
                        getInvoiceDetail();
                        DisplayCurrentPage();
                        setDGAssociatedCNDN();
                        //						ErrMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MSG_SAVESUCCESS",utility.GetUserCulture());
                        //						Response.Write("<script language=javascript>window.close();</script>");

                        btnGenerate_Click(null, null);
                        divInvManageList.Visible = true;
                        pnlinvupdate.Visible = false;
                        lblInvDispStat.Text = "Status";
                    }


                }
            }
            catch
            {
            }

        }

        private bool validateForm()
        {
            bool pass = true;
            try
            {
                DataSet InvDS = (DataSet)Session["InvDS"];
                DateTime actBPD, internalDD, promisedPD;
                DateTime dbActBPD, dbInternalDD, dbNextBPD;
                switch (InvDS.Tables[0].Rows[0]["invoice_status"].ToString().ToUpper())
                {
                    case "A": // approve
                        if (txtInternalDueDate.Text.Trim() != "")
                        {
                            internalDD = DateTime.ParseExact(txtInternalDueDate.Text.Trim(), "dd/MM/yyyy", null);
                            if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["internal_due_date"]))
                            {
                                dbInternalDD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["internal_due_date"]);

                                //if(internalDD!=dbInternalDD)//internal DD is update
                                if (internalDD.Date != dbInternalDD.Date)//internal DD is update
                                {
                                    if (txtActualBPD.Text.Trim() != "")
                                    {
                                        ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_INDD_NOT_CHANGE", utility.GetUserCulture());
                                        return false;
                                    }

                                }
                            }
                            /*else
							{
								ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_NULL_FIELD",utility.GetUserCulture()).Replace("[field]","internal_due_date");								
								return false;
							}*/
                            if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]))
                            {
                                dbNextBPD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]);
                                if (dbNextBPD.Date > internalDD.Date)
                                {
                                    ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_DD_ELIER_NEXTBPD", utility.GetUserCulture());
                                    return false;
                                }
                            }
                            else
                            {
                                ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_NULL_FIELD", utility.GetUserCulture()).Replace("[field]", "next_bill_placement_date");
                                return false;
                            }
                        }
                        if (txtActualBPD.Text.Trim() != "")
                        {
                            actBPD = DateTime.ParseExact(txtActualBPD.Text.Trim(), "dd/MM/yyyy", null);
                            if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["actual_bill_placement_date"]))
                            {
                                dbActBPD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["actual_bill_placement_date"]);
                                if (actBPD.Date != dbActBPD.Date)//actual BPD is update
                                {
                                    if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["internal_due_date"]))
                                    {//UAT 9.2 Actual BPD can earlier than Next BPD
                                     //										dbInternalDD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["internal_due_date"]);										
                                     //										if(dbInternalDD<actBPD)
                                     //										{
                                     //											ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_ACTBPD_NOT_LATER_INDD",utility.GetUserCulture());
                                     //											return false;
                                     //										}
                                    }
                                    else
                                    {
                                        ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_NULL_FIELD", utility.GetUserCulture()).Replace("[field]", "internal_due_date");
                                        return false;
                                    }
                                    if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]))
                                    {
                                        //										dbNextBPD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]);										
                                        //										if(dbNextBPD>actBPD)
                                        //										{
                                        //											ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_ACTBPD_NOT_ELIER_NEXTBPD",utility.GetUserCulture());
                                        //											return false;
                                        //										}
                                    }
                                    else
                                    {
                                        ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_NULL_FIELD", utility.GetUserCulture()).Replace("[field]", "next_bill_placement_date");
                                        return false;
                                    }
                                }
                            }
                            else // dbActualBPD null
                            {
                                if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["internal_due_date"]))
                                {
                                    dbInternalDD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["internal_due_date"]);
                                    if (dbInternalDD.Date < actBPD.Date)
                                    {
                                        ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_ACTBPD_NOT_LATER_INDD", utility.GetUserCulture());
                                        return false;
                                    }
                                }
                                else
                                {
                                    ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_NULL_FIELD", utility.GetUserCulture()).Replace("[field]", "internal_due_date");
                                    return false;
                                }
                                if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]))
                                {
                                    //									dbNextBPD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]);										
                                    //									if(dbNextBPD>actBPD)
                                    //									{
                                    //										ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_ACTBPD_NOT_ELIER_NEXTBPD",utility.GetUserCulture());
                                    //										return false;
                                    //									}
                                }
                                else
                                {
                                    ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_NULL_FIELD", utility.GetUserCulture()).Replace("[field]", "next_bill_placement_date");
                                    return false;
                                }
                            }
                            if (txtPromisedPaymentDate.Text.Trim() != "")
                            {
                                promisedPD = DateTime.ParseExact(txtPromisedPaymentDate.Text.Trim(), "dd/MM/yyyy", null);
                                if (actBPD.Date > promisedPD.Date)
                                {
                                    ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_PPD_NOT_ELIER_ACTBPD", utility.GetUserCulture());
                                    return false;
                                }
                            }
                        }
                        else
                        {
                            if (txtPromisedPaymentDate.Text.Trim() != "")
                            {
                                ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_PPD_NOT_INPUT", utility.GetUserCulture());
                                return false;
                            }
                        }
                        break;
                    case "L": // placed
                        if (txtAmntPaid.Text.Trim() != "")
                        {
                            Decimal dCAmntPaid = 0;
                            Decimal dAmntPaid = 0;
                            Decimal dBalanceDue = 0;
                            if (Session["dCAmntPaid"] != null)
                            {
                                dCAmntPaid = (Decimal)Session["dCAmntPaid"];
                            }
                            if (Session["dBalanceDue"] != null)
                            {
                                dBalanceDue = (Decimal)Session["dBalanceDue"];
                            }
                            if (txtAmntPaid.Text.Trim() != "")
                            {
                                dAmntPaid = Convert.ToDecimal(txtAmntPaid.Text.Trim());
                                if (dAmntPaid < 0)
                                {
                                    if (Math.Abs(dAmntPaid) > dCAmntPaid)
                                    {
                                        ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_NEGATIVE_AMNT_EXCEEDS_PAID", utility.GetUserCulture());
                                        return false;
                                    }
                                }
                                if (dAmntPaid != 0)
                                {
                                    if (dAmntPaid > dBalanceDue)
                                    {
                                        ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_PAY_AMNT_EXCEEDS_BALDUE", utility.GetUserCulture());
                                        return false;
                                    }
                                    if (txtPaidDate.Text.Trim() != "")
                                    {
                                        DateTime dtPayment = DateTime.ParseExact(txtPaidDate.Text.Trim(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentUICulture);
                                        DateTime dtToday = DateTime.Now;
                                        TimeSpan span;
                                        if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["actual_bill_placement_date"]))
                                        {
                                            dbActBPD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["actual_bill_placement_date"]);
                                        }
                                        else
                                        {
                                            ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_NULL_FIELD", utility.GetUserCulture()).Replace("[field]", "actual_bill_placement_date");
                                            return false;
                                        }
                                        if (dtPayment.Day != dtToday.Day || dtPayment.Month != dtToday.Month || dtPayment.Year != dtToday.Year)
                                        {
                                            span = dtPayment - dtToday;
                                            int day = (int)span.TotalDays;
                                            if (day > 90)
                                            {
                                                ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_F_PAYMENT_MUST_IN_90_DAY", utility.GetUserCulture());
                                                return false;
                                            }
                                            if (dAmntPaid < 0)
                                            {
                                                ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_F_PAYMENT_MUST_NOT_NEGATIVE", utility.GetUserCulture());
                                                return false;
                                            }
                                            span = dtPayment - dbActBPD;
                                            day = (int)span.TotalDays;
                                            if (day < 0)
                                            {
                                                ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_PAY_DATE_ELIER_PL_DATE", utility.GetUserCulture());
                                                return false;
                                            }


                                        }
                                    }
                                    else
                                    {
                                        ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_PAY_DATE_REQ", utility.GetUserCulture());
                                        return false;
                                    }
                                }
                            }
                        }
                        if (txtActualBPD.Text.Trim() == "")
                        {
                            if (txtPromisedPaymentDate.Text.Trim() != "")
                            {
                                ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_PPD_NOT_INPUT", utility.GetUserCulture());
                                return false;
                            }
                        }
                        break;
                    case "N": // not yet approve
                        if (txtInternalDueDate.Text.Trim() != "")
                        {
                            internalDD = DateTime.ParseExact(txtInternalDueDate.Text.Trim(), "dd/MM/yyyy", null);
                            if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["internal_due_date"]))
                            {
                                dbInternalDD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["internal_due_date"]);
                                if (internalDD.Date != dbInternalDD.Date)//internal DD is update
                                {
                                    if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]))
                                    {
                                        dbNextBPD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]);
                                        if (dbNextBPD.Date > internalDD.Date)
                                        {
                                            ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_DD_NOT_ELIER_NEXTBPD", utility.GetUserCulture());
                                            return false;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (Utility.IsNotDBNull(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]))
                                {
                                    dbNextBPD = Convert.ToDateTime(InvDS.Tables[0].Rows[0]["next_bill_placement_date"]);
                                    if (dbNextBPD.Date > internalDD.Date)
                                    {
                                        ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_IPU_DD_NOT_ELIER_NEXTBPD", utility.GetUserCulture());
                                        return false;
                                    }
                                }
                                //								ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_IPU_NULL_FIELD",utility.GetUserCulture()).Replace("[field]","internal_due_date");
                                //								return false;
                            }

                        }
                        break;
                    default:
                        if (txtAmntPaid.Text != "")
                        {
                            if (Convert.ToDecimal(txtAmntPaid.Text.Trim()) >= 0)
                            {
                                ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_INV_MGR_ALLOW_ONLY_NEGATIVE_VAL", utility.GetUserCulture()); ;
                                return false;
                            }
                        }

                        int iday = 0;
                        if (txtPaidDate.Text != "")
                        {
                            DateTime dtPayment = DateTime.ParseExact(txtPaidDate.Text.Trim(), "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentUICulture);
                            DateTime dtToday = DateTime.Today;
                            TimeSpan span = dtPayment - dtToday;
                            iday = (int)span.TotalDays;
                            if (iday > 0)
                            {
                                ErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "MSG_INV_MGR_ALLOW_ONLY_BEFORE_TODAY", utility.GetUserCulture()); ;
                                return false;

                            }
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                pass = false;
            }
            return pass;
        }

        private void btnCancelinvoice_Click(object sender, System.EventArgs e)
        {
            divInvManageList.Visible = true;
            pnlinvupdate.Visible = false;
            lblInvDispStat.Text = "Status";
            txtAmntPaid.Text = "";
            txtPaidDate.Text = "";
            txtBatchNo.Text = "";

        }

        private void btnPrintInvPkg_Click(object sender, System.EventArgs e)
        {
            lblErrorMessage.Text = "";
            bool icheck = ValidateValues();

            bool foundChecked = false;
            for (int i = 0; i <= dgPreview.Items.Count - 1; i++)
            {
                CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");

                if (chkSelect.Checked)
                {
                    foundChecked = true;
                    break;
                }
            }

            if (foundChecked == false)
            {
                //lblErr.txt = ""
                return;
            }

            if (icheck == false)
            {
                String strModuleID = Request.Params["MODID"];
                DataSet dsInvoiceRP = GetInvoiceQueryData();

                String strUrl = null;
                strUrl = "ReportViewer.aspx";
                Session["FORMID"] = "InvoiceDetailPkg";
                Session["SESSION_DS1"] = dsInvoiceRP;
                ArrayList paramList = new ArrayList();
                paramList.Add(strUrl);
                String sScript = Utility.GetScript("openParentWindow.js", paramList);
                Utility.RegisterScriptString(sScript, "ShowPopupScript", this.Page);
            }
        }

        private void txtInvDateto_TextChanged(object sender, System.EventArgs e)
        {
            if (txtInvDateFrom.Text == "")
            {
                lblErrorMessage.Text = "Please enter a date rang";
                return;
            }
            if (txtInvDateFrom.Text != "" && txtInvDateto.Text != "")
                LoadCustomerAccount();
        }

        private void txtInvoiceNo_TextChanged(object sender, System.EventArgs e)
        {
            if (txtInvoiceNo.Text != "")
                LoadCustomerAccount();
        }

        private void txtDueDateto_TextChanged(object sender, System.EventArgs e)
        {
            if (txtDueDateFrom.Text == "")
            {
                lblErrorMessage.Text = "Please enter a date rang";
                return;
            }
            if (txtDueDateFrom.Text != "" && txtDueDateto.Text != "")
                LoadCustomerAccount();
        }

        private void ddlInvType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadCustomerAccount();
        }

        private void ddlInvStatus_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadCustomerAccount();
        }

        private void dgEditDtl_SelectedIndexChanged(object sender, System.EventArgs e)
        {

        }

        private void ddbCustomerAcc_SelectedIndexChanged_1(object sender, System.EventArgs e)
        {
            Customer customer = new Customer();
            customer.Populate(appID, enterpriseID, ddbCustomerAcc.SelectedItem.Text);
            txtCustName.Text = customer.CustomerName;
        }

        private void ExecuteQuery_ServerClick(object sender, System.EventArgs e)
        {
            lblErrorMessage.Text = "test";
        }

        private void ExecuteCheckBox_ServerClick(object sender, System.EventArgs e)
        {
        }
        private void txtExcludeCustomerList_TextChanged(object sender, System.EventArgs e)
        {
            if (IsPostBack)
            {
                if (txtExcludeCustomerList.Visible)
                {
                    if (txtExcludeCustomerList.Text != "Y")
                    {
                        DataSet dsInvoiceNumberList = GetInvoiceQueryData();
                        if (dsInvoiceNumberList.Tables[0].Rows.Count > 0)
                        {
                            string userID = utility.GetUserID();

                            lblErrorMsg.Text = "";

                            DataSet result = new DataSet();
                            string invoice_list = "";
                            string exclude_customer_list = txtExcludeCustomerList.Text;
                            for (int i = 0; i < dsInvoiceNumberList.Tables[0].Rows.Count; i++)
                            {
                                string strInvoiceNumber = dsInvoiceNumberList.Tables[0].Rows[i]["invoice_no"].ToString();
                                invoice_list += strInvoiceNumber;
                                if (i != dsInvoiceNumberList.Tables[0].Rows.Count - 1)
                                    invoice_list += ",";
                                //InvoiceManagementMgrDAL.UpdateExported(this.appID, this.enterpriseID, strInvoiceNumber, m_strUserID);						
                                //InvoiceManagementMgrDAL.UpdateInvoiceAmt(this.appID, this.enterpriseID, strInvoiceNumber, m_strUserID);						
                            }
                            //btnApproveSelected_Click(sender, e);
                            result = InvoiceManagementMgrDAL.GetCSVExport(this.appID, this.enterpriseID, exclude_customer_list, invoice_list, m_strUserID);
                            string file_name = "AccPac_" + DateTime.Now.Year.ToString() +
                                (DateTime.Now.Month.ToString().Length == 1 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString()) +
                                (DateTime.Now.Day.ToString().Length == 1 ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString()) +
                                (DateTime.Now.Hour.ToString().Length == 1 ? "0" + DateTime.Now.Hour.ToString() : DateTime.Now.Hour.ToString()) +
                                (DateTime.Now.Minute.ToString().Length == 1 ? "0" + DateTime.Now.Minute.ToString() : DateTime.Now.Minute.ToString());
                            ExportCSVFile(result.Tables[0], file_name);
                            BindGridPriview();
                            ActiveDIVGrid();
                            btnExport.Enabled = false;
                            btnCancelSelected.Enabled = false;
                            txtExcludeCustomerList.Text = "";
                            txtExcludeCustomerList.Visible = false;
                        }
                    }
                }
            }
        }
    }
}
