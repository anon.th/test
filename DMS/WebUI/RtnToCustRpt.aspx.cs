using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;
using com.common.RBAC;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for RtnToCustRpt.
	/// </summary>
	public class RtnToCustRpt : BasePage
	{
		
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnCancelQry;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.RadioButton rbBookingDate;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbLongRoute;
		protected System.Web.UI.WebControls.RadioButton rbShortRoute;
		protected System.Web.UI.WebControls.RadioButton rbAirRoute;
		protected System.Web.UI.WebControls.Label lblConsignmentNo;
		protected System.Web.UI.WebControls.TextBox txtConsignmentNo;
		protected System.Web.UI.WebControls.Label lblStatusCode;
		protected com.common.util.msTextBox txtStatusCode;
		protected com.common.util.msTextBox txtExceptionCode;
		protected System.Web.UI.WebControls.Label lblShipmentClosed;
		protected System.Web.UI.WebControls.RadioButton rbShipmentYes;
		protected System.Web.UI.WebControls.RadioButton rbShipmentNo;
		protected System.Web.UI.WebControls.RadioButton rbInvoiceYes;
		protected System.Web.UI.WebControls.Label lblPayerCode;
		protected System.Web.UI.WebControls.TextBox txtPayerCode;
		protected System.Web.UI.WebControls.Button btnPayerCode;
		protected System.Web.UI.WebControls.Button btnStatusCode;
		protected System.Web.UI.WebControls.Button btnExceptionCode;
		protected System.Web.UI.WebControls.Label lblZipCode;
		protected com.common.util.msTextBox txtZipCode;
		protected System.Web.UI.WebControls.Button btnZipCode;
		protected System.Web.UI.WebControls.Label lblStateCode;
		protected com.common.util.msTextBox txtStateCode;
		protected System.Web.UI.WebControls.Button btnStateCode;
		protected System.Web.UI.WebControls.RadioButton rbInvoiceNo;
		protected System.Web.UI.WebControls.Label lblInvoiceIssued;
		protected System.Web.UI.WebControls.Label lblBookingType;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.TextBox txtPeriod2;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		protected System.Web.UI.HtmlControls.HtmlTable tblPayerType;
		protected System.Web.UI.HtmlControls.HtmlTable tblRouteType;
		protected System.Web.UI.HtmlControls.HtmlTable tblDestination;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipment;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.Label lblPayerType;
		protected System.Web.UI.WebControls.Label lblRouteType;
		protected System.Web.UI.WebControls.Label lblDestination;
		protected System.Web.UI.WebControls.Label lblShipment;
		protected com.common.util.msTextBox txtBookingNo;
		protected System.Web.UI.WebControls.Label lblExceptionCode;
		protected System.Web.UI.WebControls.Label lblBookingNo;
		protected System.Web.UI.WebControls.Label lblRouteCode;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.RadioButton rbPkDate;
		protected System.Web.UI.WebControls.DropDownList Drp_BookingType;
		protected System.Web.UI.WebControls.ListBox lsbCustType;
		protected System.Web.UI.HtmlControls.HtmlTable tblInternal;
		protected System.Web.UI.WebControls.Button btnStateCodeCust;
		protected com.common.util.msTextBox txtStateCodeCust;
		protected System.Web.UI.WebControls.Button btnZipCodeCust;
		protected com.common.util.msTextBox txtZipCodeCust;
		protected System.Web.UI.WebControls.Label Label8;
		protected com.common.util.msTextBox txtDateCust;
		protected System.Web.UI.WebControls.RadioButton rbDateCust;
		protected com.common.util.msTextBox txtToCust;
		protected com.common.util.msTextBox txtPeriodCust;
		protected System.Web.UI.WebControls.RadioButton rbPeriodCust;
		protected com.common.util.msTextBox txtYearCust;
		protected System.Web.UI.WebControls.DropDownList ddMonthCust;
		protected System.Web.UI.WebControls.RadioButton rbMonthCust;
		protected System.Web.UI.HtmlControls.HtmlTable tblExternal;
		protected System.Web.UI.HtmlControls.HtmlTable Table3;
		protected System.Web.UI.HtmlControls.HtmlTable Table6;
		protected System.Web.UI.WebControls.Label lblDateCust;
		protected System.Web.UI.WebControls.Label lblPostalCodeCust;
		protected System.Web.UI.WebControls.Label lblStateCust;
		protected System.Web.UI.WebControls.Label lblYearCust;
		protected System.Web.UI.WebControls.RadioButton rbBookingDateCust;
		protected System.Web.UI.WebControls.RadioButton rbPickupDateCust;
		protected System.Web.UI.WebControls.RadioButton rbStatusDateCust;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.RadioButton rbCheckAllYes;
		protected System.Web.UI.WebControls.RadioButton rbCheckAllNo;
		protected System.Web.UI.WebControls.RadioButton rbStatusDate;

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.btnPayerCode.Click += new System.EventHandler(this.btnPayerCode_Click);
			this.btnStatusCode.Click += new System.EventHandler(this.btnStatusCode_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		String m_strAppID=null;
		String m_strEnterpriseID=null;
		String m_strCulture=null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			com.common.classes.User user = RBACManager.GetUser(m_strAppID, m_strEnterpriseID, (String)Session["userID"]);
			ViewState["usertype"] = user.UserType;
			ViewState["PayerID"] = user.PayerID;

			if ((String)ViewState["usertype"]  == "C")
			{
				tblInternal.Visible = false;
			}
			else
			{
				tblInternal.Visible = true;
			}

			if(!Page.IsPostBack)
			{
				DefaultScreen();
				ddlmonths();
				Session["toRefresh"]=false;
			}

		}


		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{

			//btnExecuteQuery.Enabled = false;
			lblErrorMessage.Text = "";
			int intChk = 0;
			intChk = ValidateData();
			if(intChk < 1)
			{
				return;
			}
			String strModuleID = Request.Params["MODID"];
			DataSet dsShipment = GetShipmentQueryData();

			
			String strUrl = null;
			//String sFeatures = "'height=700,width=900,left=20,top=5,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
			strUrl = "ReportViewer.aspx";
			//String strFeatures = "'height=700,width=900,left=20,top=5,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
			Session["FORMID"] = "RtnToCustRpt";
			Session["SESSION_DS1"] = dsShipment;
			//OpenWindowpage(strUrl, strFeatures);
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);

			//check for module id before opening another web form
			//			if (strModuleID.Equals("03ShipmentTracking"))
			//			{
			//				String strUrl = null;
			//				SessionDS sessionds = new SessionDS();
			//				sessionds = ShipmentTrackingMgrDAL.QueryShipmentTracking(dsShipment, m_strAppID, m_strEnterpriseID, 0, 10);
			//				strUrl = "ShipmentTracking.aspx?FORMID=TRUE_CHECK";
			//				//String strFeatures = "'height=500,width=900,left=30,top=30,location=no,menubar=no,scrollbars=yes,resizable=yes,status=yes,titlebar=no,toolbar=no'";
			//				Session["SESSION_DS1"] = sessionds;
			//				Session["QUERY_DS"] = dsShipment;
			//				ArrayList paramList = new ArrayList();
			//				paramList.Add(strUrl);
			//				String sScript = Utility.GetScript("openParentWindow.js",paramList);
			//				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			//			}
			//			else
			//			{
			//				String strUrl = null;
			//				//String sFeatures = "'height=700,width=900,left=20,top=5,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
			//				strUrl = "ReportViewer.aspx";
			//				//String strFeatures = "'height=700,width=900,left=20,top=5,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no'";
			//				Session["FORMID"] = "ShipmentTrackingQuery";
			//				Session["SESSION_DS1"] = dsShipment;
			//				//OpenWindowpage(strUrl, strFeatures);
			//				ArrayList paramList = new ArrayList();
			//				paramList.Add(strUrl);
			//				String sScript = Utility.GetScript("openParentWindow.js",paramList);
			//				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			//			}

		}


		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}


		#region "Page Functions"

		private DataTable CreateEmptyDataTable()
		{
			DataTable dtShipment = new DataTable();

			#region "Dates"
			dtShipment.Columns.Add(new DataColumn("tran_date", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
			dtShipment.Columns.Add(new DataColumn("end_date", typeof(DateTime)));
			#endregion
			
			#region "Payer Type"
			dtShipment.Columns.Add(new DataColumn("payer_code", typeof(string)));
			#endregion

			#region "Shipment"
			dtShipment.Columns.Add(new DataColumn("consignment_no", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("status_code", typeof(string)));
			#endregion

			#region "Report To Show"
			dtShipment.Columns.Add(new DataColumn("RptType", typeof(string)));
			#endregion

			return dtShipment;
		}


		private DataSet GetShipmentQueryData()
		{
			DataSet dsShipment = new DataSet();
			DataTable dtShipment = CreateEmptyDataTable();
			DataRow dr = dtShipment.NewRow(); 
			
			#region "Dates"

			if ((String)ViewState["usertype"] != "C")
			{
				if (rbStatusDate.Checked) 
				{	// Status Date Selected
					dr["tran_date"] = "S";
				}
			
				string strMonth =null;

				if (rbMonth.Checked == true) 
				{	// Month Selected
					strMonth = ddMonth.SelectedItem.Value;
					if (strMonth != "" && txtYear.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact ("01/"+strMonth.ToString()+"/"+txtYear.Text, "dd/MM/yyyy", null);
						int intLastDay = 0;
						intLastDay = LastDayOfMonth(strMonth, txtYear.Text);
						dr["end_date"] = DateTime.ParseExact (intLastDay.ToString()+"/"+strMonth.ToString()+"/"+txtYear.Text+" 23:59", "dd/MM/yyyy HH:mm", null);
					}
				}
				if (rbPeriod.Checked == true) 
				{	// Period Selected
					if (txtPeriod.Text != ""  && txtTo.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact(txtPeriod.Text ,"dd/MM/yyyy",null);
						dr["end_date"] = DateTime.ParseExact(txtTo.Text+" 23:59" ,"dd/MM/yyyy HH:mm",null);
					}
				}
				if (rbDate.Checked == true) 
				{	// Date Selected
					if (txtDate.Text != "") 
					{
						dr["start_date"] = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy",null);
						dr["end_date"] = DateTime.ParseExact(txtDate.Text+" 23:59", "dd/MM/yyyy HH:mm",null);
					}
				}
			}
			else
			{
				;
			}
		
			#endregion

			#region "Payer Type"
			if ((String)ViewState["usertype"]  == "C")
				dr["payer_code"] = (String)ViewState["PayerID"];
			else
				dr["payer_code"] = txtPayerCode.Text.Trim();

			#endregion

			#region "Shipment"
			dr["consignment_no"] = txtConsignmentNo.Text;
			dr["status_code"] = txtStatusCode.Text;
			#endregion
			#region "Report To Show"

			if ((String)ViewState["usertype"] != "C")
			{
				dr["RptType"] = "N";
			}
			else
			{
				dr["RptType"] = "C";
			}

			#endregion

			dtShipment.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipment);
			return dsShipment;
		}


		private void DefaultScreen()
		{
			#region "Dates"

			rbStatusDate.Checked = true;
			rbMonth.Checked = true;
			rbPeriod.Checked = false;
			rbDate.Checked = false;

			ddMonth.Enabled = true;
			ddMonth.SelectedIndex = -1;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;

			#endregion

			#region "Payer Type"
			txtPayerCode.Text = null;

			#endregion

			#region "Shipment"

			txtConsignmentNo.Text = null;
			txtStatusCode.Text = null;
			#endregion
			
			lblErrorMessage.Text = "";
		}


		private int ValidateData()
		{
			if (txtYear.Text != "" )
			{
				if (Convert.ToInt32(txtYear.Text) < 2000 || Convert.ToInt32(txtYear.Text) > 2099)
				{
					lblErrorMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"YEAR_00_99",utility.GetUserCulture());
					return -1;
				}
			}
			if(txtStatusCode.Text == "")
			{
				lblErrorMessage.Text = "Please Enter status Code";
				return -1;
			}
			return 1;
		}
		

		private int LastDayOfMonth(String strMonth, String strYear)
		{
			int intLastDay = 0;
			switch (strMonth)
			{
				case "01":
				case "03":
				case "05":
				case "07":
				case "08":
				case "10":
				case "12":
					intLastDay = 31;
					break;
				case "04":
				case "06":
				case "09":
				case "11":
					intLastDay = 30;
					break;
				case "02":
					//intLastDay = 28;
					//by ching 19/02/2008
					DateTime dt = System.DateTime.ParseExact("01/02/"+strYear,"dd/MM/yyyy",null);
					DateTime firstDayOfThisMonth = dt.Subtract(TimeSpan.FromDays(dt.Day - 1));
					DateTime firstDayOfNextMonth = firstDayOfThisMonth.AddMonths(1);
					DateTime lastDayOfThisMonth = firstDayOfNextMonth.Subtract(TimeSpan.FromDays(1)) ;

					intLastDay = Int32.Parse(lastDayOfThisMonth.Day.ToString());
					break;
			}
			return intLastDay;
		}


		private void OpenWindowpage(String strUrl, String strFeatures)
		{
			
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion


		#region "Dates Part : Controls"

		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}


		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = true;
		}

		
		#endregion

		
		#region "Payer Type Part : Controls

		private void btnPayerCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "CustomerPopup.aspx?FORMID="+"ShipmentTrackingQueryRpt"+
				"&CustType="+"";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion

		#region "Shipment : Controls"

		private void btnStatusCode_Click(object sender, System.EventArgs e)
		{
			String sUrl = "StatusCodePopup.aspx?FORMID=ShipmentTrackingQueryRpt"+"&STATUSCODE="+txtStatusCode.Text+"&EXCEPTCODE="+"";
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		#endregion


		#region "Dates Part : DropDownList"

		private void ddlmonths()
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);

			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);

			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}


		#endregion
	}
}