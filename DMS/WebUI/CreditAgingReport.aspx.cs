using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CreditAgingReport.
	/// </summary>
	public class CreditAgingReport : BasePage
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.RadioButton rbDateInvoice;
		protected System.Web.UI.WebControls.RadioButton rbBPDate;
		protected System.Web.UI.WebControls.RadioButton rbFirstBPDate;
		protected System.Web.UI.WebControls.RadioButton rbDueDate;
		protected System.Web.UI.WebControls.RadioButton rbActive;
		protected System.Web.UI.WebControls.RadioButton rbInactive;
		protected System.Web.UI.WebControls.RadioButton rbBoth;
		protected System.Web.UI.WebControls.Label lblSalesMan;
		protected com.common.util.msTextBox txtSalesmanID;
		protected System.Web.UI.WebControls.Label lblCustID;
		protected com.common.util.msTextBox txtCustID;
		protected System.Web.UI.WebControls.Label lblMasterAcc;
		protected Cambro.Web.DbCombo.DbCombo DbComboMasterAccount;
		protected System.Web.UI.WebControls.Button btnDisplayCustDtls;
		protected System.Web.UI.WebControls.Button btnDisplaySalesDtls;
		protected System.Web.UI.WebControls.RadioButton rbSummary;
		protected System.Web.UI.WebControls.RadioButton rbDetail;
		protected System.Web.UI.HtmlControls.HtmlTable tblInternal;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			DbComboMasterAccount.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			if(!Page.IsPostBack)
			{
				DefaultScreen();
			}			
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnDisplaySalesDtls.Click += new System.EventHandler(this.btnDisplaySalesDtls_Click);
			this.btnDisplayCustDtls.Click += new System.EventHandler(this.btnDisplayCustDtls_Click);
			this.rbSummary.CheckedChanged += new System.EventHandler(this.rbSummary_CheckedChanged);
			this.rbDetail.CheckedChanged += new System.EventHandler(this.rbDetail_CheckedChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private String m_strAppID;
		private String m_strEnterpriseID;
		private String m_strCulture;
		DataSet m_dsQuery=null;

		private void btnDisplayCustDtls_Click(object sender, System.EventArgs e)
		{		
			poppulateCustomerAddress();
		}
		private void DefaultScreen()
		{
			#region "Dates"
			this.rbDateInvoice.Checked = false;
			this.rbBPDate.Checked = false;
			this.rbFirstBPDate.Checked = false;
			this.rbDueDate.Checked = true;
			#endregion

			#region "Account"
			this.rbActive.Checked = true;
			this.rbInactive.Checked = false;
			this.rbBoth.Checked = false;			
			#endregion

			#region "Summary or detail"
			this.rbSummary.Checked = true;
			this.rbDetail.Checked = false;		
			#endregion

			#region "ID"
			this.txtSalesmanID.Text = "";
			this.txtCustID.Text = "";
			this.btnDisplayCustDtls.Enabled = false;
			#endregion

			#region "Master Account"
			this.DbComboMasterAccount.Text = "";
			this.DbComboMasterAccount.Value = "";
			#endregion

			lblErrorMessage.Text = "";
		}

		private void poppulateCustomerAddress()
		{
			String custID = txtCustID.Text;
			String strSalesmanID = txtSalesmanID.Text;
			String sUrl = "CustomerPopup.aspx?FORMID="+"CreditAgingReport"+
				"&CustType=C&CustID="+custID+"&SalesID="+strSalesmanID;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnDisplaySalesDtls_Click(object sender, System.EventArgs e)
		{
			String strSalesmanID = txtSalesmanID.Text;
			String sUrl = "SalesPersonPopup.aspx?FORMID=CreditAgingReport&QUERY_SALESMANID="+strSalesmanID;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openWindowParam.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
		public static object MasterAccountServerMethod(Cambro.Web.DbCombo.ServerMethodArgs args)
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			String strAppID = util.GetAppID();
			String strEnterpriseID = util.GetEnterpriseID();
			DataSet dataset = com.ties.classes.DbComboDAL.MasterAccountQuery(strAppID,strEnterpriseID,args);
			return dataset;
		}

		private void rbSummary_CheckedChanged(object sender, System.EventArgs e)
		{
			txtCustID.ReadOnly = true;
			btnDisplayCustDtls.Enabled = false;
		}

		private void rbDetail_CheckedChanged(object sender, System.EventArgs e)
		{
			txtCustID.ReadOnly = false;
			btnDisplayCustDtls.Enabled = true;
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text="";
			Session["FORMID"]="CREDIT AGING REPORT"; 	
			setParamDataset();			

			lblErrorMessage.Text="";
			String strUrl = null;
			strUrl = "ReportViewer.aspx";
			OpenWindowpage(strUrl);			
		}

		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openParentWindowReport.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}
		private void setParamDataset()
		{
			int reportType = 0;
			int dueDate = 0;
			String accountStatus = null;
			String salesmanID = null;
			String custID = null;
			String masterAccount = null;

			if(rbSummary.Checked == true)
				reportType = 1;
			else if(rbDetail.Checked == true)
				reportType = 2;

			if(rbDateInvoice.Checked == true)
				dueDate = 1;
			else if(rbBPDate.Checked == true)
				dueDate = 2;
			else if(rbFirstBPDate.Checked == true)
				dueDate = 3;
			else if(rbDueDate.Checked == true)
				dueDate = 4;

			if(rbActive.Checked == true)
				accountStatus = "Y";
			else if(rbInactive.Checked == true)
				accountStatus = "N";
			//else if(rbBoth.Checked == true)
				//accountStatus = "";

			if(txtSalesmanID.Text != null && txtSalesmanID.Text != "")
				salesmanID = txtSalesmanID.Text;

			if(txtCustID.Text != null && txtCustID.Text != "")
				custID = txtCustID.Text;

			if(DbComboMasterAccount.Value != null && DbComboMasterAccount.Value != "")
				masterAccount = DbComboMasterAccount.Value;

			DataSet m_dsQuery =  new DataSet();
			DataTable dtParam = new DataTable();

			dtParam.Columns.Add("applicationId",typeof(string));
			dtParam.Columns.Add("enterpriseId",typeof(string));
			dtParam.Columns.Add("custid",typeof(string));
			dtParam.Columns.Add("salesmanid",typeof(string));
			dtParam.Columns.Add("status_active",typeof(string));
			dtParam.Columns.Add("master_account",typeof(string));
			dtParam.Columns.Add("ref_date_no",typeof(int));
			dtParam.Columns.Add("isSummary",typeof(bool));
			dtParam.Columns.Add("isReport",typeof(bool));	
			
			DataRow drEach = dtParam.NewRow();			

			drEach["applicationId"] = utility.GetAppID();
			drEach["enterpriseId"] = utility.GetEnterpriseID();
			drEach["custid"] = custID ;
			drEach["salesmanid"] = salesmanID;
			drEach["status_active"] = accountStatus;
			drEach["master_account"] = masterAccount;
			drEach["ref_date_no"] = dueDate;
			drEach["isSummary"] = rbSummary.Checked;
			drEach["isReport"] = true;

			m_dsQuery.Tables.Add(dtParam) ; 
			m_dsQuery.Tables[0].Rows.Add(drEach);
			Session["SESSION_DS1"] = m_dsQuery;
			Session["REPORTTYPE"] = reportType;
		}
		
		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}
		private DataTable CreateEmptyDataTable()
		{
			DataTable dtCreditAging = new DataTable();

			#region "Dates"
			dtCreditAging.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
			dtCreditAging.Columns.Add(new DataColumn("end_date", typeof(DateTime)));
			#endregion
			
			#region "Payer Type"
			dtCreditAging.Columns.Add(new DataColumn("custid", typeof(string)));
			dtCreditAging.Columns.Add(new DataColumn("custname", typeof(string)));
			dtCreditAging.Columns.Add(new DataColumn("invoice_no", typeof(string)));
			dtCreditAging.Columns.Add(new DataColumn("p1", typeof(float)));
			dtCreditAging.Columns.Add(new DataColumn("p2", typeof(float)));
			dtCreditAging.Columns.Add(new DataColumn("p3", typeof(float)));
			#endregion
		
			#region "Route / DC Selection"
			dtCreditAging.Columns.Add(new DataColumn("route_type", typeof(string)));
			dtCreditAging.Columns.Add(new DataColumn("route_code", typeof(string)));
			dtCreditAging.Columns.Add(new DataColumn("origin_dc", typeof(string)));
			dtCreditAging.Columns.Add(new DataColumn("destination_dc", typeof(string)));
			dtCreditAging.Columns.Add(new DataColumn("delPath_origin_dc", typeof(string)));
			dtCreditAging.Columns.Add(new DataColumn("delPath_destination_dc", typeof(string)));
			#endregion

			#region "Destination"
			dtCreditAging.Columns.Add(new DataColumn("zip_code", typeof(string)));
			dtCreditAging.Columns.Add(new DataColumn("state_code", typeof(string)));
			#endregion

			#region "Shipment Allocation Cost Based On"
			dtCreditAging.Columns.Add(new DataColumn("sa_cost_method", typeof(string)));
			#endregion

			#region "Line Haul Cost Based On"
			dtCreditAging.Columns.Add(new DataColumn("lh_cost_method", typeof(string)));
			#endregion

			return dtCreditAging;
		}
	}
}