<%@ Page language="c#" Codebehind="RouteCodePopup.aspx.cs" AutoEventWireup="false" Inherits="com.ties.RouteCodePopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>RouteCodePopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="RouteCodePopup" method="post" runat="server">
			<asp:datagrid id="dgRouteMaster" style="Z-INDEX: 102; POSITION: absolute; TOP: 96px; LEFT: 45px"
				runat="server" AllowCustomPaging="True" CssClass="gridHeading" BorderStyle="None" BorderWidth="1px"
				CellPadding="3" BackColor="White" BorderColor="#CCCCCC" AutoGenerateColumns="False" Width="598px"
				AllowPaging="True" OnSelectedIndexChanged="dgRouteMaster_SelectedIndexChanged" OnPageIndexChanged="Paging">
				<SelectedItemStyle Font-Bold="True" ForeColor="White" CssClass="drilldownFieldSelected"></SelectedItemStyle>
				<ItemStyle CssClass="drilldownField"></ItemStyle>
				<HeaderStyle Width="10%" CssClass="drilldownHeading"></HeaderStyle>
				<FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
				<Columns>
					<asp:BoundColumn DataField="DC" HeaderText="Origin DC"></asp:BoundColumn>
					<asp:BoundColumn DataField="route_code" HeaderText="Route Code"></asp:BoundColumn>
					<asp:BoundColumn DataField="route_description" HeaderText="Path Description"></asp:BoundColumn>
					<asp:ButtonColumn Text="Select" CommandName="Select"></asp:ButtonColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" ForeColor="#000066"
					BackColor="White" CssClass="normalText"></PagerStyle>
			</asp:datagrid><asp:textbox id="txtPathCode" style="Z-INDEX: 107; POSITION: absolute; TOP: 63px; LEFT: 45px"
				runat="server" CssClass="textField" Width="116px" Height="19px"></asp:textbox><asp:label id="lblPathCode" style="Z-INDEX: 106; POSITION: absolute; TOP: 38px; LEFT: 45px"
				runat="server" CssClass="tableLabel" Width="154px" Height="16px" Font-Bold="True">Route Code</asp:label><asp:button id="btnSearch" style="Z-INDEX: 105; POSITION: absolute; TOP: 63px; LEFT: 166px"
				runat="server" CssClass="buttonProp" Width="78" Height="21" Font-Bold="True" CausesValidation="False" Text="Search"></asp:button><asp:button id="btnOk" style="Z-INDEX: 101; POSITION: absolute; TOP: 63px; LEFT: 245px" runat="server"
				CssClass="buttonProp" Width="78px" Height="21px" Font-Bold="True" CausesValidation="False" Text="Close"></asp:button></form>
	</body>
</HTML>
