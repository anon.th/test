using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using TIESDAL;

namespace TIES.WebUI
{
	public class CustomerProcessingAccess : BasePage
	{
		
		#region Web Form Designer generated code
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected com.common.util.msTextBox txtUserID;
		protected System.Web.UI.WebControls.DataGrid gridCusProAcc;
		protected System.Web.UI.WebControls.Label lbError;
		protected System.Web.UI.WebControls.Label lbCustomerID;
		protected System.Web.UI.WebControls.Label lbBack;
		protected com.common.util.msTextBox txtCustomerID;
		protected System.Web.UI.WebControls.Label lbItemUserID;
		protected System.Web.UI.WebControls.Label lbUserID;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.gridCusProAcc.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.gridCusProAcc_ItemCommand);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private string appID = string.Empty;
		private string enterpriseID = string.Empty;
		private string userLoggin = string.Empty;

		public DataSet dsCustProcAcc
		{
			get
			{
				if(ViewState["dsCustProcAcc"] == null)
					return new DataSet();
				else
					return (DataSet)ViewState["dsCustProcAcc"];
			}
			set
			{
				ViewState["dsCustProcAcc"] = value;
			} 
		}

		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				System.Text.StringBuilder s = new System.Text.StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("SetScrollPosition();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}

		private void InitLoad()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("userid");
			dt.Columns.Add("user_name");
			dt.Columns.Add("custid");
			dt.Columns.Add("cust_name");

			dt.AcceptChanges();

			gridCusProAcc.EditItemIndex = -1;
			gridCusProAcc.CurrentPageIndex = 0;

			gridCusProAcc.DataSource = dt;
			gridCusProAcc.DataBind();
		}
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings, Page.Session);
				appID = utility.GetAppID();
				enterpriseID = utility.GetEnterpriseID();
				userLoggin = utility.GetUserID();

				if(!IsPostBack)
				{
					InitLoad();
					SetInitialFocus(txtUserID);
				}
			}
			catch(Exception ex)
			{
				this.lbError.Text = string.Format("{0}|{1}", ex.Message, ex.InnerException);
			}
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			try
			{
				InitLoad();

				lbError.Text = string.Empty;
				txtUserID.Text = string.Empty;
				txtCustomerID.Text = string.Empty;

				SetInitialFocus(txtUserID);
			}
			catch(Exception ex)
			{
				lbError.Text = ex.Message + "|" + ex.InnerException;
			}
            btnExecuteQuery.Enabled = true;
        }

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			try
			{
				CustomerProcessingAccessDAL db = new CustomerProcessingAccessDAL(this.appID, this.enterpriseID);
				TIESClasses.CustomerProcessingAccess objInfo = new TIESClasses.CustomerProcessingAccess();
				objInfo.EnterpriseId = this.enterpriseID;
				objInfo.Action = "0";
				objInfo.UserId = txtUserID.Text.Trim();
				objInfo.CustId = txtCustomerID.Text.Trim();
                btnExecuteQuery.Enabled = false;
				
				dsCustProcAcc = db.ExecCSS_CustomerProcessingAccess(objInfo);

				if(dsCustProcAcc.Tables[0].Rows[0][0].ToString().Trim().Equals("0"))
				{
					gridCusProAcc.DataSource = dsCustProcAcc.Tables[1];
					gridCusProAcc.DataBind();

					lbError.Text = string.Format("{0} rows returned", dsCustProcAcc.Tables[2].Rows[0][0].ToString().Trim());
				}else
					lbError.Text = dsCustProcAcc.Tables[0].Rows[0][2].ToString().Trim();
			}
			catch(Exception ex)
			{
				lbError.Text = ex.Message + "|" + ex.InnerException;
			}
		}

		private void gridCusProAcc_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			string command = e.CommandName;

			lbError.Text = string.Empty;

			if(command.Equals("ADD_ITEM"))
			{
				#region ADD_ITEM
				try
				{
					msTextBox txtAddUserId = (msTextBox)e.Item.FindControl("txtAddUserId");
					msTextBox txtAddCustID = (msTextBox)e.Item.FindControl("txtAddCustID");

					CustomerProcessingAccessDAL db = new CustomerProcessingAccessDAL(this.appID, this.enterpriseID);
					TIESClasses.CustomerProcessingAccess objInfo = new TIESClasses.CustomerProcessingAccess();
					objInfo.EnterpriseId = this.enterpriseID;
					objInfo.Action = "1";
					objInfo.UserId = txtAddUserId.Text.Trim();
					objInfo.CustId = txtAddCustID.Text.Trim();

					dsCustProcAcc = db.ExecCSS_CustomerProcessingAccess(objInfo);

					if(dsCustProcAcc.Tables[0].Rows[0][0].ToString().Trim().Equals("0"))
					{
						gridCusProAcc.DataSource = dsCustProcAcc.Tables[1];
						gridCusProAcc.DataBind();

						lbError.Text = dsCustProcAcc.Tables[0].Rows[0][2].ToString().Trim();
					}
					else
						lbError.Text = dsCustProcAcc.Tables[0].Rows[0][2].ToString().Trim();
				}
				catch(Exception ex)
				{
					lbError.Text = ex.Message + "|" + ex.InnerException;
				}
				#endregion
			}
			else if(command.Equals("DELETE_ITEM"))
			{
				#region DELETE_ITEM
				try
				{
					Label lbItemUserID = (Label)e.Item.FindControl("lbItemUserID");
					Label lbItemCustID = (Label)e.Item.FindControl("lbItemCustID");

					CustomerProcessingAccessDAL db = new CustomerProcessingAccessDAL(this.appID, this.enterpriseID);
					TIESClasses.CustomerProcessingAccess objInfo = new TIESClasses.CustomerProcessingAccess();
					objInfo.EnterpriseId = this.enterpriseID;
					objInfo.Action = "2";
					objInfo.UserId = lbItemUserID.Text.Trim();
					objInfo.CustId = lbItemCustID.Text.Trim();

					dsCustProcAcc = db.ExecCSS_CustomerProcessingAccess(objInfo);

					if(dsCustProcAcc.Tables[0].Rows[0][0].ToString().Trim().Equals("0"))
					{
						gridCusProAcc.DataSource = dsCustProcAcc.Tables[1];
						gridCusProAcc.DataBind();

						lbError.Text = dsCustProcAcc.Tables[0].Rows[0][2].ToString().Trim();
					}
					else
						lbError.Text = dsCustProcAcc.Tables[0].Rows[0][2].ToString().Trim();
				}
				catch(Exception ex)
				{
					lbError.Text = ex.Message + "|" + ex.InnerException;
				}
				#endregion
			}
		}		
	}
}
