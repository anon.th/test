<%@ Page language="c#" Codebehind="RetrieveCustomerConsignments.aspx.cs" ValidateRequest="false" AutoEventWireup="false" Inherits="TIES.WebUI.RetrieveCustomerConsignments" %>
<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Retrieve Customer Consignments</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
			function funcDisBack(){
				history.go(+1);
			}		
			
			function validate(evt) {
				var theEvent = evt || window.event;
				var key = theEvent.keyCode || theEvent.which;
				key = String.fromCharCode( key );
				var regex = /[0-9]/;
				if( !regex.test(key) ) {
					theEvent.returnValue = false;
					if(theEvent.preventDefault) theEvent.preventDefault();
				}
			}
			
			function validateInvoice(evt) {
				var theEvent = evt || window.event;
				var key = theEvent.keyCode || theEvent.which;
				key = String.fromCharCode( key );
				var regex = /[,0-9]/;
				if( !regex.test(key) ) {
					theEvent.returnValue = false;
					if(theEvent.preventDefault) theEvent.preventDefault();
				}
			}
			
			function AfterPasteInvoiceNo(obj) {
				setTimeout(function () {
					strTemp = document.getElementById('Invoice_No').value;
					strTemp = strTemp.replace(/[^0-9,]/g, ''); //replace non-numeric
					strTemp = strTemp.replace(/\s/g, ''); // replace space
					document.getElementById('Invoice_No').value = strTemp;
				}, 1);
			}
			
			function RemoveBadNonNumber(strTemp, obj) {
				strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			function AfterPasteNonNumber(obj) {
				setTimeout(function () {
					RemoveBadNonNumber(obj.value, obj);
				}, 1); //or 4
			}			
			
			function ValidateMasterAWBNumber(evt) {
				var theEvent = evt || window.event;
				var key = theEvent.keyCode || theEvent.which;
				key = String.fromCharCode( key );
				var regex = /[-_/_a-zA-Z0-9]/;
				if( !regex.test(key) ) {
					theEvent.returnValue = false;
					if(theEvent.preventDefault) theEvent.preventDefault();
				}
			}
			
			function RemoveBadMasterAWBNumber(strTemp, obj) {
				strTemp = strTemp.replace(/[^-_/_a-zA-Z0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			function AfterPasteMasterAWBNumber(obj) {
				setTimeout(function () {
					RemoveBadMasterAWBNumber(obj.value, obj);
				}, 1); //or 4
			}					
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="RetrieveCustomerConsignments" method="post" runat="server">
			<INPUT style="Z-INDEX: 101; POSITION: absolute; TOP: 0px; LEFT: 16px" type="hidden" name="ScrollPosition">
			<asp:validationsummary style="Z-INDEX: 105; POSITION: absolute; COLOR: red; TOP: 672px; LEFT: 240px" id="Validationsummary1"
				Runat="server" DisplayMode="BulletList" ShowMessageBox="True" HeaderText="Please enter the missing  fields."
				ShowSummary="False"></asp:validationsummary>
			<div style="Z-INDEX: 102; POSITION: relative; HEIGHT: 1248px; TOP: 16px; LEFT: 32px"
				id="divMain" MS_POSITIONING="GridLayout" runat="server">
				<TABLE style="Z-INDEX: 101; POSITION: absolute; TOP: 8px; LEFT: 0px" id="MainTable" border="0"
					width="100%" runat="server">
					<tr>
						<td colSpan="2" align="left"><asp:label id="Label1" runat="server" CssClass="mainTitleSize" Height="27px" Width="421px">
								Retrieve Customer Consignments</asp:label></td>
					</tr>
					<tr>
						<td style="HEIGHT: 24px" vAlign="top" colSpan="2" align="left"><asp:button id="btnQry" runat="server" CssClass="queryButton" Width="61px" CausesValidation="False"
								Text="Query"></asp:button><asp:button id="btnExecQry" runat="server" CssClass="queryButton" Width="130px" CausesValidation="False"
								Text="Execute Query" Enabled="False"></asp:button><asp:button style="Z-INDEX: 0" id="btnRetrieveCons" runat="server" CssClass="queryButton" Width="130px"
								CausesValidation="False" Text="Retrieve Cons"></asp:button><asp:button style="Z-INDEX: 0" id="btnCustomsRetrieveCons" runat="server" Width="200px" CssClass="queryButton"
								Text="Customs Invoice Received" CausesValidation="False"></asp:button>
							<asp:button style="DISPLAY: none; VISIBILITY: hidden" id="btnDelete" runat="server" CssClass="queryButton"
								Width="62px" CausesValidation="False" Text="Delete"></asp:button>
							<asp:button style="Z-INDEX: 0; DISPLAY: none" id="btnExecQryHidden" runat="server" Width="130px"
								Text="Execute Query" CausesValidation="False"></asp:button>
						</td>
					</tr>
					<tr>
						<td vAlign="top" colSpan="2" align="left">
							<FIELDSET style="Z-INDEX: 105; WIDTH: 700px; HEIGHT: 48px; TOP: 88px; LEFT: 16px" id="Fieldset1"
								runat="server">
								<LEGEND>
									<asp:label id="Label16" runat="server" Width="170px" Height="16px" CssClass="tableHeadingFieldset">From Customer Self-Service</asp:label></LEGEND>
								<table border="0" cellSpacing="1" cellPadding="1">
									<tr>
										<td style="WIDTH: 3px" width="3"></td>
										<td><asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel" Width="100px">Shipping List
										</asp:label></td>
										<td style="WIDTH: 246px" width="246" colSpan="3">
											<asp:textbox id="txtShipListNo" runat="server" CssClass="textField" MaxLength="50" Width="185px"
												onblur="if( this.value !=''){document.getElementById('btnExecQryHidden').click();}" tabIndex="1"></asp:textbox>
										</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
								</table>
								<TABLE style="Z-INDEX: 0" id="Table1" border="0" cellSpacing="1" cellPadding="1">
									<TR>
										<td style="WIDTH: 3px" width="3"></td>
										<TD><asp:label style="Z-INDEX: 0" id="Label4" runat="server" CssClass="tableLabel" Width="100px"> Booking Number
										</asp:label></TD>
										<TD style="WIDTH: 246px" width="246" colSpan="3"><cc1:mstextbox id="txtBookingNo" Runat="server" CssClass="textField" Text="" Enabled="True" TextMaskType="msNumeric"
												MaxLength="10" NumberPrecision="10" NumberMaxValue="2147483647" Width="185px" tabIndex="2"></cc1:mstextbox></TD>
										<TD><asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel" Width="100px">Actual PUP D/T
										</asp:label></TD>
										<TD>
											<cc1:mstextbox id="txtActDate" runat="server" CssClass="textField" Width="114px" TextMaskType="msDateTime"
												MaxLength="16" TextMaskString="99/99/9999 99:99" tabIndex="3"></cc1:mstextbox>
										</TD>
									</TR>
								</TABLE>
							</FIELDSET>
						</td>
					</tr>
					<tr>
						<td vAlign="top" colSpan="2" align="left">
							<FIELDSET style="Z-INDEX: 105; WIDTH: 700px; HEIGHT: 48px; TOP: 88px; LEFT: 16px" id="Fieldset2"
								runat="server">
								<LEGEND>
									<asp:label id="Label17" runat="server" Width="100px" Height="16px" CssClass="tableHeadingFieldset">From Customs</asp:label></LEGEND>
								<TABLE style="Z-INDEX: 0" id="Table11" border="0" cellSpacing="1" cellPadding="1">
									<TR>
										<td style="WIDTH: 3px" width="3"></td>
										<TD valign="top"><asp:label style="Z-INDEX: 0" id="Label19" runat="server" CssClass="tableLabel" Width="100px"> Invoice Number
										</asp:label></TD>
										<TD valign="top" style="WIDTH: 246px" width="246" colSpan="3">
											<asp:textbox style="Z-INDEX: 0" id="Invoice_No" tabIndex="4" onkeypress="validateInvoice(event)"
												onpaste="AfterPasteInvoiceNo(this)" runat="server" Width="230px" CssClass="textField" MaxLength="8000"
												TextMode="MultiLine" Rows="5"></asp:textbox></TD>
										<TD valign="top"><asp:label style="Z-INDEX: 0" id="Label20" runat="server" CssClass="tableLabel" Width="130px"> Consignment Number
										</asp:label></TD>
										<TD valign="top">
											<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="txtConsNo" tabIndex="5" onkeypress="ValidateMasterAWBNumber(event)"
												onpaste="AfterPasteMasterAWBNumber(this)" runat="server" CssClass="textField" Width="150px"
												onblur="if( this.value !=''){document.getElementById('btnExecQryHidden').click();}" MaxLength="30"></asp:textbox>
										</TD>
									</TR>
								</TABLE>
							</FIELDSET>
						</td>
					</tr>
					<tr>
						<td colSpan="2"><asp:label style="Z-INDEX: 0" id="lblErrorMsg" runat="server" Height="19px" Width="700px" ForeColor="Red"
								Font-Bold="True" Font-Size="X-Small"></asp:label></td>
					</tr>
					<tr>
						<td colSpan="2"><asp:panel id="pnlImport" runat="server" Width="800px">
								<FIELDSET style="Z-INDEX: 105; WIDTH: 700px; HEIGHT: 48px; TOP: 88px; LEFT: 16px" id="fsImport"
									runat="server"><LEGEND>
										<asp:label id="lblImportSIPs" runat="server" Width="100px" Height="16px" CssClass="tableHeadingFieldset">Pickup Request</asp:label></LEGEND>
									<TABLE id="Table2" border="0" cellSpacing="0" cellPadding="0" align="center">
										<TR>
											<TD>
												<asp:label style="Z-INDEX: 0" id="Label5" runat="server" Width="100px" CssClass="tableLabel">Booking D/T</asp:label></TD>
											<TD>
												<asp:TextBox style="Z-INDEX: 0" id="txtBookingDate" runat="server" Width="150px" CssClass="textField"
													Enabled="False">03/03/2014 08:00</asp:TextBox></TD>
											<TD width="20"></TD>
											<TD>
												<asp:label style="Z-INDEX: 0" id="Label14" runat="server" Width="100px" CssClass="tableLabel">Sender Name</asp:label></TD>
											<TD>
												<asp:TextBox style="Z-INDEX: 0" id="txtSenderName" runat="server" Width="150px" CssClass="textField"
													Enabled="False">BSPLAE</asp:TextBox></TD>
										</TR>
										<TR>
											<TD>
												<asp:label style="Z-INDEX: 0" id="Label13" runat="server" Width="100px" CssClass="tableLabel">Customer ID</asp:label></TD>
											<TD>
												<asp:TextBox id="txtCustID" runat="server" Width="120px" CssClass="textField" Enabled="False">20650</asp:TextBox></TD>
											<TD width="20"></TD>
											<TD>
												<asp:label style="Z-INDEX: 0" id="Label8" runat="server" Width="100px" CssClass="tableLabel">Sender Address</asp:label></TD>
											<TD>
												<asp:TextBox style="Z-INDEX: 0" id="txtSenderAddr1" runat="server" Width="230px" CssClass="textField"
													Enabled="False">PO BOX 2123</asp:TextBox></TD>
										</TR>
										<TR>
											<TD>
												<asp:label style="Z-INDEX: 0" id="Label7" runat="server" Width="100px" CssClass="tableLabel">Customer Name</asp:label></TD>
											<TD width="20">
												<asp:TextBox style="Z-INDEX: 0" id="txtCustName" runat="server" Width="150px" CssClass="textField"
													Enabled="False">BANK SOUTH FACIFIC</asp:TextBox></TD>
											<TD></TD>
											<TD></TD>
											<TD>
												<asp:TextBox style="Z-INDEX: 0" id="txtSenderAddr2" runat="server" Width="230px" CssClass="textField"
													Enabled="False"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD>
												<asp:label style="Z-INDEX: 0" id="Label9" runat="server" Width="100px" CssClass="tableLabel">Billing Address</asp:label></TD>
											<TD>
												<asp:TextBox id="txtBillingAddr1" runat="server" Width="230px" CssClass="textField" Enabled="False">P.O. BOX 173</asp:TextBox></TD>
											<TD width="20"></TD>
											<TD>
												<asp:label style="Z-INDEX: 0" id="Label10" runat="server" Width="100px" CssClass="tableLabel">Postal Code</asp:label></TD>
											<TD>
												<asp:TextBox style="Z-INDEX: 0" id="txtSenderZipcode" runat="server" Width="42px" CssClass="textField"
													Enabled="False">LAE</asp:TextBox>
												<asp:TextBox style="Z-INDEX: 0" id="txtSenderStateName" runat="server" Width="182px" CssClass="textField"
													Enabled="False"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD style="HEIGHT: 19px"></TD>
											<TD style="HEIGHT: 19px">
												<asp:TextBox id="txtBillingAddr2" runat="server" Width="230px" CssClass="textField" Enabled="False">PORT MORESBY</asp:TextBox></TD>
											<TD style="HEIGHT: 19px" width="20"></TD>
											<TD style="HEIGHT: 19px">
												<asp:label style="Z-INDEX: 0" id="Label6" runat="server" Width="100px" CssClass="tableLabel">Phone Number</asp:label></TD>
											<TD style="HEIGHT: 19px">
												<asp:TextBox style="Z-INDEX: 0" id="txtSenderPhone" runat="server" Width="150px" CssClass="textField"
													Enabled="False">2342342</asp:TextBox></TD>
										</TR>
										<TR>
											<TD>
												<asp:label style="Z-INDEX: 0" id="Label11" runat="server" Width="100px" CssClass="tableLabel">Postal Code</asp:label></TD>
											<TD>
												<asp:TextBox style="Z-INDEX: 0" id="txtZipCode" runat="server" Width="42px" CssClass="textField"
													Enabled="False">POM</asp:TextBox>
												<asp:TextBox style="Z-INDEX: 0" id="txtStateName" runat="server" Width="182px" CssClass="textField"
													Enabled="False">NATIONAL CAPITAL DISTRICT</asp:TextBox></TD>
											<TD width="20"></TD>
											<TD>
												<asp:label style="Z-INDEX: 0" id="Label12" runat="server" Width="100px" CssClass="tableLabel">Contact Name</asp:label></TD>
											<TD>
												<asp:TextBox id="txtSenderContactName" runat="server" Width="150px" CssClass="textField" Enabled="False">Anifreda</asp:TextBox></TD>
										</TR>
										<TR>
											<TD>
												<asp:label style="Z-INDEX: 0" id="Label15" runat="server" Width="100px" CssClass="tableLabel">Est.PUP D/T </asp:label></TD>
											<TD>
												<asp:TextBox style="Z-INDEX: 0" id="txtEstDate" runat="server" Width="150px" CssClass="textField"
													Enabled="False">03/03/2014 18:00</asp:TextBox></TD>
											<TD width="20"></TD>
											<TD></TD>
											<TD></TD>
										</TR>
									</TABLE>
									<BR>
								</FIELDSET>
							</asp:panel></td>
					</tr>
					<tr>
						<td colSpan="2"><asp:datagrid style="Z-INDEX: 0" id="grdConList" runat="server" Width="800px" PageSize="25" HorizontalAlign="Left"
								AutoGenerateColumns="False" SelectedItemStyle-CssClass="gridFieldSelected">
								<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
								<ItemStyle Height="15px" CssClass="gridField"></ItemStyle>
								<HeaderStyle Font-Bold="True" HorizontalAlign="Center" Height="40px" Width="2%" CssClass="gridHeading"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Include">
										<ItemStyle HorizontalAlign="Center" Height="25px" Width="70px"></ItemStyle>
										<ItemTemplate>
											<asp:CheckBox Runat="server" ID="cbkConsignment_no" CrsID='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' Checked='<%# DataBinder.Eval(Container.DataItem,"Check").ToString().ToLower().Equals("true") %>'>
											</asp:CheckBox>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:Label></asp:Label>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Consignment No.">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:label ID="lblConsignment_no" Runat="server" Visible="true">
												<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>
											</asp:label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="recipient_zipcode" HeaderText="To">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="recipient_telephone" HeaderText="Recipient Phone">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="recipient_name" HeaderText="Recipient Name">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="service_code" HeaderText="Service">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="tot_pkgs" HeaderText="Pkgs">
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:BoundColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
							</asp:datagrid></td>
					</tr>
				</TABLE>
			</div>
			<INPUT type="hidden" name="hdnRefresh">
			<asp:button style="Z-INDEX: 105; POSITION: absolute; DISPLAY: none; TOP: 1064px; LEFT: 672px"
				id="btnHidReport" runat="server" Text=".."></asp:button></form>
	</body>
</HTML>
