using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.util;
using com.common.classes;
using com.common.applicationpages;
using com.common.RBAC;


namespace com.ties
{
	/// <summary>
	/// Summary description for StatusException.
	/// </summary>
	/// 
	

	public class StatusException : BasePage
	{
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnDelete;

		//15/11/2002 Mohan
		DataView m_dvShipmentUpdateOptions = null;
		DataView m_dvCloseStatusOptions = null;
		DataView m_dvInvoiceableOptions = null;
		DataView m_dvMBGOptions = null;
		DataView m_dvScanningTypeOptions = null;
		DataView m_dvAllowUPD_ByBatchOptions = null;
		DataView m_dvAllowInSWB_EmuOptions = null;
		DataView m_dvAllowUPDByBatchOptions=null;
		
		SessionDS m_sdsStatusCode = null;
		SessionDS m_sdsExceptionCode = null;
		
		protected System.Web.UI.WebControls.DataGrid dgStatusCodes;
		protected System.Web.UI.WebControls.DataGrid dgExceptionCodes;
		protected System.Web.UI.WebControls.Button btnExceptionCodeInsert;
		protected System.Web.UI.WebControls.Label lblSCMessage;
		protected System.Web.UI.WebControls.Label lblEXMessage;
		protected System.Web.UI.WebControls.ValidationSummary ValidationSummary1;
		protected System.Web.UI.WebControls.Label lblMainTitle;

		//Utility utility = null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			ShowButtonColumns(true);
			

		
			if(!Page.IsPostBack)
			{
				Session["SESSION_DS2"] = m_sdsExceptionCode;
				ViewState["SCOperation"] = Operation.None;
				ViewState["EXMode"] = ScreenMode.None;
				ViewState["EXOperation"] = Operation.None;
				ResetScreenForQuery();
			}
			else
			{
				m_sdsStatusCode = (SessionDS)Session["SESSION_DS1"];
				m_sdsExceptionCode = (SessionDS)Session["SESSION_DS2"];
				int iMode = (int) ViewState["SCMode"];
				switch(iMode)
				{
					case (int) ScreenMode.Query:
						LoadComboLists(true);
						break;
					default:
						LoadComboLists(false);
						break;
				}
				//lblSCMessage.Text = "";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExceptionCodeInsert.Click += new System.EventHandler(this.btnExceptionCodeInsert_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			ResetScreenForQuery();
		}

		private void ResetScreenForQuery()
		{
			
			LoadComboLists(true);
			m_sdsStatusCode = SysDataMgrDAL.GetEmptyStatusCodeDS(1);
			
			ViewState["SCMode"] = ScreenMode.Query;

			Session["SESSION_DS1"] = m_sdsStatusCode;
			dgStatusCodes.EditItemIndex = 0;
			dgStatusCodes.CurrentPageIndex = 0;
			BindSCGrid();
			btnExecQry.Enabled = true;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			lblSCMessage.Text = "";

			ShowButtonColumns(false);
			
			ResetDetailsGrid();
		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			LoadComboLists(false);
			if(!FillSCDataRow( dgStatusCodes.Items[0],0))
			{
				this.lblSCMessage.Text = "Viewable Remark for T&T or Viewable Location for T&T cannot be Yes.";
				return;
			}

			ViewState["QUERY_DS"] = m_sdsStatusCode.ds;
			dgStatusCodes.CurrentPageIndex = 0;
			ShowCurrentSCPage();
			btnExecQry.Enabled = false;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			lblSCMessage.Text = "";
			ViewState["SCMode"] = ScreenMode.ExecuteQuery;
		}

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			//If coming to any other mode to Insert Mode then create the empty data set.

			if((int)ViewState["SCMode"] != (int)ScreenMode.Insert || m_sdsStatusCode.ds.Tables[0].Rows.Count >= dgStatusCodes.PageSize)
			{
				m_sdsStatusCode = SysDataMgrDAL.GetEmptyStatusCodeDS(0);
			}

			AddRowInSCGrid();	
			dgStatusCodes.EditItemIndex = m_sdsStatusCode.ds.Tables[0].Rows.Count - 1;
			dgStatusCodes.CurrentPageIndex = 0;			
			LoadComboLists(false);
			BindSCGrid();
			ViewState["SCMode"] = ScreenMode.Insert;
			ViewState["SCOperation"] = Operation.Insert;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,false,m_moduleAccessRights);
			btnExecQry.Enabled = false;
			lblSCMessage.Text = "";
			ResetDetailsGrid();
			getPageControls(Page);
		}

		private void btnExceptionCodeInsert_Click(object sender, System.EventArgs e)
		{
			//If coming to any other mode to Insert Mode then create the empty data set.

			if((int)ViewState["EXMode"] != (int)ScreenMode.Insert || m_sdsExceptionCode.ds.Tables[0].Rows.Count >= dgExceptionCodes.PageSize)
			{
				m_sdsExceptionCode = SysDataMgrDAL.GetEmptyExceptionCodeDS(0);
			}

			AddRowInExGrid();	
			dgExceptionCodes.EditItemIndex = m_sdsExceptionCode.ds.Tables[0].Rows.Count - 1;
			dgExceptionCodes.CurrentPageIndex = 0;
			
			LoadComboLists(false);
			BindExGrid();
			ViewState["EXMode"] = ScreenMode.Insert;
			ViewState["EXOperation"] = Operation.Insert;
			btnExceptionCodeInsert.Enabled = false;
			getPageControls(Page);
		}


		#region Status Grid procedure
		public void dgStatusCodes_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			Label lblStatusCode = (Label)dgStatusCodes.SelectedItem.FindControl("lblStatusCode");
			msTextBox txtStatusCode = (msTextBox)dgStatusCodes.SelectedItem.FindControl("txtStatusCode");
			String strStatusCode = null;

			if(lblStatusCode != null)
			{
				strStatusCode = lblStatusCode.Text;
			}

			if(txtStatusCode != null)
			{
				strStatusCode = txtStatusCode.Text;
			}

			ViewState["CurrentSC"] = strStatusCode;
			dgExceptionCodes.CurrentPageIndex = 0;

			
			ShowCurrentEXPage();

			ViewState["EXMode"] = ScreenMode.ExecuteQuery;

			if((int)ViewState["SCMode"] != (int)ScreenMode.Insert && (int)ViewState["SCOperation"] != (int)Operation.Insert)
			{
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			}

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && dgStatusCodes.EditItemIndex == dgStatusCodes.SelectedIndex && (int)ViewState["SCOperation"] == (int)Operation.Insert)
			{
				btnExceptionCodeInsert.Enabled = false;
			}
			else
			{
				btnExceptionCodeInsert.Enabled = true;
			}
			lblSCMessage.Text = "";

		}

		protected void dgStatusCodes_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && (int) ViewState["SCOperation"] == (int)Operation.Insert && dgStatusCodes.EditItemIndex > 0)
			{
				m_sdsStatusCode.ds.Tables[0].Rows.RemoveAt(dgStatusCodes.EditItemIndex);
				m_sdsStatusCode.QueryResultMaxSize--;
				dgStatusCodes.CurrentPageIndex = 0;
			}

			dgStatusCodes.EditItemIndex = e.Item.ItemIndex;
			ViewState["SCOperation"] = Operation.Update;
			BindSCGrid();
			lblSCMessage.Text = "";
			
		}
		
		private bool checkRoleDup(string strRole)
		{
			try
			{
				string[] s = strRole.Split(';');
				bool isDup = false;
				for(int k=0;k<s.Length;k++)
				{
					for(int i=0;i<s.Length;i++)
					{
						if(s[k].ToString().ToUpper().Trim()==s[i].ToString().Trim().ToUpper() && k!= i)		
						{
							isDup = true;
							this.lblSCMessage.Text = "Duplicate Role:" + s[k].ToString().Trim();
							return isDup;
						}
						else
						{
							isDup = false;
						}
					}
				}
				return isDup;
			}
			catch
			{
				return false;
			}
		}


		private bool checkRole(string strRole)
		{
			try
			{
				string[] s = strRole.Split(';');
				Role roleFake = new Role();
				ArrayList userRoleArray = com.common.RBAC.RBACManager.GetAllRoles(utility.GetAppID(),utility.GetEnterpriseID(),roleFake);
				Role role;
				bool isRole = true;
				for(int k=0;k<s.Length;k++)
				{
					for(int i=0;i<userRoleArray.Count;i++)
					{
						role = (Role)userRoleArray[i];
						if(role.RoleName.ToUpper().Trim()==s[k].ToString().Trim().ToUpper())				
						{
							isRole = true;
							break;
						}
						else
						{
							isRole = false;
						}
						if( i==userRoleArray.Count-1 && isRole==false )
						{
							this.lblSCMessage.Text = "Role does not exist:" + s[k].ToString().Trim();
							return isRole;
						}
					}
				}
				return isRole;
			}
			catch
			{
				return false;
			}
		}

		public void dgStatusCodes_Update(object sender, DataGridCommandEventArgs e)
		{
			
			if(!FillSCDataRow(e.Item,e.Item.ItemIndex))
			{
				this.lblSCMessage.Text = "Viewable Remark for T&T or Viewable Location must be No if Viewable Status for T&T is No";
				return;
			}
			TextBox txtAllowRoles = (TextBox)dgStatusCodes.Items[e.Item.ItemIndex].FindControl("txtAllowRoles");
			//if 
			
			if(txtAllowRoles.Text.Trim().Length>0)
			{
				if(!checkRole(txtAllowRoles.Text.Trim()))
				{
					return;
				}
				if(checkRoleDup(txtAllowRoles.Text.Trim()))
				{
					return;
				}
			}
			
			TextBox txtAutoCPN= (TextBox)dgStatusCodes.Items[e.Item.ItemIndex].FindControl("txtAutoCPN");
			DropDownList ddlAllowUPDByBatch = (DropDownList)dgStatusCodes.Items[e.Item.ItemIndex].FindControl("ddlAllowUPDByBatch");			

			if(txtAutoCPN.Text.Trim()!="21" && ddlAllowUPDByBatch.SelectedValue.ToUpper()=="A" )
			{
				lblSCMessage.Text =  "SIP in Allow Update by Batch is not allowed for this status code";				
				return;
			}


			int iOperation = (int)ViewState["SCOperation"];
			switch(iOperation)
			{

				case (int)Operation.Update:

					DataSet dsToUpdate = m_sdsStatusCode.ds.GetChanges();
					SysDataMgrDAL.ModifyStatusCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),dsToUpdate);
					lblSCMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
					Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
					m_sdsStatusCode.ds.AcceptChanges();

					break;

				case (int)Operation.Insert:

					DataSet dsToInsert = m_sdsStatusCode.ds.GetChanges();
					try
					{
						SysDataMgrDAL.AddStatusCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),dsToInsert);
						lblSCMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
						m_sdsStatusCode.ds.AcceptChanges();
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblSCMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"DUP_STATUS_CODE",utility.GetUserCulture());				
							 return;
						}
						if(strMsg.IndexOf("unique constraint ") != -1 )
						{
								lblSCMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"DUP_STATUS_CODE",utility.GetUserCulture());				
								return;
						}
						lblSCMessage.Text =  strMsg;				
						
					}
					
					break;

			}
			dgStatusCodes.EditItemIndex = -1;
			ViewState["SCOperation"] = Operation.None;
			m_sdsStatusCode.ds.AcceptChanges();
			lblEXMessage.Text="";
			BindSCGrid();
		}

		protected void dgStatusCodes_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgStatusCodes.EditItemIndex = -1;

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert && (int)ViewState["SCOperation"] == (int)Operation.Insert)
			{
				m_sdsStatusCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsStatusCode.QueryResultMaxSize--;
				dgStatusCodes.CurrentPageIndex = 0;
			}
			ViewState["SCOperation"] = Operation.None;
			BindSCGrid();
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			lblSCMessage.Text = "";
			
		}

		public void dgStatusCodes_Delete(object sender, DataGridCommandEventArgs e)
		{
			msTextBox txtStatusCode = (msTextBox)e.Item.FindControl("txtStatusCode");
			Label lblStatusCode = (Label)e.Item.FindControl("lblStatusCode");
			String strStatusCode = null;
			if(e.Item.Cells[2].Enabled ==false)
			{
				lblSCMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"NO_DEL_POSSIBLE",utility.GetUserCulture());
				return;
			}
			if(txtStatusCode != null)
			{
				strStatusCode = txtStatusCode.Text;
			}

			if(lblStatusCode != null)
			{
				strStatusCode = lblStatusCode.Text;
			}
			try
			{
				SysDataMgrDAL.DeleteStatusCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strStatusCode);
				lblSCMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblSCMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"ERR_DEL_EXP_TRANS",utility.GetUserCulture()); 										
				}
				return;
			}

			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
			{
				m_sdsStatusCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsStatusCode.QueryResultMaxSize--;
				dgStatusCodes.CurrentPageIndex = 0;

				if((int) ViewState["SCOperation"] == (int)Operation.Insert && dgStatusCodes.EditItemIndex > 0)
				{
					m_sdsStatusCode.ds.Tables[0].Rows.RemoveAt(dgStatusCodes.EditItemIndex-1);
					m_sdsStatusCode.QueryResultMaxSize--;
				}
				dgStatusCodes.EditItemIndex = -1;
				dgStatusCodes.SelectedIndex = -1;
				BindSCGrid();
				ResetDetailsGrid();
				
			}
			else
			{
				ShowCurrentSCPage();
			}

			
			ViewState["SCOperation"] = Operation.None;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			lblEXMessage.Text="";
		}

		protected void dgStatusCodes_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsStatusCode.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			msTextBox txtStatusCode = (msTextBox)e.Item.FindControl("txtStatusCode");
			int iOperation = (int) ViewState["SCOperation"];
			if(txtStatusCode != null && iOperation == (int)Operation.Update)
			{
				txtStatusCode.Enabled = false;
			}

			Label lblSCSystemCode = (Label)e.Item.FindControl("lblSCSystemCode");			
			if(lblSCSystemCode != null)
			{
				if(lblSCSystemCode.Text == "y" || lblSCSystemCode.Text == "Y")
				{
					e.Item.Cells[2].Enabled = false;
				}
			}

			DropDownList ddlSCShipmentUpdate = (DropDownList)e.Item.FindControl("ddlSCShipmentUpdate");			
			if(ddlSCShipmentUpdate != null)
			{
				ddlSCShipmentUpdate.DataSource = LoadShipmentUpdateOptions();
				ddlSCShipmentUpdate.DataTextField = "Text";
				ddlSCShipmentUpdate.DataValueField = "StringValue";
				ddlSCShipmentUpdate.DataBind();
				if((drSelected["Status_type"]!= null) && (!drSelected["Status_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{				
					String strShipmentUpdate = (String)drSelected["Status_type"];
					ddlSCShipmentUpdate.SelectedIndex = ddlSCShipmentUpdate.Items.IndexOf(ddlSCShipmentUpdate.Items.FindByValue(strShipmentUpdate));  //.FindByValue("N"));
					
				}
			}

			DropDownList ddlSCCloseStatus = (DropDownList)e.Item.FindControl("ddlSCCloseStatus");			
			if(ddlSCCloseStatus != null)
			{
				ddlSCCloseStatus.DataSource = LoadCloseStatusOptions();
				ddlSCCloseStatus.DataTextField = "Text";
				ddlSCCloseStatus.DataValueField = "StringValue";
				ddlSCCloseStatus.DataBind();
				if((drSelected["status_close"]!= null) && (!drSelected["status_close"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{							
					String strCloseStatus = (String)drSelected["status_close"];
					ddlSCCloseStatus.SelectedIndex = ddlSCCloseStatus.Items.IndexOf(ddlSCCloseStatus.Items.FindByValue(strCloseStatus));//.FindByValue("N"));
					
				}
			}

			DropDownList ddlSCInvoiceable = (DropDownList)e.Item.FindControl("ddlSCInvoiceable");			
			if(ddlSCInvoiceable != null)
			{
				ddlSCInvoiceable.DataSource = LoadInvoiceableOptions();
				ddlSCInvoiceable.DataTextField = "Text";
				ddlSCInvoiceable.DataValueField = "StringValue";
				ddlSCInvoiceable.DataBind();
				if((drSelected["invoiceable"]!= null) && (!drSelected["invoiceable"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{	
					String strInvoiceable = (String)drSelected["invoiceable"];
					ddlSCInvoiceable.SelectedIndex = ddlSCInvoiceable.Items.IndexOf(ddlSCInvoiceable.Items.FindByValue(strInvoiceable));//.FindByValue("N"));
					
				}
			}

			DropDownList ddlAllowMobile = (DropDownList)e.Item.FindControl("ddlAllowMobile");			
			if(ddlAllowMobile != null)
			{
				ddlAllowMobile.DataSource = LoadInvoiceableOptions();
				ddlAllowMobile.DataTextField = "Text";
				ddlAllowMobile.DataValueField = "StringValue";
				ddlAllowMobile.DataBind();
				if((drSelected["MobileApp"]!= null) && (!drSelected["MobileApp"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{	
					String strMobileApp = (String)drSelected["MobileApp"];
					ddlAllowMobile.SelectedIndex = ddlAllowMobile.Items.IndexOf(ddlAllowMobile.Items.FindByValue(strMobileApp));//.FindByValue("N"));
					
				}
			}

//ADD NEW COLUMNS BY X JAN 23 08
			DropDownList ddlVSTT = (DropDownList)e.Item.FindControl("ddlVSTT");			
			if(ddlVSTT != null)
			{
				ddlVSTT.DataSource = LoadCloseStatusOptions();
				ddlVSTT.DataTextField = "Text";
				ddlVSTT.DataValueField = "StringValue";
				ddlVSTT.DataBind();
				if((drSelected["tt_display_status"]!= null) && (!drSelected["tt_display_status"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{							
					String tt_display_status = (String)drSelected["tt_display_status"];
					ddlVSTT.SelectedIndex = ddlVSTT.Items.IndexOf(ddlVSTT.Items.FindByValue(tt_display_status));//.FindByValue("N"));
				}
			}

			DropDownList ddlVRTT = (DropDownList)e.Item.FindControl("ddlVRTT");			
			if(ddlVRTT != null)
			{
				ddlVRTT.DataSource = LoadCloseStatusOptions();
				ddlVRTT.DataTextField = "Text";
				ddlVRTT.DataValueField = "StringValue";
				ddlVRTT.DataBind();
				if((drSelected["tt_display_remarks"]!= null) && (!drSelected["tt_display_remarks"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{							
					String tt_display_remarks = (String)drSelected["tt_display_remarks"];
					ddlVRTT.SelectedIndex = ddlVRTT.Items.IndexOf(ddlVRTT.Items.FindByValue(tt_display_remarks));//.FindByValue("N"));
				}
			}

//TXTAUTOCPN


			DropDownList ddlAutoPLC = (DropDownList)e.Item.FindControl("ddlAutoPLC");			
			if(ddlAutoPLC != null)
			{
				ddlAutoPLC.DataSource = LoadCloseStatusOptions();
				ddlAutoPLC.DataTextField = "Text";
				ddlAutoPLC.DataValueField = "StringValue";
				ddlAutoPLC.DataBind();
				if((drSelected["auto_location"]!= null) && (!drSelected["auto_location"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{							
					String auto_location = (String)drSelected["auto_location"];
					ddlAutoPLC.SelectedIndex = ddlAutoPLC.Items.IndexOf(ddlAutoPLC.Items.FindByValue(auto_location));//.FindByValue("N"));
				}
			}

			DropDownList ddlVLTT = (DropDownList)e.Item.FindControl("ddlVLTT");			
			if(ddlVLTT != null)
			{
				ddlVLTT.DataSource = LoadCloseStatusOptions();
				ddlVLTT.DataTextField = "Text";
				ddlVLTT.DataValueField = "StringValue";
				ddlVLTT.DataBind();
				if((drSelected["tt_display_location"]!= null) && (!drSelected["tt_display_location"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{							
					String tt_display_location = (String)drSelected["tt_display_location"];
					ddlVLTT.SelectedIndex = ddlVLTT.Items.IndexOf(ddlVLTT.Items.FindByValue(tt_display_location));//.FindByValue("N"));
				}
			}


			DropDownList ddlScanningType = (DropDownList)e.Item.FindControl("ddlScanningType");			
			if(ddlScanningType != null)
			{
				ddlScanningType.DataSource = LoadScanningTypeOptions();
				ddlScanningType.DataTextField = "Text";
				ddlScanningType.DataValueField = "StringValue";
				ddlScanningType.DataBind();
				if((drSelected["scanning_type"]!= null) && (!drSelected["scanning_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{				
					String strScanningType = (String)drSelected["scanning_type"];
					ddlScanningType.SelectedIndex = ddlScanningType.Items.IndexOf(ddlScanningType.Items.FindByValue(strScanningType));  //.FindByValue("N"));
					
				}
			}

			

//END ADD NEW COLUMNS
			DropDownList ddlAllowInSWBEmu = (DropDownList)e.Item.FindControl("ddlAllowInSWBEmu");			
			if(ddlAllowInSWBEmu != null)
			{
				ddlAllowInSWBEmu.DataSource = LoadCloseStatusOptions();
				ddlAllowInSWBEmu.DataTextField = "Text";
				ddlAllowInSWBEmu.DataValueField = "StringValue";
				ddlAllowInSWBEmu.DataBind();
				if((drSelected["AllowInSWB_Emu"]!= null) && (!drSelected["AllowInSWB_Emu"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{							
					String AllowInSWB_Emu = (String)drSelected["AllowInSWB_Emu"];
					ddlAllowInSWBEmu.SelectedIndex = ddlAllowInSWBEmu.Items.IndexOf(ddlAllowInSWBEmu.Items.FindByValue(AllowInSWB_Emu));//.FindByValue("N"));
				}
			}

			DropDownList ddlAllowUPDByBatch = (DropDownList)e.Item.FindControl("ddlAllowUPDByBatch");			
			if(ddlAllowUPDByBatch != null)
			{
				ddlAllowUPDByBatch.DataSource = LoadAllowUPDByBatchOptions();
				ddlAllowUPDByBatch.DataTextField = "Text";
				ddlAllowUPDByBatch.DataValueField = "StringValue";
				ddlAllowUPDByBatch.DataBind();
				if((drSelected["AllowUPD_ByBatch"]!= null) && (!drSelected["AllowUPD_ByBatch"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{							
					String AllowUPD_ByBatch = (String)drSelected["AllowUPD_ByBatch"];
					ddlAllowUPDByBatch.SelectedIndex = ddlAllowUPDByBatch.Items.IndexOf(ddlAllowUPDByBatch.Items.FindByValue(AllowUPD_ByBatch));//.FindByValue("N"));
				}
			}



		}

		protected void dgStatusCodes_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgStatusCodes.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentSCPage();
			lblSCMessage.Text = "";
		}

		private void BindSCGrid()
		{
			dgStatusCodes.VirtualItemCount = System.Convert.ToInt32(m_sdsStatusCode.QueryResultMaxSize);
			dgStatusCodes.DataSource = m_sdsStatusCode.ds;
			dgStatusCodes.DataBind();
			Session["SESSION_DS1"] = m_sdsStatusCode;
		}

		private void AddRowInSCGrid()
		{
			SysDataMgrDAL.AddNewRowInStatusCodeDS(m_sdsStatusCode);
			Session["SESSION_DS1"] = m_sdsStatusCode;
		}		
		private void ShowCurrentSCPage()
		{
			int iStartIndex = dgStatusCodes.CurrentPageIndex * dgStatusCodes.PageSize;
			DataSet dsQuery = (DataSet)ViewState["QUERY_DS"];
			m_sdsStatusCode = SysDataMgrDAL.GetStatusCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),iStartIndex,dgStatusCodes.PageSize,dsQuery);
			decimal pgCnt = (m_sdsStatusCode.QueryResultMaxSize - 1)/dgStatusCodes.PageSize;
			if(pgCnt < dgStatusCodes.CurrentPageIndex)
			{
				dgStatusCodes.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgStatusCodes.SelectedIndex = -1;
			dgStatusCodes.EditItemIndex = -1;
			//this.lblSCMessage.Text = "";

			BindSCGrid();
			ResetDetailsGrid();
		}

		private bool FillSCDataRow(DataGridItem item, int drIndex)
		{

			DropDownList ddlVSTT = (DropDownList)item.FindControl("ddlVSTT");
			DropDownList ddlVRTT = (DropDownList)item.FindControl("ddlVRTT");
			DropDownList ddlVLTT = (DropDownList)item.FindControl("ddlVLTT");
			DropDownList ddlAllowInSWBEmu = (DropDownList)item.FindControl("ddlAllowInSWBEmu");
			DropDownList ddlAllowUPDByBatch = (DropDownList)item.FindControl("ddlAllowUPDByBatch");
			TextBox txtAllowRoles = (TextBox)item.FindControl("txtAllowRoles");

			if(ddlVSTT != null && ddlVRTT != null && ddlVLTT != null)
			{
				if(ddlVSTT.SelectedItem.Value=="N"&&(ddlVRTT.SelectedItem.Value=="Y"||ddlVLTT.SelectedItem.Value=="Y"))
				{
					return false;
				}
			}

			msTextBox txtStatusCode = (msTextBox)item.FindControl("txtStatusCode");
			if(txtStatusCode != null)
			{
				DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["status_code"] = txtStatusCode.Text;
			}

			TextBox txtStatusDescription = (TextBox)item.FindControl("txtStatusDescription");
			if(txtStatusDescription != null)
			{
				DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["status_description"] = txtStatusDescription.Text;
			}
			
			DropDownList ddlSCShipmentUpdate = (DropDownList)item.FindControl("ddlSCShipmentUpdate");
			if(ddlSCShipmentUpdate != null)
			{
				DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["Status_Type"] = ddlSCShipmentUpdate.SelectedItem.Value;
			}

			DropDownList ddlSCCloseStatus = (DropDownList)item.FindControl("ddlSCCloseStatus");
			if(ddlSCCloseStatus != null)
			{
				DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["status_close"] = ddlSCCloseStatus.SelectedItem.Value;
			}

			DropDownList ddlSCInvoiceable = (DropDownList)item.FindControl("ddlSCInvoiceable");
			if(ddlSCInvoiceable != null)
			{
				DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["invoiceable"] = ddlSCInvoiceable.SelectedItem.Value;
			}

			DropDownList ddlAllowMobile = (DropDownList)item.FindControl("ddlAllowMobile");
			if(ddlSCInvoiceable != null)
			{
				DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["MobileApp"] = ddlAllowMobile.SelectedItem.Value;
			}

			//BY X JAN 24 08
			//DropDownList ddlVSTT = (DropDownList)item.FindControl("ddlVSTT");
			if(ddlVSTT != null)
			{
				DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["tt_display_status"] = ddlVSTT.SelectedItem.Value;
			}

			//DropDownList ddlVRTT = (DropDownList)item.FindControl("ddlVRTT");
			if(ddlVRTT != null)
			{
				DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["tt_display_remarks"] = ddlVRTT.SelectedItem.Value;
			}

			DropDownList ddlAutoPLC = (DropDownList)item.FindControl("ddlAutoPLC");
			if(ddlAutoPLC != null)
			{
				DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["auto_location"] = ddlAutoPLC.SelectedItem.Value;
			}

			//DropDownList ddlVLTT = (DropDownList)item.FindControl("ddlVLTT");
			if(ddlVLTT != null)
			{
				DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["tt_display_location"] = ddlVLTT.SelectedItem.Value;
			}


			TextBox txtAutoCPN = (TextBox)item.FindControl("txtAutoCPN");
			if(txtAutoCPN != null)
			{
				DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				if(txtAutoCPN.Text.Length > 0)
					drCurrent["auto_process"] = Int32.Parse(txtAutoCPN.Text.Trim());
			}

			TextBox txtAutoCPR = (TextBox)item.FindControl("txtAutoCPR");
			if(txtAutoCPN != null)
			{
				DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["auto_remark"] = txtAutoCPR.Text.Trim();
			}

			DropDownList ddlScanningType = (DropDownList)item.FindControl("ddlScanningType");
			if(ddlScanningType != null)
			{
				DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["scanning_type"] = ddlScanningType.SelectedItem.Value;
			}

			if(txtAllowRoles != null)
			{
				DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["AllowRoles"] = txtAllowRoles.Text.Trim();
			}
			if(ddlAllowInSWBEmu != null)
			{
				DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["AllowInSWB_Emu"] = ddlAllowInSWBEmu.SelectedItem.Value;
			}
			if(ddlAllowUPDByBatch != null)
			{
				DataRow drCurrent = m_sdsStatusCode.ds.Tables[0].Rows[drIndex];
				drCurrent["AllowUPD_ByBatch"] = ddlAllowUPDByBatch.SelectedItem.Value;
			}
			return true;
			//END BY X JAN 24 08


		}

		#endregion

		#region Exception Grid procedure
		protected void dgExceptionCodes_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((int)ViewState["EXMode"] == (int)ScreenMode.Insert && (int) ViewState["EXOperation"] == (int)Operation.Insert && dgExceptionCodes.EditItemIndex > 0)
			{
				m_sdsExceptionCode.ds.Tables[0].Rows.RemoveAt(dgExceptionCodes.EditItemIndex);
				m_sdsExceptionCode.QueryResultMaxSize--;
				dgExceptionCodes.CurrentPageIndex = 0;
			}

			dgExceptionCodes.EditItemIndex = e.Item.ItemIndex;
			ViewState["EXOperation"] = Operation.Update;
			BindExGrid();
			lblEXMessage.Text = "";
			lblSCMessage.Text="";
			
		}

		public void dgExceptionCodes_Update(object sender, DataGridCommandEventArgs e)
		{
			FillEXDataRow(e.Item,e.Item.ItemIndex);

			int iOperation = (int)ViewState["EXOperation"];

			String strStatusCode = (String)ViewState["CurrentSC"];
			switch(iOperation)
			{

				case (int)Operation.Update:

					DataSet dsToUpdate = m_sdsExceptionCode.ds.GetChanges();
					SysDataMgrDAL.ModifyExceptionCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strStatusCode,dsToUpdate);
					m_sdsExceptionCode.ds.AcceptChanges();
					lblEXMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
					btnExceptionCodeInsert.Enabled = true;
					break;

				case (int)Operation.Insert:

					DataSet dsToInsert = m_sdsExceptionCode.ds.GetChanges();
					try
					{
						SysDataMgrDAL.AddExceptionCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strStatusCode,dsToInsert);
						lblEXMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						btnExceptionCodeInsert.Enabled = true;
						m_sdsExceptionCode.ds.AcceptChanges();
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;
						strMsg = appException.InnerException.InnerException.Message;
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblEXMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_EXP_CODE_NO",utility.GetUserCulture());
						}
						else if(strMsg.IndexOf("conflict") != -1 )
						{
							//unique constraint violeated //Duplicate key
							lblEXMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_EXP_CODE_NO",utility.GetUserCulture());
						}
						else
						{
							lblEXMessage.Text = strMsg;
						}
						return;						
					}
			
					break;
			}
			dgExceptionCodes.EditItemIndex = -1;
			ViewState["EXOperation"] = Operation.None;
			BindExGrid();	
			lblSCMessage.Text="";
			
		}

		protected void dgExceptionCodes_Cancel(object sender, DataGridCommandEventArgs e)
		{
			dgExceptionCodes.EditItemIndex = -1;
			if((int)ViewState["EXMode"] == (int)ScreenMode.Insert && (int)ViewState["EXOperation"] == (int)Operation.Insert)
			{
				m_sdsExceptionCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsExceptionCode.QueryResultMaxSize--;
				dgExceptionCodes.CurrentPageIndex = 0;
			}
			btnExceptionCodeInsert.Enabled = true;
			lblEXMessage.Text = "";

			ViewState["EXOperation"] = Operation.None;
			BindExGrid();
			
		}

		protected void dgExceptionCodes_Delete(object sender, DataGridCommandEventArgs e)
		{
			msTextBox txtExceptionCode = (msTextBox)e.Item.FindControl("txtExceptionCode");
			Label lblExceptionCode = (Label)e.Item.FindControl("lblExceptionCode");
			String strExceptionCode = null;
			if(txtExceptionCode != null)
			{
				strExceptionCode = txtExceptionCode.Text;
			}

			if(lblExceptionCode != null)
			{
				strExceptionCode = lblExceptionCode.Text;
			}

			String strStatusCode = (String)ViewState["CurrentSC"];
			try
			{
				SysDataMgrDAL.DeleteExceptionCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strStatusCode,strExceptionCode);
				lblEXMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblEXMessage.Text = "Error Deleting Exception code. Exception code being used in some transaction(s)";					
				}
				return;
			}

			if((int)ViewState["EXMode"] == (int)ScreenMode.Insert)
			{
				m_sdsExceptionCode.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsExceptionCode.QueryResultMaxSize--;
				dgExceptionCodes.CurrentPageIndex = 0;

				if((int) ViewState["EXOperation"] == (int)Operation.Insert && dgExceptionCodes.EditItemIndex > 0)
				{
					m_sdsExceptionCode.ds.Tables[0].Rows.RemoveAt(dgExceptionCodes.EditItemIndex-1);
					m_sdsExceptionCode.QueryResultMaxSize--;
				}
				dgExceptionCodes.EditItemIndex = -1;
				BindExGrid();
			}
			else
			{
				ShowCurrentEXPage();
			}
			

			ViewState["EXOperation"] = Operation.None;
			btnExceptionCodeInsert.Enabled = true;
			lblSCMessage.Text="";
		}

		protected void dgExceptionCodes_Bound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			DataRow drSelected = m_sdsExceptionCode.ds.Tables[0].Rows[e.Item.ItemIndex];
			if(drSelected.RowState == DataRowState.Deleted)
			{
				return;
			}

			msTextBox txtExceptionCode = (msTextBox)e.Item.FindControl("txtExceptionCode");
			int iOperation = (int) ViewState["EXOperation"];
			if(txtExceptionCode != null && iOperation == (int)Operation.Update)
			{
				txtExceptionCode.Enabled = false;
			}

			DropDownList ddlExMBG = (DropDownList)e.Item.FindControl("ddlExMBG");			
			if(ddlExMBG != null)
			{
				ddlExMBG.DataSource = LoadMBGOptions();
				ddlExMBG.DataTextField = "Text";
				ddlExMBG.DataValueField = "StringValue";
				ddlExMBG.DataBind();
				if((drSelected["mbg"]!= null) && (!drSelected["mbg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strMBG = (String)drSelected["mbg"];
					ddlExMBG.SelectedIndex = ddlExMBG.Items.IndexOf(ddlExMBG.Items.FindByValue(strMBG));//.FindByValue("N"));
					
				}				
			}

			DropDownList ddlExCloseStatus = (DropDownList)e.Item.FindControl("ddlExCloseStatus");			
			if(ddlExCloseStatus != null)
			{
				ddlExCloseStatus.DataSource = LoadCloseStatusOptions();
				ddlExCloseStatus.DataTextField = "Text";
				ddlExCloseStatus.DataValueField = "StringValue";
				ddlExCloseStatus.DataBind();
				if((drSelected["status_close"]!= null) && (!drSelected["status_close"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strCloseStatus = (String)drSelected["status_close"];
					ddlExCloseStatus.SelectedIndex = ddlExCloseStatus.Items.IndexOf(ddlExCloseStatus.Items.FindByValue(strCloseStatus));//.FindByValue("N"));
					
				}
			}

			DropDownList ddlExInvoiceable = (DropDownList)e.Item.FindControl("ddlExInvoiceable");			
			if(ddlExInvoiceable != null)
			{
				ddlExInvoiceable.DataSource = LoadInvoiceableOptions();
				ddlExInvoiceable.DataTextField = "Text";
				ddlExInvoiceable.DataValueField = "StringValue";
				ddlExInvoiceable.DataBind();
				if((drSelected["invoiceable"]!= null) && (!drSelected["invoiceable"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strInvoiceable = (String)drSelected["invoiceable"];
					ddlExInvoiceable.SelectedIndex = ddlExInvoiceable.Items.IndexOf(ddlExInvoiceable.Items.FindByValue(strInvoiceable));//.FindByValue("N"));
					
				}
			}
		}

		protected void dgExceptionCodes_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgExceptionCodes.CurrentPageIndex = e.NewPageIndex;
			ShowCurrentEXPage();
			lblEXMessage.Text = "";			
		}

		private void BindExGrid()
		{
			dgExceptionCodes.VirtualItemCount =System.Convert.ToInt32(m_sdsExceptionCode.QueryResultMaxSize);
			dgExceptionCodes.DataSource = m_sdsExceptionCode.ds;
			dgExceptionCodes.DataBind();
			Session["SESSION_DS2"] = m_sdsExceptionCode;
		}

		private void AddRowInExGrid()
		{
			SysDataMgrDAL.AddNewRowInExceptionCodeDS(m_sdsExceptionCode);
			Session["SESSION_DS2"] = m_sdsExceptionCode;
		}
		
		private void FillEXDataRow(DataGridItem item, int drIndex)
		{
			try
			{
				msTextBox txtExceptionCode = (msTextBox)item.FindControl("txtExceptionCode");
				if(txtExceptionCode != null)
				{
					DataRow drCurrent = m_sdsExceptionCode.ds.Tables[0].Rows[drIndex];
					drCurrent["exception_code"] = txtExceptionCode.Text;
				}

				TextBox txtExceptionDescription = (TextBox)item.FindControl("txtExceptionDescription");
				if(txtExceptionDescription != null)
				{
					DataRow drCurrent = m_sdsExceptionCode.ds.Tables[0].Rows[drIndex];
					drCurrent["exception_description"] = txtExceptionDescription.Text;
				}

				DropDownList ddlExMBG = (DropDownList)item.FindControl("ddlExMBG");
				if(ddlExMBG != null)
				{
					DataRow drCurrent = m_sdsExceptionCode.ds.Tables[0].Rows[drIndex];
					drCurrent["mbg"] = ddlExMBG.SelectedItem.Value;
				}

				DropDownList ddlExCloseStatus = (DropDownList)item.FindControl("ddlExCloseStatus");
				if(ddlExCloseStatus != null)
				{
					DataRow drCurrent = m_sdsExceptionCode.ds.Tables[0].Rows[drIndex];
					drCurrent["status_close"] = ddlExCloseStatus.SelectedItem.Value;
				}


				DropDownList ddlExInvoiceable = (DropDownList)item.FindControl("ddlExInvoiceable");
				if(ddlExInvoiceable != null)
				{
					DataRow drCurrent = m_sdsExceptionCode.ds.Tables[0].Rows[drIndex];
					drCurrent["invoiceable"] = ddlExInvoiceable.SelectedItem.Value;
				}
			}
			catch(Exception ex)
			{
				String msg = ex.ToString();
				lblEXMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_EXP_CODE_NO",utility.GetUserCulture());
			}

		}

		private void ShowCurrentEXPage()
		{
			int iStartIndex = dgExceptionCodes.CurrentPageIndex * dgExceptionCodes.PageSize;
			String strStatusCode = (String)ViewState["CurrentSC"];
			m_sdsExceptionCode = SysDataMgrDAL.GetExceptionCodeDS(utility.GetAppID(),utility.GetEnterpriseID(),strStatusCode,iStartIndex,dgExceptionCodes.PageSize);

			decimal pgCnt = (m_sdsExceptionCode.QueryResultMaxSize - 1)/dgExceptionCodes.PageSize;
			if(pgCnt < dgExceptionCodes.CurrentPageIndex)
			{
				dgExceptionCodes.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			dgExceptionCodes.EditItemIndex = -1;
			BindExGrid();
			//lblEXMessage.Text = "";
		}

		#endregion

		/// <summary>
		/// Other methods..
		/// </summary>
		/// <param name="bNilValue"></param>

		protected ICollection LoadMBGOptions()
		{
			
			return m_dvMBGOptions;
		}

		private DataView GetMBGOptions(bool showNilOption) 
		{
			DataTable dtMBGOptions = new DataTable();
 
			dtMBGOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMBGOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList mbgOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"mbg",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtMBGOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtMBGOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode mbgSysCode in mbgOptionArray)
			{
				DataRow drEach = dtMBGOptions.NewRow();
				drEach[0] = mbgSysCode.Text;
				drEach[1] = mbgSysCode.StringValue;
				dtMBGOptions.Rows.Add(drEach);
			}

			DataView dvMBGOptions = new DataView(dtMBGOptions);
			return dvMBGOptions;
		}


		protected ICollection LoadShipmentUpdateOptions()
		{
			
			return m_dvShipmentUpdateOptions;
		}

		protected ICollection LoadScanningTypeOptions()
		{
			
			return m_dvScanningTypeOptions;
		}

		private DataView GetShipmentUpdateOptions(bool showNilOption) 
		{
			DataTable dtShipmentUpdateOptions = new DataTable();
 
			dtShipmentUpdateOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtShipmentUpdateOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList shipmentUpdateOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"status_type",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtShipmentUpdateOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtShipmentUpdateOptions.Rows.Add(drNilRow);
			}
	
			foreach(SystemCode shipmentUpdateSysCode in shipmentUpdateOptionArray)
			{
				DataRow drEach = dtShipmentUpdateOptions.NewRow();
				drEach[0] = shipmentUpdateSysCode.Text;
				drEach[1] = shipmentUpdateSysCode.StringValue;
				dtShipmentUpdateOptions.Rows.Add(drEach);
			}

			DataView dvShipmentUpdateOptions = new DataView(dtShipmentUpdateOptions);
			return dvShipmentUpdateOptions;
		}


		private DataView GetScanningType(bool showNilOption) 
		{
			DataTable dtScanningType = new DataTable();
 
			dtScanningType.Columns.Add(new DataColumn("Text", typeof(string)));
			dtScanningType.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList ScanningTypeOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"scanning_type",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtScanningType.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtScanningType.Rows.Add(drNilRow);
			}
	
			foreach(SystemCode shipmentUpdateSysCode in ScanningTypeOptionArray)
			{
				DataRow drEach = dtScanningType.NewRow();
				drEach[0] = shipmentUpdateSysCode.Text;
				drEach[1] = shipmentUpdateSysCode.StringValue;
				dtScanningType.Rows.Add(drEach);
			}

			DataView dvShipmentUpdateOptions = new DataView(dtScanningType);
			return dvShipmentUpdateOptions;
		}


		protected ICollection LoadCloseStatusOptions()
		{
			
			return m_dvCloseStatusOptions;
		}

		private DataView GetCloseStatusOptions(bool showNilOption) 
		{
			DataTable dtCloseStatusOptions = new DataTable();
 
			dtCloseStatusOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtCloseStatusOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList closeStatusOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"close_status",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtCloseStatusOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtCloseStatusOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode closeStatusSysCode in closeStatusOptionArray)
			{
				DataRow drEach = dtCloseStatusOptions.NewRow();
				drEach[0] = closeStatusSysCode.Text;
				drEach[1] = closeStatusSysCode.StringValue;
				dtCloseStatusOptions.Rows.Add(drEach);
			}

			DataView dvCloseStatusOptions = new DataView(dtCloseStatusOptions);
			return dvCloseStatusOptions;
		}
		protected ICollection LoadAllowUPDByBatchOptions()
		{
			
			return m_dvAllowUPDByBatchOptions;
		}
		private DataView GetAllowUPDByBatchOptions(bool showNilOption) 
		{
			DataTable dtAllowUPDByBatchOptions = new DataTable();
 
			dtAllowUPDByBatchOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtAllowUPDByBatchOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList AllowUPDByBatchOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"AllowUPD_byBatch",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtAllowUPDByBatchOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtAllowUPDByBatchOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode AllowUPDByBatchSysCode in AllowUPDByBatchOptionArray)
			{
				DataRow drEach = dtAllowUPDByBatchOptions.NewRow();
				drEach[0] = AllowUPDByBatchSysCode.Text;
				drEach[1] = AllowUPDByBatchSysCode.StringValue;
				dtAllowUPDByBatchOptions.Rows.Add(drEach);
			}

			DataView dvAllowUPDByBatchOptions = new DataView(dtAllowUPDByBatchOptions);
			return dvAllowUPDByBatchOptions;
		}
		protected ICollection LoadInvoiceableOptions()
		{
			
			return m_dvInvoiceableOptions;
		}

		private DataView GetInvoiceableOptions(bool showNilOption) 
		{
			
			DataTable dtInvoiceableOptions = new DataTable();
			
			dtInvoiceableOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtInvoiceableOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList invoiceableOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"invoiceable",CodeValueType.StringValue);


			if(showNilOption)
			{
				DataRow drNilRow = dtInvoiceableOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtInvoiceableOptions.Rows.Add(drNilRow);
			}
	


			foreach(SystemCode invoiceableSysCode in invoiceableOptionArray)
			{
				DataRow drEach = dtInvoiceableOptions.NewRow();
				drEach[0] = invoiceableSysCode.Text;
				drEach[1] = invoiceableSysCode.StringValue;
				dtInvoiceableOptions.Rows.Add(drEach);
			}

			DataView dvInvoiceableOptions = new DataView(dtInvoiceableOptions);
			return dvInvoiceableOptions;
		}

		protected ICollection LoadAllowInSWB_EmuOptions()
		{
			
			return m_dvAllowInSWB_EmuOptions;
		}

		private DataView GetAllowInSWB_EmuOptions(bool showNilOption) 
		{
			DataTable dtAllowInSWB_EmuOptions = new DataTable();
 
			dtAllowInSWB_EmuOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtAllowInSWB_EmuOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList AllowInSWB_EmuOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"AllowInSWB_Emu",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtAllowInSWB_EmuOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtAllowInSWB_EmuOptions.Rows.Add(drNilRow);
			}
	
			foreach(SystemCode AllowInSWB_EmuSysCode in AllowInSWB_EmuOptionArray)
			{
				DataRow drEach = dtAllowInSWB_EmuOptions.NewRow();
				drEach[0] = AllowInSWB_EmuSysCode.Text;
				drEach[1] = AllowInSWB_EmuSysCode.StringValue;
				dtAllowInSWB_EmuOptions.Rows.Add(drEach);
			}

			DataView dvAllowInSWB_EmuOptions = new DataView(dtAllowInSWB_EmuOptions);
			return dvAllowInSWB_EmuOptions;
		}
		
		protected ICollection LoadAllowUPD_ByBatchOptions()
		{
			
			return m_dvAllowUPD_ByBatchOptions;
		}

		private DataView GetAllowUPD_ByBatchOptions(bool showNilOption) 
		{
			DataTable dtAllowUPD_ByBatchOptions = new DataTable();
 
			dtAllowUPD_ByBatchOptions.Columns.Add(new DataColumn("Text", typeof(string)));
			dtAllowUPD_ByBatchOptions.Columns.Add(new DataColumn("StringValue", typeof(string)));

			ArrayList AllowUPD_ByBatchOptionArray = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"AllowUPD_ByBatch",CodeValueType.StringValue);

			if(showNilOption)
			{
				DataRow drNilRow = dtAllowUPD_ByBatchOptions.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtAllowUPD_ByBatchOptions.Rows.Add(drNilRow);
			}

			foreach(SystemCode AllowUPD_ByBatchSysCode in AllowUPD_ByBatchOptionArray)
			{
				DataRow drEach = dtAllowUPD_ByBatchOptions.NewRow();
				drEach[0] = AllowUPD_ByBatchSysCode.Text;
				drEach[1] = AllowUPD_ByBatchSysCode.StringValue;
				dtAllowUPD_ByBatchOptions.Rows.Add(drEach);
			}

			DataView dvAllowUPD_ByBatchOptions = new DataView(dtAllowUPD_ByBatchOptions);
			return dvAllowUPD_ByBatchOptions;
		}

		private void LoadComboLists(bool bNilValue)
		{
			m_dvShipmentUpdateOptions = GetShipmentUpdateOptions(bNilValue);
			m_dvCloseStatusOptions = GetCloseStatusOptions(bNilValue);
			m_dvInvoiceableOptions = GetInvoiceableOptions(bNilValue);
			m_dvMBGOptions = GetMBGOptions(bNilValue);
			m_dvScanningTypeOptions = GetScanningType(true);
			m_dvAllowUPDByBatchOptions=GetAllowUPDByBatchOptions(bNilValue);
//			m_dvAllowInSWB_EmuOptions = GetAllowInSWB_EmuOptions(bNilValue);
//			m_dvAllowUPD_ByBatchOptions = GetAllowUPD_ByBatchOptions(bNilValue);
		}

		private void ShowButtonColumns(bool bShow)
		{
			dgStatusCodes.Columns[0].Visible = bShow;
			dgStatusCodes.Columns[1].Visible = bShow;
			dgStatusCodes.Columns[2].Visible = bShow;
		}

		private void ResetDetailsGrid()
		{
			dgExceptionCodes.CurrentPageIndex = 0;
			btnExceptionCodeInsert.Enabled = false;			
			m_sdsExceptionCode = SysDataMgrDAL.GetEmptyExceptionCodeDS(0);
			Session["SESSION_DS2"] = m_sdsExceptionCode;
			lblEXMessage.Text = "";
			BindExGrid();

		}
		
	
	}
}
