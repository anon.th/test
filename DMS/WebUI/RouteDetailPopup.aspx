<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<%@ Page language="c#" Codebehind="RouteDetailPopup.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.RouteDetailPopup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>RouteDetailPopup</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="RouteDetailPopup" method="post" runat="server">
			<TABLE id="ShipmentUpdMainTable" style="Z-INDEX: 112; WIDTH: 724px" width="724" border="0" runat="server">
				<tr>
					<td>
						<TABLE id="tblShipmentUpdate" style="WIDTH: 721px" width="721" align="left" border="0">
							<TR height="9">
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="76" style="WIDTH: 76px"></TD>
								<TD width="1" style="WIDTH: 1px"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
								<TD width="5%"></TD>
							</TR>
							<tr height="27">
								<td colSpan="15"><asp:label id="lblErrorMsg" runat="server" Width="100%" CssClass="errorMsgColor"></asp:label></td>
								<td colSpan="5"></td>
							</tr>
							<tr height="27">
								<TD colSpan="1"></TD>
								<TD colSpan="3"><asp:label id="lblRouteCode" tabIndex="7" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Route Code</asp:label></TD>
								<TD colSpan="3"><cc1:mstextbox id="txtRouteCode" tabIndex="8" runat="server" Width="100%" CssClass="textField" AutoPostBack="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></TD>
								<TD style="WIDTH: 76px"><FONT face="Tahoma"><FONT face="Tahoma">
											<asp:button id="btnFind" tabIndex="13" runat="server" CssClass="queryButton" Width="100%" Text="Search" CausesValidation="False"></asp:button></FONT></FONT></TD>
								<td colSpan="13"></td>
							</tr>
							<tr height="27">
								<TD colSpan="1"></TD>
								<TD colSpan="3"><asp:label id="lblDeprtDate" tabIndex="11" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Departure Date</asp:label></TD>
								<TD colSpan="3"><cc1:mstextbox id="txtDeprtDate" tabIndex="12" runat="server" Width="100%" CssClass="textField" AutoPostBack="True" MaxLength="10" TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
								<td colSpan="2" style="WIDTH: 62px">
									<asp:button id="btnClose" tabIndex="13" runat="server" CssClass="queryButton" Width="100%" Text="Close" CausesValidation="False"></asp:button></td>
								<td colSpan="11"></td>
							</tr>
							<tr height="27">
								<td colspan="20">&nbsp;</td>
							</tr>
							<tr>
								<TD vAlign="top" colSpan="19"><asp:datagrid id="dgRoute" tabIndex="17" runat="server" Width="100%" ItemStyle-Height="20" AutoGenerateColumns="False" OnItemCommand="dgShipTrack_OnItemCommand" SelectedItemStyle-CssClass="gridFieldSelected">
										<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
										<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="Route Code">
												<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle Wrap="False"></ItemStyle>
												<ItemTemplate>
													<asp:LinkButton CssClass="gridLabel" ID="lblDGRouteCode" Text='<%#DataBinder.Eval(Container.DataItem,"route_code")%>' Runat="server">
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Description">
												<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
												<ItemTemplate>
													<asp:Label CssClass="gridLabel" ID="lblDescription" Text='<%#DataBinder.Eval(Container.DataItem,"route_description")%>' Runat="server" Enabled="True">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Departure Date">
												<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label CssClass="gridLabel" ID="lblDepartDate" Text='<%#DataBinder.Eval(Container.DataItem,"departure_date","{0:dd/MM/yyyy HH:mm}")%>' Runat="server" Enabled="True">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Vehicle No">
												<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label CssClass="gridLabel" ID="lblVehicleNo" Text='<%#DataBinder.Eval(Container.DataItem,"flight_vehicle_no")%>' Runat="server" Enabled="True">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Driver Name">
												<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label CssClass="gridLabel" ID="lblDriverName" Text='<%#DataBinder.Eval(Container.DataItem,"awb_driver_name")%>' Runat="server" Enabled="True">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="" Visible="False">
												<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label CssClass="gridLabel" ID="lblOriginStateCode" Text='<%#DataBinder.Eval(Container.DataItem,"origin_state_code")%>' Runat="server" Enabled="True">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="" Visible="False">
												<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
												<ItemStyle VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label CssClass="gridLabel" ID="lblDestinationStateCode" Text='<%#DataBinder.Eval(Container.DataItem,"destination_state_code")%>' Runat="server" Enabled="True">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></TD>
								<td colSpan="1"></td>
							</tr>
							<tr height="27">
								<TD colSpan="9" style="WIDTH: 313px"></TD>
								<TD colSpan="2"></TD>
								<TD colSpan="9"></TD>
							</tr>
						</TABLE>
					</td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>
