using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using TIESDAL;
using TIESClasses;

using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;

using com.common.applicationpages;
using com.ties;
using System.Text;
using System.Globalization;
using System.IO;


namespace TIES.WebUI
{
    /// <summary>
    /// Summary description for InvoicedConsigmentPrinting.
    /// </summary>
    public class InvoicedConsigmentPrinting : BasePage
    {
        protected System.Web.UI.WebControls.ValidationSummary Validationsummary1;
        protected System.Web.UI.WebControls.Label Label1;
        protected System.Web.UI.WebControls.Button btnQry;
        protected System.Web.UI.WebControls.Button btnExecQry;
        protected System.Web.UI.WebControls.Button btnReprintConsNote;
        protected System.Web.UI.WebControls.Button btnPrintAgencyInvoice;
        protected System.Web.UI.WebControls.Button btnSelectAll;
        protected System.Web.UI.WebControls.Label lblSearch;
        protected System.Web.UI.WebControls.Label Label4;
        protected System.Web.UI.WebControls.Label lblErrorMsg;
        protected System.Web.UI.WebControls.DataGrid dgConsignmentStatus;
        protected System.Web.UI.HtmlControls.HtmlTableRow TrNonCustoms;
        protected System.Web.UI.WebControls.Label Label2;
        protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
        protected System.Web.UI.HtmlControls.HtmlTable MainTable;
        protected System.Web.UI.HtmlControls.HtmlTable Table1;
        protected System.Web.UI.WebControls.Label Label3;
        protected System.Web.UI.WebControls.Label Label5;
        protected System.Web.UI.WebControls.Label lblTo;
        protected System.Web.UI.WebControls.Label Label7;
        protected System.Web.UI.HtmlControls.HtmlTableRow TrCustoms;

        private string appID = null;
        private string enterpriseID = null;
        private string userID = null;
        protected System.Web.UI.WebControls.TextBox txtConsignNo;
        protected System.Web.UI.WebControls.TextBox txtCustomerID;
        protected com.common.util.msTextBox txtManifestedDataFrom;
        protected com.common.util.msTextBox txtManifestedDatato;
        protected com.common.util.msTextBox txtInvoiceDateFrom;
        protected com.common.util.msTextBox txtInvoiceDateto;

        public com.ties.classes.ConsignmentStatusPrintingConfigurations EnterpriseConfig
        {
            get
            {
                if (ViewState["EnterpriseConfigurations"] == null)
                {
                    ViewState["EnterpriseConfigurations"] = new com.ties.classes.ConsignmentStatusPrintingConfigurations();
                    return null;
                }
                return (com.ties.classes.ConsignmentStatusPrintingConfigurations)ViewState["EnterpriseConfigurations"];
            }
            set
            {
                ViewState["EnterpriseConfigurations"] = value;
            }
        }

        private void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here

            appID = utility.GetAppID();
            enterpriseID = utility.GetEnterpriseID();
            userID = utility.GetUserID();

            // Put user code to initialize the page here	
            if (Page.IsPostBack == false)
            {
                GridRows = EnterpriseConfigMgrDAL.ConsStatusPrintingGridRows(appID, enterpriseID);

                //txtRecipPhoneNo.Attributes.Add("onkeypress","if (window.event.keyCode >= 48 && window.event.keyCode <= 57) return ture; else return false;");
                //txtShipList.Attributes.Add("onkeypress","if (window.event.keyCode >= 48 && window.event.keyCode <= 57) return ture; else return false;");
                clearscreen();
                //BindControl();
            }
            else
            {
                string cons = "", agencyinv = "";
                foreach (DataGridItem item in dgConsignmentStatus.Items)
                {
                    CheckBox chkConNo = (CheckBox)item.FindControl("chkSelect");
                    if (chkConNo != null && chkConNo.Checked == true)
                    {
                        string id = chkConNo.Attributes["CrsID"].ToString();
                        if (cons.Length > 0)
                        {
                            cons += ";";
                        }
                        cons += id;

                        string AgencyInvoiceID = chkConNo.Attributes["AgencyInvoice"].ToString();
                        if (agencyinv.Length > 0)
                        {
                            agencyinv += ";";
                        }
                        agencyinv += AgencyInvoiceID;
                    }
                }

                if (cons != "")
                {
                    btnReprintConsNote.Enabled = true;
                }
                else
                {
                    btnReprintConsNote.Enabled = false;
                }

                if (agencyinv != "")
                {
                    btnPrintAgencyInvoice.Enabled = true;
                }
                else
                {
                    btnPrintAgencyInvoice.Enabled = false;
                }
            }
        }




        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            base.OnInit(e);
            InitializeComponent();
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
            this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
            this.btnReprintConsNote.Click += new System.EventHandler(this.btnReprintConsNote_Click);
            this.dgConsignmentStatus.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgConsignmentStatus_PageIndexChanged);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        public string ConsList
        {
            get
            {
                if (ViewState["ConsList"] == null)
                {
                    return "";
                }
                else
                {
                    return (string)ViewState["ConsList"];
                }
            }
            set
            {
                ViewState["ConsList"] = value;
            }
        }
        public string ExcConsignment_no
        {
            get
            {
                if (ViewState["ExcConsignment_no"] == null)
                {
                    return "";
                }
                else
                {
                    return (string)ViewState["ExcConsignment_no"];
                }
            }
            set
            {
                ViewState["ExcConsignment_no"] = value;
            }
        }
        public string Payer_id
        {
            get
            {
                if (ViewState["Payer_id"] == null)
                {
                    return "";
                }
                else
                {
                    return (string)ViewState["Payer_id"];
                }
            }
            set
            {
                ViewState["Payer_id"] = value;
            }
        }
        public DateTime ExcManifestedDataFrom
        {
            get
            {
                if (ViewState["ExcManifestedDataFrom"] == null)
                {
                    return DateTime.MinValue;
                }
                else
                {
                    return (DateTime)ViewState["ExcManifestedDataFrom"];
                }
            }
            set
            {
                ViewState["ExcManifestedDataFrom"] = value;
            }
        }
        public DateTime ExcManifestedDatato
        {
            get
            {
                if (ViewState["ExcManifestedDatato"] == null)
                {
                    return DateTime.MinValue;
                }
                else
                {
                    return (DateTime)ViewState["ExcManifestedDatato"];
                }
            }
            set
            {
                ViewState["ExcManifestedDatato"] = value;
            }
        }
        public DateTime ExcInvoiceDataFrom
        {
            get
            {
                if (ViewState["ExcInvoiceDataFrom"] == null)
                {
                    return DateTime.MinValue;
                }
                else
                {
                    return (DateTime)ViewState["ExcInvoiceDataFrom"];
                }
            }
            set
            {
                ViewState["ExcInvoiceDataFrom"] = value;
            }
        }
        public DateTime ExcInvoiceDatato
        {
            get
            {
                if (ViewState["ExcInvoiceDatato"] == null)
                {
                    return DateTime.MinValue;
                }
                else
                {
                    return (DateTime)ViewState["ExcInvoiceDatato"];
                }
            }
            set
            {
                ViewState["ExcInvoiceDatato"] = value;
            }
        }


        private void btnReprintConsNote_Click(object sender, System.EventArgs e)
        {
            ViewState["BindStatus"] = "ReprintConsNote";
            ConsList = "";
            string cons = "";
            foreach (DataGridItem item in dgConsignmentStatus.Items)
            {
                CheckBox chkConNo = (CheckBox)item.FindControl("chkSelect");
                if (chkConNo != null && chkConNo.Checked == true)
                {
                    string id = chkConNo.Attributes["CrsID"].ToString();
                    if (cons.Length > 0)
                    {
                        cons += ";";
                    }
                    cons += id;
                }
            }
            ConsList = cons;

            if (this.ConsList.Length > 0)
            {
                string strConsignmentNo = this.ConsList;

                DataSet dsConsignments = CustomerConsignmentDAL.ReprintInvoicedConsNote(appID, enterpriseID, userID, strConsignmentNo, 0, -1);
                if (dsConsignments != null && dsConsignments.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToInt32(dsConsignments.Tables[0].Rows[0]["ErrorCode"]) > 0)
                    {
                        this.lblErrorMsg.Text = dsConsignments.Tables[0].Rows[0]["ErrorMessage"].ToString();
                    }
                    else if (dsConsignments.Tables[0].Rows[0]["ReportTemplate"].ToString() == "")
                    {
                        lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "TEMPLATE_NOT_FOUND", utility.GetUserCulture()); ;
                    }
                    else
                    {
                        lblErrorMsg.Text = "";
                    }
                }
                if (lblErrorMsg.Text == "")
                {
                    String strUrl = null;
                    strUrl = "ReportViewer1.aspx";
                    String reportTemplate = dsConsignments.Tables[0].Rows[0]["ReportTemplate"].ToString();
                    Session["REPORT_TEMPLATE"] = reportTemplate;
                    Session["FORMID"] = "RePrintConsNotes";
                    Session["SESSION_DS_PRINTCONSNOTES"] = dsConsignments;
                    Session["FormatType"] = ".pdf";
                    ArrayList paramList = new ArrayList();
                    paramList.Add(strUrl);
                    String sScript = Utility.GetScript("openParentWindowReport.js", paramList);
                    Utility.RegisterScriptString(sScript, "ShowPopupScript", this.Page);


                    this.dgConsignmentStatus.CurrentPageIndex = 0;
                    this.dgConsignmentStatus.PageSize = GridRows;
                    BindGrid();
                }
            }
        }

        public int GridRows
        {
            get
            {
                if (ViewState["GridRows"] == null)
                {
                    return 50;
                }
                else
                {
                    return (int)ViewState["GridRows"];
                }
            }
            set
            {
                ViewState["GridRows"] = value;
            }
        }

        private void BindGrid()
        {
            string strAppID = this.appID;
            string strEnterpriseID = this.enterpriseID;
            string userloggedin = this.userID;

            string consignment_no = ExcConsignment_no;
            string payerid = Payer_id;
            DateTime ManifestedDataFrom = ExcManifestedDataFrom;
            DateTime ManifestedDatato = ExcManifestedDatato;
            DateTime InvoiceDataFrom = ExcInvoiceDataFrom;
            DateTime InvoiceDatato = ExcInvoiceDatato;

            DataSet dsResult = CustomerConsignmentDAL.QueryInvoicedConsignment(strAppID, strEnterpriseID, userloggedin, Payer_id,
                ExcConsignment_no, ExcManifestedDataFrom,
                ExcManifestedDatato, ExcInvoiceDataFrom, ExcInvoiceDatato, DateTime.MinValue, DateTime.MinValue, 0);

            System.Data.DataTable dtList = dsResult.Tables[0];
            //System.Data.DataTable dtList = dsResult.Tables[1];
            dtList.Columns.Add("Check", typeof(bool));
            this.dgConsignmentStatus.DataSource = dtList;
            this.dgConsignmentStatus.DataBind();
            lblErrorMsg.Text = "";
            btnReprintConsNote.Enabled = false;
        }

        private void btnQry_Click(object sender, System.EventArgs e)
        {

            ViewState["BindStatus"] = "Qry";
            this.dgConsignmentStatus.CurrentPageIndex = 0;
            this.dgConsignmentStatus.PageSize = GridRows;
            this.dgConsignmentStatus.DataSource = null;
            this.dgConsignmentStatus.DataBind();

            clearscreen();
        }

        private void clearscreen()
        {

            this.txtConsignNo.Text = "";
            this.txtCustomerID.Text = "";
            this.txtManifestedDataFrom.Text = "";
            this.txtManifestedDatato.Text = "";
            this.txtInvoiceDateFrom.Text = "";
            this.txtInvoiceDateto.Text = "";

            lblErrorMsg.Text = "";

            ConsList = "";

            DataSet dsResult = CustomerConsignmentDAL.CSS_ConsignmentStatus(this.appID, this.enterpriseID, "-1", "-1", null, -1, null, null, null, null, null, DateTime.MinValue, DateTime.MinValue, null, null, null);
            System.Data.DataTable dtList = dsResult.Tables[1];
            dtList.Columns.Add("Check", typeof(bool));

            this.dgConsignmentStatus.CurrentPageIndex = 0;
            this.dgConsignmentStatus.PageSize = GridRows;
            this.dgConsignmentStatus.DataSource = dtList;
            this.dgConsignmentStatus.DataBind();
            this.btnExecQry.Enabled = true;
            this.btnSelectAll.Enabled = false;
        }

        private void btnExecQry_Click(object sender, System.EventArgs e)
        {
            //Binding Data

            this.ExcConsignment_no = "";
            if (txtConsignNo.Text.Trim() != "")
            {
                this.ExcConsignment_no = txtConsignNo.Text.Trim();
            }

            this.Payer_id = "";
            if (txtCustomerID.Text.Trim() != "")
            {
                this.Payer_id = txtCustomerID.Text.Trim();
            }

            this.ExcManifestedDataFrom = DateTime.MinValue;
            if (txtManifestedDataFrom.Text.Trim() != "")
            {
                try
                {
                    this.ExcManifestedDataFrom = DateTime.ParseExact(txtManifestedDataFrom.Text.Trim(), "dd/MM/yyyy", null);
                }
                catch
                {
                    this.ExcManifestedDataFrom = DateTime.MinValue;
                }
            }

            this.ExcManifestedDatato = DateTime.MinValue;
            if (txtManifestedDatato.Text.Trim() != "")
            {
                try
                {
                    this.ExcManifestedDatato = DateTime.ParseExact(txtManifestedDatato.Text.Trim(), "dd/MM/yyyy", null);
                }
                catch
                {
                    this.ExcManifestedDatato = DateTime.MinValue;
                }
            }

            this.ExcInvoiceDataFrom = DateTime.MinValue;
            if (txtInvoiceDateFrom.Text.Trim() != "")
            {
                try
                {
                    this.ExcInvoiceDataFrom = DateTime.ParseExact(txtInvoiceDateFrom.Text.Trim(), "dd/MM/yyyy", null);
                }
                catch
                {
                    this.ExcInvoiceDataFrom = DateTime.MinValue;
                }
            }

            this.ExcInvoiceDatato = DateTime.MinValue;
            if (txtInvoiceDateto.Text.Trim() != "")
            {
                try
                {
                    this.ExcInvoiceDatato = DateTime.ParseExact(txtInvoiceDateto.Text.Trim(), "dd/MM/yyyy", null);
                }
                catch
                {
                    this.ExcInvoiceDatato = DateTime.MinValue;
                }
            }

            //change enabled status of buttons
            btnQry.Enabled = true;

            btnReprintConsNote.Enabled = true;
            btnSelectAll.Enabled = true;

            ViewState["BindStatus"] = "Execute";
            this.dgConsignmentStatus.CurrentPageIndex = 0;
            this.dgConsignmentStatus.PageSize = GridRows;
            BindGrid();
        }

        private void dgConsignmentStatus_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            dgConsignmentStatus.CurrentPageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            this.lblErrorMsg.Text = "";

            CheckAll(500);

        }

        private void CheckAll(int maxSelectedCount)
        {
            //int selectedCount = 1;
            int CountSeletct = 0;
            foreach (DataGridItem dgItem in this.dgConsignmentStatus.Items)
            {
                CheckBox getCheck = (CheckBox)dgItem.Cells[0].Controls[1];

                getCheck.Checked = true;
                if (getCheck.Checked == true)
                {
                    if (CountSeletct <= maxSelectedCount)
                        CountSeletct++;
                }

                if (maxSelectedCount > 0 && CountSeletct > maxSelectedCount)
                {
                    getCheck.Enabled = false;
                    getCheck.Checked = false;
                    //break;
                }
            }
            if (CountSeletct > maxSelectedCount)
                CountSeletct -= 1;
            if (CountSeletct > 0)
            {
                lblErrorMsg.Text = CountSeletct.ToString() + " Selected";
                btnReprintConsNote.Enabled = true;
            }
        }

        protected void btnPrintAgencyInvoice_Click(object sender, EventArgs e)
        {
            ViewState["BindStatus"] = "ReprintConsNote";
            ConsList = "";
            string cons = "";
            foreach (DataGridItem item in dgConsignmentStatus.Items)
            {
                CheckBox chkConNo = (CheckBox)item.FindControl("chkSelect");
                if (chkConNo != null && chkConNo.Checked == true)
                {
                    string id = chkConNo.Attributes["AgencyInvoice"].ToString();
                    if (cons.Length > 0)
                    {
                        cons += ";";
                    }
                    cons += id;
                }
            }
            ConsList = cons;

            if (this.ConsList.Length > 0)
            {
                string strAgencyInvoiceNo = this.ConsList;

                DataSet m_dsQuery = CustomsDAL.Customs_Invoice_Report(appID, enterpriseID, userID, strAgencyInvoiceNo);

                if (m_dsQuery != null && m_dsQuery.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToInt32(m_dsQuery.Tables[0].Rows[0]["ErrorCode"]) > 0)
                    {
                        this.lblErrorMsg.Text = m_dsQuery.Tables[0].Rows[0]["ErrorMessage"].ToString();
                    }
                    else if (m_dsQuery.Tables[0].Rows[0]["InvoiceTemplate"].ToString() == "")
                    {
                        lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "TEMPLATE_NOT_FOUND", utility.GetUserCulture()); ;
                    }
                    else
                    {
                        lblErrorMsg.Text = "";
                    }
                }
                if (lblErrorMsg.Text == "")
                {
                    String strUrl = null;
                    strUrl = "ReportViewer1.aspx";
                    String reportTemplate = m_dsQuery.Tables[0].Rows[0]["InvoiceTemplate"].ToString();
                    Session["REPORT_TEMPLATE"] = reportTemplate;
                    Session["FORMID"] = "Customs_Invoice_InvoicedPrinting";
                    Session["SESSION_DS_CUSTOMS_INVOICE"] = m_dsQuery;
                    Session["SESSION_DS_CUSTOMS_INVOICE_NO"] = m_dsQuery.Tables[1].Rows[0]["Invoice_Number"].ToString();
                    Session["FormatType"] = ".pdf";
                    ArrayList paramList = new ArrayList();
                    paramList.Add(strUrl);
                    String sScript = Utility.GetScript("openParentWindowReport.js", paramList);
                    Utility.RegisterScriptString(sScript, "ShowPopupScript", this.Page);


                    this.dgConsignmentStatus.CurrentPageIndex = 0;
                    this.dgConsignmentStatus.PageSize = GridRows;
                    BindGrid();
                }
            }
        }

    }
}