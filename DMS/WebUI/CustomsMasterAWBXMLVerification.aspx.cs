using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.applicationpages;
using TIESDAL;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CustomsMasterAWBXMLVerification.
	/// </summary>

	public class CustomsMasterAWBXMLVerification : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnAccept;
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.DataGrid GrdAWBXMLVerification;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.WebControls.TextBox txtMasterAWBNo;

		//Utility Utility = null;
		string appID = null;
		string enterpriseID = null;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label37;
		string userID = null;
		string verifiedBy = string.Empty;
		string strverifiedDT = string.Empty;

	
		private void Page_Load(object sender, System.EventArgs e)
		{
			//Utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();
			txtMasterAWBNo.Text = Convert.ToString(Session["MAWBNumber"]);
			
			if(!Page.IsPostBack)
			{
				GetDataCustomsMAWBVerify();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.GrdAWBXMLVerification.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.GrdAWBXMLVerification_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void GetDataCustomsMAWBVerify()
		{
			DataSet dsCustomsMAWBVerify = CustomsDAL.Customs_AWB_XML_Verification(appID, enterpriseID, userID, "0", Convert.ToString(Session["MAWBNumber"]));
			
			if(dsCustomsMAWBVerify.Tables[0].Rows.Count > 0)
			{
				GrdAWBXMLVerification.DataSource = dsCustomsMAWBVerify.Tables[0];
				GrdAWBXMLVerification.DataBind();
			}
			
		}

		private void btnAccept_Click(object sender, System.EventArgs e)
		{
			DataSet dsCustomsMAWBVerify = CustomsDAL.Customs_AWB_XML_Verification(appID, enterpriseID, userID, "1", Convert.ToString(Session["MAWBNumber"]));

			if(dsCustomsMAWBVerify.Tables[0].Rows.Count > 0)
			{
				verifiedBy = dsCustomsMAWBVerify.Tables[0].Rows[0]["VerifyBy"].ToString();
				DateTime dtverifiedDT = Convert.ToDateTime(dsCustomsMAWBVerify.Tables[0].Rows[0]["VerrifyDate"]);
				strverifiedDT = dtverifiedDT.ToString("dd/MM/yyyy HH:mm");

				Session["VerifiedBy"] = verifiedBy;
				Session["StrverifiedDT"] = strverifiedDT;

				string sScript = "";
				sScript += "<script language=javascript>";
//				sScript += "    window.opener.document.getElementById('VerifiedBy').value = '" + verifiedBy + "';";
//				sScript += "    window.opener.document.getElementById('VerifiedDT').value = '" + strverifiedDT + "';";
				sScript += "    window.opener.callbackbtnVerifyClient();";
				sScript += "    window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			string sScript = "";
			sScript += "<script language=javascript>";
			sScript += "    window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void GrdAWBXMLVerification_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{

				Color colour = ColorTranslator.FromHtml("#FFCC00");

				Label lblShipFlightNo_Color = (Label)e.Item.FindControl("lblShipFlightNo_Color");
				Label lblDepartureCity_Color = (Label)e.Item.FindControl("lblDepartureCity_Color");
				Label lblDepartureCountry_Color = (Label)e.Item.FindControl("lblDepartureCountry_Color");
				Label lblArrivalDate_Color = (Label)e.Item.FindControl("lblArrivalDate_Color");
				Label lblTransportMode_Color = (Label)e.Item.FindControl("lblTransportMode_Color");
				Label lblCustomerID_Color = (Label)e.Item.FindControl("lblCustomerID_Color");
				Label lblExporterName_Color = (Label)e.Item.FindControl("lblExporterName_Color");
				Label lblExporterAddress1_Color = (Label)e.Item.FindControl("lblExporterAddress1_Color");
				Label lblExporterAddress2_Color = (Label)e.Item.FindControl("lblExporterAddress2_Color");
				Label lblExporterCountry_Color = (Label)e.Item.FindControl("lblExporterCountry_Color");
				Label lblGoodsDescription_Color = (Label)e.Item.FindControl("lblGoodsDescription_Color");
				Label lblPkgQty_Color = (Label)e.Item.FindControl("lblPkgQty_Color");
				Label lblWeight_Color = (Label)e.Item.FindControl("lblWeight_Color");
				Label lblArrivalDate = (Label)e.Item.FindControl("lblArrivalDate");

				if(lblShipFlightNo_Color != null)
				{
					if(lblShipFlightNo_Color.Text.Equals("1"))
					{
						e.Item.Cells[3].BackColor = colour;
					}
				}

				if(lblDepartureCity_Color != null)
				{
					if(lblDepartureCity_Color.Text.Equals("1"))
					{
						e.Item.Cells[4].BackColor = colour;
					}
				}

				if(lblDepartureCountry_Color != null)
				{
					if(lblDepartureCountry_Color.Text.Equals("1"))
					{
						e.Item.Cells[5].BackColor = colour;
					}
				}

				if(lblArrivalDate_Color != null)
				{
					if(lblArrivalDate_Color.Text.Equals("1"))
					{
						e.Item.Cells[6].BackColor = colour;
					}
				}

				if(lblTransportMode_Color != null)
				{
					if(lblTransportMode_Color.Text.Equals("1"))
					{
						e.Item.Cells[7].BackColor = colour;
					}
				}

				if(lblCustomerID_Color != null)
				{
					if(lblCustomerID_Color.Text.Equals("1"))
					{
						e.Item.Cells[8].BackColor = colour;
					}
				}

				if(lblExporterName_Color != null)
				{
					if(lblExporterName_Color.Text.Equals("1"))
					{
						e.Item.Cells[9].BackColor = colour;
					}
				}

				if(lblExporterAddress1_Color != null)
				{
					if(lblExporterAddress1_Color.Text.Equals("1"))
					{
						e.Item.Cells[10].BackColor = colour;
					}
				}

				if(lblExporterAddress2_Color != null)
				{
					if(lblExporterAddress2_Color.Text.Equals("1"))
					{
						e.Item.Cells[11].BackColor = colour;
					}
				}

				if(lblExporterCountry_Color != null)
				{
					if(lblExporterCountry_Color.Text.Equals("1"))
					{
						e.Item.Cells[12].BackColor = colour;
					}
				}

				if(lblGoodsDescription_Color != null)
				{
					if(lblGoodsDescription_Color.Text.Equals("1"))
					{
						e.Item.Cells[13].BackColor = colour;
					}
				}

				if(lblPkgQty_Color != null)
				{
					if(lblPkgQty_Color.Text.Equals("1"))
					{
						e.Item.Cells[14].BackColor = colour;
					}
				}

				if(lblWeight_Color != null)
				{
					if(lblWeight_Color.Text.Equals("1"))
					{
						e.Item.Cells[15].BackColor = colour;
					}
				}

				DataRowView dr = (DataRowView)e.Item.DataItem;
				if(dr["ArrivalDate"] != DBNull.Value && dr["ArrivalDate"].ToString() != "" && lblArrivalDate != null)
				{
					try
					{
						lblArrivalDate.Text = DateTime.ParseExact(dr["ArrivalDate"].ToString(),"yyyy-MM-dd",null).ToString("dd/MM/yyyy");
					}
					catch
					{
						lblArrivalDate.Text = dr["ArrivalDate"].ToString();
					}
					
				}
			}
		}
	}
}
