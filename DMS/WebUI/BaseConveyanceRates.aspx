<%@ Page language="c#" Codebehind="BaseConveyanceRates.aspx.cs" AutoEventWireup="false" Inherits="com.ties.BaseConveyanceRates" smartNavigation="false" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>BaseConveyanceRates</title>
		<link href="css/styles.css" rel="stylesheet" rev="stylesheet">
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="BaseConveyanceRates" method="post" runat="server">
			<asp:Label id="lblTitle" style="Z-INDEX: 101; LEFT: 20px; POSITION: absolute; TOP: 6px" runat="server" Width="330px" Height="32px" CssClass="mainTitleSize">Base Rates - By Conveyance</asp:Label>
			<asp:Label id="lblErrorMessage" style="Z-INDEX: 107; LEFT: 24px; POSITION: absolute; TOP: 71px" runat="server" Width="656px" CssClass="errorMsgColor">Error Message</asp:Label>
			<asp:ValidationSummary id="Pagevalid" style="Z-INDEX: 106; LEFT: 359px; POSITION: absolute; TOP: 4px" runat="server" ShowMessageBox="True" ShowSummary="False" HeaderText="Please enter the missing  fields."></asp:ValidationSummary>
			<asp:Button id="btnInsert" style="Z-INDEX: 104; LEFT: 211px; POSITION: absolute; TOP: 41px" runat="server" Text="Insert" CausesValidation="False" CssClass="queryButton"></asp:Button>
			<asp:Button id="btnExecuteQuery" style="Z-INDEX: 103; LEFT: 81px; POSITION: absolute; TOP: 41px" runat="server" Text="Execute Query" CausesValidation="False" Width="130px" CssClass="queryButton"></asp:Button>
			<asp:Button id="btnQuery" style="Z-INDEX: 102; LEFT: 22px; POSITION: absolute; TOP: 41px" runat="server" Text="Query" CausesValidation="False" Width="59px" CssClass="queryButton"></asp:Button>
			<asp:DataGrid id="dgBaseConveyance" runat="server" AllowCustomPaging="True" AllowPaging="True" AutoGenerateColumns="False" style="Z-INDEX: 105; LEFT: 21px; POSITION: absolute; TOP: 98px" HorizontalAlign="Left" Width="700px" OnEditCommand="dgBaseConvey_Edit" OnCancelCommand="dgBaseConvey_Cancel" OnItemDataBound="dgBaseConvey_ItemDataBound" OnDeleteCommand="dgBaseConvey_Delete" OnUpdateCommand="dgBaseConvey_Update" OnPageIndexChanged="dgBaseConvey_PageChange" OnItemCommand="dgBaseConvey_Button">
				<ItemStyle HorizontalAlign="Left" CssClass="gridfield"></ItemStyle>
				<Columns>
					<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;" CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:EditCommandColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton" CommandName="Delete">
						<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle HorizontalAlign="Left"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Origin Zone Code" HeaderStyle-Font-Bold="true">
						<HeaderStyle CssClass="gridHeading" Width="20%"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblOZCode" Text='<%#DataBinder.Eval(Container.DataItem,"origin_zone_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtOZCode" Text='<%#DataBinder.Eval(Container.DataItem,"origin_zone_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" ReadOnly="True">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="ozcValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtOZCode" ErrorMessage="Origin Zone Code "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton" CommandName="OriginSearch" Visible="true">
						<HeaderStyle CssClass="gridHeading" Width="5%"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Destination Zone Code" HeaderStyle-Font-Bold="true">
						<HeaderStyle CssClass="gridHeading" Width="20%"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label CssClass="gridLabel" ID="lblDZCode" Text='<%#DataBinder.Eval(Container.DataItem,"destination_zone_code")%>' Runat="server">
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtDZCode" Text='<%#DataBinder.Eval(Container.DataItem,"destination_zone_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" ReadOnly="True">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="odcValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtDZCode" ErrorMessage="Destination Zone Code "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton" CommandName="DestinationSearch" Visible="true">
						<HeaderStyle CssClass="gridHeading" Width="5%"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Conveyance Code" HeaderStyle-Font-Bold="true">
						<HeaderStyle HorizontalAlign="Left" Width="20%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblConveyance" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"conveyance_code")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtConveyance" Text='<%#DataBinder.Eval(Container.DataItem,"conveyance_code")%>' Runat="server" Enabled="True" TextMaskType="msUpperAlfaNumeric" MaxLength="12" ReadOnly="True">
							</cc1:mstextbox>
							<asp:RequiredFieldValidator ID="cConveyance" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtConveyance" ErrorMessage="Conveyance Code "></asp:RequiredFieldValidator>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton" CommandName="ConveyanceSearch" Visible="true">
						<HeaderStyle CssClass="gridHeading" Width="5%"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
					</asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Start Price">
						<HeaderStyle HorizontalAlign="Left" Width="15%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="lblPrice" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Start_Price","{0:n}")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtStartPrice" Text='<%#DataBinder.Eval(Container.DataItem,"Start_Price","{0:n}")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" >
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Increment Price">
						<HeaderStyle HorizontalAlign="Left" Width="15%" CssClass="gridHeading"></HeaderStyle>
						<ItemStyle CssClass="gridField"></ItemStyle>
						<ItemTemplate>
							<asp:Label ID="Label1" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Increment_Price","{0:n}")%>'>
							</asp:Label>
						</ItemTemplate>
						<EditItemTemplate>
							<cc1:mstextbox CssClass="gridTextBox" ID="txtIncrementPrice" Text='<%#DataBinder.Eval(Container.DataItem,"Increment_Price","{0:n}")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="-999999" NumberPrecision="8" NumberScale="2" TextMaskString="#.00" >
							</cc1:mstextbox>
						</EditItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle NextPageText="Next" PrevPageText="Previous" CssClass="normalText" Position="Bottom" HorizontalAlign="Left"></PagerStyle>
			</asp:DataGrid>
			<input type=hidden value="<%=strScrollPosition%>" name=ScrollPosition>
		</form>
	</body>
</HTML>
