using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using System.Text.RegularExpressions;
using TIESDAL;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CustomsChangeCustomerIDforJob.
	/// </summary>
	public class CustomsChangeCustomerIDforJob : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.Button btnChangeCustomerID;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.TextBox txtCustomsJobEntryNumber;
		protected System.Web.UI.WebControls.TextBox txtHouseAWBNumber;
		protected System.Web.UI.WebControls.TextBox txtCurrentCustomerID;
		protected System.Web.UI.WebControls.TextBox txtNewCustomerID;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.WebControls.Label lblProm;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divProm;
		protected System.Web.UI.HtmlControls.HtmlTable Table4;

		Utility Utility = null;
		String strEnterpriseID = null;
		String strAppID = null;
		protected System.Web.UI.WebControls.Button btnPromYes;
		protected System.Web.UI.WebControls.Button btnPromNo;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.TextBox txtNewHouseAWBNumber;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.RadioButton rbCustomerID;
		protected System.Web.UI.WebControls.RadioButton rbHouseAWBNumber;
		protected System.Web.UI.WebControls.Label lblNewCustomerID;
		protected System.Web.UI.WebControls.Label lblNewHouseAWBNumber;
		protected System.Web.UI.WebControls.Label lblNewCustomerIDRed;
		protected System.Web.UI.WebControls.Label lblNewHouseAWBNumberRed;
		String strUserLoggin = null; 
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			Utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = Utility.GetAppID();
			strEnterpriseID = Utility.GetEnterpriseID();
			strUserLoggin = Utility.GetUserID();

			if(this.IsPostBack==false)
			{
				SetInitialFocus(txtCustomsJobEntryNumber);				
			}
			
			VisibleControls();
			this.btnChangeCustomerID.Attributes.Add("onclick","javascript:document.getElementById('"+ this.btnChangeCustomerID.ClientID + "').disabled=true;" + GetPostBackEventReference(this.btnChangeCustomerID));
			
		}
		
		private void VisibleControls()
		{
			// Visible or invisible the textbox based on radio selection.
			if (rbCustomerID.Checked == true)
			{
				lblNewCustomerID.Visible = true;
				lblNewCustomerIDRed.Visible = true;
				txtNewCustomerID.Visible = true;

				lblNewHouseAWBNumber.Visible = false;
				lblNewHouseAWBNumberRed.Visible = false;
				txtNewHouseAWBNumber.Visible = false;
			}
			else if (rbHouseAWBNumber.Checked == true)
			{
				lblNewCustomerID.Visible = false;
				lblNewCustomerIDRed.Visible = false;
				txtNewCustomerID.Visible = false;

				lblNewHouseAWBNumber.Visible = true;
				lblNewHouseAWBNumberRed.Visible = true;
				txtNewHouseAWBNumber.Visible = true;
			}
		}
		
		private void ClearControls()
		{
			txtCustomsJobEntryNumber.Text = null;
			txtHouseAWBNumber.Text = null;
			txtCurrentCustomerID.Text = null;
			txtNewCustomerID.Text = null;
			txtNewHouseAWBNumber.Text = null;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnChangeCustomerID.Click += new System.EventHandler(this.btnChangeCustomerID_Click);
			this.rbCustomerID.CheckedChanged += new System.EventHandler(this.rbCustomerID_CheckedChanged);
			this.rbHouseAWBNumber.CheckedChanged += new System.EventHandler(this.rbHouseAWBNumber_CheckedChanged);
			this.btnPromYes.Click += new System.EventHandler(this.btnPromYes_Click);
			this.btnPromNo.Click += new System.EventHandler(this.btnPromNo_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnChangeCustomerID_Click(object sender, System.EventArgs e)
		{
			btnChangeCustomerID.Enabled=true;

			if(txtHouseAWBNumber.Text.Equals(String.Empty))
			{
				lblError.Text = "House AWB Number is required";
				SetInitialFocus(txtHouseAWBNumber);
				return;
			}

			if(!GetConsignmentRegExFromEnterpriseConfiguration(txtHouseAWBNumber.Text))
			{
				lblError.Text = "House AWB Number is invalid";
				SetInitialFocus(txtHouseAWBNumber);
				return;
			}

			if(txtCurrentCustomerID.Text.Equals(String.Empty))
			{
				lblError.Text = "Current Customer ID is required";
				SetInitialFocus(txtCurrentCustomerID);
				return;
			}

			if((txtNewCustomerID.Text.Equals(String.Empty)) && (rbCustomerID.Checked==true))
			{
				lblError.Text = "New Customer ID is required";
				SetInitialFocus(txtNewCustomerID);
				return;
			}

			if((txtNewHouseAWBNumber.Text.Equals(String.Empty)) && (rbHouseAWBNumber.Checked==true))
			{
				lblError.Text = "New House AWB Number is required";
				SetInitialFocus(txtNewHouseAWBNumber);
				return;
			}

			if(txtCurrentCustomerID.Text.Equals(txtNewCustomerID.Text))
			{
				lblError.Text = "Old customer ID must be different than new customer ID";
				SetInitialFocus(txtNewCustomerID);
				return;
			}
			
			DataSet dsResult = null;
			if (rbCustomerID.Checked==true)
				dsResult = CustomsChangeCustomerIDforJobDAL.GetCustomsChangeCustomerID(strAppID
					, strEnterpriseID
					, strUserLoggin
					, txtCustomsJobEntryNumber.Text
					, txtHouseAWBNumber.Text
					, txtCurrentCustomerID.Text
					, txtNewCustomerID.Text
					, "");
			else if (rbHouseAWBNumber.Checked==true)
				dsResult = CustomsChangeCustomerIDforJobDAL.GetCustomsChangeCustomerID(strAppID
					, strEnterpriseID
					, strUserLoggin
					, txtCustomsJobEntryNumber.Text
					, txtHouseAWBNumber.Text
					, txtCurrentCustomerID.Text
					, ""
					, txtNewHouseAWBNumber.Text);

			DataTable dtResult = dsResult.Tables[0];

			if(dtResult.Rows.Count > 0)
			{
				string errormessage = dtResult.Rows[0]["ErrorMessage"].ToString();
				string errorcode = dtResult.Rows[0]["ErrorCode"].ToString();

				lblError.Text = errormessage;

				if(errorcode.Equals("0"))
				{
					ClearControls();
					SetInitialFocus(txtCustomsJobEntryNumber);
				}
				else if(errorcode.Equals("6"))
				{
					divMain.Visible = false;
					lblProm.Text = errormessage;
					divProm.Visible = true;
				}
			}
			
		}

		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				System.Text.StringBuilder s = new System.Text.StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}

		private bool GetConsignmentRegExFromEnterpriseConfiguration(string strHouseAWBNumber)
		{
			DataSet dsConsignmentRegEx = CustomsChangeCustomerIDforJobDAL.GetConsignmentRegEx(strAppID, strEnterpriseID);
			DataTable drConsignmentRegEx = dsConsignmentRegEx.Tables[0];
			string consignmentRegExKey = drConsignmentRegEx.Rows[0]["value"].ToString();
			if(Regex.IsMatch(strHouseAWBNumber, consignmentRegExKey))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		private void btnPromYes_Click(object sender, System.EventArgs e)
		{
			if(txtCustomsJobEntryNumber.Text.Equals(String.Empty))
			{
				lblError.Text = "Customs Job Entry Number is required";
				SetInitialFocus(txtCustomsJobEntryNumber);
				divMain.Visible = true;
				divProm.Visible = false;
				return;
			}

			if(txtHouseAWBNumber.Text.Equals(String.Empty))
			{
				lblError.Text = "House AWB Number is required";
				SetInitialFocus(txtHouseAWBNumber);
				divMain.Visible = true;
				divProm.Visible = false;
				return;
			}

			if(!GetConsignmentRegExFromEnterpriseConfiguration(txtHouseAWBNumber.Text))
			{
				lblError.Text = "Hounse AWB Number is invalid";
				SetInitialFocus(txtHouseAWBNumber);
				divMain.Visible = true;
				divProm.Visible = false;
				return;
			}

			if(txtCurrentCustomerID.Text.Equals(String.Empty))
			{
				lblError.Text = "Current Customer ID is required";
				SetInitialFocus(txtCurrentCustomerID);
				divMain.Visible = true;
				divProm.Visible = false;
				return;
			}

			if(txtNewCustomerID.Text.Equals(String.Empty))
			{
				lblError.Text = "New Customer ID is required";
				SetInitialFocus(txtNewCustomerID);
				divMain.Visible = true;
				divProm.Visible = false;
				return;
			}

			if(txtCurrentCustomerID.Text.Equals(txtNewCustomerID.Text))
			{
				lblError.Text = "Old customer ID must be different than new customer ID";
				SetInitialFocus(txtNewCustomerID);
				divMain.Visible = true;
				divProm.Visible = false;
				return;
			}

			DataSet dsResult = CustomsChangeCustomerIDforJobDAL.GetCustomsChangeCustomerID(strAppID
				, strEnterpriseID
				, strUserLoggin
				, txtCustomsJobEntryNumber.Text
				, txtHouseAWBNumber.Text
				, txtCurrentCustomerID.Text
				, txtNewCustomerID.Text
				, ""
				, "1");
			
			DataTable dtResult = dsResult.Tables[0];

			if(dtResult.Rows.Count > 0)
			{
				string errormessage = dtResult.Rows[0]["ErrorMessage"].ToString();
				string errorcode = dtResult.Rows[0]["ErrorCode"].ToString();

				lblError.Text = errormessage;

				if(errorcode.Equals("0"))
				{
					txtCustomsJobEntryNumber.Text = "";
					txtHouseAWBNumber.Text = "";
					txtCurrentCustomerID.Text = "";
					txtNewCustomerID.Text = "";
					SetInitialFocus(txtCustomsJobEntryNumber);
				}
			}

			divMain.Visible = true;
			divProm.Visible = false;
			SetInitialFocus(txtCustomsJobEntryNumber);
		}

		private void btnPromNo_Click(object sender, System.EventArgs e)
		{
			if(txtCustomsJobEntryNumber.Text.Equals(String.Empty))
			{
				lblError.Text = "Customs Job Entry Number is required";
				SetInitialFocus(txtCustomsJobEntryNumber);
				divMain.Visible = true;
				divProm.Visible = false;
				return;
			}

			if(txtHouseAWBNumber.Text.Equals(String.Empty))
			{
				lblError.Text = "House AWB Number is required";
				SetInitialFocus(txtHouseAWBNumber);
				divMain.Visible = true;
				divProm.Visible = false;
				return;
			}

			if(!GetConsignmentRegExFromEnterpriseConfiguration(txtHouseAWBNumber.Text))
			{
				lblError.Text = "House AWB Number is invalid";
				SetInitialFocus(txtHouseAWBNumber);
				divMain.Visible = true;
				divProm.Visible = false;
				return;
			}

			if(txtCurrentCustomerID.Text.Equals(String.Empty))
			{
				lblError.Text = "Current Customer ID is required";
				SetInitialFocus(txtCurrentCustomerID);
				divMain.Visible = true;
				divProm.Visible = false;
				return;
			}

			if(txtNewCustomerID.Text.Equals(String.Empty))
			{
				lblError.Text = "New Customer ID is required";
				SetInitialFocus(txtNewCustomerID);
				divMain.Visible = true;
				divProm.Visible = false;
				return;
			}

			if(txtCurrentCustomerID.Text.Equals(txtNewCustomerID.Text))
			{
				lblError.Text = "Old customer ID must be different than new customer ID";
				SetInitialFocus(txtNewCustomerID);
				divMain.Visible = true;
				divProm.Visible = false;
				return;
			}

			DataSet dsResult = CustomsChangeCustomerIDforJobDAL.GetCustomsChangeCustomerID(strAppID, strEnterpriseID, strUserLoggin,
				txtCustomsJobEntryNumber.Text, txtHouseAWBNumber.Text, txtCurrentCustomerID.Text, txtNewCustomerID.Text,"", "0");
			
			DataTable dtResult = dsResult.Tables[0];

			if(dtResult.Rows.Count > 0)
			{
				string errormessage = dtResult.Rows[0]["ErrorMessage"].ToString();
				string errorcode = dtResult.Rows[0]["ErrorCode"].ToString();

				lblError.Text = errormessage;

				if(errorcode.Equals("0"))
				{
					txtCustomsJobEntryNumber.Text = "";
					txtHouseAWBNumber.Text = "";
					txtCurrentCustomerID.Text = "";
					txtNewCustomerID.Text = "";
					SetInitialFocus(txtCustomsJobEntryNumber);
				}
			}

			divMain.Visible = true;
			divProm.Visible = false;
			SetInitialFocus(txtCustomsJobEntryNumber);
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			lblError.Text = "";
			divMain.Visible = true;
			divProm.Visible = false;
			SetInitialFocus(txtCustomsJobEntryNumber);
		}

		private void rbCustomerID_CheckedChanged(object sender, System.EventArgs e)
		{
			lblError.Text = null;
			ClearControls();
			VisibleControls();
			SetInitialFocus(txtCustomsJobEntryNumber);
		}

		private void rbHouseAWBNumber_CheckedChanged(object sender, System.EventArgs e)
		{
			lblError.Text = null;
			ClearControls();
			VisibleControls();
			SetInitialFocus(txtCustomsJobEntryNumber);
		}
	}
}
