using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using GenCode128;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using com.ties.DAL;
using com.common.applicationpages;
using Cambro.Web.DbCombo;
using System.Data.SqlClient;

namespace TIES
{
	/// <summary>
	/// Summary description for PrintBarcode.
	/// </summary>
	public class PrintBarcode : BasePage
	{
		string m_strAppID=null;
		string m_strEnterpriseID=null;
		string m_strCulture=null;

		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.Label lblYear;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentTracking;
		protected System.Web.UI.WebControls.Label Label2;
		protected com.common.util.msTextBox txtAmount;
		protected com.common.util.msTextBox txtConsignmentNo;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.HtmlControls.HtmlTable tblDates;
		DataTable dt = new DataTable();
		DataTable dtResult = new DataTable();
		DataTable dtResult1 = new DataTable();

		DataTable dt1 = new DataTable();
		DataTable dt2 = new DataTable();
		DataTable dt3 = new DataTable();
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();

			//			DbComboPathCode.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			//			DbComboOriginDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			//			DbComboDestinationDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];

			if(!Page.IsPostBack)
			{
				DefaultScreen();
				Session["toRefresh"]=false;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}

		private void DefaultScreen()
		{
			#region "Consignment"
			txtConsignmentNo.Text = null;
			txtConsignmentNo.Enabled = true;
			txtAmount.Text = null;
			txtAmount.Enabled = true;
			lblErrorMessage.Text = "";

			#endregion
		}
		
		private int findInt(string checkInt)
		{
			int pos = 1;
			for(int i = 0; i <checkInt.Length; i++)
			{
				string checkInt1 = checkInt.Substring(i,1);
				if(char.IsControl(char.Parse(checkInt1)))
				{
					pos = i;
					break;
				}
			}
			return pos;
		}
		public void GenerateConsignmentBarCode()
		{
			ClearBarCodeTable();
			int Pos = findInt(txtConsignmentNo.Text);
			string ValueHead = txtConsignmentNo.Text.Substring(0,Pos);
			string ValueTail = txtConsignmentNo.Text.Substring(Pos);
			int intVal = int.Parse(ValueTail);
			for(int i = 0; i < int.Parse(txtAmount.Text);i++)
			{
				string ValueResult = string.Empty;
				if(i == 0)
					ValueResult = txtConsignmentNo.Text;
				else
				{
					intVal += 1;
					int LastValue = intVal.ToString().Length;
					int ValuePos = ValueTail.Length;
					int EndPos = ValuePos - LastValue;
					ValueResult = ValueTail.Trim().Substring(0,EndPos);
					ValueResult += intVal.ToString();
					ValueResult = ValueHead + ValueResult;
				}
				txtConsignmentNo.Text = ValueResult;
				System.Drawing.Image myimg = Code128Rendering.MakeBarcodeImage(txtConsignmentNo.Text.Trim(), int.Parse("2"), true);
				byte[] content = ReadBitmap2ByteArray(myimg);
				StoreBlob2DataBase(content,txtConsignmentNo.Text.Trim(),i);
			}

		}

		protected static void ClearBarCodeTable()
		{
			
			Utility objUtility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,null);
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(objUtility.GetAppID() ,objUtility.GetEnterpriseID());
			IDbConnection conApp = dbCon.GetConnection();
			SqlConnection strcon = new SqlConnection(conApp.ConnectionString);
			strcon.Open();
			
			SqlCommand ssql =new SqlCommand("Delete from BarCode",strcon);
			ssql.ExecuteNonQuery();

			strcon.Close();
		}

		protected static byte[] ReadBitmap2ByteArray(System.Drawing.Image fileName)
		{
			using(Bitmap image = new Bitmap(fileName))
			{
				System.IO.MemoryStream stream = new System.IO.MemoryStream();
				image.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);
				return stream.ToArray();
			}
		
		}

		protected static void StoreBlob2DataBase(byte[] content,string txt,int Amount)
		{
			Utility objUtility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,null);
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(objUtility.GetAppID() ,objUtility.GetEnterpriseID());
			IDbConnection conApp = dbCon.GetConnection();
			SqlConnection strcon = new SqlConnection(conApp.ConnectionString);
			strcon.Open();
			
			try
			{
				int Remain = Amount % 3;
				int Row = (Amount / 3) + 1;
				if(Remain == 0)
				{
					// insert new entry into table
					SqlCommand ssql =new SqlCommand("Insert into BarCode (BarCode1,BarCode1Text,Row) VALUES(@image,'" + txt + "'," + Row + ")",strcon);
					SqlParameter imageParameter =  ssql.Parameters.Add("@image", SqlDbType.Binary);
					imageParameter.Value = content;
					imageParameter.Size  = content.Length;
					ssql.ExecuteNonQuery();
				}
				else if(Remain == 1)
				{
					// insert new entry into table
					SqlCommand ssql =new SqlCommand("Update BarCode Set BarCode2=@image,BarCode2Text='" + txt + "' Where Row='" + Row + "'" ,strcon);
					SqlParameter imageParameter =  ssql.Parameters.Add("@image", SqlDbType.Binary);
					imageParameter.Value = content;
					imageParameter.Size  = content.Length;
					ssql.ExecuteNonQuery();
				}
				else if(Remain == 2)
				{
					// insert new entry into table
					SqlCommand ssql =new SqlCommand("Update BarCode Set BarCode3=@image,BarCode3Text='" + txt + "' Where Row='" + Row + "'" ,strcon);
					SqlParameter imageParameter =  ssql.Parameters.Add("@image", SqlDbType.Binary);
					imageParameter.Value = content;
					imageParameter.Size  = content.Length;
					ssql.ExecuteNonQuery();
				}
				//				dbExcute1 = dbCon.CreateCommand(ssql,CommandType.Text);
				//				dbCon.ExecuteNonQuery(dbExcute1);
			}
			finally
			{
				strcon.Close();
			}
		}

		

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			bool icheck =  ValidateValues();
			if(icheck == false)
			{
				GenerateConsignmentBarCode();
				String strUrl = null;
				strUrl = "ReportViewer.aspx";
				Session["FORMID"] = "PrintBarcode";
				ArrayList paramList = new ArrayList();
				paramList.Add(strUrl);
				String sScript = Utility.GetScript("openParentWindow.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			}
		}
		private bool ValidateValues()
		{
			bool iCheck=false;
	
			if(txtAmount.Text=="" && txtConsignmentNo.Text=="")
			{					
				lblErrorMessage.Text="Please Enter Consignment No. and Amount";	
				return iCheck=true;				
			}
			else if(txtAmount.Text=="" && txtConsignmentNo.Text!="")
			{					
				lblErrorMessage.Text="Please Enter Amount";	
				return iCheck=true;				
			}
			else if(txtAmount.Text!="" && txtConsignmentNo.Text=="")
			{					
				lblErrorMessage.Text="Please Enter Consignment No.";	
				return iCheck=true;				
			}

			return iCheck;
			
		}
		public static byte[] GetPhoto2()
		{
			FileStream fs = new FileStream(@"\images\Bar.tiff",FileMode.Open,FileAccess.Read);
			BinaryReader br = new BinaryReader(fs);

			byte[] photo = br.ReadBytes((int)fs.Length);

			br.Close();
			fs.Close();

			return photo;
		}
		public void PrintReport()
		{
			string[] str = new string[6];
			int k = 0;

			dt1.Rows.Clear();
			dt2.Rows.Clear();
			dt3.Rows.Clear();
			dtResult1.Clear();

			for (int i = 0; i < dtResult.Rows.Count; i++)
			{                
				k += 1;
				DataRow dr;
				if (k % 3 == 1)
				{
					dr = dt1.NewRow();
					dr["Consignment_no"] = dtResult.Rows[i]["Consignment_no"].ToString();
					dr["Img"] = dtResult.Rows[i]["ImageBar"];
					dt1.Rows.Add(dr);
					dt1.AcceptChanges();
				}
				if (k % 3 == 2)
				{
					dr = dt2.NewRow();
					dr["Consignment_no"] = dtResult.Rows[i]["Consignment_no"].ToString();
					dr["Img"] = dtResult.Rows[i]["ImageBar"];
					dt2.Rows.Add(dr);
					dt2.AcceptChanges();
				}
				if (k % 3 == 0)
				{
					dr = dt3.NewRow();
					dr["Consignment_no"] = dtResult.Rows[i]["Consignment_no"].ToString();
					dr["Img"] = dtResult.Rows[i]["ImageBar"];
					dt3.Rows.Add(dr);
					dt3.AcceptChanges();
				}
			}
			DataRow dr1;
			int n = dt1.Rows.Count;
			int nn = dt2.Rows.Count;
			int nnn = dt3.Rows.Count;
			if (nn < n)
			{
				dr1 = dt2.NewRow();
				dr1["Consignment_no"] = "";
				dr1["Img"] = null;
				dt2.Rows.Add(dr1);
				dt2.AcceptChanges();
			}
			if (nnn < n)
			{
				dr1 = dt3.NewRow();
				dr1["Consignment_no"] = "";
				dr1["Img"] =null; 
				dt3.Rows.Add(dr1);
				dt3.AcceptChanges();
			}

            

			DataRow dr2;
			for (int l = 0; l < dt1.Rows.Count; l++)
			{
				dr2 = dtResult1.NewRow();
				dr2["Consignment_no1"] = dt1.Rows[l]["Consignment_no"];
				dr2["Img1"] = dt1.Rows[l]["Img"];
				dr2["Consignment_no2"] = dt2.Rows[l]["Consignment_no"];
				dr2["Img2"] = dt2.Rows[l]["Img"];
				dr2["Consignment_no3"] = dt3.Rows[l]["Consignment_no"];
				dr2["Img3"] = dt3.Rows[l]["Img"];

				dtResult1.Rows.Add(dr2);
				dtResult1.AcceptChanges();
			}
		}
	}
}
