<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Page language="c#" Codebehind="ShipmentPendingPODReport.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.ShipmentPendingPODReport"  EnableEventValidation="False" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ShipmentPendingPODReport</title>
		<meta content="False" name="vs_showGrid">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
				<script language="javascript">
			function makeUppercase(objId)
			{
				document.getElementById(objId).value = document.getElementById(objId).value.toUpperCase();
			}
		</script>
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="ShipmentPendingPODReport" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; POSITION: absolute; TOP: 39px; LEFT: 21px" runat="server"
				CssClass="queryButton" Text="Query" Width="64px"></asp:button>
			<asp:button id="btnGenerateReport" style="Z-INDEX: 107; POSITION: absolute; TOP: 39px; LEFT: 173px"
				runat="server" Width="157px" Text="Generate (Optimized)" CssClass="queryButton" Visible="False"></asp:button>
			<TABLE id="tblExternal" style="Z-INDEX: 106; POSITION: absolute; WIDTH: 797px; HEIGHT: 504px; TOP: 93px; LEFT: 15px"
				border="0" runat="server">
				<TR>
					<TD style="WIDTH: 400px; HEIGHT: 137px" vAlign="top">
						<FIELDSET style="WIDTH: 390px; HEIGHT: 129px"><LEGEND><asp:label id="Label17" runat="server" CssClass="tableHeadingFieldset" Width="173px" Font-Bold="True">Estimated (Delivery) Date</asp:label></LEGEND>
							<TABLE id="Table3" style="WIDTH: 360px; HEIGHT: 91px" cellSpacing="0" cellPadding="0" align="left"
								border="0" runat="server">
								<TR>
									<TD></TD>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonthCust" runat="server" CssClass="tableRadioButton" Text="Month" Width="73px"
											AutoPostBack="True" GroupName="EnterDate" Checked="True" Height="21px"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonthCust" runat="server" CssClass="textField" Width="88px" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="Label16" runat="server" CssClass="tableLabel" Width="30px" Height="17px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYearCust" runat="server" CssClass="textField" Width="83" MaxLength="5" TextMaskType="msNumeric"
											NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox>
									</TD>
									<TD></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 20px"></TD>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriodCust" runat="server" CssClass="tableRadioButton" Text="Period" Width="62px"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriodCust" runat="server" CssClass="textField" Width="82" MaxLength="10"
											TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtToCust" runat="server" CssClass="textField" Width="83" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD style="HEIGHT: 20px"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDateCust" runat="server" CssClass="tableRadioButton" Text="Date" Width="74px"
											AutoPostBack="True" GroupName="EnterDate" Height="11px"></asp:radiobutton>&nbsp;
										<cc1:mstextbox id="txtDateCust" runat="server" CssClass="textField" Width="80px" MaxLength="10"
											TextMaskType="msDate" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
					<TD style="HEIGHT: 137px" vAlign="top">&nbsp;
						<FIELDSET style="WIDTH: 320px; HEIGHT: 89px"><LEGEND><asp:label id="Label8" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Destination</asp:label></LEGEND>
							<TABLE id="Table6" style="WIDTH: 300px; HEIGHT: 89px" cellSpacing="0" cellPadding="0" align="left"
								border="0" runat="server">
								<TR height="37">
									<TD></TD>
									<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
										<asp:label id="Label7" runat="server" CssClass="tableLabel" Width="84px" Height="18px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCodeCust" runat="server" CssClass="textField" Width="139px" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithHyphen"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCodeCust" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<TD></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
										<asp:label id="Label6" runat="server" CssClass="tableLabel" Width="100px" Height="16px"> Province</asp:label><cc1:mstextbox id="txtStateCodeCust" runat="server" CssClass="textField" Width="139" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCodeCust" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</FIELDSET>
					</TD>
				</TR>
			</TABLE>
			<asp:label id="lblErrorMessage" style="Z-INDEX: 105; POSITION: absolute; TOP: 64px; LEFT: 23px"
				runat="server" CssClass="errorMsgColor" Width="589px" Height="27px">Place holder for err msg</asp:label>&nbsp;
			<asp:button id="btnGenerate" style="Z-INDEX: 102; POSITION: absolute; TOP: 39px; LEFT: 86px"
				runat="server" CssClass="queryButton" Text="Generate" Width="82px"></asp:button><asp:label id="lblTitle" style="Z-INDEX: 103; POSITION: absolute; TOP: 3px; LEFT: 20px" runat="server"
				CssClass="maintitleSize" Width="514px" Height="35px">Shipment Pending POD Report</asp:label>
			<TABLE id="tblInternal" style="Z-INDEX: 104; POSITION: absolute; WIDTH: 800px; TOP: 92px; LEFT: 15px"
				border="0" runat="server">
				<TR>
					<TD style="WIDTH: 400px; HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 390px; HEIGHT: 129px"><legend><asp:label id="lblDates" runat="server" CssClass="tableHeadingFieldset" Width="173px" Font-Bold="True">Estimated (Delivery) Date</asp:label></legend>
							<TABLE id="Table1" style="WIDTH: 360px; HEIGHT: 91px" cellSpacing="0" cellPadding="0" align="left"
								border="0" runat="server">
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbMonth" runat="server" CssClass="tableRadioButton" Text="Month" Width="73px"
											AutoPostBack="True" GroupName="EnterDate" Checked="True" Height="21px"></asp:radiobutton>&nbsp;
										<asp:dropdownlist id="ddMonth" runat="server" CssClass="textField" Width="88px" Height="19px"></asp:dropdownlist>&nbsp;
										<asp:label id="lblYear" runat="server" CssClass="tableLabel" Width="30px" Height="22px">Year</asp:label>&nbsp;
										<cc1:mstextbox id="txtYear" runat="server" CssClass="textField" Width="83" MaxLength="5" TextMaskType="msNumeric"
											NumberMaxValue="2999" NumberMinValue="1" NumberPrecision="4"></cc1:mstextbox></TD>
									<td></td>
								</TR>
								<TR>
									<td style="HEIGHT: 20px"></td>
									<TD style="HEIGHT: 20px" colSpan="3">&nbsp;
										<asp:radiobutton id="rbPeriod" runat="server" CssClass="tableRadioButton" Text="Period" Width="62px"
											AutoPostBack="True" GroupName="EnterDate"></asp:radiobutton>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtPeriod" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										&nbsp;
										<cc1:mstextbox id="txtTo" runat="server" CssClass="textField" Width="83" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td style="HEIGHT: 20px"></td>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="3">&nbsp;
										<asp:radiobutton id="rbDate" runat="server" CssClass="tableRadioButton" Text="Date" Width="74px"
											AutoPostBack="True" GroupName="EnterDate" Height="22px"></asp:radiobutton>&nbsp;&nbsp;<cc1:mstextbox id="txtDate" runat="server" CssClass="textField" Width="82" MaxLength="10" TextMaskType="msDate"
											TextMaskString="99/99/9999"></cc1:mstextbox></TD>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 137px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 105px"><legend><asp:label id="lblPayerType" runat="server" CssClass="tableHeadingFieldset" Width="79px" Font-Bold="True">Payer Type</asp:label></legend>
							<TABLE id="tblPayerType" style="WIDTH: 300px; HEIGHT: 73px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<tr>
									<td></td>
									<td colSpan="2" height="1">&nbsp;</td>
									<td></td>
									<td></td>
								</tr>
								<TR>
									<td></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="Label4" runat="server" CssClass="tableLabel" Width="79px" Height="22px">Payer Type</asp:label></TD>
									<td><asp:listbox id="lsbCustType" runat="server" Width="137px" Height="61px" SelectionMode="Multiple"></asp:listbox></td>
									<td></td>
								</TR>
								<TR height="33">
									<td bgColor="blue"></td>
									<TD class="tableLabel" vAlign="top" colSpan="2">&nbsp;
										<asp:label id="lblPayerCode" runat="server" CssClass="tableLabel" Width="79px" Height="22px">Payer Code</asp:label>&nbsp;&nbsp;</TD>
									<td><asp:textbox id="txtPayerCode" runat="server" CssClass="textField" Width="148px"></asp:textbox>&nbsp;&nbsp;
										<asp:button id="btnPayerCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></td>
									<td></td>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 400px; HEIGHT: 134px" vAlign="top">
						<fieldset style="WIDTH: 390px; HEIGHT: 120px"><legend><asp:label id="lblRouteType" runat="server" CssClass="tableHeadingFieldset" Width="145px" Font-Bold="True">Route / DC Selection</asp:label></legend>
							<TABLE id="tblRouteType" style="WIDTH: 360px; HEIGHT: 47px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<TR>
									<TD class="tableLabel" colSpan="2">&nbsp;
										<asp:radiobutton id="rbLongRoute" runat="server" CssClass="tableRadioButton" Text="Linehaul" Width="106px"
											AutoPostBack="True" GroupName="QueryByRoute" Checked="True" Height="22px"></asp:radiobutton><asp:radiobutton id="rbShortRoute" runat="server" CssClass="tableRadioButton" Text="Delivery/CL"
											Width="131px" AutoPostBack="True" GroupName="QueryByRoute" Height="22px"></asp:radiobutton><asp:radiobutton id="rbAirRoute" runat="server" CssClass="tableRadioButton" Text="Air Route" Width="120px"
											AutoPostBack="True" GroupName="QueryByRoute" Height="22px"></asp:radiobutton></TD>
								</TR>
								<tr>
									<TD class="tableLabel" width="20%">&nbsp;<asp:label id="lblRouteCode" runat="server" CssClass="tableLabel" Width="111px" Height="22px">Route Code</asp:label></TD>
									<td><DBCOMBO:DBCOMBO id="DbComboPathCode" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											ServerMethod="DbComboPathCodeSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v" RegistrationKey=" "
											TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></td>
								</tr>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label2" runat="server" CssClass="tableLabel" Width="154px" Height="22px">Origin Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboOriginDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											ServerMethod="DbComboDistributionCenterSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v"
											RegistrationKey=" " TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></TD>
								</TR>
								<TR>
									<TD class="tableLabel">&nbsp;<asp:label id="Label3" runat="server" CssClass="tableLabel" Width="181px" Height="22px">Destination Distribution Center</asp:label></TD>
									<TD><DBCOMBO:DBCOMBO id="DbComboDestinationDC" tabIndex="14" CssClass="tableLabel" Width="25px" AutoPostBack="True"
											ServerMethod="DbComboDistributionCenterSelect" ShowDbComboLink="False" TextUpLevelSearchButton="v"
											RegistrationKey=" " TextBoxColumns="18" Runat="server"></DBCOMBO:DBCOMBO></TD>
								</TR>
							</TABLE>
						</fieldset>
					</TD>
					<TD style="HEIGHT: 134px" vAlign="top">
						<fieldset style="WIDTH: 320px; HEIGHT: 89px"><legend><asp:label id="lblDestination" runat="server" CssClass="tableHeadingFieldset" Width="79px"
									Font-Bold="True">Destination</asp:label></legend>
							<TABLE id="tblDestination" style="WIDTH: 300px; HEIGHT: 89px" cellSpacing="0" cellPadding="0"
								align="left" border="0" runat="server">
								<tr height="37">
									<td></td>
									<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
										<asp:label id="lblZipCode" runat="server" CssClass="tableLabel" Width="84px" Height="22px">Postal Code</asp:label>&nbsp;&nbsp;&nbsp;
										<cc1:mstextbox id="txtZipCode" runat="server" CssClass="textField" Width="139px" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithHyphen"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnZipCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
								<tr>
									<td></td>
									<TD class="tableLabel" style="WIDTH: 360px" colSpan="3">&nbsp;
										<asp:label id="lblStateCode" runat="server" CssClass="tableLabel" Width="100px" Height="22px">State / Province</asp:label><cc1:mstextbox id="txtStateCode" runat="server" CssClass="textField" Width="139" Height="22" MaxLength="10"
											TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;&nbsp;
										<asp:button id="btnStateCode" runat="server" CssClass="searchButton" Text="..." CausesValidation="False"></asp:button></TD>
									<td></td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
				<TR>
					<TD vAlign="top" colSpan="2">
						<fieldset style="WIDTH: 608px; HEIGHT: 53px"><legend><asp:label id="Label1" Runat="server">Report Type</asp:label></legend>
							<TABLE id="tblDates" style="WIDTH: 608px; HEIGHT: 53px" width="608" align="left" border="0"
								runat="server">
								<TR height="27">
									<TD style="WIDTH: 162px" colSpan="9"><asp:radiobutton id="rbShipPendPOD" tabIndex="1" runat="server" CssClass="tableRadioButton" Text="Shipment Pending POD"
											AutoPostBack="True" GroupName="QueryByReportType" Checked="true"></asp:radiobutton></TD>
									<TD colSpan="11"><asp:radiobutton id="rbShipPendPODClosure" tabIndex="2" runat="server" CssClass="tableRadioButton"
											Text="Shipment Pending POD Closure" Width="219px" AutoPostBack="True" GroupName="QueryByReportType"></asp:radiobutton></TD>
								</TR>
								<tr height="5">
									<td colSpan="20"></td>
								</tr>
							</TABLE>
						</fieldset>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
