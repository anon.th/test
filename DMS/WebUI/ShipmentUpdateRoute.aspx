<%@ Import Namespace="System.Collections" %>
<%@ Page language="c#" Codebehind="ShipmentUpdateRoute.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ShipmentUpdateRoute" smartNavigation="false" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" 	Assembly="Cambro.Web.DbCombo" %>
<%@ Import Namespace="Cambro.Web.DbCombo" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>ShipmentUpdateRoute</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript">
var PathCode =-1;

function DbComboPathCodeOnSelect(Value, Text, SelectionType)
    {
        switch (SelectionType)
        {
            case 1: // The user is scrolling down the list 
                    // ClinetOnSelectFunction fires each 
                    // time a new item is selected.
            case 3: // The user has clicked on an item. The 
                    // results disappear, and the item is 
                    // selected.
            case 8: // The user has entered an exact match to 
                    // one of the data items. This item is 
                    // selected.
            case 9: // In a �re-query on postback� or an 
                    // �auto-query on load�, both the text and 
                    // value properties have matched with one 
                    // of the items. This item is selected.
                if (PathCode != Value)
                {
                    PathCode = Value;
                    UpdateProduct();
                }
                break;
            case 2: // The user has pressed enter when an item 
                    // is selected. The results disappear, and 
                    // the item is selected.
                break;
            case 4: // The user has entered more text into the 
                    // textbox. Another query will be run, and 
                    // the selection is cleared. Nothing is 
                    // selected.
            case 5: // The user has scrolled off the bottom or 
                    // top of the drop-down, and cleared the 
                    // current selection. Nothing is selected.
            case 6: // The user has pressed escape to clear 
                    // the DbCombo, and hide the results. 
                    // Nothing is selected.
            case 7: // DbCombo has evaluated the client state 
                    // function, and found it to have changed. 
                    // The current selection is cleared. 
                    // Nothing is selected.
                if (PathCode != -1)
                {
                    PathCode = -1;
                    UpdateProduct();
                }
                break;
            default:
                break;
        }
    }
    
function DbComboDlvryDtState()
{
    var state = new Object();
    state["PathCode"]=PathCode;
    return state;
}

function UpdateProduct()
{
    if (typeof(DbComboServerExists)!='undefined')
    // This checks to make sure the DbCombo javascript 
    // file exists - if we are in down-level mode, the 
    // next line would fail.
        DbComboStateChanged('&lt;%# DbComboDlvryDt.UniqueID %&gt;');
        // This informs a specific DbCombo that it's 
        // ClientState has changed. It will reevaluate 
        // it, and if it has changed, it will get new 
        // data from the server.
}
    

    
		</script>
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);"
		MS_POSITIONING="flowlayout">
		<form id="ShipmentUpdateRoute" method="post" runat="server">
			<asp:label id="TITLE" Width="602px" CssClass="mainTitleSize" Runat="server">Shipment Update (By Delivery Path)</asp:label>
			<TABLE id="ShipmentUpdRouteMainTable" style="Z-INDEX: 112; WIDTH: 724px" width="724" border="0"
				runat="server">
				<tr>
					<td>
						<TABLE id="tblShipmentUpdateRoute" width="721" align="left" border="0" runat="server">
							<TR height="27">
								<TD colSpan="7">&nbsp;</TD>
							</TR>
							<TR height="27">
								<TD width="10">&nbsp;
									<asp:label id="Label3" runat="server" Width="30px" Visible="False"></asp:label></TD>
								<TD><asp:button id="btnSave" tabIndex="2" runat="server" CssClass="queryButton" Text="Save" style="Z-INDEX: 0"></asp:button></TD>
								<TD colSpan="4">&nbsp;</TD>
								<TD width="10">&nbsp;
									<asp:label id="Label2" runat="server" Width="30px" Visible="False"></asp:label></TD>
							</TR>
							<TR height="27">
								<TD>&nbsp;</TD>
								<TD colspan="5"><asp:label id="lblErrorMsg" runat="server" Width="100%" CssClass="errorMsgColor" EnableViewState="False"></asp:label></TD>
								<TD>&nbsp;</TD>
							</TR>
							<TR height="27">
								<TD style="HEIGHT: 27px">&nbsp;</TD>
								<TD style="HEIGHT: 27px"><asp:label id="lblRouteRadioButton" tabIndex="51" runat="server" CssClass="tableLabel">Delivery Type</asp:label></TD>
								<TD style="HEIGHT: 27px" colSpan="4">&nbsp;</TD>
								<td style="HEIGHT: 27px">&nbsp;</td>
							</TR>
							<tr height="27">
								<TD>&nbsp;</TD>
								<td><asp:radiobutton id="rbtnLong" tabIndex="1" Width="179px" CssClass="tableRadioButton" Runat="server"
										Text="Linehaul" GroupName="GrpRouteType" AutoPostBack="True" Font-Size="Smaller"></asp:radiobutton></td>
								<td style="WIDTH: 170px"><asp:radiobutton id="rbtnShort" tabIndex="2" Width="104px" CssClass="tableRadioButton" Runat="server"
										Text="Short Route" GroupName="GrpRouteType" AutoPostBack="True" Font-Size="Smaller"></asp:radiobutton></td>
								<td width="10">&nbsp;</td>
								<td>&nbsp;<asp:radiobutton id="rbtnAir" tabIndex="3" Width="182px" CssClass="tableRadioButton" Runat="server"
										Text="Air Route" GroupName="GrpRouteType" AutoPostBack="True" Font-Size="Smaller"></asp:radiobutton></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<TR height="27">
								<TD colSpan="7">&nbsp;</TD>
							</TR>
							<TR height="27">
								<TD>
									<P align="right"><asp:requiredfieldvalidator id="validDlvryPath" tabIndex="6" Runat="server" ControlToValidate="DbComboPathCode"
											ErrorMessage="Delivery Path code" Display="None">*</asp:requiredfieldvalidator></P>
								</TD>
								<td><asp:label id="lblDlvryPath" tabIndex="52" runat="server" Width="100%" CssClass="tableLabel">Delivery Path Code</asp:label></td>
								<TD style="WIDTH: 170px"><dbcombo:dbcombo id="DbComboPathCode" tabIndex="4" Runat="server" AutoPostBack="True" TextBoxColumns="18"
										ServerMethod="DbComboPathCodeSelect" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" CloseResultsOnBlur="False"
										ForceDownLevel="False" ClientStateFunction="DbComboPathCodeOnSelect"></dbcombo:dbcombo></TD>
								<TD>&nbsp;</TD>
								<td><asp:textbox id="txtPathDesc" tabIndex="5" runat="server" Width="135px" CssClass="textField"
										ReadOnly="True"></asp:textbox></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</TR>
							<TR height="27">
								<TD>
									<P align="right"><asp:requiredfieldvalidator id="validVehicleNo" tabIndex="6" Runat="server" ControlToValidate="dbFlightVehicleNo"
											ErrorMessage="Vehicle No" Display="None">*</asp:requiredfieldvalidator></P>
								</TD>
								<TD><asp:label id="lblVehNo" tabIndex="55" runat="server" Width="100%" CssClass="tableLabel">Vehicle No</asp:label></TD>
								<TD style="WIDTH: 170px"><DBCOMBO:DBCOMBO id="dbFlightVehicleNo" tabIndex="6" Runat="server" AutoPostBack="True" TextBoxColumns="18"
										ServerMethod="ShowFlightVehicleNo" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False" NewVersionAvailable="True"></DBCOMBO:DBCOMBO></TD>
								<TD><asp:requiredfieldvalidator id="validDepartDateTime" tabIndex="6" Runat="server" ControlToValidate="txtDeptrDtTime"
										ErrorMessage="Departure Date" Display="None">*</asp:requiredfieldvalidator></TD>
								<TD><asp:label id="lblDeptrDtTime" tabIndex="54" runat="server" Width="100%" CssClass="tableLabel">Departure Date</asp:label></TD>
								<td>&nbsp;<cc1:mstextbox id="txtDeptrDtTime" tabIndex="9" runat="server" Width="135px" CssClass="textField"
										AutoPostBack="True" TextMaskString="99/99/9999 99:99" MaxLength="16" TextMaskType="msDateTime"></cc1:mstextbox></td>
								<td>&nbsp;</td>
							</TR>
							<TR height="27">
								<TD>
									<P align="right"><asp:requiredfieldvalidator id="validDlvryDateTime" tabIndex="6" runat="server" ControlToValidate="DbComboDlvryDt"
											ErrorMessage="Manifested Date" Display="None">*</asp:requiredfieldvalidator></P>
								</TD>
								<td><asp:label id="lblDlvryDateTime" runat="server" Width="100%" CssClass="tableLabel">Manifested Date</asp:label></td>
								<TD style="WIDTH: 170px"><dbcombo:dbcombo id="DbComboDlvryDt" tabIndex="7" runat="server" Width="120px" AutoPostBack="True"
										TextBoxColumns="18" ServerMethod="ShowManifestedDate" RegistrationKey=" " TextUpLevelSearchButton="v" ShowDbComboLink="False"
										CloseResultsOnBlur="False" ClientStateFunction="DbComboDlvryDtState()" Height="4px"></dbcombo:dbcombo></TD>
								<TD>&nbsp;</TD>
								<td><asp:label id="lblDriverName" tabIndex="58" runat="server" Width="100%" CssClass="tableLabel">Driver Name</asp:label></td>
								<td>&nbsp;<asp:textbox id="txtDriverName" tabIndex="8" runat="server" Width="135px" CssClass="textField"></asp:textbox></td>
								<td>&nbsp;</td>
							</TR>
							<TR height="27">
								<TD colSpan="7">&nbsp;</TD>
							</TR>
							<TR height="27">
								<td>&nbsp;</td>
								<td><asp:label id="lblRouteCode" tabIndex="53" runat="server" Width="100%" CssClass="tableLabel">Route Code</asp:label></td>
								<TD style="WIDTH: 170px"><cc1:mstextbox id="txtRouteCode" tabIndex="10" runat="server" Width="135px" CssClass="textField"
										AutoPostBack="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;
									<asp:button id="btnRouteCodeSearch" tabIndex="11" runat="server" CssClass="queryButton" Text="..."
										CausesValidation="False"></asp:button></TD>
								<td>&nbsp;</td>
								<td><asp:textbox id="txtRouteDesc" tabIndex="12" runat="server" Width="135px" CssClass="textField"
										ReadOnly="True"></asp:textbox></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</TR>
							<TR height="27">
								<td>&nbsp;</td>
								<td><asp:label id="lblOrigin" tabIndex="56" runat="server" Width="100%" CssClass="tableLabel">Origin</asp:label></td>
								<TD style="WIDTH: 170px"><asp:textbox id="txtOrigin" tabIndex="13" runat="server" Width="135px" CssClass="textField" ReadOnly="True"></asp:textbox></TD>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</TR>
							<TR>
								<td>&nbsp;</td>
								<TD><asp:label id="lblDestination" tabIndex="57" runat="server" Width="100%" CssClass="tableLabel">Destination</asp:label></TD>
								<TD style="WIDTH: 170px"><asp:textbox id="txtDestination" tabIndex="14" runat="server" Width="135px" CssClass="textField"
										ReadOnly="True"></asp:textbox></TD>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</TR>
							<TR height="27">
								<TD colSpan="7">&nbsp;</TD>
							</TR>
							<TR>
								<td align="right"><asp:requiredfieldvalidator id="validStatusCode" tabIndex="6" Runat="server" ControlToValidate="txtStatusCode"
										ErrorMessage="Status Code" Display="None">*</asp:requiredfieldvalidator></td>
								<td><asp:label id="lblStatusCode" tabIndex="59" runat="server" Width="100%" CssClass="tableLabel"
										Height="22px">Status Code</asp:label></td>
								<td colSpan="4"><cc1:mstextbox id="txtStatusCode" tabIndex="18" runat="server" Width="135px" CssClass="textField"
										AutoPostBack="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;
									<asp:button id="btnStatusCodeSearch" tabIndex="19" runat="server" CssClass="queryButton" Text="..."
										CausesValidation="False"></asp:button>&nbsp; &nbsp;
									<asp:textbox id="txtStatusDesc" tabIndex="20" runat="server" Width="200px" CssClass="textField"
										ReadOnly="True" Enabled="True"></asp:textbox></td>
								<td>&nbsp;</td>
							</TR>
							<TR>
								<td>&nbsp;</td>
								<td><asp:label id="lblExceptionCode" tabIndex="60" runat="server" Width="100%" CssClass="tableLabel"
										Height="22px">Exception Code</asp:label></td>
								<td colSpan="4"><cc1:mstextbox id="txtExceptioncode" tabIndex="21" runat="server" Width="135px" CssClass="textField"
										AutoPostBack="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox>&nbsp;
									<asp:button id="btnExcepDescSrch" tabIndex="22" runat="server" CssClass="queryButton" Text="..."
										CausesValidation="False"></asp:button>&nbsp; &nbsp;
									<asp:textbox id="txtExceptionDesc" tabIndex="23" runat="server" Width="200px" CssClass="textField"
										ReadOnly="True" Enabled="True"></asp:textbox></td>
								<td>&nbsp;</td>
							</TR>
							<TR>
								<td>
									<P align="right">&nbsp;
										<asp:requiredfieldvalidator id="validStatusDateTime" tabIndex="6" Runat="server" ControlToValidate="txtStatusDateTime"
											ErrorMessage="Status Date" Display="None">*</asp:requiredfieldvalidator></P>
								</td>
								<td><asp:label id="lblStatusDate" tabIndex="61" runat="server" Width="100%" CssClass="tableLabel"
										Height="22px">Status Date</asp:label></td>
								<td style="WIDTH: 170px"><cc1:mstextbox id="txtStatusDateTime" tabIndex="24" runat="server" Width="165px" CssClass="textField"
										TextMaskString="99/99/9999 99:99" MaxLength="16" TextMaskType="msDateTime"></cc1:mstextbox></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</TR>
							<TR>
								<TD>
									<P align="right"><asp:requiredfieldvalidator id="validPersonInChrg" tabIndex="6" Runat="server" ControlToValidate="txtPersonInchrg"
											ErrorMessage="Person In Charge" Display="None">*</asp:requiredfieldvalidator></P>
								</TD>
								<TD><asp:label id="lblPersonInchrg" tabIndex="62" runat="server" Width="100%" CssClass="tableLabel"
										Height="22px">Person In-Charge</asp:label></TD>
								<TD style="WIDTH: 170px"><cc1:mstextbox id="txtPersonInchrg" tabIndex="25" Width="195px" CssClass="textfield" Runat="server"
										MaxLength="100" TextMaskType="msAlfaNumericWithSpace"></cc1:mstextbox></TD>
								<TD></TD>
								<TD></TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD>
									<asp:label id="lblLocation" tabIndex="62" runat="server" CssClass="tableLabel" Width="100%"
										Height="22px">Location</asp:label></TD>
								<TD style="WIDTH: 170px">
									<cc1:mstextbox id="txtLocation" tabIndex="25" Runat="server" CssClass="textfield" Width="195px"
										TextMaskType="msAlfaNumericWithSpace" MaxLength="100"></cc1:mstextbox></TD>
								<TD></TD>
								<TD></TD>
								<TD></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD><asp:label id="lblRemarks" tabIndex="63" runat="server" Width="100%" CssClass="tableLabel"
										Height="22px">Remarks</asp:label></TD>
								<TD colSpan="4"><cc1:mstextbox id="txtRemarks" tabIndex="26" Width="600px" CssClass="textfield" Runat="server"
										MaxLength="100" TextMaskType="msAlfaNumericWithSpace"></cc1:mstextbox></TD>
								<TD></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td>
						<DIV style="BORDER-BOTTOM: whitesmoke thin solid; BORDER-LEFT: whitesmoke thin solid; HEIGHT: 175px; OVERFLOW: auto; BORDER-TOP: whitesmoke thin solid; BORDER-RIGHT: whitesmoke thin solid"><asp:datagrid id="dgShipRoute" tabIndex="10" runat="server" Width="100%" ItemStyle-Height="20"
								AutoGenerateColumns="False" PageSize="5" SelectedItemStyle-CssClass="gridFieldSelected">
								<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
								<ItemStyle Height="20px" CssClass="gridField" HorizontalAlign="Center"></ItemStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Select">
										<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle CssClass="gridField" HorizontalAlign="Center"></ItemStyle>
										<ItemTemplate>
											<asp:CheckBox ID="chkSelect" Runat="server"></asp:CheckBox>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Consignment No">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblConsgmntNo" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Route Code">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblRouteCoe" Text='<%#DataBinder.Eval(Container.DataItem,"route_code")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Origin">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblDGOrigin" Text='<%#DataBinder.Eval(Container.DataItem,"origin_state_code")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Destination">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblDGDestination" Text='<%#DataBinder.Eval(Container.DataItem,"destination_state_code")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn Visible="False">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label id=Label1 Runat="server" CssClass="gridLabel" Text='<%#DataBinder.Eval(Container.DataItem,"isSelected")%>' Enabled="True">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid></DIV>
					</td>
				</tr>
				<tr>
					<td align="center"><asp:button id="btnRetrieveWayBill" tabIndex="15" runat="server" CssClass="queryButton" Text="Retrieve Consignments"
							CausesValidation="False"></asp:button><asp:button id="btnSelectAll" tabIndex="16" runat="server" CssClass="queryButton" Text="Select All"
							CausesValidation="False"></asp:button><asp:button id="btnClearAll" tabIndex="17" runat="server" CssClass="queryButton" Text="Clear All"
							CausesValidation="False"></asp:button></td>
				</tr>
			</TABLE>
			<asp:validationsummary id="PageValidationSummary" runat="server" Width="205px" Font-Size="Smaller" Height="39px"
				ShowMessageBox="True" DisplayMode="BulletList" HeaderText="Please check the following information:"
				ShowSummary="False"></asp:validationsummary><INPUT 
type=hidden value="<%=strScrollPosition%>" name=ScrollPosition>
		</form>
	</body>
</HTML>
