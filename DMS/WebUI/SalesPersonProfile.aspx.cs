using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.DAL;
using com.common.RBAC;
using com.ties.DAL;
using com.common.util;
using com.common.applicationpages;
namespace com.ties
{
	/// <summary>
	/// Summary description for SalesPersonProfile.
	/// </summary>
	public class SalesPersonProfile : BasePage
	{
		protected System.Web.UI.WebControls.DataGrid dgSalesman;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnInsert;
		private String m_strAppID=null;
		private String m_strEnterpriseID=null;
		protected System.Web.UI.WebControls.Label lblSalesPerson;
		protected System.Web.UI.WebControls.Label lblErrMsg;
		protected System.Web.UI.WebControls.ValidationSummary reqSummary;
		protected System.Web.UI.WebControls.ValidationSummary Pagevalid;
		private SessionDS m_sdsSalesman=null;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			if(!Page.IsPostBack)
			{
				m_sdsSalesman = new SessionDS();
				m_sdsSalesman.ds = SysDataManager1.GetEmptySalesman();
				BindSalesman();
				showEditColumn(false);
				showDeleteColumn(false);
				btnExecuteQuery.Enabled=false;
				Session["SESSION_DS1"] = m_sdsSalesman;

				ViewState["SCMode"] = ScreenMode.None;
				ViewState["SCOperation"] = Operation.None;

				//-----START QUERYING----
				lblErrMsg.Text="";
				m_sdsSalesman.ds = SysDataManager1.GetEmptySalesman();
				AddRow();
				btnExecuteQuery.Enabled=true;
				dgSalesman.EditItemIndex=0;
				ViewState["SCOperation"]=Operation.Update;
				ViewState["SCMode"] = ScreenMode.Query;
				BindSalesman();
				showDeleteColumn(false);
				showEditColumn(false);
				btnExecuteQuery.Enabled=true;				
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				Session["QUERY_DS"] = m_sdsSalesman;
			}
			else
			{
				m_sdsSalesman = (SessionDS)Session["SESSION_DS1"];
			}
			Pagevalid.HeaderText = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_MISS_FIELD", utility.GetUserCulture());
		}
		private void showEditColumn(bool toShow)
		{
			dgSalesman.Columns[0].Visible=toShow;
		}
		private void showDeleteColumn(bool toShow)
		{
			dgSalesman.Columns[1].Visible=toShow;
		}
		protected void OnCancel_Salesman(object sender, DataGridCommandEventArgs e)
		{	
			lblErrMsg.Text="";
			if((int)ViewState["SCOperation"] == (int)Operation.Insert ||(int)ViewState["SCOperation"] == (int)Operation.Update)
			{
				m_sdsSalesman.ds.Tables[0].Rows.RemoveAt(e.Item.ItemIndex);
				m_sdsSalesman.ds.AcceptChanges();
			}
			dgSalesman.EditItemIndex=-1;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			BindSalesman();
		}
		protected void OnEdit_Salesman(object sender, DataGridCommandEventArgs e)
		{	
			lblErrMsg.Text="";
			ViewState["SCOperation"]=Operation.Update;
			int rowIndex=e.Item.ItemIndex;
			
			if(dgSalesman.EditItemIndex > rowIndex)
				return;

			dgSalesman.EditItemIndex=rowIndex;			

			if(dgSalesman.EditItemIndex >0)
			{
				msTextBox txtSalesID = (msTextBox)dgSalesman.Items[dgSalesman.EditItemIndex].FindControl("txtSalesID");
				
				if(txtSalesID !=null && txtSalesID.Text=="")	
				{
//					ViewState["m_strCode"] = txtSalesID.Text;
					m_sdsSalesman.ds.Tables[0].Rows.RemoveAt(dgSalesman.EditItemIndex);
					m_sdsSalesman.ds.AcceptChanges();
				}
			}
			BindSalesman();
		}
		protected void OnUpdate_Salesman(object sender, DataGridCommandEventArgs e)
		{	
			SessionDS sds = new SessionDS();
			if(CaptureNewRow(e.Item.ItemIndex))
			{
				sds.ds = m_sdsSalesman.ds.GetChanges();
				m_sdsSalesman.ds.AcceptChanges();
				if((int)ViewState["SCOperation"]==(int)Operation.Insert)
				{
					try
					{
						SysDataManager1.InsertSalesman(m_strAppID,m_strEnterpriseID,sds);
						dgSalesman.EditItemIndex=-1;
						BindSalesman();
						lblErrMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
						Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
						//ViewState["SCOperation"] = Operation.Saved;
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;						
						if(strMsg.IndexOf("duplicate key") != -1 )
						{
							lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());								
						}
						else if(strMsg.IndexOf("FOREIGN KEY") != -1 )
						{
							lblErrMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"FOREIGN_KEY_FOUND",utility.GetUserCulture());	
						}
						else if(strMsg.IndexOf("unique") != -1 )
						{
							lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());	
						}
						else if(strMsg.IndexOf("parent key") != -1 )
						{
							lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());	
						}
						return;
					}
					
				}
				if((int)ViewState["SCOperation"]==(int)Operation.Update)
				{
					try
					{
						SysDataManager1.UpdateSalesman(m_strAppID,m_strEnterpriseID,sds);
						dgSalesman.EditItemIndex=-1;
						BindSalesman();
						lblErrMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());
						ViewState["SCOperation"] = Operation.Saved;
					}
					catch(ApplicationException appException)
					{
						String strMsg = appException.Message;
						strMsg = appException.InnerException.Message;						
						if(strMsg.IndexOf("duplicate key") != -1 )						
						{
							lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());								
						}
						else if(strMsg.IndexOf("FOREIGN KEY") != -1 )
						{
							lblErrMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"FOREIGN_KEY_FOUND",utility.GetUserCulture());	
						}
						else if(strMsg.IndexOf("unique") != -1 )
						{
							lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());	
						}
						else if(strMsg.IndexOf("parent key") != -1 )
						{
							lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());	
						}
						return;
					}
				}
			}
			
		}
		private bool isCodeNotBlank(int row)
		{
			//msTextBox txtSalesID = (msTextBox)dgSalesman.Items[row].FindControl("txtSalesID");
			DataRow dr = m_sdsSalesman.ds.Tables[0].Rows[row];			
			if(dr!=null)
			{
				if(dr["salesmanid"].ToString()!="")
				{
					return true;
				}
			}
			return false;
		}
		protected void OnDelete_Salesman(object sender, DataGridCommandEventArgs e)
		{
			//*Raj*//
			if (dgSalesman.EditItemIndex == e.Item.ItemIndex)
			{
				return;
			}			
			if((int)ViewState["SCOperation"]==(int)Operation.Insert)
			{
				DeleteSalesman(e.Item.ItemIndex);
			}
			else
			{
				DeleteSalesman(e.Item.ItemIndex);
				showCurrentPage();
			}
			if(dgSalesman.EditItemIndex < -1)
			{
				m_sdsSalesman.ds.Tables[0].Rows.RemoveAt(dgSalesman.EditItemIndex);
			}	
			BindSalesman();			
		}

		private void DeleteSalesman(int row)
		{
			DataRow dr = m_sdsSalesman.ds.Tables[0].Rows[row];
			try
			{
				bool success = SysDataManager1.DeleteSalesman(m_strAppID,m_strEnterpriseID,dr["salesmanid"].ToString());
				if(success)
				{
					lblErrMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
					m_sdsSalesman.ds.Tables[0].Rows.RemoveAt(row);
				}
				else
				{
					lblErrMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"NO_DEL",utility.GetUserCulture());
				}
			}
			
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1 || strMsg.IndexOf("REFERENCE") != -1)
				{
					lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());	
				}
				if(strMsg.IndexOf("child record") != -1)
				{
					lblErrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());	
				}
				return;
			}	
		}

		protected void OnSalesman_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgSalesman.CurrentPageIndex = e.NewPageIndex;
			showCurrentPage();
			dgSalesman.SelectedIndex = -1;
			dgSalesman.EditItemIndex = -1;
			BindSalesman();
		}
		protected void OnItemBound_Salesman(object sender, DataGridItemEventArgs e)
		{
			msTextBox txtSalesID = (msTextBox)e.Item.FindControl("txtSalesID");
			if(txtSalesID!=null)
			{
				if((int)ViewState["SCOperation"]!=(int)Operation.Insert)
				{
					txtSalesID.Enabled=false;
				}
				if((int)ViewState["SCMode"] == (int)ScreenMode.Query)
				{
					txtSalesID.Enabled=true;
				}
			}
			
		}
		private bool CaptureNewRow(int row)
		{
			DataRow dr = m_sdsSalesman.ds.Tables[0].Rows[row];
			
			msTextBox txtSalesID = (msTextBox)dgSalesman.Items[row].FindControl("txtSalesID");
			TextBox txtRef=(TextBox)dgSalesman.Items[row].FindControl("txtRef");
			TextBox txtName=(TextBox)dgSalesman.Items[row].FindControl("txtName");
			msTextBox txtCommission=(msTextBox)dgSalesman.Items[row].FindControl("txtCommission");
			msTextBox txtTelephone=(msTextBox)dgSalesman.Items[row].FindControl("txtTelephone");
			msTextBox txtMobile=(msTextBox)dgSalesman.Items[row].FindControl("txtMobile");
			TextBox txtEmail=(TextBox)dgSalesman.Items[row].FindControl("txtEmail");
			TextBox txtDesignation=(TextBox)dgSalesman.Items[row].FindControl("txtDesignation");
			if((int)ViewState["SCOperation"]==(int)Operation.Insert)
			{
				if(txtSalesID!=null)
				{
					if(txtSalesID.Text!="")
					{
						try
						{
							dr["salesmanid"] = txtSalesID.Text;
							dr["salesman_ref"] = txtRef.Text;
							if(txtName.Text=="")
							{
								lblErrMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SALEMAN_NAME",utility.GetUserCulture());
								return false;
							}
							dr["salesman_name"] = txtName.Text;
							dr["commission"] = txtCommission.Text;
							dr["telephone"] = txtTelephone.Text;
							dr["mobile_phone"] = txtMobile.Text;
							dr["email"] = txtEmail.Text;
							dr["designation"] = txtDesignation.Text;
							return true;
						}
						catch(Exception ex)
						{
							lblErrMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
							Logger.LogTraceError("salesman","CaptureNewRow","Error 1",ex.Message);
						}
					}
					else
					{
						lblErrMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SALEMAN_ID",utility.GetUserCulture());
					}
				}
			}
			else if((int)ViewState["SCOperation"]==(int)Operation.Update)
			{
				dr["salesmanid"] = txtSalesID.Text;
				dr["salesman_ref"] = txtRef.Text;
				dr["salesman_name"] = txtName.Text;

				if (txtCommission.Text.Trim()=="")
					dr["commission"]=System.DBNull.Value;
				else
					dr["commission"] = Convert.ToDecimal(txtCommission.Text);
				
				dr["telephone"] = txtTelephone.Text;
				dr["mobile_phone"] = txtMobile.Text;
				dr["email"] = txtEmail.Text;
				dr["designation"] = txtDesignation.Text;
				return true;
			}

			return false;

		}
		
		private void AddRow()
		{
			SysDataManager1.AddNewRowInSalesManDS(ref m_sdsSalesman);
			BindSalesman();
		}

//		private void AddRow()
//		{	
//			DataRow drNew = m_sdsSalesman.ds.Tables[0].NewRow();
//			m_sdsSalesman.ds.Tables[0].Rows.Add(drNew);	
//		}

		private void BindSalesman()
		{
			dgSalesman.VirtualItemCount = System.Convert.ToInt32(m_sdsSalesman.QueryResultMaxSize);
			dgSalesman.DataSource=m_sdsSalesman.ds;
			dgSalesman.DataBind();
			Session["SESSION_DS1"] = m_sdsSalesman;
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			lblErrMsg.Text="";
			ViewState["SCMode"] = ScreenMode.Insert;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,false,m_moduleAccessRights);
			btnExecuteQuery.Enabled = false;
			dgSalesman.CurrentPageIndex = 0;
			if((int)ViewState["SCOperation"]!=(int)Operation.Insert || m_sdsSalesman.ds.Tables[0].Rows.Count >= dgSalesman.PageSize)
			{
				m_sdsSalesman.ds = SysDataManager1.GetEmptySalesman();
				m_sdsSalesman.DataSetRecSize=0;
				m_sdsSalesman.QueryResultMaxSize=0;
				AddRow();
				BindSalesman();
				dgSalesman.EditItemIndex = 0;//m_sdsSalesman.ds.Tables[0].Rows.Count-1;
			}
			else
			{
				AddRow();
				dgSalesman.EditItemIndex = m_sdsSalesman.ds.Tables[0].Rows.Count-1;
			}
			
			showEditColumn(true);
			showDeleteColumn(true);
			//dgSalesman.EditItemIndex+=1;
			ViewState["SCOperation"]=Operation.Insert;
			BindSalesman();
			getPageControls(Page);
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			lblErrMsg.Text="";
			ViewState["SCMode"] = ScreenMode.ExecuteQuery;
			CaptureNewRow(0);
			m_sdsSalesman = SysDataManager1.QuerySalesman(m_strAppID,m_strEnterpriseID,0,dgSalesman.PageSize,m_sdsSalesman);
			
			if(m_sdsSalesman.QueryResultMaxSize<1)
			{
				lblErrMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			}
			dgSalesman.EditItemIndex=-1;
			ViewState["SCOperation"] = Operation.Update;
			showEditColumn(true);
			showDeleteColumn(true);
			btnExecuteQuery.Enabled=false;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);			
			BindSalesman();

		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			lblErrMsg.Text="";
			m_sdsSalesman.ds = SysDataManager1.GetEmptySalesman();
			AddRow();
			btnExecuteQuery.Enabled=true;
			dgSalesman.EditItemIndex=0;
			ViewState["SCOperation"]=Operation.Update;
			ViewState["SCMode"] = ScreenMode.Query;
			BindSalesman();
			showDeleteColumn(false);
			showEditColumn(false);
			btnExecuteQuery.Enabled=true;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);			
			Session["QUERY_DS"] = m_sdsSalesman;

		}
		private void showCurrentPage()
		{
			SessionDS m_sdsQuerySalesman = (SessionDS)Session["QUERY_DS"];
			int iStartIndex = dgSalesman.CurrentPageIndex * dgSalesman.PageSize;
			
			m_sdsSalesman = SysDataManager1.QuerySalesman(m_strAppID, m_strEnterpriseID,iStartIndex,dgSalesman.PageSize,m_sdsQuerySalesman);
			int pgCnt = (Convert.ToInt32(m_sdsSalesman.QueryResultMaxSize - 1))/dgSalesman.PageSize;
			if(pgCnt < dgSalesman.CurrentPageIndex)
			{
				dgSalesman.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			//Session["SESSION_DS1"] = m_sdsSalesman;
			
		}

	}
}
