using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for ZipCodeGeneratePopup.
	/// </summary>
	public class ZipCodeGeneratePopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label lblCountry;
		protected System.Web.UI.WebControls.Label lblCutoffTime;
		protected System.Web.UI.WebControls.Label lblStateCode;
		protected System.Web.UI.WebControls.Label lblZoneCode;
		protected System.Web.UI.WebControls.Label lblArea;
		protected System.Web.UI.WebControls.Label lblRegion;
		protected System.Web.UI.WebControls.Label lblDeliveryRoute;
		protected System.Web.UI.WebControls.Button btnGenerate;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Label lblPickupRoute;
		protected System.Web.UI.WebControls.Label lblFZipCode;
		protected System.Web.UI.WebControls.Label lblTZipCode;
		protected System.Web.UI.WebControls.TextBox txtArea;
		protected System.Web.UI.WebControls.TextBox txtRegion;
		protected System.Web.UI.WebControls.TextBox txtFZipCode;
		protected System.Web.UI.WebControls.TextBox txtTZipCode;
		protected System.Web.UI.WebControls.Button btnCountrySearch;
		protected System.Web.UI.WebControls.TextBox txtStateCode;
		protected System.Web.UI.WebControls.Button btnStateSearch;
		protected System.Web.UI.WebControls.TextBox txtZoneCode;
		protected System.Web.UI.WebControls.Button btnZoneSearch;
		protected System.Web.UI.WebControls.TextBox txtCutoffTime;
		protected System.Web.UI.WebControls.TextBox txtPickupRoute;
		protected System.Web.UI.WebControls.TextBox txtDeliveryRoute;
		protected System.Web.UI.WebControls.RequiredFieldValidator validZoneCode;
		protected System.Web.UI.WebControls.RequiredFieldValidator validTZipCode;
		protected System.Web.UI.WebControls.RequiredFieldValidator validStateCode;
		protected System.Web.UI.WebControls.RequiredFieldValidator validCountry;
		protected System.Web.UI.WebControls.RequiredFieldValidator validFZipCode;
		protected com.common.util.msTextBox txtCountry;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
