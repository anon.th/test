<%@ Page language="c#" Codebehind="DomesticShipmentDisplay.aspx.cs" AutoEventWireup="false" Inherits="com.ties.DomesticShipmentDisplay" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DomesticShipmentDisplay</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript">
		<!-- This is called at the click of Package Details button
		//-->
			function CallCustomerWindow()
			{
				var popupWindow = window.open("CustomerPopup.aspx",'',"'height=320;width=160;left=100;top=50;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'");
			}
			
			function CallCommodityWin()
			{
				window.open("CommodityPopup.aspx",'',"'height=320;width=160;left=100;top=50;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'");
			}
		
			function CallServiceWin()
			{
				window.open("ServiceCodePopup.aspx",'',"'height=320;width=160;left=100;top=50;location=no;menubar=no;resizable=yes;scrollbars=no;status=no;titlebar=yes;toolbar=no'");
			}
		
		</script>
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="DomesticShipmentDisplay" method="post" runat="server">
			<asp:button id="btnQry" style="Z-INDEX: 110; LEFT: 3px; POSITION: absolute; TOP: 46px" tabIndex="1" runat="server" Text="Query" CausesValidation="False" CssClass="queryButton" Height="22px" Width="62px" Visible="False"></asp:button><asp:panel id="DomstcShipPanel" style="Z-INDEX: 114; LEFT: 6px; POSITION: absolute; TOP: 2168px" runat="server" Height="138px" Width="311px" Visible="False" BackColor="White">
				<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</P>
				<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:Panel id="ConfirmPanel" runat="server" Width="191px" Height="56px" BackColor="Gray"></P>
				<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<asp:Label id="lblConfirmMsg" runat="server"></asp:Label></P>
				<P>
				<P><FONT style="BACKGROUND-COLOR: #808080">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </FONT>
					<asp:Button id="btnToSaveChanges" runat="server" CausesValidation="False" Text="Yes"></asp:Button>
					<asp:Button id="btnNotToSave" runat="server" CausesValidation="False" Text="No"></asp:Button>
					<asp:Button id="btnToCancel" runat="server" CausesValidation="False" Text="Cancel"></asp:Button></P>
			</asp:panel>
			<P></P>
			</asp:panel><asp:button id="btnExecQry" style="Z-INDEX: 102; LEFT: 65px; POSITION: absolute; TOP: 46px" tabIndex="2" runat="server" Text="Execute Query" CausesValidation="False" CssClass="queryButton" Visible="False"></asp:button><asp:button id="btnInsert" style="Z-INDEX: 108; LEFT: 195px; POSITION: absolute; TOP: 47px" tabIndex="3" runat="server" Text="Insert" CausesValidation="False" CssClass="queryButton" Height="21px" Width="62px" Visible="False"></asp:button><asp:button id="btnSave" style="Z-INDEX: 101; LEFT: 257px; POSITION: absolute; TOP: 46px" tabIndex="5" runat="server" Text="save" CssClass="queryButton" Height="22px" Width="66px" Visible="False"></asp:button><asp:button id="btnDelete" style="Z-INDEX: 109; LEFT: 323px; POSITION: absolute; TOP: 47px" tabIndex="5" runat="server" Text="Delete" CausesValidation="False" CssClass="queryButton" Height="21px" Width="62px" Visible="False"></asp:button><asp:button id="btnGoToFirstPage" style="Z-INDEX: 107; LEFT: 598px; POSITION: absolute; TOP: 49px" tabIndex="6" runat="server" Text="|<" CausesValidation="False" CssClass="queryButton" Width="24px" Visible="False"></asp:button><asp:button id="btnPreviousPage" style="Z-INDEX: 103; LEFT: 622px; POSITION: absolute; TOP: 49px" tabIndex="7" runat="server" Text="<" CausesValidation="False" CssClass="queryButton" Width="24px" Visible="False"></asp:button><asp:button id="btnNextPage" style="Z-INDEX: 104; LEFT: 668px; POSITION: absolute; TOP: 49px" tabIndex="9" runat="server" Text=">" CausesValidation="False" CssClass="queryButton" Width="24px" Visible="False"></asp:button><asp:button id="btnGoToLastPage" style="Z-INDEX: 105; LEFT: 692px; POSITION: absolute; TOP: 49px" tabIndex="10" runat="server" Text=">|" CausesValidation="False" CssClass="queryButton" Width="24px" Visible="False"></asp:button><asp:label id="lblMainTitle" style="Z-INDEX: 111; LEFT: 4px; POSITION: absolute; TOP: 4px" runat="server" CssClass="mainTitleSize" Height="26px" Width="477px">Domestic Shipment</asp:label><asp:label id="lblErrorMsg" style="Z-INDEX: 112; LEFT: 17px; POSITION: absolute; TOP: 69px" runat="server" CssClass="errorMsgColor" Height="19px" Width="566px"></asp:label><asp:textbox id="txtGoToRec" style="Z-INDEX: 106; LEFT: 646px; POSITION: absolute; TOP: 50px" tabIndex="8" runat="server" Height="17px" Width="23px" Visible="False" Enabled="False"></asp:textbox>
			<table id="MainTable" style="Z-INDEX: 113; LEFT: 4px; WIDTH: 713px; POSITION: absolute; TOP: 94px; HEIGHT: 1623px" height="1623" width="713" border="0" runat="server">
				<tr>
					<td style="WIDTH: 738px; HEIGHT: 185px" width="738">
						<TABLE id="TblBookDtl" height="125" width="548" border="0" runat="server">
							<tr height="17%">
								<td width="3%"></td>
								<td style="WIDTH: 153px" width="153"><asp:label id="lblBookingNo" tabIndex="11" runat="server" CssClass="tableLabel" Height="22px" Width="141px">Booking No</asp:label></td>
								<td style="WIDTH: 139px" width="139"><asp:textbox id="txtBookingNo" tabIndex="12" runat="server" CssClass="textField" Width="141px" MaxLength="99999999" AutoPostBack="True"></asp:textbox></td>
								<td width="5%"></td>
								<td width="20%"><asp:label id="lblBookDate" tabIndex="13" runat="server" CssClass="tableLabel" Height="22px" Width="108px">Booking Date</asp:label></td>
								<td width="20%"><asp:textbox id="txtBookDate" tabIndex="14" runat="server" CssClass="textField" Width="99px" Enabled="False"></asp:textbox></td>
								<td width="15%"><asp:label id="lblOrigin" tabIndex="15" runat="server" CssClass="tableLabel" Height="22px" Width="82px">Origin</asp:label></td>
								<td width="15%"><asp:textbox id="txtOrigin" tabIndex="16" runat="server" CssClass="textField" Width="75px" Enabled="False" MaxLength="10"></asp:textbox></td>
							</tr>
							<tr height="17%">
								<td style="HEIGHT: 16.29%" width="3%"><asp:requiredfieldvalidator id="validConsgNo" Runat="server" ControlToValidate="txtConsigNo">*</asp:requiredfieldvalidator></td>
								<td style="WIDTH: 153px; HEIGHT: 16.29%" width="153"><asp:label id="lblConsgmtNo" tabIndex="17" runat="server" CssClass="tableLabel" Height="22px" Width="113px">Consignment No</asp:label></td>
								<td style="WIDTH: 139px; HEIGHT: 16.29%" width="139"><cc1:mstextbox id="txtConsigNo" tabIndex="18" runat="server" CssClass="textField" Width="141px" MaxLength="30" TextMaskType="msUpperAlfaNumericWithUnderscore"></cc1:mstextbox></td>
								<td style="HEIGHT: 16.29%" width="5%"></td>
								<td style="HEIGHT: 16.29%" width="20%"><asp:label id="lblRefNo" tabIndex="19" runat="server" CssClass="tableLabel" Height="22px" Width="108px">Ref No</asp:label></td>
								<td style="HEIGHT: 16.29%" width="20%"><asp:textbox id="txtRefNo" tabIndex="20" runat="server" CssClass="textField" Width="99px" MaxLength="20"></asp:textbox></td>
								<td style="HEIGHT: 16.29%" width="15%"><asp:label id="lblDestination" tabIndex="21" runat="server" CssClass="tableLabel" Height="22px" Width="82px">Destination</asp:label></td>
								<td style="HEIGHT: 16.29%" width="15%"><asp:textbox id="txtDestination" tabIndex="22" runat="server" CssClass="textField" Width="75px" Enabled="False" MaxLength="10"></asp:textbox></td>
							</tr>
							<tr height="15%">
								<td width="3%"></td>
								<td style="WIDTH: 153px" width="153"></td>
								<td style="WIDTH: 139px" width="139"></td>
								<td width="5%"></td>
								<td width="20%"></td>
								<td width="20%"></td>
								<td width="15%"></td>
								<td width="15%"></td>
							</tr>
							<tr height="17%">
								<td width="3%"></td>
								<td style="WIDTH: 153px" width="153"><asp:label id="lblShipManifestDt" tabIndex="23" runat="server" CssClass="tableLabel" Height="22px" Width="148px">Shipment Manifest Dt</asp:label></td>
								<td style="WIDTH: 139px" width="139"><asp:textbox id="txtShipManifestDt" tabIndex="24" runat="server" CssClass="textField" Width="139px" Enabled="False"></asp:textbox></td>
								<td width="5%"></td>
								<td width="55%" colSpan="2"><asp:label id="lblStatusCode" tabIndex="25" runat="server" CssClass="tableLabel" Height="22px" Width="186px">Latest Status Code</asp:label></td>
								<td width="15%"><asp:textbox id="txtLatestStatusCode" tabIndex="26" runat="server" CssClass="textField" Width="99px" Enabled="False"></asp:textbox></td>
							</tr>
							<tr height="17%">
								<td width="3%"></td>
								<td style="WIDTH: 153px" width="153"><asp:label id="lblRouteCode" tabIndex="27" runat="server" CssClass="tableLabel" Height="22px" Width="139px">Route Code</asp:label></td>
								<td style="WIDTH: 139px" width="139"><asp:textbox id="txtRouteCode" tabIndex="28" runat="server" CssClass="textField" Width="115px" Enabled="False" MaxLength="12" ReadOnly="True"></asp:textbox><asp:button id="btnRouteCode" tabIndex="29" runat="server" Text="..." CausesValidation="False" CssClass="searchButton" Width="21px" Visible="False" Enabled="False"></asp:button></td>
								<td width="5%"></td>
								<td width="55%" colSpan="2"><asp:label id="lblExcepCode" tabIndex="30" runat="server" CssClass="tableLabel" Height="22px" Width="185px">Latest Exception Code</asp:label></td>
								<td width="15%"><asp:textbox id="txtLatestExcepCode" tabIndex="31" runat="server" CssClass="textField" Width="99px" Enabled="False"></asp:textbox></td>
							</tr>
							<tr height="17%">
								<td width="3%"></td>
								<td style="WIDTH: 153px" width="153"></td>
								<td style="WIDTH: 139px" width="139"></td>
								<td width="5%"></td>
								<td width="55%" colSpan="2"><asp:label id="lblDelManifest" tabIndex="32" runat="server" CssClass="tableLabel" Height="22px" Width="185px">Delivery Manifest</asp:label></td>
								<td width="15%"><asp:textbox id="txtDelManifest" tabIndex="33" runat="server" CssClass="textField" Width="99px" Enabled="False"></asp:textbox></td>
							</tr>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td>
						<FIELDSET style="WIDTH: 739px; HEIGHT: 172px"><legend><asp:label id="lblCustInfo" CssClass="tableHeadingFieldset" Runat="server">Customer Information</asp:label></legend>
							<table id="tblCustInfo" style="WIDTH: 729px; HEIGHT: 158px" align="left" border="0" runat="server">
								<TR>
									<TD width="63"><asp:requiredfieldvalidator id="validCustType" Runat="server" ControlToValidate="ddbCustType">*</asp:requiredfieldvalidator></TD>
									<TD width="164"><asp:label id="lblCustType" tabIndex="34" runat="server" CssClass="tableLabel" Height="22px" Width="108px">Customer Type</asp:label></TD>
									<TD width="174"><asp:dropdownlist id="ddbCustType" tabIndex="35" runat="server" Height="26px" Width="157px"></asp:dropdownlist></TD>
									<TD width="117" colSpan="2"><asp:checkbox id="chkNewCust" tabIndex="36" runat="server" Text="New Customer" CssClass="normalText" Height="16px" Width="185px" Enabled="False" AutoPostBack="True"></asp:checkbox></TD>
									<TD></TD>
									<TD></TD>
								</TR>
								<tr>
									<TD style="WIDTH: 63px" width="63"><asp:requiredfieldvalidator id="validCustID" Runat="server" ControlToValidate="txtCustID">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 164px" width="164"><asp:label id="lblCustID" tabIndex="37" runat="server" CssClass="tableLabel" Height="22px" Width="144px">Customer ID</asp:label></TD>
									<TD style="WIDTH: 174px" width="174"><asp:textbox id="txtCustID" tabIndex="38" runat="server" CssClass="textField" Width="135px" MaxLength="20" AutoPostBack="True"></asp:textbox><asp:button id="btnDisplayCustDtls" tabIndex="39" runat="server" Text="..." CausesValidation="False" CssClass="searchButton" Width="21px" Visible="False"></asp:button></TD>
									<TD style="WIDTH: 117px" width="117"></TD>
									<TD width="15%"></TD>
									<TD width="15%"></TD>
									<TD width="10%"></TD>
								</tr>
								<tr>
									<TD style="WIDTH: 63px" width="63"><asp:requiredfieldvalidator id="validCustName" Runat="server" ControlToValidate="txtCustName">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 164px" width="164"><asp:label id="lblName" tabIndex="40" runat="server" CssClass="tableLabel" Height="22px" Width="108px">Name</asp:label></TD>
									<TD style="WIDTH: 174px" width="174"><asp:textbox id="txtCustName" tabIndex="41" runat="server" CssClass="textField" Width="157px" MaxLength="100"></asp:textbox></TD>
									<TD style="WIDTH: 117px" width="117"></TD>
									<TD width="15%"><asp:label id="lblPaymode" tabIndex="42" runat="server" CssClass="tableLabel" Height="22px" Width="108px">Payment Mode</asp:label></TD>
									<TD width="15%"><asp:radiobutton id="rbCash" tabIndex="43" runat="server" Text="Cash" Height="15px" Width="89px" Enabled="False" AutoPostBack="True" GroupName="PaymentType" Font-Size="Smaller"></asp:radiobutton></TD>
									<TD width="10%"><asp:radiobutton id="rbCredit" tabIndex="44" runat="server" Text="Credit" Height="15px" Width="71px" Enabled="False" AutoPostBack="True" GroupName="PaymentType" Font-Size="Smaller"></asp:radiobutton></TD>
								</tr>
								<tr>
									<TD style="WIDTH: 63px" width="63"><asp:requiredfieldvalidator id="validCustAdd1" Runat="server" ControlToValidate="txtCustAdd1">*</asp:requiredfieldvalidator></TD>
									<TD style="WIDTH: 164px" width="164"><asp:label id="lblAddress" tabIndex="45" runat="server" CssClass="tableLabel" Height="22px" Width="108px">Address</asp:label></TD>
									<TD style="WIDTH: 316px" width="316" colSpan="2"><asp:textbox id="txtCustAdd1" tabIndex="46" runat="server" CssClass="textField" Width="266px" MaxLength="100"></asp:textbox></TD>
									<TD width="15%"><asp:label id="lblTelphone" tabIndex="47" runat="server" CssClass="tableLabel" Height="22px" Width="108px">Telephone</asp:label></TD>
									<TD width="15%"><asp:textbox id="txtCustTelephone" tabIndex="48" runat="server" CssClass="textField" Width="99px" MaxLength="20"></asp:textbox></TD>
									<TD width="10%"></TD>
								</tr>
								<tr>
									<TD></TD>
									<TD></TD>
									<TD colSpan="2"><asp:textbox id="txtCustAdd2" tabIndex="49" runat="server" CssClass="textField" Width="266px"></asp:textbox></TD>
									<TD><asp:label id="lblFax" tabIndex="50" runat="server" CssClass="tableLabel" Height="22px" Width="108px">Fax</asp:label></TD>
									<TD><asp:textbox id="txtCustFax" tabIndex="51" runat="server" CssClass="textField" Width="99px" MaxLength="20"></asp:textbox></TD>
									<TD></TD>
								</tr>
								<tr>
									<TD><asp:requiredfieldvalidator id="validCustZip" Runat="server" ControlToValidate="txtCustZipCode" Display="Dynamic">*</asp:requiredfieldvalidator></TD>
									<TD><asp:label id="lblZip" tabIndex="52" runat="server" CssClass="tableLabel" Height="22px" Width="108px">Zip Code</asp:label></TD>
									<TD><asp:textbox id="txtCustZipCode" tabIndex="53" runat="server" CssClass="textField" Width="75px" MaxLength="10" AutoPostBack="True"></asp:textbox><asp:textbox id="txtCustCity" tabIndex="54" runat="server" CssClass="textField" Width="75px" ReadOnly="True"></asp:textbox></TD>
									<TD><asp:textbox id="txtCustStateCode" tabIndex="55" runat="server" CssClass="textField" Width="102px" ReadOnly="True"></asp:textbox></TD>
									<TD></TD>
									<TD></TD>
								</tr>
							</table>
						</FIELDSET>
					</td>
				</tr>
				<tr>
					<td style="WIDTH: 738px; HEIGHT: 210px" align="left" width="738">
						<fieldset style="WIDTH: 740px; HEIGHT: 201px"><legend><asp:label id="lblSendInfo" CssClass="tableHeadingFieldset" Runat="server">Sender Information</asp:label></legend>
							<table id="tblSenderInfo" border="0" runat="server" style="WIDTH: 728px; HEIGHT: 169px">
								<tr height="10%">
									<td width="100%" colSpan="7"><asp:checkbox id="chkSendCustInfo" tabIndex="56" runat="server" Text="Same as the Customer Information" CssClass="normalText" Height="14px" Width="504px" Enabled="False" AutoPostBack="True"></asp:checkbox></td>
								</tr>
								<tr>
									<td style="WIDTH: 15px" width="15"><asp:requiredfieldvalidator id="validSendName" Runat="server" ControlToValidate="txtSendName">*</asp:requiredfieldvalidator></td>
									<td style="WIDTH: 169px" width="169"><asp:label id="lblSendNm" tabIndex="57" runat="server" CssClass="tableLabel" Height="20px" Width="103px">Name</asp:label></td>
									<td style="WIDTH: 185px" width="185" colSpan="2"><asp:textbox id="txtSendName" tabIndex="58" runat="server" CssClass="textField" Width="179px" MaxLength="100"></asp:textbox></td>
									<td style="WIDTH: 66px" width="66"><asp:button id="btnSendCust" tabIndex="59" runat="server" Text="..." CausesValidation="False" CssClass="searchButton" Width="21px" Visible="False"></asp:button></td>
									<td style="WIDTH: 149px" width="149"><asp:label id="lblSendConPer" tabIndex="60" runat="server" CssClass="tableLabel" Width="118px">Contact Person</asp:label></td>
									<td width="25%"><asp:textbox id="txtSendContPer" tabIndex="61" runat="server" CssClass="textField" Width="161px"></asp:textbox></td>
								</tr>
								<tr>
									<td style="WIDTH: 15px" width="15"><asp:requiredfieldvalidator id="validSenderAdd1" Runat="server" ControlToValidate="txtSendAddr1">*</asp:requiredfieldvalidator></td>
									<td style="WIDTH: 169px" width="169"><asp:label id="lblSendAddr1" tabIndex="61" runat="server" CssClass="tableLabel" Height="21px" Width="103px">Address</asp:label></td>
									<td style="WIDTH: 185px" width="185" colSpan="2"><asp:textbox id="txtSendAddr1" tabIndex="62" runat="server" CssClass="textField" Width="182px" MaxLength="100"></asp:textbox></td>
									<td style="WIDTH: 66px" width="66"></td>
									<td style="WIDTH: 149px" width="149"><asp:label id="lblSendTel" tabIndex="63" runat="server" CssClass="tableLabel" Width="116px">Telephone</asp:label></td>
									<td width="25%"><asp:textbox id="txtSendTel" tabIndex="64" runat="server" CssClass="textField" Width="161px"></asp:textbox></td>
								</tr>
								<tr>
									<td style="WIDTH: 15px" width="15"></td>
									<td style="WIDTH: 169px" width="169"></td>
									<td style="WIDTH: 185px" width="185" colSpan="2"><asp:textbox id="txtSendAddr2" tabIndex="65" runat="server" CssClass="textField" Width="181px" MaxLength="100"></asp:textbox></td>
									<td style="WIDTH: 66px" width="66"></td>
									<td style="WIDTH: 149px" width="149"><asp:label id="lblSendFax" tabIndex="66" runat="server" CssClass="tableLabel" Width="116px">Fax</asp:label></td>
									<td width="25%"><asp:textbox id="txtSendFax" tabIndex="67" runat="server" CssClass="textField" Width="162px"></asp:textbox></td>
								</tr>
								<tr>
									<td style="WIDTH: 15px" width="15"><asp:requiredfieldvalidator id="validSendZip" Runat="server" ControlToValidate="txtSendZip">*</asp:requiredfieldvalidator></td>
									<td style="WIDTH: 169px" width="169"><asp:label id="lblSendZip" tabIndex="68" runat="server" CssClass="tableLabel" Height="21px" Width="103px">Zip Code</asp:label></td>
									<td width="15%"><asp:textbox id="txtSendZip" tabIndex="69" runat="server" CssClass="textField" Width="107px" AutoPostBack="True"></asp:textbox></td>
									<td style="WIDTH: 72px" width="72"><asp:textbox id="txtSendCity" tabIndex="70" runat="server" CssClass="textField" Width="63px" ReadOnly="True"></asp:textbox></td>
									<td style="WIDTH: 66px" width="58"><asp:textbox id="txtSendState" tabIndex="71" runat="server" CssClass="textField" Width="63px" ReadOnly="True"></asp:textbox></td>
									<td style="WIDTH: 149px" width="149"></td>
									<td width="25%"></td>
								</tr>
								<tr>
									<td style="WIDTH: 15px" width="15"></td>
									<td style="WIDTH: 169px" width="169"><asp:label id="lblSendCuttOffTime" tabIndex="72" runat="server" CssClass="tableLabel" Height="21px" Width="103px">Cut-off Time</asp:label></td>
									<td width="15%"><asp:textbox id="txtSendCuttOffTime" tabIndex="73" runat="server" CssClass="textField" Width="107px" MaxLength="5" ReadOnly="True"></asp:textbox></td>
									<td style="WIDTH: 72px" width="72"></td>
									<td style="WIDTH: 58px" width="58"></td>
									<td style="WIDTH: 149px" width="149"></td>
									<td width="25%"></td>
								</tr>
							</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td style="WIDTH: 738px; HEIGHT: 174px" align="left" width="738">
						<fieldset style="WIDTH: 739px; HEIGHT: 174px"><legend><asp:label id="lblRepInfo" CssClass="tableHeadingFieldset" Runat="server">Recipient Information</asp:label></legend>
							<table id="tblRecepInfo" border="0" runat="server" style="WIDTH: 728px; HEIGHT: 142px">
								<tr height="10%">
									<td style="HEIGHT: 10.9%" width="100%" colSpan="6"><asp:checkbox id="chkRecip" tabIndex="74" runat="server" Text="Same as the Customer Information" CssClass="normalText" Height="14px" Width="605px" Enabled="False" AutoPostBack="True"></asp:checkbox></td>
								</tr>
								<tr>
									<td width="3%"><asp:requiredfieldvalidator id="validRecipName" Width="1px" Runat="server" ControlToValidate="txtRecName">*</asp:requiredfieldvalidator></td>
									<td style="WIDTH: 160px" width="160"><asp:label id="lblRecipNm" tabIndex="75" runat="server" CssClass="tableLabel" Height="20px" Width="139px">Name</asp:label></td>
									<td style="WIDTH: 184px" width="184"><asp:textbox id="txtRecName" tabIndex="76" runat="server" CssClass="textField" Width="181px" MaxLength="100"></asp:textbox></td>
									<td style="WIDTH: 72px" width="72"><asp:button id="btnRecipNm" tabIndex="77" runat="server" Text="..." CausesValidation="False" CssClass="searchButton" Width="21px" Visible="False"></asp:button></td>
									<td width="25%"><asp:label id="lblRecpContPer" tabIndex="81" runat="server" CssClass="tableLabel" Width="118px">Contact Person</asp:label></td>
									<td width="25%"><asp:textbox id="txtRecpContPer" tabIndex="82" runat="server" CssClass="textField" Width="145px" MaxLength="100"></asp:textbox></td>
								</tr>
								<tr>
									<td width="3%"><asp:requiredfieldvalidator id="validRecipAdd1" Width="1px" Runat="server" ControlToValidate="txtRecipAddr1" ErrorMessage="Recipient Address1">*</asp:requiredfieldvalidator></td>
									<td style="WIDTH: 160px" width="160"><asp:label id="lblRecipAddr1" tabIndex="78" runat="server" CssClass="tableLabel" Height="21px" Width="93px">Address</asp:label></td>
									<td style="WIDTH: 184px" width="184"><asp:textbox id="txtRecipAddr1" tabIndex="79" runat="server" CssClass="textField" Width="180px" MaxLength="100"></asp:textbox></td>
									<td style="WIDTH: 72px" width="72"></td>
									<td width="25%"><asp:label id="lblRecipTelephone" tabIndex="83" runat="server" CssClass="tableLabel" Width="115px">Telephone</asp:label></td>
									<td width="25%"><asp:textbox id="txtRecipTel" tabIndex="84" runat="server" CssClass="textField" Width="144px"></asp:textbox></td>
								</tr>
								<tr>
									<td width="3%"></td>
									<td style="WIDTH: 160px" width="160"></td>
									<td style="WIDTH: 184px" width="184"><asp:textbox id="txtRecipAddr2" tabIndex="80" runat="server" CssClass="textField" Width="182px" MaxLength="100"></asp:textbox></td>
									<td style="WIDTH: 72px" width="72"></td>
									<td width="25%"><asp:label id="lblRecipFax" tabIndex="85" runat="server" CssClass="tableLabel" Width="119px">Fax</asp:label></td>
									<td width="25%"><asp:textbox id="txtRecipFax" tabIndex="86" runat="server" CssClass="textField" Width="144px" MaxLength="20"></asp:textbox></td>
								</tr>
								<tr>
									<td width="3%"><asp:requiredfieldvalidator id="validRecipZip" Width="3px" Runat="server" ControlToValidate="txtRecipZip">*</asp:requiredfieldvalidator></td>
									<td style="WIDTH: 160px" width="160"><asp:label id="lblRecipZip" tabIndex="87" runat="server" CssClass="tableLabel" Height="21px" Width="67px">Zip Code</asp:label></td>
									<td style="WIDTH: 184px" width="184"><asp:textbox id="txtRecipZip" tabIndex="88" runat="server" CssClass="textField" Width="109px" MaxLength="10"></asp:textbox><asp:textbox id="txtRecipCity" tabIndex="89" runat="server" CssClass="textField" Width="71px" ReadOnly="True"></asp:textbox></td>
									<td style="WIDTH: 72px" width="72"><asp:textbox id="txtRecipState" tabIndex="90" runat="server" CssClass="textField" Width="75px" ReadOnly="True"></asp:textbox></td>
									<td width="25%"></td>
									<td width="25%"></td>
								</tr>
							</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td style="WIDTH: 738px; HEIGHT: 149px" align="left" width="738">
						<fieldset style="Z-INDEX: 117; WIDTH: 740px; HEIGHT: 161px"><legend><asp:label id="lblPkgDetails" CssClass="tableHeadingFieldset" Runat="server">Package Information</asp:label></legend>
							<table id="tblPkgInfo" border="0" Runat="server" style="WIDTH: 725px; HEIGHT: 129px">
								<TR height="10%">
									<TD style="WIDTH: 1px; HEIGHT: 9.58%" width="1"></TD>
									<TD style="WIDTH: 231px; HEIGHT: 9.58%" width="231"><asp:label id="lblPkgActualWt" runat="server" CssClass="tableLabel" Height="21px" Width="113px">Actual Weight</asp:label></TD>
									<TD style="WIDTH: 138px; HEIGHT: 9.58%" width="138"><asp:textbox id="txtPkgActualWt" tabIndex="91" runat="server" CssClass="textField" Width="119px" ReadOnly="True"></asp:textbox></TD>
									<TD style="WIDTH: 21px; HEIGHT: 9.58%" width="21"></TD>
									<td style="WIDTH: 122px; HEIGHT: 9.58%" width="122"><asp:requiredfieldvalidator id="validCommCode" Runat="server" ControlToValidate="txtPkgCommCode">*</asp:requiredfieldvalidator><asp:label id="lblPkgCommCode" runat="server" CssClass="tableLabel" Height="11px" Width="100px">Commodity Code</asp:label></td>
									<TD style="WIDTH: 196px; HEIGHT: 9.58%" width="196"><asp:textbox id="txtPkgCommCode" tabIndex="95" runat="server" CssClass="textField" Width="76px" AutoPostBack="True"></asp:textbox></TD>
									<TD style="HEIGHT: 9.58%" width="35%"><asp:button id="btnPkgCommCode" tabIndex="96" runat="server" Text="..." CausesValidation="False" CssClass="searchButton" Width="21px" Visible="False"></asp:button></TD>
								</TR>
								<TR height="10%">
									<TD style="WIDTH: 1px" width="1"></TD>
									<TD style="WIDTH: 231px" width="231"><asp:label id="lblPkgDimWt" runat="server" CssClass="tableLabel" Height="21px" Width="130px">Dim Weight</asp:label></TD>
									<TD style="WIDTH: 138px" width="138"><asp:textbox id="txtPkgDimWt" tabIndex="92" runat="server" CssClass="textField" Width="118px" ReadOnly="True"></asp:textbox></TD>
									<TD style="WIDTH: 21px" width="21"></TD>
									<td style="WIDTH: 122px" width="122"><asp:button id="btnPkgDetails" tabIndex="98" runat="server" Text="Package Detail" CausesValidation="False" Height="19px" Width="112px" Visible="False"></asp:button></td>
									<TD width="35%" colSpan="2"><asp:textbox id="txtPkgCommDesc" tabIndex="97" runat="server" CssClass="textField" Width="184px" Enabled="False" ReadOnly="True"></asp:textbox></TD>
								</TR>
								<TR height="10%">
									<TD style="WIDTH: 1px; HEIGHT: 10%" width="1"></TD>
									<TD style="WIDTH: 231px; HEIGHT: 10%" width="231"><asp:label id="lblPkgTotPkgs" runat="server" CssClass="tableLabel" Height="21px" Width="130px">Total Packages</asp:label></TD>
									<TD style="WIDTH: 138px; HEIGHT: 10%" width="138"><asp:textbox id="txtPkgTotpkgs" tabIndex="93" runat="server" CssClass="textField" Width="119px" ReadOnly="True"></asp:textbox></TD>
									<TD style="WIDTH: 21px; HEIGHT: 10%" width="21"></TD>
									<td style="WIDTH: 122px; HEIGHT: 10%" width="122"></td>
									<TD style="WIDTH: 196px; HEIGHT: 10%" width="196"></TD>
									<TD style="HEIGHT: 10%" width="35%"></TD>
								</TR>
								<TR height="10%">
									<TD style="WIDTH: 4px" width="4"></TD>
									<TD style="WIDTH: 231px" width="231"><asp:label id="lblPkgChargeWt" runat="server" CssClass="tableLabel" Height="21px" Width="130px">Chargeable Weight</asp:label></TD>
									<TD style="WIDTH: 138px" width="138"><asp:textbox id="txtPkgChargeWt" tabIndex="94" runat="server" CssClass="textField" Width="119px" ReadOnly="True"></asp:textbox></TD>
									<TD style="WIDTH: 21px" width="21"></TD>
									<td style="WIDTH: 122px" width="122"></td>
									<TD style="WIDTH: 196px" width="196"></TD>
									<TD width="35%"></TD>
								</TR>
							</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td style="WIDTH: 738px; HEIGHT: 170px" align="left" width="738">
						<fieldset style="Z-INDEX: 118; WIDTH: 731px; HEIGHT: 174px"><legend><asp:label id="lblService" CssClass="tableHeadingFieldset" Runat="server">Shipment Service</asp:label></legend>
							<TABLE id="tblShipService" border="0" runat="server" style="WIDTH: 736px; HEIGHT: 142px">
								<tr height="10%">
									<td width="3%"><asp:requiredfieldvalidator id="validSrvcCode" Runat="server" ControlToValidate="txtShpSvcCode">*</asp:requiredfieldvalidator></td>
									<td width="10%"><asp:label id="lblShipSerCode" runat="server" CssClass="tableLabel" Height="20px" Width="100px">Service Code</asp:label></td>
									<td width="17%"><asp:textbox id="txtShpSvcCode" tabIndex="99" runat="server" CssClass="textField" Width="132px" MaxLength="12" AutoPostBack="True"></asp:textbox></td>
									<td width="5%"><asp:button id="btnShpSvcCode" tabIndex="100" runat="server" Text="..." CausesValidation="False" CssClass="searchButton" Width="21px" Visible="False"></asp:button></td>
									<td style="WIDTH: 115px" width="115"><asp:textbox id="txtShpSvcDesc" tabIndex="101" runat="server" CssClass="textField" Width="137px" ReadOnly="True"></asp:textbox></td>
									<td style="WIDTH: 154px" width="154"><asp:label id="lblShipPickUpTime" tabIndex="106" runat="server" CssClass="tableLabel" Height="20px" Width="133px">Pickup Dt/Time</asp:label></td>
									<td width="20%"><asp:textbox id="txtShipPckUpTime" tabIndex="107" runat="server" CssClass="textField" Width="119px" AutoPostBack="True"></asp:textbox></td>
								</tr>
								<tr height="10%">
									<td width="3%"><asp:requiredfieldvalidator id="validDclrValue" Runat="server" ControlToValidate="txtShpDclrValue">*</asp:requiredfieldvalidator></td>
									<td width="10%"><asp:label id="lblShipDclValue" runat="server" CssClass="tableLabel" Height="20px" Width="86px">Declare Value</asp:label></td>
									<td width="17%"><asp:textbox id="txtShpDclrValue" tabIndex="102" runat="server" CssClass="textField" Width="131px" AutoPostBack="True" style="TEXT-ALIGN: right"></asp:textbox></td>
									<td width="5%"></td>
									<td style="WIDTH: 115px" width="115"></td>
									<td style="WIDTH: 154px" width="154"><asp:label id="lblShipEstDlvryDt" tabIndex="108" runat="server" CssClass="tableLabel" Height="20px" Width="107px">Est. Delivery Dt</asp:label></td>
									<td width="20%"><asp:textbox id="txtShipEstDlvryDt" tabIndex="109" runat="server" CssClass="textField" Width="120px" AutoPostBack="True"></asp:textbox></td>
								</tr>
								<tr height="10%">
									<td width="3%"></td>
									<td width="10%"><asp:label id="lblShipAdpercDV" runat="server" CssClass="tableLabel" Height="20px" Width="162px">Additional % DV</asp:label></td>
									<td width="17%"><asp:textbox id="txtShpAddDV" tabIndex="103" runat="server" CssClass="textField" Width="131px" ReadOnly="True" style="TEXT-ALIGN: right"></asp:textbox></td>
									<td width="5%"></td>
									<td style="WIDTH: 115px" width="115"></td>
									<td style="WIDTH: 154px" width="154"><asp:label id="lblActualDeliveryDt" tabIndex="108" runat="server" CssClass="tableLabel" Height="20px" Width="132px">Actual Delivery Dt</asp:label></td>
									<td width="20%"><asp:textbox id="txtActualDeliveryDt" tabIndex="109" runat="server" CssClass="textField" Width="120px" AutoPostBack="True" ReadOnly="True"></asp:textbox></td>
								</tr>
								<tr height="10%">
									<td width="3%"></td>
									<td width="10%"><asp:label id="lblShipMaxCovg" runat="server" CssClass="tableLabel" Height="20px" Width="164px" DESIGNTIMEDRAGDROP="4512">Max Coverage</asp:label></td>
									<td width="17%"><asp:textbox id="txtShpMaxCvrg" tabIndex="104" runat="server" CssClass="textField" Width="131px" ReadOnly="True" style="TEXT-ALIGN: right"></asp:textbox></td>
									<td width="5%"></td>
									<td style="WIDTH: 115px" width="115"></td>
									<td style="WIDTH: 154px" width="154"></td>
									<td width="20%"></td>
								</tr>
								<tr height="10%">
									<td width="3%"></td>
									<td width="10%"><asp:label id="lblShipInsSurchrg" runat="server" CssClass="tableLabel" Height="20px" Width="141px">Insurance Surcharge</asp:label></td>
									<td width="17%"><asp:textbox id="txtShpInsSurchrg" tabIndex="105" runat="server" CssClass="textField" Width="131px" ReadOnly="True" style="TEXT-ALIGN: right"></asp:textbox></td>
									<td width="5%"></td>
									<td style="WIDTH: 115px" width="115">
										<asp:label id="lblCODAmount" runat="server" Width="130px" Height="20px" CssClass="tableLabel">COD Amount</asp:label></td>
									<td style="WIDTH: 154px" width="154">
										<asp:textbox id="txtCODAmount" tabIndex="52" runat="server" Width="131px" CssClass="textFieldRightAlign" AutoPostBack="True"></asp:textbox></td>
									<td width="20%"></td>
								</tr>
							</TABLE>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td style="WIDTH: 738px" align="left" width="738">
						<fieldset style="Z-INDEX: 122; WIDTH: 741px; HEIGHT: 102px"><legend><asp:label id="lblshipHan" CssClass="tableHeadingFieldset" Runat="server">Shipment Handling Charges</asp:label></legend>
							<TABLE id="tblShipHandling" border="0" runat="server" style="WIDTH: 732px; HEIGHT: 70px">
								<tr height="10%">
									<td style="HEIGHT: 10.45%" width="30%"><asp:checkbox id="chkshpRtnHrdCpy" tabIndex="110" runat="server" Text="Return POD Slip" CssClass="normalText" Height="16px" Width="171px" Enabled="False"></asp:checkbox></td>
									<td style="HEIGHT: 10.45%" width="10%"></td>
									<td style="HEIGHT: 10.45%" width="50%"></td>
								</tr>
								<tr height="20%">
									<td width="30%"><asp:radiobutton id="rbtnShipFrghtPre" tabIndex="111" runat="server" Text="Freight Prepaid" CssClass="normalText" Height="16px" Width="187px" Enabled="False" GroupName="Freight"></asp:radiobutton><asp:radiobutton id="rbtnShipFrghtColl" tabIndex="112" runat="server" Text="Freight Collect" CssClass="normalText" Height="16px" Width="195px" Enabled="False" GroupName="Freight"></asp:radiobutton></td>
									<td width="10%"></td>
									<td width="50%"></td>
								</tr>
							</TABLE>
						</fieldset>
					</td>
				</tr>
				<tr width="738">
					<td height="10%">
						<asp:button id="btnPopulateVAS" tabIndex="113" Text="GetQuotationVAS" CausesValidation="False" Enabled="False" Runat="server" CssClass="queryButton" Visible="False"></asp:button><asp:button id="btnBind" tabIndex="114" Text="BindGrid" CausesValidation="False" Enabled="False" Runat="server" CssClass="queryButton" Visible="False"></asp:button></td>
				</tr>
				<tr>
					<td style="WIDTH: 738px" align="left" width="738"><asp:datagrid id="dgVAS" tabIndex="115" runat="server" Height="48px" Width="722px" OnItemDataBound="dgVAS_ItemDataBound" OnItemCommand="dgVAS_Button" AutoGenerateColumns="False" OnEditCommand="dgVAS_Edit" AllowPaging="True" OnPageIndexChanged="dgVAS_PageChange" OnCancelCommand="dgVAS_Cancel" OnDeleteCommand="dgVAS_Delete" OnUpdateCommand="dgVAS_Update" PageSize="4">
							<Columns>
								<asp:ButtonColumn Text="D" ButtonType="PushButton" CommandName="Delete" Visible="False">
									<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle ForeColor="White" BackColor="#3333CC"></ItemStyle>
								</asp:ButtonColumn>
								<asp:EditCommandColumn Visible="False" ButtonType="PushButton" UpdateText="U" CancelText="C" EditText="E">
									<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle ForeColor="White" BackColor="#3333CC"></ItemStyle>
								</asp:EditCommandColumn>
								<asp:TemplateColumn HeaderText="VAS">
									<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label ID="lblVASCode" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtVASCode" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:ButtonColumn Text="..." ButtonType="PushButton" CommandName="Search" Visible="False">
									<HeaderStyle Width="3%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle ForeColor="White" BackColor="#3333CC"></ItemStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Description">
									<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label runat="server" CssClass="gridLabel" ID="lblVASDesc" Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtVASDesc" Enabled=True ReadOnly=True EnableViewState=True Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Surcharge">
									<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label runat="server" CssClass="gridLabel" ID="lblSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge")%>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBox" runat="server" ID="txtSurcharge" TextMaskType="msNumeric" MaxLength="9" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge")%>'>
										</cc1:mstextbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Remarks">
									<HeaderStyle Width="56%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle CssClass="gridField"></ItemStyle>
									<ItemTemplate>
										<asp:Label runat="server" CssClass="gridLabel" ID="lblRemarks" Text='<%#DataBinder.Eval(Container.DataItem,"remarks")%>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox CssClass="gridTextBox" runat="server" ID="txtRemarks" Text='<%#DataBinder.Eval(Container.DataItem,"remarks")%>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td style="WIDTH: 722px" align="right" width="738"><asp:button id="btnDGInsert" tabIndex="116" runat="server" Text="Insert" CausesValidation="False" CssClass="queryButton" Height="23px" Width="69px" Enabled="False" Visible="False"></asp:button></td>
				</tr>
				<TR>
					<TD style="WIDTH: 738px; HEIGHT: 6.78%" align="left" width="738">
						<TABLE id="tblCharges" style="Z-INDEX: 120" runat="server">
							<tr>
								<td style="WIDTH: 144px" width="144"><asp:label id="lblFreightChrg" tabIndex="117" runat="server" CssClass="tableLabel" Height="20px" Width="141px">Freight Charge</asp:label></td>
								<td width="20%"><asp:textbox id="txtFreightChrg" tabIndex="118" runat="server" CssClass="textField" Width="131px" ReadOnly="True" style="TEXT-ALIGN: right"></asp:textbox></td>
								<td width="60%"></td>
							</tr>
							<tr>
								<td style="WIDTH: 144px" width="144"><asp:label id="lblInsChrg" tabIndex="119" runat="server" CssClass="tableLabel" Height="20px" Width="141px">Insurance Surcharge</asp:label></td>
								<td width="20%"><asp:textbox id="txtInsChrg" tabIndex="120" runat="server" CssClass="textField" Width="131px" ReadOnly="True" style="TEXT-ALIGN: right"></asp:textbox></td>
								<td width="60%"></td>
							</tr>
							<tr>
								<td style="WIDTH: 144px" width="144"><asp:label id="lblTotVASSurChrg" tabIndex="121" runat="server" CssClass="tableLabel" Height="20px" Width="141px">Total VAS Surcharge</asp:label></td>
								<td width="20%"><asp:textbox id="txtTotVASSurChrg" tabIndex="122" runat="server" CssClass="textField" Width="131px" ReadOnly="True" style="TEXT-ALIGN: right"></asp:textbox></td>
								<td width="60%"></td>
							</tr>
							<tr>
								<td style="WIDTH: 144px" width="144"><asp:label id="lblESASurchrg" tabIndex="123" runat="server" CssClass="tableLabel" Height="20px" Width="141px">ESA Surcharge</asp:label></td>
								<td width="20%"><asp:textbox id="txtESASurchrg" tabIndex="124" runat="server" CssClass="textField" Width="131px" ReadOnly="True" style="TEXT-ALIGN: right"></asp:textbox></td>
								<td width="60%"></td>
							</tr>
							<tr>
								<td style="WIDTH: 144px" width="144"><asp:label id="lblShpTotAmt" tabIndex="125" runat="server" CssClass="tableLabel" Height="20px" Width="141px">Total Amount</asp:label></td>
								<td width="20%"><asp:textbox id="txtShpTotAmt" tabIndex="126" runat="server" CssClass="textField" Width="131px" ReadOnly="True" style="TEXT-ALIGN: right"></asp:textbox></td>
								<td width="60%"></td>
							</tr>
						</TABLE>
					</TD>
				</TR>
			</table>
			<asp:validationsummary id="PageValidationSummary" style="Z-INDEX: 115; LEFT: 415px; POSITION: absolute; TOP: 34px" runat="server" Height="21px" Width="172px" Font-Size="Smaller" ShowMessageBox="True" ShowSummary="False"></asp:validationsummary><asp:button id="btnClose" style="Z-INDEX: 116; LEFT: 3px; POSITION: absolute; TOP: 44px" runat="server" Width="90px" Text="Close" CssClass="queryButton"></asp:button></form>
	</body>
</HTML>
