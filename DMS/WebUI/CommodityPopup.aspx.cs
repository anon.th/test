using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.DAL;
using com.common.classes;
using System.Text;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CommodityPopup.
	/// </summary>
	public class CommodityPopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.DataGrid dgCommodity;
		protected System.Web.UI.WebControls.Button btnSearch;
		protected System.Web.UI.WebControls.Label lblCustID;
		protected System.Web.UI.WebControls.TextBox txtCommodityCode;
		protected System.Web.UI.WebControls.TextBox txtCommodityDescription;
		protected System.Web.UI.WebControls.Button btnRefresh;
	//	Utility utility = null;	
		private String appID = null;
		protected System.Web.UI.WebControls.Label lblCustName;
		protected System.Web.UI.WebControls.Button btnClose;
		private String enterpriseID = null;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.dgCommodity.SelectedIndexChanged += new System.EventHandler(this.dgCommodity_SelectedIndexChanged);
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion


		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			RefreshData();
			getPageControls(Page);
		}
		public void Paging(Object sender, DataGridPageChangedEventArgs e)
		{
			//runs when one of the page control is clicked
			//update the current page from the parameter values
			dgCommodity.CurrentPageIndex = e.NewPageIndex;
			//bind data to the grid
			RefreshData();
		}

		private void RefreshData()
		{
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsCommodity = null;
			
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("CommodityPopup.aspx.cs","RefreshData","CommoE001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select commodity_code,commodity_description from commodity where applicationid='");
			strQry.Append(appID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(enterpriseID);
			strQry.Append("' and commodity_code like '%");
			strQry.Append(Utility.ReplaceSingleQuote(txtCommodityCode.Text.ToString()));
			strQry.Append("%' and commodity_description like N'%");
			strQry.Append(Utility.ReplaceSingleQuote(txtCommodityDescription.Text.ToString()));
			strQry.Append("%'");
			dbCmd = dbConApp.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsCommodity =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("CustomerDetails.aspx.cs","RefreshData","CommE002","Error in the query String");
				throw appExpection;
			}
			if(dsCommodity.Tables[0].Rows.Count >0)
			{
				dgCommodity.DataSource = dsCommodity;
				dgCommodity.DataBind();
			}
		}

		

		private void dgCommodity_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			int iSelIndex = dgCommodity.SelectedIndex;
			DataGridItem dgRow = dgCommodity.Items[iSelIndex];
			String strCommodityCode = dgRow.Cells[0].Text;
			String strCommodityDesc = dgRow.Cells[1].Text;

			if(strCommodityDesc == "&nbsp;")
			{
				strCommodityDesc = "";
			}
			
			String pageType = Request.Params["FORMID"].ToString();

			if(pageType == "DomesticShipment")
			{
				String sScript = "";
				sScript += "<script language=javascript>";
				sScript += "  window.opener.DomesticShipment.txtPkgCommCode.value = \"" + strCommodityCode + "\";";
				sScript += "  window.opener.DomesticShipment.txtPkgCommDesc.value = \"" + strCommodityDesc + "\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
			else if(pageType == "ImportConsignments")
			{
				String sScript = "";
				String COMCODECLIENT = Request.Params["COMCODECLIENT"].ToString();
				sScript += "<script language=javascript>";
				sScript += "  window.opener.ImportConsignments." + COMCODECLIENT + ".value = \"" + strCommodityCode + "\";";
				sScript += "  window.close();";
				sScript += "</script>";
				Response.Write(sScript);
			}
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
	}
}
