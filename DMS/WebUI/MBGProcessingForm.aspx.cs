using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using com.ties.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.ties.BAL;
using com.common.applicationpages;
using System.Text;
using Cambro.Web.DbCombo;
using System.Xml;

namespace com.ties
{
	/// <summary>
	/// Summary description for ManualRatingOverride.
	/// </summary>
	public class MBGProcessingForm : BasePage
	{
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.WebControls.Button btnApplyMBG;
		protected System.Web.UI.WebControls.Button btnRevertMBG;
		protected System.Web.UI.WebControls.TextBox txtConsigNo;
		protected System.Web.UI.WebControls.Label lblConsgmtNo;
		protected System.Web.UI.WebControls.Label lblInvoinceNo;
		protected System.Web.UI.WebControls.Label lblInvoiceStatus;
		protected System.Web.UI.WebControls.TextBox txtInvoinceNo;
		protected System.Web.UI.WebControls.Label lblPayerID;
		protected System.Web.UI.WebControls.TextBox txtPayerID;
		protected System.Web.UI.WebControls.TextBox txtInvoiceStatus;
		protected System.Web.UI.WebControls.Label lblLastExcepCode;
		protected System.Web.UI.WebControls.TextBox txtLastExcepCode;
		protected System.Web.UI.WebControls.Label lblFrightCharge;
		protected System.Web.UI.WebControls.Label lblTotalVAS;
		protected System.Web.UI.WebControls.Label lblInsurance;
		protected System.Web.UI.WebControls.Label lblESA;
		protected System.Web.UI.WebControls.Label lblOther;
		protected System.Web.UI.WebControls.Label lblTotalRate;
		protected System.Web.UI.WebControls.Label lblMBGAmount;
		protected com.common.util.msTextBox Mstextbox1;
		protected System.Web.UI.WebControls.TextBox Textbox1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox Textbox2;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.TextBox Textbox3;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.TextBox Textbox4;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.TextBox Textbox5;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Label Label6;
		protected com.common.util.msTextBox Mstextbox2;
		protected System.Web.UI.WebControls.Label Label7;
		protected System.Web.UI.WebControls.TextBox Textbox6;
		protected System.Web.UI.WebControls.Label Label8;
		protected System.Web.UI.WebControls.TextBox Textbox7;
		protected System.Web.UI.WebControls.Label Label9;
		protected System.Web.UI.WebControls.TextBox Textbox8;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.TextBox Textbox9;
		protected System.Web.UI.WebControls.Label Label11;
		protected System.Web.UI.WebControls.TextBox Textbox10;
		protected System.Web.UI.WebControls.Label Label12;
		protected System.Web.UI.WebControls.ValidationSummary Validationsummary1;
		protected System.Web.UI.WebControls.Label Label13;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.Button Button2;
		protected System.Web.UI.WebControls.Button Button3;
		protected System.Web.UI.WebControls.Button Button4;
		protected System.Web.UI.WebControls.Button Button5;
		protected System.Web.UI.HtmlControls.HtmlGenericControl Div1;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected com.common.util.msTextBox txtFrightCharge;
		protected com.common.util.msTextBox txtTotalVAS;
		protected com.common.util.msTextBox txtInsurance;
		protected com.common.util.msTextBox txtESA;
		protected com.common.util.msTextBox txtOther;
		protected com.common.util.msTextBox txtTotalRate;
		protected com.common.util.msTextBox txtMBGAmount;
		protected System.Web.UI.WebControls.Label lblMBGStatus;
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.HtmlControls.HtmlTable TblDtl;
				
		#region Properties & Variables
			private SessionDS m_sdsMBG
			{
				get{return (SessionDS)ViewState["ViewStateData"];}
				set{ViewState["ViewStateData"]=value;}
			}
			private string m_format
			{
				get
				{
					if(ViewState["m_format"]==null)
						ViewState["m_format"]="0.00";
					return ViewState["m_format"].ToString();
				}
				set{ViewState.Add("m_format", value);}
			}
		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		private void InitializeComponent()
		{
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.btnApplyMBG.Click += new System.EventHandler(this.btnApplyMBG_Click);
			this.btnRevertMBG.Click += new System.EventHandler(this.btnRevertMBG_Click);
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
				GetEnterpriseProfile();
				ResetScreenForQuery();	

				if(Request.QueryString["PopupType"]!=null)
				{
					btnClose.Visible=true;
                    txtConsigNo.Text=Request.QueryString["Consignment"].ToString();
					ExcecuteQuery();
				}
				else
				{
					btnClose.Visible=false;
				}
					
			}
		}

		private void DisplayData()
		{
			if(m_sdsMBG.ds.Tables[0].Rows.Count>0)
			{
				DataRow dr = m_sdsMBG.ds.Tables[0].Rows[0];
				if(dr["Customer_MBG"]!=DBNull.Value)
				{
					if(!dr["Customer_MBG"].ToString().Equals("Y"))
					{
						if (dr["MBG"]!=DBNull.Value)
						{
							if (dr["MBG"].ToString().Equals("Y"))
							{
								lblMBGStatus.Text="MBG Applied";
								btnSave.Enabled=true;
								btnApplyMBG.Enabled=false;
								btnRevertMBG.Enabled=true;
								txtMBGAmount.ReadOnly=false;
							}
							else
							{
								lblMBGStatus.Text="No MBG Applied";
								btnSave.Enabled=false;
								btnApplyMBG.Enabled=true;
								btnRevertMBG.Enabled=false;
							}
						}
						else
						{
							lblMBGStatus.Text="No MBG Applied";
							btnSave.Enabled=false;
							btnApplyMBG.Enabled=true;
							btnRevertMBG.Enabled=false;
						}
					}
					else
					{
						btnSave.Enabled=false;
						btnApplyMBG.Enabled=false;
						btnRevertMBG.Enabled=false;
					}
				}
				else
				{
					if (dr["MBG"]!=DBNull.Value)
					{
						if (dr["MBG"].ToString().Equals("Y"))
						{
							lblMBGStatus.Text="MBG Applied";
							btnSave.Enabled=true;
							btnApplyMBG.Enabled=false;
							btnRevertMBG.Enabled=true;
							txtMBGAmount.ReadOnly=false;
						}
						else
						{
							lblMBGStatus.Text="No MBG Applied";
							btnSave.Enabled=false;
							btnApplyMBG.Enabled=true;
							btnRevertMBG.Enabled=false;
						}
					}
					else
					{
						lblMBGStatus.Text="No MBG Applied";
						btnSave.Enabled=false;
						btnApplyMBG.Enabled=true;
						btnRevertMBG.Enabled=false;
					}
				}

				
				txtConsigNo.Text = dr["consignment_no"].ToString();
				if(dr["invoice_no"]!=DBNull.Value)
				{
					txtInvoinceNo.Text=dr["invoice_no"].ToString();
				}
				if(dr["payerid"]!=DBNull.Value)
				{
					txtPayerID.Text=dr["payerid"].ToString();
				}
				if(dr["invoice_status"]!=DBNull.Value)
				{
					if(!dr["invoice_status"].ToString().Trim().Equals("N") && !dr["invoice_status"].ToString().Trim().Equals("C"))
					{
						btnApplyMBG.Enabled=false;
						btnSave.Enabled=false;
						btnRevertMBG.Enabled=false;
						txtMBGAmount.ReadOnly=true;
					}
					txtInvoiceStatus.Text = InvoiceManagementMgrDAL.GetCodeTextByValue(utility.GetAppID(),utility.GetUserCulture(),"invoice_status",dr["invoice_status"].ToString(),CodeValueType.StringValue);
				}

				if(dr["last_exception_code"]!=DBNull.Value)
				{
					txtLastExcepCode.Text=dr["last_exception_code"].ToString();
					if(dr["last_exception_code"].ToString().Trim().IndexOf("PODEX")<0)
					{
						btnApplyMBG.Enabled=false;
						btnSave.Enabled=false;
						btnRevertMBG.Enabled=false;
					}
				}
				else
				{
					btnApplyMBG.Enabled=false;
					btnSave.Enabled=false;
					btnRevertMBG.Enabled=false;
				}

				txtFrightCharge.Text = SurcStr(dr["tot_freight_charge"], true);
				txtTotalVAS.Text = SurcStr(dr["tot_vas_surcharge"], true);
				txtInsurance.Text = SurcStr(dr["insurance_surcharge"], true);
				txtESA.Text = SurcStr(dr["esa_surcharge"], true);
				txtTotalRate.Text = SurcStr(dr["total_rated_amount"], true);
				txtOther.Text = SurcStr(dr["other_surch_amount"], true);
				txtMBGAmount.Text = SurcStr(dr["forcembg_amount"], true);
				if(lblMBGStatus.Text.Trim().Equals("MBG Applied") && txtMBGAmount.Text.Trim().Equals(""))
				{
					txtMBGAmount.Text= SurcStr(dr["mbg_amount"], true);
				}
				
			}
		}


		private void ExcecuteQuery()
		{
			if(txtConsigNo.Text.Trim().Equals(""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CNSGMNT_NOT_FILL",utility.GetUserCulture());
				return;
			}
			else lblErrorMsg.Text = "";

			m_sdsMBG = TIESDAL.ForceMBGMgrDAL.Get(utility, txtConsigNo.Text.Trim(), 0, 1, false);
			btnExecQry.Enabled = false;
			if(m_sdsMBG.QueryResultMaxSize == 0)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CNSGMNT_NOT_FOUND",utility.GetUserCulture());
			}
			else lblErrorMsg.Text = "";
			btnExecQry.Enabled = false;
			if(m_sdsMBG != null && m_sdsMBG.DataSetRecSize > 0)
			{
				btnSave.Enabled=true;
			}
			else 
			{
				btnSave.Enabled=false;
			}
			DisplayData();
			txtConsigNo.ReadOnly=true;
			
		}

		private void ResetScreenForQuery()
		{
			txtConsigNo.ReadOnly=false;
			btnQry.Enabled=true;
			btnExecQry.Enabled = true;
			btnSave.Enabled=false;
			btnApplyMBG.Enabled=false;
			btnRevertMBG.Enabled=false;
			txtConsigNo.Text="";
			txtInvoinceNo.Text="";
			txtInvoiceStatus.Text="";
			txtFrightCharge.Text="";
			txtInsurance.Text="";
			txtOther.Text="";
			txtPayerID.Text="";
			txtLastExcepCode.Text="";
			txtTotalVAS.Text="";
			txtESA.Text="";
			txtTotalRate.Text="";
			txtMBGAmount.Text="";
			lblMBGStatus.Text="";
			lblErrorMsg.Text="";
			txtMBGAmount.ReadOnly=true;
		}

		
		private string SurcStr(object val, bool isTextBox)
		{
			string retVal = "";
			if(val!=DBNull.Value)
			{
				retVal = String.Format(m_format, Convert.ToDecimal(val));
			}
			else
			{
				retVal = isTextBox?"":String.Format(m_format, 0);
			}
			return retVal;
		}

		private void GetEnterpriseProfile()
		{
			DataSet profileDS = SysDataManager1.GetEnterpriseProfile(utility.GetAppID(), utility.GetEnterpriseID());
			if(profileDS.Tables[0].Rows.Count > 0)
			{
				DataRow row = profileDS.Tables[0].Rows[0];
				if ( row["currency_decimal"] !=DBNull.Value ) m_format = "{0:F" + row["currency_decimal"] + "}";
			}
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			ResetScreenForQuery();		
		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			ExcecuteQuery();
		}

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.opener;" ;
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnApplyMBG_Click(object sender, System.EventArgs e)
		{
			decimal decMBGAMOUNT=0;
			decimal decFreight= Convert.ToDecimal(txtFrightCharge.Text.Trim());
			decimal decEsa= Convert.ToDecimal(txtESA.Text.Trim());
			decMBGAMOUNT=Convert.ToDecimal("0");
			decMBGAMOUNT=decMBGAMOUNT-(decFreight+decEsa);
			txtMBGAmount.Text=SurcStr(decMBGAMOUNT, true);
			btnApplyMBG.Enabled=false;
			btnRevertMBG.Enabled=false;
			btnExecQry.Enabled=false;
			btnSave.Enabled=true;
			txtMBGAmount.ReadOnly=false;
			lblErrorMsg.Text="";
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			if (txtMBGAmount.Text.Trim().Equals(""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MBG_AMOUNT_REQ",utility.GetUserCulture());
				return;
			}

			try
			{
				decimal d = Convert.ToDecimal(txtMBGAmount.Text.Trim());
			}
			catch(Exception ex)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MBG_AMOUNT_CH",utility.GetUserCulture());
				return;
			}


			decimal decFreight= Convert.ToDecimal(txtFrightCharge.Text.Trim());
			decimal decEsa= Convert.ToDecimal(txtESA.Text.Trim());
			decimal decMBGAMOUNT=Convert.ToDecimal(txtMBGAmount.Text);
			if(decMBGAMOUNT>=0 || decMBGAMOUNT < (0-(decFreight+decEsa)))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MBG_VLD_AMOUNT",utility.GetUserCulture());
				return;
			}
			bool IsUpdate=false;
			IsUpdate = Convert.ToBoolean(TIESDAL.ForceMBGMgrDAL.ApplyMBG(utility,txtConsigNo.Text.Trim(),txtInvoinceNo.Text.Trim(), decMBGAMOUNT,"Y","Applied MBG"));
			if(IsUpdate)
			{
				lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
				lblMBGStatus.Text="MBG Applied";
				btnExecQry.Enabled=false;
				btnSave.Enabled=false;
				btnApplyMBG.Enabled=false;
				btnRevertMBG.Enabled=false;
			}
			else
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_UPD",utility.GetUserCulture());
			}
		}

		private void btnRevertMBG_Click(object sender, System.EventArgs e)
		{
			if (txtMBGAmount.Text.Trim().Equals(""))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MBG_AMOUNT_REQ",utility.GetUserCulture());
				return;
			}

			try
			{
				decimal d = Convert.ToDecimal(txtMBGAmount.Text.Trim());
			}
			catch(Exception ex)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MBG_AMOUNT_CH",utility.GetUserCulture());
				return;
			}


			decimal decMBGAMOUNT=Convert.ToDecimal(txtMBGAmount.Text);
			bool IsUpdate=false;
			IsUpdate = Convert.ToBoolean(TIESDAL.ForceMBGMgrDAL.RevertMBG(utility,txtConsigNo.Text.Trim(),txtInvoinceNo.Text.Trim(), decMBGAMOUNT,"N"));
			if(IsUpdate)
			{
				lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());
				lblMBGStatus.Text="No MBG Applied";
				btnExecQry.Enabled=false;
				btnSave.Enabled=false;
				btnApplyMBG.Enabled=false;
				btnRevertMBG.Enabled=false;
				txtMBGAmount.Text=SurcStr(0.00, true);
			}
			else
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_UPD",utility.GetUserCulture());
			}
		}
		

		#region Events of control
		
		#endregion
		
	}

}

