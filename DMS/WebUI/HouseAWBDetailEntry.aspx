<%@ Page language="c#" Codebehind="HouseAWBDetailEntry.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.HouseAWBDetailEntry" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>House AWB Detail Entry</title>
		<meta name="vs_snapToGrid" content="True">
		<meta name="vs_showGrid" content="True">
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/JScript_TextControl.js"></script>
		<script language="javascript" type="text/javascript">
			function funcDisBack(){
				history.go(+1);
			}

			function RemoveBadNonNumber(strTemp, obj) {
				strTemp = strTemp.replace(/[^0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			function AfterPasteNonNumber(obj) {
				setTimeout(function () {
					RemoveBadNonNumber(obj.value, obj);
				}, 1); //or 4
			}
			
			function RemoveBadPaseNumberwhithDot(strTemp, obj) {
				strTemp = strTemp.replace(/[^.0-9]/g, ''); //replace non-numeric
				obj.value = strTemp.replace(/\s/g, ''); // replace space
			}
			
			function AfterPasteNumberwhithDot(obj) {
						setTimeout(function () {
							RemoveBadPaseNumberwhithDot(obj.value, obj);
						}, 1); //or 4
					}
			
			
			 function callback()
			{
					var btn = document.getElementById('btnClientEvent');
					if(btn != null)
					{					
						btn.click();
					}else{
						alert('error call code behind');
					}				        
				
			}
			function validateCode(key) { 
            var keycode = (key.which) ? key.which : key.keyCode; 
            if (!(keycode == 8 || keycode == 46) && (keycode < 48 || keycode > 57)) {
                return false;
            }
             
			}
			
			function isDecimalValidKey(textboxIn, evt)
			{
				var charCode = (evt.which) ? evt.which : event.keyCode;    

				if (charCode > 31 && (charCode < 45 || charCode == 47 || charCode > 57))
				{
				return false;
				}     

				if(charCode == 46 && textboxIn.value.indexOf(".") != -1)
				{
					return false;
				}   
				return true;
			}
			
			function ExporterFocus(obj)
			{
				var exporter = document.getElementById(obj);	
				exporter.focus();	
				
			}
			
			function CustomerIDTextChanged()
			{

			}
			
			function ExporterNameOnBlur()
			{
				var expName = document.getElementById('ExporterName');			
				var expAddr1 = document.getElementById('ExporterAddress1');	
				if(expName.value != "" && expAddr1.value =="")
				{
					expAddr1.value =expName.value;
					expAddr1.select();
				}
			}
			
			function ConsigneeNameOnBlur()
			{
				var expName = document.getElementById('ConsigneeName');			
				var expAddr1 = document.getElementById('ConsigneeAddress1');	
				if(expName.value != "" && expAddr1.value =="")
				{
					expAddr1.value =expName.value;
					expAddr1.select();
				}			
			}
		</script>
</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="HouseAWBDetailEntry" method="post" runat="server">
			<div style="POSITION: relative; WIDTH: 860px" id="divMain" MS_POSITIONING="GridLayout"
				runat="server">
				<TABLE style="POSITION: absolute" id="MainTable" border="0" width="100%" runat="server">
					<tr>
						<td colSpan="2" align="left"><asp:label id="Label1" runat="server" Height="27px" CssClass="mainTitleSize">
							House AWB Detail Entry</asp:label></td>
					</tr>
					<tr>
						<td style="HEIGHT: 26px" vAlign="top" colSpan="2" align="left">
							<asp:button id="btnSave" runat="server" CssClass="queryButton" Text="Save" CausesValidation="False"></asp:button>
							<asp:button id="btnClose" runat="server" CssClass="queryButton" Text="Close" CausesValidation="False"></asp:button>
						</td>
					</tr>
					<tr>
						<td vAlign="top" colSpan="2" align="left">
							<table border="0" cellSpacing="1" cellPadding="1" width="850">
								<TR>
									<TD style="HEIGHT: 19px" colSpan="4"><asp:label id="lblError" runat="server" Font-Size="X-Small" Font-Bold="True" ForeColor="Red"></asp:label></TD>
								</TR>
								<tr>
									<td style="WIDTH: 190px"><asp:label id="Label4" runat="server" CssClass="tableLabel"> Master AWB Number:</asp:label></td>
									<td style="WIDTH: 235px"><FONT face="Tahoma">
											<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="MAWBNumber"
												runat="server" CssClass="textField" Width="225px" Enabled="False" MaxLength="100" ReadOnly="True">231-2311123</asp:textbox></FONT></td>
									<td colSpan="2" rowspan="8" align="right" valign="top" width="425">
										<fieldset style="WIDTH: 400px" id="fsOtherData" runat="server"><legend style="COLOR: black">Other 
												Data Captured on Import</legend>
											<table style="MARGIN: 2px" width="395">
												<tr>
													<td width="100"><asp:label style="Z-INDEX: 0" id="Label39" runat="server" CssClass="tableLabel">Assigned Pieces:</asp:label></td>
													<td><FONT face="Tahoma"><asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="ImportedAssignedPieces" tabIndex="14"
																runat="server" CssClass="textField" Width="80px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox></FONT></td>
													<TD>
														<asp:label style="Z-INDEX: 0" id="Label20" runat="server" CssClass="tableLabel"> GrossWeight:</asp:label></TD>
													<TD>
														<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="ImportedActualGrossWt" tabIndex="14"
															runat="server" CssClass="textField" Width="80px" ReadOnly="True" Enabled="False"></asp:textbox></TD>
												</tr>
												<tr>
													<td><asp:label style="Z-INDEX: 0" id="Label40" runat="server" CssClass="tableLabel"> Volume:</asp:label></td>
													<td><asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="ImportedVolume" tabIndex="14"
															runat="server" CssClass="textField" Width="80px" Enabled="False" ReadOnly="True"></asp:textbox></td>
													<TD>
														<asp:label style="Z-INDEX: 0" id="Label26" runat="server" CssClass="tableLabel">Terms Of Payment:</asp:label></TD>
													<TD><FONT face="Tahoma">
															<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="ImportedTermsOfPayment" tabIndex="14"
																runat="server" CssClass="textField" Width="80px" ReadOnly="True" Enabled="False"></asp:textbox></FONT></TD>
												</tr>
												<TR>
													<TD>
														<asp:label style="Z-INDEX: 0" id="Label28" runat="server" CssClass="tableLabel">Origin Depot:</asp:label></TD>
													<TD>
														<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="ImportedOriginDepot" tabIndex="14"
															runat="server" CssClass="textField" Width="80px" ReadOnly="True" Enabled="False"></asp:textbox></TD>
													<TD>
														<asp:label style="Z-INDEX: 0" id="Label29" runat="server" CssClass="tableLabel">Destination Depot:</asp:label></TD>
													<TD><FONT face="Tahoma">
															<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="ImportedDestinationDepot" tabIndex="14"
																runat="server" CssClass="textField" Width="80px" ReadOnly="True" Enabled="False"></asp:textbox></FONT></TD>
												</TR>
												<TR>
													<TD>
														<asp:label style="Z-INDEX: 0" id="Label31" runat="server" CssClass="tableLabel">Tariff ID:</asp:label></TD>
													<TD>
														<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="ImportedTariffID" tabIndex="14"
															runat="server" CssClass="textField" Width="80px" ReadOnly="True" Enabled="False"></asp:textbox></TD>
													<TD>
														<asp:label style="Z-INDEX: 0" id="Label33" runat="server" CssClass="tableLabel">Invoice Value:</asp:label></TD>
													<TD><FONT face="Tahoma">
															<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="ImportedInvValue" tabIndex="14"
																runat="server" CssClass="textField" Width="80px" ReadOnly="True" Enabled="False"></asp:textbox></FONT></TD>
												</TR>
												<TR>
													<TD>
														<asp:label style="Z-INDEX: 0" id="Label34" runat="server" CssClass="tableLabel">Division:</asp:label></TD>
													<TD>
														<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="ImportedDivision" tabIndex="14"
															runat="server" CssClass="textField" Width="80px" ReadOnly="True" Enabled="False"></asp:textbox></TD>
													<TD>
														<asp:label style="Z-INDEX: 0" id="Label35" runat="server" CssClass="tableLabel">Product:</asp:label></TD>
													<TD>
														<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="ImportedProduct" tabIndex="14"
															runat="server" CssClass="textField" Width="80px" ReadOnly="True" Enabled="False"></asp:textbox></TD>
												</TR>
												<TR>
													<TD>
														<asp:label style="Z-INDEX: 0" id="Label36" runat="server" CssClass="tableLabel">Item Quantity:</asp:label></TD>
													<TD>
														<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="ImportedItemQuantity" tabIndex="14"
															runat="server" CssClass="textField" Width="80px" ReadOnly="True" Enabled="False"></asp:textbox></TD>
													<TD>
														<asp:label style="Z-INDEX: 0" id="Label37" runat="server" CssClass="tableLabel">Value Indicator:</asp:label></TD>
													<TD>
														<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="ImportedValueIndicator" tabIndex="14"
															runat="server" CssClass="textField" Width="80px" ReadOnly="True" Enabled="False"></asp:textbox></TD>
												</TR>
												<TR>
													<TD>
														<asp:label style="Z-INDEX: 0" id="Label41" runat="server" CssClass="tableLabel">Unit ID:</asp:label></TD>
													<TD>
														<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="ImportedUnitID" tabIndex="14"
															runat="server" CssClass="textField" Width="80px" ReadOnly="True" Enabled="False"></asp:textbox></TD>
													<TD></TD>
													<TD></TD>
												</TR>
											</table>
										</fieldset>
									</td>
								</tr>
								<TR>
									<TD>
										<asp:label style="Z-INDEX: 0" id="Label7" runat="server" CssClass="tableLabel">House AWB Number: </asp:label>
										<asp:label style="Z-INDEX: 0" id="Label18" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
									<TD><FONT face="Tahoma">
											<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="consignment_no" tabIndex="1" runat="server"
												CssClass="textField" Width="225px" MaxLength="100" AutoPostBack="True"></asp:textbox></FONT></TD>
								</TR>
								<TR>
									<TD>
										<asp:label style="Z-INDEX: 0" id="Label14" runat="server" CssClass="tableLabel">Quantity of Kind:</asp:label>
										<asp:label style="Z-INDEX: 0" id="Label22" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
									<TD><FONT face="Tahoma">
											<asp:textbox style="Z-INDEX: 0; TEXT-ALIGN: right" id="QuantityOfKind" tabIndex="2" runat="server"
												CssClass="textField" Width="50px" onkeypress="return validateCode(event)" onpaste="AfterPasteNumberwhithDot(this)"
												MaxLength="3"></asp:textbox>
											<asp:label style="Z-INDEX: 0" id="Label15" runat="server" CssClass="tableLabel"> Weight (kg):</asp:label>
											<asp:label style="Z-INDEX: 0" id="Label24" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
											<cc1:mstextbox style="Z-INDEX: 0" id="WeightKG" tabIndex="3" onpaste="AfterPasteNonNumber(this)"
												onkeyup="WeightKeyPress();" onkeypress="WeightKeyPress();" runat="server" CssClass="textField"
												Width="80px" MaxLength="14" TextMaskType="msNumericCOD" NumberPrecision="8" NumberMaxValueCOD="999999.9"
												NumberScale="1"></cc1:mstextbox>
										</FONT>
									</TD>
								</TR>
								<TR>
									<TD colspan="2" style="HEIGHT: 22px">
										<asp:label style="Z-INDEX: 0" id="Label10" runat="server" CssClass="tableLabel">Exporter:</asp:label>
										<asp:CheckBox style="Z-INDEX: 0" id="cbkSameShipper" tabIndex="4" runat="server" CssClass="tableRadioButton"
											Text="Same as consolidation shipper" AutoPostBack="True"></asp:CheckBox></TD>
								</TR>
								<TR>
									<TD colspan="2">
										<asp:label style="POSITION: absolute" id="Label27" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
										<asp:textbox style="TEXT-TRANSFORM: uppercase;MARGIN-LEFT: 10px" id="ExporterName" tabIndex="5"
											runat="server" CssClass="textField" Width="400px" onblur="ExporterNameOnBlur();" MaxLength="100"></asp:textbox></TD>
								</TR>
								<TR>
									<TD colspan="2">
										<asp:label style="POSITION: absolute" id="Label23" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; MARGIN-LEFT: 10px" id="ExporterAddress1"
											tabIndex="6" runat="server" CssClass="textField" Width="400px" MaxLength="100"></asp:textbox></TD>
								</TR>
								<TR>
									<TD colSpan="2">
										<asp:textbox style="Z-INDEX: 0;TEXT-TRANSFORM: uppercase;MARGIN-LEFT: 10px" id="ExporterAddress2"
											tabIndex="7" runat="server" CssClass="textField" Width="400px" MaxLength="100"></asp:textbox></TD>
								</TR>
								<TR>
									<TD>
										<asp:label style="Z-INDEX: 0" id="Label11" runat="server" CssClass="tableLabel">Country:</asp:label>
										<asp:label style="Z-INDEX: 0" id="Label25" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
										<asp:dropdownlist style="Z-INDEX: 0" id="ExporterCountry" tabIndex="8" runat="server" Width="100px"
											AutoPostBack="True">
											<asp:ListItem Selected="True"></asp:ListItem>
											<asp:ListItem Value="AU">AU</asp:ListItem>
										</asp:dropdownlist></TD>
									<TD>
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="ExporterCountry_Desc"
											tabIndex="9" runat="server" CssClass="textField" Width="220px" ReadOnly="True" MaxLength="100"
											Enabled="False"></asp:textbox></TD>
								</TR>
								<TR>
									<TD>
										<asp:label style="Z-INDEX: 0" id="Label3" runat="server" CssClass="tableLabel">Consignee (Customer ID): </asp:label>
										<asp:label style="Z-INDEX: 0" id="Label17" runat="server" ForeColor="Red" CssClass="tableLabel">*</asp:label></TD>
									<TD colSpan="3">
										<asp:textbox style="Z-INDEX: 0;TEXT-TRANSFORM: uppercase" id="CustomerID" tabIndex="10" runat="server"
											CssClass="textField" Width="100px" MaxLength="20" Enabled="False" AutoPostBack="True"></asp:textbox>
										<asp:button style="Z-INDEX: 0" id="btnDisplayCustDtls" runat="server" CssClass="searchButton"
											Height="19px" Width="21px" CausesValidation="False" Text="..." tabIndex="11"></asp:button>
										<asp:CheckBox style="Z-INDEX: 0" id="cbkOverrideConsignee" tabIndex="12" runat="server" CssClass="tableRadioButton"
											Text="Override consignee details" AutoPostBack="True"></asp:CheckBox></TD>
								</TR>
								<TR>
									<TD>
										<asp:label style="Z-INDEX: 0" id="Label6" runat="server" CssClass="tableLabel">Customer�s Tax Code: </asp:label></TD>
									<TD colSpan="3">
										<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="CustomerTaxCode" tabIndex="13"
											runat="server" CssClass="textField" Width="100px" Enabled="False" ReadOnly="True" MaxLength="100"></asp:textbox></TD>
								</TR>
								<TR>
									<TD><asp:label style="Z-INDEX: 0" id="Label9" runat="server" CssClass="tableLabel">Consignee�s Details:</asp:label></TD>
									<TD><FONT face="Tahoma"></FONT></TD>
									<TD colSpan="2"><FONT face="Tahoma">
											<asp:label style="Z-INDEX: 0" id="Label21" runat="server" CssClass="tableLabel">Notify:</asp:label>
											<asp:CheckBox style="Z-INDEX: 0" id="cbkSameConsignee" tabIndex="27" runat="server" CssClass="tableRadioButton"
												Text="Same as Consignee" AutoPostBack="True"></asp:CheckBox></FONT>
									</TD>
								</TR>
								<tr>
									<td colspan="2">
										<asp:label style="POSITION: absolute" id="lblRegConsigneeName" runat="server" CssClass="tableLabel"
											ForeColor="Red">*</asp:label>
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; MARGIN-LEFT: 10px" id="ConsigneeName"
											tabIndex="14" runat="server" CssClass="textField" Width="400px" onblur="ConsigneeNameOnBlur();"
											Enabled="False" MaxLength="100"></asp:textbox></td>
									<TD colSpan="2">
										<asp:textbox style="Z-INDEX: 0;TEXT-TRANSFORM: uppercase" id="NotifyName" tabIndex="28" runat="server"
											CssClass="textField" Width="400px" MaxLength="100"></asp:textbox></TD>
								</tr>
								<tr>
									<td colspan="2">
										<asp:label style="POSITION: absolute" id="lblRegConsigneeAddress1" runat="server" CssClass="tableLabel"
											ForeColor="Red">*</asp:label>
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; MARGIN-LEFT: 10px" id="ConsigneeAddress1"
											tabIndex="15" runat="server" CssClass="textField" Width="400px" Enabled="False" MaxLength="100"></asp:textbox></td>
									<TD colSpan="2">
										<asp:textbox style="Z-INDEX: 0;TEXT-TRANSFORM: uppercase" id="NotifyAddress1" tabIndex="29" runat="server"
											CssClass="textField" Width="400px" MaxLength="100"></asp:textbox></TD>
								</tr>
								<tr>
									<td colspan="2">
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; MARGIN-LEFT: 10px" id="ConsigneeAddress2"
											tabIndex="16" runat="server" CssClass="textField" Width="400px" Enabled="False" MaxLength="100"></asp:textbox></td>
									<TD colSpan="2">
										<asp:textbox style="Z-INDEX: 0;TEXT-TRANSFORM: uppercase" id="NotifyAddress2" tabIndex="30" runat="server"
											CssClass="textField" Width="400px" MaxLength="100"></asp:textbox></TD>
								</tr>
								<tr>
									<td colspan="2">
										<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea; MARGIN-LEFT: 10px" id="ConsigneeZipcode"
											tabIndex="17" runat="server" CssClass="textField" Width="100px" Enabled="False" MaxLength="100"
											ReadOnly="True"></asp:textbox>
										<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="ConsigneeCountry" tabIndex="18"
											runat="server" CssClass="textField" Width="230px" Enabled="False" MaxLength="100" ReadOnly="True"></asp:textbox>
										<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="TempConsigneeCountry" tabIndex="6"
											runat="server" CssClass="textField" Width="250px" Enabled="False" MaxLength="100" Visible="False"
											ReadOnly="True"></asp:textbox>
									</td>
									<TD style="WIDTH: 150px;HEIGHT: 21px">
										<asp:label style="Z-INDEX: 0" id="Label19" runat="server" CssClass="tableLabel">Country:</asp:label>
										<asp:dropdownlist style="Z-INDEX: 0" id="NotifyCountry" tabIndex="31" runat="server" Width="80px"
											AutoPostBack="True">
											<asp:ListItem Selected="True"></asp:ListItem>
											<asp:ListItem Value="AU">AU</asp:ListItem>
										</asp:dropdownlist></FONT></TD>
									<TD>
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase; BACKGROUND-COLOR: #eaeaea" id="NotifyCountry_Desc"
											tabIndex="32" runat="server" CssClass="textField" Width="200px" ReadOnly="True" MaxLength="100"
											Enabled="False"></asp:textbox></TD>
								</tr>
								<tr>
									<td colspan="2">
										<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea; MARGIN-LEFT: 10px" id="ConsigneeTelephone"
											tabIndex="19" runat="server" CssClass="textField" Width="200px" Enabled="False" MaxLength="100"
											ReadOnly="True"></asp:textbox></td>
									<TD colSpan="2"></TD>
								</tr>
								<TR>
									<TD align="right">
										<asp:label style="Z-INDEX: 0" id="Label5" runat="server" CssClass="tableLabel">Goods Description:</asp:label>
										<asp:label style="Z-INDEX: 0" id="Label8" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
									<TD colSpan="3">
										<asp:textbox style="Z-INDEX: 0; TEXT-TRANSFORM: uppercase" id="GoodsDescription" tabIndex="20"
											runat="server" CssClass="textField" Width="320px" MaxLength="100"></asp:textbox></TD>
								</TR>
								<TR>
									<TD align="right">
										<asp:label style="Z-INDEX: 0" id="Label2" runat="server" CssClass="tableLabel">Location:</asp:label>
										<asp:label style="Z-INDEX: 0" id="Label12" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label>
									</TD>
									<TD colSpan="3">
										<asp:dropdownlist style="Z-INDEX: 0" id="CustomsShed_Location" tabIndex="21" runat="server" Width="80px"
											AutoPostBack="True">
											<asp:ListItem Selected="True"></asp:ListItem>
											<asp:ListItem Value="AU">AU</asp:ListItem>
										</asp:dropdownlist>
										<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="LocationCustomsShed_Desc" tabIndex="22"
											runat="server" CssClass="textField" Width="230px" ReadOnly="True" MaxLength="100" Enabled="False"></asp:textbox></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 21px" align="right">
										<asp:label style="Z-INDEX: 0" id="Label13" runat="server" CssClass="tableLabel">Kind:</asp:label>
										<asp:label style="Z-INDEX: 0" id="Label30" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
									<TD style="WIDTH: 235px; HEIGHT: 21px"><FONT face="Tahoma">
											<asp:dropdownlist style="Z-INDEX: 0" id="Kind_UOM" tabIndex="23" runat="server" Width="80px" AutoPostBack="True">
												<asp:ListItem Value="BX " Selected="True">BX </asp:ListItem>
											</asp:dropdownlist>
											<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="Kind_UOM_Desc" tabIndex="24" runat="server"
												CssClass="textField" Width="120px" ReadOnly="True" MaxLength="100" Enabled="False">BOX</asp:textbox></FONT></TD>
									<TD style="HEIGHT: 21px" colSpan="2"><FONT face="Tahoma"></FONT></TD>
								</TR>
								<TR>
									<TD align="right"><FONT face="Tahoma">
											<asp:label style="Z-INDEX: 0" id="Label16" runat="server" CssClass="tableLabel"> Nature:</asp:label></FONT>
										<asp:label style="Z-INDEX: 0" id="Label32" runat="server" CssClass="tableLabel" ForeColor="Red">*</asp:label></TD>
									<TD style="WIDTH: 235px">
										<asp:dropdownlist style="Z-INDEX: 0" id="Nature" tabIndex="25" runat="server" Width="80px" AutoPostBack="True">
											<asp:ListItem Selected="True"></asp:ListItem>
											<asp:ListItem Value="AU">AU</asp:ListItem>
										</asp:dropdownlist>
										<asp:textbox style="Z-INDEX: 0; BACKGROUND-COLOR: #eaeaea" id="Nature_Desc" tabIndex="26" runat="server"
											CssClass="textField" Width="120px" ReadOnly="True" MaxLength="100" Enabled="False"></asp:textbox></TD>
									<TD colSpan="2"></TD>
								</TR>
								<TR>
									<TD align="right">
										<FONT face="Tahoma">
											<asp:label style="Z-INDEX: 0" id="Label38" runat="server" CssClass="tableLabel"> Gross Mass:</asp:label>
										</FONT>
									</TD>
									<TD style="WIDTH: 235px">
										<asp:textbox style="Z-INDEX: 0; TEXT-ALIGN: right" id="Gross_Mass" tabIndex="26" runat="server"
												CssClass="textField" Width="150px" onkeypress="return validateCode(event)" onpaste="AfterPasteNumberwhithDot(this)"
												MaxLength="18"></asp:textbox>										
									</TD>
									<TD colSpan="2"></TD>
								</TR>
								<TR>
									<TD align="right">
										<FONT face="Tahoma">
											<asp:label style="Z-INDEX: 0" id="Label42" runat="server" CssClass="tableLabel"> Shipping Marks:</asp:label>
										</FONT>
									</TD>
									<TD style="WIDTH: 235px">
										<asp:textbox style="Z-INDEX: 107" id="Shipping_Marks" tabIndex="27" runat="server" CssClass="textField"
											Width="372px" MaxLength="1000" AutoPostBack="True" Height="50px" TextMode="MultiLine"></asp:textbox>
									</TD>
									<TD colSpan="2"></TD>
								</TR>
							</table>
						</td>
					</tr>
				</TABLE>
			</div>
		</form>
	</body>
</HTML>
