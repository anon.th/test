using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.common.applicationpages;
using com.ties.DAL;
using com.common.RBAC;
using com.ties.classes;
using TIESClasses;
using TIESDAL;
using System.Text;
using System.Configuration;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using com.common.DAL;
//using Cambro.Web.DbCombo;
using System.Globalization;

namespace TIES.WebUI.Manifest
{
	/// <summary>
	/// Summary description for Quick_POD.
	/// </summary>
	public class Quick_POD : BasePage
	{
		#region Web Form Designer generated code

		protected System.Web.UI.WebControls.Label Header1;
		protected System.Web.UI.WebControls.Label lblErrrMsg;
		protected System.Web.UI.WebControls.ValidationSummary reqSummary;
		protected System.Web.UI.WebControls.Label lblColConsignment;
		protected System.Web.UI.WebControls.Label lblColService;
		protected System.Web.UI.WebControls.Label lblColDestination;
		protected System.Web.UI.WebControls.Label lblColPkgs;
		protected System.Web.UI.WebControls.Label lblColDataTime;
		protected System.Web.UI.WebControls.ListBox lbSIPCon;
		protected System.Web.UI.HtmlControls.HtmlInputButton hddConNo;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hidHost;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.HtmlControls.HtmlGenericControl fsCreateSIP;
		protected System.Web.UI.WebControls.Label lblColPOD;
		protected System.Web.UI.WebControls.Label lblColUser;
		protected System.Web.UI.WebControls.Label lblConNo;
		protected com.common.util.msTextBox txtConNo;
		protected System.Web.UI.WebControls.Label lblOriginDC;
		protected System.Web.UI.WebControls.Label lblDestPostCode;
		protected com.common.util.msTextBox txtDestPostCode;
		protected System.Web.UI.WebControls.Label lblProvince;
		protected System.Web.UI.WebControls.Label lbltmpDestDC;
		protected System.Web.UI.WebControls.Label lblServiceType;
		protected System.Web.UI.WebControls.DropDownList ddlServiceType;
		protected System.Web.UI.WebControls.Label lblNumOfPackages;
		protected com.common.util.msTextBox txtPkgNum;
		protected System.Web.UI.WebControls.Label lblPOD_Detail;
		protected System.Web.UI.WebControls.Label lblActual_Datetine;
		protected com.common.util.msTextBox txtActual;
		protected System.Web.UI.WebControls.Label lblRemark;
		protected System.Web.UI.WebControls.TextBox txtRemark;
		protected System.Web.UI.WebControls.Label lblConsignee;
		protected System.Web.UI.WebControls.TextBox txtConsignee;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hidPort;
		//protected Cambro.Web.DbCombo.DbCombo DbComboOriginDC;
		protected System.Web.UI.WebControls.DropDownList ddlOriginDC;

		String m_strAppID=null;
		String m_strEnterpriseID=null;
		String m_strCulture=null;

		String userID=null;
		int diff;

		protected System.Web.UI.HtmlControls.HtmlInputText txtConNo_;
		//protected System.Web.UI.HtmlControls.HtmlInputButton hddConNo;
		public int CharBarcodeCutStartIndex = Convert.ToInt32(ConfigurationSettings.AppSettings["32CharBarcodeCutStartIndex"]);
		protected System.Web.UI.WebControls.TextBox tbTempText;
		protected System.Web.UI.WebControls.Label Label1;
		//protected System.Web.UI.WebControls.DropDownList ddlOrigin;
		
		//protected System.Web.UI.WebControls.ddlOriginDC;
		public int CharBarcodeCutLength= Convert.ToInt32(ConfigurationSettings.AppSettings["32CharBarcodeCutLength"]);		

		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.hddConNo.ServerClick += new System.EventHandler(this.hddConNo_ServerClick);
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.txtConNo.TextChanged += new System.EventHandler(this.txtConNo_TextChanged);
			this.ddlOriginDC.SelectedIndexChanged += new System.EventHandler(this.ddlOriginDC_SelectedIndexChanged);
			this.txtDestPostCode.TextChanged += new System.EventHandler(this.txtDestPostCode_TextChanged);
			this.ddlServiceType.SelectedIndexChanged += new System.EventHandler(this.ddlServiceType_SelectedIndexChanged);
			//this.txtPkgNum.TextChanged += new System.EventHandler(this.txtPkgNum_TextChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}

		#endregion

		#region Event Method
		
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
	
			getSessionTimeOut(true);

			txtConNo.Attributes.Add("onblur","return suppressOnBluer(this);");
			//txtConNo.Attributes.Add("onkeyDown", "return suppressNonEng(event,this);");

			txtConNo.Attributes.Add("onpaste", "return true;");
			txtConNo.Attributes.Add("onkeyUp", "KeyDownHandler();");

			//txtPkgNum.Attributes.Add("onkeyUp", "return IsNumeric(this.value);");
			txtPkgNum.Attributes.Add("onpaste", "return false;");

			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			userID = utility.GetUserID();

			//DbComboOriginDC.ClientOnSelectFunction="makeUppercase('DbComboOriginDC:ulbTextBox');";
			//DbComboOriginDC.RegistrationKey = System.Configuration.ConfigurationSettings.AppSettings["DbComboRegKey"];
			
			//btnQry.Enabled = true;
			btnExecQry.Enabled = true;

			if(!Page.IsPostBack)
			{
				Session["SAVE_EVENT"] = "FALSE";
				btnSave.Enabled = false;
				//Check role
				if(!checkRole())
				{
					fsCreateSIP.Style.Add("Z-INDEX","105");
					fsCreateSIP.Style.Add("POSITION","absolute");
					fsCreateSIP.Style.Add("TOP","110px");
				}
	
				com.common.classes.User user =  RBACManager.GetUser(m_strAppID,m_strEnterpriseID, (String)Session["userID"]);				
				setFocusCtrl(txtConNo.ClientID);

				//DbComboOriginDC.Text="";
				//DbComboOriginDC.Value="";

				//ddlOriginDC.SelectedValue = "";

				//**Bind data to Origin DC dropdownlist**\\
				bindDataToDropdownOrigicDC();
			}			
		}
		
		private void btnQry_Click(object sender, System.EventArgs e)
		{
			ClearCriteria();
			btnSave.Enabled = false;
		}

		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			FieldData();
		}

		private void hddConNo_ServerClick(object sender, System.EventArgs e)
		{			
			FieldData();						
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{
			DataSet dsSavePOD = new DataSet();
			string tmp = tbTempText.Text;

			// Validate **Required** field 
			if(validateRequiredField() == true)
			{
				//Save data 
				dsSavePOD = QuickPODMgrDAL.SaveQuickPOD(m_strAppID, 
														m_strEnterpriseID, 
														userID, 
														txtConNo.Text, 
														ddlOriginDC.SelectedValue.ToString(), 
														txtDestPostCode.Text, 
														ddlServiceType.SelectedValue, 
														txtPkgNum.Text, 
														txtActual.Text, 
														txtConsignee.Text, 
														txtRemark.Text);

				if(dsSavePOD.Tables[0].Rows[0]["return_code"].ToString() == "0")
				{
					lblErrrMsg.Text = "POD Saved Successfully.";
					//Generate data to PODs listbox after complete saving 
					generateDataToListbox();
					txtConNo.Text = "";
					txtConNo.Enabled = true;
					txtConNo.ReadOnly = false;
					//ddlOriginDC.SelectedItem.Text="0";
					ddlOriginDC.SelectedValue="";
					txtDestPostCode.Text ="";
					//ddlServiceType.SelectedValue ="" ;
					ddlServiceType.Items.Clear();
					txtPkgNum.Text = "";
					txtActual.Text ="";
					txtConsignee.Text = "";
					txtRemark.Text = "";
					lblProvince.Text ="";
					setFocusCtrl(txtConNo.ClientID);
				}
				else
				{
					lblErrrMsg.Text = Convert.ToString(dsSavePOD.Tables[0].Rows[0]["errormsg"]);
				}
			}			
		}

//		[Cambro.Web.DbCombo.ResultsMethodAttribute(true)]
//		public static object DbComboDistributionCenterSelectOrigin(Cambro.Web.DbCombo.ServerMethodArgs args)
//		{
//			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
//			string strAppID = util.GetAppID();
//			string strEnterpriseID = util.GetEnterpriseID();	
//
//			DataSet dataset = new DataSet();
//			dataset = com.ties.classes.DbComboDAL.DistributionCenterQuery(strAppID,strEnterpriseID,args);
//			return dataset;
//		}

		private void txtConNo_TextChanged(object sender, System.EventArgs e)
		{
			
		}

		private void txtDestPostCode_TextChanged(object sender, System.EventArgs e)
		{
			Zipcode zipcode = new Zipcode();
			DataSet dsCountZip = new DataSet();
			dsCountZip = zipcode.ckZipcodeInSystem(m_strAppID, m_strEnterpriseID,txtDestPostCode.Text.ToString());
			if (dsCountZip.Tables[0].Rows.Count<=0)
			{
				btnSave.Enabled = false;
				lblErrrMsg.Text = "Invalid postal code.";
				setFocusCtrl(txtDestPostCode.ClientID);
				return;
			}
			else
			{
				btnSave.Enabled = true;
			}
			GetProvinceFromPostalCode();
			if(bindServiceTypefromPostalCode(txtDestPostCode.Text) == true)
			{				
				ddlServiceType.SelectedIndex = 0;
				setFocusCtrl(ddlServiceType.ClientID);
			}
		}

		private void ddlOriginDC_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			tbTempText.Text = ddlOriginDC.SelectedValue.ToString();
			if(bindServiceTypefromPostalCode(txtDestPostCode.Text) == true)
			{				
				ddlServiceType.SelectedIndex = 0;	
			}
		}

		private void txtConsignee_TextChanged(object sender, System.EventArgs e)
		{
			setFocusCtrl(txtRemark.ClientID);
		}

		private void ddlServiceType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			setFocusCtrl(txtPkgNum.ClientID);
		}

//		private void txtPkgNum_TextChanged(object sender, System.EventArgs e)
//		{
//			setFocusCtrl(txtActual.ClientID);
//		}

		#endregion

		#region Function

		private void setFocusCtrl(string ctrlName)
		{
			string script="<script langauge='javacript'>";
			script+= "document.all['"+ ctrlName +"'].focus();";
			script+= "</script>";
			Page.RegisterStartupScript("setFocus"+DateTime.Now.ToString("ddMMyyyyhhmmss"),script);
		
		}

		private bool bindServiceTypefromPostalCode(string DestPostCode)
		{
			//string OriginDC = string.Empty;
			string OriginDC = ddlOriginDC.SelectedValue.ToString();
			string tmpOriginDC = tbTempText.Text;

			if(txtConNo.Text!="")
			{	
				DataSet dsChk_SIP = new DataSet();
				dsChk_SIP = SWB_Emulator.SWB_SIP_Read(m_strAppID,m_strEnterpriseID,txtConNo.Text);
				//dsChk_POD = QuickPODMgrDAL.QueryQuickPOD(m_strAppID,m_strEnterpriseID,con);
				if(dsChk_SIP.Tables[0].Rows.Count>0)
				{
					//OriginDC=ds.Tables[0].Rows[0]["origin_DC"].ToString();
				}
				else
				{
					lblErrrMsg.Text = "Consignment does not exist - Quick POD may proceed.";
				}
			}
			
			DataSet dstmp = QuickPODMgrDAL.SearchAllServiceAvailable(m_strAppID,m_strEnterpriseID,OriginDC,DestPostCode);

			string strBestService="";
			if(dstmp.Tables[0].Rows.Count>0)
			{
				strBestService=dstmp.Tables[0].Rows[0]["service_code"].ToString();
			}
			DataSet dsDlvType = QuickPODMgrDAL.SearchAllServiceAvailable(m_strAppID,m_strEnterpriseID,OriginDC,DestPostCode);
			if(dsDlvType!=null)
			{
				ddlServiceType.DataSource = dsDlvType;
				ddlServiceType.DataTextField = "service_code"; 
				ddlServiceType.DataValueField="service_code";
				ddlServiceType.DataBind();
				
				if(dsDlvType.Tables[0].Rows.Count < 1 )
				{
					ListItem defItem = new ListItem();
					defItem.Text = "";			
					defItem.Value = "";
					ddlServiceType.Items.Insert(0,(defItem));				
				}
				
				ddlServiceType.ClearSelection();						
			}
			if(dsDlvType == null || dsDlvType.Tables.Count == 0 || dsDlvType.Tables[0].Rows.Count == 0)
			{
				//txtDestPostCode.Text = "";
				lblProvince.Text="";
				//ddlServiceType.Enabled=false;
				//lblErrrMsg.Text = "No Consignment Number.";
				setFocusCtrl(txtDestPostCode.ClientID);
				return false;
			}	
			return true;
		}

		private void GetProvinceFromPostalCode()
		{
			Zipcode zipcode = new Zipcode();
			zipcode.Populate(m_strAppID,m_strEnterpriseID,txtDestPostCode.Text.ToString());
			DataSet dsCountZip = new DataSet();
			dsCountZip = zipcode.ckZipcodeInSystem(m_strAppID, m_strEnterpriseID,txtDestPostCode.Text.ToString());
			if(dsCountZip.Tables[0].Rows.Count>0)
			{
				lblProvince.Text = dsCountZip.Tables[0].Rows[0]["state_code"].ToString();
				DataSet dsDC = ImportConsignmentsDAL.getDCToDestStation(m_strAppID, m_strEnterpriseID,
					0, 0, zipcode.DeliveryRoute).ds;
				lbltmpDestDC.Text  = dsDC.Tables[0].Rows[0]["origin_station"].ToString();

			}
			else
			{
				lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_DESTPOSTALCODE_ERR",m_strCulture); 
			}
		}

		private bool checkRole()
		{
			try
			{
				User user = new User();
				user.UserID = userID;
				ArrayList userRoleArray = com.common.RBAC.RBACManager.GetAllRoles(m_strAppID,m_strEnterpriseID,user);
				Role role;
				bool isSWRole = false;
				for(int i=0;i<userRoleArray.Count;i++)
				{
					role = (Role)userRoleArray[i];
					if(role.RoleName.ToUpper().Trim()=="SWIMPORT")				
					{
						isSWRole = true;
						return true;
					}
					else
					{
						isSWRole = false;
					}
				}
				return isSWRole;
			}
			catch
			{
				return false;
			}
		}

		private void ClearCriteria()
		{
			txtConNo.Text = "";
			txtConNo.Enabled = true;
			txtConNo.ReadOnly = false;
			ddlOriginDC.SelectedValue="";
			txtDestPostCode.Text ="";
			ddlServiceType.Items.Clear();
			txtPkgNum.Text = "";
			txtActual.Text ="";
			txtConsignee.Text = "";
			txtRemark.Text = "";
			lblProvince.Text ="";
			//lbSIPCon.Items.Clear();
			setFocusCtrl(txtConNo.ClientID);
			btnSave.Enabled = true;
			lblErrrMsg.Text = "";
		}

		private void FieldData()
		{
			lblErrrMsg.Text = "";
			string ConNo="";
			if(Utility.ValidateConsignmentNo(txtConNo.Text.Trim(),ref ConNo)==false)
			{
				if(txtConNo.Text.Trim() != "")
				{
					lblErrrMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"CREATE_SIP_MSG_CONSNO_INVALID",m_strCulture); 
					setFocusCtrl(txtConNo.ClientID);
				}
				return;
			}
			txtConNo.Text=ConNo;

			//btnSave.Enabled = true;
			
			if (txtConNo.Text!="")
			{
				DataSet ds = new DataSet();
				string con = txtConNo.Text.ToString();
	
				if(txtConNo.Text.Length==32)//Fedex32 Format
				{
					con= txtConNo.Text.Substring(CharBarcodeCutStartIndex,CharBarcodeCutLength);
				}
				else
				{
					string[] tmpcon = con.Split(' ');
					con=tmpcon[0];
				}
												
				//ds = SWB_Emulator.SWB_SIP_Read(m_strAppID,m_strEnterpriseID,con);

				ds = QuickPODMgrDAL.QueryQuickPOD(m_strAppID,m_strEnterpriseID,con);

				if (ds.Tables[0].Rows.Count>0)
				{
					int donotAllowPOD = Convert.ToInt16(ds.Tables[0].Rows[0]["DoNotAllowPOD"]);

					string originDC = ds.Tables[0].Rows[0]["origin_DC"].ToString();

					if(originDC == "")
					{
						setFocusCtrl(ddlOriginDC.ClientID);
					}
					else
					{
						setFocusCtrl(txtActual.ClientID);
						//setFocusCtrl(txtConNo.ClientID);
					}
			
					txtConNo.Enabled=false;
					
					ddlOriginDC.SelectedValue = ds.Tables[0].Rows[0]["origin_DC"].ToString();
					tbTempText.Text = ddlOriginDC.SelectedValue.ToString();
					txtConNo.Text = ds.Tables[0].Rows[0]["consignment_no"].ToString();
					txtDestPostCode.Text = ds.Tables[0].Rows[0]["dest_postal_code"].ToString();
					
					if(Convert.ToString(ds.Tables[0].Rows[0]["act_delivery_date"]) != "")
					{
						Session["TMP_ACT_DATETIME"] = Convert.ToDateTime(ds.Tables[0].Rows[0]["act_delivery_date"].ToString()).ToString("dd/MM/yyyy HH:mm");
					}
					else
					{
						txtActual.Text = "";
					}

//					if (bindServiceTypefromPostalCode(ds.Tables[0].Rows[0]["dest_postal_code"].ToString()) == false)
//					{
//						return;
//					}
					
					GetProvinceFromPostalCode();
					bindServiceTypefromPostalCode(txtDestPostCode.Text);
					if(ddlServiceType.Items.FindByValue(ds.Tables[0].Rows[0]["service_code"].ToString())!=null)
					{
						ddlServiceType.SelectedValue= ds.Tables[0].Rows[0]["service_code"].ToString();
					}
					else
					{
						ddlServiceType.Items.Insert(0,"");
						ddlServiceType.SelectedIndex=0;
						lblErrrMsg.Text = "Invalid Service Type.";
						setFocusCtrl(ddlServiceType.ClientID);
						//return;
					}
					txtPkgNum.Text= ds.Tables[0].Rows[0]["number_of_package"].ToString();
					
					txtConsignee.Text = ds.Tables[0].Rows[0]["consignee_name"].ToString();
					txtRemark.Text = ds.Tables[0].Rows[0]["remark"].ToString();

					txtConNo.ReadOnly=true;

					if(donotAllowPOD == 0)
					{
						btnSave.Enabled = true;
					}
					else
					{		
						btnSave.Enabled = false;

						if(donotAllowPOD == 1)
						{
							lblErrrMsg.Text = "Shipment already has been manifest data entered - Quick POD is not allowed.";
						}
						else if(donotAllowPOD == 2)
						{
							lblErrrMsg.Text = "Shipment already has a POD";
							generateDataToListbox();
							
							txtConNo.Text = "";
							txtConNo.Enabled = true;
							txtConNo.ReadOnly = false;
							ddlOriginDC.SelectedValue="";
							txtDestPostCode.Text ="";
						    //ddlServiceType.SelectedValue ="" ;
							ddlServiceType.Items.Clear();
							txtPkgNum.Text = "";
							txtActual.Text ="";
							txtConsignee.Text = "";
							txtRemark.Text = "";
							lblProvince.Text ="";
							setFocusCtrl(txtConNo.ClientID);

							return;
						}
					}

					//setFocusCtrl(btnUpdate.ClientID);				
				}
				else
				{
					txtConNo.Enabled=true;
					//txtConNo.Text =hddConNo.Value;
					txtPkgNum.Text="1";

					txtConNo.ReadOnly=false;
				}

				setFocusCtrl(ddlOriginDC.ClientID);	
			
			}
			//setFocusCtrl(txtConNo.ClientID);	
		}

		private void generateDataToListbox()
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			string con = txtConNo.Text.ToString();
			string[] tmpcon = con.Split(' ');
			con=tmpcon[0];
			string actDateTime;
			
			IFormatProvider culture = new CultureInfo("en-GB", true);
			//string actDateTime = Convert.ToDateTime(txtActual.Text,culture).ToString("dd/MM/yyyy HH:mm");

			if(txtActual.Text != "")
			{
				actDateTime = Convert.ToDateTime(txtActual.Text,culture).ToString("dd/MM/yyyy HH:mm").ToString();
			}
			else
			{
				actDateTime = Session["TMP_ACT_DATETIME"].ToString();
			}
			
			string data  = con.ToString()+utility.generateHTMLSpace(25-con.ToString().Length);
			data += ddlServiceType.SelectedValue.ToString()+utility.generateHTMLSpace(10- ddlServiceType.SelectedValue.ToString().Length);
			data += txtDestPostCode.Text .ToString()+utility.generateHTMLSpace(11- txtDestPostCode.Text.ToString().Length);
			data += txtPkgNum.Text.ToString()+utility.generateHTMLSpace(7-txtPkgNum.Text.ToString().Length);
			data += actDateTime+utility.generateHTMLSpace(20-actDateTime.Length);
			data += ddlOriginDC.SelectedValue.ToString()+utility.generateHTMLSpace(15-ddlOriginDC.SelectedValue.ToString().Length);
			data += txtConsignee.Text.ToString()+utility.generateHTMLSpace(20-txtConsignee.Text.ToString().Length);

			//ListItemCollection tmpListCollection  =  new ListItemCollection();
			foreach(ListItem list in lbSIPCon.Items)
			{	 			
				string lValue = list.Value;
				string[] tmpval = lValue.Split('@');
				lValue=tmpval[0];
				if(lValue==con)
				{
					lbSIPCon.Items.Remove(list);
					break;
				}
			}

			string val = con.ToString()+"@"+DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
			ListItem lData = new ListItem(data,val);
			lbSIPCon.Items.Insert(lbSIPCon.Items.Count,lData);
			setFocusCtrl(txtConNo.ClientID);
		}

		private bool validateRequiredField()
		{
			//DataSet ds = QuickPODMgrDAL.QueryQuickPOD(m_strAppID,m_strEnterpriseID,txtConNo.Text);
			//DateTime oldTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["act_delivery_date"]);
			//DateTime newTime = Convert.ToDateTime(txtActual.Text);

			if(txtActual.Text != "")
			{
				DateTime newDate = DateTime.ParseExact(txtActual.Text,"dd/MM/yyyy HH:mm",null);
				//DateTime DatetimeNow = new DateTime.
				DateTime dNow = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"),"dd/MM/yyyy HH:mm",null);
				diff = dNow.CompareTo(newDate);
			}
			else
			{
				txtActual.Text = "";
			}
			
			/* 
			diff = 0  ---> equal 
			diff = 1  ---> less than now 
			diff = -1  ---> more than now 
			*/

			int checkNumPackages = QuickPODMgrDAL.CheckNumberOfPackages(m_strAppID,m_strEnterpriseID);

			if(ddlOriginDC.SelectedValue.ToString() == "")
			{
				lblErrrMsg.Text = "Origin DC is required.";
				setFocusCtrl(ddlOriginDC.ClientID);
				return false;
			}
			else if(txtDestPostCode.Text == "")
			{
				lblErrrMsg.Text = "Destination Postal Code is required.";
				setFocusCtrl(txtDestPostCode.ClientID);
				return false;
			}
			else if(ddlServiceType.SelectedValue.ToString() == "")
			{
				lblErrrMsg.Text = "Service Type is required.";
				setFocusCtrl(ddlServiceType.ClientID);
				return false;
			}
			else if(txtPkgNum.Text == "")
			{
				lblErrrMsg.Text = "Number of Packages is required.";
				setFocusCtrl(txtPkgNum.ClientID);
				return false;
			}
			else if(Convert.ToInt16(txtPkgNum.Text) > checkNumPackages)
			{
				lblErrrMsg.Text = "Number of Packages is not allowed.";
				setFocusCtrl(txtPkgNum.ClientID);
				return false;
			}
			else if(Convert.ToInt16(txtPkgNum.Text) == 0)
			{
				lblErrrMsg.Text = "Invalid or missing number of packages: must be >0 and <= 9999.";
				setFocusCtrl(txtPkgNum.ClientID);
				return false;
			}
			else if(txtActual.Text == "")
			{
				lblErrrMsg.Text = "Actual D/T is required.";
				setFocusCtrl(txtActual.ClientID);
				return false;
			}
			else if(diff < 0)
			{
				lblErrrMsg.Text = "Actual D/T entered must be <= " + Session["TMP_ACT_DATETIME"].ToString() +".";
				setFocusCtrl(txtActual.ClientID);
				return false;
			}
			else if(txtConsignee.Text == "")
			{
				lblErrrMsg.Text = "Consignee is required.";
				setFocusCtrl(txtConsignee.ClientID);
				return false;
			}	
			return true;
		}

		private bool bindDataToDropdownOrigicDC()
		{
			Utility util = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			string strAppID = util.GetAppID();
			string strEnterpriseID = util.GetEnterpriseID();	

			DataSet dsOriginDC = new DataSet();
			dsOriginDC = QuickPODMgrDAL.ReadOriginDCData(m_strAppID,m_strEnterpriseID);
			
			if (dsOriginDC!=null)
			{
				ddlOriginDC.DataSource = dsOriginDC;
				ddlOriginDC.DataTextField = "origin_code"; 
				ddlOriginDC.DataValueField="origin_code";
				ddlOriginDC.DataBind();

				ListItem defItem = new ListItem();
				defItem.Text = "";
			
				defItem.Value = "";
				ddlOriginDC.Items.Insert(0,(defItem));
			}
			if(dsOriginDC == null || dsOriginDC.Tables.Count == 0 || dsOriginDC.Tables[0].Rows.Count == 0)
			{
				txtDestPostCode.Text = "";
				lblProvince.Text="";
				//ddlServiceType.Enabled=false;
				//lblErrrMsg.Text = "No Consignment Number.";
				setFocusCtrl(txtConNo.ClientID);
				return false;
			}	
			return true;
		}

		
		#endregion		


	}
}
