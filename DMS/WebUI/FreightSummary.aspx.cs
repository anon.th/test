using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using System.Text;
using System.Configuration;
using System.Data.OleDb;
using Cambro.Web.DbCombo;
using TIESDAL;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for FreightSummary.
	/// </summary>
	public class FreightSummary : com.common.applicationpages.BasePage
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnGenerate;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentTracking;
		protected System.Web.UI.WebControls.Label lblDates;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label Label5;
		protected com.common.util.msTextBox txtYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected com.common.util.msTextBox txtPeriod;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.ListBox lsbCustType;
		protected System.Web.UI.WebControls.Label Label6;
		protected System.Web.UI.WebControls.Label Label10;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected com.common.util.msTextBox txtPayerCode;
		protected System.Web.UI.WebControls.Button btnPayerCode;

		private String m_strAppID;
		private String m_strEnterpriseID;
		private String m_strCulture;
		protected System.Web.UI.WebControls.RadioButton rbInvoicedDate;
		protected System.Web.UI.WebControls.RadioButton rbManifestedDate;
		protected System.Web.UI.WebControls.RadioButton rbNoInvoice;
		protected System.Web.UI.WebControls.Label Label2;

        protected System.Web.UI.WebControls.RadioButton rdoShipmentTypeDomestic;
        protected System.Web.UI.WebControls.RadioButton rdoShipmentTypeInternational;
        protected System.Web.UI.WebControls.RadioButton rdoShipmentTypeBoth;

        protected System.Web.UI.WebControls.RadioButton rdoReportNameFreightSummary;
        protected System.Web.UI.WebControls.RadioButton rdoReportNameFedEx;
        
        private DataSet m_dsQuery=null;


		private void ddlmonths()
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
		
			DataRow drNilRow = dtMonths.NewRow();
			drNilRow[0] = "";
			drNilRow[1] = "";
			dtMonths.Rows.Add(drNilRow);
   
			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);

			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}


		public void LoadCustomerTypeList()
		{
			ArrayList systemCodes = Utility.GetCodeValues(utility.GetAppID(),utility.GetUserCulture(),"customer_type",CodeValueType.StringValue);
			foreach(SystemCode sysCode in systemCodes)
			{	
				ListItem lstItem = new ListItem();
				lstItem.Text = sysCode.Text;
				lstItem.Value = sysCode.StringValue;
				lsbCustType.Items.Add(lstItem);
			}
		}


		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			if(!Page.IsPostBack)
			{
				lblErrorMessage.Text = "";

				utility = new Utility(System.Configuration.ConfigurationManager.AppSettings,Page.Session);
				m_strAppID = utility.GetAppID();
				m_strEnterpriseID = utility.GetEnterpriseID();
				m_strCulture = utility.GetUserCulture();

				ddlmonths();
				LoadCustomerTypeList();

				txtPeriod.Enabled = false;
				txtTo.Enabled = false;
				txtDate.Enabled = false;
			}
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
			this.rbInvoicedDate.CheckedChanged += new System.EventHandler(this.rbInvoicedDate_CheckedChanged);
			this.rbManifestedDate.CheckedChanged += new System.EventHandler(this.rbManifestedDate_CheckedChanged);
			this.rbNoInvoice.CheckedChanged += new System.EventHandler(this.rbNoInvoice_CheckedChanged);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.btnPayerCode.Click += new System.EventHandler(this.btnPayerCode_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private DataTable CreateEmptyDataTable()
		{
			DataTable dtShipment = new DataTable();

			#region "Dates"
			dtShipment.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
			dtShipment.Columns.Add(new DataColumn("end_date", typeof(DateTime)));
			#endregion
			
			#region "Payer Type"
			dtShipment.Columns.Add(new DataColumn("payer_type", typeof(string)));
			dtShipment.Columns.Add(new DataColumn("payer_code", typeof(string)));
			#endregion
		
			dtShipment.Columns.Add(new DataColumn("date_mode", typeof(string)));
            dtShipment.Columns.Add(new DataColumn("shipment_type", typeof(string)));
            dtShipment.Columns.Add(new DataColumn("report_name", typeof(string)));
            return dtShipment;
		}

		private DataSet GetShipmentQueryData()
		{
			DataSet dsShipment = new DataSet();
			DataTable dtShipment = CreateEmptyDataTable();
			DataRow dr = dtShipment.NewRow(); 
			string sStartDate = string.Empty;

			#region "Dates"
	
			string strMonth =null;
			if (rbMonth.Checked)
			{
				strMonth = ddMonth.SelectedItem.Value;
				if (strMonth != "" && txtYear.Text != "")
				{
					// Thosapol Yennam (08/08/2013) Modify Code
					if(Convert.ToInt32(strMonth) < 10){ strMonth = "0"+strMonth;}

					sStartDate="01"+"/"+strMonth+"/"+txtYear.Text;
					DateTime dtStartDate = DateTime.ParseExact (sStartDate, "dd/MM/yyyy", null);
					DateTime dtEndDate = dtStartDate.AddMonths(1).AddDays(-1);
					dtEndDate = DateTime.ParseExact(dtEndDate.ToString("dd/MM/yyyy")+" 23:59", "dd/MM/yyyy HH:mm", null);
					dr["start_date"] = dtStartDate;
					dr["end_date"] = dtEndDate;
				}
			}
			else if (rbPeriod.Checked)
			{
				if (txtPeriod.Text != ""  && txtTo.Text != "")
				{
					dr["start_date"] = DateTime.ParseExact(txtPeriod.Text ,"dd/MM/yyyy",null);
					dr["end_date"] = DateTime.ParseExact(txtTo.Text+" 23:59" ,"dd/MM/yyyy HH:mm",null);
				}
			}
			else 
			{
				if (txtDate.Text != "")
				{
					dr["start_date"] = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy",null);
					dr["end_date"] = DateTime.ParseExact(txtDate.Text+" 23:59", "dd/MM/yyyy HH:mm",null);
				}
			}
			#endregion

			#region PayerType
			string strCustPayerType = "";

			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
			{
				if(lsbCustType.Items[i].Selected == true)
				{
					strCustPayerType += "'" + lsbCustType.Items[i].Value + "', ";
				}
			}
			if((strCustPayerType != null) && (strCustPayerType.Trim().Length > 0))
				strCustPayerType = strCustPayerType.Substring(0, strCustPayerType.Length - 2);

			if (strCustPayerType != "") 
				dr["payer_type"] = strCustPayerType;
			else
				dr["payer_type"] = System.DBNull.Value;
			#endregion

			if(txtPayerCode.Text != "")
				dr["payer_code"] = txtPayerCode.Text;
			else
				dr["payer_code"] = System.DBNull.Value;

			if(rbInvoicedDate.Checked)
				dr["date_mode"] = "I";
			else if(rbManifestedDate.Checked)
				dr["date_mode"] = "M";
			else if(rbNoInvoice.Checked)
				dr["date_mode"] = "N";

            if (rdoShipmentTypeDomestic.Checked)
                dr["shipment_type"] = "D";
            else if (rdoShipmentTypeInternational.Checked)
                dr["shipment_type"] = "I";
            else if (rdoShipmentTypeBoth.Checked)
                dr["shipment_type"] = "B";

            dr["report_name"] = "FreightSummary";
            /*
            if (rdoReportNameFreightSummary.Checked)
                dr["report_name"] = "FreightSummary";
            else if (rdoReportNameFedEx.Checked)
                dr["report_name"] = "FedEx_TNT";
            */
            dtShipment.Rows.Add(dr);
			dsShipment.Tables.Add(dtShipment);
			return dsShipment;
		}

		private void OpenWindowpage(String strUrl)
		{
			ArrayList paramList = new ArrayList();
			paramList.Add(strUrl);
			String sScript = Utility.GetScript("openWindowReport.js", paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}


		private void DefaultScreen()
		{
			#region "Dates"
			rbMonth.Checked = true;
			rbPeriod.Checked = false;
			rbDate.Checked = false;

			ddMonth.Enabled = true;
			ddMonth.SelectedIndex = -1;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;

			#endregion

			#region "Payer Type"

			lsbCustType.SelectedIndex = -1;
			txtPayerCode.Text="";

			#endregion
			
			lblErrorMessage.Text = "";

			
		}

		private bool ValidateValues()
		{
			bool iCheck=false;
	
			if((ddMonth.SelectedIndex==0)&&(txtYear.Text=="")&&(txtPeriod.Text=="")&&(txtTo.Text=="")&&(txtDate.Text==""))
			{			
				lblErrorMessage.Text = "Please enter the criterias to search.";
				//lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_EST_DVL",utility.GetUserCulture());
				return iCheck=true;
			
			}
			if(rbMonth.Checked == true)
			{
				if((ddMonth.SelectedIndex>0) &&(txtYear.Text==""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((ddMonth.SelectedIndex==0) &&(txtYear.Text!=""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"MM_YEAR_REQ",utility.GetUserCulture());
					return iCheck=true;
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbPeriod.Checked == true )
			{
				if((txtPeriod.Text!="")&&(txtTo.Text==""))
				{					
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_END_DT",utility.GetUserCulture());
					return iCheck=true;
				}
				else if((txtPeriod.Text=="")&&(txtTo.Text!=""))
				{
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_START_DT",utility.GetUserCulture());					
					return iCheck=true;			
				}
				else
				{
					return iCheck=false;
				}
			}
			if(rbDate.Checked == true )
			{
				if(txtDate.Text=="")
				{					
					lblErrorMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DT",utility.GetUserCulture());	
					return iCheck=true;				
				}
			}			

			return iCheck;
			
		}

		private void btnPayerCode_Click(object sender, System.EventArgs e)
		{
			string strCustPayerType = "";
			for (int i = 0; i <= lsbCustType.Items.Count - 1; i ++)
				if(lsbCustType.Items[i].Selected == true)
					strCustPayerType += lsbCustType.Items[i].Value;

			String sUrl = "CustomerPopup.aspx?FORMID="+"FreightSummary"+"&CUSTID_TEXT="+txtPayerCode.Text.Trim().ToString()+"&CustType="+strCustPayerType;
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);
			String sScript = Utility.GetScript("openParentWindow.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
		}

		private void btnGenerate_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";

			if(ValidateValues() == false)
			{
				m_dsQuery = GetShipmentQueryData();

				String strUrl = null;
				strUrl = "ReportViewerDataSet.aspx";
				Session["FORMID"] = "FreightSummary";
				Session["SESSION_DS1"] = m_dsQuery;
				OpenWindowpage(strUrl);
			}
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
			rbInvoicedDate.Checked = true;
			rbManifestedDate.Checked = false;
		}

		private void rbManifestedDate_CheckedChanged(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}

		private void rbInvoicedDate_CheckedChanged(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}

		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true;
			ddMonth.SelectedIndex = -1;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;

			txtPeriod.Enabled = true;
			txtTo.Enabled = true;
			ddMonth.Enabled = false;
			txtYear.Enabled = false;

			ddMonth.SelectedIndex = 0;
			txtYear.Text = "";

			txtDate.Enabled = false;
			txtDate.Text = "";
		}

		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true;
			ddMonth.SelectedIndex = -1;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;

			txtPeriod.Enabled = false;
			txtTo.Enabled = false;

			ddMonth.Enabled = true;
			txtYear.Enabled = true;

			ddMonth.SelectedIndex = 0;
			txtYear.Text = "";

			txtDate.Enabled = false;
			txtDate.Text = "";
		}

		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true;
			ddMonth.SelectedIndex = -1;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;

			txtPeriod.Enabled = false;
			txtTo.Enabled = false;

			ddMonth.Enabled = false;
			txtYear.Enabled = false;

			ddMonth.SelectedIndex = 0;
			txtYear.Text = "";

			txtDate.Enabled = true;
			txtDate.Text = "";
		}

		private void rbNoInvoice_CheckedChanged(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}
	}
}
