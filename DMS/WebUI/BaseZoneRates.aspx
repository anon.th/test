<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>

<%@ Page Language="c#" CodeBehind="BaseZoneRates.aspx.cs" AutoEventWireup="false" Inherits="com.ties.BaseZoneRates" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>BaseZoneRates</title>
    <link href="css/Styles.css" type="text/css" rel="stylesheet">
    <meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
    <meta content="C#" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <!--#INCLUDE FILE="msFormValidations.inc"-->
    <script runat="server">
    </script>
    <script language="javascript">
        //by X oct 03 08
        function setValue() {
            var s = document.all['inFile'];
            document.all['txtFilePath'].innerText = s.value;
            if (s.value != '') {
                document.all['btnImport'].disabled = false;
                document.getElementById('<%= btnNewImport.ClientID %>').disabled = false;
            }
            else {
                document.all['btnImport'].disabled = true;
                document.getElementById('<%= btnNewImport.ClientID %>').disabled = true;
            }
        }

        function upBrowse() {
            document.all['divBrowse'].style.backgroundImage = 'url(images/btn-browse-up-30.GIF)';
        }

        function downBrowse() {
            document.all['divBrowse'].style.backgroundImage = 'url(images/btn-browse-down-30.GIF)';

        }

        function clearValue() {
            if (document.all['txtFilePath'].innerText == '') {
                document.location.reload();
            }
        }

        function setOnOff() {
            //if(document.all['btnImport'].visible==false)
            var btnImp = document.getElementById("<%= btnImport_Show.ClientID %>");
            if (btnImp.visible == true)
                document.all['divBrowse'].style.visibility = 'visible';
            else
                document.all['divBrowse'].style.visibility = "hidden";
        }

        function isNotSpecialCharacter() {

            var returnValue = false;
            var keyCode = (window.event.which) ? window.event.which : window.event.keyCode;
            if ((keyCode == 37) || (keyCode == 42) || (keyCode == 91) || (keyCode == 93) || (keyCode == 38) || (keyCode == 33) || (keyCode == 64) || (keyCode == 35) || (keyCode == 36) || (keyCode == 94) || (keyCode == 40) || (keyCode == 41) || (keyCode == 95) || (keyCode == 43) || (keyCode == 34) || (keyCode == 58) || (keyCode == 59) || (keyCode == 39)) {
                returnValue = true;
            }

            if (window.event.returnValue)
                window.event.returnValue = returnValue;
            return returnValue;

        }

        function testShowKeyCode() {
            var keyCode = (window.event.which) ? window.event.which : window.event.keyCode;
            alert(keyCode);
        }

        function ShowPopup() {
            window.open("CustomerPopup.aspx?FORMID=" + "BaseZoneRates", '', 'height=550,width=800,left=100,top=50,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no');
        }

        function validateRow(element) {
            if (document.all['txtRow'].value != '' && document.all['txtEndRow'].value != '') {
                if (parseInt(document.all['txtRow'].value) > parseInt(document.all['txtEndRow'].value)) {
                    alert("Start row must be less than end row.");
                    document.all['txtEndRow'].value = document.all['txtRow'].value;
                }
            }

            if (parseInt(element.value) == 0) {
                if (element.id == 'txtRow')
                    alert("(City origin) Row must be more than 0");
                else if (element.id == 'txtEndRow')
                    alert("(City Destination) Row must be more than 0");

                element.value = "";
            }
        }

        function validateColumn(element) {
            if (element.id == 'txtColumn' && parseInt(element.value) == 0) {
                alert("(City origin) Column must be more than 0");
                element.value = "";
            }
        }

        function validateEffectiveDate() {
            var today = new Date();
            var d1 = new Date(today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate());
            var d2 = new Date(document.all['txtEffDate'].value.replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$3/$2/$1"));

            if (d2 < d1) {
                alert("Effective date must be more than or equal current date.");
                document.all['txtEffDate'].value = "";
            }
            else
                document.getElementById('txtBasicRate').focus();
        }

        function showProgress() {
            if (document.getElementById("txtFilePath").value != ""
                && document.getElementById("txtCustID").value != ""
                && document.getElementById("txtEffDate").value != ""
                && document.getElementById("txtBasicRate").value != ""
                && document.getElementById("txtValuable").value != ""
                && document.getElementById("txtHuman").value != ""
                && document.getElementById("txtLive").value != ""
                && document.getElementById("txtExpress").value != ""
                && document.getElementById("txtSheet").value != "" && document.getElementById("txtSheet").value != "0"
                && document.getElementById("txtColumn").value != "" && document.getElementById("txtColumn").value != "0"
                && document.getElementById("txtRow").value != "" && document.getElementById("txtRow").value != "0"
                && document.getElementById("txtEndRow").value != "" && document.getElementById("txtEndRow").value != "0"
               ) {
                var updateProgress = $get("<%= UpdateProgress1.ClientID %>");
                updateProgress.style.display = "block";
                document.body.style.overflow = "hidden";
            }
        }

        function showSaveProgress() {
            var updateProgress = $get("<%= UpdateProgress1.ClientID %>");
            updateProgress.style.display = "block";
            document.body.style.overflow = "hidden";
        }

    </script>

    <style type="text/css">
        .overlayLoading {
            position: absolute;
            z-index: 500;
            height: 100%;
            width: 101%;
            top: 0px;
            background-color: white;
            filter: alpha(opacity=85);
            opacity: 0.8;
            -moz-opacity: 0.8;
            left: 0px;
            overflow:hidden;
            
        }
    </style>
</head>
<%--onload="setOnOff();"--%>
<%--id="bodyID" onload="test()"--%>
<body onunload="window.top.displayBanner.fnCloseAll(0);" ms_positioning="GridLayout">
    <form id="BaseZoneRates" method="post" enctype="multipart/form-data" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table id="xxx" style="z-index: 104; left: 38px; width: 686px; position: absolute; top: 14px; height: 80px" width="686" border="0" runat="server">
                    <tr>
                        <td>
                            <asp:Label ID="lblTitle" runat="server" CssClass="mainTitleSize" Height="26px" Width="477px">Base Rates - By Weight</asp:Label></td>
                    </tr>
                    <tr id="trQuery" runat="server">
                        <td>
                            <asp:Button ID="btnQuery" runat="server" CssClass="queryButton" Width="60px" Height="30px" Text="Query" CausesValidation="False"></asp:Button>
                            <asp:Button ID="btnExecuteQuery" runat="server" CssClass="queryButton" Width="139px" Height="30px" Text="Execute Query" CausesValidation="False"></asp:Button>
                            <asp:Button ID="btnInsert" runat="server" CssClass="queryButton" Width="68px" Height="30px" Text="Insert" CausesValidation="False"></asp:Button>&nbsp;
                            <asp:Button ID="btnImport_Show" runat="server" CssClass="queryButton" Width="140px" Height="30px" Text="Import Base Rates" CausesValidation="False"></asp:Button>
                        </td>
                    </tr>
                    <tr id="trImport" runat="server">
                        <td>
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="btnImport_Cancel" runat="server" CssClass="queryButton" Width="62px" Height="30px" Text="Cancel" CausesValidation="False"></asp:Button>&nbsp;
						        <%--<div onmouseup="upBrowse();" onmousedown="downBrowse();" id="divBrowse" style="DISPLAY: inline; BACKGROUND-IMAGE: url(images/btn-browse-up.GIF); WIDTH: 50px; HEIGHT: 21px">
                                    <INPUT id="inFile" onblur="setValue();" style="BORDER-RIGHT: #88a0c8 1px outset; BORDER-TOP: #88a0c8 1px outset; FONT-SIZE: 11px; FILTER: alpha(opacity: 0); BORDER-LEFT: #88a0c8 1px outset; WIDTH: 0px; COLOR: #003068; BORDER-BOTTOM: #88a0c8 1px outset; FONT-FAMILY: Arial; BACKGROUND-COLOR: #e9edf0; TEXT-DECORATION: none" onfocus="setValue();" type="file" name="inFile" runat="server">
						        </div>--%>
                                        <label class="btn btn-default btn-file" id="divBrowse" style="position: relative; overflow: hidden; background-image: 'url(./images/btn-browse-up-30.GIF)'; background-repeat: no-repeat; background-size: cover; height: 30px; width: 70px; top: 4px; margin-right: 10px"
                                            onmouseup="upBrowse();" onmousedown="downBrowse();">
                                            <input type="file" id="inFile" onchange="setValue();" runat="server" style="display: none; position: absolute; top: 0; right: 0; min-width: 100%; min-height: 100%; font-size: 100px; text-align: right; filter: alpha(opacity=0); opacity: 0; outline: none; cursor: inherit; display: block; background-color: red;">
                                        </label>
                                        &nbsp;<asp:Button ID="btnImport" runat="server" CssClass="queryButton" Width="65px" Height="30px" Text="Import" CausesValidation="False" Enabled="False"></asp:Button>&nbsp;
                                        <asp:Button ID="btnNewImport" runat="server" CssClass="queryButton" Width="100px" Height="30px" Text="New Import" OnClientClick="showProgress()"></asp:Button>&nbsp;
                                        <asp:Button ID="btnImport_Save" runat="server" CssClass="queryButton" Width="62px" Height="30px" Text="Save" Enabled="False" CausesValidation="False"></asp:Button>
                                    </td>
                                </tr>
                                <tr style="height: 15px">
                                    <td></td>
                                </tr>
                                <tr style="height: 15px">
                                    <td></td>
                                </tr>
                                <tr style="font-size: 11px; height: 30px;">
                                    <td style="width: 130px">
                                        <asp:RequiredFieldValidator ID="validateAccNo" Width="100%" runat="server" ErrorMessage="File Name" Display="None"
                                            ControlToValidate="txtFilePath"></asp:RequiredFieldValidator>
                                        <asp:Label ID="Label1" runat="server" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:Label>
                                        File Name
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFilePath" runat="server" CssClass="textField" Width="402px" Enabled="false"></asp:TextBox></td>
                                </tr>
                                <tr style="font-size: 11px; height: 30px;">
                                    <td>
                                        <asp:RequiredFieldValidator ID="REQUIREDFIELDVALIDATOR1" Width="100%" runat="server" ErrorMessage="Customer No" Display="None"
                                            ControlToValidate="txtCustID"></asp:RequiredFieldValidator>
                                        <asp:Label ID="Label2" runat="server" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:Label>
                                        Customer No
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCustID" runat="server" CssClass="textField" Width="70px"></asp:TextBox>
                                        &nbsp;
                                <button id="btnDisplayCustDtls" class="searchButton" style="width: 21px; height: 19px" onclick="ShowPopup();">...</button>
                                        <%--<asp:Button ID="btnDisplayCustDtls" runat="server" CssClass="searchButton" Width="21px" 
                                     CausesValidation="False" Text="..." Height="19px"></asp:Button>--%>
                                &nbsp;
                                <asp:TextBox ID="txtCustName" runat="server" CssClass="textField" Width="291px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="font-size: 11px; height: 30px;">
                                    <td>
                                        <asp:RequiredFieldValidator ID="REQUIREDFIELDVALIDATOR10" Width="100%" runat="server" ErrorMessage="Effective Date" Display="None"
                                            ControlToValidate="txtEffDate"></asp:RequiredFieldValidator>
                                        <asp:Label ID="Label11" runat="server" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:Label>
                                        Effective Date
                                    </td>
                                    <td>
                                        <cc1:msTextBox ID="txtEffDate" TabIndex="11" runat="server" Width="150px" CssClass="textField"
                                            TextMaskString="99/99/9999" TextMaskType="msDate" MaxLength="12" onchange="validateEffectiveDate()"></cc1:msTextBox>
                                    </td>
                                </tr>
                                <tr style="font-size: 11px; height: 30px;">
                                    <td>
                                        <asp:RequiredFieldValidator ID="REQUIREDFIELDVALIDATOR2" Width="100%" runat="server" ErrorMessage="Basic Rate" Display="None"
                                            ControlToValidate="txtBasicRate"></asp:RequiredFieldValidator>
                                        <asp:Label ID="Label3" runat="server" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:Label>
                                        Basic Rate</td>
                                    <td>
                                        <cc1:msTextBox ID="txtBasicRate" runat="server"
                                            Width="150px" CssClass="textFieldRightAlign" NumberPrecision="7" TextMaskType="msNumeric" NumberScale="2"
                                            NumberMinValue="0" NumberMaxValue="9999999"></cc1:msTextBox>
                                    </td>
                                </tr>
                                <tr style="height: 15px">
                                    <td></td>
                                </tr>
                                <tr style="font-size: 11px; height: 30px;">
                                    <td>
                                        <asp:RequiredFieldValidator ID="REQUIREDFIELDVALIDATOR3" Width="100%" runat="server" ErrorMessage="Valuable Cargo" Display="None"
                                            ControlToValidate="txtValuable"></asp:RequiredFieldValidator>
                                        <asp:Label ID="Label4" runat="server" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:Label>
                                        Valuable Cargo</td>
                                    <td>
                                        <cc1:msTextBox ID="txtValuable" runat="server"
                                            Width="150px" CssClass="textFieldRightAlign" NumberPrecision="4" TextMaskType="msNumeric"
                                            NumberMinValue="0" NumberMaxValue="9999"></cc1:msTextBox>
                                        &nbsp; % of normal rate</td>
                                </tr>
                                <tr style="font-size: 11px; height: 30px;">
                                    <td>
                                        <asp:RequiredFieldValidator ID="REQUIREDFIELDVALIDATOR4" Width="100%" runat="server" ErrorMessage="Human Remains" Display="None"
                                            ControlToValidate="txtHuman"></asp:RequiredFieldValidator>
                                        <asp:Label ID="Label5" runat="server" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:Label>
                                        Human Remains</td>
                                    <td>
                                        <cc1:msTextBox ID="txtHuman" runat="server"
                                            Width="150px" CssClass="textFieldRightAlign" NumberPrecision="4" TextMaskType="msNumeric"
                                            NumberMinValue="0" NumberMaxValue="9999"></cc1:msTextBox>
                                        &nbsp; % of normal rate</td>
                                </tr>
                                <tr style="font-size: 11px; height: 30px;">
                                    <td>
                                        <asp:RequiredFieldValidator ID="REQUIREDFIELDVALIDATOR5" Width="100%" runat="server" ErrorMessage="Live Animals" Display="None"
                                            ControlToValidate="txtLive"></asp:RequiredFieldValidator>
                                        <asp:Label ID="Label6" runat="server" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:Label>
                                        Live Animals</td>
                                    <td>
                                        <cc1:msTextBox ID="txtLive" runat="server"
                                            Width="150px" CssClass="textFieldRightAlign" NumberPrecision="4" TextMaskType="msNumeric"
                                            NumberMinValue="0" NumberMaxValue="9999"></cc1:msTextBox>
                                        &nbsp; % of normal rate</td>
                                </tr>
                                <tr style="font-size: 11px; height: 30px;">
                                    <td>
                                        <asp:RequiredFieldValidator ID="REQUIREDFIELDVALIDATOR6" Width="100%" runat="server" ErrorMessage="Express Cargo" Display="None"
                                            ControlToValidate="txtExpress"></asp:RequiredFieldValidator>
                                        <asp:Label ID="Label7" runat="server" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:Label>
                                        Express Cargo</td>
                                    <td>
                                        <cc1:msTextBox ID="txtExpress" runat="server"
                                            Width="150px" CssClass="textFieldRightAlign" NumberPrecision="4" TextMaskType="msNumeric"
                                            NumberMinValue="0" NumberMaxValue="9999"></cc1:msTextBox>
                                        &nbsp; % of normal rate</td>
                                </tr>
                                <tr style="height: 15px">
                                    <td></td>
                                </tr>
                                <tr style="font-size: 11px; height: 30px;">
                                    <td>
                                        <asp:RequiredFieldValidator ID="REQUIREDFIELDVALIDATOR11" Width="100%" runat="server" ErrorMessage="Sheet No" Display="None"
                                            ControlToValidate="txtSheet"></asp:RequiredFieldValidator>
                                        <asp:Label ID="Label12" runat="server" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:Label>
                                        Sheet No</td>
                                    <td>
                                        <cc1:msTextBox ID="txtSheet" runat="server"
                                            Width="150px" CssClass="textFieldRightAlign" NumberPrecision="9" TextMaskType="msNumeric"
                                            NumberMinValue="0" NumberMaxValue="999999999"></cc1:msTextBox>
                                    </td>
                                </tr>
                                <tr style="font-size: 11px; height: 30px;">
                                    <td>
                                        <asp:RequiredFieldValidator ID="REQUIREDFIELDVALIDATOR7" Width="100%" runat="server" ErrorMessage="(City origin) Column" Display="None"
                                            ControlToValidate="txtColumn"></asp:RequiredFieldValidator>
                                        <asp:Label ID="Label8" runat="server" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:Label>
                                        (City origin) Column</td>
                                    <td>
                                        <cc1:msTextBox ID="txtColumn" runat="server"
                                            Width="150px" CssClass="textFieldRightAlign" NumberPrecision="9" TextMaskType="msNumeric"
                                            NumberMinValue="0" NumberMaxValue="999999999" onchange="validateColumn(this)"></cc1:msTextBox>

                                        <asp:RequiredFieldValidator ID="REQUIREDFIELDVALIDATOR8" Width="100%" runat="server" ErrorMessage="(City origin) Row" Display="None"
                                            ControlToValidate="txtRow"></asp:RequiredFieldValidator>
                                        <asp:Label ID="Label9" runat="server" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:Label>
                                        Row
                                <cc1:msTextBox ID="txtRow" runat="server"
                                    Width="150px" CssClass="textFieldRightAlign" NumberPrecision="9" TextMaskType="msNumeric"
                                    NumberMinValue="0" NumberMaxValue="999999999" onchange="validateRow(this)"></cc1:msTextBox>
                                    </td>
                                </tr>
                                <tr style="font-size: 11px; height: 30px;">
                                    <td>
                                        <asp:RequiredFieldValidator ID="REQUIREDFIELDVALIDATOR9" Width="100%" runat="server" ErrorMessage="(City Destination) Row" Display="None"
                                            ControlToValidate="txtEndRow"></asp:RequiredFieldValidator>
                                        <asp:Label ID="Label10" runat="server" CssClass="errorMsgColor">&nbsp;*&nbsp;</asp:Label>
                                        (City Destination) Row</td>
                                    <td>
                                        <cc1:msTextBox ID="txtEndRow" runat="server"
                                            Width="150px" CssClass="textFieldRightAlign" NumberPrecision="9" TextMaskType="msNumeric"
                                            NumberMinValue="0" NumberMaxValue="999999999" onchange="validateRow(this)"></cc1:msTextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="height: 30px;">
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DataGrid ID="dgBZRates" runat="server" Width="800px" AllowPaging="False" OnEditCommand="dgBZRates_Edit" OnCancelCommand="dgBZRates_Cancel" OnUpdateCommand="dgBZRates_Update" OnDeleteCommand="dgBZRates_Delete" OnItemDataBound="dgBZRates_Bound" OnPageIndexChanged="dgBZRates_PageChange" OnItemCommand="dgBZRates_Button" AutoGenerateColumns="False" ItemStyle-Height="20" PageSize="300">
                                <ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderStyle CssClass="gridHeading"></HeaderStyle>
                                        <ItemStyle Wrap="False" CssClass="gridField"></ItemStyle>
                                        <ItemTemplate>
                                            &nbsp;&nbsp;
							<asp:ImageButton CommandName="Select" ID="imgSelect" runat="server" ImageUrl="images/butt-select.gif" Enabled="False"></asp:ImageButton>&nbsp;&nbsp;
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
                                        CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;"
                                        EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;" ValidationGroup="GenValidationGroup">
                                        <HeaderStyle CssClass="gridHeading"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:EditCommandColumn>
                                    <asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
                                        <HeaderStyle CssClass="gridHeading"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:ButtonColumn>
                                    <asp:TemplateColumn HeaderText="Origin Zone Code">
                                        <HeaderStyle Font-Bold="True" Width="140px" CssClass="gridHeading"></HeaderStyle>
                                        <ItemStyle CssClass="gridField"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="gridLabel" ID="lblOZCode" Text='<%#DataBinder.Eval(Container.DataItem,"origin_zone_code")%>' runat="server">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <cc1:msTextBox CssClass="gridTextBox" ID="txtOZCode" Text='<%#DataBinder.Eval(Container.DataItem,"origin_zone_code")%>' runat="server" Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore"
                                                MaxLength="12" onclick="return false;" onpaste="return false;" onkeydown="return false;" onkeypress="return false;"
                                                ValidationGroup="GenValidationGroup">
                                            </cc1:msTextBox>
                                            <asp:RequiredFieldValidator ID="ozcValidator" runat="server" BorderWidth="0" Display="None" ControlToValidate="txtOZCode" ErrorMessage="Origin Zone Code "></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="OriginSearch">
                                        <HeaderStyle CssClass="gridHeading"></HeaderStyle>
                                        <ItemStyle CssClass="gridField"></ItemStyle>
                                    </asp:ButtonColumn>
                                    <asp:TemplateColumn HeaderText="Destination Zone Code">
                                        <HeaderStyle Font-Bold="True" Width="160px" CssClass="gridHeading"></HeaderStyle>
                                        <ItemStyle CssClass="gridField"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="gridLabel" ID="lblDZCode" Text='<%#DataBinder.Eval(Container.DataItem,"destination_zone_code")%>' runat="server">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <cc1:msTextBox CssClass="gridTextBox" ID="txtDZCode" Text='<%#DataBinder.Eval(Container.DataItem,"destination_zone_code")%>' runat="server"
                                                Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" onclick="return false;" onpaste="return false;"
                                                onkeydown="return false;" onkeypress="return false;" ValidationGroup="GenValidationGroup">
                                            </cc1:msTextBox>
                                            <asp:RequiredFieldValidator ID="odcValidator" runat="server" BorderWidth="0" Display="None" ControlToValidate="txtDZCode" ErrorMessage="Destination Zone Code "></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="DestinationSearch">
                                        <HeaderStyle CssClass="gridHeading"></HeaderStyle>
                                        <ItemStyle CssClass="gridField"></ItemStyle>
                                    </asp:ButtonColumn>
                                    <asp:TemplateColumn HeaderText="Service Type">
                                        <HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
                                        <ItemStyle CssClass="gridField"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="gridLabel" ID="lblServiceCode" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' runat="server">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <cc1:msTextBox CssClass="gridTextBox" ID="txtServiceCode" Text='<%#DataBinder.Eval(Container.DataItem,"service_code")%>' runat="server"
                                                Enabled="True" TextMaskType="msUpperAlfaNumericWithUnderscore" MaxLength="12" onclick="return false;" onpaste="return false;"
                                                onkeydown="return false;" onkeypress="return false;" ValidationGroup="GenValidationGroup">
                                            </cc1:msTextBox>
                                            <asp:RequiredFieldValidator ID="ScValidator" runat="server" BorderWidth="0" Display="None" ControlToValidate="txtServiceCode" ErrorMessage="Service Type "></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" CommandName="ServiceCodeSearch">
                                        <HeaderStyle CssClass="gridHeading"></HeaderStyle>
                                        <ItemStyle CssClass="gridField"></ItemStyle>
                                    </asp:ButtonColumn>
                                    <asp:TemplateColumn HeaderText="Start WT">
                                        <HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
                                        <ItemStyle CssClass="gridField"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="gridLabelNumber" ID="lblStartWt" Text='<%#DataBinder.Eval(Container.DataItem,"start_wt","{0:#0}")%>' runat="server" Enabled="True">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <cc1:msTextBox CssClass="gridTextBoxNumber" ID="txtStartWt" Text='<%#DataBinder.Eval(Container.DataItem,"start_wt")%>' runat="server"
                                                MaxLength="6" TextMaskType="msNumeric" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="5" NumberScale="1"
                                                onpaste="return false;" ValidationGroup="GenValidationGroup">
                                            </cc1:msTextBox>
                                            <asp:RequiredFieldValidator ID="rfTxtStartWt" runat="server" BorderWidth="0" Display="None" ControlToValidate="txtStartWt" ErrorMessage="Start WT "></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="End WT">
                                        <HeaderStyle Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
                                        <ItemStyle CssClass="gridField"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="gridLabelNumber" ID="lblEndWt" Text='<%#DataBinder.Eval(Container.DataItem,"end_wt","{0:#0}")%>' runat="server" Enabled="True">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <cc1:msTextBox CssClass="gridTextBoxNumber" ID="txtEndWt" Text='<%#DataBinder.Eval(Container.DataItem,"end_wt")%>' runat="server"
                                                MaxLength="6" TextMaskType="msNumeric" NumberMaxValue="9999" NumberMinValue="0" NumberPrecision="5" NumberScale="1" onpaste="return false;"
                                                ValidationGroup="GenValidationGroup">
                                            </cc1:msTextBox>
                                            <asp:RequiredFieldValidator ID="rfTxtEndWt" runat="server" BorderWidth="0" Display="None" ControlToValidate="txtEndWt" ErrorMessage="End WT "></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Start Price">
                                        <HeaderStyle Font-Bold="True" Width="90px" CssClass="gridHeading"></HeaderStyle>
                                        <ItemStyle CssClass="gridField"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="gridLabel" Width="50px" ID="lblSPrice" Text='<%#DataBinder.Eval(Container.DataItem,"start_price","{0:n}")%>' runat="server" Enabled="True">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <cc1:msTextBox CssClass="gridTextBoxNumber" ID="txtSPrice" Text='<%#DataBinder.Eval(Container.DataItem,"start_price","{0:n}")%>' runat="server"
                                                MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2"
                                                onpaste="return false;" ValidationGroup="GenValidationGroup">
                                            </cc1:msTextBox>
                                            <asp:RequiredFieldValidator ID="rfTxtSPrice" runat="server" BorderWidth="0" Display="None" ControlToValidate="txtSPrice" ErrorMessage="Start Price "></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Increment Price">
                                        <HeaderStyle Font-Bold="True" Width="90px" CssClass="gridHeading"></HeaderStyle>
                                        <ItemStyle CssClass="gridField"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="gridLabelNumber" ID="lblIPrice" Text='<%#DataBinder.Eval(Container.DataItem,"increment_price","{0:n}")%>' runat="server" Enabled="True">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <cc1:msTextBox CssClass="gridTextBoxNumber" ID="txtIPrice" Text='<%#DataBinder.Eval(Container.DataItem,"increment_price","{0:n}")%>'
                                                runat="server" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2"
                                                onpaste="return false;" ValidationGroup="GenValidationGroup">
                                            </cc1:msTextBox>
                                            <asp:RequiredFieldValidator ID="rfTxtIPrice" runat="server" BorderWidth="0" Display="None" ControlToValidate="txtIPrice" ErrorMessage="Increment Price "></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Effective Date">
                                        <HeaderStyle Font-Bold="True" Width="90px" CssClass="gridHeading"></HeaderStyle>
                                        <ItemStyle CssClass="gridField" HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label CssClass="gridLabel" ID="lblEffectivedate" Text='<%#DataBinder.Eval(Container.DataItem,"effective_date","{0:dd/MM/yyyy}")%>' runat="server" Enabled="True">
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <cc1:msTextBox ID="txtEffectiveDate" MaxLength="10" TextMaskString="99/99/9999" TextMaskType="msDate" Text='<%#DataBinder.Eval(Container.DataItem,"effective_date","{0:dd/MM/yyyy}")%>'
                                                runat="server" ValidationGroup="GenValidationGroup">
                                            </cc1:msTextBox>
                                            <asp:RequiredFieldValidator ID="rfTxtEffectiveDate" runat="server" BorderWidth="0" Display="None" ControlToValidate="txtEffectiveDate" ErrorMessage="Effective Date "></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <%--<PagerStyle VerticalAlign="Top" NextPageText="Next" Height="40px" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>--%>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
            <ProgressTemplate>
                <div class="overlayLoading">
                    <div style="z-index: 500; margin-left: 40%; margin-top: 200px; opacity: 1; -moz-opacity: 1;">
                        <asp:Image ID="Image1" ImageUrl="~/WebUI/images/loading.gif" runat="server" />
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:Label ID="lblErrorMessage" Style="z-index: 102; left: 37px; position: absolute; top: 97px" runat="server" CssClass="errorMsgColor" Height="35px" Width="540px">Place holder for err msg</asp:Label>
        <asp:ValidationSummary ID="ValidationSummary1" Style="z-index: 103; left: 72px; position: absolute; top: 103px" Height="36px" Width="346px" ShowSummary="False" DisplayMode="BulletList" ShowMessageBox="True" HeaderText="The following field(s) are required:" runat="server"></asp:ValidationSummary>
    </form>
</body>
</html>
