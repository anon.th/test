<%@ Register TagPrefix="dbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="VersusReport.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.VersusReport" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>ServiceFailure</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
	</HEAD>
	<body onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="ServiceFailure" method="post" runat="server">
			<asp:button id="btnQuery" style="Z-INDEX: 101; LEFT: 16px; POSITION: absolute; TOP: 53px" tabIndex="100" runat="server" Text="Query" Width="64px" CssClass="queryButton"></asp:button>
			<FIELDSET style="Z-INDEX: 108; LEFT: 322px; WIDTH: 299px; POSITION: absolute; TOP: 248px; HEIGHT: 78px"><LEGEND><asp:label id="Label9" Runat="server">Route Code</asp:label></LEGEND>
				<TABLE id="Table3" style="WIDTH: 291px; HEIGHT: 59px" runat="server">
					<TR>
						<TD style="HEIGHT: 14px" colSpan="2"><asp:radiobuttonlist id="rdlDeliveryType" runat="server" CssClass="tableRadioButton" AutoPostBack="True" RepeatDirection="Horizontal">
								<asp:ListItem Value="L">Long Route</asp:ListItem>
								<asp:ListItem Value="S" Selected="True">Short Route</asp:ListItem>
								<asp:ListItem Value="A">Air Route</asp:ListItem>
							</asp:radiobuttonlist></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 14px" width="30"><asp:label id="Label10" runat="server" Width="100px" CssClass="tableLabel" Height="22px">Route Code</asp:label></TD>
						<TD style="HEIGHT: 14px" align="left"><dbcombo:dbcombo id="DbcmbRoutecode" tabIndex="13" runat="server" Width="91px" AutoPostBack="True" Height="17px" TextBoxColumns="12" ServerMethod="DbComboPathCodeSelect" TextUpLevelSearchButton="v" ShowDbComboLink="False"></dbcombo:dbcombo></TD>
					</TR>
				</TABLE>
			</FIELDSET>
			<FIELDSET style="Z-INDEX: 107; LEFT: 8px; WIDTH: 613px; POSITION: absolute; TOP: 339px; HEIGHT: 258px"><LEGEND><asp:label id="Label7" Runat="server">Status Versus</asp:label></LEGEND>
				<TABLE id="Table2" style="WIDTH: 608px; HEIGHT: 144px" runat="server">
					<TR height="14">
						<TD colSpan="20"><asp:checkbox id="chkMissFirstStatus" runat="server" Text="Report Exception if missing first status code below" CssClass="tableRadioButton"></asp:checkbox></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 14px" colSpan="4"><asp:label id="Label6" runat="server" Width="100px" CssClass="tableLabel" Height="22px">Status Code</asp:label></TD>
						<TD style="HEIGHT: 14px" colSpan="6"><dbcombo:dbcombo id="dbCmbStatusCode1" tabIndex="13" runat="server" Width="91px" AutoPostBack="True" Height="17px" TextBoxColumns="12" ServerMethod="StatusCodeServerMethod" TextUpLevelSearchButton="v" ShowDbComboLink="False"></dbcombo:dbcombo></TD>
						<TD style="HEIGHT: 14px" align="left" colSpan="5"><cc1:mstextbox id="txtDesStatus1" tabIndex="14" runat="server" Width="150px" CssClass="textField"></cc1:mstextbox></TD>
						<TD style="HEIGHT: 14px" colSpan="5"></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 14px" colSpan="4"><asp:label id="Label3" runat="server" Width="100px" CssClass="tableLabel" Height="22px">Status Code</asp:label></TD>
						<TD style="HEIGHT: 14px" colSpan="6"><dbcombo:dbcombo id="dbCmbStatusCode2" tabIndex="13" runat="server" Width="91px" AutoPostBack="True" Height="17px" TextBoxColumns="12" ServerMethod="StatusCodeServerMethod" TextUpLevelSearchButton="v" ShowDbComboLink="False"></dbcombo:dbcombo></TD>
						<TD style="HEIGHT: 14px" align="left" colSpan="5"><FONT face="Tahoma"><cc1:mstextbox id="txtDesStatus2" tabIndex="14" runat="server" Width="150px" CssClass="textField"></cc1:mstextbox></FONT></TD>
						<TD style="HEIGHT: 14px" colSpan="5"></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 14px" colSpan="4"><asp:label id="Label4" runat="server" Width="100px" CssClass="tableLabel" Height="22px">Status Code</asp:label></TD>
						<TD style="HEIGHT: 14px" colSpan="6"><FONT face="Tahoma"><dbcombo:dbcombo id="dbCmbStatusCode3" tabIndex="13" runat="server" Width="91px" AutoPostBack="True" Height="17px" TextBoxColumns="12" ServerMethod="StatusCodeServerMethod" TextUpLevelSearchButton="v" ShowDbComboLink="False"></dbcombo:dbcombo></FONT></TD>
						<TD style="HEIGHT: 14px" align="left" colSpan="5"><cc1:mstextbox id="txtDesStatus3" tabIndex="14" runat="server" Width="150px" CssClass="textField"></cc1:mstextbox></TD>
						<TD style="HEIGHT: 14px" colSpan="5"></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 14px" colSpan="4"><asp:label id="Label5" runat="server" Width="100px" CssClass="tableLabel" Height="22px">Status Code</asp:label></TD>
						<TD style="HEIGHT: 14px" colSpan="6"><FONT face="Tahoma"><dbcombo:dbcombo id="dbCmbStatusCode4" tabIndex="13" runat="server" Width="91px" AutoPostBack="True" Height="17px" TextBoxColumns="12" ServerMethod="StatusCodeServerMethod" TextUpLevelSearchButton="v" ShowDbComboLink="False"></dbcombo:dbcombo></FONT></TD>
						<TD style="HEIGHT: 14px" align="left" colSpan="5"><cc1:mstextbox id="txtDesStatus4" tabIndex="14" runat="server" Width="150px" CssClass="textField"></cc1:mstextbox></TD>
						<TD style="HEIGHT: 14px" colSpan="5"></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 14px" colSpan="4"><asp:label id="Label8" runat="server" Width="100px" CssClass="tableLabel" Height="22px">Status Code</asp:label></TD>
						<TD style="HEIGHT: 14px" colSpan="6"><dbcombo:dbcombo id="dbCmbStatusCode5" tabIndex="13" runat="server" Width="91px" AutoPostBack="True" Height="17px" TextBoxColumns="12" ServerMethod="StatusCodeServerMethod" TextUpLevelSearchButton="v" ShowDbComboLink="False"></dbcombo:dbcombo></TD>
						<TD style="HEIGHT: 14px" align="left" colSpan="5"><cc1:mstextbox id="txtDesStatus5" tabIndex="14" runat="server" Width="150px" CssClass="textField"></cc1:mstextbox></TD>
						<TD style="HEIGHT: 14px" colSpan="5"></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 14px" colSpan="4"><asp:label id="Label11" runat="server" Width="100px" CssClass="tableLabel" Height="22px">Status Code</asp:label></TD>
						<TD style="HEIGHT: 14px" colSpan="6"><dbcombo:dbcombo id="dbCmbStatusCode6" tabIndex="13" runat="server" Width="91px" AutoPostBack="True" Height="17px" TextBoxColumns="12" ServerMethod="StatusCodeServerMethod" TextUpLevelSearchButton="v" ShowDbComboLink="False"></dbcombo:dbcombo></TD>
						<TD style="HEIGHT: 14px" align="left" colSpan="5"><cc1:mstextbox id="txtDesStatus6" tabIndex="14" runat="server" Width="150px" CssClass="textField"></cc1:mstextbox></TD>
						<TD style="HEIGHT: 14px" colSpan="5"></TD>
					</TR>
					<TR>
						<TD style="HEIGHT: 14px" colSpan="4"><asp:label id="Label12" runat="server" Width="100px" CssClass="tableLabel" Height="22px">Status Code</asp:label></TD>
						<TD style="HEIGHT: 14px" colSpan="6"><dbcombo:dbcombo id="dbCmbStatusCode7" tabIndex="13" runat="server" Width="91px" AutoPostBack="True" Height="17px" TextBoxColumns="12" ServerMethod="StatusCodeServerMethod" TextUpLevelSearchButton="v" ShowDbComboLink="False"></dbcombo:dbcombo></TD>
						<TD style="HEIGHT: 14px" align="left" colSpan="5"><cc1:mstextbox id="txtDesStatus7" tabIndex="14" runat="server" Width="150px" CssClass="textField"></cc1:mstextbox></TD>
						<TD style="HEIGHT: 14px" colSpan="5"></TD>
					</TR>
					<TR height="27">
						<TD colSpan="20"></TD>
					</TR>
				</TABLE>
			</FIELDSET>
			<fieldset style="Z-INDEX: 104; LEFT: 8px; WIDTH: 613px; POSITION: absolute; TOP: 112px; HEIGHT: 129px"><legend><asp:label id="Label1" Runat="server">Booking Date</asp:label></legend>
				<TABLE id="tblDates" style="WIDTH: 608px; HEIGHT: 110px" align="left" border="0" runat="server">
					<TR height="14">
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
						<TD width="5%"></TD>
					</TR>
					<TR height="27">
						<TD colSpan="3"><asp:radiobutton id="rbMonth" tabIndex="1" runat="server" Text="Monthly" Width="100%" CssClass="tableRadioButton" AutoPostBack="True" Height="22px" Checked="True" GroupName="EnterDate"></asp:radiobutton></TD>
						<TD colSpan="5"><asp:dropdownlist id="ddMonth" tabIndex="2" runat="server" Width="100%" CssClass="textField"></asp:dropdownlist></TD>
						<td colSpan="2"></td>
						<TD align="left" colSpan="2"><asp:label id="lblYear" runat="server" Width="100%" CssClass="tableLabel" Height="22px">Year</asp:label></TD>
						<TD colSpan="3"><cc1:mstextbox id="txtYear" tabIndex="3" runat="server" Width="100%" CssClass="textField" NumberPrecision="4" NumberMinValue="1" NumberMaxValue="2999" TextMaskType="msNumeric" MaxLength="5"></cc1:mstextbox></TD>
						<TD colSpan="5"></TD>
					</TR>
					<TR height="27">
						<TD colSpan="3"><asp:radiobutton id="rbPeriod" tabIndex="4" runat="server" Text="Period" Width="100%" CssClass="tableRadioButton" AutoPostBack="True" Height="22px" GroupName="EnterDate"></asp:radiobutton></TD>
						<TD colSpan="5"><cc1:mstextbox id="txtPeriod" tabIndex="5" runat="server" Width="100%" CssClass="textField" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
						<td colSpan="2"></td>
						<TD align="left" colSpan="2"><asp:label id="lblTo" runat="server" Width="100%" CssClass="tableLabel" Height="22px">To</asp:label></TD>
						<TD colSpan="3"><cc1:mstextbox id="txtTo" tabIndex="6" runat="server" Width="100%" CssClass="textField" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999"></cc1:mstextbox></TD>
						<TD colSpan="5"></TD>
					</TR>
					<TR height="27">
						<TD colSpan="3"><asp:radiobutton id="rbDate" tabIndex="7" runat="server" Text="Daily" Width="100%" CssClass="tableRadioButton" AutoPostBack="True" Height="22px" GroupName="EnterDate"></asp:radiobutton></TD>
						<TD colSpan="5"><cc1:mstextbox id="txtDate" tabIndex="8" runat="server" Width="100%" CssClass="textField" TextMaskType="msDate" MaxLength="10" TextMaskString="99/99/9999 99:99"></cc1:mstextbox></TD>
						<TD colSpan="12"></TD>
					</TR>
				</TABLE>
			</fieldset>
			<fieldset style="Z-INDEX: 106; LEFT: 12px; WIDTH: 306px; POSITION: absolute; TOP: 248px; HEIGHT: 46px"><legend><asp:label id="Label2" Runat="server">History Type</asp:label></legend>
				<table id="table1" style="WIDTH: 242px; HEIGHT: 58px" runat="server">
					<TR>
						<TD colSpan="4"><asp:radiobuttonlist id="rblHistoryType" runat="server" CssClass="tableRadioButton" AutoPostBack="True" RepeatDirection="Horizontal">
								<asp:ListItem Value="D" Selected="True">Domestic</asp:ListItem>
								<asp:ListItem Value="P">Pickup</asp:ListItem>
							</asp:radiobuttonlist></TD>
					</TR>
				</table>
			</fieldset>
			<asp:label id="lblErrorMessage" style="Z-INDEX: 103; LEFT: 16px; POSITION: absolute; TOP: 80px" runat="server" Width="616px" CssClass="errorMsgColor" Height="30px">Place holder for err msg</asp:label><asp:label id="lblTitle" style="Z-INDEX: 102; LEFT: 15px; POSITION: absolute; TOP: 16px" runat="server" Width="558px" CssClass="maintitleSize" Height="32px"> Versus Report</asp:label><asp:button id="btnGenerate" style="Z-INDEX: 100; LEFT: 81px; POSITION: absolute; TOP: 53px" runat="server" Text="Generate" Width="82px" CssClass="queryButton"></asp:button></form>
	</body>
</HTML>
