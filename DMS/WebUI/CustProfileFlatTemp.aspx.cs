using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CustProfileFlatTemp.
	/// </summary>
	public class CustProfileFlatTemp : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.RequiredFieldValidator validateAccNo;
		protected System.Web.UI.WebControls.Label lblAccNo;
		protected System.Web.UI.WebControls.TextBox txtAccNo;
		protected System.Web.UI.WebControls.Label lblRefNo;
		protected System.Web.UI.WebControls.TextBox txtRefNo;
		protected System.Web.UI.WebControls.Label lblStatus;
		protected System.Web.UI.WebControls.TextBox txtStatus;
		protected System.Web.UI.WebControls.Label lblCustomerName;
		protected System.Web.UI.WebControls.TextBox txtCustomerName;
		protected System.Web.UI.WebControls.RequiredFieldValidator Requiredfieldvalidator2;
		protected System.Web.UI.WebControls.Label lblContactPerson;
		protected System.Web.UI.WebControls.TextBox txtContactPerson;
		protected System.Web.UI.WebControls.Label lblAddress1;
		protected System.Web.UI.WebControls.TextBox txtAddress1;
		protected System.Web.UI.WebControls.Label lblTelphone;
		protected System.Web.UI.WebControls.TextBox txtTelephone;
		protected System.Web.UI.WebControls.Label lblAddress2;
		protected System.Web.UI.WebControls.TextBox txtAddress2;
		protected System.Web.UI.WebControls.Label lblFax;
		protected System.Web.UI.WebControls.TextBox txtFax;
		protected System.Web.UI.WebControls.RequiredFieldValidator validCustZip;
		protected System.Web.UI.WebControls.Label lblZip;
		protected System.Web.UI.WebControls.TextBox txtZipCode;
		protected System.Web.UI.WebControls.Button btnZipcodePopup;
		protected System.Web.UI.WebControls.Label lblState;
		protected System.Web.UI.WebControls.TextBox txtState;
		protected System.Web.UI.WebControls.Label lblEmail;
		protected System.Web.UI.WebControls.TextBox txtEmail;
		protected System.Web.UI.WebControls.RequiredFieldValidator Requiredfieldvalidator3;
		protected System.Web.UI.WebControls.Label lblCountry;
		protected System.Web.UI.WebControls.TextBox txtCountry;
		protected System.Web.UI.WebControls.RequiredFieldValidator Requiredfieldvalidator1;
		protected System.Web.UI.WebControls.Label lblCreditTerm;
		protected System.Web.UI.WebControls.TextBox txtCreditTerm;
		protected System.Web.UI.WebControls.Label lblCreditLimit;
		protected System.Web.UI.WebControls.TextBox txtCreditLimit;
		protected System.Web.UI.WebControls.Label lblCreditOutstanding;
		protected System.Web.UI.WebControls.TextBox txtCreditOutstanding;
		protected System.Web.UI.WebControls.Label lblPaymentMode;
		protected System.Web.UI.WebControls.RadioButton radioBtnCash;
		protected System.Web.UI.WebControls.RadioButton radioBtnCredit;
		protected System.Web.UI.WebControls.Label lblMBG;
		protected System.Web.UI.WebControls.TextBox txtMBG;
		protected System.Web.UI.WebControls.Label lblActiveQuatationNo;
		protected System.Web.UI.WebControls.TextBox txtActiveQuatationNo;
		protected System.Web.UI.WebControls.Button btnViewAll;
		protected System.Web.UI.WebControls.Button btnRefInsert;
		protected System.Web.UI.WebControls.Button btnRefSave;
		protected System.Web.UI.WebControls.Button btnRefDelete;
		protected System.Web.UI.WebControls.Button btnFirstRef;
		protected System.Web.UI.WebControls.Button btnPreviousRef;
		protected System.Web.UI.WebControls.TextBox txtRefJumpCount;
		protected System.Web.UI.WebControls.Button btnNextRef;
		protected System.Web.UI.WebControls.Button btnLastRef;
		protected System.Web.UI.HtmlControls.HtmlTable CustomerInfoTable;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
