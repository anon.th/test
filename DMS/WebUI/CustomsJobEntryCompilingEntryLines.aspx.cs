using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.common.DAL;
using com.common.applicationpages;
using com.ties.DAL;
using com.ties.classes;
using System.Text;
using System.Configuration;
using System.Data.OleDb;
using Cambro.Web.DbCombo;
using TIESDAL; 

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for CustomsJobEntryCompilingEntryLines.
	/// </summary>
	public class CustomsJobEntryCompilingEntryLines : BasePopupPage
	{
		String strEnterpriseID=null;
		String strAppID=null;
		String strUserLoggin=null; 
 		
		public string GridCmd
		{
			get
			{
				if(ViewState["GridCmd"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["GridCmd"];
				}
			}
			set
			{
				ViewState["GridCmd"]=value;
			}
		}

		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnClose;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.WebControls.DataGrid gvJobEntryLines;
		protected System.Web.UI.WebControls.DataGrid gvMarksAndNo;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Button btnExecTariffCodeFooter;
		protected System.Web.UI.WebControls.Button btnExecTariffCodeEdit;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;

		public DataSet dsAllResult
		{
			get
			{
				if(ViewState["dsAllResult"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsAllResult"];
				}
			}
			set
			{
				ViewState["dsAllResult"]=value;
			}
		}

		public DataTable dtEditResult
		{
			get
			{
				if(ViewState["dtEditResult"] == null)
				{
					return new DataTable();
				}
				else
				{
					return (DataTable)ViewState["dtEditResult"];
				}
			}
			set
			{
				ViewState["dtEditResult"]=value;
			}
		}
 
		public DataSet dsCountry
		{
			get
			{
				if(ViewState["dsCountry"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsCountry"];
				}
			}
			set
			{
				ViewState["dsCountry"]=value;
			}
		}

		public DataSet dsCurrency
		{
			get
			{
				if(ViewState["dsCurrency"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsCurrency"];
				}
			}
			set
			{
				ViewState["dsCurrency"]=value;
			}
		}

		public DataSet dsModel
		{
			get
			{
				if(ViewState["dsModel"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsModel"];
				}
			}
			set
			{
				ViewState["dsModel"]=value;
			}
		}

		public DataSet dsdeclaration
		{
			get
			{
				if(ViewState["dsdeclaration"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsdeclaration"];
				}
			}
			set
			{
				ViewState["dsdeclaration"]=value;
			}
		}

		public DataSet dsUOM
		{
			get
			{
				if(ViewState["dsUOM"] == null)
				{
					return new DataSet();
				}
				else
				{
					return (DataSet)ViewState["dsUOM"];
				}
			}
			set
			{
				ViewState["dsUOM"]=value;
			}
		}

		public string strJobEntryNo
		{
			get
			{
				if(ViewState["strJobEntryNo"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["strJobEntryNo"];
				}
			}
			set
			{
				ViewState["strJobEntryNo"]=value;
			}
		}
		public string consignment_no
		{
			get
			{
				if(ViewState["consignment_no"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["consignment_no"];
				}
			}
			set
			{
				ViewState["consignment_no"]=value;
			}
		}
		public string countryCode
		{
			get
			{
				if(ViewState["countryCode"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["countryCode"];
				}
			}
			set
			{
				ViewState["countryCode"]=value;
			}
		}
		public string currencyCode
		{
			get
			{
				if(ViewState["currencyCode"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["currencyCode"];
				}
			}
			set
			{
				ViewState["currencyCode"]=value;
			}
		}
		public string Models
		{
			get
			{
				if(ViewState["Models"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["Models"];
				}
			}
			set
			{
				ViewState["Models"]=value;
			}
		}
		public string Declaretion
		{
			get
			{
				if(ViewState["Declaretion"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["Declaretion"];
				}
			}
			set
			{
				ViewState["Declaretion"]=value;
			}
		}
		public string CustomerID
		{
			get
			{
				if(ViewState["CustomerID"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["CustomerID"];
				}
			}
			set
			{
				ViewState["CustomerID"]=value;
			}
		}
 
		public string strFormatTariff
		{
			get
			{
				if(ViewState["strFormatTariff"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["strFormatTariff"];
				}
			}
			set
			{
				ViewState["strFormatTariff"]=value;
			}
		}
		public string strDeleteSeq
		{
			get
			{
				if(ViewState["strDeleteSeq"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["strDeleteSeq"];
				}
			}
			set
			{
				ViewState["strDeleteSeq"]=value;
			}
		}
 
		public Int16 indLine
		{
			get
			{
				if(ViewState["indLine"] == null)
				{
					return 0;
				}
				else
				{
					return (Int16)ViewState["indLine"];
				}
			}
			set
			{
				ViewState["indLine"]=value;
			}
		}

		public int Amount_required
		{
			get
			{
				if(ViewState["Amount_required"] == null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["Amount_required"];
				}
			}
			set
			{
				ViewState["Amount_required"]=value;
			}
		}

		public int Tariff_qty_required
		{
			get
			{
				if(ViewState["Tariff_qty_required"] == null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["Tariff_qty_required"];
				}
			}
			set
			{
				ViewState["Tariff_qty_required"]=value;
			}
		}

		public int Suppl_qty_required
		{
			get
			{
				if(ViewState["Suppl_qty_required"] == null)
				{
					return 0;
				}
				else
				{
					return (int)ViewState["Suppl_qty_required"];
				}
			}
			set
			{
				ViewState["Suppl_qty_required"]=value;
			}
		}


		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,HttpContext.Current.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();
			strUserLoggin = utility.GetUserID();

			strAppID = Request.QueryString["appID"].ToString();
			strEnterpriseID = Request.QueryString["enterpriseID"].ToString();
			strUserLoggin = Request.QueryString["userID"].ToString(); 
			if(!Page.IsPostBack)
			{				
				//clearscreen();
				GridCmd=null;
				if(Request.QueryString["payerid"] != null)
				{
					CustomerID =Request.QueryString["payerid"].ToString();
				}
				if(Request.QueryString["consignment_no"] != null)
				{
					consignment_no =Request.QueryString["consignment_no"].ToString();
				}
				if(Request.QueryString["JobEntryNo"] != null)
				{
					strJobEntryNo =Request.QueryString["JobEntryNo"].ToString();
				} 
				if(Request.QueryString["countryCode"] != null)
				{
					countryCode =Request.QueryString["countryCode"].ToString();
				}
				if(Request.QueryString["currencyCode"] != null)
				{
					currencyCode =Request.QueryString["currencyCode"].ToString();
				}
				if(Request.QueryString["Models"] != null)
				{
					Models =Request.QueryString["Models"].ToString();
				}
				if(Request.QueryString["Declaretion"] != null)
				{
					Declaretion =Request.QueryString["Declaretion"].ToString();
				}
				//strUrl+="&Models="+ddlModelOfDeclaration.SelectedValue+"&Declaretion="+ddlCustomsProcedureCode.SelectedValue;
				//SetInitialFocus(InfoReceivedDate);

				dsCurrency =   CustomJobCompilingDAL.GetCurrencies(strAppID, strEnterpriseID);  
	
				dsCountry =   CustomJobCompilingDAL.GetCountry(strAppID, strEnterpriseID);  
	 
				dsModel = CustomJobCompilingDAL.GetDropDownListDisplayValue(strAppID ,strEnterpriseID,"ModelOfDeclaration"); 
	
				dsdeclaration = CustomJobCompilingDAL.GetDropDownListDisplayValue(strAppID ,strEnterpriseID,"CustomsProcedureCode");

				dsUOM = CustomJobCompilingDAL.GetUOM(strAppID ,strEnterpriseID );

				ExecQry();
				try
				{
					DataSet dsFormat =   CustomJobCompilingDAL.GetFormatTariff(strAppID, strEnterpriseID); 
					if (dsFormat.Tables[0].Rows[0]["key"].ToString() == "GridRows")
					{
						gvJobEntryLines.PageSize = Convert.ToInt16( dsFormat.Tables[0].Rows[0]["value"].ToString());;
					} 
					else
					{
						gvJobEntryLines.PageSize = 25;
					}
					strFormatTariff =  dsFormat.Tables[0].Rows[1]["value"].ToString(); 
				}
				catch
				{}
				TextBox txtAddtariff_code = (TextBox)gvJobEntryLines.Controls[0].Controls[gvJobEntryLines.Controls[0].Controls.Count-1].FindControl("txtAddtariff_code");
				SetInitialFocus(txtAddtariff_code);
			}
		}


		private void ExecQry()
		{
			Amount_required=0;
			Tariff_qty_required=0;
			Suppl_qty_required=0;
			DataSet dsResult = CustomJobCompilingDAL.SearchCustoms_JobEntryLines(strAppID, strEnterpriseID, strUserLoggin,consignment_no, strJobEntryNo, CustomerID );
            ////aw_Khatawut comment 2019-06-20 : DMSUpgrade 2.4.1) After searching Job Status, there should be able to update data on the search result
            //if(Request.QueryString["pagemode"] != null && Request.QueryString["pagemode"].ToString().Trim() == "popup")
            //{
            //	gvJobEntryLines.ShowFooter=false;
            //	gvJobEntryLines.Columns[0].Visible=false;
            //	gvJobEntryLines.Columns[0].Visible=false;
            //	gvJobEntryLines.ShowFooter=false;
            //}
            gvJobEntryLines.DataSource = dsResult.Tables[1]; 
			gvJobEntryLines.DataBind();
			gvJobEntryLines.EditItemIndex = -1; 
			dsAllResult = dsResult;
			dtEditResult = dsResult.Tables[1]; 
			
		}



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			this.btnExecTariffCodeFooter.Click += new System.EventHandler(this.btnExecTariffCodeFooter_Click);
			this.gvJobEntryLines.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.gvJobEntryLines_ItemCommand);
			this.gvJobEntryLines.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.gvJobEntryLines_ItemDataBound);
			this.btnExecTariffCodeEdit.Click += new System.EventHandler(this.btnExecTariffCodeEdit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void gvJobEntryLines_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if((e.Item.ItemType == ListItemType.Footer))
			{
				Label lblDescription1 = (Label)e.Item.FindControl("lblDescription1");
				DropDownList ddlAddModelOfDeclaration = (DropDownList)e.Item.FindControl("ddlAddModelOfDeclaration");
				DropDownList ddlAddCustomsProcedureCode = (DropDownList)e.Item.FindControl("ddlAddCustomsProcedureCode");
				//DropDownList ddlAddDescription2 = (DropDownList)e.Item.FindControl("ddlAddDescription2");
				DropDownList ddlAddOriginCountry = (DropDownList)e.Item.FindControl("ddlAddOriginCountry");
				DropDownList ddlAddCurrencyCode = (DropDownList)e.Item.FindControl("ddlAddCurrencyCode");
				DropDownList ddlAddUnitsOfMeasure = (DropDownList)e.Item.FindControl("ddlAddUnitsOfMeasure");//  (DropDownList)e.Item.FindControl("ddlChargeCurrencyCode");
				if (ddlAddModelOfDeclaration != null)
				{   
					//indLine=(short)e.Item.ItemIndex;
					ddlAddOriginCountry.SelectedIndexChanged += new System.EventHandler(this.ddlAddOriginCountry_SelectedIndexChanged);
					ddlAddOriginCountry.DataSource = dsCountry;
					ddlAddOriginCountry.DataTextField="CountryCode";
					ddlAddOriginCountry.DataValueField="CountryCode";					
					ddlAddOriginCountry.DataBind(); 
					ddlAddOriginCountry.SelectedValue = countryCode;

					ddlAddCurrencyCode.DataSource = dsCurrency;
					ddlAddCurrencyCode.DataTextField="CurrencyCode";
					ddlAddCurrencyCode.DataValueField="CurrencyCode";					
					ddlAddCurrencyCode.DataBind();
					ddlAddCurrencyCode.SelectedValue = currencyCode;
					
					ddlAddModelOfDeclaration.DataTextField="DropDownListDisplayValue";
					ddlAddModelOfDeclaration.DataValueField="CodedValue";
					ddlAddModelOfDeclaration.DataSource = dsModel;
					ddlAddModelOfDeclaration.DataBind();
					ddlAddModelOfDeclaration.SelectedValue = Models;

					ddlAddCustomsProcedureCode.DataTextField="DropDownListDisplayValue";
					ddlAddCustomsProcedureCode.DataValueField="CodedValue";
					ddlAddCustomsProcedureCode.DataSource = dsdeclaration;
					ddlAddCustomsProcedureCode.DataBind(); 
					ddlAddCustomsProcedureCode.SelectedValue = Declaretion; 

					ddlAddUnitsOfMeasure.DataTextField="DropDownListDisplayValue";
					ddlAddUnitsOfMeasure.DataValueField="CodedValue";
					ddlAddUnitsOfMeasure.DataSource = dsUOM;
					ddlAddUnitsOfMeasure.DataBind();

					//SetInitialFocus(lblDescription1);
				}
				if(GridCmd=="ADD_ITEM")
				{
					GridCmd=null;
					TextBox txtAddtariff_code = (TextBox)e.Item.FindControl("txtAddtariff_code");
					SetInitialFocus(txtAddtariff_code);
				}
			}
			else if((e.Item.ItemType == ListItemType.EditItem))
			{
				DataRowView dr = (DataRowView)e.Item.DataItem;
				DropDownList ddlEditModelOfDeclaration = (DropDownList)e.Item.FindControl("ddlEditModelOfDeclaration");
				DropDownList ddlEditCustomsProcedureCode = (DropDownList)e.Item.FindControl("ddlEditCustomsProcedureCode"); 
				DropDownList ddlEditOriginCountry = (DropDownList)e.Item.FindControl("ddlEditOriginCountry");
				DropDownList ddlEditCurrencyCode = (DropDownList)e.Item.FindControl("ddlEditCurrencyCode");
				DropDownList ddlEditUnitsOfMeasure = (DropDownList)e.Item.FindControl("ddlEditUnitsOfMeasure");//   

				Amount_required=0;
				Tariff_qty_required=0;
				Suppl_qty_required=0;
				TextBox txtEdittariff_code = (TextBox)e.Item.FindControl("txtEdittariff_code");
				TextBox txtEditDescription1 = (TextBox)e.Item.FindControl("txtEditDescription1");
				if(txtEdittariff_code != null)
				{
					txtEditDescription1.Text="";
					DataTable dt =CustomJobCompilingDAL.GetCustomsTariffDescription(strAppID,strEnterpriseID,txtEdittariff_code.Text.Trim());
					if(dt.Rows.Count>0)
					{
						txtEditDescription1.Text=dt.Rows[0]["tariff_description"].ToString();
						Amount_required=Convert.ToInt32(dt.Rows[0]["amount_required"].ToString());
						Tariff_qty_required=Convert.ToInt32(dt.Rows[0]["tariff_qty_required"].ToString());
						Suppl_qty_required=Convert.ToInt32(dt.Rows[0]["suppl_qty_required"].ToString());
					}
				}

				if (ddlEditModelOfDeclaration != null)
				{ 
					//ddlEditOriginCountry.SelectedIndexChanged += new System.EventHandler(this.ddlEditOriginCountry_SelectedIndexChanged);
					ddlEditOriginCountry.DataSource = dsCountry;
					ddlEditOriginCountry.DataTextField="CountryCode";
					ddlEditOriginCountry.DataValueField="CountryCode";					
					ddlEditOriginCountry.DataBind(); 
					ddlEditOriginCountry.SelectedValue = dr["OriginCountry"].ToString();
 

					ddlEditCurrencyCode.DataSource = dsCurrency;
					ddlEditCurrencyCode.DataTextField="CurrencyCode";
					ddlEditCurrencyCode.DataValueField="CurrencyCode";					
					ddlEditCurrencyCode.DataBind();
					ddlEditCurrencyCode.SelectedValue = dr["CurrencyCode"].ToString();

					ddlEditModelOfDeclaration.DataTextField="DropDownListDisplayValue";
					ddlEditModelOfDeclaration.DataValueField="CodedValue";
					ddlEditModelOfDeclaration.DataSource = dsModel;
					ddlEditModelOfDeclaration.DataBind();
					ddlEditModelOfDeclaration.SelectedValue = dr["ModelOfDeclaration"].ToString();
 
					ddlEditCustomsProcedureCode.DataTextField="DropDownListDisplayValue";
					ddlEditCustomsProcedureCode.DataValueField="CodedValue";
					ddlEditCustomsProcedureCode.DataSource = dsdeclaration;
					ddlEditCustomsProcedureCode.DataBind();
					ddlEditCustomsProcedureCode.SelectedValue = dr["CustomsProcedureCode"].ToString(); 

					ddlEditUnitsOfMeasure.DataTextField="DropDownListDisplayValue";
					ddlEditUnitsOfMeasure.DataValueField="CodedValue";
					ddlEditUnitsOfMeasure.DataSource = dsUOM;
					ddlEditUnitsOfMeasure.DataBind();
					ddlEditUnitsOfMeasure.SelectedValue = dr["UnitsOfMeasure"].ToString(); 

					SetInitialFocus(txtEdittariff_code);
				}
			}
		}

		public void ddlAddOriginCountry_SelectedIndexChanged(object sender, System.EventArgs e)
		{ 
			try
			{
				Table tbl = (Table)gvJobEntryLines.Controls[0];
				foreach(TableRow row in tbl.Rows)
				{ 
					DropDownList ddlAddOriginCountry  = (DropDownList)row.FindControl("ddlAddOriginCountry");
					DropDownList ddlAddCurrencyCode  = (DropDownList)row.FindControl("ddlAddCurrencyCode");

					if(ddlAddOriginCountry != null)
					{
						DataRow[] result=  dsCountry.Tables[0].Select("CountryCode = '" + ddlAddOriginCountry.SelectedValue + "'"); 
						ddlAddCurrencyCode.SelectedValue = result[0]["Currencycode"].ToString(); 
					}
				}
				//				DropDownList ddlAddOriginCountry  = (DropDownList)gvJobEntryLines.FindControl("ddlAddOriginCountry");
				//				DropDownList ddlAddCurrencyCode  = (DropDownList)gvJobEntryLines.FindControl("ddlAddCurrencyCode");

				//				DropDownList ddlAddOriginCountry  =  (DropDownList)gvJobEntryLines.Items [dsAllResult.Tables[1].Rows.Count].FindControl("ddlAddOriginCountry");
				//				DropDownList ddlAddCurrencyCode  =  (DropDownList)gvJobEntryLines.Items [dsAllResult.Tables[1].Rows.Count].FindControl("ddlAddCurrencyCode"); 
				//				DataRow[] result=  dsCountry.Tables[0].Select("CountryCode = '" + ddlAddOriginCountry.SelectedValue + "'"); 
				//				ddlAddCurrencyCode.SelectedValue = result[0]["Currencycode"].ToString(); 
			}
			catch (Exception ex)
			{ 
			}
		}
 
		public void ddlEditOriginCountry_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				DropDownList ddlEditOriginCountry  =  (DropDownList)gvJobEntryLines.Items [indLine].FindControl("ddlEditOriginCountry");
				DropDownList ddlEditCurrencyCode  =  (DropDownList)gvJobEntryLines.Items [indLine].FindControl("ddlEditCurrencyCode"); 
				DataRow[] result=  dsCountry.Tables[0].Select("CountryCode = '" + ddlEditOriginCountry.SelectedValue + "'"); 
				ddlEditCurrencyCode.SelectedValue = result[0]["Currencycode"].ToString(); 
			}
			catch
			{}
		}
		private void gvJobEntryLines_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			lblError.Text = "";
			string cmd = e.CommandName; 
			GridCmd=cmd;
			if(cmd=="EDIT_ITEM")
			{ 
				Amount_required=0;
				Tariff_qty_required=0;
				Suppl_qty_required=0;

				indLine=(short)e.Item.ItemIndex;
				gvJobEntryLines.EditItemIndex=e.Item.ItemIndex; 
				gvJobEntryLines.DataSource=dtEditResult;
				gvJobEntryLines.DataBind();
			} 
			else if(cmd=="SAVE_ITEM")
			{
				DropDownList ddlEditModelOfDeclaration = (DropDownList)e.Item.FindControl("ddlEditModelOfDeclaration");
				DropDownList ddlEditCustomsProcedureCode = (DropDownList)e.Item.FindControl("ddlEditCustomsProcedureCode"); 
				DropDownList ddlEditOriginCountry = (DropDownList)e.Item.FindControl("ddlEditOriginCountry");
				DropDownList ddlEditCurrencyCode = (DropDownList)e.Item.FindControl("ddlEditCurrencyCode");
				DropDownList ddlEditUnitsOfMeasure = (DropDownList)e.Item.FindControl("ddlEditUnitsOfMeasure");
				TextBox txtEditDescription1 = (TextBox)e.Item.FindControl("txtEditDescription1");
				TextBox txtEditDescription2 = (TextBox)e.Item.FindControl("txtEditDescription2");
				TextBox txtEdittariff_code = (TextBox)e.Item.FindControl("txtEdittariff_code");
				//com.common.util.msTextBox txtEditTariff_Qty = (com.common.util.msTextBox)e.Item.FindControl("txtEditTariff_Qty");
				TextBox txtEditTariff_Qty = (TextBox)e.Item.FindControl("txtEditTariff_Qty");
				com.common.util.msTextBox txtEditSupplemental_Qty = (com.common.util.msTextBox)e.Item.FindControl("txtEditSupplemental_Qty");
				com.common.util.msTextBox txtEditAmount = (com.common.util.msTextBox)e.Item.FindControl("txtEditAmount"); 
				if (ddlEditModelOfDeclaration.SelectedValue.Trim() == "")
				{ 
					lblError.Text = "Model of Declaration is required for this operation";
					SetInitialFocus(ddlEditModelOfDeclaration);
					return;
				}
				else if (ddlEditCustomsProcedureCode.SelectedValue.Trim() == "")
				{ 
					lblError.Text = "Customs Procedure Code is required for this operation";
					SetInitialFocus(ddlEditCustomsProcedureCode);
					return;
				}
				else if (ddlEditOriginCountry.SelectedValue.Trim() == "")
				{ 
					lblError.Text = "Country of origin is required for this operation";
					SetInitialFocus(ddlEditOriginCountry);
					return;
				}
				else if (ddlEditCurrencyCode.SelectedValue.Trim() == "")
				{ 
					lblError.Text = "Currency code is required for this operation";
					SetInitialFocus(ddlEditCurrencyCode);
					return;
				}
				else if (ddlEditUnitsOfMeasure.SelectedValue.Trim() == "")
				{ 
					lblError.Text = "Units of measure is required for this operation";
					SetInitialFocus(ddlEditUnitsOfMeasure);
					return;
				}
				else if (txtEditDescription1.Text.Trim() == "")
				{ 
					lblError.Text = "At least the first description is required for this operation";
					SetInitialFocus(txtEditDescription1);
					return;
				}
				else if(txtEdittariff_code.Text.Trim() == "" )
				{
					lblError.Text="Tariff code is required for this operation";
					SetInitialFocus(txtEdittariff_code);
					return ;
				}
				else if(txtEdittariff_code.Text.Trim() != "" && com.common.util.Utility.ValidateFormat(txtEdittariff_code.Text.Trim(),strFormatTariff) ==false)
				{
					lblError.Text="Tariff Code is not valid";
					SetInitialFocus(txtEdittariff_code);
					return ;
				}
				else if(txtEditTariff_Qty.Text == "" && Tariff_qty_required == 1)
				{
					lblError.Text="Tariff quantity is required for this tariff code";
					SetInitialFocus(txtEditTariff_Qty);
					return ;
				}
				else if (txtEditTariff_Qty.Text != "" && Convert.ToDecimal(txtEditTariff_Qty.Text) < 1 && Tariff_qty_required == 1) 
				{
					lblError.Text="Tariff quantity must be > 0";
					SetInitialFocus(txtEditTariff_Qty);
					return ;
				}
				else if(txtEditAmount.Text == "" && Amount_required == 1)
				{
					lblError.Text="Amount is required for this tariff code";
					SetInitialFocus(txtEditAmount);
					return ;
				}
				else if (txtEditAmount.Text != "" && Convert.ToDecimal(txtEditAmount.Text) < 1 && Amount_required == 1) 
				{
					lblError.Text="Amount must be > 0";
					SetInitialFocus(txtEditAmount);
					return ;
				}		
				else if(txtEditSupplemental_Qty.Text == "" && Suppl_qty_required == 1)
				{
					lblError.Text="Supplemental quantity is required for this tariff code";
					SetInitialFocus(txtEditSupplemental_Qty);
					return ;
				}
				else if (txtEditSupplemental_Qty.Text != "" && Convert.ToDecimal(txtEditSupplemental_Qty.Text) < 1 && Suppl_qty_required == 1) 
				{
					lblError.Text="Supplemental quantity  must be > 0";
					SetInitialFocus(txtEditSupplemental_Qty);
					return ;
				}
				else 
				{ 
					string Tariff_Qty, Supplemental_Qty;  
					if (txtEditTariff_Qty.Text  == "")
					{
						Tariff_Qty= "0";
					} 
					else
					{
						Tariff_Qty = txtEditTariff_Qty.Text;
					}
					if (txtEditSupplemental_Qty.Text == "")
					{
						Supplemental_Qty= "0";
					} 
					else
					{
						Supplemental_Qty = txtEditSupplemental_Qty.Text;
					}

					DataSet ds  = CustomJobCompilingDAL.SaveCustoms_JobEntryLines(strAppID,
						strEnterpriseID,
						strUserLoggin ,
						"1",
						consignment_no,
						CustomerID,
						strJobEntryNo, 
						dtEditResult.Rows[e.Item.ItemIndex]["SeqNo"].ToString() ,
						ddlEditModelOfDeclaration.SelectedValue,
						ddlEditCustomsProcedureCode.SelectedValue ,
						txtEditDescription1.Text ,
						txtEditDescription2.Text ,
						ddlEditOriginCountry.SelectedValue,
						ddlEditCurrencyCode.SelectedValue ,
						txtEdittariff_code.Text,
						Tariff_Qty,
						Supplemental_Qty,
						ddlEditUnitsOfMeasure.SelectedValue,
						txtEditAmount.Text); 

					if(ds !=null && ds.Tables[0].Rows.Count > 0)
					{
						if(Convert.ToInt32( ds.Tables[0].Rows[0]["ErrorCode"]) > 0)
						{
							lblError.Text = ds.Tables[0].Rows[0]["ErrorMessage"].ToString();
							if (Convert.ToInt32( ds.Tables[0].Rows[0]["ErrorCode"]) == 21)
							{
								com.common.util.msTextBox txtValuation = (com.common.util.msTextBox)e.Item.FindControl("txtValuation"); 
								TextBox txtExchange_Rate = (TextBox)e.Item.FindControl("txtExchange_Rate"); 
								txtValuation.Text = "";
								txtExchange_Rate.Text = "";
							}
						}
						else
						{
							gvJobEntryLines.EditItemIndex = -1;
							ExecQry();
						}
					}

                     
				}
				Amount_required=0;
				Tariff_qty_required=0;
				Suppl_qty_required=0;
			}
			else if(cmd=="CANCEL_ITEM")
			{
				gvJobEntryLines.EditItemIndex = -1;
				gvJobEntryLines.DataSource= dtEditResult;
				gvJobEntryLines.DataBind();

				Amount_required=0;
				Tariff_qty_required=0;
				Suppl_qty_required=0;
			} 
			else if(cmd=="DELETE_ITEM")
			{  
				DataSet ds = CustomJobCompilingDAL.DeleteCustoms_JobEntryLines(strAppID,
					strEnterpriseID,
					strUserLoggin ,
					"2",
					consignment_no,
					CustomerID,
					strJobEntryNo, 
					dtEditResult.Rows[e.Item.ItemIndex]["SeqNo"].ToString());
 
				if(ds !=null && ds.Tables[0].Rows.Count > 0)
				{
					if(Convert.ToInt32( ds.Tables[0].Rows[0]["ErrorCode"]) > 0)
					{
						lblError.Text = ds.Tables[0].Rows[0]["ErrorMessage"].ToString(); 
					}
					else
					{
						gvJobEntryLines.EditItemIndex = -1;
						ExecQry(); 
					}
				}				
			} 
			else if(cmd=="ADD_ITEM") 
			{
				DropDownList ddlAddModelOfDeclaration = (DropDownList)e.Item.FindControl("ddlAddModelOfDeclaration");
				DropDownList ddlAddCustomsProcedureCode = (DropDownList)e.Item.FindControl("ddlAddCustomsProcedureCode"); 
				DropDownList ddlAddOriginCountry = (DropDownList)e.Item.FindControl("ddlAddOriginCountry");
				DropDownList ddlAddCurrencyCode = (DropDownList)e.Item.FindControl("ddlAddCurrencyCode");
				DropDownList ddlAddUnitsOfMeasure = (DropDownList)e.Item.FindControl("ddlAddUnitsOfMeasure");
				TextBox txtAddDescription1 = (TextBox)e.Item.FindControl("txtAddDescription1");
				TextBox txtAddDescription2 = (TextBox)e.Item.FindControl("txtAddDescription2");
				TextBox txtAddtariff_code = (TextBox)e.Item.FindControl("txtAddtariff_code");
				//com.common.util.msTextBox txtAddTariff_Qty = (com.common.util.msTextBox)e.Item.FindControl("txtAddTariff_Qty")
				TextBox txtAddTariff_Qty = (TextBox)e.Item.FindControl("txtAddTariff_Qty");
				com.common.util.msTextBox txtAddSupplemental_Qty = (com.common.util.msTextBox)e.Item.FindControl("txtAddSupplemental_Qty");
				com.common.util.msTextBox txtAddAmount = (com.common.util.msTextBox)e.Item.FindControl("txtAddAmount"); 
				if (ddlAddModelOfDeclaration.SelectedValue.Trim() == "")
				{ 
					lblError.Text = "Model of Declaration is required for this operation";
					SetInitialFocus(ddlAddModelOfDeclaration);
					return;
				}
				else if (ddlAddCustomsProcedureCode.SelectedValue.Trim() == "")
				{ 
					lblError.Text = "Customs Procedure Code is required for this operation";
					SetInitialFocus(ddlAddCustomsProcedureCode);
					return;
				}
				else if (ddlAddOriginCountry.SelectedValue.Trim() == "")
				{ 
					lblError.Text = "Country of origin is required for this operation";
					SetInitialFocus(ddlAddOriginCountry);
					return;
				}
				else if (ddlAddCurrencyCode.SelectedValue.Trim() == "")
				{ 
					lblError.Text = "Currency code is required for this operation";
					SetInitialFocus(ddlAddCurrencyCode);
					return;
				}
				else if (ddlAddUnitsOfMeasure.SelectedValue.Trim() == "")
				{ 
					lblError.Text = "Units of measure is required for this operation";
					SetInitialFocus(ddlAddUnitsOfMeasure);
					return;
				}
				else if (txtAddDescription1.Text.Trim() == "")
				{ 
					lblError.Text = "At least the first description is required for this operation";
					SetInitialFocus(txtAddDescription1);
					return;
				}
				else if(txtAddtariff_code.Text.Trim() == "" )
				{
					lblError.Text="Tariff code is required for this operation";
					SetInitialFocus(txtAddtariff_code);
					return ;
				}
				else if(txtAddtariff_code.Text.Trim() != "" && com.common.util.Utility.ValidateFormat(txtAddtariff_code.Text.Trim(),strFormatTariff) ==false)
				{
					lblError.Text="Tariff Code is not valid";
					SetInitialFocus(txtAddtariff_code);
					return ;
				}
				else if(txtAddTariff_Qty.Text == "" && Tariff_qty_required == 1)
				{
					lblError.Text="Tariff quantity is required for this tariff code";
					SetInitialFocus(txtAddTariff_Qty);
					return ;
				}
				else if (txtAddTariff_Qty.Text != "" && Convert.ToDecimal(txtAddTariff_Qty.Text) < 1 && Tariff_qty_required == 1) 
				{
					lblError.Text="Tariff quantity must be > 0";
					SetInitialFocus(txtAddTariff_Qty);
					return ;
				}
				else if(txtAddAmount.Text == "" && Amount_required == 1)
				{
					lblError.Text="Amount is required for this tariff code";
					SetInitialFocus(txtAddAmount);
					return ;
				}
				else if (txtAddAmount.Text != "" && Convert.ToDecimal(txtAddAmount.Text) < 1 && Amount_required == 1) 
				{
					lblError.Text="Amount must be > 0";
					SetInitialFocus(txtAddAmount);
					return ;
				}		
				else if( txtAddSupplemental_Qty.Text == "" && Suppl_qty_required == 1)
				{
					lblError.Text="Supplemental quantity is required for this tariff code";
					SetInitialFocus(txtAddSupplemental_Qty);
					return ;
				}
				else if (txtAddSupplemental_Qty.Text != "" && Convert.ToDecimal(txtAddSupplemental_Qty.Text) < 1 && Suppl_qty_required == 1) 
				{
					lblError.Text="Supplemental quantity must be > 0";
					SetInitialFocus(txtAddSupplemental_Qty);
					return ;
				}
				else 
				{ 
					string Tariff_Qty, Supplemental_Qty;  
					if (txtAddTariff_Qty.Text  == "")
					{
						Tariff_Qty= "0";
					} 
					else
					{
						Tariff_Qty = txtAddTariff_Qty.Text;
					}
					if (txtAddSupplemental_Qty.Text == "")
					{
						Supplemental_Qty= "0";
					} 
					else
					{
						Supplemental_Qty = txtAddSupplemental_Qty.Text;
					}



					DataSet ds = CustomJobCompilingDAL.SaveCustoms_JobEntryLines(strAppID,
						strEnterpriseID,
						strUserLoggin ,
						"1",
						consignment_no,
						CustomerID,
						strJobEntryNo, 
						dsAllResult.Tables[0].Rows[0]["NextSeqNo"].ToString(),
						ddlAddModelOfDeclaration.SelectedValue,
						ddlAddCustomsProcedureCode.SelectedValue ,
						txtAddDescription1.Text ,
						txtAddDescription2.Text ,
						ddlAddOriginCountry.SelectedValue,
						ddlAddCurrencyCode.SelectedValue ,
						txtAddtariff_code.Text,
						Tariff_Qty,
						Supplemental_Qty,
						ddlAddUnitsOfMeasure.SelectedValue,
						txtAddAmount.Text); 

					if(ds !=null && ds.Tables[0].Rows.Count > 0)
					{
						if(Convert.ToInt32( ds.Tables[0].Rows[0]["ErrorCode"]) > 0)
						{
							lblError.Text = ds.Tables[0].Rows[0]["ErrorMessage"].ToString();
						}
						else
						{
							gvJobEntryLines.EditItemIndex = -1;
							ExecQry();
						}
					} 
				} 
			}

		}
		


		private void btnClose_Click(object sender, System.EventArgs e)
		{
			string sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.opener.callback();";
			sScript += "    window.close();";
			sScript += "</script>";
			Response.Write(sScript);
			//sScript += "  window.opener.callback();  window.close();";
		}


		private void btnSaveAndClose_Click(object sender, System.EventArgs e)
		{
			try
			{
				//Insert Or Update Row
				string Tariff_Qty, Supplemental_Qty;
				for(int i = 0; i <= dtEditResult.Rows.Count -1; i ++)
				{ 
					if (dtEditResult.Rows[i]["Tariff_Qty"].ToString() == "")
					{
						Tariff_Qty= "0";
					} 
					else
					{
						Tariff_Qty= dtEditResult.Rows[i]["Tariff_Qty"].ToString();
					}
					if (dtEditResult.Rows[i]["Supplemental_Qty"].ToString() == "")
					{
						Supplemental_Qty= "0";
					} 
					else
					{
						Supplemental_Qty= dtEditResult.Rows[i]["Supplemental_Qty"].ToString();
					}

					DataSet dsJobEntryLines = CustomJobCompilingDAL.SaveCustoms_JobEntryLines(strAppID,
						strEnterpriseID,
						strUserLoggin ,
						"1",
						consignment_no,
						CustomerID,
						strJobEntryNo, 
						dtEditResult.Rows[i]["SeqNo"].ToString() ,
						dtEditResult.Rows[i]["ModelOfDeclaration"].ToString() ,
						dtEditResult.Rows[i]["CustomsProcedureCode"].ToString() ,
						dtEditResult.Rows[i]["Description1"].ToString() ,
						dtEditResult.Rows[i]["Description2"].ToString() ,
						dtEditResult.Rows[i]["OriginCountry"].ToString(),
						dtEditResult.Rows[i]["CurrencyCode"].ToString() ,
						dtEditResult.Rows[i]["tariff_code"].ToString() ,
						Tariff_Qty.ToString() ,
						Supplemental_Qty.ToString(),
						dtEditResult.Rows[i]["UnitsOfMeasure"].ToString(),
						dtEditResult.Rows[i]["Amount"].ToString()); 
				}

				//Delete Row
				string[] arrDelete = strDeleteSeq.Split(':');
				for(int j = 0; j <= arrDelete.Length -1; j++)
				{
					DataSet dsJobEntryLines = CustomJobCompilingDAL.DeleteCustoms_JobEntryLines(strAppID,
						strEnterpriseID,
						strUserLoggin ,
						"2",
						consignment_no,
						CustomerID,
						strJobEntryNo, 
						arrDelete[j].ToString() );
				}
			}
			catch
			{
			
			}

			string sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.opener.callback();  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}


		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				StringBuilder s = new StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}


		private void btnExecTariffCodeFooter_Click(object sender, System.EventArgs e)
		{
			Amount_required=0;
			Tariff_qty_required=0;
			Suppl_qty_required=0;

			TextBox txtAddtariff_code = (TextBox)gvJobEntryLines.Controls[0].Controls[gvJobEntryLines.Controls[0].Controls.Count-1].FindControl("txtAddtariff_code");
			TextBox txtAddDescription1 = (TextBox)gvJobEntryLines.Controls[0].Controls[gvJobEntryLines.Controls[0].Controls.Count-1].FindControl("txtAddDescription1");
			DropDownList ddlAddModelOfDeclaration = (DropDownList)gvJobEntryLines.Controls[0].Controls[gvJobEntryLines.Controls[0].Controls.Count-1].FindControl("ddlAddModelOfDeclaration");
			if(txtAddtariff_code != null)
			{
				txtAddDescription1.Text="";
				DataTable dt =CustomJobCompilingDAL.GetCustomsTariffDescription(strAppID,strEnterpriseID,txtAddtariff_code.Text.Trim());
				if(dt.Rows.Count>0)
				{
					txtAddDescription1.Text=dt.Rows[0]["tariff_description"].ToString();
					Amount_required=Convert.ToInt32(dt.Rows[0]["amount_required"].ToString());
					Tariff_qty_required=Convert.ToInt32(dt.Rows[0]["tariff_qty_required"].ToString());
					Suppl_qty_required=Convert.ToInt32(dt.Rows[0]["suppl_qty_required"].ToString());
				}
			}
			SetInitialFocus(ddlAddModelOfDeclaration);
		}

		private void btnExecTariffCodeEdit_Click(object sender, System.EventArgs e)
		{
			Amount_required=0;
			Tariff_qty_required=0;
			Suppl_qty_required=0;

			TextBox txtEdittariff_code = (TextBox)gvJobEntryLines.Items[gvJobEntryLines.EditItemIndex].FindControl("txtEdittariff_code");
			TextBox txtEditDescription1 = (TextBox)gvJobEntryLines.Items[gvJobEntryLines.EditItemIndex].FindControl("txtEditDescription1");
			DropDownList ddlEditModelOfDeclaration = (DropDownList)gvJobEntryLines.Items[gvJobEntryLines.EditItemIndex].FindControl("ddlEditModelOfDeclaration");
			if(txtEdittariff_code != null)
			{
				txtEditDescription1.Text="";
				DataTable dt =CustomJobCompilingDAL.GetCustomsTariffDescription(strAppID,strEnterpriseID,txtEdittariff_code.Text.Trim());
				if(dt.Rows.Count>0)
				{
					txtEditDescription1.Text=dt.Rows[0]["tariff_description"].ToString();
					Amount_required=Convert.ToInt32(dt.Rows[0]["amount_required"].ToString());
					Tariff_qty_required=Convert.ToInt32(dt.Rows[0]["tariff_qty_required"].ToString());
					Suppl_qty_required=Convert.ToInt32(dt.Rows[0]["suppl_qty_required"].ToString());
				}
			}
			SetInitialFocus(ddlEditModelOfDeclaration);
		}


	}
}
