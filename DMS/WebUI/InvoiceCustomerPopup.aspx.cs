using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.util;
using com.common.classes;
using com.common.DAL;
using com.ties.classes;   
using System.Text;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for InvoiceCustomerPopup.
	/// </summary>
	public class InvoiceCustomerPopup : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.DataGrid dgPreview;

		string strAppID;
		string strEnterpriseID;
		protected System.Web.UI.WebControls.Button btnOk;
		protected System.Web.UI.WebControls.Button btnExport;
		string CustomerList;
		string m_strUserID;
		string strFormID;
		string strTarget;

		private void Page_Load(object sender, System.EventArgs e)
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppID = utility.GetAppID();
			strEnterpriseID = utility.GetEnterpriseID();
			m_strUserID = utility.GetUserID();
			CustomerList = Request.Params["CUSTOMER_LIST"];
			strFormID = Request.Params["FORMID"];
			strTarget = Request.Params["TARGET"];

			DataSet dsInvoiceCustomer = GetInvoiceQueryData(CustomerList);
			dsInvoiceCustomer.Tables[0].DefaultView.Sort = "payerid";
			
			if (!IsPostBack)
			{
				this.dgPreview.DataSource = dsInvoiceCustomer.Tables[0];
				this.dgPreview.DataBind();
			}
		}

		private DataTable CreateEmptyDataTable()
		{
			DataTable dtInvoiceRP = new DataTable();

			#region "Dates"
			//			dtInvoiceRP.Columns.Add(new DataColumn("start_date", typeof(DateTime)));
			//			dtInvoiceRP.Columns.Add(new DataColumn("end_date", typeof(DateTime)));
			#endregion

			#region "Invoice"
			dtInvoiceRP.Columns.Add(new DataColumn("payerid", typeof(string)));
			#endregion

			return dtInvoiceRP;

		}

		private DataSet GetInvoiceQueryData(string list_customer)
		{

			DataSet dsInvoiceRP = new DataSet();
			DataTable dtInvoiceRP = CreateEmptyDataTable();
			DataRow dr;
			string[] listCustomerID = list_customer.Split(',');

			for(int i = 0; i <= listCustomerID.Length - 1; i++)
			{
				if(listCustomerID[i] != "")
				{
					dr = dtInvoiceRP.NewRow();
					dr["payerid"] = listCustomerID[i];
					dtInvoiceRP.Rows.Add(dr);
				}
			}
			
			dsInvoiceRP.Tables.Add(dtInvoiceRP);
			return dsInvoiceRP;

		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnOk_Click(object sender, System.EventArgs e)
		{
			// Get the value and return it to the parent window
			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		private void btnExport_Click(object sender, System.EventArgs e)
		{
			string listExcludeCustomer = "";
			string[] listArray;
			for(int i = 0; i <= dgPreview.Items.Count - 1; i++)
			{
				CheckBox chkSelect = (CheckBox)this.dgPreview.Items[i].FindControl("chkSelect");

				if(chkSelect.Checked)
				{

					listExcludeCustomer += dgPreview.Items[i].Cells[1].Text+",";
				}
			}

			listArray = listExcludeCustomer.Split(',');
			listExcludeCustomer = "";
			for(int i = 0; i <= listArray.Length - 2; i++)
			{
				listExcludeCustomer += listArray[i];
				if(i != listArray.Length - 2)
					listExcludeCustomer += ",";
			}


			String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  window.opener."+strFormID+"." + strTarget + ".value = '"+listExcludeCustomer+"';" ;
			sScript += "  window.opener."+strFormID+".btnHiddenExport.click();" ;
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}

		public void test()
		{

		}
	}
}
