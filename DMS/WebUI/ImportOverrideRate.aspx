<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<%@ Page language="c#" Codebehind="ImportOverrideRate.aspx.cs" AutoEventWireup="false" Inherits="com.ties.ImportOverrideRate" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Import Rating Override</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript" src="Scripts/WorkProgress.js"></script>
		<script language="javascript">
		<!--
		
		function setValue()
		{
			var s = document.all['inFile'];
			document.all['txtFilePath'].innerText = s.value;
			if(s.value != '') 
				document.all['btnImport'].disabled = false;
			else
				document.all['btnImport'].disabled = true;
			
		}
		
		function upBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-up.GIF)';
		}
		
		function downBrowse()
		{
			document.all['divBrowse'].style.backgroundImage='url(images/btn-browse-down.GIF)';
		}
		
		function clearValue()
		{
			if(document.all['txtFilePath'].innerText == '') {
				document.location.reload();
			}
		}
		
		
		//-->
		</script>
	</HEAD>
	<body onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();"
		onunload="window.top.displayBanner.fnCloseAll(0); HideWait();" MS_POSITIONING="FlowLayout">
		<form id="ImportOverrideRate" name="ImportOverrideRate" method="post" encType="multipart/form-data"
			runat="server">
			<div id="CSSDiv"><FONT face="Tahoma"></FONT></div>
			<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
			<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
			<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
			<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
			<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
			<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
			<FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT><FONT face="Tahoma"></FONT>
			<FONT face="Tahoma"></FONT>
			<br>
			<table id="Table2" style="WIDTH: 3182px; HEIGHT: 120px" align="center" border="0">
				<TR>
					<td><asp:label id="lblMainTitle" runat="server" Height="26px" Width="477px" CssClass="mainTitleSize">&nbsp;Import Rating Override</asp:label></td>
				</TR>
				<tr>
					<td>
						<table id="Table3" width="100%" border="0">
							<tr>
								<td width="17%">
									<div onmouseup="upBrowse();" onmousedown="downBrowse();" id="divBrowse" style="DISPLAY: inline; BACKGROUND-IMAGE: url(images/btn-browse-up.GIF); WIDTH: 70px; HEIGHT: 20px"><INPUT id="inFile" onblur="setValue();" style="BORDER-RIGHT: #88a0c8 1px outset; BORDER-TOP: #88a0c8 1px outset; FONT-SIZE: 11px; FILTER: alpha(opacity: 0); BORDER-LEFT: #88a0c8 1px outset; WIDTH: 0px; COLOR: #003068; BORDER-BOTTOM: #88a0c8 1px outset; FONT-FAMILY: Arial; BACKGROUND-COLOR: #e9edf0; TEXT-DECORATION: none"
											onfocus="setValue();" type="file" name="inFile" runat="server"></div>
									</INPUT><asp:button id="btnImport" runat="server" Width="62px" CssClass="queryButton" Enabled="False"
										Text="Import"></asp:button><asp:button id="btnSave" runat="server" Width="66px" CssClass="queryButton" Enabled="False"
										Text="Save"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:textbox id="txtFilePath" runat="server" Width="240px" CssClass="textField"></asp:textbox>
								</td>
								<td><asp:label id="lblErrorMsg" runat="server" Height="19px" Width="566px" CssClass="errorMsgColor"></asp:label><BR>
									<asp:label id="lblTotalSavedCon" runat="server" Height="19px" Width="566px" CssClass="errorMsgColor"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><asp:label id="lblvalid" style="FONT-WEIGHT: bold; FONT-SIZE: 13px" runat="server" Height="26px"
							Width="477px">&nbsp;Valid Imported Data:</asp:label></td>
				</tr>
				<TR>
					<td><asp:datagrid id="dgValidImportData" tabIndex="115" runat="server" Width="38%" OnPageIndexChanged="dgValidImportData_PageIndexChanged"
							SelectedItemStyle-CssClass="gridFieldSelected" AutoGenerateColumns="False" AllowPaging="True"
							PageSize="5" ItemStyle-Height="20px" HeaderStyle-Height="20px">
							<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
							<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
							<HeaderStyle Height="20px"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Consignment No">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblconsignment_no" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Ref No">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblref_no" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ref_no")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Destination DC">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lbldestination_station" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"destination_station")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Delivery Route Code">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblroute_code" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"route_code")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Customer ID">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblpayerid" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"payerid")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Actual POD Date">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblact_delivery_date" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "act_delivery_date", "{0:dd/MM/yyyy HH:mm}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Freight Charge Rated">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lbltot_freight_charge" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"tot_freight_charge", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Freight Charge Override">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lbloverride_rated_freight" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"override_rated_freight", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Insurance Surcharge Rated">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblinsurance_surcharge" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"insurance_surcharge", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Insurance Surcharge Override">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lbloverride_rated_ins" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"override_rated_ins", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Other Surcharge Rated">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblother_surch_amount" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"other_surch_amount", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Other Surcharge Override">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lbloverride_rated_other" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"override_rated_other", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Total VAS Surcharge Rated">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lbltot_vas_surcharge" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"tot_vas_surcharge", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="ESA Surcharge Rated">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblesa_surcharge" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"esa_surcharge", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="ESA Surcharge Override">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lbloverride_rated_esa" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"override_rated_esa", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Total Amount Rated">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lbltotal_rated_amount" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"total_rated_amount", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Total Amount Override">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lbloverride_rated_total" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"override_rated_total", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle NextPageText="Next" Height="20px" Font-Size="Smaller" PrevPageText="Previous" HorizontalAlign="Left"></PagerStyle>
						</asp:datagrid></td>
				</TR>
				<tr>
					<td><asp:label id="lblinvalid" style="FONT-WEIGHT: bold; FONT-SIZE: 13px" runat="server" Height="26px"
							Width="176px">&nbsp;Invalid Imported Data:</asp:label><asp:button id="Button1" runat="server" Width="144px" CssClass="queryButton" Text="Export Invalid Data"></asp:button></td>
				</tr>
				<TR>
					<TD><asp:datagrid id="dgInvalidImportData" tabIndex="115" runat="server" Width="1758px" OnPageIndexChanged="dgInvalidImportData_PageIndexChanged"
							SelectedItemStyle-CssClass="gridFieldSelected" AutoGenerateColumns="False" AllowPaging="True"
							PageSize="5" ItemStyle-Height="20px" HeaderStyle-Height="20px">
							<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
							<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Consignment No">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvConsignment_no" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"consignment_no")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Ref No">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvRef_no" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ref_no")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Destination DC">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvDestination_station" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"destination_station")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Delivery Route Code">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvRoute_code" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"route_code")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Customer ID">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvPayerid" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"payerid")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Actual POD Date">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvAct_delivery_date" CssClass="gridLabel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"act_delivery_date", "{0:dd/MM/yyyy HH:mm}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Freight Charge Rated">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvTot_freight_charge" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"tot_freight_charge", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Freight Charge Override">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvOverride_rated_freight" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"override_rated_freight", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Insurance Surcharge Rated">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvInsurance_surcharge" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"insurance_surcharge", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Insurance Surcharge Override">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvOverride_rated_ins" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"override_rated_ins", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Other Surcharge Rated">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvOther_surch_amount" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"other_surch_amount", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Other Surcharge Override">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvOverride_rated_other" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"override_rated_other", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Total VAS Surcharge Rated">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvTot_vas_surcharge" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"tot_vas_surcharge", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="ESA Surcharge Rated">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvEsa_surcharge" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"esa_surcharge", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="ESA Surcharge Override">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvOverride_rated_esa" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"override_rated_esa", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Total Amount Rated">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvTotal_rated_amount" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"total_rated_amount", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Total Amount Override">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvOverride_rated_total" CssClass="gridLabel" runat="server" style="text-align:right;" Text='<%#DataBinder.Eval(Container.DataItem,"override_rated_total", "{0:#,##0.00}")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Error  reason" HeaderStyle-Width="500px">
									<HeaderStyle CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label ID="lblInvexcluded_reason" CssClass="gridLabel" runat="server" style="text-align:left;" Text='<%#DataBinder.Eval(Container.DataItem,"excluded_reason")%>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle NextPageText="Next" Font-Size="Smaller" PrevPageText="Previous" HorizontalAlign="left"
								Height="20"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
			</table>
		</form>
	</body>
</HTML>
