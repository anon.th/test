using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.RBAC;
using com.common.util;
using com.ties;
using com.ties.DAL;
using com.ties.classes;
using com.common.applicationpages;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for PublicHoliday.
	/// </summary>
	public class PublicHolidays : BasePage
	{
		protected System.Web.UI.WebControls.DataGrid dgPublicHoliday;
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Button btnExecuteQuery;
		protected System.Web.UI.WebControls.Button btnInsert;
		private SessionDS m_sdsPublicHoliday;
		private String m_strAppID;
		private String m_strEnterpriseID;
		private String m_strCulture;
		protected System.Web.UI.WebControls.Label lblPHMessage;
		protected System.Web.UI.WebControls.ValidationSummary PageVlid;
		protected System.Web.UI.WebControls.ValidationSummary PageValidationSummary;
		protected System.Web.UI.WebControls.CheckBox chkApplyHolAll;
		protected System.Web.UI.WebControls.Label Label1;
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//Utility utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			if(!Page.IsPostBack)
			{			
				ViewState["m_strCode"] = "";
				ViewState["index"]=0;
				ViewState["prevRow"] = 0;
				ViewState["SCMode"] = ScreenMode.None;
				ViewState["SCOperation"] = Operation.None;
				chkApplyHolAll.Enabled=false;
				dgPublicHoliday.EditItemIndex = -1;
				m_sdsPublicHoliday = SysDataMgrDAL.GetEmptyPublicHoliday(1);
				enableEditColumn(false);
				BindPublicHoliday();
				Session["SESSION_DS1"] = m_sdsPublicHoliday;
				Session["QUERY_DS"] = m_sdsPublicHoliday;
				//----START QUERYING------
				lblPHMessage.Text = "";
				m_sdsPublicHoliday = SysDataMgrDAL.GetEmptyPublicHoliday(1);
				dgPublicHoliday.CurrentPageIndex = 0;
				btnExecuteQuery.Enabled = true;
				ViewState["SCMode"] = ScreenMode.Query;
				ViewState["SCOperation"] = Operation.None;
				Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
				ViewState["m_strCode"] = "";
				ViewState["prevRow"]=0;
				ViewState["index"]=0;
			    dgPublicHoliday.EditItemIndex = 0;
				enableEditColumn(false);
                dgPublicHoliday.PagerStyle.HorizontalAlign = HorizontalAlign.Right;
                //m_dvTypeOptions = CreateTypeOptions(true);
                BindPublicHoliday();
			}
			else
			{
				m_sdsPublicHoliday = (SessionDS)Session["SESSION_DS1"];
			}
			
		}

		
		#region Public Holiday Grid methods
		private void AddRow()
		{
			SysDataMgrDAL.AddNewRowInPHDS(ref m_sdsPublicHoliday);
			BindPublicHoliday();
		}
		
		private void BindPublicHoliday()
		{
			dgPublicHoliday.VirtualItemCount = System.Convert.ToInt32(m_sdsPublicHoliday.QueryResultMaxSize);
			dgPublicHoliday.DataSource = m_sdsPublicHoliday.ds;
			dgPublicHoliday.DataBind();
			Session["SESSION_DS1"] = m_sdsPublicHoliday;
		}

		public void dgPublicHoliday_Button(object sender, DataGridCommandEventArgs e)
		{
			int iOperation = (int)ViewState["SCOperation"];
			if ((iOperation == (int)Operation.Insert) || (iOperation == (int)Operation.Update) || (btnExecuteQuery.Enabled))
			{
				String strCmdNm = e.CommandName;
				String strCountry = null;
				String strStateCode = null;
				//String strZoneCode = null;
				if(strCmdNm.Equals("StateSearch"))
				{
					msTextBox txtStateCode = (msTextBox)e.Item.FindControl("txtCode");
					String strtxtStateCode = null;
					if(txtStateCode != null)
					{
						strtxtStateCode = txtStateCode.ClientID;
						strStateCode = txtStateCode.Text;
					}
					msTextBox txtCountry = (msTextBox)e.Item.FindControl("txtCountry");
					String strtxtCountry = null;
					if(txtCountry != null)
					{
						strtxtCountry = txtCountry.ClientID;
						strCountry = txtCountry.Text;
					}
				
					String sUrl = "StatePopup.aspx?FORMID=PublicHoliday&COUNTRY_TEXT="+strCountry+"&COUNTRY="+strtxtCountry+"&STATECODE_TEXT="+strStateCode+"&STATECODE="+strtxtStateCode;
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScript("openWindowParam.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
					Logger.LogDebugInfo("ZipCode","dgZipCode_Button","","Data Grid State Search button clicked");
				}
			}
		}

		protected void OnEdit_PublicHoliday(object sender, DataGridCommandEventArgs e)
		{				
			lblPHMessage.Text="";
			int rowIndex = e.Item.ItemIndex;			
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			ViewState["index"] = rowIndex;
			Label lblCode = (Label)dgPublicHoliday.Items[rowIndex].FindControl("lblCode");
			ViewState["m_strCode"] = lblCode.Text;
			if((int)ViewState["SCOperation"] == (int)Operation.Insert)
			{
				ViewState["SCMode"]= ScreenMode.Insert;
			}
			else
			{
				ViewState["SCMode"] = ScreenMode.None;
			}
			if(dgPublicHoliday.EditItemIndex >0)
			{
				TextBox txtCode = (TextBox)dgPublicHoliday.Items[dgPublicHoliday.EditItemIndex].FindControl("txtCode");				
				if(txtCode.Text != null && txtCode.Text == "")	
				{
					ViewState["m_strCode"] = txtCode.Text;
					m_sdsPublicHoliday.ds.Tables[0].Rows.RemoveAt(dgPublicHoliday.EditItemIndex);
				}
			}
			dgPublicHoliday.EditItemIndex = rowIndex;
			BindPublicHoliday();
			msTextBox txtDateFrom = (msTextBox)dgPublicHoliday.Items[rowIndex].FindControl("txtDateFrom");
			msTextBox txtDateTo = (msTextBox)dgPublicHoliday.Items[rowIndex].FindControl("txtDateTo");
			msTextBox txtCountry = (msTextBox) dgPublicHoliday.Items[rowIndex].FindControl("txtCountry");
			txtCountry.ReadOnly = true;
			txtDateFrom.ReadOnly = true;
			txtDateTo.ReadOnly = true;
			//ViewState["SCOperation"] = Operation.Update;
		}

		protected void OnCancel_PublicHoliday(object sender, DataGridCommandEventArgs e)
		{		
			lblPHMessage.Text="";
			dgPublicHoliday.EditItemIndex = -1;
			m_sdsPublicHoliday = (SessionDS)Session["SESSION_DS1"];
			int rowIndex = e.Item.ItemIndex;
			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
			{
				m_sdsPublicHoliday.ds.Tables[0].Rows.RemoveAt(rowIndex);
			}
			enableDeleteColumn(true);
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			BindPublicHoliday();
	
		}

		protected void OnUpdate_PublicHoliday(object sender, DataGridCommandEventArgs e)
		{
            int rowIndex = e.Item.ItemIndex;
			bool isCountrySateValid=false;
			bool isHolidayUsed=false;
			if (updateLatestPublicHoliday(rowIndex)==false)
			{
				lblPHMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage, "DT_FROM_GT_TO", utility.GetUserCulture());
				return;
			}			
			ViewState["index"] = rowIndex;
			ViewState["SCOperation"] = Operation.Update;
		
			m_sdsPublicHoliday = (SessionDS)Session["SESSION_DS1"];
			
			String sCountryCode="";
			String sStateCode="";
			DateTime dtFrom=System.DateTime.Now;
			DateTime dtTo=System.DateTime.Now;

			DataRow dr=m_sdsPublicHoliday.ds.Tables[0].Rows[rowIndex];
			if(Utility.IsNotDBNull(dr["Country"]) && dr["Country"].ToString()!="")
				sCountryCode=dr["Country"].ToString();
	
			if(Utility.IsNotDBNull(dr["State_Code"]) && dr["State_Code"].ToString()!="")
				sStateCode=dr["State_Code"].ToString();
			
			if(Utility.IsNotDBNull(dr["DateFrom"]) && dr["DateFrom"].ToString()!="")			
				dtFrom=Convert.ToDateTime(dr["DateFrom"]);

			if(Utility.IsNotDBNull(dr["DateTo"]) && dr["DateTo"].ToString()!="")			
				dtTo=Convert.ToDateTime(dr["DateTo"]);

			
			isCountrySateValid=SysDataMgrDAL.IsState_Country_Valid( m_strAppID, m_strEnterpriseID, sStateCode, sCountryCode);
			if (isCountrySateValid ==false)
			{
				lblPHMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage, "INVALID_STATE_COUNTRY", utility.GetUserCulture());
				return;
			}
			
			try
			{
				if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
				{
					//m_sdsPublicHoliday.ds.GetChanges();
					int applyHolAll = 0;
					if(chkApplyHolAll.Checked)
						applyHolAll = 1;
					SysDataMgrDAL.InsertPublicHoliday(m_sdsPublicHoliday,rowIndex,m_strAppID,m_strEnterpriseID, applyHolAll);
					lblPHMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());	
					chkApplyHolAll.Checked = false;
					chkApplyHolAll.Enabled=false;
				}
				else
				{
					//Check for used Public Holidays
					isHolidayUsed= SysDataMgrDAL.IsHolidayUsed(m_strAppID,m_strEnterpriseID,sStateCode,sCountryCode,dtFrom,dtTo);
					if(isHolidayUsed==true)
					{
						bool isHolidayUpdateable=true;
						isHolidayUpdateable=SysDataMgrDAL.IsPHUpdateAllowed(m_strAppID,m_strEnterpriseID,sStateCode,sCountryCode,dtFrom,dtTo);
						if (isHolidayUpdateable==false)
						{
							lblPHMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "CNT_UPD_PUBLIC_HOLIDAY", utility.GetUserCulture());
							return;
						}						
					}
					m_sdsPublicHoliday.ds.GetChanges();
					//if (SysDataMgrDAL.UpdatePublicHoliday(m_sdsPublicHoliday, m_strAppID, m_strEnterpriseID))
					//if (SysDataMgrDAL.UpdatePublicHoliday(m_sdsPublicHoliday, (DataRow) Session["Session_ROW"], rowIndex, m_strAppID, m_strEnterpriseID))
					if (SysDataMgrDAL.UpdatePublicHoliday(m_sdsPublicHoliday, rowIndex, m_strAppID, m_strEnterpriseID))
					{
						lblPHMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "UPD_SUCCESSFULLY", utility.GetUserCulture());
					}
					else
					{
						lblPHMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "ERR_UPD_PUBLIC_HOLIDAY", utility.GetUserCulture());
					}
				}
			}
			catch(ApplicationException err)
			{
				String strMsg = err.Message;
				if(strMsg.IndexOf("duplicate") != -1 || strMsg.IndexOf("PRIMARY") != -1 )
					lblPHMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
				else if(strMsg.IndexOf("FK") != -1 || strMsg.IndexOf("FORIEGN KEY")!=-1)
					lblPHMessage.Text  = Utility.GetLanguageText(ResourceType.UserMessage,"INVALID_ENTERPRISE",utility.GetUserCulture());					
				if(strMsg.IndexOf("unique") != -1 )
				{
					lblPHMessage.Text =Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND",utility.GetUserCulture());
				}
				return;	
			}
			catch(Exception err)
			{
				lblPHMessage.Text = err.Message ;
			}							
			
			ViewState["SCOperation"] = Operation.None;
			m_sdsPublicHoliday.ds.Tables[0].Rows[rowIndex].AcceptChanges();			
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			dgPublicHoliday.EditItemIndex = -1;			
			BindPublicHoliday();	
		}

		protected void OnDelete_PublicHoliday(object sender, DataGridCommandEventArgs e)
		{	
			ViewState["SCOperation"] = Operation.Delete;
			int rowNum = e.Item.ItemIndex;				
			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
			{
                DeletePublicHoliday(rowNum);
                lblPHMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage, "DLD_SUCCESSFULLY", utility.GetUserCulture());
            }
			else
			{
				DeletePublicHoliday(rowNum);
				showCurrentPage();
			}
			if((int)ViewState["SCMode"] == (int)ScreenMode.Insert)
			{
				//Do nothing
			}
			else
			{
				if(dgPublicHoliday.EditItemIndex>-1)
				{
					m_sdsPublicHoliday.ds.Tables[0].Rows.RemoveAt(dgPublicHoliday.EditItemIndex-1);
				}
			}
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, true, m_moduleAccessRights);
			BindPublicHoliday();
		}
		
		protected void OnPublicHoliday_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			SessionDS m_sdsQueryPublicHoliday = (SessionDS)Session["QUERY_DS"];
			lblPHMessage.Text = "";
			dgPublicHoliday.CurrentPageIndex = e.NewPageIndex;
			int iStartIndex = dgPublicHoliday.CurrentPageIndex * dgPublicHoliday.PageSize;
			
			m_sdsPublicHoliday = SysDataMgrDAL.QueryPublicHoliday(m_sdsQueryPublicHoliday,m_strAppID,m_strEnterpriseID, iStartIndex,dgPublicHoliday.PageSize);
			Session["SESSION_DS1"] = m_sdsPublicHoliday;
			dgPublicHoliday.VirtualItemCount = System.Convert.ToInt32(m_sdsPublicHoliday.QueryResultMaxSize);
			dgPublicHoliday.CurrentPageIndex = e.NewPageIndex;
			
			dgPublicHoliday.SelectedIndex = -1;
			dgPublicHoliday.EditItemIndex = -1;
			
			BindPublicHoliday();
		}
		
		protected void OnItemBound_PublicHoliday(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}
			DataRow drSelected = m_sdsPublicHoliday.ds.Tables[0].Rows[e.Item.ItemIndex];
			
			TextBox txtCode = (TextBox)e.Item.FindControl("txtCode");
			if(txtCode!=null && ( (int)ViewState["SCMode"] != (int)ScreenMode.Insert && (int)ViewState["SCMode"] != (int)ScreenMode.Query) )
			{
				txtCode.Enabled = false;
			}
		}

		#endregion
		
		private void DeletePublicHoliday(int rowIndex)
		{
			m_sdsPublicHoliday = (SessionDS)Session["SESSION_DS1"];
			DataRow dr = m_sdsPublicHoliday.ds.Tables[0].Rows[rowIndex];
			bool bHolidayUsed=false;
			String strCountryCode = (String)dr[0];
			String strStateCode = (String)dr[1];
			DateTime dtDateFrom = System.Convert.ToDateTime(dr[2]);			
			DateTime dtDateTo = System.Convert.ToDateTime(dr[3]);

			bHolidayUsed=SysDataMgrDAL.IsHolidayUsed(m_strAppID,m_strEnterpriseID,strCountryCode,strStateCode,dtDateFrom,dtDateTo);
			if (bHolidayUsed==true)
			{
				lblPHMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage, "CNT_DEL_PUBLIC_HOLIDAY", utility.GetUserCulture());
				return;
			}
			try
			{
				if (SysDataMgrDAL.DeletePublicHoliday(m_strAppID, m_strEnterpriseID, strCountryCode, strStateCode, dtDateFrom, dtDateTo))
				{
					m_sdsPublicHoliday.ds.Tables[0].Rows.RemoveAt(rowIndex);
					lblPHMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage, "DLD_SUCCESSFULLY", utility.GetUserCulture());
				}
				else
				{
					lblPHMessage.Text=Utility.GetLanguageText(ResourceType.UserMessage, "ERR_DEL_PUBLIC_HOLIDAY", utility.GetUserCulture());
				}
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1)
				{
					lblPHMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());
				}
				if(strMsg.IndexOf("child record") != -1)
				{//Oralce Child Record
					lblPHMessage.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"CHILD_FOUND_CANNOT_DEL",utility.GetUserCulture());
				}
				else
				{
					lblPHMessage.Text = strMsg;
				}
			}
			catch(Exception err)
			{
				String msg = err.ToString();
				lblPHMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ERROR",utility.GetUserCulture());
			}
			
		}

		private void enableCodeColumn(bool toEnable)
		{	
			dgPublicHoliday.Columns[2].Visible=toEnable;//Code column
		}

		private void enableDeleteColumn(bool toEnable)
		{
			dgPublicHoliday.Columns[1].Visible=toEnable;//Delete column
		}

		private bool updateLatestPublicHoliday(int rowIndex)
		{
			bool bValid=true;
			msTextBox txtCountry = (msTextBox)dgPublicHoliday.Items[rowIndex].FindControl("txtCountry");			
			msTextBox txtCode = (msTextBox)dgPublicHoliday.Items[rowIndex].FindControl("txtCode");	
			msTextBox txtDateFrom = (msTextBox)dgPublicHoliday.Items[rowIndex].FindControl("txtDateFrom");			
			msTextBox txtDateTo = (msTextBox)dgPublicHoliday.Items[rowIndex].FindControl("txtDateTo");			
			TextBox txtDescription = (TextBox)dgPublicHoliday.Items[rowIndex].FindControl("txtDescription");
			DataRow dr = m_sdsPublicHoliday.ds.Tables[0].Rows[rowIndex];
			String strCountry ="";
			String strCode ="";
			DateTime dtDateFrom=System.DateTime.Now;
			DateTime dtDateTo =System.DateTime.Now;
			String strDesc ="";
			if (txtCountry !=null)
			{
				strCountry = txtCountry.Text.ToString();
				dr[0] = strCountry;
			}
			if (txtCode !=null)
			{
				strCode = txtCode.Text.ToString();
				dr[1] = strCode;			
			}
			if (txtDateFrom !=null)
			{
				dtDateFrom = DateTime.ParseExact(txtDateFrom.Text,"dd/MM/yyyy",null);				
				dr[2]=dtDateFrom;				
			}
			if (txtDateTo !=null)
			{
				dtDateTo = DateTime.ParseExact(txtDateTo.Text,"dd/MM/yyyy",null);
				dr[3] = dtDateTo;
			}
			if (txtDescription!=null)
			{
				strDesc = txtDescription.Text.ToString();
				dr[4] = strDesc;
			}
			
			if (dtDateFrom > dtDateTo)
			{
				bValid=false;
			}
			//Session["Session_ROW"] = dr;
			Session["SESSION_DS1"] = m_sdsPublicHoliday;
			return bValid;
		}

		private bool checkEmptyFields(int rowIndex)
		{			
			msTextBox txtCountry = (msTextBox)dgPublicHoliday.Items[rowIndex].FindControl("txtCountry");
			msTextBox txtCode = (msTextBox)dgPublicHoliday.Items[rowIndex].FindControl("txtCode");
			msTextBox txtDateFrom = (msTextBox)dgPublicHoliday.Items[rowIndex].FindControl("txtDateFrom");
			msTextBox txtDateTo = (msTextBox)dgPublicHoliday.Items[rowIndex].FindControl("txtDateTo");
			TextBox txtDescription = (TextBox)dgPublicHoliday.Items[rowIndex].FindControl("txtDescription");
			if(txtCountry != null && txtCode!=null && txtDescription!=null && txtDateFrom !=null && txtDateTo != null)
			{
				DataRow dr = m_sdsPublicHoliday.ds.Tables[0].Rows[rowIndex];
				if((int)ViewState["SCMode"] == (int)ScreenMode.Query)
				{
					dr[0] = txtCountry.Text;					
				}				
				dr[1] = txtCode.Text;				
				if (txtDateFrom.Text != "")
				{
					dr[2] = DateTime.ParseExact(txtDateFrom.Text,"dd/MM/yyyy",null);
				}
				else
				{
					dr[2]=System.DBNull.Value;
				}
				if (txtDateTo.Text != "")
				{
					dr[3] = DateTime.ParseExact(txtDateTo.Text,"dd/MM/yyyy",null);
				}
				else
				{
					dr[3]=System.DBNull.Value;
				}
				dr[4] = txtDescription.Text;

				Session["SESSION_DS1"] = m_sdsPublicHoliday;
				Session["QUERY_DS"] = m_sdsPublicHoliday;
				return false;
			}
			return true;
		}

		private void QueryPublicHoliday()
		{
			checkEmptyFields((int)ViewState["index"]);
			SessionDS sdsPublicHoliday = (SessionDS)Session["SESSION_DS1"];
			m_sdsPublicHoliday = SysDataMgrDAL.QueryPublicHoliday(sdsPublicHoliday, m_strAppID,m_strEnterpriseID,0 , dgPublicHoliday.PageSize);		
			dgPublicHoliday.VirtualItemCount = System.Convert.ToInt32(m_sdsPublicHoliday.QueryResultMaxSize);
			if(m_sdsPublicHoliday.QueryResultMaxSize<1)
			{
				lblPHMessage.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND",utility.GetUserCulture());
			}
			Session["SESSION_DS1"] = m_sdsPublicHoliday;
			
		}
		private void enableEditColumn(bool toEnable)
		{
			dgPublicHoliday.Columns[0].Visible=toEnable;
			dgPublicHoliday.Columns[1].Visible=toEnable;
		}

		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnExecuteQuery.Click += new System.EventHandler(this.btnExecuteQuery_Click);
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		
		private void btnInsert_Click(object sender, System.EventArgs e)
		{
			chkApplyHolAll.Enabled = true;
			lblPHMessage.Text = "";
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert, false, m_moduleAccessRights);
			btnExecuteQuery.Enabled = false;			
			dgPublicHoliday.CurrentPageIndex = 0;			
			ViewState["index"] = dgPublicHoliday.Items.Count-1;
			if (((int) ViewState["SCMode"] != (int) ScreenMode.Insert) || (m_sdsPublicHoliday.ds.Tables[0].Rows.Count >= dgPublicHoliday.PageSize))
			{
				m_sdsPublicHoliday = SysDataMgrDAL.GetEmptyPublicHoliday(1);
				dgPublicHoliday.EditItemIndex = m_sdsPublicHoliday.ds.Tables[0].Rows.Count-1;
			}
			else
			{
				AddRow();
				dgPublicHoliday.EditItemIndex = m_sdsPublicHoliday.ds.Tables[0].Rows.Count-1;
			}
			dgPublicHoliday.VirtualItemCount = m_sdsPublicHoliday.ds.Tables[0].Rows.Count;			
			enableEditColumn(true);
			ViewState["SCMode"] = ScreenMode.Insert;
			ViewState["SCOperation"] = Operation.Insert;			
			BindPublicHoliday();
            //msTextBox txtStateCode = (msTextBox)dgPublicHoliday.Items[0].FindControl("txtCode");
            //msTextBox txtCountry = (msTextBox)dgPublicHoliday.Items[0].FindControl("txtCountry");
            //if (txtCountry != null)
            //{
            //    txtCountry.ReadOnly = true;
            //}
            //if (txtStateCode != null)
            //{
            //    txtStateCode.ReadOnly = true;
            //}
            getPageControls(Page);
		}

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			chkApplyHolAll.Enabled = false;
			lblPHMessage.Text = "";
			m_sdsPublicHoliday = SysDataMgrDAL.GetEmptyPublicHoliday(1);
			dgPublicHoliday.CurrentPageIndex = 0;
			btnExecuteQuery.Enabled = true;
			ViewState["SCMode"] = ScreenMode.Query;
			ViewState["SCOperation"] = Operation.None;
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			ViewState["m_strCode"] = "";
			ViewState["prevRow"]=0;
			ViewState["index"]=0;
			dgPublicHoliday.EditItemIndex = 0;
			enableEditColumn(false);
			BindPublicHoliday();
		}

		private void btnExecuteQuery_Click(object sender, System.EventArgs e)
		{
			chkApplyHolAll.Enabled = false;

			lblPHMessage.Text="";
			dgPublicHoliday.EditItemIndex = -1;
			QueryPublicHoliday();
			btnExecuteQuery.Enabled = false;
			enableEditColumn(true);
			enableDeleteColumn(true);
			Utility.EnableButton(ref btnInsert, com.common.util.ButtonType.Insert,true,m_moduleAccessRights);
			BindPublicHoliday();
		}

		private void showCurrentPage()
		{
			SessionDS m_sdsQueryPublicHoliday = (SessionDS)Session["QUERY_DS"];
			int iStartIndex = dgPublicHoliday.CurrentPageIndex * dgPublicHoliday.PageSize;
			m_sdsPublicHoliday = SysDataMgrDAL.QueryPublicHoliday(m_sdsQueryPublicHoliday,m_strAppID,m_strEnterpriseID, iStartIndex,dgPublicHoliday.PageSize);
			int pgCnt = Convert.ToInt32((m_sdsPublicHoliday.QueryResultMaxSize - 1))/dgPublicHoliday.PageSize;
			if(pgCnt < dgPublicHoliday.CurrentPageIndex)
			{
				dgPublicHoliday.CurrentPageIndex = System.Convert.ToInt32(pgCnt);
			}
			Session["SESSION_DS1"] = m_sdsPublicHoliday;
		}
	}
}
