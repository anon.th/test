<%@ Page language="c#" Codebehind="InvManageList.aspx.cs" AutoEventWireup="false" Inherits="com.ties.InvManageList" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Import Namespace="com.common.util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>InvoiceGenerationPreview</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body onkeypress="microsoftKeyPress()" onclick="GetScrollPosition();" onload="SetScrollPosition();" onunload="window.top.displayBanner.fnCloseAll(0);" MS_POSITIONING="GridLayout">
		<form id="InvManageList" method="post" runat="server">
			<FONT face="Tahoma">
				<asp:label id="lblMainTitle" style="Z-INDEX: 100; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server" CssClass="mainTitleSize" Width="477px" Height="26px">Invoice Management Listing</asp:label><asp:label id="lblShowStatust" style="Z-INDEX: 109; LEFT: 175px; POSITION: absolute; TOP: 110px" runat="server" CssClass="mainTitleSize" Width="275px" Height="26px">[lblShowStatust]</asp:label><asp:label id="Label1" style="Z-INDEX: 108; LEFT: 11px; POSITION: absolute; TOP: 109px" runat="server" CssClass="mainTitleSize" Width="155px" Height="26px">Status Selected:</asp:label><asp:button id="btnPrintHeader" style="Z-INDEX: 107; LEFT: 547px; POSITION: absolute; TOP: 69px" runat="server" CssClass="queryButton" Width="154px" Text="Print Header Selected"></asp:button><asp:button id="btnCancelSelected" style="Z-INDEX: 106; LEFT: 387px; POSITION: absolute; TOP: 69px" runat="server" CssClass="queryButton" Width="154px" Text="Cancel Selected"></asp:button><asp:datagrid id="dgPreview" style="Z-INDEX: 105; LEFT: 8px; POSITION: absolute; TOP: 141px" runat="server" Width="890px" Height="95px" ItemStyle-Height="20" AutoGenerateColumns="False" SelectedItemStyle-CssClass="gridFieldSelected">
					<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
					<ItemStyle Height="10px" CssClass="gridField"></ItemStyle>
					<Columns>
						<asp:TemplateColumn>
							<HeaderStyle Width="20px" CssClass="gridHeading"></HeaderStyle>
							<ItemTemplate>
								<asp:CheckBox ID="chkSelect" Runat="server"></asp:CheckBox>
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:TemplateColumn>
							<HeaderStyle CssClass="gridHeading"></HeaderStyle>
							<ItemTemplate>
								&nbsp;&nbsp;
								<asp:ImageButton CommandName="Select" id="imgSelect" runat="server" ImageUrl="images/butt-select.gif"></asp:ImageButton>&nbsp;&nbsp;
							</ItemTemplate>
						</asp:TemplateColumn>
						<asp:BoundColumn DataField="invoice_no" HeaderText="Invoice No.">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="invoice_date" HeaderText="Generation Date">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="Due_date" HeaderText="Pickup Date">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="payment_mode" HeaderText="Invoice Type">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="payerid" HeaderText="Customer ID">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Left" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="invoice_adj_amount" HeaderText="Adjustment">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="invoice_amt" HeaderText="Total Invoice Amount">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
                        <asp:BoundColumn DataField="agency_charges" HeaderText="Agency Charges">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
						<asp:BoundColumn DataField="invoice_status" HeaderText="Invoice Status">
							<HeaderStyle HorizontalAlign="Center" CssClass="gridHeading"></HeaderStyle>
							<ItemStyle HorizontalAlign="Right" CssClass="gridField"></ItemStyle>
						</asp:BoundColumn>
					</Columns>
					<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
				</asp:datagrid><asp:button id="btnSelectAll" style="Z-INDEX: 104; LEFT: 117px; POSITION: absolute; TOP: 70px" runat="server" CssClass="queryButton" Width="96px" Text="Select All" CausesValidation="False"></asp:button><asp:button id="btnCancel" style="Z-INDEX: 101; LEFT: 11px; POSITION: absolute; TOP: 70px" runat="server" CssClass="queryButton" Width="96px" Text="Cancel" CausesValidation="False"></asp:button><asp:button id="btnPrintDetail" style="Z-INDEX: 102; LEFT: 707px; POSITION: absolute; TOP: 69px" runat="server" CssClass="queryButton" Width="153px" Text="Print Detail Selected" CausesValidation="False"></asp:button><asp:button id="btnApproveSelected" style="Z-INDEX: 103; LEFT: 226px; POSITION: absolute; TOP: 69px" runat="server" Width="154px" CssClass="queryButton" Text="Approve Selected"></asp:button></FONT></form>
	</body>
</HTML>
