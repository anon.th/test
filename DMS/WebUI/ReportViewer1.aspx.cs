using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CrystalDecisions.Web;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using CrystalDecisions.CrystalReports.Engine;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.common.applicationpages;
using com.ties.DAL;
using TIES.WebUI;
using System.IO;
using TIESDAL;
using TIES.WebUI.ConsignmentNote;
using TIES.WebUI.Manifest;


using ICSharpCode.SharpZipLib.Zip;
using System.Text;
using BarcodeLib;

namespace TIES.WebUI.ConsignmentNote
{
    /// <summary>
    /// Summary description for ReportViewer.
    /// </summary>
    public class ReportViewer : BasePopupPage
    {
        String strFormatType = null;
        string strappid = null;
        string strenterpriseid = null;
        string strUserLoggin = null;
        protected System.Web.UI.WebControls.Label lblErrorMesssage;
        ReportDocument orpt = null;

        public string pageTitle = "ReportViewer";

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                String strFormid = (String)Session["FORMID"];
                String strReportTemplate = (String)Session["REPORT_TEMPLATE"];
                strappid = utility.GetAppID();
                strenterpriseid = utility.GetEnterpriseID();
                strUserLoggin = utility.GetUserID();
                if (strFormid == "ExportXMLAWB")
                {
                    string fileName = "mawb" + DateTime.Now.ToString("yyyyMMddHHmm");

                    Response.Clear();
                    Response.ContentType = "text/csv";
                    Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}", fileName + ".txt"));
                    Response.Write(((DataSet)Session["DSExport"]).Tables[1].Rows[0][0].ToString());
                }
                if (strFormid == "PrintConsNotes")
                {

                    if (strReportTemplate == "PreprintConsignmentTNT.rpt")
                    {
                        pageTitle = "PrintConsignmentNote";
                        orpt = new PreprintConsignmentTNT2();
                        strFormatType = (String)Session["FormatType"];
                        DataSet dsConsignments = (DataSet)Session["SESSION_DS_PRINTCONSNOTES"];

                        CSS_PreprintConsNotes dsReport = new CSS_PreprintConsNotes();

                        #region PrintConsNotes
                        foreach (DataRow row in dsConsignments.Tables[1].Rows)
                        {
                            DataRow drXSD = dsReport.Tables["CSS_PrintConsNotes"].NewRow();
                            if ((row["consignment_no"] != null) && (!row["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["consignment_no"] = (string)row["consignment_no"];
                            }

                            if ((row["payerid"] != null) && (!row["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["payerid"] = (string)row["payerid"];
                            }

                            if ((row["service_code"] != null) && (!row["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["service_code"] = (string)row["service_code"];
                            }

                            if ((row["ref_no"] != null) && (!row["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["ref_no"] = (string)row["ref_no"];
                            }

                            if ((row["sender_name"] != null) && (!row["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_name"] = (string)row["sender_name"];
                            }
                            if ((row["sender_address1"] != null) && (!row["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_address1"] = (string)row["sender_address1"];
                            }

                            if ((row["sender_address2"] != null) && (!row["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_address2"] = (string)row["sender_address2"];
                            }

                            if ((row["sender_zipcode"] != null) && (!row["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_zipcode"] = (string)row["sender_zipcode"];
                            }


                            if ((row["sender_state"] != null) && (!row["sender_state"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_state"] = (string)row["sender_state"];
                            }

                            if ((row["sender_telephone"] != null) && (!row["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_telephone"] = (string)row["sender_telephone"];
                            }

                            if ((row["sender_fax"] != null) && (!row["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_fax"] = (string)row["sender_fax"];
                            }
                            if ((row["sender_contact_person"] != null) && (!row["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_contact_person"] = (string)row["sender_contact_person"];
                            }
                            if ((row["sender_email"] != null) && (!row["sender_email"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_email"] = (string)row["sender_email"];
                            }

                            if ((row["recipient_telephone"] != null) && (!row["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_telephone"] = (string)row["recipient_telephone"];
                            }
                            if ((row["recipient_name"] != null) && (!row["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_name"] = (string)row["recipient_name"];
                            }
                            if ((row["recipient_address1"] != null) && (!row["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_address1"] = (string)row["recipient_address1"];
                            }

                            if ((row["recipient_address2"] != null) && (!row["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_address2"] = (string)row["recipient_address2"];
                            }

                            if ((row["recipient_zipcode"] != null) && (!row["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_zipcode"] = ((string)row["recipient_zipcode"]).ToUpper();
                            }

                            if ((row["recipient_state"] != null) && (!row["recipient_state"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_state"] = (string)row["recipient_state"];
                            }

                            if ((row["recipient_fax"] != null) && (!row["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_fax"] = (string)row["recipient_fax"];
                            }
                            if ((row["recipient_contact_person"] != null) && (!row["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_contact_person"] = (string)row["recipient_contact_person"];
                            }
                            if ((row["declare_value"] != null) && (!row["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["declare_value"] = (decimal)row["declare_value"];
                            }
                            if ((row["cod_amount"] != null) && (!row["cod_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["cod_amount"] = (decimal)row["cod_amount"];
                            }
                            if ((row["remark"] != null) && (!row["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["remark"] = (string)row["remark"];
                            }
                            if ((row["return_pod_slip"] != null) && (!row["return_pod_slip"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["return_pod_slip"] = (string)row["return_pod_slip"];
                            }
                            if ((row["return_invoice_hc"] != null) && (!row["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["return_invoice_hc"] = (string)row["return_invoice_hc"];
                            }
                            if ((row["DangerousGoods"] != null) && (!row["DangerousGoods"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["DangerousGoods"] = (byte)row["DangerousGoods"];
                            }
                            if ((row["MoreInfo"] != null) && (!row["MoreInfo"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["MoreInfo"] = (string)row["MoreInfo"];
                            }
                            if ((row["MorePkgs"] != null) && (!row["MorePkgs"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["MorePkgs"] = (string)row["MorePkgs"];
                            }
                            if ((row["Qty1"] != null) && (!row["Qty1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty1"] = (int)row["Qty1"];
                            }
                            if ((row["Qty2"] != null) && (!row["Qty2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty2"] = (int)row["Qty2"];
                            }
                            if ((row["Qty3"] != null) && (!row["Qty3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty3"] = (int)row["Qty3"];
                            }
                            if ((row["Qty4"] != null) && (!row["Qty4"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty4"] = (int)row["Qty4"];
                            }
                            if ((row["Qty5"] != null) && (!row["Qty5"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty5"] = (int)row["Qty5"];
                            }
                            if ((row["Qty6"] != null) && (!row["Qty6"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty6"] = (int)row["Qty6"];
                            }
                            if ((row["Qty7"] != null) && (!row["Qty7"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty7"] = (int)row["Qty7"];
                            }
                            if ((row["Qty8"] != null) && (!row["Qty8"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty8"] = (int)row["Qty8"];
                            }
                            if ((row["Qty"] != null) && (!row["Qty"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty"] = (int)row["Qty"];
                            }

                            if ((row["Wgt1"] != null) && (!row["Wgt1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt1"] = (decimal)row["Wgt1"];
                            }
                            if ((row["Wgt2"] != null) && (!row["Wgt2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt2"] = (decimal)row["Wgt2"];
                            }
                            if ((row["Wgt3"] != null) && (!row["Wgt3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt3"] = (decimal)row["Wgt3"];
                            }
                            if ((row["Wgt4"] != null) && (!row["Wgt4"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt4"] = (decimal)row["Wgt4"];
                            }
                            if ((row["Wgt5"] != null) && (!row["Wgt5"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt5"] = (decimal)row["Wgt5"];
                            }
                            if ((row["Wgt6"] != null) && (!row["Wgt6"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt6"] = (decimal)row["Wgt6"];
                            }
                            if ((row["Wgt7"] != null) && (!row["Wgt7"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt7"] = (decimal)row["Wgt7"];
                            }
                            if ((row["Wgt8"] != null) && (!row["Wgt8"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt8"] = (decimal)row["Wgt8"];
                            }
                            if ((row["Wgt"] != null) && (!row["Wgt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt"] = (decimal)row["Wgt"];
                            }
                            if ((row["CopyName"] != null) && (!row["CopyName"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["CopyName"] = (string)row["CopyName"];
                            }
                            if ((row["sender_country"] != null) && (!row["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_country"] = (string)row["sender_country"];
                            }
                            if ((row["recipient_country"] != null) && (!row["recipient_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_country"] = (string)row["recipient_country"];
                            }
                            if ((row["GoodsDescription"] != null) && (!row["GoodsDescription"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["GoodsDescription"] = (string)row["GoodsDescription"];
                            }

                            string Barcode = "*" + drXSD["consignment_no"].ToString().ToUpper().ToUpper() + "*";

                            Barcode bcd = new Barcode();
                            bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                            bcd.IncludeLabel = false;
                            bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                            bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 240, 65);

                            MemoryStream ms = new MemoryStream();
                            bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                            Byte[] oByte = ms.ToArray();
                            drXSD["imgBarcode"] = oByte;

                            dsReport.Tables["CSS_PrintConsNotes"].Rows.Add(drXSD);
                        }
                        #endregion PrintConsNotes
                        orpt.SetDataSource(dsReport);
                    }
                }
                else if (strFormid == "PrintConsNotesLabel")
                {

                    if (strReportTemplate == "PreprintConsignmentTNT.rpt")
                    {
                        pageTitle = "PrintConsignmentNote";
                        orpt = new PreprintConsignmentTNT3();
                        orpt.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
                        strFormatType = (String)Session["FormatType"];
                        DataSet dsConsignments = (DataSet)Session["SESSION_DS_PRINTCONSNOTES"];

                        CSS_PreprintConsNotes dsReport = new CSS_PreprintConsNotes();

                        #region PrintConsNotes
                        foreach (DataRow row in dsConsignments.Tables[1].Rows)
                        {
                            DataRow drXSD = dsReport.Tables["CSS_PrintConsNotes"].NewRow();
                            if ((row["consignment_no"] != null) && (!row["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["consignment_no"] = (string)row["consignment_no"];
                            }

                            if ((row["payerid"] != null) && (!row["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["payerid"] = (string)row["payerid"];
                            }

                            if ((row["service_code"] != null) && (!row["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["service_code"] = (string)row["service_code"];
                            }

                            if ((row["ref_no"] != null) && (!row["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["ref_no"] = (string)row["ref_no"];
                            }

                            if ((row["sender_name"] != null) && (!row["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_name"] = (string)row["sender_name"];
                            }
                            if ((row["sender_address1"] != null) && (!row["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_address1"] = (string)row["sender_address1"];
                            }

                            if ((row["sender_address2"] != null) && (!row["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_address2"] = (string)row["sender_address2"];
                            }

                            if ((row["sender_zipcode"] != null) && (!row["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_zipcode"] = (string)row["sender_zipcode"];
                            }


                            if ((row["sender_state"] != null) && (!row["sender_state"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_state"] = (string)row["sender_state"];
                            }

                            if ((row["sender_telephone"] != null) && (!row["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_telephone"] = (string)row["sender_telephone"];
                            }

                            if ((row["sender_fax"] != null) && (!row["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_fax"] = (string)row["sender_fax"];
                            }
                            if ((row["sender_contact_person"] != null) && (!row["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_contact_person"] = (string)row["sender_contact_person"];
                            }
                            if ((row["sender_email"] != null) && (!row["sender_email"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_email"] = (string)row["sender_email"];
                            }

                            if ((row["recipient_telephone"] != null) && (!row["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_telephone"] = (string)row["recipient_telephone"];
                            }
                            if ((row["recipient_name"] != null) && (!row["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_name"] = (string)row["recipient_name"];
                            }
                            if ((row["recipient_address1"] != null) && (!row["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_address1"] = (string)row["recipient_address1"];
                            }

                            if ((row["recipient_address2"] != null) && (!row["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_address2"] = (string)row["recipient_address2"];
                            }

                            if ((row["recipient_zipcode"] != null) && (!row["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_zipcode"] = ((string)row["recipient_zipcode"]).ToUpper();
                            }

                            if ((row["recipient_state"] != null) && (!row["recipient_state"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_state"] = (string)row["recipient_state"];
                            }

                            if ((row["recipient_fax"] != null) && (!row["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_fax"] = (string)row["recipient_fax"];
                            }
                            if ((row["recipient_contact_person"] != null) && (!row["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_contact_person"] = (string)row["recipient_contact_person"];
                            }
                            if ((row["declare_value"] != null) && (!row["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["declare_value"] = (decimal)row["declare_value"];
                            }
                            if ((row["cod_amount"] != null) && (!row["cod_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["cod_amount"] = (decimal)row["cod_amount"];
                            }
                            if ((row["remark"] != null) && (!row["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["remark"] = (string)row["remark"];
                            }
                            if ((row["return_pod_slip"] != null) && (!row["return_pod_slip"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["return_pod_slip"] = (string)row["return_pod_slip"];
                            }
                            if ((row["return_invoice_hc"] != null) && (!row["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["return_invoice_hc"] = (string)row["return_invoice_hc"];
                            }
                            if ((row["DangerousGoods"] != null) && (!row["DangerousGoods"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["DangerousGoods"] = (byte)row["DangerousGoods"];
                            }
                            if ((row["MoreInfo"] != null) && (!row["MoreInfo"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["MoreInfo"] = (string)row["MoreInfo"];
                            }
                            if ((row["MorePkgs"] != null) && (!row["MorePkgs"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["MorePkgs"] = (string)row["MorePkgs"];
                            }
                            if ((row["Qty1"] != null) && (!row["Qty1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty1"] = (int)row["Qty1"];
                            }
                            if ((row["Qty2"] != null) && (!row["Qty2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty2"] = (int)row["Qty2"];
                            }
                            if ((row["Qty3"] != null) && (!row["Qty3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty3"] = (int)row["Qty3"];
                            }
                            if ((row["Qty4"] != null) && (!row["Qty4"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty4"] = (int)row["Qty4"];
                            }
                            if ((row["Qty5"] != null) && (!row["Qty5"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty5"] = (int)row["Qty5"];
                            }
                            if ((row["Qty6"] != null) && (!row["Qty6"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty6"] = (int)row["Qty6"];
                            }
                            if ((row["Qty7"] != null) && (!row["Qty7"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty7"] = (int)row["Qty7"];
                            }
                            if ((row["Qty8"] != null) && (!row["Qty8"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty8"] = (int)row["Qty8"];
                            }
                            if ((row["Qty"] != null) && (!row["Qty"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty"] = (int)row["Qty"];
                            }

                            if ((row["Wgt1"] != null) && (!row["Wgt1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt1"] = (decimal)row["Wgt1"];
                            }
                            if ((row["Wgt2"] != null) && (!row["Wgt2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt2"] = (decimal)row["Wgt2"];
                            }
                            if ((row["Wgt3"] != null) && (!row["Wgt3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt3"] = (decimal)row["Wgt3"];
                            }
                            if ((row["Wgt4"] != null) && (!row["Wgt4"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt4"] = (decimal)row["Wgt4"];
                            }
                            if ((row["Wgt5"] != null) && (!row["Wgt5"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt5"] = (decimal)row["Wgt5"];
                            }
                            if ((row["Wgt6"] != null) && (!row["Wgt6"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt6"] = (decimal)row["Wgt6"];
                            }
                            if ((row["Wgt7"] != null) && (!row["Wgt7"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt7"] = (decimal)row["Wgt7"];
                            }
                            if ((row["Wgt8"] != null) && (!row["Wgt8"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt8"] = (decimal)row["Wgt8"];
                            }
                            if ((row["Wgt"] != null) && (!row["Wgt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt"] = (decimal)row["Wgt"];
                            }
                            if ((row["CopyName"] != null) && (!row["CopyName"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["CopyName"] = (string)row["CopyName"];
                            }
                            if ((row["sender_country"] != null) && (!row["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_country"] = (string)row["sender_country"];
                            }
                            if ((row["recipient_country"] != null) && (!row["recipient_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_country"] = (string)row["recipient_country"];
                            }
                            if ((row["GoodsDescription"] != null) && (!row["GoodsDescription"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["GoodsDescription"] = (string)row["GoodsDescription"];
                            }

                            string Barcode = "*" + drXSD["consignment_no"].ToString().ToUpper().ToUpper() + "*";

                            Barcode bcd = new Barcode();
                            bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                            bcd.IncludeLabel = false;
                            bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                            bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 240, 65);

                            MemoryStream ms = new MemoryStream();
                            bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                            Byte[] oByte = ms.ToArray();
                            drXSD["imgBarcode"] = oByte;

                            dsReport.Tables["CSS_PrintConsNotes"].Rows.Add(drXSD);
                        }
                        #endregion PrintConsNotes
                        orpt.SetDataSource(dsReport);
                    }
                }
                else if (strFormid == "PrintShipping")
                {
                    #region PrintShipping

                    pageTitle = "PrintShipping";
                    orpt = new PreprintShippingTNT();
                    strFormatType = (String)Session["FormatType"];
                    DataSet dsShipping = (DataSet)Session["SESSION_DS1_ShipmentList"];

                    CSS_PreprintConsNotes dsReport = new CSS_PreprintConsNotes();

                    foreach (DataRow row in dsShipping.Tables[1].Rows)
                    {
                        DataRow drXSD = dsReport.Tables["CSS_PrintShippingList"].NewRow();
                        if ((row["enterprise_name"] != null) && (!row["enterprise_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["enterprise_name"] = (string)row["enterprise_name"];
                        }

                        if ((row["enterprise_address"] != null) && (!row["enterprise_address"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["enterprise_address"] = (string)row["enterprise_address"];
                        }

                        if ((row["enterprise_telephone"] != null) && (!row["enterprise_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["enterprise_telephone"] = (string)row["enterprise_telephone"];
                        }

                        if ((row["custid"] != null) && (!row["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["custid"] = (string)row["custid"];
                        }

                        if ((row["customer_address"] != null) && (!row["customer_address"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["customer_address"] = (string)row["customer_address"];
                        }
                        if ((row["customer_zipcode"] != null) && (!row["customer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["customer_zipcode"] = (string)row["customer_zipcode"];
                        }

                        if ((row["customer_telephone"] != null) && (!row["customer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["customer_telephone"] = (string)row["customer_telephone"];
                        }

                        if ((row["sender_name"] != null) && (!row["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["sender_name"] = (string)row["sender_name"];
                        }


                        if ((row["sender_address"] != null) && (!row["sender_address"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["sender_address"] = (string)row["sender_address"];
                        }

                        if ((row["sender_zipcode"] != null) && (!row["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["sender_zipcode"] = (string)row["sender_zipcode"];
                        }

                        if ((row["sender_telephone"] != null) && (!row["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["sender_telephone"] = (string)row["sender_telephone"];
                        }
                        if ((row["consignment_no"] != null) && (!row["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["consignment_no"] = (string)row["consignment_no"];
                        }
                        if ((row["total_packages"] != null) && (!row["total_packages"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["total_packages"] = (int)row["total_packages"];
                        }

                        if ((row["service_code"] != null) && (!row["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["service_code"] = (string)row["service_code"];
                        }
                        if ((row["recipient_telephone"] != null) && (!row["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["recipient_telephone"] = (string)row["recipient_telephone"];
                        }
                        if ((row["recipient_name"] != null) && (!row["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["recipient_name"] = (string)row["recipient_name"];
                        }

                        if ((row["recipient_address"] != null) && (!row["recipient_address"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["recipient_address"] = (string)row["recipient_address"];
                        }

                        if ((row["recipient_zipcode"] != null) && (!row["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["recipient_zipcode"] = (string)row["recipient_zipcode"];
                        }

                        if ((row["state_name"] != null) && (!row["state_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["state_name"] = (string)row["state_name"];
                        }
                        if ((row["ShippingList_No"] != null) && (!row["ShippingList_No"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["ShippingList_No"] = (string)row["ShippingList_No"];
                        }

                        if ((row["cust_name"] != null) && (!row["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["cust_name"] = (string)row["cust_name"];
                        }

                        if ((row["Printed_Date_Time"] != null) && (!row["Printed_Date_Time"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["Printed_Date_Time"] = (DateTime)row["Printed_Date_Time"];
                        }

                        if ((row["GoodsDescription"] != null) && (!row["GoodsDescription"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["GoodsDescription"] = (string)row["GoodsDescription"];
                        }

                        if ((row["cost_centre"] != null) && (!row["cost_centre"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["cost_centre"] = (string)row["cost_centre"];
                        }

                        string Barcode = "*" + drXSD["ShippingList_No"].ToString().ToUpper() + "*";

                        Barcode bcd = new Barcode();
                        bcd.Alignment = BarcodeLib.AlignmentPositions.LEFT;
                        bcd.IncludeLabel = false;
                        bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                        bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 265, 65);

                        MemoryStream ms = new MemoryStream();
                        bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                        Byte[] oByte = ms.ToArray();
                        drXSD["imgBarcode"] = oByte;


                        dsReport.Tables["CSS_PrintShippingList"].Rows.Add(drXSD);
                    }
                    orpt.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
                    orpt.PrintOptions.PaperSize = PaperSize.PaperA4;
                    orpt.SetDataSource(dsReport);
                    #endregion PrintShipping
                }
                else if (strFormid == "ChequeReq")
                {
                    #region TNT_Formal_ChequeReq_Adhoc
                    pageTitle = "Cheque Req Report";
                    TNT_Formal_ChequeReq objReport = new TNT_Formal_ChequeReq();
                    objReport.ResourceName = strReportTemplate;
                    orpt = objReport;
                    strFormatType = (String)Session["FormatType"];
                    DataSet dsChequeRep = (DataSet)Session["SESSION_DS_ChequeReq"];
                    CustomDS dsReport = new CustomDS();

                    foreach (DataRow drData in dsChequeRep.Tables[0].Rows)
                    {
                        DataRow dr = dsReport.Tables["ChequeReq_Formal"].NewRow();

                        // EnterpriseName
                        if (drData["EnterpriseName"] != null && drData["EnterpriseName"].ToString() != "")
                        {
                            dr["EnterpriseName"] = drData["EnterpriseName"].ToString();
                        }

                        // EnterpriseAddress1
                        if (drData["EnterpriseAddress1"] != null && drData["EnterpriseAddress1"].ToString() != "")
                        {
                            dr["EnterpriseAddress1"] = drData["EnterpriseAddress1"].ToString();
                        }

                        // EnterpriseAddress2
                        if (drData["EnterpriseAddress2"] != null && drData["EnterpriseAddress2"].ToString() != "")
                        {
                            dr["EnterpriseAddress2"] = drData["EnterpriseAddress2"].ToString();
                        }

                        // ChequeRequisitionNo
                        if (drData["ChequeRequisitionNo"] != null && drData["ChequeRequisitionNo"].ToString() != "")
                        {
                            dr["ChequeRequisitionNo"] = drData["ChequeRequisitionNo"].ToString();
                        }

                        // ChequeRequisitionStatus
                        if (drData["ChequeRequisitionStatus"] != null && drData["ChequeRequisitionStatus"].ToString() != "")
                        {
                            dr["ChequeRequisitionStatus"] = drData["ChequeRequisitionStatus"].ToString();
                        }

                        // Payee
                        if (drData["Payee"] != null && drData["Payee"].ToString() != "")
                        {
                            dr["Payee"] = drData["Payee"].ToString();
                        }

                        // ChequeNo
                        if (drData["ChequeNo"] != null && drData["ChequeNo"].ToString() != "")
                        {
                            dr["ChequeNo"] = drData["ChequeNo"].ToString();
                        }

                        // ChequeIssueDate --
                        if (drData["ChequeIssueDate"] != null && drData["ChequeIssueDate"].ToString() != "")
                        {
                            dr["ChequeIssueDate"] = Convert.ToDateTime(drData["ChequeIssueDate"].ToString());
                        }

                        // ReceiptNo
                        if (drData["ReceiptNo"] != null && drData["ReceiptNo"].ToString() != "")
                        {
                            dr["ReceiptNo"] = drData["ReceiptNo"].ToString();
                        }

                        // ReceiptDate --
                        if (drData["ReceiptDate"] != null && drData["ReceiptDate"].ToString() != "")
                        {
                            dr["ReceiptDate"] = Convert.ToDateTime(drData["ReceiptDate"].ToString());
                        }

                        // PreparedBy
                        if (drData["PreparedBy"] != null && drData["PreparedBy"].ToString() != "")
                        {
                            dr["PreparedBy"] = drData["PreparedBy"].ToString();
                        }

                        // PreparedDT --
                        if (drData["PreparedDT"] != null && drData["PreparedDT"].ToString() != "")
                        {
                            dr["PreparedDT"] = Convert.ToDateTime(drData["PreparedDT"].ToString());
                        }

                        // ApprovedBy
                        if (drData["ApprovedBy"] != null && drData["ApprovedBy"].ToString() != "")
                        {
                            dr["ApprovedBy"] = drData["ApprovedBy"].ToString();
                        }

                        // ApprovedDT ---
                        if (drData["ApprovedDT"] != null && drData["ApprovedDT"].ToString() != "")
                        {
                            dr["ApprovedDT"] = Convert.ToDateTime(drData["ApprovedDT"].ToString());
                        }

                        // CompletedBy
                        if (drData["CompletedBy"] != null && drData["CompletedBy"].ToString() != "")
                        {
                            dr["CompletedBy"] = drData["CompletedBy"].ToString();
                        }

                        // CompletedDT --
                        if (drData["CompletedDT"] != null && drData["CompletedDT"].ToString() != "")
                        {
                            dr["CompletedDT"] = Convert.ToDateTime(drData["CompletedDT"].ToString());
                        }

                        // TotalChequeReq
                        if (drData["TotalChequeReq"] != null && drData["TotalChequeReq"].ToString() != "")
                        {
                            dr["TotalChequeReq"] = Convert.ToDecimal(drData["TotalChequeReq"]).ToString("N2");
                        }

                        // PrintedDT --
                        if (drData["PrintedDT"] != null && drData["PrintedDT"].ToString() != "")
                        {
                            dr["PrintedDT"] = Convert.ToDateTime(drData["PrintedDT"].ToString());
                        }

                        // InvoiceNo
                        if ((drData["InvoiceNo"] != null) && (!drData["InvoiceNo"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["InvoiceNo"] = drData["InvoiceNo"].ToString();
                        }

                        // Client
                        if ((drData["Client"] != null) && (!drData["Client"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["Client"] = drData["Client"].ToString();
                        }
                        // Assessment
                        if ((drData["Assessment"] != null) && (!drData["Assessment"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["Assessment"] = drData["Assessment"].ToString();
                        }
                        // AssessmentDate
                        if (drData["AssessmentDate"] != null && drData["AssessmentDate"].ToString() != "")
                        {
                            dr["AssessmentDate"] = Convert.ToDateTime(drData["AssessmentDate"].ToString());
                        }
                        // DutyTax
                        if ((drData["DutyTax"] != null) && (!drData["DutyTax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["DutyTax"] = Convert.ToDecimal(drData["DutyTax"]).ToString("N2");
                        }
                        // EPF
                        if ((drData["EPF"] != null) && (!drData["EPF"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["EPF"] = Convert.ToDecimal(drData["EPF"]).ToString("N2");
                        }

                        // Total
                        if ((drData["Total"] != null) && (!drData["Total"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["Total"] = Convert.ToDecimal(drData["Total"]).ToString("N2");
                        }
                        
                        dsReport.Tables[2].Rows.Add(dr);
                    }

                    int numberOfRow = 25 - dsReport.Tables[2].Rows.Count;

                    if (numberOfRow < 25)
                    {
                        for (int i = 0; i <= numberOfRow - 1; i++)
                        {
                            DataRow drEmpty = dsReport.Tables[2].NewRow();
                            DataRow drCurrent = dsReport.Tables[2].Rows[0];
                            // TotalChequeReq
                            if (drCurrent["TotalChequeReq"] != null && drCurrent["TotalChequeReq"].ToString() != "")
                            {
                                drEmpty["TotalChequeReq"] = Convert.ToDecimal(drCurrent["TotalChequeReq"]).ToString("N2");
                            }

                            // PrintedDT --
                            if (drCurrent["PrintedDT"] != null && drCurrent["PrintedDT"].ToString() != "")
                            {
                                drEmpty["PrintedDT"] = Convert.ToDateTime(drCurrent["PrintedDT"].ToString());
                            }

                            dsReport.Tables[2].Rows.Add(drEmpty);
                        }
                    }

                    orpt.SetDataSource(dsReport);
                    #endregion
                }
                else if (strFormid == "TNT_Formal_ChequeReq_Adhoc")
                {
                    #region TNT_Formal_ChequeReq_Adhoc

                    pageTitle = "TNT_Formal_ChequeReq_Adhoc";

                    TNT_Formal_ChequeReq objReport = new TNT_Formal_ChequeReq();
                    objReport.ResourceName = strReportTemplate;
                    orpt = objReport;

                    strFormatType = (String)Session["FormatType"];
                    DataSet dsChequeRep = (DataSet)Session["SESSION_DS_PRINTCHEQUEREQ_FORMAL"];
                    CustomDS dsReport = new CustomDS();

                    foreach (DataRow drData in dsChequeRep.Tables[0].Rows)
                    {
                        DataRow dr = dsReport.Tables["ChequeReq_Formal"].NewRow();

                        // EnterpriseName
                        if (drData["EnterpriseName"] != null && drData["EnterpriseName"].ToString() != "")
                        {
                            dr["EnterpriseName"] = drData["EnterpriseName"].ToString();
                        }

                        // EnterpriseAddress1
                        if (drData["EnterpriseAddress1"] != null && drData["EnterpriseAddress1"].ToString() != "")
                        {
                            dr["EnterpriseAddress1"] = drData["EnterpriseAddress1"].ToString();
                        }

                        // EnterpriseAddress2
                        if (drData["EnterpriseAddress2"] != null && drData["EnterpriseAddress2"].ToString() != "")
                        {
                            dr["EnterpriseAddress2"] = drData["EnterpriseAddress2"].ToString();
                        }

                        // ChequeRequisitionNo
                        if (drData["ChequeRequisitionNo"] != null && drData["ChequeRequisitionNo"].ToString() != "")
                        {
                            dr["ChequeRequisitionNo"] = drData["ChequeRequisitionNo"].ToString();
                        }

                        // ChequeRequisitionStatus
                        if (drData["ChequeRequisitionStatus"] != null && drData["ChequeRequisitionStatus"].ToString() != "")
                        {
                            dr["ChequeRequisitionStatus"] = drData["ChequeRequisitionStatus"].ToString();
                        }

                        // Payee
                        if (drData["Payee"] != null && drData["Payee"].ToString() != "")
                        {
                            dr["Payee"] = drData["Payee"].ToString();
                        }

                        // ChequeNo
                        if (drData["ChequeNo"] != null && drData["ChequeNo"].ToString() != "")
                        {
                            dr["ChequeNo"] = drData["ChequeNo"].ToString();
                        }

                        // ChequeIssueDate --
                        if (drData["ChequeIssueDate"] != null && drData["ChequeIssueDate"].ToString() != "")
                        {
                            dr["ChequeIssueDate"] = Convert.ToDateTime(drData["ChequeIssueDate"].ToString());
                        }

                        // ReceiptNo
                        if (drData["ReceiptNo"] != null && drData["ReceiptNo"].ToString() != "")
                        {
                            dr["ReceiptNo"] = drData["ReceiptNo"].ToString();
                        }

                        // ReceiptDate --
                        if (drData["ReceiptDate"] != null && drData["ReceiptDate"].ToString() != "")
                        {
                            dr["ReceiptDate"] = Convert.ToDateTime(drData["ReceiptDate"].ToString());
                        }

                        // PreparedBy
                        if (drData["PreparedBy"] != null && drData["PreparedBy"].ToString() != "")
                        {
                            dr["PreparedBy"] = drData["PreparedBy"].ToString();
                        }

                        // PreparedDT --
                        if (drData["PreparedDT"] != null && drData["PreparedDT"].ToString() != "")
                        {
                            dr["PreparedDT"] = Convert.ToDateTime(drData["PreparedDT"].ToString());
                        }

                        // ApprovedBy
                        if (drData["ApprovedBy"] != null && drData["ApprovedBy"].ToString() != "")
                        {
                            dr["ApprovedBy"] = drData["ApprovedBy"].ToString();
                        }

                        // ApprovedDT ---
                        if (drData["ApprovedDT"] != null && drData["ApprovedDT"].ToString() != "")
                        {
                            dr["ApprovedDT"] = Convert.ToDateTime(drData["ApprovedDT"].ToString());
                        }

                        // CompletedBy
                        if (drData["CompletedBy"] != null && drData["CompletedBy"].ToString() != "")
                        {
                            dr["CompletedBy"] = drData["CompletedBy"].ToString();
                        }

                        // CompletedDT --
                        if (drData["CompletedDT"] != null && drData["CompletedDT"].ToString() != "")
                        {
                            dr["CompletedDT"] = Convert.ToDateTime(drData["CompletedDT"].ToString());
                        }

                        // TotalChequeReq
                        if (drData["TotalChequeReq"] != null && drData["TotalChequeReq"].ToString() != "")
                        {
                            dr["TotalChequeReq"] = Convert.ToDecimal(drData["TotalChequeReq"]).ToString("N2");
                        }

                        // PrintedDT --
                        if (drData["PrintedDT"] != null && drData["PrintedDT"].ToString() != "")
                        {
                            dr["PrintedDT"] = Convert.ToDateTime(drData["PrintedDT"].ToString());
                        }

                        // InvoiceNo
                        if ((drData["InvoiceNo"] != null) && (!drData["InvoiceNo"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["InvoiceNo"] = drData["InvoiceNo"].ToString();
                        }

                        // Client
                        if ((drData["Client"] != null) && (!drData["Client"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["Client"] = drData["Client"].ToString();
                        }
                        // Assessment
                        if ((drData["Assessment"] != null) && (!drData["Assessment"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["Assessment"] = drData["Assessment"].ToString();
                        }
                        // AssessmentDate
                        if (drData["AssessmentDate"] != null && drData["AssessmentDate"].ToString() != "")
                        {
                            dr["AssessmentDate"] = Convert.ToDateTime(drData["AssessmentDate"].ToString());
                        }
                        // DutyTax
                        if ((drData["DutyTax"] != null) && (!drData["DutyTax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["DutyTax"] = Convert.ToDecimal(drData["DutyTax"]).ToString("N2");
                        }
                        // EPF
                        if ((drData["EPF"] != null) && (!drData["EPF"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["EPF"] = Convert.ToDecimal(drData["EPF"]).ToString("N2");
                        }

                        // Total
                        if ((drData["Total"] != null) && (!drData["Total"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["Total"] = Convert.ToDecimal(drData["Total"]).ToString("N2");
                        }

                        //03072019 Pongsakorn
                        string Barcode = "*" + dr["ChequeRequisitionNo"].ToString().ToUpper().ToUpper() + "*";

                        Barcode bcd = new Barcode();
                        bcd.Alignment = BarcodeLib.AlignmentPositions.RIGHT;
                        bcd.IncludeLabel = false;
                        bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                        bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 240, 65);

                        MemoryStream ms = new MemoryStream();
                        bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                        Byte[] oByte = ms.ToArray();
                        dr["imgBarcode"] = oByte;

                        //

                        dsReport.Tables[2].Rows.Add(dr);
                    }

                    int numberOfRow = 25 - dsReport.Tables[2].Rows.Count;

                    if (numberOfRow < 25)
                    {
                        for (int i = 0; i <= numberOfRow - 1; i++)
                        {
                            DataRow drEmpty = dsReport.Tables[2].NewRow();
                            DataRow drCurrent = dsReport.Tables[2].Rows[0];
                            // TotalChequeReq
                            if (drCurrent["TotalChequeReq"] != null && drCurrent["TotalChequeReq"].ToString() != "")
                            {
                                drEmpty["TotalChequeReq"] = Convert.ToDecimal(drCurrent["TotalChequeReq"]).ToString("N2");
                            }

                            // PrintedDT --
                            if (drCurrent["PrintedDT"] != null && drCurrent["PrintedDT"].ToString() != "")
                            {
                                drEmpty["PrintedDT"] = Convert.ToDateTime(drCurrent["PrintedDT"].ToString());
                            }

                            dsReport.Tables[2].Rows.Add(drEmpty);
                        }
                    }

                    orpt.SetDataSource(dsReport);
                    #endregion
                }

                else if (strFormid == "TNT_ITF_ChequeReq")
                {
                    //					if(strReportTemplate.Equals("TNT_ITF_ChequeReq.rpt"))
                    //					{
                    #region TNT_ITF_ChequeReq

                    pageTitle = "TNT_ITF_ChequeReq";

                    TNT_ITF_ChequeReq objReport = new TNT_ITF_ChequeReq();
                    objReport.ResourceName = strReportTemplate;
                    orpt = objReport;

                    strFormatType = (String)Session["FormatType"];
                    DataSet dsChequeRep = (DataSet)Session["SESSION_DS_PRINTCHEQUEREQ_ITF"];
                    CustomDS dsReport = new CustomDS();

                    foreach (DataRow drData in dsChequeRep.Tables[0].Rows)
                    {
                        DataRow dr = dsReport.Tables["ChequeReq_ITF"].NewRow();

                        // EnterpriseName
                        if (drData["EnterpriseName"] != null && drData["EnterpriseName"].ToString() != "")
                        {
                            dr["EnterpriseName"] = drData["EnterpriseName"].ToString();
                        }

                        // EnterpriseAddress1
                        if (drData["EnterpriseAddress1"] != null && drData["EnterpriseAddress1"].ToString() != "")
                        {
                            dr["EnterpriseAddress1"] = drData["EnterpriseAddress1"].ToString();
                        }

                        // EnterpriseAddress2
                        if (drData["EnterpriseAddress2"] != null && drData["EnterpriseAddress2"].ToString() != "")
                        {
                            dr["EnterpriseAddress2"] = drData["EnterpriseAddress2"].ToString();
                        }

                        // ChequeRequisitionNo
                        if (drData["ChequeRequisitionNo"] != null && drData["ChequeRequisitionNo"].ToString() != "")
                        {
                            dr["ChequeRequisitionNo"] = drData["ChequeRequisitionNo"].ToString();
                        }

                        // ChequeRequisitionStatus
                        if (drData["ChequeRequisitionStatus"] != null && drData["ChequeRequisitionStatus"].ToString() != "")
                        {
                            dr["ChequeRequisitionStatus"] = drData["ChequeRequisitionStatus"].ToString();
                        }

                        // Payee
                        if (drData["Payee"] != null && drData["Payee"].ToString() != "")
                        {
                            dr["Payee"] = drData["Payee"].ToString();
                        }

                        // ChequeNo
                        if (drData["ChequeNo"] != null && drData["ChequeNo"].ToString() != "")
                        {
                            dr["ChequeNo"] = drData["ChequeNo"].ToString();
                        }

                        // ChequeIssueDate --
                        if (drData["ChequeIssueDate"] != null && drData["ChequeIssueDate"].ToString() != "")
                        {
                            dr["ChequeIssueDate"] = Convert.ToDateTime(drData["ChequeIssueDate"].ToString());
                        }

                        // ReceiptNo
                        if (drData["ReceiptNo"] != null && drData["ReceiptNo"].ToString() != "")
                        {
                            dr["ReceiptNo"] = drData["ReceiptNo"].ToString();
                        }

                        // ReceiptDate --
                        if (drData["ReceiptDate"] != null && drData["ReceiptDate"].ToString() != "")
                        {
                            dr["ReceiptDate"] = Convert.ToDateTime(drData["ReceiptDate"].ToString());
                        }

                        // PreparedBy
                        if (drData["PreparedBy"] != null && drData["PreparedBy"].ToString() != "")
                        {
                            dr["PreparedBy"] = drData["PreparedBy"].ToString();
                        }

                        // PreparedDT --
                        if (drData["PreparedDT"] != null && drData["PreparedDT"].ToString() != "")
                        {
                            dr["PreparedDT"] = Convert.ToDateTime(drData["PreparedDT"].ToString());
                        }

                        // ApprovedBy
                        if (drData["ApprovedBy"] != null && drData["ApprovedBy"].ToString() != "")
                        {
                            dr["ApprovedBy"] = drData["ApprovedBy"].ToString();
                        }

                        // ApprovedDT ---
                        if (drData["ApprovedDT"] != null && drData["ApprovedDT"].ToString() != "")
                        {
                            dr["ApprovedDT"] = Convert.ToDateTime(drData["ApprovedDT"].ToString());
                        }

                        // CompletedBy
                        if (drData["CompletedBy"] != null && drData["CompletedBy"].ToString() != "")
                        {
                            dr["CompletedBy"] = drData["CompletedBy"].ToString();
                        }

                        // CompletedDT --
                        if (drData["CompletedDT"] != null && drData["CompletedDT"].ToString() != "")
                        {
                            dr["CompletedDT"] = Convert.ToDateTime(drData["CompletedDT"].ToString());
                        }

                        // MasterAWBNumber
                        if (drData["MasterAWBNumber"] != null && drData["MasterAWBNumber"].ToString() != "")
                        {
                            dr["MasterAWBNumber"] = drData["MasterAWBNumber"].ToString();
                        }

                        // ITFAmount
                        if (drData["ITFAmount"] != null && drData["ITFAmount"].ToString() != "")
                        {
                            dr["ITFAmount"] = Convert.ToDecimal(drData["ITFAmount"]).ToString("N2");
                        }

                        // FreightCollectAmount
                        if (drData["FreightCollectAmount"] != null && drData["FreightCollectAmount"].ToString() != "")
                        {
                            dr["FreightCollectAmount"] = Convert.ToDecimal(drData["FreightCollectAmount"]).ToString("N2");
                        }

                        // TotalChequeReq
                        if (drData["TotalChequeReq"] != null && drData["TotalChequeReq"].ToString() != "")
                        {
                            dr["TotalChequeReq"] = Convert.ToDecimal(drData["TotalChequeReq"]).ToString("N2");
                        }

                        // PrintedDT --
                        if (drData["PrintedDT"] != null && drData["PrintedDT"].ToString() != "")
                        {
                            dr["PrintedDT"] = Convert.ToDateTime(drData["PrintedDT"].ToString());
                        }

                        // Total
                        if ((drData["Total"] != null) && (!drData["Total"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["Total"] = Convert.ToDecimal(drData["Total"]).ToString("N2");
                        }
                        //FolioNumber
                        if ((drData["FolioNumber"] != null) && (!drData["FolioNumber"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["FolioNumber"] = drData["FolioNumber"].ToString();
                        }

                        //03072019 Pongsakorn
                        string Barcode = "*" + dr["ChequeRequisitionNo"].ToString().ToUpper().ToUpper() + "*";

                        Barcode bcd = new Barcode();
                        bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                        bcd.IncludeLabel = false;
                        bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                        bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 240, 65);

                        MemoryStream ms = new MemoryStream();
                        bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                        Byte[] oByte = ms.ToArray();
                        dr["imgBarcode"] = oByte;

                        //

                        //dsReport.Tables["ChequeReq_ITF"].Rows.Add(dr);
                        dsReport.Tables[3].Rows.Add(dr);
                    }

                    int numberOfRow = 25 - dsReport.Tables["ChequeReq_ITF"].Rows.Count;

                    if (numberOfRow < 25)
                    {
                        for (int i = 0; i <= numberOfRow - 1; i++)
                        {
                            DataRow drEmpty = dsReport.Tables["ChequeReq_ITF"].NewRow();
                            DataRow drCurrent = dsReport.Tables["ChequeReq_ITF"].Rows[0];
                            // TotalChequeReq
                            if (drCurrent["TotalChequeReq"] != null && drCurrent["TotalChequeReq"].ToString() != "")
                            {
                                drEmpty["TotalChequeReq"] = Convert.ToDecimal(drCurrent["TotalChequeReq"]).ToString("N2");
                            }

                            // PrintedDT --
                            if (drCurrent["PrintedDT"] != null && drCurrent["PrintedDT"].ToString() != "")
                            {
                                drEmpty["PrintedDT"] = Convert.ToDateTime(drCurrent["PrintedDT"].ToString());
                            }

                            //dsReport.Tables["ChequeReq_ITF"].Rows.Add(drEmpty);
                            dsReport.Tables[3].Rows.Add(drEmpty);
                        }
                    }

                    orpt.SetDataSource(dsReport);
                    #endregion
                    //					}
                }
                else if (strFormid == "TNT_Normal_ChequeReq")
                {
                    #region TNT_Normal_ChequeReq
                    pageTitle = "TNT_Normal_ChequeReq";

                    TNT_Normal_ChequeReq objReport = new TNT_Normal_ChequeReq();
                    objReport.ResourceName = strReportTemplate;
                    orpt = objReport;

                    strFormatType = (String)Session["FormatType"];
                    DataSet dsChequeRep = (DataSet)Session["SESSION_DS_PRINTCHEQUEREQ"];
                    CustomDS dsReport = new CustomDS();

                    foreach (DataRow drData in dsChequeRep.Tables[0].Rows)
                    {
                        DataRow dr = dsReport.Tables[0].NewRow();

                        /** applicationid
							if(drData["applicationid"] != null && drData["applicationid"].ToString() != "")
							{
								dr["applicationid"] = drData["applicationid"].ToString();
							}
							**/

                        /** enterpriseid
							if(drData["enterpriseid"] != null && drData["enterpriseid"].ToString() != "")
							{
								dr["enterpriseid"] = drData["enterpriseid"].ToString();
							}
							**/

                        // EnterpriseName
                        if (drData["EnterpriseName"] != null && drData["EnterpriseName"].ToString() != "")
                        {
                            dr["EnterpriseName"] = drData["EnterpriseName"].ToString();
                        }

                        // EnterpriseAddress1
                        if (drData["EnterpriseAddress1"] != null && drData["EnterpriseAddress1"].ToString() != "")
                        {
                            dr["EnterpriseAddress1"] = drData["EnterpriseAddress1"].ToString();
                        }

                        // EnterpriseAddress2
                        if (drData["EnterpriseAddress2"] != null && drData["EnterpriseAddress2"].ToString() != "")
                        {
                            dr["EnterpriseAddress2"] = drData["EnterpriseAddress2"].ToString();
                        }

                        // ChequeRequisitionNo
                        if (drData["ChequeRequisitionNo"] != null && drData["ChequeRequisitionNo"].ToString() != "")
                        {
                            dr["ChequeRequisitionNo"] = drData["ChequeRequisitionNo"].ToString();
                        }

                        // ChequeRequisitionStatus
                        if (drData["ChequeRequisitionStatus"] != null && drData["ChequeRequisitionStatus"].ToString() != "")
                        {
                            dr["ChequeRequisitionStatus"] = drData["ChequeRequisitionStatus"].ToString();
                        }

                        // Payee
                        if (drData["Payee"] != null && drData["Payee"].ToString() != "")
                        {
                            dr["Payee"] = drData["Payee"].ToString();
                        }

                        // ChequeNo
                        if (drData["ChequeNo"] != null && drData["ChequeNo"].ToString() != "")
                        {
                            dr["ChequeNo"] = drData["ChequeNo"].ToString();
                        }

                        // ChequeIssueDate --
                        if (drData["ChequeIssueDate"] != null && drData["ChequeIssueDate"].ToString() != "")
                        {
                            dr["ChequeIssueDate"] = Convert.ToDateTime(drData["ChequeIssueDate"].ToString());
                        }

                        // ReceiptNo
                        if (drData["ReceiptNo"] != null && drData["ReceiptNo"].ToString() != "")
                        {
                            dr["ReceiptNo"] = drData["ReceiptNo"].ToString();
                        }

                        // ReceiptDate --
                        if (drData["ReceiptDate"] != null && drData["ReceiptDate"].ToString() != "")
                        {
                            dr["ReceiptDate"] = Convert.ToDateTime(drData["ReceiptDate"].ToString());
                        }

                        // PreparedBy
                        if (drData["PreparedBy"] != null && drData["PreparedBy"].ToString() != "")
                        {
                            dr["PreparedBy"] = drData["PreparedBy"].ToString();
                        }

                        // PreparedDT --
                        if (drData["PreparedDT"] != null && drData["PreparedDT"].ToString() != "")
                        {
                            dr["PreparedDT"] = Convert.ToDateTime(drData["PreparedDT"].ToString());
                        }

                        // ApprovedBy
                        if (drData["ApprovedBy"] != null && drData["ApprovedBy"].ToString() != "")
                        {
                            dr["ApprovedBy"] = drData["ApprovedBy"].ToString();
                        }

                        // ApprovedDT ---
                        if (drData["ApprovedDT"] != null && drData["ApprovedDT"].ToString() != "")
                        {
                            dr["ApprovedDT"] = Convert.ToDateTime(drData["ApprovedDT"].ToString());
                        }

                        // CompletedBy
                        if (drData["CompletedBy"] != null && drData["CompletedBy"].ToString() != "")
                        {
                            dr["CompletedBy"] = drData["CompletedBy"].ToString();
                        }

                        // CompletedDT --
                        if (drData["CompletedDT"] != null && drData["CompletedDT"].ToString() != "")
                        {
                            dr["CompletedDT"] = Convert.ToDateTime(drData["CompletedDT"].ToString());
                        }

                        // TotalChequeReq
                        if (drData["TotalChequeReq"] != null && drData["TotalChequeReq"].ToString() != "")
                        {
                            dr["TotalChequeReq"] = Convert.ToDecimal(drData["TotalChequeReq"]).ToString("N2");
                        }

                        // PrintedDT --
                        if (drData["PrintedDT"] != null && drData["PrintedDT"].ToString() != "")
                        {
                            dr["PrintedDT"] = Convert.ToDateTime(drData["PrintedDT"].ToString());
                        }

                        // InvoiceNo
                        if ((drData["InvoiceNo"] != null) && (!drData["InvoiceNo"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["InvoiceNo"] = drData["InvoiceNo"].ToString();
                        }

                        // Client
                        if ((drData["Client"] != null) && (!drData["Client"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["Client"] = drData["Client"].ToString();
                        }

                        // DutyTax
                        if ((drData["DutyTax"] != null) && (!drData["DutyTax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["DutyTax"] = Convert.ToDecimal(drData["DutyTax"]).ToString("N2");
                        }

                        // EPF
                        if ((drData["EPF"] != null) && (!drData["EPF"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["EPF"] = Convert.ToDecimal(drData["EPF"]).ToString("N2");
                        }

                        // Total
                        if ((drData["Total"] != null) && (!drData["Total"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["Total"] = Convert.ToDecimal(drData["Total"]).ToString("N2");
                        }

                        //03072019 Pongsakorn
                        string Barcode = "*" + dr["ChequeRequisitionNo"].ToString().ToUpper().ToUpper() + "*";

                        Barcode bcd = new Barcode();
                        bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                        bcd.IncludeLabel = false;
                        bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                        bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 240, 65);

                        MemoryStream ms = new MemoryStream();
                        bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                        Byte[] oByte = ms.ToArray();
                        dr["imgBarcode"] = oByte;

                        //

                        dsReport.Tables[0].Rows.Add(dr);
                    }

                    int numberOfRow = 25 - dsReport.Tables[0].Rows.Count;

                    if (numberOfRow < 25)
                    {
                        for (int i = 0; i <= numberOfRow - 1; i++)
                        {
                            DataRow drEmpty = dsReport.Tables[0].NewRow();
                            DataRow drCurrent = dsReport.Tables[0].Rows[0];
                            // TotalChequeReq
                            if (drCurrent["TotalChequeReq"] != null && drCurrent["TotalChequeReq"].ToString() != "")
                            {
                                drEmpty["TotalChequeReq"] = Convert.ToDecimal(drCurrent["TotalChequeReq"]).ToString("N2");
                            }

                            // PrintedDT --
                            if (drCurrent["PrintedDT"] != null && drCurrent["PrintedDT"].ToString() != "")
                            {
                                drEmpty["PrintedDT"] = Convert.ToDateTime(drCurrent["PrintedDT"].ToString());
                            }

                            dsReport.Tables[0].Rows.Add(drEmpty);
                        }
                    }

                    orpt.SetDataSource(dsReport);
                    #endregion TNT_Normal_ChequeReq
                }
                else if (strFormid == "TNT_Normal_ChequeReq_Adhoc")
                {
                    #region TNT_Adhoc_ChequeRep
                    pageTitle = "TNT_Normal_ChequeReq";

                    TNT_Adhoc_ChequeReq objReport = new TNT_Adhoc_ChequeReq();
                    objReport.ResourceName = strReportTemplate;
                    orpt = objReport;

                    strFormatType = (String)Session["FormatType"];
                    DataSet dsChequeRepAdhoc = (DataSet)Session["SESSION_DS_PRINTCHEQUEREQ_ADHOC"];

                    CustomDS dsReport = new CustomDS();

                    foreach (DataRow drData in dsChequeRepAdhoc.Tables[0].Rows)
                    {
                        DataRow dr = dsReport.Tables["ChequeReq_Adhoc"].NewRow();

                        /** applicationid
							if(drData["applicationid"] != null && drData["applicationid"].ToString() != "")
							{
								dr["applicationid"] = drData["applicationid"].ToString();
							}
							**/

                        /** enterpriseid
							if(drData["enterpriseid"] != null && drData["enterpriseid"].ToString() != "")
							{
								dr["enterpriseid"] = drData["enterpriseid"].ToString();
							}
							**/

                        // EnterpriseName
                        if (drData["EnterpriseName"] != null && drData["EnterpriseName"].ToString() != "")
                        {
                            dr["EnterpriseName"] = drData["EnterpriseName"].ToString();
                        }

                        // EnterpriseAddress1
                        if (drData["EnterpriseAddress1"] != null && drData["EnterpriseAddress1"].ToString() != "")
                        {
                            dr["EnterpriseAddress1"] = drData["EnterpriseAddress1"].ToString();
                        }

                        // EnterpriseAddress2
                        if (drData["EnterpriseAddress2"] != null && drData["EnterpriseAddress2"].ToString() != "")
                        {
                            dr["EnterpriseAddress2"] = drData["EnterpriseAddress2"].ToString();
                        }

                        // ChequeRequisitionNo
                        if (drData["ChequeRequisitionNo"] != null && drData["ChequeRequisitionNo"].ToString() != "")
                        {
                            dr["ChequeRequisitionNo"] = drData["ChequeRequisitionNo"].ToString();
                        }

                        // ChequeRequisitionStatus
                        if (drData["ChequeRequisitionStatus"] != null && drData["ChequeRequisitionStatus"].ToString() != "")
                        {
                            dr["ChequeRequisitionStatus"] = drData["ChequeRequisitionStatus"].ToString();
                        }

                        // Payee
                        if (drData["Payee"] != null && drData["Payee"].ToString() != "")
                        {
                            dr["Payee"] = drData["Payee"].ToString();
                        }

                        // ChequeNo
                        if (drData["ChequeNo"] != null && drData["ChequeNo"].ToString() != "")
                        {
                            dr["ChequeNo"] = drData["ChequeNo"].ToString();
                        }

                        // ChequeIssueDate --
                        if (drData["ChequeIssueDate"] != null && drData["ChequeIssueDate"].ToString() != "")
                        {
                            dr["ChequeIssueDate"] = Convert.ToDateTime(drData["ChequeIssueDate"].ToString());
                        }

                        // ReceiptNo
                        if (drData["ReceiptNo"] != null && drData["ReceiptNo"].ToString() != "")
                        {
                            dr["ReceiptNo"] = drData["ReceiptNo"].ToString();
                        }

                        // ReceiptDate --
                        if (drData["ReceiptDate"] != null && drData["ReceiptDate"].ToString() != "")
                        {
                            dr["ReceiptDate"] = Convert.ToDateTime(drData["ReceiptDate"].ToString());
                        }

                        // PreparedBy
                        if (drData["PreparedBy"] != null && drData["PreparedBy"].ToString() != "")
                        {
                            dr["PreparedBy"] = drData["PreparedBy"].ToString();
                        }

                        // PreparedDT --
                        if (drData["PreparedDT"] != null && drData["PreparedDT"].ToString() != "")
                        {
                            dr["PreparedDT"] = Convert.ToDateTime(drData["PreparedDT"].ToString());
                        }

                        // ApprovedBy
                        if (drData["ApprovedBy"] != null && drData["ApprovedBy"].ToString() != "")
                        {
                            dr["ApprovedBy"] = drData["ApprovedBy"].ToString();
                        }

                        // ApprovedDT ---
                        if (drData["ApprovedDT"] != null && drData["ApprovedDT"].ToString() != "")
                        {
                            dr["ApprovedDT"] = Convert.ToDateTime(drData["ApprovedDT"].ToString());
                        }

                        // CompletedBy
                        if (drData["CompletedBy"] != null && drData["CompletedBy"].ToString() != "")
                        {
                            dr["CompletedBy"] = drData["CompletedBy"].ToString();
                        }

                        // CompletedDT --
                        if (drData["CompletedDT"] != null && drData["CompletedDT"].ToString() != "")
                        {
                            dr["CompletedDT"] = Convert.ToDateTime(drData["CompletedDT"].ToString());
                        }

                        // TotalChequeReq
                        if (drData["TotalChequeReq"] != null && drData["TotalChequeReq"].ToString() != "")
                        {
                            dr["TotalChequeReq"] = drData["TotalChequeReq"].ToString();
                        }

                        // PrintedDT --
                        if (drData["PrintedDT"] != null && drData["PrintedDT"].ToString() != "")
                        {
                            dr["PrintedDT"] = Convert.ToDateTime(drData["PrintedDT"].ToString());
                        }


                        // InvoiceNo
                        if ((drData["InvoiceNo"] != null) && (!drData["InvoiceNo"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["InvoiceNo"] = drData["InvoiceNo"].ToString();
                        }

                        // Client
                        if ((drData["Client"] != null) && (!drData["Client"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["Client"] = drData["Client"].ToString();
                        }

                        // DisbursementType
                        if ((drData["DisbursementType"] != null) && (!drData["DisbursementType"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["DisbursementType"] = drData["DisbursementType"].ToString();
                        }

                        // Amount
                        if ((drData["Amount"] != null) && (!drData["Amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["Amount"] = Convert.ToDecimal(drData["Amount"]).ToString("N2");
                        }

                        //03072019 Pongsakorn
                        string Barcode = "*" + dr["ChequeRequisitionNo"].ToString().ToUpper().ToUpper() + "*";

                        Barcode bcd = new Barcode();
                        bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                        bcd.IncludeLabel = false;
                        bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                        bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 240, 65);

                        MemoryStream ms = new MemoryStream();
                        bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                        Byte[] oByte = ms.ToArray();
                        dr["imgBarcode"] = oByte;

                        //

                        dsReport.Tables["ChequeReq_Adhoc"].Rows.Add(dr);
                    }

                    int numberOfRow = 25 - dsReport.Tables["ChequeReq_Adhoc"].Rows.Count;

                    if (numberOfRow < 25)
                    {
                        for (int i = 0; i <= numberOfRow - 1; i++)
                        {
                            DataRow drEmpty = dsReport.Tables["ChequeReq_Adhoc"].NewRow();
                            DataRow drCurrent = dsReport.Tables["ChequeReq_Adhoc"].Rows[0];
                            // TotalChequeReq
                            if (drCurrent["TotalChequeReq"] != null && drCurrent["TotalChequeReq"].ToString() != "")
                            {
                                drEmpty["TotalChequeReq"] = Convert.ToDecimal(drCurrent["TotalChequeReq"]).ToString("N2");
                            }

                            // PrintedDT --
                            if (drCurrent["PrintedDT"] != null && drCurrent["PrintedDT"].ToString() != "")
                            {
                                drEmpty["PrintedDT"] = Convert.ToDateTime(drCurrent["PrintedDT"].ToString());
                            }

                            dsReport.Tables["ChequeReq_Adhoc"].Rows.Add(drEmpty);
                        }
                    }

                    orpt.SetDataSource(dsReport);
                    #endregion TNT_Adhoc_ChequeRep
                }
                else if (strFormid == "Customs_Invoice")
                {
                    #region Customs_Invoice

                    pageTitle = "PNGAFCustomsInvoiceTemplate";
                    PNGAFCustomsInvoiceTemplate objReport = new PNGAFCustomsInvoiceTemplate();
                    objReport.ResourceName = strReportTemplate;
                    orpt = objReport;

                    strFormatType = (String)Session["FormatType"];
                    DataSet dsCustomsInvoice = (DataSet)Session["SESSION_DS_CUSTOMS_INVOICE"];

                    Customs_Invioce dsReport = new Customs_Invioce();

                    foreach (DataRow drData in dsCustomsInvoice.Tables[1].Rows)
                    {
                        DataRow dr = dsReport.Tables["Invoice"].NewRow();

                        // Customer_Name
                        if (drData["Customer_Name"] != null && drData["Customer_Name"].ToString() != "")
                        {
                            dr["Customer_Name"] = drData["Customer_Name"].ToString();
                        }

                        // Customer_Address1
                        if (drData["Customer_Address1"] != null && drData["Customer_Address1"].ToString() != "")
                        {
                            dr["Customer_Address1"] = drData["Customer_Address1"].ToString();
                        }

                        // Customer_Address2
                        if (drData["Customer_Address2"] != null && drData["Customer_Address2"].ToString() != "")
                        {
                            dr["Customer_Address2"] = drData["Customer_Address2"].ToString();
                        }

                        // Customer_Address3
                        if (drData["Customer_Address3"] != null && drData["Customer_Address3"].ToString() != "")
                        {
                            dr["Customer_Address3"] = drData["Customer_Address3"].ToString();
                        }

                        // Customer_Address4
                        if (drData["Customer_Address4"] != null && drData["Customer_Address4"].ToString() != "")
                        {
                            dr["Customer_Address4"] = drData["Customer_Address4"].ToString();
                        }

                        // Customer_Telephone
                        if (drData["Customer_Telephone"] != null && drData["Customer_Telephone"].ToString() != "")
                        {
                            dr["Customer_Telephone"] = drData["Customer_Telephone"].ToString();
                        }

                        // Customer_Fax
                        if (drData["Customer_Fax"] != null && drData["Customer_Fax"].ToString() != "")
                        {
                            dr["Customer_Fax"] = drData["Customer_Fax"].ToString();
                        }

                        // Delivery_Name
                        if (drData["Delivery_Name"] != null && drData["Delivery_Name"].ToString() != "")
                        {
                            dr["Delivery_Name"] = drData["Delivery_Name"].ToString();
                        }

                        // Delivery_Address1
                        if (drData["Delivery_Address1"] != null && drData["Delivery_Address1"].ToString() != "")
                        {
                            dr["Delivery_Address1"] = drData["Delivery_Address1"].ToString();
                        }

                        // Delivery_Address2
                        if (drData["Delivery_Address2"] != null && drData["Delivery_Address2"].ToString() != "")
                        {
                            dr["Delivery_Address2"] = drData["Delivery_Address2"].ToString();
                        }

                        // Delivery_Address3
                        if (drData["Delivery_Address3"] != null && drData["Delivery_Address3"].ToString() != "")
                        {
                            dr["Delivery_Address3"] = drData["Delivery_Address3"].ToString();
                        }

                        // Delivery_Address4
                        if (drData["Delivery_Address4"] != null && drData["Delivery_Address4"].ToString() != "")
                        {
                            dr["Delivery_Address4"] = drData["Delivery_Address4"].ToString();
                        }

                        // Delivery_Telephone
                        if (drData["Delivery_Telephone"] != null && drData["Delivery_Telephone"].ToString() != "")
                        {
                            dr["Delivery_Telephone"] = drData["Delivery_Telephone"].ToString();
                        }

                        // Delivery_Fax
                        if (drData["Delivery_Fax"] != null && drData["Delivery_Fax"].ToString() != "")
                        {
                            dr["Delivery_Fax"] = drData["Delivery_Fax"].ToString();
                        }

                        // Aircraft
                        if (drData["Aircraft"] != null && drData["Aircraft"].ToString() != "")
                        {
                            dr["Aircraft"] = drData["Aircraft"].ToString();
                        }

                        // Folio
                        if (drData["Folio"] != null && drData["Folio"].ToString() != "")
                        {
                            dr["Folio"] = drData["Folio"].ToString();
                        }

                        // LoadingPortCity
                        if (drData["LoadingPortCity"] != null && drData["LoadingPortCity"].ToString() != "")
                        {
                            dr["LoadingPortCity"] = drData["LoadingPortCity"].ToString();
                        }

                        // ArrivalDate --
                        if (drData["ArrivalDate"] != null && drData["ArrivalDate"].ToString() != "")
                        {
                            dr["ArrivalDate"] = drData["ArrivalDate"].ToString();
                        }

                        // WayBillNo
                        if (drData["WayBillNo"] != null && drData["WayBillNo"].ToString() != "")
                        {
                            dr["WayBillNo"] = drData["WayBillNo"].ToString();
                        }

                        // Packages
                        if (drData["Packages"] != null && drData["Packages"].ToString() != "")
                        {
                            dr["Packages"] = drData["Packages"].ToString();
                        }

                        // Description_Line1
                        if (drData["Description_Line1"] != null && drData["Description_Line1"].ToString() != "")
                        {
                            dr["Description_Line1"] = drData["Description_Line1"].ToString();
                        }

                        // Description_Line2
                        if (drData["Description_Line2"] != null && drData["Description_Line2"].ToString() != "")
                        {
                            dr["Description_Line2"] = drData["Description_Line2"].ToString();
                        }

                        // Description_Line3
                        if (drData["Description_Line3"] != null && drData["Description_Line3"].ToString() != "")
                        {
                            dr["Description_Line3"] = drData["Description_Line3"].ToString();
                        }

                        // Description_Line4
                        if (drData["Description_Line4"] != null && drData["Description_Line4"].ToString() != "")
                        {
                            dr["Description_Line4"] = drData["Description_Line4"].ToString();
                        }

                        // Description_Line5
                        if (drData["Description_Line5"] != null && drData["Description_Line5"].ToString() != "")
                        {
                            dr["Description_Line5"] = drData["Description_Line5"].ToString();
                        }

                        // Description_Line6
                        if (drData["Description_Line6"] != null && drData["Description_Line6"].ToString() != "")
                        {
                            dr["Description_Line6"] = drData["Description_Line6"].ToString();
                        }

                        // Weight
                        if (drData["Weight"] != null && drData["Weight"].ToString() != "")
                        {
                            dr["Weight"] = drData["Weight"].ToString();
                        }

                        // Invoice_Date --
                        if (drData["Invoice_Date"] != null && drData["Invoice_Date"].ToString() != "")
                        {
                            dr["Invoice_Date"] = drData["Invoice_Date"].ToString();
                        }

                        // CustomerAccountNo
                        if (drData["CustomerAccountNo"] != null && drData["CustomerAccountNo"].ToString() != "")
                        {
                            dr["CustomerAccountNo"] = drData["CustomerAccountNo"].ToString();
                        }

                        // Invoice_Number
                        if (drData["Invoice_Number"] != null && drData["Invoice_Number"].ToString() != "")
                        {
                            dr["Invoice_Number"] = drData["Invoice_Number"].ToString();
                        }

                        // TotalInvoiceAmount
                        if (drData["TotalInvoiceAmount"] != null && drData["TotalInvoiceAmount"].ToString() != "")
                        {
                            dr["TotalInovoiceAmount"] = Convert.ToDecimal(drData["TotalInvoiceAmount"]).ToString("N2").Split('.')[0];
                            dr["TotalInovoiceAmountDecimal"] = Convert.ToDecimal(drData["TotalInvoiceAmount"]).ToString("N2").Split('.')[1];
                        }

                        // POD_Date
                        if (drData["POD_Date"] != null && drData["POD_Date"].ToString() != "")
                        {
                            dr["POD_Date"] = drData["POD_Date"].ToString();
                        }

                        // POD_Consignee
                        if (drData["POD_Consignee"] != null && drData["POD_Consignee"].ToString() != "")
                        {
                            dr["POD_Consignee"] = drData["POD_Consignee"].ToString();
                        }

                        // Consignee_Signature
                        if ((drData["Consignee_Signature"] != null) && (!drData["Consignee_Signature"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["Consignee_Signature"] = Convert.FromBase64String((string)drData["Consignee_Signature"]);
                        }


                        for (int i = 1; i <= 14; i++)
                        {
                            string indexRow = i.ToString();
                            if (i >= 1 && i <= 9) { indexRow = "0" + i; }

                            // Fee01_Description
                            if (drData["Fee" + indexRow + "_Description"] != null && drData["Fee" + indexRow + "_Description"].ToString() != "")
                            {
                                dr["Fee" + indexRow + "_Description"] = drData["Fee" + indexRow + "_Description"].ToString();
                            }

                            // Fee01_GLCode
                            if (drData["Fee" + indexRow + "_GLCode"] != null && drData["Fee" + indexRow + "_GLCode"].ToString() != "")
                            {
                                dr["Fee" + indexRow + "_GLCode"] = drData["Fee" + indexRow + "_GLCode"].ToString();
                            }

                            // Fee01_Amount
                            if (drData["Fee" + indexRow + "_Amount"] != null && drData["Fee" + indexRow + "_Amount"].ToString() != "")
                            {
                                string[] Fee_Amount = Convert.ToDecimal(drData["Fee" + indexRow + "_Amount"]).ToString("N2").Split('.');
                                dr["Fee" + indexRow + "_Amount"] = Fee_Amount[0];
                                dr["Fee" + indexRow + "_Amount_Decimal"] = Fee_Amount[1];
                            }
                        }

                        for (int i = 1; i <= 9; i++)
                        {

                            // Disb_Description
                            if (drData["Disb" + i + "_Description"] != null && drData["Disb" + i + "_Description"].ToString() != "")
                            {
                                dr["Disb" + i + "_Description"] = drData["Disb" + i + "_Description"].ToString();
                            }

                            // Disb_GLCode
                            if (drData["Disb" + i + "_GLCode"] != null && drData["Disb" + i + "_GLCode"].ToString() != "")
                            {
                                dr["Disb" + i + "_GLCode"] = drData["Disb" + i + "_GLCode"].ToString();
                            }

                            // Disb_Amount
                            if (drData["Disb" + i + "_Amount"] != null && drData["Disb" + i + "_Amount"].ToString() != "")
                            {
                                dr["Disb" + i + "_Amount"] = Convert.ToDecimal(drData["Disb" + i + "_Amount"]).ToString("N2").Split('.')[0];
                                dr["Disb" + i + "_Amount_Decimal"] = Convert.ToDecimal(drData["Disb" + i + "_Amount"]).ToString("N2").Split('.')[1];

                            }
                        }

                        for (int i = 1; i <= 4; i++)
                        {
                            if (drData["Disb_Other" + i] != null && drData["Disb_Other" + i].ToString() != "")
                            {
                                dr["Disb_Other" + i] = drData["Disb_Other" + i].ToString();
                            }
                        }


                        string Barcode = "*" + drData["Invoice_Number"].ToString().ToUpper() + "*";

                        Barcode bcd = new Barcode();
                        bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                        bcd.IncludeLabel = false;
                        bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                        //bcd.LabelPosition = LabelPositions.BOTTOMCENTER;
                        //bcd.LabelFont = new Font("Tahoma", 14, System.Drawing.FontStyle.Regular);
                        bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 265, 65);

                        MemoryStream ms = new MemoryStream();
                        bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                        Byte[] oByte = ms.ToArray();
                        dr["imgBarcode"] = oByte;

                        dsReport.Tables["Invoice"].Rows.Add(dr);
                    }

                    orpt.SetDataSource(dsReport);
                    #endregion Customs_Invoice
                }
                else if (strFormid == "Customs_Invoice_InvoicedPrinting")
                {
                    #region Customs_Invoice

                    pageTitle = "PNGAFCustomsInvoiceTemplate";
                    PNGAFCustomsInvoiceTemplate objReport = new PNGAFCustomsInvoiceTemplate();
                    objReport.ResourceName = strReportTemplate;
                    orpt = objReport;

                    strFormatType = (String)Session["FormatType"];
                    DataSet dsCustomsInvoice = (DataSet)Session["SESSION_DS_CUSTOMS_INVOICE"];

                    Customs_Invioce dsReportOnePage = new Customs_Invioce();
                    #region CustomsInvoiceOnePage
                    foreach (DataRow drData in dsCustomsInvoice.Tables[1].Rows)
                    {
                        DataRow _dr = dsReportOnePage.Tables["Invoice"].NewRow();

                        // Customer_Name
                        if (drData["Customer_Name"] != null && drData["Customer_Name"].ToString() != "")
                        {
                            _dr["Customer_Name"] = drData["Customer_Name"].ToString();
                        }

                        // Customer_Address1
                        if (drData["Customer_Address1"] != null && drData["Customer_Address1"].ToString() != "")
                        {
                            _dr["Customer_Address1"] = drData["Customer_Address1"].ToString();
                        }

                        // Customer_Address2
                        if (drData["Customer_Address2"] != null && drData["Customer_Address2"].ToString() != "")
                        {
                            _dr["Customer_Address2"] = drData["Customer_Address2"].ToString();
                        }

                        // Customer_Address3
                        if (drData["Customer_Address3"] != null && drData["Customer_Address3"].ToString() != "")
                        {
                            _dr["Customer_Address3"] = drData["Customer_Address3"].ToString();
                        }

                        // Customer_Address4
                        if (drData["Customer_Address4"] != null && drData["Customer_Address4"].ToString() != "")
                        {
                            _dr["Customer_Address4"] = drData["Customer_Address4"].ToString();
                        }

                        // Customer_Telephone
                        if (drData["Customer_Telephone"] != null && drData["Customer_Telephone"].ToString() != "")
                        {
                            _dr["Customer_Telephone"] = drData["Customer_Telephone"].ToString();
                        }

                        // Customer_Fax
                        if (drData["Customer_Fax"] != null && drData["Customer_Fax"].ToString() != "")
                        {
                            _dr["Customer_Fax"] = drData["Customer_Fax"].ToString();
                        }

                        // Delivery_Name
                        if (drData["Delivery_Name"] != null && drData["Delivery_Name"].ToString() != "")
                        {
                            _dr["Delivery_Name"] = drData["Delivery_Name"].ToString();
                        }

                        // Delivery_Address1
                        if (drData["Delivery_Address1"] != null && drData["Delivery_Address1"].ToString() != "")
                        {
                            _dr["Delivery_Address1"] = drData["Delivery_Address1"].ToString();
                        }

                        // Delivery_Address2
                        if (drData["Delivery_Address2"] != null && drData["Delivery_Address2"].ToString() != "")
                        {
                            _dr["Delivery_Address2"] = drData["Delivery_Address2"].ToString();
                        }

                        // Delivery_Address3
                        if (drData["Delivery_Address3"] != null && drData["Delivery_Address3"].ToString() != "")
                        {
                            _dr["Delivery_Address3"] = drData["Delivery_Address3"].ToString();
                        }

                        // Delivery_Address4
                        if (drData["Delivery_Address4"] != null && drData["Delivery_Address4"].ToString() != "")
                        {
                            _dr["Delivery_Address4"] = drData["Delivery_Address4"].ToString();
                        }

                        // Delivery_Telephone
                        if (drData["Delivery_Telephone"] != null && drData["Delivery_Telephone"].ToString() != "")
                        {
                            _dr["Delivery_Telephone"] = drData["Delivery_Telephone"].ToString();
                        }

                        // Delivery_Fax
                        if (drData["Delivery_Fax"] != null && drData["Delivery_Fax"].ToString() != "")
                        {
                            _dr["Delivery_Fax"] = drData["Delivery_Fax"].ToString();
                        }

                        // Aircraft
                        if (drData["Aircraft"] != null && drData["Aircraft"].ToString() != "")
                        {
                            _dr["Aircraft"] = drData["Aircraft"].ToString();
                        }

                        // Folio
                        if (drData["Folio"] != null && drData["Folio"].ToString() != "")
                        {
                            _dr["Folio"] = drData["Folio"].ToString();
                        }

                        // LoadingPortCity
                        if (drData["LoadingPortCity"] != null && drData["LoadingPortCity"].ToString() != "")
                        {
                            _dr["LoadingPortCity"] = drData["LoadingPortCity"].ToString();
                        }

                        // ArrivalDate --
                        if (drData["ArrivalDate"] != null && drData["ArrivalDate"].ToString() != "")
                        {
                            _dr["ArrivalDate"] = drData["ArrivalDate"].ToString();
                        }

                        // WayBillNo
                        if (drData["WayBillNo"] != null && drData["WayBillNo"].ToString() != "")
                        {
                            _dr["WayBillNo"] = drData["WayBillNo"].ToString();
                        }

                        // Packages
                        if (drData["Packages"] != null && drData["Packages"].ToString() != "")
                        {
                            _dr["Packages"] = drData["Packages"].ToString();
                        }

                        // Description_Line1
                        if (drData["Description_Line1"] != null && drData["Description_Line1"].ToString() != "")
                        {
                            _dr["Description_Line1"] = drData["Description_Line1"].ToString();
                        }

                        // Description_Line2
                        if (drData["Description_Line2"] != null && drData["Description_Line2"].ToString() != "")
                        {
                            _dr["Description_Line2"] = drData["Description_Line2"].ToString();
                        }

                        // Description_Line3
                        if (drData["Description_Line3"] != null && drData["Description_Line3"].ToString() != "")
                        {
                            _dr["Description_Line3"] = drData["Description_Line3"].ToString();
                        }

                        // Description_Line4
                        if (drData["Description_Line4"] != null && drData["Description_Line4"].ToString() != "")
                        {
                            _dr["Description_Line4"] = drData["Description_Line4"].ToString();
                        }

                        // Description_Line5
                        if (drData["Description_Line5"] != null && drData["Description_Line5"].ToString() != "")
                        {
                            _dr["Description_Line5"] = drData["Description_Line5"].ToString();
                        }

                        // Description_Line6
                        if (drData["Description_Line6"] != null && drData["Description_Line6"].ToString() != "")
                        {
                            _dr["Description_Line6"] = drData["Description_Line6"].ToString();
                        }

                        // Weight
                        if (drData["Weight"] != null && drData["Weight"].ToString() != "")
                        {
                            _dr["Weight"] = drData["Weight"].ToString();
                        }

                        // Invoice_Date --
                        if (drData["Invoice_Date"] != null && drData["Invoice_Date"].ToString() != "")
                        {
                            _dr["Invoice_Date"] = drData["Invoice_Date"].ToString();
                        }

                        // CustomerAccountNo
                        if (drData["CustomerAccountNo"] != null && drData["CustomerAccountNo"].ToString() != "")
                        {
                            _dr["CustomerAccountNo"] = drData["CustomerAccountNo"].ToString();
                        }

                        // Invoice_Number
                        if (drData["Invoice_Number"] != null && drData["Invoice_Number"].ToString() != "")
                        {
                            _dr["Invoice_Number"] = drData["Invoice_Number"].ToString();
                        }

                        // TotalInvoiceAmount
                        if (drData["TotalInvoiceAmount"] != null && drData["TotalInvoiceAmount"].ToString() != "")
                        {
                            _dr["TotalInovoiceAmount"] = Convert.ToDecimal(drData["TotalInvoiceAmount"]).ToString("N2").Split('.')[0];
                            _dr["TotalInovoiceAmountDecimal"] = Convert.ToDecimal(drData["TotalInvoiceAmount"]).ToString("N2").Split('.')[1];
                        }

                        // POD_Date
                        if (drData["POD_Date"] != null && drData["POD_Date"].ToString() != "")
                        {
                            _dr["POD_Date"] = drData["POD_Date"].ToString();
                        }

                        // POD_Consignee
                        if (drData["POD_Consignee"] != null && drData["POD_Consignee"].ToString() != "")
                        {
                            _dr["POD_Consignee"] = drData["POD_Consignee"].ToString();
                        }

                        // Consignee_Signature
                        if ((drData["Consignee_Signature"] != null) && (!drData["Consignee_Signature"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            _dr["Consignee_Signature"] = Convert.FromBase64String((string)drData["Consignee_Signature"]);
                        }


                        for (int i = 1; i <= 14; i++)
                        {
                            string indexRow = i.ToString();
                            if (i >= 1 && i <= 9) { indexRow = "0" + i; }

                            // Fee01_Description
                            if (drData["Fee" + indexRow + "_Description"] != null && drData["Fee" + indexRow + "_Description"].ToString() != "")
                            {
                                _dr["Fee" + indexRow + "_Description"] = drData["Fee" + indexRow + "_Description"].ToString();
                            }

                            // Fee01_GLCode
                            if (drData["Fee" + indexRow + "_GLCode"] != null && drData["Fee" + indexRow + "_GLCode"].ToString() != "")
                            {
                                _dr["Fee" + indexRow + "_GLCode"] = drData["Fee" + indexRow + "_GLCode"].ToString();
                            }

                            // Fee01_Amount
                            if (drData["Fee" + indexRow + "_Amount"] != null && drData["Fee" + indexRow + "_Amount"].ToString() != "")
                            {
                                string[] Fee_Amount = Convert.ToDecimal(drData["Fee" + indexRow + "_Amount"]).ToString("N2").Split('.');
                                _dr["Fee" + indexRow + "_Amount"] = Fee_Amount[0];
                                _dr["Fee" + indexRow + "_Amount_Decimal"] = Fee_Amount[1];
                            }
                        }

                        for (int i = 1; i <= 9; i++)
                        {

                            // Disb_Description
                            if (drData["Disb" + i + "_Description"] != null && drData["Disb" + i + "_Description"].ToString() != "")
                            {
                                _dr["Disb" + i + "_Description"] = drData["Disb" + i + "_Description"].ToString();
                            }

                            // Disb_GLCode
                            if (drData["Disb" + i + "_GLCode"] != null && drData["Disb" + i + "_GLCode"].ToString() != "")
                            {
                                _dr["Disb" + i + "_GLCode"] = drData["Disb" + i + "_GLCode"].ToString();
                            }

                            // Disb_Amount
                            if (drData["Disb" + i + "_Amount"] != null && drData["Disb" + i + "_Amount"].ToString() != "")
                            {
                                _dr["Disb" + i + "_Amount"] = Convert.ToDecimal(drData["Disb" + i + "_Amount"]).ToString("N2").Split('.')[0];
                                _dr["Disb" + i + "_Amount_Decimal"] = Convert.ToDecimal(drData["Disb" + i + "_Amount"]).ToString("N2").Split('.')[1];

                            }
                        }

                        for (int i = 1; i <= 4; i++)
                        {
                            if (drData["Disb_Other" + i] != null && drData["Disb_Other" + i].ToString() != "")
                            {
                                _dr["Disb_Other" + i] = drData["Disb_Other" + i].ToString();
                            }
                        }


                        string Barcode = "*" + drData["Invoice_Number"].ToString().ToUpper() + "*";

                        Barcode bcd = new Barcode();
                        bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                        bcd.IncludeLabel = false;
                        bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                        //bcd.LabelPosition = LabelPositions.BOTTOMCENTER;
                        //bcd.LabelFont = new Font("Tahoma", 14, System.Drawing.FontStyle.Regular);
                        bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 265, 65);

                        MemoryStream ms = new MemoryStream();
                        bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                        Byte[] oByte = ms.ToArray();
                        _dr["imgBarcode"] = oByte;

                        dsReportOnePage.Tables["Invoice"].Rows.Add(_dr);
                        orpt.SetDataSource(dsReportOnePage);
                        BindReportOnePage((string)drData["Invoice_Number"].ToString().ToUpper());
                        dsReportOnePage.Tables["Invoice"].Clear();
                    }
                    #endregion CustomsInvoiceOnePage

                    Customs_Invioce dsReport = new Customs_Invioce();
                    #region CustomsInvoice
                    foreach (DataRow drData in dsCustomsInvoice.Tables[1].Rows)
                    {
                        DataRow dr = dsReport.Tables["Invoice"].NewRow();

                        // Customer_Name
                        if (drData["Customer_Name"] != null && drData["Customer_Name"].ToString() != "")
                        {
                            dr["Customer_Name"] = drData["Customer_Name"].ToString();
                        }

                        // Customer_Address1
                        if (drData["Customer_Address1"] != null && drData["Customer_Address1"].ToString() != "")
                        {
                            dr["Customer_Address1"] = drData["Customer_Address1"].ToString();
                        }

                        // Customer_Address2
                        if (drData["Customer_Address2"] != null && drData["Customer_Address2"].ToString() != "")
                        {
                            dr["Customer_Address2"] = drData["Customer_Address2"].ToString();
                        }

                        // Customer_Address3
                        if (drData["Customer_Address3"] != null && drData["Customer_Address3"].ToString() != "")
                        {
                            dr["Customer_Address3"] = drData["Customer_Address3"].ToString();
                        }

                        // Customer_Address4
                        if (drData["Customer_Address4"] != null && drData["Customer_Address4"].ToString() != "")
                        {
                            dr["Customer_Address4"] = drData["Customer_Address4"].ToString();
                        }

                        // Customer_Telephone
                        if (drData["Customer_Telephone"] != null && drData["Customer_Telephone"].ToString() != "")
                        {
                            dr["Customer_Telephone"] = drData["Customer_Telephone"].ToString();
                        }

                        // Customer_Fax
                        if (drData["Customer_Fax"] != null && drData["Customer_Fax"].ToString() != "")
                        {
                            dr["Customer_Fax"] = drData["Customer_Fax"].ToString();
                        }

                        // Delivery_Name
                        if (drData["Delivery_Name"] != null && drData["Delivery_Name"].ToString() != "")
                        {
                            dr["Delivery_Name"] = drData["Delivery_Name"].ToString();
                        }

                        // Delivery_Address1
                        if (drData["Delivery_Address1"] != null && drData["Delivery_Address1"].ToString() != "")
                        {
                            dr["Delivery_Address1"] = drData["Delivery_Address1"].ToString();
                        }

                        // Delivery_Address2
                        if (drData["Delivery_Address2"] != null && drData["Delivery_Address2"].ToString() != "")
                        {
                            dr["Delivery_Address2"] = drData["Delivery_Address2"].ToString();
                        }

                        // Delivery_Address3
                        if (drData["Delivery_Address3"] != null && drData["Delivery_Address3"].ToString() != "")
                        {
                            dr["Delivery_Address3"] = drData["Delivery_Address3"].ToString();
                        }

                        // Delivery_Address4
                        if (drData["Delivery_Address4"] != null && drData["Delivery_Address4"].ToString() != "")
                        {
                            dr["Delivery_Address4"] = drData["Delivery_Address4"].ToString();
                        }

                        // Delivery_Telephone
                        if (drData["Delivery_Telephone"] != null && drData["Delivery_Telephone"].ToString() != "")
                        {
                            dr["Delivery_Telephone"] = drData["Delivery_Telephone"].ToString();
                        }

                        // Delivery_Fax
                        if (drData["Delivery_Fax"] != null && drData["Delivery_Fax"].ToString() != "")
                        {
                            dr["Delivery_Fax"] = drData["Delivery_Fax"].ToString();
                        }

                        // Aircraft
                        if (drData["Aircraft"] != null && drData["Aircraft"].ToString() != "")
                        {
                            dr["Aircraft"] = drData["Aircraft"].ToString();
                        }

                        // Folio
                        if (drData["Folio"] != null && drData["Folio"].ToString() != "")
                        {
                            dr["Folio"] = drData["Folio"].ToString();
                        }

                        // LoadingPortCity
                        if (drData["LoadingPortCity"] != null && drData["LoadingPortCity"].ToString() != "")
                        {
                            dr["LoadingPortCity"] = drData["LoadingPortCity"].ToString();
                        }

                        // ArrivalDate --
                        if (drData["ArrivalDate"] != null && drData["ArrivalDate"].ToString() != "")
                        {
                            dr["ArrivalDate"] = drData["ArrivalDate"].ToString();
                        }

                        // WayBillNo
                        if (drData["WayBillNo"] != null && drData["WayBillNo"].ToString() != "")
                        {
                            dr["WayBillNo"] = drData["WayBillNo"].ToString();
                        }

                        // Packages
                        if (drData["Packages"] != null && drData["Packages"].ToString() != "")
                        {
                            dr["Packages"] = drData["Packages"].ToString();
                        }

                        // Description_Line1
                        if (drData["Description_Line1"] != null && drData["Description_Line1"].ToString() != "")
                        {
                            dr["Description_Line1"] = drData["Description_Line1"].ToString();
                        }

                        // Description_Line2
                        if (drData["Description_Line2"] != null && drData["Description_Line2"].ToString() != "")
                        {
                            dr["Description_Line2"] = drData["Description_Line2"].ToString();
                        }

                        // Description_Line3
                        if (drData["Description_Line3"] != null && drData["Description_Line3"].ToString() != "")
                        {
                            dr["Description_Line3"] = drData["Description_Line3"].ToString();
                        }

                        // Description_Line4
                        if (drData["Description_Line4"] != null && drData["Description_Line4"].ToString() != "")
                        {
                            dr["Description_Line4"] = drData["Description_Line4"].ToString();
                        }

                        // Description_Line5
                        if (drData["Description_Line5"] != null && drData["Description_Line5"].ToString() != "")
                        {
                            dr["Description_Line5"] = drData["Description_Line5"].ToString();
                        }

                        // Description_Line6
                        if (drData["Description_Line6"] != null && drData["Description_Line6"].ToString() != "")
                        {
                            dr["Description_Line6"] = drData["Description_Line6"].ToString();
                        }

                        // Weight
                        if (drData["Weight"] != null && drData["Weight"].ToString() != "")
                        {
                            dr["Weight"] = drData["Weight"].ToString();
                        }

                        // Invoice_Date --
                        if (drData["Invoice_Date"] != null && drData["Invoice_Date"].ToString() != "")
                        {
                            dr["Invoice_Date"] = drData["Invoice_Date"].ToString();
                        }

                        // CustomerAccountNo
                        if (drData["CustomerAccountNo"] != null && drData["CustomerAccountNo"].ToString() != "")
                        {
                            dr["CustomerAccountNo"] = drData["CustomerAccountNo"].ToString();
                        }

                        // Invoice_Number
                        if (drData["Invoice_Number"] != null && drData["Invoice_Number"].ToString() != "")
                        {
                            dr["Invoice_Number"] = drData["Invoice_Number"].ToString();
                        }

                        // TotalInvoiceAmount
                        if (drData["TotalInvoiceAmount"] != null && drData["TotalInvoiceAmount"].ToString() != "")
                        {
                            dr["TotalInovoiceAmount"] = Convert.ToDecimal(drData["TotalInvoiceAmount"]).ToString("N2").Split('.')[0];
                            dr["TotalInovoiceAmountDecimal"] = Convert.ToDecimal(drData["TotalInvoiceAmount"]).ToString("N2").Split('.')[1];
                        }

                        // POD_Date
                        if (drData["POD_Date"] != null && drData["POD_Date"].ToString() != "")
                        {
                            dr["POD_Date"] = drData["POD_Date"].ToString();
                        }

                        // POD_Consignee
                        if (drData["POD_Consignee"] != null && drData["POD_Consignee"].ToString() != "")
                        {
                            dr["POD_Consignee"] = drData["POD_Consignee"].ToString();
                        }

                        // Consignee_Signature
                        if ((drData["Consignee_Signature"] != null) && (!drData["Consignee_Signature"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            dr["Consignee_Signature"] = Convert.FromBase64String((string)drData["Consignee_Signature"]);
                        }


                        for (int i = 1; i <= 14; i++)
                        {
                            string indexRow = i.ToString();
                            if (i >= 1 && i <= 9) { indexRow = "0" + i; }

                            // Fee01_Description
                            if (drData["Fee" + indexRow + "_Description"] != null && drData["Fee" + indexRow + "_Description"].ToString() != "")
                            {
                                dr["Fee" + indexRow + "_Description"] = drData["Fee" + indexRow + "_Description"].ToString();
                            }

                            // Fee01_GLCode
                            if (drData["Fee" + indexRow + "_GLCode"] != null && drData["Fee" + indexRow + "_GLCode"].ToString() != "")
                            {
                                dr["Fee" + indexRow + "_GLCode"] = drData["Fee" + indexRow + "_GLCode"].ToString();
                            }

                            // Fee01_Amount
                            if (drData["Fee" + indexRow + "_Amount"] != null && drData["Fee" + indexRow + "_Amount"].ToString() != "")
                            {
                                string[] Fee_Amount = Convert.ToDecimal(drData["Fee" + indexRow + "_Amount"]).ToString("N2").Split('.');
                                dr["Fee" + indexRow + "_Amount"] = Fee_Amount[0];
                                dr["Fee" + indexRow + "_Amount_Decimal"] = Fee_Amount[1];
                            }
                        }

                        for (int i = 1; i <= 9; i++)
                        {

                            // Disb_Description
                            if (drData["Disb" + i + "_Description"] != null && drData["Disb" + i + "_Description"].ToString() != "")
                            {
                                dr["Disb" + i + "_Description"] = drData["Disb" + i + "_Description"].ToString();
                            }

                            // Disb_GLCode
                            if (drData["Disb" + i + "_GLCode"] != null && drData["Disb" + i + "_GLCode"].ToString() != "")
                            {
                                dr["Disb" + i + "_GLCode"] = drData["Disb" + i + "_GLCode"].ToString();
                            }

                            // Disb_Amount
                            if (drData["Disb" + i + "_Amount"] != null && drData["Disb" + i + "_Amount"].ToString() != "")
                            {
                                dr["Disb" + i + "_Amount"] = Convert.ToDecimal(drData["Disb" + i + "_Amount"]).ToString("N2").Split('.')[0];
                                dr["Disb" + i + "_Amount_Decimal"] = Convert.ToDecimal(drData["Disb" + i + "_Amount"]).ToString("N2").Split('.')[1];

                            }
                        }

                        for (int i = 1; i <= 4; i++)
                        {
                            if (drData["Disb_Other" + i] != null && drData["Disb_Other" + i].ToString() != "")
                            {
                                dr["Disb_Other" + i] = drData["Disb_Other" + i].ToString();
                            }
                        }

                        
                        string Barcode = "*" + drData["Invoice_Number"].ToString().ToUpper() + "*";

                        Barcode bcd = new Barcode();
                        bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                        bcd.IncludeLabel = false;
                        bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                        //bcd.LabelPosition = LabelPositions.BOTTOMCENTER;
                        //bcd.LabelFont = new Font("Tahoma", 14, System.Drawing.FontStyle.Regular);
                        bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 265, 65);

                        MemoryStream ms = new MemoryStream();
                        bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                        Byte[] oByte = ms.ToArray();
                        dr["imgBarcode"] = oByte;

                        dsReport.Tables["Invoice"].Rows.Add(dr);
                    }
                    #endregion CustomsInvoice

                    orpt.SetDataSource(dsReport);
                    #endregion Customs_Invoice
                }
                else if (strFormid == "Deliver_Manifest")//psk
                {
                    #region Deliver_Manifest
                    //pageTitle = "Delivery_Batch_Manifest";

                    DeliveryManifestReport_MF1 objReport = new DeliveryManifestReport_MF1();
                    objReport.ResourceName = "DeliveryManifestReport_MF1.rpt";
                    orpt = objReport;

                    strFormatType = (String)Session["FormatType"];
                    DataSet dsConsignments = (DataSet)Session["SESSION_MF_Deli"];
                    MF_Batch_Manifest_Barcode dsReport = new MF_Batch_Manifest_Barcode();
                    

                    foreach (DataRow row in dsConsignments.Tables[0].Rows)
                    {
                        DataRow drXSD = dsReport.Tables["MF_Barcode"].NewRow();
                        if ((row["applicationid"] != null) && (!row["applicationid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["applicationid"] = (string)row["applicationid"];
                        }
                        if ((row["Batch_ID"] != null) && (!row["Batch_ID"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["Batch_ID"] = (string)row["Batch_ID"];
                        }
                        if ((row["Batch_No"] != null) && (!row["Batch_No"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["Batch_No"] = (string)row["Batch_No"];
                        }
                        if ((row["Batch_No1"] != null) && (!row["Batch_No"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["Batch_No1"] = (string)row["Batch_No"];
                        }

                        if ((row["Enterprise_Address"] != null) && (!row["Enterprise_Address"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["Enterprise_Address"] = (string)row["Enterprise_Address"];
                        }
                        if ((row["enterprise_name"] != null) && (!row["enterprise_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["enterprise_name"] = (string)row["enterprise_name"];
                        }
                        if ((row["enterpriseid"] != null) && (!row["enterpriseid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["enterpriseid"] = (string)row["enterpriseid"];
                        }
                        if ((row["remark"] != null) && (!row["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["remark"] = (string)row["remark"];
                        }
                        if ((row["Truck_ID"] != null) && (!row["Truck_ID"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["Truck_ID"] = (string)row["Truck_ID"];
                        }
                        if ((row["AWB_Driver_Name"] != null) && (!row["AWB_Driver_Name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["AWB_Driver_Name"] = (string)row["AWB_Driver_Name"];
                        }
                        
                            if ((row["ref_no"] != null) && (!row["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["ref_no"] = (string)row["ref_no"];
                        }
                        
                            if ((row["cod_amount_type"] != null) && (!row["cod_amount_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["cod_amount_type"] = (string)row["cod_amount_type"];
                        }
                        
                            if ((row["destination_state_code"] != null) && (!row["destination_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["destination_state_code"] = (string)row["destination_state_code"];
                        }
                        
                            if ((row["DestinationStateCode"] != null) && (!row["DestinationStateCode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["DestinationStateCode"] = (string)row["DestinationStateCode"];
                        }
                        
                            if ((row["DestinationStateName"] != null) && (!row["DestinationStateName"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["DestinationStateName"] = (string)row["DestinationStateName"];
                        }
                        
                            if ((row["delivery_type"] != null) && (!row["delivery_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["delivery_type"] = (string)row["delivery_type"];
                        }
                        
                            if ((row["origin_state_code"] != null) && (!row["origin_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["origin_state_code"] = (string)row["origin_state_code"];
                        }
                        
                            if ((row["OriginStateCode"] != null) && (!row["OriginStateCode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["OriginStateCode"] = (string)row["OriginStateCode"];
                        }
                        
                            if ((row["OriginStateName"] != null) && (!row["OriginStateName"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["OriginStateName"] = (string)row["OriginStateName"];
                        }
                        
                            if ((row["Batch_No"] != null) && (!row["Batch_No"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["Batch_No"] = (string)row["Batch_No"];
                        }

                        if ((row["consignment_no"] != null) && (!row["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["consignment_no"] = (string)row["consignment_no"];
                        }

                        if ((row["sender_name"] != null) && (!row["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["sender_name"] = (string)row["sender_name"];
                        }
                        if ((row["sender_zipcode"] != null) && (!row["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["sender_zipcode"] = (string)row["sender_zipcode"];
                        }

                        if ((row["sender_telephone"] != null) && (!row["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["sender_telephone"] = (string)row["sender_telephone"];
                        }

                        if ((row["sender_fax"] != null) && (!row["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["sender_fax"] = (string)row["sender_fax"];
                        }

                        if ((row["recipient_telephone"] != null) && (!row["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["recipient_telephone"] = (string)row["recipient_telephone"];
                        }
                        if ((row["recipient_name"] != null) && (!row["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["recipient_name"] = (string)row["recipient_name"];
                        }
                        if ((row["recipient_zipcode"] != null) && (!row["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["recipient_zipcode"] = ((string)row["recipient_zipcode"]).ToUpper();
                        }
                        if ((row["recipient_fax"] != null) && (!row["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["recipient_fax"] = (string)row["recipient_fax"];
                        }
                        if ((row["recipient_contact_person"] != null) && (!row["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["recipient_contact_person"] = (string)row["recipient_contact_person"];
                        }
                        if ((row["Batch_Date"] != null) && (!row["Batch_Date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["Batch_Date"] = (DateTime)row["Batch_Date"];
                        }
                        if ((row["cod_amount"] != null) && (!row["cod_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["cod_amount"] = (decimal)row["cod_amount"];
                        }
                        if ((row["est_delivery_datetime"] != null) && (!row["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["est_delivery_datetime"] = (DateTime)row["est_delivery_datetime"];
                        }
                        if ((row["last_status_code"] != null) && (!row["last_status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["last_status_code"] = (string)row["last_status_code"];
                        }
                        if ((row["MobileNo"] != null) && (!row["MobileNo"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["MobileNo"] = (string)row["MobileNo"];
                        }
                        if ((row["RecipientAddress"] != null) && (!row["RecipientAddress"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["RecipientAddress"] = (string)row["RecipientAddress"];
                        }
                        if ((row["Recv_Pkg_Qty"] != null) && (!row["Recv_Pkg_Qty"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["Recv_Pkg_Qty"] = (int)row["Recv_Pkg_Qty"];
                        }
                        if ((row["return_invoice_hc"] != null) && (!row["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["return_invoice_hc"] = (string)row["return_invoice_hc"];
                        }
                        if ((row["return_pod_slip"] != null) && (!row["return_pod_slip"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["return_pod_slip"] = (string)row["return_pod_slip"];
                        }
                        if ((row["SenderAddress"] != null) && (!row["SenderAddress"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["SenderAddress"] = (string)row["SenderAddress"];
                        }
                        if ((row["service_code"] != null) && (!row["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["service_code"] = (string)row["service_code"];
                        }
                        if ((row["Shipped_Pkg_Qty"] != null) && (!row["Shipped_Pkg_Qty"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["Shipped_Pkg_Qty"] = (int)row["Shipped_Pkg_Qty"];
                        }
                        if ((row["Shipped_Pkgs"] != null) && (!row["Shipped_Pkgs"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["Shipped_Pkgs"] = (int)row["Shipped_Pkgs"];
                        }
                        if ((row["StatusReport"] != null) && (!row["StatusReport"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["StatusReport"] = (string)row["StatusReport"];
                        }
                        if ((row["tot_dim_wt"] != null) && (!row["tot_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["tot_dim_wt"] = (decimal)row["tot_dim_wt"];
                        }
                        if ((row["Tot_Pkg"] != null) && (!row["Tot_Pkg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["Tot_Pkg"] = (int)row["Tot_Pkg"];
                        }
                        if ((row["tot_wt"] != null) && (!row["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["tot_wt"] = (decimal)row["tot_wt"];
                        }
                            if ((row["MobileNo"] != null) && (!row["MobileNo"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                        {
                            drXSD["MobileNo"] = (string)row["MobileNo"];
                        }


                        //03072019 Pongsakorn
                        string Barcode = "*$" + drXSD["Batch_No"].ToString().ToUpper().ToUpper() + "*";

                        Barcode bcd = new Barcode();
                        bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                        bcd.IncludeLabel = false;
                        bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                        bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 350, 65);

                        MemoryStream ms = new MemoryStream();
                        bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                        Byte[] oByte = ms.ToArray();
                        drXSD["imgBarcode"] = oByte;
                        string prmUserId = utility.GetUserID();
                        drXSD["prmUserId"] = prmUserId;


                        dsReport.Tables[0].Rows.Add(drXSD);
                    }
                    

                    orpt.SetDataSource(dsReport);
                    # endregion Deliver_Manifest
                }
                else if (strFormid.Equals("CustomsDeclarationForm"))
                {
                    #region Customs_Declaration

                    DataSet dsCustomsDeclaration = (DataSet)Session["SESSION_DS_CUSTOMS_DECLARATION"];
                    orpt = new CustomsDeclarationForm();
                    strFormatType = (String)Session["FormatType"];
                    CustomDeclarationDS dsReport = new CustomDeclarationDS();

                    if (dsCustomsDeclaration.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow drData in dsCustomsDeclaration.Tables[0].Rows)
                        {
                            DataRow dr = dsReport.Tables["CustomDeclaration"].NewRow();

                            // ModelOfDeclaration
                            if (drData["ModelOfDeclaration"] != null && drData["ModelOfDeclaration"].ToString() != "")
                            {
                                dr["ModelOfDeclaration"] = drData["ModelOfDeclaration"].ToString();
                            }

                            // ExporterName
                            if (drData["ExporterName"] != null && drData["ExporterName"].ToString() != "")
                            {
                                dr["ExporterName"] = drData["ExporterName"].ToString();
                            }

                            // ExporterAddress1
                            if (drData["ExporterAddress1"] != null && drData["ExporterAddress1"].ToString() != "")
                            {
                                dr["ExporterAddress1"] = drData["ExporterAddress1"].ToString();
                            }

                            // ExporterAddress2
                            if (drData["ExporterAddress2"] != null && drData["ExporterAddress2"].ToString() != "")
                            {
                                dr["ExporterAddress2"] = drData["ExporterAddress2"].ToString();
                            }

                            // ExporterCountryName
                            if (drData["ExporterCountryName"] != null && drData["ExporterCountryName"].ToString() != "")
                            {
                                dr["ExporterCountryName"] = drData["ExporterCountryName"].ToString();
                            }

                            // Pages
                            if (drData["Pages"] != null && drData["Pages"].ToString() != "")
                            {
                                dr["Pages"] = drData["Pages"].ToString();
                            }

                            // PkgQty
                            if (drData["PkgQty"] != null && drData["PkgQty"].ToString() != "")
                            {
                                dr["PkgQty"] = drData["PkgQty"].ToString();
                            }

                            // DeclarantRefNo
                            if (drData["DeclarantRefNo"] != null && drData["DeclarantRefNo"].ToString() != "")
                            {
                                dr["DeclarantRefNo"] = drData["DeclarantRefNo"].ToString();
                            }

                            // ConsigneeName
                            if (drData["ConsigneeName"] != null && drData["ConsigneeName"].ToString() != "")
                            {
                                dr["ConsigneeName"] = drData["ConsigneeName"].ToString();
                            }

                            // ConsigneeAddress1
                            if (drData["ConsigneeAddress1"] != null && drData["ConsigneeAddress1"].ToString() != "")
                            {
                                dr["ConsigneeAddress1"] = drData["ConsigneeAddress1"].ToString();
                            }

                            // ConsigneeAddress2
                            if (drData["ConsigneeAddress2"] != null && drData["ConsigneeAddress2"].ToString() != "")
                            {
                                dr["ConsigneeAddress2"] = drData["ConsigneeAddress2"].ToString();
                            }

                            // ConsigneeZipcode
                            if (drData["ConsigneeZipcode"] != null && drData["ConsigneeZipcode"].ToString() != "")
                            {
                                dr["ConsigneeZipcode"] = drData["ConsigneeZipcode"].ToString();
                            }

                            // ConsigneeState
                            if (drData["ConsigneeState"] != null && drData["ConsigneeState"].ToString() != "")
                            {
                                dr["ConsigneeState"] = drData["ConsigneeState"].ToString();
                            }

                            // ConsigneeCountry
                            if (drData["ConsigneeCountry"] != null && drData["ConsigneeCountry"].ToString() != "")
                            {
                                dr["ConsigneeCountry"] = drData["ConsigneeCountry"].ToString();
                            }

                            // ConsigneeTCNo
                            if (drData["ConsigneeTCNo"] != null && drData["ConsigneeTCNo"].ToString() != "")
                            {
                                dr["ConsigneeTCNo"] = drData["ConsigneeTCNo"].ToString();
                            }

                            // CustomsAgentCode
                            if (drData["CustomsAgentCode"] != null && drData["CustomsAgentCode"].ToString() != "")
                            {
                                dr["CustomsAgentCode"] = drData["CustomsAgentCode"].ToString();
                            }

                            // CustomsAgent
                            if (drData["CustomsAgent"] != null && drData["CustomsAgent"].ToString() != "")
                            {
                                dr["CustomsAgent"] = drData["CustomsAgent"].ToString();
                            }

                            // ShipFlightNo
                            if (drData["ShipFlightNo"] != null && drData["ShipFlightNo"].ToString() != "")
                            {
                                dr["ShipFlightNo"] = drData["ShipFlightNo"].ToString();
                            }

                            // ExporterCountry
                            if (drData["ExporterCountry"] != null && drData["ExporterCountry"].ToString() != "")
                            {
                                dr["ExporterCountry"] = drData["ExporterCountry"].ToString();
                            }

                            // DeliveryTerms
                            if (drData["DeliveryTerms"] != null && drData["DeliveryTerms"].ToString() != "")
                            {
                                dr["DeliveryTerms"] = drData["DeliveryTerms"].ToString();
                            }

                            // Exch1
                            if (drData["Exch1"] != null && drData["Exch1"].ToString() != "")
                            {
                                dr["Exch1"] = drData["Exch1"].ToString();
                            }

                            // TransportMode
                            if (drData["TransportMode"] != null && drData["TransportMode"].ToString() != "")
                            {
                                dr["TransportMode"] = drData["TransportMode"].ToString();
                            }

                            // LoadingPortCity
                            if (drData["LoadingPortCity"] != null && drData["LoadingPortCity"].ToString() != "")
                            {
                                dr["LoadingPortCity"] = drData["LoadingPortCity"].ToString();
                            }

                            // DischargePort
                            if (drData["DischargePort"] != null && drData["DischargePort"].ToString() != "")
                            {
                                dr["DischargePort"] = drData["DischargePort"].ToString();
                            }

                            // BondedWarehouse
                            if (drData["BondedWarehouse"] != null && drData["BondedWarehouse"].ToString() != "")
                            {
                                dr["BondedWarehouse"] = drData["BondedWarehouse"].ToString();
                            }

                            // MarksNos1
                            if (drData["MarksNos1"] != null && drData["MarksNos1"].ToString() != "")
                            {
                                dr["MarksNos1"] = drData["MarksNos1"].ToString();
                            }

                            // MarksNos2
                            if (drData["MarksNos2"] != null && drData["MarksNos2"].ToString() != "")
                            {
                                dr["MarksNos2"] = drData["MarksNos2"].ToString();
                            }

                            // MarksNos3
                            if (drData["MarksNos3"] != null && drData["MarksNos3"].ToString() != "")
                            {
                                dr["MarksNos3"] = drData["MarksNos3"].ToString();
                            }

                            // MarksNos4
                            if (drData["MarksNos4"] != null && drData["MarksNos4"].ToString() != "")
                            {
                                dr["MarksNos4"] = drData["MarksNos4"].ToString();
                            }

                            // MarksNos5
                            if (drData["MarksNos5"] != null && drData["MarksNos5"].ToString() != "")
                            {
                                dr["MarksNos5"] = drData["MarksNos5"].ToString();
                            }

                            // DS11
                            if (drData["DS11"] != null && drData["DS11"].ToString() != "")
                            {
                                dr["DS11"] = drData["DS11"].ToString();
                            }

                            // DS21
                            if (drData["DS21"] != null && drData["DS21"].ToString() != "")
                            {
                                dr["DS21"] = drData["DS21"].ToString();
                            }

                            // DS12
                            if (drData["DS12"] != null && drData["DS12"].ToString() != "")
                            {
                                dr["DS12"] = drData["DS12"].ToString();
                            }

                            // DS22
                            if (drData["DS22"] != null && drData["DS22"].ToString() != "")
                            {
                                dr["DS22"] = drData["DS22"].ToString();
                            }

                            // DS13
                            if (drData["DS13"] != null && drData["DS13"].ToString() != "")
                            {
                                dr["DS13"] = drData["DS13"].ToString();
                            }

                            // DS23
                            if (drData["DS23"] != null && drData["DS23"].ToString() != "")
                            {
                                dr["DS23"] = drData["DS23"].ToString();
                            }

                            // ItmNo1
                            if (drData["ItmNo1"] != null && drData["ItmNo1"].ToString() != "")
                            {
                                dr["ItmNo1"] = drData["ItmNo1"].ToString();
                            }

                            // ItmNo2
                            if (drData["ItmNo2"] != null && drData["ItmNo2"].ToString() != "")
                            {
                                dr["ItmNo2"] = drData["ItmNo2"].ToString();
                            }
                            // ItmNo3
                            if (drData["ItmNo3"] != null && drData["ItmNo3"].ToString() != "")
                            {
                                dr["ItmNo3"] = drData["ItmNo3"].ToString();
                            }

                            // TC1
                            if (drData["TC1"] != null && drData["TC1"].ToString() != "")
                            {
                                dr["TC1"] = drData["TC1"].ToString();
                            }

                            // TC2
                            if (drData["TC2"] != null && drData["TC2"].ToString() != "")
                            {
                                dr["TC2"] = drData["TC2"].ToString();
                            }

                            // TC3
                            if (drData["TC3"] != null && drData["TC3"].ToString() != "")
                            {
                                dr["TC3"] = drData["TC3"].ToString();
                            }

                            // OC1
                            if (drData["OC1"] != null && drData["OC1"].ToString() != "")
                            {
                                dr["OC1"] = drData["OC1"].ToString();
                            }

                            // OC2
                            if (drData["OC2"] != null && drData["OC2"].ToString() != "")
                            {
                                dr["OC2"] = drData["OC2"].ToString();
                            }

                            // OC3
                            if (drData["OC3"] != null && drData["OC3"].ToString() != "")
                            {
                                dr["OC3"] = drData["OC3"].ToString();
                            }

                            // CPC1
                            if (drData["CPC1"] != null && drData["CPC1"].ToString() != "")
                            {
                                dr["CPC1"] = drData["CPC1"].ToString();
                            }

                            // CPC2
                            if (drData["CPC2"] != null && drData["CPC2"].ToString() != "")
                            {
                                dr["CPC2"] = drData["CPC2"].ToString();
                            }

                            // CPC3
                            if (drData["CPC3"] != null && drData["CPC3"].ToString() != "")
                            {
                                dr["CPC3"] = drData["CPC3"].ToString();
                            }
                            // TQ1
                            if (drData["TQ1"] != null && drData["TQ1"].ToString() != "")
                            {
                                // Decimal Remove digit
                                dr["TQ1"] = ChangeFormatWithCommaAndRemoveDigit(drData["TQ1"].ToString());
                            }

                            // TQ2
                            if (drData["TQ2"] != null && drData["TQ2"].ToString() != "")
                            {
                                dr["TQ2"] = ChangeFormatWithCommaAndRemoveDigit(drData["TQ2"].ToString());
                            }

                            // TQ3
                            if (drData["TQ3"] != null && drData["TQ3"].ToString() != "")
                            {
                                dr["TQ3"] = ChangeFormatWithCommaAndRemoveDigit(drData["TQ3"].ToString());
                            }

                            // SQ1
                            if (drData["SQ1"] != null && drData["SQ1"].ToString() != "")
                            {
                                dr["SQ1"] = ChangeFormatWithCommaAndRemoveDigit(drData["SQ1"].ToString());
                            }

                            // SQ2
                            if (drData["SQ2"] != null && drData["SQ2"].ToString() != "")
                            {
                                dr["SQ2"] = ChangeFormatWithCommaAndRemoveDigit(drData["SQ2"].ToString());
                            }

                            // SQ3
                            if (drData["SQ3"] != null && drData["SQ3"].ToString() != "")
                            {
                                dr["SQ3"] = ChangeFormatWithCommaAndRemoveDigit(drData["SQ3"].ToString());
                            }

                            // AWBNo
                            if (drData["AWBNo"] != null && drData["AWBNo"].ToString() != "")
                            {
                                dr["AWBNo"] = drData["AWBNo"].ToString();
                            }

                            // FRT_Amount
                            if (drData["FRT_Amount"] != null && drData["FRT_Amount"].ToString() != "")
                            {
                                dr["FRT_Amount"] = ChangeFormatOfDecimal(drData["FRT_Amount"].ToString());
                            }

                            // FRT_Currency
                            if (drData["FRT_Currency"] != null && drData["FRT_Currency"].ToString() != "")
                            {
                                dr["FRT_Currency"] = drData["FRT_Currency"].ToString();
                            }

                            // INS_Amount
                            if (drData["INS_Amount"] != null && drData["INS_Amount"].ToString() != "")
                            {
                                dr["INS_Amount"] = ChangeFormatOfDecimal(drData["INS_Amount"].ToString());
                            }

                            // INS_Currency
                            if (drData["INS_Currency"] != null && drData["INS_Currency"].ToString() != "")
                            {
                                dr["INS_Currency"] = drData["INS_Currency"].ToString();
                            }

                            // OTF_Amount
                            if (drData["OTF_Amount"] != null && drData["OTF_Amount"].ToString() != "")
                            {
                                dr["OTF_Amount"] = ChangeFormatOfDecimal(drData["OTF_Amount"].ToString());
                            }

                            // OTF_Currency
                            if (drData["OTF_Currency"] != null && drData["OTF_Currency"].ToString() != "")
                            {
                                dr["OTF_Currency"] = drData["OTF_Currency"].ToString();
                            }

                            // OTD_Amount
                            if (drData["OTD_Amount"] != null && drData["OTD_Amount"].ToString() != "")
                            {
                                dr["OTD_Amount"] = ChangeFormatOfDecimal(drData["OTD_Amount"].ToString());
                            }

                            // OTD_Currency
                            if (drData["OTD_Currency"] != null && drData["OTD_Currency"].ToString() != "")
                            {
                                dr["OTD_Currency"] = drData["OTD_Currency"].ToString();
                            }

                            // AWD_Amount
                            if (drData["AWD_Amount"] != null && drData["AWD_Amount"].ToString() != "")
                            {
                                dr["AWD_Amount"] = ChangeFormatOfDecimal(drData["AWD_Amount"].ToString());
                            }

                            // AWD_Currency
                            if (drData["AWD_Currency"] != null && drData["AWD_Currency"].ToString() != "")
                            {
                                dr["AWD_Currency"] = drData["AWD_Currency"].ToString();
                            }

                            // Location
                            if (drData["Location"] != null && drData["Location"].ToString() != "")
                            {
                                dr["Location"] = drData["Location"].ToString();
                            }

                            // PrintedDT
                            if (drData["PrintedDT"] != null && drData["PrintedDT"].ToString() != "")
                            {
                                try
                                {
                                    dr["PrintedDT"] = Convert.ToDateTime(drData["PrintedDT"].ToString());
                                }
                                catch
                                {
                                    dr["PrintedDT"] = drData["PrintedDT"].ToString();
                                }
                            }

                            // Duty_Rate1
                            if (drData["Duty_Rate1"] != null && drData["Duty_Rate1"].ToString() != "")
                            {
                                dr["Duty_Rate1"] = ChangeFormatOfDecimal(drData["Duty_Rate1"].ToString());
                            }

                            // Duty_Amount1
                            if (drData["Duty_Amount1"] != null && drData["Duty_Amount1"].ToString() != "")
                            {
                                dr["Duty_Amount1"] = ChangeFormatOfDecimal(drData["Duty_Amount1"].ToString());
                            }

                            // Excise_Rate1
                            if (drData["Excise_Rate1"] != null && drData["Excise_Rate1"].ToString() != "")
                            {
                                dr["Excise_Rate1"] = ChangeFormatOfDecimal(drData["Excise_Rate1"].ToString());
                            }

                            // Excise_Amount1
                            if (drData["Excise_Amount1"] != null && drData["Excise_Amount1"].ToString() != "")
                            {
                                dr["Excise_Amount1"] = ChangeFormatOfDecimal(drData["Excise_Amount1"].ToString());
                            }

                            // Tax_Rate1
                            if (drData["Tax_Rate1"] != null && drData["Tax_Rate1"].ToString() != "")
                            {
                                dr["Tax_Rate1"] = drData["Tax_Rate1"].ToString();
                            }

                            // Tax_Amount1
                            if (drData["Tax_Amount1"] != null && drData["Tax_Amount1"].ToString() != "")
                            {
                                dr["Tax_Amount1"] = ChangeFormatOfDecimal(drData["Tax_Amount1"].ToString());
                            }

                            // Total_DutyExciseTax1
                            if (drData["Total_DutyExciseTax1"] != null && drData["Total_DutyExciseTax1"].ToString() != "")
                            {
                                dr["Total_DutyExciseTax1"] = ChangeFormatOfDecimal(drData["Total_DutyExciseTax1"].ToString());
                            }

                            // Duty_Rate2
                            if (drData["Duty_Rate2"] != null && drData["Duty_Rate2"].ToString() != "")
                            {
                                dr["Duty_Rate2"] = ChangeFormatOfDecimal(drData["Duty_Rate2"].ToString());
                            }

                            // Duty_Amount2
                            if (drData["Duty_Amount2"] != null && drData["Duty_Amount2"].ToString() != "")
                            {
                                dr["Duty_Amount2"] = ChangeFormatOfDecimal(drData["Duty_Amount2"].ToString());
                            }

                            // Excise_Rate2
                            if (drData["Excise_Rate2"] != null && drData["Excise_Rate2"].ToString() != "")
                            {
                                dr["Excise_Rate2"] = ChangeFormatOfDecimal(drData["Excise_Rate2"].ToString());
                            }

                            // Excise_Amount2
                            if (drData["Excise_Amount2"] != null && drData["Excise_Amount2"].ToString() != "")
                            {
                                dr["Excise_Amount2"] = ChangeFormatOfDecimal(drData["Excise_Amount2"].ToString());
                            }

                            // Tax_Rate2
                            if (drData["Tax_Rate2"] != null && drData["Tax_Rate2"].ToString() != "")
                            {
                                dr["Tax_Rate2"] = drData["Tax_Rate2"].ToString();
                            }

                            // Tax_Amount2
                            if (drData["Tax_Amount2"] != null && drData["Tax_Amount2"].ToString() != "")
                            {
                                dr["Tax_Amount2"] = ChangeFormatOfDecimal(drData["Tax_Amount2"].ToString());
                            }

                            // Total_DutyExciseTax2
                            if (drData["Total_DutyExciseTax2"] != null && drData["Total_DutyExciseTax2"].ToString() != "")
                            {
                                dr["Total_DutyExciseTax2"] = ChangeFormatOfDecimal(drData["Total_DutyExciseTax2"].ToString());
                            }

                            // Duty_Rate3
                            if (drData["Duty_Rate3"] != null && drData["Duty_Rate3"].ToString() != "")
                            {
                                dr["Duty_Rate3"] = ChangeFormatOfDecimal(drData["Duty_Rate3"].ToString());
                            }

                            // Duty_Amount3
                            if (drData["Duty_Amount3"] != null && drData["Duty_Amount3"].ToString() != "")
                            {
                                dr["Duty_Amount3"] = ChangeFormatOfDecimal(drData["Duty_Amount3"].ToString());
                            }

                            // Excise_Rate3
                            if (drData["Excise_Rate3"] != null && drData["Excise_Rate3"].ToString() != "")
                            {
                                dr["Excise_Rate3"] = ChangeFormatOfDecimal(drData["Excise_Rate3"].ToString());
                            }

                            // Excise_Amount3
                            if (drData["Excise_Amount3"] != null && drData["Excise_Amount3"].ToString() != "")
                            {
                                dr["Excise_Amount3"] = ChangeFormatOfDecimal(drData["Excise_Amount3"].ToString());
                            }

                            // Tax_Rate3
                            if (drData["Tax_Rate3"] != null && drData["Tax_Rate3"].ToString() != "")
                            {
                                dr["Tax_Rate3"] = drData["Tax_Rate3"].ToString();
                            }

                            // Tax_Amount3
                            if (drData["Tax_Amount3"] != null && drData["Tax_Amount3"].ToString() != "")
                            {
                                dr["Tax_Amount3"] = ChangeFormatOfDecimal(drData["Tax_Amount3"].ToString());
                            }

                            // Total_DutyExciseTax3
                            if (drData["Total_DutyExciseTax3"] != null && drData["Total_DutyExciseTax3"].ToString() != "")
                            {
                                dr["Total_DutyExciseTax3"] = ChangeFormatOfDecimal(drData["Total_DutyExciseTax3"].ToString());
                            }

                            // Total_Import
                            if (drData["Total_Import"] != null && drData["Total_Import"].ToString() != "")
                            {
                                dr["Total_Import"] = ChangeFormatOfDecimal(drData["Total_Import"].ToString());
                            }

                            // Total_Tax
                            if (drData["Total_Tax"] != null && drData["Total_Tax"].ToString() != "")
                            {
                                dr["Total_Tax"] = ChangeFormatOfDecimal(drData["Total_Tax"].ToString());
                            }

                            // Total_Declaration
                            if (drData["Total_Declaration"] != null && drData["Total_Declaration"].ToString() != "")
                            {
                                dr["Total_Declaration"] = ChangeFormatOfDecimal(drData["Total_Declaration"].ToString());
                            }

                            // Total_Invoice_Price
                            if (drData["Total_Invoice_Price"] != null && drData["Total_Invoice_Price"].ToString() != "")
                            {
                                dr["Total_Invoice_Price"] = ChangeFormatOfDecimal(drData["Total_Invoice_Price"].ToString());
                            }

                            // Invoice_Currency
                            if (drData["Invoice_Currency"] != null && drData["Invoice_Currency"].ToString() != "")
                            {
                                dr["Invoice_Currency"] = drData["Invoice_Currency"].ToString();
                            }

                            // AMT1
                            if (drData["AMT1"] != null && drData["AMT1"].ToString() != "")
                            {
                                dr["AMT1"] = ChangeFormatOfDecimal(drData["AMT1"].ToString());
                            }

                            // AMT2
                            if (drData["AMT2"] != null && drData["AMT2"].ToString() != "")
                            {
                                dr["AMT2"] = ChangeFormatOfDecimal(drData["AMT2"].ToString());
                            }

                            // AMT3
                            if (drData["AMT3"] != null && drData["AMT3"].ToString() != "")
                            {
                                dr["AMT3"] = ChangeFormatOfDecimal(drData["AMT3"].ToString());
                            }

                            // CIF1
                            if (drData["CIF1"] != null && drData["CIF1"].ToString() != "")
                            {
                                dr["CIF1"] = ChangeFormatOfDecimal(drData["CIF1"].ToString());
                            }

                            // CIF2
                            if (drData["CIF2"] != null && drData["CIF2"].ToString() != "")
                            {
                                dr["CIF2"] = ChangeFormatOfDecimal(drData["CIF2"].ToString());
                            }

                            // CIF3
                            if (drData["CIF3"] != null && drData["CIF3"].ToString() != "")
                            {
                                dr["CIF3"] = ChangeFormatOfDecimal(drData["CIF3"].ToString());
                            }
                            // MarksNos1_2
                            if (drData["MarksNos1_2"] != null && drData["MarksNos1_2"].ToString() != "")
                            {
                                dr["MarksNos1_2"] = drData["MarksNos1_2"].ToString();
                            }

                            // MarksNos2_2
                            if (drData["MarksNos2_2"] != null && drData["MarksNos2_2"].ToString() != "")
                            {
                                dr["MarksNos2_2"] = drData["MarksNos2_2"].ToString();
                            }

                            // MarksNos3_2
                            if (drData["MarksNos3_2"] != null && drData["MarksNos3_2"].ToString() != "")
                            {
                                dr["MarksNos3_2"] = drData["MarksNos3_2"].ToString();
                            }

                            // MarksNos4_2
                            if (drData["MarksNos4_2"] != null && drData["MarksNos4_2"].ToString() != "")
                            {
                                dr["MarksNos4_2"] = drData["MarksNos4_2"].ToString();
                            }

                            // MarksNos5_2
                            if (drData["MarksNos5_2"] != null && drData["MarksNos5_2"].ToString() != "")
                            {
                                dr["MarksNos5_2"] = drData["MarksNos5_2"].ToString();
                            }

                            // MarksNos1_3
                            if (drData["MarksNos1_3"] != null && drData["MarksNos1_3"].ToString() != "")
                            {
                                dr["MarksNos1_3"] = drData["MarksNos1_3"].ToString();
                            }

                            // MarksNos2_3
                            if (drData["MarksNos2_3"] != null && drData["MarksNos2_3"].ToString() != "")
                            {
                                dr["MarksNos2_3"] = drData["MarksNos2_3"].ToString();
                            }

                            // MarksNos3_3
                            if (drData["MarksNos3_3"] != null && drData["MarksNos3_3"].ToString() != "")
                            {
                                dr["MarksNos3_3"] = drData["MarksNos3_3"].ToString();
                            }

                            // MarksNos4_3
                            if (drData["MarksNos4_3"] != null && drData["MarksNos4_3"].ToString() != "")
                            {
                                dr["MarksNos4_3"] = drData["MarksNos4_3"].ToString();
                            }

                            // MarksNos5_3
                            if (drData["MarksNos5_3"] != null && drData["MarksNos5_3"].ToString() != "")
                            {
                                dr["MarksNos5_3"] = drData["MarksNos5_3"].ToString();
                            }

                            // AWBNo_2
                            if (drData["AWBNo_2"] != null && drData["AWBNo_2"].ToString() != "")
                            {
                                dr["AWBNo_2"] = drData["AWBNo_2"].ToString();
                            }

                            // AWBNo_3
                            if (drData["AWBNo_3"] != null && drData["AWBNo_3"].ToString() != "")
                            {
                                dr["AWBNo_3"] = drData["AWBNo_3"].ToString();
                            }

                            // Total_Fees
                            if (drData["Total_Fees"] != null && drData["Total_Fees"].ToString() != "")
                            {
                                dr["Total_Fees"] = ChangeFormatOfDecimal(drData["Total_Fees"].ToString());
                            }

                            dsReport.Tables["CustomDeclaration"].Rows.Add(dr);
                        }

                        orpt.SetDataSource(dsReport);
                    }
                    #endregion Customs_Declaration
                }
                else if (strFormid == "RePrintConsNotes")
                {

                    if (strReportTemplate == "PreprintConsignmentTNT.rpt")
                    {
                        pageTitle = "PrintConsignmentNote";
                        orpt = new PreprintConsignmentTNT4();
                        strFormatType = (String)Session["FormatType"];
                        DataSet dsConsignments = (DataSet)Session["SESSION_DS_PRINTCONSNOTES"];

                        CSS_PreprintConsNotes dsReportOnePage = new CSS_PreprintConsNotes();
                        #region PrintConsNotesOnePage
                        foreach (DataRow row in dsConsignments.Tables[1].Rows)
                        {
                            DataRow _drXSD = dsReportOnePage.Tables["CSS_PrintConsNotes"].NewRow();
                            if ((row["consignment_no"] != null) && (!row["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["consignment_no"] = (string)row["consignment_no"];
                            }
                            if ((row["payerid"] != null) && (!row["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["payerid"] = (string)row["payerid"];
                            }
                            if ((row["service_code"] != null) && (!row["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["service_code"] = (string)row["service_code"];
                            }

                            if ((row["ref_no"] != null) && (!row["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["ref_no"] = (string)row["ref_no"];
                            }

                            if ((row["sender_name"] != null) && (!row["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_name"] = (string)row["sender_name"];
                            }
                            if ((row["sender_address1"] != null) && (!row["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_address1"] = (string)row["sender_address1"];
                            }

                            if ((row["sender_address2"] != null) && (!row["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_address2"] = (string)row["sender_address2"];
                            }

                            if ((row["sender_zipcode"] != null) && (!row["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_zipcode"] = (string)row["sender_zipcode"];
                            }


                            if ((row["sender_state"] != null) && (!row["sender_state"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_state"] = (string)row["sender_state"];
                            }

                            if ((row["sender_telephone"] != null) && (!row["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_telephone"] = (string)row["sender_telephone"];
                            }

                            if ((row["sender_fax"] != null) && (!row["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_fax"] = (string)row["sender_fax"];
                            }
                            if ((row["sender_contact_person"] != null) && (!row["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_contact_person"] = (string)row["sender_contact_person"];
                            }
                            if ((row["sender_email"] != null) && (!row["sender_email"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_email"] = (string)row["sender_email"];
                            }

                            if ((row["recipient_telephone"] != null) && (!row["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_telephone"] = (string)row["recipient_telephone"];
                            }
                            if ((row["recipient_name"] != null) && (!row["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_name"] = (string)row["recipient_name"];
                            }
                            if ((row["recipient_address1"] != null) && (!row["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_address1"] = (string)row["recipient_address1"];
                            }

                            if ((row["recipient_address2"] != null) && (!row["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_address2"] = (string)row["recipient_address2"];
                            }

                            if ((row["recipient_zipcode"] != null) && (!row["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_zipcode"] = ((string)row["recipient_zipcode"]).ToUpper();
                            }

                            if ((row["recipient_state"] != null) && (!row["recipient_state"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_state"] = (string)row["recipient_state"];
                            }

                            if ((row["recipient_fax"] != null) && (!row["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_fax"] = (string)row["recipient_fax"];
                            }
                            if ((row["recipient_contact_person"] != null) && (!row["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_contact_person"] = (string)row["recipient_contact_person"];
                            }
                            if ((row["declare_value"] != null) && (!row["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["declare_value"] = (decimal)row["declare_value"];
                            }
                            if ((row["cod_amount"] != null) && (!row["cod_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["cod_amount"] = (decimal)row["cod_amount"];
                            }
                            if ((row["remark"] != null) && (!row["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["remark"] = (string)row["remark"];
                            }
                            if ((row["return_pod_slip"] != null) && (!row["return_pod_slip"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["return_pod_slip"] = (string)row["return_pod_slip"];
                            }
                            if ((row["return_invoice_hc"] != null) && (!row["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["return_invoice_hc"] = (string)row["return_invoice_hc"];
                            }
                            if ((row["DangerousGoods"] != null) && (!row["DangerousGoods"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["DangerousGoods"] = (byte)row["DangerousGoods"];
                            }
                            if ((row["MoreInfo"] != null) && (!row["MoreInfo"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["MoreInfo"] = (string)row["MoreInfo"];
                            }
                            if ((row["MorePkgs"] != null) && (!row["MorePkgs"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["MorePkgs"] = (string)row["MorePkgs"];
                            }
                            if ((row["Qty1"] != null) && (!row["Qty1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty1"] = (int)row["Qty1"];
                            }
                            if ((row["Qty2"] != null) && (!row["Qty2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty2"] = (int)row["Qty2"];
                            }
                            if ((row["Qty3"] != null) && (!row["Qty3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty3"] = (int)row["Qty3"];
                            }
                            if ((row["Qty4"] != null) && (!row["Qty4"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty4"] = (int)row["Qty4"];
                            }
                            if ((row["Qty5"] != null) && (!row["Qty5"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty5"] = (int)row["Qty5"];
                            }
                            if ((row["Qty6"] != null) && (!row["Qty6"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty6"] = (int)row["Qty6"];
                            }
                            if ((row["Qty7"] != null) && (!row["Qty7"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty7"] = (int)row["Qty7"];
                            }
                            if ((row["Qty8"] != null) && (!row["Qty8"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty8"] = (int)row["Qty8"];
                            }
                            if ((row["Qty"] != null) && (!row["Qty"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty"] = (int)row["Qty"];
                            }
                            if ((row["Wgt1"] != null) && (!row["Wgt1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt1"] = (decimal)row["Wgt1"];
                            }
                            if ((row["Wgt2"] != null) && (!row["Wgt2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt2"] = (decimal)row["Wgt2"];
                            }
                            if ((row["Wgt3"] != null) && (!row["Wgt3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt3"] = (decimal)row["Wgt3"];
                            }
                            if ((row["Wgt4"] != null) && (!row["Wgt4"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt4"] = (decimal)row["Wgt4"];
                            }
                            if ((row["Wgt5"] != null) && (!row["Wgt5"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt5"] = (decimal)row["Wgt5"];
                            }
                            if ((row["Wgt6"] != null) && (!row["Wgt6"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt6"] = (decimal)row["Wgt6"];
                            }
                            if ((row["Wgt7"] != null) && (!row["Wgt7"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt7"] = (decimal)row["Wgt7"];
                            }
                            if ((row["Wgt8"] != null) && (!row["Wgt8"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt8"] = (decimal)row["Wgt8"];
                            }
                            if ((row["Wgt"] != null) && (!row["Wgt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt"] = (decimal)row["Wgt"];
                            }
                            if ((row["CopyName"] != null) && (!row["CopyName"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["CopyName"] = (string)row["CopyName"];
                            }
                            if ((row["sender_country"] != null) && (!row["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_country"] = (string)row["sender_country"];
                            }
                            if ((row["recipient_country"] != null) && (!row["recipient_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_country"] = (string)row["recipient_country"];
                            }
                            if ((row["GoodsDescription"] != null) && (!row["GoodsDescription"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["GoodsDescription"] = (string)row["GoodsDescription"];
                            }
                            if ((row["tot_freight_charge"] != null) && (!row["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Rtg1"] = (string)row["tot_freight_charge"];
                            }
                            if ((row["total_rated_amount_toea"] != null) && (!row["total_rated_amount_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Rtg1_toea"] = (string)row["total_rated_amount_toea"];
                            }
                            if ((row["tax_on_rated_amount_kina"] != null) && (!row["tax_on_rated_amount_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Rtg2"] = (string)row["tax_on_rated_amount_kina"];
                            }
                            if ((row["tax_on_rated_amount_toea"] != null) && (!row["tax_on_rated_amount_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Rtg2_toea"] = (string)row["tax_on_rated_amount_toea"];
                            }
                            if ((row["total_amount_kina"] != null) && (!row["total_amount_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Rtg3"] = (string)row["total_amount_kina"];
                            }
                            if ((row["total_amount_toea"] != null) && (!row["total_amount_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Rtg3_toea"] = (string)row["total_amount_toea"];
                            }
                            if ((row["consignee_name"] != null) && (!row["consignee_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Consignee_name"] = (string)row["consignee_name"];
                            }
                            if ((row["pod_date"] != null) && (!row["pod_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Pod_date"] = (string)row["pod_date"];
                            }
                            if ((row["pod_time"] != null) && (!row["pod_time"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Pod_time"] = (string)row["pod_time"];
                            }
                            if ((row["basic_charge_kina"] != null) && (!row["basic_charge_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["basic_charge"] = (string)row["basic_charge_kina"];
                            }
                            if ((row["basic_charge_toea"] != null) && (!row["basic_charge_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["basic_charge_toea"] = (string)row["basic_charge_toea"];
                            }
                            if ((row["other_surch_amount_kina"] != null) && (!row["other_surch_amount_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["other_surch_amount"] = (string)row["other_surch_amount_kina"];
                            }
                            if ((row["other_surch_amount_toea"] != null) && (!row["other_surch_amount_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["other_surch_amount_toea"] = (string)row["other_surch_amount_toea"];
                            }
                            if ((row["Signature_Base64"] != null) && (!row["Signature_Base64"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Signature_Base64"] = Convert.FromBase64String((string)row["Signature_Base64"]);
                            }
                            if ((row["ConsignmentDescription"] != null) && (!row["ConsignmentDescription"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["ConsignmentDescription"] = (string)row["ConsignmentDescription"];
                            }
                            if ((row["tot_vas_surcharge_kina"] != null) && (!row["tot_vas_surcharge_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["tot_vas_surcharge_kina"] = (string)row["tot_vas_surcharge_kina"];
                            }
                            if ((row["tot_vas_surcharge_toea"] != null) && (!row["tot_vas_surcharge_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["tot_vas_surcharge_toea"] = (string)row["tot_vas_surcharge_toea"];
                            }
                            if ((row["export_freight_charge_kina"] != null) && (!row["export_freight_charge_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["export_freight_charge_kina"] = (string)row["export_freight_charge_kina"];
                            }
                            if ((row["export_freight_charge_toea"] != null) && (!row["export_freight_charge_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["export_freight_charge_toea"] = (string)row["export_freight_charge_toea"];
                            }

                            string Barcode = "*" + _drXSD["consignment_no"].ToString().ToUpper() + "*";

                            Barcode bcd = new Barcode();
                            bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                            bcd.IncludeLabel = false;
                            bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                            bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 235, 65);

                            MemoryStream ms = new MemoryStream();
                            bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                            Byte[] oByte = ms.ToArray();
                            _drXSD["imgBarcode"] = oByte;

                            dsReportOnePage.Tables["CSS_PrintConsNotes"].Rows.Add(_drXSD);
                            orpt.SetDataSource(dsReportOnePage);
                            BindReportOnePage((string)row["consignment_no"]);
                            dsReportOnePage.Tables["CSS_PrintConsNotes"].Clear();
                        }
                        #endregion PrintConsNotesOnePage

                        CSS_PreprintConsNotes dsReport = new CSS_PreprintConsNotes();
                        #region PrintConsNotes
                        foreach (DataRow row in dsConsignments.Tables[1].Rows)
                        {

                            DataRow drXSD = dsReport.Tables["CSS_PrintConsNotes"].NewRow();

                            if ((row["consignment_no"] != null) && (!row["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["consignment_no"] = (string)row["consignment_no"];
                            }

                            if ((row["payerid"] != null) && (!row["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["payerid"] = (string)row["payerid"];
                            }

                            if ((row["service_code"] != null) && (!row["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["service_code"] = (string)row["service_code"];
                            }

                            if ((row["ref_no"] != null) && (!row["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["ref_no"] = (string)row["ref_no"];
                            }

                            if ((row["sender_name"] != null) && (!row["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_name"] = (string)row["sender_name"];
                            }
                            if ((row["sender_address1"] != null) && (!row["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_address1"] = (string)row["sender_address1"];
                            }

                            if ((row["sender_address2"] != null) && (!row["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_address2"] = (string)row["sender_address2"];
                            }

                            if ((row["sender_zipcode"] != null) && (!row["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_zipcode"] = (string)row["sender_zipcode"];
                            }


                            if ((row["sender_state"] != null) && (!row["sender_state"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_state"] = (string)row["sender_state"];
                            }

                            if ((row["sender_telephone"] != null) && (!row["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_telephone"] = (string)row["sender_telephone"];
                            }

                            if ((row["sender_fax"] != null) && (!row["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_fax"] = (string)row["sender_fax"];
                            }
                            if ((row["sender_contact_person"] != null) && (!row["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_contact_person"] = (string)row["sender_contact_person"];
                            }
                            if ((row["sender_email"] != null) && (!row["sender_email"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_email"] = (string)row["sender_email"];
                            }

                            if ((row["recipient_telephone"] != null) && (!row["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_telephone"] = (string)row["recipient_telephone"];
                            }
                            if ((row["recipient_name"] != null) && (!row["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_name"] = (string)row["recipient_name"];
                            }
                            if ((row["recipient_address1"] != null) && (!row["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_address1"] = (string)row["recipient_address1"];
                            }

                            if ((row["recipient_address2"] != null) && (!row["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_address2"] = (string)row["recipient_address2"];
                            }

                            if ((row["recipient_zipcode"] != null) && (!row["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_zipcode"] = ((string)row["recipient_zipcode"]).ToUpper();
                            }

                            if ((row["recipient_state"] != null) && (!row["recipient_state"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_state"] = (string)row["recipient_state"];
                            }

                            if ((row["recipient_fax"] != null) && (!row["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_fax"] = (string)row["recipient_fax"];
                            }
                            if ((row["recipient_contact_person"] != null) && (!row["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_contact_person"] = (string)row["recipient_contact_person"];
                            }
                            if ((row["declare_value"] != null) && (!row["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["declare_value"] = (decimal)row["declare_value"];
                            }
                            if ((row["cod_amount"] != null) && (!row["cod_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["cod_amount"] = (decimal)row["cod_amount"];
                            }
                            if ((row["remark"] != null) && (!row["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["remark"] = (string)row["remark"];
                            }
                            if ((row["return_pod_slip"] != null) && (!row["return_pod_slip"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["return_pod_slip"] = (string)row["return_pod_slip"];
                            }
                            if ((row["return_invoice_hc"] != null) && (!row["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["return_invoice_hc"] = (string)row["return_invoice_hc"];
                            }
                            if ((row["DangerousGoods"] != null) && (!row["DangerousGoods"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["DangerousGoods"] = (byte)row["DangerousGoods"];
                            }
                            if ((row["MoreInfo"] != null) && (!row["MoreInfo"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["MoreInfo"] = (string)row["MoreInfo"];
                            }
                            if ((row["MorePkgs"] != null) && (!row["MorePkgs"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["MorePkgs"] = (string)row["MorePkgs"];
                            }
                            if ((row["Qty1"] != null) && (!row["Qty1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty1"] = (int)row["Qty1"];
                            }
                            if ((row["Qty2"] != null) && (!row["Qty2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty2"] = (int)row["Qty2"];
                            }
                            if ((row["Qty3"] != null) && (!row["Qty3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty3"] = (int)row["Qty3"];
                            }
                            if ((row["Qty4"] != null) && (!row["Qty4"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty4"] = (int)row["Qty4"];
                            }
                            if ((row["Qty5"] != null) && (!row["Qty5"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty5"] = (int)row["Qty5"];
                            }
                            if ((row["Qty6"] != null) && (!row["Qty6"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty6"] = (int)row["Qty6"];
                            }
                            if ((row["Qty7"] != null) && (!row["Qty7"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty7"] = (int)row["Qty7"];
                            }
                            if ((row["Qty8"] != null) && (!row["Qty8"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty8"] = (int)row["Qty8"];
                            }
                            if ((row["Qty"] != null) && (!row["Qty"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Qty"] = (int)row["Qty"];
                            }

                            if ((row["Wgt1"] != null) && (!row["Wgt1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt1"] = (decimal)row["Wgt1"];
                            }
                            if ((row["Wgt2"] != null) && (!row["Wgt2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt2"] = (decimal)row["Wgt2"];
                            }
                            if ((row["Wgt3"] != null) && (!row["Wgt3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt3"] = (decimal)row["Wgt3"];
                            }
                            if ((row["Wgt4"] != null) && (!row["Wgt4"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt4"] = (decimal)row["Wgt4"];
                            }
                            if ((row["Wgt5"] != null) && (!row["Wgt5"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt5"] = (decimal)row["Wgt5"];
                            }
                            if ((row["Wgt6"] != null) && (!row["Wgt6"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt6"] = (decimal)row["Wgt6"];
                            }
                            if ((row["Wgt7"] != null) && (!row["Wgt7"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt7"] = (decimal)row["Wgt7"];
                            }
                            if ((row["Wgt8"] != null) && (!row["Wgt8"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt8"] = (decimal)row["Wgt8"];
                            }
                            if ((row["Wgt"] != null) && (!row["Wgt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Wgt"] = (decimal)row["Wgt"];
                            }
                            if ((row["CopyName"] != null) && (!row["CopyName"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["CopyName"] = (string)row["CopyName"];
                            }
                            if ((row["sender_country"] != null) && (!row["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["sender_country"] = (string)row["sender_country"];
                            }
                            if ((row["recipient_country"] != null) && (!row["recipient_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["recipient_country"] = (string)row["recipient_country"];
                            }
                            if ((row["GoodsDescription"] != null) && (!row["GoodsDescription"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["GoodsDescription"] = (string)row["GoodsDescription"];
                            }
                            /*
							if((row["total_rated_amount"]!= null) && (!row["total_rated_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								drXSD["Rtg1"]= (decimal)row["total_rated_amount"];
							}
							if((row["tax_on_rated_amount"]!= null) && (!row["tax_on_rated_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								drXSD["Rtg2"]= (decimal)row["tax_on_rated_amount"];
							}
							if((row["total_amount"]!= null) && (!row["total_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								drXSD["Rtg3"]= (decimal)row["total_amount"];
							}
							*/
                            if ((row["tot_freight_charge"] != null) && (!row["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Rtg1"] = (string)row["tot_freight_charge"];
                            }
                            if ((row["total_rated_amount_toea"] != null) && (!row["total_rated_amount_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Rtg1_toea"] = (string)row["total_rated_amount_toea"];
                            }
                            if ((row["tax_on_rated_amount_kina"] != null) && (!row["tax_on_rated_amount_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Rtg2"] = (string)row["tax_on_rated_amount_kina"];
                            }
                            if ((row["tax_on_rated_amount_toea"] != null) && (!row["tax_on_rated_amount_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Rtg2_toea"] = (string)row["tax_on_rated_amount_toea"];
                            }
                            if ((row["total_amount_kina"] != null) && (!row["total_amount_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Rtg3"] = (string)row["total_amount_kina"];
                            }
                            if ((row["total_amount_toea"] != null) && (!row["total_amount_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Rtg3_toea"] = (string)row["total_amount_toea"];
                            }
                            if ((row["consignee_name"] != null) && (!row["consignee_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Consignee_name"] = (string)row["consignee_name"];
                            }
                            if ((row["pod_date"] != null) && (!row["pod_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Pod_date"] = (string)row["pod_date"];
                            }
                            if ((row["pod_time"] != null) && (!row["pod_time"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Pod_time"] = (string)row["pod_time"];
                            }
                            if ((row["basic_charge_kina"] != null) && (!row["basic_charge_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["basic_charge"] = (string)row["basic_charge_kina"];
                            }
                            if ((row["basic_charge_toea"] != null) && (!row["basic_charge_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["basic_charge_toea"] = (string)row["basic_charge_toea"];
                            }
                            if ((row["other_surch_amount_kina"] != null) && (!row["other_surch_amount_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["other_surch_amount"] = (string)row["other_surch_amount_kina"];
                            }
                            if ((row["other_surch_amount_toea"] != null) && (!row["other_surch_amount_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["other_surch_amount_toea"] = (string)row["other_surch_amount_toea"];
                            }
                            if ((row["Signature_Base64"] != null) && (!row["Signature_Base64"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["Signature_Base64"] = Convert.FromBase64String((string)row["Signature_Base64"]);
                            }
                            if ((row["ConsignmentDescription"] != null) && (!row["ConsignmentDescription"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["ConsignmentDescription"] = (string)row["ConsignmentDescription"];
                            }
                            if ((row["tot_vas_surcharge_kina"] != null) && (!row["tot_vas_surcharge_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["tot_vas_surcharge_kina"] = (string)row["tot_vas_surcharge_kina"];
                            }
                            if ((row["tot_vas_surcharge_toea"] != null) && (!row["tot_vas_surcharge_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["tot_vas_surcharge_toea"] = (string)row["tot_vas_surcharge_toea"];
                            }
                            if ((row["export_freight_charge_kina"] != null) && (!row["export_freight_charge_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["export_freight_charge_kina"] = (string)row["export_freight_charge_kina"];
                            }
                            if ((row["export_freight_charge_toea"] != null) && (!row["export_freight_charge_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drXSD["export_freight_charge_toea"] = (string)row["export_freight_charge_toea"];
                            }

                            string Barcode = "*" + drXSD["consignment_no"].ToString().ToUpper() + "*";

                            Barcode bcd = new Barcode();
                            bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                            bcd.IncludeLabel = false;
                            bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                            bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 235, 65);

                            MemoryStream ms = new MemoryStream();
                            bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                            Byte[] oByte = ms.ToArray();
                            drXSD["imgBarcode"] = oByte;

                            dsReport.Tables["CSS_PrintConsNotes"].Rows.Add(drXSD);
                        }
                        #endregion PrintConsNotes
                        orpt.SetDataSource(dsReport);

                    }
                }

                BindReport();

            }
            catch (Exception ex)
            {
                lblErrorMesssage.Text = ex.ToString();
                orpt.Close();
                orpt.Dispose();
            }
        }


        private byte[] FromBase64Bytes(byte[] base64Bytes)
        {

            string base64String = Encoding.Default.GetString(base64Bytes, 0, base64Bytes.Length);
            return Convert.FromBase64String(base64String);
        }

        private string ChangeFormatOfDecimal(string textDecimal)
        {
            try
            {
                decimal decimalText = Convert.ToDecimal(textDecimal);
                string strWitchComma = decimalText.ToString("#,##0.00");
                return strWitchComma;
            }
            catch
            {
                return textDecimal;
            }
        }

        private string ChangeFormatWithCommaAndRemoveDigit(string textDecimal)
        {
            try
            {
                decimal decimalText = Convert.ToDecimal(textDecimal);
                string strWitchComma = decimalText.ToString("#,##0.00").TrimEnd('0');

                strWitchComma = strWitchComma.Replace(".00", "");

                string checkString = strWitchComma;
                if (checkString.IndexOf(".") != -1)
                {
                    int begin = checkString.IndexOf(".");
                    int end = checkString.Length - 1;

                    if (begin == end)
                    {
                        strWitchComma = checkString.Substring(0, checkString.IndexOf("."));
                    }
                }


                return strWitchComma;
            }
            catch
            {
                return textDecimal;
            }
        }

        private void BindReport()
        {
            ExportOptions ExportOptions = new ExportOptions();
            DiskFileDestinationOptions DiskFileDstOptions = new DiskFileDestinationOptions();
            String strExportFile = null;

            if (strFormatType == "")
            {
                return;
            }

            string strExportFolder = System.Configuration.ConfigurationManager.AppSettings["ReportExportFolder"];
            strExportFolder = Server.MapPath(strExportFolder);
            if (!System.IO.Directory.Exists(strExportFolder))
            {
                lblErrorMesssage.Text = "Export folder not found. Please create the folder :'" + strExportFolder + "' and export";
                return;
            }

            ///************IMPORTANT:Get the Export File directory from Web.config, Permissions must be given for
            ///************"aspnet" user on the Folder read from "ReportExportFolder"
            strExportFile = strExportFolder + "\\" + Session.SessionID.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();

            //			strExportFile = "C:\\" + Session.SessionID.ToString();

            string strFormid = (String)Session["FORMID"];
            if (strFormid == "PrintShipping")
            {
                orpt.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
                orpt.PrintOptions.PaperSize = PaperSize.PaperA4;
            }
            else if(strFormid == "Deliver_Manifest")//psk
            {
                orpt.PrintOptions.PaperOrientation = PaperOrientation.Landscape;
                orpt.PrintOptions.PaperSize = PaperSize.PaperA4;
            }

            DiskFileDstOptions.DiskFileName = strExportFile;
            ExportOptions = orpt.ExportOptions;
            ExportOptions.DestinationOptions = DiskFileDstOptions;
            ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;

            switch (strFormatType)
            {
                case ".pdf":
                    ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    ExportOptions.FormatOptions = new PdfRtfWordFormatOptions();

                    break;
                case ".doc":
                    ExportOptions.ExportFormatType = ExportFormatType.WordForWindows;
                    ExportOptions.FormatOptions = new PdfRtfWordFormatOptions();
                    break;
                case ".rtf":
                    ExportOptions.ExportFormatType = ExportFormatType.RichText;
                    ExportOptions.FormatOptions = new PdfRtfWordFormatOptions();
                    break;
                case ".xls":

                    ExportOptions.ExportFormatType = ExportFormatType.Excel;

                    //BY X MAY 30 08
                    ExcelFormatOptions objExcelOptions = new ExcelFormatOptions();
                    ExportOptions.FormatOptions = objExcelOptions;
                    break;

                default:
                    ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    ExportOptions.FormatOptions = new PdfRtfWordFormatOptions();
                    break;
            }

            orpt.Export();
            orpt.Close();
            orpt.Dispose();

            Response.ClearContent();
            Response.ClearHeaders();

            if (strFormatType == ".pdf")
            {
                Response.ContentType = "application/pdf";
            }
            Response.WriteFile(strExportFile);
            Response.Flush();
            //Response.End();
            
            // Save pdf file
            string f = null;
            string s = null;
            string invoiceNo = "";
            string PDFFilename = "";
            
            if (strFormid == "Customs_Invoice")
            {
                f = System.Configuration.ConfigurationManager.AppSettings["FirstPrintedInvoicePDFFolder"];
                invoiceNo = Session["SESSION_DS_CUSTOMS_INVOICE_NO"].ToString();
                if (f != null && f != "")
                {
                    // Key exists		
                    PDFFilename = f + "\\" + invoiceNo + ".pdf";
                    if (!System.IO.File.Exists(PDFFilename)) // Not exists on First Printed folder
                    {
                        System.IO.File.Copy(strExportFile, PDFFilename, true); // Copy the pdf file to the original folder
                        s = System.Configuration.ConfigurationManager.AppSettings["CustomsInvoicePDFFolder"];
                        if (s != null && s != "")
                        {
                            // Key exists		
                            PDFFilename = s + "\\" + invoiceNo + ".pdf";
                            System.IO.File.Copy(strExportFile, PDFFilename, true); // Copy the pdf file to the Customs Invoice folder
                        }
                    }                   
                    else
                    {
                        s = System.Configuration.ConfigurationManager.AppSettings["AmemdedCustomsInvoicePDFFolder"];
                        if (s != null && s != "")
                        {
                            // Key exists		
                            PDFFilename = s + "\\" + invoiceNo + ".pdf";
                            System.IO.File.Copy(strExportFile, PDFFilename, true); // Copy the pdf file to the Amended Customs Invoice folder
                        }
                    }
                }
            }
            /*
            else if (strFormid == "RePrintConsNotes")
            {
                DataSet dsCheckedPrintConnotes = (DataSet)Session["SESSION_DS_PRINTCONSNOTES"];
                int iTot = dsCheckedPrintConnotes.Tables[1].Rows.Count;
                for (int i = 0; i < iTot; i++)
                {
                    f = System.Configuration.ConfigurationManager.AppSettings["FirstPrintedInvoicePDFFolder"];
                    invoiceNo = dsCheckedPrintConnotes.Tables[1].Rows[i]["consignment_no"].ToString();
                    if (f != null && f != "")
                    {
                        // Key exists		
                        PDFFilename = f + "\\" + invoiceNo + ".pdf";
                        if (!System.IO.File.Exists(PDFFilename)) // Not exists on First Printed folder
                        {
                            System.IO.File.Copy(strExportFile, PDFFilename, true); // Copy the pdf file to the original folder
                            s = System.Configuration.ConfigurationManager.AppSettings["DomesticInvoicePDFFolder"];
                            if (s != null && s != "")
                            {
                                // Key exists		
                                PDFFilename = s + "\\" + invoiceNo + ".pdf";
                                System.IO.File.Copy(strExportFile, PDFFilename, true); // Copy the pdf file to the Domestic Invoice folder
                            }
                        }
                        else
                        {
                            s = System.Configuration.ConfigurationManager.AppSettings["AmendedDomesticInvoicePDFFolder"];
                            if (s != null && s != "")
                            {
                                // Key exists		
                                PDFFilename = s + "\\" + invoiceNo + ".pdf";
                                System.IO.File.Copy(strExportFile, PDFFilename, true); // Copy the pdf file to the Amended Domestic Invoice folder
                            }
                        }
                    }
                }
            }
            */
            System.IO.File.Delete(strExportFile);

            //Response.Close();
        }

        private void BindReportOnePage(string invoiceNo)
        {
            ExportOptions ExportOptions = new ExportOptions();
            DiskFileDestinationOptions DiskFileDstOptions = new DiskFileDestinationOptions();
            String strExportFile = null;

            if (strFormatType == "")
            {
                return;
            }

            string strExportFolder = System.Configuration.ConfigurationManager.AppSettings["ReportExportFolder"];
            strExportFolder = Server.MapPath(strExportFolder);
            if (!System.IO.Directory.Exists(strExportFolder))
            {
                lblErrorMesssage.Text = "Export folder not found. Please create the folder :'" + strExportFolder + "' and export";
                return;
            }

            ///************IMPORTANT:Get the Export File directory from Web.config, Permissions must be given for
            ///************"aspnet" user on the Folder read from "ReportExportFolder"
            strExportFile = strExportFolder + "\\" + Session.SessionID.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();

            //			strExportFile = "C:\\" + Session.SessionID.ToString();

            string strFormid = (String)Session["FORMID"];

            DiskFileDstOptions.DiskFileName = strExportFile;
            ExportOptions = orpt.ExportOptions;
            ExportOptions.DestinationOptions = DiskFileDstOptions;
            ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;

            switch (strFormatType)
            {
                case ".pdf":
                    ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    ExportOptions.FormatOptions = new PdfRtfWordFormatOptions();

                    break;
                case ".doc":
                    ExportOptions.ExportFormatType = ExportFormatType.WordForWindows;
                    ExportOptions.FormatOptions = new PdfRtfWordFormatOptions();
                    break;
                case ".rtf":
                    ExportOptions.ExportFormatType = ExportFormatType.RichText;
                    ExportOptions.FormatOptions = new PdfRtfWordFormatOptions();
                    break;
                case ".xls":

                    ExportOptions.ExportFormatType = ExportFormatType.Excel;

                    //BY X MAY 30 08
                    ExcelFormatOptions objExcelOptions = new ExcelFormatOptions();
                    ExportOptions.FormatOptions = objExcelOptions;
                    break;

                default:
                    ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    ExportOptions.FormatOptions = new PdfRtfWordFormatOptions();
                    break;
            }

            orpt.Export();

            // Save pdf file
            string f = null;
            string s = null;
            string PDFFilename = "";

            f = System.Configuration.ConfigurationManager.AppSettings["FirstPrintedInvoicePDFFolder"];
            if (f != null && f != "")
            {
                // Key exists		
                PDFFilename = f + "\\" + invoiceNo + ".pdf";
                if (!System.IO.File.Exists(PDFFilename)) // Not exists on First Printed folder
                {
                    System.IO.File.Copy(strExportFile, PDFFilename, true); // Copy the pdf file to the original folder
                    if (strFormid == "Customs_Invoice_InvoicedPrinting")
                    {
                        s = System.Configuration.ConfigurationManager.AppSettings["CustomsInvoicePDFFolder"];
                    }
                    else
                    {
                        s = System.Configuration.ConfigurationManager.AppSettings["DomesticInvoicePDFFolder"];
                    }
                    
                    if (s != null && s != "")
                    {
                        // Key exists		
                        PDFFilename = s + "\\" + invoiceNo + ".pdf";
                        System.IO.File.Copy(strExportFile, PDFFilename, true); // Copy the pdf file to the Domestic Invoice folder
                    }
                }
                else
                {
                    
                    if (strFormid == "Customs_Invoice_InvoicedPrinting")
                    {
                        s = System.Configuration.ConfigurationManager.AppSettings["AmemdedCustomsInvoicePDFFolder"];
                    }
                    else
                    {
                        s = System.Configuration.ConfigurationManager.AppSettings["AmendedDomesticInvoicePDFFolder"];
                    }
                    if (s != null && s != "")
                    {
                        // Key exists		
                        PDFFilename = s + "\\" + invoiceNo + ".pdf";
                        System.IO.File.Copy(strExportFile, PDFFilename, true); // Copy the pdf file to the Amended Domestic Invoice folder
                    }
                }
            }

            System.IO.File.Delete(strExportFile);

            //Response.Close();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            base.OnInit(e);
            InitializeComponent();
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion



    }
}
