using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.applicationpages;
using com.common.util;
using com.common.RBAC;
using TIESClasses;
using TIESDAL;


namespace TIES.WebUI
{
	public class CurrencyExchangeRates : BasePage
	{
		#region web controls variable
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnQry;
		protected System.Web.UI.WebControls.Button btnExecQry;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMain;
		protected System.Web.UI.HtmlControls.HtmlTable MainTable;
		protected System.Web.UI.WebControls.Button btnPrintConsNote;
		protected System.Web.UI.WebControls.Label lblError;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Button btnV3;
		protected System.Web.UI.WebControls.Button btnV2;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.Button Button2;
		protected System.Web.UI.WebControls.DropDownList ddlExchangeRateType;
		protected System.Web.UI.WebControls.DataGrid gridCurrency;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		#endregion
	
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			base.OnInit(e);
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQry.Click += new System.EventHandler(this.btnQry_Click);
			this.btnExecQry.Click += new System.EventHandler(this.btnExecQry_Click);
			this.ddlExchangeRateType.SelectedIndexChanged += new System.EventHandler(this.ddlExchangeRateType_SelectedIndexChanged);
			this.gridCurrency.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.gridCurrency_ItemCommand);
			this.gridCurrency.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.gridCurrency_PageIndexChanged);
			this.gridCurrency.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.gridCurrency_ItemDataBound);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion		
		
		private string appID = string.Empty;
		private string enterpriseID = string.Empty;
		private string userLoggin = string.Empty;
		public string GridCmd
		{
			get
			{
				if(ViewState["GridCmd"] == null)
				{
					return "";
				}
				else
				{
					return (string)ViewState["GridCmd"];
				}
			}
			set
			{
				ViewState["GridCmd"]=value;
			}
		}

		public DataSet dsExchangeRates
		{
			get
			{
				if(ViewState["dtExchangeRates"] == null)
					return new DataSet();
				else
					return (DataSet)ViewState["dtExchangeRates"];
			}
			set
			{
				ViewState["dtExchangeRates"] = value;
			} 
		}

		private void InitLoad()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("Currency");
			dt.Columns.Add("Date");
			dt.Columns.Add("Rate");

			dt.AcceptChanges();

			gridCurrency.EditItemIndex = -1;
			gridCurrency.CurrentPageIndex = 0;

			gridCurrency.DataSource = dt;
			gridCurrency.DataBind();

//			btnQry.Enabled = true;
//			btnExecQry.Enabled = true;
		}
		
		private void InitGridPage()
		{
			CurrencyExchangeRatesDAL db = new CurrencyExchangeRatesDAL(appID, 
																	   enterpriseID);
			gridCurrency.PageSize = int.Parse(db.GetEnterpriseConfigurations(enterpriseID));
		}

		private void DisplayddlExchangeRateType()
		{
			CurrencyExchangeRatesDAL db = new CurrencyExchangeRatesDAL(appID, 
																	   enterpriseID);
			TIESClasses.CurrencyExchangeRates objInfo = new TIESClasses.CurrencyExchangeRates(0,
																							  enterpriseID,
																							  userLoggin,
																							  string.Empty,
																							  string.Empty);
			ddlExchangeRateType.DataSource = db.GetCurrencyExchangeRates(objInfo).Tables[1];
			ddlExchangeRateType.DataTextField = "DropDownListDisplayValue";
			ddlExchangeRateType.DataValueField = "CodedValue";
			ddlExchangeRateType.DataBind();
		}

		private void DisplayGridExchangeRate()
		{	
			//DropDownList ddlCurrency = (DropDownList)gridCurrency.Controls[0].Controls[gridCurrency.Controls[0].Controls.Count - 1].FindControl("ddlCurrency");
			//DropDownList ddlCurrency = (DropDownList)gridCurrency.FindControl("ddlCurrency");
			//com.common.util.msTextBox mtxtDate = (com.common.util.msTextBox)gridCurrency.FindControl("mtxtDate");
			//DropDownList ddlCurrency = (DropDownList)gridCurrency.Controls[gridCurrency.Controls.Count - 1].FindControl("ddlCurrency");
			
			DropDownList ddlCurrency = (DropDownList)gridCurrency.Controls[0].Controls[gridCurrency.Controls[0].Controls.Count - 2].FindControl("ddlCurrency");
			com.common.util.msTextBox txtDate = (com.common.util.msTextBox)gridCurrency.Controls[0].Controls[gridCurrency.Controls[0].Controls.Count - 2].FindControl("mtxtDate");
			
			string rateDate = txtDate.Text.Trim();
			if(!(ddlCurrency.SelectedIndex > 0) && txtDate.Text.Equals(""))
			{
				this.lblError.Text = "Enter either Currency or Rate Date to perform a search";
				return;
			}
			if(!txtDate.Text.Equals(""))
				rateDate = DateTime.ParseExact(rateDate, "dd/MM/yyyy", null).ToString("yyyy-MM-dd");

			//gridCurrency.AllowPaging = true;

			//DataSet dsReturn = new DataSet();
			CurrencyExchangeRatesDAL db = new CurrencyExchangeRatesDAL(appID, 
																	   enterpriseID);
			TIESClasses.CurrencyExchangeRates objInfo = new TIESClasses.CurrencyExchangeRates(1,
																							  enterpriseID,
																							  userLoggin,
																							  ddlExchangeRateType.SelectedValue,
																							  ddlCurrency.SelectedItem.Text,
																							  rateDate);
			dsExchangeRates = db.FitterCurrencyExchangeRates(objInfo);
			//ViewState["dtExchangeRates"] = dsReturn;

			if(dsExchangeRates.Tables[0].Rows[0][0].ToString().Equals("0"))
			{
				if(dsExchangeRates.Tables[1].Rows.Count > 0)
				{
					gridCurrency.DataSource = dsExchangeRates.Tables[1];
					gridCurrency.DataBind();
				}
				else
				{
					this.InitLoad();
					DropDownList ddlCurrencyNew = (DropDownList)gridCurrency.Controls[0].Controls[gridCurrency.Controls[0].Controls.Count - 2].FindControl("ddlCurrency");
					ddlCurrencyNew.SelectedIndex = ddlCurrency.SelectedIndex;
				}
				lblError.Text = dsExchangeRates.Tables[0].Rows[0][1].ToString();
			}
			else
			{
				this.InitLoad();
				DropDownList ddlCurrencyNew = (DropDownList)gridCurrency.Controls[0].Controls[gridCurrency.Controls[0].Controls.Count - 2].FindControl("ddlCurrency");
				ddlCurrencyNew.SelectedIndex = ddlCurrency.SelectedIndex;
				lblError.Text = dsExchangeRates.Tables[0].Rows[0][1].ToString();
			}
		}

		private void ViewStateExchangeRate()
		{
			gridCurrency.DataSource = dsExchangeRates.Tables[1];
			gridCurrency.DataBind();
		}

		public static void SetInitialFocus(Control control) 
		{ 
			if (control.Page == null) 
			{ 
				throw new ArgumentException( 
					"The Control must be added to a Page before you can set the IntialFocus to it."); 
			} 
			if (control.Page.Request.Browser.JavaScript == true) 
			{ 
				// Create JavaScript 
				System.Text.StringBuilder s = new System.Text.StringBuilder(); 
				s.Append("\n<SCRIPT LANGUAGE='JavaScript'>\n"); 
				s.Append("<!--\n"); 
				s.Append("function SetInitialFocus()\n"); 
				s.Append("{\n"); 
				s.Append("   document."); 

				// Find the Form 
				Control p = control.Parent; 
				while (!(p is System.Web.UI.HtmlControls.HtmlForm)) 
					p = p.Parent; 
				s.Append(p.ClientID); 

				s.Append("['"); 
				s.Append(control.ClientID); 
				s.Append("'].focus();\n"); 
				s.Append("SetScrollPosition();\n"); 
				s.Append("}\n"); 
				s.Append("// -->\n"); 
				s.Append("window.onload = SetInitialFocus;\n"); 
				s.Append("</SCRIPT>"); 

				// Register Client Script 
				control.Page.RegisterClientScriptBlock("InitialFocus", s.ToString()); 
			} 
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings, Page.Session);
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userLoggin = utility.GetUserID();

			if(!Page.IsPostBack)
			{
				GridCmd=null;
				InitGridPage();
				InitLoad();
				DisplayddlExchangeRateType();
				SetInitialFocus(ddlExchangeRateType);
			}
		}
		
		private void btnExecQry_Click(object sender, System.EventArgs e)
		{
			GridCmd=null;
			lblError.Text = string.Empty;

//			if(ddlExchangeRateType.SelectedIndex != 0)
//			{
				//int cc =gridCurrency.Controls[0].Controls.Count;
				

				DisplayGridExchangeRate();
//				ddlExchangeRateType.Enabled = false;
//				btnQry.Enabled = true;
//				btnExecQry.Enabled = false;
//			}
//			else
//			{
//				lblError.Text = "Exchange rate type is required";
//				InitLoad();
//			}
		}

		private void btnQry_Click(object sender, System.EventArgs e)
		{
			GridCmd=null;
			InitLoad();

			lblError.Text = string.Empty;

			ddlExchangeRateType.SelectedIndex = 0;
			SetInitialFocus(ddlExchangeRateType);

//			btnQry.Enabled = false;
//			btnExecQry.Enabled = true;
//			ddlExchangeRateType.Enabled = true;
		}

		private void gridCurrency_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if((e.Item.ItemType == ListItemType.Footer))
			{
				DropDownList ddlCurrency = (DropDownList)e.Item.FindControl("ddlCurrency");
				if (ddlCurrency != null)
				{
					CurrencyExchangeRatesDAL db = new CurrencyExchangeRatesDAL(appID, 
																			   enterpriseID);
					ddlCurrency.DataSource = db.GetCurrencies(enterpriseID).Tables[0];
					ddlCurrency.DataTextField = "CurrencyCode";
					ddlCurrency.DataValueField = "CurrencyCode";
					ddlCurrency.DataBind();

					if(GridCmd =="ADD_ITEM")
					{
						GridCmd=null;
						SetInitialFocus(ddlCurrency);
					}
				}
			}
			else if((e.Item.ItemType == ListItemType.EditItem))
			{
				DataRowView drItem = (DataRowView)e.Item.DataItem;
				DropDownList ddlCurrencyEdit = (DropDownList)e.Item.FindControl("ddlCurrencyEdit");
				if (ddlCurrencyEdit != null)
				{
					CurrencyExchangeRatesDAL db = new CurrencyExchangeRatesDAL(appID, 
																			   enterpriseID);
					ddlCurrencyEdit.DataSource = db.GetCurrencies(enterpriseID).Tables[0];
					ddlCurrencyEdit.DataTextField = "CurrencyCode";
					ddlCurrencyEdit.DataValueField = "CurrencyCode";
					ddlCurrencyEdit.DataBind();
					ddlCurrencyEdit.SelectedValue = drItem["CurrencyCode"].ToString();

					msTextBox txtExchangeRateEdit = (msTextBox)e.Item.FindControl("txtExchangeRateEdit");
					SetInitialFocus(txtExchangeRateEdit);
				}
			}
		}

		private void gridCurrency_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
		{			
			gridCurrency.CurrentPageIndex = e.NewPageIndex;
			ViewState["CurrentPageIndex"] = e.NewPageIndex;
			ViewStateExchangeRate();
		}

		private void gridCurrency_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
		{
			string command = e.CommandName;

			lblError.Text = string.Empty;
			GridCmd=e.CommandName;
			if(command.Equals("ADD_ITEM"))
			{
				#region ADD_ITEM
				DataSet dsReturn = new DataSet();

				DropDownList ddlCurrency = (DropDownList)e.Item.FindControl("ddlCurrency");
				com.common.util.msTextBox mtxtDate = (com.common.util.msTextBox)e.Item.FindControl("mtxtDate");
				com.common.util.msTextBox txtExchangeRate = (com.common.util.msTextBox)e.Item.FindControl("txtExchangeRate");

				//if(ddlExchangeRateType.SelectedIndex <= 0)
				//{
				//	lblError.Text = "Exchange rate type is required";
				//	return;
				//}
				//if(ddlCurrency.SelectedIndex <= 0)
				//{
				//	lblError.Text = "Currency code is required";
				//	return;
				//}
				string rateDate = string.Empty;
				if(!mtxtDate.Text.Trim().Equals(""))
					rateDate = DateTime.ParseExact(mtxtDate.Text, "dd/MM/yyyy", null).ToString("yyyy-MM-dd");
				//if(txtExchangeRate.Text.Trim().Equals(""))
				//{
				//	lblError.Text = "Exchange rate is required";
				//	return;
				//}

				TIESClasses.CurrencyExchangeRates objInfo = new TIESClasses.CurrencyExchangeRates(2,
																									enterpriseID,
																									userLoggin,
																									ddlExchangeRateType.SelectedValue,
																									ddlCurrency.SelectedItem.Text.Trim(),
																									rateDate,
																									txtExchangeRate.Text.Trim());
				CurrencyExchangeRatesDAL db = new CurrencyExchangeRatesDAL(appID, 
					enterpriseID);
				dsReturn = db.InsupdCurrencyExchangeRates(objInfo);
				
				if(dsReturn.Tables[0].Rows[0][0].ToString().Equals("0"))
				{
					gridCurrency.CurrentPageIndex = 0;
					lblError.Text = dsReturn.Tables[0].Rows[0][1].ToString();
					gridCurrency.DataSource = dsReturn.Tables[1];
					gridCurrency.DataBind();

					dsExchangeRates = dsReturn;
				}
				else
					lblError.Text = dsReturn.Tables[0].Rows[0][1].ToString();
				#endregion
			}
			else if(command.Equals("EDIT_ITEM"))
			{ 
				gridCurrency.EditItemIndex = e.Item.ItemIndex; 

				gridCurrency.DataSource = dsExchangeRates.Tables[1];
				gridCurrency.DataBind();

				//com.common.util.msTextBox txtExchangeRateEdit = (com.common.util.msTextBox)e.Item.FindControl("txtExchangeRateEdit");
				//SetInitialFocus(txtExchangeRateEdit);
			} 
			else if(command.Equals("SAVE_ITEM"))
			{
				#region SAVE_ITEM
				DataSet dsReturn = new DataSet();

				DropDownList ddlCurrencyEdit = (DropDownList)e.Item.FindControl("ddlCurrencyEdit");
				com.common.util.msTextBox mtxtDateEdit = (com.common.util.msTextBox)e.Item.FindControl("mtxtDateEdit");
				com.common.util.msTextBox txtExchangeRateEdit = (com.common.util.msTextBox)e.Item.FindControl("txtExchangeRateEdit");

				TIESClasses.CurrencyExchangeRates objInfo = new TIESClasses.CurrencyExchangeRates(2,
					enterpriseID,
					userLoggin,
					ddlExchangeRateType.SelectedValue,
					ddlCurrencyEdit.SelectedItem.Text,
					DateTime.ParseExact(mtxtDateEdit.Text, "dd/MM/yyyy", null).ToString("yyyy-MM-dd"),
					txtExchangeRateEdit.Text.Replace(",", "").Trim());
				CurrencyExchangeRatesDAL db = new CurrencyExchangeRatesDAL(appID, 
																		   enterpriseID);
				dsReturn = db.InsupdCurrencyExchangeRates(objInfo);
				
				if(dsReturn.Tables[0].Rows[0][0].ToString().Equals("0"))
				{					
					lblError.Text = dsReturn.Tables[0].Rows[0][1].ToString();

					gridCurrency.EditItemIndex = -1;
					
					if(dsReturn.Tables[1].Rows.Count > 0)
					{
						gridCurrency.CurrentPageIndex = 0;
						gridCurrency.DataSource = dsReturn.Tables[1];
						gridCurrency.DataBind();
					}

					dsExchangeRates = dsReturn;
				}
				else
					lblError.Text = dsReturn.Tables[0].Rows[0][1].ToString();

				#endregion
			}
			else if(command.Equals("CANCEL_ITEM"))
			{
				gridCurrency.EditItemIndex = -1;
				ViewStateExchangeRate();
			}
		}

		private void ddlExchangeRateType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			SetInitialFocus(ddlExchangeRateType);
		}
	}
}
