<%@ Import Namespace="com.common.util" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="VASZipcodeExcluded.aspx.cs" AutoEventWireup="false" Inherits="com.ties.VASZipcodeExcluded" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>VASZipcodeExcluded</title>
		<LINK rel="stylesheet" type="text/css" href="css/Styles.css" name="stylesheet">
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript">
		
		/*
		function RemoveBadMasterAWBNumber(strTemp, obj) {
            strTemp = strTemp.replace(/[;a-zA-Z0-9]/g, ''); //replace non-numeric
            obj.value = strTemp.replace(/\s/g, ''); // replace space
        }
                                
        function AfterPasteMasterAWBNumber(obj) {
            setTimeout(function () {
                RemoveBadMasterAWBNumber(obj.value, obj);
            }, 1); //or 4
        }
		*/

        function ValidateExcludedServiceTypes(evt) {
            var theEvent = evt || window.event;
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
            var regex = /[;a-zA-Z0-9]/;;
            if (!regex.test(key)) {
                theEvent.returnValue = false;
                if (theEvent.preventDefault) theEvent.preventDefault();
            }
        }
		
		function CheckToDate(aText)
		{	
			if (aText.value !="")
			{	
				var myDays=["Sun","Mon","Tue","Wed","Thur","Fri","Sat","Sun"];
				var dt=aText.value;							
				var d=dt.substring(3,5)+'/'+dt.substring(0,2)+'/'+dt.substring(6,10);
				//alert(d);
				myDate=new Date(eval('"'+d+'"'));		
				var day =myDays[myDate.getDay()];		
				var Ctlname=aText.name;		
				var arr=Ctlname.split(':')		
				var actualCtl=eval(arr[0]+'_'+arr[1]+'_lblEffectiveDate');		
				actualCtl.innerHTML=day;
			}
			else
			{
				var Ctlname=aText.name;		
				var arr=Ctlname.split(':')		
				var actualCtl=eval(arr[0]+'_'+arr[1]+'_lblEffectiveDate');		
				actualCtl.innerHTML="";
			}
		}
		//-->
		</script>
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<FORM id="VASZipcodeExcluded" method="post" runat="server">
			<asp:button style="Z-INDEX: 104; POSITION: absolute; TOP: 43px; LEFT: 92px" id="btnExecQry"
				runat="server" Enabled="False" CssClass="queryButton" Text="Execute Query" CausesValidation="False"
				Width="130px"></asp:button><asp:label style="Z-INDEX: 113; POSITION: absolute; TOP: 6px; LEFT: 31px" id="lblTitle" runat="server"
				CssClass="mainTitleSize" Width="436px" Height="26px"> Surcharges</asp:label><asp:button style="Z-INDEX: 101; POSITION: absolute; TOP: 43px; LEFT: 221px" id="btnInsert"
				runat="server" CssClass="queryButton" Text="Insert" CausesValidation="False" Width="62px"></asp:button><asp:button style="Z-INDEX: 102; POSITION: absolute; TOP: 43px; LEFT: 283px" id="btnSave" runat="server"
				CssClass="queryButton" Text="save" CausesValidation="True" Width="66px" Visible="False"></asp:button><asp:button style="Z-INDEX: 103; POSITION: absolute; TOP: 43px; LEFT: 349px" id="btnDelete"
				runat="server" CssClass="queryButton" Text="Delete" CausesValidation="False" Width="62px" Visible="False"></asp:button><asp:button style="Z-INDEX: 105; POSITION: absolute; TOP: 43px; LEFT: 31px" id="btnQry" runat="server"
				CssClass="queryButton" Text="Query" CausesValidation="False" Width="62px"></asp:button>
			<table style="Z-INDEX: 100; POSITION: absolute; TOP: 100px; LEFT: 30px">
				<tr>
					<td><asp:datagrid id="dgVAS" runat="server" Width="800px" PageSize="25" SelectedItemStyle-CssClass="gridFieldSelected"
							OnDeleteCommand="dgVAS_Delete" AllowCustomPaging="True" AllowPaging="True" OnSelectedIndexChanged="dgVAS_SelectedIndexChanged"
							OnUpdateCommand="dgVAS_Update" OnCancelCommand="dgVAS_Cancel" OnEditCommand="dgVAS_Edit" OnItemDataBound="dgVAS_Bound"
							OnPageIndexChanged="dgVAS_PageChange" AutoGenerateColumns="False" ItemStyle-Height="20">
							<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
							<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
							<Columns>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-select.gif' border=0 title='Select' &gt;" CommandName="Select">
									<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:ButtonColumn>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
									CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;">
									<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Left"></ItemStyle>
								</asp:EditCommandColumn>
								<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
									<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
								</asp:ButtonColumn>
								<asp:TemplateColumn HeaderText="Surcharge Code">
									<HeaderStyle Font-Bold="True" Width="19%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblVASCode" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBox" ID="txtVASCode" Text='<%#DataBinder.Eval(Container.DataItem,"vas_code")%>' Runat="server" Enabled="True" MaxLength="12" TextMaskType="msUpperAlfaNumericWithUnderscore">
										</cc1:mstextbox>
										<asp:RequiredFieldValidator ID="scValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtVASCode"
											ErrorMessage="Surcharge Code is required field.<br>"></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Surcharge Description">
									<HeaderStyle Width="60%" CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblVASDescription" Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>' Runat="server" Enabled="True">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox CssClass="gridTextBox" ID="txtVASDescription" Text='<%#DataBinder.Eval(Container.DataItem,"vas_description")%>' Runat="server" MaxLength="200">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Surcharge Amount">
									<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
									<ItemStyle Wrap="False"></ItemStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabelNumber" ID="lblSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>' Runat="server">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBoxNumber" ID="txtSurcharge" Text='<%#DataBinder.Eval(Container.DataItem,"surcharge","{0:n}")%>' Runat="server" Enabled="True" MaxLength="9" TextMaskType="msNumeric" NumberMaxValue="999999" NumberMinValue="0" NumberPrecision="8" NumberScale="2">
										</cc1:mstextbox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Allow for Customer">
									<HeaderStyle Width="23%" CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:DropDownList id="ddlAllowForCust" runat="server" CssClass="gridTextBox"></asp:DropDownList>
										<%--<asp:RequiredFieldValidator id="ValidatorAllowCust" runat="server" ErrorMessage="Allow for Customer flag must be Yes or No.<br>" ControlToValidate="ddlAllowForCust" Display="None"></asp:RequiredFieldValidator>--%>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Effective Date<br />(dd/mm/yyyy)">
									<HeaderStyle Width="100px" CssClass="gridHeading" Wrap="False"></HeaderStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblEffectiveDate" Text='<%#DataBinder.Eval(Container.DataItem,"effective_date")%>' Runat="server" Enabled="True">
										</asp:Label>
										<asp:Label CssClass="gridLabel" ID="lblEffectiveDateHide" Text='<%#DataBinder.Eval(Container.DataItem,"effective_date")%>' Runat="server" Enabled="True" Visible="False">
										</asp:Label>
									</ItemTemplate>
									<ItemStyle Wrap="False"></ItemStyle>
									<EditItemTemplate>
										<cc1:mstextbox CssClass="gridTextBox" ID="txtEffectiveDate" Visible=True Text='<%#DataBinder.Eval(Container.DataItem,"effective_date","{0:dd/MM/yyyy}")%>' Runat="server" MaxLength="10" TextMaskString="99/99/9999" TextMaskType="msDate" AutoPostBack="false" onblur="return CheckToDate(this)">
										</cc1:mstextbox>
										<cc1:mstextbox CssClass="gridTextBox" ID="txtEffectiveDateHide" Visible=false Text='<%#DataBinder.Eval(Container.DataItem,"effective_date")%>' Runat="server" MaxLength="10" TextMaskType="msDate" AutoPostBack="false">
										</cc1:mstextbox>
										<asp:RequiredFieldValidator ID="rcDateTo" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtEffectiveDate"
											ErrorMessage="Effective Date is required field."></asp:RequiredFieldValidator>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Amount=0 for Service Types">
									<HeaderStyle Width="100px" CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:Label CssClass="gridLabel" ID="lblExcludedServiceTypes" Text='<%#DataBinder.Eval(Container.DataItem,"ExcludedServiceTypes")%>' Runat="server" Enabled="True">
										</asp:Label>
										<asp:Label CssClass="gridLabel" ID="lblExcludedServiceTypesTemp" Text='<%#DataBinder.Eval(Container.DataItem,"ExcludedServiceTypes")%>' Runat="server" Visible="False" Enabled="True">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox CssClass="gridTextBox" ID="txtExcludedServiceTypes" Text='<%#DataBinder.Eval(Container.DataItem,"ExcludedServiceTypes")%>' onkeypress="ValidateExcludedServiceTypes(event)" Runat="server" MaxLength="200">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Value Added?">
									<HeaderStyle Width="30px" CssClass="gridHeading"></HeaderStyle>
									<ItemTemplate>
										<asp:DropDownList id="ddlValueAdded" runat="server" CssClass="gridTextBox"></asp:DropDownList>
										<%--<asp:RequiredFieldValidator id="ValidatorAllowCust" runat="server" ErrorMessage="Allow for Customer flag must be Yes or No.<br>" ControlToValidate="ddlAllowForCust" Display="None"></asp:RequiredFieldValidator>--%>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Left" CssClass="normalText"></PagerStyle>
						</asp:datagrid></td>
				</tr>
				<%--<asp:ValidationSummary ID="sumValidateVas" DisplayMode="BulletList" ShowMessageBox="True" Runat="server" ></asp:ValidationSummary>--%>
				<tr>
					<td><asp:button id="btnZVASEInsert" runat="server" Enabled="False" CssClass="queryButton" Text="Insert"
							CausesValidation="False" Width="62px"></asp:button><asp:button id="btnZVASEInsertMultiple" runat="server" Enabled="False" CssClass="queryButton"
							Text="Insert Multiple" CausesValidation="False" Width="128px" Visible="False"></asp:button></td>
				</tr>
				<tr>
					<td><asp:label id="lblZVASEMessage" runat="server" CssClass="errorMsgColor" Width="490px" Height="28px"></asp:label></td>
				</tr>
				<tr>
					<td><asp:panel id="pnlSecondaryGrid" runat="server">
							<FIELDSET style="WIDTH: 592px; HEIGHT: 178px"><LEGEND>
									<asp:label id="Label1" runat="server" CssClass="tableField">VAS Unavailable Postal Codes</asp:label></LEGEND>
								<asp:datagrid id="dgZipcodeVASExcluded" runat="server" Width="587px" PageSize="10" OnDeleteCommand="dgZipcodeVASExcluded_Delete"
									AllowCustomPaging="True" AllowPaging="True" OnUpdateCommand="dgZipcodeVASExcluded_Update"
									OnCancelCommand="dgZipcodeVASExcluded_Cancel" OnEditCommand="dgZipcodeVASExcluded_Edit" OnItemDataBound="dgZipcodeVASExcluded_Bound"
									OnPageIndexChanged="dgZipcodeVASExcluded_PageChange" AutoGenerateColumns="False" ItemStyle-Height="20"
									OnItemCommand="dgZipcodeVASExcluded_Button">
									<ItemStyle Height="20px"></ItemStyle>
									<Columns>
										<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Update' &gt;"
											CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Edit' &gt;"
											Visible="False">
											<HeaderStyle Width="4%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
										</asp:EditCommandColumn>
										<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" ButtonType="LinkButton"
											CommandName="Delete">
											<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
										</asp:ButtonColumn>
										<asp:TemplateColumn HeaderText="Postal Code">
											<HeaderStyle Width="30%" Font-Bold="True" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabel" ID="lblZipcode" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Runat="server">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<cc1:mstextbox CssClass="gridTextBox" ID="txtZipcode" Text='<%#DataBinder.Eval(Container.DataItem,"zipcode")%>' Runat="server" Enabled="True" MaxLength="10" TextMaskType="msUpperAlfaNumericWithUnderscore">
												</cc1:mstextbox>
												<asp:RequiredFieldValidator ID="exValidator" Runat="server" BorderWidth="0" Display="None" ControlToValidate="txtZipcode"
													ErrorMessage="Zipcode is required field"></asp:RequiredFieldValidator>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-search.gif' border=0 title='Search' &gt;" ButtonType="LinkButton"
											CommandName="search" Visible="False">
											<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle HorizontalAlign="Center" CssClass="gridField"></ItemStyle>
										</asp:ButtonColumn>
										<asp:TemplateColumn HeaderText="State">
											<HeaderStyle Width="30%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabel" ID="lblState" Text='<%#DataBinder.Eval(Container.DataItem,"state_name")%>' Runat="server" >
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:TextBox CssClass="gridTextBox" ID="txtState" Text='<%#DataBinder.Eval(Container.DataItem,"state_name")%>' Runat="server" ReadOnly="true">
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="Country">
											<HeaderStyle Width="30%" CssClass="gridHeading"></HeaderStyle>
											<ItemStyle CssClass="gridField"></ItemStyle>
											<ItemTemplate>
												<asp:Label CssClass="gridLabel" ID="lblCountry" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server" >
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:TextBox CssClass="gridTextBox" ID="txtCountry" Text='<%#DataBinder.Eval(Container.DataItem,"country")%>' Runat="server" ReadOnly="true">
												</asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
								</asp:datagrid></FIELDSET>
						</asp:panel></td>
				</tr>
			</table>
			<asp:label style="Z-INDEX: 108; POSITION: absolute; TOP: 70px; LEFT: 31px" id="lblVASMessage"
				runat="server" CssClass="errorMsgColor" Width="483px" Height="26px"></asp:label><asp:validationsummary style="Z-INDEX: 99; POSITION: absolute; TOP: 76px; LEFT: 52px" id="ValidationSummary1"
				Width="444px" Height="23px" ShowSummary="False" DisplayMode="SingleParagraph" ShowMessageBox="True" Runat="server"></asp:validationsummary></FORM>
	</body>
</HTML>
