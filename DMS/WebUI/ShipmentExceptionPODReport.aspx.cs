using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.common.classes;
using com.common.util;
using com.ties.DAL;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for ShipmentExceptionPODReport.
	/// </summary>
	public class ShipmentExceptionPODReport : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Button btnQuery;
		protected System.Web.UI.WebControls.Label lblErrorMessage;
		protected System.Web.UI.WebControls.Button btnGenerate;
		protected System.Web.UI.WebControls.RadioButton rbMonth;
		protected System.Web.UI.WebControls.DropDownList ddMonth;
		protected System.Web.UI.WebControls.Label lblYear;
		protected com.common.util.msTextBox txtYear;
		protected System.Web.UI.WebControls.RadioButton rbPeriod;
		protected com.common.util.msTextBox txtPeriod;
		protected System.Web.UI.WebControls.Label lblTo;
		protected com.common.util.msTextBox txtTo;
		protected System.Web.UI.WebControls.RadioButton rbDate;
		protected com.common.util.msTextBox txtDate;
		protected System.Web.UI.WebControls.RadioButton rbSender;
		protected System.Web.UI.WebControls.RadioButton rbRecipent;
		protected System.Web.UI.WebControls.Label lblArea;
		protected com.common.util.msTextBox txtArea;
		protected System.Web.UI.WebControls.Label lblRegion;
		protected com.common.util.msTextBox txtRegion;
		protected System.Web.UI.WebControls.Label lblTitle;
		protected System.Web.UI.WebControls.Label lblPayer;
		protected com.common.util.msTextBox txtPayer;
		protected com.common.util.msTextBox txtPayerAc;
		protected System.Web.UI.WebControls.Label lblPayerAc;
		protected System.Web.UI.WebControls.Label lblOrigin;
		protected com.common.util.msTextBox txtOrigin;
		protected System.Web.UI.WebControls.Label lblDestination;
		protected com.common.util.msTextBox txtDestination;
		protected System.Web.UI.HtmlControls.HtmlTable tblShipmentExcepPODRepQry;
		Utility utility = null;
		private String m_strAppID;
		private String m_strEnterpriseID;
		private String m_strCulture;
		protected com.common.util.msTextBox txtRouteType;
		private DataView m_dvMonths;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			m_strAppID = utility.GetAppID();
			m_strEnterpriseID = utility.GetEnterpriseID();
			m_strCulture = utility.GetUserCulture();
			if(!Page.IsPostBack)
			{
				DefaultScreen();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
			this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
			this.rbMonth.CheckedChanged += new System.EventHandler(this.rbMonth_CheckedChanged);
			this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
			this.rbDate.CheckedChanged += new System.EventHandler(this.rbDate_CheckedChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnQuery_Click(object sender, System.EventArgs e)
		{
			DefaultScreen();
		}

		private void btnGenerate_Click(object sender, System.EventArgs e)
		{
			lblErrorMessage.Text = "";
			int intChk = 0;
			intChk = ValidateData();
			if(intChk < 1)
			{
				return;
			}
			//DataSet dsShipment = GetShipmentQueryData();
			String strUrl = null;
			SessionDS sessionds = new SessionDS();
			//sessionds = ShipmentTrackingMgrDAL.QueryShipmentTracking(dsShipment, m_strAppID, m_strEnterpriseID, 0, 10);
			strUrl = "Shipment_exception_Upon_POD_Report_BKK.htm";
			OpenWindowpage(strUrl);
		}

		private void rbMonth_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}

		private void rbPeriod_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = true;
			txtTo.Text = null;
			txtTo.Enabled = true;
			txtDate.Text = null;
			txtDate.Enabled = false;
		}

		private void rbDate_CheckedChanged(object sender, System.EventArgs e)
		{
			ddMonth.Enabled = false;
			txtYear.Text = null;
			txtYear.Enabled = false;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = true;
		}

		private void DefaultScreen()
		{
			rbMonth.Checked = true;
			rbPeriod.Checked = false;
			rbDate.Checked = false;
			ddMonth.Enabled = true;
			txtYear.Text = null;
			txtYear.Enabled = true;
			txtPeriod.Text = null;
			txtPeriod.Enabled = false;
			txtTo.Text = null;
			txtTo.Enabled = false;
			txtDate.Text = null;
			txtDate.Enabled = false;
			txtPayer.Text = null;
			txtPayerAc.Text = null;
			txtOrigin.Text = null;
			txtDestination.Text = null;
			rbSender.Checked = true;
			rbRecipent.Checked = false;
			txtArea.Text = null;
			txtRegion.Text = null;
			m_dvMonths = CreateMonths(true);
			BindMonths(m_dvMonths);
			lblErrorMessage.Text = "";
		}

		private DataView CreateMonths(bool showNilOption)
		{
			DataTable dtMonths = new DataTable();
			dtMonths.Columns.Add(new DataColumn("Text", typeof(string)));
			dtMonths.Columns.Add(new DataColumn("StringValue", typeof(string)));
			ArrayList listMonthTxt = Utility.GetCodeValues(m_strAppID,m_strCulture, "months", CodeValueType.StringValue);
			if(showNilOption)
			{
				DataRow drNilRow = dtMonths.NewRow();
				drNilRow[0] = "";
				drNilRow[1] = "";
				dtMonths.Rows.Add(drNilRow);
			}
			foreach(SystemCode typeSysCode in listMonthTxt)
			{
				DataRow drEach = dtMonths.NewRow();
				drEach[0] = typeSysCode.Text;
				drEach[1] = typeSysCode.StringValue;
				dtMonths.Rows.Add(drEach);
			}
			DataView dvMonths = new DataView(dtMonths);
			return dvMonths;
		}

		private void BindMonths(DataView dvMonths)
		{
			ddMonth.DataSource = dvMonths;
			ddMonth.DataTextField = "Text";
			ddMonth.DataValueField = "StringValue";
			ddMonth.DataBind();	
		}

		private void OpenWindowpage(String strUrl)
		{
			String strOpenWin = "<script language=javascript>";
			String url = strUrl;
			String features = "'height=570,width=950,left=20,top=20,location=no,menubar=no,scrollbars=yes,resizable=yes,status=yes,titlebar=no,toolbar=no'";
			strOpenWin += "window.open('" + url+"', '',"+features +");";
			strOpenWin += "</script>";
			Response.Write(strOpenWin);
		}
	
		private int ValidateData()
		{
			if (txtYear.Text != "" )
			{
				if (Convert.ToInt32(txtYear.Text) < 2000 || Convert.ToInt32(txtYear.Text) > 2099)
				{
					lblErrorMessage.Text = "Enter year between 2000 to 2099";
					return -1;
				}
			}
			return 1;
		}
	
	}
}
