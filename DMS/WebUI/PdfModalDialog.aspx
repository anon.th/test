<%@ Page language="c#" Codebehind="PdfModalDialog.aspx.cs" AutoEventWireup="false" Inherits="TIES.WebUI.PdfModalDialog" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PdfModalDialog</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">

        <script type="text/javascript">
            function LoadData() {
                document.getElementById("pdfFrame").src = qs('path');
            }

            function qs(name) {
                var query = window.location.search.substring(1); // Remove question mark
                var parameters = query.split('&');

                for (var i = 0; i < parameters.length; i++) {
                    var pair = parameters[i].split('=');

                    if (pair[0] == name) {
                        return pair[1];
                    }
                }

                return null;
            }
        </script>
	</HEAD>
	<body MS_POSITIONING="GridLayout" onload="LoadData();">
		<form id="Form1" method="post" runat="server">
			<iframe id="pdfFrame"  width="100%" height="100%"></iframe>
		</form>
	</body>
</HTML>
