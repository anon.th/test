<%@ Import Namespace="Cambro.Web.DbCombo" %>
<%@ Import Namespace="com.common.util" %>
<%@ Import Namespace="System.Configuration" %>
<%@ Register TagPrefix="DbCombo" Namespace="Cambro.Web.DbCombo" Assembly="Cambro.Web.DbCombo" %>
<%@ Register TagPrefix="cc1" Namespace="com.common.util" Assembly="Util" %>
<%@ Page language="c#" Codebehind="DebitNote.aspx.cs" AutoEventWireup="false" Inherits="com.ties.DebitNote" smartNavigation="False" EnableEventValidation="False" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>DebitNote</title>
		<LINK href="css/Styles.css" type="text/css" rel="stylesheet" name="stylesheet">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<!--#INCLUDE FILE="msFormValidations.inc"-->
		<script language="javascript" src="Scripts/settingScrollPosition.js"></script>
		<script language="javascript">
		var CustomerID = "";
		var InvoiceID = "";
		function CustomerOnSelect(Value, Text, SelectionType)
		{					
			if(SelectionType!=3 &&SelectionType!=2){
			CustomerID = Text;
			validateInput();
			}
		}
		function InvoiceOnSelect(Value, Text, SelectionType)
		{	
			
			if(SelectionType!=3 &&SelectionType!=2){			
			InvoiceID = Text;
			validateInput();
			}
		}		
		function validateInput(){
		var frm = document.forms[0];		
			if(InvoiceID!="" && CustomerID!=""){				
				frm.btnInsertCons.disabled = false;
			}else{
				frm.btnInsertCons.disabled = true;
			}
		}
		</script>
	</HEAD>
	<body onclick="GetScrollPosition();" onload="SetScrollPosition();" MS_POSITIONING="GridLayout">
		<form id="DebitNote" method="post" runat="server">
			<DIV id="msgPanel" style="CLEAR: both; Z-INDEX: 104; LEFT: 8px; WIDTH: 472px; POSITION: relative; TOP: 40px; HEIGHT: 120px"
				runat="server" ms_positioning="GridLayout"><asp:label id="lblMessage" style="Z-INDEX: 101; LEFT: 31px; POSITION: absolute; TOP: 19px; TEXT-ALIGN: center"
					runat="server" Width="410px" Height="56px" CssClass="tableLabel"></asp:label><asp:button id="btnToSaveChanges" style="Z-INDEX: 102; LEFT: 174px; POSITION: absolute; TOP: 87px"
					tabIndex="9" runat="server" Width="40px" CssClass="queryButton" CausesValidation="False" Text="Yes"></asp:button><asp:button id="btnNotToSave" style="Z-INDEX: 102; LEFT: 215px; POSITION: absolute; TOP: 87px"
					tabIndex="9" runat="server" Width="40px" CssClass="queryButton" CausesValidation="False" Text="No"></asp:button><asp:button id="btnNO" style="Z-INDEX: 103; LEFT: 291px; POSITION: absolute; TOP: 87px" tabIndex="9"
					runat="server" Width="63" CssClass="queryButton" CausesValidation="False" Text="Cancel" Visible="False"></asp:button></DIV>
			<asp:validationsummary id="PageValidationSummary" runat="server" HeaderText="Please check the following Invalid/Mandatory field(s):<br><br>"
				DisplayMode="BulletList" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary><asp:label id="lblMainTitle" style="Z-INDEX: 102; LEFT: 4px; POSITION: absolute; TOP: 1px"
				runat="server" Width="392px" Height="26px" CssClass="mainTitleSize">Debit Note</asp:label><asp:label id="lblErrorMsg" style="Z-INDEX: 103; LEFT: 7px; POSITION: absolute; TOP: 25px"
				runat="server" Width="100%" Height="22px" CssClass="errorMsgColor"></asp:label>
			<div id="mainPanel" style="CLEAR: both; Z-INDEX: 101; LEFT: 8px; WIDTH: 736px; POSITION: relative; TOP: 60px; HEIGHT: 593px"
				runat="server" ms_positioning="GridLayout"><asp:button id="btnQry" style="Z-INDEX: 101; LEFT: 0px; POSITION: absolute; TOP: 8px" tabIndex="20"
					runat="server" Width="62px" CssClass="queryButton" CausesValidation="False" Text="Query"></asp:button><asp:button id="btnExecQry" style="Z-INDEX: 106; LEFT: 63px; POSITION: absolute; TOP: 8px" tabIndex="19"
					runat="server" Width="130px" CssClass="queryButton" CausesValidation="False" Text="Execute Query" Enabled="False"></asp:button><asp:button id="btnInsert" style="Z-INDEX: 110; LEFT: 194px; POSITION: absolute; TOP: 8px" tabIndex="18"
					runat="server" Width="63" CssClass="queryButton" CausesValidation="False" Text="Insert"></asp:button><asp:button id="btnSave" style="Z-INDEX: 107; LEFT: 257px; POSITION: absolute; TOP: 8px" tabIndex="17"
					runat="server" Width="66px" CssClass="queryButton" CausesValidation="False" Text="Save" Enabled="False"></asp:button><asp:button id="btnCancel" style="Z-INDEX: 103; LEFT: 384px; POSITION: absolute; TOP: 8px" tabIndex="16"
					runat="server" Width="62px" CssClass="queryButton" CausesValidation="False" Text="Cancel" Visible="False"></asp:button><asp:button id="btnGoToFirstPage" style="Z-INDEX: 105; LEFT: 608px; POSITION: absolute; TOP: 8px"
					tabIndex="11" runat="server" Width="24px" CssClass="queryButton" CausesValidation="False" Text="|<"></asp:button><asp:button id="btnPreviousPage" style="Z-INDEX: 100; LEFT: 633px; POSITION: absolute; TOP: 8px"
					tabIndex="12" runat="server" Width="24px" CssClass="queryButton" CausesValidation="False" Text="<"></asp:button><asp:textbox id="txtGoToRec" style="Z-INDEX: 102; LEFT: 658px; POSITION: absolute; TOP: 8px"
					tabIndex="13" runat="server" Width="24px" CssClass="textField" Enabled="False"></asp:textbox><asp:button id="btnNextPage" style="Z-INDEX: 108; LEFT: 683px; POSITION: absolute; TOP: 8px"
					tabIndex="14" runat="server" Width="24px" CssClass="queryButton" CausesValidation="False" Text=">"></asp:button><asp:button id="btnGoToLastPage" style="Z-INDEX: 109; LEFT: 708px; POSITION: absolute; TOP: 8px"
					tabIndex="15" runat="server" Width="24px" CssClass="queryButton" CausesValidation="False" Text=">|"></asp:button>
				<table id="mainTable" style="Z-INDEX: 111; LEFT: 0px; POSITION: absolute; TOP: 32px" width="100%"
					runat="server">
					<tr height="15">
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td align="right"></td>
						<td colSpan="4"><asp:label id="lblDebitNoteNo" runat="server" Width="120px" Height="22px" CssClass="tableLabel">Debit Note Number</asp:label></td>
						<td colSpan="4"><cc1:mstextbox id="txtDebitNoteNo" tabIndex="1" runat="server" Width="144px" CssClass="textField"
								ReadOnly="True" MaxLength="100"></cc1:mstextbox></td>
						<td colSpan="3"></td>
						<td colSpan="4"><asp:label id="lblTotalDebitAmt" runat="server" Width="130px" Height="22px" CssClass="tableLabel">Total Debit Amount</asp:label></td>
						<td colSpan="4"><asp:textbox id="txtTotalDebitAmt" tabIndex="2" runat="server" Width="144px" CssClass="textFieldRightAlign"
								ReadOnly="True" MaxLength="100"></asp:textbox></td>
					</tr>
					<TR>
						<TD align="right"></TD>
						<TD colSpan="4"><asp:label id="lblDebitNoteDate" runat="server" Height="22px" CssClass="tableLabel">Debit Note Date</asp:label></TD>
						<TD colSpan="4"><cc1:mstextbox id="txtDebitNoteDate" tabIndex="3" runat="server" Width="144px" CssClass="textField"
								MaxLength="16" AutoPostBack="True" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></TD>
						<TD colSpan="3"></TD>
						<TD colSpan="4"><asp:label id="lblCreateBy" runat="server" Height="22px" CssClass="tableLabel">Created by / date</asp:label></TD>
						<TD colSpan="4"><FONT face="Tahoma"><cc1:mstextbox id="txtCreateUserID" tabIndex="4" runat="server" Width="96px" CssClass="textField"
									ReadOnly="True" MaxLength="40"></cc1:mstextbox>&nbsp;
								<cc1:mstextbox id="txtCreateDate" tabIndex="5" runat="server" Width="144px" CssClass="textField"
									ReadOnly="True" MaxLength="16" TextMaskString="99/99/9999 99:99" TextMaskType="msDateTime"></cc1:mstextbox></FONT></TD>
					</TR>
					<TR>
						<TD align="right"><asp:requiredfieldvalidator id="validCustomerID" ControlToValidate="DbCustomerID" ErrorMessage="Customer ID"
								Runat="server">*</asp:requiredfieldvalidator></TD>
						<TD colSpan="4"><asp:label id="lblCustomerID" runat="server" Height="22px" CssClass="tableLabel">Customer ID</asp:label></TD>
						<TD colSpan="4"><FONT face="Tahoma"><DBCOMBO:DBCOMBO id=DbCustomerID tabIndex=8 AutoPostBack="True" Runat="server" TabToNextFieldOnEnter="True" ValidationProperty="Text" TextBoxColumns="13" RegistrationKey='<%#ConfigurationSettings.AppSettings["DbComboRegKey"]%>' TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbCustomerSelect"></DBCOMBO:DBCOMBO></FONT></TD>
						<TD colSpan="3"></TD>
						<TD colSpan="4"><asp:label id="lblPayerName" runat="server" CssClass="tablelabel">Customer Name</asp:label></TD>
						<TD colSpan="4"><cc1:mstextbox id="txtPayerName" tabIndex="7" runat="server" Width="160px" CssClass="textField"
								ReadOnly="True" MaxLength="100"></cc1:mstextbox></TD>
					</TR>
					<tr>
						<TD align="right"><asp:requiredfieldvalidator id="validInvoiceNo" ControlToValidate="DbInvoiceNo" ErrorMessage="Invoice No" Runat="server">*</asp:requiredfieldvalidator></TD>
						<td colSpan="4"><asp:label id="lblInvoiceNo" runat="server" Height="22px" CssClass="tableLabel">Invoice No</asp:label></td>
						<td colSpan="4"><DBCOMBO:DBCOMBO id=DbInvoiceNo tabIndex=8 AutoPostBack="True" Runat="server" TabToNextFieldOnEnter="True" ValidationProperty="Text" TextBoxColumns="13" RegistrationKey='<%#ConfigurationSettings.AppSettings["DbComboRegKey"]%>' TextUpLevelSearchButton="v" ShowDbComboLink="False" ServerMethod="DbInvoiceSelect"></DBCOMBO:DBCOMBO></td>
						<td colSpan="3"></td>
						<td colSpan="4"><asp:label id="lblInvoiceDate" runat="server" Height="22px" CssClass="tableLabel">Invoice Date</asp:label></td>
						<td colSpan="4"><cc1:mstextbox id="txtInvoiceDate" tabIndex="9" runat="server" Width="144px" CssClass="textField"
								ReadOnly="True" MaxLength="16" TextMaskString="99/99/9999" TextMaskType="msDate"></cc1:mstextbox></td>
					</tr>
					<tr>
						<td></td>
						<td colSpan="4"><asp:label id="lblStatus" runat="server" Height="22px" CssClass="tableLabel"> Status</asp:label></td>
						<td colSpan="4"><asp:dropdownlist id="ddlStatus" tabIndex="13" runat="server" CssClass="textField" Enabled="True"
								AutoPostBack="True"></asp:dropdownlist></td>
						<td colSpan="3"></td>
						<td colSpan="4"><asp:label id="lblStatusUpdatedBy" runat="server" Height="22px" CssClass="tableLabel">Status updated by / date</asp:label></td>
						<td colSpan="4"><FONT face="Tahoma"><cc1:mstextbox id="txtStatusUpdateUserID" tabIndex="11" runat="server" Width="96px" CssClass="textField"
									ReadOnly="True" MaxLength="40"></cc1:mstextbox>&nbsp;</FONT>
							<cc1:mstextbox id="txtStatusUpdatedDate" tabIndex="12" runat="server" Width="144px" CssClass="textField"
								ReadOnly="True" MaxLength="16" TextMaskString="99/99/9999 99:99" TextMaskType="msDateTime"></cc1:mstextbox></td>
					</tr>
					<tr>
						<td></td>
						<td colSpan="4"><asp:label id="lblRemarks" runat="server" CssClass="tablelabel">Remarks</asp:label></td>
						<td colSpan="4"><asp:textbox id="txtRemarks" tabIndex="13" runat="server" Width="150px" Height="40px" CssClass="textField"
								MaxLength="200" Rows="2" TextMode="MultiLine" Wrap="true" Columns="20"></asp:textbox></td>
						<td colSpan="3"></td>
						<td colSpan="4"><asp:label id="lblLastUpdateBy" runat="server" Height="22px" CssClass="tableLabel">Last updated by / date</asp:label></td>
						<td colSpan="4"><FONT face="Tahoma"><cc1:mstextbox id="txtLastUpdatedUserID" tabIndex="14" runat="server" Width="96px" CssClass="textField"
									ReadOnly="True" MaxLength="40"></cc1:mstextbox>&nbsp;</FONT>
							<cc1:mstextbox id="txtLastUpdatedDate" tabIndex="15" runat="server" Width="144px" CssClass="textField"
								ReadOnly="True" MaxLength="16" TextMaskString="99/99/9999 99:99" TextMaskType="msDateTime"></cc1:mstextbox></td>
					</tr>
					<tr>
						<td></td>
						<td colSpan="4"></td>
						<td colSpan="4"></td>
						<td colSpan="3"></td>
						<td colSpan="4"></td>
						<td colSpan="4"></td>
					</tr>
					<tr>
						<td></td>
						<td colSpan="4"><asp:button id="btnInsertCons" tabIndex="9" runat="server" Width="63" CssClass="queryButton"
								CausesValidation="False" Text="Insert" Enabled="False"></asp:button></td>
						<td colSpan="4"></td>
						<td colSpan="3"></td>
						<td colSpan="4"></td>
						<td colSpan="4"></td>
					</tr>
					<tr>
						<td></td>
						<td colSpan="19"><asp:datagrid id="dgAssignedConsignment" tabIndex="10" runat="server" Width="800px" AllowPaging="True"
								OnItemDataBound="dgAssignedConsignment_Bound" OnEditCommand="dgAssignedConsignment_Edit" OnCancelCommand="dgAssignedConsignment_Cancel"
								OnUpdateCommand="dgAssignedConsignment_Update" OnDeleteCommand="dgAssignedConsignment_Delete" ItemStyle-Height="20"
								AutoGenerateColumns="False">
								<SelectedItemStyle CssClass="gridFieldSelected"></SelectedItemStyle>
								<ItemStyle Height="20px" CssClass="gridField"></ItemStyle>
								<Columns>
									<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="&lt;IMG SRC='images/butt-update.gif' border=0 title='Save' &gt;"
										CancelText="&lt;IMG SRC='images/butt-cancel.gif' border=0 title='Cancel' &gt;" EditText="&lt;IMG SRC='images/butt-edit.gif' border=0 title='Update' &gt;">
										<HeaderStyle Width="5%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Left"></ItemStyle>
									</asp:EditCommandColumn>
									<asp:ButtonColumn Text="&lt;IMG SRC='images/butt-delete.gif' border=0 title='Delete' &gt;" CommandName="Delete">
										<HeaderStyle HorizontalAlign="Center" Width="3%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center"></ItemStyle>
									</asp:ButtonColumn>
									<asp:TemplateColumn HeaderText="Consignment No.">
										<HeaderStyle Width="12%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Wrap="False"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblConsignmentNo" Text='<%#DataBinder.Eval(Container.DataItem,"Consignment_No")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<dbCombo:DBCOMBO id="DbConsignmentNo" ValidationProperty="Text" Width="72px" Runat="server" ServerMethod="DbConsignmentNoSelect" ShowDbComboLink="False" TextUpLevelSearchButton=" " Text='<%#DataBinder.Eval(Container.DataItem,"Consignment_No")%>' RegistrationKey='<%#ConfigurationSettings.AppSettings["DbComboRegKey"]%>' TextBoxColumns="22" AutoPostBack="True" >
											</dbCombo:DBCOMBO>
											<asp:requiredfieldvalidator id="reqDbConsignment" Runat="server" ErrorMessage="DbConsignmentNo" ControlToValidate="DbConsignmentNo"
												Display="None"></asp:requiredfieldvalidator>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Charge Code">
										<HeaderStyle Width="12%" CssClass="gridHeading"></HeaderStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="Label2" Text='<%#DataBinder.Eval(Container.DataItem,"charge_code")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<dbCombo:DBCOMBO id="DbChargeCode" ValidationProperty="Text" Width="36px" Runat="server" ServerMethod="DbChargeCodeSelect" ShowDbComboLink="False" TextUpLevelSearchButton=" " Text='<%#DataBinder.Eval(Container.DataItem,"charge_code")%>' RegistrationKey='<%#ConfigurationSettings.AppSettings["DbComboRegKey"]%>' TextBoxColumns="2" AutoPostBack="True" >
											</dbCombo:DBCOMBO>
											<asp:requiredfieldvalidator id="reqDbChargeCode" Runat="server" ErrorMessage="DbChargeCode" ControlToValidate="DbChargeCode"
												Display="None"></asp:requiredfieldvalidator>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Invoice Amount">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="lblInvoiceAmt" Runat="server" Enabled="True" Text='<%#DataBinder.Eval(Container.DataItem,"Invoice_Amt","{0:n}")%>'>
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBoxNumber" Height="21px" ID="txtInvoiceAmt" Runat="server" ReadOnly="True" Text='<%#DataBinder.Eval(Container.DataItem,"Invoice_Amt","{0:n}")%>'>
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Reason Code">
										<HeaderStyle Width="11%" CssClass="gridHeading"></HeaderStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="Label3" Text='<%#DataBinder.Eval(Container.DataItem,"reason_code")%>' Runat="server">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<dbCombo:DBCOMBO id="DbReasonCode" ValidationProperty="Text" Width="36px" Runat="server" ServerMethod="DbReasonCodeSelect" ShowDbComboLink="False" TextUpLevelSearchButton=" " Text='<%#DataBinder.Eval(Container.DataItem,"reason_code")%>' RegistrationKey='<%#ConfigurationSettings.AppSettings["DbComboRegKey"]%>' TextBoxColumns="2" AutoPostBack="True" >
											</dbCombo:DBCOMBO>
											<asp:requiredfieldvalidator id="reqReasonCode" Runat="server" ErrorMessage="DbReasonCode" ControlToValidate="DbReasonCode"
												Display="None"></asp:requiredfieldvalidator>
											<!--<asp:TextBox CssClass="gridTextBoxNumber" Height="21px" ID="txtReasonCode" Runat="server" ReadOnly="True" Text='<%#DataBinder.Eval(Container.DataItem,"reason_code")%>'>
											</asp:TextBox>-->
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Description">
										<HeaderStyle Width="30%" CssClass="gridHeading"></HeaderStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblDebitNoteDescription" Text='<%#DataBinder.Eval(Container.DataItem,"Description")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" ID="txtDebitNoteDescription" Height="21px" Text='<%#DataBinder.Eval(Container.DataItem,"Description")%>' ReadOnly='True' Runat="server" MaxLength="200">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Remark">
										<HeaderStyle Width="15%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle Width="15%"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabel" ID="lblRemark" Text='<%#DataBinder.Eval(Container.DataItem,"Remark")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<asp:TextBox CssClass="gridTextBox" ID="txtRemark" Height="21px" Text='<%#DataBinder.Eval(Container.DataItem,"Remark")%>' Runat="server" MaxLength="200">
											</asp:TextBox>
										</EditItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Debit Amount">
										<HeaderStyle Width="10%" CssClass="gridHeading"></HeaderStyle>
										<ItemStyle VerticalAlign="Middle"></ItemStyle>
										<ItemTemplate>
											<asp:Label CssClass="gridLabelNumber" ID="lblDebitAmt" Text='<%#DataBinder.Eval(Container.DataItem,"Amt","{0:n}")%>' Runat="server" Enabled="True">
											</asp:Label>
										</ItemTemplate>
										<EditItemTemplate>
											<cc1:mstextbox ID="txtDebitAmt" width="80px" Text='<%#DataBinder.Eval(Container.DataItem,"Amt","{0:n}")%>' Height="21px" Runat="server" CssClass="gridTextBoxNumber" AutoPostBack="false" TextMaskType="msNumeric" MaxLength="11" NumberMaxValue="99999999" NumberMinValue="0" NumberPrecision="10" NumberScale="2" TextMaskString="#.00">
											</cc1:mstextbox>
										</EditItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle NextPageText="Next" PrevPageText="Previous" HorizontalAlign="Right" CssClass="normalText"></PagerStyle>
							</asp:datagrid></td>
					</tr>
				</table>
				<asp:label id="lblNumRec" style="Z-INDEX: 112; LEFT: 456px; POSITION: absolute; TOP: 8px" runat="server"
					Width="136px" Height="19px" CssClass="RecMsg"></asp:label><asp:textbox id="txtDNtest" style="Z-INDEX: 104; LEFT: 449px; POSITION: absolute; TOP: 9px" runat="server"
					Width="8px" CssClass="textFieldRightAlign" Visible="False"></asp:textbox><asp:button id="btnPrint" style="Z-INDEX: 113; LEFT: 322px; POSITION: absolute; TOP: 8px" tabIndex="16"
					runat="server" Width="62px" CssClass="queryButton" CausesValidation="False" Text="Print"></asp:button></div>
			<input 
type=hidden value="<%=strScrollPosition%>" name=ScrollPosition>
		</form>
	</body>
</HTML>
