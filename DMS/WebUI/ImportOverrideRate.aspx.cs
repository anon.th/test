using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.classes;
using com.common.util;
using com.ties.classes;
using com.ties.BAL;
using com.common.applicationpages;
using System.Text;
using System.IO;
using System.Configuration;
using System.Data.OleDb;
using TIESBAL;
using TIESDAL;

namespace com.ties
{
	/// <summary>
	/// Summary description for ImportOverrideRate.
	/// </summary>
	public class ImportOverrideRate : BasePage
	{
		String appID = null;
		String enterpriseID = null;
		String userID = null;

		DataSet dsImportData = null;
		DataSet dsValidImportData = null;
		DataSet dsInvalidImportData = null;
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.dgValidImportData.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgValidImportData_PageIndexChanged);
			this.dgValidImportData.SelectedIndexChanged += new System.EventHandler(this.dgValidImportData_SelectedIndexChanged);
			this.dgValidImportData.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgValidImportData_ItemDataBound);
			this.Button1.Click += new System.EventHandler(this.Button1_Click);
			this.dgInvalidImportData.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.dgInvalidImportData_PageIndexChanged);
			this.dgInvalidImportData.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.dgInvalidImportData_ItemDataBound);
			this.dgInvalidImportData.SelectedIndexChanged += new System.EventHandler(this.dgInvalidImportData_SelectedIndexChanged);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		protected System.Web.UI.WebControls.Label lblMainTitle;
		protected System.Web.UI.WebControls.Button btnImport;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.TextBox txtFilePath;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		protected System.Web.UI.WebControls.Label lblTotalSavedCon;
		protected System.Web.UI.HtmlControls.HtmlInputFile inFile;
		
		protected System.Web.UI.WebControls.DataGrid dgValidImportData;
		protected System.Web.UI.WebControls.Label lblvalid;
		protected System.Web.UI.WebControls.Label lblinvalid;
		protected System.Web.UI.WebControls.Button Button1;
		protected System.Web.UI.WebControls.DataGrid dgInvalidImportData;

		#endregion
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			appID = utility.GetAppID();
			enterpriseID = utility.GetEnterpriseID();
			userID = utility.GetUserID();

			if(!IsPostBack)
			{			
				//Clear Temp Table of Logged In User
				ImportOverrideRateDAL.ClearTempDataByUserId(appID, enterpriseID, userID);
				
				//QueryMode();	  
				lblvalid.Visible = false;
				lblinvalid.Visible = false;

				txtFilePath.Attributes.Add("onkeyup", "clearValue();");

				btnSave.Attributes.Add("onclick", "StartShowWait(this);");
				btnImport.Attributes.Add("onclick", "StartShowWait(this);");
			}
			else
			{
				dsImportData = (DataSet)Session["dsImportData"];
			}
		}


		private void btnImport_Click(object sender, System.EventArgs e)
		{
			lblTotalSavedCon.Text = "";

			try
			{
				if(uploadFiletoServer())
				{
					//Cleare existing data
					ImportOverrideRateDAL.ClearTempDataByUserId(appID, enterpriseID, userID);

					//Get data from Excel WorkSheet
					getExcelFiles();

					//Get data from session and put into DataSet
					dsImportData = (DataSet)Session["dsImportData"];

					//Audit and Insert all data into temp table
					ImportOverrideRateDAL.ImportToTempTables(appID, enterpriseID,
						dsImportData, userID);

					//Get data from DataBase
					retreiveImportData();

//					btnSave.Enabled = true;
					lblvalid.Visible = true;
					lblinvalid.Visible = true;
				}
			}
			catch(Exception ex)
			{
				
				lblErrorMsg.Text = ex.Message.ToString();
				txtFilePath.Text = "";
			}
		}


		private bool uploadFiletoServer()
		{
			bool status = true;

			if((inFile.PostedFile != null ) && (inFile.PostedFile.ContentLength > 0))
			{
				string extension = System.IO.Path.GetExtension(inFile.PostedFile.FileName);

				if(extension.ToUpper() == ".XLS")
				{
					string fn = System.IO.Path.GetFileName(inFile.PostedFile.FileName);
					string path = Server.MapPath("Excel") + "\\";
					string pathFn = Server.MapPath("Excel") + "\\" + userID + "_" + fn;

					ViewState["FileName"] = fn.Replace(extension, "");
					ViewState["FilePath"] = pathFn;

					try
					{
						if(System.IO.Directory.Exists(path) == false)
							System.IO.Directory.CreateDirectory(path);

						if (System.IO.File.Exists(pathFn)) 
							System.IO.File.Delete(pathFn);

						inFile.PostedFile.SaveAs(pathFn);

						lblErrorMsg.Text = "";
					}
					catch(Exception ex)
					{
						throw new ApplicationException("Error during upload file to the server", null);
					}
				}
				else
				{
					lblErrorMsg.Text = "Not a valid Excel Workbook.";
					txtFilePath.Text = "";
					status = false;
				}
			}
			else
			{
				lblErrorMsg.Text = "Please select a file to upload.";
				status = false;
			}

			return status;
		}


		private void getExcelFiles()
		{
			int i = 0;
			decimal FreightNum = 0;
			decimal InsNum = 0;
			decimal OtherNum = 0;
			decimal ESANum = 0;

			try
			{	
				#region Get Import Data from Excel 

				String excelConn = ConfigurationSettings.AppSettings["ExcelConn"];
				excelConn = excelConn.Replace("(DestinationFile)", (String)ViewState["FilePath"]);

				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append("SELECT consignment_no , override_rated_freight  , override_rated_ins, override_rated_other, override_rated_esa ");
				strBuilder.Append("FROM [OverrideRate$] ");

				OleDbDataAdapter daExcel = new OleDbDataAdapter(strBuilder.ToString(), excelConn);

				DataSet dsExcel = new DataSet();
				daExcel.Fill(dsExcel);

				#endregion

				#region Get Shipment Data from DB 

				string conList = "";
//				string bookingList = "";
				foreach(DataRow dr in dsExcel.Tables[0].Rows)
				{
					if(dr["consignment_no"].ToString().Trim() != "") {
						conList = conList + "'" + dr["consignment_no"].ToString().Trim() + "', ";
					}
//					if(dr["booking_no"].ToString().Trim() != "") 
//					{
//						bookingList = bookingList + "'" + dr["booking_no"].ToString().Trim() + "', ";
//					}
				}
				conList = conList.Substring(0, conList.Length - 2);
//				bookingList = bookingList.Substring(0, conList.Length - 2);

				DataSet dsDB = new DataSet();
				dsDB = ImportOverrideRateDAL.GetShipmentByConList(appID, enterpriseID, conList);  // ,bookingList

				#endregion

				#region Prepare Temporary Import Data

				dsImportData = ImportOverrideRateDAL.GetEmptyImport();
				if(dsImportData.Tables[0].Rows.Count == 1)
				{
					dsImportData.Tables[0].Rows[0].Delete();
					dsImportData.AcceptChanges();
				}
				#endregion

				int recImport = 1;
				string  decFreight  = "";

				foreach(DataRow drExcel in dsExcel.Tables[0].Rows)
				{

					decFreight = Convert.ToString((drExcel["override_rated_freight"].ToString().Trim()));
					if((drExcel["consignment_no"].ToString().Trim() != "") &&
						((drExcel["override_rated_freight"].ToString().Trim() != "") ||
						(drExcel["override_rated_ins"].ToString().Trim() != "") || 
						(drExcel["override_rated_other"].ToString().Trim() != "") ||
						(drExcel["override_rated_esa"].ToString().Trim() != ""))
						)
					{
						//Get data from dsDB
						DataRow[] drDB = dsDB.Tables[0].Select("consignment_no = '" + drExcel["consignment_no"].ToString().Trim() + "'");

						#region validate data in excel file

						string[] isValid = ImportOverrideRateDAL.ValidateImportData(drExcel, drDB);

						#endregion

						DataRow tmpDr = dsImportData.Tables[0].NewRow();

						tmpDr["recordid"] = recImport.ToString();
						recImport++;

						tmpDr["consignment_no"] = Convert.ToString(drExcel["consignment_no"]);	
						
						tmpDr["isvalid_consignment_no"] = 1;
						tmpDr["isvalid_override_rated_freight"] = 1;
						tmpDr["isvalid_override_rated_ins"] = 1;
						tmpDr["isvalid_override_rated_other"] = 1;
						tmpDr["isvalid_override_rated_esa"] = 1;

						if (isValid[0] == "")																			
						{
							tmpDr["ref_no"] = drDB[0]["ref_no"].ToString();												
							tmpDr["destination_station"] = drDB[0]["destination_station"].ToString();
							tmpDr["route_code"] = drDB[0]["route_code"].ToString();
							tmpDr["payerid"] = drDB[0]["payerid"].ToString();
							tmpDr["act_delivery_date"] = drDB[0]["act_delivery_date"];	

							tmpDr["tot_freight_charge"] = Convert.ToDecimal(drDB[0]["tot_freight_charge"].ToString());		
							tmpDr["insurance_surcharge"] = Convert.ToDecimal(drDB[0]["insurance_surcharge"].ToString());
							tmpDr["other_surch_amount"] = Convert.ToDecimal(drDB[0]["other_surch_amount"].ToString());
							tmpDr["tot_vas_surcharge"] = Convert.ToDecimal(drDB[0]["tot_vas_surcharge"].ToString());
							tmpDr["esa_surcharge"] = Convert.ToDecimal(drDB[0]["esa_surcharge"].ToString());
							tmpDr["total_rated_amount"] = Convert.ToDecimal(drDB[0]["total_rated_amount"].ToString());	
	
							if (drExcel["override_rated_freight"].ToString() != "")
							{
								tmpDr["override_rated_freight"] = Convert.ToDecimal(drExcel["override_rated_freight"].ToString());	
								FreightNum = Convert.ToDecimal(drExcel["override_rated_freight"].ToString());	
							}
							else
							{
								tmpDr["override_rated_freight"] = null;  //tmpDr["override_rated_freight"] = 0
								FreightNum = 0;
							}

							if (drExcel["override_rated_ins"].ToString() != "")
							{
								tmpDr["override_rated_ins"] = Convert.ToDecimal(drExcel["override_rated_ins"].ToString());
								InsNum = Convert.ToDecimal(drExcel["override_rated_ins"].ToString());
							}
							else
							{
								tmpDr["override_rated_ins"] = null;
								InsNum=0;
							}

							if (drExcel["override_rated_other"].ToString() != "")
							{
								tmpDr["override_rated_other"] = Convert.ToDecimal(drExcel["override_rated_other"].ToString());
								OtherNum = Convert.ToDecimal(drExcel["override_rated_other"].ToString());
							}
							else
							{
								tmpDr["override_rated_other"] = null;
								OtherNum = 0;
							}

							if (drExcel["override_rated_esa"].ToString() != "")
							{
								tmpDr["override_rated_esa"] = Convert.ToDecimal(drExcel["override_rated_esa"].ToString());
								ESANum=Convert.ToDecimal(drExcel["override_rated_esa"].ToString());
							}
							else
							{
								tmpDr["override_rated_esa"] = null;															
								ESANum =0;
							}
	
							tmpDr["override_rated_total"] = FreightNum +  InsNum  + OtherNum + ESANum ;
						}
						else 
						{
							tmpDr["ref_no"] = "";
							tmpDr["destination_station"] = "";
							tmpDr["route_code"] = "";
							tmpDr["payerid"] = "";
							tmpDr["act_delivery_date"] = System.DBNull.Value;

							tmpDr["tot_freight_charge"] = 0;		
							tmpDr["insurance_surcharge"] = 0;
							tmpDr["other_surch_amount"] = 0;
							tmpDr["tot_vas_surcharge"] = 0;
							tmpDr["esa_surcharge"] = 0;
							tmpDr["total_rated_amount"] = 0;	
	
							tmpDr["override_rated_freight"] = Convert.ToString(drExcel["override_rated_freight"]);
							tmpDr["override_rated_ins"] = Convert.ToString(drExcel["override_rated_ins"]);
							tmpDr["override_rated_other"] = Convert.ToString(drExcel["override_rated_other"]);
							tmpDr["override_rated_esa"] = Convert.ToString(drExcel["override_rated_esa"]);
							tmpDr["override_rated_total"] = "";

							string isValidStr = isValid[1].ToString();
							tmpDr["isvalid_consignment_no"] = Convert.ToInt16(isValidStr.Substring(0, 1));
							tmpDr["isvalid_override_rated_freight"] = Convert.ToInt16(isValidStr.Substring(1, 1));
							tmpDr["isvalid_override_rated_ins"] =  Convert.ToInt16(isValidStr.Substring(2, 1));
							tmpDr["isvalid_override_rated_other"] =  Convert.ToInt16(isValidStr.Substring(3, 1));
							tmpDr["isvalid_override_rated_esa"] =  Convert.ToInt16(isValidStr.Substring(4, 1));
						}																								
						
						tmpDr["excluded_reason"] = isValid[0];

						dsImportData.Tables[0].Rows.Add(tmpDr);
					}			
					dsImportData.AcceptChanges();
				}

				dsExcel.Dispose();
				daExcel.Dispose();

				dsDB.Dispose();
				
				dsExcel = null;
				daExcel = null;

				dsDB = null;
			
				Session["dsImportData"] = dsImportData;
			}
			catch(Exception ex)
			{
				String strMsg = ex.Message;

				if(strMsg.IndexOf("'Consignments$' is not a valid name") != -1)
				{
					throw new ApplicationException("Excel Workbook must contain two named sheets: Consignments and Packages.", null);
				}
				else if(strMsg.IndexOf("'Packages$' is not a valid name") != -1)
				{
					throw new ApplicationException("Excel Workbook must contain two named sheets: Consignments and Packages.", null);
				}
				else if(strMsg.IndexOf("No value given for one or more required parameters") != -1)
				{
					if(i == 0)
						throw new ApplicationException("Consignments worksheet has invalid columns", null);
					else
						throw new ApplicationException("Packages worksheet has invalid columns", null);
				}
				else
				{
					throw ex;
				}
			}
		}


		public void QueryData()
		{
			dsValidImportData = ImportOverrideRateDAL.GetEmptyValidImport();
			Session["dsValidImportData"] = dsValidImportData;
			BindValidGrid();

			dsInvalidImportData = ImportOverrideRateDAL.GetEmptyInvalidImport();
			Session["dsInvalidImportData"] = dsInvalidImportData;
			BindInvalidGrid();
		}


		public void BindValidGrid()
		{
			dsValidImportData = (DataSet)Session["dsValidImportData"];
			if(dsValidImportData.Tables[0].Rows.Count == 0) 
			{
				dsValidImportData = ImportOverrideRateDAL.GetEmptyValidImport();
				Session["dsValidImportData"] = dsValidImportData;
				btnSave.Enabled=false;
			}
			else
			{
				btnSave.Enabled=true;
			}

			try
			{
				dgValidImportData.DataSource = dsValidImportData;
				dgValidImportData.DataBind(); 
			
			}
			catch
			{
				dgValidImportData.CurrentPageIndex = 0; 
				dgValidImportData.DataSource = dsValidImportData;
				dgValidImportData.DataBind(); 
			}
		}


		public void BindInvalidGrid()
		{
			dsInvalidImportData = (DataSet)Session["dsInvalidImportData"];
			if(dsInvalidImportData.Tables[0].Rows.Count == 0) 
			{
				dsInvalidImportData = ImportOverrideRateDAL.GetEmptyInvalidImport();
				Session["dsInvalidImportData"] = dsInvalidImportData;
				Button1.Enabled=false;
			}
			else
			{
				Button1.Enabled=true;
			}

			try
			{
				dgInvalidImportData.DataSource = dsInvalidImportData;
				dgInvalidImportData.DataBind(); 
			
			}
			catch
			{
				dgInvalidImportData.CurrentPageIndex = 0; 
				dgInvalidImportData.DataSource = dsInvalidImportData;
				dgInvalidImportData.DataBind(); 
			}
		}


		private void retreiveImportData()
		{
			dsValidImportData = ImportOverrideRateDAL.GetImportDataByRecordType(appID, enterpriseID, 0, 0, userID, "valid");
			Session["dsValidImportData"] = dsValidImportData;
			BindValidGrid();

			dsInvalidImportData = ImportOverrideRateDAL.GetImportDataByRecordType(appID, enterpriseID, 0, 0, userID, "invalid");
			Session["dsInvalidImportData"] = dsInvalidImportData;
			BindInvalidGrid();
		}


		private void ExportData(DataTable dtInvalidImport)
		{
			string Res = "";			
 
			try
			{
				if ((dtInvalidImport != null) && (dtInvalidImport.Columns.Count > 0) && (dtInvalidImport.Rows.Count > 0))
				{
					TIESClasses.CreateXls Xls = new TIESClasses.CreateXls();

					Res = Xls.createXlsOverride(dtInvalidImport, ViewState["FilePath"].ToString());

					if (Res == "0")
					{	
						GetDownloadFile(Res);
				
						// Kill Process after save
						Xls.KillProcesses("EXCEL");
					} 
					else
					{
						lblErrorMsg.Text = Res;
					}
				}
			}
			catch(Exception ex)
			{
				lblErrorMsg.Text = ex.Message.ToString() ;
			}
			finally
			{
				dtInvalidImport.Dispose();
			}
		}


		private void GetDownloadFile(string Res)
		{
			if (Res == "0")
			{
				Response.ContentType="application/ms-excel";
				Response.AddHeader( "content-disposition","attachment; filename=" + ViewState["FileName"].ToString() + "_invalid" + ".xls");
				FileStream fs = new FileStream(ViewState["FilePath"].ToString(),FileMode.Open);
				long FileSize;
				FileSize = fs.Length;
				byte[] getContent = new byte[(int)FileSize];
				fs.Read(getContent, 0, (int)fs.Length);
				fs.Close();

				Response.BinaryWrite(getContent);
			} 
		}


		private void dgInvalidImportData_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}


		private void dgValidImportData_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}


		protected void dgValidImportData_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
		{
			dgValidImportData.CurrentPageIndex = e.NewPageIndex;
			dgValidImportData.EditItemIndex = -1;
			dgValidImportData.SelectedIndex = -1;
			BindValidGrid();
		}


		protected void dgInvalidImportData_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
		{
			dgInvalidImportData.CurrentPageIndex = e.NewPageIndex;
			dgInvalidImportData.EditItemIndex = -1;
			dgInvalidImportData.SelectedIndex = -1;
			BindInvalidGrid();
		}


		private void dgInvalidImportData_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			Label lblInvConsignment_no = (Label)e.Item.FindControl("lblInvConsignment_no");
			Label lblInvBooking_no = (Label)e.Item.FindControl("lblInvBooking_no");
			if(lblInvConsignment_no != null)
			{
				if((DataBinder.Eval(e.Item.DataItem,"isvalid_consignment_no") != null) && 
					(DataBinder.Eval(e.Item.DataItem,"isvalid_consignment_no") != System.DBNull.Value))
				{
					string strError = Convert.ToString(  DataBinder.Eval(e.Item.DataItem,"excluded_reason") );  
					if (Convert.ToInt16(DataBinder.Eval(e.Item.DataItem,"isvalid_consignment_no")) == 0) 
					{
						if (strError.Trim().Substring(0,7) == "Booking" )
						{
							lblInvBooking_no.ForeColor = System.Drawing.Color.Red;
							lblInvBooking_no.Font.Bold = true;
						}
						else
						{
							lblInvConsignment_no.ForeColor = System.Drawing.Color.Red;
							lblInvConsignment_no.Font.Bold = true;
						}
					}
				}				
			}

			Label lblInvOverride_rated_freight = (Label)e.Item.FindControl("lblInvOverride_rated_freight");
			lblInvOverride_rated_freight.BackColor = System.Drawing.Color.PeachPuff;
			if(lblInvOverride_rated_freight != null)
			{
				if((DataBinder.Eval(e.Item.DataItem,"isvalid_override_rated_freight") != null) && 
					(DataBinder.Eval(e.Item.DataItem,"isvalid_override_rated_freight") != System.DBNull.Value))
				{
					if (Convert.ToInt16(DataBinder.Eval(e.Item.DataItem,"isvalid_override_rated_freight")) == 0) 
					{
						lblInvOverride_rated_freight.ForeColor = System.Drawing.Color.Red;
						lblInvOverride_rated_freight.Font.Bold = true; 
					}
				}				
			}

			Label lblInvOverride_rated_ins = (Label)e.Item.FindControl("lblInvOverride_rated_ins");
			lblInvOverride_rated_ins.BackColor = System.Drawing.Color.PeachPuff;
			if(lblInvOverride_rated_ins != null)
			{
				if((DataBinder.Eval(e.Item.DataItem,"isvalid_override_rated_ins") != null) && 
					(DataBinder.Eval(e.Item.DataItem,"isvalid_override_rated_ins") != System.DBNull.Value))
				{
					if (Convert.ToInt16(DataBinder.Eval(e.Item.DataItem,"isvalid_override_rated_ins")) == 0) 
					{
						lblInvOverride_rated_ins.ForeColor = System.Drawing.Color.Red;
						lblInvOverride_rated_ins.Font.Bold = true;
					}
				}				
			}

			Label lblInvOverride_rated_other = (Label)e.Item.FindControl("lblInvOverride_rated_other");
			lblInvOverride_rated_other.BackColor = System.Drawing.Color.PeachPuff;
			if(lblInvOverride_rated_other != null)
			{
				if((DataBinder.Eval(e.Item.DataItem,"isvalid_override_rated_other") != null) && 
					(DataBinder.Eval(e.Item.DataItem,"isvalid_override_rated_other") != System.DBNull.Value))
				{
					if (Convert.ToInt16(DataBinder.Eval(e.Item.DataItem,"isvalid_override_rated_other")) == 0) 
					{
						lblInvOverride_rated_other.ForeColor = System.Drawing.Color.Red;
						lblInvOverride_rated_other.Font.Bold = true;
					}
				}				
			}

			Label lblInvOverride_rated_esa = (Label)e.Item.FindControl("lblInvOverride_rated_esa");
			lblInvOverride_rated_esa.BackColor = System.Drawing.Color.PeachPuff;
			if(lblInvOverride_rated_esa != null)
			{
				if((DataBinder.Eval(e.Item.DataItem,"isvalid_override_rated_esa") != null) && 
					(DataBinder.Eval(e.Item.DataItem,"isvalid_override_rated_esa") != System.DBNull.Value))
				{
					if (Convert.ToInt16(DataBinder.Eval(e.Item.DataItem,"isvalid_override_rated_esa")) == 0) 
					{
						lblInvOverride_rated_esa.ForeColor = System.Drawing.Color.Red;
						lblInvOverride_rated_esa.Font.Bold = true;
					}
				}				
			}		
			
			Label lblInvoverride_rated_total = (Label)e.Item.FindControl("lblInvoverride_rated_total");
			lblInvoverride_rated_total.BackColor = System.Drawing.Color.PeachPuff;

			Label lblInvexcluded_reason = (Label)e.Item.FindControl("lblInvexcluded_reason");
			lblInvexcluded_reason.ForeColor = System.Drawing.Color.Red;
	}

		private void dgValidImportData_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
		{
			if(e.Item.ItemIndex == -1)
			{
				return;
			}

			Label lblOverride_rated_freight = (Label)e.Item.FindControl("lblOverride_rated_freight");
			lblOverride_rated_freight.BackColor = System.Drawing.Color.PeachPuff;

			Label lblOverride_rated_ins = (Label)e.Item.FindControl("lblOverride_rated_ins");
			lblOverride_rated_ins.BackColor = System.Drawing.Color.PeachPuff;

			Label lblOverride_rated_other = (Label)e.Item.FindControl("lblOverride_rated_other");
			lblOverride_rated_other.BackColor = System.Drawing.Color.PeachPuff;

			Label lblOverride_rated_esa = (Label)e.Item.FindControl("lblOverride_rated_esa");
			lblOverride_rated_esa.BackColor = System.Drawing.Color.PeachPuff;

			Label lbloverride_rated_total = (Label)e.Item.FindControl("lbloverride_rated_total");
			lbloverride_rated_total.BackColor = System.Drawing.Color.PeachPuff;

		}

	private void btnSave_Click(object sender, System.EventArgs e)
		{
			lblTotalSavedCon.Text = "";
			dsValidImportData = (DataSet)Session["dsValidImportData"];

			if(dsValidImportData.Tables[0].Rows.Count == 0)
			{
				lblErrorMsg.Text = "No import data.";
				lblTotalSavedCon.Text = "Total Imported: 0 Consignments.";
			}
			else
			{
				int iRowsAffected = 0;
				
				try 
				{
					iRowsAffected = ImportOverrideRateDAL.InsertOverrideRate(appID, enterpriseID, userID);

					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"MSG_SAVESUCCESS",utility.GetUserCulture());	
					lblTotalSavedCon.Text = "Total Imported: " + iRowsAffected.ToString() + " Consignments.";

					//Clear Valid  grid
					dsValidImportData = ImportOverrideRateDAL.GetEmptyValidImport();
					Session["dsValidImportData"] = dsValidImportData;
					BindValidGrid();
				}
				catch(Exception ex)
				{
					String tmpErrMessage = "";
					tmpErrMessage = ex.Message;

					lblErrorMsg.Text = tmpErrMessage;	
					lblTotalSavedCon.Text = "Total Imported: 0 Consignments.";
				}
			}
		}

		private void Button1_Click(object sender, System.EventArgs e)
		{
			dsInvalidImportData = (DataSet)Session["dsInvalidImportData"];
			ExportData(dsInvalidImportData.Tables[0]);
		}

		

	}
}
