using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using com.common.DAL;  
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using com.ties.BAL;   
using com.common.RBAC;    
using com.common.applicationpages;

namespace com.ties
{
	/// <summary>
	/// Summary description for ShipmentDetails.
	/// </summary>
	public class ShipmentDetails : BasePopupPage
	{
		protected System.Web.UI.WebControls.Label lblSerialNo;
		protected System.Web.UI.WebControls.TextBox txtSerialNo;
		protected System.Web.UI.WebControls.Label lblRecpZipCode;
		protected System.Web.UI.WebControls.Label lblServicecode;
		protected System.Web.UI.WebControls.Label lblESAApplied;
		protected System.Web.UI.WebControls.Label lblESASurcharge;
		protected System.Web.UI.WebControls.TextBox txtESASurcharge;
		protected System.Web.UI.WebControls.Label lblEStDevlDt;
		protected System.Web.UI.WebControls.Label lblFrightChrg;
		protected System.Web.UI.WebControls.Label lblTotalVASSurcharge;
		protected System.Web.UI.WebControls.Label lblDeclareValue;
		protected System.Web.UI.WebControls.Label lblTolAmt;
		protected System.Web.UI.WebControls.Label lblInsurSurcharge;
		protected System.Web.UI.WebControls.DataGrid dgVAS;
		protected System.Web.UI.WebControls.Button btnServiceCode;
		protected System.Web.UI.WebControls.Button btnRecpZipCode;
		protected System.Web.UI.WebControls.Button btnInsert;
		protected System.Web.UI.WebControls.Button btnSave;
		protected System.Web.UI.WebControls.Button BtnDelete;
		protected System.Web.UI.WebControls.Button btnCancel;
		protected System.Web.UI.WebControls.Button btnGoToFirstPage;
		protected System.Web.UI.WebControls.Button btnPreviousPage;
		protected System.Web.UI.WebControls.TextBox txtCountRec;
		protected System.Web.UI.WebControls.Button btnNextPage;
		protected System.Web.UI.WebControls.Button btnGoToLastPage;
		protected System.Web.UI.HtmlControls.HtmlTable tblPickupShipment;
		DataSet m_dsVAS=null;
		DataSet m_dsPkgDetails = null;
		DataSet m_dsShpmDetls=null;
		//	Utility utility =null;
		string strAppId=null;
		protected System.Web.UI.WebControls.DataGrid dgPkgInfo;
		protected System.Web.UI.WebControls.TextBox txtRecipZip;
		protected System.Web.UI.WebControls.Label lblErrorMsg;
		string strEnterpriseId=null;
		private string strCustType=null;
		private string strCustId=null;
		private string strSendZipCd=null;
		private string strBookingNo;
		private decimal iLength = 0;
		private decimal iBreadth = 0;
		private decimal iHeight = 0;
		private decimal iWeight = 0;
		private int iQty = 0;
		private float fDimWt = 0;
		private decimal decTotalWt = 0;
		private decimal decChrgeable = 0;
		private string strApplyDimWt=null;
		protected System.Web.UI.WebControls.TextBox txtShpSvcCode;
		protected System.Web.UI.WebControls.TextBox txtShpSvcDesc;
		protected System.Web.UI.WebControls.TextBox txtESA;
		protected System.Web.UI.WebControls.Button btnPopulateVAS;
		protected System.Web.UI.WebControls.Button btnBind;
		protected com.common.util.msTextBox txtEstDelvdate;
		private string strErrMsg=null;
		private string strPickupDt=null;
		protected System.Web.UI.WebControls.Button bntClose;
		string strMode =null;
		protected com.common.util.msTextBox txtFreightChrg;
		protected com.common.util.msTextBox txtTotVASSurChrg;
		protected com.common.util.msTextBox txtInsSurcharge;
		protected com.common.util.msTextBox txtShpTotAmt;
		protected System.Web.UI.WebControls.TextBox txtShpDclrValue;
		protected System.Web.UI.WebControls.TextBox txtShpAddDV;
		SessionDS m_sdsPickupRequest=null;
		String strErrorMsg=null;
		ArrayList m_QryList = null;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Button btnToCancel;
		protected System.Web.UI.WebControls.Button btnNotToSave;
		protected System.Web.UI.WebControls.Button btnToSaveChanges;
		protected System.Web.UI.WebControls.Label lblConfirmMsg;
		protected System.Web.UI.HtmlControls.HtmlGenericControl ShipmentPanel;
		protected System.Web.UI.WebControls.Button btnBaseRateYes;
		protected System.Web.UI.WebControls.Button btnBaseRateNo;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divBaseRate;
		protected System.Web.UI.HtmlControls.HtmlGenericControl divMainPanel;
		protected System.Web.UI.WebControls.Label lblBaseRate;
		protected System.Web.UI.HtmlControls.HtmlTable Table1;
		protected System.Web.UI.WebControls.Label lblAddDV;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.RequiredFieldValidator ValidServiceCode;
		protected System.Web.UI.WebControls.RequiredFieldValidator ValidRecpZip;
		protected System.Web.UI.WebControls.Button btnDGInsert;
		protected System.Web.UI.WebControls.Button btnInstPkgDetalis;
		decimal [] m_ValueList = new decimal[4]  ;
		private string stateCheck="";
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			//utility = new Utility(System.Configuration.ConfigurationSettings.AppSettings,Page.Session);
			strAppId = utility.GetAppID();
			strEnterpriseId=utility.GetEnterpriseID(); 
			if(!IsPostBack)
			{
				
				//Get values from PickUp
				if(Request.Params["CUSTTYPE"].Length>0)
				{
					strCustType=Request.Params["CUSTTYPE"]; 
					Session["strCustType"]=strCustType;
				}
				if(Request.Params["CUSTID"].Length>0)
				{
					strCustId=Request.Params["CUSTID"]; 
					Session["strCustId"]=strCustId;
				}
				if(Request.Params["SENDERZIPCD"].Length>0)
				{
					strSendZipCd=Request.Params["SENDERZIPCD"]; 
					Session["strSendZipCd"]=strSendZipCd;
				}
				if(Request.Params["BOOKINGNO"].Length>0)
				{
					strBookingNo=Request.Params["BOOKINGNO"]; 
					Session["strBookingNo"]=strBookingNo;
				}
				if(Request.Params["PICKUPDT"].Length>0)
				{
					strPickupDt=Request.Params["PICKUPDT"]; 
					Session["strPickupDt"]=strPickupDt;
				}
				if(Request.Params["MODE"].Length>0)
				{
					strMode=Request.Params["MODE"]; 
					Session["strMode"]=strMode;
				}
				if(Request.Params["PAYMODE"].Length>0)
				{
					Session["strPayMode"]=Request.Params["PAYMODE"]; 
					
				}
				//**Initializing ViewState Variables..
				ViewState["iCurrentRow"]=0;
				ViewState["Chargwt"]=0;
				ViewState["isTextChanged"]= false;
				ViewState["MovePrevious"]=false;
				ViewState["MoveNext"]=false;
				ViewState["MoveLast"]=false;
				ViewState["MoveFirst"]=false;
				ViewState["iCheckPkgDtl"]=false;
				ViewState["Day"] = "";
				ViewState["ServiceExists"] = false;
				ViewState["ServiceVASCode"] = "";
				ViewState["isHoliday"] = false;
				ViewState["AddSurcharge"] = "none";
				ViewState["isEstDlvry"]=false;
				ViewState["SetInsert"]=false;
				btnInsert.Enabled=true;
				ViewState["isStateChanged"]= false;				
				btnInsert.Enabled=true;			
				divMainPanel.Visible=true;
				divBaseRate.Visible=false;
				ShipmentPanel.Visible=false;
				ViewState["NextOperation"] = "None";
				ViewState["stateCheck"] = "none";
				if(Session["strMode"].ToString()=="EXECUTEQUERY")
				{
					ViewState["PckShpMode"]=ScreenMode.ExecuteQuery;  
					ViewState["PckShpOperation"] = Operation.None; 
					ViewState["isTextChanged"]= false;
					m_dsShpmDetls=PRBMgrDAL.GetPikupShp(strAppId,strEnterpriseId,Convert.ToInt32(Session["strBookingNo"].ToString()));
					Session["SESSION_DS2"] =m_dsShpmDetls;
					int cnt = m_dsShpmDetls.Tables[0].Rows.Count;    
					if( m_dsShpmDetls.Tables[0].Rows.Count > 0)
					{
						ViewState["stateCheck"] = "update";
						GetDatafromPickupShipment(0);
					}
					else
					{
						ViewState["stateCheck"] = "none";
						//VAS Grid...
						m_dsVAS = PRBMgrDAL.GetEmptyVASDS();
						dgVAS.EditItemIndex = -1;
						BindVASGrid();
						Session["SESSION_DS3"]=m_dsVAS;
						//Package Details Grid....
						m_dsPkgDetails = PRBMgrDAL.GetEmptyPkgDtlDS();
						dgPkgInfo.EditItemIndex = -1;
						BindPkgGrid(); 
						Session["SESSION_DS4"]=m_dsPkgDetails;
														
						//Adding to Session
						m_dsShpmDetls=PRBMgrDAL.GetEmptyPickupRequestShipment();
						Session["SESSION_DS2"]=m_dsShpmDetls; 
					}
					btnGoToFirstPage.Enabled =true;
					btnGoToFirstPage.Enabled=true;
					btnNextPage.Enabled =true;
					btnPreviousPage.Enabled =true; 
					btnGoToLastPage.Enabled =true;					
					btnInsert.Enabled=true;
					fillViewState();
					fillCashAmountViewState(); 
				}
				else
				{
					ViewState["PckShpMode"]=ScreenMode.None; 
					ViewState["PckShpOperation"]=Operation.None;  
					//VAS Grid...
					m_dsVAS = PRBMgrDAL.GetEmptyVASDS();
					dgVAS.EditItemIndex = -1;
					BindVASGrid();
					Session["SESSION_DS3"]=m_dsVAS;
					//Package Details Grid....
					m_dsPkgDetails = PRBMgrDAL.GetEmptyPkgDtlDS();
					dgPkgInfo.EditItemIndex = -1;
					BindPkgGrid(); 
					Session["SESSION_DS4"]=m_dsPkgDetails;
														
					//Adding to Session
					m_dsShpmDetls=PRBMgrDAL.GetEmptyPickupRequestShipment();
					m_dsShpmDetls=PRBMgrDAL.GetPikupShp(strAppId,strEnterpriseId,Convert.ToInt32(Session["strBookingNo"].ToString()));
					int icnt= m_dsShpmDetls.Tables[0].Rows.Count;
					if(icnt >0)
					{
						GetDatafromPickupShipment(0);
					}
					Session["SESSION_DS2"]=m_dsShpmDetls; 
					fillViewState();
					fillCashAmountViewState(); 
				}
			}
			else
			{
				m_dsShpmDetls=(DataSet)Session["SESSION_DS2"];
				m_dsVAS = (DataSet)Session["SESSION_DS3"];
				m_dsPkgDetails=(DataSet)Session["SESSIOn_DS4"];
			

			}
			string strQuery=null;
			if(Request.Params["hdnRefresh"] != null && Request.Params["hdnRefresh"] == "REFRESH")
			{
				strQuery=Request.Params["hdnRefresh"];
			}
			if(strQuery != null)
			{
				GetTotalSurcharge();
				BindVASGrid();	
			}
			lblBaseRate.Text = Utility.GetLanguageText(ResourceType.UserMessage,"USE_BASE_RATES",utility.GetUserCulture());
		}
		public void BindVASGrid()
		{
			dgVAS.DataSource = m_dsVAS;
			dgVAS.DataBind();
			Session["SESSION_DS3"] = m_dsVAS;
		}
		private void AddRowInVASGrid()
		{
			PRBMgrDAL.AddNewRowInVAS(m_dsVAS);
			//Session["SESSION_DS3"]=m_dsVAS;
		}
		private void AddRowInPkgGrid()
		{
			PRBMgrDAL.AddNewRowInPkgDS(m_dsPkgDetails);
			Session["SESSION_DS4"] = m_dsPkgDetails;
		}
		private void BindPkgGrid()
		{
			dgPkgInfo.DataSource = m_dsPkgDetails;
			dgPkgInfo.DataBind();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			base.OnInit(e);
			InitializeComponent();
			
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			this.bntClose.Click += new System.EventHandler(this.bntClose_Click);
			this.btnGoToFirstPage.Click += new System.EventHandler(this.btnGoToFirstPage_Click);
			this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
			this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
			this.btnGoToLastPage.Click += new System.EventHandler(this.btnGoToLastPage_Click);
			this.txtSerialNo.TextChanged += new System.EventHandler(this.txtSerialNo_TextChanged);
			this.txtRecipZip.TextChanged += new System.EventHandler(this.txtRecipZip_TextChanged);
			this.btnRecpZipCode.Click += new System.EventHandler(this.btnRecpZipCode_Click);
			this.txtShpSvcCode.TextChanged += new System.EventHandler(this.txtShpSvcCode_TextChanged);
			this.btnServiceCode.Click += new System.EventHandler(this.btnServiceCode_Click);
			this.txtShpDclrValue.TextChanged += new System.EventHandler(this.txtShpDclrValue_TextChanged);
			this.txtESA.TextChanged += new System.EventHandler(this.txtESA_TextChanged);
			this.txtESASurcharge.TextChanged += new System.EventHandler(this.txtESASurcharge_TextChanged);
			this.btnPopulateVAS.Click += new System.EventHandler(this.btnPopulateVAS_Click);
			this.btnBind.Click += new System.EventHandler(this.btnBind_Click);
			this.btnDGInsert.Click += new System.EventHandler(this.btnDGInsert_Click);
			this.btnInstPkgDetalis.Click += new System.EventHandler(this.btnInstPkgDetalis_Click);
			this.btnToSaveChanges.Click += new System.EventHandler(this.btnToSaveChanges_Click);
			this.btnNotToSave.Click += new System.EventHandler(this.btnNotToSave_Click);
			this.btnToCancel.Click += new System.EventHandler(this.btnToCancel_Click);
			this.btnBaseRateYes.Click += new System.EventHandler(this.btnBaseRateYes_Click);
			this.btnBaseRateNo.Click += new System.EventHandler(this.btnBaseRateNo_Click);
			this.ID = "ShipmentDetails";
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void btnInsert_Click(object sender, System.EventArgs e)
		{	
			string strPaymode = null;
			strPaymode = Session["strPayMode"].ToString();
			strBookingNo = (String)Session["strBookingNo"];
			ViewState["NextOperation"] = "Insert";
			if(strPaymode.Equals("C"))
			{
				/*if(!DomesticShipmentMgrDAL.IsPkg_detailsForCash(strAppId,strEnterpriseId,Int32.Parse(strBookingNo)))
				{*/
					fillViewState();					
					btnSave.Enabled = true;
					if(((int)ViewState["PckShpOperation"] == (int)Operation.Insert)||((int)ViewState["PckShpOperation"]==(int)Operation.Update))
					{
						lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
						ShipmentPanel.Visible = true;
						divMainPanel.Visible = false; 
						divBaseRate.Visible = false;
						ViewState["isTextChanged"] = false;
					}
					btnInsert.Enabled = false;
					Insert();
				/*}
				else
				{
					lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_CREATE_CASEMODE",utility.GetUserCulture());
					return;
				}*/
			}
			else
			{
				fillViewState();				
				btnSave.Enabled=true;
				if(((int)ViewState["PckShpOperation"]== (int)Operation.Insert)||((int)ViewState["PckShpOperation"]==(int)Operation.Update))
				{
					lblConfirmMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT ",utility.GetUserCulture());
					ShipmentPanel.Visible = true;
					divMainPanel.Visible = false; 
					divBaseRate.Visible=false;
					ViewState["isTextChanged"] = false;	
					return;
				}
				btnInsert.Enabled = false;
				Insert();
			}
		}

		private void Insert()
		{
			lblErrorMsg.Text ="";
			//Pickup_Shipment
			m_dsShpmDetls = PRBMgrDAL.GetEmptyPickupRequestShipment();  
			Session["SESSION_DS2"]=m_dsShpmDetls; 
			//Pick_VAS
			m_dsVAS = PRBMgrDAL.GetEmptyVASDS();
			Session["SESSION_DS3"]=m_dsVAS;
			AddRowInVASGrid();
			dgVAS.EditItemIndex =m_dsVAS.Tables[0].Rows.Count-1;
			//dgVAS.EditItemIndex = -1;
			BindVASGrid();
			Session["SESSION_DS3"]=m_dsVAS;
			//Package Details Grid....
			m_dsPkgDetails = PRBMgrDAL.GetEmptyPkgDtlDS();
			dgPkgInfo.EditItemIndex = -1;
			BindPkgGrid(); 
			Session["SESSION_DS4"]=m_dsPkgDetails;

			EmptyFields(); 
			ViewState["PckShpMode"]=ScreenMode.Insert ;  
			ViewState["PckShpOperation"]=Operation.Insert; 
			ViewState["SetInsert"]=true;
			ViewState["isTextChanged"]= false;
			ViewState["iCurrentRow"]=0;  
			btnGoToFirstPage.Enabled =false;
			btnGoToFirstPage.Enabled=false;
			btnNextPage.Enabled =false;
			btnPreviousPage.Enabled =false; 
			btnGoToLastPage.Enabled = false;
		}

		private void calculateFrieght()
		{
			strErrMsg="";
			if (strCustType==null)
			{
				strErrMsg = "Please Select the CustomerType";
			}
			if(strCustId ==null)
			{
				strErrMsg ="Please Enter the CustomerId";
			}
			else if(strSendZipCd=="")
			{
				strErrMsg ="Please Enter the SenderZipCode";
			}
			else if(txtRecipZip.Text.Trim() =="")
			{
				strErrMsg ="Please Enter the RecipientZipCode";
			}
			else if(txtShpSvcCode.Text.Trim()=="")
			{
				strErrMsg ="Please Enter the ServiceCode";
			}
			//TextBox txtSurcharge = (TextBox)e.Item.FindControl("txtSurcharge");


			lblErrorMsg.Text = strErrMsg;  

		}

		private void btnServiceCode_Click(object sender, System.EventArgs e)
		{
			strErrMsg = "";

			if(txtRecipZip.Text.Trim().Length > 0)
			{
				//Call the popup window.
				String sUrl = "ServiceCodePopup.aspx?PickUpDtTime="+Session["strPickupDt"]+"&DestZipCode="+txtRecipZip.Text.Trim()+"&FORMID=ShipmentDetails&SERVICECODE="+txtShpSvcCode.Text+"&SERVICEDESC="+txtShpSvcDesc.Text;
				ArrayList paramList = new ArrayList();
				paramList.Add(sUrl);
				String sScript = Utility.GetScript("PkgDetails.js",paramList);
				Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				lblErrorMsg.Text = strErrMsg;
			}
			else
			{
				strErrMsg = Utility.GetLanguageText(ResourceType.UserMessage,"RECIP_ZIP_REQ ",utility.GetUserCulture());

			}
			lblErrorMsg.Text = strErrMsg;
		}

		private void btnRecpZipCode_Click(object sender, System.EventArgs e)
		{
	
			ViewState["isTextChanged"]= false;
			strCustId =(String)Session["strCustId"];
			strCustType=(String)Session["strCustType"];
			strErrMsg = "";
			String sUrl = null;
			if(strCustType == "")
			{
				strErrMsg = "Select the Customer type.";
			}			
			//else if(strCustType == "C")
			else if((strCustType == "C") || (strCustType.Equals("0")))
			{
				//sUrl = "SndRecipPopup.aspx?FORMID="+"ShipmentDetails"+"&SENDRCPTYPE="+"R"+"&ZIPCODE="+txtRecipZip.Text.Trim()+"&CUSTTYPE="+strCustType+"&CUSTID="+strCustId+"&PAYMODE="+Session["strPayMode"].ToString()+"&DestZipCode="+txtRecipZip.Text+"&NEWCUST="+Request.Params["NEWCUST"];
				//sUrl = "ZipcodePopup.aspx?FORMID="+"ShipmentDetails"+"&SENDRCPTYPE="+"R"+"&ZIPCODE="+txtRecipZip.Text.Trim()+"&CUSTTYPE="+strCustType+"&CUSTID="+strCustId+"&PAYMODE="+Session["strPayMode"].ToString()+"&DestZipCode="+txtRecipZip.Text+"&NEWCUST="+Request.Params["NEWCUST"];
				sUrl = "ZipcodePopup.aspx?FORMID=ShipmentDetails&ZIPCODE="+txtRecipZip.Text.Trim();
			}
			else if(strCustType == "A")
			{
				//sUrl = "AgentSndRecPopup.aspx?FORMID="+"ShipmentDetails"+"&SENDRCPTYPE="+"R"+"&ZIPCODE="+txtRecipZip.Text.Trim()+"&CUSTTYPE="+strCustType+"&CUSTID="+strCustId+"&PAYMODE="+Session["strPayMode"].ToString()+"&DestZipCode="+txtRecipZip.Text+"&NEWCUST="+Request.Params["NEWCUST"] ;
				//sUrl = "ZipcodePopup.aspx?FORMID="+"ShipmentDetails"+"&SENDRCPTYPE="+"R"+"&ZIPCODE="+txtRecipZip.Text.Trim()+"&CUSTTYPE="+strCustType+"&CUSTID="+strCustId+"&PAYMODE="+Session["strPayMode"].ToString()+"&DestZipCode="+txtRecipZip.Text+"&NEWCUST="+Request.Params["NEWCUST"] ;
				sUrl = "ZipcodePopup.aspx?FORMID=ShipmentDetails&ZIPCODE="+txtRecipZip.Text.Trim();
			}
			ArrayList paramList = new ArrayList();
			paramList.Add(sUrl);

			String sScript = Utility.GetScript("PkgDetails.js",paramList);
			Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
			lblErrorMsg.Text = strErrMsg;
		}

		private void GetDataintoPickupshipment(int iRowindex)
		{
			DataRow drEach = m_dsShpmDetls.Tables[0].Rows[iRowindex] ; 
			if(txtSerialNo.Text.Trim().Length >0)
			{
				drEach["serial_no"]=Convert.ToInt16(txtSerialNo.Text); 
			}
			if(txtRecipZip.Text.Trim().Length >0)
			{
				drEach["recipient_zipcode"]=txtRecipZip.Text;
			}
			if(txtShpSvcCode.Text.Trim().Length >0)
			{
				drEach["service_code"]=txtShpSvcCode.Text;
			}
			if(txtShpDclrValue.Text.Trim().Length >0)
			{
				drEach["declare_value"]=txtShpDclrValue.Text;
			}
			if(txtShpAddDV.Text.Trim ().Length >0)
			{
				drEach["percent_dv_additional"]=txtShpAddDV.Text;
			}
	
			if (txtEstDelvdate.Text.Trim().Length >0)
			{
				DateTime strMyDateTime = System.DateTime.ParseExact(txtEstDelvdate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
				drEach["est_delivery_datetime"]=strMyDateTime;
			}
			if(txtFreightChrg.Text.Trim().Length >0)
			{
				drEach["tot_freight_charge"]=txtFreightChrg.Text;
			}
			if(txtTotVASSurChrg.Text.Trim().Length >0)
			{
				drEach["tot_vas_surcharge"]=txtTotVASSurChrg.Text;
			}
			if(txtInsSurcharge.Text.Trim().Length >0)
			{
				drEach["insurance_surcharge"]=txtInsSurcharge.Text;
			}
			if(txtESASurcharge.Text.Trim().Length >0)
			{
				drEach["esa_delivery_surcharge"]=txtESASurcharge.Text; 
			}		
			Session["SESSION_DS2"]=m_dsShpmDetls;   
		}

	
		private void btnNextPage_Click(object sender, System.EventArgs e)
		{
			MoveNext();
		}
		private void MoveNext()
		{
			ViewState["MoveNext"]=true;
			if(((int)ViewState["PckShpOperation"]== (int)Operation.Insert)||((int)ViewState["PckShpOperation"]==(int)Operation.Update))
			{
				lblConfirmMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				ShipmentPanel.Visible = true;
				divMainPanel.Visible = false; 
				divBaseRate.Visible=false;
				ViewState["isTextChanged"] = false;
			}
			
			if ((int)ViewState["iCurrentRow"]<m_dsShpmDetls.Tables[0].Rows.Count-1)
			{
				ViewState["iCurrentRow"]=(int)ViewState["iCurrentRow"]+1;
				GetDatafromPickupShipment((int)ViewState["iCurrentRow"]);
				
			}
			else
			{
				ViewState["iCurrentRow"]=m_dsShpmDetls.Tables[0].Rows.Count;
				btnNextPage.Enabled=false;
				btnPreviousPage.Enabled =true; 
				return;
			}
		}

		private void btnSave_Click(object sender, System.EventArgs e)
		{   			
			//string strBookingNo=Request.Params["BOOKINGNO"]; 
//			foreach (DataGridItem item in dgPkgInfo.Items)
//			{
//				if (item.ItemType == ListItemType.EditItem)
//				{
//					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_QNTY", utility.GetUserCulture());
//					return;	
//				}
//			}

			if(( ViewState["stateCheck"].ToString() == "update") ||(ViewState["NextOperation"].ToString() == "Insert"))
			{
				saverecord();
			}
			else
			{
				lblErrorMsg.Text= "Can't save in Query Mode";//Utility.GetLanguageText(ResourceType.UserMessage,"NO_PICKUP_SHMPT",utility.GetUserCulture());
			}
		}
		public void saverecord()
		{
			String strBookingNo=(String)Session["strBookingNo"];
			if(!DomesticShipmentMgrDAL.IsDomesticShpExist(strAppId,strEnterpriseId,Int32.Parse(strBookingNo)))
			{
				m_sdsPickupRequest = (SessionDS)Session["SESSION_DS1"];
				btnInsert.Enabled=true;				
				CaculateEstDvlryDt();
				if((bool)ViewState["isEstDlvry"]!=true)
				{
					//CalculateCashAmt();
					if(CalculateFrieght()) 
					{
						totalAmtCalc();
					}
				}
			}
			else
			{
				lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"BKG_POSTED_MODIFY_DOMESTIC",utility.GetUserCulture());

				btnSave.Enabled=false;
				return;
			}
		}
		private void FillViewArray()
		{
			if(ViewState["Qty"]!=null)
			{
				m_ValueList[0]= (int)ViewState["Qty"];
				
			}
			else
			{
				m_ValueList[0]=0;
			}
			if(ViewState["ActualWt"]!=null)
			{
				m_ValueList[1]=Convert.ToDecimal(ViewState["ActualWt"]);
				
			}
			else
			{
				
				m_ValueList[1]=0;
			}
			if((ViewState["dim_wt"])!=null)
			{
				m_ValueList[2]=Convert.ToDecimal(ViewState["dim_wt"]);
				
			}
			else
			{
				m_ValueList[2]=0;
			}
			if(ViewState["iCashAmount"]!=null)
			{
				m_ValueList[3]=Convert.ToDecimal(ViewState["iCashAmount"]);
				
			}
			else
			{
				m_ValueList[3]=0;
				
			}
			
		}
		private void SavetoDatabase()
		{
			string strMsg=null;
			m_QryList = new ArrayList();
			//m_ValueList= new Array(); 
			
			strBookingNo=(String)Session["strBookingNo"];
			if(strBookingNo.Length.Equals(0))
			{
				lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_PICKUP_SHMPT",utility.GetUserCulture());
			}
				
			string strPayemtMode = Session["strPayMode"].ToString();

			if ((int)ViewState["PckShpMode"]==(int)ScreenMode.Insert && (bool)ViewState["iCheckPkgDtl"]==false &&(int)ViewState["PckShpOperation"]==(int)Operation.Insert)
			{
				txtSerialNo.Text =Convert.ToInt32(Counter.GetNext(strAppId,strEnterpriseId,"ShpSerial_number")).ToString() ;   
			}
			if ((int)ViewState["PckShpOperation"]==(int)Operation.Insert) 
			{
				DataSet dsVAS = m_dsVAS.GetChanges();
				DataSet dsPkgDetails = m_dsPkgDetails.GetChanges();
				try
				{
					//fillCashAmountViewState();
					ViewState["iCashAmount"]=Convert.ToDecimal(ViewState["iCashAmount"])-CalculatePreviousCashAmt();
					
					GetDataintoPickupshipment((int)ViewState["iCurrentRow"]);
					
					DataSet dsShpmDetls = m_dsShpmDetls.GetChanges();
									
					if ((Session["strPayMode"].ToString()=="C")&&(dsPkgDetails== null))  
					{
						ViewState["iCheckPkgDtl"]=true;
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PKG_INFO_REQ",utility.GetUserCulture());

						return;
					}
					if (dsShpmDetls!=null)
					{
						
						if(Convert.ToDecimal(ViewState["iCashAmount"])>Convert.ToDecimal(Request.Params["ESASURCHARGE"]))
						{
							ViewState["iCashAmount"]=Convert.ToDecimal(ViewState["iCashAmount"])+(CalculateCurrentCashAmt());
						}
						else if(Convert.ToDecimal(ViewState["iCashAmount"])==0)
						{
							ViewState["iCashAmount"]=Convert.ToDecimal(ViewState["iCashAmount"])+(CalculateCurrentCashAmt()+Convert.ToDecimal(Request.Params["ESASURCHARGE"]));
						}

//						ViewState["iCashAmount"]=Convert.ToDecimal(ViewState["iCashAmount"])+(CalculateCurrentCashAmt()+Convert.ToDecimal(Request.Params["ESASURCHARGE"]));
						FillViewArray();


						
						//int iRowsEffected = PRBMgrDAL.AddShpDetails(strAppId,strEnterpriseId,strBookingNo,dsShpmDetls,dsVAS,dsPkgDetails);       
						//iRowsEffected = PRBMgrDAL.UpdatePickupRequestFromShipment(strAppId,strEnterpriseId,strBookingNo,(int)ViewState["Qty"],Convert.ToDecimal(ViewState["ActualWt"]),Convert.ToDecimal(ViewState["dim_wt"]),Convert.ToDecimal(ViewState["iCashAmount"]));
						PRBMgrDAL.GetInsertShipment(strAppId,strEnterpriseId,strBookingNo,dsShpmDetls,dsVAS,dsPkgDetails,ref m_QryList,m_ValueList);
						RBACManager.SaveRecordsInBatch(strAppId,strEnterpriseId,ref m_QryList);
						ChangePckShpState();
						ViewState["isTextChanged"]=false;
							
						m_dsShpmDetls.Tables[0].Rows[(int)ViewState["iCurrentRow"]].AcceptChanges();
						if(dsVAS!=null)
						{
							m_dsVAS.Tables[0].Rows[(int)ViewState["iCurrentRow"]].AcceptChanges();   
						}
						if(dsPkgDetails!=null)
						{
							m_dsPkgDetails.Tables[0].Rows[(int)ViewState["iCurrentRow"]].AcceptChanges();   
								
						}
						Session["SESSION_DS2"]=m_dsShpmDetls; 
						Session["SESSION_DS3"]=m_dsVAS; 
						Session["SESSION_DS4"]=m_dsPkgDetails; 
						lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"INS_SUCCESSFULLY",utility.GetUserCulture());

						return;
					}
					else
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SHMNT_DETAIL",utility.GetUserCulture());
					}
				}
				catch(ApplicationException appException)
				{
					strMsg = appException.Message;
					if(strMsg.IndexOf("duplicate key") != -1 )
					{
						strMsg = "Duplicate Key cannot update the record";
					}
					else if(strMsg.IndexOf("FK") != -1 )
					{
						strMsg = "Foreign Key Error cannot update the record";
					}
					else
					{
						strMsg = appException.Message.ToString();
					}
					lblErrorMsg.Text= strMsg; 
					return;
				}
				
			}
				//			else if((int)ViewState["PckShpOperation"]==(int)Operation.Update)
			else
			{
				DataSet dsPkgDetails=null;
				DataSet dsVAS=null;
				try
				{
					//	fillCashAmountViewState();
					//ViewState["iCashAmount"]=Convert.ToDecimal(ViewState["iCashAmount"])-(CalculatePreviousCashAmt()+Convert.ToDecimal(Request.Params["ESASURCHARGE"]));
					ViewState["iCashAmount"]=Convert.ToDecimal(ViewState["iCashAmount"])-(CalculatePreviousCashAmt());
					GetDataintoPickupshipment((int)ViewState["iCurrentRow"]);
					DataSet dsShipmentDetls = m_dsShpmDetls.GetChanges();
					if(Convert.ToDecimal(Request.Params["ESASURCHARGE"]) == Convert.ToDecimal(ViewState["iCashAmount"]))				
						ViewState["iCashAmount"]=Convert.ToDecimal(ViewState["iCashAmount"])+ CalculateCurrentCashAmt();//+Convert.ToDecimal(Request.Params["ESASURCHARGE"]);
					else
						ViewState["iCashAmount"]=Convert.ToDecimal(ViewState["iCashAmount"])+ (CalculateCurrentCashAmt()+Convert.ToDecimal(Request.Params["ESASURCHARGE"]));//+Convert.ToDecimal(Request.Params["ESASURCHARGE"]);
					
					FillViewArray();
					//int iRowsEffected=PRBMgrDAL.UpdatePickupShipment(strAppId,strEnterpriseId,Request.Params["BOOKINGNO"],Convert.ToInt32(txtSerialNo.Text) ,dsShipmentDetls,m_dsVAS,m_dsPkgDetails);   
					//iRowsEffected = PRBMgrDAL.UpdatePickupRequestFromShipment(strAppId,strEnterpriseId,strBookingNo,Convert.ToInt32(ViewState["Qty"]),Convert.ToDecimal(ViewState["ActualWt"]),Convert.ToDecimal(ViewState["dim_wt"]),Convert.ToDecimal(ViewState["iCashAmount"]));
					PRBMgrDAL.ModifyPickupShipment(strAppId,strEnterpriseId,strBookingNo,Convert.ToInt32(txtSerialNo.Text),dsShipmentDetls,m_dsVAS,m_dsPkgDetails,ref m_QryList,m_ValueList);
					RBACManager.SaveRecordsInBatch(strAppId,strEnterpriseId,ref m_QryList);
					m_dsShpmDetls.Tables[0].Rows[(int)ViewState["iCurrentRow"]].AcceptChanges();
					if(dsVAS!=null)
					{
						m_dsVAS.Tables[0].AcceptChanges();   
					}
					if(dsPkgDetails!=null) 
					{
						m_dsPkgDetails.Tables[0].AcceptChanges();   
					}
					ViewState["isTextChanged"]=false;
					ChangePckShpState();
					dgVAS.EditItemIndex= -1;  
					lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"UPD_SUCCESSFULLY",utility.GetUserCulture());

					Session["SESSION_DS2"]=m_dsShpmDetls; 
					Session["SESSION_DS3"]=m_dsVAS; 
					Session["SESSION_DS4"]=m_dsPkgDetails; 
				}
				catch(ApplicationException appException)
				{
					strMsg = appException.Message;
					if(strMsg.IndexOf("duplicate key") != -1 )
					{
						strMsg = "Duplicate Key cannot update the record";
					}
					else if(strMsg.IndexOf("FK") != -1 )
					{
						strMsg = "Foreign Key Error cannot update the record";
					}
					else
					{
						strMsg = appException.Message.ToString();
					}
					lblErrorMsg.Text= strMsg; 
					return;
				}
			}
		
		}
		private void ChangePckShpState()
		{
			if((int)ViewState["PckShpMode"] == (int) ScreenMode.Insert && (int)ViewState["PckShpOperation"] == (int)Operation.None)
			{
				ViewState["PckShpOperation"] = Operation.Insert;
			}
			else if((int)ViewState["PckShpMode"] == (int) ScreenMode.Insert && (int)ViewState["PckShpOperation"] == (int)Operation.Insert)
			{
				ViewState["PckShpOperation"] = Operation.Saved;
			}
			else if((int)ViewState["PckShpMode"] == (int) ScreenMode.Insert && (int)ViewState["PckShpOperation"] == (int)Operation.Saved)
			{
				ViewState["PckShpOperation"] = Operation.Update;
			}
			else if((int)ViewState["PckShpMode"] == (int) ScreenMode.Insert && (int)ViewState["PckShpOperation"] == (int)Operation.Update)
			{
				ViewState["PckShpOperation"] = Operation.Saved;
			}
			else if((int)ViewState["PckShpMode"] == (int) ScreenMode.ExecuteQuery && (int)ViewState["PckShpOperation"] == (int)Operation.None)
			{
				ViewState["PckShpOperation"] = Operation.Update;
			}
			else if((int)ViewState["PckShpMode"] == (int) ScreenMode.ExecuteQuery && (int)ViewState["PckShpOperation"] == (int)Operation.Update)
			{
				ViewState["PckShpOperation"] = Operation.None;
			}

			
		}

		private void GetDatafromPickupShipment(int iRowindex)
		{
			strBookingNo=(String)Session["strBookingNo"];
			DataRow drEach = m_dsShpmDetls.Tables[0].Rows[iRowindex] ;
			int cnt1 = m_dsShpmDetls.Tables[0].Rows.Count ;  

			if((drEach["serial_no"]!= null) && (!drEach["serial_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtSerialNo.Text=drEach["serial_no"].ToString();
			}
			else
			{
				txtSerialNo.Text="";
			}
				
			if((drEach["recipient_zipcode"]!= null) && (!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtRecipZip.Text=drEach["recipient_zipcode"].ToString();
			}
			else
			{
				txtRecipZip.Text="";
			}
			if((drEach["service_code"]!= null) && (!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtShpSvcCode.Text=drEach["service_code"].ToString();
				Service service = new Service();
				service.Populate(strAppId ,strEnterpriseId,txtShpSvcCode.Text);
				txtShpSvcDesc.Text = service.ServiceDescription;
			}
			else
			{
				txtShpSvcCode.Text="";
			}
			if((drEach["declare_value"]!= null) && (!drEach["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decValue =(decimal)drEach["declare_value"]; 
				txtShpDclrValue.Text=decValue.ToString("#0.00");
			}
			else
			{
				txtShpDclrValue.Text="";
			}
			if((drEach["percent_dv_additional"]!= null) && (!drEach["percent_dv_additional"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtShpAddDV.Text=drEach["percent_dv_additional"].ToString();
			}
			else
			{
				txtShpAddDV.Text="";
			}
			if((drEach["est_delivery_datetime"]!= null) && (!drEach["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime T1 = System.Convert.ToDateTime(drEach["est_delivery_datetime"]);
				String date = T1.ToString("dd/MM/yyyy");
				String time = T1.ToString("HH:mm"); 
				txtEstDelvdate.Text = date +" "+time;
			}
			else
			{
				txtEstDelvdate.Text="";

			}
			if((drEach["tot_freight_charge"]!= null) && (!drEach["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decFrieght =(decimal)drEach["tot_freight_charge"];
				txtFreightChrg.Text=decFrieght.ToString("#0.00");

			}
			else
			{
				txtFreightChrg.Text="";
			}
			if((drEach["tot_vas_surcharge"]!= null) && (!drEach["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decTotVasSurchg =(decimal)drEach["tot_vas_surcharge"]; 
				txtTotVASSurChrg.Text=decTotVasSurchg.ToString("#0.00");
			}
			else
			{
				txtFreightChrg.Text="";
			}
			if((drEach["insurance_surcharge"]!= null) && (!drEach["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decInsurSurchrge = (decimal)drEach["insurance_surcharge"];
				txtInsSurcharge.Text=decInsurSurchrge.ToString("#0.00");
			}
			else
			{
				txtInsSurcharge.Text="";
			}	
			if((drEach["esa_delivery_surcharge"]!= null) && (!drEach["esa_delivery_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				txtESASurcharge.Text=drEach["esa_delivery_surcharge"].ToString();
				txtESA.Text ="YES"; 
			}
			else
			{

				txtESA.Text ="NO"; 
				txtESASurcharge.Text="";
			}
			CalculateTotalAmt(); 
					
			//PickupVas
			if(txtSerialNo.Text!="") 
			{
				m_dsVAS = PRBMgrDAL.GetVASData(strAppId,strEnterpriseId,strBookingNo,Convert.ToInt32(txtSerialNo.Text  ));
				int cnt = m_dsVAS.Tables[0].Rows.Count;
				BindVASGrid(); 
				Session["SESSION_DS3"] =m_dsVAS;
				//PkgDtl
				m_dsPkgDetails=PRBMgrDAL.GetPkgData(strAppId,strEnterpriseId,strBookingNo,Convert.ToInt32(txtSerialNo.Text));
				cnt= m_dsPkgDetails.Tables[0].Rows.Count;
				BindPkgGrid(); 
				Session["SESSION_DS4"] =m_dsPkgDetails;
			}
			else
			{
				//VAS Grid...
				m_dsVAS = PRBMgrDAL.GetEmptyVASDS();
				dgVAS.EditItemIndex = -1;
				BindVASGrid();
				Session["SESSION_DS3"]=m_dsVAS;
				//Package Details Grid....
				m_dsPkgDetails = PRBMgrDAL.GetEmptyPkgDtlDS();
				dgPkgInfo.EditItemIndex = -1;
				BindPkgGrid(); 
				Session["SESSION_DS4"]=m_dsPkgDetails;
											
				//Adding to Session
				m_dsShpmDetls=PRBMgrDAL.GetEmptyPickupRequestShipment();
				Session["SESSION_DS2"]=m_dsShpmDetls;
			}
 
			  

			
		}
		
		private void EmptyFields()
		{
			txtSerialNo.Text  ="";
			txtShpDclrValue.Text ="";
			txtShpAddDV.Text="";
			txtCountRec.Text ="";
			txtESA.Text ="";
			txtESASurcharge.Text ="";
			txtEstDelvdate.Text ="";
			txtFreightChrg.Text="";
			txtInsSurcharge.Text ="";
			txtRecipZip.Text ="";
			txtShpSvcCode.Text ="";
			txtShpSvcDesc.Text="";
			txtShpTotAmt.Text="";
			txtTotVASSurChrg.Text ="";
			lblErrorMsg.Text ="";
			//VAS Grid...
			m_dsVAS = PRBMgrDAL.GetEmptyVASDS();
			dgVAS.EditItemIndex = -1;
			BindVASGrid();
			Session["SESSION_DS3"]=m_dsVAS;
			//Package Details Grid....
			m_dsPkgDetails = PRBMgrDAL.GetEmptyPkgDtlDS();
			dgPkgInfo.EditItemIndex = -1;
			BindPkgGrid(); 
			Session["SESSION_DS4"]=m_dsPkgDetails;
											
			//Adding to Session
			m_dsShpmDetls=PRBMgrDAL.GetEmptyPickupRequestShipment();
			Session["SESSION_DS2"]=m_dsShpmDetls; 
		}

		private void btnPreviousPage_Click(object sender, System.EventArgs e)
		{
			MovePrevious();
		}
		private void MovePrevious()
		{
			ViewState["MovePrevious"]=true;
			if(((int)ViewState["PckShpOperation"]== (int)Operation.Insert)||((int)ViewState["PckShpOperation"]==(int)Operation.Update))
			{
				lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				ShipmentPanel.Visible = true;
				divMainPanel.Visible = false; 
				divBaseRate.Visible=false;
				ViewState["isTextChanged"] = false;
			}
			
			
			if ((int)ViewState["iCurrentRow"]>0)
			{
				ViewState["iCurrentRow"]=(int)ViewState["iCurrentRow"]-1;
				GetDatafromPickupShipment((int)ViewState["iCurrentRow"]);
				
			}
			else
			{
				ViewState["iCurrentRow"]=0;
				GetDatafromPickupShipment((int)ViewState["iCurrentRow"]);
				btnNextPage.Enabled=true;
				btnPreviousPage.Enabled =false;
			}

		}

		//		private void Button1_Click(object sender, System.EventArgs e)
		//		{
		//			EditPickupshipment((int)ViewState["iCurrentRow"]);
		//			Session["SESSION_DS2"]=m_dsShpmDetls; 
		//			DataSet dsShpmDetls = m_dsShpmDetls.GetChanges();
		//		}

		
		protected void dgVAS_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((Session["strMode"].ToString()!="EXECUTEQUERY")&& ((int)ViewState["PckShpOperation"]==(int)(Operation.None)))
			{
				ViewState["PckShpMode"]= ScreenMode.ExecuteQuery; 
			}
			dgVAS.EditItemIndex = e.Item.ItemIndex;
			BindVASGrid();
			ViewState["InsertdgVAS"] = false;
		}
		protected void dgVAS_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text = "";
			TextBox txtdgVASCode = (TextBox)e.Item.FindControl("txtVASCode");
			dgVAS.EditItemIndex = -1;
			//if (txtdgVASCode.Text =="")
			//{
			//	DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
			//	//drCurrent["vas_code"] = txtVASCode.Text;
			//	drCurrent.Delete();
			//	Session["SESSION_DS3"]=m_dsVAS; 
			//}
			if((bool)ViewState["InsertdgVAS"] == true)
			{
				DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
				drCurrent.Delete();
				Session["SESSION_DS3"]=m_dsVAS; 
			}

			ViewState["InsertdgVAS"] = false;
			BindVASGrid();
		}
		protected void dgVAS_Delete(object sender, DataGridCommandEventArgs e)
		{
			DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
			drCurrent.Delete();
			
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePckShpState();
				ViewState["isTextChanged"]=true;
				btnSave.Enabled=true;
			}
			BindVASGrid();
			Session["SESSION_DS3"]=m_dsVAS; 
		}
		protected void dgVAS_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgVAS.CurrentPageIndex = e.NewPageIndex;
			BindVASGrid();
		}
		public void dgVAS_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text = "";
			btnSave.Enabled=true;
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePckShpState();
				ViewState["isTextChanged"]=true;
				btnSave.Enabled=true;
			}
			string strErrorMsg = "";
			String strChkVasCode = null;
			

			TextBox txtdgVASCode = (TextBox)e.Item.FindControl("txtVASCode");
			TextBox txtSurcharge = (TextBox)e.Item.FindControl("txtSurcharge");
			TextBox txtVASDesc = (TextBox)e.Item.FindControl("txtVASDesc");

			VAS vas = new VAS();
			vas.Populate(strAppId,strEnterpriseId,txtdgVASCode.Text.Trim());


			if((txtdgVASCode.Text!="")&&(txtSurcharge.Text=="0"))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_VAS_CHARGE",utility.GetUserCulture());
				return;
			}
			else
			{
				txtVASDesc.Text = vas.VasDescription; 
			}
			if ((txtSurcharge.Text.ToString().Equals("")) || (txtSurcharge.Text.ToString().Equals("0")))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_VAS_CHARGE",utility.GetUserCulture());
				return;
			}

			bool isServiceExcluded = TIESUtility.IsVASExcluded(strAppId,strEnterpriseId,txtdgVASCode.Text,txtRecipZip.Text);  
			if(isServiceExcluded)
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_VAS",utility.GetUserCulture());
				return;
			}

			
			if(txtdgVASCode != null)
			{
				
				strChkVasCode = vas.VASCode;
				if(strChkVasCode != null)
				{
					int cnt = m_dsVAS.Tables[0].Rows.Count;
					int i = 0;
					String strDSVASCode = "";

					for(i = 0;i<cnt;i++)
					{
						DataRow drEach = m_dsVAS.Tables[0].Rows[i];
						strDSVASCode = (String)drEach["vas_code"];
						if((strDSVASCode.Equals(strChkVasCode)) &&(i!=e.Item.ItemIndex))
						{
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"VAS_PRESENT",utility.GetUserCulture());

							dgVAS.SelectedItemStyle.ForeColor = System.Drawing.Color.Red;
							return;
						}

					}
				}
				else
				{
					strErrorMsg = "VAS Code doesn't exists";
				}
			}
			// m_dsVAS = m_dsVAS.GetChanges();
			
			
			if(strErrorMsg.Equals(""))
			{
				//TextBox txtSurcharge = (TextBox)e.Item.FindControl("txtSurcharge");
				if(txtSurcharge != null)
				{
					DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
					drCurrent["surcharge"] = Convert.ToDecimal(txtSurcharge.Text);
				}

				TextBox txtRemarks = (TextBox)e.Item.FindControl("txtRemarks");
				if(txtRemarks != null)
				{
					DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
					drCurrent["remarks"] = txtRemarks.Text;
				}
			
				TextBox txtVASCode = (TextBox)e.Item.FindControl("txtVASCode");
				if(txtVASCode != null)
				{
					DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
					drCurrent["vas_code"] = txtVASCode.Text;
					//					if(txtSerialNo.Text.Length >0)
					//					{
					//						drCurrent["serial_no"] = Convert.ToInt16(txtSerialNo.Text);
					//					}
				}

				
				if(txtVASDesc != null)
				{
					DataRow drCurrent = m_dsVAS.Tables[0].Rows[e.Item.ItemIndex];
					drCurrent["vas_description"] = txtVASDesc.Text;
				}
				
			
				dgVAS.EditItemIndex = -1;
				BindVASGrid();
				Session["SESSION_DS3"] = m_dsVAS;
			}
			
			lblErrorMsg.Text = strErrorMsg;
		}

		private void btnDGInsert_Click(object sender, System.EventArgs e)
		{
			AddRowInVASGrid();	
			dgVAS.EditItemIndex = m_dsVAS.Tables[0].Rows.Count - 1;			
			BindVASGrid();
			ViewState["InsertdgVAS"] = true;
		}
		private decimal GetTotalSurcharge()
		{
			decimal decTotSurchrg = 0;
			if(m_dsVAS != null)
			{
			
				int cnt = m_dsVAS.Tables[0].Rows.Count;
				int i = 0;
				

				for(i = 0;i<cnt;i++)
				{
					DataRow drEach = m_dsVAS.Tables[0].Rows[i];
					
					String vas_code = "";
					if((drEach["surcharge"] != null) && 
						(!drEach["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
						(drEach["surcharge"].ToString() != ""))
					{
						vas_code = drEach["vas_code"].ToString().Trim();
					}
					
					if (vas_code == "COD")
					{
						DataSet dscustInfo = SysDataMgrDAL.GetCustInforByCustID(strAppId,strEnterpriseId, 
							0, 0, Session["strCustId"].ToString().Trim()).ds;

						if(dscustInfo.Tables[0].Rows.Count > 0)
						{
							if(((dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"] != null) && 
								(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].ToString() != "")) ||
								((dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"] != null) && 
								(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].ToString() != "")))
							{
								if((dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"] != null) && 
									(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"].ToString() != ""))
								{
									decTotSurchrg += (decimal)dscustInfo.Tables[0].Rows[0]["cod_surcharge_amt"];
								}

								if((dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"] != null) && 
									(!dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(dscustInfo.Tables[0].Rows[0]["cod_surcharge_percent"].ToString() != ""))
								{
									if(!drEach["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
										decTotSurchrg += Convert.ToDecimal(drEach["surcharge"]);
								}
							}
							else
							{
								if(!drEach["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
									decTotSurchrg += Convert.ToDecimal(drEach["surcharge"]);
							}
						}
						else
						{
							if(!drEach["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
								decTotSurchrg += Convert.ToDecimal(drEach["surcharge"]);
						}
					}
					else
					{
						if(!drEach["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
							decTotSurchrg += Convert.ToDecimal(drEach["surcharge"]);
					}
				}
			}
			return decTotSurchrg;
		}

		protected void dgPkgInfo_Edit(object sender, DataGridCommandEventArgs e)
		{
			if((Session["strMode"].ToString()!="EXECUTEQUERY") && ((int)ViewState["PckShpOperation"]==(int)Operation.None))  
			{
				ViewState["PckShpMode"]= ScreenMode.ExecuteQuery; 
			}
			dgPkgInfo.EditItemIndex = e.Item.ItemIndex;
			BindPkgGrid();
			ViewState["InsertdgPkgInfo"] = false;
		}

		protected void dgPkgInfo_Cancel(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text = "";
			dgPkgInfo.EditItemIndex = -1;
			TextBox txtMPSNoChk =(TextBox)e.Item.FindControl("txtMPSNo");   
//			if(txtMPSNoChk.Text =="0")
//			{
//				DataRow drCurrent = m_dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];
//				drCurrent.Delete();
//				Session["SESSION_DS4"]=m_dsPkgDetails; 
//			}
			if((bool)ViewState["InsertdgPkgInfo"] == true)
			{
				DataRow drCurrent = m_dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];
				drCurrent.Delete();
				Session["SESSION_DS4"]=m_dsPkgDetails;	
			}

			ViewState["InsertdgPkgInfo"] = false;
		
			BindPkgGrid();
		}

		protected void dgPkgInfo_Delete(object sender, DataGridCommandEventArgs e)
		{
			//fillViewState(); 
			
			if ((m_dsPkgDetails.Tables[0].Rows.Count< 1)&&(Session["strPayMode"].ToString()=="C")) 
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PKG_INFO_REQ",utility.GetUserCulture());

				return;
				
			}
			else
			{
				if((bool)ViewState["isTextChanged"] == false)
				{
					ChangePckShpState();
					ViewState["isTextChanged"] = true;
					btnSave.Enabled=true;
				}
				
				DataRow drCurrent = m_dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];
				if (ViewState["Qty"]==null)
				{
					ViewState["Qty"]=0;
				}
				if (ViewState["ActualWt"]==null)
				{
					ViewState["ActualWt"]=(decimal)0;

				}
				if (ViewState["dim_wt"]==null)
				{
					ViewState["dim_wt"]=(decimal)0;

				}
				if (((int)ViewState["Qty"]!=(int)0)&& (ViewState["Qty"]!=null))
				{
					ViewState["Qty"]=(int)ViewState["Qty"]-(int)drCurrent["pkg_qty"];
				}
				if((Convert.ToDecimal(ViewState["ActualWt"])!=(decimal)(0))&&(ViewState["ActualWt"]!=null))
				{
					ViewState["ActualWt"]=(decimal)(ViewState["ActualWt"])-(decimal)drCurrent["tot_wt"];
				}
				if((Convert.ToDecimal(ViewState["dim_wt"])!=(decimal)(0))&&(ViewState["dim_wt"]!=null))
				{
					ViewState["dim_wt"]=(decimal)(ViewState["dim_wt"])-(decimal)drCurrent["tot_dim_wt"];
				}
				ViewState["strStatus"] ="D";
				drCurrent.Delete();
				dgPkgInfo.EditItemIndex = -1;
				// m_dsPkgDetails.GetChanges();
				//	int cnt = m_dsPkgDetails.Tables[0].Rows.Count;   
				Session["SESSION_DS4"]=m_dsPkgDetails; 
				BindPkgGrid();	
			}
		}

		protected void dgPkgInfo_PageChange(object sender, DataGridPageChangedEventArgs e)
		{
			dgPkgInfo.CurrentPageIndex = e.NewPageIndex;
			BindPkgGrid();
		}

		public void dgVAS_Button(object sender, DataGridCommandEventArgs e)
		{
			String strCmdNm = e.CommandName;
			if(strCmdNm.Equals("Search"))
			{
				TextBox txtVASCode = (TextBox)e.Item.FindControl("txtVASCode");
				String strtxtVASCode = null;
				if(txtVASCode != null)
				{
					strtxtVASCode = txtVASCode.ClientID;
				}
				
				TextBox txtVASDesc = (TextBox)e.Item.FindControl("txtVASDesc");
				String strtxtVASDesc = null;
				if (txtVASDesc != null)
				{
					strtxtVASDesc = txtVASDesc.ClientID;
				}

				TextBox txtSurcharge = (TextBox)e.Item.FindControl("txtSurcharge");
				String strtxtSurcharge = null;
				if (txtSurcharge != null)
				{
					strtxtSurcharge = txtSurcharge.ClientID;
				}
				if(e.Item.ItemType == ListItemType.EditItem)
				{									
					String sUrl = "VASPopup.aspx?VASID="+strtxtVASCode+"&VASDESC="+strtxtVASDesc+"&VASSURCHARGE="+strtxtSurcharge+"&FORMID="+"ShipmentDetails+&strDestZipCode="+txtRecipZip.Text;
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScript("PkgDetails.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
				}
			}
		}

		private void btnInstPkgDetalis_Click(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text = "";
			AddRowInPkgGrid();	
			ViewState["InsertdgPkgInfo"] = true;
			//fillViewState();
			dgPkgInfo.EditItemIndex = m_dsPkgDetails.Tables[0].Rows.Count - 1;
			BindPkgGrid();
		}

		private bool CalculateFrieght()
		{
			decimal decChargeWt=0;
			bool blStatus=false;			
			strCustType = (String)Session["strCustType"];
			strCustId =(String)Session["strCustId"];
			strSendZipCd=(String)Session["strSendZipCd"];
			strBookingNo=(String)Session["strBookingNo"];
			if (strCustType.Equals(null))
			{
				lblErrorMsg.Text  = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_CUST_TYPE",utility.GetUserCulture());

				return blStatus;
			}
			if (strCustId.Equals(null))
			{
				lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_CUST_ID",utility.GetUserCulture()); 
				return blStatus;
			}
			if(strSendZipCd.Equals(null))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SEND_ZIP",utility.GetUserCulture()); 
				return blStatus;

			}
			if(txtRecipZip.Text=="")
			{
				lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_RECIP_ZIP",utility.GetUserCulture()); 
				return blStatus;

			}
			if(m_dsPkgDetails!=null)
			{
				int cnt = m_dsPkgDetails.Tables[0].Rows.Count;
				int i = 0;
		
				for(i = 0;i<cnt;i++)
				{
					DataRow drEach = m_dsPkgDetails.Tables[0].Rows[i];
					decChargeWt += Convert.ToDecimal(drEach["chargeable_wt"]);
				}
			}
			if(Convert.ToDecimal(decChargeWt)==0)
			{
				txtFreightChrg.Text="0.00";

				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_PKG_DETAILS",utility.GetUserCulture());

				return blStatus;
			}
			
			Zipcode zipcode = new Zipcode();
			//Get the Origin Zone Code
			zipcode.Populate(strAppId,strEnterpriseId,strSendZipCd);
			String strOrgZone = zipcode.ZoneCode;

			//Get the desctination Zone Code
			Zipcode desZipCode = new Zipcode();
			desZipCode.Populate(strAppId,strEnterpriseId,txtRecipZip.Text);
			String strDestnZone = desZipCode.ZoneCode;
			if (strDestnZone == null) 
			{
				Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECIP_ZIP",utility.GetUserCulture());
				return false;
			}
			if((strOrgZone !="")&&(strDestnZone !=""))
			{
				QuotationData structQtnData;
				String strQuotation = null;
				if(strCustType.Equals("C"))
				{
					structQtnData = TIESUtility.CustomerQtnActive(strAppId,strEnterpriseId,strCustId.ToString());
					strQuotation = structQtnData.strQuotationNo;
				}
				if(strCustType.Equals("A"))
				{
					structQtnData = TIESUtility.AgentQtnActive(strAppId,strEnterpriseId,strCustId.ToString());
					strQuotation = structQtnData.strQuotationNo;
				}
				if(strQuotation == null)
				{
					divBaseRate.Visible=true;
					divMainPanel.Visible=false;
					ShipmentPanel.Visible=false;
					return blStatus;
				}
			}

			float fCalcFrgtChrg = 0;

			try
			{
				//Compute the Freight Charge	
				
				fCalcFrgtChrg = TIESUtility.ComputeFreightCharge(strAppId,strEnterpriseId,
					strCustId.ToString(),strCustType.ToString() ,strOrgZone.ToString(),
					strDestnZone.ToString(),(float)decChargeWt,txtShpSvcCode.Text,
					strSendZipCd, txtRecipZip.Text.Trim());
				//				
				if((m_dsPkgDetails != null)&& (m_dsPkgDetails.Tables[0].Rows.Count > 0) && (fCalcFrgtChrg.ToString("#0.00").Equals("0.00")))
				{
					txtFreightChrg.Text = fCalcFrgtChrg.ToString("#0.00");
					//Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,false,m_moduleAccessRights);
					lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_RATES",utility.GetUserCulture());

					return blStatus;
				}
				if((m_dsPkgDetails == null))
				{
					txtFreightChrg.Text = fCalcFrgtChrg.ToString("#0.00");
					lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_PKG_DETAILS",utility.GetUserCulture());
					return blStatus;
				}
				txtFreightChrg.Text = fCalcFrgtChrg.ToString("#0.00");
				blStatus=true;
				return blStatus;

				
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				lblErrorMsg.Text = strMsg;
				return blStatus;
			}
				
		}

		private bool CalculateFrieghtBaseRate()
		{
			decimal decChargeWt=0;	
			bool blStatus=false;
			strCustType = (String)Session["strCustType"];
			strCustId =(String)Session["strCustId"];
			strSendZipCd=(String)Session["strSendZipCd"];
			strBookingNo=(String)Session["strBookingNo"];

			if (strCustType.Equals(null))
			{
				lblErrorMsg.Text  = Utility.GetLanguageText(ResourceType.UserMessage,"SEL_CUST_TYPE",utility.GetUserCulture());

				return blStatus;
			}
			if (strCustId.Equals(null))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SEL_CUST_TYPE",utility.GetUserCulture());

				return blStatus;
			}
			if(strSendZipCd.Equals(null))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_SEND_ZIP",utility.GetUserCulture());
				return blStatus;

			}
			if(txtRecipZip.Text=="")
			{
				lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_RECIP_ZIP",utility.GetUserCulture());
				return blStatus;

			}
			if(m_dsPkgDetails!=null)
			{
				int cnt = m_dsPkgDetails.Tables[0].Rows.Count;
				int i = 0;
		
				for(i = 0;i<cnt;i++)
				{
					DataRow drEach = m_dsPkgDetails.Tables[0].Rows[i];
					decChargeWt += Convert.ToDecimal(drEach["chargeable_wt"]);
				}
			}
			if(Convert.ToDecimal(decChargeWt)==0)
			{
				txtFreightChrg.Text="0.00";
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PKG_REQ",utility.GetUserCulture());

				return blStatus;
			}
			
			Zipcode zipcode = new Zipcode();
			//Get the Origin Zone Code
			zipcode.Populate(strAppId,strEnterpriseId,strSendZipCd);
			String strOrgZone = zipcode.ZoneCode;


			//Get the desctination Zone Code
			zipcode.Populate(strAppId,strEnterpriseId,txtRecipZip.Text);
			String strDestnZone = zipcode.ZoneCode;
			float fCalcFrgtChrg = 0;
			try
			{
				//Compute the Freight Charge	
				
				fCalcFrgtChrg = TIESUtility.ComputeFreightCharge(strAppId,strEnterpriseId,strCustId.ToString(),
					strCustType.ToString() ,strOrgZone.ToString(),strDestnZone.ToString(),
					(float)decChargeWt,txtShpSvcCode.Text,strSendZipCd, txtRecipZip.Text.Trim());
				//				
				if((m_dsPkgDetails != null)&& (m_dsPkgDetails.Tables[0].Rows.Count > 0) && (fCalcFrgtChrg.ToString("#0.00").Equals("0.00")))
				{
					txtFreightChrg.Text = fCalcFrgtChrg.ToString("#0.00");
					btnSave.Enabled=false;
					lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_RATES",utility.GetUserCulture());

					return blStatus;
				}
				if((m_dsPkgDetails == null))
				{
					txtFreightChrg.Text = fCalcFrgtChrg.ToString("#0.00");
					lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_PKG_DETAILS",utility.GetUserCulture());
					return blStatus;
				}
				txtFreightChrg.Text = fCalcFrgtChrg.ToString("#0.00");
				blStatus=true;
				return blStatus;
				
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				lblErrorMsg.Text = strMsg;
				return blStatus;
			}	
		}

		public void dgPkgInfo_Update(object sender, DataGridCommandEventArgs e)
		{
			lblErrorMsg.Text = "";
			int qty =0;
			decimal tot_wt=0;
			decimal dim_wt=0;
			btnSave.Enabled=true;
					
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePckShpState();
				ViewState["isTextChanged"]=true;
				btnSave.Enabled =true;
			}
			String strChkMpsCode = null;
			TextBox txtMPSNoChk = (TextBox)e.Item.FindControl("txtMPSNo");
	
			if(txtMPSNoChk != null)
			{
				strChkMpsCode = txtMPSNoChk.Text;
				if(strChkMpsCode != null)
				{
					int cnt = m_dsPkgDetails.Tables[0].Rows.Count;
					int i = 0;
					String strDSMpsCode = "";

					for(i = 0;i<cnt;i++)
					{
						DataRow drEach = m_dsPkgDetails.Tables[0].Rows[i]  ;
						strDSMpsCode = (String)drEach["mps_no"];
						if((strDSMpsCode.Equals(strChkMpsCode))&&(i!=e.Item.ItemIndex))  
						{
							lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_MPS_CODE_REQ",utility.GetUserCulture());

							return;
						}
					}
				 
					DataRow drEach1 = m_dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex] ;
					qty = (int)drEach1["pkg_qty"];
					tot_wt =(decimal)drEach1["tot_wt"];
					dim_wt =(decimal)drEach1["tot_dim_wt"];
				}
			}
		
			msTextBox txtMPSNo = (msTextBox)e.Item.FindControl("txtMPSNo");
			if(txtMPSNo != null)
			{
				DataRow drCurrent = m_dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];
				drCurrent["mps_no"] = txtMPSNo.Text;
			}
							
			msTextBox txtLength = (msTextBox)e.Item.FindControl("txtLength");
			if(txtLength != null)
			{
				DataRow drCurrent = m_dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];
				drCurrent["pkg_length"] = txtLength.Text;
				iLength =  Convert.ToDecimal((txtLength.Text.ToString()));
			}

			msTextBox txtBreadth = (msTextBox)e.Item.FindControl("txtBreadth");
			if(txtBreadth != null)
			{
				DataRow drCurrent = m_dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];
				drCurrent["pkg_breadth"] = txtBreadth.Text;	
				iBreadth = Convert.ToDecimal(txtBreadth.Text.ToString());
			}

			msTextBox txtHeight = (msTextBox)e.Item.FindControl("txtHeight");
			if(txtBreadth != null)
			{
				DataRow drCurrent = m_dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];
				drCurrent["pkg_height"] = txtHeight.Text;	
				iHeight = Convert.ToDecimal(txtHeight.Text.ToString());
			}
			
			Label lblVolume = (Label)e.Item.FindControl("lblVolume");
			if((iLength != 0 ) && (iBreadth != 0) && (iHeight != 0))
			{
				lblVolume.Text = Convert.ToString(Math.Round((iLength * iBreadth * iHeight),2));
				DataRow drCurrent = m_dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];
				drCurrent["pkg_volume"] = lblVolume.Text;
			}

			msTextBox txtQty = (msTextBox)e.Item.FindControl("txtQty");
			if(txtQty != null)
			{
				if (Convert.ToInt16(txtQty.Text.ToString()) == 0)
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_QNTY", utility.GetUserCulture());
					return;
				}
				DataRow drCurrent = m_dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];
				drCurrent["pkg_qty"] = txtQty.Text;	
				iQty = Convert.ToInt16(txtQty.Text.ToString());	
			}
			else
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage, "PLS_ENTER_QNTY", utility.GetUserCulture());
				return;
			}			

			ViewState["Qty"]=(int)ViewState["Qty"]-qty;
			ViewState["Qty"]=(int)ViewState["Qty"]+iQty;
						
			msTextBox txtWeight = (msTextBox)e.Item.FindControl("txtWeight");
			if(txtWeight != null)
			{
				DataRow drCurrent = m_dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];
				drCurrent["pkg_wt"] = txtWeight.Text;	
				iWeight = Convert.ToDecimal(txtWeight.Text.ToString());	
			}
			

			Label lblTotWt = (Label)e.Item.FindControl("lblTotWt");
			if((iQty > 0 ) && (iWeight > 0))
			{
				lblTotWt.Text = Convert.ToString((iQty * iWeight));
				DataRow drCurrent = m_dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];
				drCurrent["tot_wt"] = Math.Round(Convert.ToDecimal(lblTotWt.Text),2);	
				decTotalWt = Convert.ToDecimal(lblTotWt.Text);
			}

			ViewState["ActualWt"]=Convert.ToDecimal(ViewState["ActualWt"])-tot_wt;
			ViewState["ActualWt"]=Convert.ToDecimal(ViewState["ActualWt"])+decTotalWt;


			Label lblDimWt = (Label)e.Item.FindControl("lblDimWt");
			if((iLength != 0 ) && (iBreadth != 0) && (iHeight != 0))
			{
				lblDimWt.Text = Convert.ToString((iLength * iBreadth * iHeight));
				DataRow drCurrent = m_dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];
				Enterprise enterprise = new Enterprise();
				enterprise.Populate(utility.GetAppID(),utility.GetEnterpriseID());
				decimal iVolume = iLength * iBreadth * iHeight;
				float fDensityFactor = (float)enterprise.DensityFactor;
				if(fDensityFactor > 0)
				{
					//fDimWt = (float)iVolume/fDensityFactor;
					fDimWt = (float)iQty*(float)iVolume/fDensityFactor;
				}
				else
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"ZERO_DENSITY",utility.GetUserCulture());

					//					DataRow drCurrent = dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];
					//					drCurrent.Delete();
					//					BindPkgGrid();
					return;
				}

				drCurrent["tot_dim_wt"] = Math.Round(fDimWt,2);

				ViewState["dim_wt"]=Convert.ToDecimal(ViewState["dim_wt"])-dim_wt;
				ViewState["dim_wt"]=Convert.ToDecimal(ViewState["dim_wt"])+(decimal)fDimWt;
			
			}

			//Calculate the Chargeable weight

			Label lblChargwt = (Label)e.Item.FindControl("lblChargwt");
			DataRow drRow = m_dsPkgDetails.Tables[0].Rows[e.Item.ItemIndex];

			if((String)Session["strCustType"] == "C")
			{
				Customer customer = new Customer();
				customer.Populate(strAppId,strEnterpriseId,(String)Session["strCustId"]);
				strApplyDimWt = customer.ApplyDimWt;
			}
			else if((String)Session["strCustType"] == "A")
			{
				Agent agent = new Agent();
				agent.Populate(strAppId,strEnterpriseId,(String)Session["strCustId"]);
				strApplyDimWt = agent.ApplyDimWt;
			}

			if(strApplyDimWt == "Y")
			{
				if(decTotalWt < (decimal)fDimWt)
				{
					decChrgeable = (decimal)fDimWt;
				}
				else
				{
					decChrgeable = (decimal)decTotalWt;
				}
			}
			else
			{
				decChrgeable = (decimal)decTotalWt;
			}

			if(decChrgeable.ToString() != null)
			{
				drRow["chargeable_wt"] = decChrgeable;	
			}
			
			ViewState["Chargwt"]=Convert.ToDecimal(ViewState["Chargwt"])+decChrgeable; 
			
			dgPkgInfo.EditItemIndex = -1;
			BindPkgGrid();
			Session["SESSION_DS4"] = m_dsPkgDetails;
			Logger.LogDebugInfo("PackageDetails","dgPkgInfo_Update","Info001","updating data grid... : "+txtMPSNo);
		}
	
		public void dgVAS_ItemDataBound(object sender, DataGridItemEventArgs e)
		{
			if(e.Item.ItemType == ListItemType.EditItem)
			{
				e.Item.Cells[3].Enabled = true;	
			}
			else
			{
				e.Item.Cells[3].Enabled = false;
			}

		}
		private void txtShpAddDV_TextChanged(object sender, System.EventArgs e)
		{
			
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePckShpState();
				ViewState["isTextChanged"]=true;
				btnSave.Enabled=true;
			}
			
			if((txtShpDclrValue.Text.Equals(null)))
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DECLARE",utility.GetUserCulture());

				return;
			}
			if((txtShpAddDV.Text.Equals(null)))
			{
				lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ADDNL_DV",utility.GetUserCulture());
				return;
			}
			if((txtShpDclrValue.Text!=null)&&(txtShpAddDV.Text!=null))
			{
				Enterprise enterprise = new Enterprise();
				string strMaxAmt= enterprise.MaxInsuranceAmt.ToString() ; 
				decimal decFreeInsAmt = 0;
				decFreeInsAmt = enterprise.FreeInsuranceAmount;
				DomesticShipmentMgrBAL domesticBAL = new DomesticShipmentMgrBAL();
				Customer customer = new Customer();
				customer.Populate(strAppId,strEnterpriseId, Session["strCustId"].ToString().Trim());

				//decimal decInsSurchrg = domesticBAL.ComputeInsSurcharge(Convert.ToDecimal(txtShpDclrValue.Text.Trim()),Convert.ToDecimal(txtShpAddDV.Text.Trim()),Convert.ToDecimal(strMaxAmt),decFreeInsAmt);
				decimal decInsSurchrg = domesticBAL.ComputeInsSurchargeByCust(strAppId, strEnterpriseId,
					Convert.ToDecimal(txtShpDclrValue.Text.Trim()), (String)Session["strCustId"], customer);

				//Display value in Insurance Surcharge Field also
				txtInsSurcharge.Text = decInsSurchrg.ToString("#0.00");
			}
			CalculateTotalAmt();
			
		}
		private void CalculateTotalAmt()
		{
			//Calculate the total Amount
			decimal decInsuranceChrg = 0;
			if(txtInsSurcharge.Text.Trim().Length > 0)
			{
				decInsuranceChrg = Convert.ToDecimal(txtInsSurcharge.Text.Trim());
			}

			decimal decFrghtChrg = 0;
			if (txtFreightChrg.Text.Trim().Length > 0)
			{
				decFrghtChrg = Convert.ToDecimal(txtFreightChrg.Text.Trim());
			}

			decimal decTotVASSurcharge = 0;
			if(txtTotVASSurChrg.Text.Trim().Length > 0)
			{
				decTotVASSurcharge = Convert.ToDecimal(txtTotVASSurChrg.Text.Trim());
			}

			decimal decESASSurcharge = 0;
			if(txtESASurcharge.Text.Length > 0)
			{
				decESASSurcharge = Convert.ToDecimal(txtESASurcharge.Text);
			}

			decimal decTotalAmt = decTotVASSurcharge + decFrghtChrg + decInsuranceChrg+decESASSurcharge;	
			//txtShpTotAmt.Text = decTotalAmt.ToString("#0.00");
			txtShpTotAmt.Text=TIESUtility.Round_Enterprise_Currency(utility.GetAppID(),utility.GetEnterpriseID(),decTotalAmt);
		}
		private void txtShpDclrValue_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePckShpState();
				ViewState["isTextChanged"]=true;
				btnSave.Enabled=true;
			}
			
			if(txtShpDclrValue.Text != null) 
			{

				DomesticShipmentMgrBAL domesticBAL = new DomesticShipmentMgrBAL();
				txtEstDelvdate.Text =  domesticBAL.CalcDlvryDtTime(strAppId,strEnterpriseId,System.DateTime.ParseExact(Session["strPickupDt"].ToString() ,"dd/MM/yyyy HH:mm",null),txtShpSvcCode.Text,txtRecipZip.Text).ToString();
				Enterprise enterprise = new Enterprise();
				enterprise.Populate(strAppId,strEnterpriseId);
				string strMaxAmt = enterprise.MaxInsuranceAmt.ToString();
				decimal decFreeInsAmt = 0;
				decFreeInsAmt = enterprise.FreeInsuranceAmount;
				txtShpAddDV.Text = enterprise.InsPercSurchrg.ToString();
				//decimal decInsSurchrg = domesticBAL.ComputeInsSurcharge(Convert.ToDecimal(txtShpDclrValue.Text.Trim()),Convert.ToDecimal(txtShpAddDV.Text.Trim()),Convert.ToDecimal(strMaxAmt),decFreeInsAmt);

				Customer customer = new Customer();
				customer.Populate(strAppId,strEnterpriseId, (String)Session["strCustId"]);
				decimal decInsSurchrg = domesticBAL.ComputeInsSurchargeByCust(strAppId, strEnterpriseId,
						Convert.ToDecimal(txtShpDclrValue.Text.Trim()), (String)Session["strCustId"],customer);


				txtInsSurcharge.Text = decInsSurchrg.ToString("#0.00");
				CalculateTotalAmt();
			}
			else
			{
				lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"PLS_ENTER_DECLARE",utility.GetUserCulture());
				return;
			}
		
		}

		private void btnPopulateVAS_Click(object sender, System.EventArgs e)
		{
			strCustType = (String)Session["strCustType"];
			strCustId =(String)Session["strCustId"];
			strSendZipCd=(String)Session["strSendZipCd"];
			strBookingNo=(String)Session["strBookingNo"];
			QuotationData quotationData;
			quotationData.iQuotationVersion = 0;
			quotationData.strQuotationNo = null;
			DataSet dsPopulateVAS = null;
			strErrMsg = "";

			if((strCustType!= null) && (strCustId.Length > 0))
			{
				if(strCustType == "C")
				{
					try
					{
						quotationData = TIESUtility.CustomerQtnActive(strAppId,strEnterpriseId,strCustId);		
					}
					catch(ApplicationException appException)
					{
						lblErrorMsg.Text = appException.Message;
						return;
					}
				}
				else if(strCustType == "A")
				{
					try
					{
						quotationData = TIESUtility.AgentQtnActive(strAppId,strEnterpriseId,strCustId);
					}
					catch(ApplicationException appException)
					{
						lblErrorMsg.Text = appException.Message.ToString();
						return;
					}
				}
				
				//Get VAS for the active quotation
				if(quotationData.strQuotationNo != null)
				{
					String strVASList = GetGridVasCode();
					try
					{
						dsPopulateVAS = QuotationVAS.GetQuotationVAS(strAppId,strEnterpriseId,strCustId,quotationData.strQuotationNo,quotationData.iQuotationVersion,strVASList,strCustType,txtRecipZip.Text);
					}
					catch(ApplicationException appException)
					{
						lblErrorMsg.Text = appException.Message;
					}
					dsPopulateVAS.Tables[0].Columns.Add(new DataColumn("isSelected", typeof(bool)));
					int cnt = dsPopulateVAS.Tables[0].Rows.Count;
					int i = 0;
					
					for(i=0;i<cnt;i++)
					{
						DataRow drCurrent = dsPopulateVAS.Tables[0].Rows[i];
						drCurrent["isSelected"] = false;
					}
					Session["SESSION_DS5"] = dsPopulateVAS;

					String sUrl = "QuotationVASPopup.aspx?FORMID=ShipmentDetails+&strDestZipCode="+txtRecipZip.Text;
					ArrayList paramList = new ArrayList();
					paramList.Add(sUrl);
					String sScript = Utility.GetScript("PkgDetails.js",paramList);
					Utility.RegisterScriptString(sScript,"ShowPopupScript",this.Page);
					if( (int)ViewState["PckShpMode"]!=(int)ScreenMode.Insert)
					{
						ChangePckShpState();  
						btnSave.Enabled=true;
					}
				}
				else
				{
					strErrMsg = Utility.GetLanguageText(ResourceType.UserMessage,"NO_ACT_AUOTN",utility.GetUserCulture());

				}
				
			}
			else if((strCustType == null) || (strCustId.Length == 0))
			{
				strErrMsg = Utility.GetLanguageText(ResourceType.UserMessage,"SEL_CUST_TYPE",utility.GetUserCulture());
			}
			lblErrorMsg.Text = strErrMsg;
		}
		private String GetGridVasCode()
		{
			String strVasCode = "";
			int i = 0;

			for(i=0;i<dgVAS.Items.Count;i++)
			{
				Label lblVASCode = (Label)dgVAS.Items[i].FindControl("lblVASCode");
				if(lblVASCode != null)
				{
					if(i == dgVAS.Items.Count - 1)
					{
						strVasCode = strVasCode+lblVASCode.Text;
					}
					else
					{
						strVasCode = strVasCode+lblVASCode.Text+":";
					}
				}
			}
			return strVasCode;
		}

		private void btnBind_Click(object sender, System.EventArgs e)
		{
			GetTotalSurcharge();

			BindVASGrid();	
		}

		private void dgVAS_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void BtnDelete_Click(object sender, System.EventArgs e)
		{
			int iPkgQty =0;
			decimal decTotWt=0;
			decimal decDimWt=0;
			m_QryList = new ArrayList();
			string strMsg=null;
			btnGoToFirstPage.Enabled =false;
			btnGoToFirstPage.Enabled=false;
			btnNextPage.Enabled =false;
			btnPreviousPage.Enabled =false; 
			btnGoToLastPage.Enabled = false; 
			btnSave.Enabled=true;					
			fillCashAmountViewState();
			strBookingNo=(String)Session["strBookingNo"];
			if(strBookingNo=="")
			{
				lblErrorMsg.Text= Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_SERIAL",utility.GetUserCulture());

				return;
			}
			if((txtSerialNo.Text=="")&&(txtRecipZip.Text==""))
			{
				lblErrorMsg.Text=Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_SERIAL",utility.GetUserCulture());
				return;
			}
			else
			{
				if(((int)ViewState["PckShpOperation"]== (int)Operation.Insert)||((int)ViewState["PckShpOperation"]==(int)Operation.Update))
				{
					lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
					ShipmentPanel.Visible = true;
					divMainPanel.Visible = false; 
					divBaseRate.Visible=false; 
					ViewState["isTextChanged"] = false;
						
					return;
				}
			}
				
			if(m_dsPkgDetails != null)
			{
			
				int cnt = m_dsPkgDetails.Tables[0].Rows.Count;
				int i = 0;
				

				for(i = 0;i<cnt;i++)
				{
					DataRow drEach = m_dsPkgDetails.Tables[0].Rows[i];
					iPkgQty += Convert.ToInt32(drEach["pkg_qty"]);
					decTotWt += Convert.ToDecimal(drEach["tot_wt"]);
					decDimWt += Convert.ToDecimal(drEach["tot_dim_wt"]);
				}
			}

			try
			{
				if((strBookingNo!="")&&(txtSerialNo.Text!=""))
				{
					if (Convert.ToDecimal(ViewState["iCashAmount"])!=Convert.ToDecimal(0))
					{
						//ViewState["iCashAmount"]=(decimal)ViewState["iCashAmount"]-CalculateCurrentCashAmt()+Convert.ToDecimal(Request.Params["ESASURCHARGE"]);
						ViewState["iCashAmount"]=(decimal)ViewState["iCashAmount"]-CalculateCurrentCashAmt();
					}
					ViewState["Qty"]=Convert.ToInt32(ViewState["Qty"])-iPkgQty ;
					ViewState["ActualWt"]=Convert.ToDecimal(ViewState["ActualWt"])-decTotWt;
					ViewState["dim_wt"]=Convert.ToDecimal(ViewState["dim_wt"])-decDimWt;

					FillViewArray(); 
					PRBMgrDAL.DeleteShpDetails(strAppId,strEnterpriseId,strBookingNo,Convert.ToInt32(txtSerialNo.Text),ref m_QryList,m_ValueList);
					RBACManager.SaveRecordsInBatch(strAppId,strEnterpriseId,ref m_QryList);
					//int iRowsEffected = PRBMgrDAL.UpdatePickupRequestFromShipment(strAppId,strEnterpriseId,strBookingNo,(int)ViewState["Qty"],Convert.ToDecimal(ViewState["ActualWt"]),Convert.ToDecimal(ViewState["dim_wt"]),Convert.ToDecimal(ViewState["iCashAmount"]));
					
				}
			}
			catch(ApplicationException appException)
			{
				strMsg = appException.Message;
				if(strMsg.IndexOf("FK") != -1 )
				{
					strMsg = "WithOut deleting the PickupVAS & PickupPackages you cannot delete the record";
				}
				else
				{
					strMsg = appException.Message.ToString();
				}
				lblErrorMsg.Text = strMsg; 
				ViewState["PRBOperation"]=Operation.None;  
				return;
			}

			 
			EmptyFields();
			lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"DLD_SUCCESSFULLY",utility.GetUserCulture());
			//Pickup_Vas
			m_dsVAS = PRBMgrDAL.GetEmptyVASDS();
			dgVAS.EditItemIndex = -1;
			BindVASGrid();
			Session["SESSION_DS3"]=m_dsVAS;
			//Package Details Grid....
			m_dsPkgDetails = PRBMgrDAL.GetEmptyPkgDtlDS();
			dgPkgInfo.EditItemIndex = -1;
			BindPkgGrid(); 
			Session["SESSION_DS4"]=m_dsPkgDetails;
 
		}

		private void btnGoToFirstPage_Click(object sender, System.EventArgs e)
		{
			MoveFirst();
			
		}
		private void MoveFirst()
		{
			ViewState["MoveFirst"]=true;
			if(((int)ViewState["PckShpOperation"]== (int)Operation.Insert)||((int)ViewState["PckShpOperation"]==(int)Operation.Update))
			{
				lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				ShipmentPanel.Visible = true;
				divMainPanel.Visible = false; 
				divBaseRate.Visible=false;
				ViewState["isTextChanged"] = false;
			}
			
			ViewState["iCurrentRow"]=0;
			GetDatafromPickupShipment((int)ViewState["iCurrentRow"]);

		}

		private void btnGoToLastPage_Click(object sender, System.EventArgs e)
		{
			MoveLast();
			
		}
		private void MoveLast()
		{
			ViewState["MoveLast"]=true;
			if(((int)ViewState["PckShpOperation"]== (int)Operation.Insert)||((int)ViewState["PckShpOperation"]==(int)Operation.Update))
			{
				lblConfirmMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SAVE_PROMPT",utility.GetUserCulture());
				ShipmentPanel.Visible = true;
				divMainPanel.Visible = false; 
				divBaseRate.Visible=false;
				ViewState["isTextChanged"] = false;				
				//return;
			}
			
			ViewState["iCurrentRow"]=m_dsShpmDetls.Tables[0].Rows.Count-1;
			GetDatafromPickupShipment((int)ViewState["iCurrentRow"]);
		}
		private void txtRecipZip_TextChanged(object sender, System.EventArgs e)
		{
			//			if ((bool)ViewState["isTextChanged"]==false)
			//			{
			//				ChangePckShpState();
			ViewState["isTextChanged"]=true;
			//			}
			
			string strZipCode=null;
			decimal strESASurcharge=0;
			string strESAApplied=null;
			lblErrorMsg.Text="";
			Zipcode  zp = new Zipcode();
			zp.Populate(strAppId,strEnterpriseId,txtRecipZip.Text);
			strZipCode = zp.ZipCode ;
			if (strZipCode==null)
			{
				txtRecipZip.Text = "";
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_ZIP",utility.GetUserCulture());
				return;
			}
			strESASurcharge = zp.EASSurcharge;
 
			Customer customer = new Customer() ;
			customer.Populate(strAppId ,strEnterpriseId,(String)Session["strCustId"]);    
			strESAApplied = customer.ESASurcharge;

			if ((Session["strPayMode"].ToString()=="C") || (Session["strPayMode"].ToString()=="R"))
			{
				if(strESASurcharge!=0 && strESAApplied=="Y" )
				{
					txtESA.Text="YES"; 
					
					txtESASurcharge.Text = strESASurcharge.ToString("#0.00"); 
				}
				else 
				{
					txtESA.Text="NO"; 
					txtESASurcharge.Text = ""; 
				}
			}
			if(((String)Session["strCustType"]=="")&& ((String)Session["strCustType"]=="C"))
			{
				if(strESASurcharge!=0)
				{
					txtESA.Text="YES"; 
					txtESASurcharge.Text = strESASurcharge.ToString("#0.00"); 
				}
				else
				{
					txtESA.Text="NO"; 
					txtESASurcharge.Text = "";
				}
			}
			
		}

		private void txtShpSvcCode_TextChanged(object sender, System.EventArgs e)
		{
			lblErrorMsg.Text="";
			bool IsServiceAvail = false;
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePckShpState();
				ViewState["isTextChanged"]=true;
				btnSave.Enabled=true;
			}
			Service service = new Service();
			service.Populate(strAppId ,strEnterpriseId,txtShpSvcCode.Text);
			string strSrvCode = service.ServiceCode;
			String strDesc = service.ServiceDescription;
			if (strSrvCode==null)
			{
				txtShpSvcCode.Text = "";
				lblErrorMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"NO_SERVICE",utility.GetUserCulture());
				return;
			}
			txtShpSvcDesc.Text = service.ServiceDescription; 
			//bool IsServiceAvail = false;
			try
			{
			
				//Check whether the service is provided for the destination zip code
				IsServiceAvail = TIESUtility.IsZipCodeServiceAvailable(strAppId,strEnterpriseId,txtShpSvcCode.Text.Trim(),txtRecipZip.Text.Trim());
			}
			catch(ApplicationException appException)
			{
				lblErrorMsg.Text = appException.Message;
				return;
			}


			if(IsServiceAvail == true)
			{
				if(Session["strPickupDt"] != null)
				{
					DomesticShipmentMgrBAL domesticMgrBAL = new DomesticShipmentMgrBAL();
					txtEstDelvdate.Text = domesticMgrBAL.CalcDlvryDtTime(strAppId,strEnterpriseId,System.DateTime.ParseExact((String)Session["strPickupDt"],"dd/MM/yyyy HH:mm",null),txtShpSvcCode.Text,txtRecipZip.Text).ToString();
				}
			}
			else if(IsServiceAvail == false)
			{
				lblErrorMsg .Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_SERVICE_ZIP",utility.GetUserCulture());

				txtEstDelvdate.Text = "";
			}
		}

		private void txtESA_TextChanged(object sender, System.EventArgs e)
		{
		}

		private void txtESASurcharge_TextChanged(object sender, System.EventArgs e)
		{
			
		}

		private void txtEstDelvdate_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ViewState["AddSurcharge"] = "none";
				ChangePckShpState();
				ViewState["isTextChanged"]=true;
				btnSave.Enabled=true;
			}
		}

		private void txtFreightChrg_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePckShpState();
				ViewState["isTextChanged"]=true;
				btnSave.Enabled=true;
			}
		}

		private void txtTotVASSurChrg_TextChanged(object sender, System.EventArgs e)
		{
			
		}

		private void txtInsSurcharge_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePckShpState();
				ViewState["isTextChanged"]=true;
				btnSave.Enabled=true;
			}
		}

		private void txtShpTotAmt_TextChanged(object sender, System.EventArgs e)
		{
			if ((bool)ViewState["isTextChanged"]==false)
			{
				ChangePckShpState();
				ViewState["isTextChanged"]=true;
				btnSave.Enabled=true;
			}
		}

		private void btnToSaveChanges_Click(object sender, System.EventArgs e)
		{
			
			ShipmentPanel.Visible = false;
			divMainPanel.Visible = true; 
			divBaseRate.Visible=false;
			if ((String)ViewState["AddSurcharge"] == "asking")
			{
				//Call the surcharge calculation method
				if((String)ViewState["ServiceVASCode"] != "")
				{
					CalcAdditionalSurchrg((String)ViewState["ServiceVASCode"]);
					ViewState["AddSurcharge"] = "updated";
					if(((int)ViewState["PckShpMode"]== (int)ScreenMode.Insert) && ((int)ViewState["PckShpOperation"]==((int)Operation.Insert))) 
					{
						ViewState["PckShpOperation"]=Operation.Insert;
					}
					if((int)ViewState["PckShpOperation"]!=(int)Operation.Insert)
					{
						ViewState["PckShpOperation"]= (int)Operation.Update; 
					}
				}
				ViewState["isEstDlvry"]=false;
				saverecord();
			}
			else
			{
				if((txtRecipZip.Text!="")||(txtRecipZip!=null))
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"RECIP_ZIP_REQ",utility.GetUserCulture());

					return;
				}
				if((txtShpSvcCode.Text!="")||(txtShpSvcCode!=null))
				{
					lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SERVICE_REQ",utility.GetUserCulture());
					return;
				}

				saverecord();
			}
			
			//			ViewState["isTextChange"] = false;
			//			ViewState["PckShpOperation"]=Operation.None; 

		}
		private void CalcAdditionalSurchrg(String strVASCode)
		{
			//The service is available
			QuotationData quotationData;
			quotationData.iQuotationVersion = 0;
			quotationData.strQuotationNo = null;
			int iQuotnVersion = 0;
			String strQuotnNo = null;

			//Get the quotation number & version 
			
			if((String)Session["strCustType"] == "C")
			{
				quotationData =  TIESUtility.CustomerQtnActive(strAppId ,strEnterpriseId,(String)Session["strCustId"]);
				iQuotnVersion = quotationData.iQuotationVersion;
				strQuotnNo = quotationData.strQuotationNo;
			}
			else if((String)Session["strCustType"]  == "A")
			{
				quotationData = TIESUtility.AgentQtnActive(strAppId,strEnterpriseId,(String)Session["strCustId"]);
				iQuotnVersion = quotationData.iQuotationVersion;
				strQuotnNo = quotationData.strQuotationNo;
			}
			
			//Check whether the quotation is active.
			String strQuotVASCode = null;
			String strVasDesc = null;
			decimal decQuotSurchrg;
			VASSurcharge  vasSurcharge;
			if(strQuotnNo != null)
			{
				vasSurcharge = TIESUtility.GetSurchrgVASCode(strAppId ,strEnterpriseId,(String)Session["strCustType"],(String)Session["strCustId"],strQuotnNo,iQuotnVersion,strVASCode);
				decQuotSurchrg = vasSurcharge.decSurcharge;			
				strQuotVASCode = vasSurcharge.strVASCode;
				strVasDesc = vasSurcharge.strVASDesc;
				if (decQuotSurchrg==0)
				{
					DatePkUpDlvryTextChange();
					VAS vas = new VAS();
					vas.Populate(strAppId,strEnterpriseId,strVASCode);
					decQuotSurchrg = vas.Surcharge;
					strQuotVASCode = vas.VASCode;
					strVasDesc = vas.VasDescription;
				}
			}
			else //There is no active quotation
			{
				//Get the Surcharge from the Vas base table
				VAS vas = new VAS();
				vas.Populate(strAppId,strEnterpriseId,strVASCode);
				decQuotSurchrg = vas.Surcharge;
				strQuotVASCode = vas.VASCode;
				strVasDesc = vas.VasDescription;
			}

			if(strQuotVASCode != null)
			{
				//Add the record to the VAS DataSet	& bind to the GRID
				int cnt = m_dsVAS.Tables[0].Rows.Count;  
				int i =0;
				for (i=0;i<cnt;i++)
				{
					DataRow drEach = m_dsVAS.Tables[0].Rows[i];
					string strVasCode = (string)drEach[1];
					if(strQuotVASCode==strVasCode)
					{
						lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"SURCHARGE_PRESENT",utility.GetUserCulture());
						ViewState["AddSurcharge"]="updated";
						ViewState["PckShpOperation"]=Operation.None; 
						return;
					}
								
				}
				
				AddRowInVASGrid();	
				dgVAS.EditItemIndex = m_dsVAS.Tables[0].Rows.Count - 1;
				DataRow drCurrent = m_dsVAS.Tables[0].Rows[dgVAS.EditItemIndex];
				drCurrent["surcharge"] = decQuotSurchrg;
				drCurrent["vas_code"] = strQuotVASCode;
				drCurrent["vas_description"] = strVasDesc;
						
				//Display the Total VAS Surcharge
				if(txtTotVASSurChrg.Text.Length > 0)
				{
					txtTotVASSurChrg.Text = (Convert.ToDecimal(txtTotVASSurChrg.Text)+decQuotSurchrg).ToString();;
				}
				else
				{
					txtTotVASSurChrg.Text = decQuotSurchrg.ToString();
				}

				//Display the Total of all charges
				CalculateTotalAmt();

				dgVAS.EditItemIndex = -1;
				BindVASGrid();
				
			}
		}

		private void btnNotToSave_Click(object sender, System.EventArgs e)
		{
			
			if ((String)ViewState["AddSurcharge"] == "asking")
			{
				ReCalculateDlvryDt();

				//Check whether it is holiday/Sat/Sun
				ServiceAvailable serviceAvailable =  CheckDayServiceAvail();

				if((serviceAvailable.isServiceAvail == true) && ((String)ViewState["AddSurcharge"] == "none"))
				{
					ViewState["ServiceVASCode"] = serviceAvailable.strVASCode;
					//display the popup msg
					lblErrorMsg.Text  =  Utility.GetLanguageText(ResourceType.UserMessage,"WKEND_PUB_SERVICE",utility.GetUserCulture());

					lblConfirmMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"WANT_WKEND_PUB_SERVICE",utility.GetUserCulture());

					ShipmentPanel.Visible = true;
					divMainPanel.Visible = false; 
					divBaseRate.Visible=false;
					
					ViewState["AddSurcharge"] = "asking";
					
				}
				ViewState["isEstDlvry"]=false;
				ViewState["isStateChanged"]=true;
				ShipmentPanel.Visible = false;
				divMainPanel.Visible = true; 
				divBaseRate.Visible=false;
				ViewState["isTextChange"] = false;
				return;
			}
		
			if((string)ViewState["AddSurcharge"]=="none")
			{
				ShipmentPanel.Visible = false;
				divMainPanel.Visible = true; 
				divBaseRate.Visible=false;
				ViewState["isTextChange"] = false;

			}
			if((bool)ViewState["isStateChanged"]!=true)
			{
				if ((int)ViewState["PckShpMode"]==(int)ScreenMode.Insert) 
				{
					ShipmentPanel.Visible = false;
					divMainPanel.Visible = true; 
					divBaseRate.Visible=false;
					ViewState["isTextChange"] = false;
					EmptyFields(); 
					ViewState["PckShpOperation"]=Operation.None;  
					Insert();
				
				}
				else if ((bool)ViewState["MovePrevious"]==true) 
				{
				
					ShipmentPanel.Visible = false;
					divMainPanel.Visible = true; 
					divBaseRate.Visible=false;
					ViewState["isTextChange"] = false;
					ViewState["PckShpOperation"]=Operation.None;
					MovePrevious();
				
				}
				else if((bool)ViewState["MoveNext"]==true) 
				{
				
					ShipmentPanel.Visible = false;
					divMainPanel.Visible = true; 
					divBaseRate.Visible=false;
					ViewState["isTextChange"] = false;
					ViewState["PckShpOperation"]=Operation.None;
					MoveNext();
				}
				else if((bool)ViewState["MoveLast"]==true) 
				{
				
					ShipmentPanel.Visible = false;
					divMainPanel.Visible = true; 
					divBaseRate.Visible=false;
					ViewState["isTextChange"] = false;
					ViewState["PckShpOperation"]=Operation.None;
					MoveLast ();
				}
				else if((bool)ViewState["MoveFirst"]==true) 
				{
				
					ShipmentPanel.Visible = false;
					divMainPanel.Visible = true; 
					divBaseRate.Visible=false;
					ViewState["isTextChange"] = false;
					ViewState["PckShpOperation"]=Operation.None;
					MoveFirst();
				
				}
				else if((String)ViewState["NextOperation"] == "Insert")
				{
					Insert();
					ViewState["NextOperation"] ="None";
				}
				else
				{
					ShipmentPanel.Visible = false;
					divMainPanel.Visible = true; 
					divBaseRate.Visible=false;
					ViewState["isTextChange"] = false;
					ViewState["PckShpOperation"]=Operation.None;

				}
			}
		}
		private ServiceAvailable CheckDayServiceAvail()
		{
			bool isWeekHolidaySrvAvail = false;
			ServiceData serviceData;
			serviceData.isServiceAvail = false;
			serviceData.strVASCode = null;
			DateTime dtDlvryDt;
			String strWeekDay = "";
			String strDayType = "";
			String strVASCode = null;
			ServiceAvailable serviceAvailable = new ServiceAvailable();
			ViewState["Day"] = "";

			strErrorMsg = "";
			//Get the day from the delivery date.
			dtDlvryDt = System.DateTime.ParseExact(txtEstDelvdate.Text.ToString(),"dd/MM/yyyy HH:mm",null);
			strWeekDay = dtDlvryDt.DayOfWeek.ToString();

			if ((strWeekDay.Equals("Saturday")) || (strWeekDay.Equals("Sunday")))
			{
				//Check whether the service is there for saturday or sunday
				strDayType = "W";

				ViewState["Day"] = "WeekEnd";

				try
				{
					serviceData = TIESUtility.IsServiceAvailable(strAppId,strEnterpriseId,txtRecipZip.Text.Trim(),strWeekDay,strDayType);
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					strErrorMsg = strMsg;
					return serviceAvailable;
				}
				
				//Flag which shows service available or not
				isWeekHolidaySrvAvail = serviceData.isServiceAvail;

				ViewState["ServiceExists"] = isWeekHolidaySrvAvail;

				//VAS Code for the service
				strVASCode = serviceData.strVASCode;

			}

			//Check whether the delivery date is on holiday 
			bool isHoliday = false;
			try
			{
				isHoliday = TIESUtility.IsDayHoliday(strAppId,strEnterpriseId,System.DateTime.ParseExact(txtEstDelvdate.Text.Trim().ToString(),"dd/MM/yyyy HH:mm",null));
			}
			catch(ApplicationException appException)
			{
				String strMsg = appException.Message;
				strErrorMsg = strMsg;
				return serviceAvailable;
			}

			if(isHoliday == true)
			{
				ViewState["isHoliday"] = true;
				//check whether service is provided for holiday
				strDayType = "H";
				
				ViewState["Day"] = "Holiday";

				try
				{
					serviceData	= TIESUtility.IsServiceAvailable(strAppId,strEnterpriseId,txtRecipZip.Text.Trim(),strWeekDay,strDayType);
				}
				catch(ApplicationException appException)
				{
					String strMsg = appException.Message;
					strErrorMsg = strMsg;
					return serviceAvailable;
				}
				
				//Flag which shows service available or not
				isWeekHolidaySrvAvail = serviceData.isServiceAvail;
				
				ViewState["ServiceExists"] = isWeekHolidaySrvAvail;
				
				//VAS Code for the service
				strVASCode = serviceData.strVASCode; 
				
			}
			
			serviceAvailable.isServiceAvail = isWeekHolidaySrvAvail;
			serviceAvailable.strVASCode = strVASCode;
			lblErrorMsg.Text =  strErrorMsg;
			
			return serviceAvailable;
		}

		private void txtSerialNo_TextChanged(object sender, System.EventArgs e)
		{
		}

		private void btnToCancel_Click(object sender, System.EventArgs e)
		{
			ShipmentPanel.Visible = false;
			divMainPanel.Visible=true;
			divBaseRate.Visible=false;
			ViewState["isTextChange"] = false;
			//EmptyFields(); 
			ViewState["PckShpOperation"]=Operation.None; 
			ViewState["AddSurcharge"] = "none";
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			/*String sScript = "";
			sScript += "<script language=javascript>";
			sScript += "  history.go(-1);";
			sScript += "</script>";
			Response.Write(sScript);*/
			btnSave.Enabled=true;
			EmptyFields(); 
		}

		private void bntClose_Click(object sender, System.EventArgs e)
		{
			decimal iCashFromPickup=0;
			decimal iTotWTFromPickup=0;
			decimal iTotDIMWTFromPickup=0;
			int iTotPkgsFromPickup=0;
			

			String strBookingno =(String)Session["strBookingNo"];

			if (strBookingno.Length >0)
			{
		
				DataSet dsPickup = PRBMgrDAL.GetValuesfromPickup(strAppId,strEnterpriseId,strBookingno); 
				DataRow drPickEach=dsPickup.Tables[0].Rows[0];   
		
				if((drPickEach["tot_pkg"]!=null)&&(drPickEach["tot_pkg"].GetType().Equals(System.Type.GetType("System.DBNull"))))   
				{
					iTotPkgsFromPickup = 0;
				}
				else
				{
					iTotPkgsFromPickup = Convert.ToInt32(drPickEach["tot_pkg"]);
				}
				if((drPickEach["cash_amount"]!=null)&&(drPickEach["cash_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))   
				{
					iCashFromPickup = 0;
				}
				else
				{
					iCashFromPickup = (decimal)drPickEach["cash_amount"];//+Convert.ToDecimal(Request.Params["ESASURCHARGE"]);
				}
				if((drPickEach["tot_wt"]!=null)&&(drPickEach["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))     
				{
					iTotWTFromPickup=0;
				}
				else
				{
					iTotWTFromPickup=(decimal)drPickEach["tot_wt"];
				}
				if((drPickEach["tot_dim_wt"]!=null)&&(drPickEach["tot_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))     
				{
					iTotDIMWTFromPickup=0;
				}
				else
				{
					iTotDIMWTFromPickup=(decimal)drPickEach["tot_dim_wt"];
				}

			}
				
			
			
			String sScript = "";
			sScript += "<script language=javascript>";
			
//			if (Request.Params["SPLRATES"]=="N")
//			{
				
				sScript += "  window.opener.PickupRequestBooking.Txt_CashAmount.value = '" + TIESUtility.Round_Enterprise_Currency(utility.GetAppID(),utility.GetEnterpriseID(),iCashFromPickup) + "';";
//			}
			sScript += "  window.opener.PickupRequestBooking.Txt_DimWeight.value = '" + iTotDIMWTFromPickup + "';";
			sScript += "  window.opener.PickupRequestBooking.Txt_Totpkg.value = '" + iTotPkgsFromPickup + "';";
			sScript += "  window.opener.PickupRequestBooking.Txt_ActualWeight.value = '" +iTotWTFromPickup + "';";
			
			
					
			sScript += "  window.close();";
			sScript += "</script>";
			Response.Write(sScript);
		}
	
		private void fillViewState()
		{
			
			String strBookingno =(String)Session["strBookingNo"];

			if (strBookingno.Length >0)
			{
		
				//				DataSet  dsPickupPKG = PRBMgrDAL.GetTotPkgs(strAppId,strEnterpriseId,strBookingno);   
				//				DataRow drEach = dsPickupPKG.Tables[0].Rows[0];
				
				DataSet dsPickup = PRBMgrDAL.GetValuesfromPickup(strAppId,strEnterpriseId,strBookingno); 
				DataRow drPickEach=dsPickup.Tables[0].Rows[0];   
			
				if((drPickEach["tot_pkg"]!=null)&&(drPickEach["tot_pkg"].GetType().Equals(System.Type.GetType("System.DBNull"))))   
				{
					ViewState["Qty"]= 0;
				}
				else
				{
					ViewState["Qty"]= System.Convert.ToInt32(drPickEach["tot_pkg"]);
				}
				if((drPickEach["tot_wt"]!=null)&&(drPickEach["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))     
				{
					ViewState["ActualWt"]=0;
				}
				else
				{
					ViewState["ActualWt"]=(decimal)drPickEach["tot_wt"];
				}
				if((drPickEach["tot_dim_wt"]!=null)&&(drPickEach["tot_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))     
				{
					ViewState["dim_wt"]=0;
				}
				else
				{
					ViewState["dim_wt"]=(decimal)drPickEach["tot_dim_wt"];
				}

			}
		}
		
		private void fillCashAmountViewState()
		{
			String strBookingno =(String)Session["strBookingNo"];
			if (strBookingno.Length >0)
			{
		
				
				DataSet dsPickup = PRBMgrDAL.GetValuesfromPickup(strAppId,strEnterpriseId,strBookingno); 
				DataRow drPickEach=dsPickup.Tables[0].Rows[0];   
			
				if((drPickEach["cash_amount"]!=null)&&(drPickEach["cash_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))   
				{
					ViewState["iCashAmount"] = 0;
				
				}
				else
				{
					ViewState["iCashAmount"] = (decimal)drPickEach["cash_amount"];//-;
				}
			}
		}
		private decimal  CalculateCurrentCashAmt()
		{
			decimal iESASur=0;
			decimal iFrigChrg=0;
			decimal iInsuChrg=0;
			decimal iTotVas=0;
			decimal iCurrentCashAmount=0;
			if (txtESASurcharge.Text.Length >0)
			{
				iESASur =Convert.ToDecimal(txtESASurcharge.Text.ToString());
				
			}
			if (txtFreightChrg.Text.Length > 0)
			{
				iFrigChrg = Convert.ToDecimal(txtFreightChrg.Text.ToString());
			}
			if(txtInsSurcharge.Text.Length >0)
			{
				iInsuChrg = Convert.ToDecimal(txtInsSurcharge.Text.ToString ());
			}
			if(txtTotVASSurChrg.Text.Length >0)
			{
				iTotVas = Convert.ToDecimal(txtTotVASSurChrg.Text.ToString());
			}
			iCurrentCashAmount= Convert.ToDecimal(iESASur+iFrigChrg +iInsuChrg +iTotVas) ;

			return iCurrentCashAmount;
		}
		
		private decimal  CalculatePreviousCashAmt()
		{
			decimal iESASur=0;
			decimal iFrigChrg=0;
			decimal iInsuChrg=0;
			decimal iTotVas=0;
			decimal iPreviousCashAmount=0;
			//	m_dsShpmDetls=(DataSet)Session["SESSION_DS2"];
			//DataSet dsShpmDelts= PRBMgrDAL.GetPikupShp(strAppId,strEnterpriseId,(int)Request.Params["BOOKINGNO"]);  
			
		
			DataRow drEach = m_dsShpmDetls.Tables[0].Rows[(int)ViewState["iCurrentRow"]]; 
					
			if((drEach["esa_delivery_surcharge"]!= null) && (!drEach["esa_delivery_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				iESASur = (decimal)drEach["esa_delivery_surcharge"];
			}
			else
			{
				iESASur=0;
			}
			if((drEach["tot_freight_charge"]!= null) && (!drEach["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				iFrigChrg = (decimal)drEach["tot_freight_charge"];
			}
			else
			{
				iFrigChrg=0;
			}
			if((drEach["insurance_surcharge"]!= null) && (!drEach["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				iInsuChrg = (decimal)drEach["insurance_surcharge"];
			}
			else
			{
				iInsuChrg=0;
			}
			if((drEach["tot_vas_surcharge"]!= null) && (!drEach["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				iTotVas = (decimal)drEach["tot_vas_surcharge"];
			}
			else
			{
				iTotVas=0;
			}

			iPreviousCashAmount= Convert.ToDecimal(iESASur+iFrigChrg +iInsuChrg +iTotVas) ;
			return iPreviousCashAmount;
		}
		
		private void  CaculateEstDvlryDt()
		{
			DomesticShipmentMgrBAL domesticMgrBAL = new DomesticShipmentMgrBAL();
			
			//			if ((bool)ViewState["isTextChanged"]==false)
			//			{
			//				ChangePckShpState();
			//				ViewState["isTextChanged"]=true;
			//				Utility.EnableButton(ref btnSave, com.common.util.ButtonType.Save,true,m_moduleAccessRights);
			//			}
			if (Session["strPickupDt"] !=null || Session["strPickupDt"].ToString() !="")
			{
				if(txtEstDelvdate.Text.Length ==0)
				{
					txtEstDelvdate.Text = domesticMgrBAL.CalcDlvryDtTime(strAppId,strEnterpriseId,System.DateTime.ParseExact((String)Session["strPickupDt"],"dd/MM/yyyy HH:mm",null),txtShpSvcCode.Text,txtRecipZip.Text).ToString();
				}
			}
			DatePkUpDlvryTextChange();
			//Get the commit time from the table.
			ServiceTime serviceTime =  domesticMgrBAL.GetCommitTime(strAppId,strEnterpriseId,txtShpSvcCode.Text,txtRecipZip.Text);
			if(serviceTime.HourServiceTime != 0)
			{
				//Find the difference of the entered time & system time
				DateTime dtSystem1 = DateTime.Now;
				String dtSysHour = dtSystem1.ToString("HH:mm");
				DateTime dtSystem = System.DateTime.ParseExact(dtSysHour,"HH:mm",null);
				String strEntered = System.DateTime.ParseExact(txtEstDelvdate.Text.Trim(),"dd/MM/yyyy HH:mm",null).ToString("HH:mm");
				DateTime dtEntered = System.DateTime.ParseExact(strEntered,"HH:mm",null);
				System.TimeSpan dtDiff = dtEntered.Subtract(dtSystem);
				int iDiffHrsMin = (dtDiff.Hours * 60);
				int iDiffTotalMin =  dtDiff.Minutes+iDiffHrsMin;
				int iSerMinutes = (serviceTime.HourServiceTime * 60);
				if(!(iDiffTotalMin >= iSerMinutes))
				{
					//If the entered time is less than the required time for delivery recalculate the time.
					String tmpDate = System.DateTime.ParseExact(txtEstDelvdate.Text.Trim(),"dd/MM/yyyy HH:mm",null).ToString("dd/MM/yyyy");
					DateTime dtEstDlvy = System.DateTime.ParseExact(tmpDate,"dd/MM/yyyy",null);
					dtEstDlvy = System.DateTime.ParseExact(dtEstDlvy.ToString("dd/MM/yyyy")+" "+dtSystem.ToString("HH:mm"),"dd/MM/yyyy HH:mm",null);
					txtEstDelvdate.Text = dtEstDlvy.AddMinutes((double)iSerMinutes).ToString("dd/MM/yyyy HH:mm");
				}
					
			}
			else if(serviceTime.RegularTime != null)
			{
				//The entered time should be less than or equal to the commit time.
				System.DateTime.ParseExact(serviceTime.RegularTime,"HH:mm",null);
				String strEnteredTime = System.DateTime.ParseExact(txtEstDelvdate.Text.Trim(),"dd/MM/yyyy HH:mm",null).ToString("HH:mm");
				System.DateTime.ParseExact(strEnteredTime,"HH:mm",null);
				if(!(System.DateTime.ParseExact(strEnteredTime,"HH:mm",null) <= System.DateTime.ParseExact(serviceTime.RegularTime,"HH:mm",null)))
				{
					String tmpDateTime = System.DateTime.ParseExact(txtEstDelvdate.Text.Trim(),"dd/MM/yyyy HH:mm",null).ToString("dd/MM/yyyy")+" "+System.DateTime.ParseExact(serviceTime.RegularTime,"HH:mm",null).ToString("HH:mm");
					DateTime dtTemp =  System.DateTime.ParseExact(tmpDateTime,"dd/MM/yyyy HH:mm",null);
					txtEstDelvdate.Text = dtTemp.ToString("dd/MM/yyyy HH:mm");
				}
			}
			else
			{
				lblErrorMsg.Text = Utility.GetLanguageText(ResourceType.UserMessage,"NO_RECORD_FOUND_COMMIT",utility.GetUserCulture());

				return;
			}
			
			
		}
		private void DatePkUpDlvryTextChange()
		{
			
			ServiceAvailable serviceAvailable =  CheckDayServiceAvail();
			if((serviceAvailable.isServiceAvail == true) && ((String)ViewState["AddSurcharge"] == "none"))
			{
				ViewState["ServiceVASCode"] = serviceAvailable.strVASCode;
				//display the popup msg
				ShipmentPanel.Visible = true;
				divMainPanel.Visible=false;
				divBaseRate.Visible=false;
				lblErrorMsg.Text  =  Utility.GetLanguageText(ResourceType.UserMessage,"WKEND_PUB_SERVICE",utility.GetUserCulture());

				lblConfirmMsg.Text =  Utility.GetLanguageText(ResourceType.UserMessage,"WANT_WKEND_PUB_SERVICE",utility.GetUserCulture());

				ViewState["AddSurcharge"] = "asking";
				ViewState["isEstDlvry"]=true;
				return;
			}

			//if no service available then get the next working day...
			if((bool)ViewState["ServiceExists"] == false && (((String)ViewState["Day"] == "WeekEnd" ||((String)ViewState["Day"] == "Holiday"))))
			{
				ReCalculateDlvryDt();
			}
		}
		
		private void ReCalculateDlvryDt()
		{
			ViewState["AddSurcharge"] = "none";

			DateTime dtDlvryDt = System.DateTime.ParseExact(txtEstDelvdate.Text.Trim(),"dd/MM/yyyy HH:mm",null);

			//Calculate the next date 
			dtDlvryDt = dtDlvryDt.AddDays(1);

			txtEstDelvdate.Text = dtDlvryDt.ToString("dd/MM/yyyy HH:mm");
			CheckTheCalcDay();
			return;
			
		}
		private void CheckTheCalcDay()
		{
			//Check whether the calculated day has service
			ServiceAvailable serviceAvailable =  CheckDayServiceAvail();

			//Weekend / Holiday
			if(((String)ViewState["Day"] == "WeekEnd") || ((String)ViewState["Day"] == "Holiday"))
			{
				//Check service for weekend/holiday
				if((serviceAvailable.isServiceAvail == true) && ((String)ViewState["AddSurcharge"] == "none"))
				{
					ViewState["ServiceVASCode"] = serviceAvailable.strVASCode;
					//display the popup msg if service available for weekend/holiday
					strErrorMsg = Utility.GetLanguageText(ResourceType.UserMessage,"WKEND_PUB_SERVICE",utility.GetUserCulture());
					lblConfirmMsg.Text =Utility.GetLanguageText(ResourceType.UserMessage,"WANT_WKEND_PUB_SERVICE",utility.GetUserCulture());
					ShipmentPanel.Visible = true;
					divMainPanel.Visible=false;
					divBaseRate.Visible=false;
				
					ViewState["AddSurcharge"] = "asking";
				}
				else
				{
					//No service available weekend/Holiday so add 1 day & again check whether weekend/holiday.
					DateTime dtDlvryDt = System.DateTime.ParseExact(txtEstDelvdate.Text.Trim(),"dd/MM/yyyy HH:mm",null);
					dtDlvryDt = dtDlvryDt.AddDays(1);
					txtEstDelvdate.Text = dtDlvryDt.ToString("dd/MM/yyyy HH:mm");
					CheckTheCalcDay();
				}
			}
		}

		private void btnBaseRateYes_Click(object sender, System.EventArgs e)
		{
			if(CalculateFrieghtBaseRate())
			{
				totalAmtCalc();
			}
			divMainPanel.Visible=true;
			divBaseRate.Visible=false;
			ShipmentPanel.Visible=false;

		}
		private void totalAmtCalc()
		{
			decimal totSurcharge = GetTotalSurcharge();
			txtTotVASSurChrg.Text = totSurcharge.ToString("#0.00");
			CalculateTotalAmt();
			SavetoDatabase(); 
		}

		private void btnBaseRateNo_Click(object sender, System.EventArgs e)
		{
			divMainPanel.Visible=true;
			divBaseRate.Visible=false;
			ShipmentPanel.Visible=false;
		}
	}
}


