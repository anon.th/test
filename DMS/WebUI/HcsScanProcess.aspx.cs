using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using com.ties.DAL;

namespace TIES.WebUI
{
	/// <summary>
	/// Summary description for HcsScanProcess.
	/// </summary>
	/// 

	public class HcsScanProcess : System.Web.UI.Page
	{
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			try
			{
				string appId = Request.Form["tAppId"].ToString();
				string entId = Request.Form["tEntId"].ToString();
				string hcsNo = Request.Form["tHcsNo"].ToString();
				string barCode = Request.Form["tBarCode"].ToString();
				string scanUserId = Request.Form["tScanUserId"].ToString();
				string scanDateTime = Request.Form["tScanDateTime"].ToString();				
				string statusCode = Request.Form["tStatusCode"].ToString();
				string location = Request.Form["tLocation"].ToString();

				int ScannedResult = HcsScanDAL.HcsDtInsert(appId, entId, hcsNo, barCode, scanUserId);
				
//				if (ScannedResult == 0)		//// Success
//				{
//					Response.Write("");
//				}
//				else
//				{
//					Response.Write("result");


			}
			catch(Exception ex)
			{
				throw(ex);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

	}
}
