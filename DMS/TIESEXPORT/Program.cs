﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.ties.DAL;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using TIESEXPORT.Reports;
using TIESDAL;
using com.common.util;

namespace TIESEXPORT
{
    class Program
    {
        static ReportDocument rptSource;
        static string[] lsCusID;
        static string path;
        static string Logpath;
        static string strExportFile = null;
        static Utility utility = null;
        static string ApplicationID = ConfigurationManager.AppSettings.Get("applicationID");
        static string EnterpriseID = ConfigurationManager.AppSettings.Get("enterpriseID");
        static string ReportDay = ConfigurationManager.AppSettings.Get("ReportDay");

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Please wait...");
                
                #region SetPath

                path = ConfigurationManager.AppSettings.Get("LogPath") + "Log\\" + DateTime.Now.Year + "\\" + DateTime.Now.Month.ToString("0#");
                if (System.IO.Directory.Exists(path) == false)
                    System.IO.Directory.CreateDirectory(path);


                Logpath = path + "\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt";

                Console.WriteLine("Log Path : " + Logpath);
                #endregion

                
                RetrySendEmail();
                
                string cfCustomer = ConfigurationManager.AppSettings.Get("CustomerID");
                lsCusID = cfCustomer.Split(',');

                if (lsCusID.Length > 0)
                {
                    foreach (string lsvalue in lsCusID)
                    {
                        System.IO.File.AppendAllLines(Logpath, new[] { "------------------------------------------------------------" });
                        System.IO.File.AppendAllLines(Logpath, new[] { "Software Version : V 0.0.0.1" });
                        System.IO.File.AppendAllLines(Logpath, new[] { "Start Time : " + DateTime.Now });
                        System.IO.File.AppendAllLines(Logpath, new[] { "Customer ID : " + lsvalue });

                        GenerateReport(lsvalue);
                    }
                }

            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllLines(Logpath, new[] { "Exception (Send Email) : " + ex.ToString() });
            }
        }

        private static void GenerateReport(string CusID)
        {
            DataSet m_dsResult = ReportDAL.GetFreightSummaryByCustomerID(ApplicationID, EnterpriseID, CusID, ReportDay);

            rptSource = new FreighSummary();
            rptSource.PrintOptions.PaperSize = PaperSize.PaperA4;
            rptSource.PrintOptions.PaperOrientation = PaperOrientation.Landscape;

            if(m_dsResult.Tables[0].Rows.Count > 0)
            {
                SetFreightSummary(m_dsResult, "M", CusID);

                SendEmail(CusID, "");
            }
        }

        private static void SetFreightSummary(DataSet dsResult, string mode, string CusID)
        {
            try
            {
                int i = 0;
                DSFreightSummary DSShippingXSDds = new DSFreightSummary();

                int iRecCnt = dsResult.Tables[0].Rows.Count;
                for (i = 0; i < iRecCnt; i++)
                {
                    DataRow drEach = dsResult.Tables[0].Rows[i];
                    DataRow drXSD = DSShippingXSDds.Tables["FreightSummary"].NewRow();

                    #region set value

                    if (Utility.IsNotDBNull(drEach["shpt_manifest_datetime"]))
                        drXSD["shpt_manifest_datetime"] = drEach["shpt_manifest_datetime"];

                    if (Utility.IsNotDBNull(drEach["consignment_no"]))
                        drXSD["consignment_no"] = drEach["consignment_no"].ToString();

                    if (Utility.IsNotDBNull(drEach["origin_station"]))
                        drXSD["origin_station"] = drEach["origin_station"].ToString();

                    if (Utility.IsNotDBNull(drEach["destination_station"]))
                        drXSD["destination_station"] = drEach["destination_station"].ToString();

                    if (Utility.IsNotDBNull(drEach["destination_station"]))
                        drXSD["destination_station"] = drEach["destination_station"].ToString();

                    if (Utility.IsNotDBNull(drEach["tot_act_wt"]))
                        drXSD["tot_act_wt"] = drEach["tot_act_wt"];

                    if (Utility.IsNotDBNull(drEach["rate"]))
                        drXSD["rate"] = drEach["rate"];

                    if (Utility.IsNotDBNull(drEach["basic_charge"]))
                        drXSD["basic_charge"] = drEach["basic_charge"];

                    if (Utility.IsNotDBNull(drEach["tot_freight_charge"]))
                        drXSD["tot_freight_charge"] = drEach["tot_freight_charge"];

                    if (Utility.IsNotDBNull(drEach["other_surch_amount"]))
                        drXSD["other_surch_amount"] = drEach["other_surch_amount"];

                    if (Utility.IsNotDBNull(drEach["tot_vas_surcharge"]))
                        drXSD["tot_vas_surcharge"] = drEach["tot_vas_surcharge"];

                    if (Utility.IsNotDBNull(drEach["tax"]))
                        drXSD["tax"] = drEach["tax"];

                    if (Utility.IsNotDBNull(drEach["MasterAWBNumber"]))
                        drXSD["MasterAWBNumber"] = drEach["MasterAWBNumber"];

                    if (Utility.IsNotDBNull(drEach["payerid"]))
                        drXSD["payerid"] = drEach["payerid"].ToString();

                    if (Utility.IsNotDBNull(drEach["cust_name"]))
                        drXSD["cust_name"] = drEach["cust_name"].ToString();

                    if (Utility.IsNotDBNull(drEach["total_rated_amount"]))
                        drXSD["total_rated_amount"] = drEach["total_rated_amount"].ToString();

                    if (Utility.IsNotDBNull(drEach["invoice_amt"]))
                        drXSD["invoice_amt"] = drEach["invoice_amt"].ToString();

                    if (Utility.IsNotDBNull(drEach["mode"]))
                        drXSD["mode"] = drEach["mode"].ToString();

                    if (Utility.IsNotDBNull(drEach["dateFrom"]))
                        drXSD["dateFrom"] = drEach["dateFrom"].ToString();

                    if (Utility.IsNotDBNull(drEach["dateTo"]))
                        drXSD["dateTo"] = drEach["dateTo"].ToString();

                    if (Utility.IsNotDBNull(drEach["code_text"]))
                        drXSD["code_text"] = drEach["code_text"].ToString();

                    if (Utility.IsNotDBNull(drEach["cost_centre"]))
                        drXSD["cost_centre"] = drEach["cost_centre"].ToString();

                    if (Utility.IsNotDBNull(drEach["service_code"]))
                        drXSD["service_code"] = drEach["service_code"].ToString();

                    #endregion

                    DSShippingXSDds.Tables["FreightSummary"].Rows.Add(drXSD);
                }
                rptSource.SetDataSource(DSShippingXSDds);

                ParameterFieldDefinitions paramFldDefs;
                ParameterValues paramVals = new ParameterValues();
                ParameterDiscreteValue paramDVal = new ParameterDiscreteValue();
                paramFldDefs = rptSource.DataDefinition.ParameterFields;

                foreach (ParameterFieldDefinition paramFldDef in paramFldDefs)
                {
                    switch (paramFldDef.ParameterFieldName)
                    {
                        case "mode":
                            paramDVal.Value = mode;
                            break;
                        default:
                            continue;
                    }

                    paramVals = paramFldDef.CurrentValues;
                    paramVals.Add(paramDVal);
                    paramFldDef.ApplyCurrentValues(paramVals);
                }

                ExportToPDF(CusID);
            }
            catch (Exception ex)
            {
                SaveLog(CusID);
            }  
        }

        private static void ExportToPDF(string CusID)
        {
            ExportOptions ExportOptions = new ExportOptions();
            DiskFileDestinationOptions DiskFileDstOptions = new DiskFileDestinationOptions();


            strExportFile = path + "\\FreightSummary_" + CusID + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".pdf";

            DiskFileDstOptions.DiskFileName = strExportFile;
            ExportOptions = rptSource.ExportOptions;
            ExportOptions.DestinationOptions = DiskFileDstOptions;
            ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
            ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
            ExportOptions.FormatOptions = new PdfRtfWordFormatOptions();

            rptSource.Export();

            System.IO.File.AppendAllLines(Logpath, new[] { "Generate Report Date : " + DateTime.Now });

            rptSource.Close();
        }

        private static void SendEmail(string CusID, string LogID)
        {
            try
            {
                utility = new Utility(ConfigurationManager.AppSettings, null);

                string from = utility.GetEmailNameSend() + "<" + utility.GetEmailSend() + ">";
                string mailTo = getEmailUser(CusID);
                string body = ConfigurationManager.AppSettings.Get("templateMail");

                //string mailTo = "vassana.r@aware.co.th";
                string subject = utility.GetEmailSubject();
                string smtpServer = utility.GetSmtpServer();


                string strErr = utility.SendEmail(body, from, mailTo, "", subject, smtpServer, strExportFile);

                System.IO.File.AppendAllLines(Logpath, new[] { "SMTP Server : " + smtpServer });
                System.IO.File.AppendAllLines(Logpath, new[] { "Send Email From : " + from });
                System.IO.File.AppendAllLines(Logpath, new[] { "Send Email To : " + mailTo });

                if (strErr == "")
                    System.IO.File.AppendAllLines(Logpath, new[] { "Send Email Time : " + DateTime.Now });
                else
                {
                    SaveLog(CusID);
                    System.IO.File.AppendAllLines(Logpath, new[] { "Exception (Send Email) : " + strErr });
                }
                    

                if (LogID != "")
                    UpdateStatusLog(LogID);
            }
            catch (Exception ex)
            {
                SaveLog(CusID);
                System.IO.File.AppendAllLines(Logpath, new[] { "Exception (Send Email) : " + ex.ToString() });
            }
        }

        private static string getEmailUser(string CusID)
        {
            try
            {
                return CustomsDAL.GetEmailCustomsbyID(utility.GetAppID(), utility.GetEnterpriseID(), CusID);
            }
            catch (Exception ex)
            {
                return "";
            }

        }

        private static void GenerateReportForResendEmail(DataRow dr)
        {
            DataSet m_dsResult = ReportDAL.GetFreightSummaryByParam(dr);

            rptSource = new FreighSummary();
            rptSource.PrintOptions.PaperSize = PaperSize.PaperA4;
            rptSource.PrintOptions.PaperOrientation = PaperOrientation.Landscape;

            if (m_dsResult.Tables[0].Rows.Count > 0)
            {
                SetFreightSummary(m_dsResult, "M", dr["CustID"].ToString());

                SendEmail(dr["CustID"].ToString(), dr["LogID"].ToString());
            }
        }

        private static void SaveLog(string CusID)
        {
            try
            {
                ReportDAL.SaveFreightSummaryLog(ApplicationID, EnterpriseID, CusID, ReportDay);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllLines(Logpath, new[] { "Exception (Save Log) : " + ex.ToString() });
            }           
        }

        private static void UpdateStatusLog(string ID)
        {
            try
            {
                ReportDAL.UpdateFreightSummaryLog(ApplicationID, EnterpriseID, ID);
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllLines(Logpath, new[] { "Exception (Save Log) : " + ex.ToString() });
            }
        }

        private static void RetrySendEmail()
        {
            string cfCustomer = ConfigurationManager.AppSettings.Get("CustomerID");
            lsCusID = cfCustomer.Split(',');

            if (lsCusID.Length > 0)
            {
                foreach (string lsvalue in lsCusID)
                {
                    DataSet m_dsResult = ReportDAL.GetLogEmailError(ApplicationID, EnterpriseID, lsvalue);

                    if(m_dsResult.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow val in m_dsResult.Tables[0].Rows)
                        {
                            System.IO.File.AppendAllLines(Logpath, new[] { "------------------------------------------------------------" });
                            System.IO.File.AppendAllLines(Logpath, new[] { "Re-send Email" });
                            System.IO.File.AppendAllLines(Logpath, new[] { "Start Time : " + DateTime.Now });
                            System.IO.File.AppendAllLines(Logpath, new[] { "Customer ID : " + lsvalue });
                            System.IO.File.AppendAllLines(Logpath, new[] { "Schedule Date : " + Convert.ToDateTime(val["startdate"]).ToString("yyyy-MM-dd")  + " to " + Convert.ToDateTime(val["enddate"]).ToString("yyyy-MM-dd")});

                            GenerateReportForResendEmail(val);
                        }
                    }
                }
            }
        }
    }
}
