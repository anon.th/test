﻿using System;
using System.Configuration;
using System.Data;
using TIESDAL;
using System.Collections;
using TIES.WebUI;
using TIES.WebUI.ConsignmentNote;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using BarcodeLib;
using System.IO;
using System.Drawing;


namespace TIESPOD
{
    class Program
    {
        static ReportDocument orpt = null;
        static DataSet dsReportOnePage = null;
        static DataSet dsConsignments = null, dsAgencyInvoices = null;
        static DataSet dsResult = null;
        static string path = ConfigurationManager.AppSettings.Get("LogPath") + "Log\\" + DateTime.Now.Year + "\\" + DateTime.Now.Month.ToString("0#");
        static string Logpath = path + "\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
        static string strExportFolder = "";
        static string strExportFile = null;

        static void Main(string[] args)
        {
            string strAppID = ConfigurationManager.AppSettings.Get("applicationID");
            string strEnterpriseID = ConfigurationManager.AppSettings.Get("enterpriseID");
            string userloggedin = ConfigurationManager.AppSettings.Get("userloggedin");
            
            DateTime InvoiceDataFrom = DateTime.Today;
            DateTime InvoiceDatato = DateTime.Today;

            if (ConfigurationManager.AppSettings.Get("invoicedDateFrom") != "")
                InvoiceDataFrom = DateTime.Parse(ConfigurationManager.AppSettings.Get("invoicedDateFrom"));
            if (ConfigurationManager.AppSettings.Get("invoicedDateTo") != "")
                InvoiceDatato = DateTime.Parse(ConfigurationManager.AppSettings.Get("invoicedDateTo"));

            if (System.IO.Directory.Exists(path) == false) System.IO.Directory.CreateDirectory(path);

            // Domestic Invoice
            try
            {
                 dsResult = CustomerConsignmentDAL.QueryInvoicedConsignment(
                    strAppID.ToString(),
                    strEnterpriseID.ToString(),
                    userloggedin.ToString(),
                    null,
                    null,
                    DateTime.MinValue,
                    DateTime.MinValue,
                    InvoiceDataFrom,
                    InvoiceDatato,
                    DateTime.MinValue,
                    DateTime.MinValue,
                    0
                );
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllLines(Logpath, new[] { DateTime.Now + ":" + ex.ToString() });
            }

            string strConnote = "";
            System.Data.DataTable dtList = dsResult.Tables[0];
            foreach (DataRow dr in dtList.Rows) {
                strConnote = dr["consignment_no"].ToString();
                #region Domestic Connotes
                if (strConnote.Length > 0)
                {
                    try
                    {
                        string strConsignmentNo = strConnote;
                        dsConsignments = CustomerConsignmentDAL.ReprintInvoicedConsNote(strAppID, strEnterpriseID, userloggedin, strConsignmentNo, 0, -1);
                    }
                    catch (Exception ex)
                    {
                        System.IO.File.AppendAllLines(Logpath, new[] { DateTime.Now + ":" + ex.ToString() });
                    }

                    string reportTemplate = dsConsignments.Tables[0].Rows[0]["ReportTemplate"].ToString();

                    ArrayList paramList = new ArrayList();
                    try
                    {
                        orpt = new PreprintConsignmentTNT4();

                        TIES.WebUI.ConsignmentNote.CSS_PreprintConsNotes dsReportOnePage = new TIES.WebUI.ConsignmentNote.CSS_PreprintConsNotes();

                        #region PrintConsNotesOnePage
                        foreach (DataRow row in dsConsignments.Tables[1].Rows)
                        {
                            DataRow _drXSD = dsReportOnePage.Tables["CSS_PrintConsNotes"].NewRow();
                            if ((row["consignment_no"] != null) && (!row["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["consignment_no"] = (string)row["consignment_no"];
                            }
                            if ((row["payerid"] != null) && (!row["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["payerid"] = (string)row["payerid"];
                            }
                            if ((row["service_code"] != null) && (!row["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["service_code"] = (string)row["service_code"];
                            }

                            if ((row["ref_no"] != null) && (!row["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["ref_no"] = (string)row["ref_no"];
                            }

                            if ((row["sender_name"] != null) && (!row["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_name"] = (string)row["sender_name"];
                            }
                            if ((row["sender_address1"] != null) && (!row["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_address1"] = (string)row["sender_address1"];
                            }

                            if ((row["sender_address2"] != null) && (!row["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_address2"] = (string)row["sender_address2"];
                            }

                            if ((row["sender_zipcode"] != null) && (!row["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_zipcode"] = (string)row["sender_zipcode"];
                            }


                            if ((row["sender_state"] != null) && (!row["sender_state"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_state"] = (string)row["sender_state"];
                            }

                            if ((row["sender_telephone"] != null) && (!row["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_telephone"] = (string)row["sender_telephone"];
                            }

                            if ((row["sender_fax"] != null) && (!row["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_fax"] = (string)row["sender_fax"];
                            }
                            if ((row["sender_contact_person"] != null) && (!row["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_contact_person"] = (string)row["sender_contact_person"];
                            }
                            if ((row["sender_email"] != null) && (!row["sender_email"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_email"] = (string)row["sender_email"];
                            }

                            if ((row["recipient_telephone"] != null) && (!row["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_telephone"] = (string)row["recipient_telephone"];
                            }
                            if ((row["recipient_name"] != null) && (!row["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_name"] = (string)row["recipient_name"];
                            }
                            if ((row["recipient_address1"] != null) && (!row["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_address1"] = (string)row["recipient_address1"];
                            }

                            if ((row["recipient_address2"] != null) && (!row["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_address2"] = (string)row["recipient_address2"];
                            }

                            if ((row["recipient_zipcode"] != null) && (!row["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_zipcode"] = ((string)row["recipient_zipcode"]).ToUpper();
                            }

                            if ((row["recipient_state"] != null) && (!row["recipient_state"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_state"] = (string)row["recipient_state"];
                            }

                            if ((row["recipient_fax"] != null) && (!row["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_fax"] = (string)row["recipient_fax"];
                            }
                            if ((row["recipient_contact_person"] != null) && (!row["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_contact_person"] = (string)row["recipient_contact_person"];
                            }
                            if ((row["declare_value"] != null) && (!row["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["declare_value"] = (decimal)row["declare_value"];
                            }
                            if ((row["cod_amount"] != null) && (!row["cod_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["cod_amount"] = (decimal)row["cod_amount"];
                            }
                            if ((row["remark"] != null) && (!row["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["remark"] = (string)row["remark"];
                            }
                            if ((row["return_pod_slip"] != null) && (!row["return_pod_slip"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["return_pod_slip"] = (string)row["return_pod_slip"];
                            }
                            if ((row["return_invoice_hc"] != null) && (!row["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["return_invoice_hc"] = (string)row["return_invoice_hc"];
                            }
                            if ((row["DangerousGoods"] != null) && (!row["DangerousGoods"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["DangerousGoods"] = (byte)row["DangerousGoods"];
                            }
                            if ((row["MoreInfo"] != null) && (!row["MoreInfo"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["MoreInfo"] = (string)row["MoreInfo"];
                            }
                            if ((row["MorePkgs"] != null) && (!row["MorePkgs"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["MorePkgs"] = (string)row["MorePkgs"];
                            }
                            if ((row["Qty1"] != null) && (!row["Qty1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty1"] = (int)row["Qty1"];
                            }
                            if ((row["Qty2"] != null) && (!row["Qty2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty2"] = (int)row["Qty2"];
                            }
                            if ((row["Qty3"] != null) && (!row["Qty3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty3"] = (int)row["Qty3"];
                            }
                            if ((row["Qty4"] != null) && (!row["Qty4"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty4"] = (int)row["Qty4"];
                            }
                            if ((row["Qty5"] != null) && (!row["Qty5"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty5"] = (int)row["Qty5"];
                            }
                            if ((row["Qty6"] != null) && (!row["Qty6"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty6"] = (int)row["Qty6"];
                            }
                            if ((row["Qty7"] != null) && (!row["Qty7"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty7"] = (int)row["Qty7"];
                            }
                            if ((row["Qty8"] != null) && (!row["Qty8"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty8"] = (int)row["Qty8"];
                            }
                            if ((row["Qty"] != null) && (!row["Qty"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Qty"] = (int)row["Qty"];
                            }
                            if ((row["Wgt1"] != null) && (!row["Wgt1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt1"] = (decimal)row["Wgt1"];
                            }
                            if ((row["Wgt2"] != null) && (!row["Wgt2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt2"] = (decimal)row["Wgt2"];
                            }
                            if ((row["Wgt3"] != null) && (!row["Wgt3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt3"] = (decimal)row["Wgt3"];
                            }
                            if ((row["Wgt4"] != null) && (!row["Wgt4"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt4"] = (decimal)row["Wgt4"];
                            }
                            if ((row["Wgt5"] != null) && (!row["Wgt5"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt5"] = (decimal)row["Wgt5"];
                            }
                            if ((row["Wgt6"] != null) && (!row["Wgt6"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt6"] = (decimal)row["Wgt6"];
                            }
                            if ((row["Wgt7"] != null) && (!row["Wgt7"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt7"] = (decimal)row["Wgt7"];
                            }
                            if ((row["Wgt8"] != null) && (!row["Wgt8"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt8"] = (decimal)row["Wgt8"];
                            }
                            if ((row["Wgt"] != null) && (!row["Wgt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Wgt"] = (decimal)row["Wgt"];
                            }
                            if ((row["CopyName"] != null) && (!row["CopyName"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["CopyName"] = (string)row["CopyName"];
                            }
                            if ((row["sender_country"] != null) && (!row["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["sender_country"] = (string)row["sender_country"];
                            }
                            if ((row["recipient_country"] != null) && (!row["recipient_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["recipient_country"] = (string)row["recipient_country"];
                            }
                            if ((row["GoodsDescription"] != null) && (!row["GoodsDescription"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["GoodsDescription"] = (string)row["GoodsDescription"];
                            }
                            if ((row["tot_freight_charge"] != null) && (!row["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Rtg1"] = (string)row["tot_freight_charge"];
                            }
                            if ((row["total_rated_amount_toea"] != null) && (!row["total_rated_amount_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Rtg1_toea"] = (string)row["total_rated_amount_toea"];
                            }
                            if ((row["tax_on_rated_amount_kina"] != null) && (!row["tax_on_rated_amount_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Rtg2"] = (string)row["tax_on_rated_amount_kina"];
                            }
                            if ((row["tax_on_rated_amount_toea"] != null) && (!row["tax_on_rated_amount_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Rtg2_toea"] = (string)row["tax_on_rated_amount_toea"];
                            }
                            if ((row["total_amount_kina"] != null) && (!row["total_amount_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Rtg3"] = (string)row["total_amount_kina"];
                            }
                            if ((row["total_amount_toea"] != null) && (!row["total_amount_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Rtg3_toea"] = (string)row["total_amount_toea"];
                            }
                            if ((row["consignee_name"] != null) && (!row["consignee_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Consignee_name"] = (string)row["consignee_name"];
                            }
                            if ((row["pod_date"] != null) && (!row["pod_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Pod_date"] = (string)row["pod_date"];
                            }
                            if ((row["pod_time"] != null) && (!row["pod_time"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Pod_time"] = (string)row["pod_time"];
                            }
                            if ((row["basic_charge_kina"] != null) && (!row["basic_charge_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["basic_charge"] = (string)row["basic_charge_kina"];
                            }
                            if ((row["basic_charge_toea"] != null) && (!row["basic_charge_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["basic_charge_toea"] = (string)row["basic_charge_toea"];
                            }
                            if ((row["other_surch_amount_kina"] != null) && (!row["other_surch_amount_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["other_surch_amount"] = (string)row["other_surch_amount_kina"];
                            }
                            if ((row["other_surch_amount_toea"] != null) && (!row["other_surch_amount_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["other_surch_amount_toea"] = (string)row["other_surch_amount_toea"];
                            }
                            if ((row["Signature_Base64"] != null) && (!row["Signature_Base64"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["Signature_Base64"] = Convert.FromBase64String((string)row["Signature_Base64"]);
                            }
                            if ((row["ConsignmentDescription"] != null) && (!row["ConsignmentDescription"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["ConsignmentDescription"] = (string)row["ConsignmentDescription"];
                            }
                            if ((row["tot_vas_surcharge_kina"] != null) && (!row["tot_vas_surcharge_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["tot_vas_surcharge_kina"] = (string)row["tot_vas_surcharge_kina"];
                            }
                            if ((row["tot_vas_surcharge_toea"] != null) && (!row["tot_vas_surcharge_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["tot_vas_surcharge_toea"] = (string)row["tot_vas_surcharge_toea"];
                            }
                            if ((row["export_freight_charge_kina"] != null) && (!row["export_freight_charge_kina"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["export_freight_charge_kina"] = (string)row["export_freight_charge_kina"];
                            }
                            if ((row["export_freight_charge_toea"] != null) && (!row["export_freight_charge_toea"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                _drXSD["export_freight_charge_toea"] = (string)row["export_freight_charge_toea"];
                            }

                            string Barcode = "*" + _drXSD["consignment_no"].ToString().ToUpper() + "*";

                            Barcode bcd = new Barcode();
                            bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                            bcd.IncludeLabel = false;
                            bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                            bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 235, 65);

                            MemoryStream ms = new MemoryStream();
                            bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                            Byte[] oByte = ms.ToArray();
                            _drXSD["imgBarcode"] = oByte;

                            dsReportOnePage.Tables["CSS_PrintConsNotes"].Rows.Add(_drXSD);
                            orpt.SetDataSource(dsReportOnePage);
                            BindReportOnePage((string)row["consignment_no"]);
                            dsReportOnePage.Tables["CSS_PrintConsNotes"].Clear();

                        }
                        #endregion PrintConsNotesOnePage
                    }
                    catch (Exception ex)
                    {
                        System.IO.File.AppendAllLines(Logpath, new[] { DateTime.Now + ":" + ex.ToString() });
                    }
                }
                #endregion Domestic Connotes
                orpt.Close();
                orpt.Dispose();
            }
            //

            // Agency Invoice
            try
            {
                dsResult = CustomerConsignmentDAL.QueryInvoicedConsignment(
                   strAppID.ToString(),
                   strEnterpriseID.ToString(),
                   userloggedin.ToString(),
                   null,
                   null,
                   DateTime.MinValue,
                   DateTime.MinValue,
                   DateTime.MinValue,
                   DateTime.MinValue,
                   InvoiceDataFrom,
                   InvoiceDatato,
                   0
               );
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllLines(Logpath, new[] { DateTime.Now + ":" + ex.ToString() });
            }

            string agencyInvoiceList = "", strAgencyInvoice = "";
            dtList = dsResult.Tables[0];
            foreach (DataRow dr in dtList.Rows)
            {
                // Agency invoices
                strAgencyInvoice = dr["pngaf_agency_invoice_no"].ToString();
                #region Agency Invoices
                agencyInvoiceList = strAgencyInvoice;
                if (agencyInvoiceList.Length > 0)
                {
                    try
                    {
                        string strPNGAFAgencyInvoice = agencyInvoiceList;
                        dsAgencyInvoices = CustomsDAL.Customs_Invoice_Report_JobPOD(strAppID, strEnterpriseID, userloggedin, strPNGAFAgencyInvoice);
                    }
                    catch (Exception ex)
                    {
                        System.IO.File.AppendAllLines(Logpath, new[] { DateTime.Now + ":" + ex.ToString() });
                    }

                    string reportTemplate = dsAgencyInvoices.Tables[0].Rows[0]["InvoiceTemplate"].ToString();

                    ArrayList paramList = new ArrayList();
                    try
                    {
                        PNGAFCustomsInvoiceTemplate objReport = new PNGAFCustomsInvoiceTemplate();
                        objReport.ResourceName = dsAgencyInvoices.Tables[0].Rows[0]["InvoiceTemplate"].ToString();
                        orpt = objReport;

                        DataSet dsCustomsInvoice = dsAgencyInvoices;

                        Customs_Invioce dsReport = new Customs_Invioce();
                        #region customs invoice
                        foreach (DataRow drData in dsCustomsInvoice.Tables[1].Rows)
                        {
                            DataRow drAgencyInvoice = dsReport.Tables["Invoice"].NewRow();

                            // Customer_Name
                            if (drData["Customer_Name"] != null && drData["Customer_Name"].ToString() != "")
                            {
                                drAgencyInvoice["Customer_Name"] = drData["Customer_Name"].ToString();
                            }

                            // Customer_Address1
                            if (drData["Customer_Address1"] != null && drData["Customer_Address1"].ToString() != "")
                            {
                                drAgencyInvoice["Customer_Address1"] = drData["Customer_Address1"].ToString();
                            }

                            // Customer_Address2
                            if (drData["Customer_Address2"] != null && drData["Customer_Address2"].ToString() != "")
                            {
                                drAgencyInvoice["Customer_Address2"] = drData["Customer_Address2"].ToString();
                            }

                            // Customer_Address3
                            if (drData["Customer_Address3"] != null && drData["Customer_Address3"].ToString() != "")
                            {
                                drAgencyInvoice["Customer_Address3"] = drData["Customer_Address3"].ToString();
                            }

                            // Customer_Address4
                            if (drData["Customer_Address4"] != null && drData["Customer_Address4"].ToString() != "")
                            {
                                drAgencyInvoice["Customer_Address4"] = drData["Customer_Address4"].ToString();
                            }

                            // Customer_Telephone
                            if (drData["Customer_Telephone"] != null && drData["Customer_Telephone"].ToString() != "")
                            {
                                drAgencyInvoice["Customer_Telephone"] = drData["Customer_Telephone"].ToString();
                            }

                            // Customer_Fax
                            if (drData["Customer_Fax"] != null && drData["Customer_Fax"].ToString() != "")
                            {
                                drAgencyInvoice["Customer_Fax"] = drData["Customer_Fax"].ToString();
                            }

                            // Delivery_Name
                            if (drData["Delivery_Name"] != null && drData["Delivery_Name"].ToString() != "")
                            {
                                drAgencyInvoice["Delivery_Name"] = drData["Delivery_Name"].ToString();
                            }

                            // Delivery_Address1
                            if (drData["Delivery_Address1"] != null && drData["Delivery_Address1"].ToString() != "")
                            {
                                drAgencyInvoice["Delivery_Address1"] = drData["Delivery_Address1"].ToString();
                            }

                            // Delivery_Address2
                            if (drData["Delivery_Address2"] != null && drData["Delivery_Address2"].ToString() != "")
                            {
                                drAgencyInvoice["Delivery_Address2"] = drData["Delivery_Address2"].ToString();
                            }

                            // Delivery_Address3
                            if (drData["Delivery_Address3"] != null && drData["Delivery_Address3"].ToString() != "")
                            {
                                drAgencyInvoice["Delivery_Address3"] = drData["Delivery_Address3"].ToString();
                            }

                            // Delivery_Address4
                            if (drData["Delivery_Address4"] != null && drData["Delivery_Address4"].ToString() != "")
                            {
                                drAgencyInvoice["Delivery_Address4"] = drData["Delivery_Address4"].ToString();
                            }

                            // Delivery_Telephone
                            if (drData["Delivery_Telephone"] != null && drData["Delivery_Telephone"].ToString() != "")
                            {
                                drAgencyInvoice["Delivery_Telephone"] = drData["Delivery_Telephone"].ToString();
                            }

                            // Delivery_Fax
                            if (drData["Delivery_Fax"] != null && drData["Delivery_Fax"].ToString() != "")
                            {
                                drAgencyInvoice["Delivery_Fax"] = drData["Delivery_Fax"].ToString();
                            }

                            // Aircraft
                            if (drData["Aircraft"] != null && drData["Aircraft"].ToString() != "")
                            {
                                drAgencyInvoice["Aircraft"] = drData["Aircraft"].ToString();
                            }

                            // Folio
                            if (drData["Folio"] != null && drData["Folio"].ToString() != "")
                            {
                                drAgencyInvoice["Folio"] = drData["Folio"].ToString();
                            }

                            // LoadingPortCity
                            if (drData["LoadingPortCity"] != null && drData["LoadingPortCity"].ToString() != "")
                            {
                                drAgencyInvoice["LoadingPortCity"] = drData["LoadingPortCity"].ToString();
                            }

                            // ArrivalDate --
                            if (drData["ArrivalDate"] != null && drData["ArrivalDate"].ToString() != "")
                            {
                                drAgencyInvoice["ArrivalDate"] = drData["ArrivalDate"].ToString();
                            }

                            // WayBillNo
                            if (drData["WayBillNo"] != null && drData["WayBillNo"].ToString() != "")
                            {
                                drAgencyInvoice["WayBillNo"] = drData["WayBillNo"].ToString();
                            }

                            // Packages
                            if (drData["Packages"] != null && drData["Packages"].ToString() != "")
                            {
                                drAgencyInvoice["Packages"] = drData["Packages"].ToString();
                            }

                            // Description_Line1
                            if (drData["Description_Line1"] != null && drData["Description_Line1"].ToString() != "")
                            {
                                drAgencyInvoice["Description_Line1"] = drData["Description_Line1"].ToString();
                            }

                            // Description_Line2
                            if (drData["Description_Line2"] != null && drData["Description_Line2"].ToString() != "")
                            {
                                drAgencyInvoice["Description_Line2"] = drData["Description_Line2"].ToString();
                            }

                            // Description_Line3
                            if (drData["Description_Line3"] != null && drData["Description_Line3"].ToString() != "")
                            {
                                drAgencyInvoice["Description_Line3"] = drData["Description_Line3"].ToString();
                            }

                            // Description_Line4
                            if (drData["Description_Line4"] != null && drData["Description_Line4"].ToString() != "")
                            {
                                drAgencyInvoice["Description_Line4"] = drData["Description_Line4"].ToString();
                            }

                            // Description_Line5
                            if (drData["Description_Line5"] != null && drData["Description_Line5"].ToString() != "")
                            {
                                drAgencyInvoice["Description_Line5"] = drData["Description_Line5"].ToString();
                            }

                            // Description_Line6
                            if (drData["Description_Line6"] != null && drData["Description_Line6"].ToString() != "")
                            {
                                drAgencyInvoice["Description_Line6"] = drData["Description_Line6"].ToString();
                            }

                            // Weight
                            if (drData["Weight"] != null && drData["Weight"].ToString() != "")
                            {
                                drAgencyInvoice["Weight"] = drData["Weight"].ToString();
                            }

                            // Invoice_Date --
                            if (drData["Invoice_Date"] != null && drData["Invoice_Date"].ToString() != "")
                            {
                                drAgencyInvoice["Invoice_Date"] = drData["Invoice_Date"].ToString();
                            }

                            // CustomerAccountNo
                            if (drData["CustomerAccountNo"] != null && drData["CustomerAccountNo"].ToString() != "")
                            {
                                drAgencyInvoice["CustomerAccountNo"] = drData["CustomerAccountNo"].ToString();
                            }

                            // Invoice_Number
                            if (drData["Invoice_Number"] != null && drData["Invoice_Number"].ToString() != "")
                            {
                                drAgencyInvoice["Invoice_Number"] = drData["Invoice_Number"].ToString();
                            }

                            // TotalInvoiceAmount
                            if (drData["TotalInvoiceAmount"] != null && drData["TotalInvoiceAmount"].ToString() != "")
                            {
                                drAgencyInvoice["TotalInovoiceAmount"] = Convert.ToDecimal(drData["TotalInvoiceAmount"]).ToString("N2").Split('.')[0];
                                drAgencyInvoice["TotalInovoiceAmountDecimal"] = Convert.ToDecimal(drData["TotalInvoiceAmount"]).ToString("N2").Split('.')[1];
                            }

                            // POD_Date
                            if (drData["POD_Date"] != null && drData["POD_Date"].ToString() != "")
                            {
                                drAgencyInvoice["POD_Date"] = drData["POD_Date"].ToString();
                            }

                            // POD_Consignee
                            if (drData["POD_Consignee"] != null && drData["POD_Consignee"].ToString() != "")
                            {
                                drAgencyInvoice["POD_Consignee"] = drData["POD_Consignee"].ToString();
                            }

                            // Consignee_Signature
                            if ((drData["Consignee_Signature"] != null) && (!drData["Consignee_Signature"].GetType().Equals(System.Type.GetType("System.DBNull"))))
                            {
                                drAgencyInvoice["Consignee_Signature"] = Convert.FromBase64String((string)drData["Consignee_Signature"]);
                            }


                            for (int i = 1; i <= 14; i++)
                            {
                                string indexRow = i.ToString();
                                if (i >= 1 && i <= 9) { indexRow = "0" + i; }

                                // Fee01_Description
                                if (drData["Fee" + indexRow + "_Description"] != null && drData["Fee" + indexRow + "_Description"].ToString() != "")
                                {
                                    drAgencyInvoice["Fee" + indexRow + "_Description"] = drData["Fee" + indexRow + "_Description"].ToString();
                                }

                                // Fee01_GLCode
                                if (drData["Fee" + indexRow + "_GLCode"] != null && drData["Fee" + indexRow + "_GLCode"].ToString() != "")
                                {
                                    drAgencyInvoice["Fee" + indexRow + "_GLCode"] = drData["Fee" + indexRow + "_GLCode"].ToString();
                                }

                                // Fee01_Amount
                                if (drData["Fee" + indexRow + "_Amount"] != null && drData["Fee" + indexRow + "_Amount"].ToString() != "")
                                {
                                    string[] Fee_Amount = Convert.ToDecimal(drData["Fee" + indexRow + "_Amount"]).ToString("N2").Split('.');
                                    drAgencyInvoice["Fee" + indexRow + "_Amount"] = Fee_Amount[0];
                                    drAgencyInvoice["Fee" + indexRow + "_Amount_Decimal"] = Fee_Amount[1];
                                }
                            }

                            for (int i = 1; i <= 9; i++)
                            {

                                // Disb_Description
                                if (drData["Disb" + i + "_Description"] != null && drData["Disb" + i + "_Description"].ToString() != "")
                                {
                                    drAgencyInvoice["Disb" + i + "_Description"] = drData["Disb" + i + "_Description"].ToString();
                                }

                                // Disb_GLCode
                                if (drData["Disb" + i + "_GLCode"] != null && drData["Disb" + i + "_GLCode"].ToString() != "")
                                {
                                    drAgencyInvoice["Disb" + i + "_GLCode"] = drData["Disb" + i + "_GLCode"].ToString();
                                }

                                // Disb_Amount
                                if (drData["Disb" + i + "_Amount"] != null && drData["Disb" + i + "_Amount"].ToString() != "")
                                {
                                    drAgencyInvoice["Disb" + i + "_Amount"] = Convert.ToDecimal(drData["Disb" + i + "_Amount"]).ToString("N2").Split('.')[0];
                                    drAgencyInvoice["Disb" + i + "_Amount_Decimal"] = Convert.ToDecimal(drData["Disb" + i + "_Amount"]).ToString("N2").Split('.')[1];

                                }
                            }

                            for (int i = 1; i <= 4; i++)
                            {
                                if (drData["Disb_Other" + i] != null && drData["Disb_Other" + i].ToString() != "")
                                {
                                    drAgencyInvoice["Disb_Other" + i] = drData["Disb_Other" + i].ToString();
                                }
                            }


                            string Barcode = "*" + drData["Invoice_Number"].ToString().ToUpper() + "*";

                            Barcode bcd = new Barcode();
                            bcd.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                            bcd.IncludeLabel = false;
                            bcd.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                            bcd.Encode(TYPE.CODE39, Barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 265, 65);

                            MemoryStream ms = new MemoryStream();
                            bcd.SaveImage(ms, BarcodeLib.SaveTypes.PNG);

                            Byte[] oByte = ms.ToArray();
                            drAgencyInvoice["imgBarcode"] = oByte;

                            dsReport.Tables["Invoice"].Rows.Add(drAgencyInvoice);
                            orpt.SetDataSource(dsReport);
                            BindReportAgencyInvoiceOnePage((string)drAgencyInvoice["Invoice_Number"]);
                            dsReport.Tables["Invoice"].Clear();
                        }
                        #endregion customs invoice
                    }
                    catch (Exception ex)
                    {
                        System.IO.File.AppendAllLines(Logpath, new[] { DateTime.Now + ":" + ex.ToString() });
                    }

                    orpt.Close();
                    orpt.Dispose();
                }
                #endregion Agency Invoices
            }
        }

        static void BindReportOnePage(string invoiceNo)
        {
            try
            {

                ExportOptions ExportOptions = new ExportOptions();
                DiskFileDestinationOptions DiskFileDstOptions = new DiskFileDestinationOptions();
                
                strExportFolder = ConfigurationManager.AppSettings.Get("ReportExportFolder");
                if (System.IO.Directory.Exists(strExportFolder) == false) System.IO.Directory.CreateDirectory(path);

                strExportFile = strExportFolder + "\\" + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();

                string strPrinterName = ConfigurationManager.AppSettings.Get("PrinterName");
                if (strPrinterName != "")
                    orpt.PrintOptions.PrinterName = strPrinterName;

                orpt.PrintOptions.PaperSize = PaperSize.PaperA4;

                DiskFileDstOptions.DiskFileName = strExportFile;
                ExportOptions = orpt.ExportOptions;
                ExportOptions.DestinationOptions = DiskFileDstOptions;
                ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;

                // pdf format
                ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                ExportOptions.FormatOptions = new PdfRtfWordFormatOptions();

                
                orpt.Export();
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllLines(Logpath, new[] { DateTime.Now + ":" + ex.ToString() });
            }

            // Save pdf file
            string f = null;
            string s = null;
            string PDFFilename = "";

            try
            {
                f = System.Configuration.ConfigurationManager.AppSettings["FirstPrintedInvoicePDFFolder"];
                if (f != null && f != "")
                {
                    // Key exists		
                    PDFFilename = f + "\\" + invoiceNo + ".pdf";
                    if (!System.IO.File.Exists(PDFFilename)) // Not exists on First Printed folder
                    {
                        System.IO.File.Copy(strExportFile, PDFFilename, true); // Copy the pdf file to the original folder
                        s = System.Configuration.ConfigurationManager.AppSettings["DomesticInvoicePDFFolder"];
                        if (s != null && s != "")
                        {
                            // Key exists		
                            PDFFilename = s + "\\" + invoiceNo + ".pdf";
                            System.IO.File.Copy(strExportFile, PDFFilename, true); // Copy the pdf file to the Domestic Invoice folder
                        }
                    }
                    else
                    {
                        s = System.Configuration.ConfigurationManager.AppSettings["AmendedDomesticInvoicePDFFolder"];
                        if (s != null && s != "")
                        {
                            // Key exists		
                            PDFFilename = s + "\\" + invoiceNo + ".pdf";
                            System.IO.File.Copy(strExportFile, PDFFilename, true); // Copy the pdf file to the Amended Domestic Invoice folder
                        }
                    }
                }
                System.IO.File.Delete(strExportFile);

                System.IO.File.AppendAllLines(Logpath, new[] { DateTime.Now + ":" + PDFFilename + " generated."});
            }
            catch (Exception ex) {
                System.IO.File.AppendAllLines(Logpath, new[] { DateTime.Now + ":" + ex.ToString() });
            }
            
        }

        static void BindReportAgencyInvoiceOnePage(string invoiceNo)
        {
            try
            {

                ExportOptions ExportOptions = new ExportOptions();
                DiskFileDestinationOptions DiskFileDstOptions = new DiskFileDestinationOptions();

                strExportFolder = ConfigurationManager.AppSettings.Get("ReportExportFolder");
                if (System.IO.Directory.Exists(strExportFolder) == false) System.IO.Directory.CreateDirectory(path);

                strExportFile = strExportFolder + "\\" + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();

                string strPrinterName = ConfigurationManager.AppSettings.Get("PrinterName");
                if (strPrinterName != "")
                    orpt.PrintOptions.PrinterName = strPrinterName;

                orpt.PrintOptions.PaperSize = PaperSize.PaperA4;

                DiskFileDstOptions.DiskFileName = strExportFile;
                ExportOptions = orpt.ExportOptions;
                ExportOptions.DestinationOptions = DiskFileDstOptions;
                ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;

                // pdf format
                ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                ExportOptions.FormatOptions = new PdfRtfWordFormatOptions();


                orpt.Export();
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllLines(Logpath, new[] { DateTime.Now + ":" + ex.ToString() });
            }

            // Save pdf file
            string f = null;
            string s = null;
            string PDFFilename = "";

            try
            {
                f = System.Configuration.ConfigurationManager.AppSettings["FirstPrintedInvoicePDFFolder"];
                if (f != null && f != "")
                {
                    // Key exists		
                    PDFFilename = f + "\\" + invoiceNo + ".pdf";
                    if (!System.IO.File.Exists(PDFFilename)) // Not exists on First Printed folder
                    {
                        System.IO.File.Copy(strExportFile, PDFFilename, true); // Copy the pdf file to the original folder
                        s = System.Configuration.ConfigurationManager.AppSettings["CustomsInvoicePDFFolder"];
                        if (s != null && s != "")
                        {
                            // Key exists		
                            PDFFilename = s + "\\" + invoiceNo + ".pdf";
                            System.IO.File.Copy(strExportFile, PDFFilename, true); // Copy the pdf file to the Domestic Invoice folder
                        }
                    }
                    else
                    {
                        s = System.Configuration.ConfigurationManager.AppSettings["AmemdedCustomsInvoicePDFFolder"];
                        if (s != null && s != "")
                        {
                            // Key exists		
                            PDFFilename = s + "\\" + invoiceNo + ".pdf";
                            System.IO.File.Copy(strExportFile, PDFFilename, true); // Copy the pdf file to the Amended Domestic Invoice folder
                        }
                    }
                }
                System.IO.File.Delete(strExportFile);

                System.IO.File.AppendAllLines(Logpath, new[] { DateTime.Now + ":agency invocie " + PDFFilename + " generated." });
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllLines(Logpath, new[] { DateTime.Now + ":" + ex.ToString() });
            }

        }
    }
}
