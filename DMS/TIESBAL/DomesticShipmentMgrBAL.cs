using System;
using System.Data;
using com.ties.classes;
using com.ties.DAL;

namespace com.ties.BAL
{
	/// <summary>
	/// This struct is called in the GetCommitTime method.
	/// </summary>
	public struct ServiceTime
	{
		public int HourServiceTime;
		public String RegularTime;
	}

	/// <summary>
	/// Summary description for DomesticShipmentMgrBAL.
	/// </summary>
	public class DomesticShipmentMgrBAL
	{
		public DomesticShipmentMgrBAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		/// <summary>
		/// This method will perform the business logic to calculate the Insurance Surcharge.
		/// </summary>
		/// <param name="decDeclareValue"> Declare Value</param>
		/// <param name="decAddPercDV"> Additional Percentage DV</param>
		/// <param name="decMaxCoverage"> Maximum Coverage </param>
		/// <returns>Return the computed Insurance Surcharge as decimal data type.</returns>
		public decimal ComputeInsSurchargeTMP(decimal decDeclareValue, decimal decAddPercDV,decimal decMaxCoverage,decimal decFreeInsAmt)
		{

			decimal decInsSurchrg = 0;
//			if(decDeclareValue >= decMaxCoverage)
//			{
//				decInsSurchrg = (decMaxCoverage - decFreeInsAmt)*(decAddPercDV/100);
//			}
//			else if((decDeclareValue < decMaxCoverage) && (decDeclareValue > decFreeInsAmt))
//			{
//				decInsSurchrg = (decDeclareValue - decFreeInsAmt)*(decAddPercDV/100);
//			}
//			if(decInsSurchrg < 0)
//			{
//				decInsSurchrg = 0;
//			}
			if(decDeclareValue > decFreeInsAmt)
			{
				decInsSurchrg = (decDeclareValue - decFreeInsAmt)*(decAddPercDV/100);
			}
			else if (decDeclareValue > decMaxCoverage)
			{
				decInsSurchrg = (decMaxCoverage - decFreeInsAmt)*(decAddPercDV/100);
			}
			else if(decDeclareValue <= decFreeInsAmt)
			{
				decInsSurchrg = 0;
			}

			if(decInsSurchrg < 0)
			{
				decInsSurchrg = 0;
			}
			return decInsSurchrg;
		}


		public decimal ComputeInsSurchargeByCust(string appID, string enterpriseID,
					decimal decDeclareValue, string custID, Customer customer)
		{
			//INS Surcharge = %DV * MIN(MAX_INS_AMT, DECLARE_VALUE - FREE_AMT)
			decimal free_insurance_amt = 0;
			decimal max_insurance_amt = 0;
			decimal insurance_percent_surcharge = 0;
			decimal minValue = 0;
			decimal returnVal = 0;
			
			Enterprise enterprise = new Enterprise();
			enterprise.Populate(appID, enterpriseID);

			free_insurance_amt = enterprise.FreeInsuranceAmount;
			max_insurance_amt = enterprise.MaxInsuranceAmt;
			insurance_percent_surcharge = enterprise.InsPercSurchrg;


			if ((custID.Trim() != "") && (custID.Trim().ToUpper() != "NEW"))
			{
				DataSet dscustInfo = SysDataMgrDAL.GetCustInforByCustID(appID,enterpriseID, 0, 0, custID).ds;
				if(dscustInfo.Tables[0].Rows.Count > 0 )
				{
					DataRow rowcust = dscustInfo.Tables[0].Rows[0];

					if(rowcust["free_insurance_amt"]!=DBNull.Value && rowcust["free_insurance_amt"].ToString() != "")
					{
						//Fee_Coverage
						free_insurance_amt = (decimal)dscustInfo.Tables[0].Rows[0]["free_insurance_amt"];
					}

					if(rowcust["insurance_maximum_amt"]!=DBNull.Value && rowcust["insurance_maximum_amt"].ToString() != "")
					{
						//Max_Coverage
						max_insurance_amt = (decimal)dscustInfo.Tables[0].Rows[0]["insurance_maximum_amt"];
					}

					if(rowcust["insurance_percent_surcharge"]!=DBNull.Value && rowcust["insurance_percent_surcharge"].ToString() != "")
					{
						//Percent_Surcharge
						insurance_percent_surcharge = (decimal)dscustInfo.Tables[0].Rows[0]["insurance_percent_surcharge"];
					}
				}
			}

			if(max_insurance_amt > decDeclareValue)
				minValue = decDeclareValue;
			else if (max_insurance_amt < decDeclareValue)
				minValue = max_insurance_amt;
			else minValue = decDeclareValue;

			decimal IA  = 0;
			if ((minValue - free_insurance_amt) > 0)
				IA = minValue - free_insurance_amt;
		
			returnVal =  IA * (insurance_percent_surcharge / 100);

			if(decDeclareValue > free_insurance_amt)
			{
				//Modified
				//			minValue = Math.Min(decDeclareValue, max_insurance_amt);
				//			returnVal = insurance_percent_surcharge * Math.Max(0, minValue-free_insurance_amt)/100;

				//Customer checking
				if(customer.MinInsuranceSurcharge!= null && customer.MinInsuranceSurcharge != "")
				{
					returnVal = Math.Max(returnVal,Convert.ToDecimal(customer.MinInsuranceSurcharge));
				}
				if(customer.MaxInsuranceSurcharge !=null && customer.MaxInsuranceSurcharge != "")
				{
					returnVal = Math.Min(returnVal,Convert.ToDecimal(customer.MaxInsuranceSurcharge));
				}
			}

			if(customer.MaxInsuranceSurcharge !=null && customer.MaxInsuranceSurcharge != "")
			{
				if (Convert.ToDecimal(customer.MaxInsuranceSurcharge) == 0)
					returnVal = 0;
			}

			return returnVal;
		}


		/// <summary>
		/// This method calculates the Delivery Date
		/// <param name="pickUpDtTime">Pick Up Date Time</param>
		/// <param name="srvCode">Service Code</param>
		/// <param name="appID">Application ID</param>
		/// <param name="enterpriseID">Enterprise ID</param>
		/// <returns>Returns the delivery date as a string</returns>
		/// </summary>
		public String CalcDlvryDtTime(String appID,String enterpriseID,DateTime pickUpDtTime,String srvCode,String recipZip)
		{
			Service service = new Service();
			service.Populate(appID,enterpriseID,srvCode);
			DateTime dtCommitTime = service.CommitTime;
			String strCommitTime = dtCommitTime.ToString("HH:mm");
			decimal iTransitDay = service.TransitDay;
			int iTransitHour = service.TransitHour;
			DateTime dtCalc = pickUpDtTime;
			String strDt = "";
			String strDlvryDtTime = "";

			if(iTransitHour > 0)
			{
				//Add the hour to the pickup date
				//dtCalc = pickUpDtTime.AddHours(iTransitHour);
				strDlvryDtTime = dtCalc.ToString("dd/MM/yyyy") +" "+strCommitTime;
			}
			else if(iTransitDay >0)
			{
				//Add the day to the pickup date
				dtCalc = pickUpDtTime.AddDays(Convert.ToDouble(iTransitDay));
				strDt = dtCalc.ToString("dd/MM/yyyy");
				strDlvryDtTime = strDt+" "+strCommitTime;
			}
			
			//Both Transit Day and  Transit Hour is zero
			if((iTransitDay == 0) && (iTransitHour == 0))
			{
				strDt = dtCalc.ToString("dd/MM/yyyy");
				strDlvryDtTime = strDt+" "+strCommitTime;
				// Comment On 26 Feb 2010
//				//Get the Pickup date & Recipient's cut off time from the zipcode
//				strDt = pickUpDtTime.ToString("dd/MM/yyyy");
//				Zipcode zipcode = new Zipcode();
//				zipcode.Populate(appID,enterpriseID,recipZip);
//				DateTime dtCuttOff =  zipcode.CuttOffTime;
//				strCommitTime = dtCuttOff.ToString("HH:mm");
//				strDlvryDtTime = strDt+" "+strCommitTime;
			}
			else if((iTransitDay > 0) && (iTransitHour == 0) && (dtCommitTime.Hour == 0) && (dtCommitTime.Minute == 0))
			{
				//Add the day to the pickup request date & Get the pickup Date's time...
				strDt = dtCalc.ToString("dd/MM/yyyy");

				//Get the commit time from pickUp request Date time
				strCommitTime = pickUpDtTime.ToString("HH:mm");
				strDlvryDtTime = strDt+" "+strCommitTime;
			}
			
			return strDlvryDtTime;
		}


		/// <summary>
		/// This methods gets the time in hours or days depending on the service code.
		/// </summary>
		/// <param name="appID">Application ID</param>
		/// <param name="enterpriseID">Enterprise ID</param>
		/// <param name="srvCode">Service Code</param>
		/// <param name="recipZip">Recipient Zip Code</param>
		/// <returns>Returns a struct(ServiceTime) which contains the Hours(HourServiceTime) & Days(RegularTime) service time. </returns>
		public ServiceTime GetCommitTime(String appID,String enterpriseID,String srvCode,String recipZip)
		{
			ServiceTime serviceTime = new ServiceTime();
			serviceTime.HourServiceTime = 0;
			serviceTime.RegularTime = null;

			Service service = new Service();
			service.Populate(appID,enterpriseID,srvCode);
			DateTime dtCommitTime = service.CommitTime;
			String strCommitTime = dtCommitTime.ToString("HH:mm");
			decimal iTransitDay = service.TransitDay;
			int iTransitHour = service.TransitHour;
			
			if(iTransitHour > 0)
			{
				//GEt the hour
				serviceTime.HourServiceTime = iTransitHour;
			}
			else if(iTransitDay >0)
			{
				//Get the day's commit time
				serviceTime.RegularTime = strCommitTime;
			}
			
			//Both Transit Day and  Transit Hour is zero
			if((iTransitDay == 0) && (iTransitHour == 0))
			{
				//Get the Recipient's cut off time from the zipcode
				
				Zipcode zipcode = new Zipcode();
				zipcode.Populate(appID,enterpriseID,recipZip);
				DateTime dtCuttOff =  zipcode.CuttOffTime;
				strCommitTime = dtCuttOff.ToString("HH:mm");
				serviceTime.RegularTime = strCommitTime;
			}
			return serviceTime;
		}
	}
}
