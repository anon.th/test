using System;
using com.common.classes;
namespace com.common.AppExtension
{
	/// <summary>
	/// Summary description for AbsractFreightCharge.
	/// </summary>
	public abstract class AbstractFreightCharge : IFreightCharge
	{
		public AbstractFreightCharge()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public abstract Decimal ComputeFreightCharge(FreightCharge fc);
		
		public Object CoreEnterprise()
		{
			CoreEnterprise objCoreEnterprise = new CoreEnterprise();
			return objCoreEnterprise;
		}

		public Object Logger(String strApplicationID,String strEnterpriseID)
		{
			Object obj = null;
			return obj;
		}

		public Object DatabaseConnection(String strApplicationID,String strEnterpriseID)
		{
			Object obj = null;
			return obj;
		}

		public Object User(String strApplicationID,String strEnterpriseID)
		{
			Object obj = null;
			return obj;
		}

		public Object SystemCode(String strApplicationID,String strEnterpriseID)
		{
			Object obj = null;
			return obj;
		}
	}
}
