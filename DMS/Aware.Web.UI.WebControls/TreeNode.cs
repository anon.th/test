﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Web.UI.WebControls;

namespace Aware.Web.UI.WebControls
{
    public class TreeNode : System.Web.UI.WebControls.TreeNode
    {

        public bool CheckBox
        {
            get
            {
                return this.Checked;
            }
            set
            {
                this.Checked = value;
            }
        }

        public string NodeData
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
            }


        }

    }

}
