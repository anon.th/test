using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;

namespace TIESDAL
{
	/// <summary>
	/// Summary description for MaintainIMEI.
	/// </summary>
	public class MaintainIMEIDAL
	{
		public MaintainIMEIDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static System.Data.DataSet GetMobilePhoneNoIMEI(string strAppID, string strEnterpriseID, string strPhoneNo, string strImei)
		{

			System.Data.DataSet result = new DataSet();		
			string strSQL = "";

			strSQL = " SELECT applicationid, enterpriseid, PhoneNo, IMEI, DeactivatedDate, applicationid+enterpriseid+PhoneNo+IMEI as modifyKey ";
			strSQL +=" FROM Mobile_PhoneNosIMEIs ";
			strSQL +=" WHERE applicationid = '"+strAppID+"' AND enterpriseid =  '"+strEnterpriseID+"' ";
			strSQL +=" AND PhoneNo = '" + strPhoneNo +"'" ;
			strSQL +=" AND IMEI = '"+ strImei + "'";
			strSQL +=" ORDER BY PhoneNo ";
				
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("MaintainIMEIDAL","GetMobilePhoneNoIMEI","GetMobilePhoneNoIMEI","DbConnection object is null!!");
				return null;
			}
			
			SessionDS sessionDS = dbCon.ExecuteQuery(strSQL, 0, 0, "Mobile_PhoneNosIMEIs");
			result = sessionDS.ds;
			return result;
		}
	
		
		public static int ModifyMobilePhoneNoIMEI(string strAppID, string strEnterpriseID, string strPhoneNo, string strImei, DateTime dateDeacivated ,string modifyKey)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MaintainIMEIDAL","ModifyMobilePhoneNoIMEI","ModifyMobilePhoneNoIMEI","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("UPDATE Mobile_PhoneNosIMEIs SET ");
			strBuilder.Append(" PhoneNo = '"+ strPhoneNo + "' ");
			strBuilder.Append(", IMEI = '"+ strImei +"' ");

			String strDateTime = Utility.DateFormat(strAppID,strEnterpriseID,dateDeacivated,DTFormat.DateTime);
			strBuilder.Append(", DeactivatedDate = '"+ strDateTime +"'");
			strBuilder.Append(" WHERE applicationid+enterpriseid+PhoneNo+IMEI = '" + modifyKey+ "' ");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("MaintainIMEIDAL","ModifyMobilePhoneNoIMEI","SDM001",iRowsAffected+" rows update in to Mobile_PhoneNosIMEIs table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyMobilePhoneNoIMEI","SDM001","Error During update "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Mobile_PhoneNosIMEIs",appException);
			}

			return iRowsAffected;

		}
	
		public static bool DeleteMobilePhoneNoIMEI(string strAppID, string strEnterpriseID, string strPhoneNo, string strImei)
		{
				  int iRowsDeleted = 0;
					bool isDelette =false;
				  DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

				  if(dbCon == null)
				  {
					  Logger.LogTraceError("MaintainIMEIDAL","DeleteMobilePhoneNoIMEI","EDB101","DbConnection object is null!!");
					  return false;
				  }
				 string strSQL = "";
			
				    strSQL =" DELETE FROM Mobile_PhoneNosIMEIs ";
					strSQL +=" WHERE applicationid = '"+strAppID+"' AND enterpriseid =  '"+strEnterpriseID+"' ";
					strSQL +=" AND PhoneNo = '" + strPhoneNo +"'" ;
					strSQL +=" AND IMEI = '"+ strImei + "'";
//					String strDateTime = Utility.DateFormat(strAppID,strEnterpriseID,dateDeacivated,DTFormat.DateTime);
//					strSQL +=" AND DeactivatedDate = '"+ strDateTime +"'";
				
				  IDbCommand dbCmd = dbCon.CreateCommand(strSQL,CommandType.Text);
			
				  try
				  {
					  iRowsDeleted = dbCon.ExecuteNonQuery(dbCmd);
					  if(iRowsDeleted >0)
						  isDelette = true;
					  Logger.LogTraceInfo("MaintainIMEIDAL","DeleteMobilePhoneNoIMEI","SDM001",iRowsDeleted+" rows deleted from  Exception_Code table");
				  }
				  catch(ApplicationException appException)
				  {
					  Logger.LogTraceError("MaintainIMEIDAL","DeleteMobilePhoneNoIMEI","SDM001","Error During Delete "+ appException.Message.ToString());
					  throw new ApplicationException("Error inserting Status Code "+appException.Message,appException);
				  }

				  return isDelette;		
			  }
	
		

		//
		public static SessionDS GetMobilePhoneNoIMEIDS(string strAppID, string strEnterpriseID, string strPhoneNo, string strImei)
		{

			SessionDS sessionDS =new SessionDS ();		
			string strSQL = "";

			strSQL = " SELECT applicationid, enterpriseid, PhoneNo, IMEI, CONVERT( varchar,DeactivatedDate,103) as DeactivatedDate,Remark, applicationid+enterpriseid+PhoneNo+IMEI as modifyKey ";
			strSQL +=" FROM Mobile_PhoneNosIMEIs ";
			strSQL +=" WHERE applicationid = '"+strAppID+"' ";
			strSQL +=" AND enterpriseid =  '"+strEnterpriseID+"' ";
			
			if ( strPhoneNo !=null && !strPhoneNo.Equals(""))
			{
				strSQL +=" AND PhoneNo like '%" + strPhoneNo +"%'" ;
			}
			if ( strImei !=null && !strImei.Equals(""))
			{
				strSQL +=" AND IMEI like '%"+ strImei + "%'";
			}
			strSQL +=" ORDER BY PhoneNo ";
				
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("MaintainIMEIDAL","GetMobilePhoneNoIMEI","GetMobilePhoneNoIMEI","DbConnection object is null!!");
				return null;
			}
			
			sessionDS = dbCon.ExecuteQuery(strSQL, 0, 0, "Mobile_PhoneNosIMEIs");
			return sessionDS;
		}
	

		public static bool ModifyMobilePhoneNoIMEI(string strAppID, string strEnterpriseID, SessionDS sessionDS, string modifyKey)
		{
			int iRowsAffected = 0;
			bool isModify = false ;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MaintainIMEIDAL","ModifyMobilePhoneNoIMEI","ModifyMobilePhoneNoIMEI","DbConnection object is null!!");
				return false;
			}
			DataRow dr = sessionDS.ds.Tables[0].Rows[0];
			String strPhoneNo = (String)dr[2];
			String strImei = (String)dr[3];
			String strDeacivated = (String)dr[4];
			String strRemark = (String)dr[5];
			DateTime dateDeacivated =new DateTime() ;
			if(strDeacivated != "")
			{
				 dateDeacivated = DateTime.ParseExact(strDeacivated,"dd/MM/yyyy",null);
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("UPDATE Mobile_PhoneNosIMEIs SET ");
			strBuilder.Append(" PhoneNo = '"+ strPhoneNo + "' ");
			strBuilder.Append(", IMEI = '"+ strImei +"' ");
			//strBuilder.Append(", Remark = '"+ strRemark +"' ");
			strBuilder.Append(", Remark = @Remark ");

			String strDateTime ="";
			if(strDeacivated !="")
			{
				strDateTime = Utility.DateFormat(strAppID,strEnterpriseID,dateDeacivated,DTFormat.DateTime);
				strBuilder.Append(", DeactivatedDate = "+ strDateTime );
			}
			else
			{
				strBuilder.Append(", DeactivatedDate = null " );
			}
	
			
			strBuilder.Append(" WHERE applicationid+enterpriseid+PhoneNo+IMEI = '" + modifyKey+ "' ");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			SqlParameter param  = new SqlParameter();
			param.ParameterName = "@Remark";
			param.Value = strRemark ;
			dbCmd.Parameters.Add(param);
			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				if(iRowsAffected >0)
					isModify = true;
			
				Logger.LogTraceInfo("MaintainIMEIDAL","ModifyMobilePhoneNoIMEI","SDM001",iRowsAffected+" rows update in to Mobile_PhoneNosIMEIs table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyMobilePhoneNoIMEI","SDM001","Error During update "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Mobile_PhoneNosIMEIs",appException);
			}

			return isModify;

		}
	
		public static bool DeleteMobilePhoneNoIMEI(string strAppID, string strEnterpriseID, SessionDS sessionDS )
		{
			int iRowsDeleted = 0;
			bool isDelette =false ;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MaintainIMEIDAL","DeleteMobilePhoneNoIMEI","EDB101","DbConnection object is null!!");
				return false;
			}

			DataRow dr = sessionDS.ds.Tables[0].Rows[0];
			String strPhoneNo = (String)dr[0];
			String strImei = (String)dr[1];

			string modifyKey = strAppID+strEnterpriseID+strPhoneNo+strImei;

			string strSQL = "";
			
			strSQL =" DELETE FROM Mobile_PhoneNosIMEIs ";
			strSQL +=" WHERE applicationid = '"+strAppID+"' AND enterpriseid =  '"+strEnterpriseID+"' ";
			strSQL +=" AND PhoneNo = '" + strPhoneNo +"'" ;
			strSQL +=" AND IMEI = '"+ strImei + "'";
			//String strDateTime = Utility.DateFormat(strAppID,strEnterpriseID,dateDeacivated,DTFormat.DateTime);
			//strSQL +=" AND DeactivatedDate = '"+ strDateTime +"'";
				
			IDbCommand dbCmd = dbCon.CreateCommand(strSQL,CommandType.Text);
			
			try
			{
				iRowsDeleted = dbCon.ExecuteNonQuery(dbCmd);
				if(iRowsDeleted >0)
					isDelette = true;
				Logger.LogTraceInfo("MaintainIMEIDAL","DeleteMobilePhoneNoIMEI","SDM001",iRowsDeleted+" rows deleted from  Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("MaintainIMEIDAL","DeleteMobilePhoneNoIMEI","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code "+appException.Message,appException);
			}

			return isDelette;		
		}

	
		public static bool InsertMobilePhoneNoIMEI(string strAppID, string strEnterpriseID,string strPhoneNo, string strImei, string strDeacivated,string strRemark )
		{
			int iRowsAdd= 0;
			bool isAdd =false ;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MaintainIMEIDAL","DeleteMobilePhoneNoIMEI","EDB101","DbConnection object is null!!");
				return false;
			}

			DateTime dateDeacivated = new DateTime() ;
			if(strDeacivated != "")
			{
				dateDeacivated = DateTime.ParseExact(strDeacivated,"dd/MM/yyyy",null);
			}

			string strSQL = "";
			strSQL =" INSERT INTO Mobile_PhoneNosIMEIs(applicationid,enterpriseid,PhoneNo,IMEI,Remark,DeactivatedDate) ";
			strSQL +=" VALUES ('"+strAppID+"','"+strEnterpriseID+"','" + strPhoneNo +"','"+ strImei + "', @Remark ";
			
			String strDateTime ="";
			if(strDeacivated !="")
			{
				strDateTime = Utility.DateFormat(strAppID,strEnterpriseID,dateDeacivated,DTFormat.DateTime);
				strSQL +=","+ strDateTime ;
			}
			else
			{
				strSQL +=", null";
			}
			strSQL +=" )";
				
			IDbCommand dbCmd = dbCon.CreateCommand(strSQL,CommandType.Text);
			SqlParameter param  = new SqlParameter();
			param.ParameterName = "@Remark";
			param.Value = strRemark ;
			dbCmd.Parameters.Add(param);

				iRowsAdd = dbCon.ExecuteNonQuery(dbCmd);
				if(iRowsAdd >0)
					isAdd = true;
			return isAdd;		
		}
	
		public static string checkDupKey(string strAppID, string strEnterpriseID, string strPhoneNo, string strImei, string culture)
		{
			string strResult = "";
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("MaintainIMEIDAL","checkDupKey","EDB101","DbConnection object is null!!");
				return null;
			}

			//check
			string phoneImei = strPhoneNo +strImei;
			string strSQLCheckDup = "";
			strSQLCheckDup = " SELECT ISNULL(SUM(dupPhone),-1) as dupPhone,ISNULL(SUM(dupIMEI),-1) as dupIMEI,ISNULL(SUM(dupAll),-1) as dupAll FROM ( ";
			strSQLCheckDup += "	SELECT ";
			strSQLCheckDup +=" CASE PhoneNo WHEN '"+strPhoneNo+"' THEN 1 ELSE 0 END dupPhone ";
			strSQLCheckDup +=" ,CASE IMEI WHEN '"+ strImei + "' THEN 1 ELSE 0 END dupIMEI ";
			strSQLCheckDup +=" ,CASE PhoneNo+ IMEI WHEN '"+phoneImei+"' THEN 1 ELSE 0 END dupAll ";
			//--,PhoneNo,IMEI,PhoneNo+ IMEI
			strSQLCheckDup +=" FROM Mobile_PhoneNosIMEIs ";
			strSQLCheckDup +=" WHERE PhoneNo= '" + strPhoneNo +"' ";
			strSQLCheckDup +=" OR IMEI ='"+ strImei + "' ";
			strSQLCheckDup +=" AND applicationid ='"+ strAppID + "' ";
			strSQLCheckDup +=" AND enterpriseid ='"+ strEnterpriseID + "' ) A";			
				
			SessionDS sessionDS = dbCon.ExecuteQuery(strSQLCheckDup, 0, 0, "CheckDupKey");
			if(sessionDS !=null && sessionDS.ds !=null && sessionDS.ds.Tables[0] !=null )
			{
			    int dupPhone = Convert.ToInt32( sessionDS.ds.Tables[0].Rows[0]["dupPhone"]);
				int dupIMEI =  Convert.ToInt32(sessionDS.ds.Tables[0].Rows[0]["dupIMEI"] ) ;
				int dupAll =  Convert.ToInt32(sessionDS.ds.Tables[0].Rows[0]["dupAll"] ) ;

				if( dupPhone == -1 )
				{
					strResult = "";
				}
				else if(dupAll >0 )
				{
					
					string mes = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_PHONE_IMEI",culture);
					strResult = string.Format( "{0}:{1}",1, mes);
				}
				else if(dupPhone >0)
				{
					
					string mes = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_PHONE",culture);
					strResult = string.Format( "{0}:{1}",2, mes );
				}
				else if(dupIMEI >0)
				{
					string mes = Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_IMEI",culture);
					strResult = string.Format( "{0}:{1}",3, mes);
				}
			}

			return strResult;	
		}
	
	
		public static SessionDS ValidateIMEI(string strAppID, string strEnterpriseID,string strImei)
		{
			SessionDS sessionDS =new SessionDS ();		
			string strSQL = "";

			strSQL = " SELECT ErrorCode, ErrorMessage ";
			strSQL +=" FROM ValidateIMEI('"+ strImei +"') ";
			
				
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("MaintainIMEIDAL","ValidateIMEI","ValidateIMEI","DbConnection object is null!!");
				return null;
			}
			
			sessionDS = dbCon.ExecuteQuery(strSQL, 0, 0, "ErrorMessage");
			return sessionDS;
		}

	}
}
