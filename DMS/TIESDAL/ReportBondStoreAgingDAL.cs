using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using TIESClasses;

namespace TIESDAL
{
	public class ReportBondStoreAgingDAL
	{
		private string SQLText = string.Empty;
		private DbConnection dbCon;
		private IDbCommand dbCmd = null;

		public ReportBondStoreAgingDAL(string _appID, string _enterpriseID) 
		{ 
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(_appID, _enterpriseID);
		}

		public DataSet GetBondedWarehouses(string _enterpriseId)
		{
			DataSet dsReturn = new DataSet();

			SQLText = string.Format(@"SELECT DropDownListDisplayValue, 
											 DefaultValue, 
											 [Description], 
											 CodedValue
									  FROM dbo.GetCustomsSpecialConfiguration('{0}', 'BondedWarehouses', 0);", _enterpriseId);
			try
			{
				dsReturn = (DataSet)dbCon.ExecuteQuery(SQLText, ReturnType.DataSetType);
				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method GetCurrencies : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CurrencyExchangeRatesDAL", "GetCurrencies", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
		}
	}
}
