using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;
using System.Collections;
using System.Text.RegularExpressions;
using TIESClasses;

namespace TIESDAL
{
	/// <summary>
	/// Summary description for CustomsChangeCustomerIDforJobDAL.
	/// </summary>
	public class CustomsChangeCustomerIDforJobDAL
	{
		public CustomsChangeCustomerIDforJobDAL()
		{
			
		}

		public static System.Data.DataSet GetConsignmentRegEx( string strAppID, string strEnterpriseID )
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsChangeCustomerIDforJobDAL","GetConsignmentRegEx","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT [key], [value] ");
				strQry.Append(" FROM dbo.EnterpriseConfigurations('" + strEnterpriseID + "', 'CustomsChangeCustomerID');" );  
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsChangeCustomerIDforJobDAL", "GetConsignmentRegEx", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			}
		}

		public static System.Data.DataSet GetCustomsChangeCustomerID(string strAppID
																	, string strEnterpriseID
																	, string strUserloggedin
																	, string jobEntryNo
																	, string houseAWBNo
																	, string currentCustomerID
																	, string newCustomerID
																	, string newHouseAWBNumber)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsChangeCustomerIDforJobDAL","GetCustomsChangeCustomerID","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append("EXEC dbo.Customs_ChangeCustomerID ");
				strQry.Append("@enterpriseid='" + strEnterpriseID + "'");
				strQry.Append(", @userloggedin='" + strUserloggedin + "'");
				strQry.Append(", @JobEntryNo=" + jobEntryNo);
				strQry.Append(", @HouseAWBNo='" + houseAWBNo + "'");
				strQry.Append(", @CurrentCustomerID = '" + currentCustomerID + "'");
				if (newCustomerID != "")
					strQry.Append(", @NewCustomerID = '" + newCustomerID + "';");
				else if (newHouseAWBNumber != "")
					strQry.Append(", @NewHouseAWBNo = '" + newHouseAWBNumber + "';");
				
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsChangeCustomerIDforJobDAL", "GetCustomsChangeCustomerID", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			}
		}

		public static System.Data.DataSet GetCustomsChangeCustomerID(string strAppID
																	, string strEnterpriseID
																	, string strUserloggedin
																	, string jobEntryNo
																	, string houseAWBNo
																	, string currentCustomerID
																	, string newCustomerID
																	, string newHouseAWBNumber
																	, string resequence)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsChangeCustomerIDforJobDAL","GetCustomsChangeCustomerID","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append("EXEC dbo.Customs_ChangeCustomerID ");
				strQry.Append("@enterpriseid = '" + strEnterpriseID + "' ");
				strQry.Append(",@userloggedin = '" + strUserloggedin + "' ");
				strQry.Append(",@JobEntryNo = " + jobEntryNo + " ");
				strQry.Append(",@HouseAWBNo = '" + houseAWBNo + "' ");
				strQry.Append(",@CurrentCustomerID = '" + currentCustomerID + "' ");
				strQry.Append(",@NewCustomerID = '" + newCustomerID + "' ");
				strQry.Append(",@Resequence = " + resequence + ";");
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsChangeCustomerIDforJobDAL", "GetCustomsChangeCustomerID", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			}
		}
	}
}
