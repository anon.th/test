using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
namespace TIESDAL
{
	/// <summary>
	/// Summary description for ConfigureCustomerSelfServiceMgrDAL.
	/// </summary>
	public class ConfigureCustomerSelfServiceMgrDAL
	{
		public ConfigureCustomerSelfServiceMgrDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		//Add by Tatiya.p 25/03/2014
		public static DataTable GetPayerID(String strAppID, String strEnterpriseID)
		{
			DataSet dsResult = new DataSet();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","ConfigureCustomerSelfServiceMgrDAL","GetPayerID","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append(" SELECT a.custid FROM dbo.Customer a JOIN dbo.User_Master b ");
				strBuilder.Append(" ON a.applicationid = b.applicationid AND a.enterpriseid = b.enterpriseid AND a.custid = b.payerid ");
				strBuilder.Append(" where a.applicationid='"+strAppID+"' and a.enterpriseid = '"+strEnterpriseID+"' ");
				strBuilder.Append(" GROUP BY a.applicationid, a.enterpriseid, a.custid;  ");

				String strSQLQuery = strBuilder.ToString();	
				dsResult = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
				return dsResult.Tables[0];
			}	
			catch
			{
				Logger.LogTraceError("TIESDAL", "ConfigureCustomerSelfServiceMgrDAL", "GetPayerID", "Select command failed");
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			}
			
		}

		
		public static bool IsMaster(String strAppID, String strEnterpriseID,String strPayerID,String strCusID)
		{
			bool isMaster=true;
			IDbCommand dbCmd = null;
			String strSQLQuery  ="";
			DataSet dsResult = new DataSet();

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConfigureCustomerSelfServiceMgrDAL", "IsMaster", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}
			try
			{
			
				strSQLQuery = " SELECT ISNULL( (  SELECT IsMaster  FROM dbo.CSS_Accounts ";
				strSQLQuery += " where applicationid = '" + strAppID  +  "' ";
				strSQLQuery += " and enterpriseid = '" + strEnterpriseID  +  "' ";
				strSQLQuery += " and payerid = '" + strPayerID  +  "' ";
				strSQLQuery += " and userid = '" + strCusID +  "' ), 1) as IsMaster; ";
			
		
				dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				DataTable dt = dsResult.Tables[0];

				if(dt.Rows.Count > 0 && dt.Rows[0]["IsMaster"] != null )
				{
					if(Convert.ToInt32( dt.Rows[0]["IsMaster"] ) > 0)
					{
						isMaster= true;
					}
					else
					{
						isMaster= false;
					}
				}
			}
			catch
			{
				Logger.LogTraceError("TIESDAL", "ConfigureCustomerSelfServiceMgrDAL", "IsMaster", "Select command failed");
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			}
			return isMaster;
		}
	
	
		public static DataTable GetSenderName(String strAppID, String strEnterpriseID, String strUserLogin, String strPayerID)
		{
			DataSet dsResult = new DataSet();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","ConfigureCustomerSelfServiceMgrDAL","GetSenderName","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append(" SELECT snd_rec_name FROM dbo.GetSenderNames ( ");
				strBuilder.Append(" '"+ strEnterpriseID+"' ");
				strBuilder.Append(" ,'" +  strUserLogin + "' ");
				strBuilder.Append(" ,'"+  strPayerID +"' )");
		
				String strSQLQuery = strBuilder.ToString();	
				dsResult = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
				return dsResult.Tables[0];
			}	
			catch
			{
				Logger.LogTraceError("TIESDAL", "ConfigureCustomerSelfServiceMgrDAL", "GetSenderName", "Select command failed");
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			}
			
		}

	
		public static DataSet SearchConfigCusSelfService( String strAppID,String strEnterpriseID,String strUserLogin,String strPayerID,String strUserID)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","ConfigureCustomerSelfServiceMgrDAL","SearchConfigCusSelfService","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" EXEC dbo.CSS_ConfigureSelfService  @action= 0  ");
				strQry.Append(" ,@enterpriseid  = '"+ strEnterpriseID +"' "); 
				if( strUserLogin != String.Empty)
				{
					strQry.Append(" ,@userloggedin  = '"+ strUserLogin +"' "); 
				}
				if( strPayerID != String.Empty)
				{
					strQry.Append(" ,@payerid  = '"+ strPayerID +"' "); 
				}

				if( strUserID != String.Empty)
				{
					strQry.Append(" ,@userid  = '"+ strUserID +"' "); 
				}
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "ConfigureCustomerSelfServiceMgrDAL", "SearchConfigCusSelfService", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			}
			
		}
	
		
		public static DataSet UpdateConfigCusSelfService( String strAppID,String strEnterpriseID,String strPayerID,String strUserID,String strlogginID,bool IsMaster,String strSenderName,int intEnterPriceCopies, int intCustomerCopies ,bool isCustomer)
		{
			DataSet dsResult = null;
			IDbCommand dbCmd = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","ConfigureCustomerSelfServiceMgrDAL","UpdateConfigCusSelfService","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
					
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" EXEC dbo.CSS_ConfigureSelfService  @action= 1  ");
				strQry.Append(" ,@enterpriseid  = '"+ strEnterpriseID +"' "); 
				strQry.Append(" ,@userloggedin  = '"+ strlogginID +"' "); 
				strQry.Append(" ,@payerid  = '"+ strPayerID +"' "); 
				strQry.Append(" ,@userid  = '"+ strUserID +"' "); 
				if(!isCustomer)strQry.Append(" ,@IsMaster  = "+ (IsMaster ? 1:0 )); 
				strQry.Append(" ,@SenderName  = '"+ strSenderName +"' "); 
				if(!isCustomer)strQry.Append(" ,@EnterpriseCopies  = "+ intEnterPriceCopies );
				strQry.Append(" ,@CustomerCopies  = "+ intCustomerCopies +" ; "); 

								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
					
				return dsResult;
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "ConfigureCustomerSelfServiceMgrDAL", "UpdateConfigCusSelfService", "ERR009", "Update Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error updateing UpdateConfigCusSelfService", ex); 
			}
			finally
			{
				dsResult.Dispose();
			}
		
		}	

		
		public static DataSet GetEmptyConfigureCustomerSelfService()
		{
			DataTable dtImport =  new DataTable();
		     
			dtImport.Columns.Add(new DataColumn("IsMaster",typeof(string)));
			dtImport.Columns.Add(new DataColumn("userid",typeof(string)));
			dtImport.Columns.Add(new DataColumn("user_name",typeof(string)));
			dtImport.Columns.Add(new DataColumn("EnterpriseCopies",typeof(int)));
			dtImport.Columns.Add(new DataColumn("CustomerCopies",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("snd_rec_name",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("address1",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("zipcode",typeof(string))); 
			dtImport.Columns.Add(new DataColumn("payerid",typeof(string)));  

			DataRow drEach = dtImport.NewRow();

			dtImport.Rows.Add(drEach);
			DataSet dsImport = new DataSet();
			dsImport.Tables.Add(dtImport);
			return  dsImport;	
		}

		public static string GetDefaultSenderName(String strAppID, String strEnterpriseID)
		{
			IDbCommand dbCmd = null;
			string strSQLQuery  = "";
			string strDefaultSenderName = "";
			DataSet dsResult = new DataSet();

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConfigureCustomerSelfServiceMgrDAL", "GetDefaultSenderName", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}
			try
			{
			
				strSQLQuery = "SELECT code_text FROM dbo.Enterprise_Configurations WHERE applicationid = '" + strAppID + "' AND enterpriseid = '" + strEnterpriseID + "' AND codeid = 'CustomerBillingReference'";
					
				dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				DataTable dt = dsResult.Tables[0];

				if(dt.Rows.Count > 0 && dt.Rows[0]["code_text"].ToString() != "" )
				{
					strDefaultSenderName = dt.Rows[0]["code_text"].ToString();
				}
			}
			catch
			{
				Logger.LogTraceError("TIESDAL", "ConfigureCustomerSelfServiceMgrDAL", "GetDefaultSenderName", "Select command failed");
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			}
			return strDefaultSenderName;
		}
	}
}
