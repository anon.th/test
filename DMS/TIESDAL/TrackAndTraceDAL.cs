
using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;
using System.Collections;
using System.Xml;


namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for TrackAndTraceDAL.
	/// </summary>
	public class TrackAndTraceDAL
	{
		public TrackAndTraceDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static bool IsConsgmentExist(String strAppID, String strEnterpriseID,String consgmtNo)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			bool isExists = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TrackAndTraceManager","IsConsgmtExist","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select * from shipment where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and consignment_no = '");
			strBuild.Append(Utility.ReplaceSingleQuote(consgmtNo)+"'");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					isExists = true;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TrackAndTraceManager","IsConsgmtExist","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("TrackAndTraceManager","IsConsgmtExist","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return isExists;
		}


		public static bool IsCustRefExist(String strAppID, String strEnterpriseID,String refNo)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			bool isExists = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TrackAndTraceManager","IsCustRefExist","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			// Thosapol Yennam 16/08/2013 Comment Chang Where clause field ref_no is LIKE [operation]
//			strBuild =  new StringBuilder();
//			strBuild.Append("select * from shipment where applicationid = '");
//			strBuild.Append(strAppID+"'");
//			strBuild.Append(" and enterpriseid = '");
//			strBuild.Append(strEnterpriseID+"'");
//			strBuild.Append(" and ref_no = '");
//			strBuild.Append(Utility.ReplaceSingleQuote(refNo)+"'");

			// Thosapol Yennam 16/08/2013 Add new code Where clause field ref_no is LIKE [operation]
			strBuild =  new StringBuilder();
			strBuild.Append("select * from shipment where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and ref_no LIKE '%");
			strBuild.Append(Utility.ReplaceSingleQuote(refNo)+"%'");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					isExists = true;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TrackAndTraceManager","IsCustRefExist","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("TrackAndTraceManager","IsCustRefExist","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return isExists;
		}


		public static bool EnableViewByConsgment(String strAppID, String strEnterpriseID,String userID,String consgmtNo )
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			String strPayername = null;
			bool isExists = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TrackAndTraceManager","EnableViewByConsgment","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strPayername = GetPayerName(strAppID,strEnterpriseID,userID);
			strBuild =  new StringBuilder();
			strBuild.Append("select * from shipment where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and consignment_no = '");
			strBuild.Append(Utility.ReplaceSingleQuote(consgmtNo)+"'");
			strBuild.Append(" and payerid = '");
			strBuild.Append(Utility.ReplaceSingleQuote(strPayername)+"'");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					isExists = true;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TrackAndTraceManager","EnableViewByConsgment","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("TrackAndTraceManager","EnableViewByConsgment","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return isExists;
		}


		public static int EnableViewByCustRef(String strAppID, String strEnterpriseID,String userID,String refNo)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			String strPayername = null;
			int count = 0;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TrackAndTraceManager","EnableViewByCustRef","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strPayername = GetPayerName(strAppID,strEnterpriseID,userID);
			strBuild =  new StringBuilder();
			strBuild.Append("select * from shipment where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and ref_no = '");
			strBuild.Append(Utility.ReplaceSingleQuote(refNo)+"'");
			strBuild.Append(" and payerid = '");
			strBuild.Append(Utility.ReplaceSingleQuote(strPayername)+"'");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					count = dsShipmnt.Tables[0].Rows.Count;
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TrackAndTraceManager","EnableViewByCustRef","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("TrackAndTraceManager","EnableViewByCustRef","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return count;
		}


		public static String GetPayerName(String strAppID, String strEnterpriseID,String userID)
		{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			String strPayername = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TrackAndTraceManager","GetPayerName","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select payerid from user_master where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and userid = '");
			strBuild.Append(Utility.ReplaceSingleQuote(userID)+"'");

			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				DataRow dr = dsShipmnt.Tables[0].Rows[0]; 
				strPayername = dr["payerid"].ToString();
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TrackAndTraceManager","GetPayerName","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("TrackAndTraceManager","GetPayerName","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return strPayername;

		}


		public static String GetTopConsgnByRefNo(String strAppID, String strEnterpriseID,String refNo)
	{
			IDbCommand dbCmd = null;
			StringBuilder strBuild = null;
			String ConsgnNo = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TrackAndTraceManager","GetTopConsgnByRefNo","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strBuild =  new StringBuilder();
			strBuild.Append("select top 1 consignment_no from shipment where applicationid = '");
			strBuild.Append(strAppID+"'");
			strBuild.Append(" and enterpriseid = '");
			strBuild.Append(strEnterpriseID+"'");
			strBuild.Append(" and ref_no = '");
			strBuild.Append(Utility.ReplaceSingleQuote(refNo)+"'");
			
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				DataSet dsShipmnt =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if (dsShipmnt.Tables[0].Rows.Count > 0)
				{
					DataRow dr= dsShipmnt.Tables[0].Rows[0];
					if ((dr["consignment_no"] != null) && (!dr["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						ConsgnNo=(String) dr["consignment_no"];
					}
				}

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TrackAndTraceManager","GetTopConsgnByRefNo","ERR002","Error : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				Logger.LogTraceError("TrackAndTraceManager","GetTopConsgnByRefNo","ERR003","Error : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			return ConsgnNo;
		}


		public static DataSet QueryShipmentTracking(String strAppID, String strEnterpriseID,String ConsgnNo)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsShpTrack = null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("TrackAndTraceDAL"," QueryShipmentTracking(","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			String strConsgnNo = null;
			strConsgnNo = ConsgnNo;
			DateTime strMdeDT = GetTimeStatusMDE(strAppID,strEnterpriseID,strConsgnNo);					

			String strQuery = "select distinct s.booking_no, s.consignment_no, ";
			strQuery += "s.booking_datetime, s.payerid, ";
			strQuery += "s.payer_name, s.ref_no, s.sender_country, s.recipient_country, s.last_status_datetime, ";
			strQuery += "s.est_delivery_datetime, s.service_code, ";
			strQuery += "stf.status_code, isnull(stf.exception_code, '') exception_code, case isnull(sc.tt_display_location,'N') when 'Y' then stf.location else '' end as location, stf.consignee_name, stf.person_incharge ,";
			strQuery += "case isnull(sc.tt_display_remarks,'N') when 'Y' then stf.remark else '' end as remark, stf.tracking_datetime, s.route_code, s.invoice_date, z1.state_code as rec_state_code,";
			strQuery += "z2.state_code as sen_state_code ,sc.status_description, ex.exception_description ";
			strQuery += "from Shipment s ";
			strQuery += "join zipcode z1 on(z1.applicationid = '"+strAppID+"' ";
			strQuery += "and z1.enterpriseid = '"+strEnterpriseID+"'" ;
			strQuery += "and s.recipient_zipcode = z1.zipcode) ";
			strQuery += "join zipcode z2 on(z2.applicationid = '"+strAppID+"' "; 
			strQuery += "and z2.enterpriseid = '"+strEnterpriseID+"'" ; 
			strQuery += "and s.sender_zipcode = z2.zipcode) ";
			strQuery += "join(";
			strQuery += "select distinct st0.applicationid,st0.enterpriseid,st0.consignment_no,st0.tracking_datetime,st0.status_code,st0.exception_code,st0.location,st0.consignee_name,st0.remark,st0.person_incharge ";
			strQuery += "from (select distinct * from shipment_tracking where isnull(deleted,'N') <> 'Y' ";
			strQuery += "and applicationid = '"+strAppID+"' "; 
			strQuery += "and enterpriseid = '"+strEnterpriseID+"' "; 
			strQuery += ")st0 ";
			strQuery += "where st0.consignment_no = '"+ConsgnNo+"' ";
			//strQuery += "and st0.tracking_datetime >= '"+strMdeDT+"' ";
			strQuery += ") stf ";
			strQuery += "on (s.applicationid = stf.applicationid and s.enterpriseid = stf.enterpriseid and s.consignment_no = stf.consignment_no) ";
			strQuery += "left join exception_code ex on(ex.applicationid=s.applicationid and ex.enterpriseid =s.enterpriseid and stf.exception_code = ex.exception_code) ";
			strQuery += "join (select distinct * from status_code ";
			strQuery += "where applicationid = '"+strAppID+"' "; 
			strQuery += "and enterpriseid = '"+strEnterpriseID+"' "; 
			strQuery += "and isnull(tt_display_status,'Y')<>'N') sc ";
			strQuery += "on(sc.applicationid=s.applicationid and sc.enterpriseid =s.enterpriseid and stf.status_code = sc.status_code ) ";
			strQuery += "where s.applicationid = '"+strAppID+"' "; 
			strQuery += "and s.enterpriseid = '"+strEnterpriseID+"' "; 
			strQuery += "and s.consignment_no = '"+ConsgnNo+"' "; 
			strQuery += "ORDER BY stf.tracking_datetime DESC ";

			dbcmd = dbCon.CreateCommand(strQuery,CommandType.Text);
 
			try
			{
				dsShpTrack = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TrackAndTraceDAL","QueryShipmentTracking","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsShpTrack;

		}


		public static DataSet QueryTrackandTrace(String AppID,String EnterpriseID,String ConsgnNo,String refNo, String PayerId ,String ExpandPackageDetail)
		{
			DataSet ds = null;
			IDbCommand dbCmd = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("module1","ConsignmentsWithoutPkgWeightsMgrDAL","QueryConsignmentsWithoutPkgWeights","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseId",EnterpriseID));				
				if(PayerId.Trim() ==null)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid ",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid ",PayerId));
				}
				if(ConsgnNo != null && (refNo == null || refNo == string.Empty  ))
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no ",ConsgnNo));
				}
				else if( (ConsgnNo == null || ConsgnNo == string.Empty ) && refNo != null)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ref_no ",refNo));
				}
				if(ExpandPackageDetail.Trim() ==null)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ReturnPackages  ",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ReturnPackages  ",ExpandPackageDetail));
				}


				dbCmd = dbCon.CreateCommand("QueryTrackandTrace",storedParams, CommandType.StoredProcedure);
				ds = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

			}
			catch(Exception ex)
			{
				throw ex;
			}
			return ds;
		}


		public static string QueryEnterpriseConfigurationsTrackandTrace(string appID,string enterpriseId)
		{
			string rDate = string.Empty;
			DataSet ds = null;
			IDbCommand dbCmd = null;
			DataSet dsEnterpriseConfig =null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseId);
			if(dbCon == null)
			{
				Logger.LogTraceError("module1","ConsignmentsWithoutPkgWeightsMgrDAL","QueryEnterpriseConfigurations","ERR002","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
				string strSql = " SELECT * FROM dbo.EnterpriseConfigurations('"+enterpriseId+"', 'TrackandTrace') ";

				dbCmd = dbCon.CreateCommand(strSql,CommandType.Text);
				dsEnterpriseConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);  

				foreach (DataRow r in dsEnterpriseConfig.Tables[0].Rows)
				{
					rDate = r["value"].ToString();
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return rDate;
		}


		public static string QueryPlayerIdTrackandTrace(string appID,string enterpriseId,string userId)
		{
			string rDate = string.Empty;
			DataSet ds = null;
			IDbCommand dbCmd = null;
			DataSet dsEnterpriseConfig =null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseId);
			if(dbCon == null)
			{
				Logger.LogTraceError("module1","ConsignmentsWithoutPkgWeightsMgrDAL","QueryEnterpriseConfigurations","ERR002","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
				string strSql = " SELECT payerid FROM dbo.User_Master WHERE userid = '" + userId + "' ";

				dbCmd = dbCon.CreateCommand(strSql,CommandType.Text);
				dsEnterpriseConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);  

				foreach (DataRow r in dsEnterpriseConfig.Tables[0].Rows)
				{
					rDate = r["payerid"].ToString();
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return rDate;
		}


		public static string ConvertSharpToAscii(string sData)
		{
			return sData.Replace("#","%23");
		}

		public static string CovertAsciiToSharp(string sData)
		{
			return sData.Replace("%23","#");
		}

		public static bool IsTrackandTraceExist(String AppID,String EnterpriseID,String ConsgnNo,String refNo, String PayerId ,String ExpandPackageDetail)
		{
			DataSet ds = null;
			IDbCommand dbCmd = null;
			bool rtn = false;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("module1","ConsignmentsWithoutPkgWeightsMgrDAL","QueryConsignmentsWithoutPkgWeights","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseId",EnterpriseID));				
				if(PayerId.Trim() ==null)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid ",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid ",PayerId));
				}
				if(ConsgnNo != null && (refNo == null || refNo == string.Empty  ))
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no ",ConsgnNo));
				}
				else if( (ConsgnNo == null || ConsgnNo == string.Empty ) && refNo != null)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ref_no ",refNo));
				}
				if(ExpandPackageDetail.Trim() ==null)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ReturnPackages  ",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ReturnPackages  ",ExpandPackageDetail));
				}


				dbCmd = dbCon.CreateCommand("QueryTrackandTrace",storedParams, CommandType.StoredProcedure);
				ds = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

				if(ds.Tables[1].Rows.Count > 0)
				{
					rtn = true;
				} 
				else
				{
					rtn = false;
				}

			}
			catch(Exception ex)
			{
				throw ex;
			}
			return rtn;
		}

		private static DateTime GetTimeStatusMDE(String strAppID, String strEnterpriseID, String ConsgnNo)
		{	
			
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsShpTrackMDE = null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("TrackAndTraceDAL"," GetTimeStatusMDE(","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			String strQuery = "select * ";
			strQuery += "from( ";
			strQuery += "select distinct * from shipment_tracking ";
			strQuery += "where isnull(deleted,'N') <> 'Y' ";
			strQuery += "and applicationid = '"+strAppID+"' ";
			strQuery += "and enterpriseid = '"+strEnterpriseID+"' ";
			strQuery += ")st0 ";
			strQuery += "where st0.consignment_no = '"+ConsgnNo+"' ";
			strQuery += "and st0.status_code ='MDE' ";

			dbcmd = dbCon.CreateCommand(strQuery,CommandType.Text);
			try
			{
				dsShpTrackMDE = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TrackAndTraceDAL","GetTimeStatusMDE","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			DataRow dr = dsShpTrackMDE.Tables[0].Rows[0];
			DateTime dt = new DateTime(1,1,1);
			if(dr["tracking_datetime"]!=System.DBNull.Value)
			{
				dt = (DateTime)dr["tracking_datetime"];
				return dt;
			}
			
			return dt;
		}

		public static DataSet ShipmentTrackingCheckStatus(String strAppID, String strEnterpriseID,String ConsgnNo,String strStatus)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsShpTrack = null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("TrackAndTraceDAL"," ShipmentTrackingCheckStatus(","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			String strQuery = "select * from Shipment_Tracking ";
			strQuery += " where consignment_no='" + ConsgnNo +"' ";
			strQuery += " and status_code='"+strStatus+"' ";
			strQuery += "and  applicationid='"+strAppID+"' ";
			strQuery += "and  enterpriseid='"+strEnterpriseID+"'" ;

			dbcmd = dbCon.CreateCommand(strQuery,CommandType.Text);
 
			try
			{
				dsShpTrack = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TrackAndTraceDAL","ShipmentTrackingCheckStatus","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsShpTrack;

		}


	}
}
