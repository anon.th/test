using System;
using System.Collections;
using System.Data;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using System.Text;
using System.Threading;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for ReportInvoiceDAL.
	/// </summary>
	public class ReportInvoiceDAL
	{
		public  ReportInvoiceDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static System.Data.DataSet GetJobStatus(String strAppID, String strEnterpriseID )
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsJobStatusDAL","GetJobStatus","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT DropDownListDisplayValue, DefaultValue, [Description], CodedValue  ");
				strQry.Append(" from dbo.GetCustomsSpecialConfiguration('"+strEnterpriseID+"','CustomsStatus',0) " ); 
				 								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetJobStatus", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}


		public static System.Data.DataSet GetGridRows(String strAppID, String strEnterpriseID )
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsJobStatusDAL","GetGridRows","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" select [key], value   ");
				strQry.Append(" from dbo.EnterpriseConfigurations('"+strEnterpriseID+"','CustomsInvoiceSearch') " ); 
				 								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);

				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetGridRows", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		} 
		
		public static System.Data.DataSet Customs_SearchInvoices(string strAppID, string strEnterpriseID, string userloggedin, System.Data.DataTable dtParams)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ReportInvoiceDAL","Customs_SearchInvoices","Customs_SearchInvoices","DbConnection object is null!!");
				return null;
			} 
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
		  
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));	 
			if(dtParams.Rows[0]["StartDate"] != DBNull.Value  && dtParams.Rows[0]["StartDate"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@StartDate",dtParams.Rows[0]["StartDate"].ToString()));
			} 
			if(dtParams.Rows[0]["EndDate"] != DBNull.Value  && dtParams.Rows[0]["EndDate"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@EndDate",dtParams.Rows[0]["EndDate"].ToString()));
			} 
			if(dtParams.Rows[0]["DateType"] != DBNull.Value  && dtParams.Rows[0]["DateType"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@DateType",dtParams.Rows[0]["DateType"].ToString()));
			}
			if(dtParams.Rows[0]["Invoice_No"] != DBNull.Value  && dtParams.Rows[0]["Invoice_No"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Invoice_No",dtParams.Rows[0]["Invoice_No"].ToString()));
			} 
			if(dtParams.Rows[0]["Invoice_No_To"] != DBNull.Value  && dtParams.Rows[0]["Invoice_No_To"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Invoice_No_To",dtParams.Rows[0]["Invoice_No_To"].ToString()));
			}
			if(dtParams.Rows[0]["HouseAWBNo"] != DBNull.Value  && dtParams.Rows[0]["HouseAWBNo"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@HouseAWBNo",dtParams.Rows[0]["HouseAWBNo"].ToString()));
			} 
			if(dtParams.Rows[0]["CustomerID"] != DBNull.Value  && dtParams.Rows[0]["CustomerID"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@CustomerID",dtParams.Rows[0]["CustomerID"].ToString()));
			} 
			if(dtParams.Rows[0]["CustomerGroup"] != DBNull.Value  && dtParams.Rows[0]["CustomerGroup"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@CustomerGroup",dtParams.Rows[0]["CustomerGroup"].ToString()));
			} 
			if(dtParams.Rows[0]["BondedWarehouse"] != DBNull.Value  && dtParams.Rows[0]["BondedWarehouse"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@BondedWarehouse",dtParams.Rows[0]["BondedWarehouse"].ToString()));
			}
			if(dtParams.Rows[0]["JobStatus"] != DBNull.Value  && dtParams.Rows[0]["JobStatus"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@JobStatus",dtParams.Rows[0]["JobStatus"].ToString()));
			}
			if(dtParams.Rows[0]["SearchStatusNotAchieved"] != DBNull.Value  && dtParams.Rows[0]["SearchStatusNotAchieved"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@SearchStatusNotAchieved",dtParams.Rows[0]["SearchStatusNotAchieved"].ToString()));
			}
			if(dtParams.Rows[0]["EntryType"] != DBNull.Value  && dtParams.Rows[0]["EntryType"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@EntryType",dtParams.Rows[0]["EntryType"].ToString()));
			}
			if(dtParams.Rows[0]["CustomsProfile"] != DBNull.Value  && dtParams.Rows[0]["CustomsProfile"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@CustomsProfile",dtParams.Rows[0]["CustomsProfile"].ToString()));
			}
			if(dtParams.Rows[0]["CustomsDeclarationNo"] != DBNull.Value  && dtParams.Rows[0]["CustomsDeclarationNo"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@CustomsDeclarationNo",dtParams.Rows[0]["CustomsDeclarationNo"].ToString()));
			}
			if(dtParams.Rows[0]["ShowAllModifiedSincePrint"] != DBNull.Value  && dtParams.Rows[0]["ShowAllModifiedSincePrint"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ShowAllModifiedSincePrint",dtParams.Rows[0]["ShowAllModifiedSincePrint"].ToString()));
			}
			if(dtParams.Rows[0]["Debug"] != DBNull.Value  && dtParams.Rows[0]["Debug"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Debug",dtParams.Rows[0]["Debug"].ToString()));
			}
			if(dtParams.Rows[0]["userloggedin"] != DBNull.Value  && dtParams.Rows[0]["userloggedin"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",dtParams.Rows[0]["userloggedin"].ToString()));
			}

			System.Data.SqlClient.SqlParameter outputRowParam = new System.Data.SqlClient.SqlParameter("@ReturnedRows", SqlDbType.Int);
			outputRowParam.Direction = ParameterDirection.Output;
			storedParams.Add(outputRowParam);

			result = (DataSet)dbCon.ExecuteProcedure("Customs_SearchInvoices",storedParams,ReturnType.DataSetType);
			
			
			DataTable dtRowReturn = new DataTable("RowReturn");
			dtRowReturn.Columns.Add("value");
			DataRow dr = dtRowReturn.NewRow();
			dr["value"] = outputRowParam.Value.ToString();
			dtRowReturn.Rows.Add(dr);	

			result.Tables.Add(dtRowReturn);

			return result;
		}

		public static System.Data.DataTable SearchInvoicesParam()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("enterpriseid");
			dt.Columns.Add("StartDate"); 
			dt.Columns.Add("EndDate");
			dt.Columns.Add("DateType");
			dt.Columns.Add("Invoice_No");
			dt.Columns.Add("Invoice_No_To");
			dt.Columns.Add("HouseAWBNo");
			dt.Columns.Add("CustomerID");
			dt.Columns.Add("CustomerGroup");
			dt.Columns.Add("BondedWarehouse");
			dt.Columns.Add("JobStatus");
			dt.Columns.Add("SearchStatusNotAchieved");
			dt.Columns.Add("EntryType");
			dt.Columns.Add("CustomsProfile");
			dt.Columns.Add("CustomsDeclarationNo");
			dt.Columns.Add("ShowAllModifiedSincePrint");
			dt.Columns.Add("Debug"); 
			dt.Columns.Add("userloggedin");
			return dt;
		}

		public static System.Data.DataSet GetDropDownListDisplayValue(String strAppID, String strEnterpriseID, String codeID, String sqlFunction)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsJobStatusDAL","SearchCustomJobCompiling","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT DropDownListDisplayValue, DefaultValue, [Description], CodedValue  ");
				strQry.Append(" FROM dbo." + sqlFunction + "( '"+strEnterpriseID+"', '"+codeID+"', 1) " );
				strQry.Append(" ORDER BY DropDownListDisplayValue");
				 
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "SearchCustomJobCompiling", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}


	}
}
