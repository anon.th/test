using System;
using System.Data;
using System.Collections;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using System.Data.SqlClient;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for InvoiceManagementMgrDAL.
	/// </summary>
	public class InvoiceManagementMgrDAL
	{
		public InvoiceManagementMgrDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		public static DataSet GetEmptyInvoice()
		{
			DataTable dtInvoice =  new DataTable();
			dtInvoice.Columns.Add(new DataColumn("applicationid", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("enterpriseid", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("invoice_no", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("invoice_date", typeof(DateTime)));
			dtInvoice.Columns.Add(new DataColumn("due_date", typeof(DateTime)));
			dtInvoice.Columns.Add(new DataColumn("payerid", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("payer_type", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("payer_name", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("payer_address1", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("payer_address2", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("payer_zipcode", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("payer_country", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("payer_telephone", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("payer_fax", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("payment_mode", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("invoice_status", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("act_payment_date", typeof(DateTime)));
			dtInvoice.Columns.Add(new DataColumn("printed_datetime", typeof(DateTime)));
			dtInvoice.Columns.Add(new DataColumn("jobid", typeof(int)));
			dtInvoice.Columns.Add(new DataColumn("tot_pkg", typeof(int)));
			dtInvoice.Columns.Add(new DataColumn("tot_wt", typeof(decimal)));
			dtInvoice.Columns.Add(new DataColumn("tot_dim_wt", typeof(decimal)));
			dtInvoice.Columns.Add(new DataColumn("chargeable_wt", typeof(decimal)));
			dtInvoice.Columns.Add(new DataColumn("incurance_amt", typeof(decimal)));
			dtInvoice.Columns.Add(new DataColumn("other_surcharge", typeof(decimal)));
			dtInvoice.Columns.Add(new DataColumn("tot_freight_charge", typeof(decimal)));
			dtInvoice.Columns.Add(new DataColumn("tot_vas_surcharge", typeof(decimal)));
			dtInvoice.Columns.Add(new DataColumn("esa_surcharge", typeof(decimal)));
			dtInvoice.Columns.Add(new DataColumn("total_exception", typeof(decimal)));
			dtInvoice.Columns.Add(new DataColumn("mbg_amount", typeof(decimal)));
			dtInvoice.Columns.Add(new DataColumn("invoice_adj_amount", typeof(decimal)));
			dtInvoice.Columns.Add(new DataColumn("invoice_amt", typeof(decimal)));
			dtInvoice.Columns.Add(new DataColumn("tot_cons", typeof(int)));
			dtInvoice.Columns.Add(new DataColumn("approved_by", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("approved_date", typeof(DateTime)));
			dtInvoice.Columns.Add(new DataColumn("cancelled_by", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("cancel_date", typeof(DateTime)));

			DataRow drEach = dtInvoice.NewRow();
			dtInvoice.Rows.Add(drEach);
			DataSet dsInvoice = new DataSet();
			dsInvoice.Tables.Add(dtInvoice);
			return  dsInvoice;	
		}

		public static DataSet GetEmptyInvoiceTmp()
		{
			DataTable dtInvoice =  new DataTable();
			dtInvoice.Columns.Add(new DataColumn("invoice_no", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("invoice_date", typeof(DateTime)));
			dtInvoice.Columns.Add(new DataColumn("due_date", typeof(DateTime)));
			dtInvoice.Columns.Add(new DataColumn("payertype_name", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("invoicestatus_name", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("actual_bill_placement_date", typeof(DateTime)));
			dtInvoice.Columns.Add(new DataColumn("invoice_adj_amount", typeof(decimal)));
			dtInvoice.Columns.Add(new DataColumn("invoice_amt", typeof(decimal)));
			dtInvoice.Columns.Add(new DataColumn("payerid", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("batch_no", typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("next_bill_placement_date", typeof(DateTime)));
			dtInvoice.Columns.Add(new DataColumn("amt_paid", typeof(decimal)));
			dtInvoice.Columns.Add(new DataColumn("payment_date", typeof(DateTime)));
			dtInvoice.Columns.Add(new DataColumn("first_possible_bpd", typeof(DateTime)));
			dtInvoice.Columns.Add(new DataColumn("promised_dt", typeof(DateTime)));
			dtInvoice.Columns.Add(new DataColumn("link_to_invoice", typeof(string)));
			DataRow drEach = dtInvoice.NewRow();
			dtInvoice.Rows.Add(drEach);
			DataSet dsInvoice = new DataSet();
			dsInvoice.Tables.Add(dtInvoice);
			return  dsInvoice;	
		}


		public static DataSet GetEmptyInvoiceDetail()
		{
			DataTable dtInvoiceDetail =  new DataTable();
			dtInvoiceDetail.Columns.Add(new DataColumn("applicationid", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("enterpriseid", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("invoice_no", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("consignment_no", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("booking_datetime", typeof(DateTime)));
			dtInvoiceDetail.Columns.Add(new DataColumn("ref_no", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("origin_state_code", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("destination_state_code", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("tot_pkg", typeof(int)));
			dtInvoiceDetail.Columns.Add(new DataColumn("tot_wt", typeof(decimal)));
			dtInvoiceDetail.Columns.Add(new DataColumn("tot_dim_wt", typeof(decimal)));
			dtInvoiceDetail.Columns.Add(new DataColumn("chargeable_wt", typeof(decimal)));
			dtInvoiceDetail.Columns.Add(new DataColumn("pod_datetime", typeof(DateTime)));
			dtInvoiceDetail.Columns.Add(new DataColumn("recipient_name", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("recipient_address1", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("recipient_address2", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("recipient_zipcode", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("recipient_country", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("sender_name", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("sender_address1", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("sender_address2", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("sender_country", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("sender_zipcode", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("mbg", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("service_code", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("pod_exception", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("person_incharge", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("pod_remark", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("insurance_amt", typeof(decimal)));
			dtInvoiceDetail.Columns.Add(new DataColumn("tot_freight_charge", typeof(decimal)));
			dtInvoiceDetail.Columns.Add(new DataColumn("tot_vas_surcharge", typeof(decimal)));
			dtInvoiceDetail.Columns.Add(new DataColumn("esa_surcharge", typeof(decimal)));
			dtInvoiceDetail.Columns.Add(new DataColumn("invoice_amt", typeof(decimal)));
			dtInvoiceDetail.Columns.Add(new DataColumn("consignee_name", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("other_surcharge", typeof(decimal)));
			dtInvoiceDetail.Columns.Add(new DataColumn("total_exception", typeof(decimal)));
			dtInvoiceDetail.Columns.Add(new DataColumn("total_amount", typeof(decimal)));
			dtInvoiceDetail.Columns.Add(new DataColumn("invoice_adj_amount", typeof(decimal)));
			dtInvoiceDetail.Columns.Add(new DataColumn("adjusted_by", typeof(string)));
			dtInvoiceDetail.Columns.Add(new DataColumn("adjusted_date", typeof(DateTime)));
			dtInvoiceDetail.Columns.Add(new DataColumn("adjusted_remark", typeof(string)));

			DataRow drEach = dtInvoiceDetail.NewRow();
			dtInvoiceDetail.Rows.Add(drEach);
			DataSet dsInvoiceDetail = new DataSet();
			dsInvoiceDetail.Tables.Add(dtInvoiceDetail);
			return  dsInvoiceDetail;	
		}


		public static DataSet GetInvoice(String strAppID, String strEnterpriseID,
			String strCulture,
			DateTime dtInvFrom, DateTime dtInvTo,
			String strInvNo,
			DateTime dtInvDueFrom, DateTime dtInvDueTo,
			String strInvType,
			String strCustID,
			String strInvStatus)
		{
			IDbCommand dbCmd = null;	
			DataSet ds = new DataSet();
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT iv.applicationid, iv.enterpriseid, iv.invoice_no, iv.invoice_date, iv.due_date, ");
			strBuilder.Append("iv.payerid, iv.payer_type, iv.payer_name, iv.payer_address1, iv.payer_address2, iv.payer_zipcode, ");
			strBuilder.Append("iv.payer_country, iv.payer_telephone, iv.payer_fax, iv.payment_mode, iv.invoice_status, iv.act_payment_date, ");
			strBuilder.Append("iv.printed_datetime, iv.jobid, iv.tot_pkg, iv.tot_wt, iv.tot_dim_wt, iv.chargeable_wt, iv.insurance_amt, ");
			strBuilder.Append("iv.other_surcharge, iv.tot_freight_charge, iv.tot_vas_surcharge, iv.esa_surcharge, iv.total_exception, iv.mbg_amount, iv.invoice_adj_amount, ");
			strBuilder.Append(" iv.invoice_amt ");
			//strBuilder.Append("(select ISNULL(ISNULL(total_rated_amount, 0) + ISNULL(tax_on_rated_amount, 0) + ISNULL(export_freight_charge, 0), 0) from Shipment  where consignment_no = (SELECT consignment_no FROM invoice_detail WHERE invoice_no=iv.invoice_no)) as 'invoice_amt' ");
			//strBuilder.Append("(SELECT SUM(ISNULL(invoice_amt,0)) FROM dbo.shipment WHERE consignment_no IN (SELECT consignment_no FROM dbo.invoice_detail WHERE invoice_no = iv.invoice_no AND applicationid = iv.applicationid AND enterpriseid = iv.enterpriseid) AND applicationid = iv.applicationid AND enterpriseid = iv.enterprisei) AS 'invoice_amt' ");
			strBuilder.Append(", iv.tot_cons, iv.approved_by, iv.approved_date, iv.cancelled_by, iv.cancel_date,csc.code_text as invoicestatus_name,csc1.code_text as payertype_name ");
			strBuilder.Append("FROM Invoice iv INNER JOIN Core_System_Code AS csc ON iv.invoice_status = csc.code_str_value ");
			strBuilder.Append("INNER JOIN Core_System_Code AS csc1 ON iv.payment_mode = csc1.code_str_value ");
			strBuilder.Append("WHERE iv.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND iv.enterpriseid = '");
			strBuilder.Append(strEnterpriseID+"'");
			strBuilder.Append(" AND csc.codeid = 'invoice_status' ");
			strBuilder.Append(" AND csc.culture LIKE '%");
			strBuilder.Append(strCulture+"%'");
			strBuilder.Append(" AND csc1.codeid = 'payment_mode' ");
			strBuilder.Append(" AND csc1.culture LIKE '%");
			strBuilder.Append(strCulture+"%'");
			
			//			if(dtInvFrom != DateTime.MinValue || dtInvTo != DateTime.MinValue)
			//			{
			//				if((dtInvFrom != new DateTime(1980, 1, 2, 0, 0, 0)) || (dtInvTo != new DateTime(2099, 12, 30, 23, 23, 23)))
			//				//if((dtInvFrom != new DateTime(2, 1, 1980, 0, 0, 0)) || (dtInvTo != new DateTime(30, 12, 2099, 23, 23, 23)))
			//				{
			//					strBuilder.Append(" and invoice_date between");
			//					strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvFrom, DTFormat.DateTime));
			//					strBuilder.Append(" and ");
			//					strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvTo, DTFormat.DateTime));
			//				}
			//			}
			if(dtInvFrom != DateTime.MinValue && dtInvTo != DateTime.MinValue)
			{
				strBuilder.Append(" and invoice_date between");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvFrom, DTFormat.DateTime));
				strBuilder.Append(" and ");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvTo, DTFormat.DateTime));
			}
			else if(dtInvFrom != DateTime.MinValue && dtInvTo == DateTime.MinValue)
			{
				strBuilder.Append(" and invoice_date between");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvFrom, DTFormat.DateTime));
				strBuilder.Append(" and ");
				strBuilder.Append(" (select max(invoice_date) from invoice)");
			}
			else if(dtInvFrom == DateTime.MinValue && dtInvTo != DateTime.MinValue)
			{
				strBuilder.Append(" and invoice_date between ");
				strBuilder.Append(" (select min(invoice_date) from invoice)  ");
				strBuilder.Append(" and ");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvTo, DTFormat.DateTime));
			}

			if((strInvNo != null) && (!strInvNo.GetType().Equals(System.Type.GetType("System.DBNull"))) &&(strInvNo.Trim() != ""))
			{
				strBuilder.Append(" and invoice_no like ");
				strBuilder.Append("'%");
				strBuilder.Append(strInvNo);
				strBuilder.Append("'");
			}
			//			if(dtInvDueFrom != DateTime.MinValue || dtInvDueTo != DateTime.MinValue)
			//			{
			//				if((dtInvDueFrom != new DateTime(1980, 1, 2, 0, 0, 0)) || (dtInvDueTo != new DateTime(2099, 12, 30, 23, 23, 23)))
			//				//if((dtInvDueFrom != new DateTime(2, 1, 1980, 0, 0, 0)) || (dtInvDueTo != new DateTime(30, 12, 2099, 23, 23, 23)))
			//				{
			//					strBuilder.Append(" and due_date between");
			//					strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvDueFrom, DTFormat.DateTime));
			//					strBuilder.Append(" and ");
			//					strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvDueTo, DTFormat.DateTime));
			//				}
			//			}
			if(dtInvDueFrom != DateTime.MinValue && dtInvDueTo != DateTime.MinValue)
			{
				strBuilder.Append(" and due_date between");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvDueFrom, DTFormat.DateTime));
				strBuilder.Append(" and ");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvDueTo, DTFormat.DateTime));
			}
			else if(dtInvDueFrom != DateTime.MinValue && dtInvDueTo == DateTime.MinValue)
			{
				strBuilder.Append(" and due_date between");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvDueFrom, DTFormat.DateTime));
				strBuilder.Append(" and ");
				strBuilder.Append(" (select max(due_date) from invoice)");
			}
			else if(dtInvDueFrom == DateTime.MinValue && dtInvDueTo != DateTime.MinValue)
			{
				strBuilder.Append(" and due_date between");
				strBuilder.Append(" (select min(due_date) from invoice) ");
				strBuilder.Append(" and ");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvDueTo, DTFormat.DateTime));
			}


			if((strInvType != null) && (!strInvType.GetType().Equals(System.Type.GetType("System.DBNull"))) &&(strInvType.Trim() != ""))
			{
				strBuilder.Append(" and rtrim(ltrim(iv.payment_mode)) = ");
				strBuilder.Append("'");
				strBuilder.Append(strInvType);
				strBuilder.Append("'");
			}

			if((strCustID != null) && (!strCustID.GetType().Equals(System.Type.GetType("System.DBNull"))) &&(strCustID.Trim() != ""))
			{
				strBuilder.Append(" and rtrim(ltrim(payerid)) = ");
				strBuilder.Append("'");
				strBuilder.Append(strCustID);
				strBuilder.Append("'");
			}

			if((strInvStatus != null) && (!strInvStatus.GetType().Equals(System.Type.GetType("System.DBNull"))) &&(strInvStatus.Trim() != ""))
			{
				strBuilder.Append(" and rtrim(ltrim(invoice_status)) = ");
				strBuilder.Append("'");
				strBuilder.Append(strInvStatus);
				strBuilder.Append("'");
			}

//			strBuilder.Append(" order by invoice_date, invoice_no ");
			strBuilder.Append(" order by  invoice_no DESC ");

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetInvoiceData","GetInvoiceData","DbConnection object is null!!");
				return ds;
			}
			
			
			//sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "InvoiceManageTable");
			dbCmd = dbCon.CreateCommand(strBuilder.ToString() , CommandType.Text);
			ds = (DataSet)dbCon.ExecuteQuery(dbCmd,ReturnType.DataSetType);
			return  ds;
		}

		public static DataSet GetInvoice(String strAppID, String strEnterpriseID,
			String strCulture,
			DateTime dtInvFrom, DateTime dtInvTo,
			String strInvNo,
			DateTime dtInvDueFrom, DateTime dtInvDueTo,
			String strInvType,
			String strCustID,
			String strInvStatus,
			String strPage)
		{
			IDbCommand dbCmd = null;	
			DataSet ds = new DataSet();
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT iv.applicationid, iv.enterpriseid, iv.invoice_no, iv.invoice_date, due_date=CAST(s.act_pickup_datetime AS DATE), ");
			strBuilder.Append("iv.payerid, iv.payer_type, iv.payer_name, iv.payer_address1, iv.payer_address2, iv.payer_zipcode, ");
			strBuilder.Append("iv.payer_country, iv.payer_telephone, iv.payer_fax, iv.payment_mode, iv.invoice_status, iv.act_payment_date, ");
			strBuilder.Append("iv.printed_datetime, iv.jobid, iv.tot_pkg, iv.tot_wt, iv.tot_dim_wt, iv.chargeable_wt, iv.insurance_amt, ");
			strBuilder.Append("iv.other_surcharge, iv.tot_freight_charge, iv.tot_vas_surcharge, iv.esa_surcharge, iv.total_exception, iv.mbg_amount, iv.invoice_adj_amount, ");
			strBuilder.Append(" iv.invoice_amt, iv.tot_cons, iv.approved_by, iv.approved_date, iv.cancelled_by, iv.cancel_date,csc.code_text as invoicestatus_name,csc1.code_text as payertype_name, ");
			strBuilder.Append("  iv.tot_cons, iv.approved_by, iv.approved_date, iv.cancelled_by, iv.cancel_date, csc.code_text AS invoicestatus_name, csc1.code_text AS payertype_name, ");
			strBuilder.Append("iv.actual_bill_placement_date, ip.batch_no, ip.payment_date, ip.amt_paid, cn.credit_no as link_to_invoice, iv.next_bill_placement_date, iv.first_bill_placement_date as first_possible_bpd, iv.promised_payment_date as promised_dt, iv.agency_charges ");
			strBuilder.Append("FROM Invoice iv INNER JOIN Core_System_Code AS csc ON iv.invoice_status = csc.code_str_value AND iv.applicationid = csc.applicationid ");
            strBuilder.Append("INNER JOIN Shipment s ON iv.applicationid = s.applicationid AND iv.enterpriseid = s.enterpriseid AND iv.invoice_no = s.invoice_no ");
            strBuilder.Append("INNER JOIN Core_System_Code AS csc1 ON iv.payment_mode = csc1.code_str_value AND iv.applicationid = csc1.applicationid ");
			strBuilder.Append("LEFT JOIN Invoice_Payment AS ip ON iv.invoice_no = ip.invoice_no ");
			strBuilder.Append("LEFT JOIN Credit_Note AS cn ON iv.invoice_no = cn.link_to_invoice ");
			//strBuilder.Append("INNER JOIN Customer AS cus ON iv.applicationid = cus.applicationid AND iv.enterpriseid = cus.enterpriseid  AND iv.payerid = cus.custid ");
			strBuilder.Append("WHERE iv.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND iv.enterpriseid = '");
			strBuilder.Append(strEnterpriseID+"'");
			strBuilder.Append(" AND csc.codeid = 'invoice_status' ");
			strBuilder.Append(" AND csc.culture LIKE '%");
			strBuilder.Append(strCulture+"%'");
			strBuilder.Append(" AND csc1.codeid = 'payment_mode' ");
			strBuilder.Append(" AND csc1.culture LIKE '%");
			strBuilder.Append(strCulture+"%'");
			
			//			if(dtInvFrom != DateTime.MinValue || dtInvTo != DateTime.MinValue)
			//			{
			//				if((dtInvFrom != new DateTime(1980, 1, 2, 0, 0, 0)) || (dtInvTo != new DateTime(2099, 12, 30, 23, 23, 23)))
			//				//if((dtInvFrom != new DateTime(2, 1, 1980, 0, 0, 0)) || (dtInvTo != new DateTime(30, 12, 2099, 23, 23, 23)))
			//				{
			//					strBuilder.Append(" and invoice_date between");
			//					strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvFrom, DTFormat.DateTime));
			//					strBuilder.Append(" and ");
			//					strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvTo, DTFormat.DateTime));
			//				}
			//			}
			if(dtInvFrom != DateTime.MinValue && dtInvTo != DateTime.MinValue)
			{
				strBuilder.Append(" and iv.invoice_date between");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvFrom, DTFormat.DateTime));
				strBuilder.Append(" and ");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvTo, DTFormat.DateTime));
			}
			else if(dtInvFrom != DateTime.MinValue && dtInvTo == DateTime.MinValue)
			{
				strBuilder.Append(" and iv.invoice_date between");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvFrom, DTFormat.DateTime));
				strBuilder.Append(" and ");
				strBuilder.Append(" (select max(invoice_date) from invoice)");
			}
			else if(dtInvFrom == DateTime.MinValue && dtInvTo != DateTime.MinValue)
			{
				strBuilder.Append(" and iv.invoice_date between ");
				strBuilder.Append(" (select min(invoice_date) from invoice)  ");
				strBuilder.Append(" and ");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvTo, DTFormat.DateTime));
			}

			if((strInvNo != null) && (!strInvNo.GetType().Equals(System.Type.GetType("System.DBNull"))) &&(strInvNo.Trim() != ""))
			{
				strBuilder.Append(" and iv.invoice_no like ");
				strBuilder.Append("'%");
				strBuilder.Append(strInvNo);
				strBuilder.Append("'");
			}
			//			if(dtInvDueFrom != DateTime.MinValue || dtInvDueTo != DateTime.MinValue)
			//			{
			//				if((dtInvDueFrom != new DateTime(1980, 1, 2, 0, 0, 0)) || (dtInvDueTo != new DateTime(2099, 12, 30, 23, 23, 23)))
			//				//if((dtInvDueFrom != new DateTime(2, 1, 1980, 0, 0, 0)) || (dtInvDueTo != new DateTime(30, 12, 2099, 23, 23, 23)))
			//				{
			//					strBuilder.Append(" and due_date between");
			//					strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvDueFrom, DTFormat.DateTime));
			//					strBuilder.Append(" and ");
			//					strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvDueTo, DTFormat.DateTime));
			//				}
			//			}
			if(dtInvDueFrom != DateTime.MinValue && dtInvDueTo != DateTime.MinValue)
			{
				strBuilder.Append(" and CAST(s.act_pickup_datetime AS DATE) between");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvDueFrom, DTFormat.DateTime));
				strBuilder.Append(" and ");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvDueTo, DTFormat.DateTime));
			}
			else if(dtInvDueFrom != DateTime.MinValue && dtInvDueTo == DateTime.MinValue)
			{
				strBuilder.Append(" and CAST(s.act_pickup_datetime AS DATE) between");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvDueFrom, DTFormat.DateTime));
				strBuilder.Append(" and ");
				strBuilder.Append(" (select max(CAST(s.act_pickup_datetime) from shipment)");
			}
			else if(dtInvDueFrom == DateTime.MinValue && dtInvDueTo != DateTime.MinValue)
			{
				strBuilder.Append(" and CAST(s.act_pickup_datetime AS DATE) between");
				strBuilder.Append(" (select min(CAST(s.act_pickup_datetime) from shipment) ");
				strBuilder.Append(" and ");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID, dtInvDueTo, DTFormat.DateTime));
			}


			if((strInvType != null) && (!strInvType.GetType().Equals(System.Type.GetType("System.DBNull"))) &&(strInvType.Trim() != ""))
			{
				strBuilder.Append(" and rtrim(ltrim(iv.payment_mode)) = ");
				strBuilder.Append("'");
				strBuilder.Append(strInvType);
				strBuilder.Append("'");
			}

			if((strCustID != null) && (!strCustID.GetType().Equals(System.Type.GetType("System.DBNull"))) &&(strCustID.Trim() != ""))
			{
				strBuilder.Append(" and rtrim(ltrim(iv.payerid)) = ");
				strBuilder.Append("'");
				strBuilder.Append(strCustID);
				strBuilder.Append("'");
			}

			if((strInvStatus != null) && (!strInvStatus.GetType().Equals(System.Type.GetType("System.DBNull"))) &&(strInvStatus.Trim() != ""))
			{
				strBuilder.Append(" and rtrim(ltrim(iv.invoice_status)) = ");
				strBuilder.Append("'");
				strBuilder.Append(strInvStatus);
				strBuilder.Append("'");
			}

			//			strBuilder.Append(" order by invoice_date, invoice_no ");
			strBuilder.Append(" order by CAST(s.act_pickup_datetime AS DATE) ASC ");

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetInvoiceData","GetInvoiceData","DbConnection object is null!!");
				return ds;
			}
			
			
			//sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "InvoiceManageTable");
			dbCmd = dbCon.CreateCommand(strBuilder.ToString() , CommandType.Text);
			ds = (DataSet)dbCon.ExecuteQuery(dbCmd,ReturnType.DataSetType);
			return  ds;
		}


		public static DataSet GetInvoiceDetail(String strAppID, String strEnterpriseID, 
			String strInvNo)
		{
			IDbCommand dbCmd = null;	
			DataSet ds = new DataSet();	
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT InvDtl.invoice_no, InvDtl.consignment_no, InvDtl.booking_datetime, ");
			strBuilder.Append("InvDtl.ref_no, InvDtl.origin_state_code, InvDtl.destination_state_code, InvDtl.tot_pkg, InvDtl.tot_wt, InvDtl.tot_dim_wt, InvDtl.chargeable_wt, InvDtl.pod_datetime, ");
			strBuilder.Append("InvDtl.recipient_name, InvDtl.recipient_address1, InvDtl.recipient_address2, InvDtl.recipient_zipcode, InvDtl.recipient_country, ");
			strBuilder.Append("InvDtl.sender_name, InvDtl.sender_address1, InvDtl.sender_address2, InvDtl.sender_country, InvDtl.sender_zipcode, InvDtl.mbg, InvDtl.service_code, InvDtl.pod_exception, ");
			strBuilder.Append("InvDtl.person_incharge, InvDtl.pod_remark, InvDtl.insurance_amt, InvDtl.tot_freight_charge, InvDtl.tot_vas_surcharge, InvDtl.esa_surcharge, InvDtl.invoice_amt,");
			strBuilder.Append("InvDtl.consignee_name, InvDtl.other_surcharge, InvDtl.total_exception, InvDtl.mbg_amount, InvDtl.invoice_adj_amount, InvDtl.adjusted_by, InvDtl.adjusted_date, InvDtl.adjusted_remark, ");
			strBuilder.Append("Shp.act_pickup_datetime, Shp.act_delivery_date, Shp.service_code ");
			strBuilder.Append("FROM Invoice_Detail as InvDtl ,Shipment as Shp ");
			strBuilder.Append("WHERE InvDtl.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND InvDtl.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' AND InvDtl.consignment_no = Shp.consignment_no");

			if((strInvNo != null) && (!strInvNo.GetType().Equals(System.Type.GetType("System.DBNull"))) &&(strInvNo.Trim() != ""))
			{
				strBuilder.Append(" and InvDtl.invoice_no like ");
				strBuilder.Append("'");
				strBuilder.Append(strInvNo);
				strBuilder.Append("%'");
			}
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetInvoiceData","GetInvoiceData","DbConnection object is null!!");
				return ds;
			}
			
			dbCmd = dbCon.CreateCommand(strBuilder.ToString() , CommandType.Text);
			dbCmd.CommandTimeout = 600; //BY X AUG 13 08
			ds = (DataSet)dbCon.ExecuteQuery(dbCmd,ReturnType.DataSetType);
			//sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "InvoiceManageTable");
			return  ds;
		}


		public static DataSet GetInvoiceDetailForCreditNote(String strAppID, String strEnterpriseID, String strInvNo)
		{
			IDbCommand dbCmd = null;	
			DataSet ds = new DataSet();	
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("Select 0 as Checked,consignment_no,invoice_amt,0.00 as FRG,0.00 as INS,0.00 as VAS,0.00 as ESA,0.00 as OTH, 0.00 as PDX, 0.00 as ADJ, 0.00 as TOT, 0.00 as TotalCredit, tot_freight_charge,insurance_amt,tot_vas_surcharge,esa_surcharge,other_surcharge,total_exception,invoice_adj_amount,mbg,mbg_amount FROM Invoice_Detail ");
			strBuilder.Append("WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' ");

			if((strInvNo != null) && (!strInvNo.GetType().Equals(System.Type.GetType("System.DBNull"))) &&(strInvNo.Trim() != ""))
			{
				strBuilder.Append(" and invoice_no = ");
				strBuilder.Append("'");
				strBuilder.Append(strInvNo);
				strBuilder.Append("'");
			}
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetInvoiceData","GetInvoiceData","DbConnection object is null!!");
				return ds;
			}
			
			dbCmd = dbCon.CreateCommand(strBuilder.ToString() , CommandType.Text);
			dbCmd.CommandTimeout = 600; //BY X AUG 13 08
			ds = (DataSet)dbCon.ExecuteQuery(dbCmd,ReturnType.DataSetType);
			//sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "InvoiceManageTable");
			DataColumn[] keys = new DataColumn[1];
			keys[0] = ds.Tables[0].Columns["consignment_no"];
			ds.Tables[0].PrimaryKey = keys;
			return  ds;
		}

		public static DataSet GetInvoice(String strAppID, String strEnterpriseID,String[] strInvNo)
		{
			IDbCommand dbCmd = null;	
			DataSet ds = new DataSet();
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT inv.invoice_no, inv.invoice_date, inv.due_date, ");
			strBuilder.Append("inv.payerid, inv.payer_type, inv.payer_name, inv.payer_address1, inv.payer_address2, inv.payer_zipcode, ");
			strBuilder.Append("inv.payer_country, inv.payer_telephone, inv.payer_fax, inv.invoice_status, inv.act_payment_date, inv.payment_mode, ");
			strBuilder.Append("inv.printed_datetime, inv.jobid, inv.tot_pkg, inv.tot_wt, inv.tot_dim_wt, inv.chargeable_wt, inv.insurance_amt, ");
			strBuilder.Append("inv.other_surcharge, inv.tot_freight_charge, inv.tot_vas_surcharge, inv.esa_surcharge, inv.total_exception, inv.mbg_amount, inv.invoice_adj_amount, ");
			strBuilder.Append("inv.invoice_amt, inv.tot_cons, inv.approved_by, inv.approved_date, inv.cancelled_by, inv.cancel_date, cus.state_code, cus.contact_person, cus.pod_slip_required, cus.credit_term, inv.actual_bill_placement_date, inv.first_bill_placement_date, inv.next_bill_placement_date, inv.printed_due_date, inv.internal_due_date, inv.updated_by, inv.updated_date, inv.promised_payment_date ");
			strBuilder.Append("FROM Invoice as inv INNER JOIN Customer as cus ");
			strBuilder.Append("ON inv.applicationid = cus.applicationid AND inv.enterpriseid = cus.enterpriseid  AND inv.payerid = cus.custid ");
			strBuilder.Append("WHERE inv.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND inv.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' ");
		

			//AND cus.custid = inv.payerid");
			String invNo="";
			foreach(String tmpInvNo in strInvNo)
			{
				invNo += "'"+ tmpInvNo + "'," ;
			}

			if((strInvNo != null) && (!strInvNo.GetType().Equals(System.Type.GetType("System.DBNull"))) &&(invNo.Trim() != ""))
			
			{
				invNo= invNo.Substring(0,invNo.Length-1);

				strBuilder.Append(" and inv.invoice_no in ");
				strBuilder.Append("'");
				strBuilder.Append(invNo);
				strBuilder.Append("'");
			}
		

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetInvoiceData","GetInvoiceData","DbConnection object is null!!");
				return ds;
			}
			
			
			//sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "InvoiceManageTable");
			dbCmd = dbCon.CreateCommand(strBuilder.ToString() , CommandType.Text);
			ds = (DataSet)dbCon.ExecuteQuery(dbCmd,ReturnType.DataSetType);
			return  ds;
		}

		public static DataSet GetInvoiceByInvNo(String strAppID, String strEnterpriseID,String strInvNo)
		{
			IDbCommand dbCmd = null;	
			DataSet ds = new DataSet();
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT inv.invoice_no, inv.invoice_date, inv.due_date, ");
			strBuilder.Append("inv.payerid, inv.payer_type, inv.payer_name, inv.payer_address1, inv.payer_address2, inv.payer_zipcode, ");
			strBuilder.Append("inv.payer_country, inv.payer_telephone, inv.payer_fax, inv.invoice_status, inv.act_payment_date, inv.payment_mode, ");
			strBuilder.Append("inv.printed_datetime, inv.jobid, inv.tot_pkg, inv.tot_wt, inv.tot_dim_wt, inv.chargeable_wt, inv.insurance_amt, ");
			strBuilder.Append("inv.other_surcharge, inv.tot_freight_charge, inv.tot_vas_surcharge, inv.esa_surcharge, inv.total_exception, inv.mbg_amount, inv.invoice_adj_amount, ");
			strBuilder.Append("inv.invoice_amt, inv.tot_cons, inv.approved_by, inv.approved_date, inv.cancelled_by, inv.cancel_date, cus.state_code, cus.contact_person, cus.pod_slip_required, cus.credit_term, inv.actual_bill_placement_date, inv.first_bill_placement_date, inv.next_bill_placement_date, inv.printed_due_date, inv.internal_due_date, inv.updated_by, inv.updated_date, inv.promised_payment_date ");
			strBuilder.Append("FROM Invoice as inv INNER JOIN Customer as cus ");
			strBuilder.Append("ON inv.applicationid = cus.applicationid AND inv.enterpriseid = cus.enterpriseid  AND inv.payerid = cus.custid ");
			strBuilder.Append("WHERE inv.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND inv.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' ");
		

			//AND cus.custid = inv.payerid");
			

			if((strInvNo != null) && (!strInvNo.GetType().Equals(System.Type.GetType("System.DBNull"))) &&(strInvNo.Trim() != ""))
			{
				strBuilder.Append(" and inv.invoice_no = ");
				strBuilder.Append("'");
				strBuilder.Append(strInvNo);
				strBuilder.Append("'");
			}
		

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetInvoiceData","GetInvoiceData","DbConnection object is null!!");
				return ds;
			}
			
			
			//sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "InvoiceManageTable");
			dbCmd = dbCon.CreateCommand(strBuilder.ToString() , CommandType.Text);
			ds = (DataSet)dbCon.ExecuteQuery(dbCmd,ReturnType.DataSetType);
			return  ds;
		}
		public static DataSet GetInvoiceByInvNo(String strAppID, String strEnterpriseID,String strInvNo, String paymentMode)
		{
			IDbCommand dbCmd = null;	
			DataSet ds = new DataSet();
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT inv.invoice_no, inv.invoice_date, inv.due_date, ");
			strBuilder.Append("inv.payerid, inv.payer_type, inv.payer_name, inv.payer_address1, inv.payer_address2, inv.payer_zipcode, ");
			strBuilder.Append("inv.payer_country, inv.payer_telephone, inv.payer_fax, inv.invoice_status, inv.act_payment_date, inv.payment_mode, ");
			strBuilder.Append("inv.printed_datetime, inv.jobid, inv.tot_pkg, inv.tot_wt, inv.tot_dim_wt, inv.chargeable_wt, inv.insurance_amt, ");
			strBuilder.Append("inv.other_surcharge, inv.tot_freight_charge, inv.tot_vas_surcharge, inv.esa_surcharge, inv.total_exception, inv.mbg_amount, inv.invoice_adj_amount, ");
			strBuilder.Append("inv.invoice_amt, inv.tot_cons, inv.approved_by, inv.approved_date, inv.cancelled_by, inv.cancel_date, cus.state_code, cus.contact_person, cus.pod_slip_required, ");
			strBuilder.Append("cus.credit_term, inv.next_bill_placement_date, inv.actual_bill_placement_date, inv.first_bill_placement_date, cn.credit_no ");
			strBuilder.Append("FROM Invoice as inv INNER JOIN Customer as cus ");
			strBuilder.Append("ON inv.applicationid = cus.applicationid AND inv.enterpriseid = cus.enterpriseid  AND inv.payerid = cus.custid ");
			strBuilder.Append("LEFT JOIN Credit_Note as cn ");
			strBuilder.Append("ON inv.applicationid = cn.applicationid AND inv.enterpriseid = cn.enterpriseid  AND inv.invoice_no = cn.link_to_invoice ");
			strBuilder.Append("WHERE inv.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND inv.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' ");
		

			//AND cus.custid = inv.payerid");
			

			if((strInvNo != null) && (!strInvNo.GetType().Equals(System.Type.GetType("System.DBNull"))) &&(strInvNo.Trim() != ""))
			{
				strBuilder.Append(" and inv.invoice_no = ");
				strBuilder.Append("'");
				strBuilder.Append(strInvNo);
				strBuilder.Append("'");
			}
			strBuilder.Append(" and inv.payment_mode = ");
			strBuilder.Append("'");
			strBuilder.Append(paymentMode);
			strBuilder.Append("'");

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetInvoiceData","GetInvoiceData","DbConnection object is null!!");
				return ds;
			}
			
			
			//sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "InvoiceManageTable");
			dbCmd = dbCon.CreateCommand(strBuilder.ToString() , CommandType.Text);
			ds = (DataSet)dbCon.ExecuteQuery(dbCmd,ReturnType.DataSetType);
			return  ds;
		}

		// By Aoo
		public static int ApproveInvoice(String strAppID, String strEnterpriseID,DataSet dsAPInv)
		{
			int getRow = 0;
			StringBuilder strBuild = null;
			IDbCommand dbCmd = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","CancelInvoice","CancelInvoice","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","CancelInvoice","CancelInvoice","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("InvoiceManagementMgrDAL","CancelInvoice","CancelInvoice","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("InvoiceManagementMgrDAL","CancelInvoice","CancelInvoice","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","CancelInvoice","CancelInvoice","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				foreach(DataRow drGetDS in dsAPInv.Tables[0].Rows)
				{
					//Added By GwanG on 09May08
					strBuild = new StringBuilder();
					strBuild.Append("SELECT cus.credit_term ");
					strBuild.Append("FROM Invoice as inv INNER JOIN Customer as cus ");
					strBuild.Append("ON inv.applicationid = cus.applicationid AND inv.enterpriseid = cus.enterpriseid  AND inv.payerid = cus.custid ");
					strBuild.Append("WHERE inv.applicationid = '");
					strBuild.Append(strAppID);
					strBuild.Append("' AND inv.enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' AND inv.invoice_no = ");
					if((drGetDS["invoice_no"] != null) && (!drGetDS["invoice_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strBuild.Append(" '");
						strBuild.Append(drGetDS["invoice_no"].ToString());
						strBuild.Append("' ");
					}
					else
					{
						strBuild.Append("NULL");
					}

					dbCmd = dbCon.CreateCommand(strBuild.ToString() , CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;

					DataSet ds  = new DataSet();
					ds = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
					int dayAdded = 0;
					DateTime dueDate = new DateTime();
					if(ds.Tables[0].Rows.Count > 0)
					{						
						if((ds.Tables[0].Rows[0]["credit_term"] != null) && (ds.Tables[0].Rows[0]["credit_term"].ToString() != "") && (!ds.Tables[0].Rows[0]["credit_term"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							dayAdded = Convert.ToInt16(ds.Tables[0].Rows[0]["credit_term"]);
						}
					}
					//
					strBuild = new StringBuilder();
					strBuild.Append("Update Invoice Set Invoice.invoice_status = 'A'");
					strBuild.Append(",Invoice.approved_date = ");

					if((drGetDS["approved_date"] != null) && (!drGetDS["approved_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strBuild.Append(" '");
						strBuild.Append(drGetDS["approved_date"].ToString());
						strBuild.Append("' ");
					}
					else
					{
						strBuild.Append("NULL");
					}

					//Added By GwanG on 09May08
					strBuild.Append(",Invoice.invoice_date = ");

					if((drGetDS["approved_date"] != null) && (!drGetDS["approved_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strBuild.Append(" '");
						strBuild.Append(drGetDS["approved_date"].ToString());
						strBuild.Append("' ");
					}
					else
					{
						strBuild.Append("NULL");
					}

					strBuild.Append(",Invoice.due_date = ");

					if((drGetDS["approved_date"] != null) && (!drGetDS["approved_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dueDate = Convert.ToDateTime(drGetDS["approved_date"].ToString());
						dueDate = dueDate.AddDays((double)dayAdded);
						strBuild.Append(" '");
						strBuild.Append(dueDate.ToString());
						strBuild.Append("' ");						
					}
					else
					{
						strBuild.Append("NULL");
					}
					//Print Due Date
					strBuild.Append(",Invoice.printed_due_date = ");

					if((drGetDS["approved_date"] != null) && (!drGetDS["approved_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						dueDate = Convert.ToDateTime(drGetDS["approved_date"].ToString());
						dueDate = dueDate.AddDays((double)dayAdded);
						strBuild.Append(" '");
						strBuild.Append(dueDate.ToString());
						strBuild.Append("' ");						
					}
					else
					{
						strBuild.Append("NULL");
					}
					//
					strBuild.Append(", Invoice.approved_by =");

					if((drGetDS["approved_by"] != null) && (!drGetDS["approved_by"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strBuild.Append(" '");
						strBuild.Append(drGetDS["approved_by"].ToString());
						strBuild.Append("' ");
					}
					else
					{
						strBuild.Append("NULL");
					}
					
					// Updated_by and Updated_date 
					strBuild.Append(", Invoice.updated_by = '"+drGetDS["approved_by"].ToString()+"'");
					strBuild.Append(", Invoice.updated_date = getdate()");

					strBuild.Append(" Where Invoice.invoice_no =");

					if((drGetDS["invoice_no"] != null) && (!drGetDS["invoice_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strBuild.Append(" '");
						strBuild.Append(drGetDS["invoice_no"].ToString());
						strBuild.Append("' ");
					}
					else
					{
						strBuild.Append("NULL");
					}

					strBuild.Append(" AND Invoice.applicationid = '"+strAppID+"' AND Invoice.enterpriseid = '"+strEnterpriseID+"'");

					dbCmd = dbCon.CreateCommand(strBuild.ToString() , CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;
					getRow = dbCon.ExecuteNonQueryInTransaction(dbCmd);


					string upShipment = "SELECT sm.booking_no, sm.consignment_no FROM Invoice_Detail AS ivd INNER JOIN Shipment AS sm ON ivd.consignment_no = sm.consignment_no WHERE (ivd.invoice_no = '"+drGetDS["invoice_no"].ToString()+"')";
					dbCmd.CommandText = upShipment;
					ds  = new DataSet();
					ds = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);

					foreach(DataRow dr in ds.Tables[0].Rows)
					{
						Utility.CreateAutoConsignmentHistory(strAppID,strEnterpriseID,4,"",Convert.ToInt32(dr["booking_no"].ToString()),dr["consignment_no"].ToString(),System.DateTime.Now,drGetDS["approved_by"].ToString(),ref dbCmd,ref dbCon);
					}
				}
				transactionApp.Commit();
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("InvoiceManagementMgrDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("InvoiceManagementMgrDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("InvoiceManagementMgrDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting audit data ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("InvoiceManagementMgrDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("InvoiceManagementMgrDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("InvoiceManagementMgrDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting audit data ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return getRow;
		}

		//End By Aoo
		public static int CancelInvoice(String strAppID
			, String strEnterpriseID
			, DataSet dsInvoice
			, DataSet dsShipment
			, String CancelType
			, String Reason)
		{
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			IDbCommand dbCmd = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","CancelInvoice","CancelInvoice","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","CancelInvoice","CancelInvoice","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("InvoiceManagementMgrDAL","CancelInvoice","CancelInvoice","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("InvoiceManagementMgrDAL","CancelInvoice","CancelInvoice","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","CancelInvoice","CancelInvoice","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{

				//Update Invoice Table
				foreach(DataRow drRecInvoice in dsInvoice.Tables[0].Rows)
				{
	
					string delShipment = "Select * from Core_Enterprise";
					dbCmd = dbCon.CreateCommand(delShipment, CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.CommandTimeout = 600; //BY X AUG 13 08
					dbCmd.Transaction = transactionApp;

					DataSet ds  = new DataSet();
					ds = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);

					//Update Invoice
					strBuild = new StringBuilder();
					strBuild.Append("Update Invoice Set ");
					strBuild.Append("invoice_status = 'C'");
					strBuild.Append(",");
					//ADD BY X AUG 11 08
					strBuild.Append("InvoiceCancelNo = '"+ drRecInvoice["invoice_no"].ToString() + CancelType +"'");
					strBuild.Append(",");
					strBuild.Append("CancelType = '"+ CancelType +"'");
					strBuild.Append(",");
					strBuild.Append("Reason = '"+ Reason +"'");
					strBuild.Append(",");
					//END ADD BY X
					strBuild.Append("cancelled_by = ");
					if((drRecInvoice["cancelled_by"]!= null) && (!drRecInvoice["cancelled_by"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strdrRecInvoice = (String) drRecInvoice["cancelled_by"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strdrRecInvoice));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}

					strBuild.Append(",");
					strBuild.Append("cancel_date = ");
					if((drRecInvoice["cancel_date"]!= null) && (!drRecInvoice["cancel_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtdrRecInvoice = Convert.ToDateTime(drRecInvoice["cancel_date"].ToString());;
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtdrRecInvoice,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}

					String invoice_no = (String)drRecInvoice["invoice_no"].ToString();
					strBuild.Append("WHERE applicationid = '");
					strBuild.Append(strAppID);
					strBuild.Append("' AND enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' AND invoice_no = '");
					strBuild.Append(invoice_no);
					strBuild.Append("'");
					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

					//Update Shipment
					strBuild = new StringBuilder();
					strBuild.Append("Update Shipment ");
					strBuild.Append("SET mbg_amount = 0, ");
					strBuild.Append("total_excp_charges = 0, ");
					strBuild.Append("invoice_adjustment = 0, ");
					strBuild.Append("invoice_amt = 0, ");
					strBuild.Append("debit_amt = 0, ");
					strBuild.Append("credit_amt = 0, ");
					strBuild.Append("total_cons_revenue = 0, ");
					strBuild.Append("invoice_no = null, ");
					strBuild.Append("invoice_date = null, "); 
					strBuild.Append("MasterAWBNumber = null ");
					strBuild.Append("FROM Shipment, Invoice_Detail ");
					strBuild.Append("WHERE Shipment.applicationid = Invoice_Detail.applicationid ");
					strBuild.Append("AND Shipment.enterpriseid = Invoice_Detail.enterpriseid ");
					strBuild.Append("AND Shipment.invoice_no = Invoice_Detail.invoice_no ");
					strBuild.Append("AND Shipment.consignment_no = Invoice_Detail.consignment_no ");
					strBuild.Append("AND Shipment.applicationid = '");
					strBuild.Append(strAppID);
					strBuild.Append("' AND Shipment.enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' AND Invoice_Detail.invoice_no = '");
					strBuild.Append(invoice_no);
					strBuild.Append("'");
					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

					/*
					//Delete Shipment History for Status 'INVA'
					strBuild = new StringBuilder();
					strBuild.Append("UPDATE Shipment_tracking WITH(ROWLOCK) ");
					strBuild.Append("SET Shipment_tracking.deleted = 'Y', ");
					strBuild.Append("Shipment_tracking.delete_remark = '', ");
					strBuild.Append("Shipment_tracking.last_userid = '" + drRecInvoice["cancelled_by"].ToString() + "', ");
					strBuild.Append("Shipment_tracking.last_updated = " + Utility.DateFormat(strAppID, strEnterpriseID, System.DateTime.Now, DTFormat.DateTime));
					strBuild.Append("FROM shipment_tracking ");
					strBuild.Append("INNER JOIN	(SELECT applicationid, enterpriseid, booking_no, consignment_no, status_code, max(tracking_datetime) as tracking_datetime ");
					strBuild.Append("FROM Shipment_tracking ");
					strBuild.Append("WHERE applicationid = '"); 
					strBuild.Append(strAppID);
					strBuild.Append("' AND enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' AND status_code = (SELECT distinct status_code ");
					strBuild.Append("FROM status_code ");
					strBuild.Append("WHERE applicationid = '"); 
					strBuild.Append(strAppID);
					strBuild.Append("' AND enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' AND auto_process = 4 ) ");
					strBuild.Append(" AND isnull(deleted, 'N') <> 'Y' ");
					strBuild.Append("group by applicationid, enterpriseid, booking_no, consignment_no, status_code) SHPT ");
					strBuild.Append("ON (shipment_tracking.applicationid = SHPT.applicationid ");
					strBuild.Append("AND shipment_tracking.enterpriseid = SHPT.enterpriseid ");
					strBuild.Append("AND shipment_tracking.consignment_no = SHPT.consignment_no ");
					strBuild.Append("AND shipment_tracking.booking_no = SHPT.booking_no ");
					strBuild.Append("AND shipment_tracking.tracking_datetime = SHPT.tracking_datetime) ");
					strBuild.Append("INNER JOIN	(SELECT applicationid, enterpriseid, invoice_no, consignment_no ");
					strBuild.Append("FROM Invoice_Detail ");
					strBuild.Append("WHERE applicationid = '"); 
					strBuild.Append(strAppID);
					strBuild.Append("' AND enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' AND Invoice_Detail.invoice_no = '");
					strBuild.Append(invoice_no);
					strBuild.Append("') INVD ");
					strBuild.Append("ON	(shipment_tracking.applicationid = INVD.applicationid ");
					strBuild.Append("AND shipment_tracking.enterpriseid = INVD.enterpriseid ");
					strBuild.Append("AND shipment_tracking.consignment_no = INVD.consignment_no) ");
					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

					//Delete Shipment History for Status 'INVA'
					strBuild = new StringBuilder();
					strBuild.Append("insert into MF_Shipment_Tracking_Staging with(rowlock) (pkg_no,applicationid, enterpriseid, consignment_no, tracking_datetime, ");
					strBuild.Append("booking_no, status_code, exception_code, person_incharge, remark, deleted, delete_remark, ");
					strBuild.Append("last_userid, last_updated, invoiceable, reason, surcharge, consignee_name, location) ");
					strBuild.Append("select 0,'" + strAppID + "', '" + strEnterpriseID + "', invoice_detail.consignment_no, ");
					strBuild.Append("'"+System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")+"'" + ", ");
					strBuild.Append("(select 0 as booking_no ");
					strBuild.Append("from shipment ");
					strBuild.Append("where applicationid = '"); 
					strBuild.Append(strAppID);
					strBuild.Append("' and enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' and	consignment_no = invoice_detail.consignment_no), ");
					strBuild.Append("(select status_code ");
					strBuild.Append("from status_code ");
					strBuild.Append("where applicationid = '"); 
					strBuild.Append(strAppID);
					strBuild.Append("' and enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' and auto_process = 131), null, null, '', 'N', null, ");
					strBuild.Append("'" + drRecInvoice["cancelled_by"].ToString() + "', "); 
					strBuild.Append("'"+System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")+"'" + ", ");
					strBuild.Append("(select invoiceable ");
					strBuild.Append("from status_code ");
					strBuild.Append("where applicationid = '"); 
					strBuild.Append(strAppID);
					strBuild.Append("' and enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' and auto_process = 131), null, null, null, ");
					strBuild.Append("(select case when auto_location = 'Y' then ");
					strBuild.Append("(select location ");
					strBuild.Append("from user_master ");
					strBuild.Append("where applicationid = '"); 
					strBuild.Append(strAppID);
					strBuild.Append("' and enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' and userid = '" +  drRecInvoice["cancelled_by"].ToString() + "') ");
					strBuild.Append("else null end ");
					strBuild.Append("from status_code ");
					strBuild.Append("where applicationid = '"); 
					strBuild.Append(strAppID);
					strBuild.Append("' and enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' and auto_process = 131) ");
					strBuild.Append("FROM invoice_detail ");
					strBuild.Append("WHERE invoice_detail.applicationid = '");
					strBuild.Append(strAppID); 
					strBuild.Append("' AND invoice_detail.enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' AND invoice_detail.invoice_no = '");
					strBuild.Append(invoice_no);
					strBuild.Append("'");
					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					*/
				}
				transactionApp.Commit();
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("InvoiceManagementMgrDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("InvoiceManagementMgrDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("InvoiceManagementMgrDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting audit data ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("InvoiceManagementMgrDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("InvoiceManagementMgrDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("InvoiceManagementMgrDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting audit data ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;

		}


		public static int CancelInvoice(String strAppID
			, String strEnterpriseID
			, DataSet dsInvoice
			, DataSet dsShipment
			, String CancelType
			, String Reason
			, IDbTransaction transactionApp
			, IDbConnection conApp
			, DbConnection dbCon)	
		{
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			IDbCommand dbCmd = null;	

			//IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","CancelInvoice","CancelInvoice","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
//			try
//			{

				//Update Invoice Table
				foreach(DataRow drRecInvoice in dsInvoice.Tables[0].Rows)
				{
	
					string delShipment = "Select * from Core_Enterprise";
					dbCmd = dbCon.CreateCommand(delShipment, CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.CommandTimeout = 600; //BY X AUG 13 08
					dbCmd.Transaction = transactionApp;

					DataSet ds  = new DataSet();
					ds = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);

					//Update Invoice
					strBuild = new StringBuilder();
					strBuild.Append("Update Invoice Set ");
					strBuild.Append("invoice_status = 'C'");
					strBuild.Append(",");
					//ADD BY X AUG 11 08
					strBuild.Append("InvoiceCancelNo = '"+ drRecInvoice["invoice_no"].ToString() + CancelType +"'");
					strBuild.Append(",");
					strBuild.Append("CancelType = '"+ CancelType +"'");
					strBuild.Append(",");
					strBuild.Append("Reason = '"+ Reason +"'");
					strBuild.Append(",");
					//END ADD BY X
					strBuild.Append("cancelled_by = ");
					if((drRecInvoice["cancelled_by"]!= null) && (!drRecInvoice["cancelled_by"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strdrRecInvoice = (String) drRecInvoice["cancelled_by"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strdrRecInvoice));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}

					strBuild.Append(",");
					strBuild.Append("cancel_date = ");
					if((drRecInvoice["cancel_date"]!= null) && (!drRecInvoice["cancel_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtdrRecInvoice = Convert.ToDateTime(drRecInvoice["cancel_date"].ToString());;
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtdrRecInvoice,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}

					String invoice_no = (String)drRecInvoice["invoice_no"].ToString();
					strBuild.Append("WHERE applicationid = '");
					strBuild.Append(strAppID);
					strBuild.Append("' AND enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' AND invoice_no = '");
					strBuild.Append(invoice_no);
					strBuild.Append("'");
					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

					//Update Shipment
					strBuild = new StringBuilder();
					strBuild.Append("Update Shipment ");
					strBuild.Append("SET mbg_amount = 0, ");
					strBuild.Append("total_excp_charges = 0, ");
					strBuild.Append("invoice_adjustment = 0, ");
					strBuild.Append("invoice_amt = 0, ");
					strBuild.Append("debit_amt = 0, ");
					strBuild.Append("credit_amt = 0, ");
					strBuild.Append("total_cons_revenue = 0, ");
					strBuild.Append("invoice_no = null, ");
					strBuild.Append("invoice_date = null, "); 
					strBuild.Append("MasterAWBNumber = null ");
					strBuild.Append("FROM Shipment, Invoice_Detail ");
					strBuild.Append("WHERE Shipment.applicationid = Invoice_Detail.applicationid ");
					strBuild.Append("AND Shipment.enterpriseid = Invoice_Detail.enterpriseid ");
					strBuild.Append("AND Shipment.invoice_no = Invoice_Detail.invoice_no ");
					strBuild.Append("AND Shipment.consignment_no = Invoice_Detail.consignment_no ");
					strBuild.Append("AND Shipment.applicationid = '");
					strBuild.Append(strAppID);
					strBuild.Append("' AND Shipment.enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' AND Invoice_Detail.invoice_no = '");
					strBuild.Append(invoice_no);
					strBuild.Append("'");
					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					
					/*
					
					//Delete Shipment History for Status 'INVA'
					strBuild = new StringBuilder();
					strBuild.Append("UPDATE Shipment_tracking ");
					strBuild.Append("SET Shipment_tracking.deleted = 'Y', ");
					strBuild.Append("Shipment_tracking.delete_remark = '', ");
					strBuild.Append("Shipment_tracking.last_userid = '" + drRecInvoice["cancelled_by"].ToString() + "', ");
					strBuild.Append("Shipment_tracking.last_updated = " + Utility.DateFormat(strAppID, strEnterpriseID, System.DateTime.Now, DTFormat.DateTime));
					strBuild.Append("FROM shipment_tracking ");
					strBuild.Append("INNER JOIN	(SELECT applicationid, enterpriseid, booking_no, consignment_no, status_code, max(tracking_datetime) as tracking_datetime ");
					strBuild.Append("FROM Shipment_tracking ");
					strBuild.Append("WHERE applicationid = '"); 
					strBuild.Append(strAppID);
					strBuild.Append("' AND enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' AND status_code = (SELECT distinct status_code ");
					strBuild.Append("FROM status_code ");
					strBuild.Append("WHERE applicationid = '"); 
					strBuild.Append(strAppID);
					strBuild.Append("' AND enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' AND auto_process = 4 ) ");
					strBuild.Append(" AND isnull(deleted, 'N') <> 'Y' ");
					strBuild.Append("group by applicationid, enterpriseid, booking_no, consignment_no, status_code) SHPT ");
					strBuild.Append("ON (shipment_tracking.applicationid = SHPT.applicationid ");
					strBuild.Append("AND shipment_tracking.enterpriseid = SHPT.enterpriseid ");
					strBuild.Append("AND shipment_tracking.consignment_no = SHPT.consignment_no ");
					strBuild.Append("AND shipment_tracking.booking_no = SHPT.booking_no ");
					strBuild.Append("AND shipment_tracking.tracking_datetime = SHPT.tracking_datetime) ");
					strBuild.Append("INNER JOIN	(SELECT applicationid, enterpriseid, invoice_no, consignment_no ");
					strBuild.Append("FROM Invoice_Detail ");
					strBuild.Append("WHERE applicationid = '"); 
					strBuild.Append(strAppID);
					strBuild.Append("' AND enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' AND Invoice_Detail.invoice_no = '");
					strBuild.Append(invoice_no);
					strBuild.Append("') INVD ");
					strBuild.Append("ON	(shipment_tracking.applicationid = INVD.applicationid ");
					strBuild.Append("AND shipment_tracking.enterpriseid = INVD.enterpriseid ");
					strBuild.Append("AND shipment_tracking.consignment_no = INVD.consignment_no) ");
					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

					//Delete Shipment History for Status 'INVA'
					strBuild = new StringBuilder();
					strBuild.Append("insert into MF_Shipment_Tracking_Staging (pkg_no,applicationid, enterpriseid, consignment_no, tracking_datetime, ");
					strBuild.Append("booking_no, status_code, exception_code, person_incharge, remark, deleted, delete_remark, ");
					strBuild.Append("last_userid, last_updated, invoiceable, reason, surcharge, consignee_name, location) ");
					strBuild.Append("select 0,'" + strAppID + "', '" + strEnterpriseID + "', invoice_detail.consignment_no, ");
					strBuild.Append("'"+System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")+"'" + ", ");
					strBuild.Append("(select 0 as booking_no ");
					strBuild.Append("from shipment ");
					strBuild.Append("where applicationid = '"); 
					strBuild.Append(strAppID);
					strBuild.Append("' and enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' and	consignment_no = invoice_detail.consignment_no), ");
					strBuild.Append("(select status_code ");
					strBuild.Append("from status_code ");
					strBuild.Append("where applicationid = '"); 
					strBuild.Append(strAppID);
					strBuild.Append("' and enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' and auto_process = 131), null, null, '', 'N', null, ");
					strBuild.Append("'" + drRecInvoice["cancelled_by"].ToString() + "', "); 
					strBuild.Append("'"+System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")+"'" + ", ");
					strBuild.Append("(select invoiceable ");
					strBuild.Append("from status_code ");
					strBuild.Append("where applicationid = '"); 
					strBuild.Append(strAppID);
					strBuild.Append("' and enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' and auto_process = 131), null, null, null, ");
					strBuild.Append("(select case when auto_location = 'Y' then ");
					strBuild.Append("(select location ");
					strBuild.Append("from user_master ");
					strBuild.Append("where applicationid = '"); 
					strBuild.Append(strAppID);
					strBuild.Append("' and enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' and userid = '" +  drRecInvoice["cancelled_by"].ToString() + "') ");
					strBuild.Append("else null end ");
					strBuild.Append("from status_code ");
					strBuild.Append("where applicationid = '"); 
					strBuild.Append(strAppID);
					strBuild.Append("' and enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' and auto_process = 131) ");
					strBuild.Append("FROM invoice_detail ");
					strBuild.Append("WHERE invoice_detail.applicationid = '");
					strBuild.Append(strAppID); 
					strBuild.Append("' AND invoice_detail.enterpriseid = '");
					strBuild.Append(strEnterpriseID);
					strBuild.Append("' AND invoice_detail.invoice_no = '");
					strBuild.Append(invoice_no);
					strBuild.Append("'");
					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					*/
				}

			return iRowsAffected;

		}


		public static int AdjustInvoice(String strAppID, String strEnterpriseID,DataSet dsInvoiceDetail)
		{
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			IDbCommand dbCmd = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				strBuild = new StringBuilder();
				DataRow drRecInvoicedtl = dsInvoiceDetail.Tables[0].Rows[0];


				//UPDATE INVOICE_DETAIL
				strBuild.Append("Update Invoice_Detail Set ");
				strBuild.Append("adjusted_by = ");
				if((drRecInvoicedtl["adjusted_by"]!= null) && (!drRecInvoicedtl["adjusted_by"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String adjusted_by = (String) drRecInvoicedtl["adjusted_by"];
					strBuild.Append("'");
					strBuild.Append(Utility.ReplaceSingleQuote(adjusted_by));
					strBuild.Append("'");
				}
				else
				{
					strBuild.Append("null");
				}
				strBuild.Append(",");
				
				strBuild.Append("adjusted_date = ");
				if((drRecInvoicedtl["adjusted_date"]!= null) && (!drRecInvoicedtl["adjusted_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime adjusted_date = (DateTime) drRecInvoicedtl["adjusted_date"];
					strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,adjusted_date,DTFormat.DateTime));
				}
				else
				{
					strBuild.Append("null");
				}
				strBuild.Append(",");

				strBuild.Append("invoice_adj_amount = ");
				if((drRecInvoicedtl["invoice_adj_amount"]!= null) && (!drRecInvoicedtl["invoice_adj_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal invoice_adj_amount = (decimal) drRecInvoicedtl["invoice_adj_amount"];
					strBuild.Append(invoice_adj_amount);
				}
				else
				{
					strBuild.Append("null");
				}
				strBuild.Append(",");


				strBuild.Append("invoice_amt = ");
				if((drRecInvoicedtl["invoice_amt"]!= null) && (!drRecInvoicedtl["invoice_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal invoice_amt = (decimal) drRecInvoicedtl["invoice_amt"];
					strBuild.Append(invoice_amt);
				}
				else
				{
					strBuild.Append("null");
				}
				strBuild.Append(",");

				strBuild.Append("adjusted_remark = ");
				if((drRecInvoicedtl["adjusted_remark"]!= null) && (!drRecInvoicedtl["adjusted_remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String adjusted_remark = (String) drRecInvoicedtl["adjusted_remark"];
					strBuild.Append("N'");
					strBuild.Append(Utility.ReplaceSingleQuote(adjusted_remark));
					strBuild.Append("'");
				}
				else
				{
					strBuild.Append("null");
				}
				
				String invoice_no = "";
				invoice_no = (String)drRecInvoicedtl["invoice_no"];
				String consgn_no = "";
				consgn_no = (String)drRecInvoicedtl["consignment_no"];
				strBuild.Append(" WHERE applicationid = '");
				strBuild.Append(strAppID);
				strBuild.Append("' AND enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' AND invoice_no = '");
				strBuild.Append(invoice_no);
				strBuild.Append("' AND consignment_no = '");
				strBuild.Append(consgn_no);
				strBuild.Append("'");

				dbCmd = dbCon.CreateCommand(strBuild.ToString() , CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				//				NEW UPDATE INVOICE
				strBuild = new StringBuilder();
				strBuild.Append("Update Invoice Set");
				strBuild.Append(" Invoice.invoice_amt = ");
				strBuild.Append(" (Select SUM(Invoice_Detail.invoice_amt)");
				strBuild.Append(" From Invoice_Detail");
				strBuild.Append(" where Invoice_Detail.invoice_no = '");
				strBuild.Append(invoice_no);
				strBuild.Append("' and  Invoice_Detail.applicationid = '");
				strBuild.Append(strAppID);
				strBuild.Append("' and Invoice_Detail.enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("')");
				
				// Modified 14-Feb-12
				strBuild.Append(", Invoice.updated_by ='"+(String) drRecInvoicedtl["adjusted_by"]+"'");
				strBuild.Append(", Invoice.updated_date = getdate()");

				strBuild.Append(", Invoice.invoice_adj_amount =");
				strBuild.Append(" (Select SUM(Invoice_Detail.invoice_adj_amount)");
				strBuild.Append(" From Invoice_Detail where Invoice_Detail.invoice_no = '");
				strBuild.Append(invoice_no);
				strBuild.Append("' and  Invoice_Detail.applicationid = '");
				strBuild.Append(strAppID);
				strBuild.Append("' and Invoice_Detail.enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("')");
				strBuild.Append(" where");
				strBuild.Append(" Invoice.invoice_no = '");
				strBuild.Append(invoice_no);
				strBuild.Append("' and  Invoice.applicationid = '");
				strBuild.Append(strAppID);
				strBuild.Append("' and Invoice.enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("'");

				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);


				//UPDATE SHIPMENT
				//DataSet dsShipment = DomesticShipmentMgrDAL.GetEmptyDomesticShipDS();
				DataSet dsShipment = GetDomesticSHP(strAppID,strEnterpriseID,consgn_no,dbCon,dbCmd);
				DataRow drRecShipment = dsShipment.Tables[0].Rows[0];

				strBuild = new StringBuilder();
				strBuild.Append("Update Shipment Set ");
				strBuild.Append("invoice_adjustment = ");
				if((drRecInvoicedtl["invoice_adj_amount"]!= null) && (!drRecInvoicedtl["invoice_adj_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal invoice_adj_amount = (decimal) drRecInvoicedtl["invoice_adj_amount"];
					strBuild.Append(invoice_adj_amount);
				}
				else
				{
					strBuild.Append("null");
				}
				strBuild.Append(",");

				//Calculate invoice_amt
				decimal temp_invoice_amt = 0;
				if((drRecInvoicedtl["invoice_adj_amount"]!= null) && (!drRecInvoicedtl["invoice_adj_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					temp_invoice_amt += (decimal) drRecInvoicedtl["invoice_adj_amount"];
				}
				if((drRecShipment["total_rated_amount"]!= null) && (!drRecShipment["total_rated_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					temp_invoice_amt += (decimal)drRecShipment["total_rated_amount"];
				}

				if((drRecShipment["total_excp_charges"]!= null) && (!drRecShipment["total_excp_charges"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					temp_invoice_amt += (decimal)drRecShipment["total_excp_charges"];
				}
				
				if((drRecShipment["mbg_amount"]!= null) && (!drRecShipment["mbg_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					temp_invoice_amt -= (decimal)drRecShipment["mbg_amount"];
				}


				strBuild.Append("invoice_amt = ");
				strBuild.Append(temp_invoice_amt);
				strBuild.Append(",");

				//Calculate total_cons_revenue
				decimal temp_total_cons_revenue = temp_invoice_amt;
				if((drRecShipment["debit_amt"]!= null) && (!drRecShipment["debit_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					temp_total_cons_revenue += (decimal)drRecShipment["debit_amt"];
				}
				if((drRecShipment["credit_amt"]!= null) && (!drRecShipment["credit_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					temp_total_cons_revenue -= (decimal)drRecShipment["credit_amt"];
				}
				strBuild.Append("total_cons_revenue = ");
				strBuild.Append(temp_total_cons_revenue);

				strBuild.Append("WHERE applicationid = '");
				strBuild.Append(strAppID);
				strBuild.Append("' AND enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' AND consignment_no = '");
				strBuild.Append(consgn_no);
				strBuild.Append("'");



				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);		
		
				//DELETE INVOICE_SUMMARY
				//Service_Code = 'SHP'
				strBuild = new StringBuilder();
				strBuild.Append("delete from Invoice_Summary  with(rowlock) where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and invoice_no = '");
				strBuild.Append(invoice_no);
				strBuild.Append("'");
				strBuild.Append(" and summary_code_type = 'SHP' ");

				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);				

				//Service_Code = 'ADJ'
				strBuild = new StringBuilder();
				strBuild.Append("delete from Invoice_Summary  where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and invoice_no = '");
				strBuild.Append(invoice_no);
				strBuild.Append("'");
				strBuild.Append(" and summary_code = 'ADJ' and summary_code_type = 'ADJ' ");

				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);	



				//UPDATE INVOICE_SUMMARY
				//Service_Code = 'SHP'
				strBuild = new StringBuilder();	
				strBuild.Append("declare @applicationid varchar(40),@enterpriseid varchar(40),@invoice_no varchar(20),@service_code varchar(12) ");
				strBuild.Append("set @applicationid = ");
				strBuild.Append("'"+strAppID+"' ");
				strBuild.Append("set @enterpriseid = ");
				strBuild.Append("'"+strEnterpriseID+"' ");
				strBuild.Append("set @invoice_no = ");
				strBuild.Append("'"+invoice_no+"' ");
				strBuild.Append("set @service_code = 'SHP' ");

				strBuild.Append("INSERT INTO Invoice_Summary ");
				strBuild.Append("(applicationid,enterpriseid,invoice_no,tot_consignment,tot_pkg, ");
				strBuild.Append("tot_chargeable_wt,invoice_amt,summary_code,summary_code_type,sequence) ");
				strBuild.Append("SELECT  @applicationid,@enterpriseid,@invoice_no,COUNT(*)as rows,SUM(tot_pkg) AS tot_pkg, ");
				strBuild.Append("SUM(chargeable_wt) AS tot_chargeable_wt,SUM(invoice_amt) AS invoice_amt,service_code,@service_code,0 ");
				strBuild.Append("FROM	Invoice_Detail WHERE  applicationid=@applicationid and enterpriseid = @enterpriseid AND ");
				strBuild.Append("invoice_no = @invoice_no GROUP BY invoice_no, service_code ");
				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);


				//Service_Code = 'ADJ'
				strBuild = new StringBuilder();
				strBuild.Append("declare @applicationid varchar(40),@enterpriseid varchar(40),@invoice_no varchar(20),@service_code varchar(12) ");
				strBuild.Append("set @applicationid = ");
				strBuild.Append("'"+strAppID+"' ");
				strBuild.Append("set @enterpriseid = ");
				strBuild.Append("'"+strEnterpriseID+"' ");
				strBuild.Append("set @invoice_no = ");
				strBuild.Append("'"+invoice_no+"' ");
				strBuild.Append("set @service_code = 'ADJ' ");

				strBuild.Append("INSERT INTO Invoice_Summary with(rowlock) ");
				strBuild.Append("(applicationid,enterpriseid,invoice_no,tot_consignment,tot_pkg, ");
				strBuild.Append("tot_chargeable_wt,invoice_amt,summary_code,summary_code_type,sequence) ");
				strBuild.Append("SELECT  @applicationid,@enterpriseid,@invoice_no,COUNT(*)as rows,SUM(tot_pkg) AS tot_pkg, ");
				strBuild.Append("SUM(chargeable_wt) AS tot_chargeable_wt,SUM(invoice_adj_amount) AS invoice_amt,@service_code,@service_code,5 ");
				strBuild.Append("FROM	Invoice_Detail 	WHERE   applicationid=@applicationid and enterpriseid=@enterpriseid and ");
				strBuild.Append("invoice_no=@invoice_no and (invoice_adj_amount <> 0) GROUP BY invoice_no ");

				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				transactionApp.Commit();

			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error adjusting audit data ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("EError adjusting audit data ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}


			return iRowsAffected;
			

		}


		//Update tot_freight_charge to the Invoice_Summary table when click print header report
		//Created by GwanG on 21May08
		public static int UpdateInvsumFreight(String strAppID, String strEnterpriseID, String strInvNo)
		{
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			IDbCommand dbCmd = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteDomesticShp","ERR004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			try
			{
				strBuild = new StringBuilder();
				strBuild.Append("UPDATE Invoice_Summary SET Invoice_Summary.tot_freight = inv0.tot_freight ");
				strBuild.Append("From (select Sum(tot_freight_charge) tot_freight,service_code from Invoice_Detail ");
				strBuild.Append("where enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and applicationid = '");
				strBuild.Append(strAppID);
				strBuild.Append("' and invoice_no = '");
				strBuild.Append(strInvNo);
				strBuild.Append("' group by service_code)inv0 ");
                strBuild.Append("Where Invoice_Summary.summary_code = inv0.service_code ");
				strBuild.Append("and Invoice_Summary.enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and Invoice_Summary.applicationid = '");
				strBuild.Append(strAppID);
				strBuild.Append("' and Invoice_Summary.invoice_no = '");
				strBuild.Append(strInvNo);
				strBuild.Append("'");
				
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dbCmd.Connection = conApp;
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","UpdateInvsumFreight","UpdateInvsumFreight","UpdateInvsumFreight Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error UpdateInvsumFreight ",appException);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}

			return iRowsAffected;
		}



		/// <summary>
		/// Gets system code values from the Core_System_Code table for the specified applicaitonid, user Culture and the codeid
		/// </summary>
		/// <param name="strAppID">Applicaitonid for the current application</param>
		/// <param name="strUserCulture">User culture string for the current user</param>
		/// <param name="strCode">System code id</param>
		/// <param name="strValue">System value id</param>
		/// <param name="valueType">one of CodeValueType enum to specify whether numeric or string values are tobe returned.</param>
		/// <returns>ArrayList containing objects of type SystemCode</returns>
		public static String GetCodeTextByValue(String strAppID, String strUserCulture, String strCode,String strValue, CodeValueType valueType)
		{
			String strCodeText = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Utility","GetCodeTextByValue","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[InvoiceManagementMgrDAL::GetCodeTextByValue()] DbConnection is null","ERROR");
				return strCodeText;
			}

			String strSQLQuery = "select code_text,code_str_value from Core_System_Code where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and culture = '"+strUserCulture+"' and codeid = '"+strCode+"'";
			strSQLQuery += " and code_str_value = '"+strValue+"'"; 
			
			DataSet dsSystemCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			int cnt = dsSystemCode.Tables[0].Rows.Count;

			if(cnt > 0)
			{

				DataRow drEach = dsSystemCode.Tables[0].Rows[0];
					
				if(!drEach["code_text"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strCodeText = (String) drEach["code_text"];
				}


			}
			else 
			{
				strCodeText = "";
			}

			return  strCodeText;
		}
	

		public static String GetValueByCodeText(String strAppID, String strUserCulture, String strCode,String strCodeText, CodeValueType valueType)
		{
			String strValue = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Utility","GetCodeTextByValue","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[InvoiceManagementMgrDAL::GetCodeTextByValue()] DbConnection is null","ERROR");
				return strCodeText;
			}

			String strSQLQuery = "select code_text,code_str_value from Core_System_Code where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and culture = '"+strUserCulture+"' and codeid = '"+strCode+"'";
			strSQLQuery += " and code_text = N'"+strCodeText+"'"; 
			
			DataSet dsSystemCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			int cnt = dsSystemCode.Tables[0].Rows.Count;

			if(cnt > 0)
			{

				DataRow drEach = dsSystemCode.Tables[0].Rows[0];
					
				if(!drEach["code_str_value"].GetType().Equals(System.Type.GetType("System.DBNull")))
				{
					strValue = (String) drEach["code_str_value"];
				}


			}
			else 
			{
				strValue = "";
			}

			return  strValue;
		}
	
		#region Comment by Sompote 2010-05-11 (Fix Database Lock)
		/*
		public static DataSet GetDomesticSHP(String strAppID, String strEnterpriseID, String consgnNo)
		{
			DataSet dsSHP = new DataSet();

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Utility","GetCodeTextByValue","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[InvoiceManagementMgrDAL::GetCodeTextByValue()] DbConnection is null","ERROR");
				return dsSHP;
			}

			String strSQLQuery = "select * From Shipment where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and consignment_no = '"+consgnNo+"'";

			
			dsSHP = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			return  dsSHP;
		}
	
		*/
		#endregion
		public static DataSet GetDomesticSHP(String strAppID, String strEnterpriseID, String consgnNo)
		{
			DataSet dsSHP = new DataSet();

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID);

			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetDomesticSHP","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[InvoiceManagementMgrDAL::GetDomesticSHP()] DbConnection is null","ERROR");
				return dsSHP;
			}
			return  GetDomesticSHP( strAppID,  strEnterpriseID,  consgnNo,dbCon);
		}
	
		public static DataSet GetDomesticSHP(String strAppID, String strEnterpriseID, String consgnNo,DbConnection dbCon)
		{
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetDomesticSHP","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetDomesticSHP","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetDomesticSHP","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	

			return  GetDomesticSHP( strAppID,  strEnterpriseID,  consgnNo, dbCon, dbCmd);
		}
	
		
		public static DataSet GetDomesticSHP(String strAppID, String strEnterpriseID, String consgnNo,DbConnection dbCon,IDbCommand dbCmd)
		{
			DataSet dsSHP = new DataSet();
			

			String strSQLQuery = "select * From Shipment where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and consignment_no = '"+consgnNo+"'";

			dbCmd.CommandText = strSQLQuery;
			//dsSHP = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			dsSHP = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			return  dsSHP;
		}
	

		#region "2009-026 by GwanG on 02Apr09"
		public static DataTable GetInvoiceDtlToXls(String strAppID, String strEnterpriseID, String strInvoiceNo)
		{
			DataSet dsXls = new DataSet();
			DataTable dtXls = new DataTable();

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Utility","GetCodeTextByValue","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[InvoiceManagementMgrDAL::GetCodeTextByValue()] DbConnection is null","ERROR");
				return dtXls;
			}

			String strSQLQuery = "select * From v_Invoice_Detail where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and invoice_no = '"+strInvoiceNo+"'";

			
			dsXls = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			if(dsXls.Tables[0].Rows.Count > 0)
			{
				dtXls = dsXls.Tables[0].Copy();
			}

			return  dtXls;
		}
		#endregion
		
		#region "Copy as a backup"
		public static int CancelInvoiceCreditDebit_Bk(String strAppID
			, String strEnterpriseID
			, String strUserID
			, String InvoiceNo
			, DataSet dsInvoice
			, DataSet dsShipment
			, String CancelType
			, String Reason)
		{
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			IDbCommand dbCmd = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			dbCmd = dbCon.CreateCommand("", CommandType.Text);
			dbCmd.Connection = conApp;			


			//Get Data Credit
			DataSet dsCD = new DataSet();
			CreditDebitMgrDAL.GetCreditNoteDS(ref dsCD, strAppID , strEnterpriseID , "", InvoiceNo,dbCon,dbCmd);
			DataSet dsCDDetail = CreditDebitMgrDAL.GetCreditDetailDS(strAppID , strEnterpriseID , "", InvoiceNo,dbCon,dbCmd);
			foreach(DataRow dr in dsCD.Tables[0].Rows)
			{
				//Validate Current Month
				if(!CreditDebitMgrDAL.ValidateCurrentMonth(strAppID , strEnterpriseID,(DateTime)dr["update_date"]))
					return -100;
			}
			//Get Data Debit
			DataSet dsDB = new DataSet();
			CreditDebitMgrDAL.GetDebitNoteDS(ref dsDB, strAppID , strEnterpriseID , "", InvoiceNo,dbCon,dbCmd);
			DataSet dsDBDetail = CreditDebitMgrDAL.GetDebitDetailDS(strAppID , strEnterpriseID , "", InvoiceNo,dbCon,dbCmd);
			foreach(DataRow dr in dsDB.Tables[0].Rows)
			{
				//Validate Current Month
				if(!CreditDebitMgrDAL.ValidateCurrentMonth(strAppID , strEnterpriseID,(DateTime)dr["update_date"]))
				{					
					return -100;
				}
			}		

			//Validate this invoice has CN/DN related
			//(1)Yes show alert message and cancel CN/DN
			//(2)No cancel only invoice

			//(1) ValidateCurrentMonth
			//(1.1) True = current month do next
			//(1.2) False = less then or more then current month will alert message and not allow do next process


			//Begin transaction
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{
				//Update status of each CN and DN
				//CD
				if(dsCD.Tables[0].Rows.Count > 0)
				{				
					foreach(DataRow dr in dsCD.Tables[0].Rows)
					{
						dr["status"] = "C";
						dr["status_update_by"] = dr["update_by"] = strUserID;
						dr["status_update_date"] = dr["update_date"] = DateTime.Now;								
					}				
					CreditDebitMgrDAL.UpdateCreditNoteDS(strAppID , strEnterpriseID  , dsCD ,dsCDDetail ,transactionApp ,conApp ,dbCon);
				}
				//DB
				if(dsDB.Tables[0].Rows.Count > 0)
				{					
					foreach(DataRow dr in dsDB.Tables[0].Rows)
					{
						dr["status"] = "C";
						dr["status_update_by"] = dr["update_by"] = strUserID;
						dr["status_update_date"] = dr["update_date"] = DateTime.Now;			
					}					
					CreditDebitMgrDAL.UpdateDebitNoteDS(strAppID , strEnterpriseID  , dsDB ,dsDBDetail,transactionApp ,conApp ,dbCon);
				}						
				//Update Shipment for each consignment no.
			
				//Cancel invoice
				CancelInvoice(strAppID
					, strEnterpriseID
					, dsInvoice
					, dsShipment
					, CancelType 
					, Reason,transactionApp,conApp ,dbCon);

				//Test Roll Back
				//throw new Exception();

				//Commit Transaction
				transactionApp.Commit();
			}//Any error execption will rollback all transaction
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting audit data ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting audit data ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;
		}		
		#endregion
		
		public static int CancelInvoiceCreditDebit(String strAppID
													, String strEnterpriseID
													, String strUserID
													, String InvoiceNo
													, DataSet dsInvoice
													, DataSet dsShipment
													, String CancelType
													, String Reason)
		{
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			IDbCommand dbCmd = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			dbCmd = dbCon.CreateCommand("", CommandType.Text);
			dbCmd.Connection = conApp;			

			//Begin transaction
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{	
			//Update Shipment for each consignment no.			
			//Cancel invoice
			CancelInvoice(strAppID
						, strEnterpriseID
						, dsInvoice
						, dsShipment
						, CancelType 
						, Reason,transactionApp,conApp ,dbCon);

			//Test Roll Back
			//throw new Exception();

			//Commit Transaction
			transactionApp.Commit();
			}//Any error execption will rollback all transaction
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting audit data ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("InvoiceManagementMgrDAL","InvoiceManagementMgrDAL","CancelInvoiceCreditDebit","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting audit data ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;
		}

		
		public static DataSet GetEmptyInvoiceForImport()
		{
			DataTable dtInvoice =  new DataTable();
			
			dtInvoice.Columns.Add(new DataColumn("recordid",typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("Invoice_Number",typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("Batch_No",typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("Payment_Date",typeof(DateTime)));
			dtInvoice.Columns.Add(new DataColumn("Amount_Paid",typeof(decimal))); 

			DataRow drEach = dtInvoice.NewRow();

			dtInvoice.Rows.Add(drEach);
			DataSet dsInvoice = new DataSet();
			dsInvoice.Tables.Add(dtInvoice);
			return  dsInvoice;	

		}
		public static DataSet GetEmptyImportValidation()
		{
			DataTable dtInvoice =  new DataTable();
			
			dtInvoice.Columns.Add(new DataColumn("recordid",typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("Invoice_Number",typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("Batch_No",typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("Payment_Date",typeof(string)));
			dtInvoice.Columns.Add(new DataColumn("Amount_Paid",typeof(string))); 
			dtInvoice.Columns.Add(new DataColumn("error",typeof(string)));

			DataRow drEach = dtInvoice.NewRow();

			dtInvoice.Rows.Add(drEach);
			DataSet dsInvoice = new DataSet();
			dsInvoice.Tables.Add(dtInvoice);
			return  dsInvoice;	
		}

		public static int AddInvoice_Payment(String strAppID, String strEnterpriseID, DataRow[] drInvoicePayment)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","AddInvoice_Payment","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			foreach(DataRow drEach in drInvoicePayment)
			{
////				StringBuilder strBuilder = new StringBuilder();
////				strBuilder.Append("Insert into Invoice_Payment(invoice_no,batch_no,payment_date,amt_paid)");
////				strBuilder.Append("values('");
////
////				String invoiceNo = (String)drEach["invoice_no"];
////				strBuilder.Append(Utility.ReplaceSingleQuote(invoiceNo)+"','");
////				if(Utility.IsNotDBNull(drEach["batch_no"]) && drEach["batch_no"].ToString()!="")
////				{
////					String batchNo = (String)drEach["batch_no"];
////					strBuilder.Append(Utility.ReplaceSingleQuote(batchNo)+"','");
////				}
////				DateTime dtPaymentDate = (DateTime)drEach["payment_date"];
////				strBuilder.Append(dtPaymentDate+"',");
////				decimal amtPaid = (decimal)drEach["amt_paid"];
////				strBuilder.Append(amtPaid);
////				strBuilder.Append(")");
////
////				String strSQLQuery = strBuilder.ToString();
////				IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
////
////				//Add data to Customer_creditused
////				CustomerCreditused ocusused = new CustomerCreditused();
////				ocusused.enterpriseid = strEnterpriseID;
////				ocusused.applicationid = strAppID;
////				ocusused.invoice_no = (String)drEach["invoice_no"];
////				ocusused.amount= amtPaid*-1;
////				ocusused.cusid = ocusused.getCustIDfromInvoice();
////				
////				ocusused.updateCreditused();
///
				DataSet dsResult=null;
				int dTotalAmnt=0;
				ArrayList arrParam = new ArrayList();
				arrParam.Add(new SqlParameter("@application_id",strAppID));
				arrParam.Add(new SqlParameter("@enterprise_id",strEnterpriseID));
				arrParam.Add(new SqlParameter("@invoice_no",drEach["invoice_no"]));
				arrParam.Add(new SqlParameter("@invoice_status",DBNull.Value));
				arrParam.Add(new SqlParameter("@amount_paid",drEach["amt_paid"]));
				arrParam.Add(new SqlParameter("@batch_no",drEach["batch_no"]));
				arrParam.Add(new SqlParameter("@user",DBNull.Value));
				arrParam.Add(new SqlParameter("@promised_payment_date",DBNull.Value));
				arrParam.Add(new SqlParameter("@paid_date",drEach["payment_date"]));
				arrParam.Add(new SqlParameter("@internal_due_date",DBNull.Value));
				arrParam.Add(new SqlParameter("@actual_bill_placement_date",DBNull.Value));
				
				dsResult = (DataSet)dbCon.ExecuteProcedure("InvoicePaymentUpdate_Update",arrParam,ReturnType.DataSetType);
				if(dsResult.Tables.Count>0)
				{
					dTotalAmnt=Convert.ToInt16(dsResult.Tables[dsResult.Tables.Count-1].Rows[0]["ReturnValue"].ToString());
				}
				else
				{
					dTotalAmnt=1;
				}
				
				try
				{
					iRowsAffected = dTotalAmnt;//dbCon.ExecuteNonQuery(dbCmd);
					Logger.LogTraceInfo("InvoiceManagementMgrDAL","AddInvoice_Payment","SDM001",iRowsAffected+" rows inserted in to Invoice_Payment table");
				}
				catch(ApplicationException appException)
				{
					Logger.LogTraceError("RBACManager","AddInvoice_Payment","SDM001","Error During Insert "+ appException.Message.ToString());
					throw new ApplicationException("Error inserting Invoice Payment Code ",appException);
				}
				
			}
			return iRowsAffected;
		}

		public static DataSet GetInvoice_Payment(String appID, String enterpriseID, String invoiceNo)
		{
			IDbCommand dbCmd = null;	
			DataSet ds = new DataSet();
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select sum(amt_paid) as amount_paid, max(payment_date) as payment_date from Invoice_Payment ");
			strBuilder.Append("where invoice_no = '");
			strBuilder.Append(invoiceNo);
			strBuilder.Append("' ");
			strBuilder.Append("group by invoice_no");

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetInvoice_Payment","GetInvoice_Payment","DbConnection object is null!!");
				return ds;
			}
			
			
			//sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "InvoiceManageTable");
			dbCmd = dbCon.CreateCommand(strBuilder.ToString() , CommandType.Text);
			ds = (DataSet)dbCon.ExecuteQuery(dbCmd,ReturnType.DataSetType);
			return  ds;
		}

		public static DataSet GetDebit_Amount(String appID, String enterpriseID, String invoiceNo)
		{
			IDbCommand dbCmd = null;	
			DataSet ds = new DataSet();
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select dn.invoice_no, sum(debit_amt) as debit_amount from debit_note dn ");
			strBuilder.Append("inner join debit_note_detail as dnd on dn.invoice_no = dnd.invoice_no ");
			strBuilder.Append("and dn.debit_no = dnd.debit_no and dn.applicationid = dnd.applicationid ");
			strBuilder.Append("and dn.enterpriseid = dnd.enterpriseid ");
			strBuilder.Append("where dn.invoice_no = '");
			strBuilder.Append(invoiceNo);
			strBuilder.Append("' and dn.status = 'A'" );
			strBuilder.Append("group by dn.invoice_no");

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetDebit_Amount","GetDebit_Amount","DbConnection object is null!!");
				return ds;
			}
			
			
			//sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "InvoiceManageTable");
			dbCmd = dbCon.CreateCommand(strBuilder.ToString() , CommandType.Text);
			ds = (DataSet)dbCon.ExecuteQuery(dbCmd,ReturnType.DataSetType);
			return  ds;
		}

		public static DataSet GetCredit_Amount(String appID, String enterpriseID, String invoiceNo)
		{
			IDbCommand dbCmd = null;	
			DataSet ds = new DataSet();
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select cn.invoice_no, sum(credit_amt) as credit_amount from credit_note cn ");
			strBuilder.Append("inner join credit_note_detail as cnd on cn.invoice_no = cnd.invoice_no ");
			strBuilder.Append("and cn.credit_no = cnd.credit_no and cn.applicationid = cnd.applicationid ");
			strBuilder.Append("and cn.enterpriseid = cnd.enterpriseid ");
			strBuilder.Append("where cn.invoice_no = '");
			strBuilder.Append(invoiceNo);
			strBuilder.Append("' and cn.status = 'A'");
			strBuilder.Append("group by cn.invoice_no");

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetCredit_Amount","GetCredit_Amount","DbConnection object is null!!");
				return ds;
			}
			
			
			//sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "InvoiceManageTable");
			dbCmd = dbCon.CreateCommand(strBuilder.ToString() , CommandType.Text);
			ds = (DataSet)dbCon.ExecuteQuery(dbCmd,ReturnType.DataSetType);
			return  ds;
		}
		public static DataSet GetLinkedCredit_Amount(String appID, String enterpriseID, String invoiceNo)
		{
			IDbCommand dbCmd = null;	
			DataSet ds = new DataSet();
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select cn.link_to_invoice, sum(credit_amt) as credit_amount from credit_note cn ");
			strBuilder.Append("inner join credit_note_detail as cnd on cn.link_to_invoice = cnd.invoice_no ");
			strBuilder.Append("and cn.credit_no = cnd.credit_no and cn.applicationid = cnd.applicationid ");
			strBuilder.Append("and cn.enterpriseid = cnd.enterpriseid ");
			strBuilder.Append("where cn.link_to_invoice = '");
			strBuilder.Append(invoiceNo);
			strBuilder.Append("' and cn.status = 'A'");
			strBuilder.Append("group by cn.link_to_invoice");

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetLinkedCredit_Amount","GetLinkedCredit_Amount","DbConnection object is null!!");
				return ds;
			}

			//sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "InvoiceManageTable");
			dbCmd = dbCon.CreateCommand(strBuilder.ToString() , CommandType.Text);
			ds = (DataSet)dbCon.ExecuteQuery(dbCmd,ReturnType.DataSetType);
			return  ds;
		}
		public static DataSet GetInvoiceStatus(String appID ,String enterpriseID, String statusID, String uiCulture)
		{
			IDbCommand dbCmd = null;	
			DataSet ds = new DataSet();
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select * from dbo.Core_System_Code with (nolock) ");			
			strBuilder.Append("where applicationid = '");
			strBuilder.Append(appID);
			strBuilder.Append("' ");
			strBuilder.Append(" and culture = '");
			strBuilder.Append(uiCulture);
			strBuilder.Append("' ");
			strBuilder.Append(" and code_str_value = '");
			strBuilder.Append(statusID);
			strBuilder.Append("' ");			
			strBuilder.Append("and codeid = 'invoice_status' ");
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetLinkedCredit_Amount","GetLinkedCredit_Amount","DbConnection object is null!!");
				return ds;
			}
					
			//sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "InvoiceManageTable");
			dbCmd = dbCon.CreateCommand(strBuilder.ToString() , CommandType.Text);
			ds = (DataSet)dbCon.ExecuteQuery(dbCmd,ReturnType.DataSetType);
			return  ds;
		}
	

		public static bool IsPlacedInvoiceCanCancelled(String appID, String enterpriseID, String invoiceNo)
		{	
			int records = 0;
			
			String strBuilder;
			strBuilder = "select count(*) from credit_note where status <> 'C' and invoice_no='"+invoiceNo+"'";
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetCredit_Amount","GetCredit_Amount","DbConnection object is null!!");
			}
			
			records = (int)dbCon.ExecuteScalar(strBuilder);

			if(records>0)
				return false;

			strBuilder = "select count(*) from debit_note where status <> 'C' and invoice_no='"+invoiceNo+"'";
			records = (int)dbCon.ExecuteScalar(strBuilder);
			if(records>0)
				return false;

			strBuilder = "select sum(amt_paid) as amount_paid from Invoice_Payment where invoice_no='"+invoiceNo+"'";
			object ob = dbCon.ExecuteScalar(strBuilder);
			if(ob!=System.DBNull.Value)
			{   if(Double.Parse(ob.ToString())!=Double.Parse("0.00"))
				 return false;
			}

			return  true;
		}

		public static DataSet GetCSVExport(string appID, string enterpriseID, string excludeCustomer, string invoiceNo, string exported_by)
		{
			IDbCommand dbCmd = null;	
			DataSet ds = new DataSet();
			String sqlText = "EXEC dbo.[ExportInvoiceToAccPac] '" + enterpriseID + "', '" + appID + "', '" + invoiceNo + "', '" + exported_by + "', '" + excludeCustomer + "'";
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","GetLinkedCredit_Amount","GetLinkedCredit_Amount","DbConnection object is null!!");
				return ds;
			}
					
			//sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "InvoiceManageTable");
			dbCmd = dbCon.CreateCommand(sqlText , CommandType.Text);
			ds = (DataSet)dbCon.ExecuteQuery(dbCmd,ReturnType.DataSetType);
			return  ds;
		}


		public static bool UpdateExported(string strAppID, string strEnterpriseID, string InvoiceNumber, string exported_by)
		{
			bool result = false;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			IDbCommand dbCmd = null;

			#region Connection
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteDomesticShp","ERR004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			#endregion

			try
			{
				strBuild = new StringBuilder();
				strBuild.Append("DECLARE @date	DATETIME SET @date = GETDATE() UPDATE dbo.Invoice ");
				strBuild.Append("SET exported_date = @date, ");
				strBuild.Append("    exported_by = '" + exported_by + "' ");
				strBuild.Append("WHERE invoice_no = '" + InvoiceNumber + "' ");
				
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dbCmd.Connection = conApp;
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				result = true;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","UpdateInvsumFreight","UpdateInvsumFreight","UpdateInvsumFreight Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error UpdateInvsumFreight ",appException);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}

			return result;
		}

		public static bool UpdateInvoiceAmt(string strAppID, string strEnterpriseID, string InvoiceNumber, string exported_by)
		{
			bool result = false;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			IDbCommand dbCmd = null;

			#region Connection
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("InvoiceManagementMgrDAL","AdjustInvoice","AdjustInvoice","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","DeleteDomesticShp","ERR004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			#endregion

			try
			{
				strBuild = new StringBuilder();
				strBuild.Append("update dbo.Shipment ");
				strBuild.Append("set invoice_amt = isnull(total_rated_amount, 0) + ");
				strBuild.Append("isnull(tax_on_rated_amount, 0) + ");
				strBuild.Append("isnull(export_freight_charge, 0) + ");
				strBuild.Append("isnull(customs_fees, 0) +  ");
				strBuild.Append("isnull(freight_collect_charges, 0) ");
				strBuild.Append("WHERE invoice_no = '" + InvoiceNumber + "' ");
				
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dbCmd.Connection = conApp;
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				result = true;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("InvoiceManagementMgrDAL","UpdateInvsumFreight","UpdateInvsumFreight","UpdateInvsumFreight Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error UpdateInvsumFreight ",appException);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}

			return result;
		}
	}

}