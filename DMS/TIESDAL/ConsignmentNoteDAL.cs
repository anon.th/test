using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;


//namespace TIESDAL
namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for ConsignmentNote.
	/// </summary>
	public class ConsignmentNoteDAL
	{
		public ConsignmentNoteDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static DataSet GetInitConData(String strAppID,String strEnterpriseID, String CustID)
		{
			string strSQL = "";
			DataSet DS = new DataSet();

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetInitConData", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}
			try
			{
				strSQL = "SELECT * FROM CUSTOMER WHERE CUSTID = '" + CustID + "'";
//				DataSet DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
				DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
				return DS;
			}
			catch
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetInitConData", "Select command failed");
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static DataSet GetCustSender(String strAppID,String strEnterpriseID, String CustID)
		{
			string strSQL = "";
			DataSet DS = new DataSet();

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetCustSender", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}
			try
			{
				strSQL = "SELECT LTRIM(RTRIM(snd_rec_name)) as snd_rec_name, contact_person, address1, address2, zipcode, telephone ";
				strSQL += " FROM CUSTOMER_SND_REC WHERE CUSTID = '" + CustID + "' ";
				strSQL += " ORDER BY snd_rec_name ";
				//				DataSet DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
				DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
				return DS;
			}
			catch
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetCustSender", "Select command failed");
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				DS.Dispose();
			}
		}		

		
		public static int SaveCustomerSndByUser(String strAppID, String strEnterpriseID, String strUserId, String strCustId, String strSndRecName)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			StringBuilder strBuild = new StringBuilder();
			int iRowsAffected = 0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SaveCustomerSndByUser", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null);
			}

			try
			{
				strBuild = new StringBuilder();

				//Delete dbo.Customer_Snd_ByUser
                strBuild = new StringBuilder();
				strBuild.Append("DELETE FROM [Customer_Snd_ByUser] ");
				strBuild.Append(" WHERE applicationid = '" + strAppID + "' ");
				strBuild.Append(" AND enterpriseid = '" + strEnterpriseID + "' ");
				strBuild.Append(" AND userid = '" + strUserId + "' ");
				strBuild.Append(" AND custid = '" + strCustId + "' ");
 
				dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd); 

 
				//Insert new record to dbo.Customer_Snd_ByUser
				strBuild = new StringBuilder();
				strBuild.Append("INSERT INTO [Customer_Snd_ByUser] ");
				strBuild.Append("(applicationid, enterpriseid, userid, ");
				strBuild.Append("custid, snd_rec_name, upd_date ) ");  
				strBuild.Append("VALUES ");
				strBuild.Append("('" + strAppID + "', '" + strEnterpriseID + "', '" + strUserId + "', ");
				strBuild.Append("N'" + strCustId + "', N'" + strSndRecName + "', '" + DateTime.Now + "')");
 
				dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd); 
			}
			catch(Exception ex)
			{
				throw new ApplicationException("Error Save Default Sender", ex); 
			}
			return iRowsAffected;
		}

		public static DataSet GetCustSenderByUser(String strAppID,String strEnterpriseID, String strCustID, String strUserID)
		{
			string strSQL = "";
			DataSet DS = new DataSet();

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetCustSenderByUser", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}
			try
			{
				strSQL = "SELECT userid, custid, snd_rec_name, upd_date ";
				strSQL += " FROM Customer_Snd_ByUser ";
				strSQL += " WHERE applicationid = '" + strAppID + "' ";
				strSQL += " AND enterpriseid = '" + strEnterpriseID + "' ";
				strSQL += " AND userid = N'" + strUserID + "' ";
				strSQL += " AND custid = N'" + strCustID + "' ";

				//				DataSet DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
				DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
				return DS;
			}
			catch
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetCustSenderByUser", "Select command failed");
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				DS.Dispose();
			}
		}
		
		public static DataSet GetCustSenderBySndNa(String strAppID,String strEnterpriseID, String CustID, String SndNA)
		{
			string strSQL = "";
			DataSet DS = new DataSet();

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetCustSender", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}
			try
			{
				strSQL = "SELECT S.state_name, C.contact_person, C.address1, C.address2, C.zipcode, C.telephone ";
				strSQL += " FROM customer_snd_rec C, zipcode Z, state S";
				strSQL += " WHERE C.applicationid = Z.applicationid AND C.enterpriseid = Z.enterpriseid ";
				strSQL += " AND C.zipcode = Z.zipcode AND Z.applicationid = S.applicationid ";
				strSQL += " AND Z.enterpriseid = S.enterpriseid AND Z.state_code = S.state_code ";
				strSQL += " AND C.snd_rec_name = N'" + SndNA + "' ";
				strSQL += " AND C.custid = '" + CustID + "' ";

				//				DataSet DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
				DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
				return DS;
			}
			catch
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetCustSender", "Select command failed");
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				DS.Dispose();
			}
		}		

		public static DataSet GetParameterConsignment(String strAppID,String strEnterpriseID, String PayerID)
		{
			string strSQL = "";
			DataSet DS = new DataSet();

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetParameterConsignment", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}
			try
			{
				strSQL = "SELECT * FROM tb_Parameter_ConsignmentNote WHERE PayerID = '" + PayerID + "'";
				DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
				return DS;
			}
			catch
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetParameterConsignment", "Select command failed");
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static DataSet SearchConsignmentNote(String appID, String enterpriseID, String PayerID, String ConsignmentNo, String CustomerCode)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
  
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchConsignmentNote", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT N.CUSTOMERCODE, N.CONSIGNMENT_NO, N.TELEPHONE, N.CUSTOMERCODE, R.REFERENCE_NAME, R.ADDRESS1 AS ADDRESS, ");
			strQry = strQry.Append("R.ADDRESS2 AS STATE, R.ZIPCODE, R.TELEPHONE, N.SENDER_NAME, N.FLAG_DELETE ");
			strQry = strQry.Append("FROM TB_CONSIGNMENTNOTE N ");
//			strQry = strQry.Append("LEFT OUTER JOIN CUSTOMER C ");
			strQry = strQry.Append("INNER JOIN CUSTOMER C ");
			strQry = strQry.Append("ON N.PAYERID = C.CUSTID  AND N.ENTERPRISEID = C.ENTERPRISEID "); 
//			strQry = strQry.Append("LEFT OUTER JOIN REFERENCES2 R ");
			strQry = strQry.Append("INNER JOIN REFERENCES2 R ");
			strQry = strQry.Append("ON N.CUSTOMERCODE = R.CUSTOMERCODE  AND N.ENTERPRISEID = R.ENTERPRISEID ");
			strQry = strQry.Append("AND R.PAYERID = N.PAYERID ");
			strQry = strQry.Append("WHERE (N.FLAG_EXPORT IS NULL OR N.FLAG_EXPORT <> 'EX' )");
			strQry = strQry.Append("AND N.FLAG_DELETE IS NULL ");
			strQry = strQry.Append("AND N.PAYERID = '" + PayerID + "' ");
			if (ConsignmentNo != "")
			{
				strQry = strQry.Append(" AND N.CONSIGNMENT_NO like UPPER('" + ConsignmentNo + "%') ");
			}
			if (CustomerCode != "")
			{
				strQry = strQry.Append(" AND N.CUSTOMERCODE LIKE UPPER('" + CustomerCode + "%') ");
			}
			strQry = strQry.Append(" ORDER BY CONSIGNMENT_NO DESC "); 
 
			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);     
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchConsignmentNote", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static DataSet SearchConsignmentNoteDetail(String appID, String enterpriseID, String PayerID, String ConsignmentNo)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
  
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchConsignmentNoteDetail", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
//			strQry = strQry.Append("SELECT C.*, R.ZIPCODE, Z.ZONE_CODE, R.CUSTOMERCODE, R.REFERENCE_NAME,R.Contactperson, R.ADDRESS1, R.ADDRESS2, T.STATE_NAME, S.SERVICE_DESCRIPTION ");
//			strQry = strQry.Append("FROM TB_CONSIGNMENTNOTE C LEFT OUTER JOIN [REFERENCES2] R "); 
//			strQry = strQry.Append("ON C.CUSTOMERCODE = R.CUSTOMERCODE ");
//			strQry = strQry.Append("LEFT OUTER JOIN ZIPCODE Z ");
//			strQry = strQry.Append("ON R.ZIPCODE = Z.ZIPCODE ");
//			strQry = strQry.Append("LEFT OUTER JOIN STATE T ");
//			strQry = strQry.Append("ON T.STATE_CODE = Z.STATE_CODE "); 
//			strQry = strQry.Append("LEFT OUTER JOIN SERVICE S ");
//			strQry = strQry.Append("ON C.SERVICETYPE = S.SERVICE_CODE ");
//			strQry = strQry.Append("WHERE C.FLAG_EXPORT IS NULL ");
//			strQry = strQry.Append("AND C.FLAG_DELETE IS NULL ");  
//			strQry = strQry.Append("AND C.PAYERID = '" + PayerID + "' ");

			strQry = strQry.Append("SELECT C.*, R.ZIPCODE, Z.ZONE_CODE, R.CUSTOMERCODE, R.REFERENCE_NAME, ");
			strQry = strQry.Append(" R.Contactperson, R.ADDRESS1, R.ADDRESS2, T.STATE_NAME, S.SERVICE_DESCRIPTION ");
			strQry = strQry.Append(" FROM TB_CONSIGNMENTNOTE C INNER JOIN [REFERENCES2] R "); 
			strQry = strQry.Append(" ON C.CUSTOMERCODE = R.CUSTOMERCODE  AND C.ENTERPRISEID = R.ENTERPRISEID ");
			strQry = strQry.Append(" AND R.PAYERID = C.PAYERID ");
			strQry = strQry.Append(" INNER JOIN ZIPCODE Z  ON R.ZIPCODE = Z.ZIPCODE ");
			strQry = strQry.Append(" INNER JOIN STATE T  ON T.STATE_CODE = Z.STATE_CODE "); 
			strQry = strQry.Append(" INNER JOIN SERVICE S  ON C.SERVICETYPE = S.SERVICE_CODE ");
			strQry = strQry.Append(" WHERE C.FLAG_EXPORT IS NULL ");
			strQry = strQry.Append(" AND C.FLAG_DELETE IS NULL ");  
			strQry = strQry.Append(" AND C.PAYERID = '" + PayerID + "' ");

			if (ConsignmentNo != "")
			{
				strQry = strQry.Append("AND C.CONSIGNMENT_NO = '" + ConsignmentNo + "' ");		
			}

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType); 
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchConsignmentNoteDetail", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static DataSet SearchPackage(String appID, String enterpriseID, String PayerID, String ConsignmentNo, String Seq, String ConsignmentNoGroup)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
  
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchPackage", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT C.CONSIGNMENT_NO, C.PAYERID, C.SEQ_NO, C.CUSTOMERCODE, C.BOXTYPE, "); 
			strQry = strQry.Append("(CASE WHEN C.BoxType = 'C' THEN 'CUSTOMIZE' ");
			strQry = strQry.Append("      WHEN C.BoxType = 'S' THEN C.BOXSIZE END) AS BOX_SIZE,"); 
			strQry = strQry.Append("(CASE WHEN C.BoxType = 'C' THEN LTRIM(SUBSTRING([DESCRIPTION], 1, 3)) + ' x ' + LTRIM(SUBSTRING([DESCRIPTION], 7, 3)) + ' x ' + LTRIM(SUBSTRING([DESCRIPTION], 13, 3)) ");
			strQry = strQry.Append("      WHEN C.BoxType = 'S' THEN CONVERT(VARCHAR,B.BOX_WIDE) + ' x ' + CONVERT(VARCHAR,B.BOX_LENGTH) + ' x ' + CONVERT(VARCHAR,B.BOX_HEIGHT) END) AS BOX_DESC, ");	
			strQry = strQry.Append("C.KILO, C.QTY, (C.KILO * C.QTY) AS TOTAL_KG, ");
			strQry = strQry.Append("(CASE WHEN C.BoxType = 'C' THEN LTRIM(SUBSTRING([DESCRIPTION], 1, 3)) ");
			strQry = strQry.Append("      WHEN C.BoxType = 'S' THEN CONVERT(VARCHAR,B.BOX_WIDE) END) AS WIDTH, ");
			strQry = strQry.Append("(CASE WHEN C.BoxType = 'C' THEN LTRIM(SUBSTRING([DESCRIPTION], 7, 3)) ");
			strQry = strQry.Append("      WHEN C.BoxType = 'S' THEN CONVERT(VARCHAR,B.BOX_LENGTH) END) AS LENGTH, ");
			strQry = strQry.Append("(CASE WHEN C.BoxType = 'C' THEN LTRIM(SUBSTRING([DESCRIPTION], 13, 3)) ");
			strQry = strQry.Append("      WHEN C.BoxType = 'S' THEN CONVERT(VARCHAR,B.BOX_HEIGHT)  END) AS HEIGTH, ");
			strQry = strQry.Append("C.CONSIGNMENTDATE, C.FLAG_EXPORT ");
			strQry = strQry.Append("FROM tb_ConsignmentNoteDetail C LEFT JOIN TB_BOXSIZE B ");
			strQry = strQry.Append("ON (C.BOXSIZE = B.BOX_SIZEID and C.PayerID = B.PayerID) ");
			strQry = strQry.Append("WHERE C.FLAG_DELETE IS NULL "); 
			strQry = strQry.Append("AND C.PayerID = '" + PayerID + "' "); 
			strQry = strQry.Append(" AND C.flag_Export IS NULL ");

			if (ConsignmentNo != "")
			{
				strQry = strQry.Append(" AND C.CONSIGNMENT_NO = '" + ConsignmentNo + "'");				
			}
			if (Seq != "")
			{
				strQry = strQry.Append(" AND C.SEQ_NO = " + Seq );	
			}

			if (ConsignmentNoGroup != "")
			{
				strQry = strQry.Append(" AND (C.CONSIGNMENT_NO IN " + ConsignmentNoGroup + ") ");		
			}

			strQry = strQry.Append(" ORDER BY C.SEQ_NO");
 
			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;     
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchPackage", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static DataSet SearchCompanny(String appID, String enterpriseID, String PayerID, String CustomerCode, String CompanyName, String Telephone)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
  
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchCompany", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT R.*, S.STATE_NAME, Z.ZONE_CODE ");
			strQry = strQry.Append("FROM [REFERENCES2] R ");
			strQry = strQry.Append("LEFT OUTER JOIN ZIPCODE Z ON R.ZIPCODE = Z.ZIPCODE ");
			strQry = strQry.Append("LEFT OUTER JOIN STATE S ON Z.STATE_CODE = S.STATE_CODE ");
			strQry = strQry.Append("WHERE R.ENTERPRISEID = '" + enterpriseID + "' ");
			strQry = strQry.Append("AND R.PAYERID = N'" + PayerID + "' ");
			strQry = strQry.Append("AND R.FLAG_DELETE IS NULL ");

			
			if (CustomerCode != "")
			{
				strQry = strQry.Append("AND R.CUSTOMERCODE LIKE UPPER(N'%" + CustomerCode + "%') ");
			}
			if (CompanyName != "")
			{
				strQry = strQry.Append("AND R.REFERENCE_NAME LIKE UPPER(N'%" + CompanyName + "%') ");
			}
			if (Telephone != "")
			{
				strQry = strQry.Append("AND R.TELEPHONE LIKE N'%" + Telephone + "%' ");
			}
			strQry = strQry.Append("ORDER BY R.REFERENCE_NAME");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);     
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchCompany", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		
		public static DataSet SearchConsigneeByCustCode(String appID, String enterpriseID, String PayerID, String CustomerCode)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
  
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchCompannyByCustCode", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT R.*, S.STATE_NAME, Z.ZONE_CODE ");
			strQry = strQry.Append(" FROM [REFERENCES2] R ");
			strQry = strQry.Append(" INNER JOIN ZIPCODE Z ON R.ZIPCODE = Z.ZIPCODE ");
			strQry = strQry.Append(" INNER JOIN STATE S ON Z.STATE_CODE = S.STATE_CODE ");
			strQry = strQry.Append(" WHERE R.ENTERPRISEID = '" + enterpriseID + "' ");
			strQry = strQry.Append(" AND R.PAYERID = N'" + PayerID + "' ");
			strQry = strQry.Append(" AND R.FLAG_DELETE IS NULL ");
			strQry = strQry.Append(" AND R.ENTERPRISEID = '" + enterpriseID + "' ");
			strQry = strQry.Append(" AND R.CUSTOMERCODE = UPPER(N'" + CustomerCode + "') ");
			strQry = strQry.Append(" ORDER BY R.REFERENCE_NAME");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);     
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchCompannyByCustCode", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		
		public static DataSet GetBoxSize(String strAppID, String strEnterpriseID, String BoxSize, String payerID)
		{
			string strSQL = "";
			DataSet DS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID); 
			
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetBoxSize", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null);
			}
			try
			{
				strSQL = "SELECT * FROM TB_BOXSIZE ";
				strSQL += "WHERE PayerID = '"+payerID+"' ";
				if (BoxSize != "")
				{
					strSQL += "AND BOX_SIZEID = '" + BoxSize + "' ";
				}
				strSQL += "ORDER BY BOX_SIZEID";

				DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetBoxSize", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static String GetMaxConsignmentNo(String strAppID, String strEnterpriseID, String PayerID)
		{
			string MaxConsignmentNo = "";
			string strSQL = "";
			DataSet DS = null;
			DataRow DR;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetMaxConsignmentNo", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}
			try
			{
				if (PayerID != "")
				{
					strSQL = "SELECT * FROM TB_RUNNING WHERE CUSTID='" + PayerID + "'";
					DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
					
					if (DS != null && DS.Tables.Count > 0 && DS.Tables[0].Rows.Count > 0)
					{
						DR = DS.Tables[0].Rows[0];
						MaxConsignmentNo = DR["Last_Consignment_Bar"] + "";
					}
				}
				return MaxConsignmentNo; 
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetMaxConsignmentNo", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		
		public static int UpdateRunningCon(String strAppID, String strEnterpriseID, String PayerID, String newConsignmentNo)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd =  null;
			StringBuilder strBuild = new StringBuilder(); 
			int iRowsAffected = 0;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateRunningCon", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}

			try
			{
				if ((PayerID != "") && (newConsignmentNo != ""))
				{
					strBuild = new StringBuilder();
					strBuild.Append("UPDATE TB_RUNNING SET Last_Consignment_Bar = '" + newConsignmentNo + "' WHERE CUSTID=N'" + PayerID + "'");

					dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
					iRowsAffected = dbCon.ExecuteNonQuery(dbCmd); 
				}				
			}
			catch (Exception ex)
			{
				throw new ApplicationException("Error updating UpdateRunningCon ", ex); 
			}
			return iRowsAffected;
		}

		public static String InsertConsignmentNote(String strAppID, String strEnterpriseID, String PayerID, String SenderName, String SenderTel, String CompanyName, String CompanyTel, String CompanyAddress,  String CustomerCode, String CustRef, String RecipionName, String ServiceType, String HCReturn, String InvReturn, String CODAmount, String DeclareValue, String SpecialInstruction, DataTable dtPackage)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd =  null;
			StringBuilder strBuild = new StringBuilder(); 
			int iRowsAffected = 0;
			int jRowsAffected = 0;
			int kRowsAffected = 0;
			String ConsignmentNo = "";
			int newConsignmentNo = 0;
			String ConsignmentDate = "";
			String LastUpdateDate = "";

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNote", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}

			IDbConnection conApp = dbCon.GetConnection(); 
			if (conApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNote", "ERR002", "conApp is null");  
				throw new ApplicationException("Connection to database failed", null); 
			}

			try
			{
				dbCon.OpenConnection(ref conApp); 
			}
			catch(ApplicationException appException)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNote", "ERR003", "Error opening connection : " + appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ", appException); 
			}
			catch(Exception ex)
			{
				if (conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNote", "ERR004", "Error opening connection : " + ex.Message.ToString());
				throw new ApplicationException("Error opening database connection ", ex); 
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp, IsolationLevel.ReadCommitted);
			if (transactionApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNote", "ERR005", "transactionApp is null");
				throw new ApplicationException("Failed begining a application transaction..", null); 
			}


			try
			{
				ConsignmentNo = GetMaxConsignmentNo(strAppID, strEnterpriseID, PayerID);
				ConsignmentDate = DateTime.Now.ToString("yyyyMMdd");
				LastUpdateDate = Utility.DateFormat(strAppID, strEnterpriseID, DateTime.Now, DTFormat.DateTime);  

				if (ConsignmentNo != "")
				{
					strBuild = new StringBuilder(); 

					//insert ConsignmentNote in tb_ConsignmentNote
					strBuild.Append("INSERT INTO TB_CONSIGNMENTNOTE");
					strBuild.Append("(ENTERPRISEID, CONSIGNMENT_NO, PAYERID, TELEPHONE, ");
					strBuild.Append("LASTUSERID, HCRETURN, INVRETURN, CODAMOUNT, LASTUPDATEDATE, ");
					strBuild.Append("CONSIGNMENTDATE, SERVICETYPE, DECLAREVALUE, RECIPION_CONTACT_PERSON, ");
					strBuild.Append("CUST_REF, FLAG_EXPORT, SPECIAL_INSTRUCTION, ADDRESS, COMPANY_NAME, ");
					strBuild.Append("CUSTOMERCODE, SENDER_NAME, SENDER_TEL, FLAG_DELETE ) "); 
					strBuild.Append(" VALUES ");
					strBuild.Append("('" + strEnterpriseID + "','" + ConsignmentNo + "','" + PayerID + "',");
					strBuild.Append("N'" + Utility.ReplaceSingleQuote(CompanyTel) + "',");
					strBuild.Append("'" + PayerID + "'," + HCReturn + "," + InvReturn + "," + CODAmount + "," + LastUpdateDate + ",");
					strBuild.Append("'" + ConsignmentDate + "','" + ServiceType + "'," + DeclareValue + ",");
					strBuild.Append("N'" + Utility.ReplaceSingleQuote(RecipionName) + "',");
					strBuild.Append("N'" + Utility.ReplaceSingleQuote(CustRef) + "',null,");
					strBuild.Append("N'" + Utility.ReplaceSingleQuote(SpecialInstruction) + "',");
					strBuild.Append("N'" + Utility.ReplaceSingleQuote(CompanyAddress) + "',");
					strBuild.Append("N'" + Utility.ReplaceSingleQuote(CompanyName) + "',");
					strBuild.Append("N'" + CustomerCode + "',");
					strBuild.Append("N'" + Utility.ReplaceSingleQuote(SenderName) + "',");
					strBuild.Append("N'" + Utility.ReplaceSingleQuote(SenderTel) + "', null)");

					dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

					Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "InsertConsignmentNote", "INF001", iRowsAffected + " rows insert into tb_consignmentNote table");
 
					//insert Package in tb_ConsignmentNoteDetail
					if (dtPackage != null && dtPackage.Rows.Count > 0 && iRowsAffected > 0 && ConsignmentNo != "")
					{
						foreach(DataRow dr in dtPackage.Rows) 
						{
							strBuild = new StringBuilder();

							strBuild.Append("INSERT INTO TB_CONSIGNMENTNOTEDETAIL ");
							strBuild.Append("(CONSIGNMENT_NO, PAYERID, SEQ_NO, TELEPHONE, ");
							strBuild.Append("BOXTYPE, [DESCRIPTION], BOXSIZE, KILO, QTY, ");
							strBuild.Append("LASTUPDATEDATE, CONSIGNMENTDATE, FLAG_EXPORT, CUSTOMERCODE, FLAG_DELETE ) "); 
							strBuild.Append(" VALUES "); 
							strBuild.Append("('" + ConsignmentNo + "','" + PayerID + "'," + dr["SEQ_NO"] + ",");
							strBuild.Append("N'" + Utility.ReplaceSingleQuote(CompanyTel) + "',");

							//BOXTYPE
							if ((dr["BOXTYPE"] != null) && (!dr["BOXTYPE"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								strBuild.Append("N'" + (String)dr["BOXTYPE"] + "',"); 
							}
							else
							{
								strBuild.Append("NULL,");
							}

							//DESCRIPTION
							if ((dr["BOX_DESC"] != null) && (!dr["BOX_DESC"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								strBuild.Append("N'" + (String)dr["BOX_DESC"] + "',");
							}
							else
							{
								strBuild.Append("NULL,");
							}

							//BOXSIZE
							if ((dr["BOX_SIZE"] != null) && (!dr["BOX_SIZE"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								if ((String)dr["BOX_SIZE"] != "CUSTOMIZE")
								{
									strBuild.Append("N'" + (String)dr["BOX_SIZE"] + "',");
								}
								else
								{
									strBuild.Append("NULL,"); 
								}
							}
							else
							{
								strBuild.Append("NULL,");
							}

							//KILO
							if ((dr["KILO"] != null) && (!dr["KILO"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								strBuild.Append("'" + dr["KILO"].ToString()  + "',"); 
							}
							else
							{
								strBuild.Append("NULL,"); 
							}

							//QTY
							if ((dr["QTY"] != null) && (!dr["QTY"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								strBuild.Append("'" + dr["QTY"].ToString()  + "',"); 
							}	
							else
							{
								strBuild.Append("NULL,"); 
							}

							strBuild.Append(LastUpdateDate + ",");
							strBuild.Append("'" + ConsignmentDate + "',"); 
							strBuild.Append("NULL,"); 
							strBuild.Append("N'" + CustomerCode + "',");
							strBuild.Append("NULL)");

							dbCmd.CommandText = strBuild.ToString();
							jRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							Logger.LogTraceInfo("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNote", "INF002", jRowsAffected + " rows insert into tb_ConsignmentNoteDetail table"); 
						} //end loop
					} //end insert Package

					//update running consignment_no
					if (iRowsAffected > 0 && PayerID != "" && ConsignmentNo != "")
					{
						newConsignmentNo = Convert.ToInt32(ConsignmentNo);
						newConsignmentNo = newConsignmentNo + 1;
						kRowsAffected = UpdateRunningCon(strAppID, strEnterpriseID, PayerID, newConsignmentNo.ToString());
					}

					transactionApp.Commit();
				}
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNote", "INF004", "Database insert transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNote", "ERR006", "Error during application transaction rollback " + rollbackException.Message.ToString()); 
				}

				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNote", "ERR007", "Insert Transaction Failed : " + appException.Message.ToString());
				throw new ApplicationException("1 Error adding ConsignmentNote ", appException);
			}
			catch (Exception ex)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNote", "INF005", "Database insert transaction rolled back.");
				}
				catch (Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNote", "ERR008", "Error during app transaction roll back " + rollbackException.Message.ToString()); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNote", "ERR009", "Insert Transaction Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error adding ConsignmentNote", ex); 
			}
			finally
			{
				if (conApp != null){conApp.Close();}
			}
			return ConsignmentNo;
		}

		public static int UpdateConsignmentNote(String strAppID, String strEnterpriseID, String ConsignmentNo, String PayerID, String SenderName, String SenderTel, String CompanyName, String CompanyTel, String CompanyAddress,  String CustomerCode, String CustRef, String RecipionName, String ServiceType, String HCReturn, String InvReturn, String CODAmount, String DeclareValue, String SpecialInstruction, DataTable dtPackage)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd =  null;
			StringBuilder strBuild = new StringBuilder(); 
			int iRowsAffected = 0;
			int jRowsAffected = 0;
			String ConsignmentDate = "";
			String LastUpdateDate = "";
			String Description = "";
			String Width = "0";
			String Length = "0";
			String Height = "0";

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}

			IDbConnection conApp = dbCon.GetConnection(); 
			if (conApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "ERR002", "conApp is null");  
				throw new ApplicationException("Connection to database failed", null); 
			}

			try
			{
				dbCon.OpenConnection(ref conApp); 
			}
			catch(ApplicationException appException)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "ERR003", "Error opening connection : " + appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ", appException); 
			}
			catch(Exception ex)
			{
				if (conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "ERR004", "Error opening connection : " + ex.Message.ToString());
				throw new ApplicationException("Error opening database connection ", ex); 
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp, IsolationLevel.ReadCommitted);
			if (transactionApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "ERR005", "transactionApp is null");
				throw new ApplicationException("Failed begining a application transaction..", null); 
			}

			try
			{
				ConsignmentDate = DateTime.Now.ToString("yyyyMMdd");
				LastUpdateDate = Utility.DateFormat(strAppID, strEnterpriseID, DateTime.Now, DTFormat.DateTime);  

				if (ConsignmentNo != "")
				{
					strBuild = new StringBuilder(); 

					//update ConsignmentNote in tb_ConsignmentNote
					strBuild.Append("UPDATE TB_CONSIGNMENTNOTE ");
					strBuild.Append(" SET ");
					strBuild.Append("ENTERPRISEID = '" + strEnterpriseID + "', ");
					strBuild.Append("PAYERID = '" + PayerID + "', ");
					strBuild.Append("TELEPHONE = N'" + Utility.ReplaceSingleQuote(CompanyTel) + "', ");
					strBuild.Append("LASTUSERID = '" + PayerID + "', ");
					strBuild.Append("HCRETURN = " + HCReturn + ", ");
					strBuild.Append("INVRETURN = " + InvReturn + ", ");
                    strBuild.Append("CODAMOUNT = " + CODAmount + ", ");
					strBuild.Append("LASTUPDATEDATE = " + LastUpdateDate + ", ");
					strBuild.Append("CONSIGNMENTDATE = '" + ConsignmentDate + "', ");
                    strBuild.Append("SERVICETYPE = '" + ServiceType + "', ");
					strBuild.Append("DECLAREVALUE = " + DeclareValue + ", ");
					strBuild.Append("RECIPION_CONTACT_PERSON = N'" + Utility.ReplaceSingleQuote(RecipionName) + "', ");
					strBuild.Append("CUST_REF = N'" + Utility.ReplaceSingleQuote(CustRef) + "', ");
					strBuild.Append("SPECIAL_INSTRUCTION = N'" + Utility.ReplaceSingleQuote(SpecialInstruction) + "', ");
					strBuild.Append("ADDRESS = N'" + Utility.ReplaceSingleQuote(CompanyAddress) + "', ");
					strBuild.Append("COMPANY_NAME = N'" + Utility.ReplaceSingleQuote(CompanyName) + "', ");
					strBuild.Append("CUSTOMERCODE = N'" + CustomerCode + "', ");
					strBuild.Append("SENDER_NAME = N'" + Utility.ReplaceSingleQuote(SenderName) + "', ");
					strBuild.Append("SENDER_TEL = N'" + Utility.ReplaceSingleQuote(SenderTel) + "' "); 
					strBuild.Append("WHERE CONSIGNMENT_NO = '" + ConsignmentNo + "'");

					dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "UpdateConsignmentNote", "INF001", iRowsAffected + " rows update into tb_consignmentNote table");

					//update Package (delete before insert into tb_ConsignmentNoteDetail)
					if (dtPackage != null && dtPackage.Rows.Count > 0 && iRowsAffected > 0)
					{
						//delete package
						strBuild = new StringBuilder();
						strBuild.Append("DELETE TB_CONSIGNMENTNOTEDETAIL ");
						strBuild.Append("WHERE CONSIGNMENT_NO = '" + ConsignmentNo + "'");

						dbCmd.CommandText = strBuild.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "UpdateConsignmentNote", "INF002", iRowsAffected + " rows delete into tb_consignmentNoteDetail table");
 
						//insert package
						foreach(DataRow dr in dtPackage.Rows) 
						{
							Description = "";
							Width = "0";
							Length = "0";
							Height = "0";

							Width = dr["WIDTH"] + "";
							Length = dr["LENGTH"] + "";
							Height = dr["HEIGTH"] + "";

							strBuild = new StringBuilder();
							strBuild.Append("INSERT INTO TB_CONSIGNMENTNOTEDETAIL ");
							strBuild.Append("(CONSIGNMENT_NO, PAYERID, SEQ_NO, TELEPHONE, ");
							strBuild.Append("BOXTYPE, [DESCRIPTION], BOXSIZE, KILO, QTY, ");
							strBuild.Append("LASTUPDATEDATE, CONSIGNMENTDATE, FLAG_EXPORT, CUSTOMERCODE, FLAG_DELETE ) "); 
							strBuild.Append(" VALUES "); 
							strBuild.Append("('" + ConsignmentNo + "','" + PayerID + "'," + dr["SEQ_NO"] + ",");
							strBuild.Append("N'" + Utility.ReplaceSingleQuote(CompanyTel) + "',");

							//BOXTYPE
							if ((dr["BOXTYPE"] != null) && (!dr["BOXTYPE"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								strBuild.Append("N'" + (String)dr["BOXTYPE"] + "',"); 
							}
							else
							{
								strBuild.Append("NULL,");
							}

							//DESCRIPTION
							Description = Utility.BoxDescFormat(Width) + " x " + Utility.BoxDescFormat(Length) + " x " + Utility.BoxDescFormat(Height);
							strBuild.Append("N'" + Description + "',");

							//BOXSIZE
							if ((dr["BOX_SIZE"] != null) && (!dr["BOX_SIZE"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								if ((String)dr["BOX_SIZE"] != "CUSTOMIZE")
								{
									strBuild.Append("N'" + (String)dr["BOX_SIZE"] + "',");
								}
								else
								{
									strBuild.Append("NULL,"); 
								}
							}
							else
							{
								strBuild.Append("NULL,");
							}

							//KILO
							if ((dr["KILO"] != null) && (!dr["KILO"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								//strBuild.Append("'" + (String)dr["KILO"] + "',"); 
								strBuild.Append("'" + dr["KILO"].ToString()  + "',"); 
							}
							else
							{
								strBuild.Append("NULL,"); 
							}

							//QTY
							if ((dr["QTY"] != null) && (!dr["QTY"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								//strBuild.Append("'" + (String)dr["QTY"] + "',"); 
								strBuild.Append("'" + dr["QTY"].ToString()  + "',"); 
							}	
							else
							{
								strBuild.Append("NULL,"); 
							}

							strBuild.Append(LastUpdateDate + ",");
							strBuild.Append("'" + ConsignmentDate + "',"); 
							strBuild.Append("NULL,"); 
							strBuild.Append("N'" + CustomerCode + "',");
							strBuild.Append("NULL)");

							dbCmd.CommandText = strBuild.ToString();
							jRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							Logger.LogTraceInfo("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "INF003", jRowsAffected + " rows insert into tb_ConsignmentNoteDetail table"); 
						}
					}
					transactionApp.Commit();
				}
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "INF004", "Database update transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "ERR006", "Error during application transaction rollback " + rollbackException.Message.ToString()); 
				}

				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "ERR007", "Update Transaction Failed : " + appException.Message.ToString());
				throw new ApplicationException("1 Error updateing ConsignmentNote ", appException);
			}
			catch (Exception ex)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "INF003", "Database update transaction rolled back.");
				}
				catch (Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "ERR008", "Error during app transaction roll back " + rollbackException.Message.ToString()); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "ERR009", "Update Transaction Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error updateing ConsignmentNote", ex); 
			}
			finally
			{
				if (conApp != null){conApp.Close();}
			}
			return iRowsAffected;
		}

		public static int DeleteConsignmentNote(String strAppID, String strEnterpriseID, String ConsignmentNo)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd =  null;
			StringBuilder strBuild = new StringBuilder(); 
			int iRowsAffected = 0;
			int jRowsAffected = 0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteConsignmentNote", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}

			IDbConnection conApp = dbCon.GetConnection(); 
			if (conApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteConsignmentNote", "ERR002", "conApp is null");  
				throw new ApplicationException("Connection to database failed", null); 
			}

			try
			{
				dbCon.OpenConnection(ref conApp); 
			}
			catch(ApplicationException appException)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteConsignmentNote", "ERR003", "Error opening connection : " + appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ", appException); 
			}
			catch(Exception ex)
			{
				if (conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteConsignmentNote", "ERR004", "Error opening connection : " + ex.Message.ToString());
				throw new ApplicationException("Error opening database connection ", ex); 
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp, IsolationLevel.ReadCommitted);
			if (transactionApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteConsignmentNote", "ERR005", "transactionApp is null");
				throw new ApplicationException("Failed begining a application transaction..", null); 
			}

			try
			{
				//not delete data but update flag_delete = Y
				if (ConsignmentNo != "")
				{
					strBuild = new StringBuilder(); 

					//update flag_delete = Y in tb_ConsignmentNote
					strBuild.Append("UPDATE TB_CONSIGNMENTNOTE ");
					strBuild.Append(" SET ");
					strBuild.Append("FLAG_DELETE = 'Y' ");
					strBuild.Append("WHERE CONSIGNMENT_NO = '" + ConsignmentNo + "'");

					dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd); 
					Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "DeleteConsignmentNote", "INF001", iRowsAffected + " rows delete into tb_consignmentNote table");
 
					if (iRowsAffected > 0)
					{
						strBuild = new StringBuilder(); 

						//update flag_delete = Y in tb_ConsignmentNoteDetail
						strBuild.Append("UPDATE TB_CONSIGNMENTNOTEDETAIL ");
						strBuild.Append(" SET ");
						strBuild.Append("FLAG_DELETE = 'Y' ");
						strBuild.Append("WHERE CONSIGNMENT_NO = '" + ConsignmentNo + "'");

						dbCmd.CommandText = strBuild.ToString();
						jRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "DeleteConsignmentNote", "INF002", jRowsAffected + " rows delete into tb_consignmentNoteDetail table");
					}
					transactionApp.Commit();
				}
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteConsignmentNote", "INF003", "Database delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteConsignmentNote", "ERR006", "Error during application transaction rollback " + rollbackException.Message.ToString()); 
				}

				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteConsignmentNote", "ERR007", "Delete Transaction Failed : " + appException.Message.ToString());
				throw new ApplicationException("1 Error deleteing ConsignmentNote ", appException);
			}
			catch (Exception ex)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("TIESDAL", "ConsignmentNoteDAL", "DeleteConsignmentNote", "INF003", "Database delete transaction rolled back.");
				}
				catch (Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteConsignmentNote", "ERR008", "Error during app transaction roll back " + rollbackException.Message.ToString()); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteConsignmentNote", "ERR009", "Delete Transaction Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error deleteing ConsignmentNote", ex); 
			}
			finally
			{
				if (conApp != null){conApp.Close();}
			}
			return iRowsAffected;
		}

		public static int UpdateParameterPrintPage(String strAppID, String strEnterpriseID, String PayerID, String P1, String P2, String P3, String P4, String P5, String P6)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd =  null;
			StringBuilder strBuild = new StringBuilder(); 
			DataSet ds = new DataSet();
			DataSet dsCustomer = new DataSet();
			int iRowsAffected = 0;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateParameterPrintPage", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}

			try
			{
				strBuild = new StringBuilder();
				ds = ConsignmentNoteDAL.GetParameterConsignment(strAppID, strEnterpriseID, PayerID);
				if(ds.Tables[0].Rows.Count > 0)
				{
					strBuild.Append(" UPDATE TB_PARAMETER_CONSIGNMENTNOTE SET PARAMVALUE = '" + P1 + "' WHERE PARAMNAME = 'P1_Shipper' AND PAYERID = '" + PayerID + "' ");
					strBuild.Append(" UPDATE TB_PARAMETER_CONSIGNMENTNOTE SET PARAMVALUE = '" + P2 + "' WHERE PARAMNAME = 'P2_DataEntry' AND PAYERID = '" + PayerID + "' ");
					strBuild.Append(" UPDATE TB_PARAMETER_CONSIGNMENTNOTE SET PARAMVALUE = '" + P3 + "' WHERE PARAMNAME = 'P3_HCR1' AND PAYERID = '" + PayerID + "' ");
					strBuild.Append(" UPDATE TB_PARAMETER_CONSIGNMENTNOTE SET PARAMVALUE = '" + P4 + "' WHERE PARAMNAME = 'P4_HCR2' AND PAYERID = '" + PayerID + "' ");
					strBuild.Append(" UPDATE TB_PARAMETER_CONSIGNMENTNOTE SET PARAMVALUE = '" + P5 + "' WHERE PARAMNAME = 'P5_Destination' AND PAYERID = '" + PayerID + "' ");
					strBuild.Append(" UPDATE TB_PARAMETER_CONSIGNMENTNOTE SET PARAMVALUE = '" + P6 + "' WHERE PARAMNAME = 'P6_Consignee' AND PAYERID = '" + PayerID + "'");
				}
				else
				{	
					string senderContactName = null;
					string senderTelephone = null;
					string senderSpInstr = null;
					dsCustomer = ConsignmentNoteDAL.SearchCustomer(strAppID, strEnterpriseID, PayerID, PayerID);
					foreach(DataRow dr in dsCustomer.Tables[0].Rows)
					{
						senderContactName = dr["cust_name"].ToString();
						senderTelephone = dr["telephone"].ToString();
						if(dr["remark2"].ToString() != "")
							senderSpInstr = dr["remark2"].ToString();
						else
							senderSpInstr = ".";
					}
					strBuild.Append(" Insert into TB_PARAMETER_CONSIGNMENTNOTE ");
					strBuild.Append(" (PayerID,ParamName,ParamValue)values('"+PayerID+"','Sender_Contact_Name','"+senderContactName+"') ");
					strBuild.Append(" Insert into TB_PARAMETER_CONSIGNMENTNOTE ");
					strBuild.Append(" (PayerID,ParamName,ParamValue)values('"+PayerID+"','Sender_Telephone','"+senderTelephone+"') ");
					strBuild.Append(" Insert into TB_PARAMETER_CONSIGNMENTNOTE ");
					strBuild.Append(" (PayerID,ParamName,ParamValue)values('"+PayerID+"','Default_Service_Type','ND') ");
					strBuild.Append(" Insert into TB_PARAMETER_CONSIGNMENTNOTE ");
					strBuild.Append(" (PayerID,ParamName,ParamValue)values('"+PayerID+"','Special_Instruction','"+senderSpInstr+"') ");
					strBuild.Append(" Insert into TB_PARAMETER_CONSIGNMENTNOTE ");
					strBuild.Append(" (PayerID,ParamName,ParamValue)values('"+PayerID+"','P1_Shipper','"+P1+"') ");
					strBuild.Append(" Insert into TB_PARAMETER_CONSIGNMENTNOTE ");
					strBuild.Append(" (PayerID,ParamName,ParamValue)values('"+PayerID+"','P2_DataEntry','"+P2+"') ");
					strBuild.Append(" Insert into TB_PARAMETER_CONSIGNMENTNOTE ");
					strBuild.Append(" (PayerID,ParamName,ParamValue)values('"+PayerID+"','P3_HCR1','"+P3+"') ");
					strBuild.Append(" Insert into TB_PARAMETER_CONSIGNMENTNOTE ");
					strBuild.Append(" (PayerID,ParamName,ParamValue)values('"+PayerID+"','P4_HCR2','"+P4+"') ");
					strBuild.Append(" Insert into TB_PARAMETER_CONSIGNMENTNOTE ");
					strBuild.Append(" (PayerID,ParamName,ParamValue)values('"+PayerID+"','P5_Destination','"+P5+"') ");
					strBuild.Append(" Insert into TB_PARAMETER_CONSIGNMENTNOTE ");
					strBuild.Append(" (PayerID,ParamName,ParamValue)values('"+PayerID+"','P6_Consignee','"+P6+"') ");
				}

				dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd); 
			}
			catch (Exception ex)
			{
				throw new ApplicationException("Error updating ParameterPrintPage ", ex); 
			}
			finally
			{
				ds.Dispose();
				dsCustomer.Dispose();
			}

			return iRowsAffected;
		}

		public static DataSet SearchCustomerCode(String appID, String enterpriseID, String PayerID, String CustomerCode)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
  
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchCustomerCode", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT * FROM [REFERENCES2] WHERE CUSTOMERCODE = N'" + CustomerCode + "'"); 

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);

			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchCustomerCode", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static DataSet SearchCustomer(String appID, String enterpriseID, String PayerID, String CustomerCode)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
  
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchCustomerCode", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT * FROM [CUSTOMER] WHERE custid = N'" + CustomerCode + "'"); 

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);

			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchCustomerCode", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static String InsertCustomer(String strAppID, String strEnterpriseID, String PayerID, String CustomerCode, String CustomerName, String Address1, String Address2, String Zipcode, String Telephone, String ContactPerson)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			StringBuilder strBuild = new StringBuilder();
			int iRowsAffected = 0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertCustomer", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null);
			}

			try
			{
				strBuild = new StringBuilder();
 
				strBuild.Append("INSERT INTO [REFERENCES2] ");
				strBuild.Append("(APPLICATIONID, ENTERPRISEID, CUSTOMERCODE, REFERENCE_NAME, ");
				strBuild.Append("ADDRESS1, ADDRESS2, ZIPCODE, TELEPHONE, ");
				strBuild.Append("CONTACTPERSON, PAYERID, FLAG_DELETE ) ");  
				strBuild.Append(" VALUES ");
				strBuild.Append("('" + strAppID + "', '" + strEnterpriseID + "', N'" + CustomerCode + "', N'" + CustomerName + "', ");
				strBuild.Append("N'" + Address1 + "', N'" + Address2 + "', '" + Zipcode + "', N'" + Telephone + "', ");
				strBuild.Append("N'" + ContactPerson + "', N'" + PayerID + "',NULL)");
 
				dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd); 
			}
			catch(Exception ex)
			{
				throw new ApplicationException("Error adding customer ", ex); 
			}
			return CustomerCode;
		}

		public static int UpdateCustomer(String strAppID, String strEnterpriseID, String PayerID, String CustomerCode, String CustomerName, String Address1, String Address2, String Zipcode, String Telephone, String ContactPerson)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			StringBuilder strBuild = new StringBuilder();
			int iRowsAffected = 0;
			DataSet DS = new DataSet();

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateCustomer", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null);
			}

			try
			{
				if (CustomerCode != "")
				{
//					//boon check before modify data - start
//					StringBuilder strQry = new StringBuilder();
//					strQry.Append("SELECT * FROM [tb_ConsignmentNote] "); 
//					strQry.Append("WHERE PAYERID = N'" + PayerID + "' ");
//					strQry.Append("AND CUSTOMERCODE = N'" + CustomerCode + "'");
//
//					dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
//
//					DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
//
//					if (DS.Tables[0].Rows.Count > 0)
//					{
//						iRowsAffected = -1;
//						return iRowsAffected;
//					}
//					//boon check before modify data - end


					strBuild = new StringBuilder();
 
					strBuild.Append("UPDATE [REFERENCES2] ");
					strBuild.Append(" SET ");
					strBuild.Append("REFERENCE_NAME = N'" + CustomerName + "', ");
					strBuild.Append("ADDRESS1 = N'" + Address1 + "', ");
					strBuild.Append("ADDRESS2 = N'" + Address2 + "', ");
					strBuild.Append("ZIPCODE = '" + Zipcode + "', ");
					strBuild.Append("TELEPHONE = N'" + Telephone + "', ");
					strBuild.Append("CONTACTPERSON = N'" + ContactPerson + "' ");
					strBuild.Append("WHERE PAYERID = N'" + PayerID + "' ");
					strBuild.Append("AND CUSTOMERCODE = N'" + CustomerCode + "'");

					dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
					iRowsAffected = dbCon.ExecuteNonQuery(dbCmd); 
				}
			}
			catch(Exception ex)
			{
				throw new ApplicationException("Error updating customer ", ex); 
			}
			finally
			{
				DS.Dispose();
			}
			return iRowsAffected;
		}

		public static int DeleteCustomer(String strAppID, String strEnterpriseID, String PayerID, String CustomerCode)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			StringBuilder strBuild = new StringBuilder();
			int iRowsAffected = 0;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteCustomer", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null);
			}

			try
			{
				if (CustomerCode != "")
				{
					//boon check befor delete - start
					StringBuilder strQry = new StringBuilder();
					strQry.Append("SELECT * FROM [tb_ConsignmentNote] "); 
					strQry.Append("WHERE PAYERID = N'" + PayerID + "' ");
					strQry.Append("AND CUSTOMERCODE = N'" + CustomerCode + "'");

					dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);

					DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

					if (DS.Tables[0].Rows.Count > 0)
					{
						iRowsAffected = DS.Tables[0].Rows.Count;
						iRowsAffected = -1;

						return iRowsAffected;
					}
					//boon check before delete - end
				
					 
					strBuild = new StringBuilder();
					strBuild.Append("DELETE [REFERENCES2] ");
					strBuild.Append("WHERE PAYERID = N'" + PayerID + "' ");
					strBuild.Append("AND CUSTOMERCODE = N'" + CustomerCode + "'");
	 
					dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
					iRowsAffected = dbCon.ExecuteNonQuery(dbCmd); 
				}
			}
			catch (Exception ex)
			{
				throw new ApplicationException("Error deleting consignee ", ex); 
			}
			finally
			{
				DS.Dispose();
			}
			return iRowsAffected;
		}

		public static DataSet SearchBoxSize(String appID, String enterpriseID, String PayerID, String BoxSizeID, String length, String Width, String Height)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
  
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchBoxSize", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT * FROM TB_BOXSIZE ");
			strQry = strQry.Append("WHERE FLAG_DELETE IS NULL ");
			strQry = strQry.Append("AND PAYERID = '" + PayerID + "' ");
 
			if (BoxSizeID != "")
			{
				strQry = strQry.Append(" AND BOX_SIZEID = '" + BoxSizeID + "'");
			}
			if (length != "")
			{
				strQry = strQry.Append(" AND Box_length = " + length);
			}
			if (Width != "")
			{
				strQry = strQry.Append(" AND Box_wide = " + Width);
			}
			if (Height != "")
			{
				strQry = strQry.Append(" AND Box_Height = " + Height);
			}

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchBoxSize", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static DataSet SearchChargeRate(String appID, String enterpriseID, String PayerID, String BoxSizeID)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
  
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchChargeRate", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT C.*, S.SERVICE_DESCRIPTION ");  
			strQry = strQry.Append("FROM TB_CHARGERATE C ");
			strQry = strQry.Append("LEFT OUTER JOIN SERVICE S ON C.SERVICE_CODE = S.SERVICE_CODE ");
			strQry = strQry.Append("WHERE C.FLAG_DELETE IS NULL ");
			strQry = strQry.Append("AND C.PAYERID = N'" + PayerID + "' ");
			if (BoxSizeID != "")
			{
				strQry = strQry.Append("AND C.BOX_SIZEID = '" + BoxSizeID + "' ");
			}
			strQry = strQry.Append(" ORDER BY C.BOX_SIZEID, ");
			strQry = strQry.Append("CONVERT(FLOAT,REPLACE(CONVERT(CHAR(5),S.COMMIT_TIME,8),':','.')) + S.TRANSIT_DAY * 24");		

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchChargeRate", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static String InsertBoxSize(String strAppID, String strEnterpriseID, String PayerID, String BoxsizeID, String Width, String Length, String Height, String MaxWeight, DataTable dtChargeRate)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			StringBuilder strBuild = new StringBuilder();
			int iRowsAffected = 0;
			int jRowsAffected = 0;
			String LastUpdateDate = "";

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertBoxSize", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}

			IDbConnection conApp = dbCon.GetConnection();
			if (conApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertBoxSize", "ERR002", "conApp is null");  
				throw new ApplicationException("Connection to database failed", null); 
			}

			try
			{
				dbCon.OpenConnection(ref conApp); 
			}
			catch (ApplicationException appException)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertBoxSize", "ERR003", "Error opening connection : " + appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ", appException);  
			}
			catch (Exception ex)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertBoxSize", "ERR004", "Error opening connection : " + ex.Message.ToString());
				throw new ApplicationException("Error opening database connection ", ex);  
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp, IsolationLevel.ReadCommitted);
			if (transactionApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertBoxSize", "ERR005", "transactionApp is null");
				throw new ApplicationException("Failed begining a application transaction..", null); 
			}

			try
			{
				if (BoxsizeID != "")
				{
					LastUpdateDate = Utility.DateFormat(strAppID, strEnterpriseID, DateTime.Now, DTFormat.DateTime);  

					strBuild = new StringBuilder();

					//insert BoxSize in TB_BOXSIZE
					strBuild.Append("INSERT INTO TB_BOXSIZE "); 
					strBuild.Append("(BOX_SIZEID, BOX_WIDE, BOX_LENGTH, BOX_HEIGHT, BOX_MAXWEIGHT, PAYERID, FLAG_DELETE, LASTUPDATEDATE) "); 
					strBuild.Append(" VALUES "); 
					strBuild.Append("(N'" + BoxsizeID + "',"); 
					strBuild.Append(Width + "," + Length + "," + Height + "," + MaxWeight + ",");
					strBuild.Append("N'" + PayerID + "',"); 
					strBuild.Append("NULL," + LastUpdateDate + ")"); 

					dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
	 
					Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "InsertBoxSize", "INF001", iRowsAffected + " rows insert into tb_boxsize table"); 
					
					//insert ChargeRate in TB_CHARGERATE
					if (dtChargeRate != null && dtChargeRate.Rows.Count > 0 && iRowsAffected > 0)
					{
						foreach(DataRow dr in dtChargeRate.Rows)
						{
							strBuild = new StringBuilder();
 
							strBuild.Append("INSERT INTO TB_CHARGERATE "); 
							strBuild.Append("(BOX_SIZEID, SERVICE_CODE, CHARGE_RATE, PAYERID, FLAG_DELETE, LASTUPDATEDATE) ");
							strBuild.Append(" VALUES ");
							strBuild.Append("(N'" + BoxsizeID + "',");
							strBuild.Append("N'" + (String)dr["SERVICE_CODE"] + "', ");
							//CHARGE_RATE
							if ((dr["CHARGE_RATE"] != null) && (!dr["CHARGE_RATE"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								strBuild.Append(dr["CHARGE_RATE"].ToString() + ",");
							}
							else
							{
								strBuild.Append("NULL,"); 
							}
							strBuild.Append("N'" + PayerID + "',NULL," + LastUpdateDate + ")");
 
							dbCmd.CommandText = strBuild.ToString();
							jRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							Logger.LogTraceInfo("TIESDAL", "ConsignmentNoteDAL", "InsertBoxSize", "INF002", jRowsAffected + " rows insert into TB_CHARGERATE table"); 
						}
					}
					transactionApp.Commit();
				}
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertBoxSize", "INF003", "Database insert transaction rolled back."); 
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertBoxSize", "ERR006", "Error during application transaction rollback " + rollbackException.Message.ToString()); 
				}

				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertBoxSize", "ERR007", "Insert Transaction Failed : " + appException.Message.ToString());
				throw new ApplicationException("1 Error adding BoxSize ", appException);
			}
			catch(Exception ex)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("TIESDAL", "ConsignmentNoteDAL", "InsertBoxSize", "INF003", "Database insert transaction rolled back.");
				}
				catch (Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertBoxSize", "ERR008", "Error during app transaction roll back " + rollbackException.Message.ToString()); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertBoxSize", "ERR009", "Insert Transaction Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error adding BoxSize", ex); 
			}
			finally
			{
				if (conApp != null){conApp.Close();}
			}
			return BoxsizeID;
		}

		public static int UpdateBoxSize(String strAppID, String strEnterpriseID, String PayerID, String BoxsizeID, String Width, String Length, String Height, String MaxWeight, DataTable dtChargeRate)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			StringBuilder strBuild = new StringBuilder();
			int iRowsAffected = 0;
			int jRowsAffected = 0;
			String LastUpdateDate = "";
			DataSet DS = new DataSet();

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateBoxSize", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}

			IDbConnection conApp = dbCon.GetConnection(); 
			if (conApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateBoxSize", "ERR002", "conApp is null");  
				throw new ApplicationException("Connection to database failed", null); 
			}

			try
			{
				dbCon.OpenConnection(ref conApp); 
			}
			catch(ApplicationException appException)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateBoxSize", "ERR003", "Error opening connection : " + appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ", appException); 
			}
			catch(Exception ex)
			{
				if (conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateBoxSize", "ERR004", "Error opening connection : " + ex.Message.ToString());
				throw new ApplicationException("Error opening database connection ", ex); 
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp, IsolationLevel.ReadCommitted);
			if (transactionApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateBoxSize", "ERR005", "transactionApp is null");
				throw new ApplicationException("Failed begining a application transaction..", null); 
			}

			try
			{
				if (BoxsizeID != "")
				{
					////boon - check before modify - start
					StringBuilder strQry = new StringBuilder();
					strQry.Append("SELECT * FROM [tb_ConsignmentNoteDetail] "); 
					strQry.Append("WHERE PAYERID = N'" + PayerID + "' ");
					strQry.Append("AND BOXSIZE = N'" + BoxsizeID + "'");

					dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);

					DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

					if (DS.Tables[0].Rows.Count > 0)
					{
						iRowsAffected = -1;
						return iRowsAffected;
					}
					////boon - check before modify - end


					LastUpdateDate = Utility.DateFormat(strAppID, strEnterpriseID, DateTime.Now, DTFormat.DateTime);

					//update BoxSize in tb_ConsignmentNote
					strBuild = new StringBuilder();
					strBuild.Append("UPDATE TB_BOXSIZE ");
					strBuild.Append(" SET ");
					strBuild.Append("BOX_WIDE = " + Width + ", ");
					strBuild.Append("BOX_LENGTH = " + Length + ", ");
					strBuild.Append("BOX_HEIGHT = " + Height + ", ");
					strBuild.Append("BOX_MAXWEIGHT = " + MaxWeight + ", ");
					strBuild.Append("LASTUPDATEDATE = " + LastUpdateDate);
					strBuild.Append(" WHERE PAYERID = N'" + PayerID + "' "); 
					strBuild.Append("AND BOX_SIZEID = N'" + BoxsizeID + "'"); 

					dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "UpdateBoxSize", "INF001", iRowsAffected + " rows update into TB_BOXSIZE table");
					
					//update ChargeRate (delete before insert into TB_CHARGERATE)
					if (dtChargeRate != null && dtChargeRate.Rows.Count > 0 && iRowsAffected > 0)
					{
						//delete ChargeRate
						strBuild = new StringBuilder();
						strBuild.Append("DELETE  TB_CHARGERATE "); 
						strBuild.Append("WHERE PAYERID = N'" + PayerID + "' "); 
						strBuild.Append("AND BOX_SIZEID = N'" + BoxsizeID + "'"); 

						dbCmd.CommandText = strBuild.ToString();
						jRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "UpdateBoxSize", "INF002", iRowsAffected + " rows delete into TB_CHARGERATE table");
 
						//insert ChargeRate in TB_CHARGERATE
						foreach(DataRow dr in dtChargeRate.Rows)
						{
							strBuild = new StringBuilder();
 
							strBuild.Append("INSERT INTO TB_CHARGERATE "); 
							strBuild.Append("(BOX_SIZEID, SERVICE_CODE, CHARGE_RATE, PAYERID, FLAG_DELETE, LASTUPDATEDATE) ");
							strBuild.Append(" VALUES ");
							strBuild.Append("(N'" + BoxsizeID + "',");
							strBuild.Append("N'" + (String)dr["SERVICE_CODE"] + "', ");
							//CHARGE_RATE
							if ((dr["CHARGE_RATE"] != null) && (!dr["CHARGE_RATE"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								strBuild.Append(dr["CHARGE_RATE"].ToString() + ",");
							}
							else
							{
								strBuild.Append("NULL,"); 
							}
							strBuild.Append("N'" + PayerID + "',NULL," + LastUpdateDate + ")");
 
							dbCmd.CommandText = strBuild.ToString();
							jRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							Logger.LogTraceInfo("TIESDAL", "ConsignmentNoteDAL", "UpdateBoxSize", "INF002", jRowsAffected + " rows insert into TB_CHARGERATE table"); 
						}
					}
					transactionApp.Commit(); 
				}
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateBoxSize", "INF004", "Database update transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateBoxSize", "ERR006", "Error during application transaction rollback " + rollbackException.Message.ToString()); 
				}

				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateBoxSize", "ERR007", "Update Transaction Failed : " + appException.Message.ToString());
				throw new ApplicationException("1 Error updateing BoxSize ", appException);
			}
			catch (Exception ex)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("TIESDAL", "ConsignmentNoteDAL", "UpdateBoxSize", "INF003", "Database update transaction rolled back.");
				}
				catch (Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateBoxSize", "ERR008", "Error during app transaction roll back " + rollbackException.Message.ToString()); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateBoxSize", "ERR009", "Update Transaction Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error updateing BoxSize", ex); 
			}
			finally
			{
				if (conApp != null){conApp.Close();}
				DS.Dispose();
			}
			return iRowsAffected;
		}

		public static int DeleteBoxSize(String strAppID, String strEnterpriseID, String PayerID, String BoxsizeID)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			StringBuilder strBuild = new StringBuilder();
			int iRowsAffected = 0;
			int jRowsAffected = 0;
			DataSet DS = new DataSet();

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteBoxSize", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}

			IDbConnection conApp = dbCon.GetConnection();
			if (conApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteBoxSize", "ERR002", "conApp is null");  
				throw new ApplicationException("Connection to database failed", null);  
			}

			try
			{
				dbCon.OpenConnection(ref conApp); 
			}
			catch(ApplicationException appException)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteBoxSize", "ERR003", "Error opening connection : " + appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ", appException);  
			}
			catch(Exception ex)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteBoxSize", "ERR004", "Error opening connection : " + ex.Message.ToString());
				throw new ApplicationException("Error opening database connection ", ex); 
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp, IsolationLevel.ReadCommitted);
			if (transactionApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteBoxSize", "ERR005", "transactionApp is null");
				throw new ApplicationException("Failed begining a application transaction..", null); 
			}

			try
			{
				//not delete data but update flag_delete = Y
				if (BoxsizeID != "")
				{
					////boon - check before delete - start
					StringBuilder strQry = new StringBuilder();
					strQry.Append("SELECT * FROM [tb_ConsignmentNoteDetail] "); 
					strQry.Append("WHERE PAYERID = N'" + PayerID + "' ");
					strQry.Append("AND BOXSIZE = N'" + BoxsizeID + "'");

					dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);

					DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

					if (DS.Tables[0].Rows.Count > 0)
					{
						iRowsAffected = DS.Tables[0].Rows.Count;
						iRowsAffected = -1;

						return iRowsAffected;
					}
					////boon - check before delete - end

					//update flag_delete = Y in TB_BOXSIZE
					strBuild = new StringBuilder();
					strBuild.Append("DELETE FROM TB_BOXSIZE "); 
//					strBuild.Append("SET FLAG_DELETE = 'Y' "); 
					strBuild.Append("WHERE BOX_SIZEID = N'" + BoxsizeID + "'" ); 
					strBuild.Append("AND PAYERID = N'" + PayerID + "'"); 

					dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd); 
					Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "DeleteBoxSize", "INF001", iRowsAffected + " rows delete into TB_BOXSIZE table");
 
					if (iRowsAffected > 0)
					{
						//update flag_delete = Y in TB_BOXSIZE
						strBuild = new StringBuilder(); 
						strBuild.Append("UPDATE TB_CHARGERATE ");
						strBuild.Append("SET FLAG_DELETE = 'Y' ");    
						strBuild.Append("WHERE BOX_SIZEID = N'" + BoxsizeID + "' "); 
						strBuild.Append("AND PAYERID = N'" + PayerID + "'"); 

						dbCmd.CommandText = strBuild.ToString();
						jRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "DeleteBoxSize", "INF002", jRowsAffected + " rows delete into TB_CHARGERATE table"); 
					}
					transactionApp.Commit();
				}
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteBoxSize", "INF003", "Database delete transaction rolled back."); 
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteBoxSize", "ERR006", "Error during application transaction rollback " + rollbackException.Message.ToString());  
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteBoxSize", "ERR007", "Delete Transaction Failed : " + appException.Message.ToString());
				throw new ApplicationException("1 Error deleteing BoxSize ", appException); 
			}
			catch(Exception ex)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("TIESDAL", "ConsignmentNoteDAL", "DeleteBoxSize", "INF003", "Database delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteBoxSize", "ERR008", "Error during app transaction roll back " + rollbackException.Message.ToString()); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "DeleteBoxSize", "ERR009", "Delete Transaction Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error deleteing BoxSize", ex); 
			}
			finally
			{
				if (conApp != null){conApp.Close();}

				DS.Dispose();
			}
			return iRowsAffected;
		}

		public static DataSet GetServiceType(String appID, String enterpriseID, String PayerID, String ServiceCode)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetServiceType", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT * FROM Service ");  
			if (ServiceCode != "")
			{
				strQry = strQry.Append(" WHERE SERVICE_CODE = '" + ServiceCode + "' "); 
			}
			strQry = strQry.Append(" ORDER BY convert(float,REPLACE(CONVERT(CHAR(5),commit_time,8),':','.')) + transit_day * 24 "); 

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text); 
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetServiceType", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}


		public static DataSet GetPrintReport(String appID, String enterpriseID, String PayerID, String ConsignmentNo)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetPrintReport", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();

//			strQry = strQry.Append("SELECT t1.Consignment_No, LTRIM(RTRIM(t1.PayerID)) AS PayerID, t1.Telephone, t1.LastUserID, ");
//			strQry = strQry.Append("  t1.HCReturn, t1.InvReturn, t1.CODAmount, t1.LastUpdateDate, t1.ConsignmentDate, t1.ServiceType, ");
//			strQry = strQry.Append("  t5.address1, t5.address2, t5.zipcode, t6.Cust_Name AS Payer_Name, t6.Address1 AS Cust_Address1, ");
//			strQry = strQry.Append("  t6.Address2 AS Cust_Address2, t6.State_Code AS Cust_State_Code, t6.zipcode AS Cust_zipcode, ");
//			strQry = strQry.Append("  (SELECT State_Name FROM State t7 WHERE (t6.State_Code = t7.State_Code) ) AS State_Name, ");
//			strQry = strQry.Append("  t6.Ref_Code, t7.Address1 AS Company_Address1, t7.Address2 AS Company_Address2, ");
//			strQry = strQry.Append("  t7.Zipcode AS Company_Zipcode, ");
//			strQry = strQry.Append("  (SELECT t9.State_Name ");
//			strQry = strQry.Append("     FROM Zipcode t8 INNER JOIN State t9 ON (t8.State_Code = t9.State_Code) ");
//			strQry = strQry.Append("     WHERE (t7.Zipcode = t8.Zipcode)) AS Company_State_Name, ");
//			strQry = strQry.Append("  t6.Contact_Person AS Sender_Contact_Person, ");
//			strQry = strQry.Append("  (SELECT State_Name ");
//			strQry = strQry.Append("     FROM State t7 INNER JOIN Zipcode t8 ON (t7.State_Code = t8.State_Code) ");
//			strQry = strQry.Append("     WHERE (t5.Zipcode = t8.Zipcode)) AS Recipion_State_Name, ");
//			strQry = strQry.Append("  t6.Telephone AS Sender_Telephone, t5.Reference_Name, t1.DeclareValue, t6.Cust_Name, ");
//			strQry = strQry.Append("  t6.Zipcode AS Sender_zipcode, LTRIM(RTRIM(Cust_Ref)) AS Cust_Ref, ");
//			strQry = strQry.Append("  LTRIM(RTRIM(Special_Instruction)) AS Special_Instruction, Recipion_Contact_Person, ");
//			strQry = strQry.Append("  NULL AS Page ");
//			strQry = strQry.Append("FROM tb_ConsignmentNote t1 ");
//			strQry = strQry.Append("LEFT OUTER JOIN dbo.[References2] t5 ON (t1.CustomerCode = t5.CustomerCode) ");
//			strQry = strQry.Append("LEFT OUTER JOIN Customer t6 ON (t1.PayerID = t6.CustID) ");
//			strQry = strQry.Append("LEFT OUTER JOIN Enterprise t7 ON (t1.EnterpriseID = t7.EnterpriseID) ");
//			strQry = strQry.Append("WHERE  t1.Payerid = '" + PayerID + "' ");


			strQry = strQry.Append(" SELECT tbC.Consignment_No, LTRIM(RTRIM(tbC.PayerID)) AS PayerID, tbC.Telephone, tbC.LastUserID, "); 
			strQry = strQry.Append(" tbC.HCReturn, tbC.InvReturn, tbC.CODAmount, tbC.LastUpdateDate, tbC.ConsignmentDate, tbC.ServiceType, "); 
			strQry = strQry.Append(" dR.address1, dR.address2, dR.zipcode, ");
			strQry = strQry.Append(" CSR.snd_rec_name AS Payer_Name, CSR.Address1 AS Cust_Address1, "); 
			strQry = strQry.Append(" CSR.Address2 AS Cust_Address2, ZC.State_Code AS Cust_State_Code, CSR.zipcode AS Cust_zipcode, "); 
			strQry = strQry.Append(" (SELECT s.State_Name "); 
			strQry = strQry.Append(" FROM Zipcode z INNER JOIN State s ON (z.State_Code = s.State_Code) "); 
			strQry = strQry.Append(" WHERE CSR.Zipcode = z.Zipcode) AS State_Name, "); 
			strQry = strQry.Append(" C.Ref_Code, E.Address1 AS Company_Address1, E.Address2 AS Company_Address2, "); 
			strQry = strQry.Append(" E.Zipcode AS Company_Zipcode, "); 
			strQry = strQry.Append(" (SELECT sC.State_Name "); 
			strQry = strQry.Append(" FROM Zipcode zC INNER JOIN State sC ON (zC.State_Code = sC.State_Code) "); 
			strQry = strQry.Append(" WHERE E.Zipcode = zC.Zipcode) AS Company_State_Name, "); 
			strQry = strQry.Append(" CSR.Contact_Person AS Sender_Contact_Person, ");
			strQry = strQry.Append(" (SELECT sR.State_Name "); 
			strQry = strQry.Append(" FROM State sR INNER JOIN Zipcode zR ON (sR.State_Code = zR.State_Code) "); 
			strQry = strQry.Append(" WHERE (dR.Zipcode = zR.Zipcode)) AS Recipion_State_Name, "); 
			strQry = strQry.Append(" CSR.Telephone AS Sender_Telephone, ");
			strQry = strQry.Append(" dR.Reference_Name, tbC.DeclareValue, C.Cust_Name, "); 
			strQry = strQry.Append(" CSR.Zipcode AS Sender_zipcode, "); 
			strQry = strQry.Append(" LTRIM(RTRIM(Cust_Ref)) AS Cust_Ref, "); 
			strQry = strQry.Append(" LTRIM(RTRIM(Special_Instruction)) AS Special_Instruction, Recipion_Contact_Person, ");
			strQry = strQry.Append(" NULL AS Page ");
			strQry = strQry.Append(" FROM tb_ConsignmentNote tbC, dbo.[References2] dR, Customer C, Customer_Snd_Rec CSR, ZipCode ZC, Enterprise E "); 
			strQry = strQry.Append(" WHERE  tbC.CustomerCode = dR.CustomerCode ");
			strQry = strQry.Append(" AND tbC.PayerID = dR.PayerID ");		//Boon - 20110211
			strQry = strQry.Append(" AND tbC.sender_name = CSR.snd_rec_name ");
			strQry = strQry.Append(" AND tbC.PayerID = C.CustID ");
			strQry = strQry.Append(" AND C.CustID = CSR.CustID ");
			strQry = strQry.Append(" AND CSR.zipcode = ZC.zipcode ");
			strQry = strQry.Append(" AND tbC.EnterpriseID = E.EnterpriseID ");
			strQry = strQry.Append(" AND tbC.Payerid = '" + PayerID + "' ");
			
			if (ConsignmentNo != "")
			{
				strQry = strQry.Append("AND (tbC.Consignment_No IN (" + ConsignmentNo + ")) ");
//				strQry = strQry.Append("AND (t1.Consignment_No IN (" + ConsignmentNo + ")) ");
			}
			strQry = strQry.Append(" ORDER BY tbC.Consignment_No");
//			strQry = strQry.Append(" ORDER BY t1.Consignment_No");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetPrintReport", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}


		public static DataSet GetPrintReportLicensePlate(String appID, String enterpriseID, String PayerID, String ConsignmentNo)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetPrintReportLicensePlate", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append(" SELECT Hd.Consignment_no as CONSIGNMENT_NO, Hd.PayerID as PAYER_ID, ");
			strQry = strQry.Append(" MAX(Hd.ServiceType) as SERVICE_TYPE, MAX(R.Zipcode) as ZIP_CODE, SUM(Dt.Qty) as TOTAL_QTY, ");
			strQry = strQry.Append(" MAX(R.Reference_name) as REFERENCE_NAME, MAX(R.Telephone) as TELEPHONE, ");
			strQry = strQry.Append(" MAX(R.Address1)+', '+MAX(R.Address2) as ADDRESS, MAX(R.ContactPerson) as CONTACTPERSON, ");
			strQry = strQry.Append(" MAX(Hd.Sender_Name) as SENDER_NAME, MAX(Hd.Sender_Tel) as SENDER_TEL, MAX(Hd.Cust_Ref) as CUST_REF ");
			strQry = strQry.Append(" FROM dbo.TB_CONSIGNMENTNOTE Hd ");
//			strQry = strQry.Append(" LEFT OUTER JOIN dbo.CUSTOMER C ON Hd.PAYERID = C.CUSTID ");
//			strQry = strQry.Append(" LEFT OUTER JOIN dbo.REFERENCES2 R ON Hd.CUSTOMERCODE = R.CUSTOMERCODE, ");
			strQry = strQry.Append(" INNER JOIN dbo.CUSTOMER C ON Hd.PAYERID = C.CUSTID ");		//Boon - 2011/02/11
			strQry = strQry.Append(" INNER JOIN dbo.REFERENCES2 R ON Hd.CUSTOMERCODE = R.CUSTOMERCODE AND Hd.PayerID = R.PayerID, "); //Boon - 2011/02/11
			strQry = strQry.Append(" dbo.tb_ConsignmentNoteDetail as Dt ");
			strQry = strQry.Append(" WHERE Hd.Consignment_no = Dt.Consignment_no ");
			strQry = strQry.Append(" AND Hd.PayerID = Dt.PayerID ");
			strQry = strQry.Append(" AND (Hd.FLAG_EXPORT IS NULL OR Hd.FLAG_EXPORT <> 'EX') ");
			strQry = strQry.Append(" AND Hd.FLAG_DELETE IS NULL ");
			strQry = strQry.Append(" AND Hd.PAYERID = '" + PayerID + "' ");
			
			if (ConsignmentNo != "")
			{
				strQry = strQry.Append(" AND Hd.Consignment_no IN (" + ConsignmentNo + ") ");
			}
			strQry = strQry.Append(" GROUP BY Hd.Consignment_no, Hd.PayerID ");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetPrintReportLicensePlate", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static DataSet GetConsReport(String appID, String enterpriseID)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetConsReport", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append(" SELECT * ");
			strQry = strQry.Append(" FROM tb_ConsignmentNoteDetailReport ");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetConsReport", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}
		
		public static DataSet GetScanSheet(string appID, string enterpriseId, string BatchDate, string Location, string ServiceType)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseId);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetScanSheet", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append(" EXEC QueryDailyLodgments ");
			strQry = strQry.Append(" @enterpriseId  = '" + enterpriseId + "', ");
			strQry = strQry.Append(" @BatchDate  = '" + BatchDate + "', ");
			strQry = strQry.Append(" @Location  = '" + Location + "', ");
			strQry = strQry.Append(" @ServiceType  = '" + ServiceType + "' ");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetScanSheet", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}


		public static DataSet GetConsDetailReport(String appID, String enterpriseID, String consignmentNo)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetConsDetailReport", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append(" SELECT * ");
			strQry = strQry.Append(" FROM tb_ConsignmentNoteDetail ");
			strQry = strQry.Append(" WHERE consignment_no in ( " + consignmentNo + " )");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetConsDetailReport", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		
		public static DataSet GetSWBRouteInformation(String appID, String enterpriseID, String ZipCode)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetSWBRouteInformation", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append(" SELECT origin_DC, dest_postal_code, line_haul1, ");
			strQry = strQry.Append(" line_haul2, delivery_route_code,destination_DC ");
			strQry = strQry.Append(" FROM [SWB].[dbo].[Route_Information] ");
			strQry = strQry.Append(" WHERE dest_postal_code = '" + ZipCode + "' ");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetSWBRouteInformation", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}


		public static DataSet GetDMSState(String appID, String enterpriseID, String destZipCode)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetSWBRouteInformation", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();

			strQry = strQry.Append(" SELECT S.state_name FROM [dbo].[State] as S, [dbo].[ZipCode] as Z ");
			strQry = strQry.Append(" WHERE Z.applicationid = S.applicationid AND Z.enterpriseid = S.enterpriseid ");
			strQry = strQry.Append(" AND Z.country = S.country AND Z.state_code = S.state_code ");
			strQry = strQry.Append(" AND Z.zipcode = '" + destZipCode + "' ");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetDMSState", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}


		public static DataSet SumQtyByConsignment(String appID, String enterpriseID, String PayerID, String ConsignmentNo)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SumQtyByConsignment", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT consignment_no, SUM(Qty) AS TotalQty, SUM(kilo * qty) AS TotalKilo ");
			strQry = strQry.Append("FROM tb_ConsignmentNoteDetail ");
			strQry = strQry.Append("WHERE Payerid = '" + PayerID + "' ");
			strQry = strQry.Append(" AND FLAG_EXPORT IS NULL ");
			if (ConsignmentNo != "")
			{
				strQry = strQry.Append("AND Consignment_No = '" + ConsignmentNo + "' ");
			}
			strQry = strQry.Append(" GROUP BY consignment_no ");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SumQtyByConsignment", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static int InsertConsignmentNoteDetailReport(String strAppID, String strEnterpriseID, String PayerID, int RandomNo, DataTable dtReport)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			StringBuilder strBuild = new StringBuilder();
			int iRowsAffected = 0;
			int jRowsAffected = 0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNoteDetailReport", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}

			IDbConnection conApp = dbCon.GetConnection();
			if (conApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNoteDetailReport", "ERR002", "conApp is null");  
				throw new ApplicationException("Connection to database failed", null); 
			}

			try
			{
				dbCon.OpenConnection(ref conApp); 
			}
			catch (ApplicationException appException)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNoteDetailReport", "ERR003", "Error opening connection : " + appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ", appException);  
			}
			catch (Exception ex)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNoteDetailReport", "ERR004", "Error opening connection : " + ex.Message.ToString());
				throw new ApplicationException("Error opening database connection ", ex);  
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp, IsolationLevel.ReadCommitted);
			if (transactionApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNoteDetailReport", "ERR005", "transactionApp is null");
				throw new ApplicationException("Failed begining a application transaction..", null); 
			}

			try
			{
				if (dtReport != null && dtReport.Rows.Count > 0)
				{
					//delete old ConsignmentNoteDetailReport 
					strBuild = new StringBuilder();
					strBuild.Append("DELETE  tb_ConsignmentNoteDetailReport "); 

					dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "InsertConsignmentNoteDetailReport", "INF001", iRowsAffected + " rows delete into tb_ConsignmentNoteDetailReport table");
 
					foreach (DataRow drReport in dtReport.Rows)
					{
						//insert new ConsignmentNoteDetailReport
						strBuild = new StringBuilder();
						strBuild.Append("INSERT INTO tb_ConsignmentNoteDetailReport ");
						strBuild.Append(" (RandomNo, [ID], Copy, consignment_No");
						strBuild.Append("  , TotalCountQTY, TotalCountKilo, PayerID, Telephone");
						strBuild.Append("  , LastUserID, HCReturn, InvReturn, codamount");
						strBuild.Append("  , LastUpdateDate, ConsignmentDate, ServiceType");
						strBuild.Append("  , address1, address2, zipcode, payer_name");
						strBuild.Append("  , cust_address1, cust_address2, Cust_State_Code");
						strBuild.Append("  , Cust_zipcode, state_name, ref_code");
						strBuild.Append("  , company_address1, company_address2, company_zipcode");
						strBuild.Append("  , company_state_name, sender_Contact_person, Recipion_state_name");
						strBuild.Append("  , sender_telephone, reference_name, DeclareValue");
						strBuild.Append("  , cust_name, sender_zipcode, cust_ref, Special_Instruction");
						strBuild.Append("  , recipion_contact_person, ConsignmentNoBarcode, ServiceTypeBarcode");
						strBuild.Append("  , RecipientCustRefZipBarcode,TotalQTYBarcode) "); 
						strBuild.Append(" VALUES ");
						strBuild.Append("(" + drReport["RandomNo"] + "," + drReport["ID"] + "," + "N'" + drReport["Copy"] + "',N'" + drReport["consignment_No"] + "'");
						strBuild.Append("," + drReport["TotalCountQTY"] + "," + drReport["TotalCountKilo"] + ",N'" + drReport["PayerID"] + "',N'" + drReport["Telephone"] + "'");
						strBuild.Append(",N'" + drReport["LastUserID"] + "'");
						if (drReport["HCReturn"].ToString() == "True")
						{
							strBuild.Append(", 1");
						}
						else
						{
							strBuild.Append(", 0");
						}
						if (drReport["InvReturn"].ToString() == "True")
						{
							strBuild.Append(", 1");
						}
						else
						{
							strBuild.Append(", 0");
						}
						//," + drReport["HCReturn"] + "," + drReport["InvReturn"]);
						strBuild.Append("," + drReport["codamount"]); 
						strBuild.Append(",N'" + drReport["LastUpdateDate"] + "'," + drReport["ConsignmentDate"] + ",N'" + drReport["ServiceType"] + "'");
						strBuild.Append(",N'" + drReport["address1"] + "',N'" + drReport["address2"] + "','" + drReport["zipcode"] + "',N'" + drReport["payer_name"] + "'");
						strBuild.Append(",N'" + drReport["cust_address1"] + "',N'" + drReport["cust_address2"] + "',N'" + drReport["Cust_State_Code"] + "'");
						strBuild.Append(",'" + drReport["Cust_zipcode"] + "',N'" + drReport["state_name"] + "',N'" + drReport["ref_code"] + "'");
						strBuild.Append(",N'" + drReport["company_address1"] + "',N'" + drReport["company_address2"] + "','" + drReport["company_zipcode"] + "'");
						strBuild.Append(",N'" + drReport["company_state_name"] + "',N'" + drReport["sender_Contact_person"] + "',N'" + drReport["Recipion_state_name"] + "'");
						strBuild.Append(",N'" + drReport["sender_telephone"] + "',N'" + drReport["reference_name"] + "'," + drReport["DeclareValue"]);
						strBuild.Append(",N'" + drReport["cust_name"] + "','" + drReport["sender_zipcode"] + "',N'" + drReport["cust_ref"] + "',N'" + drReport["Special_Instruction"] + "'");
						strBuild.Append(",N'" + drReport["recipion_contact_person"] + "',N'" + drReport["ConsignmentNoBarcode"] + "',N'" + drReport["ServiceTypeBarcode"] + "'");
						strBuild.Append(",N'" + drReport["RecipientCustRefZipBarcode"] + "',N'" + drReport["TotalQTYBarcode"] + "')");

						dbCmd.CommandText = strBuild.ToString();
						jRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "InsertConsignmentNoteDetailReport", "INF002", jRowsAffected + " rows insert into tb_ConsignmentNoteDetailReport table"); 
					}
					transactionApp.Commit();
				}
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNoteDetailReport", "INF003", "Database insert transaction rolled back."); 
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNoteDetailReport", "ERR006", "Error during application transaction rollback " + rollbackException.Message.ToString()); 
				}

				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNoteDetailReport", "ERR007", "Insert Transaction Failed : " + appException.Message.ToString());
				throw new ApplicationException("1 Error adding BoxSize ", appException);
			}
			catch(Exception ex)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNoteDetailReport", "INF003", "Database insert transaction rolled back.");
				}
				catch (Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNoteDetailReport", "ERR008", "Error during app transaction roll back " + rollbackException.Message.ToString()); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertConsignmentNoteDetailReport", "ERR009", "Insert Transaction Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error adding BoxSize", ex); 
			}
			finally
			{
				if (conApp != null){conApp.Close();}
			}
			return iRowsAffected;
		}


		public static int InsertTmpConsignment(String strAppID, String strEnterpriseID, String PayerID, DataTable dtConsignment)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			StringBuilder strBuild = new StringBuilder();
			int iRowsAffected = 0;
			int jRowsAffected = 0;
			string return_pod_hc = "";
			string return_invoice_hc = "";
			decimal cod_amount;
			decimal declare_value;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignment", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}

			IDbConnection conApp = dbCon.GetConnection();
			if (conApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignment", "ERR002", "conApp is null");  
				throw new ApplicationException("Connection to database failed", null); 
			}

			try
			{
				dbCon.OpenConnection(ref conApp); 
			}
			catch (ApplicationException appException)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignment", "ERR003", "Error opening connection : " + appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ", appException);  
			}
			catch (Exception ex)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignment", "ERR004", "Error opening connection : " + ex.Message.ToString());
				throw new ApplicationException("Error opening database connection ", ex);  
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp, IsolationLevel.ReadCommitted);
			if (transactionApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignment", "ERR005", "transactionApp is null");
				throw new ApplicationException("Failed begining a application transaction..", null); 
			}

			try
			{
				if (dtConsignment != null && dtConsignment.Rows.Count > 0)
				{
					//delete old TmpConsignment 
					strBuild = new StringBuilder();
					strBuild.Append("DELETE FROM tmp_consignment"); 

					dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;
					jRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "InsertTmpConsignment", "INF001", iRowsAffected + " rows delete from tmp_Consignment table");
 
					foreach (DataRow DR in dtConsignment.Rows)
					{
						return_pod_hc = "";
						return_invoice_hc = "";
						cod_amount = 0;
						declare_value = 0;

						if (DR["return_pod_hc"].ToString().Trim() == "Y") { return_pod_hc = "1"; }  else { return_pod_hc = "0"; }
						if (DR["return_invoice_hc"].ToString().Trim() == "Y") { return_invoice_hc = "1"; }  else { return_invoice_hc = "0"; }
						if (DR["cod_amount"].ToString().Trim() != "") { cod_amount = Convert.ToDecimal(DR["cod_amount"]); }
						if (DR["declare_value"].ToString().Trim() != "") { declare_value = Convert.ToDecimal(DR["declare_value"]); }

						//insert new TmpConsignment
						strBuild = new StringBuilder();
						strBuild.Append("INSERT INTO tmp_consignment ");
						strBuild.Append(" (booking_no, consignment_no, ref_no, payerid, payer_name, ");
						strBuild.Append(" payer_address1, payer_address2, payer_zipcode, payer_telephone, ");
						strBuild.Append(" payer_fax, payment_mode, sender_name, sender_address1, sender_address2, ");
						strBuild.Append(" sender_zipcode, sender_telephone, sender_fax, sender_contact_person, ");
						strBuild.Append(" recipient_name, recipient_address1, recipient_address2, recipient_zipcode, ");
						strBuild.Append(" recipient_telephone, recipient_fax, recipient_contact_person, service_code, ");
						strBuild.Append(" declare_value, payment_type, commodity_code, remark, cod_amount, ");
						strBuild.Append(" return_pod_hc, return_invoice_hc) "); 
						strBuild.Append("VALUES");
						strBuild.Append(" (N'" + DR["booking_no"].ToString() + "'");
						strBuild.Append(" ,N'" + DR["consignment_no"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["ref_no"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["payerid"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["payer_name"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["payer_address1"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["payer_address2"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["payer_zipcode"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["payer_telephone"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["payer_fax"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["payment_mode"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["sender_name"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["sender_address1"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["sender_address2"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["sender_zipcode"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["sender_telephone"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["sender_fax"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["sender_contact_person"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["recipient_name"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["recipient_address1"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["recipient_address2"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["recipient_zipcode"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["recipient_telephone"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["recipient_fax"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["recipient_contact_person"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["service_code"].ToString().Trim() + "'");
						strBuild.Append(" ," + declare_value);
						strBuild.Append(" ,N'" + DR["payment_type"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["commodity_code"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["remark"].ToString().Trim() + "'");
						strBuild.Append(" ," + cod_amount);
						strBuild.Append(" ," + return_pod_hc);
						strBuild.Append(" ," + return_invoice_hc + ")");

						dbCmd.CommandText = strBuild.ToString();
						jRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "InsertTmpConsignment", "INF002", iRowsAffected + " rows insert into tmp_Consignment table"); 
					}
					transactionApp.Commit();
				}
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignment", "INF003", "Database insert transaction rolled back."); 
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignment", "ERR006", "Error during application transaction rollback " + rollbackException.Message.ToString()); 
				}

				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignment", "ERR007", "Insert Transaction Failed : " + appException.Message.ToString());
				throw new ApplicationException("1 Error adding BoxSize ", appException);
			}
			catch(Exception ex)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignment", "INF003", "Database insert transaction rolled back.");
				}
				catch (Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignment", "ERR008", "Error during app transaction roll back " + rollbackException.Message.ToString()); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignment", "ERR009", "Insert Transaction Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error adding BoxSize", ex); 
			}
			finally
			{
				if (conApp != null){conApp.Close();}
			}
			return jRowsAffected;
		}

		public static string InsertTmpPackage(String strAppID, String strEnterpriseID, String PayerID, DataTable dtPackage)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			StringBuilder strBuild = new StringBuilder();
			
			DataSet dsConsignmentNote = new DataSet();
			DataTable dtConsignmentNote = new DataTable();
			DataRow drConsignmentNote = null;			
			int iRowsAffected = 0;
			int jRowsAffected = 0;
			string Telephone = "";
			string CustomerName = "";
			string cntRow = "";
			string ErrMsg = "";			
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpPackage", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}

			IDbConnection conApp = dbCon.GetConnection();
			if (conApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpPackage", "ERR002", "conApp is null");  
				throw new ApplicationException("Connection to database failed", null); 
			}

			try
			{
				dbCon.OpenConnection(ref conApp); 
			}
			catch (ApplicationException appException)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpPackage", "ERR003", "Error opening connection : " + appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ", appException);  
			}
			catch (Exception ex)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpPackage", "ERR004", "Error opening connection : " + ex.Message.ToString());
				throw new ApplicationException("Error opening database connection ", ex);  
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp, IsolationLevel.ReadCommitted);
			if (transactionApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpPackage", "ERR005", "transactionApp is null");
				throw new ApplicationException("Failed begining a application transaction..", null); 
			}

			try
			{
				DateTime dateT = DateTime.Now;
				string ConDate = dateT.Year.ToString();
				ConDate += dateT.Month.ToString().Length == 1 ? "0" + dateT.Month.ToString() : dateT.Month.ToString();
				ConDate += dateT.Day.ToString().Length == 1 ? "0" + dateT.Day.ToString() : dateT.Day.ToString();

				if (dtPackage != null && dtPackage.Rows.Count > 0)
				{
					//delete old TmpPackage 
					strBuild = new StringBuilder();
					strBuild.Append("DELETE FROM tmp_package"); 

					dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "InsertTmpPackage", "INF001", iRowsAffected + " rows delete from tmp_package table");
 

					foreach (DataRow DR in dtPackage.Rows)
					{
						PayerID = "";
						Telephone = "";
						CustomerName = "";
						
						StringBuilder strQry = new StringBuilder();
						strQry = strQry.Append("SELECT * FROM tmp_consignment ");
                        strQry = strQry.Append("WHERE Consignment_No = '" + DR["consignment_no"].ToString() + "'");

						dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
						dsConsignmentNote = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
						
						if ((dsConsignmentNote != null) && (dsConsignmentNote.Tables.Count > 0))
						{
							dtConsignmentNote = dsConsignmentNote.Tables[0];
							if ((dtConsignmentNote != null) && (dtConsignmentNote.Rows.Count == 1))
							{
								drConsignmentNote = dtConsignmentNote.Rows[0];
								PayerID = drConsignmentNote["PayerID"].ToString();
								Telephone = drConsignmentNote["recipient_telephone"].ToString();
								CustomerName = drConsignmentNote["recipient_name"].ToString();
							}
							else
							{
								ErrMsg = "Error about consignment_no data.!\n\r";
							}
						}

						if (ErrMsg == "")
						{
							//insert new TmpPackage
							strBuild = new StringBuilder();
							strBuild.Append("INSERT INTO tmp_package ");
							strBuild.Append(" (consignment_no, PayerID, Seq_no, Telephone, Width, Length, Height, kilo, Qty, ");
							strBuild.Append("  LastUpdateDate, ConsignmentDate, flag_Export, CustomerName) "); 
							strBuild.Append("VALUES");
							strBuild.Append(" ('" + DR["consignment_no"].ToString() + "'");
							strBuild.Append(" ,N'" + PayerID + "'");
							if (DR["mps_code"].ToString().Trim() != "")
							{
								strBuild.Append(" ," + DR["mps_code"].ToString().Trim());
							}
							else
							{
								strBuild.Append(" ,null");
							}
							strBuild.Append(" ,N'" + Telephone + "'");
							if (DR["pkg_breadth"].ToString().Trim() != "")
							{
								strBuild.Append(" ," + DR["pkg_breadth"].ToString().Trim());
							}
							else
							{
								strBuild.Append(" ,null");
							}
							if (DR["pkg_length"].ToString().Trim() != "")
							{
								strBuild.Append(" ," + DR["pkg_length"].ToString().Trim());
							}
							else
							{
								strBuild.Append(" ,null");
							}
							if (DR["pkg_height"].ToString().Trim() != "")
							{
								strBuild.Append(" ," + DR["pkg_height"].ToString().Trim());
							}
							else
							{
								strBuild.Append(" ,null");
							}
							if (DR["pkg_wt"].ToString().Trim() != "")
							{
								strBuild.Append(" ," + DR["pkg_wt"].ToString().Trim());
							}
							else
							{
								strBuild.Append(" ,null");
							}
							if (DR["pkg_qty"].ToString().Trim() != "")
							{
								strBuild.Append(" ," + DR["pkg_qty"].ToString().Trim());
							}
							else
							{
								strBuild.Append(" ,null");
							}
							strBuild.Append(" ,N'" + dateT + "'");
							strBuild.Append(" ,'" + ConDate + "'");
							strBuild.Append(" ,null");
							strBuild.Append(" ,N'" + CustomerName + "')");

							dbCmd.CommandText = strBuild.ToString();
							if (dbCmd.Connection.State == ConnectionState.Closed) {dbCmd.Connection = conApp;}
							dbCmd.Transaction = transactionApp;
							jRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							cntRow = jRowsAffected.ToString(); 
							Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "InsertTmpPackage", "INF002", iRowsAffected + " rows insert into tmp_package table"); 
						
						}
					}
					transactionApp.Commit();
				}
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpPackage", "INF003", "Database insert transaction rolled back."); 
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpPackage", "ERR006", "Error during application transaction rollback " + rollbackException.Message.ToString()); 
				}

				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpPackage", "ERR007", "Insert Transaction Failed : " + appException.Message.ToString());
				throw new ApplicationException("1 Error adding BoxSize ", appException);
			}
			catch(Exception ex)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("TIESDAL", "ConsignmentNoteDAL", "InsertTmpPackage", "INF003", "Database insert transaction rolled back.");
				}
				catch (Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpPackage", "ERR008", "Error during app transaction roll back " + rollbackException.Message.ToString()); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpPackage", "ERR009", "Insert Transaction Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error adding BoxSize", ex); 
			}
			finally
			{
				if (conApp != null){conApp.Close();}

				dsConsignmentNote.Dispose();
				dtConsignmentNote.Dispose();
			}

			if (ErrMsg != "")
			{
				return ErrMsg;
			}
			else
			{
				return cntRow;
			}
		}

		public static DataSet GetTmpConsignment(String strAppID,String strEnterpriseID, String PayerID)
		{
			string strSQL = "";
			DataSet DS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetTmpConsignment", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}
			try
			{
				strSQL = "SELECT * FROM tmp_consignment WHERE PayerID = '" + PayerID + "'";
				DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
				return DS;
			}
			catch
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetTmpConsignment", "Select command failed");
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				DS.Dispose();
			}
		}
		
		public static DataSet GetTmpPackage(String strAppID,String strEnterpriseID, String PayerID)
		{
			string strSQL = "";
			DataSet DS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetTmpPackage", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}
			try
			{
				strSQL = "SELECT * FROM tmp_package WHERE PayerID = '" + PayerID + "'";
				DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
				return DS;
			}
			catch
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetTmpPackage", "Select command failed");
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static DataSet SearchPayerID(String strAppID,String strEnterpriseID, String PayerID)
		{
			string strSQL = "";
			DataSet DS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchPayerID", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}
			try
			{
				strSQL = "SELECT * FROM customer WHERE UPPER(custid) = '" + PayerID + "'";
				DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
				return DS;
			}
			catch
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchPayerID", "Select command failed");
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static DataSet SearchZipcode(String strAppID,String strEnterpriseID, String Zipcode)
		{
			string strSQL = "";
			DataSet DS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchZipcode", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}
			try
			{
				strSQL = "SELECT * FROM zipcode WHERE zipcode = '" + Zipcode + "'";
				DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
				return DS;
			}
			catch
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchZipcode", "Select command failed");
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static DataSet SearchStatecode(String strAppID,String strEnterpriseID, String Zipcode)
		{
			string strSQL = "";
			DataSet DS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchZipcode", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}
			try
			{
				strSQL = "SELECT z.*,s.state_name FROM zipcode z ";
				strSQL += "Left Join State s ON ";
				strSQL += "z.state_code=s.state_code ";
				strSQL += "WHERE z.zipcode = '" + Zipcode + "' ";
				DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
				return DS;
			}
			catch
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchZipcode", "Select command failed");
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static DataSet SearchReference2(String appID, String enterpriseID, String PayerID, String RecipientName, String CustomerCode)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
  
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchReference2", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT * FROM [REFERENCES2] WHERE PayerID = N'" + PayerID + "'"); 
			if (RecipientName != "")
			{
				strQry = strQry.Append(" AND reference_name = N'" + RecipientName + "' ");
			}
			if (CustomerCode != "")
			{
				strQry = strQry.Append(" AND CustomerCode = N'" + CustomerCode + "' ");
			}

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);

			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchReference2", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static DataSet SearchService(String appID, String enterpriseID, String ServiceCode)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
  
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchService", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT * FROM service "); 
			if (ServiceCode != "")
			{
				strQry = strQry.Append(" WHERE UPPER(service_code) = '" + ServiceCode + "' ");
			}

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);

			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchService", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static string InsertTmpConsignmentToTable(String strAppID, String strEnterpriseID, String PayerID, String UserID, DataTable dtConsignment, DataTable dtPackage)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd =  null;
			StringBuilder strBuild = new StringBuilder(); 
			int iRowsAffected = 0;
			int jRowsAffected = 0;
			int kRowsAffected = 0;
			int mRowsAffected = 0;
			int nRowsAffected = 0;

			DataSet DS = new DataSet();
			DataTable DT = new DataTable(); 
			DataSet DS1 = new DataSet();
			DataTable DT1 = new DataTable(); 
			DataSet DS2 = new DataSet();
			DataTable DT2 = new DataTable(); 
			string consignment_no = "";
			string HCReturn = "";
			string InvReturn = "";
			string recipient_address = "";
			string CustomerCode = "";
			int newConsignmentNo = 0;
			string strMsg  = "";

			string recipient_name = "";
			string Boxsize = "";
			string BoxType = "";
			string Description = "";

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}

			IDbConnection conApp = dbCon.GetConnection(); 
			if (conApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "ERR002", "conApp is null");  
				throw new ApplicationException("Connection to database failed", null); 
			}

			try
			{
				dbCon.OpenConnection(ref conApp); 
			}
			catch(ApplicationException appException)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "ERR003", "Error opening connection : " + appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ", appException); 
			}
			catch(Exception ex)
			{
				if (conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "ERR004", "Error opening connection : " + ex.Message.ToString());
				throw new ApplicationException("Error opening database connection ", ex); 
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp, IsolationLevel.ReadCommitted);
			if (transactionApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "ERR005", "transactionApp is null");
				throw new ApplicationException("Failed begining a application transaction..", null); 
			}

			try
			{
				//************************ Insert data from tmp_Consignment to tb_Consignment **************************
				if ((dtConsignment != null) && (dtConsignment.Rows.Count > 0))
				{
					DateTime dateT = DateTime.Now;
					string ConDate = dateT.Year.ToString();
					ConDate += dateT.Month.ToString().Length == 1 ? "0" + dateT.Month.ToString() : dateT.Month.ToString();
					ConDate += dateT.Day.ToString().Length == 1 ? "0" + dateT.Day.ToString() : dateT.Day.ToString();

					foreach (DataRow drConsignment in dtConsignment.Rows)
					{
						DS = null;
						consignment_no = "";
						HCReturn = "";
						InvReturn = "";
						recipient_address = "";
						CustomerCode = "";

						consignment_no = drConsignment["consignment_no"].ToString().Trim();

						if (consignment_no != "")
						{
							HCReturn = drConsignment["return_pod_hc"].ToString().Trim();
							InvReturn = drConsignment["return_invoice_hc"].ToString().Trim();
							if (HCReturn.ToUpper() == "TRUE")  { HCReturn = "1"; }   
							else  { HCReturn = "0"; }
							if (InvReturn.ToUpper() == "TRUE") { InvReturn = "1"; }  
							else  { InvReturn = "0"; }
							recipient_address = drConsignment["recipient_address1"].ToString().Trim();
							if (recipient_address != "") { recipient_address += " "; }
							recipient_address += drConsignment["recipient_address2"].ToString().Trim();

							DS = SearchReference2(strAppID, strEnterpriseID, PayerID, drConsignment["recipient_name"].ToString().Trim(), "");
							if ((DS != null) && (DS.Tables.Count > 0) && (DS.Tables[0] != null))
							{
								DT = DS.Tables[0];
								if ((DT != null) && (DT.Rows.Count == 1)) {CustomerCode = DT.Rows[0]["CustomerCode"].ToString(); }
							}

							//delete before insert ConsignmentNote in tb_ConsignmentNote
							strBuild = new StringBuilder();
							strBuild.Append("DELETE FROM tb_consignmentNote WHERE Consignment_No = '" + consignment_no + "'"); 

							dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
							dbCmd.Connection = conApp;
							dbCmd.Transaction = transactionApp;
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "INF001", iRowsAffected + " rows delete into tb_ConsignmentNote table");
 

							//insert
							strBuild = new StringBuilder();
							strBuild.Append("INSERT INTO tb_consignmentNote");
							strBuild.Append(" (Enterpriseid, Consignment_No, PayerID, Telephone, LastUserID, HCReturn, InvReturn, ");
							strBuild.Append("  CODAmount, LastUpdateDate, ConsignmentDate, ServiceType, DeclareValue, ");
							strBuild.Append("  recipion_contact_person, cust_ref, flag_Export, Special_Instruction, ");
							strBuild.Append("  address, company_name, CustomerCode, Sender_Name, Sender_Tel)");
							strBuild.Append(" VALUES ");
							strBuild.Append(" ('" + strEnterpriseID + "'");
							strBuild.Append(" ,N'" + drConsignment["Consignment_No"].ToString().Trim() + "'");
							strBuild.Append(" ,N'" + drConsignment["PayerID"].ToString().Trim() + "'");
							strBuild.Append(" ,N'" + drConsignment["recipient_telephone"].ToString().Trim() + "'");
							strBuild.Append(" ,N'" + UserID + "'");
							strBuild.Append(" ," + HCReturn);
							strBuild.Append(" ," + InvReturn);
							strBuild.Append(" ," + drConsignment["cod_amount"].ToString().Trim());
							strBuild.Append(" ,'" + dateT + "'");
							strBuild.Append(" ,'" + ConDate + "'");
							strBuild.Append(" ,N'" + drConsignment["service_code"].ToString().Trim() + "'");
							strBuild.Append(" ," + drConsignment["declare_value"].ToString().Trim());
							strBuild.Append(" ,N'" + drConsignment["recipient_contact_person"].ToString().Trim() + "'");
							strBuild.Append(" ,N'" + drConsignment["ref_no"].ToString().Trim() + "'");
							strBuild.Append(" ,null");
							strBuild.Append(" ,N'" + drConsignment["remark"].ToString().Trim() + "'");
							strBuild.Append(" ,N'" + recipient_address + "'");
							strBuild.Append(" ,N'" + drConsignment["recipient_name"].ToString().Trim() + "'");
							strBuild.Append(" ,N'" + CustomerCode + "'");
							strBuild.Append(" ,N'" + drConsignment["sender_name"].ToString().Trim() + "'");
							strBuild.Append(" ,N'" + drConsignment["sender_telephone"].ToString().Trim() + "')");

							dbCmd.CommandText = strBuild.ToString();
							jRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "INF002", jRowsAffected + " rows insert into tb_ConsignmentNote table"); 
						}
						else
						{
							strMsg = "No consignment_no.";
						}   //end check consignment_no != ""
					}       //end loop dtConsignment

					//update running consignment_no
//					newConsignmentNo = Convert.ToInt32(consignment_no);
//					newConsignmentNo = newConsignmentNo + 1;
//					kRowsAffected = UpdateRunningCon(strAppID, strEnterpriseID, PayerID, newConsignmentNo.ToString());
//					Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "INF003", kRowsAffected + " rows update into tb_Running table"); 

					
				//************************ Insert data from tmp_Package to tb_ConsignmentDetail ************************
					if ((dtPackage != null) && (dtPackage.Rows.Count > 0))
					{
						foreach (DataRow drPackage in dtPackage.Rows)
						{
							DS1 = null;
							DS2 = null;
							consignment_no = "";
							recipient_name = "";
							CustomerCode = "";
							Boxsize = "";
							BoxType = "";
							Description = "";
						
							consignment_no = drPackage["consignment_no"].ToString().Trim();
							if (consignment_no != "")
							{
								//delete package
								strBuild = new StringBuilder();
								strBuild.Append("DELETE FROM TB_CONSIGNMENTNOTEDETAIL ");
								strBuild.Append("WHERE CONSIGNMENT_NO = '" + consignment_no + "'");

								dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
								dbCmd.Connection = conApp;
								dbCmd.Transaction = transactionApp;
								mRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "INF004", mRowsAffected + " rows delete into tb_consignmentNoteDetail table");

								//Search Boxsize
								DS1 = SearchBoxSize(strAppID, strEnterpriseID, PayerID, "", drPackage["Length"].ToString().Trim(), drPackage["Width"].ToString().Trim(), drPackage["Height"].ToString().Trim());
								if ((DS1 != null) && (DS1.Tables.Count > 0) && (DS1.Tables[0] != null))
								{
									DT1 = DS1.Tables[0];
									if ((DT1 != null) && (DT1.Rows.Count == 1)) 
									{
										BoxType = "S";
										Boxsize = DT1.Rows[0]["Box_SizeID"].ToString(); 
									}
									else
									{
										BoxType = "C";
									}
								}

								//find CustomerCode 
								DataRow[] filterRows = dtConsignment.Select("consignment_no = '" + consignment_no + "'");
								recipient_name = filterRows[0]["recipient_name"].ToString().Trim();  
								DS2 = SearchReference2(strAppID, strEnterpriseID, PayerID, recipient_name, "");
								if ((DS2 != null) && (DS2.Tables.Count > 0) && (DS2.Tables[0] != null))
								{
									DT2 = DS2.Tables[0];
									if ((DT2 != null) && (DT2.Rows.Count == 1)) { CustomerCode = DT2.Rows[0]["CustomerCode"].ToString(); }
								}


								//Description
								Description = Utility.BoxDescFormat(String.Format("{0:#.##}", drPackage["Width"])) + " x " + Utility.BoxDescFormat(String.Format("{0:#.##}", drPackage["Length"])) + " x " + Utility.BoxDescFormat(String.Format("{0:#.##}", drPackage["Height"]));  

								strBuild = new StringBuilder();
								strBuild.Append("INSERT INTO TB_CONSIGNMENTNOTEDETAIL ");
								strBuild.Append("  (CONSIGNMENT_NO, PAYERID, SEQ_NO, TELEPHONE, ");
								strBuild.Append("  BOXTYPE, [DESCRIPTION], BOXSIZE, KILO, QTY, ");
								strBuild.Append("  LASTUPDATEDATE, CONSIGNMENTDATE, FLAG_EXPORT, CUSTOMERCODE, FLAG_DELETE ) "); 
								strBuild.Append(" VALUES "); 
								strBuild.Append("  ('" + consignment_no + "','" + PayerID + "'," + drPackage["Seq_No"] + ",");

								//Telephone
								if ((drPackage["Telephone"] != null) && (!drPackage["Telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									strBuild.Append("N'" + drPackage["Telephone"].ToString()  + "',"); 
								}
								else
								{
									strBuild.Append("NULL,"); 
								}

								strBuild.Append("  N'" + BoxType + "',"); 
								strBuild.Append("  N'" + Description + "',"); 

								//BOXSIZE
								if (Boxsize != "")
								{
									strBuild.Append("N'" + Boxsize + "',");
								}
								else
								{
									strBuild.Append("NULL,"); 
								}

								//KILO
								if ((drPackage["Kilo"] != null) && (!drPackage["Kilo"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									strBuild.Append("'" + drPackage["Kilo"].ToString()  + "',"); 
								}
								else
								{
									strBuild.Append("NULL,"); 
								}

								//QTY
								if ((drPackage["Qty"] != null) && (!drPackage["Qty"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									strBuild.Append("'" + drPackage["Qty"].ToString()  + "',"); 
								}	
								else
								{
									strBuild.Append("NULL,"); 
								}

								//LastUpdateDate
								if ((drPackage["LastUpdateDate"] != null) && (!drPackage["LastUpdateDate"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									strBuild.Append("'" + drPackage["LastUpdateDate"].ToString() + "',");
								}	
								else
								{
									strBuild.Append("NULL,"); 
								}

								//ConsignmentDate
								if ((drPackage["ConsignmentDate"] != null) && (!drPackage["ConsignmentDate"].GetType().Equals(System.Type.GetType("System.DBNull"))))
								{
									strBuild.Append("'" + drPackage["ConsignmentDate"].ToString() + "',"); 
								}	
								else
								{
									strBuild.Append("NULL,"); 
								}

								strBuild.Append("NULL,"); 
								strBuild.Append("N'" + CustomerCode + "',");
								strBuild.Append("NULL)");

								dbCmd.CommandText = strBuild.ToString();
								nRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								Logger.LogTraceInfo("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "INF004", nRowsAffected + " rows insert into tb_ConsignmentNoteDetail table"); 
							}
							else
							{
								strMsg = "No consignment_no.";
							}
						}  //end loop dtPackage
					}  //end if dtPackage != null


					if (strMsg != "")
					{
						transactionApp.Rollback();
					}
					else
					{
						transactionApp.Commit();
					}

				} //end if dtConsignment != null
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "INF005", "Database insert transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "ERR006", "Error during application transaction rollback " + rollbackException.Message.ToString()); 
				}

				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "ERR007", "Insert Transaction Failed : " + appException.Message.ToString());
				throw new ApplicationException("1 Error adding ConsignmentNote ", appException);
			}
			catch (Exception ex)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "INF006", "Database insert transaction rolled back.");
				}
				catch (Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "ERR008", "Error during app transaction roll back " + rollbackException.Message.ToString()); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "ERR009", "Insert Transaction Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error adding ConsignmentNote", ex); 
			}
			finally
			{
				if (conApp != null){conApp.Close();}

				DS.Dispose();
				DT.Dispose(); 
				DS1.Dispose();
				DT1.Dispose(); 
				DS2.Dispose();
				DT2.Dispose(); 
			}
			return strMsg;
		}

		public static DataSet GetExportConsignment(String appID, String enterpriseID, String PayerID)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetExportConsignment", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT null as booking_no, t1.consignment_no, t1.cust_ref as ref_no, t1.payerid, ");
			strQry = strQry.Append("t2.cust_name as payer_name, t2.address1 as payer_address1, t2.address2 as payer_address2, ");
			strQry = strQry.Append("t2.zipcode as payer_zipcode, t2.telephone as payer_telephone, t2.fax as payer_fax, ");
			strQry = strQry.Append("t2.payment_mode as payment_mode, csr.snd_rec_name as sender_name, csr.address1 as sender_address1, ");
			strQry = strQry.Append("csr.address2 as sender_address2, csr.zipcode as sender_zipcode, csr.telephone as sender_telephone, ");
			strQry = strQry.Append("csr.fax as sender_fax, csr.contact_person as sender_contact_person, t3.reference_name as recipient_name, ");
			strQry = strQry.Append("t3.address1 as recipient_address1, t3.address2 as recipient_address2, t3.zipcode as recipient_zipcode, ");
			strQry = strQry.Append("t3.telephone as recipient_telephone, null as recipient_fax, ");
			strQry = strQry.Append("t1.recipion_contact_person as recipient_contact_person, t1.servicetype as service_code, ");
			strQry = strQry.Append("Convert(decimal(18,2),t1.DeclareValue) as declare_value, 'FP' as payment_type, null as commodity_code, ");
			strQry = strQry.Append("ltrim(rtrim(t1.special_instruction)) as remark, Convert(decimal(18,2),CODAmount) as cod_amount, ");
			strQry = strQry.Append("(case when t1.HCReturn = 1 then 'Y' else 'N' end) as return_pod_hc , ");
			strQry = strQry.Append("(case when t1.invreturn = 1 then 'Y' else 'N' end) as return_invoice_hc  ");
			strQry = strQry.Append("FROM tb_consignmentNote t1 ");
			strQry = strQry.Append("inner join customer t2 on t1.payerid = t2.custid ");
			strQry = strQry.Append("and t1.enterpriseid = t2.enterpriseid ");
			strQry = strQry.Append("inner join customer_snd_rec csr on t1.payerid = csr.custid and t1.sender_name = csr.snd_rec_name ");
			strQry = strQry.Append("and t1.enterpriseid = csr.enterpriseid ");
			strQry = strQry.Append("inner join [References2] t3 on t1.customercode = t3.customercode ");
			strQry = strQry.Append("and t1.company_name = t3.reference_name ");
			strQry = strQry.Append("and t1.enterpriseid = t3.enterpriseid ");
			strQry = strQry.Append("and t1.payerid = t3.payerid ");
			strQry = strQry.Append("WHERE t1.flag_export is null ");
			strQry = strQry.Append("and t1.flag_Delete is null ");
			strQry = strQry.Append("and t1.enterpriseid = '" + enterpriseID + "' ");			
			strQry = strQry.Append("and t1.payerid = N'" + PayerID + "' ");
			strQry = strQry.Append("ORDER BY t1.consignment_no desc");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetExportConsignment", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static DataSet GetExportPackage(String appID, String enterpriseID, String PayerID)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetExportPackage", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
//			strQry = strQry.Append("SELECT t1.consignment_no AS consignment_no, t1.seq_no AS mps_code,  ");
//			strQry = strQry.Append("  t2.box_length AS pkg_length, t2.box_wide AS pkg_breadth, "); 
//			strQry = strQry.Append("  t2.box_height AS pkg_height, t1.kilo AS pkg_wt, t1.qty AS pkg_qty  ");
//			strQry = strQry.Append("FROM tb_consignmentNoteDetail t1 ");
//			strQry = strQry.Append(" LEFT OUTER JOIN tb_boxsize AS t2  ON (t1.boxsize = t2.box_sizeid)  ");
//			strQry = strQry.Append(" LEFT OUTER JOIN tb_consignmentNote AS t3  ON (t2.payerid = t3.payerid) ");
//			strQry = strQry.Append(" AND (t1.consignment_no = t3.consignment_no) ");
//			strQry = strQry.Append(" AND (t1.customercode = t3.customercode) ");
//			strQry = strQry.Append(" LEFT OUTER JOIN tb_chargerate AS t4  ON (t3.servicetype = t4.service_code)  ");
//			strQry = strQry.Append(" AND (t1.boxsize = t4.box_sizeid)  ");
//			strQry = strQry.Append("WHERE t1.payerid = '" + PayerID + "' and t1.flag_Delete is null ");
//			strQry = strQry.Append(" and t1.flag_export is null ");
//			strQry = strQry.Append("ORDER BY t1.consignment_no, t1.seq_no");

			strQry = strQry.Append("SELECT C.CONSIGNMENT_NO As consignment_no, C.SEQ_NO As mps_code, "); 				
			strQry = strQry.Append("(CASE WHEN C.BoxType = 'C' THEN LTRIM(SUBSTRING([DESCRIPTION], 7, 3)) ");
			strQry = strQry.Append("      WHEN C.BoxType = 'S' THEN CONVERT(VARCHAR,B.BOX_LENGTH) END) AS pkg_length, ");
			strQry = strQry.Append("(CASE WHEN C.BoxType = 'C' THEN LTRIM(SUBSTRING([DESCRIPTION], 1, 3)) ");
			strQry = strQry.Append("      WHEN C.BoxType = 'S' THEN CONVERT(VARCHAR,B.BOX_WIDE) END) AS pkg_breadth, ");			
			strQry = strQry.Append("(CASE WHEN C.BoxType = 'C' THEN LTRIM(SUBSTRING([DESCRIPTION], 13, 3)) ");
			strQry = strQry.Append("      WHEN C.BoxType = 'S' THEN CONVERT(VARCHAR,B.BOX_HEIGHT)  END) AS pkg_height, ");
			strQry = strQry.Append("C.KILO As pkg_wt, C.QTY AS pkg_qty ");
			strQry = strQry.Append("FROM tb_ConsignmentNoteDetail C LEFT JOIN TB_BOXSIZE B ");
			strQry = strQry.Append("ON (C.BOXSIZE = B.BOX_SIZEID and C.PayerID = B.PayerID) ");
			strQry = strQry.Append("WHERE C.FLAG_DELETE IS NULL "); 
			strQry = strQry.Append("AND C.PayerID = '" + PayerID + "' "); 
			strQry = strQry.Append(" AND C.flag_Export IS NULL ");
			strQry = strQry.Append("ORDER BY C.consignment_no, C.seq_no");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetPrintReport", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static int UpdateConsignmentFlagExport(String strAppID, String strEnterpriseID, String PayerID)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd =  null;
			StringBuilder strBuild = new StringBuilder(); 
			int iRowsAffected = 0;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentFlagExport", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}
		
			try
			{
				strBuild = new StringBuilder();

				strBuild.Append(" UPDATE tb_consignmentNote SET flag_export = 'EX' WHERE flag_Delete is null and payerid = '" + PayerID + "' ");
				strBuild.Append(" UPDATE tb_consignmentNoteDetail SET flag_export = 'EX' WHERE flag_Delete is null and payerid = '" + PayerID + "' ");

				dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd); 
			}
			catch (Exception ex)
			{
				throw new ApplicationException("Error updating consignment export flag ", ex); 
			}
			return iRowsAffected;
		}

		public static int InsertTmpReference2(String strAppID, String strEnterpriseID, String PayerID, DataTable DT)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			StringBuilder strBuild = new StringBuilder();
			int iRowsAffected = 0;
			int jRowsAffected = 0;
			
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpReference2", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}

			IDbConnection conApp = dbCon.GetConnection();
			if (conApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpReference2", "ERR002", "conApp is null");  
				throw new ApplicationException("Connection to database failed", null); 
			}

			try
			{
				dbCon.OpenConnection(ref conApp); 
			}
			catch (ApplicationException appException)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpReference2", "ERR003", "Error opening connection : " + appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ", appException);  
			}
			catch (Exception ex)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpReference2", "ERR004", "Error opening connection : " + ex.Message.ToString());
				throw new ApplicationException("Error opening database connection ", ex);  
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp, IsolationLevel.ReadCommitted);
			if (transactionApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpReference2", "ERR005", "transactionApp is null");
				throw new ApplicationException("Failed begining a application transaction..", null); 
			}

			try
			{
				if ((DT != null) && (DT.Rows.Count > 0))
				{
					//delete old tmpReferences2 
					strBuild = new StringBuilder();
					strBuild.Append("DELETE FROM tmp_References2"); 

					dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;
					jRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "InsertTmpReference2", "INF001", iRowsAffected + " rows delete from tmp_References2 table");
 
					foreach (DataRow DR in DT.Rows)
					{
						//insert new tmpReferences2
						strBuild = new StringBuilder();
						strBuild.Append("INSERT INTO tmp_References2 ");
						strBuild.Append(" (CustomerCode, reference_name, address1, address2, zipcode, telephone, contactperson, PayerID) "); 
						strBuild.Append("VALUES");
						strBuild.Append(" (N'" + DR["CUSCODE"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["CUSNAME"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["ADDR01"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["ADDR02"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["ZIPCODE"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["TELNUM"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + DR["CONTACT"].ToString().Trim() + "'");
						strBuild.Append(" ,N'" + PayerID + "')"); 
						dbCmd.CommandText = strBuild.ToString();
						jRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "InsertTmpReference2", "INF002", iRowsAffected + " rows insert into tmp_Reference2 table"); 
					}
					transactionApp.Commit();
				}//end if check DT != null
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpReference2", "INF003", "Database insert transaction rolled back."); 
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpReference2", "ERR006", "Error during application transaction rollback " + rollbackException.Message.ToString()); 
				}

				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpReference2", "ERR007", "Insert Transaction Failed : " + appException.Message.ToString());
				throw new ApplicationException("1 Error adding TmpReference2 ", appException);
			}
			catch(Exception ex)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("TIESDAL", "ConsignmentNoteDAL", "InsertTmpReference2", "INF003", "Database insert transaction rolled back.");
				}
				catch (Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpReference2", "ERR008", "Error during app transaction roll back " + rollbackException.Message.ToString()); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpReference2", "ERR009", "Insert Transaction Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error adding TmpReference2", ex); 
			}
			finally
			{
				if (conApp != null){conApp.Close();}
			}
			return jRowsAffected;
		}

		public static DataSet GetTmpReference2(String strAppID,String strEnterpriseID, String PayerID)
		{
			string strSQL = "";
			DataSet DS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetTmpReference2", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}

			try
			{
				strSQL = "SELECT * FROM tmp_References2 WHERE PayerID = '" + PayerID + "'";
				DS = (DataSet)dbCon.ExecuteQuery(strSQL, ReturnType.DataSetType);
				return DS;
			}
			catch
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetTmpReference2", "Select command failed");
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static string InsertTmpReferences2ToTable(String strAppID, String strEnterpriseID, String PayerID, DataTable DT)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd =  null;
			StringBuilder strBuild = new StringBuilder(); 
			int iRowsAffected = 0;
			int jRowsAffected = 0;
			string CustomerCode = "";
			string strMsg  = "";

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpReferences2ToTable", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}

			IDbConnection conApp = dbCon.GetConnection(); 
			if (conApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpReferences2ToTable", "ERR002", "conApp is null");  
				throw new ApplicationException("Connection to database failed", null); 
			}

			try
			{
				dbCon.OpenConnection(ref conApp); 
			}
			catch(ApplicationException appException)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpReferences2ToTable", "ERR003", "Error opening connection : " + appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ", appException); 
			}
			catch(Exception ex)
			{
				if (conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpReferences2ToTable", "ERR004", "Error opening connection : " + ex.Message.ToString());
				throw new ApplicationException("Error opening database connection ", ex); 
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp, IsolationLevel.ReadCommitted);
			if (transactionApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpReferences2ToTable", "ERR005", "transactionApp is null");
				throw new ApplicationException("Failed begining a application transaction..", null); 
			}

			try
			{
				if ((DT != null) && (DT.Rows.Count > 0))
				{
					foreach (DataRow DR in DT.Rows)
					{
						CustomerCode = DR["CustomerCode"] + "";
						if (CustomerCode != "")
						{
							//delete before insert into tsble Reference2 
							strBuild = new StringBuilder();
							strBuild.Append("DELETE FROM References2 WHERE CustomerCode = N'" + CustomerCode + "'"); 

							dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
							dbCmd.Connection = conApp;
							dbCmd.Transaction = transactionApp;
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "INF001", iRowsAffected + " rows delete into References2 table");
 

							//insert
							strBuild = new StringBuilder();
							strBuild.Append("INSERT INTO References2");
							strBuild.Append("  (Applicationid, Enterpriseid, CustomerCode, reference_name, ");
							strBuild.Append("  address1, address2, zipcode, telephone, contactperson, PayerID, Flag_Delete)"); 
							strBuild.Append(" VALUES ");
							strBuild.Append("  ('" + strAppID + "'");
							strBuild.Append("  , '" + strEnterpriseID + "'");
							strBuild.Append("  , N'" + CustomerCode + "'");
							strBuild.Append("  , N'" + DR["reference_name"].ToString().Trim() + "'");
							strBuild.Append("  , N'" + DR["address1"].ToString().Trim() + "'");
							strBuild.Append("  , N'" + DR["address2"].ToString().Trim() + "'");
							strBuild.Append("  , N'" + DR["zipcode"].ToString().Trim() + "'");
							strBuild.Append("  , N'" + DR["telephone"].ToString().Trim() + "'");
							strBuild.Append("  , N'" + DR["contactperson"].ToString().Trim() + "'");
							strBuild.Append("  , '" + PayerID + "'");
							strBuild.Append("  , null)");
							
							dbCmd.CommandText = strBuild.ToString();
							jRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "INF002", jRowsAffected + " rows insert into References2 table"); 
						}
//						else
//						{
//							strMsg = "No CustomerCode.";
//						}
					}
					transactionApp.Commit();
				}				
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "INF003", "Database insert transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "ERR006", "Error during application transaction rollback " + rollbackException.Message.ToString()); 
				}

				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "ERR007", "Insert Transaction Failed : " + appException.Message.ToString());
				throw new ApplicationException("1 Error adding ConsignmentNote ", appException);
			}
			catch (Exception ex)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "INF004", "Database insert transaction rolled back.");
				}
				catch (Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "ERR008", "Error during app transaction roll back " + rollbackException.Message.ToString()); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "InsertTmpConsignmentToTable", "ERR009", "Insert Transaction Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error adding ConsignmentNote", ex); 
			}
			finally
			{
				if (conApp != null){conApp.Close();}
			}
			return strMsg;
		}

		public static DataSet SearchBestService(String appID, String enterpriseID, String OriginDC, String RecipientZipCode)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
  
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchBestService", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT * FROM Best_Service_Available ");
			strQry = strQry.Append("WHERE enterpriseid = N'" + enterpriseID + "' ");// AND recipientZipCode = '77110'");
			if (OriginDC != "")
			{
				strQry = strQry.Append(" AND origin_dc = N'" + OriginDC + "' ");
			}
			if (RecipientZipCode != "")
			{
				strQry = strQry.Append(" AND recipientZipCode = N'" + RecipientZipCode + "' ");
			}

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchBestService", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		public static DataSet GetPossibleService(String appID, String enterpriseID, String BestService)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
  
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetPossibleService", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			if (BestService != "")
			{
				//Get Posible Service
				strQry = strQry.Append("SELECT possible.* ");
				strQry = strQry.Append(" FROM ");
				strQry = strQry.Append("  (SELECT CONVERT(NUMERIC,REPLACE(CONVERT(CHAR(5),commit_time,8),':','.')) + transit_day * 24 AS service_hour "); 
				strQry = strQry.Append("    FROM service ");
				strQry = strQry.Append("    where service_code = '" + BestService + "')AS best, "); 
				strQry = strQry.Append("  (SELECT  *, CONVERT(numeric,REPLACE(CONVERT(CHAR(5),commit_time,8),':','.')) + transit_day * 24 AS service_hour "); 
				strQry = strQry.Append("    FROM service)  AS possible ");
				strQry = strQry.Append("WHERE possible.service_hour >= best.service_hour ");
				strQry = strQry.Append("ORDER BY possible.service_hour");
			}
			else
			{
				//Get All Service
				strQry = strQry.Append("SELECT  *, CONVERT(numeric,REPLACE(CONVERT(CHAR(5),commit_time,8),':','.')) + transit_day * 24 AS service_hour ");
				strQry = strQry.Append("FROM service ");
				strQry = strQry.Append("ORDER BY service_hour");
			}

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetPossibleService", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}


		public static DataSet GetExportConsignmentByStaff(String appID, String enterpriseID, String PayerID, String strExDate)
		{
			int intYear = 0;
			int intMonth = 0;
			int intDay = 0;
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetExportConsignmentByStaff", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			String[] strTmp = strExDate.Split('/');

			intYear = Convert.ToInt32(strTmp[2]);
			intMonth = Convert.ToInt32(strTmp[1]);
			intDay = Convert.ToInt32(strTmp[0]);
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT null as booking_no, t1.consignment_no, t1.cust_ref as ref_no, t1.payerid, ");
			strQry = strQry.Append("t2.cust_name as payer_name, t2.address1 as payer_address1, t2.address2 as payer_address2, ");
			strQry = strQry.Append("t2.zipcode as payer_zipcode, t2.telephone as payer_telephone, t2.fax as payer_fax, ");
			strQry = strQry.Append("t2.payment_mode as payment_mode, csr.snd_rec_name as sender_name, csr.address1 as sender_address1, ");
			strQry = strQry.Append("csr.address2 as sender_address2, csr.zipcode as sender_zipcode, csr.telephone as sender_telephone, ");
			strQry = strQry.Append("csr.fax as sender_fax, csr.contact_person as sender_contact_person, t3.reference_name as recipient_name, ");
			strQry = strQry.Append("t3.address1 as recipient_address1, t3.address2 as recipient_address2, t3.zipcode as recipient_zipcode, ");
			strQry = strQry.Append("t3.telephone as recipient_telephone, null as recipient_fax, ");
			strQry = strQry.Append("t1.recipion_contact_person as recipient_contact_person, t1.servicetype as service_code, ");
			strQry = strQry.Append("Convert(decimal(18,2),t1.DeclareValue) as declare_value, 'FP' as payment_type, null as commodity_code, ");
			strQry = strQry.Append("ltrim(rtrim(t1.special_instruction)) as remark, Convert(decimal(18,2),CODAmount) as cod_amount, ");
			strQry = strQry.Append("(case when t1.HCReturn = 1 then 'Y' else 'N' end) as return_pod_hc , ");
			strQry = strQry.Append("(case when t1.invreturn = 1 then 'Y' else 'N' end) as return_invoice_hc  ");
			strQry = strQry.Append("FROM tb_consignmentNote t1 ");
			strQry = strQry.Append("inner join customer t2 on t1.payerid = t2.custid ");
			strQry = strQry.Append("and t1.enterpriseid = t2.enterpriseid ");
			strQry = strQry.Append("inner join customer_snd_rec csr on t1.payerid = csr.custid and t1.sender_name = csr.snd_rec_name ");
			strQry = strQry.Append("and t1.enterpriseid = csr.enterpriseid ");
			strQry = strQry.Append("inner join [References2] t3 on t1.customercode = t3.customercode ");
			strQry = strQry.Append("and t1.company_name = t3.reference_name ");
			strQry = strQry.Append("and t1.enterpriseid = t3.enterpriseid ");
			strQry = strQry.Append("and t1.payerid = t3.payerid ");
			strQry = strQry.Append("WHERE t1.flag_Delete is null ");
			strQry = strQry.Append("and t1.enterpriseid = '" + enterpriseID + "' ");			
			strQry = strQry.Append("and t1.payerid = N'" + PayerID + "' ");
			strQry = strQry.Append(" and year(t1.LastUpdateDate) = " + intYear );
			strQry = strQry.Append(" and month(t1.LastUpdateDate) = " + intMonth );
			strQry = strQry.Append(" and day(t1.LastUpdateDate) = " + intDay );
			strQry = strQry.Append(" ORDER BY t1.consignment_no desc");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetExportConsignment", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		
		public static DataSet GetExportPackageByStaff(String appID, String enterpriseID, String PayerID, String strExDate)
		{
			int intYear = 0;
			int intMonth = 0;
			int intDay = 0;
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetExportPackage", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			String[] strTmp = strExDate.Split('/');

			intYear = Convert.ToInt32(strTmp[2]);
			intMonth = Convert.ToInt32(strTmp[1]);
			intDay = Convert.ToInt32(strTmp[0]);

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT C.CONSIGNMENT_NO As consignment_no, C.SEQ_NO As mps_code, "); 				
			strQry = strQry.Append("(CASE WHEN C.BoxType = 'C' THEN LTRIM(SUBSTRING([DESCRIPTION], 7, 3)) ");
			strQry = strQry.Append("      WHEN C.BoxType = 'S' THEN CONVERT(VARCHAR,B.BOX_LENGTH) END) AS pkg_length, ");
			strQry = strQry.Append("(CASE WHEN C.BoxType = 'C' THEN LTRIM(SUBSTRING([DESCRIPTION], 1, 3)) ");
			strQry = strQry.Append("      WHEN C.BoxType = 'S' THEN CONVERT(VARCHAR,B.BOX_WIDE) END) AS pkg_breadth, ");			
			strQry = strQry.Append("(CASE WHEN C.BoxType = 'C' THEN LTRIM(SUBSTRING([DESCRIPTION], 13, 3)) ");
			strQry = strQry.Append("      WHEN C.BoxType = 'S' THEN CONVERT(VARCHAR,B.BOX_HEIGHT)  END) AS pkg_height, ");
			strQry = strQry.Append("C.KILO As pkg_wt, C.QTY AS pkg_qty ");
			strQry = strQry.Append("FROM tb_ConsignmentNoteDetail C LEFT JOIN TB_BOXSIZE B ");
			strQry = strQry.Append("ON (C.BOXSIZE = B.BOX_SIZEID and C.PayerID = B.PayerID) ");
			strQry = strQry.Append("WHERE C.FLAG_DELETE IS NULL "); 
			strQry = strQry.Append("AND C.PayerID = '" + PayerID + "' "); 
			strQry = strQry.Append(" and year(C.LastUpdateDate) = " + intYear );
			strQry = strQry.Append(" and month(C.LastUpdateDate) = " + intMonth );
			strQry = strQry.Append(" and day(C.LastUpdateDate) = " + intDay );
			strQry = strQry.Append(" ORDER BY C.consignment_no, C.seq_no");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "GetPrintReport", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

		
		public static int UpdateCancelExport(String strAppId, String strEnterpriseId, String strPayerId, String strCancelExDate)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd =  null;
			StringBuilder strBuild = new StringBuilder(); 
			int iRowsAffected = 0;
			int jRowsAffected = 0;
			int intYear = 0;
			int intMonth = 0;
			int intDay = 0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId, strEnterpriseId);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateCancelExport", "ERR001", "DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null", null); 
			}

			IDbConnection conApp = dbCon.GetConnection(); 
			if (conApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateCancelExport", "ERR002", "conApp is null");  
				throw new ApplicationException("Connection to database failed", null); 
			}

			try
			{
				dbCon.OpenConnection(ref conApp); 
			}
			catch(ApplicationException appException)
			{
				if (conApp != null)
				{
					conApp.Close(); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateCancelExport", "ERR003", "Error opening connection : " + appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ", appException); 
			}
			catch(Exception ex)
			{
				if (conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateCancelExport", "ERR004", "Error opening connection : " + ex.Message.ToString());
				throw new ApplicationException("Error opening database connection ", ex); 
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp, IsolationLevel.ReadCommitted);
			if (transactionApp == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateCancelExport", "ERR005", "transactionApp is null");
				throw new ApplicationException("Failed begining a application transaction..", null); 
			}

			try
			{
				String[] strTmp = strCancelExDate.Split('/');

				intYear = Convert.ToInt32(strTmp[2]);
				intMonth = Convert.ToInt32(strTmp[1]);
				intDay = Convert.ToInt32(strTmp[0]);

				strBuild = new StringBuilder();

				//update to tb_ConsignmentNote
				strBuild.Append("UPDATE TB_CONSIGNMENTNOTE ");
				strBuild.Append(" SET FLAG_EXPORT = NULL ");
				strBuild.Append(" WHERE ENTERPRISEID = '" + strEnterpriseId + "' ");
				strBuild.Append(" AND PAYERID = '" + strPayerId + "' ");
				strBuild.Append(" AND year(LastUpdateDate) = " + intYear );
				strBuild.Append(" AND month(LastUpdateDate) = " + intMonth );
				strBuild.Append(" AND day(LastUpdateDate) = " + intDay );

				dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "UpdateConsignmentNote", "INF001", iRowsAffected + " rows update into tb_consignmentNote table");

				if (iRowsAffected > 0)
				{
					//update to tb_ConsignmentNoteDetail
					strBuild.Append("UPDATE TB_CONSIGNMENTNOTEDETAIL ");
					strBuild.Append(" SET FLAG_EXPORT = NULL ");
					strBuild.Append(" WHERE PAYERID = '" + strPayerId + "' ");
					strBuild.Append(" AND year(LastUpdateDate) = " + intYear );
					strBuild.Append(" AND month(LastUpdateDate) = " + intMonth );
					strBuild.Append(" AND day(LastUpdateDate) = " + intDay );

					dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;
					jRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("ConsignmentNote", "ConsignmentNoteDAL", "UpdateConsignmentNote", "INF002", jRowsAffected + " rows update into tb_consignmentNoteDetail table");
				}
				transactionApp.Commit();
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "INF004", "Database update transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "ERR006", "Error during application transaction rollback " + rollbackException.Message.ToString()); 
				}

				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "ERR007", "Update Transaction Failed : " + appException.Message.ToString());
				throw new ApplicationException("1 Error updateing ConsignmentNote ", appException);
			}
			catch (Exception ex)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "INF003", "Database update transaction rolled back.");
				}
				catch (Exception rollbackException)
				{
					Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "ERR008", "Error during app transaction roll back " + rollbackException.Message.ToString()); 
				}
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "UpdateConsignmentNote", "ERR009", "Update Transaction Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error updateing ConsignmentNote", ex); 
			}
			finally
			{
				if (conApp != null){conApp.Close();}
			}
			return iRowsAffected;
		}


		public static string ImportCustomerFile(string ApplicationID, string EnterpriseID, string PayerID, string UserID, string ServiceType, string FLAG_EXPORT, string xmlCustomerFile)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(ApplicationID, EnterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SumQtyByConsignment", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			string strQry = "EXEC CustomerFile_Import '"+ApplicationID+"','"+EnterpriseID+"','"+PayerID+"','"+UserID+"','"+ServiceType+"','"+FLAG_EXPORT+"',N'"+xmlCustomerFile+"' ";

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				
				string strReturn = "";
				strReturn = DS.Tables[0].Rows[0][0].ToString();

				return strReturn;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SumQtyByConsignment", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}
	
		
		public static DataSet PrePrintConsNote( String strAppID ,String strEnterpriseID, String strUserLogin, String arrConsignmentsList )
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","ConfigureCustomerSelfServiceMgrDAL","ReportConsignments","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
		
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" EXEC CSS_PrintConsNotes  ");
				strQry.Append(" @enterpriseid  = '"+ strEnterpriseID +"' "); 
				if( strUserLogin != String.Empty)
				{
					strQry.Append(" ,@userloggedin  = '"+ strUserLogin +"' "); 
				}
				if( arrConsignmentsList != String.Empty)
				{
					strQry.Append(" ,@ConsignmentsList  = '"+ arrConsignmentsList +"' "); 
				}
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "ConfigureCustomerSelfServiceMgrDAL", "ReportConsignments", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			}
			
		}

	
		public static DataSet SearchServiceType(String appID, String enterpriseID)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchServiceType", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT * FROM Service ");  
			strQry = strQry.Append(" ORDER BY service_charge_amt asc "); 

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text); 
			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "ConsignmentNoteDAL", "SearchServiceType", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}
	
	}
}
