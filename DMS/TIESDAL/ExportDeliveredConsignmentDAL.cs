using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;
using System.Collections;
using System.Text.RegularExpressions;
using TIESClasses;

namespace TIESDAL
{
	/// <summary>
	/// Summary description for ExportDeliveredConsignmentDAL.
	/// </summary>
	public class ExportDeliveredConsignmentDAL
	{
		


		public ExportDeliveredConsignmentDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static System.Data.DataSet SearchPODs(string strAppID,string strEnterpriseID,string PODDate,string PODDate2,int OnIntlMAWB,int IncludeIntlDocs,int OnlyIfNotExported,string MAWB,string consignment_no, string ExportFormat)
		{
			System.Data.DataSet result = new DataSet();	

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ExportDeliveredConsignmentDAL","SearchPODs","SearchPODs","DbConnection object is null!!");
				return null;
			}

			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			if(PODDate != null && PODDate.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@PODDate",PODDate));
			}
			else
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@PODDate",DBNull.Value));
			}
			if(PODDate2 != null && PODDate2.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@PODDate2",PODDate2));
			}
			else
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@PODDate2",DBNull.Value));
			}
			if(OnIntlMAWB != -1)
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@OnIntlMAWB",OnIntlMAWB));
			}
			if(IncludeIntlDocs != -1)
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@IncludeIntlDocs",IncludeIntlDocs));
			}
			if(OnlyIfNotExported != -1)
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@OnlyIfNotExported",OnlyIfNotExported));
			}
			if(MAWB != null && MAWB.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@MAWB",MAWB));
			}
			if(consignment_no != null && consignment_no.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",consignment_no));
			}
			if(ExportFormat != null && ExportFormat.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ExportFormat",ExportFormat));
			}
			System.Data.SqlClient.SqlParameter outPutParameter = new System.Data.SqlClient.SqlParameter();
			outPutParameter.ParameterName = "@RowCount";
			outPutParameter.SqlDbType = System.Data.SqlDbType.Int;
			outPutParameter.Direction = System.Data.ParameterDirection.Output;
			storedParams.Add(outPutParameter);

			result = (DataSet)dbCon.ExecuteProcedure("dbo.SearchPODs",storedParams,ReturnType.DataSetType);
			
			return result;
		}
	
		public static System.Data.DataSet ExportPODs(string strAppID,string strEnterpriseID,string strUserloggedin,string strConsignment,string ExportFormat)
		{
			DataSet dsReturn = new DataSet();	
			string SQLText = string.Empty;
		    IDbCommand dbCmd = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ExportDeliveredConsignmentDAL","ExportPODs","ExportPODs","DbConnection object is null!!");
				return null;
			}
			try
			{
				if(strAppID.Trim().Equals(""))
					throw new ApplicationException("Please specify Action");
				if(strEnterpriseID.Trim().Equals(""))
					throw new ApplicationException("Please specify EnterpriseId");
				if(strUserloggedin.Trim().Equals(""))
					throw new ApplicationException("Please specify UserLoggedin");
				
				SQLText = @"DECLARE	@FileString varchar(max)
									EXEC [dbo].[ExportPODs] 
									@enterpriseid = '{0}',
									@userloggedin = '{1}'";

				SQLText = string.Format(SQLText,
					strEnterpriseID.Trim(),
					strUserloggedin.Trim());

				if(strConsignment != null && strConsignment.Trim() != "")
				{
					SQLText += string.Format(", @ConsignmentsString = '{0}'", strConsignment.Trim());
				}
				if(ExportFormat != null && ExportFormat.Trim() != "")
				{
					SQLText += string.Format(", @ExportFormat = '{0}'", ExportFormat.Trim());
				}

				SQLText += @", @FileString = @FileString OUTPUT
							 SELECT	@FileString as N'@FileString'";

				dbCmd = dbCon.CreateCommand(SQLText, CommandType.Text);
				dsReturn = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method ExecCustomsExchangeRates : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "ExportDeliveredConsignmentDAL", "ExecCustomsExchangeRates", msgException);
				throw new ApplicationException(msgException);
			}
		}


        public static System.Data.DataSet ExportPODFTP(string strAppID, string strEnterpriseID, string strUserloggedin, string strConsignment, string ExportFormat)
        {
            DataSet dsReturn = new DataSet();
            string SQLText = string.Empty;
            IDbCommand dbCmd = null;

            DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
            if (dbCon == null)
            {
                Logger.LogTraceError("ExportDeliveredConsignmentDAL", "GetDataExportPODs", "GetDataExportPODs", "DbConnection object is null!!");
                return null;
            }
            try
            {
                if (strAppID.Trim().Equals(""))
                    throw new ApplicationException("Please specify Action");
                if (strEnterpriseID.Trim().Equals(""))
                    throw new ApplicationException("Please specify EnterpriseId");
                if (strUserloggedin.Trim().Equals(""))
                    throw new ApplicationException("Please specify UserLoggedin");

                SQLText = @"DECLARE	@FileString varchar(max)
									EXEC [dbo].[GetDataExportPODs] 
									@enterpriseid = '{0}',
									@userloggedin = '{1}'";

                SQLText = string.Format(SQLText,
                    strEnterpriseID.Trim(),
                    strUserloggedin.Trim());

                if (strConsignment != null && strConsignment.Trim() != "")
                {
                    SQLText += string.Format(", @ConsignmentsString = '{0}'", strConsignment.Trim());
                }
                if (ExportFormat != null && ExportFormat.Trim() != "")
                {
                    SQLText += string.Format(", @ExportFormat = '{0}'", ExportFormat.Trim());
                }

                SQLText += @", @FileString = @FileString OUTPUT
							 SELECT	@FileString as N'@FileString'";

                dbCmd = dbCon.CreateCommand(SQLText, CommandType.Text);
                dsReturn = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
                return dsReturn;
            }
            catch (Exception ex)
            {
                string msgException = string.Format("Method ExecCustomsExchangeRates : {0}|{1}", ex.Message, ex.InnerException);
                Logger.LogTraceError("TIESDAL", "ExportDeliveredConsignmentDAL", "ExecCustomsExchangeRates", msgException);
                throw new ApplicationException(msgException);
            }
        }
    }
}
