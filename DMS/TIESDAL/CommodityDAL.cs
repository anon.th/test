using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for CommodityDAL.
	/// </summary>
	public class CommodityDAL
	{
		public CommodityDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static SessionDS  GetCommodities(String strAppID, String strEnterpriseID,
				int iCurrent, int iDSRecSize)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","GetEnterpriseOfAutoManifest","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder str = new StringBuilder();
			str.Append("SELECT * ");
			str.Append("FROM Commodity ");
			str.Append("WHERE applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("'");
			
			sessionDS = dbCon.ExecuteQuery(str.ToString(),iCurrent, iDSRecSize,"Enterprise");
			return  sessionDS;	
		}
	}
}
