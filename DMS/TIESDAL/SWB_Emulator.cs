using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for SWB_Emulator.
	/// </summary>
	public class SWB_Emulator
	{
		public SWB_Emulator()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static int SWB_SIP_Insert(string m_strAppID,string m_strEnterpriseID,string consignment_no,string dest_postal_code,string NumOfPkg,string service_code,string Origin_DC,string user_id_created,string processid)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",consignment_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@dest_postal_code",dest_postal_code));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@number_of_package",NumOfPkg));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@service_code",service_code));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@origin_DC",Origin_DC));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@user_id_created",user_id_created));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@processid",processid));

			int result = (int)dbCon.ExecuteProcedure("MF_Consignment_Details_Insert",storedParams);
			return result;
		}
		public static int SWB_SIP_Update(string m_strAppID,string m_strEnterpriseID,string consignment_no,string dest_postal_code,string NumOfPkg,string service_code,string Origin_DC,string user_id_updated)
		{

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",consignment_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@dest_postal_code",dest_postal_code));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@number_of_package",NumOfPkg));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@service_code",service_code));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@origin_DC",Origin_DC));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@user_id_updated",user_id_updated));

			int result = (int)dbCon.ExecuteProcedure("MF_Consignment_Details_Update",storedParams);
			return result;
		}
		public static DataSet SWB_SIP_Read(string strAppID,string strEnterpriseID,string ConsignmentNo)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SWB_Emulator.cs","getConsignmentDetails","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			ConsignmentNo=ConsignmentNo.Replace("'","''");
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * from Consignment_details  ");
			strQry.Append("where  consignment_no = '");
			strQry.Append(ConsignmentNo);
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}

		public static DataSet SWB_Pkg_Read(string strAppID,string strEnterpriseID,string ConsignmentNo)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","getPackageDetails","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
				
			StringBuilder strQry = new StringBuilder();
			//strQry = strQry.Append("select consignment_no, mps_no, quantity,  weight, convert(int,length) length, convert(int,breadth) breadth, convert(int,height) height, user_id_created, created_datetime, user_id_updated, updated_datetime  from Package_Details ");
			strQry = strQry.Append("select consignment_no, mps_no, quantity,  weight, length, breadth, height, user_id_created, created_datetime, user_id_updated, updated_datetime  from Package_Details ");
			strQry.Append("where  consignment_no = '");
			strQry.Append(ConsignmentNo);
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}
		public static int SWB_Pkg_Insert(string m_strAppID,string m_strEnterpriseID,string consignment_no,string mps_no,string quantity,string weight
			,string length,string breadth,string height,string user_id_created)
		{

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",consignment_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@mps_no",mps_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@quantity",quantity));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@weight",weight));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@length",length));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@breadth",breadth));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@height",height));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@user_id_created",user_id_created));

			int result = (int)dbCon.ExecuteProcedure("MF_Package_Details_Insert",storedParams);
			return result;
		}

		public static int SWB_Pkg_Update(string m_strAppID,string m_strEnterpriseID,string consignment_no,string mps_no,string quantity,string weight
			,string length,string breadth,string height,string user_id_updated)
		{

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",consignment_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@mps_no",mps_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@quantity",quantity));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@weight",weight));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@length",length));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@breadth",breadth));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@height",height));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@user_id_updated",user_id_updated));

			int result = (int)dbCon.ExecuteProcedure("MF_Package_Details_Update",storedParams);
			return result;
		}
		public static int SWB_Pkg_Delete(string m_strAppID,string m_strEnterpriseID,string consignment_no,string mps_no)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",consignment_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@mps_no",mps_no));

			int result = (int)dbCon.ExecuteProcedure("MF_Package_Details_Delete",storedParams);
			return result;
		}

		public static DataSet GetStatusCodeDS(String strAppID, String strEnterpriseID)
		{
			DataSet dsStatusCode = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return dsStatusCode;
			}

			String strSQLQuery = "select status_code,status_description,Status_type,status_close,invoiceable,system_code,tt_display_status,tt_display_remarks,auto_process,auto_location,auto_remark,tt_display_location, scanning_type  from Status_Code where ";			
			strSQLQuery += " applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and AllowInSWB_Emu='Y'";
 		
			dsStatusCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			return  dsStatusCode;
	
		}

		public static int SWB_ScanPkg_Insert(string m_strAppID,string m_strEnterpriseID,string scanData,string status_code,string location,string batch_no,string ScannerId,string ScanDT,string UserId  )
		{

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@AppId",m_strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@EnterpriseId",m_strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@scanData",scanData));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@status_code",status_code));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@location",location));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@batch_no",batch_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@ScannerId",ScannerId));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@ScanDT",ScanDT));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@UserId",UserId));
			int result = (int)dbCon.ExecuteProcedureScalar("MF_ScanPkg_Insert",storedParams);
			
			return result;
		}
		public static DataSet SWB_ScanPkg_Read(string m_strAppID,string m_strEnterpriseID,string scanData,string status_code,string location,string batch_no)
		{

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@AppId",m_strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@EnterpriseId",m_strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@scanData",scanData));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@status_code",status_code));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@location",location));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@batch_no",batch_no));

			DataSet ds = (DataSet)dbCon.ExecuteProcedure("MF_ScanPkg_Read",storedParams,ReturnType.DataSetType);
			return ds;
		}
		public static DataSet SWB_Batch_Manifest_Insert(string m_strAppID,string m_strEnterpriseID,
			string Batch_no,string Truck_Id,string BatchId,DateTime BatchDate,
			string BatchType,string UserId,string Status , string ServiceType)
		{

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@AppId",m_strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@EnterpriseId",m_strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Batch_no",Batch_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Truck_Id",Truck_Id));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@BatchID",BatchId));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@BatchDate",BatchDate));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@BatchType",BatchType));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Create_By",UserId));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Status",Status));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@ServiceType",ServiceType));
			//int result = (int)dbCon.ExecuteProcedure("MF_Batch_Manifest_Insert",storedParams);
			DataSet ds = (DataSet)dbCon.ExecuteProcedure("MF_Batch_Manifest_Insert",storedParams,ReturnType.DataSetType);
			return ds;
		}
		public static DataSet SWB_Batch_Manifest_Update(string m_strAppID,string m_strEnterpriseID,
			string Batch_no,string UserId,string Status  )
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@AppId",m_strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@EnterpriseId",m_strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Batch_no",Batch_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@UserId",UserId));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Status",Status));

			//int result = (int)dbCon.ExecuteProcedureScalar("MF_Batch_Manifest_Update",storedParams);
			DataSet ds = (DataSet)dbCon.ExecuteProcedure("MF_Batch_Manifest_Upd",storedParams,ReturnType.DataSetType);
			return ds;
		}

		public static DataSet SWB_Batch_Manifest_Read(string strAppID,string strEnterpriseID,string BatchID,string Batchdate,string TruckID)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SWB_Emulator.cs","getPackageDetails","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
				
			StringBuilder strQry = new StringBuilder();
			if(TruckID=="")
			{
				TruckID= " is null";
			}
			else
			{
				TruckID= " = '" + TruckID +"'";
			
			}
			strQry = strQry.Append("select Batch_No+' for '+Batch_ID  +' on ' + CONVERT(CHAR(10),Batch_Date, 103) +' on '+ ISNULL(Truck_ID,'')  +' created by  '+ Created_By +' on '+ CONVERT(CHAR(10),Created_Date, 103) + ' ' + CONVERT(CHAR(5), Created_Date, 108) as text ,*  from dbo.MF_Batch_Manifest ");
			strQry.Append(" WHERE applicationid = '" + strAppID + "'");
			strQry.Append(" AND enterpriseid = '" + strEnterpriseID + "'");
			strQry.Append(" AND Batch_ID = '" + BatchID + "'");
			strQry.Append(" AND Truck_ID  " + TruckID );
			strQry.Append(" AND Batch_Date BETWEEN (CONVERT(datetime, '" + Batchdate + "' +' '+ '00:00:00', 103)) AND(CONVERT(datetime, '" + Batchdate + "' +' '+ '23:59:59', 103))");



			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("SWB_Emulator.cs","SWB_Batch_Manifest_Read","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}
		public static DataSet SWB_Batch_Manifest_Read(string strAppID,string strEnterpriseID,string BatchNo)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SWB_Emulator.cs","getPackageDetails","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
				
			StringBuilder strQry = new StringBuilder();

			strQry = strQry.Append("select *  from dbo.MF_Batch_Manifest ");
			strQry.Append(" WHERE applicationid = '" + strAppID + "'");
			strQry.Append(" AND enterpriseid = '" + strEnterpriseID + "'");
			strQry.Append(" AND Batch_No = '" + BatchNo + "'");



			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("SWB_Emulator.cs","SWB_Batch_Manifest_Read","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}
			
		public static int SWB_Scan_Complete_Read(string m_strAppID,string m_strEnterpriseID,string BeginScanDT ,string EndScanDT,
			string Batch_no,string UserId,string Location  )
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@AppId",m_strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@EnterpriseId",m_strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@BeginScanDT",BeginScanDT));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@EndScanDT",EndScanDT));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@batch_no",Batch_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@location",Location));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@UserId",UserId));


			int result = (int)dbCon.ExecuteProcedureScalar("MF_ScanComplete_Read",storedParams);
			//DataSet ds = (DataSet)dbCon.ExecuteProcedure("MF_ScanComplete_Read",storedParams,ReturnType.DataSetType);
			return result;
		}
		public static DataSet SWB_GetMPSNo(string strAppID,string strEnterpriseID,string ConsignmentNo)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","getPackageDetails","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
				
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select isnull(max(cast(mps_no as int))+1,1) mps_no from Package_Details ");
			strQry.Append("where  consignment_no = '");
			strQry.Append(ConsignmentNo);
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}

	}
}
