using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for Staff.
	/// </summary>
	public class Staff
	{
		public Staff()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		public static SessionDS GetEmptyStaff()
		{
			DataSet  dsStaff = new DataSet();
			DataTable dtStaff =  new DataTable();
			
			dtStaff.Columns.Add(new DataColumn("emp_id",typeof(string)));
			dtStaff.Columns.Add(new DataColumn("emp_type",typeof(string)));
			dtStaff.Columns.Add(new DataColumn("emp_name",typeof(string))); 
			dtStaff.Columns.Add(new DataColumn("mobileno",typeof(string))); 
			dtStaff.Columns.Add(new DataColumn("base_location",typeof(string)));
			dtStaff.Columns.Add(new DataColumn("initial_in_service_date",typeof(DateTime))); 
			dtStaff.Columns.Add(new DataColumn("separation_date",typeof(DateTime))); 
			dtStaff.Columns.Add(new DataColumn("out_of_service_date",typeof(DateTime))); 
			dtStaff.Columns.Add(new DataColumn("in_service_date",typeof(DateTime)));			

			
			dsStaff.Tables.Add(dtStaff); 

			dsStaff.Tables[0].Columns["emp_id"].AllowDBNull = false;

			SessionDS sessionds = new SessionDS();
			sessionds.ds = dsStaff;
				
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;
			return  sessionds;

		}

		
		public static SessionDS GetStaffCodeDS(String strAppID, String strEnterpriseID, int iCurRow, int iDSRowSize, DataSet dsQuery)
		{
			SessionDS sessionDS = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];

			DateTime dtInitialInServiceDate;
			DateTime dtSeparationDate;
			DateTime dtOutOfServiceDate;
			DateTime dtInServiceDate;

			string strEmpId = (string)dr["emp_id"];
			string strEmpType = (string)dr["emp_type"];
			string strEmpNa = (string)dr["emp_name"];
			string strMobileNo = (string)dr["mobileno"];
			string strBaseLocation = (string)dr["base_location"];


			String strSQLQuery;
			string strSQLWhere = "";

			if (strEmpId != null && strEmpId != "")
				strSQLWhere += " and emp_id like '%" + strEmpId + "%' ";

			if (strEmpType != null && strEmpType != "")
				strSQLWhere += " and emp_type like '%" + strEmpType + "%' ";			
			
			if (strEmpNa != null && strEmpNa != "")
				strSQLWhere += " and emp_name like N'%" + Utility.ReplaceSingleQuote(strEmpNa) + "%' ";
			
			if (strMobileNo != null && strMobileNo != "")
				strSQLWhere += " and mobileno like '%" + Utility.ReplaceSingleQuote(strMobileNo) + "%' ";

			if (strBaseLocation != null && strBaseLocation != "")
				strSQLWhere += " and RTRIM(BASE_LOCATION) like '%" + Utility.ReplaceSingleQuote(strBaseLocation) + "%' ";

			if(Utility.IsNotDBNull(dr["initial_in_service_date"]) && dr["initial_in_service_date"].ToString()!= "")
			{
				dtInitialInServiceDate = System.Convert.ToDateTime(dr["initial_in_service_date"]);							
				strSQLWhere += " and initial_in_service_date ='"+dtInitialInServiceDate+"'"; 								
			}

			if(Utility.IsNotDBNull(dr["separation_date"]) && dr["separation_date"].ToString()!= "")
			{
				dtSeparationDate = System.Convert.ToDateTime(dr["separation_date"]);							
				strSQLWhere += " and separation_date ='"+dtSeparationDate+"'"; 								
			}

			if(Utility.IsNotDBNull(dr["out_of_service_date"]) && dr["out_of_service_date"].ToString()!= "")
			{
				dtOutOfServiceDate = System.Convert.ToDateTime(dr["out_of_service_date"]);							
				strSQLWhere += " and out_of_service_date ='"+dtOutOfServiceDate+"'"; 								
			}

			if(Utility.IsNotDBNull(dr["in_service_date"]) && dr["in_service_date"].ToString()!= "")
			{
				dtInServiceDate = System.Convert.ToDateTime(dr["in_service_date"]);							
				strSQLWhere += " and in_service_date ='"+dtInServiceDate+"'"; 								
			}
						
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Staff","GetStaffCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			
			strSQLQuery = "SELECT EMP_ID, EMP_NAME, EMP_TYPE, MOBILENO, RTRIM(BASE_LOCATION) AS BASE_LOCATION, ";
			strSQLQuery += " INITIAL_IN_SERVICE_DATE, SEPARATION_DATE, OUT_OF_SERVICE_DATE, IN_SERVICE_DATE ";
			strSQLQuery += " FROM MF_STAFF  WHERE ";
			strSQLQuery += " ApplicationId = '" + strAppID + "'  AND enterpriseid = '" + strEnterpriseID + "'"; 

			if (strSQLWhere != null && strSQLWhere != "")
				strSQLQuery += strSQLWhere;
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery, iCurRow, iDSRowSize, "Mf_StaffTable");

			return  sessionDS;
		}

		
		public static void AddNewRowStaffCodeDS(ref SessionDS dsStaffCode)
		{
			DataRow drNew = dsStaffCode.ds.Tables[0].NewRow();
			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			try
			{
				dsStaffCode.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("Staff","AddNewRowStaffCodeDS","R005",ex.Message.ToString());
			}
		}

		
		public static int InsertStaff(DataSet dsStaff, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsStaff.Tables[0].Rows[rowIndex];

			string strEmpId = (string)dr["emp_id"];
			string strEmpNa = (string)dr["emp_name"];
			string strEmpType=(string)dr["emp_type"];
			string strMobileNo = (string)dr["mobileno"];
			string strBaseLocation = (string)dr["base_location"];	
		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("Staff","InsertStaff","DP001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strEmpId != null && strEmpId != "")
				{
					string strSQLQuery = "";

					strSQLQuery = "INSERT INTO MF_STAFF ";
					strSQLQuery += " (APPLICATIONID, ENTERPRISEID, EMP_ID, EMP_NAME, EMP_TYPE, MOBILENO, BASE_LOCATION, ";
					strSQLQuery += " INITIAL_IN_SERVICE_DATE, SEPARATION_DATE, OUT_OF_SERVICE_DATE, IN_SERVICE_DATE " + ") ";
					strSQLQuery += " VALUES ('" + strAppID + "', '" + strEnterpriseID + "', '" + strEmpId + "', N'" + Utility.ReplaceSingleQuote(strEmpNa) + "', '" + strEmpType + "', ";
					strSQLQuery += " '" + strMobileNo + "', '" + strBaseLocation + "',";

					if (!dr["initial_in_service_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
                        strSQLQuery += " '" + dr["initial_in_service_date"] + "',";
					else
						strSQLQuery += " NULL,";

					if (!dr["separation_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
                        strSQLQuery += " '" + dr["separation_date"] + "',";
					else
						strSQLQuery += " NULL,";

					if (!dr["out_of_service_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery += " '" + dr["out_of_service_date"] + "',";
					else
						strSQLQuery += " NULL,";

					if (!dr["in_service_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
                        strSQLQuery += " '" + dr["in_service_date"] + "'";
					else
						strSQLQuery += " NULL";
					
					strSQLQuery += ")";
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

					iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("Staff","InsertStaff","DP002",appException.Message.ToString());
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			return iRowsInserted;
		}


		public static int UpdateStaff(DataSet dsStaff, String strAppID, String strEnterpriseID)
		{
			int iRowsUpdated = 0;
			String strSQLQuery=null;
			IDbCommand dbCom = null;
			DataRow dr = dsStaff.Tables[0].Rows[0];

			string strEmpId = (string)dr["emp_id"];
			string strEmpType = (string)dr["emp_type"];
			string strEmpNa = (string)dr["emp_name"];
			string strMobileNo = (string)dr["mobileno"];
			string strBaseLocation = (string)dr["base_location"];	

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Staff", "UpdateStaff", "DP004", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null);
			}

			try
			{
				if(strEmpId != null && strEmpId != "")
				{
					strSQLQuery = "UPDATE MF_STAFF ";
					strSQLQuery +=" SET emp_name = N'" + Utility.ReplaceSingleQuote(strEmpNa) + "'";
					strSQLQuery =strSQLQuery+ ", emp_type = '" + Utility.ReplaceSingleQuote(strEmpType) + "'";
					strSQLQuery =strSQLQuery+ ", mobileno = N'" + Utility.ReplaceSingleQuote(strMobileNo) + "'";
					strSQLQuery =strSQLQuery+ ", base_location = '" + Utility.ReplaceSingleQuote(strBaseLocation) + "'";
					
					if (!dr["initial_in_service_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery = strSQLQuery + ", initial_in_service_date = '" + dr["initial_in_service_date"] + "'";
					else
						strSQLQuery = strSQLQuery + ", initial_in_service_date = NULL ";

					if (!dr["separation_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery = strSQLQuery + ", separation_date = '" + dr["separation_date"] + "'";
					else
						strSQLQuery = strSQLQuery + ", separation_date = NULL ";

					if (!dr["out_of_service_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery =strSQLQuery + ", out_of_service_date = '" + dr["out_of_service_date"] + "'";
					else
						strSQLQuery =strSQLQuery + ", out_of_service_date = NULL ";

					if (!dr["in_service_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
						strSQLQuery =strSQLQuery + ", in_service_date = '" + dr["in_service_date"] + "'";
					else
						strSQLQuery =strSQLQuery + ", in_service_date = NULL ";
					
					strSQLQuery += " WHERE applicationid = '" + strAppID + "' AND enterpriseid = '" + strEnterpriseID;
					strSQLQuery += "' AND emp_id = '" + strEmpId + "'"; 
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("Staff", "UpdateStaff", "DP005", appException.Message.ToString());
				throw new ApplicationException("Update operation failed", appException);				
			}
			return iRowsUpdated;
		}
		

		public static DataSet GetDistributionCenter(String appID, String enterpriseID)
		{
			DbConnection dbConnect = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbConnect = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseID);
  
			if (dbConnect == null)
			{
				Logger.LogTraceError("TIESDAL", "Staff", "GetDestinationCode", "dbConnect is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();

			strQry = strQry.Append("SELECT distinct origin_code ");
			strQry = strQry.Append(" FROM DISTRIBUTION_CENTER "); 
			strQry = strQry.Append(" WHERE APPLICATIONID = '" + appID + "' "); 
			strQry = strQry.Append(" AND ENTERPRISEID = '" + enterpriseID + "' ");
			strQry = strQry.Append(" UNION ALL  SELECT '' ");
			strQry = strQry.Append(" ORDER BY origin_code");
 
			dbCmd = dbConnect.CreateCommand(strQry.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbConnect.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;     
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "Staff", "GetDestinationCode", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}	
		
		
		public static bool IsEmpIdIsExistManifest(String strAppID, String strEnterpriseID, String strEmpId)
		{
			int iCurRow = 0;
			int iDSRowSize = 0;
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Staff", "SelectDistributionCenter", "DPC001", "dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				if(strEmpId != null && strEmpId != "")
				{
					string strSQLQuery = "";
					strSQLQuery = "SELECT * FROM dbo.MF_Batch_Manifest_DepArr WITH (NOLOCK) ";
					strSQLQuery += " WHERE applicationid = '" + strAppID + "' AND enterpriseid = '" + strEnterpriseID + "' ";
					strSQLQuery += " AND (dep_driver_id = '" + strEmpId + "' OR arr_driver_id = '" + strEmpId + "') ";
	
					sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurRow,iDSRowSize,"DCTable");
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("Staff", "IsEmpIDIsExistManifest", "DPC002", appException.Message.ToString());			
				throw new ApplicationException("Select operation failed" + appException.Message,appException);
			}

			if (sessionDS.ds.Tables[0].Rows.Count == 0)
				return false;
			else
				return true;
		}
		
		
		public static int DeleteStaff(DataSet dsStaff, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsDeleted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsStaff.Tables[0].Rows[rowIndex];
			string strEmpId = (string)dr["emp_id"];		

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Staff", "DeleteStaff", "DPC001", "dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				if(strEmpId != null && strEmpId != "")
				{
					string strSQLQuery = "DELETE FROM MF_STAFF ";
					strSQLQuery += " WHERE applicationid = " + "'" + strAppID + "' AND enterpriseid = '" + strEnterpriseID + "' ";
					strSQLQuery += " AND emp_id = '" + strEmpId + "'"; 
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("Staff", "DeleteStaff", "DPC002", appException.Message.ToString());
			
				throw new ApplicationException("Delete operation failed" + appException.Message,appException);
			}
			return iRowsDeleted;
		}		
	}
}
