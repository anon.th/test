using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using TIESClasses;

namespace TIESDAL
{
	public class CustomsTariffsDAL
	{
		private string SQLText = string.Empty;
		private DbConnection dbCon;
		private IDbCommand dbCmd = null;

		public CustomsTariffsDAL(string _appID, 
								 string _enterpriseID) 
		{ 
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(_appID, _enterpriseID);
		}

		public string GetEnterpriseConfigurations(string _enterpriseId)
		{
			string result = string.Empty;

			DataSet dsReturn = new DataSet();

			SQLText = string.Format(@"SELECT [key], [value] 
								      FROM dbo.EnterpriseConfigurations('{0}', 'CustomsTariffs')", _enterpriseId);
			try
			{
				dsReturn = (DataSet)dbCon.ExecuteQuery(SQLText, ReturnType.DataSetType);
				if(dsReturn.Tables[0].Rows.Count > 0)
					result = dsReturn.Tables[0].Rows[0][1].ToString();
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method GetCurrencies : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CustomsTariffsDAL", "GetEnterpriseConfigurations", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
			return result;
		}

		public string GetRegEx(string _enterpriseId)
		{
			string result = string.Empty;

			DataSet dsReturn = new DataSet();

			SQLText = string.Format(@"SELECT [key], [value] 
								      FROM dbo.EnterpriseConfigurations('{0}', 'CustomsTariffs')", _enterpriseId);
			try
			{
				dsReturn = (DataSet)dbCon.ExecuteQuery(SQLText, ReturnType.DataSetType);
				if(dsReturn.Tables[0].Rows.Count > 0)
					result = dsReturn.Tables[0].Rows[1][1].ToString();
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method GetCurrencies : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CustomsTariffsDAL", "GetRegEx", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
			return result;
		}

		public DataTable GetCustomsSpecialConfiguration(string _enterpriseId, string _codeId)
		{
			DataSet dsReturn = new DataSet();

			SQLText = string.Format(@"SELECT DropDownListDisplayValue, 
											 DefaultValue, 
											 [Description], 
											 CodedValue 
									  FROM dbo.GetCustomsSpecialConfiguration('{0}', '{1}', 1);", _enterpriseId, _codeId);

			try
			{
				dsReturn = (DataSet)dbCon.ExecuteQuery(SQLText, ReturnType.DataSetType);
				return dsReturn.Tables[0];
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method GetCurrencies : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CustomsTariffsDAL", "GetCustomsSpecialConfiguration", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
		}

		public DataSet ExecCustomsTariffs(CustomsTariffs objInfo)
		{
			DataSet dsReturn = new DataSet();
			try
			{
				if(objInfo.Action.Trim().Equals(""))
					throw new ApplicationException("Please specify Action");
				if(objInfo.EnterpriseId.Trim().Equals(""))
					throw new ApplicationException("Please specify EnterpriseId");
				if(objInfo.UserLoggedin.Trim().Equals(""))
					throw new ApplicationException("Please specify UserLoggedin");

				SQLText = @"Declare @RowReturn INT
							EXEC dbo.Customs_Tariff_Codes @action = {0},
														  @enterpriseid = '{1}',
													      @userloggedin = '{2}'";
				SQLText = string.Format(SQLText, 
					objInfo.Action.Trim(),
					objInfo.EnterpriseId.Trim(),
					objInfo.UserLoggedin.Trim());
				
				if(!objInfo.TariffCode.Trim().Equals(""))
					SQLText += string.Format(", @Tariff_Code = '{0}'", objInfo.TariffCode.Trim());
				if(!objInfo.Description.Trim().Equals(""))
					SQLText += string.Format(", @description = '{0}'", objInfo.Description.Trim());
				if(!objInfo.DutyRate.Trim().Equals(""))
					SQLText += string.Format(", @duty_rate = '{0}'", objInfo.DutyRate.Trim());
				if(!objInfo.DutyCalc.Trim().Equals(""))
					SQLText += string.Format(", @duty_calc = '{0}'", objInfo.DutyCalc.Trim());
				if(!objInfo.ExciseRate.Trim().Equals(""))
					SQLText += string.Format(", @excise_rate = '{0}'", objInfo.ExciseRate.Trim());
				if(!objInfo.ExciseCalc.Trim().Equals(""))
					SQLText += string.Format(", @excise_calc = '{0}'", objInfo.ExciseCalc.Trim());
				if(!objInfo.PermitRequired.Trim().Equals(""))
					SQLText += string.Format(", @permit_required = '{0}'", objInfo.PermitRequired.Trim());

				SQLText += @", @ReturnedRows = @RowReturn OUT
							 SELECT @RowReturn";

				dbCmd = dbCon.CreateCommand(SQLText, CommandType.Text);
				dsReturn = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method ExecCustomsExchangeRates : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CustomsTariffsDAL", "ExecCustomsExchangeRates", msgException);
				throw new ApplicationException(msgException);
			}
		}

		public DataSet RunCustomsTariffs(CustomsTariffs objInfo)
		{
			DataSet dsReturn = new DataSet();
			try
			{
				if(objInfo.Action.Trim().Equals(""))
					throw new ApplicationException("Please specify Action");
				if(objInfo.EnterpriseId.Trim().Equals(""))
					throw new ApplicationException("Please specify EnterpriseId");
				if(objInfo.UserLoggedin.Trim().Equals(""))
					throw new ApplicationException("Please specify UserLoggedin");

				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@action", objInfo.Action.Trim()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid", objInfo.EnterpriseId.Trim()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin", objInfo.UserLoggedin.Trim()));
				
				if(!objInfo.TariffCode.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Tariff_Code", objInfo.TariffCode.Trim()));
				if(!objInfo.Description.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@description", objInfo.Description.Trim()));
				if(!objInfo.DutyRate.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@duty_rate", objInfo.DutyRate.Trim()));
				if(!objInfo.DutyCalc.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@duty_calc", objInfo.DutyCalc.Trim()));
				if(!objInfo.ExciseRate.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@excise_rate", objInfo.ExciseRate.Trim()));
				if(!objInfo.ExciseCalc.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@excise_calc", objInfo.ExciseCalc.Trim()));
				if(!objInfo.PermitRequired.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@permit_required", objInfo.PermitRequired.Trim()));

				dbCmd = dbCon.CreateCommand("Customs_Tariff_Codes", storedParams, CommandType.StoredProcedure);
				dsReturn = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);  

				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method ExecCustomsExchangeRates : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CustomsTariffsDAL", "RunCustomsExchangeRates", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
		}
	}
}
