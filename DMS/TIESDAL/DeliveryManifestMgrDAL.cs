using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;



namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for DeliveryManifestMgrDAL.
	/// </summary>
	public class DeliveryManifestMgrDAL
	{
		/// <summary>
		/// CONSTRUCTOR
		/// </summary>
		public DeliveryManifestMgrDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		
		/// <summary>
/// Search Delivery Manifest based on the user input: Also used to select all Delivery Manifest
/// </summary>
/// <param name="strAppID"></param>
/// <param name="strEnterpriseID"></param>
/// <param name="iCurrent"></param>
/// <param name="iDSRecSize"></param>
/// <param name="dsQueryParam"></param>
/// <returns></returns>
		public static SessionDS GetDeliveryManifestDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","GetDeliveryManifestDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			String strBuilder = "";				
			strBuilder =" SELECT DM.path_code, DM.flight_vehicle_no, DM.delivery_manifest_datetime, DM.departure_datetime, DM.awb_driver_name, ";			
			strBuilder+=" DM.line_haul_cost, DM.printed_datetime, DP.Delivery_Type, DP.Path_Description ";
			strBuilder+=" FROM Delivery_Manifest DM, Delivery_Path DP where DM.path_code=DP.path_code and ";
			strBuilder+=" DM.applicationid=DP.applicationid and DM.enterpriseid=DP.enterpriseid and ";
			strBuilder+=" DM.applicationid ='"+strAppID+"' and DM.enterpriseid ='"+strEnterpriseID+"'";
			
			DataRow drEach = dsQueryParam.Tables[0].Rows[0];			
			if((drEach["path_code"]!= null) && (!drEach["path_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPathCode = (String) drEach["path_code"];
				strBuilder+=" and DM.path_code = '"+strPathCode+"' ";
			}
			if((drEach["flight_vehicle_no"]!= null) && (!drEach["flight_vehicle_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strFlightVehicleNo = (String) drEach["flight_vehicle_no"];
				strBuilder+=" and DM.flight_vehicle_no = '"+strFlightVehicleNo+"' ";
			}	
			if((drEach["departure_datetime"]!= null) && (!drEach["departure_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtDepartureDateTime = System.Convert.ToDateTime(drEach["departure_datetime"]);
				String strDepartureDateTime=Utility.DateFormat(strAppID, strEnterpriseID,dtDepartureDateTime,DTFormat.DateTime);
				strBuilder+=" and DM.departure_datetime ="+strDepartureDateTime;
			}
			if((drEach["delivery_manifest_datetime"]!= null) && (!drEach["delivery_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtDelManifestDateTime = System.Convert.ToDateTime(drEach["delivery_manifest_datetime"]);
				String strDelManifestDateTime=Utility.DateFormat(strAppID, strEnterpriseID,dtDelManifestDateTime,DTFormat.DateTime);
				strBuilder+=" and DM.delivery_manifest_datetime ="+strDelManifestDateTime;
			}
			if((drEach["awb_driver_name"]!= null) && (!drEach["awb_driver_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAWBDriverName = (String) drEach["awb_driver_name"];
				strBuilder+=" and DM.awb_driver_name = N'"+strAWBDriverName+"' ";
			}
			if((drEach["line_haul_cost"]!= null) && (!drEach["line_haul_cost"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				Decimal dLineHaulCost = Convert.ToDecimal(drEach["line_haul_cost"]);
				String strLineHaulCost = dLineHaulCost.ToString("#.00");
				strBuilder+=" and DM.line_haul_cost ="+strLineHaulCost;
			}

			strBuilder+=" Order by DM.path_code, DM.awb_driver_name";			
			sessionDS = dbCon.ExecuteQuery(strBuilder,iCurrent,iDSRecSize,"DeliveryManifestTable");

			return  sessionDS;	
		}

		public static SessionDS GetDMListDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,String sDeliveryType, String sPathCode, String sFlightVehNo, String sDepDateTime, String sManifestDateTime, String sAWBDriverName, String sLineHaulCost)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","GetDeliveryManifestDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			String strBuilder = "";				
//			strBuilder  =" SELECT DM.Path_Code, DP.Delivery_Type, DP.Path_Description, DM.Departure_DateTime, ";
//			strBuilder +=" DM.Flight_Vehicle_No, DM.AWB_Driver_Name, DM.Printed_DateTime, DM.Delivery_Manifest_DateTime,";
//			strBuilder +=" DM.Line_Haul_Cost, Count(DMD.Consignment_No) Total_Consignment, ";
//			strBuilder +=" SUM(S.Chargeable_Wt) Total_Weight, SUM(S.Tot_Pkg) Total_Package ";
//			strBuilder +=" FROM Delivery_Manifest DM, Delivery_Manifest_Detail DMD, Shipment S, Delivery_Path DP ";
//			strBuilder +=" WHERE DM.Path_Code=DMD.Path_Code and DM.Path_Code=DP.Path_code ";
//			strBuilder +=" and DM.Flight_Vehicle_No=DMD.Flight_Vehicle_No ";
//			strBuilder +=" and DM.Delivery_Manifest_DateTime=DMD.Delivery_Manifest_DateTime ";
//			strBuilder +=" and DM.applicationid=DP.applicationid and DM.enterpriseid=DP.enterpriseid ";
//			strBuilder +=" and DM.applicationid=DMD.applicationid and DM.enterpriseid=DMD.enterpriseid ";			
//			strBuilder +=" and S.applicationid=DMD.applicationid and S.enterpriseid=DMD.enterpriseid ";
//			strBuilder +=" and S.Consignment_No =DMD.Consignment_No ";
									
			strBuilder  =" SELECT     DM.path_code, DP.delivery_type, DP.path_description, DM.departure_datetime, DM.flight_vehicle_no, DM.awb_driver_name, DM.printed_datetime, ";
			strBuilder +=" DM.delivery_manifest_datetime, DM.line_haul_cost, isnull(COUNT(DMD.consignment_no),0) AS Total_Consignment, isnull(SUM(S.chargeable_wt),0) AS Total_Weight, ";
			strBuilder +=" isnull(SUM(S.tot_pkg),0) AS Total_Package  into #temp ";
			strBuilder +=" FROM         Delivery_Manifest DM left outer JOIN";
			strBuilder +=" Delivery_Manifest_Detail DMD ON DM.path_code = DMD.path_code AND DM.flight_vehicle_no = DMD.flight_vehicle_no AND ";
			strBuilder +=" DM.delivery_manifest_datetime = DMD.delivery_manifest_datetime AND DM.applicationid = DMD.applicationid AND ";
			strBuilder +=" DM.enterpriseid = DMD.enterpriseid left outer JOIN";
			strBuilder +=" Delivery_Path DP ON DM.path_code = DP.path_code AND DM.applicationid = DP.applicationid AND DM.enterpriseid = DP.enterpriseid left outer JOIN";
			strBuilder +=" Shipment S ON DMD.applicationid = S.applicationid AND DMD.enterpriseid = S.enterpriseid AND DMD.consignment_no = S.consignment_no";


			

			strBuilder +=" and DM.applicationid ='"+strAppID+"' and DM.enterpriseid ='"+strEnterpriseID+"'";			
			strBuilder +=" GROUP BY DM.Path_Code, DP.Delivery_Type, DM.Departure_DateTime, DM.Flight_Vehicle_No, ";
			strBuilder +=" DM.AWB_Driver_Name, DM.Printed_DateTime, DP.Path_Description, ";
			strBuilder +=" DM.Delivery_Manifest_DateTime, DM.Line_Haul_Cost ";
			strBuilder +=" Order by DM.Departure_DateTime Desc";

			strBuilder +=" select * from #temp where ";

			string strwhere = " ";
			
			if(sPathCode!= null && sPathCode !="")
			{				
				strwhere+="  path_code = '"+sPathCode+"' ";
				if(strwhere !=""){strwhere+= " and ";} 
			}
			if(sDeliveryType != null && sDeliveryType !="")
			{			 
				strwhere+="  Delivery_Type ='"+sDeliveryType+"' ";
				if(strwhere !=""){strwhere+= " and ";}
			}
			if(sFlightVehNo != null && sFlightVehNo !="")
			{				
				strwhere+="  Flight_Vehicle_No = '"+sFlightVehNo+"' ";
				if(strwhere !=""){strwhere+= " and ";}
			}	
			if(sDepDateTime !=null && sDepDateTime != "")
			{
				DateTime dtDepartureDateTime = System.DateTime.ParseExact(sDepDateTime,"dd/MM/yyyy HH:mm",null);
				sDepDateTime=Utility.DateFormat(strAppID, strEnterpriseID,dtDepartureDateTime,DTFormat.DateTime);
				strwhere+="  departure_datetime ="+sDepDateTime;
				if(strwhere !=""){strwhere+= " and ";}
			}
			if(sManifestDateTime != null && sManifestDateTime != "")
			{
				DateTime dtDelManifestDateTime = System.DateTime.ParseExact(sManifestDateTime,"dd/MM/yyyy HH:mm",null);
				sManifestDateTime=Utility.DateFormat(strAppID, strEnterpriseID,dtDelManifestDateTime,DTFormat.DateTime);
				strwhere+="  delivery_manifest_datetime ="+sManifestDateTime;
				if(strwhere !=""){strwhere+= " and ";}
			}
			if(sAWBDriverName != null && sAWBDriverName != "")
			{				
				strwhere+="  AWB_Driver_Name = N'"+sAWBDriverName+"' ";
				if(strwhere !=""){strwhere+= " and ";}
			}
			if(sLineHaulCost != null && sLineHaulCost != "")
			{
				Decimal dLineHaulCost = Convert.ToDecimal(sLineHaulCost);
				String strLineHaulCost=dLineHaulCost.ToString("#.00");
				strwhere+="  line_haul_cost ="+strLineHaulCost;
			}
			// strwhere.ToCharArray(strwhere.Length-4,4).ToString()=="and "
			if(strwhere.EndsWith("and ")) { strwhere = strwhere.Substring(0,strwhere.Length-4);}
			strBuilder +=strwhere;
			strBuilder +=" drop table #temp ";

			sessionDS = dbCon.ExecuteQuery(strBuilder,iCurrent,iDSRecSize,"DMList");
			return  sessionDS;	
		}

		public static SessionDS GetDMListDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","GetDeliveryManifestDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			String strBuilder = "";				
			strBuilder  =" SELECT DM.Path_Code, DP.Delivery_Type, DP.Path_Description, DM.Departure_DateTime, ";
			strBuilder +=" DM.Flight_Vehicle_No, DM.AWB_Driver_Name, DM.Printed_DateTime, DM.Delivery_Manifest_DateTime,";
			strBuilder +=" DM.Line_Haul_Cost, Count(DMD.Consignment_No) Total_Consignment, ";
			strBuilder +=" SUM(S.Chargeable_Wt) Total_Weight, SUM(S.Tot_Pkg) Total_Package ";
			strBuilder +=" FROM Delivery_Manifest DM, Delivery_Manifest_Detail DMD, Shipment S, Delivery_Path DP ";
			strBuilder +=" WHERE DM.Path_Code=DMD.Path_Code and DM.Path_Code=DP.Path_code ";
			strBuilder +=" and DM.Flight_Vehicle_No=DMD.Flight_Vehicle_No ";
			strBuilder +=" and DM.Delivery_Manifest_DateTime=DMD.Delivery_Manifest_DateTime ";
			strBuilder +=" and DM.applicationid=DP.applicationid and DM.enterpriseid=DP.enterpriseid ";
			strBuilder +=" and DM.applicationid=DMD.applicationid and DM.enterpriseid=DMD.enterpriseid ";			
			strBuilder +=" and S.applicationid=DMD.applicationid and S.enterpriseid=DMD.enterpriseid ";
			strBuilder +=" and S.Consignment_No =DMD.Consignment_No ";
						
			DataRow drEach = dsQueryParam.Tables[0].Rows[0];			
			if((drEach["path_code"]!= null) && (!drEach["path_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPathCode = (String) drEach["path_code"];
				strBuilder+=" and DM.path_code like '%"+strPathCode+"%' ";
			}
			if((drEach["Delivery_Type"]!= null) && (!drEach["Delivery_Type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strDelvryType = (String) drEach["Delivery_Type"];
				strBuilder+=" and DP.Delivery_Type ='"+strDelvryType+"' ";
			}
			if((drEach["Flight_Vehicle_No"]!= null) && (!drEach["Flight_Vehicle_No"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strFlightVehicleNo = (String) drEach["Flight_Vehicle_No"];
				strBuilder+=" and DM.Flight_Vehicle_No like '%"+strFlightVehicleNo+"%' ";
			}	
			if((drEach["departure_datetime"]!= null) && (!drEach["departure_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtDepartureDateTime = System.Convert.ToDateTime(drEach["departure_datetime"]);
				String strDepartureDateTime=Utility.DateFormat(strAppID, strEnterpriseID,dtDepartureDateTime,DTFormat.DateTime);
				strBuilder+=" and DM.departure_datetime ="+strDepartureDateTime;
			}
			if((drEach["delivery_manifest_datetime"]!= null) && (!drEach["delivery_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtDelManifestDateTime = System.Convert.ToDateTime(drEach["delivery_manifest_datetime"]);
				String strDelManifestDateTime=Utility.DateFormat(strAppID, strEnterpriseID,dtDelManifestDateTime,DTFormat.DateTime);
				strBuilder+=" and DM.delivery_manifest_datetime ="+strDelManifestDateTime;
			}
			if((drEach["AWB_Driver_Name"]!= null) && (!drEach["AWB_Driver_Name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAWBDriverName = (String) drEach["AWB_Driver_Name"];
				strBuilder+=" and DM.AWB_Driver_Name like N'%"+strAWBDriverName+"%' ";
			}
			if((drEach["line_haul_cost"]!= null) && (!drEach["line_haul_cost"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				Decimal dLineHaulCost = Convert.ToDecimal(drEach["line_haul_cost"]);
				String strLineHaulCost=dLineHaulCost.ToString("#.00");
				strBuilder+=" and DM.line_haul_cost ="+strLineHaulCost;
			}

			strBuilder +=" and DM.applicationid ='"+strAppID+"' and DM.enterpriseid ='"+strEnterpriseID+"'";			
			strBuilder +=" GROUP BY DM.Path_Code, DP.Delivery_Type, DM.Departure_DateTime, DM.Flight_Vehicle_No, ";
			strBuilder +=" DM.AWB_Driver_Name, DM.Printed_DateTime, DP.Path_Description, ";
			strBuilder +=" DM.Delivery_Manifest_DateTime, DM.Line_Haul_Cost ";
			strBuilder +=" Order by DM.Departure_DateTime Desc";
			
			sessionDS = dbCon.ExecuteQuery(strBuilder,iCurrent,iDSRecSize,"DMList");
			return  sessionDS;	
		}

		public static SessionDS GetConsignmentDS(String strAppID, String strEnterpriseID, String strDelvryPathCode,String strDelvryType,String strFlightVehNo,String strManifestDate,int iCurrent, int iDSRecSize)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","GetDeliveryManifestDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			String strBuilder = "";		
			strBuilder  =" Select DMD.Consignment_No, DMD.Origin_State_Code, DMD.Destination_State_Code , DMD.Route_Code, ";
			strBuilder +=" S.Chargeable_Wt, S.Tot_Dim_Wt*E.Density_Factor as Pkg_Vol, S.Tot_Pkg ";
			strBuilder +=" From Shipment S, Delivery_Manifest_Detail DMD, Enterprise E ";
			strBuilder +=" where S.ApplicationID=E.ApplicationID and S.EnterpriseID=E.EnterpriseID and ";
			strBuilder +=" S.ApplicationID=DMD.ApplicationID and S.EnterpriseID=DMD.EnterpriseID and ";
			strBuilder +=" S.Consignment_No =DMD.Consignment_No ";
					
			if((strDelvryPathCode!= null) && (strDelvryPathCode !=""))
			{				
				strBuilder+=" and DMD.path_code like '%"+strDelvryPathCode+"%' ";
			}
			if((strFlightVehNo!= null) && (strFlightVehNo !=""))
			{				
				strBuilder+=" and DMD.Flight_Vehicle_No like '%"+strFlightVehNo+"%' ";
			}				
			if((strManifestDate!= null) && (strManifestDate!=""))
			{
				DateTime dtDelManifestDateTime = System.DateTime.ParseExact(strManifestDate,"dd/MM/yyyy HH:mm",null);
				strManifestDate=Utility.DateFormat(strAppID, strEnterpriseID,dtDelManifestDateTime,DTFormat.DateTime);
				strBuilder+=" and DMD.delivery_manifest_datetime ="+strManifestDate;
			}			
			strBuilder +=" and DMD.applicationid ='"+strAppID+"' and DMD.enterpriseid ='"+strEnterpriseID+"'";						

			sessionDS = dbCon.ExecuteQuery(strBuilder,iCurrent,iDSRecSize,"ConsignmentList");
			return  sessionDS;	
		}

		public static bool DeliveryManifestExists(String strAppID, String strEnterpriseID, String strDelPathCode, String strFlightVehNo,  String strDelManifestDate)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeliveryManifestExists","ERR0001","DbConnection object is null!!");				
				return false;
			}
			String strBuilder = " SELECT DM.* ";
			strBuilder+=" FROM Delivery_Manifest DM, Delivery_Path DP where DM.path_code=DP.path_code and ";
			strBuilder+=" DM.applicationid=DP.applicationid and DM.enterpriseid=DP.enterpriseid and ";
			strBuilder+=" DM.applicationid ='"+strAppID+"' and DM.enterpriseid ='"+strEnterpriseID+"'";			
			
			if((strDelPathCode != null) && (strDelPathCode !=""))
			{			
				strBuilder+=" and DM.path_code = '"+strDelPathCode+"' ";
			}
			if((strFlightVehNo != null) && (strFlightVehNo !=""))
			{			
				strBuilder+=" and DM.flight_vehicle_no = '"+strFlightVehNo+"' ";
			}				
			if((strDelManifestDate != null) && (strDelManifestDate!=""))
			{
				DateTime dtDelManifestDateTime = System.Convert.ToDateTime(strDelManifestDate);
				strDelManifestDate=Utility.DateFormat(strAppID, strEnterpriseID,dtDelManifestDateTime,DTFormat.DateTime);
				strBuilder+=" and DM.delivery_manifest_datetime ="+strDelManifestDate;
			}

			Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeliveryManifestExists","ERR0001","The Date:"+strDelManifestDate);				
			
			int iCurrent=0; 			int iDSRecSize=0;
			Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeliveryManifestExists","MSG001", strBuilder);			
			sessionDS = dbCon.ExecuteQuery(strBuilder,iCurrent,iDSRecSize,"DeliveryManifestTable");
			
			if (sessionDS.ds.Tables[0].Rows.Count>0)
			{
				return true;
			}
			else
			{
				return false;
			}				
		}


		public static DataSet GetAvailConsignmentList(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize, String strDelType, String strDelPathCode)
		{
			SessionDS sessionDS=null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","GetAssignedConsignmentList","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DeliveryManifestMgrDAL::GetAssignedConsignmentList()] DbConnection is null","ERROR");
				return sessionDS.ds;
			}			

			String strSQLQuery=null;
			strSQLQuery =  " Select S.Consignment_no, S.Origin_State_Code, S.Destination_State_Code, S.Route_Code ,S.est_delivery_datetime,S.cod_amount";
			strSQLQuery += " From  Shipment S,Route_Code RC, Delivery_Path DP ";
			strSQLQuery += " where  S.Route_Code = RC.Route_Code and ";
			strSQLQuery += " S.applicationid=RC.applicationid and S.EnterpriseID=RC.EnterpriseID and ";
 			strSQLQuery += " RC.Path_Code=DP.Path_Code  and ";
			strSQLQuery += " RC.applicationid=DP.applicationid and RC.EnterpriseID=DP.EnterpriseID and ";
			strSQLQuery += " S.delivery_manifested='N' and ";
			strSQLQuery += " S.applicationid = '"+strAppID+"' and S.EnterpriseID = '"+strEnterpriseID+"' ";							

			if (strDelPathCode !=null && strDelPathCode.Trim() !="" )
			{
				strSQLQuery +=" and DP.Path_Code='"+strDelPathCode+"'";	
			}
			if (strDelType != null && strDelType.Trim() != "")
			{
				strSQLQuery +="	and DP.Delivery_Type='"+strDelType+"'";
			}
			if (strDelType != null && strDelType.Trim() != "")
			{
				if (strDelType.Trim().Equals("S"))
				{
					strSQLQuery += " order by S.Route_Code, S.Origin_State_Code"; 
				}
				else
				{
					strSQLQuery += " AND S.origin_state_code <> S.destination_state_code";
					strSQLQuery += " order by S.Origin_State_Code, S.Route_Code";
				}
			}
			else
			{
				strSQLQuery += " order by S.Destination_State_Code"; 
			}
			sessionDS = (SessionDS)dbCon.ExecuteQuery(strSQLQuery, iCurrent, iDSRecSize, "ShipmentConsignment");
			return sessionDS.ds;
		}

		/*public static DataSet GetAvailConsignmentList(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize, String strDelType, String strDelPathCode, String strOriState)
		{
			SessionDS sessionDS=null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","GetAssignedConsignmentList","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DeliveryManifestMgrDAL::GetAssignedConsignmentList()] DbConnection is null","ERROR");
				return sessionDS.ds;
			}			

			String strSQLQuery=null;
			strSQLQuery =  " Select S.Consignment_no, S.Origin_State_Code, S.Destination_State_Code, S.Route_Code ";
			strSQLQuery += " From  Shipment S,Route_Code RC, Delivery_Path DP ";
			strSQLQuery += " where  S.Route_Code = RC.Route_Code and ";
			strSQLQuery += " S.applicationid=RC.applicationid and S.EnterpriseID=RC.EnterpriseID and ";
			strSQLQuery += " RC.Path_Code=DP.Path_Code  and ";
			strSQLQuery += " RC.applicationid=DP.applicationid and RC.EnterpriseID=DP.EnterpriseID and ";
			strSQLQuery += " S.delivery_manifested='N' and ";
			strSQLQuery += " S.applicationid = '"+strAppID+"' and S.EnterpriseID = '"+strEnterpriseID+"' ";							

			if (strDelPathCode !=null && strDelPathCode.Trim() !="" )
			{
				strSQLQuery +=" and DP.Path_Code='"+strDelPathCode+"'";	
			}
			if (strDelType != null && strDelType.Trim() != "")
			{
				strSQLQuery +="	and DP.Delivery_Type='"+strDelType+"'";
			}
			if (strOriState != null && strOriState.Trim() != "")
			{
				strSQLQuery += " and S.Origin_State_Code='"+strOriState+"'";
			}
			if (strDelType != null && strDelType.Trim() != "")
			{
				if (strDelType.Trim().Equals("S"))
				{
					strSQLQuery += " order by S.Route_Code, S.Origin_State_Code"; 
				}
				else
				{
					strSQLQuery += " order by S.Origin_State_Code, S.Route_Code";
				}
			}
			else
			{
				strSQLQuery += " order by S.Destination_State_Code"; 
			}
			sessionDS = (SessionDS)dbCon.ExecuteQuery(strSQLQuery, iCurrent, iDSRecSize, "ShipmentConsignment");
			return sessionDS.ds;
		}*/

		public static DataSet GetAssignedConsignmentList(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize, DataSet dsQueryParam)
		{						
			SessionDS sessionDS =null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","GetAssignedConsignmentList","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DeliveryManifestMgrDAL::GetAvailableConsignmentList()] DbConnection is null","ERROR");
				return sessionDS.ds;
			}
		
			String strBuilder="";
			strBuilder  = " Select Consignment_no, Origin_State_Code, Destination_State_Code, Route_Code ";
			strBuilder += " From Delivery_Manifest_Detail where ";								
			strBuilder += " Applicationid = '"+strAppID+"' and EnterpriseID = '"+strEnterpriseID+"' ";			

			DataRow drEach = dsQueryParam.Tables[0].Rows[0];			
			if((drEach["path_code"]!= null) && (!drEach["path_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPathCode = (String) drEach["path_code"];
				strBuilder+=" and path_code like '%"+strPathCode+"%' ";
			}
			if((drEach["Flight_Vehicle_No"]!= null) && (!drEach["Flight_Vehicle_No"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strFlightVehicleNo = (String) drEach["Flight_Vehicle_No"];
				strBuilder+=" and Flight_Vehicle_No like '%"+strFlightVehicleNo+"%' ";
			}				
			if((drEach["delivery_manifest_datetime"]!= null) && (!drEach["delivery_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtDelManifestDateTime = System.Convert.ToDateTime(drEach["delivery_manifest_datetime"]);
				String strDelManifestDateTime=Utility.DateFormat(strAppID, strEnterpriseID,dtDelManifestDateTime,DTFormat.DateTime);
				strBuilder+=" and delivery_manifest_datetime ="+strDelManifestDateTime;
			}
			strBuilder +=" Order by Consignment_No ";			
			sessionDS = (SessionDS)dbCon.ExecuteQuery(strBuilder, iCurrent, iDSRecSize, "AvailableConsignment");
			return sessionDS.ds;
			
		}

		/// <summary>
		/// Delivery Manifest
		/// </summary>
		/// <returns></returns>
		public static SessionDS GetDelManifestSessionDS()
		{
			DataSet dsDelManifest = GetEmptyDelManifestSessionDS();

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsDelManifest;
			
			sessionDS.DataSetRecSize = dsDelManifest.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			
			return sessionDS;
		}

		/// <summary>
		/// Empty Delivery Manifest Dataset
		/// </summary>
		/// <returns></returns>
		public static DataSet GetEmptyDelManifestSessionDS()
		{
			DataTable dtDeliveryManifest = new DataTable();
			dtDeliveryManifest.Columns.Add(new DataColumn("path_code", typeof(string)));
			dtDeliveryManifest.Columns.Add(new DataColumn("flight_vehicle_no", typeof(string)));
			dtDeliveryManifest.Columns.Add(new DataColumn("delivery_manifest_datetime", typeof(DateTime)));
			dtDeliveryManifest.Columns.Add(new DataColumn("departure_datetime", typeof(DateTime)));
			dtDeliveryManifest.Columns.Add(new DataColumn("awb_driver_name", typeof(string)));
			dtDeliveryManifest.Columns.Add(new DataColumn("line_haul_cost", typeof(decimal)));
			dtDeliveryManifest.Columns.Add(new DataColumn("printed_datetime", typeof(DateTime)));
			dtDeliveryManifest.Columns.Add(new DataColumn("Delivery_type", typeof(string)));
			dtDeliveryManifest.Columns.Add(new DataColumn("path_description", typeof(string)));

			DataRow drEach = dtDeliveryManifest.NewRow();				
			
			dtDeliveryManifest.Rows.Add(drEach);
			DataSet dsDeliveryManifest = new DataSet();
			dsDeliveryManifest.Tables.Add(dtDeliveryManifest);
			return  dsDeliveryManifest;	
		}

		public static DataSet GetEmptyAvailSessionDS()
		{
			DataTable dtShipCons = new DataTable();
			dtShipCons.Columns.Add(new DataColumn("Consignment_no", typeof(string)));
			dtShipCons.Columns.Add(new DataColumn("Origin_State_Code", typeof(string)));
			dtShipCons.Columns.Add(new DataColumn("Destination_State_Code", typeof(string)));
			dtShipCons.Columns.Add(new DataColumn("Route_Code", typeof(string)));
			
			DataRow drEach = dtShipCons.NewRow();				
			
			dtShipCons.Rows.Add(drEach);
			DataSet dsShipCons = new DataSet();
			dsShipCons.Tables.Add(dtShipCons);
			
			return  dsShipCons;	
		}

		public static DataSet GetEmptyAssignedSessionDS()
		{
			DataTable dtDMCons = new DataTable();
			dtDMCons.Columns.Add(new DataColumn("Consignment_no", typeof(string)));
			dtDMCons.Columns.Add(new DataColumn("Origin_State_Code", typeof(string)));
			dtDMCons.Columns.Add(new DataColumn("Destination_State_Code", typeof(string)));
			dtDMCons.Columns.Add(new DataColumn("Route_Code", typeof(string)));

			DataRow drEach = dtDMCons.NewRow();				
			
			dtDMCons.Rows.Add(drEach);
			DataSet dsDMCons = new DataSet();
			dsDMCons.Tables.Add(dtDMCons);
			
			return  dsDMCons;	
		}
		/// <summary>
/// Creates Delivery Manifest and Consignment records in Delivery_Manifest & Delivery_Manifest_Detail tables
/// within Transaction
/// </summary>
/// <param name="strAppID"></param>
/// <param name="strEnterpriseID"></param>
/// <param name="consignmentList"></param>
/// <param name="dsToInsert"></param>
/// <returns></returns>
		public static int CreateDMsNConsignments(String strAppID,String strEnterpriseID,DataSet dsAssignedConsignment, DataSet dsToInsert)
		{
			int iRows = 0;
			String sConsignmentNo = "";
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			String strQry = "";
			String strPathcode="";
			String strFlightVehicleNo="";
			String strDepartureDateTime="";
			String strDelManifestDateTime="";
			String strAWBDriverName="";
			String strLineHaulCost="";			
			String strPrintedDateTime="";			
			String strDeliveryType="";
			String strPathDescription="";

			//Dwain request
			String strDeliveryManifested = "";
			
			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				int iRowCnt = dsToInsert.Tables[0].Rows.Count;
				DataRow drEach = dsToInsert.Tables[0].Rows[0];

				if((drEach["path_code"]!= null) && (!drEach["path_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPathcode = (String) drEach["path_code"];					
				}							
				if((drEach["flight_vehicle_no"]!= null) && (!drEach["flight_vehicle_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strFlightVehicleNo = (String) drEach["flight_vehicle_no"];					
				}				
				if((drEach["departure_datetime"]!= null) && (!drEach["departure_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtDepartureDateTime =System.Convert.ToDateTime(drEach["departure_datetime"]);
					strDepartureDateTime=Utility.DateFormat(strAppID,strEnterpriseID,dtDepartureDateTime,DTFormat.DateTime);
				}	
				else
					strDepartureDateTime="null";

				if((drEach["delivery_manifest_datetime"]!= null) && (!drEach["delivery_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtDelManifestDateTime =System.Convert.ToDateTime(drEach["delivery_manifest_datetime"]);
					strDelManifestDateTime=Utility.DateFormat(strAppID,strEnterpriseID,dtDelManifestDateTime,DTFormat.DateTime);
				}
				if((drEach["awb_driver_name"]!= null) && (!drEach["awb_driver_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strAWBDriverName = (String) drEach["awb_driver_name"];				
				else
					strAWBDriverName="null";

				if((drEach["line_haul_cost"]!= null) && (!drEach["line_haul_cost"].GetType().Equals(System.Type.GetType("System.DBNull"))))				
				{
					Decimal dLineHaulCost = Convert.ToDecimal(drEach["line_haul_cost"]);
					strLineHaulCost = dLineHaulCost.ToString("#.00");					
				}
				else
					strLineHaulCost="0.00";
				
				if((drEach["printed_datetime"]!= null) && (!drEach["printed_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtPrintedDateTime = System.Convert.ToDateTime(drEach["printed_datetime"]);				
					strPrintedDateTime=Utility.DateFormat(strAppID, strEnterpriseID,dtPrintedDateTime,DTFormat.DateTime);
				}				
				else
					strPrintedDateTime="null";

				if((drEach["delivery_type"]!= null) && (!drEach["delivery_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strDeliveryType = (String) drEach["delivery_type"];
					if (strDeliveryType.Equals("L") || strDeliveryType.Equals("A"))
					{
						strDeliveryManifested = "N";
					}
					else if (strDeliveryType.Equals("S"))
					{
						strDeliveryManifested = "Y";
					}
				}
				if((drEach["path_description"]!= null) && (!drEach["path_description"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strPathDescription = (String) drEach["path_description"];
				else
					strPathDescription ="null";

				//Insert record in Delivery_Manifest table.								
				strQry =  "INSERT INTO Delivery_Manifest(applicationid, enterpriseid, path_code, flight_vehicle_no, ";
				strQry += " departure_datetime, delivery_manifest_datetime, awb_driver_name, line_haul_cost, printed_datetime,manifest_count) ";
				strQry += " values('"+strAppID+"','"+strEnterpriseID+"','"+strPathcode+"','"+strFlightVehicleNo+"',";
				strQry +=  strDepartureDateTime+","+strDelManifestDateTime+",N'"+strAWBDriverName+"',"+strLineHaulCost+","+strPrintedDateTime+",0)";
				
				dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0012",iRows + " rows inserted into Delivery_Manifest table");

				//insert into Delivery_Manifest_Detail --> CHILD 						
				
				for(int i=0;i< dsAssignedConsignment.Tables[0].Rows.Count;i++)
				{
					DataRow drCons = dsAssignedConsignment.Tables[0].Rows[i];
					
					if((drCons["Consignment_No"]!= null) && (!drCons["Consignment_No"].GetType().Equals(System.Type.GetType("System.DBNull"))))					
						sConsignmentNo = (String)drCons["Consignment_No"];
					else
						sConsignmentNo="null";
					
					String sOrigin=null;
					if((drCons["Origin_State_Code"]!= null) && (!drCons["Origin_State_Code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						sOrigin=(String)drCons["Origin_State_Code"];
					else
						sOrigin="null";

					String sDestination=null;
					if((drCons["Destination_State_Code"]!= null) && (!drCons["Destination_State_Code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						sDestination=(String)drCons["Destination_State_Code"];
					else
						sDestination="null";

					String strRouteCode=null;
					if((drCons["Route_Code"]!= null) && (!drCons["Route_Code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						strRouteCode=(String) drCons["Route_Code"];
					else
						strRouteCode="null";

					// INSERT INTO Delivery_Manifest_Detail
					strQry = " INSERT INTO Delivery_Manifest_Detail(applicationid, enterpriseid, path_code, flight_vehicle_no, ";
					strQry += " delivery_manifest_datetime, consignment_no, origin_state_code, destination_state_code, route_code) values('";
					strQry += strAppID+"','"+strEnterpriseID+"','"+strPathcode+"','"+strFlightVehicleNo+"',"+strDelManifestDateTime+",'";
					strQry += sConsignmentNo+"','"+sOrigin+"','"+sDestination+"','"+strRouteCode+"')";
					dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;

					//dbCommandApp.CommandText = strQry;
					iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0013",iRows + " rows inserted into Delivery_Manifest_Detail table");					
					// UDATE Shipment Table
					//trQry="UPDATE Shipment set Delivery_Manifested='Y' where Consignment_No='"+sConsignmentNo+"' and Origin_State_Code='"+sOrigin+"' and Destination_State_Code='"+sDestination+"' and Route_Code='"+strRouteCode+"' and ";
					//update shpt_manifest_datetime if strDeliveryManifested <> 'Y'
					if (strDeliveryManifested.Equals("Y"))
					{
						strQry="UPDATE Shipment set Delivery_Manifested='Y' where Consignment_No='"+sConsignmentNo+"' and Origin_State_Code='"+sOrigin+"' and Destination_State_Code='"+sDestination+"' and Route_Code='"+strRouteCode+"' and ";
						strQry+=" applicationid='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"'";
					}
					else
					{
						strQry="UPDATE Shipment set shpt_manifest_datetime=GETDATE() where Consignment_No='"+sConsignmentNo+"' and Origin_State_Code='"+sOrigin+"' and Destination_State_Code='"+sDestination+"' and Route_Code='"+strRouteCode+"' and ";
						strQry+=" applicationid='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"'";
					}
					dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					//dbCommandApp.CommandText = strQry;
					iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0013",iRows + " rows updated in Shipment table");					

				}		
				
				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0014","App db Transaction committed.");
			}
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0006","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0009","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				
				dbCommandApp.Dispose();
				conApp.Dispose();
				transactionApp.Dispose();
				
			}
			
			return iRows;
		}

		/// <summary>
/// Updates Delivery_Manifest and Consignment details within a "Transaction"
/// </summary>
/// <param name="strAppID"></param>
/// <param name="strEnterpriseID"></param>
/// <param name="consignmentList"></param>
/// <param name="dsToInsert"></param>
/// <returns></returns>
		public static int ModifyDMsNConsignments(String strAppID,String strEnterpriseID,DataSet dsAssignedConsignment, DataSet dsToInsert)
		{
			int iRows = 0;
			String sConsignmentNo = "";			
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

			//Get App dbConnection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ModifyDMsNConsignments","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ModifyDMsNConsignments","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ModifyDMsNConsignments","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ModifyDMsNConsignments","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			String strQry = "";
			String strPathcode=null;
			String strFlightVehicleNo=null;
			String strDepartureDateTime=null;
			String strDelManifestDateTime=null;
			String strAWBDriverName=null;
			String strLineHaulCost="";
			String strPrintedDateTime=null;			
			String strDeliveryType=null;
			String strPathDescription=null;
			
			//Dwain request
			String strDeliveryManifested = "";

			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ModifyDMsNConsignments","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{
				int iRowCnt = dsToInsert.Tables[0].Rows.Count;
				DataRow drEach = dsToInsert.Tables[0].Rows[0];
				if((drEach["path_code"]!= null) && (!drEach["path_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPathcode = (String) drEach["path_code"];					
				}							
				if((drEach["flight_vehicle_no"]!= null) && (!drEach["flight_vehicle_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strFlightVehicleNo = (String) drEach["flight_vehicle_no"];					
				}				
				if((drEach["departure_datetime"]!= null) && (!drEach["departure_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtDepartureDateTime = System.Convert.ToDateTime(drEach["departure_datetime"]);
					strDepartureDateTime=Utility.DateFormat(strAppID,strEnterpriseID,dtDepartureDateTime,DTFormat.DateTime);
				}	
				else
					strDepartureDateTime="null";

				if((drEach["delivery_manifest_datetime"]!= null) && (!drEach["delivery_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtDelManifestDateTime = System.Convert.ToDateTime(drEach["delivery_manifest_datetime"]);
					strDelManifestDateTime=Utility.DateFormat(strAppID,strEnterpriseID,dtDelManifestDateTime,DTFormat.DateTime);
				}			
				
				if((drEach["awb_driver_name"]!= null) && (!drEach["awb_driver_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strAWBDriverName = (String) drEach["awb_driver_name"];
				}
				else
					strAWBDriverName="null";

				if((drEach["line_haul_cost"]!= null) && (!drEach["line_haul_cost"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					Decimal dLineHaulCost = Convert.ToDecimal(drEach["line_haul_cost"]);	
					strLineHaulCost=dLineHaulCost.ToString("#.00");					
				}				
				else
					strLineHaulCost="0.00";

				if((drEach["printed_datetime"]!= null) && (!drEach["printed_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtPrintedDateTime = System.Convert.ToDateTime(drEach["printed_datetime"]);				
					strPrintedDateTime=Utility.DateFormat(strAppID,strEnterpriseID,dtPrintedDateTime,DTFormat.DateTime);
				}
				else
					strPrintedDateTime="null";

				if((drEach["delivery_type"]!= null) && (!drEach["delivery_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strDeliveryType = (String) drEach["delivery_type"];
					if (strDeliveryType.Equals("L") || strDeliveryType.Equals("A"))
					{
						strDeliveryManifested = "N";
					}
					else if (strDeliveryType.Equals("S"))
					{
						strDeliveryManifested = "Y";
					}
				}
				else
				{
					strDeliveryType="null";
					strDeliveryManifested = "N";
				}

				if((drEach["path_description"]!= null) && (!drEach["path_description"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPathDescription = (String) drEach["path_description"];
				}
				else
					strPathDescription="null";

				// UDATE Shipment Table
				strQry=   " UPDATE Shipment set Delivery_Manifested='N' where Consignment_No in ( Select Consignment_No ";
				strQry += " From Delivery_Manifest_Detail where  enterpriseid ='"+strEnterpriseID+"' and applicationid = '"+strAppID+"'";
				strQry += " and Path_code='"+strPathcode+"' and Flight_Vehicle_No='"+strFlightVehicleNo+"' and delivery_manifest_datetime="+strDelManifestDateTime+")";		
				dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;					
				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0013",iRows + " rows updated in Shipment table");
                
//				Delete from Delivery_Manifest_Detail 
				strQry = " DELETE from Delivery_Manifest_Detail where  enterpriseid ='"+strEnterpriseID+"' and applicationid = '"+strAppID+"'";
				strQry += " and Path_code='"+strPathcode+"' and Flight_Vehicle_No='"+strFlightVehicleNo+"' and delivery_manifest_datetime="+strDelManifestDateTime;
				dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ModifyDMsNConsignments","ERR0012",iRows + " rows deleted from module_tree_user_relation table");

				//UPDATE Delivery_Manifest 
				strQry = " UPDATE Delivery_Manifest set AWB_Driver_Name=N'"+strAWBDriverName+"', Line_Haul_Cost="+strLineHaulCost+",";
				strQry +=" departure_datetime="+strDepartureDateTime+",printed_datetime="+strPrintedDateTime;
				strQry +=" where enterpriseid ='"+strEnterpriseID+"' and applicationid ='"+strAppID+"' and path_code ='"+strPathcode+"'";
				strQry +=" and flight_Vehicle_No='"+strFlightVehicleNo+"' and delivery_manifest_datetime="+strDelManifestDateTime;
				dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				//dbCommandApp.CommandText = strQry;
				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ModifyDMsNConsignments","MSG0014",iRows + " rows updated in Delivery_Manifest table");
				//insert into Delivery_Manifest_Detail --> CHILD 
				
				for(int i=0;i< dsAssignedConsignment.Tables[0].Rows.Count;i++)
				{
					DataRow drCons = dsAssignedConsignment.Tables[0].Rows[i];
					if((drCons["Consignment_No"]!= null) && (!drCons["Consignment_No"].GetType().Equals(System.Type.GetType("System.DBNull"))))					
						sConsignmentNo = (String)drCons["Consignment_No"];
					else
						sConsignmentNo="null";

					String sOrigin=null;
					if((drCons["Origin_State_Code"]!= null) && (!drCons["Origin_State_Code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						sOrigin=(String)drCons["Origin_State_Code"];
					else
						sOrigin="null";

					String sDestination=null;
					if((drCons["Destination_State_Code"]!= null) && (!drCons["Destination_State_Code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						sDestination=(String)drCons["Destination_State_Code"];
					else
						sDestination="null";

					String strRouteCode=null;
					if((drCons["Route_Code"]!= null) && (!drCons["Route_Code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						strRouteCode=(String) drCons["Route_Code"];
					else
						strRouteCode="null";
					
					// INSERT INTO Delivery_Manifest_Detail
					strQry = " INSERT INTO Delivery_Manifest_Detail(applicationid, enterpriseid, path_code, flight_vehicle_no, ";
					strQry +=" delivery_manifest_datetime, consignment_no, origin_state_code, destination_state_code, route_code) values('";
					strQry +=  strAppID+"','"+strEnterpriseID+"','"+strPathcode+"','"+strFlightVehicleNo+"',"+strDelManifestDateTime+",'";
					strQry +=  sConsignmentNo+"','"+sOrigin+"','"+sDestination+"','"+strRouteCode+"')";
					dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;					
					iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0013",iRows + " rows inserted into Delivery_Manifest_Detail table");					
					
					// UDATE Shipment Table
					//strQry=  " UPDATE Shipment set Delivery_Manifested='Y' where Consignment_No='"+sConsignmentNo+"' and ";
					//update shpt_manifest_datetime if strDeliveryManifested <> 'Y'
					if (strDeliveryManifested.Equals("Y"))
					{
						strQry=  " UPDATE Shipment set Delivery_Manifested='Y' where Consignment_No='"+sConsignmentNo+"' and ";
						strQry+= " Origin_State_Code='"+sOrigin+"' and Destination_State_Code='"+sDestination+"' and Route_Code='"+strRouteCode+"' and ";
						strQry+= " applicationid='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"'";
					}
					else
					{
						strQry=  " UPDATE Shipment set shpt_manifest_datetime=GETDATE() where Consignment_No='"+sConsignmentNo+"' and ";
						strQry+= " Origin_State_Code='"+sOrigin+"' and Destination_State_Code='"+sDestination+"' and Route_Code='"+strRouteCode+"' and ";
						strQry+= " applicationid='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"'";
					}
					dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;					
					iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0013",iRows + " rows updated in Shipment table");						
				}		
	
				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ModifyDMsNConsignments","MSG0016","App db Transaction committed.");
			}
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ModifyDMsNConsignments","ERR0006","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ModifyDMsNConsignments","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ModifyDMsNConsignments","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ModifyDMsNConsignments","ERR0009","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ModifyDMsNConsignments","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ModifyDMsNConsignments","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				dbCommandApp.Dispose();
				conApp.Dispose();
				transactionApp.Dispose();
			}
			
			return iRows;
		}

		/// <summary>
		/// Delete Delivery_Manifest_Detail, Delivery_Manifest records within a "Transaction"
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="dsToDelete"></param>
		/// <returns></returns>
		public static int DeleteDMsNConsignments(String strAppID,String strEnterpriseID, DataSet dsToDelete)
		{
			int iRows = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

			//Get App dbConnection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeleteDMsNConsignments","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeleteDMsNConsignments","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeleteDMsNConsignments","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeleteDMsNConsignments","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			String strQry = "";
			String strPathcode=null;
			String strFlightVehicleNo=null;			
			String strDelManifestDateTime=null;					

			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeleteDMsNConsignments","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{
				
				for (int i=0;i<dsToDelete.Tables[0].Rows.Count;i++)
				{

					DataRow drEach = dsToDelete.Tables[0].Rows[i];

					if((drEach["path_code"]!= null) && (!drEach["path_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strPathcode = (String) drEach["path_code"];					
					}							
					if((drEach["flight_vehicle_no"]!= null) && (!drEach["flight_vehicle_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strFlightVehicleNo = (String) drEach["flight_vehicle_no"];					
					}				
					if((drEach["delivery_manifest_datetime"]!= null) && (!drEach["delivery_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtDelManifestDateTime = System.Convert.ToDateTime(drEach["delivery_manifest_datetime"]);
						strDelManifestDateTime=Utility.DateFormat(strAppID,strEnterpriseID,dtDelManifestDateTime,DTFormat.DateTime);
					}							
					
					//Revert the Consignment_Nos status to 'N' ie NOT MANIFESTED
					strQry  = " UPDATE SHIPMENT  Set DELIVERY_MANIFESTED='N' where Consignment_No IN ";
					strQry += " ( Select DMD.Consignment_No From Delivery_Manifest_Detail DMD, Shipment S where ";
					strQry += " DMD.Consignment_No=S.Consignment_No and DMD.ApplicationID=S.ApplicationID and ";
					strQry += " DMD.EnterpriseID = S.EnterpriseID and DMD.Path_code='"+strPathcode+"' and ";
					strQry += " DMD.Flight_Vehicle_No='"+strFlightVehicleNo+"' and DMD.delivery_manifest_datetime="+strDelManifestDateTime+" and ";
					strQry += " DMD.enterpriseid ='"+strEnterpriseID+"' and DMD.applicationid = '"+strAppID+"')";

					dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeleteDMsNConsignments","MSG0014",iRows + " rows UPDATED IN SHIPMENT table");

					//DELETE from Delivery_Manifest_Detail 
					strQry = "DELETE from Delivery_Manifest_Detail where  enterpriseid ='"+strEnterpriseID+"' and applicationid = '"+strAppID+"'";
					strQry +=" and Path_code='"+strPathcode+"' and Flight_Vehicle_No='"+strFlightVehicleNo+"' and delivery_manifest_datetime="+strDelManifestDateTime;
				
					dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeleteDMsNConsignments","MSG0014",iRows + " rows deleted from Delivery_Manifest_Detail table");

					//DELETE Delivery_Manifest 
					strQry = "DELETE from Delivery_Manifest where enterpriseid ='"+strEnterpriseID+"' and applicationid ='"+strAppID+"' and path_code ='"+strPathcode+"'";
					strQry +=" and flight_Vehicle_No='"+strFlightVehicleNo+"' and delivery_manifest_datetime="+strDelManifestDateTime;
				
					dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeleteDMsNConsignments","MSG0014",iRows + " rows updated in Delivery_Manifest table");
					
				}

				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeleteDMsNConsignments","MSG0016","App db Transaction committed.");

			}

			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeleteDMsNConsignments","ERR0006","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeleteDMsNConsignments","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeleteDMsNConsignments","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeleteDMsNConsignments","ERR0009","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeleteDMsNConsignments","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeleteDMsNConsignments","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				dbCommandApp.Dispose();
				conApp.Dispose();
				transactionApp.Dispose();
			}
			
			return iRows;
		}


		/// <summary>
		/// This method is for the Shipment Update by route method
		/// </summary>
		/// <returns>An empty dataset is returned</returns>
		public static DataSet GetEmptyDMConsignmentDS()
		{
			DataTable dtDMConsignment = new DataTable();
			dtDMConsignment.Columns.Add(new DataColumn("consignment_no",typeof(string)));
			dtDMConsignment.Columns.Add(new DataColumn("origin_state_code",typeof(string)));
			dtDMConsignment.Columns.Add(new DataColumn("destination_state_code",typeof(string)));
			dtDMConsignment.Columns.Add(new DataColumn("route_code",typeof(string)));

			DataSet dsDMConsignment = new DataSet();
			dsDMConsignment.Tables.Add(dtDMConsignment);
			return dsDMConsignment;
		}

		public static int ReAssignRouteDM(String strAppID,String strEnterpriseID,DataSet dsAvailConsignment,DataSet dsToInsert, DataSet dsToDelete)
		{
			int iRows = 0;			
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			String strQry = "";
			//OLD 
			String strPathcodeOld=null;
			String strFlightVehicleNoOld=null;
			String strDepartureDateTimeOld="null";
			String strDelManifestDateTimeOld=null;
			String strAWBDriverNameOld="null";
			String strLineHaulCostOld="0.00";	

			// NEW
			String strPathcode=null;
			String strFlightVehicleNo=null;
			String strDepartureDateTime="null";
			String strDelManifestDateTime=null;
			String strAWBDriverName="null";
			String strLineHaulCost="0.00";						
			String strRemarks="null";
			
			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{		
		
				// OLD
				DataRow drOld = dsToDelete.Tables[0].Rows[0];	
				
				if((drOld["path_code"]!= null) && (!drOld["path_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))				
					strPathcodeOld = (String) drOld["path_code"];									
				if((drOld["flight_vehicle_no"]!= null) && (!drOld["flight_vehicle_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strFlightVehicleNoOld = (String) drOld["flight_vehicle_no"];					
				else
					strFlightVehicleNoOld = "";
				if((drOld["delivery_manifest_datetime"]!= null) && (!drOld["delivery_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtDelManifestDateTime = System.Convert.ToDateTime(drOld["delivery_manifest_datetime"]);
					strDelManifestDateTimeOld=Utility.DateFormat(strAppID,strEnterpriseID,dtDelManifestDateTime,DTFormat.DateTime);
				}
				if((drOld["Departure_DateTime"]!= null) && (!drOld["Departure_DateTime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtDepartureDateTime = System.Convert.ToDateTime(drOld["Departure_DateTime"]);
					strDepartureDateTimeOld=Utility.DateFormat(strAppID,strEnterpriseID,dtDepartureDateTime,DTFormat.DateTime);
				}				
				if((drOld["awb_driver_name"]!= null) && (!drOld["awb_driver_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strAWBDriverNameOld = (String) drOld["awb_driver_name"];				
				if((drOld["line_haul_cost"]!= null) && (!drOld["line_haul_cost"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					Decimal dLineHaulCostOld = Convert.ToDecimal(drOld["line_haul_cost"]);					
					strLineHaulCostOld=dLineHaulCostOld.ToString("#.00");
				}
				
				// NEW
				DataRow drNew = dsToInsert.Tables[0].Rows[0];	
				
				if((drNew["path_code"]!= null) && (!drNew["path_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))				
					strPathcode = (String) drNew["path_code"];									
				if((drNew["flight_vehicle_no"]!= null) && (!drNew["flight_vehicle_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strFlightVehicleNo = (String) drNew["flight_vehicle_no"];					
				if((drNew["delivery_manifest_datetime"]!= null) && (!drNew["delivery_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtDelManifestDateTime = System.Convert.ToDateTime(drNew["delivery_manifest_datetime"]);
					strDelManifestDateTime=Utility.DateFormat(strAppID,strEnterpriseID,dtDelManifestDateTime,DTFormat.DateTime);
				}
				if((drNew["Departure_DateTime"]!= null) && (!drNew["Departure_DateTime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtDepartureDateTime = System.Convert.ToDateTime(drNew["Departure_DateTime"]);
					strDepartureDateTime=Utility.DateFormat(strAppID,strEnterpriseID,dtDepartureDateTime,DTFormat.DateTime);
				}				
				if((drNew["awb_driver_name"]!= null) && (!drNew["awb_driver_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strAWBDriverName = (String) drNew["awb_driver_name"];				
				if((drNew["line_haul_cost"]!= null) && (!drNew["line_haul_cost"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					Decimal dLineHaulCost = Convert.ToDecimal(drNew["line_haul_cost"]);					
					strLineHaulCost=dLineHaulCost.ToString("#.00");
				}
				if((drNew["remark"]!= null) && (!drNew["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strRemarks = (String) drNew["remark"];					

				//Update the Re-Assigned DM in Delivery_Manifest table, since it could have been modified like remarks.. etc
				strQry  =  " UPDATE Delivery_Manifest Set awb_driver_name=N'"+strAWBDriverName+"',Departure_DateTime="+strDepartureDateTime+",";
				strQry +=  " line_haul_cost="+strLineHaulCost+",Remark=N'"+Utility.ReplaceSingleQuote(strRemarks)+"' where applicationid='"+strAppID+"' and ";
				strQry +=  " enterpriseid='"+strEnterpriseID+"' and path_code='"+strPathcode+"' and ";
				strQry +=  " flight_vehicle_no='"+strFlightVehicleNo+"' and delivery_manifest_datetime="+strDelManifestDateTime;
												
				dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0012",iRows + " rows inserted into Delivery_Manifest table");

				//now DELETE and INSERT into Delivery_Manifest_Detail --> CHILD 										
				for(int i=0;i< dsAvailConsignment.Tables[0].Rows.Count;i++)
				{
					DataRow drCons = dsAvailConsignment.Tables[0].Rows[i];

					String sConsignmentNo ="null";
					String sOrigin="null";
					String sDestination="null";
					String sRouteCode="null";

					if((drCons["Consignment_No"]!= null) && (!drCons["Consignment_No"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						sConsignmentNo = (String)drCons["Consignment_No"];
					if((drCons["Origin_State_Code"]!= null) && (!drCons["Origin_State_Code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						sOrigin=(String)drCons["Origin_State_Code"];				
					if((drCons["Destination_State_Code"]!= null) && (!drCons["Destination_State_Code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						sDestination=(String)drCons["Destination_State_Code"];					
					if((drCons["Route_Code"]!= null) && (!drCons["Route_Code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						sRouteCode=(String) drCons["Route_Code"];

					//Delete the Existing!
					strQry =  " DELETE from Delivery_Manifest_Detail where  enterpriseid ='"+strEnterpriseID+"' and ";
					strQry += " applicationid = '"+strAppID+"' and  Path_code='"+strPathcodeOld+"' and ";
//					strQry += " Flight_Vehicle_No='"+strFlightVehicleNoOld+"' and delivery_manifest_datetime="+strDelManifestDateTimeOld+" and ";
					//boon 2010/12/27 -- start (2)
					if (strFlightVehicleNoOld != null || strFlightVehicleNoOld != "")
						strQry += " Flight_Vehicle_No='"+strFlightVehicleNoOld+"' " + " and ";
					strQry += " delivery_manifest_datetime="+strDelManifestDateTimeOld+" and ";
					//boon 2010/12/27 -- end (2)
					strQry += " Consignment_No='"+sConsignmentNo+"' and Origin_State_Code='"+sOrigin+"' and ";
					strQry += " Destination_State_Code='"+sDestination+"' and Route_Code='"+sRouteCode+"'";
					
					dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0012",iRows + " rows deleted from Delivery_Manifest_Detail table");

					// INSERT the re-assigned
					strQry =  " INSERT INTO Delivery_Manifest_Detail(applicationid, enterpriseid, path_code, flight_vehicle_no, ";
					strQry += " delivery_manifest_datetime, consignment_no, origin_state_code, destination_state_code, route_code) values('";
					strQry +=   strAppID+"','"+strEnterpriseID+"','"+strPathcode+"','"+strFlightVehicleNo+"',"+strDelManifestDateTime+",'";
					strQry +=   sConsignmentNo+"','"+sOrigin+"','"+sDestination+"','"+sRouteCode+"')";

					dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;					
					iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);

					
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0013",iRows + " rows inserted into Delivery_Manifest_Detail table");					

				}		
				
				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0014","App db Transaction committed.");
			}
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0006","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0009","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				dbCommandApp.Dispose();
				conApp.Dispose();
				transactionApp.Dispose();
			}
			
			return iRows;
		}



		public static int ReAssignRouteDM(String strAppID,String strEnterpriseID,DataSet dsAvailConsignment,DataSet dsToInsert, DataSet dsToDelete, String newRouteCode ,int ProcessID, String UserID)
		{
			int iRows = 0;			
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			String strQry = "";
			//OLD 
			String strPathcodeOld=null;
			String strFlightVehicleNoOld=null;
			String strDepartureDateTimeOld="null";
			String strDelManifestDateTimeOld=null;
			String strAWBDriverNameOld="null";
			String strLineHaulCostOld="0.00";	

			// NEW
			String strPathcode=null;
			String strFlightVehicleNo=null;
			String strDepartureDateTime="null";
			String strDelManifestDateTime=null;
			String strAWBDriverName="null";
			String strLineHaulCost="0.00";						
			String strRemarks="null";
			
			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{		
		
				// OLD
				DataRow drOld = dsToDelete.Tables[0].Rows[0];	
				
				if((drOld["path_code"]!= null) && (!drOld["path_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))				
					strPathcodeOld = (String) drOld["path_code"];									
				if((drOld["flight_vehicle_no"]!= null) && (!drOld["flight_vehicle_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strFlightVehicleNoOld = (String) drOld["flight_vehicle_no"];					
				else
					strFlightVehicleNoOld = "";
				if((drOld["delivery_manifest_datetime"]!= null) && (!drOld["delivery_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtDelManifestDateTime = System.Convert.ToDateTime(drOld["delivery_manifest_datetime"]);
					strDelManifestDateTimeOld=Utility.DateFormat(strAppID,strEnterpriseID,dtDelManifestDateTime,DTFormat.DateTime);
				}
				if((drOld["Departure_DateTime"]!= null) && (!drOld["Departure_DateTime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtDepartureDateTime = System.Convert.ToDateTime(drOld["Departure_DateTime"]);
					strDepartureDateTimeOld=Utility.DateFormat(strAppID,strEnterpriseID,dtDepartureDateTime,DTFormat.DateTime);
				}				
				if((drOld["awb_driver_name"]!= null) && (!drOld["awb_driver_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strAWBDriverNameOld = (String) drOld["awb_driver_name"];				
				if((drOld["line_haul_cost"]!= null) && (!drOld["line_haul_cost"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					Decimal dLineHaulCostOld = Convert.ToDecimal(drOld["line_haul_cost"]);					
					strLineHaulCostOld=dLineHaulCostOld.ToString("#.00");
				}
				
				// NEW
				DataRow drNew = dsToInsert.Tables[0].Rows[0];	
				
				if((drNew["path_code"]!= null) && (!drNew["path_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))				
					strPathcode = (String) drNew["path_code"];									
				if((drNew["flight_vehicle_no"]!= null) && (!drNew["flight_vehicle_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strFlightVehicleNo = (String) drNew["flight_vehicle_no"];					
				if((drNew["delivery_manifest_datetime"]!= null) && (!drNew["delivery_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtDelManifestDateTime = System.Convert.ToDateTime(drNew["delivery_manifest_datetime"]);
					strDelManifestDateTime=Utility.DateFormat(strAppID,strEnterpriseID,dtDelManifestDateTime,DTFormat.DateTime);
				}
				if((drNew["Departure_DateTime"]!= null) && (!drNew["Departure_DateTime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtDepartureDateTime = System.Convert.ToDateTime(drNew["Departure_DateTime"]);
					strDepartureDateTime=Utility.DateFormat(strAppID,strEnterpriseID,dtDepartureDateTime,DTFormat.DateTime);
				}				
				if((drNew["awb_driver_name"]!= null) && (!drNew["awb_driver_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strAWBDriverName = (String) drNew["awb_driver_name"];				
				if((drNew["line_haul_cost"]!= null) && (!drNew["line_haul_cost"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					Decimal dLineHaulCost = Convert.ToDecimal(drNew["line_haul_cost"]);					
					strLineHaulCost=dLineHaulCost.ToString("#.00");
				}
				if((drNew["remark"]!= null) && (!drNew["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strRemarks = (String) drNew["remark"];					

				//Update the Re-Assigned DM in Delivery_Manifest table, since it could have been modified like remarks.. etc
				strQry  =  " UPDATE Delivery_Manifest Set awb_driver_name=N'"+strAWBDriverName+"',Departure_DateTime="+strDepartureDateTime+",";
				strQry +=  " line_haul_cost="+strLineHaulCost+",Remark=N'"+Utility.ReplaceSingleQuote(strRemarks)+"' where applicationid='"+strAppID+"' and ";
				strQry +=  " enterpriseid='"+strEnterpriseID+"' and path_code='"+strPathcode+"' and ";
				strQry +=  " flight_vehicle_no='"+strFlightVehicleNo+"' and delivery_manifest_datetime="+strDelManifestDateTime;
												
				dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0012",iRows + " rows inserted into Delivery_Manifest table");

				//now DELETE and INSERT into Delivery_Manifest_Detail --> CHILD 										
				for(int i=0;i< dsAvailConsignment.Tables[0].Rows.Count;i++)
				{
					DataRow drCons = dsAvailConsignment.Tables[0].Rows[i];

					String sConsignmentNo ="null";
					String sOrigin="null";
					String sDestination="null";
					String sRouteCode="null";

					if((drCons["Consignment_No"]!= null) && (!drCons["Consignment_No"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						sConsignmentNo = (String)drCons["Consignment_No"];
					if((drCons["Origin_State_Code"]!= null) && (!drCons["Origin_State_Code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						sOrigin=(String)drCons["Origin_State_Code"];				
					if((drCons["Destination_State_Code"]!= null) && (!drCons["Destination_State_Code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						sDestination=(String)drCons["Destination_State_Code"];					
					if((drCons["Route_Code"]!= null) && (!drCons["Route_Code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						sRouteCode=(String) drCons["Route_Code"];

					//Delete the Existing!
					strQry =  " DELETE from Delivery_Manifest_Detail where  enterpriseid ='"+strEnterpriseID+"' and ";
					strQry += " applicationid = '"+strAppID+"' and  Path_code='"+strPathcodeOld+"' and ";
//					strQry += " Flight_Vehicle_No='"+strFlightVehicleNoOld+"' and delivery_manifest_datetime="+strDelManifestDateTimeOld+" and ";
					//boon - 2010/12/27 start
					if (strFlightVehicleNoOld != "")
                        strQry += " Flight_Vehicle_No='"+strFlightVehicleNoOld+"' " + " and ";
					strQry += " delivery_manifest_datetime="+strDelManifestDateTimeOld+" and ";
					//boon - 2010/12/27 end
					strQry += " Consignment_No='"+sConsignmentNo+"' and Origin_State_Code='"+sOrigin+"' and ";
					strQry += " Destination_State_Code='"+sDestination+"' and Route_Code='"+sRouteCode+"'";
					
					dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0012",iRows + " rows deleted from Delivery_Manifest_Detail table");

					// INSERT the re-assigned
					strQry =  " INSERT INTO Delivery_Manifest_Detail(applicationid, enterpriseid, path_code, flight_vehicle_no, ";
					strQry += " delivery_manifest_datetime, consignment_no, origin_state_code, destination_state_code, route_code) values('";
					strQry +=   strAppID+"','"+strEnterpriseID+"','"+strPathcode+"','"+strFlightVehicleNo+"',"+strDelManifestDateTime+",'";
					strQry +=   sConsignmentNo+"','"+sOrigin+"','"+sDestination+"'";
					
					//Set New Route Code in case reassign short route.
					if (newRouteCode != null && newRouteCode.Trim() != "")
					{
						// Added by GwanG on 02May08
						strQry += ",'"+newRouteCode+"' ";
					}
					//Set Old Route Code in case reassign long route.
					else
					{
						strQry += ",'"+sRouteCode+"'";
					}
					strQry += " )";
					// End GwanG


					dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;					
					iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0013",iRows + " rows inserted into Delivery_Manifest_Detail table");					
					
					// Added by GwanG on 02May08
					if (newRouteCode != null && newRouteCode.Trim() != "")
					{				
						//now Update into Shipment
						strQry  =  " UPDATE Shipment Set route_code=N'"+newRouteCode+"'";
						//Added by GwanG on 14Jul08
						strQry +=  " ,destination_station = (select origin_station from DELIVERY_PATH where path_code = N'"+newRouteCode+"' and applicationid='"+strAppID+"' and  enterpriseid='"+strEnterpriseID+"' )" ;
						//End Added
						strQry +=  " where applicationid='"+strAppID+"' and ";
						strQry +=  " enterpriseid='"+strEnterpriseID+"' and ";
						strQry +=  " consignment_no='"+sConsignmentNo+ "'";
												
						dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
						dbCommandApp.Connection = conApp;
						dbCommandApp.Transaction = transactionApp;					
						iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					}
					// End GwanG

					string upShipment = "SELECT booking_no,consignment_no FROM shipment WHERE consignment_no = '"+sConsignmentNo+"'";
					dbCommandApp.CommandText = upShipment;
					DataSet ds  = new DataSet();
					ds = (DataSet)dbConApp.ExecuteQueryInTransaction(dbCommandApp,ReturnType.DataSetType);
					
					//Added by Tom 8/6/09
					DateTime DepartTime = Convert.ToDateTime(strDepartureDateTime.Replace("'",""));
					foreach(DataRow dr in ds.Tables[0].Rows)
					{
						Utility.CreateAutoConsignmentHistory(strAppID,strEnterpriseID,ProcessID,"Consignment reassigned to route : "+strPathcode+" "+DepartTime.ToString("dd/MM/yyyy  HH:mm"),Convert.ToInt32(dr["booking_no"].ToString()),dr["consignment_no"].ToString(),System.DateTime.Now,UserID,ref dbCommandApp,ref dbConApp);
					}
					//End Added by Tom 8/6/09
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0014",iRows + " rows update route_code into Shipment table");					

				}		
				
				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0015","App db Transaction committed.");
			}
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0006","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0009","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				dbCommandApp.Dispose();
				conApp.Dispose();
				transactionApp.Dispose();
			}
			
			return iRows;
		}


		public static DataSet DelManifestRecord(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{						
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsDM = null;			
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DelManifestRecord","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
		
			String strBuilder="";
			strBuilder  = " Select AWB_Driver_Name, Line_Haul_Cost, Departure_DateTime, Remark ";
			strBuilder += " From Delivery_Manifest where ";			
			strBuilder += " Applicationid = '"+strAppID+"' and EnterpriseID = '"+strEnterpriseID+"' ";			
			
			DataRow drEach = dsQuery.Tables[0].Rows[0];			
			if((drEach["path_code"]!= null) && (!drEach["path_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPathCode = (String) drEach["path_code"];
				strBuilder+=" and path_code like '%"+strPathCode+"%' ";
			}
			if((drEach["Flight_Vehicle_No"]!= null) && (!drEach["Flight_Vehicle_No"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strFlightVehicleNo = (String) drEach["Flight_Vehicle_No"];
				strBuilder+=" and Flight_Vehicle_No like '%"+strFlightVehicleNo+"%' ";
			}				
			if((drEach["delivery_manifest_datetime"]!= null) && (!drEach["delivery_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtDelManifestDateTime = System.Convert.ToDateTime(drEach["delivery_manifest_datetime"]);
				String strDelManifestDateTime=Utility.DateFormat(strAppID, strEnterpriseID,dtDelManifestDateTime,DTFormat.DateTime);
				strBuilder+=" and delivery_manifest_datetime ="+strDelManifestDateTime;
				//strBuilder += "and datediff(day, delivery_manifest_datetime, " + strDelManifestDateTime + ") = 0 ";
			}			

			dbCmd = dbConApp.CreateCommand(strBuilder,CommandType.Text);
			try
			{
				dsDM =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DelManifestRecord","ERR002","Error in the query String");
				throw appExpection;
			}

			return dsDM;
						
		}

		//Added by Gwang on 16June08
		public static DataSet DepartureRecord(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{						
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsDM = null;			
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DepartureRecord","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
		
			String strBuilder="";
			strBuilder  = " Select AWB_Driver_Name, Line_Haul_Cost, delivery_manifest_datetime, Remark ";
			strBuilder += " From Delivery_Manifest where ";			
			strBuilder += " Applicationid = '"+strAppID+"' and EnterpriseID = '"+strEnterpriseID+"' ";			
			
			DataRow drEach = dsQuery.Tables[0].Rows[0];			
			if((drEach["path_code"]!= null) && (!drEach["path_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPathCode = (String) drEach["path_code"];
				strBuilder+=" and path_code like '%"+strPathCode+"%' ";
			}
			if((drEach["Flight_Vehicle_No"]!= null) && (!drEach["Flight_Vehicle_No"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strFlightVehicleNo = (String) drEach["Flight_Vehicle_No"];
				strBuilder+=" and Flight_Vehicle_No like '%"+strFlightVehicleNo+"%' ";
			}				
			if((drEach["departure_datetime"]!= null) && (!drEach["departure_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtDepartureDateTime = System.Convert.ToDateTime(drEach["departure_datetime"]);
				String strDepartureDateTime=Utility.DateFormat(strAppID, strEnterpriseID,dtDepartureDateTime,DTFormat.DateTime);
				strBuilder+=" and departure_datetime ="+strDepartureDateTime;
				//strBuilder += "and datediff(day, delivery_manifest_datetime, " + strDelManifestDateTime + ") = 0 ";
			}			

			dbCmd = dbConApp.CreateCommand(strBuilder,CommandType.Text);
			try
			{
				dsDM =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DepartureRecord","ERR002","Error in the query String");
				throw appExpection;
			}

			return dsDM;
						
		}

		//End Added
		//Added by Tom on 18/12/09
		public static DataSet DepartureRecordDR(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{						
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;
			DataSet dsDM = null;			
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DepartureRecord","ERR001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
		
			String strBuilder="";
			strBuilder  = " Select Flight_Vehicle_No,AWB_Driver_Name, Line_Haul_Cost, delivery_manifest_datetime, Remark ";
			strBuilder += " From Delivery_Manifest where ";			
			strBuilder += " Applicationid = '"+strAppID+"' and EnterpriseID = '"+strEnterpriseID+"' ";			
			
			DataRow drEach = dsQuery.Tables[0].Rows[0];			
			if((drEach["path_code"]!= null) && (!drEach["path_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPathCode = (String) drEach["path_code"];
				strBuilder+=" and path_code like '%"+strPathCode+"%' ";
			}
			if((drEach["Flight_Vehicle_No"]!= null) && (!drEach["Flight_Vehicle_No"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strFlightVehicleNo = (String) drEach["Flight_Vehicle_No"];
				strBuilder+=" and Flight_Vehicle_No like '%"+strFlightVehicleNo+"%' ";
			}				
			if((drEach["departure_datetime"]!= null) && (!drEach["departure_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtDepartureDateTime = System.Convert.ToDateTime(drEach["departure_datetime"]);
				String strDepartureDateTime=Utility.DateFormat(strAppID, strEnterpriseID,dtDepartureDateTime,DTFormat.DateTime);
				strBuilder+=" and departure_datetime ="+strDepartureDateTime;
				//strBuilder += "and datediff(day, delivery_manifest_datetime, " + strDelManifestDateTime + ") = 0 ";
			}			

			dbCmd = dbConApp.CreateCommand(strBuilder,CommandType.Text);
			try
			{
				dsDM =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DepartureRecord","ERR002","Error in the query String");
				throw appExpection;
			}

			return dsDM;
						
		}

		//End Added
		public static int DeleteConsignment(String strAppID, String strEnterpriseID, DataSet dsDM, DataSet dsCons)
		{
			int iRows = 0;			
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			String strPathcode="";
			String strFlightVehicleNo="";
			String strDelManifestDateTime="";
			String sConsignmentNo ="";
			String sOrigin="";
			String sDestination="";
			String sRouteCode="";

			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{		

				DataRow drDMDel = dsDM.Tables[0].Rows[0];	
					
				if((drDMDel["path_code"]!= null) && (!drDMDel["path_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))				
					strPathcode = (String) drDMDel["path_code"];									
				if((drDMDel["flight_vehicle_no"]!= null) && (!drDMDel["flight_vehicle_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					strFlightVehicleNo = (String) drDMDel["flight_vehicle_no"];					
				if((drDMDel["delivery_manifest_datetime"]!= null) && (!drDMDel["delivery_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtDelManifestDateTime = System.Convert.ToDateTime(drDMDel["delivery_manifest_datetime"]);
					strDelManifestDateTime=Utility.DateFormat(strAppID,strEnterpriseID,dtDelManifestDateTime,DTFormat.DateTime);
				}

				DataRow drCons = dsCons.Tables[0].Rows[0];

				if((drCons["Consignment_No"]!= null) && (!drCons["Consignment_No"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					sConsignmentNo = (String)drCons["Consignment_No"];
				if((drCons["Origin_State_Code"]!= null) && (!drCons["Origin_State_Code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					sOrigin=(String)drCons["Origin_State_Code"];				
				if((drCons["Destination_State_Code"]!= null) && (!drCons["Destination_State_Code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					sDestination=(String)drCons["Destination_State_Code"];					
				if((drCons["Route_Code"]!= null) && (!drCons["Route_Code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					sRouteCode=(String) drCons["Route_Code"];
				
				//Delete the Existing!
				String strQry="";
				strQry =  " DELETE from Delivery_Manifest_Detail where  enterpriseid ='"+strEnterpriseID+"' and ";
				strQry += " applicationid = '"+strAppID+"' and  Path_code='"+strPathcode+"' and ";
				strQry += " Flight_Vehicle_No='"+strFlightVehicleNo+"' and delivery_manifest_datetime="+strDelManifestDateTime+" and ";
				strQry += " Consignment_No='"+sConsignmentNo+"' and Origin_State_Code='"+sOrigin+"' and ";
				strQry += " Destination_State_Code='"+sDestination+"' and Route_Code='"+sRouteCode+"'";
				
				dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DeleteConsignment","ERR0012",iRows + " rows Deleted From Delivery_Manifest_Detail table");
				
				strQry=  " UPDATE Shipment set Delivery_Manifested='N' where Consignment_No='"+sConsignmentNo+"' and Origin_State_Code='"+sOrigin+"' ";
				strQry+= " and Destination_State_Code='"+sDestination+"' and Route_Code='"+sRouteCode+"' and ";
				strQry+= " applicationid='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"'";
				dbCommandApp = dbConApp.CreateCommand(strQry,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				//dbCommandApp.CommandText = strQry;
				iRows = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","CreateDMsNConsignments","ERR0013",iRows + " rows updated in Shipment table");					

				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0014","App db Transaction committed.");

			}
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0006","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0009","App db delete transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ReAssignRouteDM","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				dbCommandApp.Dispose();
				conApp.Dispose();
				transactionApp.Dispose();
			}
			

			return iRows;
		}

		/*Comment by Sompote 2010-04-05
		/// <summary>
		/// //Automatically Assing to Manifests (DMS Phase 1)
		/// 8. Identify the exact date / time of the Delivery Manifest record:
		/// </summary>
		public static DateTime GetExactDateTimeOfAutoManifest(String strAppID, String strEnterpriseID, String pathCode,
									String flightVehicleNo, DateTime deliveryManifestDatetime)
		{
			DateTime DMFDateTime = new DateTime(1,1,1);
			SessionDS sessionDS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","GetExactDateTimeOfAutoManifest","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DeliveryManifestMgrDAL::GetExactDateTimeOfAutoManifest()] DbConnection is null","ERROR");
				return DMFDateTime;
			}

			StringBuilder str = new StringBuilder();
			str.Append("SELECT delivery_manifest_datetime ");
			str.Append("FROM Delivery_Manifest ");
			str.Append("WHERE applicationid = '" + strAppID + "' ");
			str.Append("AND enterpriseid = '" + strEnterpriseID + "' ");
			str.Append("AND path_code = '" + pathCode + "' ");
			str.Append("AND flight_vehicle_no = '" + flightVehicleNo + "' ");
			str.Append("AND DATEDIFF(day, departure_datetime," + Utility.DateFormat(strAppID, strEnterpriseID,deliveryManifestDatetime,DTFormat.DateTime) + " ) = 0");		
			
			sessionDS = dbCon.ExecuteQuery(str.ToString(),0, 0,"DMFDateTime");

			if (sessionDS.ds.Tables[0].Rows.Count > 0)
				DMFDateTime = (DateTime)sessionDS.ds.Tables[0].Rows[0]["delivery_manifest_datetime"];

			return  DMFDateTime;
		}
	
		*/
		/// <summary>
		/// Add by Sompote 2010-04-05
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="pathCode"></param>
		/// <param name="flightVehicleNo"></param>
		/// <param name="deliveryManifestDatetime"></param>
		/// <returns></returns>
		public static DateTime GetExactDateTimeOfAutoManifest(String strAppID, String strEnterpriseID, String pathCode,
			String flightVehicleNo, DateTime deliveryManifestDatetime)
		{
			DateTime DMFDateTime = new DateTime(1,1,1);
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","GetExactDateTimeOfAutoManifest","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DeliveryManifestMgrDAL::GetExactDateTimeOfAutoManifest()] DbConnection is null","ERROR");
				return DMFDateTime;
			}
			return GetExactDateTimeOfAutoManifest( strAppID,  strEnterpriseID,  pathCode, flightVehicleNo,  deliveryManifestDatetime, dbCon);
		}
		/// <summary>
		/// Add by Sompote 2010-04-05
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="pathCode"></param>
		/// <param name="flightVehicleNo"></param>
		/// <param name="deliveryManifestDatetime"></param>
		/// <returns></returns>
		public static DateTime GetExactDateTimeOfAutoManifest(String strAppID, String strEnterpriseID, String pathCode,
			String flightVehicleNo, DateTime deliveryManifestDatetime,DbConnection dbCon)
		{
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","GetExactDateTimeOfAutoManifest","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryManifestMgrDAL","GetExactDateTimeOfAutoManifest","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryManifestMgrDAL","GetExactDateTimeOfAutoManifest","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	

			//return GetExactDateTimeOfAutoManifest( strAppID,  strEnterpriseID,  pathCode, flightVehicleNo,  deliveryManifestDatetime, dbCon,dbCmd);
			DateTime result = DateTime.MinValue ;	
			try
			{
				result = GetExactDateTimeOfAutoManifest( strAppID,  strEnterpriseID,  pathCode, flightVehicleNo,  deliveryManifestDatetime, dbCon,dbCmd);
			}
			catch(ApplicationException appExpection)
			{
				throw appExpection;
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return result;

		}
		/// <summary>
		/// Add by Sompote 2010-04-05
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="pathCode"></param>
		/// <param name="flightVehicleNo"></param>
		/// <param name="deliveryManifestDatetime"></param>
		/// <param name="dbCon"></param>
		/// <param name="dbCmd"></param>
		/// <returns></returns>
		public static DateTime GetExactDateTimeOfAutoManifest(String strAppID, String strEnterpriseID, String pathCode,
			String flightVehicleNo, DateTime deliveryManifestDatetime,DbConnection dbCon,IDbCommand dbCmd)
		{
			DateTime DMFDateTime = new DateTime(1,1,1);
			SessionDS sessionDS = new SessionDS();

			/*DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","GetExactDateTimeOfAutoManifest","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DeliveryManifestMgrDAL::GetExactDateTimeOfAutoManifest()] DbConnection is null","ERROR");
				return DMFDateTime;
			}*/

			StringBuilder str = new StringBuilder();
			str.Append("SELECT delivery_manifest_datetime ");
			str.Append("FROM Delivery_Manifest ");
			str.Append("WHERE applicationid = '" + strAppID + "' ");
			str.Append("AND enterpriseid = '" + strEnterpriseID + "' ");
			str.Append("AND path_code = '" + pathCode + "' ");
			str.Append("AND flight_vehicle_no = '" + flightVehicleNo + "' ");
			str.Append("AND DATEDIFF(day, departure_datetime," + Utility.DateFormat(strAppID, strEnterpriseID,deliveryManifestDatetime,DTFormat.DateTime) + " ) = 0");		
			
			//sessionDS = dbCon.ExecuteQuery(str.ToString(),0, 0,"DMFDateTime");
			dbCmd.CommandText = str.ToString();
			sessionDS.ds = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			if (sessionDS.ds.Tables[0].Rows.Count > 0)
				DMFDateTime = (DateTime)sessionDS.ds.Tables[0].Rows[0]["delivery_manifest_datetime"];

			return  DMFDateTime;
		}
	
		/// <summary>
		/// //Automatically Assing to Manifests (DMS Phase 1)
		/// 11. Insert a new Delivery_Manifest_Detail record where the fields are assigned as follows:
		/// </summary>
		public static int INSERTDeliveryManifestDetailOfAutoManifest(String strAppID,String strEnterpriseID,
			String pathCode, String flightVehicleNo, DateTime deliveryManifestDatetime,
			String consignmentNo, String originStateCode, String destinationStateCode,
			String routeCode)
		{
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDeliveryManifestDetailOfAutoManifest","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDeliveryManifestDetailOfAutoManifest","ERR002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDeliveryManifestDetailOfAutoManifest","ERR003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDeliveryManifestDetailOfAutoManifest","ERR004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			try
			{

				String strDelManifestDateTime=Utility.DateFormat(strAppID,strEnterpriseID,deliveryManifestDatetime,DTFormat.DateLongTime);

				strBuild = new StringBuilder();

				if (!IsExistingRecord(strAppID, strEnterpriseID,
					pathCode, flightVehicleNo, deliveryManifestDatetime,
					consignmentNo))
				{
					strBuild.Append("INSERT INTO Delivery_Manifest_Detail(applicationid, enterpriseid, path_code, flight_vehicle_no, ");
					strBuild.Append("delivery_manifest_datetime, consignment_no, origin_state_code, destination_state_code, route_code) values('");
					strBuild.Append(strAppID+"','"+strEnterpriseID+"','"+pathCode+"','"+flightVehicleNo+"',"+strDelManifestDateTime+",'"+consignmentNo+"'");

					strBuild.Append(",");
					if(originStateCode.Trim() != "")
					{
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(originStateCode));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					if(destinationStateCode.Trim() != "")
					{
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(destinationStateCode));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					if(routeCode.Trim() != "")
					{
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(routeCode));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(")");
				
				}
				else
				{
					strBuild.Append("UPDATE Delivery_Manifest_Detail ");
					strBuild.Append("SET origin_state_code = ");
					if(originStateCode.Trim() != "")
					{
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(originStateCode));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(", ");

					strBuild.Append("destination_state_code = ");
					if(destinationStateCode.Trim() != "")
					{
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(destinationStateCode));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(", ");

					strBuild.Append("route_code = ");
					if(routeCode.Trim() != "")
					{
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(routeCode));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append("WHERE applicationid = '" + strAppID + "' ");
					strBuild.Append("AND enterpriseid = '" + strEnterpriseID + "' ");
					strBuild.Append("AND path_code = '" + pathCode + "' ");
					strBuild.Append("AND flight_vehicle_no = '" + flightVehicleNo + "' ");
					strBuild.Append("AND delivery_manifest_datetime = " + strDelManifestDateTime + " ");
					strBuild.Append("AND consignment_no = '" + consignmentNo + "' ");
				}
				dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceInfo("DomesticShipmentMgrDAL","UpdateDeliveryManifestDetailOfAutoManifest","ERR005","unable to update shipment data.");
				throw new ApplicationException("Insert delivery manifest detail data fail",appException);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;
		}

		
		/* Comment by Sompote 2010-04-07
		/// <summary>
		/// //Automatically Assing to Manifests (DMS Phase 1)
		/// 13. Identify the line haul route from origin and destination stations.
		/// </summary>
		public static String GetLineHaulOfAutoManifest(String strAppID, String strEnterpriseID, 
			String originStateCode, String destinationStateCode)
		{
			String path_code = "";
			SessionDS sessionDS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","GetLineHaulOfAutoManifest","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DeliveryManifestMgrDAL::GetLineHaulOfAutoManifest()] DbConnection is null","ERROR");
				return path_code;
			}

			StringBuilder str = new StringBuilder();
			str.Append("select path_code ");
			str.Append(" from Delivery_Path  ");
			str.Append(" where delivery_type = 'L' ");
			str.Append(" and applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("' and origin_station = '");
			str.Append(Utility.ReplaceSingleQuote(originStateCode));
			str.Append("' and destination_station = '");
			str.Append(Utility.ReplaceSingleQuote(destinationStateCode));
			str.Append("'");

			sessionDS = dbCon.ExecuteQuery(str.ToString(),0, 0,"HaulOfAutoManifest");

			if (sessionDS.ds.Tables[0].Rows.Count > 0)
				path_code = (String)sessionDS.ds.Tables[0].Rows[0]["path_code"];

			return  path_code;
		}
		*/
		/// <summary>
		/// Add by Sompote 2010-04-07
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="originStateCode"></param>
		/// <param name="destinationStateCode"></param>
		/// <returns></returns>
		public static String GetLineHaulOfAutoManifest(String strAppID, String strEnterpriseID, 
			String originStateCode, String destinationStateCode)
		{			
			String path_code = "";
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","GetLineHaulOfAutoManifest","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DeliveryManifestMgrDAL::GetLineHaulOfAutoManifest()] DbConnection is null","ERROR");
				return path_code;
			}

			return  GetLineHaulOfAutoManifest( strAppID,  strEnterpriseID, originStateCode,  destinationStateCode, dbCon);
		}
		/// <summary>
		/// Add by Sompote 2010-04-07
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="originStateCode"></param>
		/// <param name="destinationStateCode"></param>
		/// <param name="dbCon"></param>
		/// <returns></returns>
		public static String GetLineHaulOfAutoManifest(String strAppID, String strEnterpriseID, 
			String originStateCode, String destinationStateCode,DbConnection dbCon)
		{
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","GetLineHaulOfAutoManifest","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryManifestMgrDAL","GetLineHaulOfAutoManifest","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryManifestMgrDAL","GetLineHaulOfAutoManifest","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	

			//return  GetLineHaulOfAutoManifest( strAppID,  strEnterpriseID, originStateCode,  destinationStateCode, dbCon, dbCmd);
			string result ;	
			try
			{
				result = GetLineHaulOfAutoManifest( strAppID,  strEnterpriseID, originStateCode,  destinationStateCode, dbCon, dbCmd);
			}
			catch(ApplicationException appExpection)
			{
				throw appExpection;
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return result;
		}
		/// <summary>
		/// Add by Sompote 2010-04-07
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="originStateCode"></param>
		/// <param name="destinationStateCode"></param>
		/// <param name="dbCon"></param>
		/// <param name="dbCmd"></param>
		/// <returns></returns>
		public static String GetLineHaulOfAutoManifest(String strAppID, String strEnterpriseID, 
			String originStateCode, String destinationStateCode,DbConnection dbCon,IDbCommand dbCmd)
		{
			String path_code = "";
			SessionDS sessionDS = null;

			//DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			/*if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","GetLineHaulOfAutoManifest","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DeliveryManifestMgrDAL::GetLineHaulOfAutoManifest()] DbConnection is null","ERROR");
				return path_code;
			}*/

			StringBuilder str = new StringBuilder();
			str.Append("select path_code ");
			str.Append(" from Delivery_Path  ");
			str.Append(" where delivery_type = 'L' ");
			str.Append(" and applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("' and origin_station = '");
			str.Append(Utility.ReplaceSingleQuote(originStateCode));
			str.Append("' and destination_station = '");
			str.Append(Utility.ReplaceSingleQuote(destinationStateCode));
			str.Append("'");

			dbCmd.CommandText = str.ToString();
			//sessionDS = dbCon.ExecuteQuery(dbCmd,0, 0,"HaulOfAutoManifest");
			sessionDS = new SessionDS();
			sessionDS.ds = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			if (sessionDS.ds.Tables[0].Rows.Count > 0)
				path_code = (String)sessionDS.ds.Tables[0].Rows[0]["path_code"];

			return  path_code;
		}
	

		public static String IsAutomanifest(String strAppID, String strEnterpriseID, 
			String serviceCode)
		{
			String automanifest = "";
			SessionDS sessionDS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","IsAutomanifest","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DeliveryManifestMgrDAL::IsAutomanifest()] DbConnection is null","ERROR");
				return automanifest;
			}

			StringBuilder str = new StringBuilder();

			str = new StringBuilder();
			str.Append("select automanifest ");
			str.Append(" from Service  ");
			str.Append(" where applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("' and service_code = '");
			str.Append(Utility.ReplaceSingleQuote(serviceCode));
			str.Append("'");

			sessionDS = dbCon.ExecuteQuery(str.ToString(),0, 0,"IsAutomanifest");

			if (sessionDS.ds.Tables[0].Rows.Count > 0)
				automanifest = (String)sessionDS.ds.Tables[0].Rows[0]["automanifest"];

			return  automanifest;
		}



		public static bool IsExistingRecord(String strAppID,String strEnterpriseID,
			String pathCode, String flightVehicleNo, DateTime deliveryManifestDatetime,
			String consignmentNo)
		{
			SessionDS sessionDS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","IsAutomanifest","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DeliveryManifestMgrDAL::IsAutomanifest()] DbConnection is null","ERROR");
				return false;
			}

			String strDelManifestDateTime=Utility.DateFormat(strAppID,strEnterpriseID,deliveryManifestDatetime,DTFormat.DateLongTime);

			StringBuilder str = new StringBuilder();
			str.Append("select * ");
			str.Append(" from delivery_manifest_detail  ");
			str.Append(" where applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("' and path_code = '");
			str.Append(Utility.ReplaceSingleQuote(pathCode));
			str.Append("' and flight_vehicle_no = '");
			str.Append(Utility.ReplaceSingleQuote(flightVehicleNo));
			str.Append("' and delivery_manifest_datetime = ");
			str.Append(strDelManifestDateTime);
			str.Append(" and consignment_no = '");
			str.Append(Utility.ReplaceSingleQuote(consignmentNo));
			str.Append("'");

			sessionDS = dbCon.ExecuteQuery(str.ToString(),0, 0,"IsExistingRecord");

			if (sessionDS.ds.Tables[0].Rows.Count > 0)
				return true;
			else
				return false;
		}


		public static String GetOriginByPath(String strAppID, String strEnterpriseID, String pathCode)
		{
				String origin = "";
				DataSet dsOrigin = null;
				DbConnection dbConApp = null;
				IDbCommand dbCmd = null;			
				dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
				
				if(dbConApp == null)
				{
					Logger.LogTraceError("DeliveryManifestMgrDAL","GetOriginByPath","EDB101","DbConnection object is null!!");
					System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DeliveryManifestMgrDAL::GetOriginByPath()] DbConnection is null","ERROR");
					return origin;
				}

				StringBuilder str = new StringBuilder();
			//select origin_station from DELIVERY_PATH where path_code = '"+newRouteCode+"' and applicationid='"+strAppID+"' and  enterpriseid= '"+strEnterpriseID+"' 
				str.Append("select origin_station ");
				str.Append(" from DELIVERY_PATH  ");
				str.Append(" where applicationid = '");
				str.Append(strAppID);
				str.Append("' and enterpriseid = '");
				str.Append(strEnterpriseID);
				str.Append("' and path_code = '");
				str.Append(Utility.ReplaceSingleQuote(pathCode));
				str.Append("'");

				dbCmd = dbConApp.CreateCommand(str.ToString(),CommandType.Text);
				try
				{
					dsOrigin =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				}
				catch(ApplicationException appExpection)
				{
					Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DepartureRecord","ERR002","Error in the query String");
					throw appExpection;
				}

			if(dsOrigin.Tables[0].Rows.Count > 0)
			{
				origin = dsOrigin.Tables[0].Rows[0][0].ToString();
			}
				return  origin;
		}
	
/*Comment by Sompote 2010-04-05
		public static bool IsTruckLeaved(String strAppID, String strEnterpriseID, String consignment_no)
		{
			DataSet dsTKV = null;
			DbConnection dbConApp = null;
			IDbCommand dbCmd = null;			
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
				
			if(dbConApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","GetOriginByPath","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DeliveryManifestMgrDAL::GetOriginByPath()] DbConnection is null","ERROR");
				return false;
			}

			StringBuilder str = new StringBuilder();
			str.Append("select * ");
			str.Append(" from shipment_tracking  ");
			str.Append(" where applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("' and consignment_no = '");
			str.Append(Utility.ReplaceSingleQuote(consignment_no));
			str.Append("' and status_code = 'TKV'");

			dbCmd = dbConApp.CreateCommand(str.ToString(),CommandType.Text);
			try
			{
				dsTKV =(DataSet)dbConApp.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DepartureRecord","ERR002","Error in the query String");
				throw appExpection;
			}

			if(dsTKV.Tables[0].Rows.Count > 0)
			{
				return true;
			}
			return  false;
		}
	
*/
		/// <summary>
		/// Add by Sompote 2010-04-05 
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="consignment_no"></param>
		/// <returns></returns>
		public static bool IsTruckLeaved(String strAppID, String strEnterpriseID, String consignment_no)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
				
			if(dbCon == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","GetOriginByPath","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DeliveryManifestMgrDAL::GetOriginByPath()] DbConnection is null","ERROR");
				return false;
			}	
			return IsTruckLeaved( strAppID,  strEnterpriseID,  consignment_no, dbCon);
		}
	
		/// <summary>
		/// Add by Sompote 2010-04-05 
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="consignment_no"></param>
		/// <param name="dbCon"></param>
		/// <returns></returns>
		public static bool IsTruckLeaved(String strAppID, String strEnterpriseID, String consignment_no,DbConnection dbCon)
		{
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	
			//return IsTruckLeaved( strAppID,  strEnterpriseID,  consignment_no, dbCon, dbCmd);
			bool result = false ;	
			try
			{
				result = IsTruckLeaved( strAppID,  strEnterpriseID,  consignment_no, dbCon, dbCmd);
			}
			catch(ApplicationException appExpection)
			{
				throw appExpection;
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return result;

		}
	
		/// <summary>
		/// Add by Sompote 2010-04-05 
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="consignment_no"></param>
		/// <param name="dbCon"></param>
		/// <param name="dbCmd"></param>
		/// <returns></returns>
		public static bool IsTruckLeaved(String strAppID, String strEnterpriseID, String consignment_no,DbConnection dbCon,IDbCommand dbCmd)
		{
			DataSet dsTKV = null;
			//DbConnection dbConApp = null;
			//IDbCommand dbCmd = null;			
			//dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
				
			/*if(dbConApp == null)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","GetOriginByPath","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DeliveryManifestMgrDAL::GetOriginByPath()] DbConnection is null","ERROR");
				return false;
			}*/

			StringBuilder str = new StringBuilder();
			str.Append("select * ");
			str.Append(" from shipment_tracking  ");
			str.Append(" where applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("' and consignment_no = '");
			str.Append(Utility.ReplaceSingleQuote(consignment_no));
			str.Append("' and status_code = 'TKV'");

			//dbCmd = dbConApp.CreateCommand(str.ToString(),CommandType.Text);
			dbCmd.CommandText = str.ToString();
			try
			{
				dsTKV =(DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("DeliveryManifestMgrDAL","DeliveryManifestMgrDAL","DepartureRecord","ERR002","Error in the query String");
				throw appExpection;
			}

			if(dsTKV.Tables[0].Rows.Count > 0)
			{
				return true;
			}
			return  false;
		}
	


	}

}

		