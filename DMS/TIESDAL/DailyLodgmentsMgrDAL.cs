using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;

namespace TIESDAL
{
	public class DailyLodgmentsMgrDAL 
	{
		public DailyLodgmentsMgrDAL()
		{

		}

		public static System.Data.DataSet QueryDailyLodgments(string appID, string enterpriseId, string BatchDate, string Location, string ServiceType)
		{
			DataSet ds = null;
			IDbCommand dbCmd = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseId);
			if(dbCon == null)
			{
				Logger.LogTraceError("module1","DailyLodgmentsMgrDAL","QueryDailyLodgments","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseId",enterpriseId));
				if(BatchDate ==null)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@BatchDate",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@BatchDate",BatchDate));
				}
				if(Location.Trim() !="0")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Location",Location));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Location",DBNull.Value));
				}
				
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ServiceType",ServiceType));

				dbCmd = dbCon.CreateCommand("QueryDailyLodgments",storedParams, CommandType.StoredProcedure);
				ds = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);  
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return ds;
		}	
		
		public static int UpdateDailyLodgment(string appID, string enterpriseId, string Batch_no, string BatchID,
												string BatchDate, string Created_By, string ServiceType,
												string DepartureDT, string ArrivalDT, string FlightNo, string AWB)
		{
			int rowEffects = 0;
			IDbCommand dbCmd = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseId);
			if(dbCon == null)
			{
				Logger.LogTraceError("module1","DailyLodgmentsMgrDAL","UpdateDailyLodgment","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
				//string dNow= DateTime.Now.ToString("yyyy-MM-dd HH:mm");
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseId",enterpriseId));
				if(Batch_no ==null)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Batch_no",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Batch_no",Batch_no));
				}
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@BatchID",BatchID));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@BatchDate",BatchDate));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Created_By",Created_By));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ServiceType",ServiceType));
				if(DepartureDT != null && DepartureDT.Trim() != "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@DepartureDT",DepartureDT));
				}
				else
				{					
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@DepartureDT",DBNull.Value));
				}
				if(ArrivalDT != null && ArrivalDT.Trim() != "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ArrivalDT",ArrivalDT));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ArrivalDT",DBNull.Value));
				}
				if(FlightNo.Trim() != "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@FlightNo",FlightNo));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@FlightNo",DBNull.Value));
				}
				
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@AWB",AWB));
				

				dbCmd = dbCon.CreateCommand("UpdateDailyLodgment",storedParams, CommandType.StoredProcedure);
				DataSet ds = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);  
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return rowEffects;
		}


		public static System.Data.DataSet QueryDailyLodgmentsSummaryReport(string appID, string enterpriseId, 
																			string BatchDateFrom, string BatchDateTO, 
																			string Location, string ServiceType)
		{
			DataSet ds = null;
			IDbCommand dbCmd = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseId);
			if(dbCon == null)
			{
				Logger.LogTraceError("module1","DailyLodgmentsMgrDAL","QueryDailyLodgmentsSummaryReport","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",appID));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseId",enterpriseId));
				if(BatchDateFrom != null && BatchDateFrom.Trim() != "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@BatchDateFrom",BatchDateFrom));					
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@BatchDateFrom",DBNull.Value));
				}
				if(BatchDateTO != null && BatchDateTO.Trim() != "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@BatchDateTO",BatchDateTO));					
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@BatchDateTO",DBNull.Value));
				}
				
				if(Location != null && Location.Trim() != "" && Location.Trim() != "0")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Location",Location));					
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Location",DBNull.Value));
				}

				if(ServiceType !=null && ServiceType.Trim() != "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ServiceType",ServiceType));					
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ServiceType",DBNull.Value));
				}
				

				dbCmd = dbCon.CreateCommand("DailyLodgmentsSummaryReport",storedParams, CommandType.StoredProcedure);
				ds = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);  
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return ds;
		}	
	}
}
