using System;
using System.Data;
using com.common.DAL;
using com.common.util ;
namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for PRBManager.
	/// </summary>
	public class PRBManager
	{
		public PRBManager()
		{
		}	
	
		public static DataSet GetPickupRequestData(String strAppID,String strEnterpriseID)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			string strPickupRequest=null;
			strPickupRequest ="select * from Pickup_Request";
			DataSet  dsPickupRequest =(DataSet)dbCon.ExecuteQuery(strPickupRequest,ReturnType.DataSetType); 
			return dsPickupRequest;		
		}
		/*public static DataSet GetCustInfo(String strAppID,String strEnterpriseID)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			String strCustQry = null;
			string strCash = "CASH";
			strCustQry = "select  * from customer where custid = '"+strCash+"'";
			DataSet dsCustomer = (DataSet)dbCon.ExecuteQuery(strCustQry,ReturnType.DataSetType);
			return dsCustomer;
		}
		public static DataSet GetZipCode(String strAppID,String strEnterpriseID,String strZipCode)
		{
			DbConnection dbCon1 = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			String strZipQry = null;
			strZipQry ="select * from zipcode where zipcode = '"+strZipCode+"'";
			DataSet dsZip = (DataSet)dbCon1.ExecuteQuery(strZipQry,ReturnType.DataSetType);
			return dsZip;
		}*/
		public static DataSet GetSearchInfo(String strAppID,String strEnterpriseID,String TbName,String FldName,String Fldvalue)
		{
			DbConnection dbCon2 = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			String strSearchQry=null;
			strSearchQry ="Select * from "+TbName+" where "+FldName+" = '"+Fldvalue+"'";
			DataSet dsSearch =(DataSet)dbCon2.ExecuteQuery(strSearchQry,ReturnType.DataSetType);
			return dsSearch;
		}	
		public static DataSet GetEmptyPRBDS()
		{ 
			DataTable dtPickupRequest = new DataTable();
			dtPickupRequest.Columns.Add("booking_no",typeof(string));
			dtPickupRequest.Columns.Add("booking_datetime",typeof(string));
			dtPickupRequest.Columns.Add("booking_type",typeof(string));
			dtPickupRequest.Columns.Add("new_account",typeof(string));
			dtPickupRequest.Columns.Add("payerid",typeof(string));
			dtPickupRequest.Columns.Add("payer_type",typeof(string));
			dtPickupRequest.Columns.Add("payer_name",typeof(string));
			dtPickupRequest.Columns.Add("payer_address1",typeof(string));
			dtPickupRequest.Columns.Add("payer_address2",typeof(string));
			dtPickupRequest.Columns.Add("payer_zipcode",typeof(string));
			dtPickupRequest.Columns.Add("payer_country",typeof(string));
			dtPickupRequest.Columns.Add("payer_telephone",typeof(string));
			dtPickupRequest.Columns.Add("payer_fax",typeof(string));
			dtPickupRequest.Columns.Add("payment_mode",typeof(string));
			dtPickupRequest.Columns.Add("sender_name",typeof(string));
			dtPickupRequest.Columns.Add("sender_address1",typeof(string));
			dtPickupRequest.Columns.Add("sender_address2",typeof(string));
			dtPickupRequest.Columns.Add("sender_zipcode",typeof(string));
			dtPickupRequest.Columns.Add("sender_country",typeof(string));
			dtPickupRequest.Columns.Add("sender_contact_person",typeof(string));
			dtPickupRequest.Columns.Add("sender_telephone",typeof(string));
			dtPickupRequest.Columns.Add("sender_fax",typeof(string));
			dtPickupRequest.Columns.Add("req_pickup_datetime",typeof(string));
			dtPickupRequest.Columns.Add("act_pickup_datetime",typeof(string));
			dtPickupRequest.Columns.Add("payment_type",typeof(string));
			dtPickupRequest.Columns.Add("tot_pkg",typeof(string));
			dtPickupRequest.Columns.Add("tot_wt",typeof(string));
			dtPickupRequest.Columns.Add("tot_dim_wt",typeof(string));
			dtPickupRequest.Columns.Add("cash_amount",typeof(string));
			dtPickupRequest.Columns.Add("cash_collected",typeof(string));
			dtPickupRequest.Columns.Add("special_rates",typeof(string));
			dtPickupRequest.Columns.Add("latest_status_code",typeof(string));
			dtPickupRequest.Columns.Add("latest_exception_code",typeof(string));
			dtPickupRequest.Columns.Add("remark",typeof(string));
			dtPickupRequest.Columns.Add("quotation_no",typeof(string));
			dtPickupRequest.Columns.Add("quotation_version",typeof(string));
			
			DataRow drEach =dtPickupRequest.NewRow();
			drEach[0]="";
			dtPickupRequest.Rows.Add (drEach);
			DataSet dsPickupRequest = new DataSet();
			dsPickupRequest.Tables.Add(dtPickupRequest);
			return dsPickupRequest;
		}
		public static int AddPickupRequest(String AppID,String EnterpriseID,DataSet dsPickupRequest)
		{
			string strInsertQry=null;
			string appid=AppID;
			string Enterpriseid = EnterpriseID;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
			DataRow drEach = dsPickupRequest.Tables[0].Rows[0];
				
			strInsertQry ="Insert into Pickup_Request(applicationid,enterpriseid,booking_no,booking_datetime,";
			strInsertQry=strInsertQry+"new_account,payerid,payer_type,payer_name,payer_address1,payer_address2,";
			strInsertQry=strInsertQry+"payer_zipcode,payer_country,payer_telephone,payer_fax,";
			strInsertQry=strInsertQry+"sender_name,sender_address1,sender_address2,sender_zipcode,sender_country,";
			strInsertQry=strInsertQry+"sender_contact_person,sender_telephone,sender_fax,req_pickup_datetime,";
			strInsertQry=strInsertQry+"act_pickup_datetime,tot_pkg,tot_wt,tot_dim_wt,cash_amount,";
			strInsertQry=strInsertQry+"cash_collected,latest_status_code,latest_exception_code,";
			strInsertQry=strInsertQry+"remark,quotation_no,quotation_version";
			strInsertQry=strInsertQry+") values(";
			strInsertQry=strInsertQry+"'"+appid+"'"+",";
			strInsertQry=strInsertQry+"'"+Enterpriseid+"'"+",";
			strInsertQry=strInsertQry+System.Convert.ToInt64(drEach["booking_no"])+",";
			if (drEach["booking_datetime"].ToString() =="")
			{
				strInsertQry=strInsertQry+"''"+",";
			}	
			else
			{
				strInsertQry=strInsertQry+"'"+System.Convert.ToDateTime(drEach["booking_datetime"])+"'"+",";
				
			}	 
			//			if(drEach["booking_type"].ToString() =="")
			//			{
			//				strInsertQry=strInsertQry+"''"+",";						
			//			}
			//			else
			//			{
			//				strInsertQry=strInsertQry+"'"+(string)drEach["booking_type"]+"'"+",";						
			//				
			//			}

			if(drEach["new_account"].ToString()=="")
			{
				strInsertQry=strInsertQry+"''"+",";	
			}
			else
			{	
				strInsertQry=strInsertQry+"'"+(string)drEach["new_account"]+"'"+",";	
			}

			if(drEach["payerid"].ToString() =="")

			{
				strInsertQry=strInsertQry+"''" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+(string)drEach["payerid"]+"'"+",";
			}


			if(drEach["payer_type"].ToString()=="")
			{
				strInsertQry=strInsertQry+"''"+",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+(string)drEach["payer_type"]+"'"+",";
			}

			if(drEach["payer_name"].ToString()=="")
			{
				strInsertQry=strInsertQry+"N''" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"N'"+(string)drEach["payer_name"]+"'"+",";
			}
			if(drEach["payer_address1"].ToString()=="")
			{
				strInsertQry=strInsertQry+"N''" +",";
			}
			else
			{
				strInsertQry=strInsertQry+"N'"+(string)drEach["payer_address1"]+"'"+",";
				
			}
			if(drEach["payer_address2"].ToString()=="")
			{
				strInsertQry=strInsertQry+"N''" +",";
			}
			else
			{
				strInsertQry=strInsertQry+"N'"+(string)drEach["payer_address2"]+"'"+",";
				
			}
			if(drEach["payer_zipcode"].ToString()=="")
			{
				strInsertQry=strInsertQry+"''" +",";
			}
			else
			{
				strInsertQry=strInsertQry+"'"+(string)drEach["payer_zipcode"]+"'"+",";
				
			}
			if(drEach["payer_country"].ToString()=="")
			{
				strInsertQry=strInsertQry+"''" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+(string)drEach["payer_country"]+"'"+",";
			}
			if(drEach["payer_telephone"].ToString()=="")
			{
				strInsertQry=strInsertQry+"''" +",";
			}
			else
			{
				strInsertQry=strInsertQry+"'"+(string)drEach["payer_telephone"]+"'"+",";
				
			}	

			if(drEach["payer_fax"].ToString()=="")
			{
				strInsertQry=strInsertQry+"''" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+(string)drEach["payer_fax"]+"'"+",";
			}	
			//			if(drEach["payment_mode"].ToString()=="")
			//			{
			//				strInsertQry=strInsertQry+"''"+",";
			//				
			//			}
			//			else
			//			{
			//				strInsertQry=strInsertQry+"'"+(string)drEach["payment_mode"]+"'"+",";
			//			}	
			if(drEach["sender_name"].ToString()=="")
			{
				strInsertQry=strInsertQry+"N''" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"N'"+(string)drEach["sender_name"]+"'"+",";
			}		
			if(drEach["sender_address1"].ToString()=="")
			{
				strInsertQry=strInsertQry+"N''" +",";
			}
			else
			{
				strInsertQry=strInsertQry+"N'"+(string)drEach["sender_address1"]+"'"+",";
				
			}		
				
			if(drEach["sender_address2"].ToString()=="")
			{
				strInsertQry=strInsertQry+"N''" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"N'"+(string)drEach["sender_address2"]+"'"+",";
			}		
			if(drEach["sender_zipcode"].ToString()=="")
			{
				strInsertQry=strInsertQry+"''" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+(string)drEach["sender_zipcode"]+"'"+",";
			}			
			if(drEach["sender_country"].ToString()=="")
			{
				strInsertQry=strInsertQry+"''" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+(string)drEach["sender_country"]+"'"+",";
			}				
				
			if(drEach["sender_contact_person"].ToString()=="")
			{
				strInsertQry=strInsertQry+"N''" +",";
			}
			else
			{
				strInsertQry=strInsertQry+"N'"+(string)drEach["sender_contact_person"]+"'"+",";
				
			}				
			if(drEach["sender_telephone"].ToString()=="")
			{
				strInsertQry=strInsertQry+"''" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+(string)drEach["sender_telephone"]+"'"+",";
			}					
			if(drEach["sender_fax"].ToString()=="")
			{
				strInsertQry=strInsertQry+"''" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+(string)drEach["sender_fax"]+"'"+",";
			}					
			if(drEach["req_pickup_datetime"].ToString()=="")
			{
				strInsertQry=strInsertQry+"0"+",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+System.Convert.ToDateTime(drEach["req_pickup_datetime"])+"'"+",";
			}	

			if(drEach["act_pickup_datetime"].ToString()=="")
			{
				strInsertQry=strInsertQry+"0"+",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+System.Convert.ToDateTime(drEach["act_pickup_datetime"])+"'"+",";
			}					
			//			if(drEach["payment_type"].ToString()=="")
			//			{
			//				strInsertQry=strInsertQry+"''" +",";
			//				
			//			}
			//			else
			//			{
			//				strInsertQry=strInsertQry+"'"+(string)drEach["payment_type"]+"'"+",";
			//			}					
				
			if(drEach["tot_pkg"].ToString()=="")
			
			{
				strInsertQry=strInsertQry+"0" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+System.Convert.ToInt64(drEach["tot_pkg"])+"'"+",";
			}					
				
			if(drEach["tot_wt"].ToString()=="")
			{
				strInsertQry=strInsertQry+"0" +",";
			}
			else
			{
				strInsertQry=strInsertQry+"'"+(decimal)drEach["tot_wt"]+"'"+",";
			}					
			if(drEach["tot_dim_wt"].ToString()=="")
			{
				strInsertQry=strInsertQry+"0" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+(decimal)drEach["tot_dim_wt"]+"'"+",";
			}					

			if(drEach["cash_amount"].ToString()=="")
			{
				strInsertQry=strInsertQry+"0" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+(decimal)drEach["cash_amount"]+"'"+",";
			}					
			if(drEach["cash_collected"].ToString()=="")
			{
				strInsertQry=strInsertQry+"0" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+(decimal)drEach["cash_collected"]+"'"+",";
			}					
				
			//			if(drEach["special_rates"].ToString()=="")
			//			{
			//				strInsertQry=strInsertQry+"''" +",";
			//				
			//			}
			//			else
			//			{
			//				strInsertQry=strInsertQry+"'"+(string)drEach["special_rates"]+"'"+",";
			//			}
			if(drEach["latest_status_code"].ToString()=="")
			{
				strInsertQry=strInsertQry+"" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+(string)drEach["latest_status_code"]+"'"+",";
			}
			if(drEach["latest_exception_code"].ToString()=="")
			{
				strInsertQry=strInsertQry+"''" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+(string)drEach["latest_exception_code"]+"'"+",";
			}
			if(drEach["remark"].ToString()=="")
			{
				strInsertQry=strInsertQry+"N''" +",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"N'"+(string)drEach["remarks"]+"'"+",";
			}
			if(drEach["quotation_no"].ToString()=="")
			{
				strInsertQry=strInsertQry+"''"+",";
				
			}
			else
			{
				strInsertQry=strInsertQry+"'"+System.Convert.ToInt64(drEach["quotation_no"])+"'"+",";
			}
			if(drEach["quotation_version"].ToString()=="")
			{
				strInsertQry=strInsertQry+"0"+")";
			}
			else
			{
				strInsertQry=strInsertQry+System.Convert.ToInt64(drEach["quotation_version"])+")";
			}
			IDbCommand dbcmd = dbCon.CreateCommand(strInsertQry,CommandType.Text);
			int rowsreturned = dbCon.ExecuteNonQuery(dbcmd);
			return rowsreturned;
				
		}
		
	}
}




