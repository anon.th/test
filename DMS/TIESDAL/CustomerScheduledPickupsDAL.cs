using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for CustomerScheduledPickupsDAL.
	/// </summary>
	public class CustomerScheduledPickupsDAL
	{

		public static DataSet GetEmptyCustomerSchedPickups()
		{
			DataTable dtCustomerSchedPickups = new DataTable();
			dtCustomerSchedPickups.Columns.Add(new DataColumn("custid", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("cust_ref_name", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("reference", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("pubholiday", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("mon_putime1", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("mon_putime2", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("mon_putime3", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("tue_putime1", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("tue_putime2", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("tue_putime3", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("wed_putime1", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("wed_putime2", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("wed_putime3", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("thu_putime1", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("thu_putime2", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("thu_putime3", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("fri_putime1", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("fri_putime2", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("fri_putime3", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("sat_putime1", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("sat_putime2", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("sat_putime3", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("sun_putime1", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("sun_putime2", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("sun_putime3", typeof(DateTime)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("mon_active", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("tue_active", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("wed_active", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("thu_active", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("fri_active", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("sat_active", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("sun_active", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("mon_routecode1", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("mon_routecode2", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("mon_routecode3", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("tue_routecode1", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("tue_routecode2", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("tue_routecode3", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("wed_routecode1", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("wed_routecode2", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("wed_routecode3", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("thu_routecode1", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("thu_routecode2", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("thu_routecode3", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("fri_routecode1", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("fri_routecode2", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("fri_routecode3", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("sat_routecode1", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("sat_routecode2", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("sat_routecode3", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("sun_routecode1", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("sun_routecode2", typeof(string)));
			dtCustomerSchedPickups.Columns.Add(new DataColumn("sun_routecode3", typeof(string)));
			DataRow drEach = dtCustomerSchedPickups.NewRow();
	

			dtCustomerSchedPickups.Rows.Add(drEach);
			DataSet dsCustomerSchedPickups = new DataSet();
			dsCustomerSchedPickups.Tables.Add(dtCustomerSchedPickups);
			return  dsCustomerSchedPickups;	
		}



		public static SessionDS GetCustomerSchedPickupsDS()
		{
			DataSet dsCustomerSchedPickups = GetEmptyCustomerSchedPickups();

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsCustomerSchedPickups;
			
			sessionDS.DataSetRecSize = dsCustomerSchedPickups.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			
			return sessionDS;
		}


		public static SessionDS GetEnterpriseProfile(String strAppID, String strEnterpriseID,
			int iCurrent, int iDSRecSize)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","GetEnterpriseProfile","GetEnterpriseProfile","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder str = new StringBuilder();
			str.Append("SELECT * ");
			str.Append("FROM Enterprise ");
			str.Append("WHERE applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("'");
			
			sessionDS = dbCon.ExecuteQuery(str.ToString(),iCurrent, iDSRecSize,"Enterprise");
			return  sessionDS;	
		}



		public static void AddCustomerSchedPickups(String strAppID, String strEnterpriseID, 
			DataSet dsCustomerSchedPickups, String userCulture)
		{
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			if (dsCustomerSchedPickups == null)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","DataSet is null!!");
				throw new ApplicationException("The Customer Schedule Pickups DataSet is null",null);
			}
			
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}



//			DataSet CSPData = GetCustomerSchedPickupsData(strAppID, strEnterpriseID, 0, 0, dsCustomerSchedPickups,ref dbCmd,ref dbCon, ref conApp,ref transactionApp).ds;
//			if(CSPData.Tables[0].Rows.Count > 0)
//			{
//				throw new ApplicationException(Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CS_PICK", userCulture),null);
//			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			DataSet CSPData = GetCustomerSchedPickupsData(strAppID, strEnterpriseID, 0, 0, dsCustomerSchedPickups,ref dbCmd,ref dbCon, ref conApp,ref transactionApp).ds;
			if(CSPData.Tables[0].Rows.Count > 0)
			{
				throw new ApplicationException(Utility.GetLanguageText(ResourceType.UserMessage,"DUP_KEY_FOUND_CS_PICK", userCulture),null);
			}


			try
			{
				DataRow drEach = dsCustomerSchedPickups.Tables[0].Rows[0];

				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append("INSERT INTO Customer_Sched_Pickups(applicationid, enterpriseid,");
				strBuilder.Append("custid, cust_ref_name, reference, pubholiday,");
				strBuilder.Append("mon_putime1, mon_putime2, mon_putime3,");
				strBuilder.Append("tue_putime1, tue_putime2, tue_putime3,");
				strBuilder.Append("wed_putime1, wed_putime2, wed_putime3,");
				strBuilder.Append("thu_putime1, thu_putime2, thu_putime3,");
				strBuilder.Append("fri_putime1, fri_putime2, fri_putime3,");
				strBuilder.Append("sat_putime1, sat_putime2, sat_putime3,");
				strBuilder.Append("sun_putime1, sun_putime2, sun_putime3,");
				strBuilder.Append("mon_active, tue_active, wed_active,");
				strBuilder.Append("thu_active, fri_active, sat_active, sun_active,");
				strBuilder.Append("mon_routecode1, mon_routecode2, mon_routecode3,");
				strBuilder.Append("tue_routecode1, tue_routecode2, tue_routecode3,");
				strBuilder.Append("wed_routecode1, wed_routecode2, wed_routecode3,");
				strBuilder.Append("thu_routecode1, thu_routecode2, thu_routecode3,");
				strBuilder.Append("fri_routecode1, fri_routecode2, fri_routecode3,");
				strBuilder.Append("sat_routecode1, sat_routecode2, sat_routecode3,");
				strBuilder.Append("sun_routecode1, sun_routecode2, sun_routecode3)");
				strBuilder.Append(" VALUES ('"+strAppID+"',"+"'"+strEnterpriseID+"',");

				//custid
				if((drEach["custid"]!= null) && (!drEach["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strCustID = Utility.ReplaceSingleQuote(drEach["custid"].ToString());
					strBuilder.Append("N'");
					strBuilder.Append(strCustID);
					strBuilder.Append("'");
				}
				strBuilder.Append(",");

				//cust_ref_name
				if((drEach["cust_ref_name"]!= null) && (!drEach["cust_ref_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strCustRefName = Utility.ReplaceSingleQuote(drEach["cust_ref_name"].ToString());
					strBuilder.Append("N'");
					strBuilder.Append(strCustRefName);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//reference
				if((drEach["reference"]!= null) && (!drEach["reference"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strReference = Utility.ReplaceSingleQuote(drEach["reference"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(strReference);
					strBuilder.Append("'");
				}
				strBuilder.Append(",");

				//pubholiday
				if((drEach["pubholiday"]!= null) && (!drEach["pubholiday"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPubholiday = Utility.ReplaceSingleQuote(drEach["pubholiday"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(strPubholiday);
					strBuilder.Append("'");
				}
				strBuilder.Append(",");

				//mon_putime1
				if((drEach["mon_putime1"]!= null) && (!drEach["mon_putime1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtMon_putime1 = Convert.ToDateTime(drEach["mon_putime1"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtMon_putime1,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//mon_putime2
				if((drEach["mon_putime2"]!= null) && (!drEach["mon_putime2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtMon_putime2 = Convert.ToDateTime(drEach["mon_putime2"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtMon_putime2,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//mon_putime3
				if((drEach["mon_putime3"]!= null) && (!drEach["mon_putime3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtMon_putime3 = Convert.ToDateTime(drEach["mon_putime3"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtMon_putime3,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//tue_putime1
				if((drEach["tue_putime1"]!= null) && (!drEach["tue_putime1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtTue_putime1 = Convert.ToDateTime(drEach["tue_putime1"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtTue_putime1,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//tue_putime2
				if((drEach["tue_putime2"]!= null) && (!drEach["tue_putime2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtTue_putime2 = Convert.ToDateTime(drEach["tue_putime2"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtTue_putime2,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//tue_putime3
				if((drEach["tue_putime3"]!= null) && (!drEach["tue_putime3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtTue_putime3 = Convert.ToDateTime(drEach["tue_putime3"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtTue_putime3,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//wed_putime1
				if((drEach["wed_putime1"]!= null) && (!drEach["wed_putime1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtWed_putime1 = Convert.ToDateTime(drEach["wed_putime1"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtWed_putime1,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//wed_putime2
				if((drEach["wed_putime2"]!= null) && (!drEach["wed_putime2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtWed_putime2 = Convert.ToDateTime(drEach["wed_putime2"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtWed_putime2,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//wed_putime3
				if((drEach["wed_putime3"]!= null) && (!drEach["wed_putime3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtWed_putime3 = Convert.ToDateTime(drEach["wed_putime3"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtWed_putime3,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//thu_putime1
				if((drEach["thu_putime1"]!= null) && (!drEach["thu_putime1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtThu_putime1 = Convert.ToDateTime(drEach["thu_putime1"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtThu_putime1,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//thu_putime2
				if((drEach["thu_putime2"]!= null) && (!drEach["thu_putime2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtThu_putime2 = Convert.ToDateTime(drEach["thu_putime2"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtThu_putime2,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//thu_putime3
				if((drEach["thu_putime3"]!= null) && (!drEach["thu_putime3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtThu_putime3 = Convert.ToDateTime(drEach["thu_putime3"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtThu_putime3,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//fri_putime1
				if((drEach["fri_putime1"]!= null) && (!drEach["fri_putime1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtFri_putime1 = Convert.ToDateTime(drEach["fri_putime1"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtFri_putime1,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//fri_putime2
				if((drEach["fri_putime2"]!= null) && (!drEach["fri_putime2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtFri_putime2 = Convert.ToDateTime(drEach["fri_putime2"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtFri_putime2,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//fri_putime3
				if((drEach["fri_putime3"]!= null) && (!drEach["fri_putime3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtFri_putime3 = Convert.ToDateTime(drEach["fri_putime3"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtFri_putime3,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//sat_putime1
				if((drEach["sat_putime1"]!= null) && (!drEach["sat_putime1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtSat_putime1 = Convert.ToDateTime(drEach["sat_putime1"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtSat_putime1,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//sat_putime2
				if((drEach["sat_putime2"]!= null) && (!drEach["sat_putime2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtSat_putime2 = Convert.ToDateTime(drEach["sat_putime2"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtSat_putime2,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//sat_putime3
				if((drEach["sat_putime3"]!= null) && (!drEach["sat_putime3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtSat_putime3 = Convert.ToDateTime(drEach["sat_putime3"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtSat_putime3,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//sun_putime1
				if((drEach["sun_putime1"]!= null) && (!drEach["sun_putime1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtSun_putime1 = Convert.ToDateTime(drEach["sun_putime1"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtSun_putime1,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//sun_putime2
				if((drEach["sun_putime2"]!= null) && (!drEach["sun_putime2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtSun_putime2 = Convert.ToDateTime(drEach["sun_putime2"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtSun_putime2,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//sun_putime3
				if((drEach["sun_putime3"]!= null) && (!drEach["sun_putime3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtSun_putime3 = Convert.ToDateTime(drEach["sun_putime3"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtSun_putime3,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//mon_active
				if((drEach["mon_active"]!= null) && (!drEach["mon_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strMon_active = Utility.ReplaceSingleQuote(drEach["mon_active"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(strMon_active);
					strBuilder.Append("'");
				}
				strBuilder.Append(",");

				//tue_active
				if((drEach["tue_active"]!= null) && (!drEach["tue_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strTue_active = Utility.ReplaceSingleQuote(drEach["tue_active"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(strTue_active);
					strBuilder.Append("'");
				}
				strBuilder.Append(",");

				//wed_active
				if((drEach["wed_active"]!= null) && (!drEach["wed_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strWed_active = Utility.ReplaceSingleQuote(drEach["wed_active"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(strWed_active);
					strBuilder.Append("'");
				}
				strBuilder.Append(",");

				//thu_active
				if((drEach["thu_active"]!= null) && (!drEach["thu_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strThu_active = Utility.ReplaceSingleQuote(drEach["thu_active"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(strThu_active);
					strBuilder.Append("'");
				}
				strBuilder.Append(",");

				//fri_active
				if((drEach["fri_active"]!= null) && (!drEach["fri_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strFri_active = Utility.ReplaceSingleQuote(drEach["fri_active"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(strFri_active);
					strBuilder.Append("'");
				}
				strBuilder.Append(",");

				//sat_active
				if((drEach["sat_active"]!= null) && (!drEach["sat_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strSat_active = Utility.ReplaceSingleQuote(drEach["sat_active"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(strSat_active);
					strBuilder.Append("'");
				}
				strBuilder.Append(",");

				//sun_active
				if((drEach["sun_active"]!= null) && (!drEach["sun_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strSun_active = Utility.ReplaceSingleQuote(drEach["sun_active"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(strSun_active);
					strBuilder.Append("'");
				}
				strBuilder.Append(",");
				//mon_routecode1
				if((drEach["mon_routecode1"]!= null) && (!drEach["mon_routecode1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["mon_routecode1"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["mon_routecode1"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//mon_routecode2
				if((drEach["mon_routecode2"]!= null) && (!drEach["mon_routecode2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["mon_routecode2"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["mon_routecode2"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//mon_routecode3
				if((drEach["mon_routecode3"]!= null) && (!drEach["mon_routecode3"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["mon_routecode3"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["mon_routecode3"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//tue_routecode1
				if((drEach["tue_routecode1"]!= null) && (!drEach["tue_routecode1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["tue_routecode1"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["tue_routecode1"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//tue_routecode2
				if((drEach["tue_routecode2"]!= null) && (!drEach["tue_routecode2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["tue_routecode2"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["tue_routecode2"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//tue_routecode3
				if((drEach["tue_routecode3"]!= null) && (!drEach["tue_routecode3"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["tue_routecode3"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["tue_routecode3"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//wed_routecode1
				if((drEach["wed_routecode1"]!= null) && (!drEach["wed_routecode1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["wed_routecode1"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["wed_routecode1"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//wed_routecode2
				if((drEach["wed_routecode2"]!= null) && (!drEach["wed_routecode2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["wed_routecode2"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["wed_routecode2"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//wed_routecode3
				if((drEach["wed_routecode3"]!= null) && (!drEach["wed_routecode3"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["wed_routecode3"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["wed_routecode3"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//thu_routecode1
				if((drEach["thu_routecode1"]!= null) && (!drEach["thu_routecode1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["thu_routecode1"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["thu_routecode1"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//thu_routecode2
				if((drEach["thu_routecode2"]!= null) && (!drEach["thu_routecode2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["thu_routecode2"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["thu_routecode2"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//thu_routecode3
				if((drEach["thu_routecode3"]!= null) && (!drEach["thu_routecode3"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["thu_routecode3"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["thu_routecode3"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//fri_routecode1
				if((drEach["fri_routecode1"]!= null) && (!drEach["fri_routecode1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["fri_routecode1"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["fri_routecode1"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//fri_routecode2
				if((drEach["fri_routecode2"]!= null) && (!drEach["fri_routecode2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["fri_routecode1"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["fri_routecode2"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//fri_routecode3
				if((drEach["fri_routecode3"]!= null) && (!drEach["fri_routecode3"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["fri_routecode3"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["fri_routecode3"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//sat_routecode1
				if((drEach["sat_routecode1"]!= null) && (!drEach["sat_routecode1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["sat_routecode1"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["sat_routecode1"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//sat_routecode2
				if((drEach["sat_routecode2"]!= null) && (!drEach["sat_routecode2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["sat_routecode2"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["sat_routecode2"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//sat_routecode3
				if((drEach["sat_routecode3"]!= null) && (!drEach["sat_routecode3"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["sat_routecode3"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["sat_routecode3"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//sun_routecode1
				if((drEach["sun_routecode1"]!= null) && (!drEach["sun_routecode1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["sun_routecode1"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["sun_routecode1"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//sun_routecode2
				if((drEach["sun_routecode2"]!= null) && (!drEach["sun_routecode2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["sun_routecode2"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["sun_routecode2"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//sun_routecode3
				if((drEach["sun_routecode3"]!= null) && (!drEach["sun_routecode3"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["sun_routecode3"].ToString()!=""))
				{
					String str_routeCode = Utility.ReplaceSingleQuote(drEach["sun_routecode3"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(str_routeCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				
				strBuilder.Append(")");

				String strQuery = strBuilder.ToString();
				dbCmd = dbCon.CreateCommand(strQuery,CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;

				Logger.LogDebugInfo("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","QUERY IS : "+strQuery);
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				transactionApp.Commit();
				Logger.LogDebugInfo("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups",iRowsAffected + " rows inserted into Shipment table ....");
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error adding Customer Scheduled Pickups ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error adding Customer Scheduled Pickups ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
		}



		public static int UpdateCustomerSchedPickups(String strAppID, String strEnterpriseID,
			DataSet dsCustomerSchedPickups, String CustID, String CustRef)
		{
			
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			if (dsCustomerSchedPickups == null)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","DataSet is null!!");
				throw new ApplicationException("The Customer Schedule Pickups DataSet is null",null);
			}
			
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				if(dsCustomerSchedPickups.Tables[0].Rows.Count > 0)
				{
					DataRow drEach = dsCustomerSchedPickups.Tables[0].Rows[0];
	
					StringBuilder strBuild = new StringBuilder();
					String strCustid = "";
					String strCustRefName = "";
					strBuild.Append("UPDATE Customer_Sched_Pickups SET "); 

					//custid
					strBuild.Append("custid = ");
					if((drEach["custid"]!= null) && (!drEach["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strCustid = (String)drEach["custid"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strCustid));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//cust_ref_name
					strBuild.Append("cust_ref_name = ");
					if((drEach["cust_ref_name"]!= null) && (!drEach["cust_ref_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strCustRefName = (String)drEach["cust_ref_name"];
						strBuild.Append("N'");
						strBuild.Append(Utility.ReplaceSingleQuote(strCustRefName));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//reference
					strBuild.Append("reference = ");
					if((drEach["reference"]!= null) && (!drEach["reference"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strReference = (String)drEach["reference"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strReference));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//pubholiday
					strBuild.Append("pubholiday = ");
					if((drEach["pubholiday"]!= null) && (!drEach["pubholiday"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strPubholiday = (String)drEach["pubholiday"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strPubholiday));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//mon_putime1
					strBuild.Append("mon_putime1 = ");
					if((drEach["mon_putime1"]!= null) && (!drEach["mon_putime1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtMon_putime1 = Convert.ToDateTime(drEach["mon_putime1"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtMon_putime1,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//mon_putime2
					strBuild.Append("mon_putime2 = ");
					if((drEach["mon_putime2"]!= null) && (!drEach["mon_putime2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtMon_putime2 = Convert.ToDateTime(drEach["mon_putime2"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtMon_putime2,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//mon_putime3
					strBuild.Append("mon_putime3 = ");
					if((drEach["mon_putime3"]!= null) && (!drEach["mon_putime3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtMon_putime3 = Convert.ToDateTime(drEach["mon_putime3"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtMon_putime3,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//tue_putime1
					strBuild.Append("tue_putime1 = ");
					if((drEach["tue_putime1"]!= null) && (!drEach["tue_putime1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtTue_putime1 = Convert.ToDateTime(drEach["tue_putime1"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtTue_putime1,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//tue_putime2
					strBuild.Append("tue_putime2 = ");
					if((drEach["tue_putime2"]!= null) && (!drEach["tue_putime2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtTue_putime2 = Convert.ToDateTime(drEach["tue_putime2"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtTue_putime2,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//tue_putime3
					strBuild.Append("tue_putime3 = ");
					if((drEach["tue_putime3"]!= null) && (!drEach["tue_putime3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtTue_putime3 = Convert.ToDateTime(drEach["tue_putime3"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtTue_putime3,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//wed_putime1
					strBuild.Append("wed_putime1 = ");
					if((drEach["wed_putime1"]!= null) && (!drEach["wed_putime1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtWed_putime1 = Convert.ToDateTime(drEach["wed_putime1"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtWed_putime1,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//wed_putime2
					strBuild.Append("wed_putime2 = ");
					if((drEach["wed_putime2"]!= null) && (!drEach["wed_putime2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtWed_putime2 = Convert.ToDateTime(drEach["wed_putime2"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtWed_putime2,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//wed_putime3
					strBuild.Append("wed_putime3 = ");
					if((drEach["wed_putime3"]!= null) && (!drEach["wed_putime3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtWed_putime3 = Convert.ToDateTime(drEach["wed_putime3"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtWed_putime3,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//thu_putime1
					strBuild.Append("thu_putime1 = ");
					if((drEach["thu_putime1"]!= null) && (!drEach["thu_putime1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtThu_putime1 = Convert.ToDateTime(drEach["thu_putime1"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtThu_putime1,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//thu_putime2
					strBuild.Append("thu_putime2 = ");
					if((drEach["thu_putime2"]!= null) && (!drEach["thu_putime2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtThu_putime2 = Convert.ToDateTime(drEach["thu_putime2"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtThu_putime2,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//thu_putime3
					strBuild.Append("thu_putime3 = ");
					if((drEach["thu_putime3"]!= null) && (!drEach["thu_putime3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtThu_putime3 = Convert.ToDateTime(drEach["thu_putime3"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtThu_putime3,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//fri_putime1
					strBuild.Append("fri_putime1 = ");
					if((drEach["fri_putime1"]!= null) && (!drEach["fri_putime1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtFri_putime1 = Convert.ToDateTime(drEach["fri_putime1"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtFri_putime1,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//fri_putime2
					strBuild.Append("fri_putime2 = ");
					if((drEach["fri_putime2"]!= null) && (!drEach["fri_putime2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtFri_putime2 = Convert.ToDateTime(drEach["fri_putime2"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtFri_putime2,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//fri_putime3
					strBuild.Append("fri_putime3 = ");
					if((drEach["fri_putime3"]!= null) && (!drEach["fri_putime3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtFri_putime3 = Convert.ToDateTime(drEach["fri_putime3"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtFri_putime3,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//sat_putime1
					strBuild.Append("sat_putime1 = ");
					if((drEach["sat_putime1"]!= null) && (!drEach["sat_putime1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtSat_putime1 = Convert.ToDateTime(drEach["sat_putime1"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtSat_putime1,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//sat_putime2
					strBuild.Append("sat_putime2 = ");
					if((drEach["sat_putime2"]!= null) && (!drEach["sat_putime2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtSat_putime2 = Convert.ToDateTime(drEach["sat_putime2"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtSat_putime2,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//sat_putime3
					strBuild.Append("sat_putime3 = ");
					if((drEach["sat_putime3"]!= null) && (!drEach["sat_putime3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtSat_putime3 = Convert.ToDateTime(drEach["sat_putime3"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtSat_putime3,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//sun_putime1
					strBuild.Append("sun_putime1 = ");
					if((drEach["sun_putime1"]!= null) && (!drEach["sun_putime1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtSun_putime1 = Convert.ToDateTime(drEach["sun_putime1"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtSun_putime1,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//sun_putime2
					strBuild.Append("sun_putime2 = ");
					if((drEach["sun_putime2"]!= null) && (!drEach["sun_putime2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtSun_putime2 = Convert.ToDateTime(drEach["sun_putime2"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtSun_putime2,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//sun_putime3
					strBuild.Append("sun_putime3 = ");
					if((drEach["sun_putime3"]!= null) && (!drEach["sun_putime3"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						DateTime dtSun_putime3 = Convert.ToDateTime(drEach["sun_putime3"]);
						strBuild.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtSun_putime3,DTFormat.DateTime));
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//mon_active
					strBuild.Append("mon_active = ");
					if((drEach["mon_active"]!= null) && (!drEach["mon_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strMon_active = (String)drEach["mon_active"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strMon_active));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//tue_active
					strBuild.Append("tue_active = ");
					if((drEach["tue_active"]!= null) && (!drEach["tue_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strTue_active = (String)drEach["tue_active"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strTue_active));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//wed_active
					strBuild.Append("wed_active = ");
					if((drEach["wed_active"]!= null) && (!drEach["wed_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strWed_active = (String)drEach["wed_active"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strWed_active));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//thu_active
					strBuild.Append("thu_active = ");
					if((drEach["thu_active"]!= null) && (!drEach["thu_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strThu_active = (String)drEach["thu_active"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strThu_active));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//fri_active
					strBuild.Append("fri_active = ");
					if((drEach["fri_active"]!= null) && (!drEach["fri_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strFri_active = (String)drEach["fri_active"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strFri_active));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//sat_active
					strBuild.Append("sat_active = ");
					if((drEach["sat_active"]!= null) && (!drEach["sat_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strSat_active = (String)drEach["sat_active"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strSat_active));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//sun_active
					strBuild.Append("sun_active = ");
					if((drEach["sun_active"]!= null) && (!drEach["sun_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						String strSun_active = (String)drEach["sun_active"];
						strBuild.Append("'");
						strBuild.Append(Utility.ReplaceSingleQuote(strSun_active));
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");
					//mon_routecode1
					strBuild.Append("mon_routecode1 = ");
					if((drEach["mon_routecode1"]!= null) && (!drEach["mon_routecode1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["mon_routecode1"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["mon_routecode1"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//mon_routecode2
					strBuild.Append("mon_routecode2 = ");
					if((drEach["mon_routecode2"]!= null) && (!drEach["mon_routecode2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["mon_routecode2"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["mon_routecode2"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//mon_routecode3
					strBuild.Append("mon_routecode3 = ");
					if((drEach["mon_routecode3"]!= null) && (!drEach["mon_routecode3"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["mon_routecode3"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["mon_routecode3"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//tue_routecode1
					strBuild.Append("tue_routecode1 = ");
					if((drEach["tue_routecode1"]!= null) && (!drEach["tue_routecode1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["tue_routecode1"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["tue_routecode1"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//tue_routecode2
					strBuild.Append("tue_routecode2 = ");
					if((drEach["tue_routecode2"]!= null) && (!drEach["tue_routecode2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["tue_routecode2"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["tue_routecode2"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");

					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//tue_routecode3
					strBuild.Append("tue_routecode3 = ");
					if((drEach["tue_routecode3"]!= null) && (!drEach["tue_routecode3"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["tue_routecode3"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["tue_routecode3"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//wed_routecode1
					strBuild.Append("wed_routecode1 = ");
					if((drEach["wed_routecode1"]!= null) && (!drEach["wed_routecode1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["wed_routecode1"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["wed_routecode1"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//wed_routecode2
					strBuild.Append("wed_routecode2 = ");
					if((drEach["wed_routecode2"]!= null) && (!drEach["wed_routecode2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["wed_routecode2"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["wed_routecode2"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//wed_routecode3
					strBuild.Append("wed_routecode3 = ");
					if((drEach["wed_routecode3"]!= null) && (!drEach["wed_routecode3"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["wed_routecode3"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["wed_routecode3"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//thu_routecode1
					strBuild.Append("thu_routecode1 = ");
					if((drEach["thu_routecode1"]!= null) && (!drEach["thu_routecode1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["thu_routecode1"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["thu_routecode1"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//thu_routecode2
					strBuild.Append("thu_routecode2 = ");
					if((drEach["thu_routecode2"]!= null) && (!drEach["thu_routecode2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["thu_routecode2"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["thu_routecode2"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//thu_routecode3
					strBuild.Append("thu_routecode3 = ");
					if((drEach["thu_routecode3"]!= null) && (!drEach["thu_routecode3"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["thu_routecode3"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["thu_routecode3"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//fri_routecode1
					strBuild.Append("fri_routecode1 = ");
					if((drEach["fri_routecode1"]!= null) && (!drEach["fri_routecode1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["fri_routecode1"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["fri_routecode1"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//fri_routecode2
					strBuild.Append("fri_routecode2 = ");
					if((drEach["fri_routecode2"]!= null) && (!drEach["fri_routecode2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["fri_routecode2"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["fri_routecode2"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//fri_routecode3
					strBuild.Append("fri_routecode3 = ");
					if((drEach["fri_routecode3"]!= null) && (!drEach["fri_routecode3"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["fri_routecode3"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["fri_routecode3"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//sat_routecode1
					strBuild.Append("sat_routecode1 = ");
					if((drEach["sat_routecode1"]!= null) && (!drEach["sat_routecode1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["sat_routecode1"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["sat_routecode1"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//sat_routecode2
					strBuild.Append("sat_routecode2 = ");
					if((drEach["sat_routecode2"]!= null) && (!drEach["sat_routecode2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["sat_routecode2"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["sat_routecode2"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//sat_routecode3
					strBuild.Append("sat_routecode3 = ");
					if((drEach["sat_routecode3"]!= null) && (!drEach["sat_routecode3"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["sat_routecode3"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["sat_routecode3"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//sun_routecode1
					strBuild.Append("sun_routecode1 = ");
					if((drEach["sun_routecode1"]!= null) && (!drEach["sun_routecode1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["sun_routecode1"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["sun_routecode1"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//sun_routecode2
					strBuild.Append("sun_routecode2 = ");
					if((drEach["sun_routecode2"]!= null) && (!drEach["sun_routecode2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["sun_routecode2"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["sun_routecode2"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}
					strBuild.Append(",");

					//sun_routecode3
					strBuild.Append("sun_routecode3 = ");
					if((drEach["sun_routecode3"]!= null) && (!drEach["sun_routecode3"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["sun_routecode3"].ToString()!=""))
					{
						String str_routeCode = Utility.ReplaceSingleQuote(drEach["sun_routecode3"].ToString());
						strBuild.Append("'");
						strBuild.Append(str_routeCode);
						strBuild.Append("'");
					}
					else
					{
						strBuild.Append("null");
					}



					strBuild.Append(" where applicationid = ");
					strBuild.Append("'" + strAppID + "'");
					strBuild.Append(" and enterpriseid = ");
					strBuild.Append("'" + strEnterpriseID + "'");
					strBuild.Append(" and custid = ");
					strBuild.Append("'" + CustID + "'");

					if (strCustRefName.Trim() != "")
					{
						strBuild.Append(" and cust_ref_name = ");
						strBuild.Append("N'" + CustRef + "'");
					}
					
					dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
					dbCmd.Connection = conApp;
					dbCmd.Transaction = transactionApp;

					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups",iRowsAffected + " rows updated in Shipment table");
				}	
				transactionApp.Commit();
				Logger.LogDebugInfo("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","App db insert transaction committed.");
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error update Customer Schedule Pickups : "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CustomerScheduledPickupsDAL","AddCustomerSchedPickups","AddCustomerSchedPickups","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error update Customer Schedule Pickups ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				dbCmd.Dispose();
				conApp.Dispose();
				transactionApp.Dispose();
			}
			return iRowsAffected;
		}

		
		public static int DeleteCustomerSchedPickups(String strAppID, String strEnterpriseID,
			String custid, String cust_ref_name)
		{
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","DeleteCustomerSchedPickups","DeleteCustomerSchedPickups","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","DeleteCustomerSchedPickups","DeleteCustomerSchedPickups","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CustomerScheduledPickupsDAL","DeleteCustomerSchedPickups","DeleteCustomerSchedPickups","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CustomerScheduledPickupsDAL","DeleteCustomerSchedPickups","DeleteCustomerSchedPickups","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","DeleteCustomerSchedPickups","DeleteCustomerSchedPickups","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{

				strBuild = new StringBuilder();
				strBuild.Append("DELETE ");
				strBuild.Append("FROM Customer_Sched_Pickups ");
				strBuild.Append("WHERE applicationid = ");
				strBuild.Append("'" + strAppID +"' ");
				strBuild.Append("AND enterpriseid = ");
				strBuild.Append("'" + strEnterpriseID + "'");

				//custid
				if((custid != null) && (!custid.GetType().Equals(System.Type.GetType("System.DBNull"))) && (custid.Trim() != ""))
				{
					strBuild.Append("AND custid = N'");
					strBuild.Append(Utility.ReplaceSingleQuote(custid));
					strBuild.Append("' ");
				}

				//cust_ref_name
				if((cust_ref_name != null) && (!cust_ref_name.GetType().Equals(System.Type.GetType("System.DBNull"))) && (cust_ref_name.Trim() != ""))
				{
					strBuild.Append("AND cust_ref_name = N'");
					strBuild.Append(Utility.ReplaceSingleQuote(cust_ref_name));
					strBuild.Append("' ");
				}
				
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;

				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogDebugInfo("CustomerScheduledPickupsDAL","DeleteCustomerSchedPickups","DeleteCustomerSchedPickups",iRowsAffected + " rows deleted from Shipment_pkg table");

				transactionApp.Commit();
				Logger.LogDebugInfo("CustomerScheduledPickupsDAL","DeleteCustomerSchedPickups","DeleteCustomerSchedPickups","App db insert transaction committed.");

			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("CustomerScheduledPickupsDAL","DeleteCustomerSchedPickups","DeleteCustomerSchedPickups","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CustomerScheduledPickupsDAL","DeleteCustomerSchedPickups","DeleteCustomerSchedPickups","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("CustomerScheduledPickupsDAL","DeleteCustomerSchedPickups","DeleteCustomerSchedPickups","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Customer Schedule Pickups ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("CustomerScheduledPickupsDAL","DeleteCustomerSchedPickups","DeleteCustomerSchedPickups","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CustomerScheduledPickupsDAL","DeleteCustomerSchedPickups","DeleteCustomerSchedPickups","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CustomerScheduledPickupsDAL","DeleteCustomerSchedPickups","DeleteCustomerSchedPickups","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Customer Schedule Pickups ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				dbCmd.Dispose();
				conApp.Dispose();
				transactionApp.Dispose();
				
			}
			return iRowsAffected;

		}


		public static SessionDS GetCustomerSchedPickupsData(String strAppID, String strEnterpriseID, 
			int iCurrent, int iDSRecSize, DataSet dsCustomerSchedPickups)
		{
			SessionDS sessionDS = new SessionDS(); 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon==null)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","GetCustomerSchedPickupsData","GetCustomerSchedPickupsData","DbConnectionis null!!");
				return sessionDS;
			}
			DataRow drEach = dsCustomerSchedPickups.Tables[0].Rows[0];

			StringBuilder strBuild = new StringBuilder();
			strBuild.Append("SELECT * ");
			strBuild.Append("FROM Customer_Sched_Pickups ");
			strBuild.Append("WHERE applicationid = ");
			strBuild.Append("'" + strAppID + "' ");
			strBuild.Append("AND enterpriseid = ");
			strBuild.Append("'" + strEnterpriseID + "' ");
			
			//custid
			if((drEach["custid"]!= null) && (!drEach["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strCustid = (String)drEach["custid"];
				strBuild.Append("AND custid LIKE N'");
				strBuild.Append(Utility.ReplaceSingleQuote(strCustid));
				strBuild.Append("' ");
			}

			//cust_ref_name
			if((drEach["cust_ref_name"]!= null) && (!drEach["cust_ref_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strCustRefName = (String)drEach["cust_ref_name"].ToString();
				strBuild.Append("AND cust_ref_name LIKE N'");
				strBuild.Append(Utility.ReplaceSingleQuote(strCustRefName));
				strBuild.Append("' ");
			}

			strBuild.Append(" order by custid desc ");
			try
			{
				sessionDS = dbCon.ExecuteQuery(strBuild.ToString(),iCurrent,iDSRecSize,"CustomerSchedPickups");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL.cs","GetCustomerSchedPickupsData","GetCustomerSchedPickupsData","Error in  Execute Query "+appException.Message);
				throw new ApplicationException("CustomerScheduledPickupsDAL  (GetCustomerSchedPickupsData) ",appException);
			}
			decimal iQryRslt = sessionDS.QueryResultMaxSize; 
			return sessionDS;
		}
		//Add By Aoo 08/04/2010
		public static SessionDS GetCustomerSchedPickupsData(String strAppID, String strEnterpriseID, 
			int iCurrent, int iDSRecSize, DataSet dsCustomerSchedPickups,ref IDbCommand dbCmd, ref DbConnection dbCon, ref IDbConnection conApp, ref IDbTransaction tranApp)
		{
			SessionDS sessionDS = new SessionDS(); 


			/*dbCon.OpenConnection(ref conApp);

			if(dbCon==null)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","GetCustomerSchedPickupsData","GetCustomerSchedPickupsData","DbConnectionis null!!");
				return sessionDS;
			}*/
			DataRow drEach = dsCustomerSchedPickups.Tables[0].Rows[0];

			StringBuilder strBuild = new StringBuilder();
			strBuild.Append("SELECT * ");
			strBuild.Append("FROM Customer_Sched_Pickups ");
			strBuild.Append("WHERE applicationid = ");
			strBuild.Append("'" + strAppID + "' ");
			strBuild.Append("AND enterpriseid = ");
			strBuild.Append("'" + strEnterpriseID + "' ");
			
			//custid
			if((drEach["custid"]!= null) && (!drEach["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strCustid = (String)drEach["custid"];
				strBuild.Append("AND custid LIKE N'");
				strBuild.Append(Utility.ReplaceSingleQuote(strCustid));
				strBuild.Append("' ");
			}

			//cust_ref_name
			if((drEach["cust_ref_name"]!= null) && (!drEach["cust_ref_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strCustRefName = (String)drEach["cust_ref_name"];
				strBuild.Append("AND cust_ref_name LIKE N'");
				strBuild.Append(Utility.ReplaceSingleQuote(strCustRefName));
				strBuild.Append("' ");
			}

			strBuild.Append(" order by custid desc ");
			try
			{
				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dbCmd.CommandText = strBuild.ToString();
				//dbCmd.Connection = conApp;
				//dbCmd.Transaction = tranApp;
				sessionDS = dbCon.ExecuteQuery(dbCmd,iCurrent,iDSRecSize,"CustomerSchedPickups");

			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL.cs","GetCustomerSchedPickupsData","GetCustomerSchedPickupsData","Error in  Execute Query "+appException.Message);
				throw new ApplicationException("CustomerScheduledPickupsDAL  (GetCustomerSchedPickupsData) ",appException);
			}
			decimal iQryRslt = sessionDS.QueryResultMaxSize; 
			return sessionDS;
		}
		

		public static DataSet GetCustData(String strAppID, String strEnterpriseID,
			String custID)
		{
			StringBuilder strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsCustData = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","GetCustData","GetCustData","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = new StringBuilder();
			strQry.Append("SELECT * ");
			strQry.Append("FROM customer ");
			strQry.Append("WHERE applicationid = ");
			strQry.Append("'" + strAppID + "' ");
			strQry.Append("AND enterpriseid = ");
			strQry.Append("'" + strEnterpriseID + "' ");

			if (custID != null && custID != "")
			{
				strQry.Append("AND custid = ");
				strQry.Append("N'");
				strQry.Append(Utility.ReplaceSingleQuote(custID));
				strQry.Append("'");
			}
			

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsCustData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","GetCustData","GetCustData","Error ");
				throw appException;
			}
			return dsCustData;
		}

	
		public static DataSet GetSenderData(String strAppID, String strEnterpriseID,
			String strSndRcpName, String strCustID, String strSndRcpType)
		{
			StringBuilder strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsSenderData = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","GetSenderData","GetSenderData","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strQry = new StringBuilder();
			strQry.Append("SELECT * ");
			strQry.Append("FROM Customer_snd_rec ");
			strQry.Append("WHERE applicationid = ");
			strQry.Append("'" + strAppID + "' ");
			strQry.Append("AND enterpriseid = ");
			strQry.Append("'" + strEnterpriseID + "' ");

			if(strSndRcpName != null && strSndRcpName != "")
			{
				strQry.Append(" and snd_rec_name = N'");
				strQry.Append(Utility.ReplaceSingleQuote(strSndRcpName));
				strQry.Append("' ");
			}

			if(strCustID != null && strCustID != "")
			{
				strQry.Append(" and custid = '");
				strQry.Append(Utility.ReplaceSingleQuote(strCustID));
				strQry.Append("' ");
			}

			strQry.Append(" and (snd_rec_type = 'B' or snd_rec_type = '");
			strQry.Append(strSndRcpType+"')");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsSenderData = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("CustomerScheduledPickupsDAL","GetSenderData","GetSenderData","Error ");
				throw appException;
			}
			return dsSenderData;
		}


		public CustomerScheduledPickupsDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}
}
