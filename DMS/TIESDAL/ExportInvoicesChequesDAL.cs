using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using TIESClasses;

namespace TIESDAL
{
	public class ExportInvoicesChequesDAL
	{
		private string SQLText = string.Empty;
		private DbConnection dbCon;
		private IDbCommand dbCmd = null;

		public ExportInvoicesChequesDAL(string _appID, string _enterpriseID)
		{
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(_appID, _enterpriseID);
		}

		public DataSet ExecCustomsSearchInvoicesCheques(ExportInvoicesCheques objInfo)
		{
			DataSet dsReturn = new DataSet();
			try
			{
				if(objInfo.Enterpriseid.Trim().Equals(""))
					throw new ApplicationException("Please specify EnterpriseId");
				if(objInfo.Userloggedin.Trim().Equals(""))
					throw new ApplicationException("Please specify UserLoggedin");
				if(objInfo.DocumentType.Trim().Equals(""))
					throw new ApplicationException("Please specify DocumentType");

				SQLText = @"DECLARE @rowsCount INT
							EXEC dbo.Customs_SearchInvoicesCheques @enterpriseid = '{0}',
																   @DocumentType = '{1}',
																   @userloggedin = '{2}',
																   @ReturnedRows = @rowsCount OUTPUT";
				SQLText = string.Format(SQLText, 
					objInfo.Enterpriseid.Trim(),
					objInfo.DocumentType.Trim(),
					objInfo.Userloggedin.Trim());
				
				if(!objInfo.StartNo.Trim().Equals(""))
					SQLText += string.Format(", @Start_No = {0}", objInfo.StartNo.Trim());
				if(!objInfo.EndNo.Trim().Equals(""))
					SQLText += string.Format(", @End_No = {0}", objInfo.EndNo.Trim());
				if(!objInfo.ShowOnlyNotExported .Trim().Equals(""))
					SQLText += string.Format(", @ShowOnlyNotExported = '{0}'", objInfo.ShowOnlyNotExported .Trim());

				SQLText += "; SELECT @rowsCount;";

				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",objInfo.Enterpriseid.Trim()));				
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",objInfo.Userloggedin.Trim()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@DocumentType",objInfo.DocumentType.Trim()));	

				if(!objInfo.StartNo.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Start_No",objInfo.StartNo.Trim()));	

				if(!objInfo.EndNo.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@End_No",objInfo.EndNo.Trim()));	

				if(!objInfo.ShowOnlyNotExported .Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ShowOnlyNotExported",objInfo.ShowOnlyNotExported .Trim()));
				
				if(!objInfo.InvoicesList.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@InvoicesList",objInfo.InvoicesList.Trim()));				
				
				System.Data.SqlClient.SqlParameter outputRowParam = new System.Data.SqlClient.SqlParameter("@ReturnedRows", SqlDbType.Int);
				outputRowParam.Direction = ParameterDirection.Output;
				storedParams.Add(outputRowParam);

				DataTable dt = new DataTable();
				dt.Columns.Add("ReturnedRows");
			
				dsReturn = (DataSet)dbCon.ExecuteProcedure("dbo.Customs_SearchInvoicesCheques",storedParams,ReturnType.DataSetType);
				DataRow dr = dt.NewRow();
				dr["ReturnedRows"] = outputRowParam.Value;
				dt.Rows.Add(dr);

				dsReturn.Tables.Add(dt);

				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method ExecCustomsSearchInvoicesCheques : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "ExportInvoicesChequesDAL", "ExecCustomsSearchInvoicesCheques", msgException);
				throw new ApplicationException(msgException);
			}
		}

		public DataSet ExecCustomsExportInvoicesCheques(ExportInvoicesCheques objInfo)
		{
			DataSet dsReturn = new DataSet();
			try
			{
				if(objInfo.Enterpriseid.Trim().Equals(""))
					throw new ApplicationException("Please specify EnterpriseId");
				if(objInfo.Userloggedin.Trim().Equals(""))
					throw new ApplicationException("Please specify UserLoggedin");
				if(objInfo.DocumentType.Trim().Equals(""))
					throw new ApplicationException("Please specify DocumentType");

				SQLText = @"EXEC dbo.Customs_Export_InvoicesCheques @enterpriseid = '{0}',
																    @userloggedin = '{1}',
																    @DocumentType = '{2}'";
				SQLText = string.Format(SQLText, 
					objInfo.Enterpriseid.Trim(),
					objInfo.Userloggedin.Trim(),
					objInfo.DocumentType.Trim());
				
				if(!objInfo.ListOfDocuments.Trim().Equals(""))
					SQLText += string.Format(", @ListOfDocuments = '{0}'", objInfo.ListOfDocuments.Trim());

				dbCmd = dbCon.CreateCommand(SQLText, CommandType.Text);
				dsReturn = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method ExecCustomsSearchInvoicesCheques : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "ExportInvoicesChequesDAL", "ExecCustomsSearchInvoicesCheques", msgException);
				throw new ApplicationException(msgException);
			}
		}
		public DataSet GetEnterpriseConfigurationsPath(string _enterpriseId)
		{
			string result = string.Empty;

			DataSet dsReturn = new DataSet();

			SQLText = string.Format(@"SELECT [key], [value] 
								      FROM dbo.EnterpriseConfigurations('{0}', 'CustomsExportChequesInvoices')", _enterpriseId);
			try
			{
				dsReturn = (DataSet)dbCon.ExecuteQuery(SQLText, ReturnType.DataSetType);
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method GetCurrencies : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CustomsTariffsDAL", "GetEnterpriseConfigurations", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
			return dsReturn;
		}
	}
}
