using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;

namespace TIESDAL
{
	/// <summary>
	/// Summary description for ManualRatingOverrideDAL.
	/// </summary>
	public class ManualRatingOverrideDAL
	{
		public ManualRatingOverrideDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static SessionDS ExecManualRatingOverride(Utility util, 
													     SessionDS dsQueryParam, 
													     int iCurrent, 
														 int iRecSize, 
														 bool schemaOnly, 
														 string type)
		{
			string AppID = util.GetAppID(), EnterpriseID=util.GetEnterpriseID();

			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsReturn = null;

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(AppID, EnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID, EnterpriseID);
			
			string consignment_no = "",
				   ref_no = "",
				   payerid = "",
				   delivery_route = "",
				   destination_station = "",
				   MasterAWBNumber = "";

			if(!schemaOnly)
			{
				DataRow drS = dsQueryParam.ds.Tables[1].Rows[0];
				if(Utility.IsNotDBNull(drS["consignment_no"]))					
					consignment_no = "'" + drS["consignment_no"].ToString() + "'";
				else consignment_no = "NULL";

				if(Utility.IsNotDBNull(drS["ref_no"]))
					ref_no = "'" + drS["ref_no"] + "'";
				else ref_no = "NULL";
								
				if(Utility.IsNotDBNull(drS["payerid"]))
					payerid = "'" + drS["payerid"] + "'";
				else payerid = "NULL";
					
				if(Utility.IsNotDBNull(drS["delivery_route"]))
					delivery_route = "'" + drS["delivery_route"] + "'";
				else delivery_route = "NULL";

				if(Utility.IsNotDBNull(drS["destination_station"]))
					destination_station = "'" + drS["destination_station"] + "'";
				else destination_station = "NULL";
					
//				if(Utility.IsNotDBNull(drS["actual_pod_datefrom"]))
//				{
//					DateTime dtFrom=System.Convert.ToDateTime(drS["actual_pod_datefrom"]);
//					string sdtFrom=Utility.DateFormat(AppID,EnterpriseID, dtFrom ,DTFormat.DateTime);
//					strSQL += " and (ST.tracking_datetime >= "+sdtFrom+") ";
//				}
//
//				if(Utility.IsNotDBNull(drS["actual_pod_dateto"]))
//				{
//					DateTime dtTo=System.Convert.ToDateTime(drS["actual_pod_dateto"]);
//					string sdtTo=Utility.DateFormat(AppID,EnterpriseID, dtTo ,DTFormat.DateTime);
//					strSQL += " and (ST.tracking_datetime <= "+sdtTo+") ";
//				}
					
				if(Utility.IsNotDBNull(drS["MasterAWBNumber"]))
					MasterAWBNumber = "'" + drS["MasterAWBNumber"] + "'";
				else MasterAWBNumber = "NULL";
					
//				if(Utility.IsNotDBNull(drS["shpt_manifest_datetime_from"]))
//				{
//					DateTime dtFrom=System.Convert.ToDateTime(drS["shpt_manifest_datetime_from"]);
//					string sdtFrom=Utility.DateFormat(AppID,EnterpriseID, dtFrom ,DTFormat.DateTime);
//					strSQL += " and (SM.shpt_manifest_datetime >= "+sdtFrom+") ";
//				}
//
//				if(Utility.IsNotDBNull(drS["shpt_manifest_datetime_to"]))
//				{
//					DateTime dtTo=System.Convert.ToDateTime(drS["shpt_manifest_datetime_to"]);
//					string sdtTo=Utility.DateFormat(AppID,EnterpriseID, dtTo ,DTFormat.DateTime);
//					strSQL += " and (SM.shpt_manifest_datetime <= "+sdtTo+") ";
//				}
			}
			SessionDS sessDS = new SessionDS(); 
			try
			{
				String strSQL = "";
				
				if(schemaOnly)
				{
					strSQL = "EXEC [dbo].[ManualRatingOverride] '" + EnterpriseID + "', 'AWARE', 0, ''";

//					System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
//					storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseId",EnterpriseID));
//
//					dbCmd = dbCon.CreateCommand("QueryTrackandTrace",storedParams, CommandType.StoredProcedure);
//					ds = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				}
				else
					strSQL = @"EXEC [dbo].[ManualRatingOverride] '" + EnterpriseID + "', 'AWARE', 0, " + consignment_no + ", " + ref_no + ", '" + type + "', " + destination_station + ", " + MasterAWBNumber + ", " + delivery_route + ", " + payerid;
				
				dbcmd = dbCon.CreateCommand(strSQL, CommandType.Text);
				//dbcmd.CommandTimeout = 300;  //Add Command TimeOut for case of serveral records: 300 seconds (5 minutes)
				dsReturn = (DataSet)dbCon.ExecuteQuery(dbcmd, com.common.DAL.ReturnType.DataSetType);  
				
				sessDS.ds = new DataSet();
				sessDS.ds = dsReturn;

				//sessDS = dbCon.ExecuteQuery(dbCmd, iCurrent, iRecSize, "ManualRatingOverride");

//				if(schemaOnly)
//				{
//					DataRow row = sessDS.ds.Tables[0].NewRow();
//					sessDS.ds.Tables[0].Rows.Add(row);					
//				}		
			}
			catch(ApplicationException ex)
			{
				throw ex;
			}

			return sessDS;
		}

		public static SessionDS Get(Utility util, SessionDS dsQueryParam, int iCurrent, int iRecSize, bool schemaOnly, string type)
		{	
			string AppID = util.GetAppID(), EnterpriseID=util.GetEnterpriseID();

			IDbCommand dbCmd = null;
			DbConnection dbCon= DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
			if (dbCon==null)
			{
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			SessionDS sessDS = null; 
			try
			{
				String strSQL = 
					@"  SELECT  cast(NULL AS datetime) AS actual_pod_datefrom,
								cast(NULL AS datetime) AS actual_pod_dateto,
								ST.tracking_datetime,
								SM.applicationid,
								SM.enterpriseid,
								SM.payerid,
								SM.booking_no,
								SM.consignment_no,
								SM.ref_no,
								SM.route_code,
								SM.destination_station,
								SM.invoice_date,
								SM.manual_override,
								SM.manual_over_user,
								SM.manual_over_datetime,
								original_rated_freight=ISNULL(SM.original_rated_freight,SM.tot_freight_charge),
								SM.tot_freight_charge,
								original_rated_ins=ISNULL(SM.original_rated_ins, SM.insurance_surcharge),
								SM.insurance_surcharge,
								original_rated_other=ISNULL(SM.original_rated_other, SM.other_surch_amount),
								SM.other_surch_amount,
									original_rated_vas=ISNULL(SM.original_rated_vas, SM.tot_vas_surcharge),
								SM.tot_vas_surcharge,
								original_rated_esa=ISNULL(SM.original_rated_esa, SM.esa_surcharge),
								SM.esa_surcharge,
								original_rated_total=ISNULL(SM.original_rated_total, SM.total_rated_amount),
								SM.total_rated_amount,
								SM.MasterAWBNumber,
								cast(NULL AS datetime) AS shpt_manifest_datetime_from,
								cast(NULL AS datetime) AS shpt_manifest_datetime_to,
								SM.destination_state_code,
								SM.act_delivery_date,
								SM.basic_charge,
								original_rated_basic_charge=ISNULL(SM.original_rated_basic_charge, SM.basic_charge)							
						FROM	Shipment SM 
					--inner join
						left join
								(select max(tracking_datetime) as tracking_datetime, applicationid, enterpriseid, consignment_no, booking_no
								from	Shipment_Tracking 
								where  isnull(deleted, 'N') <> 'Y' and  status_code = 'POD'
								group by applicationid, enterpriseid, consignment_no, booking_no
								) AS ST ON  SM.applicationid = ST.applicationid AND SM.enterpriseid = ST.enterpriseid AND 
											SM.consignment_no = ST.consignment_no AND SM.booking_no = ST.booking_no 
						WHERE	(1="+(schemaOnly?0:1)+@") and (SM.applicationid = '"+AppID+@"') and (SM.enterpriseid = '"+EnterpriseID+@"') ";

				//Search
				if(!schemaOnly)
				{
					DataRow drS = dsQueryParam.ds.Tables[0].Rows[0];
					if(Utility.IsNotDBNull(drS["consignment_no"]))					
						strSQL += " and (SM.consignment_no like '%"+drS["consignment_no"]+"%') ";					

					if(Utility.IsNotDBNull(drS["ref_no"]))
						strSQL += " and (SM.ref_no like '%"+drS["ref_no"]+"%') ";
								
					if(Utility.IsNotDBNull(drS["payerid"]))
						strSQL += " and (SM.payerid like '%"+drS["payerid"]+"%') ";
					
					if(Utility.IsNotDBNull(drS["route_code"]))
						strSQL += " and (SM.route_code like '%"+drS["route_code"]+"%') ";					

					if(Utility.IsNotDBNull(drS["destination_station"]))
						strSQL += " and (SM.destination_station like '%"+drS["destination_station"]+"%') ";
					
					if(Utility.IsNotDBNull(drS["actual_pod_datefrom"]))
					{
						DateTime dtFrom=System.Convert.ToDateTime(drS["actual_pod_datefrom"]);
						string sdtFrom=Utility.DateFormat(AppID,EnterpriseID, dtFrom ,DTFormat.DateTime);
						strSQL += " and (ST.tracking_datetime >= "+sdtFrom+") ";
					}

					if(Utility.IsNotDBNull(drS["actual_pod_dateto"]))
					{
						DateTime dtTo=System.Convert.ToDateTime(drS["actual_pod_dateto"]);
						string sdtTo=Utility.DateFormat(AppID,EnterpriseID, dtTo ,DTFormat.DateTime);
						strSQL += " and (ST.tracking_datetime <= "+sdtTo+") ";
					}
					
					if(Utility.IsNotDBNull(drS["MasterAWBNumber"]))
						strSQL += " and (SM.MasterAWBNumber like '%"+drS["MasterAWBNumber"]+"%') ";
					
					if(Utility.IsNotDBNull(drS["shpt_manifest_datetime_from"]))
					{
						DateTime dtFrom=System.Convert.ToDateTime(drS["shpt_manifest_datetime_from"]);
						string sdtFrom=Utility.DateFormat(AppID,EnterpriseID, dtFrom ,DTFormat.DateTime);
						strSQL += " and (SM.shpt_manifest_datetime >= "+sdtFrom+") ";
					}

					if(Utility.IsNotDBNull(drS["shpt_manifest_datetime_to"]))
					{
						DateTime dtTo=System.Convert.ToDateTime(drS["shpt_manifest_datetime_to"]);
						string sdtTo=Utility.DateFormat(AppID,EnterpriseID, dtTo ,DTFormat.DateTime);
						strSQL += " and (SM.shpt_manifest_datetime <= "+sdtTo+") ";
					}
				}
				if(type=="D")
				{
					string strType = @" and SM.consignment_no in (SELECT S.consignment_no
																	FROM dbo.Shipment S
																	INNER JOIN dbo.Enterprise E
																		ON S.sender_country = E.country
																	WHERE E.applicationid = '{0}'
																	AND E.enterpriseid = '{1}'
																	AND S.invoice_no IS NULL
																	AND S.export_flag != 'Y'
																	and S.sender_name NOT IN (SELECT [code1] 
																		FROM Customs_Configurations 
																		WHERE codeid='AW_BondedWarehouses' 
																				AND code3 = 'Y'
																				AND applicationid='{2}'
																				AND enterpriseid='{3}'))";
					strSQL += string.Format(strType, AppID, EnterpriseID, AppID, EnterpriseID);
				}
				else if(type=="E")
					strSQL += @" and SM.consignment_no in (SELECT S.consignment_no
																	FROM dbo.Shipment S
																	WHERE S.export_flag = 'Y'
																	AND S.invoice_no IS NULL)";
				else if(type=="I")
				{
					string strType = @" and SM.consignment_no in (SELECT S.consignment_no
																	FROM dbo.Shipment S
																	INNER JOIN dbo.Enterprise E
																		ON S.sender_country = E.country
																	WHERE E.applicationid = '{0}'
																	AND E.enterpriseid = '{1}'
																	AND S.invoice_no IS NULL
																	and S.sender_name IN (SELECT [code1] 
																	FROM Customs_Configurations 
																	WHERE codeid='AW_BondedWarehouses' 
																			AND code3 = 'Y'
																			AND applicationid='{2}'
																			AND enterpriseid='{3}'))";
					strSQL += string.Format(strType, AppID, EnterpriseID, AppID, EnterpriseID);
				}

				strSQL += " order by SM.consignment_no ASC ";

				//sessDS = dbCon.ExecuteQuery(strSQL, iCurrent, iRecSize, "ManualRatingOverride");
				dbCmd = dbCon.CreateCommand(strSQL, CommandType.Text);
				dbCmd.CommandTimeout = 300;  //Add Command TimeOut for case of serveral records: 300 seconds (5 minutes)
				sessDS = dbCon.ExecuteQuery(dbCmd, iCurrent, iRecSize, "ManualRatingOverride");

				if(schemaOnly)
				{
					DataRow row = sessDS.ds.Tables[0].NewRow();
					sessDS.ds.Tables[0].Rows.Add(row);					
				}		
				
			}
			catch(ApplicationException ex)
			{
				throw ex;
			}

			return sessDS;
		}

		
		public static int UpdateManualRatingOverride(Utility util, ref DataSet dsUpdate, string custid)
		{	
			string AppID = util.GetAppID(), EnterpriseID=util.GetEnterpriseID(), action = "1";
			int iRowsEffected = 0;

			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsReturn = null;

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(AppID, EnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID, EnterpriseID);

			try
			{
				DataTable gcTb = dsUpdate.Tables[1].GetChanges(); if(gcTb==null) return iRowsEffected;
				gcTb = gcTb.GetChanges(DataRowState.Modified); if(gcTb==null) return iRowsEffected;				
				DataRow r = gcTb.Rows[0];

				if(NullDB(r["original_rated_freight"]) == "NULL" &&
					NullDB(r["original_rated_ins"]) == "NULL" &&
					NullDB(r["original_rated_other"]) == "NULL" &&
					NullDB(r["original_rated_esa"]) == "NULL" &&
					NullDB(r["original_rated_total"]) == "NULL" &&
					NullDB(r["original_rated_basic_charge"]) == "NULL" &&
					NullDB(r["original_rated_vas"]) == "NULL")
					action = "2";


				string SQLText = @"DECLARE @_booking_no INT;
								SELECT @_booking_no=booking_no FROM dbo.shipment WHERE enterpriseid='" + EnterpriseID + "' AND applicationid='" + AppID + "' AND consignment_no='"+r["consignment_no"]+"' ";
				SQLText += @" EXEC [dbo].[ManualRatingOverride]  @enterpriseid='"+EnterpriseID+@"'
																,@userloggedin='AWARE'
																,@action=" + action + ",@consignment_no='"+r["consignment_no"]+"',@booking_no=@_booking_no";
				if(action == "1")
					SQLText += " ,@override_basic_charge="+NullDB(r["original_rated_basic_charge"]) + ",@override_tot_freight_charge=" + NullDB(r["original_rated_freight"]) + ",@override_insurance_surcharge="+NullDB(r["original_rated_ins"]) + ", @override_other_surch_amount="+NullDB(r["original_rated_other"]) + ",@override_tot_vas_surcharge="+NullDB(r["original_rated_vas"]) + ",@override_esa_surcharge="+NullDB(r["original_rated_esa"]);
			

				dbcmd = dbCon.CreateCommand(SQLText, CommandType.Text);
				dbcmd.CommandTimeout = 300;  //Add Command TimeOut for case of serveral records: 300 seconds (5 minutes)
				dsReturn = (DataSet)dbCon.ExecuteQuery(dbcmd, com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException ex)
			{
				//Logger.LogTraceError("SysDataMgrDAL","UpdateRouteCode","R002","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error Updating Surcharge ",ex);
			}
			return 1;
		}
		
		public static int Update(Utility util, ref DataSet dsUpdate, string custid)
		{			
			if(util == null) throw new ApplicationException("Utility isn't define", null);			
			string AppID = util.GetAppID(), EnterpriseID=util.GetEnterpriseID();

			int iRowsEffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
   
			if(dbCon==null)
			{
				throw new Exception("DbConnection object is null!!");
			}
			
			try
			{
				DataTable gcTb = dsUpdate.Tables[0].GetChanges(); if(gcTb==null) return iRowsEffected;
				gcTb = gcTb.GetChanges(DataRowState.Modified); if(gcTb==null) return iRowsEffected;				
				DataRow r = gcTb.Rows[0];

				string manual_over_user = "";
				string manual_over_datetime = "";
				
				if(NullDB(r["original_rated_freight"]) == "NULL" &&
					NullDB(r["original_rated_ins"]) == "NULL" &&
					NullDB(r["original_rated_other"]) == "NULL" &&
					NullDB(r["original_rated_esa"]) == "NULL" &&
					NullDB(r["original_rated_total"]) == "NULL" &&
					NullDB(r["original_rated_basic_charge"]) == "NULL" &&
					NullDB(r["original_rated_vas"]) == "NULL")
				{
					manual_over_user = "'SYSTEM'";
					manual_over_datetime = "NULL";
				}
				else
				{
					manual_over_user = "'" + util.GetUserID() + "'";
					manual_over_datetime = "getdate()";
				}
				String strSQL = 
					@"UPDATE dbo.Shipment 
						SET    original_rated_basic_charge = ISNULL(original_rated_basic_charge, basic_charge)
						,original_rated_freight = ISNULL(original_rated_freight, tot_freight_charge)
						,original_rated_ins = ISNULL(original_rated_ins, insurance_surcharge)
						,original_rated_other = ISNULL(original_rated_other, other_surch_amount)
						,original_rated_vas = ISNULL(original_rated_vas, tot_vas_surcharge)
						,original_rated_esa = ISNULL(original_rated_esa, esa_surcharge)
						,original_rated_total = ISNULL(original_rated_total, total_rated_amount)
						,manual_over_user="+manual_over_user+@"
						,manual_over_datetime="+manual_over_datetime+@"
						,manual_override='Y'
						,basic_charge="+NullDB(r["original_rated_basic_charge"])+@"
						,tot_freight_charge="+NullDB(r["original_rated_freight"])+@"
						,insurance_surcharge="+NullDB(r["original_rated_ins"])+@"
						,other_surch_amount="+NullDB(r["original_rated_other"])+@"
						,tot_vas_surcharge="+NullDB(r["original_rated_vas"])+@"
						,esa_surcharge="+NullDB(r["original_rated_esa"])+@"
						,total_rated_amount="+NullDB(r["original_rated_total"])+@"
						,tax_on_rated_amount = "+NullDB(r["original_rated_total"])+@" * (SELECT CASE c.Customs_TaxExempt WHEN 0 THEN e.tax_percent/100.0 ELSE 0 END
                                                              FROM dbo.Customer c
                                                              INNER JOIN dbo.Enterprise e 
                                                                     ON c.applicationid = e.applicationid AND c.enterpriseid = e.enterpriseid
                                                              WHERE c.custid = '"+custid+@"'
                                                              AND c.applicationid = '"+r["applicationid"]+@"' AND c.enterpriseid = '"+r["enterpriseid"]+@"') 

						WHERE  applicationid='"+r["applicationid"]+@"' and 
							 enterpriseid='"+r["enterpriseid"]+@"' and 
							 booking_no='"+r["booking_no"]+@"' and 
							 consignment_no='"+r["consignment_no"]+"' ";

//				String strSQL = 
//					@"UPDATE Shipment 
//					  SET	 original_rated_freight="+ NullDB(r["original_rated_freight"])+@"
//							 ,tot_freight_charge="+ NullDB(r["tot_freight_charge"])+@"
//							 ,original_rated_ins="+ NullDB(r["original_rated_ins"])+@"
//							 ,insurance_surcharge="+ NullDB(r["insurance_surcharge"]) +@"
//							 ,original_rated_other="+ NullDB(r["original_rated_other"])+@"
//							 ,other_surch_amount="+ NullDB(r["other_surch_amount"]) +@"
//							 ,original_rated_esa="+ NullDB(r["original_rated_esa"])+@"
//							 ,esa_surcharge="+ NullDB(r["esa_surcharge"]) +@"
//							 ,original_rated_total="+ NullDB(r["original_rated_total"])+@"
//							 ,total_rated_amount="+ NullDB(r["total_rated_amount"]) +@"
//							 ,manual_over_user="+manual_over_user+@"
//							 ,manual_over_datetime="+manual_over_datetime+@"
//							 ,manual_override='"+r["manual_override"]+@"'
//							 ,basic_charge=" + NullDB(r["original_rated_basic_charge"]) + @"
//							 ,original_rated_vas="+ NullDB(r["original_rated_vas"]) +@" 
//							 ,tax_on_rated_amount = "+NullDB(r["total_rated_amount"])+@" * (SELECT CASE c.Customs_TaxExempt
//																			WHEN 0 THEN e.tax_percent/100.0
//																			ELSE 0
//																		END
//																FROM dbo.Customer c
//																INNER JOIN dbo.Enterprise e ON c.applicationid = e.applicationid
//																AND c.enterpriseid = e.enterpriseid
//																WHERE c.custid = a.payerid
//																	AND c.applicationid = '"+r["applicationid"]+@"'
//																	AND c.enterpriseid = '"+r["enterpriseid"]+@"')
//							 
//					 WHERE  applicationid='"+r["applicationid"]+@"' and 
//							 enterpriseid='"+r["enterpriseid"]+@"' and 
//							 booking_no='"+r["booking_no"]+@"' and 
//							 consignment_no='"+r["consignment_no"]+"' ";

				IDbCommand dbcmd = dbCon.CreateCommand(strSQL,CommandType.Text);
				iRowsEffected = dbCon.ExecuteNonQuery(dbcmd);
			}
			catch(ApplicationException ex)
			{
				//Logger.LogTraceError("SysDataMgrDAL","UpdateRouteCode","R002","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error Updating Surcharge ",ex);
			}

			return iRowsEffected;
		}

		private static string NullDB(object obj)
		{
			if(obj==DBNull.Value) return "NULL";
			else return obj.ToString();
		}
	}
}
