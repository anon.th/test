using System;
using System.Text;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Data.SqlClient;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for SysDataManager2.
	/// </summary>
	public class SysDataManager2
	{
		public SysDataManager2()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		//Added By Tom 10/7/09
		//Master Account Module
		public static SessionDS GetEmptyMasterAccountDS()
		{
			DataTable dtMasterAccount = new DataTable();
			dtMasterAccount.Columns.Add(new DataColumn("master_account", typeof(string)));

			for(int i=0; i < 1; i++)
			{
				DataRow drEach = dtMasterAccount.NewRow();
				drEach["master_account"] = "";
				dtMasterAccount.Rows.Add(drEach);
			}

			DataSet dsMasterAccount = new DataSet();
			dsMasterAccount.Tables.Add(dtMasterAccount);
			
			dsMasterAccount.Tables[0].Columns["master_account"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsMasterAccount;
			sessionDS.DataSetRecSize = dsMasterAccount.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return  sessionDS;
		}

		public static SessionDS GetMasterAccountDS(String strAppID, String strEnterpriseID, int iCurRow, int iDSRowSize, DataSet dsQuery)
		{
			SessionDS sessionDS = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			string strCode = (string)dr[0];

			string strSQLWhere = "";

			if (strCode!=null && strCode!="")
			{
				strSQLWhere += "and code_str_value like '%"+strCode+"%' ";
			}

			//DataSet dsZoneCode = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetZoneCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			String strSQLQuery = "select code_str_value as master_account from core_system_code where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and codeid = 'master_account' "; 
			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strSQLQuery += strSQLWhere;
			}
			strSQLQuery += "order by master_account "; 
			
			//dsZoneCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			//return  dsZoneCode;
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurRow,iDSRowSize,"ZoneTable");
			return  sessionDS;
	
		}

		public static void AddNewRowInMasterAccountDS(ref SessionDS dsMasterAccount)
		{
			DataRow drNew = dsMasterAccount.ds.Tables[0].NewRow();

			drNew[0] = "";
			try
			{
				dsMasterAccount.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataManager2","AddNewRowInZoneCodeDS","R005",ex.Message.ToString());
			}
			
		}

		public static int UpdateMasterAccount(DataSet dsMasterAccountChanged, String strAppID, String strEnterpriseID)
		{
			int iRowsUpdated = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsMasterAccountChanged.Tables[0].Rows[0];
			string strCode = (string)dr[0];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","UpdateZone","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strCode!=null && strCode!="")
				{
					string strSQLQuery = "update core_system_code set code_str_value = N'"+Utility.ReplaceSingleQuote(strCode)+"' where ";
					strSQLQuery += "applicationid = "+"'"+strAppID+"' and codeid = 'master_account' and code_str_value =N'"+Utility.ReplaceSingleQuote(strCode)+"'"; 
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","InsertZone","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Update operation failed",appException);
				
			}
			return iRowsUpdated;
		}

		public static int InsertMasterAccount(DataSet dsMasterAccountTable, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsMasterAccountTable.Tables[0].Rows[rowIndex];
			string strCode = (string)dr[0];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","InsertZone","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				//Modified By Tom Jan 19, 10
				int lastseq = -1;
				String strQuery = "select top 1 sequence from core_system_code where codeid = 'master_account' order by sequence desc";
				dbCom = dbCon.CreateCommand(strQuery,CommandType.Text);
				DataSet roleDataSet =(DataSet)dbCon.ExecuteQuery(dbCom,com.common.DAL.ReturnType.DataSetType);
				for(int i = 0; i < roleDataSet.Tables[0].Rows.Count;i++)
				{
					DataRow drEach = roleDataSet.Tables[0].Rows[i];

					lastseq = Convert.ToInt32(drEach["sequence"]) + 1;
				}

				if(strCode!=null && strCode!="")
				{
						string strSQLQuery = "insert into core_system_code (applicationid, codeid, culture, sequence, code_text, code_str_value) ";
						strSQLQuery += "values ('"+strAppID+"', 'master_account', 'en_US', " + lastseq + ",N'"+Utility.ReplaceSingleQuote(strCode)+"',N'"+Utility.ReplaceSingleQuote(strCode)+"')";

						dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

						iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","InsertZone","RBAC003",appException.Message.ToString());
				//throw new ApplicationException("Insert operation failed",appException);
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			return iRowsInserted;
		}

		public static int DeleteMasterAccount(DataSet dsMasterAccountTable, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsDeleted = 0;
			IDbCommand dbCom = null;
			IDbCommand dbCom2 = null;
			DataRow dr = dsMasterAccountTable.Tables[0].Rows[rowIndex];
			string strCode = (string)dr[0];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","DeleteZone","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				if(strCode!=null && strCode!="")
				{
					string strSQLQuery = "delete from core_system_code where ";
					strSQLQuery += "applicationid = "+"'"+strAppID+"' and codeid = 'master_account' and code_str_value = '"+strCode+"' "; 
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);

					string strSQLQuery2 = "update customer set master_account = null where ";
					strSQLQuery2 += "applicationid = "+"'"+strAppID+"' and master_account = '"+strCode+"' "; 
	
					dbCom2 = dbCon.CreateCommand(strSQLQuery2,CommandType.Text);
					iRowsDeleted = dbCon.ExecuteNonQuery(dbCom2);
					

					// Delete customercredit history
					strSQLQuery  = " delete from Customer_Credit where ";
					strSQLQuery += "applicationid = "+"'"+strAppID+"' and custid = '" + strCode  + "'"; 
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);


				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","DeleteZone","RBAC003",appException.Message.ToString());
				//throw new ApplicationException("Delete operation failed",appException);
				throw new ApplicationException("Delete operation failed"+appException.Message,appException);
			}
			return iRowsDeleted;
		}

		//End Added By Tom 10/7/09

		//Zone Module
		public static SessionDS GetEmptyZoneCodeDS()
		{
			DataTable dtZoneCode = new DataTable();
			dtZoneCode.Columns.Add(new DataColumn("zone_code", typeof(string)));
			dtZoneCode.Columns.Add(new DataColumn("zone_name", typeof(string)));

			for(int i=0; i < 1; i++)
			{
				DataRow drEach = dtZoneCode.NewRow();
				drEach["zone_code"] = "";
				drEach["zone_name"] = "";
				dtZoneCode.Rows.Add(drEach);
			}

			DataSet dsZoneCode = new DataSet();
			dsZoneCode.Tables.Add(dtZoneCode);
			
//			dsZoneCode.Tables[0].Columns["zone_code"].Unique = true; //Checks duplicate records..
			dsZoneCode.Tables[0].Columns["zone_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsZoneCode;
			sessionDS.DataSetRecSize = dsZoneCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return  sessionDS;
		}

		public static SessionDS GetEmptyPostCodeDS()
		{
			DataTable dtPostCode = new DataTable();
			dtPostCode.Columns.Add(new DataColumn("origin_dc", typeof(string)));
			dtPostCode.Columns.Add(new DataColumn("recipientZipCode", typeof(string)));
			dtPostCode.Columns.Add(new DataColumn("service_code", typeof(string)));

			for(int i=0; i < 1; i++)
			{
				DataRow drEach = dtPostCode.NewRow();
				drEach["origin_dc"] = "";
				drEach["recipientZipCode"] = "";
				drEach["service_code"] = "";
				dtPostCode.Rows.Add(drEach);
			}

			DataSet dsPostCode = new DataSet();
			dsPostCode.Tables.Add(dtPostCode);
			
			//			dsZoneCode.Tables[0].Columns["zone_code"].Unique = true; //Checks duplicate records..
			dsPostCode.Tables[0].Columns["origin_dc"].AllowDBNull = false;
			dsPostCode.Tables[0].Columns["recipientZipCode"].AllowDBNull = false;
			dsPostCode.Tables[0].Columns["service_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsPostCode;
			sessionDS.DataSetRecSize = dsPostCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return  sessionDS;
		}

		public static SessionDS GetZoneCodeDS(String strAppID, String strEnterpriseID, int iCurRow, int iDSRowSize, DataSet dsQuery)
		{
			SessionDS sessionDS = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			string strCode = (string)dr[0];
			string strName = (string)dr[1];

			string strSQLWhere = "";

			if (strCode!=null && strCode!="")
			{
				strSQLWhere += "and zone_code like '%"+strCode.TrimStart().TrimEnd()+"%' ";
			}
			if (strName!=null && strName!="")
			{
				strSQLWhere += "and zone_name like N'%"+Utility.ReplaceSingleQuote(strName)+"%' ";
			}

			//DataSet dsZoneCode = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetZoneCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			String strSQLQuery = "select zone_code, zone_name from zone where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"; 
			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strSQLQuery += strSQLWhere;
			}
			
			//dsZoneCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			//return  dsZoneCode;
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurRow,iDSRowSize,"ZoneTable");
			return  sessionDS;
	
		}
		public static bool checkcredittermdup(String strAppID, String strEnterpriseID, String masterAcc)
		{
		
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
		
	
			

			String strSQLQuery = "select customercreditid from Customer_Credit ";
			strSQLQuery += " where applicationid = '" + strAppID + "' " ;
			strSQLQuery += " and enterpriseid = '" + strEnterpriseID  + "' ";
			strSQLQuery += " and custid = '" + masterAcc + "'";
			
		
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			DataSet  ds = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			DataTable dt = ds.Tables[0];

			if(dt.Rows.Count > 0 )
			{
				
					return true;
			}
			return false;
			

			
	
		}
		public static bool checkbeforeDelmacc(String strAppID, String strEnterpriseID, String masterAcc)
		{
		
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			String strQry = "";


			
			

			String strSQLQuery = " select sum(credit_limit) as sumcredit from customer ";
			strSQLQuery += " where master_account = '" + masterAcc  +  "' ";
			strSQLQuery += " and applicationid = '" + strAppID  +  "' ";
			strSQLQuery += " and enterpriseid = '" + strEnterpriseID  +  "' ";
			
		
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			DataSet  ds = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			DataTable dt = ds.Tables[0];

			if(dt.Rows.Count > 0 )
			{
				if(dt.Rows[0]["sumcredit"] != null &&  dt.Rows[0]["sumcredit"].ToString().Length > 0)
				{
					return true;
				}
			}
			return false;
			

			
	
		}

		public static SessionDS GetBestServiceDS(String strAppID, String strEnterpriseID, int iCurRow, int iDSRowSize, DataSet dsQuery)
		{
			SessionDS sessionDS = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			string strOriginCode = (string)dr[0];
			string strRecipPostCode = (string)dr[1];
			string strBestService = (string)dr[2];

			string strSQLWhere = "";

			if (strOriginCode!=null && strOriginCode!="")
			{
				strSQLWhere += "and origin_dc like '%"+strOriginCode+"%' ";
			}
			if (strRecipPostCode!=null && strRecipPostCode!="")
			{
				strSQLWhere += "and recipientZipCode like '%"+strRecipPostCode+"%' ";
			}
			if (strBestService!=null && strBestService!="")
			{
				strSQLWhere += "and service_code like '%"+strBestService+"%' ";
			}

			//DataSet dsZoneCode = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetBestServiceDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			String strSQLQuery = "select origin_dc, recipientZipCode, service_code from Best_Service_Available where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"; 
			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strSQLQuery += strSQLWhere;
			}
			
			//dsZoneCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			//return  dsZoneCode;
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurRow,iDSRowSize,"ZoneTable");
			return  sessionDS;
	
		}

		public static void AddNewRowInZoneCodeDS(ref SessionDS dsZoneCode)
		{
			DataRow drNew = dsZoneCode.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			try
			{
				dsZoneCode.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataManager2","AddNewRowInZoneCodeDS","R005",ex.Message.ToString());
			}
			
		}

		public static void AddNewRowInPostCodeDS(ref SessionDS dsPostCode)
		{
			DataRow drNew = dsPostCode.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			try
			{
				dsPostCode.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataManager2","AddNewRowInPostCodeDS","R005",ex.Message.ToString());
			}
			
		}

		public static int UpdateZone(DataSet dsZoneChanged, String strAppID, String strEnterpriseID)
		{
			int iRowsUpdated = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsZoneChanged.Tables[0].Rows[0];
			string strCode = (string)dr[0];
			string strName = (string)dr[1];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","UpdateZone","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strCode!=null && strCode!="")
				{
					string strSQLQuery = "update Zone set zone_name = N'"+Utility.ReplaceSingleQuote(strName)+"' where ";
					strSQLQuery += "applicationid = "+"'"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and zone_code = '"+strCode.TrimStart().TrimEnd()+"' "; 
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","InsertZone","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Update operation failed",appException);
				
			}
			return iRowsUpdated;
		}

		public static int UpdateBestService(DataSet dsBestServiceChanged, String strAppID, String strEnterpriseID)
		{
			int iRowsUpdated = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsBestServiceChanged.Tables[0].Rows[0];
			string strOrigin = (string)dr[0];
			string strRecipPostCode = (string)dr[1];
			string strBestService = (string)dr[2];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","UpdateBestService","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strOrigin!=null && strOrigin!="")
				{
					string strSQLQuery = "update Best_Service_Available set service_code = '"+strBestService+"' where ";
					strSQLQuery += "applicationid = "+"'"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and origin_dc = '"+strOrigin+"' ";
					strSQLQuery += "and recipientZipCode = '"+strRecipPostCode+"'";
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","UpdateBestService","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Update operation failed",appException);
				
			}
			return iRowsUpdated;
		}

		public static int InsertZone(DataSet dsZoneTable, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsZoneTable.Tables[0].Rows[rowIndex];
			string strCode = (string)dr[0];
			string strName = (string)dr[1];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","InsertZone","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strCode!=null && strCode!="")
				{
					string strSQLQuery = "insert into Zone (applicationid, enterpriseid, zone_code, zone_name) ";
					strSQLQuery += "values ('"+strAppID+"', '"+strEnterpriseID+"', '"+strCode.TrimStart().TrimEnd()+"',N'"+Utility.ReplaceSingleQuote(strName)+"')";

					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

					iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","InsertZone","RBAC003",appException.Message.ToString());
				//throw new ApplicationException("Insert operation failed",appException);
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			return iRowsInserted;
		}

		public static int InsertBestService(DataSet dsBestServiceTable, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsInserted = 0;
			int chkService = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsBestServiceTable.Tables[0].Rows[rowIndex];
			string strOriginCode = (string)dr[0];
			string strRecipPostCode = (string)dr[1];
			string strBestService = (string)dr[2];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","InsertBestService","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			string strSQLChkService = "select count(*) from zipcode_service_excluded where zipcode = '"+strRecipPostCode+"' ";
				   strSQLChkService += "and service_code = '"+strBestService+"'";
			//dbCom = dbCon.CreateCommand(strSQLChkService,CommandType.Text);
			chkService = (int)dbCon.ExecuteScalar(strSQLChkService);

			if(chkService == 0)
			{
				try
				{
					if(strOriginCode!=null && strOriginCode!="")
					{
						string strSQLQuery = "insert into Best_Service_Available (applicationid, enterpriseid, origin_dc, recipientZipCode, service_code) ";
						strSQLQuery += "values ('"+strAppID+"', '"+strEnterpriseID+"', '"+strOriginCode+"','"+strRecipPostCode+"','"+strBestService+"')";

						dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

						iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
					}
				}
				catch(ApplicationException appException)
				{
					Logger.LogTraceError("SysDataManager2","InsertZone","RBAC003",appException.Message.ToString());
					//throw new ApplicationException("Insert operation failed",appException);
					throw new ApplicationException("Insert operation failed"+appException.Message,appException);
				}
			}
			else
			{
				iRowsInserted = -1;
			}
			return iRowsInserted;
		}

		public static int VerifyZipcode(DataSet dsBestServiceTable, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsZipcode = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsBestServiceTable.Tables[0].Rows[rowIndex];
			string strOriginCode = (string)dr[0];
			string strRecipPostCode = (string)dr[1];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","VerifyBestService","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strRecipPostCode!=null && strRecipPostCode!="")
				{
					string strSQLQuery = "select count(*) from Zipcode ";
					strSQLQuery += "where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and zipcode = '"+strRecipPostCode+"'";

//					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
//
//					iRowsZipcode = dbCon.ExecuteNonQuery(dbCom);
					iRowsZipcode=Convert.ToInt32(dbCon.ExecuteScalar(strSQLQuery.ToString()));
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","VerifyZipcode","RBAC003",appException.Message.ToString());
				//throw new ApplicationException("Insert operation failed",appException);
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			
			return iRowsZipcode;
		}

		public static int VerifyOriginCode(DataSet dsBestServiceTable, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsOriginCode = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsBestServiceTable.Tables[0].Rows[rowIndex];
			string strOriginCode = (string)dr[0];
			string strRecipPostCode = (string)dr[1];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","VerifyBestService","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strOriginCode!=null && strOriginCode!="")
				{
					string strSQLQuery = "select count(*) from Distribution_center ";
					strSQLQuery += "where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and origin_code = '"+strOriginCode+"'";

//					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
//
//					iRowsOriginCode = dbCon.ExecuteNonQuery(dbCom);
					iRowsOriginCode = Convert.ToInt32(dbCon.ExecuteScalar(strSQLQuery.ToString()));
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","VerifyOriginCode","RBAC003",appException.Message.ToString());
				//throw new ApplicationException("Insert operation failed",appException);
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			
			return iRowsOriginCode;
		}

		public static int DeleteZone(DataSet dsZoneTable, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsDeleted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsZoneTable.Tables[0].Rows[rowIndex];
			string strCode = (string)dr[0];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","DeleteZone","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				if(strCode!=null && strCode!="")
				{
					string strSQLQuery = "delete from Zone where ";
					strSQLQuery += "applicationid = "+"'"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and zone_code = '"+strCode+"' "; 
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","DeleteZone","RBAC003",appException.Message.ToString());
				//throw new ApplicationException("Delete operation failed",appException);
				throw new ApplicationException("Delete operation failed"+appException.Message,appException);
			}
			return iRowsDeleted;
		}

		public static int DeleteBestService(DataSet dsBestServiceTable, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsDeleted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsBestServiceTable.Tables[0].Rows[rowIndex];
			string strOriginCode = (string)dr[0];
			string strPostalCode = (string)dr[1];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","DeleteBestService","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				if(strOriginCode!=null && strOriginCode!="")
				{
					string strSQLQuery = "delete from Best_Service_Available where ";
					strSQLQuery += "applicationid = "+"'"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and origin_dc = '"+strOriginCode+"' ";
					strSQLQuery += "and recipientZipCode = '"+strPostalCode+"' ";
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","DeleteBestService","RBAC003",appException.Message.ToString());
				//throw new ApplicationException("Delete operation failed",appException);
				throw new ApplicationException("Delete operation failed"+appException.Message,appException);
			}
			return iRowsDeleted;
		}


		//State Module
		public static SessionDS GetEmptyState()
		{
			DataTable dtState = new DataTable();
			dtState.Columns.Add(new DataColumn("country", typeof(string)));
			dtState.Columns.Add(new DataColumn("state_code", typeof(string)));
			dtState.Columns.Add(new DataColumn("state_name", typeof(string)));
			//HC Return Task
			dtState.Columns.Add(new DataColumn("hc_return_within_days", typeof(string)));
			//HC Return Task

			for(int i=0; i < 1; i++)
			{
				DataRow drState = dtState.NewRow();
				drState["country"] = "";
				drState["state_code"] = "";
				drState["state_name"] = "";
				//HC Return Task
				drState["hc_return_within_days"] = "";
				//HC Return Task
				dtState.Rows.Add(drState);
			}

			DataSet dsState = new DataSet();
			dsState.Tables.Add(dtState);

//			dsState.Tables[0].Columns["state_code"].Unique = true; //Checks duplicate records..
			dsState.Tables[0].Columns["state_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsState;
			sessionDS.DataSetRecSize = dsState.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return  sessionDS;
		}

		public static SessionDS GetStateCodeDS(String strAppID, String strEnterpriseID, int iCurRow, int iDSRowSize, DataSet dsQuery)
		{
			SessionDS sessionDS = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			string strCountry = (string)dr[0];
			string strCode = (string)dr[1];
			string strName = (string)dr[2];
			//HC Return Task
			string strReturnDays = (string)dr[3];
			//HC Return Task

			string strSQLWhere = "";

			if (strCountry!=null && strCountry!="")
			{
				strSQLWhere += "and country like '%"+strCountry+"%' ";
			}
			if (strCode!=null && strCode!="")
			{
				strSQLWhere += "and state_code like '%"+strCode+"%' ";
			}
			if (strName!=null && strName!="")
			{
				strSQLWhere += "and state_name like N'%"+Utility.ReplaceSingleQuote(strName)+"%' ";
			}
			//HC Return Task
			if (strReturnDays!=null && strReturnDays!="")
			{
				strSQLWhere += "and hc_return_within_days like N'%"+Utility.ReplaceSingleQuote(strReturnDays)+"%' ";
			}
			//HC Return Task

			//DataSet dsStateCode = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetStateCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			
			String strSQLQuery = "select country, state_code, state_name, ";
			strSQLQuery += "hc_return_within_days ";
			strSQLQuery += "from state where ";
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"; 
			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strSQLQuery += strSQLWhere;
			}
			
			//dsStateCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			//return  dsStateCode;
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurRow,iDSRowSize,"StateTable");
			return  sessionDS;
		}

		public static void AddNewRowInStateCodeDS(ref SessionDS dsStateCode)
		{
			DataRow drNew = dsStateCode.ds.Tables[0].NewRow();
			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			//HC Return Task
			drNew[3] = "";
			//HC Return Task

			try
			{
				dsStateCode.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataManager2","AddNewRowInStateCodeDS","R005",ex.Message.ToString());
			}
			
		}

		public static int UpdateState(DataSet dsStateChanged, String strAppID, String strEnterpriseID)
		{
			int iRowsUpdated = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsStateChanged.Tables[0].Rows[0];
			string strCountry = (string)dr[0];
			string strCode = (string)dr[1];
			string strName = (string)dr[2];
			//HC Return Task
			string strReturnDays = Convert.ToString(dr[3].ToString());
			//HC Return Task

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","UpdateState","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strCode!=null && strCode!="")
				{
					string strSQLQuery = "update State set state_name = N'"+Utility.ReplaceSingleQuote(strName)+"', ";

					//HC Return Task
					strSQLQuery += "hc_return_within_days = " + strReturnDays.Trim() + " ";
					//HC Return Task
	
					strSQLQuery += "where applicationid = "+"'"+strAppID+"' and enterpriseid = '"+strEnterpriseID;
					strSQLQuery += "' and country = '"+strCountry+"' and state_code = '"+strCode+"' "; 

					
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","UpdateState","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Update operation failed",appException);
				
			}
			return iRowsUpdated;
		}

		public static int InsertState(DataSet dsStateTable, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsStateTable.Tables[0].Rows[rowIndex];
			string strCountry = (string)dr[0];
			string strCode = (string)dr[1];
			string strName = (string)dr[2];
			//HC Return Task
			string strReturnDays = (string)dr[3];
			//HC Return Task

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","InsertState","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strCode!=null && strCode!="")
				{
					string strSQLQuery = "insert into State (applicationid, enterpriseid, country, state_code, state_name, ";

					//HC Return Task
					strSQLQuery += "hc_return_within_days) ";
					//HC Return Task
					strSQLQuery += "values ('"+strAppID+"', '"+strEnterpriseID+"', '"+strCountry+"', '"+strCode+"', N'"+Utility.ReplaceSingleQuote(strName)+"', ";

					//HC Return Task
					strSQLQuery += strReturnDays.Trim() + ")";
					//HC Return Task
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

					iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","InsertState","RBAC003",appException.Message.ToString());
				//throw new ApplicationException("Insert operation failed",appException);
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			return iRowsInserted;
		}

		public static int DeleteState(DataSet dsStateTable, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsDeleted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsStateTable.Tables[0].Rows[rowIndex];
			string strCountry = (string)dr[0];
			string strCode = (string)dr[1];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","DeleteState","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				if(strCode!=null && strCode!="")
				{
					string strSQLQuery = "delete from state where ";
					strSQLQuery += "applicationid = "+"'"+strAppID+"' and enterpriseid = '"+strEnterpriseID;
					strSQLQuery += "' and country = '"+strCountry+"' and state_code = '"+strCode+"' "; 
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","DeleteState","RBAC003",appException.Message.ToString());
				//throw new ApplicationException("Delete operation failed",appException);
				throw new ApplicationException("Delete operation failed"+appException.Message,appException);
			}
			return iRowsDeleted;
		}
		

		//HC Return Task
		public static SessionDS GetHCReturnDayFromState(String strAppID, String strEntID, String strStateCode, int iCurRow, int iDSRowSize)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEntID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetHCReturnDayFromState","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			
			String strSQLQuery = "select hc_return_within_days ";
			strSQLQuery += "from state where ";
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEntID+"' "; 
			strSQLQuery += "and state_code = '"+ strStateCode + "'";
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurRow,iDSRowSize,"HCStateTable");
			return  sessionDS;
		}

		//HC Return Task

		//Zip Module
		public static SessionDS GetEmptyZipCode()
		{
			DataTable dtZip = new DataTable();
			dtZip.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtZip.Columns.Add(new DataColumn("country", typeof(string)));
			dtZip.Columns.Add(new DataColumn("state_code", typeof(string)));
			dtZip.Columns.Add(new DataColumn("zone_code", typeof(string)));
			dtZip.Columns.Add(new DataColumn("cutoff_time", typeof(DateTime)));
			dtZip.Columns.Add(new DataColumn("pickup_route", typeof(string)));
			dtZip.Columns.Add(new DataColumn("delivery_route", typeof(string)));
			dtZip.Columns.Add(new DataColumn("area", typeof(string)));
			dtZip.Columns.Add(new DataColumn("region", typeof(string)));
			dtZip.Columns.Add(new DataColumn("esa_surcharge", typeof(decimal)));

			for(int i=0; i < 1; i++)
			{
				DataRow drZip = dtZip.NewRow();
				drZip["zipcode"] = "";
				drZip["country"] = "";
				drZip["state_code"] = "";
				drZip["zone_code"] = "";				
				drZip["pickup_route"] = "";
				drZip["delivery_route"] = "";
				drZip["area"] = "";
				drZip["region"] = "";
				dtZip.Rows.Add(drZip);				
			}

			DataSet dsZip = new DataSet();
			dsZip.Tables.Add(dtZip);			
			dsZip.Tables[0].Columns["zipcode"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsZip;
			sessionDS.DataSetRecSize = dsZip.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return  sessionDS;
		}

		public static DataSet GetZipCodeDS(DataSet dsZipCodeGrid, int rowIndex, String strAppID, String strEnterpriseID)
		{

			DataRow dr = dsZipCodeGrid.Tables[0].Rows[rowIndex];
			string strCountry = (string)dr[0];
			string strState = (string)dr[1];
			string strZone = (string)dr[2];
			string strZip = (string)dr[3];
			//string strCutoff = (string)dr[4];
			string strCutoff = Convert.ToString(dr[4]);
			string strPickup = (string)dr[5];
			string strDelivery = (string)dr[5];
			string strArea = (string)dr[6];
			string strRegion = (string)dr[7];

			string strSQLWhere = "";

			if (strCountry!=null && strCountry!="")
			{
				strSQLWhere += "and country like '%"+strCountry+"%' ";
			}
			if (strState!=null && strState!="")
			{
				strSQLWhere += "and state_code like '%"+strState+"%' ";
			}
			if (strZone!=null && strZone!="")
			{
				strSQLWhere += "and zone_code like '%"+strZone+"%' ";
			}
			if (strZip!=null && strZip!="")
			{
				strSQLWhere += "and zipcode like '%"+strZip+"%' ";
			}
			/*if (strCutoff!=null && strCutoff!="")
			{
				strSQLWhere += "and cutoff_time = '"+strCutoff+"' ";
			}*/
			if (strPickup!=null && strPickup!="")
			{
				strSQLWhere += "and pickup_route like '%"+strPickup+"%' ";
			}
			if (strDelivery!=null && strDelivery!="")
			{
				strSQLWhere += "and delivery_route like '%"+strDelivery+"%' ";
			}
			if (strArea!=null && strArea!="")
			{
				strSQLWhere += "and area like '%"+Utility.ReplaceSingleQuote(strArea)+"%' ";
			}
			if (strRegion!=null && strRegion!="")
			{
				strSQLWhere += "and region like '%"+Utility.ReplaceSingleQuote(strRegion)+"%' ";
			}

			DataSet dsZipCode = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetZipCodeDS","EDB101","DbConnection object is null!!");
				return dsZipCode;
			}
			
			String strSQLQuery = "select country, state_code, zone_code, zipcode, ";
			strSQLQuery += "cutoff_time, ";
			//strSQLQuery += "cutoff_time, ";
			strSQLQuery += "pickup_route, delivery_route, area, region from zipcode where ";
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"; 
			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strSQLQuery += strSQLWhere;
			}
			
			dsZipCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			return  dsZipCode;
		}

		public static SessionDS GetZipCodeDS(String strAppID, String strEnterpriseID, int iCurRow, int iDSRowSize, DataSet dsQuery)
		{
			SessionDS sessionDS = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			string strZip = (string)dr[0];
			string strCountry = (string)dr[1];
			string strState = (string)dr[2];
			string strZone = (string)dr[3];
			//string strCutoff = Convert.ToString(dr[4]);
			//String strCutoff = Utility.DateFormat(strAppID,strEnterpriseID,dtCutoff,DTFormat.Time);
			//DateTime dtCutoff = (DateTime) dr[4];
			string strPickup = (string)dr[5];
			string strDelivery = (string)dr[6];
			string strArea = (string)dr[7];
			string strRegion = (string)dr[8];
			decimal decESurcharge = Convert.ToDecimal(dr[9]); 

			string strSQLWhere = "";

			if (strZip!=null && strZip!="")
			{
				strSQLWhere += "and zipcode like '"+strZip+"' ";
			}
			if (strCountry!=null && strCountry!="")
			{
				strSQLWhere += "and country like '"+strCountry+"' ";
			}
			if (strState!=null && strState!="")
			{
				strSQLWhere += "and state_code like '"+strState+"' ";
			}
			if (strZone!=null && strZone!="")
			{
				strSQLWhere += "and zone_code like '"+strZone+"' ";
			}
			if ((dr[4]!=null) && (!dr[4].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtCutoff = (DateTime) dr[4];
				String strCutoff = dtCutoff.ToString("HH:mm");
				strSQLWhere += "and SUBSTRING(convert(varchar, cutoff_time, 114),1, 5) = '"+strCutoff+"' ";
			}
			/*if (strCutoff!=null && strCutoff!="")
			{
				strSQLWhere += "and cutoff_time = '"+strCutoff+"' ";
			}*/
			if (strPickup!=null && strPickup!="")
			{
				strSQLWhere += "and pickup_route = '"+strPickup+"' ";
			}
			if (strDelivery!=null && strDelivery!="")
			{
				strSQLWhere += "and delivery_route = '"+strDelivery+"' ";
			}
			if (strArea!=null && strArea!="")
			{
				strSQLWhere += "and area = '"+Utility.ReplaceSingleQuote(strArea)+"' ";
			}
			if (strRegion!=null && strRegion!="")
			{
				strSQLWhere += "and region = '"+Utility.ReplaceSingleQuote(strRegion)+"' ";
			}
			if (decESurcharge>0)
			{
				strSQLWhere += "and esa_surcharge = "+decESurcharge;
			}
			//DataSet dsZipCode = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetZipCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			
			String strSQLQuery = "select zipcode, country, state_code, zone_code, ";
			strSQLQuery += "cutoff_time, ";
			//strSQLQuery += " RTRIM(LTRIM(STR({ fn HOUR(cutoff_time) }))) + ':' + RTRIM(LTRIM(STR({ fn HOUR(cutoff_time) }))) AS cutoff_time, ";
			strSQLQuery += "pickup_route, delivery_route, area, region, esa_surcharge from zipcode where ";
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"; 

			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strSQLQuery += strSQLWhere;
			}

			//dsZipCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			//return  dsZipCode;
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurRow,iDSRowSize,"ZipCodeTable");
			return  sessionDS;
		}

		public static void AddNewRowInZipCodeDS(ref SessionDS dsZipCode)
		{
			DataRow drNew = dsZipCode.ds.Tables[0].NewRow();
			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			drNew[3] = "";			
			drNew[5] = "";
			drNew[6] = "";
			drNew[7] = "";
			drNew[8] = "";
			try
			{
				dsZipCode.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataManager2","AddNewRowInZipCodeDS","R005",ex.Message.ToString());
			}
			
		}

		public static int UpdateZipCode(DataSet dsZipCodeChanged, String strAppID, String strEnterpriseID)
		{
			int iRowsUpdated = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsZipCodeChanged.Tables[0].Rows[0];
			string strZip = (string)dr[0];
			string strCountry = (string)dr[1];
			string strState = (string)dr[2];
			string strZone = (string)dr[3];
			//string strCutoff = Convert.ToString(dr[4]);
			DateTime dtCutoff = (DateTime)dr[4];
			String strCutoff = Utility.DateFormat(strAppID,strEnterpriseID,dtCutoff,DTFormat.Time);
			string strPickup = Convert.ToString(dr[5]);
			string strDelivery = Convert.ToString(dr[6]);
			string strArea = Convert.ToString(dr[7]);
			string strRegion = Convert.ToString(dr[8]);
			decimal decESurcharge = Convert.ToDecimal(dr[9]);

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","UpdateState","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strCountry!=null && strCountry!="" && strState!=null && strState!="" &&
					strZone!=null && strZone!="" && strZip!=null && strZip!="")
				{
					string strSQLQuery = " UPDATE ZIPCODE SET zone_code = '"+strZone+"', pickup_route = '"+strPickup+"',";
					strSQLQuery += " cutoff_time = "+strCutoff+", delivery_route = '"+strDelivery+"',";	
					
					if (Utility.IsNotDBNull(strArea) && strArea !="")
						strSQLQuery +=" area = '"+Utility.ReplaceSingleQuote(strArea)+"',";
					else
						strSQLQuery+=" area=null,";

					if (Utility.IsNotDBNull(strRegion) && strRegion !="")
						strSQLQuery +=" region = '"+Utility.ReplaceSingleQuote(strRegion)+"',";
					else
					{
						strSQLQuery +=" region =null,";
					}
                    strSQLQuery += " esa_surcharge = "+decESurcharge+" where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'";
					strSQLQuery += " and zipcode = '"+strZip+"' and country = '"+strCountry+"' and state_code = '"+ strState+"'";
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","UpdateZipCode","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Update operation failed",appException);				
			}
			return iRowsUpdated;
		}

		public static int InsertZipCode(DataSet dsZipCodeGrid, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsZipCodeGrid.Tables[0].Rows[rowIndex];
			string strZip = (string)dr[0];
			string strCountry = (string)dr[1];
			string strState = (string)dr[2];
			string strZone = (string)dr[3];
			
			DateTime dtCutoff = (DateTime)dr[4];
			String strCutoff = Utility.DateFormat(strAppID,strEnterpriseID,dtCutoff,DTFormat.Time);
			string strPickup =  (string)dr[5];
			string strDelivery =  (string)dr[6];
			string strArea = (string)dr[7];
			string strRegion = (string)dr[8];
			decimal decESurcharge = Convert.ToDecimal(0);//by X OCT 2 08 Convert.ToDecimal(dr[9]);

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","InsertZipCode","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strZip!=null && strZip!="")
				{
					string strSQLQuery = "insert into zipcode (applicationid, enterpriseid, zipcode, country, state_code, zone_code, ";
					strSQLQuery += "cutoff_time, ";
					strSQLQuery += "pickup_route, delivery_route, area, region, esa_surcharge) ";
					strSQLQuery += "values ('"+strAppID+"', '"+strEnterpriseID+"', '"+strZip+"', '"+strCountry+"', '"+strState+"', '"+strZone+"', ";
					strSQLQuery += strCutoff+", ";
					strSQLQuery += "'"+strPickup+"', '"+strDelivery+"', '"+Utility.ReplaceSingleQuote(strArea)+"', '"+Utility.ReplaceSingleQuote(strRegion)+"',"+decESurcharge+")";

					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

					iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","InsertZipCode","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			return iRowsInserted;
		}

		
		public static int DeleteZipCode(DataSet dsZipCodeGrid, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsDeleted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsZipCodeGrid.Tables[0].Rows[rowIndex];
			string strZip = (string)dr[0];
			string strState = (string)dr[2];
			string strCountry = (string)dr[1];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","DeleteZipCode","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				if(strZip!=null && strZip!="")
				{
					string strSQLQuery = "delete from zipcode where ";
					strSQLQuery += "applicationid = "+"'"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and zipcode = '"+strZip+"' and State_Code = '" + strState + "' and Country = '" + strCountry +"'"; 
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","DeleteZipCode","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Delete operation failed"+appException.Message,appException);
			}
			return iRowsDeleted;
		}

		//Base Zone Rates Module
		public static DataSet GetBZRatesDS(DataSet dsBZRatesGrid, String strAppID, String strEnterpriseID)
		{
			DataRow dr = dsBZRatesGrid.Tables[0].Rows[0];
			string strOZCode = (string)dr[0];
			string strDZCode = (string)dr[1];
			int intStartWt = Convert.ToInt32(dr[2]);
			int intEndWt = Convert.ToInt32(dr[3]); 
			string strService_Code = (string)dr[6];

//			int intSPrice = Convert.ToInt32(dr[4]); 
//			int intIPrice = Convert.ToInt32(dr[5]);
			string strEffectiveDate = null;
			if(dr[7]!=null)
				strEffectiveDate = dr[7].ToString();

			string strSQLWhere = "";
			strSQLWhere += " and origin_zone_code='" +strOZCode+ "' and destination_zone_code='"+strDZCode +"' and service_code='"+strService_Code+"' and effective_date='"+ (strEffectiveDate)+"'";

			DataSet dsBZRates = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetBZRatesDS","EDB101","DbConnection object is null!!");
				return dsBZRates;
			}
			
			String strSQLQuery = "select origin_zone_code, destination_zone_code, start_wt, end_wt, start_price, increment_price, service_code,effective_date ";
			strSQLQuery += "from base_zone_rates where ";
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"; 
			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strSQLQuery += strSQLWhere;
			}
			
			dsBZRates = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			return  dsBZRates;	
		}


		public static DataSet GetBZRatesDS(DataSet dsBZRatesGrid, int rowIndex, String strAppID, String strEnterpriseID)
		{
			DataRow dr = dsBZRatesGrid.Tables[0].Rows[rowIndex];
			string strOZCode = (string)dr[0];
			string strDZCode = (string)dr[1];

			int intStartWt = Convert.ToInt32(dr[2]);
			int intEndWt = Convert.ToInt32(dr[3]); 
			int intSPrice = Convert.ToInt32(dr[4]); 
			int intIPrice = Convert.ToInt32(dr[5]);
			string strEffectiveDate = null;
			if(dr[7]!=null)
				strEffectiveDate = (string)dr[7];

			string strSQLWhere = "";

			DataSet dsBZRates = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetBZRatesDS","EDB101","DbConnection object is null!!");
				return dsBZRates;
			}
			
			String strSQLQuery = "select origin_zone_code, destination_zone_code, start_wt, end_wt, start_price, increment_price, service_code,effective_date ";
			strSQLQuery += "from base_zone_rates where ";
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"; 
			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strSQLQuery += strSQLWhere;
			}
			
			dsBZRates = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			return  dsBZRates;	
		}

		public static SessionDS GetBZRatesDS(String strAppID, String strEnterpriseID, int iCurRow, int iDSRowSize, DataSet dsQuery)
		{
			SessionDS sessionDS = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			string strOZCode = "";
			string strDZCode = "";
			decimal intStartWt = 0;
			decimal intEndWt = 0;
			decimal intSPrice = 0;
			decimal intIPrice = 0;
			string strServiceCode = "";
			string strEffectiveDate = null;

			if(dr[0] != null)
				strOZCode = (string)dr[0];
			if(dr[1] != null)
				strDZCode = (string)dr[1];
			if(dr[2] != System.DBNull.Value)
				intStartWt = Convert.ToDecimal(dr[2]);
			if(dr[3] != System.DBNull.Value)
				intEndWt = Convert.ToDecimal(dr[3]);
			if(dr[4] != System.DBNull.Value)
				intSPrice = Convert.ToDecimal(dr[4]); 
			if(dr[5] != System.DBNull.Value)
				intIPrice = Convert.ToDecimal(dr[5]);
			if(dr[6] != System.DBNull.Value)
				strServiceCode = (string)dr[6];
			if(!dr[7].Equals(System.DBNull.Value))
				strEffectiveDate = dr[7].ToString();
			
			string strSQLWhere = "";

			if (strOZCode!=null && strOZCode!="")
			{
				strSQLWhere += "and origin_zone_code like '"+strOZCode+"' ";
			}
			if (strDZCode!=null && strDZCode!="")
			{
				strSQLWhere += "and destination_zone_code like '"+strDZCode+"' ";
			}
			if (intStartWt>0)
			{
				strSQLWhere += "and start_wt = "+intStartWt;
			}
			if (intEndWt>0)
			{
				strSQLWhere += "and end_wt = "+intEndWt;
			}
			if (intSPrice>0)
			{
				strSQLWhere += "and start_price = "+intSPrice;
			}
			if (intIPrice>0)
			{
				strSQLWhere += "and increment_price = "+intIPrice;
			}
			if (strServiceCode!=null && strServiceCode!="")
			{
				strSQLWhere += " and service_code like '"+strServiceCode+"' ";
			}
			if (strEffectiveDate!=null && strEffectiveDate!="")
			{
				strSQLWhere += " and effective_date ='"+Convert.ToDateTime(strEffectiveDate)+"' ";
			}

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetBZRatesDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			String strSQLQuery = "select origin_zone_code, destination_zone_code, start_wt, end_wt, start_price, increment_price, service_code,effective_date ";
			strSQLQuery += "from base_zone_rates where ";
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"; 
			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strSQLQuery += strSQLWhere;
			}

			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurRow,iDSRowSize,"BaseZoneRateTable");
			return  sessionDS;
		}

		public static DataSet GetESASurchargeDS(string strAppid,string strEnterpriseid,string custid,
			string postal_code,string service_code)
		{
			DataSet ds = new DataSet();
			string strESA_Sector = "";
			string strSQLQuery;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppid,strEnterpriseid);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetBZRatesDS","EDB101","DbConnection object is null!!");
				return ds;
			}
			//first find which ESA belongs to postal_code?
				strSQLQuery = "";
				strSQLQuery += " select top 1 esa_sector from enterprise_esa_sector where ";
				strSQLQuery += "applicationid = '"+strAppid+"' and enterpriseid = '"+strEnterpriseid+"'"; 
				strSQLQuery += " and postal_code = '"+postal_code+"'"; 
				strSQLQuery += " and effective_date < getdate() order by effective_date DESC "; 

				ds = (DataSet)dbCon.ExecuteQuery(strSQLQuery.ToString(),ReturnType.DataSetType);

				if(ds.Tables[0].Rows.Count>0)
				{
					strESA_Sector = ds.Tables[0].Rows[0][0].ToString();
				}
				else
				{
					//NO ESA SECTOR FOUND FROM CUSTID and ZIPCODE PROVIDED/ NO CALC.
					return GetEmptyESAServiceTypeDS().ds;
				}

			//second, use ESA Sector from the first + ServiceCode to retrieve ESA_Surcharge_Service_Type DS,
			//if not exists, look into default custID = ' '

			strSQLQuery = "";
			strSQLQuery = "select top 1 * ";
			strSQLQuery += "from ESA_Surcharge_Service_Type where ";
			strSQLQuery += "applicationid = '"+strAppid+"' and enterpriseid = '"+strEnterpriseid+"'"; 
			strSQLQuery += " and esa_sector = '"+strESA_Sector+"' and custid = '"+custid+"'"; 
			strSQLQuery += " and service_code = '"+service_code+"' and effective_date < getdate() order by effective_date DESC "; 

			ds = (DataSet)dbCon.ExecuteQuery(strSQLQuery.ToString(),ReturnType.DataSetType);

			if(ds.Tables[0].Rows.Count>0)
			{
				return ds; //found surcharge
			}
			else
			{
				//NO ESA SURCHARGE FOUND FROM CUSTID, ESA SECTOR, and Service CODE PROVIDED, DEFAULT VALUE INSTEAD
				strSQLQuery = "";
				strSQLQuery = "select top 1 * ";
				strSQLQuery += "from ESA_Surcharge_Service_Type where ";
				strSQLQuery += "applicationid = '"+strAppid+"' and enterpriseid = '"+strEnterpriseid+"'"; 
				strSQLQuery += " and esa_sector = '"+strESA_Sector+"' and custid = ' '";  //one space custid
				strSQLQuery += " and service_code = '"+service_code+"' and effective_date < getdate() order by effective_date DESC "; 

				ds = (DataSet)dbCon.ExecuteQuery(strSQLQuery.ToString(),ReturnType.DataSetType);

				if(ds.Tables[0].Rows.Count>0)
				{
					return ds; //found surcharge, return default value
				}

				return GetEmptyESAServiceTypeDS().ds; //AT LAST IF FOUND NOTHING
				
			}

		}


		public static DataSet GetESAServiceTypeDS(string strAppid,string strEnterpriseid,string custid,
			string esa_sector,string service_code,string effective_date)
		{
			DataSet ds = new DataSet();

			string strSQLWhere = "";

			if (custid!=null && custid!="")
			{
				strSQLWhere += "and custid like '%"+custid+"%' ";
			}
			if (esa_sector!=null && esa_sector!="")
			{
				strSQLWhere += "and esa_sector = '"+esa_sector+"' ";
			}
			if (service_code!=null && service_code!="")
			{
				strSQLWhere += " and service_code like '"+service_code+"' ";
			}
			if (effective_date!=null && effective_date!="")
			{
				strSQLWhere += " and effective_date ='"+effective_date+"' ";
			}

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppid,strEnterpriseid);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetBZRatesDS","EDB101","DbConnection object is null!!");
				return ds;
			}

			String strSQLQuery = "select * ";
			strSQLQuery += "from ESA_Surcharge_Service_Type where ";
			strSQLQuery += "applicationid = '"+strAppid+"' and enterpriseid = '"+strEnterpriseid+"'"; 
			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strSQLQuery += strSQLWhere;
			}



			ds = (DataSet)dbCon.ExecuteQuery(strSQLQuery.ToString(),ReturnType.DataSetType);
			return  ds;

		}

		public static DataSet GetBZRatesDS(String strAppID, String strEnterpriseID,string strOZCode,string strDZCode,int intStartWt,int intEndWt,int intSPrice,int intIPrice,string strServiceCode,string strEffectiveDate)
		{
			DataSet ds = new DataSet();

			string strSQLWhere = "";

			if (strOZCode!=null && strOZCode!="")
			{
				strSQLWhere += "and origin_zone_code like '"+strOZCode+"' ";
			}
			if (strDZCode!=null && strDZCode!="")
			{
				strSQLWhere += "and destination_zone_code like '"+strDZCode+"' ";
			}
			if (intStartWt>0)
			{
				strSQLWhere += "and start_wt = "+intStartWt;
			}
			if (intEndWt>0)
			{
				strSQLWhere += "and end_wt = "+intEndWt;
			}
			if (intSPrice>0)
			{
				strSQLWhere += "and start_price = "+intSPrice;
			}
			if (intIPrice>0)
			{
				strSQLWhere += "and increment_price = "+intIPrice;
			}
			if (strServiceCode!=null && strServiceCode!="")
			{
				strSQLWhere += " and service_code like '"+strServiceCode+"' ";
			}
			if (strEffectiveDate!=null && strEffectiveDate!="")
			{
				strSQLWhere += " and effective_date ='"+Convert.ToDateTime(strEffectiveDate)+"' ";
			}

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetBZRatesDS","EDB101","DbConnection object is null!!");
				return ds;
			}

			String strSQLQuery = "select origin_zone_code, destination_zone_code, start_wt, end_wt, start_price, increment_price, service_code,effective_date ";
			strSQLQuery += "from base_zone_rates where ";
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"; 
			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strSQLQuery += strSQLWhere;
			}



			ds = (DataSet)dbCon.ExecuteQuery(strSQLQuery.ToString(),ReturnType.DataSetType);
			return  ds;
		}

		public static SessionDS GetEmptyESAServiceTypeDS()
		{
			
			DataTable dtBZRates = new DataTable();
			dtBZRates.Columns.Add(new DataColumn("custid", typeof(string)));
			dtBZRates.Columns.Add(new DataColumn("esa_sector", typeof(string)));
			dtBZRates.Columns.Add(new DataColumn("service_code", typeof(string)));
			dtBZRates.Columns.Add(new DataColumn("esa_surcharge_amt", typeof(decimal)));
			dtBZRates.Columns.Add(new DataColumn("esa_surcharge_percent", typeof(decimal)));
			dtBZRates.Columns.Add(new DataColumn("min_esa_amount", typeof(decimal)));
			dtBZRates.Columns.Add(new DataColumn("max_esa_amount", typeof(decimal)));
			dtBZRates.Columns.Add(new DataColumn("effective_date", typeof(DateTime)));
			for(int i=0; i < 1; i++)
			{
				DataRow drEach = dtBZRates.NewRow();
//				drEach["esa_sector"] = "";
//				drEach["destination_zone_code"] = "";				
				dtBZRates.Rows.Add(drEach);
			}

			DataSet dsBZRates = new DataSet();
			dsBZRates.Tables.Add(dtBZRates);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsBZRates;
			sessionDS.DataSetRecSize = dsBZRates.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return  sessionDS;
		}

		public static SessionDS GetEmptyBZRatesDS()
		{
			
			DataTable dtBZRates = new DataTable();
 
			dtBZRates.Columns.Add(new DataColumn("origin_zone_code", typeof(string)));
			dtBZRates.Columns.Add(new DataColumn("destination_zone_code", typeof(string)));
			dtBZRates.Columns.Add(new DataColumn("start_wt", typeof(decimal)));
			dtBZRates.Columns.Add(new DataColumn("end_wt", typeof(decimal)));
			dtBZRates.Columns.Add(new DataColumn("start_price", typeof(decimal)));
			dtBZRates.Columns.Add(new DataColumn("increment_price", typeof(decimal)));
			dtBZRates.Columns.Add(new DataColumn("service_code", typeof(string)));
			dtBZRates.Columns.Add(new DataColumn("effective_date", typeof(DateTime)));
			for(int i=0; i < 1; i++)
			{
				DataRow drEach = dtBZRates.NewRow();
				drEach["origin_zone_code"] = "";
				drEach["destination_zone_code"] = "";				
				dtBZRates.Rows.Add(drEach);
			}

			DataSet dsBZRates = new DataSet();
			dsBZRates.Tables.Add(dtBZRates);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsBZRates;
			sessionDS.DataSetRecSize = dsBZRates.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return  sessionDS;
		}

		public static void AddNewRowInBZRatesDS(ref SessionDS dsBZRatesGrid)
		{
			DataRow drNew = dsBZRatesGrid.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			
			try
			{
				dsBZRatesGrid.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataManager2","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}
			
		}

		public static int UpdateBZRates(DataSet dsBZRatesChanged, String strAppID, String strEnterpriseID)
		{
			int iRowsUpdated = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsBZRatesChanged.Tables[0].Rows[0];
			string strOZCode = (string)dr[0];
			string strDZCode = (string)dr[1];
			decimal intStartWt = Convert.ToDecimal(dr[2]);
			decimal intEndWt = Convert.ToDecimal(dr[3]); 
			decimal intSPrice = Convert.ToDecimal(dr[4]); 
			decimal intIPrice = Convert.ToDecimal(dr[5]); 
			string strServiceCode = (string)dr[6];
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","UpdateBZRates","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strOZCode!=null && strOZCode!="" && strDZCode!=null && strDZCode!="")
				{					
					StringBuilder strSQLQuery = new StringBuilder();
					strSQLQuery.Append("update base_zone_rates set start_price = ");
					strSQLQuery.Append(intSPrice);
					strSQLQuery.Append(", increment_price = ");
					strSQLQuery.Append(intIPrice);
					strSQLQuery.Append(", service_code = '");
					strSQLQuery.Append(strServiceCode);
					strSQLQuery.Append("' where applicationid = '");
					strSQLQuery.Append(strAppID);
					strSQLQuery.Append("' and enterpriseid = '");
					strSQLQuery.Append(strEnterpriseID);
					strSQLQuery.Append("' and origin_zone_code = '");
					strSQLQuery.Append(strOZCode);
					strSQLQuery.Append("' and destination_zone_code = '");
					strSQLQuery.Append(strDZCode);
					strSQLQuery.Append("' and start_wt = ");
					strSQLQuery.Append(intStartWt);
					strSQLQuery.Append(" and end_wt = ");
					strSQLQuery.Append(intEndWt);

					dbCom = dbCon.CreateCommand(strSQLQuery.ToString(),CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","UpdateBZRates","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Update operation failed",appException);
				
			}
			return iRowsUpdated;
		}

		public static bool CheckBZRWeights(DataSet dsBZRatesWt, int rowIndex, String strAppId, String strEnterpriseId)
		{
			DataRow dr = dsBZRatesWt.Tables[0].Rows[rowIndex];
			string strOZCode = (string)dr[0];
			string strDZCode = (string)dr[1];
			decimal intStartWt = Convert.ToDecimal(dr[2]);
			decimal intEndWt = Convert.ToDecimal(dr[3]); 
			decimal dRowsAffected=0;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId,strEnterpriseId);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","InsertBZRates","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strOZCode!=null && strOZCode!="" && strDZCode!=null && strDZCode!="")
				{
					StringBuilder strSQLQuery=new StringBuilder();
					strSQLQuery.Append(" Select Count(*) from base_zone_rates where applicationid = "+"'"+strAppId+"'");
					strSQLQuery.Append(" and enterpriseid = '"+strEnterpriseId+"' and origin_zone_code = '"+strOZCode+"' and "); 
					strSQLQuery.Append(" destination_zone_code = '"+strDZCode+"' and ((start_wt between "+intStartWt+" and "+intEndWt+") ");
					strSQLQuery.Append(" OR (end_wt between "+intStartWt+" and "+intEndWt+"))");
					
					dRowsAffected=Convert.ToDecimal(dbCon.ExecuteScalar(strSQLQuery.ToString()));
				}
			}

			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","InsertZone","RBAC003",appException.Message.ToString());				
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			
			if (dRowsAffected > 0)
				return true;
			else
				return false;
			
		}

		public static bool VerifyZoneCode(string strAppID, string strEnterpriseID,string ZoneCode)
		{
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select count(*) from zone where applicationid='");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("'");
			if (ZoneCode!= null && ZoneCode != "")
			{
				strQry.Append(" and zone_code ='");
				strQry.Append(Utility.ReplaceSingleQuote(ZoneCode));
				strQry.Append("'");
			}

			String strSQLQuery = strQry.ToString();


			decimal dRowsAffected=0;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","InsertBZRates","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{							
				dRowsAffected=Convert.ToDecimal(dbCon.ExecuteScalar(strSQLQuery.ToString()));
			}

			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","Zone","RBAC003",appException.Message.ToString());				
				throw new ApplicationException("Get operation failed"+appException.Message,appException);
			}
			
			if (dRowsAffected > 0)
				return true;
			else
				return false;

		}


		public static bool VerifyServiceCode(string strAppID, string strEnterpriseID,string ServiceCode)
		{
			StringBuilder strQry = new StringBuilder();
			strQry.Append("select count(*) from service where applicationid='");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID);
			strQry.Append("'");
			if (ServiceCode!= null && ServiceCode != "")
			{
				strQry.Append(" and service_code ='");
				strQry.Append(Utility.ReplaceSingleQuote(ServiceCode));
				strQry.Append("'");
			}

			String strSQLQuery = strQry.ToString();


			decimal dRowsAffected=0;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","ServiceCode","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{							
				dRowsAffected=Convert.ToDecimal(dbCon.ExecuteScalar(strSQLQuery.ToString()));
			}

			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","ServiceCode","RBAC003",appException.Message.ToString());				
				throw new ApplicationException("Get operation failed"+appException.Message,appException);
			}
			
			if (dRowsAffected > 0)
				return true;
			else
				return false;

		}



		public static bool CheckBZRWeights(DataSet dsBZRatesWt, int rowIndex, String strAppId, String strEnterpriseId, String strAgentId)
		{
			DataRow dr = dsBZRatesWt.Tables[0].Rows[rowIndex];
			string strOZCode = (string)dr[0];
			string strDZCode = (string)dr[1];
			decimal intStartWt = Convert.ToDecimal(dr[2]);
			decimal intEndWt = Convert.ToDecimal(dr[3]); 
			decimal dRowsAffected=0;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId,strEnterpriseId);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","InsertBZRates","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strOZCode!=null && strOZCode!="" && strDZCode!=null && strDZCode!="")
				{
					StringBuilder strSQLQuery=new StringBuilder();
					strSQLQuery.Append(" Select Count(*) from Agent_Zone_Wt_Cost where Agentid='"+strAgentId+"' and applicationid = '"+strAppId+"'");
					strSQLQuery.Append(" and enterpriseid = '"+strEnterpriseId+"' and origin_zone_code = '"+strOZCode+"' and "); 
					strSQLQuery.Append(" destination_zone_code = '"+strDZCode+"' and ((start_wt between "+intStartWt+" and "+intEndWt+") ");
					strSQLQuery.Append(" OR (end_wt between "+intStartWt+" and "+intEndWt+"))");
					
					dRowsAffected=Convert.ToDecimal(dbCon.ExecuteScalar(strSQLQuery.ToString()));
				}
			}

			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","InsertZone","RBAC003",appException.Message.ToString());				
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			
			if (dRowsAffected > 0)
				return true;
			else
				return false;
			
		}

		public static int InsertBZRates(DataSet dsBZRatesGrid, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsBZRatesGrid.Tables[0].Rows[rowIndex];
			string strOZCode = (string)dr[0];
			string strDZCode = (string)dr[1];
			decimal intStartWt = Convert.ToDecimal(dr[2]);
			decimal intEndWt = Convert.ToDecimal(dr[3]); 
			decimal intSPrice = Convert.ToDecimal(dr[4]); 
			decimal intIPrice = Convert.ToDecimal(dr[5]); 
			string strServiceCode = (string)dr[6];
			//string sEffectiveDate=dr[7].ToString();
			DateTime dtEffectiveDate = DateTime.Parse(dr[7].ToString());
			DateTime EffectiveDate = dtEffectiveDate.Date;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","InsertBZRates","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteFR","DeleteFR","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{
				if(strOZCode!=null && strOZCode!="" && strDZCode!=null && strDZCode!="")
				{
					//object objEffectivedate = DateTime.ParseExact(strEffectiveDate,"dd/MM/yyyy",null);
					StringBuilder strSqlSel = new StringBuilder();
					strSqlSel.Append("select * from base_zone_rates where applicationid = '");
					strSqlSel.Append(strAppID);
					strSqlSel.Append("'and  enterpriseid = '");
					strSqlSel.Append(strEnterpriseID);
					strSqlSel.Append("' and origin_zone_code = '");
					strSqlSel.Append(strOZCode);
					strSqlSel.Append("' and  destination_zone_code = '");
					strSqlSel.Append(strDZCode);
					strSqlSel.Append("' and service_code = '");
					strSqlSel.Append(strServiceCode);
					strSqlSel.Append("' and effective_date = '");
					strSqlSel.Append(EffectiveDate);
					strSqlSel.Append("'");

					dbCom = dbCon.CreateCommand(strSqlSel.ToString(),CommandType.Text);
					dbCom.Connection = conApp;
					dbCom.Transaction = transactionApp;
					DataSet dsResult = (DataSet)dbCon.ExecuteQueryInTransaction(dbCom,ReturnType.DataSetType);

					if(dsResult.Tables[0].Rows.Count >0)
					{
						StringBuilder strSqlDel = new StringBuilder();
						strSqlDel.Append("Delete from base_zone_rates where applicationid = '");
						strSqlDel.Append(strAppID);
						strSqlDel.Append("'and  enterpriseid = '");
						strSqlDel.Append(strEnterpriseID);
						strSqlDel.Append("' and origin_zone_code = '");
						strSqlDel.Append(strOZCode);
						strSqlDel.Append("' and  destination_zone_code = '");
						strSqlDel.Append(strDZCode);
						strSqlDel.Append("' and service_code = '");
						strSqlDel.Append(strServiceCode);
						strSqlDel.Append("' and effective_date = '");
						strSqlDel.Append(EffectiveDate);
						strSqlDel.Append("'");

						dbCom = dbCon.CreateCommand(strSqlDel.ToString(),CommandType.Text);
						dbCom.Connection = conApp;
						dbCom.Transaction = transactionApp;
						dbCon.ExecuteNonQueryInTransaction(dbCom);
					}

					StringBuilder strSQLQuery =  new StringBuilder();
					strSQLQuery.Append("insert into base_zone_rates (applicationid, enterpriseid, origin_zone_code, destination_zone_code,");
					strSQLQuery.Append("start_wt, end_wt, start_price, increment_price, service_code,effective_date)values ('");
					strSQLQuery.Append(strAppID);
					strSQLQuery.Append("', '");
					strSQLQuery.Append(strEnterpriseID);
					strSQLQuery.Append("', '");
					strSQLQuery.Append(strOZCode);
					strSQLQuery.Append("', '");
					strSQLQuery.Append(strDZCode);
					strSQLQuery.Append("', ");
					strSQLQuery.Append(intStartWt);
					strSQLQuery.Append(", ");
					strSQLQuery.Append(intEndWt);
					strSQLQuery.Append(", ");
					strSQLQuery.Append(intSPrice);
					strSQLQuery.Append(", ");
					strSQLQuery.Append(intIPrice);
					strSQLQuery.Append(",'");
					strSQLQuery.Append(strServiceCode);
					strSQLQuery.Append("','");
					strSQLQuery.Append(EffectiveDate);
					strSQLQuery.Append("')");

					dbCom = dbCon.CreateCommand(strSQLQuery.ToString(),CommandType.Text);
					dbCom.Connection = conApp;
					dbCom.Transaction = transactionApp;
					iRowsInserted = dbCon.ExecuteNonQueryInTransaction(dbCom);

					transactionApp.Commit();
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","InsertZone","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			finally
			{
			
			}
			return iRowsInserted;
		}

		public static int InsertESAServiceType(DataRow dr, String strAppID, String strEnterpriseID)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;
			string custid = (string)dr[0];
			string esa_sector = (string)dr[1];
			string strServiceCode = (string)dr[2];
			decimal esa_surcharge_amt = Convert.ToDecimal(dr[3]); 
			decimal esa_surcharge_percent = Convert.ToDecimal(dr[4]); 
			decimal min_esa_amount = Convert.ToDecimal(dr[5]); 
			decimal max_esa_amount = Convert.ToDecimal(dr[6]);
			string strEffectiveDate = dr[7].ToString();
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","InsertBZRates","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(custid!=null && esa_sector!="" && strServiceCode!=null && strEffectiveDate!="")
				{
					string strSQLQuery = "insert into ESA_Surcharge_Service_type (applicationid, enterpriseid, custid, esa_sector, service_code, esa_surcharge_amt, esa_surcharge_percent, min_esa_amount, max_esa_amount, effective_date) ";
					strSQLQuery += "values ('"+strAppID+"', '"+strEnterpriseID+"','"+custid+"', '"+esa_sector+"', '"+strServiceCode+"', "+esa_surcharge_amt+", "+esa_surcharge_percent+","+min_esa_amount+","+max_esa_amount+",'"+strEffectiveDate+"')";

					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

					iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","InsertZone","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			return iRowsInserted;
		}


		public static int InsertBZRates(DataRow dr, String strAppID, String strEnterpriseID)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;
			string strOZCode = (string)dr[0];
			string strDZCode = (string)dr[1];
			decimal intStartWt = Convert.ToDecimal(dr[2]);
			decimal intEndWt = Convert.ToDecimal(dr[3]); 
			decimal intSPrice = Convert.ToDecimal(dr[4]); 
			decimal intIPrice = Convert.ToDecimal(dr[5]); 
			string strServiceCode = (string)dr[6];
			string strEffectiveDate = dr[7].ToString();

			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","InsertBZRates","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("SysDataManager2","InsertBZRates","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataManager2","InsertBZRates","ERR004","Error opening connection : "+ 

					appException.Message.ToString());
				throw new ApplicationException("Error opening database connection",appException);
				}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataManager2","InsertBZRates","ERR005","Error opening connection : "+ 	exception.Message.ToString());
				throw new ApplicationException("Error opening database connection",exception);
				}
	
			try
			{
				if(strOZCode!=null && strOZCode!="" && strDZCode!=null && strDZCode!="")
				{
					string strSQLQuery = "insert into base_zone_rates (applicationid, enterpriseid, origin_zone_code, destination_zone_code, start_wt, end_wt, start_price, increment_price, service_code, effective_date) ";
					strSQLQuery += "values ('"+strAppID+"', '"+strEnterpriseID+"', '"+strOZCode+"', '"+strDZCode+"', "+intStartWt+", "+intEndWt+", "+intSPrice+", "+intIPrice+",'"+strServiceCode+"','"+strEffectiveDate+"')";

					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

					iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","InsertZone","RBAC003",appException.Message.ToString());
				//throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsInserted;
		}

		public static int DeleteBZRates(DataSet dsBZRatesGrid, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsDeleted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsBZRatesGrid.Tables[0].Rows[rowIndex];
			string strOZCode = (string)dr[0];
			string strDZCode = (string)dr[1];
			DateTime dtEffectiveDate = DateTime.UtcNow;
			if(dr[7] != System.DBNull.Value)
				dtEffectiveDate = (DateTime)dr[7];
			decimal intStartWt;
			if(dr[2] != System.DBNull.Value)
				intStartWt = Convert.ToDecimal(dr[2]);
			decimal intEndWt;
			if(dr[3] != System.DBNull.Value)
				intEndWt = Convert.ToDecimal(dr[3]); 
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","DeleteZone","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			//*****Bigin: Add by Sompote 2010-04-05*****
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("SysDataManager2","DeleteZone","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataManager2","DeleteZone","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataManager2","DeleteZone","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}			
			//*****End: Add by Sompote 2010-04-05*****
			try
			{
				if(strOZCode!=null && strOZCode!="" && strDZCode!=null && strDZCode!="")
				{
					StringBuilder  strSQLQuery =  new StringBuilder();
					strSQLQuery.Append("delete from base_zone_rates where applicationid = '");
					strSQLQuery.Append(strAppID);
					strSQLQuery.Append("' and enterpriseid = '");
					strSQLQuery.Append(strEnterpriseID);
					strSQLQuery.Append("' and origin_zone_code = '");
					strSQLQuery.Append(strOZCode);
					strSQLQuery.Append("' and destination_zone_code = '"); 
					strSQLQuery.Append(strDZCode);
					strSQLQuery.Append("' and effective_date = '");
					strSQLQuery.Append(dtEffectiveDate);
					strSQLQuery.Append("'");
	
					dbCom = dbCon.CreateCommand(strSQLQuery.ToString(),CommandType.Text);
					//iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);
					iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","DeleteZone","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Delete operation failed"+appException.Message,appException);
			}
			finally
			{				
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsDeleted;
		}

		public static int CheckWtInBZRatesDS(DataSet dsBZRatesChanged, String strAppID, String strEnterpriseID)
		{
			int intRet = 0;

			DataRow dr = dsBZRatesChanged.Tables[0].Rows[0];
			string strOZCode = (string)dr[0];
			string strDZCode = (string)dr[1];
			decimal intStartWt = Convert.ToDecimal(dr[2]);
			decimal intEndWt = Convert.ToDecimal(dr[3]); 

			if (intEndWt < intStartWt)
			{
				intRet = -1;
			}
			else if ((intStartWt % 1 > 0)&& (intStartWt != intEndWt))
			{
				intRet = 1;
			}
			else
			{
				intRet = 1;
			}
			
			return intRet;
		}



		/*
		 *  AGENT Base Zone Rates Module
		 */

		public static DataSet GetAgentBZRatesDS(DataSet dsBZRatesGrid, int rowIndex, String strAppID, String strEnterpriseID, String strAgentId)
		{
			DataRow dr = dsBZRatesGrid.Tables[0].Rows[rowIndex];
			string strOZCode = (string)dr[0];
			string strDZCode = (string)dr[1];
			int intStartWt = Convert.ToInt32(dr[2]);
			int intEndWt = Convert.ToInt32(dr[3]);
			int intSPrice = Convert.ToInt32(dr[4]);
			int intIPrice = Convert.ToInt32(dr[5]); 
			
			DataSet dsBZRates = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetBZRatesDS","EDB101","DbConnection object is null!!");
				return dsBZRates;
			}
			
			StringBuilder strSQLQuery=new StringBuilder();

			strSQLQuery.Append( " Select origin_zone_code, destination_zone_code, start_wt, end_wt, start_price, increment_price ");
			strSQLQuery.Append(" from base_zone_rates where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"); 
			strSQLQuery.Append(" and AgentID='"+strAgentId+"'");
						
			dsBZRates = (DataSet)dbCon.ExecuteQuery(strSQLQuery.ToString(),ReturnType.DataSetType);
			return  dsBZRates;
	
		}

		public static SessionDS GetAgentBZRatesDS(String strAppID, String strEnterpriseID, String strAgentId, int iCurRow, int iDSRowSize, DataSet dsQuery)
		{
			SessionDS sessionDS = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			string strOZCode = (string)dr[0];
			string strDZCode = (string)dr[1];
			decimal intStartWt = Convert.ToDecimal(dr[2]);
			decimal intEndWt = Convert.ToDecimal(dr[3]); 
			decimal intSPrice = Convert.ToDecimal(dr[4]); 
			decimal intIPrice = Convert.ToDecimal(dr[5]); 
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetBZRatesDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			StringBuilder strSQLQuery=new StringBuilder();
            			
			strSQLQuery.Append(" select origin_zone_code, destination_zone_code, start_wt, end_wt, start_price, increment_price ");
			strSQLQuery.Append(" from Agent_Zone_Wt_Cost where AgentID='"+strAgentId+"'");
			
			if (strOZCode!=null && strOZCode!="")
			{
				strSQLQuery.Append(" and origin_zone_code like '%"+strOZCode+"%'");
			}
			if (strDZCode!=null && strDZCode!="")
			{
				strSQLQuery.Append(" and destination_zone_code like '%"+strDZCode+"%'");
			}
			if (intStartWt>0)
			{
				strSQLQuery.Append(" and start_wt = "+intStartWt);
			}
			if (intEndWt>0)
			{
				strSQLQuery.Append(" and end_wt = "+intEndWt);
			}
			if (intSPrice>0)
			{
				strSQLQuery.Append(" and start_price = "+intSPrice);
			}
			if (intIPrice>0)
			{
				strSQLQuery.Append(" and increment_price = "+intIPrice);
			}				
			strSQLQuery.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'"); 						
			sessionDS = dbCon.ExecuteQuery(strSQLQuery.ToString(),iCurRow,iDSRowSize,"Agent_Zone_Wt_Cost");
			return  sessionDS;
		}

		public static SessionDS GetEmptyAgentBZRatesDS()
		{
			
			DataTable dtBZRates = new DataTable();
 
			dtBZRates.Columns.Add(new DataColumn("origin_zone_code", typeof(string)));
			dtBZRates.Columns.Add(new DataColumn("destination_zone_code", typeof(string)));
			dtBZRates.Columns.Add(new DataColumn("start_wt", typeof(decimal)));
			dtBZRates.Columns.Add(new DataColumn("end_wt", typeof(decimal)));
			dtBZRates.Columns.Add(new DataColumn("start_price", typeof(decimal)));
			dtBZRates.Columns.Add(new DataColumn("increment_price", typeof(decimal)));
			
			for(int i=0; i < 1; i++)
			{
				DataRow drEach = dtBZRates.NewRow();
				drEach["origin_zone_code"] = "";
				drEach["destination_zone_code"] = "";				
				dtBZRates.Rows.Add(drEach);
			}

			DataSet dsBZRates = new DataSet();
			dsBZRates.Tables.Add(dtBZRates);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsBZRates;
			sessionDS.DataSetRecSize = dsBZRates.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return  sessionDS;
		}

		public static void AddRowAgentBZRatesDS(ref SessionDS dsBZRates)
		{
			DataRow drNew = dsBZRates.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = 0;
			drNew[3] = 0;
			drNew[4] = 0;
			drNew[5] = 0;
			
			try
			{
				dsBZRates.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataManager2","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}
		}

		public static int UpdateAgentBZRates(DataSet dsBZRatesChanged, String strAppID, String strEnterpriseID, String strAgentId)
		{
			int iRowsUpdated = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsBZRatesChanged.Tables[0].Rows[0];
			string strOZCode = (string)dr[0];
			string strDZCode = (string)dr[1];
			decimal intStartWt = Convert.ToDecimal(dr[2]);
			decimal intEndWt = Convert.ToDecimal(dr[3]); 
			decimal intSPrice = Convert.ToDecimal(dr[4]); 
			decimal intIPrice = Convert.ToDecimal(dr[5]); 
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","UpdateBZRates","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strSQLQuery=new StringBuilder();

			try
			{
				if(strOZCode!=null && strOZCode!="" && strDZCode!=null && strDZCode!="")
				{					
					strSQLQuery.Append(" Update Agent_Zone_Wt_Cost set start_price = "+intSPrice+", increment_price = "+intIPrice);
					strSQLQuery.Append(" where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");
					strSQLQuery.Append(" and origin_zone_code = '"+strOZCode+"' and destination_zone_code = '"+strDZCode+"'");
					strSQLQuery.Append(" and start_wt = "+intStartWt+" and end_wt = "+intEndWt+" and AgentId='"+strAgentId+"'");

					dbCom = dbCon.CreateCommand(strSQLQuery.ToString(),CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				}
			}

			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","UpdateBZRates","RBAC003",appException.Message.ToString());
				throw new ApplicationException("Update operation failed",appException);
				
			}
			return iRowsUpdated;
		}

		public static int InsertAgentBZRates(DataSet dsBZRatesGrid, int rowIndex, String strAppID, String strEnterpriseID, String strAgentId)
		{
			int iRowsInserted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsBZRatesGrid.Tables[0].Rows[rowIndex];
			string strOZCode = (string)dr[0];
			string strDZCode = (string)dr[1];
			decimal intStartWt = Convert.ToDecimal(dr[2]);
			decimal intEndWt = Convert.ToDecimal(dr[3]); 
			decimal intSPrice = Convert.ToDecimal(dr[4]); 
			decimal intIPrice = Convert.ToDecimal(dr[5]); 
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","InsertBZRates","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strSQLQuery=new StringBuilder();
			try
			{
				if(strOZCode!=null && strOZCode!="" && strDZCode!=null && strDZCode!="")
				{			
					strSQLQuery.Append(" Insert into Agent_Zone_Wt_Cost (applicationid, enterpriseid, AgentID,");
					strSQLQuery.Append(" origin_zone_code, destination_zone_code, start_wt, end_wt, start_price,"); 
					strSQLQuery.Append(" increment_price) values ('"+strAppID+"', '"+strEnterpriseID+"', '"+strAgentId);
					strSQLQuery.Append("','"+ strOZCode+"','"+strDZCode+"',"+intStartWt+","+intEndWt+","+intSPrice+","+intIPrice+")");

					dbCom = dbCon.CreateCommand(strSQLQuery.ToString(),CommandType.Text);
					iRowsInserted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","InsertZone","RBAC003",appException.Message.ToString());				
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			return iRowsInserted;
		}

		public static int DeleteAgentBZRates(DataSet dsBZRatesGrid, int rowIndex, String strAppID, String strEnterpriseID, String strAgentId)
		{
			int iRowsDeleted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsBZRatesGrid.Tables[0].Rows[rowIndex];
			string strOZCode = (string)dr[0];
			string strDZCode = (string)dr[1];
			decimal intStartWt = Convert.ToDecimal(dr[2]);
			decimal intEndWt = Convert.ToDecimal(dr[3]); 
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","DeleteZone","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strSQLQuery=new StringBuilder();

			try
			{
				if(strOZCode!=null && strOZCode!="" && strDZCode!=null && strDZCode!="")
				{
					strSQLQuery.Append(" Delete from Agent_Zone_Wt_Cost where applicationid = '"+strAppID+"'");
					strSQLQuery.Append(" and enterpriseid = '"+strEnterpriseID+"' and origin_zone_code = '");
					strSQLQuery.Append(strOZCode+"' and destination_zone_code = '"+strDZCode+"' and ");
					strSQLQuery.Append(" start_wt = "+intStartWt+" and end_wt = "+intEndWt+" and AgentId='"+strAgentId+"'");
	
					dbCom = dbCon.CreateCommand(strSQLQuery.ToString(),CommandType.Text);
					iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","DeleteZone","RBAC003",appException.Message.ToString());				
				throw new ApplicationException("Delete operation failed"+appException.Message,appException);
			}
			return iRowsDeleted;
		}

		/*
		 * Enterprise VAS 
		*/


		//Methods for VAS Code - ZipCode VAS Excluded....

		public static SessionDS GetEntVASDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{

			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetEntVASDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" Select EDS.VAS_Code, V.vas_description, EDS.Effective_From, EDS.Status_Active From ");
			strBuilder.Append(" Enterprise_Default_Surcharge EDS INNER JOIN VAS V ON EDS.VAS_Code=V.VAS_Code and ");
			strBuilder.Append(" EDS.ApplicationID=V.ApplicationID and EDS.EnterpriseID=V.EnterpriseID ");
			strBuilder.Append(" where EDS.applicationid='"+strAppID+"' and EDS.enterpriseid = '"+strEnterpriseID+"' ");

			DataRow drEach = dsQueryParam.Tables[0].Rows[0];				
			String strVASCode = (String) drEach["vas_code"];
			String strVASDescp="";
			
			if((strVASCode != null) && (strVASCode != ""))
				strBuilder.Append(" and vas_code like '%"+strVASCode+"%' ");
			
			if(Utility.IsNotDBNull(drEach["vas_description"]) && (drEach["vas_description"].ToString() != ""))
			{
				strVASDescp=drEach["vas_description"].ToString().Trim();
				strBuilder.Append(" and vas_Description like N'%"+Utility.ReplaceSingleQuote(strVASDescp)+"%' ");
			}

			if(Utility.IsNotDBNull(drEach["effective_from"]) && drEach["effective_from"].ToString()!="")
			{
				DateTime dtEffectiveFrom=Convert.ToDateTime(drEach["effective_from"]);
				String strEffectiveFrom=Utility.DateFormat(strAppID,strEnterpriseID,dtEffectiveFrom,DTFormat.Date);				
				strBuilder.Append(" and effective_from ="+strEffectiveFrom);				
			}

			String strStatusActive="";
			if(Utility.IsNotDBNull(drEach["Status_Active"]) && drEach["Status_Active"].ToString()!="")
			{
				strStatusActive=drEach["Status_Active"].ToString();
				strBuilder.Append(" and Status_Active = '"+ strStatusActive+"'");
			}
			
			strBuilder.Append(" Order by Status_Active desc, EDS.VAS_Code asc");
			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"VASCodeTable");

			return  sessionDS;
	
		}

		public static bool checkRepEntVas(String strAppID, String strEnterpriseID, String vasCode)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT COUNT(vas_code) FROM Enterprise_Default_Surcharge WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' AND vas_code = '");
			strBuilder.Append(vasCode);
			strBuilder.Append("'");
			String strSQLQuery = strBuilder.ToString();
			//IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery, CommandType.Text);
			try
			{
				int count = Convert.ToInt16(dbCon.ExecuteScalar(strSQLQuery).ToString());
				if (count == 0)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","InsertEntVAS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting VAS Code ",appException);
			}
		}


		public static int InsertEntVAS(String strAppID, String strEnterpriseID,DataSet dsToInsert, int rowIndex)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","GetEntVASDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" insert into Enterprise_Default_Surcharge (applicationid,enterpriseid,vas_code, ");
			strBuilder.Append(" Effective_from, Status_Active) values ('"+strAppID+"','"+strEnterpriseID+"','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;
			DataRow drEach = dsToInsert.Tables[0].Rows[rowIndex];				
			String strVASCode = (String) drEach["vas_code"];
			strBuilder.Append(strVASCode+"',");

			if (checkRepEntVas(strAppID, strEnterpriseID, strVASCode))
			{
				return 0;
			}
			
			if(Utility.IsNotDBNull(drEach["Effective_from"]) && drEach["Effective_from"].ToString()!="")
			{
				DateTime dtEffectiveFrom=Convert.ToDateTime(drEach["Effective_from"]);
				String strEffectiveFrom=Utility.DateFormat(strAppID,strEnterpriseID,dtEffectiveFrom,DTFormat.Date);
				strBuilder.Append(strEffectiveFrom+",");
			}
						
			String strStatusActive="";
			if(Utility.IsNotDBNull(drEach["Status_Active"]) &&  drEach["Status_Active"].ToString() !="")
			{
				strStatusActive= drEach["Status_Active"].ToString();							
				strBuilder.Append("'"+strStatusActive+"')");
			}
			else
			{
				strBuilder.Append("null)");
			}

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataManager2","InsertEntVAS","SDM001",iRowsAffected+" rows inserted in to VAS table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","InsertEntVAS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting VAS Code ",appException);
			}
			return iRowsAffected;

		}

		public static int UpdateEntVAS(String strAppID, String strEnterpriseID,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","UpdateEntVAS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];
			StringBuilder strBuilder = new StringBuilder();			
			strBuilder.Append("update Enterprise_Default_Surcharge set Status_Active=");
		
			String strStatusActive="";			
			if(Utility.IsNotDBNull(drEach["Status_Active"]) &&  drEach["Status_Active"].ToString() !="")
			{
				strStatusActive= drEach["Status_Active"].ToString();							
				strBuilder.Append("'"+strStatusActive+"'");
			}
			else
			{
				strBuilder.Append("null");
			}

			String strEffectiveFrom="";
			 if(Utility.IsNotDBNull(drEach["Effective_from"]) && drEach["Effective_from"].ToString()!="")
			{
				DateTime dtEffectiveFrom=Convert.ToDateTime(drEach["Effective_from"]);
				strEffectiveFrom=Utility.DateFormat(strAppID,strEnterpriseID,dtEffectiveFrom,DTFormat.Date);				
			}
			
			String strVASCode = (String) drEach["vas_code"];
			strBuilder.Append(" where applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID);
			strBuilder.Append("' and vas_code = '"+strVASCode+"' and Effective_From="+strEffectiveFrom);

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataManager2","UpdateEntVAS","SDM001",iRowsAffected+" rows inserted in to VAS_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","UpdateEntVAS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting VAS Code ",appException);
			}
			return iRowsAffected;

		}

		public static SessionDS GetEmptyEntVAS()
		{			
			DataTable dtEntVAS = new DataTable(); 
			dtEntVAS.Columns.Add(new DataColumn("vas_code", typeof(string)));
			dtEntVAS.Columns.Add(new DataColumn("vas_description", typeof(string)));
			dtEntVAS.Columns.Add(new DataColumn("Effective_from", typeof(DateTime)));
			dtEntVAS.Columns.Add(new DataColumn("Status_Active", typeof(string)));

			DataSet dsVASCode = new DataSet();
			dsVASCode.Tables.Add(dtEntVAS);
			
			dsVASCode.Tables[0].Columns["vas_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsVASCode;
			sessionDS.DataSetRecSize = dsVASCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}

		public static void AddNewRowInEntVASDS(ref SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = System.DBNull.Value;
			drNew[3] = "";

			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataManager2","AddNewRowInEntVASDS","R005",ex.Message.ToString());
			}			
			sessionDS.QueryResultMaxSize++;
		}

		public static int DeleteEntVAS(String strAppID, String strEnterpriseID,String strVASCode, DateTime dtEffectiveFrom)
		{
			int iRowsDeleted = 0;
			IDbCommand dbCom = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","DeleteEntVAS","DPC001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQuery=new StringBuilder();
			try
			{
				if(strVASCode!=null && strVASCode!="")
				{
					strQuery.Append(" Delete from Enterprise_Default_Surcharge where applicationid = '"+strAppID+"'");
					strQuery.Append(" and enterpriseid = '"+strEnterpriseID+"' and vas_code = '"+strVASCode+"'"); 
				}
				if (Utility.IsNotDBNull(dtEffectiveFrom))
				{
					strQuery.Append(" and Effective_From="+Utility.DateFormat(strAppID,strEnterpriseID,dtEffectiveFrom,DTFormat.Date));
				}
				dbCom = dbCon.CreateCommand(strQuery.ToString(),CommandType.Text);
				iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);				
			}

			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgr","DeleteCostCode","DPC002",appException.Message.ToString());
			
				throw new ApplicationException("Delete operation failed"+appException.Message,appException);
			}
			return iRowsDeleted;

		}


		//Add by Panas
		public static bool ckZoneOrServiceInSystem(String strEnterpriseId,String strAppId,String strParameter,String ckType)
		{
			decimal dRowsAffected=0;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId,strEnterpriseId);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","InsertBZRates","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(ckType.Equals("Zone"))
				{
					if(strParameter != null && strParameter != "")
					{
						StringBuilder strSQLQuery=new StringBuilder();

						strSQLQuery.Append(" select count(*) from zone ");
						strSQLQuery.Append(" where applicationid = '");
						strSQLQuery.Append(strAppId);
						strSQLQuery.Append("' and enterpriseid = '");
						strSQLQuery.Append(strEnterpriseId);
						strSQLQuery.Append("' and zone_code = '");
						strSQLQuery.Append(strParameter);
						strSQLQuery.Append("'"); 
					
						dRowsAffected=Convert.ToDecimal(dbCon.ExecuteScalar(strSQLQuery.ToString()));
					}
				}
				else if(ckType.Equals("ServiceType"))
				{
					if(strParameter != null && strParameter != "")
					{
						StringBuilder strSQLQuery=new StringBuilder();

						strSQLQuery.Append(" select count(*) from service ");
						strSQLQuery.Append(" where applicationid = '");
						strSQLQuery.Append(strAppId);
						strSQLQuery.Append("' and enterpriseid = '");
						strSQLQuery.Append(strEnterpriseId);
						strSQLQuery.Append("' and service_code = '");
						strSQLQuery.Append(strParameter);
						strSQLQuery.Append("'"); 
					
						dRowsAffected=Convert.ToDecimal(dbCon.ExecuteScalar(strSQLQuery.ToString()));
					}
				}
				else if(ckType.Equals("ZoneESA"))
				{
					if(strParameter != null && strParameter != "")
					{
						StringBuilder strSQLQuery=new StringBuilder();

						strSQLQuery.Append(" select count(*) from core_system_code where codeid = 'ESA_Sector' ");
						strSQLQuery.Append(" and code_str_value = '");
						strSQLQuery.Append(strParameter);
						strSQLQuery.Append("'");
					
						dRowsAffected=Convert.ToDecimal(dbCon.ExecuteScalar(strSQLQuery.ToString()));
					}
				}
			}

			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","Checked Zone","RBAC003",appException.Message.ToString());				
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			
			if (dRowsAffected > 0)
				return true;
			else
				return false;
		}


		//Add by Panas 13/11/2008
		public static bool ckCustID(string strEntID,string strAppID,string strCustID)
		{
			decimal dRowsAffected=0;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEntID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","InsertBZRates","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				StringBuilder strSQLQuery=new StringBuilder();

				strSQLQuery.Append(" select count(*) from customer ");
				strSQLQuery.Append(" where applicationid = '");
				strSQLQuery.Append(strAppID);
				strSQLQuery.Append("' and enterpriseid = '");
				strSQLQuery.Append(strEntID);
				strSQLQuery.Append("' and custid = '");
				strSQLQuery.Append(strCustID);
				strSQLQuery.Append("'"); 
			
				dRowsAffected=Convert.ToDecimal(dbCon.ExecuteScalar(strSQLQuery.ToString()));	
			}

			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","Checked Zone","RBAC003",appException.Message.ToString());				
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			
			if (dRowsAffected > 0)
				return true;
			else
				return false;
		}

        public static DataSet GetCustomerZonesDS(String strAppID, String strEnterpriseID, string custid)
        {
            DataSet dsBZRates = null;
            DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

            if (dbCon == null)
            {
                Logger.LogTraceError("SysDataManager2", "GetCustomerZonesDS", "EDB101", "DbConnection object is null!!");
                return dsBZRates;
            }

            StringBuilder strSQLQuery = new StringBuilder();

            strSQLQuery.Append(" select distinct zipcode, zone_code");
            strSQLQuery.Append(" from Customer_Zones where applicationid = '" + strAppID + "' and enterpriseid = '" + strEnterpriseID + "'");
            strSQLQuery.Append(" and custid='" + custid + "' and effective_date < GETDATE()");

            dsBZRates = (DataSet)dbCon.ExecuteQuery(strSQLQuery.ToString(), ReturnType.DataSetType);
            return dsBZRates;

        }

    }
}
