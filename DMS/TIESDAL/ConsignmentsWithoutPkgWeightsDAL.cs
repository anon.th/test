using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;
using System.Collections;
using System.Text.RegularExpressions;

namespace TIESDAL
{
	/// <summary>
	/// Summary description for ConsignmentsWithoutPkgWeightsDAL.
	/// </summary>
	public class ConsignmentsWithoutPkgWeightsDAL
	{
		private const string ConfigFormName = "OpsMonitoring1";

		public ConsignmentsWithoutPkgWeightsDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		public static System.Data.DataSet QueryConsignmentsWithoutPkgWeights(string appID, string enterpriseId, string BatchDate, string Location)
		{
			DataSet ds = null;
			IDbCommand dbCmd = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseId);
			if(dbCon == null)
			{
				Logger.LogTraceError("module1","ConsignmentsWithoutPkgWeightsMgrDAL","QueryConsignmentsWithoutPkgWeights","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseId",enterpriseId));
				if(BatchDate ==null)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Created_Date",DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Created_Date",BatchDate));
				}
				if(Location.Trim() !="0")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Location",Location));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Location",DBNull.Value));
				}
				

				dbCmd = dbCon.CreateCommand("QueryOpsMonitoring1",storedParams, CommandType.StoredProcedure);
				ds = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return ds;
		}


		public static string QueryEnterpriseConfigurations(string appID,string enterpriseId,string ConfigName)
		{
			string rDate = string.Empty;
			DataSet ds = null;
			IDbCommand dbCmd = null;
			DataSet dsEnterpriseConfig =null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseId);
			if(dbCon == null)
			{
				Logger.LogTraceError("module1","ConsignmentsWithoutPkgWeightsMgrDAL","QueryEnterpriseConfigurations","ERR002","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
				string strSql = " SELECT * FROM dbo.EnterpriseConfigurations('" + enterpriseId + "', '" + ConfigFormName + "') ";

				dbCmd = dbCon.CreateCommand(strSql,CommandType.Text);
				dsEnterpriseConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);  

				
				foreach (DataRow r in dsEnterpriseConfig.Tables[0].Rows)
				{
					if(r["key"].ToString() == ConfigName)
					{
						rDate = r["value"].ToString();
					}
				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return rDate;
		}

	}
}
