using System;
using System.Collections;
using System.Data;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using System.Text;
using System.Threading;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for InvoiceJobMgrDAL.
	/// </summary>
	public class InvoiceJobMgrDAL
	{

		public InvoiceJobMgrDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static decimal GetInvoiceGenerationTaskJobID(String strAppID, String strEnterpriseID)
		{
			decimal lInvoiceTaskJobID = -1;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceJobMgrDAL","GetInvoiceGenerationTaskJobID","EDB101","DbConnection object is null!!");
				return lInvoiceTaskJobID;
			}

			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("select max(jobid) as jobid from invoice_generation_task");
			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and job_status = 'I'");

			String strSQLQuery = strBuilder.ToString();

			Object objInvoiceTaskJobID = dbCon.ExecuteScalar(strSQLQuery);

			if(Utility.IsNotDBNull(objInvoiceTaskJobID))
			{
				lInvoiceTaskJobID  = System.Convert.ToDecimal(objInvoiceTaskJobID);
			}

			return lInvoiceTaskJobID;			
		}

		public static DataSet GetInvoiceJobStatusDS(DbConnection dbCon, IDbTransaction dbTransaction, IDbConnection con, String strAppID, String strEnterpriseID)
		{

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			StringBuilder strBuilder = new StringBuilder();
			
			switch (dbProvider)
			{
				case DataProviderType.Sql :

					//Form the select query with table lock on Invoice_Generation_Task table and read the job_status 			
					strBuilder.Append("select jobid, start_datetime, end_datetime, job_status, remark from invoice_generation_task with (tablockx) where applicationid = '");
					strBuilder.Append(strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and job_status = 'I'");
					
					break;
				case DataProviderType.Oracle:
					
					//Lock the Table
					IDbCommand dbCmdLock=dbCon.CreateCommand("Lock Table invoice_generation_task in Exclusive mode NOWAIT",CommandType.Text);
					dbCmdLock.Connection=con;
					dbCmdLock.Transaction=dbTransaction;
					dbCon.ExecuteNonQueryInTransaction(dbCmdLock);

					//Form the select query on Invoice_Generation_Task table and read the job_status 			
					strBuilder.Append("select jobid, start_datetime, end_datetime, job_status, remark from invoice_generation_task where applicationid = '");
					strBuilder.Append(strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and job_status = 'I'");
					
					break;
			}

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCommand = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

			dbCommand.Connection = con;
			dbCommand.Transaction = dbTransaction;

			return (DataSet)dbCon.ExecuteQueryInTransaction(dbCommand,ReturnType.DataSetType);
		}

		public static JobStatusInfo AddInvoiceJob(DbConnection dbCon, IDbTransaction dbTransaction, IDbConnection con, String strAppID, String strEnterpriseID)
		{
			JobStatusInfo jobStatusInfo = new JobStatusInfo();
			decimal iJobID = Counter.GetNext(strAppID,strEnterpriseID,"InvoiceGenerationJobID");
			jobStatusInfo.iJobID = Convert.ToDecimal(iJobID);

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into invoice_generation_task (applicationid, enterpriseid, jobid, start_datetime, end_datetime, job_status, remark) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("',");
			strBuilder.Append(iJobID);
			strBuilder.Append(",");
			jobStatusInfo.dtStartDateTime = DateTime.Now;
			String strStartDateTime = Utility.DateFormat(strAppID,strEnterpriseID,DateTime.Now,DTFormat.DateTime);
			strBuilder.Append(strStartDateTime);
			jobStatusInfo.strRemark = "New Job Assigned";
			jobStatusInfo.strJobStatus = "In Progress";
			strBuilder.Append(",null,'I',N'New Job Assigned')");

			String strInsertQuery = strBuilder.ToString();

			IDbCommand dbCommand = dbCon.CreateCommand(strInsertQuery,CommandType.Text);
			dbCommand.Connection = con;
			dbCommand.Transaction = dbTransaction;

			int iRowsInserted = dbCon.ExecuteNonQueryInTransaction(dbCommand);

			//return iJobID;
			return jobStatusInfo;
		}

		public static int InsertInvoice_Generation_Task(String strAppID, String strEnterpriseID,string iJobID, DateTime dtStartDateTime, String strJobStatus, String strRemark)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceJobMgrDAL","UpdateInvoiceJobStatus","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}


			StringBuilder strBuilder = new StringBuilder();			
			strBuilder.Append("INSERT INTO Invoice_Generation_Task (applicationid,enterpriseid,jobid,start_datetime,end_datetime,job_status,remark) ");
			strBuilder.Append(" VALUES('" + Utility.ReplaceSingleQuote(strAppID) + "','" + Utility.ReplaceSingleQuote(strEnterpriseID) + "'," + Utility.ReplaceSingleQuote(iJobID) + ",getdate(),null,'"+ Utility.ReplaceSingleQuote(strJobStatus) + "','"+ Utility.ReplaceSingleQuote(strRemark) +"')");
			
			
			try
			{
				IDbCommand dbCmd = dbCon.CreateCommand(strBuilder.ToString(),CommandType.Text);
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("InvoiceJobMgrDAL","InsertInvoice_Generation_Task","SDM001","status updated into invoice_generation_task table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("InvoiceJobMgrDAL","InsertInvoice_Generation_Task","SDM001","Error During update "+ appException.Message.ToString());
				throw new ApplicationException("Error Insert InvoiceGenerationJob Status",appException);
			}
			return iRowsAffected;
			
		}

		public static int UpdateInvoiceJobStatus(String strAppID, String strEnterpriseID,decimal iJobID, DateTime dtEndDateTime, String strJobStatus, String strRemark)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceJobMgrDAL","UpdateInvoiceJobStatus","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update invoice_generation_task set end_datetime = getdate() ");

			//String strEndDate = Utility.DateFormat(strAppID,strEnterpriseID,dtEndDateTime,DTFormat.DateTime);
			//strBuilder.Append(strEndDate);
			strBuilder.Append(",job_status = '");
			strBuilder.Append(strJobStatus);
			strBuilder.Append("',remark = N'");
			strBuilder.Append(strRemark);
			strBuilder.Append("' where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and jobid = ");
			strBuilder.Append(iJobID);

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);

			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("InvoiceJobMgrDAL","UpdateInvoiceJobStatus","SDM001","status updated into invoice_generation_task table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("InvoiceJobMgrDAL","UpdateInvoiceJobStatus","SDM001","Error During update "+ appException.Message.ToString());
				throw new ApplicationException("Error updating InvoiceGenerationJob Status",appException);
			}
			return iRowsAffected;
			
		}

		delegate void GenerateInvoicesConDelegate(string strAppID, string strEnterpriseID, string userID, string sessionID, string strInvJobID, int autogen);

		public void SyncGenerateInvoicesCon(string strAppID, string strEnterpriseID, string userID, string sessionID, string strInvJobID, int autogen)
		{
			try
			{	
				// Create an instance
				GenerateInvoicesConDelegate dlgt = new GenerateInvoicesConDelegate (this.GenerateInvoicesCon);
				// Call GenerateInvoice using the delegate.
				IAsyncResult ar = dlgt.BeginInvoke(strAppID, strEnterpriseID, userID, sessionID, strInvJobID, autogen, null, null);
			}
			catch(ApplicationException appException)
			{
				throw new ApplicationException(appException.Message, appException);
			}
			
		}

		public void GenerateInvoicesCon(string strAppID, string strEnterpriseID, string userID, string sessionID, string strInvJobID, int autogen)
		{			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid", strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userID", userID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@sessionID", sessionID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@jobid", strInvJobID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@invoiceNumberType", autogen));
			//			int result = (int)dbCon.ExecuteProcedure("GenerateInvoices",storedParams);
			int result = (int)dbCon.ExecuteProcedure("GenerateInvoices_Consignment",storedParams);
			//return result;
		}

		void GenerateInvoices(string strAppID, string strEnterpriseID, string userID, string sessionID, string strInvJobID)
		{			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid", strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userID", userID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@sessionID", sessionID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@jobid", strInvJobID));
//			int result = (int)dbCon.ExecuteProcedure("GenerateInvoices",storedParams);
			int result = (int)dbCon.ExecuteProcedure("GenerateInvoices_Consignment",storedParams);
			//return result;
		}

		delegate void MethodDelegate(string strAppID, string strEnterpriseID, string userID, string sessionID, string strInvJobID);
		
		public void SyncGenerateInvoice(string strAppID, string strEnterpriseID, string userID, string sessionID, string strInvJobID){
			try
			{	
				// Create an instance
				MethodDelegate dlgt = new MethodDelegate (this.GenerateInvoices);
				// Call GenerateInvoice using the delegate.
				IAsyncResult ar = dlgt.BeginInvoke(strAppID, strEnterpriseID, userID, sessionID, strInvJobID, null, null);
			}
			catch(ApplicationException appException)
			{
				throw new ApplicationException(appException.Message, appException);
			}
			
		}

		//Invoice Generation Task Module
		public static int DeleteInvoiceTask(DataSet dsParentGrid, int rowIndex, String strAppID, String strEnterpriseID)
		{
			int iRowsDeleted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsParentGrid.Tables[0].Rows[rowIndex];
			string strZip = (string)dr[0];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataManager2","DeleteInvoiceTask","SDMG001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				if(strZip!=null && strZip!="")
				{
					string strSQLQuery = "delete from zip where ";
					strSQLQuery += "applicationid = "+"'"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and zipcode = '"+strZip+"'"; 
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataManager2","DeleteZipCode","RBAC003",appException.Message.ToString());
				//throw new ApplicationException("Delete operation failed",appException);
				throw new ApplicationException("Delete operation failed"+appException.Message,appException);
			}
			return iRowsDeleted;
		}

		public static SessionDS GetEmptyInvoiceJob()
		{
			DataTable dtTask = new DataTable();
			dtTask.Columns.Add(new DataColumn("jobid", typeof(int)));
			dtTask.Columns.Add(new DataColumn("start_datetime", typeof(DateTime)));
			dtTask.Columns.Add(new DataColumn("end_datetime", typeof(DateTime)));
			dtTask.Columns.Add(new DataColumn("job_status", typeof(string)));
			dtTask.Columns.Add(new DataColumn("remark", typeof(string)));
			dtTask.Columns.Add(new DataColumn("no_of_invoice_generated", typeof(int)));

			for(int i=0; i < 1; i++)
			{
				DataRow dr = dtTask.NewRow();
				//dr["jobid"] = 0;
				dr["job_status"] = "";
				dr["remark"] = "";
				dtTask.Rows.Add(dr);
			}

			DataSet dsTask = new DataSet();
			dsTask.Tables.Add(dtTask);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsTask;
			sessionDS.DataSetRecSize = dsTask.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return  sessionDS;
		}

		public static SessionDS GetInvoiceJobDS(String strAppID, String strEnterpriseID, int iCurRow, int iDSRowSize, DataSet dsQuery)
		{
			SessionDS sessionDS = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];

			string strSQLWhere = "";

			if (Utility.IsNotDBNull(dr[0]))
			{
				int intJobID = Convert.ToInt32(dr[0]);
				if (intJobID > 0)
					strSQLWhere += " and igt.jobid = "+intJobID;
			}
			if (Utility.IsNotDBNull(dr[1]))
			{
				DateTime dtStartDateTime = (DateTime) dr[1];
				String strSStartDT = Utility.DateFormat(strAppID,strEnterpriseID,dtStartDateTime,DTFormat.DateTime);
				String strEStartDT = Utility.DateFormat(strAppID,strEnterpriseID,dtStartDateTime.AddDays(1),DTFormat.DateTime);
				strSQLWhere += " and igt.start_datetime >= "+strSStartDT;
				strSQLWhere += " and igt.start_datetime < "+strEStartDT;
			}
			if (Utility.IsNotDBNull(dr[2]))
			{
				DateTime dtEndDateTime = (DateTime) dr[2];
				String strSEndDT = Utility.DateFormat(strAppID,strEnterpriseID,dtEndDateTime,DTFormat.DateTime);
				String strEEndDT = Utility.DateFormat(strAppID,strEnterpriseID,dtEndDateTime.AddDays(1),DTFormat.DateTime);
				strSQLWhere += " and igt.end_datetime >= "+strSEndDT;
				strSQLWhere += " and igt.end_datetime < "+strEEndDT;
			}

			string strJobStatus = (string) dr[3];
			if((strJobStatus != null) && (strJobStatus != ""))
				strSQLWhere += " and igt.job_status = '"+strJobStatus+"'";

			string strRemark = (string)dr[4];
			if (strRemark!=null && strRemark!="")
			{
				strSQLWhere += " and igt.remark like N'%"+Utility.ReplaceSingleQuote(strRemark)+"%' ";
			}
			
			if (Utility.IsNotDBNull(dr[5]))
			{
				int noOfInvoiceGenerated = Convert.ToInt32(dr[5]);
				strSQLWhere += " and (select count(invoice_no) from dbo.invoice where jobid=igt.jobid) = "+noOfInvoiceGenerated.ToString();
			}

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("InvoiceJobMgrDAL.cs","GetInvoiceJobDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			
			String strSQLQuery = "select igt.jobid, igt.start_datetime, igt.end_datetime, igt.job_status, igt.remark, no_of_invoice_generated=(select count(invoice_no) from dbo.invoice where jobid=igt.jobid) from dbo.invoice_generation_task igt where ";
			strSQLQuery += "igt.applicationid = '"+strAppID+"' and igt.enterpriseid = '"+strEnterpriseID+"'"; 

			if (strSQLWhere!=null && strSQLWhere!="")
			{
				strSQLQuery += strSQLWhere;
			}
			strSQLQuery += " order by igt.start_datetime desc";

			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurRow,iDSRowSize,"invoice_generation_task");
			return  sessionDS;
		}

	}
}
