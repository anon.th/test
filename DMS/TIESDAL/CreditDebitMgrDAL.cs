using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using System.Data.SqlClient;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for CreditDebitMgrDAL.
	/// </summary>
	public class CreditDebitMgrDAL
	{
		public CreditDebitMgrDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static SessionDS GetEmptyCreditNoteDS(int iNumRows)
		{
			DataTable dtDebitNote = new DataTable();
 
			dtDebitNote.Columns.Add(new DataColumn("credit_no", typeof(string)));
			dtDebitNote.Columns.Add(new DataColumn("credit_date", typeof(DateTime)));
			dtDebitNote.Columns.Add(new DataColumn("invoice_no", typeof(string)));
			dtDebitNote.Columns.Add(new DataColumn("Invoice_Date", typeof(DateTime)));						
			dtDebitNote.Columns.Add(new DataColumn("PayerId", typeof(string)));			
			dtDebitNote.Columns.Add(new DataColumn("Payer_name", typeof(string)));							
			dtDebitNote.Columns.Add(new DataColumn("remark", typeof(string)));	
			dtDebitNote.Columns.Add(new DataColumn("Deleted", typeof(string)));
			dtDebitNote.Columns.Add(new DataColumn("status", typeof(string)));
			dtDebitNote.Columns.Add(new DataColumn("Create_By", typeof(string)));
			dtDebitNote.Columns.Add(new DataColumn("Create_Date", typeof(DateTime)));
			dtDebitNote.Columns.Add(new DataColumn("Update_By", typeof(string)));
			dtDebitNote.Columns.Add(new DataColumn("Update_Date", typeof(DateTime)));
			dtDebitNote.Columns.Add(new DataColumn("Status_update_By", typeof(string)));
			dtDebitNote.Columns.Add(new DataColumn("Status_update_Date", typeof(DateTime)));
			dtDebitNote.Columns.Add(new DataColumn("invoice_status", typeof(string)));
			
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtDebitNote.NewRow();
				dtDebitNote.Rows.Add(drEach);
			}

			DataSet dsDN = new DataSet();
			dsDN.Tables.Add(dtDebitNote);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsDN;
			sessionDS.DataSetRecSize = dsDN.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
		}
		public static SessionDS GetEmptyCreditDetailDS(int iNumRows)
		{
			DataTable dtCreditDetail = new DataTable();
 
			dtCreditDetail.Columns.Add(new DataColumn("Credit_no", typeof(string)));			
			dtCreditDetail.Columns.Add(new DataColumn("invoice_no", typeof(string)));
			dtCreditDetail.Columns.Add(new DataColumn("Consignment_no", typeof(string)));
			dtCreditDetail.Columns.Add(new DataColumn("charge_code", typeof(string)));
			dtCreditDetail.Columns.Add(new DataColumn("Invoice_Amt", typeof(decimal)));						
			dtCreditDetail.Columns.Add(new DataColumn("reason_code", typeof(string)));
			dtCreditDetail.Columns.Add(new DataColumn("Amt", typeof(decimal)));						
			dtCreditDetail.Columns.Add(new DataColumn("Description", typeof(string)));			
			dtCreditDetail.Columns.Add(new DataColumn("remark", typeof(string)));	
			
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtCreditDetail.NewRow();
				dtCreditDetail.Rows.Add(drEach);
			}

			DataSet dsCreditDtl = new DataSet();
			dsCreditDtl.Tables.Add(dtCreditDetail);

			//			dsCreditDtl.Tables[0].Columns["Consignment_no"].Unique = true; //Checks duplicate records..
			//			dsCreditDtl.Tables[0].Columns["Consignment_no"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsCreditDtl;
			sessionDS.DataSetRecSize = dsCreditDtl.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;	
		}
		
		public static SessionDS GetInvoiceDetails(String strAppID, String strEnterpriseID,String strInvoiceNo)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","GetCreditNoteDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" Select Invoice_No, Invoice_Date, PayerId, Payer_Name from Invoice where applicationid = '"+strAppID);
			strQry.Append("' and enterpriseid = '"+strEnterpriseID+"' and Invoice_No = '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"'");
		
			sessionDS = dbCon.ExecuteQuery(strQry.ToString(),0,0,"Invoice");			
			return  sessionDS;
		}

		public static SessionDS GetCustomerDetails(String strAppID, String strEnterpriseID,String strCustomerID)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","GetCreditNoteDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" Select custid, cust_name from Customer where applicationid = '"+strAppID);
			strQry.Append("' and enterpriseid = '"+strEnterpriseID+"' and custid = '"+Utility.ReplaceSingleQuote(strCustomerID)+"'");
		
			sessionDS = dbCon.ExecuteQuery(strQry.ToString(),0,0,"Invoice");			
			return  sessionDS;
		}		
		
		/*public static SessionDS GetCreditNoteDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("CreditCreditMgrDAL","GetCreditNoteDS","EDB101","DbConnection object is null!!");
				return sessionDS;
							}

			bool skip = false;
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" Select Credit_No,Credit_Date,I.Invoice_No, Invoice_Date, PayerId, Payer_Name, CN.Remark, CN.Deleted,CN.status,CN.Create_By,CN.Create_Date,CN.Update_By,CN.Update_Date,CN.status_update_by,CN.status_update_date from Credit_Note DN, Invoice I where ");
			strBuilder.Append(" CN.Invoice_No=I.Invoice_No and CN.ApplicationID=I.ApplicationID and CN.EnterpriseID=I.EnterpriseID and CN.ApplicationID = '");
			strBuilder.Append(strAppID+ "' and CN.Enterpriseid = '"+strEnterpriseID+"' ");
			
			DataRow drEach = dsQueryParam.Tables[0].Rows[0];
			if(Utility.IsNotDBNull(drEach["Credit_no"])==true && drEach["Credit_no"].ToString() !="" )
			{
				String strCredit_No = (String) drEach["Credit_no"];
				strBuilder.Append(" and CN.Credit_no like '%"+strCredit_No+"%' ");
				skip = true;
			}
			if(Utility.IsNotDBNull(drEach["Credit_date"])==true && drEach["Credit_date"].ToString() != "")
			{
				DateTime dtCredit_Date = System.Convert.ToDateTime(drEach["Credit_date"]);
				strBuilder.Append(" and Credit_date =");				
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID,dtCredit_Date,DTFormat.DateTime));				
				skip = true;
			}			

			if(Utility.IsNotDBNull(drEach["invoice_no"])== true && drEach["invoice_no"].ToString() != "")
			{
				String strInvoiceNo = (String) drEach["invoice_no"];
				strBuilder.Append(" and I.Invoice_no = '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' ");
			}
			
			if(Utility.IsNotDBNull(drEach["PayerId"])== true && drEach["PayerId"].ToString() != "")
			{
				String strPayerId = (String) drEach["PayerId"];
				strBuilder.Append(" and I.PayerId = '"+Utility.ReplaceSingleQuote(strPayerId)+"' ");
			}
			if(!skip)
			{
				if(Utility.IsNotDBNull(drEach["PayerId"])== true && drEach["PayerId"].ToString() == "" && Utility.IsNotDBNull(drEach["invoice_no"])== true && drEach["invoice_no"].ToString() == "")
				{
					strBuilder.Append(" and I.Invoice_no = '' and I.PayerId = '' ");
				}
			}

			if(Utility.IsNotDBNull(drEach["remark"])== true && drEach["remark"].ToString() != "")
			{
				String strRemark= (String) drEach["remark"];
				strBuilder.Append(" and remark like N'%"+Utility.ReplaceSingleQuote(strRemark)+"%' ");
			}	
			if(Utility.IsNotDBNull(drEach["status"])== true && drEach["status"].ToString() != "")
			{
				String strStatus= (String) drEach["status"];
				strBuilder.Append(" and status = '"+Utility.ReplaceSingleQuote(strStatus)+"' ");
			}
			strBuilder.Append(" Order by Credit_No");	
			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"CreditNoteTable");

			return  sessionDS;	
		}*/

		public static SessionDS GetCreditNoteDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("CreditCreditMgrDAL","GetCreditNoteDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			bool skip = false;
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" Select Credit_No,Credit_Date,I.Invoice_No, Invoice_Date, PayerId, Payer_Name, DN.Remark, DN.Deleted,DN.status,DN.Create_By,DN.Create_Date,DN.Update_By,DN.Update_Date,DN.status_update_by,DN.status_update_date,I.invoice_status from Credit_Note DN, Invoice I where ");
			strBuilder.Append(" DN.Invoice_No=I.Invoice_No and DN.ApplicationID=I.ApplicationID and DN.EnterpriseID=I.EnterpriseID and DN.ApplicationID = '");
			strBuilder.Append(strAppID+ "' and DN.Enterpriseid = '"+strEnterpriseID+"' ");
			
			DataRow drEach = dsQueryParam.Tables[0].Rows[0];
			if(Utility.IsNotDBNull(drEach["Credit_no"])==true && drEach["Credit_no"].ToString() !="" )
			{
				String strCredit_No = (String) drEach["Credit_no"];
				strBuilder.Append(" and DN.Credit_no like '%"+strCredit_No+"%' ");
				skip = true;
			}
			if(Utility.IsNotDBNull(drEach["Credit_date"])==true && drEach["Credit_date"].ToString() != "")
			{
				DateTime dtCredit_Date = System.Convert.ToDateTime(drEach["Credit_date"]);
				DateTime dtCredit_toDate = new DateTime(dtCredit_Date.Year,dtCredit_Date.Month,dtCredit_Date.Day,23,59,59);				
				strBuilder.Append(" and Credit_date between ");				
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID,dtCredit_Date,DTFormat.DateTime));
				strBuilder.Append(" and ");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID,dtCredit_toDate,DTFormat.DateTime));
				/*strBuilder.Append(" and Credit_date =");				
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID,dtCredit_Date,DTFormat.DateTime));*/				
				skip = true;
			}			

			if(Utility.IsNotDBNull(drEach["invoice_no"])== true && drEach["invoice_no"].ToString() != "")
			{
				String strInvoiceNo = (String) drEach["invoice_no"];
				strBuilder.Append(" and I.Invoice_no = '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' ");
			}
			
			if(Utility.IsNotDBNull(drEach["PayerId"])== true && drEach["PayerId"].ToString() != "")
			{
				String strPayerId = (String) drEach["PayerId"];
				strBuilder.Append(" and I.PayerId = '"+Utility.ReplaceSingleQuote(strPayerId)+"' ");
			}
			if(!skip)
			{
				if(Utility.IsNotDBNull(drEach["PayerId"])== true && drEach["PayerId"].ToString() == "" && Utility.IsNotDBNull(drEach["invoice_no"])== true && drEach["invoice_no"].ToString() == "")
				{
					strBuilder.Append(" and I.Invoice_no = '' and I.PayerId = '' ");
				}
			}

			if(Utility.IsNotDBNull(drEach["remark"])== true && drEach["remark"].ToString() != "")
			{
				String strRemark= (String) drEach["remark"];
				strBuilder.Append(" and remark like N'%"+Utility.ReplaceSingleQuote(strRemark)+"%' ");
			}	
			if(Utility.IsNotDBNull(drEach["status"])== true && drEach["status"].ToString() != "")
			{
				String strStatus= (String) drEach["status"];
				strBuilder.Append(" and status = '"+Utility.ReplaceSingleQuote(strStatus)+"' ");
			}
			strBuilder.Append(" Order by Credit_No");	
			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"CreditNoteTable");

			return  sessionDS;	
		}

		public static int AddCreditNoteDS(String strAppID, String strEnterpriseID,DataSet dsToInsert, ref String strCreditNote_No)//new by ching
		{
			int iRowsAffected = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)				
					conApp.Close();
				
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}		
			
			String strInvoiceNo=null;
			//String strConsignmentNo=null;
			//Decimal dDebitAmt=0;
			//String strDebitDesc=null;

			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{				
				//generate a new DebitNote Number and insert into  table		
				String strPrefix = "CND"+DateTime.Now.Year.ToString().Substring(2,2);
				int runnigNo = (int)Counter.GetNext(strAppID,strEnterpriseID,"CreditNote_Number");
				strCreditNote_No = strPrefix+runnigNo.ToString("000000");
				//strCreditNote_No = (int)Counter.GetNext(strAppID,strEnterpriseID,"DebitNote_Number");
			
				StringBuilder strBuilder = new StringBuilder();
				//Insert a Record into Debit_Note
				strBuilder.Append("insert into Credit_Note (ApplicationID,EnterpriseID,Invoice_No,Credit_No,Credit_Date,Remark,Deleted,status,Create_by,Create_date,Update_By,Update_Date) values (");
				strBuilder.Append("'"+strAppID+"','"+strEnterpriseID);

				int iRowCnt = dsToInsert.Tables[0].Rows.Count;
				if (iRowCnt<=0) 
					return iRowsAffected;

				DataRow drEach = dsToInsert.Tables[0].Rows[0];			
			
				strBuilder.Append("',");
				if(Utility.IsNotDBNull(drEach["Invoice_No"]) ==true && drEach["Invoice_No"].ToString() != "")
				{
					strInvoiceNo=Utility.ReplaceSingleQuote(drEach["Invoice_No"].ToString());
					strBuilder.Append("'"+strInvoiceNo+"'");							
				}
				else			
					strBuilder.Append("null");
		
				strBuilder.Append(",");
				if(strCreditNote_No != "" && strCreditNote_No != null)
					strBuilder.Append("'"+strCreditNote_No+"'");				
				else			
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Credit_Date"]) ==true && drEach["Credit_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Credit_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Remark"]) ==true && drEach["Remark"].ToString() != "")
					strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drEach["Remark"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",'N'");		//Deleted is 'N' default when inserting a new record

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["status"]) ==true && drEach["status"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["status"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Create_By"]) ==true && drEach["Create_By"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["Create_By"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Create_Date"]) ==true && drEach["Create_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Create_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Update_By"]) ==true && drEach["Update_By"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["Update_By"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Update_Date"]) ==true && drEach["Update_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Update_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(")");
				String strSQLQuery = strBuilder.ToString();			
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note table");
															
				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0014","App db Transaction committed.");
			}
					
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0006","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0009","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsAffected;
		}

		public static int AddCreditNoteDS(String strAppID, String strEnterpriseID,DataSet dsToInsert, ref String strCreditNote_No,DataSet dsCNDetail)//new by ching
		{
			int iRowsAffected = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)				
					conApp.Close();
				
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}		
			
			String strInvoiceNo=null;
			//String strConsignmentNo=null;
			//Decimal dDebitAmt=0;
			//String strDebitDesc=null;

			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{			
				//generate a new DebitNote Number and insert into  table		
				String strPrefix = "CND"+DateTime.Now.Year.ToString().Substring(2,2);
				int runnigNo = (int)Counter.GetNext(strAppID,strEnterpriseID,"CreditNote_Number");
				strCreditNote_No = strPrefix+runnigNo.ToString("000000");
				//strCreditNote_No = (int)Counter.GetNext(strAppID,strEnterpriseID,"DebitNote_Number");
			
				StringBuilder strBuilder = new StringBuilder();
				//Insert a Record into Debit_Note
				strBuilder.Append("insert into Credit_Note (ApplicationID,EnterpriseID,Invoice_No,Credit_No,Credit_Date,Remark,Deleted,status,Create_by,Create_date,Update_By,Update_Date) values (");
				strBuilder.Append("'"+strAppID+"','"+strEnterpriseID);

				int iRowCnt = dsToInsert.Tables[0].Rows.Count;
				if (iRowCnt<=0) 
					return iRowsAffected;

				DataRow drEach = dsToInsert.Tables[0].Rows[0];			
			
				strBuilder.Append("',");
				if(Utility.IsNotDBNull(drEach["Invoice_No"]) ==true && drEach["Invoice_No"].ToString() != "")
				{
					strInvoiceNo=Utility.ReplaceSingleQuote(drEach["Invoice_No"].ToString());
					strBuilder.Append("'"+strInvoiceNo+"'");							
				}
				else			
					strBuilder.Append("null");
		
				strBuilder.Append(",");
				if(strCreditNote_No != "" && strCreditNote_No != null)
					strBuilder.Append("'"+strCreditNote_No+"'");				
				else			
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Credit_Date"]) ==true && drEach["Credit_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Credit_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Remark"]) ==true && drEach["Remark"].ToString() != "")
					strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drEach["Remark"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",'N'");		//Deleted is 'N' default when inserting a new record

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["status"]) ==true && drEach["status"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["status"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Create_By"]) ==true && drEach["Create_By"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["Create_By"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Create_Date"]) ==true && drEach["Create_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Create_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Update_By"]) ==true && drEach["Update_By"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["Update_By"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Update_Date"]) ==true && drEach["Update_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Update_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(")");
				String strSQLQuery = strBuilder.ToString();			
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;				
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note table");
				Decimal dCreditAmt=0;		
				Decimal dOLDCreditAmt=0;
				//delete creditnote detail
				String strSQL=" Delete From Credit_Note_Detail where ApplicationID='"+strAppID+"'";
				strSQL+=" and EnterpriseID='"+strEnterpriseID+"'";
				strSQL+=" and Credit_No='"+ strCreditNote_No +"' ";					
				dbCommandApp = dbConApp.CreateCommand(strSQL,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;				
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0012",iRowsAffected + " rows Deleted from Debit_Note_Detail table");

				for (int i=0;i<dsCNDetail.Tables[0].Rows.Count;i++)
				{
					DataRow drDND=dsCNDetail.Tables[0].Rows[i];	

					strBuilder.Remove(0,strBuilder.Length);	
				
					strBuilder = new StringBuilder();			
					strBuilder.Append(" Insert into Credit_Note_Detail(ApplicationID, EnterpriseID, Invoice_No, ");
					strBuilder.Append(" Credit_No, Consignment_No, Credit_Amt, Credit_Description,charge_code,reason_code,remark) Values(");
					strBuilder.Append("'"+ strAppID+"','"+strEnterpriseID+"','"+strInvoiceNo+"','"+strCreditNote_No+"',");

					if(Utility.IsNotDBNull(drDND["Consignment_No"]) ==true && drDND["Consignment_No"].ToString() != "")
					{
						String strConsignmentNo=Utility.ReplaceSingleQuote(drDND["Consignment_No"].ToString());						
						strBuilder.Append("'"+strConsignmentNo+"'");						
					}
					
					strBuilder.Append(",");
					if(Utility.IsNotDBNull(drDND["Amt"]) ==true && drDND["Amt"].ToString() != "")
					{
						dCreditAmt=(Decimal) drDND["Amt"];						
						if (drDND.HasVersion(DataRowVersion.Original))
						{					
							if (drDND["Amt", DataRowVersion.Original].ToString() !="")
								dOLDCreditAmt=(Decimal) drDND["Amt", DataRowVersion.Original];
						}	
						strBuilder.Append(dCreditAmt);
					}
					else
					{
						strBuilder.Append("null");
						dCreditAmt=0;
					}

					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["Description"]) ==true && drDND["Description"].ToString() != "")
					{
						String strCreditDesc=(String) drDND["Description"];
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(strCreditDesc) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
		
					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["charge_code"]) ==true && drDND["charge_code"].ToString() != "")
					{						
						strBuilder.Append("'"+Utility.ReplaceSingleQuote(drDND["charge_code"].ToString()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	

					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["reason_code"]) ==true && drDND["reason_code"].ToString().Trim() != "")
					{						
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drDND["reason_code"].ToString().Trim()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["remark"]) ==true && drDND["remark"].ToString().Trim() != "")
					{						
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drDND["remark"].ToString().Trim()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
					strBuilder.Append(")");	
									

					

					//NOW INSERT Debit_Note_Detail Information
					strSQLQuery = strBuilder.ToString();			
					dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;					
					iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note_Detail table");
					//dTotaCreditAmnt=0;															
				}					
				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0014","App db Transaction committed.");
			}
					
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0006","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0009","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsAffected;
		}
		/*public static int AddCreditNoteDS(String strAppID, String strEnterpriseID,DataSet dsToInsert, ref String strCreditNote_No, DataSet dsCNDetail)
		{
			int iRowsAffected = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)				
					conApp.Close();
				
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}		
			
			String strInvoiceNo=null;
			String strConsignmentNo=null;
			Decimal dDebitAmt=0;
			String strDebitDesc=null;

			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{
				//generate a new CreditNote Number and insert into  table		
				String strPrefix = "DN"+DateTime.Now.Year.ToString().Substring(2,2);
				int runnigNo = (int)Counter.GetNext(strAppID,strEnterpriseID,"CreditNote_Number");
				strCreditNote_No = strPrefix+runnigNo.ToString("000000");
				//strCreditNote_No = (int)Counter.GetNext(strAppID,strEnterpriseID,"CreditNote_Number");
			
				StringBuilder strBuilder = new StringBuilder();
				//Insert a Record into Credit_Note
				strBuilder.Append("insert into Credit_Note (ApplicationID,EnterpriseID,Invoice_No,Credit_No,Credit_Date,Remark,Deleted,status,Create_by,Create_date,Update_By,Update_Date) values (");
				strBuilder.Append("'"+strAppID+"','"+strEnterpriseID);

				int iRowCnt = dsToInsert.Tables[0].Rows.Count;
				if (iRowCnt<=0) 
					return iRowsAffected;

				DataRow drEach = dsToInsert.Tables[0].Rows[0];			
			
				strBuilder.Append("',");
				if(Utility.IsNotDBNull(drEach["Invoice_No"]) ==true && drEach["Invoice_No"].ToString() != "")
				{
					strInvoiceNo=Utility.ReplaceSingleQuote(drEach["Invoice_No"].ToString());
					strBuilder.Append("'"+strInvoiceNo+"'");							
				}
				else			
					strBuilder.Append("null");
		
				strBuilder.Append(",");
				if(strCreditNote_No != "" && strCreditNote_No != null)
					strBuilder.Append("'"+strCreditNote_No+"'");				
				else			
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Credit_Date"]) ==true && drEach["Credit_Date"].ToString() != "")
				{	
					DateTime dtCredit_Date=System.Convert.ToDateTime(drEach["Credit_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCredit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Remark"]) ==true && drEach["Remark"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["Remark"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",'N'");		//Deleted is 'N' default when inserting a new record

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["status"]) ==true && drEach["status"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["status"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Create_By"]) ==true && drEach["Create_By"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["Create_By"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Create_Date"]) ==true && drEach["Create_Date"].ToString() != "")
				{	
					DateTime dtCredit_Date=System.Convert.ToDateTime(drEach["Create_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCredit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Update_By"]) ==true && drEach["Update_By"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["Update_By"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Update_Date"]) ==true && drEach["Update_Date"].ToString() != "")
				{	
					DateTime dtCredit_Date=System.Convert.ToDateTime(drEach["Update_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCredit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(")");
				String strSQLQuery = strBuilder.ToString();			
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0012",iRowsAffected + " rows inserted into Credit_Note table");
											
				for (int i=0;i<dsCNDetail.Tables[0].Rows.Count;i++)
				{
					DataRow drCND=dsCNDetail.Tables[0].Rows[i];
					
					strBuilder.Remove(0,strBuilder.Length);
					//Insert Records into Credit_Note_Detail
					strBuilder.Append("insert into Credit_Note_Detail (applicationid,enterpriseid,Invoice_No, Credit_No,");
					strBuilder.Append("Consignment_No, Credit_Amt, Credit_Description) values ('"+strAppID+"','");
					strBuilder.Append(strEnterpriseID+"','"+strInvoiceNo+"','"+strCreditNote_No+"'");			
				
					strBuilder.Append(",");
					if(Utility.IsNotDBNull(drCND["Consignment_No"]) ==true && drCND["Consignment_No"].ToString() != "")
					{
						strConsignmentNo=Utility.ReplaceSingleQuote(drCND["Consignment_No"].ToString());
						strBuilder.Append("'"+strConsignmentNo+"'");			
					}
					else
						strBuilder.Append("null");

					strBuilder.Append(",");
					if(Utility.IsNotDBNull(drCND["Amt"]) ==true && drCND["Amt"].ToString() != "")
					{	
						dDebitAmt=(Decimal) drCND["Amt"];
						strBuilder.Append(dDebitAmt);
					}
					else
						strBuilder.Append("null");

					strBuilder.Append(",");
					if(Utility.IsNotDBNull(drCND["Description"]) ==true && drCND["Description"].ToString() != "")					
					{
						strDebitDesc=(String) drCND["Description"];
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(strDebitDesc)+"'");				
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append (")");

					strSQLQuery = strBuilder.ToString();			
					dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0012",iRowsAffected + " rows inserted into Credit_Note_Detail table");
				
					// Update Shipment table
					strBuilder.Remove(0,strBuilder.Length);
					strBuilder.Append("Update shipment set Credit_Amt= Credit_Amt + "+dDebitAmt+" where Consignment_No='"+strConsignmentNo+"' ");
					strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");
															
					strSQLQuery = strBuilder.ToString();			
					dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
				}
				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0014","App db Transaction committed.");
			}
					
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0006","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0009","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsAffected;
		}

		public static int AddCreditNoteDS(String strAppID, String strEnterpriseID,DataSet dsToInsert, ref String strCreditNote_No)
		{
			int iRowsAffected = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)				
					conApp.Close();
				
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}		
			
			String strInvoiceNo=null;
			String strConsignmentNo=null;
			Decimal dDebitAmt=0;
			String strDebitDesc=null;

			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{
				//generate a new CreditNote Number and insert into  table		
				String strPrefix = "DND"+DateTime.Now.Year.ToString().Substring(2,2);
				int runnigNo = (int)Counter.GetNext(strAppID,strEnterpriseID,"CreditNote_Number");
				strCreditNote_No = strPrefix+runnigNo.ToString("000000");
				//strCreditNote_No = (int)Counter.GetNext(strAppID,strEnterpriseID,"CreditNote_Number");
			
				StringBuilder strBuilder = new StringBuilder();
				//Insert a Record into Credit_Note
				strBuilder.Append("insert into Credit_Note (ApplicationID,EnterpriseID,Invoice_No,Credit_No,Credit_Date,Remark,Deleted,status,Create_by,Create_date,Update_By,Update_Date) values (");
				strBuilder.Append("'"+strAppID+"','"+strEnterpriseID);

				int iRowCnt = dsToInsert.Tables[0].Rows.Count;
				if (iRowCnt<=0) 
					return iRowsAffected;

				DataRow drEach = dsToInsert.Tables[0].Rows[0];			
			
				strBuilder.Append("',");
				if(Utility.IsNotDBNull(drEach["Invoice_No"]) ==true && drEach["Invoice_No"].ToString() != "")
				{
					strInvoiceNo=Utility.ReplaceSingleQuote(drEach["Invoice_No"].ToString());
					strBuilder.Append("'"+strInvoiceNo+"'");							
				}
				else			
					strBuilder.Append("null");
		
				strBuilder.Append(",");
				if(strCreditNote_No != "" && strCreditNote_No != null)
					strBuilder.Append("'"+strCreditNote_No+"'");				
				else			
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Credit_Date"]) ==true && drEach["Credit_Date"].ToString() != "")
				{	
					DateTime dtCredit_Date=System.Convert.ToDateTime(drEach["Credit_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCredit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Remark"]) ==true && drEach["Remark"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["Remark"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",'N'");		//Deleted is 'N' default when inserting a new record

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["status"]) ==true && drEach["status"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["status"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Create_By"]) ==true && drEach["Create_By"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["Create_By"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Create_Date"]) ==true && drEach["Create_Date"].ToString() != "")
				{	
					DateTime dtCredit_Date=System.Convert.ToDateTime(drEach["Create_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCredit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Update_By"]) ==true && drEach["Update_By"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["Update_By"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Update_Date"]) ==true && drEach["Update_Date"].ToString() != "")
				{	
					DateTime dtCredit_Date=System.Convert.ToDateTime(drEach["Update_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCredit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(")");
				String strSQLQuery = strBuilder.ToString();			
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0012",iRowsAffected + " rows inserted into Credit_Note table");

				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0014","App db Transaction committed.");
					}
					
			catch(ApplicationException appException)
					{
				try
						{					
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0006","App db  transaction rolled back.");
					}
				catch(Exception rollbackException)
					{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
					}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
					}
			catch(Exception exception)
					{
				try
					{						
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0009","App db  transaction rolled back.");
					}
				catch(Exception rollbackException)
					{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditNoteDS","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsAffected;
		}*/
		/*public static int ModifyCreditNoteDS(String strAppID, String strEnterpriseID,DataSet dsToModify, DataSet dsCNDetail)
		{
			int iRowsAffected = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)				
					conApp.Close();
				
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}		
			
			String strInvoiceNo=null;
			String strConsignmentNo=null;
			String strCreditNo=null;
			Decimal dCreditAmt=0;
			Decimal dOLDCreditAmt=0;
			Decimal dActCreditAmt=0;
			String strCreditDesc=null;

			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			
			try
			{
				int iRowCnt = dsToModify.Tables[0].Rows.Count;
				if (iRowCnt<=0) 
					return iRowsAffected;

				StringBuilder strBuilder = new StringBuilder();

				DataRow drEach = dsToModify.Tables[0].Rows[0];				
				if(Utility.IsNotDBNull(drEach["Credit_No"]) ==true && drEach["Credit_No"].ToString() != "")
					strCreditNo=(String) drEach["Credit_No"];
				if(Utility.IsNotDBNull(drEach["Invoice_No"]) ==true && drEach["Invoice_No"].ToString() != "")
					strInvoiceNo=Utility.ReplaceSingleQuote(drEach["Invoice_No"].ToString());

				//Update Credit_Note Information
				strBuilder.Append("Update Credit_Note set Credit_Date =");				
				if(Utility.IsNotDBNull(drEach["Credit_Date"]) ==true && drEach["Credit_Date"].ToString() != "")
				{				
					DateTime dtCredit_Date=System.Convert.ToDateTime(drEach["Credit_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCredit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", remark = ");
				if(Utility.IsNotDBNull(drEach["remark"]) ==true && drEach["remark"].ToString() != "")
				{	
					strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drEach["remark"].ToString())+"'");				
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(" where ApplicationID = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID);
				strBuilder.Append("' and Invoice_No = '"+strInvoiceNo);
				strBuilder.Append("' and Credit_No='"+drEach["Credit_No"].ToString()+"'");

				String strSQLQuery = strBuilder.ToString();			
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows inserted into Credit_Note table");
						

				for (int i=0;i<dsCNDetail.Tables[0].Rows.Count;i++)
				{
					DataRow drCND=dsCNDetail.Tables[0].Rows[i];	

					strBuilder.Remove(0,strBuilder.Length);	
				
					strBuilder = new StringBuilder();			
					strBuilder.Append(" Insert into Credit_Note_Detail(ApplicationID, EnterpriseID, Invoice_No, ");
					strBuilder.Append(" Credit_No, Consignment_No, Credit_Amt, Credit_Description) Values(");
					strBuilder.Append("'"+ strAppID+"','"+strEnterpriseID+"','"+strInvoiceNo+"','"+strCreditNo+"',");

					if(Utility.IsNotDBNull(drCND["Consignment_No"]) ==true && drCND["Consignment_No"].ToString() != "")
					{
						strConsignmentNo=(String) drCND["Consignment_No"];						
						strBuilder.Append("'"+strConsignmentNo+"'");						
					}
					
					strBuilder.Append(",");
					if(Utility.IsNotDBNull(drCND["Amt"]) ==true && drCND["Amt"].ToString() != "")
					{
						dCreditAmt=(Decimal) drCND["Amt"];
						if (drCND.HasVersion(DataRowVersion.Original))
						{					
							if (drCND["Amt", DataRowVersion.Original].ToString() !="")
								dOLDCreditAmt=(Decimal) drCND["Amt", DataRowVersion.Original];
						}	
						strBuilder.Append(dCreditAmt);
					}
					else
					{
						strBuilder.Append("null");
						dCreditAmt=0;
					}

					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drCND["Description"]) ==true && drCND["Description"].ToString() != "")
					{
						strCreditDesc=(String) drCND["Description"];
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(strCreditDesc) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
					strBuilder.Append(")");	
									

					//DELETE the Credit_Note_Detail ONLY for the CURRENT Consignment
					String strSQL=" Delete From Credit_Note_Detail where ApplicationID='"+strAppID+"'";
					strSQL+=" and EnterpriseID='"+strEnterpriseID+"' and Invoice_no='"+strInvoiceNo+"'";
					strSQL+=" and Credit_No='"+ strCreditNo +"' and Consignment_No='"+ strConsignmentNo +"'";					
					dbCommandApp = dbConApp.CreateCommand(strSQL,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows Deleted from Credit_Note_Detail table");
					
					//NOW INSERT Credit_Note_Detail Information
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows inserted into Credit_Note_Detail table");

							// Update Shipment Table
							strBuilder.Remove(0,strBuilder.Length);
					dActCreditAmt=(dCreditAmt-dOLDCreditAmt);
					strBuilder.Append("Update shipment set Credit_Amt=Credit_Amt + "+dActCreditAmt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

						}
				
				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0014","App db Transaction committed.");
			}					
					
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0006","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0009","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}			
			
			return iRowsAffected;
		}*/

		public static int ModifyCreditNoteDS(String strAppID, String strEnterpriseID,DataSet dsToModify, DataSet dsCNDetail)// new by ching
		{
			int iRowsAffected = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)				
					conApp.Close();
				
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}		
			
			String strInvoiceNo=null;
			String strConsignmentNo=null;
			String strCreditNo=null;
			Decimal dCreditAmt=0;
			Decimal dOLDCreditAmt=0;
			Decimal dActCreditAmt=0;
			Decimal dTotaCreditAmnt=0;
			String strCreditDesc=null;
			String strOldStat="";
			String strStat="";	
			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			
			try
			{
				int iRowCnt = dsToModify.Tables[0].Rows.Count;
				if (iRowCnt<=0) 
					return iRowsAffected;

				StringBuilder strBuilder = new StringBuilder();
				StringBuilder strSumBuilder = new StringBuilder();

				DataRow drEach = dsToModify.Tables[0].Rows[0];				
				if(Utility.IsNotDBNull(drEach["Credit_No"]) ==true && drEach["Credit_No"].ToString() != "")
					strCreditNo=(String) drEach["Credit_No"];
				if(Utility.IsNotDBNull(drEach["Invoice_No"]) ==true && drEach["Invoice_No"].ToString() != "")
					strInvoiceNo=Utility.ReplaceSingleQuote(drEach["Invoice_No"].ToString());

				//Update Debit_Note Information
				strBuilder.Append("Update Credit_Note set Credit_Date =");				
				if(Utility.IsNotDBNull(drEach["Credit_Date"]) ==true && drEach["Credit_Date"].ToString() != "")
				{	
					DateTime dtCredit_Date=System.Convert.ToDateTime(drEach["Credit_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCredit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", remark = ");
				if(Utility.IsNotDBNull(drEach["remark"]) ==true && drEach["remark"].ToString() != "")
				{				
					strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drEach["remark"].ToString())+"'");				
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status = ");
				if(Utility.IsNotDBNull(drEach["status"]) ==true && drEach["status"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["status"].ToString())+"'");
					strStat = Utility.ReplaceSingleQuote(drEach["status"].ToString());
					strOldStat = Utility.ReplaceSingleQuote(drEach["status",System.Data.DataRowVersion.Original].ToString());
				}
				else
				{
					strStat="";
					strOldStat="";
					strBuilder.Append("null");
				}

				strBuilder.Append(", update_by = ");
				if(Utility.IsNotDBNull(drEach["Update_By"]) ==true && drEach["Update_By"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["Update_By"].ToString())+"'");				
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", update_date = ");
				if(Utility.IsNotDBNull(drEach["Update_Date"]) ==true && drEach["Update_Date"].ToString() != "")
				{	
					DateTime dtCredit_Date=System.Convert.ToDateTime(drEach["Update_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCredit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status_update_by = ");
				if(Utility.IsNotDBNull(drEach["Status_Update_By"]) ==true && drEach["Status_Update_By"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["Status_Update_By"].ToString())+"'");	
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status_update_date = ");
				if(Utility.IsNotDBNull(drEach["Status_Update_Date"]) ==true && drEach["Status_Update_Date"].ToString() != "")
				{	
					DateTime dtCredit_Date=System.Convert.ToDateTime(drEach["Status_Update_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCredit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(" where ApplicationID = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID);
				strBuilder.Append("' and Invoice_No = '"+strInvoiceNo);
				strBuilder.Append("' and Credit_No='"+drEach["Credit_No"].ToString()+"'");

				String strSQLQuery = strBuilder.ToString();			
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note table");

				//delete creditnote detail
				String strSQL=" Delete From Credit_Note_Detail where ApplicationID='"+strAppID+"'";
				strSQL+=" and EnterpriseID='"+strEnterpriseID+"'";
				strSQL+=" and Credit_No='"+ strCreditNo +"' ";					
				dbCommandApp = dbConApp.CreateCommand(strSQL,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows Deleted from Debit_Note_Detail table");

				for (int i=0;i<dsCNDetail.Tables[0].Rows.Count;i++)
				{
					DataRow drDND=dsCNDetail.Tables[0].Rows[i];	

					strBuilder.Remove(0,strBuilder.Length);	
				
					strBuilder = new StringBuilder();			
					strBuilder.Append(" Insert into Credit_Note_Detail(ApplicationID, EnterpriseID, Invoice_No, ");
					strBuilder.Append(" Credit_No, Consignment_No, Credit_Amt, Credit_Description,charge_code,reason_code,remark) Values(");
					strBuilder.Append("'"+ strAppID+"','"+strEnterpriseID+"','"+strInvoiceNo+"','"+strCreditNo+"',");

					if(Utility.IsNotDBNull(drDND["Consignment_No"]) ==true && drDND["Consignment_No"].ToString() != "")
					{
						strConsignmentNo=Utility.ReplaceSingleQuote(drDND["Consignment_No"].ToString());						
						strBuilder.Append("'"+strConsignmentNo+"'");						
					}
					
					strBuilder.Append(",");
					if(Utility.IsNotDBNull(drDND["Amt"]) ==true && drDND["Amt"].ToString() != "")
					{
						dCreditAmt=(Decimal) drDND["Amt"];						
						if (drDND.HasVersion(DataRowVersion.Original))
						{					
							if (drDND["Amt", DataRowVersion.Original].ToString() !="")
								dOLDCreditAmt=(Decimal) drDND["Amt", DataRowVersion.Original];
						}	
						strBuilder.Append(dCreditAmt);
					}
					else
					{
						strBuilder.Append("null");
						dCreditAmt=0;
					}

					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["Description"]) ==true && drDND["Description"].ToString() != "")
					{
						strCreditDesc=(String) drDND["Description"];
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(strCreditDesc) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
		
					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["charge_code"]) ==true && drDND["charge_code"].ToString() != "")
					{						
						strBuilder.Append("'"+Utility.ReplaceSingleQuote(drDND["charge_code"].ToString()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	

					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["reason_code"]) ==true && drDND["reason_code"].ToString().Trim() != "")
					{						
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drDND["reason_code"].ToString().Trim()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	

					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["remark"]) ==true && drDND["remark"].ToString().Trim() != "")
					{						
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drDND["remark"].ToString().Trim()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
					strBuilder.Append(")");	
									

					//DELETE the Debit_Note_Detail ONLY for the CURRENT Consignment
					/*String strSQL=" Delete From Debit_Note_Detail where ApplicationID='"+strAppID+"'";
					strSQL+=" and EnterpriseID='"+strEnterpriseID+"' and Invoice_no='"+strInvoiceNo+"'";
					strSQL+=" and Credit_No='"+ strCreditNo +"' and Consignment_No='"+ strConsignmentNo +"'";					
					dbCommandApp = dbConApp.CreateCommand(strSQL,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows Deleted from Debit_Note_Detail table");*/
					
					

					//NOW INSERT Debit_Note_Detail Information
					strSQLQuery = strBuilder.ToString();			
					dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note_Detail table");
					dTotaCreditAmnt=0;
					if(strStat.ToUpper() == "A")
					{
						//calculate total debit amnt
						strSumBuilder.Remove(0,strSumBuilder.Length);
						strSumBuilder.Append(" select  isnull(sum(isnull(Credit_Amt,0)),0) as Credit_Amt from credit_note_detail dt with(nolock) ");
						strSumBuilder.Append(" inner join credit_note dn with(nolock) on dt.Credit_No = dn.Credit_No and dt.applicationid = dn.applicationid and dt.enterpriseid = dn.enterpriseid ");
						strSumBuilder.Append(" where dt.Consignment_No='"+strConsignmentNo+"' and dt.applicationid = '"+strAppID+"' and dt.enterpriseid = '"+strEnterpriseID+"' and dn.status='A' ");
						dbCommandApp = dbConApp.CreateCommand(strSumBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsTotal = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(Utility.IsNotDBNull(dsTotal.Tables[0].Rows[0])&&dsTotal.Tables[0].Rows.Count>0)
						{
							try
							{
								dTotaCreditAmnt = (Decimal)dsTotal.Tables[0].Rows[0]["Credit_Amt"];
							}
							catch
							{
								dTotaCreditAmnt=0;
							}
						}
						// Update Shipment Table
						if(strOldStat.ToUpper()=="A")
						{
							strBuilder.Remove(0,strBuilder.Length);
							dActCreditAmt=(dCreditAmt-dOLDCreditAmt);
							strBuilder.Append("Update shipment set Credit_Amt= "+dTotaCreditAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						else if(strOldStat.ToUpper()=="N")
						{
							strBuilder.Remove(0,strBuilder.Length);
							dActCreditAmt=(dCreditAmt);
							strBuilder.Append("Update shipment set Credit_Amt="+dActCreditAmt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						strBuilder.Remove(0,strBuilder.Length);
						strBuilder.Append("select isnull(inv.invoice_amt,0)+isnull(s.debit_Amt,0)-isnull(s.credit_amt,0) total_cons_revenue from invoice_detail inv with(nolock) inner join shipment s with(nolock) on inv.invoice_no=s.invoice_no and inv.Consignment_No=s.Consignment_No ");
						strBuilder.Append("where s.Consignment_No='"+strConsignmentNo+"' and s.invoice_no = '"+strInvoiceNo+"' ");
						strBuilder.Append(" and s.applicationid = '"+strAppID+"' and s.enterpriseid = '"+strEnterpriseID+"'");
						dbCommandApp = dbConApp.CreateCommand(strBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsResult = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(dsResult.Tables[0].Rows.Count>0)
							if(Utility.IsNotDBNull(dsResult.Tables[0].Rows[0]))
							{
								Decimal dRevenue = (Decimal)dsResult.Tables[0].Rows[0]["total_cons_revenue"];
								strBuilder.Remove(0,strBuilder.Length);
								strBuilder.Append("Update shipment set total_cons_revenue = "+dRevenue+" where Consignment_No='"+strConsignmentNo+"' ");
								strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
								strSQLQuery = strBuilder.ToString();			
								dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
								dbCommandApp.Connection = conApp;
								dbCommandApp.Transaction = transactionApp;
								iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
								Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

							}
					}
					else if (strStat.ToUpper() == "C")
					{
						if(strOldStat.ToUpper()=="A")
						{
							//calculate total debit amnt
							strSumBuilder.Remove(0,strSumBuilder.Length);
							strSumBuilder.Append(" select  isnull(sum(isnull(Credit_Amt,0)),0) as Credit_Amt from debit_note_detail dt with(nolock) ");
							strSumBuilder.Append(" inner join debit_note dn with(nolock) on dt.Credit_No = dn.Credit_No and dt.applicationid = dn.applicationid and dt.enterpriseid = dn.enterpriseid ");
							strSumBuilder.Append(" where dt.Consignment_No='"+strConsignmentNo+"' and dt.applicationid = '"+strAppID+"' and dt.enterpriseid = '"+strEnterpriseID+"' and dn.status='A' ");
							dbCommandApp = dbConApp.CreateCommand(strSumBuilder.ToString(),CommandType.Text);
							dbCommandApp.Connection = conApp;
							DataSet dsTotal = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
							if(Utility.IsNotDBNull(dsTotal.Tables[0].Rows[0])&&dsTotal.Tables[0].Rows.Count>0)
							{
								try
								{
									dTotaCreditAmnt = (Decimal)dsTotal.Tables[0].Rows[0]["Credit_Amt"];
								}
								catch
								{
									dTotaCreditAmnt=0;
								}
							}

							// Update Shipment Table
							strBuilder.Remove(0,strBuilder.Length);
							//dActCreditAmt=(dCreditAmt-dOLDCreditAmt);
							strBuilder.Append("Update shipment set Credit_Amt="+dTotaCreditAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						strBuilder.Remove(0,strBuilder.Length);
						strBuilder.Append("select isnull(inv.invoice_amt,0)+isnull(s.debit_Amt,0)-isnull(s.credit_amt,0) total_cons_revenue from invoice_detail inv with(nolock) inner join shipment s with(nolock) on inv.invoice_no=s.invoice_no and inv.Consignment_No=s.Consignment_No ");
						strBuilder.Append("where s.Consignment_No='"+strConsignmentNo+"' and s.invoice_no = '"+strInvoiceNo+"' ");
						strBuilder.Append(" and s.applicationid = '"+strAppID+"' and s.enterpriseid = '"+strEnterpriseID+"'");
						dbCommandApp = dbConApp.CreateCommand(strBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsResult = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(dsResult.Tables[0].Rows.Count>0)
							if(Utility.IsNotDBNull(dsResult.Tables[0].Rows[0]))
							{
								Decimal dRevenue = (Decimal)dsResult.Tables[0].Rows[0]["total_cons_revenue"];
								strBuilder.Remove(0,strBuilder.Length);
								strBuilder.Append("Update shipment set total_cons_revenue = "+dRevenue+" where Consignment_No='"+strConsignmentNo+"' ");
								strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
								strSQLQuery = strBuilder.ToString();			
								dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
								dbCommandApp.Connection = conApp;
								dbCommandApp.Transaction = transactionApp;
								iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
								Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

							}
					}
										
				}
				
				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0014","App db Transaction committed.");
			}					
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0006","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0009","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}			
			return iRowsAffected;
		}

		public static int ModifyCreditNoteDS(String strAppID, String strEnterpriseID,DataSet dsToModify, DataSet dsCNDetail , int PageIndex)// new by Aoo
		{
			int iRowsAffected = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)				
					conApp.Close();
				
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}		
			
			String strInvoiceNo=null;
			String strConsignmentNo=null;
			String strCreditNo=null;
			Decimal dCreditAmt=0;
			Decimal dOLDCreditAmt=0;
			Decimal dActCreditAmt=0;
			Decimal dTotaCreditAmnt=0;
			String strCreditDesc=null;
			String strOldStat="";
			String strStat="";
			String strRemark=null;
			int bookingNo=0;
			String statusUpdateBy = null;
			String strTmpCons="";
			//Begin App Transaction
			
			try
			{
				transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
				if(transactionApp == null)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0005","transactionApp is null");
					throw new ApplicationException("Failed Begining a App Transaction..",null);
				}
				int iRowCnt = dsToModify.Tables[0].Rows.Count;
				if (iRowCnt<=0) 
					return iRowsAffected;

				StringBuilder strBuilder = new StringBuilder();
				StringBuilder strSumBuilder = new StringBuilder();

				DataRow drEach = dsToModify.Tables[0].Rows[PageIndex];				
				if(Utility.IsNotDBNull(drEach["Credit_No"]) ==true && drEach["Credit_No"].ToString() != "")
					strCreditNo=(String) drEach["Credit_No"];
				if(Utility.IsNotDBNull(drEach["Invoice_No"]) ==true && drEach["Invoice_No"].ToString() != "")
					strInvoiceNo=Utility.ReplaceSingleQuote(drEach["Invoice_No"].ToString());

				//Update Debit_Note Information
				strBuilder.Append("Update Credit_Note set Credit_Date =");				
				if(Utility.IsNotDBNull(drEach["Credit_Date"]) ==true && drEach["Credit_Date"].ToString() != "")
				{	
					DateTime dtCredit_Date=System.Convert.ToDateTime(drEach["Credit_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCredit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(", remark = ");
				if(Utility.IsNotDBNull(drEach["remark"]) ==true && drEach["remark"].ToString() != "")
				{				
					strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drEach["remark"].ToString())+"'");
					strRemark = Utility.ReplaceSingleQuote(drEach["remark"].ToString());
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status = ");
				if(Utility.IsNotDBNull(drEach["status"]) ==true && drEach["status"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["status"].ToString())+"'");
					strStat = Utility.ReplaceSingleQuote(drEach["status"].ToString());
					strOldStat = Utility.ReplaceSingleQuote(drEach["status",System.Data.DataRowVersion.Original].ToString());
				}
				else
				{
					strStat="";
					strOldStat="";
					strBuilder.Append("null");
				}

				strBuilder.Append(", update_by = ");
				if(Utility.IsNotDBNull(drEach["Update_By"]) ==true && drEach["Update_By"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["Update_By"].ToString())+"'");				
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", update_date = ");
				if(Utility.IsNotDBNull(drEach["Update_Date"]) ==true && drEach["Update_Date"].ToString() != "")
				{	
					DateTime dtCredit_Date=System.Convert.ToDateTime(drEach["Update_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCredit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status_update_by = ");
				if(Utility.IsNotDBNull(drEach["Status_Update_By"]) ==true && drEach["Status_Update_By"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["Status_Update_By"].ToString())+"'");	
					statusUpdateBy = Utility.ReplaceSingleQuote(drEach["Status_Update_By"].ToString());
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status_update_date = ");
				if(Utility.IsNotDBNull(drEach["Status_Update_Date"]) ==true && drEach["Status_Update_Date"].ToString() != "")
				{	
					DateTime dtCredit_Date=System.Convert.ToDateTime(drEach["Status_Update_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCredit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(" where ApplicationID = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID);
				strBuilder.Append("' and Invoice_No = '"+strInvoiceNo);
				strBuilder.Append("' and Credit_No='"+drEach["Credit_No"].ToString()+"'");

				String strSQLQuery = strBuilder.ToString();			
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note table");

				//delete creditnote detail
				String strSQL=" Delete From Credit_Note_Detail where ApplicationID='"+strAppID+"'";
				strSQL+=" and EnterpriseID='"+strEnterpriseID+"'";
				strSQL+=" and Credit_No='"+ strCreditNo +"' ";					
				dbCommandApp = dbConApp.CreateCommand(strSQL,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows Deleted from Debit_Note_Detail table");
				transactionApp.Commit();
				for (int i=0;i<dsCNDetail.Tables[0].Rows.Count;i++)
				{					
					transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
					if(transactionApp == null)
					{
						Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0005","transactionApp is null");
						throw new ApplicationException("Failed Begining a App Transaction..",null);
					}
					DataRow drDND=dsCNDetail.Tables[0].Rows[i];	

					strBuilder.Remove(0,strBuilder.Length);	
				
					//strBuilder = new StringBuilder();			
					strBuilder.Append(" Insert into Credit_Note_Detail(ApplicationID, EnterpriseID, Invoice_No, ");
					strBuilder.Append(" Credit_No, Consignment_No, Credit_Amt, Credit_Description,charge_code,reason_code,remark) Values(");
					strBuilder.Append("'"+ strAppID+"','"+strEnterpriseID+"','"+strInvoiceNo+"','"+strCreditNo+"',");

					if(Utility.IsNotDBNull(drDND["Consignment_No"]) ==true && drDND["Consignment_No"].ToString() != "")
					{
						strConsignmentNo=Utility.ReplaceSingleQuote(drDND["Consignment_No"].ToString());						
						strBuilder.Append("'"+strConsignmentNo+"'");						
					}
					
					strBuilder.Append(",");
					if(Utility.IsNotDBNull(drDND["Amt"]) ==true && drDND["Amt"].ToString() != "")
					{
						dCreditAmt=(Decimal) drDND["Amt"];						
						if (drDND.HasVersion(DataRowVersion.Original))
						{					
							if (drDND["Amt", DataRowVersion.Original].ToString() !="")
								dOLDCreditAmt=(Decimal) drDND["Amt", DataRowVersion.Original];
						}	
						strBuilder.Append(dCreditAmt);
					}
					else
					{
						strBuilder.Append("null");
						dCreditAmt=0;
					}

					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["Description"]) ==true && drDND["Description"].ToString() != "")
					{
						strCreditDesc=(String) drDND["Description"];
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(strCreditDesc) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
		
					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["charge_code"]) ==true && drDND["charge_code"].ToString() != "")
					{						
						strBuilder.Append("'"+Utility.ReplaceSingleQuote(drDND["charge_code"].ToString()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	

					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["reason_code"]) ==true && drDND["reason_code"].ToString().Trim() != "")
					{						
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drDND["reason_code"].ToString().Trim()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["remark"]) ==true && drDND["remark"].ToString().Trim() != "")
					{						
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drDND["remark"].ToString().Trim()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
					strBuilder.Append(")");	
									

					//DELETE the Debit_Note_Detail ONLY for the CURRENT Consignment
					/*String strSQL=" Delete From Debit_Note_Detail where ApplicationID='"+strAppID+"'";
					strSQL+=" and EnterpriseID='"+strEnterpriseID+"' and Invoice_no='"+strInvoiceNo+"'";
					strSQL+=" and Credit_No='"+ strCreditNo +"' and Consignment_No='"+ strConsignmentNo +"'";					
					dbCommandApp = dbConApp.CreateCommand(strSQL,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows Deleted from Debit_Note_Detail table");*/
					
				

					//NOW INSERT Debit_Note_Detail Information
					strSQLQuery = strBuilder.ToString();			
					dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note_Detail table");
					dTotaCreditAmnt=0;
					if(strStat.ToUpper() == "A")
					{
						//calculate total debit amnt
						strSumBuilder.Remove(0,strSumBuilder.Length);
						strSumBuilder.Append(" select  isnull(sum(isnull(Credit_Amt,0)),0) as Credit_Amt from credit_note_detail dt with(nolock) ");
						strSumBuilder.Append(" inner join credit_note dn with(nolock) on dt.Credit_No = dn.Credit_No and dt.applicationid = dn.applicationid and dt.enterpriseid = dn.enterpriseid ");
						strSumBuilder.Append(" where dt.Consignment_No='"+strConsignmentNo+"' and dt.applicationid = '"+strAppID+"' and dt.enterpriseid = '"+strEnterpriseID+"' and dn.status='A' ");
						dbCommandApp = dbConApp.CreateCommand(strSumBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsTotal = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(Utility.IsNotDBNull(dsTotal.Tables[0].Rows[0])&&dsTotal.Tables[0].Rows.Count>0)
						{
							try
							{
								dTotaCreditAmnt = (Decimal)dsTotal.Tables[0].Rows[0]["Credit_Amt"];
							}
							catch
							{
								dTotaCreditAmnt=0;
							}
						}
						// Update Shipment Table
						if(strOldStat.ToUpper()=="A")
						{
							strBuilder.Remove(0,strBuilder.Length);
							dActCreditAmt=(dCreditAmt-dOLDCreditAmt);
							strBuilder.Append("Update shipment set Credit_Amt= "+dTotaCreditAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						else if(strOldStat.ToUpper()=="N")
						{
							strBuilder.Remove(0,strBuilder.Length);
							dActCreditAmt=(dCreditAmt);
							strBuilder.Append("Update shipment set Credit_Amt= "+dTotaCreditAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						strBuilder.Remove(0,strBuilder.Length);
						strBuilder.Append("select isnull(inv.invoice_amt,0)+isnull(s.debit_Amt,0)-isnull(s.credit_amt,0) total_cons_revenue, s.booking_no from invoice_detail inv with(nolock) inner join shipment s with(nolock) on inv.invoice_no=s.invoice_no and inv.Consignment_No=s.Consignment_No ");
						strBuilder.Append("where s.Consignment_No='"+strConsignmentNo+"' and s.invoice_no = '"+strInvoiceNo+"' ");
						strBuilder.Append(" and s.applicationid = '"+strAppID+"' and s.enterpriseid = '"+strEnterpriseID+"'");
						dbCommandApp = dbConApp.CreateCommand(strBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsResult = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(dsResult.Tables[0].Rows.Count>0)
							if(Utility.IsNotDBNull(dsResult.Tables[0].Rows[0]))
							{
								Decimal dRevenue = (Decimal)dsResult.Tables[0].Rows[0]["total_cons_revenue"];
								bookingNo = (int)dsResult.Tables[0].Rows[0]["booking_no"];
								strBuilder.Remove(0,strBuilder.Length);
								strBuilder.Append("Update shipment set total_cons_revenue = "+dRevenue+" where Consignment_No='"+strConsignmentNo+"' ");
								strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
								strSQLQuery = strBuilder.ToString();			
								dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
								dbCommandApp.Connection = conApp;
								dbCommandApp.Transaction = transactionApp;
								iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
								Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

							}
					}
					else if (strStat.ToUpper() == "C")
					{
						if(strOldStat.ToUpper()=="A")
						{
							//calculate total debit amnt
							strSumBuilder.Remove(0,strSumBuilder.Length);
							strSumBuilder.Append(" select  isnull(sum(isnull(Credit_Amt,0)),0) as Credit_Amt from credit_note_detail dt with(nolock) ");
							strSumBuilder.Append(" inner join credit_note dn with(nolock) on dt.Credit_No = dn.Credit_No and dt.applicationid = dn.applicationid and dt.enterpriseid = dn.enterpriseid ");
							strSumBuilder.Append(" where dt.Consignment_No='"+strConsignmentNo+"' and dt.applicationid = '"+strAppID+"' and dt.enterpriseid = '"+strEnterpriseID+"' and dn.status='A' ");
							dbCommandApp = dbConApp.CreateCommand(strSumBuilder.ToString(),CommandType.Text);
							dbCommandApp.Connection = conApp;
							DataSet dsTotal = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
							if(Utility.IsNotDBNull(dsTotal.Tables[0])&&dsTotal.Tables[0].Rows.Count>0)
							{
								try
								{
									dTotaCreditAmnt = (Decimal)dsTotal.Tables[0].Rows[0]["Credit_Amt"];
								}
								catch
								{
									dTotaCreditAmnt=0;
								}
							}

							// Update Shipment Table
							strBuilder.Remove(0,strBuilder.Length);
							//dActCreditAmt=(dCreditAmt-dOLDCreditAmt);
							strBuilder.Append("Update shipment set Credit_Amt="+dTotaCreditAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						strBuilder.Remove(0,strBuilder.Length);
						strBuilder.Append("select isnull(inv.invoice_amt,0)+isnull(s.debit_Amt,0)-isnull(s.credit_amt,0) total_cons_revenue, s.booking_no from invoice_detail inv with(nolock) inner join shipment s with(nolock) on inv.invoice_no=s.invoice_no and inv.Consignment_No=s.Consignment_No ");
						strBuilder.Append("where s.Consignment_No='"+strConsignmentNo+"' and s.invoice_no = '"+strInvoiceNo+"' ");
						strBuilder.Append(" and s.applicationid = '"+strAppID+"' and s.enterpriseid = '"+strEnterpriseID+"'");
						dbCommandApp = dbConApp.CreateCommand(strBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsResult = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(dsResult.Tables[0].Rows.Count>0)
							if(Utility.IsNotDBNull(dsResult.Tables[0].Rows[0]))
							{
								Decimal dRevenue = (Decimal)dsResult.Tables[0].Rows[0]["total_cons_revenue"];
								bookingNo = (int)dsResult.Tables[0].Rows[0]["booking_no"];
								strBuilder.Remove(0,strBuilder.Length);
								strBuilder.Append("Update shipment set total_cons_revenue = "+dRevenue+" where Consignment_No='"+strConsignmentNo+"' ");
								strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
								strSQLQuery = strBuilder.ToString();			
								dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
								dbCommandApp.Connection = conApp;
								dbCommandApp.Transaction = transactionApp;
								iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
								Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

							}
					}
					transactionApp.Commit();
					if(strTmpCons!=strConsignmentNo)
					{
						strTmpCons = strConsignmentNo;
						if(strStat == "A")
						{
							strRemark = " CN #: "+strCreditNo+" amount THB: "+Convert.ToDecimal(dsCNDetail.Tables[0].Compute("sum(Amt)","Consignment_No='"+strConsignmentNo+"'")).ToString("####.00");
							ShipmentTrackingMgrDAL.UpdateShipmentStatus(strAppID,strEnterpriseID,bookingNo,DateTime.Now,strConsignmentNo,"CNA",
								strRemark,statusUpdateBy);
						}
						else if(strStat == "C")
						{
							strRemark = " CN #: "+strCreditNo+" amount THB: "+Convert.ToDecimal(dsCNDetail.Tables[0].Compute("sum(Amt)","Consignment_No='"+strConsignmentNo+"'")).ToString("####.00");
								ShipmentTrackingMgrDAL.UpdateShipmentStatus(strAppID,strEnterpriseID,bookingNo,DateTime.Now,strConsignmentNo,"CNC",
									strRemark,statusUpdateBy);
							}
						}
					}
				
				//commit the transaction
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0014","App db Transaction committed.");
			}					
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0006","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0009","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyCreditNoteDS","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}			
			return iRowsAffected;
		}


		public static int DeleteCreditNoteDS(String strAppID, String strEnterpriseID,String strInvoiceNo, String strCredit_No)
		{
			int iRowsAffected = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteCreditNoteDS","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteCreditNoteDS","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteCreditNoteDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)				
					conApp.Close();
				
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteCreditNoteDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}		

			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteCreditNoteDS","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				//Logical DELETE in Credit_Note Table
				StringBuilder strBuilder = new StringBuilder();			
				strBuilder.Append("Update Credit_Note set Deleted='Y' where Invoice_No='"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' and Credit_No='");
				strBuilder.Append(strCredit_No +"' and ApplicationId='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"'");
			
				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteCreditNoteDS","ERR0012",iRowsAffected + " rows Logically Deleted from Credit_Note table");				

				//DELETE from Credit_Note_Detail table
				strBuilder.Remove(0,strBuilder.Length);
				strBuilder.Append(" Delete from Credit_Note_Detail where applicationid = '"+strAppID+"' ");
				strBuilder.Append(" and enterpriseid = '"+strEnterpriseID+"' and Invoice_No = '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' ");
				strBuilder.Append(" and Credit_No = '"+strCredit_No+"'");

				strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteCreditNoteDS","ERR0012",iRowsAffected + " rows Deleted from Credit_Note_Detail table");				

				// UPDATE  Credit_Amt=0 for ALL CONSIGNMNETS of the INVOICE found in SHIPMENT 
				strBuilder.Remove(0,strBuilder.Length);
				//				strBuilder.Append(" UPDATE Shipment SET Credit_Amt=0 FROM Shipment SH,Invoice_Detail IND ");
				//				strBuilder.Append(" Where SH.ApplicationID=IND.ApplicationID and SH.EnterpriseID=IND.EnterpriseID and ");
				//				strBuilder.Append(" SH.Consignment_No=IND.Consignment_No and IND.Invoice_No='"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' and ");
				//				strBuilder.Append(" SH.Applicationid = '"+strAppID+"' and SH.Enterpriseid = '"+strEnterpriseID+"'");		

				strBuilder.Append(" UPDATE Shipment SH SET Credit_Amt=0 where SH.Consignment_No in (");
				strBuilder.Append(" Select IND.Consignment_No from Invoice_Detail IND where SH.ApplicationID=IND.ApplicationID ");
				strBuilder.Append(" and SH.EnterpriseID=IND.EnterpriseID and IND.Invoice_No='"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' ");
				strBuilder.Append(" and SH.Consignment_No=IND.Consignment_No) and SH.Applicationid = '"+strAppID+"' and SH.Enterpriseid = '"+strEnterpriseID+"'");

				strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteCreditNoteDS","ERR0012",iRowsAffected + " rows Deleted from Credit_Note_Detail table");				

				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditDetailDS","ERR0014","App db Transaction committed.");
			}
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditDetailDS","ERR0006","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditDetailDS","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditDetailDS","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditDetailDS","ERR0009","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditDetailDS","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditDetailDS","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsAffected;
		}

		public static SessionDS GetCreditDetailDS(String strAppID, String strEnterpriseID, String strInvoiceNo, String strCreditNote, int iCurrent, int iDSRecSize)
		{
			SessionDS dsCreditDetail = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CreditCreditMgrDAL","CreditCreditMgrDAL","GetCreditDetailDS","EDB101","DbConnection object is null!!");
				return dsCreditDetail;
			}
			StringBuilder strBuilder = new StringBuilder();		
			strBuilder.Append(" Select Invoice_No,Credit_No,Consignment_No,charge_code,reason_code, 0 as Invoice_Amt,Credit_Amt,Credit_Description as Description, 1 as flag,Remark");
			strBuilder.Append(" From Credit_Note_Detail ");
			strBuilder.Append(" Where Applicationid = '"+strAppID+"' and Enterpriseid = '"+strEnterpriseID+"' and ");
			strBuilder.Append(" Invoice_No= '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' and Credit_No='"+strCreditNote+"'");			
			
			dsCreditDetail = (SessionDS) dbCon.ExecuteQuery(strBuilder.ToString(),iCurrent,iDSRecSize,"Credit_Note_Detail");
			return  dsCreditDetail ;

		}

		/*public static DataSet GetCreditDetailDS(String strAppID, String strEnterpriseID, String strInvoiceNo, String strCreditNote)
		{
			DataSet dsCreditDetail = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","GetCreditDetailDS","EDB101","DbConnection object is null!!");
				return dsCreditDetail;
			}
			StringBuilder strBuilder = new StringBuilder();		

			strBuilder.Append(" Select CND.Invoice_No,CND.Credit_No,CND.Consignment_No,SH.Invoice_Amt, CND.Credit_Amt as Amt, CND.Credit_Description as Description");
			strBuilder.Append(" From Credit_Note_Detail CND, Shipment SH Where CND.ApplicationID=SH.ApplicationID and ");
			strBuilder.Append(" CND.Enterpriseid =SH.Enterpriseid  and CND.Consignment_No=SH.Consignment_No and ");
			strBuilder.Append(" CND.Applicationid = '"+strAppID+"' and CND.Enterpriseid = '"+strEnterpriseID+"' and ");
			strBuilder.Append(" CND.Invoice_No= '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' and CND.Credit_No='"+strCreditNote+"'"); 
									
			dsCreditDetail = (DataSet) dbCon.ExecuteQuery(strBuilder.ToString(),ReturnType.DataSetType);
			return  dsCreditDetail;
	
		}*/
		public static DataSet GetCreditDetailDS(String strAppID, String strEnterpriseID, String strInvoiceNo, String strCreditNote)
		{
			DataSet dsCreditDetail = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CreditCreditMgrDAL","CreditDebitMgrDAL","GetCreditDetailDS","EDB101","DbConnection object is null!!");
				return dsCreditDetail;
			}
			StringBuilder strBuilder = new StringBuilder();		
	
			strBuilder.Append(" Select Invoice_No,Credit_No,Consignment_No,charge_code,reason_code, 0.00 as Invoice_Amt,Credit_Amt as Amt,Credit_Description as Description, 1 as flag,Remark");
			strBuilder.Append(" From Credit_Note_Detail ");
			strBuilder.Append(" Where Applicationid = '"+strAppID+"' and Enterpriseid = '"+strEnterpriseID+"' and ");
			strBuilder.Append(" Invoice_No= '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' and Credit_No='"+strCreditNote+"'");
			
			dsCreditDetail = (DataSet) dbCon.ExecuteQuery(strBuilder.ToString(),ReturnType.DataSetType);
			Decimal amnt=0;
			for(int i=0;i<dsCreditDetail.Tables[0].Rows.Count;i++)
			{
				amnt = GetCalcuratedInvoiceAmt(strAppID,strEnterpriseID,dsCreditDetail.Tables[0].Rows[i]["Consignment_No"].ToString(),strInvoiceNo,dsCreditDetail.Tables[0].Rows[i]["charge_code"].ToString());
				/*if(amnt==null)
				{
					amnt="0";
		}
				else
		{			
					if(amnt.Trim()=="")
			{
						amnt="0";
					}*/
				dsCreditDetail.Tables[0].Rows[i]["Invoice_Amt"]= amnt;
			}				
			return  dsCreditDetail ;

		}

		public static DataSet GetCreditDetailDS(String strAppID, String strEnterpriseID, String strInvoiceNo, String strCreditNote,DbConnection dbCon)
		{			
			#region Connection			
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("CreditCreditMgrDAL","CreditCreditMgrDAL","GetCreditDetailDS","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditCreditMgrDAL","CreditCreditMgrDAL","GetCreditDetailDS","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditCreditMgrDAL","CreditCreditMgrDAL","GetCreditDetailDS","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	
			#endregion

			return  GetCreditDetailDS( strAppID,  strEnterpriseID,  strInvoiceNo,  strCreditNote, dbCon, dbCmd) ;
		}

		public static DataSet GetCreditDetailDS(String strAppID, String strEnterpriseID, String strInvoiceNo, String strCreditNote,DbConnection dbCon,IDbCommand dbCmd)
		{
			DataSet dsCreditDetail = null;			

			StringBuilder strBuilder = new StringBuilder();		
	
			strBuilder.Append(" Select Invoice_No,Credit_No,Consignment_No,charge_code,reason_code, 0.00 as Invoice_Amt,Credit_Amt as Amt,Credit_Description as Description, 1 as flag,Remark");
			strBuilder.Append(" From Credit_Note_Detail ");
			strBuilder.Append(" Where Applicationid = '"+strAppID+"' and Enterpriseid = '"+strEnterpriseID+"' and ");
			strBuilder.Append(" Invoice_No= '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' and Credit_No='"+strCreditNote+"'");
			
			dbCmd.CommandText = strBuilder.ToString();

			dsCreditDetail = (DataSet) dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			
			Decimal amnt=0;
			for(int i=0;i<dsCreditDetail.Tables[0].Rows.Count;i++)
			{
				amnt = GetCalcuratedInvoiceAmt(strAppID,strEnterpriseID,dsCreditDetail.Tables[0].Rows[i]["Consignment_No"].ToString(),strInvoiceNo,dsCreditDetail.Tables[0].Rows[i]["charge_code"].ToString(),dbCon,dbCmd);

				dsCreditDetail.Tables[0].Rows[i]["Invoice_Amt"]= amnt;
				
			}
			return  dsCreditDetail ;
		}



		public static void AddNewRowInConsDS(DataSet dsCreditDetail)
		{
			DataRow drNew = dsCreditDetail.Tables[0].NewRow();
			drNew[0] = "";
			try
			{
				dsCreditDetail.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","AddNewRowInConsDS","R005",ex.Message.ToString());
			}			
			//			dsCreditDetail.Tables[0].Rows.Add(drNew);
		}

		public static void AddNewRowInConsDS(ref SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();
			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
						
			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","AddNewRowInConsDS","R005",ex.Message.ToString());
			}			
			//			sessionDS.QueryResultMaxSize++;
		}
		
		public static DataSet StatusDNQuery(String strAppID,String strEnterpriseID,String code_id,String culture)
		{
			String sTempWhere=null;
			sTempWhere= " and codeid ='"+code_id+"' and culture = '"+culture+"' and code_str_value <>'P'";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);	
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT code_text AS DbComboText, code_str_value AS DbComboValue FROM Core_System_Code ";
					strQry+= " WHERE applicationID='"+strAppID+"' "+sTempWhere+" ORDER BY codeid";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT code_text AS DbComboText, code_str_value AS DbComboValue FROM Core_System_Code where ";
					strQry+= "applicationID='"+strAppID+"' "+sTempWhere+" ORDER BY codeid";
					break;
				
				case DataProviderType.Oledb:
					break;
			}
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static String GetInvoiceAmt(String strAppID, String strEnterpriseID, String strConsignmentNo)
		{
			String strInvoiceAmt = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","GetInvoiceAmt","EDB101","DbConnection object is null!!");
				return strInvoiceAmt;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select Invoice_Amt from Shipment where applicationid = '"+strAppID+"' ");
			strBuilder.Append(" and enterpriseid = '"+strEnterpriseID+"' and Consignment_No='"+Utility.ReplaceSingleQuote(strConsignmentNo)+"'");
			String strSQLQuery = strBuilder.ToString();
			DataSet dsInvoice = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			int iRowCnt = dsInvoice.Tables[0].Rows.Count;
			
			if(iRowCnt > 0)
			{
				DataRow drCurrent = dsInvoice.Tables[0].Rows[0];
				Decimal dInvoiceAmt = (Decimal)drCurrent["Invoice_Amt"];
				strInvoiceAmt=dInvoiceAmt.ToString("#.00");
			}
			return  strInvoiceAmt;
		}
		

		public static String GetInvoiceAmt(String strAppID, String strEnterpriseID, String strConsignmentNo,String strInvNo,String strChargeCode)
		{
			String strInvoiceAmt = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","GetInvoiceAmt","EDB101","DbConnection object is null!!");
				return strInvoiceAmt;
			}
			String columnName = "";
			switch (strChargeCode.Trim().ToUpper())
			{
				case "FRG" :
					columnName="tot_freight_charge";
					break;
				case "INS" :
					columnName="insurance_amt";
					break;
				case "VAS" :
					columnName="tot_vas_surcharge";
					break;
				case "ESA" :
					columnName="esa_surcharge";
					break;
				case "OTH" :
					columnName="other_surcharge";
					break;
				case "PDX" :
					columnName="total_exception";
					break;
				case "ADJ" :
					columnName="invoice_adj_amount";
					break;
				case "TOT" :
					columnName="invoice_amt";
					break;
			}
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select isnull("+columnName+",0.0) as "+columnName+" from Invoice_Detail where applicationid = '"+strAppID+"' ");
			strBuilder.Append(" and enterpriseid = '"+strEnterpriseID+"' and Consignment_No='"+Utility.ReplaceSingleQuote(strConsignmentNo)+"' and invoice_no='"+Utility.ReplaceSingleQuote(strInvNo)+"'");
			String strSQLQuery = strBuilder.ToString();
			DataSet dsInvoice = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			int iRowCnt = dsInvoice.Tables[0].Rows.Count;
			
			if(iRowCnt > 0)
			{
				DataRow drCurrent = dsInvoice.Tables[0].Rows[0];
				Decimal dInvoiceAmt = (Decimal)drCurrent[columnName];
				strInvoiceAmt=dInvoiceAmt.ToString("#.00");
			}
			return  strInvoiceAmt;
		}
		#region Comment by Sompote 2010-05-10
		/*
		public static Decimal GetCalcuratedInvoiceAmt(String strAppID, String strEnterpriseID, String strConsignmentNo,String strInvNo,String strChargeCode)
		{
			Decimal dInvoiceAmt = 0;
			try
			{
				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
				if(dbCon == null)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","GetInvoiceAmt","EDB101","DbConnection object is null!!");
					return dInvoiceAmt;
				}
				String columnName = "";
				switch (strChargeCode.Trim().ToUpper())
				{
					case "FRG" :
						columnName="tot_freight_charge";
						break;
					case "INS" :
						columnName="insurance_amt";
						break;
					case "VAS" :
						columnName="tot_vas_surcharge";
						break;
					case "ESA" :
						columnName="esa_surcharge";
						break;
					case "OTH" :
						columnName="other_surcharge";
						break;
					case "PDX" :
						columnName="total_exception";
						break;
					case "ADJ" :
						columnName="invoice_adj_amount";
						break;
					case "TOT" :
						columnName="invoice_amt";
						break;
				}
				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append("select isnull("+columnName+",0.0) as "+columnName+" from Invoice_Detail where applicationid = '"+strAppID+"' ");
				strBuilder.Append(" and enterpriseid = '"+strEnterpriseID+"' and Consignment_No='"+Utility.ReplaceSingleQuote(strConsignmentNo)+"' and invoice_no='"+Utility.ReplaceSingleQuote(strInvNo)+"'");
				String strSQLQuery = strBuilder.ToString();
				DataSet dsInvoice = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
				int iRowCnt = dsInvoice.Tables[0].Rows.Count;
			
				if(iRowCnt > 0)
				{
					DataRow drCurrent = dsInvoice.Tables[0].Rows[0];
					dInvoiceAmt = (Decimal)drCurrent[columnName];
				}
				strBuilder.Remove(0,strBuilder.Length);
				strBuilder.Append("select isnull(sum(debit_amt),0) as DNAmount from dbo.Debit_Note_Detail as dd ");
				strBuilder.Append(" inner join dbo.Debit_Note as dn ");
				strBuilder.Append(" on  dd.applicationid=dn.applicationid and dd.enterpriseid=dn.enterpriseid and dd.debit_no=dn.debit_no and dd.invoice_no=dn.invoice_no ");
				strBuilder.Append(" where dd.charge_code='"+strChargeCode.Trim().ToUpper()+"'  and dn.status='A' ");
				strBuilder.Append(" and dn.applicationid = '"+strAppID+"' and dn.enterpriseid = '"+strEnterpriseID+"' and dd.Consignment_No='"+Utility.ReplaceSingleQuote(strConsignmentNo)+"' and dn.invoice_no='"+Utility.ReplaceSingleQuote(strInvNo)+"'");				
				strSQLQuery = strBuilder.ToString();
				DataSet dsDN = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
				iRowCnt = dsDN.Tables[0].Rows.Count; 
				Decimal dDNAmount = 0;
				if(iRowCnt > 0)
				{
					DataRow drCurrent = dsDN.Tables[0].Rows[0];
					dDNAmount = (Decimal)drCurrent["DNAmount"];
				}

				strBuilder.Remove(0,strBuilder.Length);
				strBuilder.Append("select isnull(sum(credit_amt),0) as CNAmount from dbo.Credit_Note_Detail as dd ");
				strBuilder.Append(" inner join dbo.Credit_Note as dn ");
				strBuilder.Append(" on  dd.applicationid=dn.applicationid and dd.enterpriseid=dn.enterpriseid and dd.credit_no=dn.credit_no and dd.invoice_no=dn.invoice_no ");
				strBuilder.Append(" where dd.charge_code='"+strChargeCode.Trim().ToUpper()+"'  and dn.status='A' ");
				strBuilder.Append(" and dn.applicationid = '"+strAppID+"' and dn.enterpriseid = '"+strEnterpriseID+"' and dd.Consignment_No='"+Utility.ReplaceSingleQuote(strConsignmentNo)+"' and dn.invoice_no='"+Utility.ReplaceSingleQuote(strInvNo)+"'");				
				strSQLQuery = strBuilder.ToString();
				DataSet dsCN = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
				iRowCnt = dsCN.Tables[0].Rows.Count; 
				Decimal dCNAmount = 0;
				if(iRowCnt > 0)
				{
					DataRow drCurrent = dsCN.Tables[0].Rows[0];
					dCNAmount = (Decimal)drCurrent["CNAmount"];
				}
				dInvoiceAmt = dInvoiceAmt+dDNAmount-dCNAmount;
			}
			catch
			{
			}
			return  dInvoiceAmt;
		}
		
		*/
		#endregion
		public static Decimal GetCalcuratedInvoiceAmt(String strAppID, String strEnterpriseID, String strConsignmentNo,String strInvNo,String strChargeCode )
		{		
			Decimal dInvoiceAmt = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","GetCalcuratedInvoiceAmt","EDB101","DbConnection object is null!!");
				return dInvoiceAmt;
			}
			return  GetCalcuratedInvoiceAmt( strAppID,  strEnterpriseID,  strConsignmentNo, strInvNo, strChargeCode, dbCon);
		}
		
		public static Decimal GetCalcuratedInvoiceAmt(String strAppID, String strEnterpriseID, String strConsignmentNo,String strInvNo,String strChargeCode,DbConnection dbCon)
		{
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","GetCalcuratedInvoiceAmt","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","GetCalcuratedInvoiceAmt","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","GetCalcuratedInvoiceAmt","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	


			return  GetCalcuratedInvoiceAmt( strAppID,  strEnterpriseID,  strConsignmentNo, strInvNo, strChargeCode, dbCon, dbCmd);
		}
		
		public static Decimal GetCalcuratedInvoiceAmt(String strAppID, String strEnterpriseID, String strConsignmentNo,String strInvNo,String strChargeCode,DbConnection dbCon,IDbCommand dbCmd)
		{
			Decimal dInvoiceAmt = 0;
			try
			{
				//				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
				//				if(dbCon == null)
				//				{
				//					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","GetInvoiceAmt","EDB101","DbConnection object is null!!");
				//					return dInvoiceAmt;
				//				}
				String columnName = "";
				switch (strChargeCode.Trim().ToUpper())
				{
					case "FRG" :
						columnName="tot_freight_charge";
						break;
					case "INS" :
						columnName="insurance_amt";
						break;
					case "VAS" :
						columnName="tot_vas_surcharge";
						break;
					case "ESA" :
						columnName="esa_surcharge";
						break;
					case "OTH" :
						columnName="other_surcharge";
						break;
					case "PDX" :
						columnName="total_exception";
						break;
					case "ADJ" :
						columnName="invoice_adj_amount";
						break;
					case "TOT" :
						columnName="invoice_amt";
						break;
				}
				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append("select isnull("+columnName+",0.0) as "+columnName+" from Invoice_Detail where applicationid = '"+strAppID+"' ");
				strBuilder.Append(" and enterpriseid = '"+strEnterpriseID+"' and Consignment_No='"+Utility.ReplaceSingleQuote(strConsignmentNo)+"' and invoice_no='"+Utility.ReplaceSingleQuote(strInvNo)+"'");
				String strSQLQuery = strBuilder.ToString();
				
				//DataSet dsInvoice = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
				dbCmd.CommandText = strSQLQuery;
				DataSet dsInvoice = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
				
				int iRowCnt = dsInvoice.Tables[0].Rows.Count;
			
				if(iRowCnt > 0)
				{
					DataRow drCurrent = dsInvoice.Tables[0].Rows[0];
					dInvoiceAmt = (Decimal)drCurrent[columnName];
				}
				strBuilder.Remove(0,strBuilder.Length);
				strBuilder.Append("select isnull(sum(debit_amt),0) as DNAmount from dbo.Debit_Note_Detail as dd ");
				strBuilder.Append(" inner join dbo.Debit_Note as dn ");
				strBuilder.Append(" on  dd.applicationid=dn.applicationid and dd.enterpriseid=dn.enterpriseid and dd.debit_no=dn.debit_no and dd.invoice_no=dn.invoice_no ");
				strBuilder.Append(" where dd.charge_code='"+strChargeCode.Trim().ToUpper()+"'  and dn.status='A' ");
				strBuilder.Append(" and dn.applicationid = '"+strAppID+"' and dn.enterpriseid = '"+strEnterpriseID+"' and dd.Consignment_No='"+Utility.ReplaceSingleQuote(strConsignmentNo)+"' and dn.invoice_no='"+Utility.ReplaceSingleQuote(strInvNo)+"'");				
				strSQLQuery = strBuilder.ToString();
				//DataSet dsDN = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
				dbCmd.CommandText = strSQLQuery;
				DataSet dsDN = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
				
				iRowCnt = dsDN.Tables[0].Rows.Count; 
				Decimal dDNAmount = 0;
				if(iRowCnt > 0)
				{
					DataRow drCurrent = dsDN.Tables[0].Rows[0];
					dDNAmount = (Decimal)drCurrent["DNAmount"];
				}

				strBuilder.Remove(0,strBuilder.Length);
				strBuilder.Append("select isnull(sum(credit_amt),0) as CNAmount from dbo.Credit_Note_Detail as dd ");
				strBuilder.Append(" inner join dbo.Credit_Note as dn ");
				strBuilder.Append(" on  dd.applicationid=dn.applicationid and dd.enterpriseid=dn.enterpriseid and dd.credit_no=dn.credit_no and dd.invoice_no=dn.invoice_no ");
				strBuilder.Append(" where dd.charge_code='"+strChargeCode.Trim().ToUpper()+"'  and dn.status='A' ");
				strBuilder.Append(" and dn.applicationid = '"+strAppID+"' and dn.enterpriseid = '"+strEnterpriseID+"' and dd.Consignment_No='"+Utility.ReplaceSingleQuote(strConsignmentNo)+"' and dn.invoice_no='"+Utility.ReplaceSingleQuote(strInvNo)+"'");				
				strSQLQuery = strBuilder.ToString();
				//DataSet dsCN = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
				dbCmd.CommandText = strSQLQuery;
				DataSet dsCN = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);

				iRowCnt = dsCN.Tables[0].Rows.Count; 
				Decimal dCNAmount = 0;
				if(iRowCnt > 0)
				{
					DataRow drCurrent = dsCN.Tables[0].Rows[0];
					dCNAmount = (Decimal)drCurrent["CNAmount"];
				}
				dInvoiceAmt = dInvoiceAmt+dDNAmount-dCNAmount;
			}
			catch
			{
			}
			return  dInvoiceAmt;
		}
		
		
		public static int DeleteCreditDetailDS(String strAppID, String strEnterpriseID,String strInvoiceNo, String strCreditNo,String strConsignmentNo, Decimal dCreditAmount)
		{
			int iRowsAffected = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteCreditDetailDS","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteCreditDetailDS","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteCreditDetailDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)				
					conApp.Close();
				
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteCreditDetailDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}		
			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteCreditDetailDS","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{

				StringBuilder strBuilder = new StringBuilder();			
				strBuilder.Append("Delete from Credit_Note_Detail where applicationid = '"+strAppID+"' ");
				strBuilder.Append(" and enterpriseid = '"+strEnterpriseID+"' and Invoice_No = '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' ");
				strBuilder.Append(" and Credit_No = '"+strCreditNo+"' and Consignment_No='"+Utility.ReplaceSingleQuote(strConsignmentNo)+"'");
				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;		
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteCreditDetailDS","SDM001",iRowsAffected+" rows deleted from  Exception_Code table");

				strBuilder.Remove(0,strBuilder.Length);
				strBuilder.Append("Update shipment set Credit_Amt=Credit_Amt - "+dCreditAmount+"  where Consignment_No='"+Utility.ReplaceSingleQuote(strConsignmentNo)+"' ");
				strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
				strSQLQuery = strBuilder.ToString();			
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteCreditDetailDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditDetailDS","ERR0014","App db Transaction committed.");
			}
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditDetailDS","ERR0006","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditDetailDS","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditDetailDS","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditDetailDS","ERR0009","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditDetailDS","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddCreditDetailDS","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;		
		}

		/// Debit Note Methods
		/// 
		public static SessionDS GetEmptyDebitNoteDS(int iNumRows)
		{
			DataTable dtDebitNote = new DataTable();
 
			dtDebitNote.Columns.Add(new DataColumn("debit_no", typeof(string)));
			dtDebitNote.Columns.Add(new DataColumn("debit_date", typeof(DateTime)));
			dtDebitNote.Columns.Add(new DataColumn("invoice_no", typeof(string)));
			dtDebitNote.Columns.Add(new DataColumn("Invoice_Date", typeof(DateTime)));						
			dtDebitNote.Columns.Add(new DataColumn("PayerId", typeof(string)));			
			dtDebitNote.Columns.Add(new DataColumn("Payer_name", typeof(string)));							
			dtDebitNote.Columns.Add(new DataColumn("remark", typeof(string)));	
			dtDebitNote.Columns.Add(new DataColumn("Deleted", typeof(string)));
			dtDebitNote.Columns.Add(new DataColumn("status", typeof(string)));
			dtDebitNote.Columns.Add(new DataColumn("Create_By", typeof(string)));
			dtDebitNote.Columns.Add(new DataColumn("Create_Date", typeof(DateTime)));
			dtDebitNote.Columns.Add(new DataColumn("Update_By", typeof(string)));
			dtDebitNote.Columns.Add(new DataColumn("Update_Date", typeof(DateTime)));
			dtDebitNote.Columns.Add(new DataColumn("Status_update_By", typeof(string)));
			dtDebitNote.Columns.Add(new DataColumn("Status_update_Date", typeof(DateTime)));
			dtDebitNote.Columns.Add(new DataColumn("invoice_status", typeof(string)));

			
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtDebitNote.NewRow();
				dtDebitNote.Rows.Add(drEach);
			}

			DataSet dsDN = new DataSet();
			dsDN.Tables.Add(dtDebitNote);

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsDN;
			sessionDS.DataSetRecSize = dsDN.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;	
		}

		public static SessionDS GetEmptyDebitDetailDS(int iNumRows)
		{
			DataTable dtDebitDetail = new DataTable();
 
			dtDebitDetail.Columns.Add(new DataColumn("debit_no", typeof(string)));			
			dtDebitDetail.Columns.Add(new DataColumn("invoice_no", typeof(string)));
			dtDebitDetail.Columns.Add(new DataColumn("Consignment_no", typeof(string)));
			dtDebitDetail.Columns.Add(new DataColumn("charge_code", typeof(string)));
			dtDebitDetail.Columns.Add(new DataColumn("Invoice_Amt", typeof(decimal)));						
			dtDebitDetail.Columns.Add(new DataColumn("reason_code", typeof(string)));
			dtDebitDetail.Columns.Add(new DataColumn("Amt", typeof(decimal)));						
			dtDebitDetail.Columns.Add(new DataColumn("Description", typeof(string)));			
			dtDebitDetail.Columns.Add(new DataColumn("remark", typeof(string)));
			
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtDebitDetail.NewRow();
				dtDebitDetail.Rows.Add(drEach);
			}

			DataSet dsDebitDtl = new DataSet();
			dsDebitDtl.Tables.Add(dtDebitDetail);

			//dsDebitDtl.Tables[0].Columns["Consignment_no"].Unique = true; //Checks duplicate records..
			//			dsDebitDtl.Tables[0].Columns["Consignment_no"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsDebitDtl;
			sessionDS.DataSetRecSize = dsDebitDtl.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;	
		}
		
		public static SessionDS GetDebitNoteDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("DebitDebitMgrDAL","GetDebitNoteDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			bool skip = false;
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" Select Debit_No,Debit_Date,I.Invoice_No, Invoice_Date, PayerId, Payer_Name, DN.Remark, DN.Deleted,DN.status,DN.Create_By,DN.Create_Date,DN.Update_By,DN.Update_Date,DN.status_update_by,DN.status_update_date,I.invoice_status from Debit_Note DN, Invoice I where ");
			strBuilder.Append(" DN.Invoice_No=I.Invoice_No and DN.ApplicationID=I.ApplicationID and DN.EnterpriseID=I.EnterpriseID and DN.ApplicationID = '");
			strBuilder.Append(strAppID+ "' and DN.Enterpriseid = '"+strEnterpriseID+"' ");
			
			DataRow drEach = dsQueryParam.Tables[0].Rows[0];
			if(Utility.IsNotDBNull(drEach["debit_no"])==true && drEach["debit_no"].ToString() !="" )
			{
				String strDebit_No = (String) drEach["debit_no"];
				strBuilder.Append(" and DN.debit_no like '%"+strDebit_No+"%' ");
				skip = true;
			}
			if(Utility.IsNotDBNull(drEach["debit_date"])==true && drEach["debit_date"].ToString() != "")
			{
				DateTime dtDebit_Date = System.Convert.ToDateTime(drEach["debit_date"]);
				DateTime dtDebit_toDate = new DateTime(dtDebit_Date.Year,dtDebit_Date.Month,dtDebit_Date.Day,23,59,59);				
				strBuilder.Append(" and debit_date between ");				
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID,dtDebit_Date,DTFormat.DateTime));
				strBuilder.Append(" and ");
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID,dtDebit_toDate,DTFormat.DateTime));
				/*strBuilder.Append(" and debit_date =");				
				strBuilder.Append(Utility.DateFormat(strAppID, strEnterpriseID,dtDebit_Date,DTFormat.DateTime));*/				
				skip = true;
			}			

			if(Utility.IsNotDBNull(drEach["invoice_no"])== true && drEach["invoice_no"].ToString() != "")
			{
				String strInvoiceNo = (String) drEach["invoice_no"];
				strBuilder.Append(" and I.Invoice_no = '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' ");
			}
			
			if(Utility.IsNotDBNull(drEach["PayerId"])== true && drEach["PayerId"].ToString() != "")
			{
				String strPayerId = (String) drEach["PayerId"];
				strBuilder.Append(" and I.PayerId = '"+Utility.ReplaceSingleQuote(strPayerId)+"' ");
			}
			if(!skip)
			{
				if(Utility.IsNotDBNull(drEach["PayerId"])== true && drEach["PayerId"].ToString() == "" && Utility.IsNotDBNull(drEach["invoice_no"])== true && drEach["invoice_no"].ToString() == "")
				{
					strBuilder.Append(" and I.Invoice_no = '' and I.PayerId = '' ");
				}
			}

			if(Utility.IsNotDBNull(drEach["remark"])== true && drEach["remark"].ToString() != "")
			{
				String strRemark= (String) drEach["remark"];
				strBuilder.Append(" and remark like N'%"+Utility.ReplaceSingleQuote(strRemark)+"%' ");
			}	
			if(Utility.IsNotDBNull(drEach["status"])== true && drEach["status"].ToString() != "")
			{
				String strStatus= (String) drEach["status"];
				strBuilder.Append(" and status = '"+Utility.ReplaceSingleQuote(strStatus)+"' ");
			}
			strBuilder.Append(" Order by Debit_No");	
			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"DebitNoteTable");
		
			return  sessionDS;	
		}

		public static int AddDebitNoteDS(String strAppID, String strEnterpriseID,DataSet dsToInsert, ref String strDebitNote_No, DataSet dsCNDetail)
		{
			int iRowsAffected = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)				
					conApp.Close();
				
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}		
			
			String strInvoiceNo=null;
			String strConsignmentNo=null;
			Decimal dDebitAmt=0;
			String strDebitDesc=null;

			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{
				//generate a new DebitNote Number and insert into  table		
				String strPrefix = "DND"+DateTime.Now.Year.ToString().Substring(2,2);
				int runnigNo = (int)Counter.GetNext(strAppID,strEnterpriseID,"DebitNote_Number");
				strDebitNote_No = strPrefix+runnigNo.ToString("000000");
				//strDebitNote_No = (int)Counter.GetNext(strAppID,strEnterpriseID,"DebitNote_Number");
			
				StringBuilder strBuilder = new StringBuilder();
				//Insert a Record into Debit_Note
				strBuilder.Append("insert into Debit_Note (ApplicationID,EnterpriseID,Invoice_No,Debit_No,Debit_Date,Remark,Deleted,status,Create_by,Create_date,Update_By,Update_Date) values (");
				strBuilder.Append("'"+strAppID+"','"+strEnterpriseID);

				int iRowCnt = dsToInsert.Tables[0].Rows.Count;
				if (iRowCnt<=0) 
					return iRowsAffected;

				DataRow drEach = dsToInsert.Tables[0].Rows[0];			
			
				strBuilder.Append("',");
				if(Utility.IsNotDBNull(drEach["Invoice_No"]) ==true && drEach["Invoice_No"].ToString() != "")
				{
					strInvoiceNo=Utility.ReplaceSingleQuote(drEach["Invoice_No"].ToString());
					strBuilder.Append("'"+strInvoiceNo+"'");							
				}
				else			
					strBuilder.Append("null");
		
				strBuilder.Append(",");
				if(strDebitNote_No != "" && strDebitNote_No != null)
					strBuilder.Append("'"+strDebitNote_No+"'");				
				else			
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Debit_Date"]) ==true && drEach["Debit_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Debit_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Remark"]) ==true && drEach["Remark"].ToString() != "")
					strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drEach["Remark"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",'N'");		//Deleted is 'N' default when inserting a new record

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["status"]) ==true && drEach["status"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["status"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Create_By"]) ==true && drEach["Create_By"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["Create_By"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Create_Date"]) ==true && drEach["Create_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Create_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Update_By"]) ==true && drEach["Update_By"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["Update_By"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Update_Date"]) ==true && drEach["Update_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Update_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(")");
				String strSQLQuery = strBuilder.ToString();			
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note table");
											
				//delete debitnote detail
				String strSQL=" Delete From Debit_Note_Detail where ApplicationID='"+strAppID+"'";
				strSQL+=" and EnterpriseID='"+strEnterpriseID+"'";
				strSQL+=" and Debit_No='"+ strDebitNote_No +"' ";					
				dbCommandApp = dbConApp.CreateCommand(strSQL,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0012",iRowsAffected + " rows Deleted from Debit_Note_Detail table");
				Decimal dOLDDebitAmt = 0;
				for (int i=0;i<dsCNDetail.Tables[0].Rows.Count;i++)
				{
					DataRow drDND=dsCNDetail.Tables[0].Rows[i];	

					strBuilder.Remove(0,strBuilder.Length);	
				
					strBuilder = new StringBuilder();			
					strBuilder.Append(" Insert into Debit_Note_Detail(ApplicationID, EnterpriseID, Invoice_No, ");
					strBuilder.Append(" Debit_No, Consignment_No, Debit_Amt, Debit_Description,charge_code,reason_code) Values(");
					strBuilder.Append("'"+ strAppID+"','"+strEnterpriseID+"','"+strInvoiceNo+"','"+strDebitNote_No+"',");

					if(Utility.IsNotDBNull(drDND["Consignment_No"]) ==true && drDND["Consignment_No"].ToString() != "")
					{
						strConsignmentNo=Utility.ReplaceSingleQuote(drDND["Consignment_No"].ToString());						
						strBuilder.Append("'"+strConsignmentNo+"'");						
					}
					
					strBuilder.Append(",");
					if(Utility.IsNotDBNull(drDND["Amt"]) ==true && drDND["Amt"].ToString() != "")
					{
						dDebitAmt=(Decimal) drDND["Amt"];						
						if (drDND.HasVersion(DataRowVersion.Original))
						{					
							if (drDND["Amt", DataRowVersion.Original].ToString() !="")
								dOLDDebitAmt=(Decimal) drDND["Amt", DataRowVersion.Original];
						}	
						strBuilder.Append(dDebitAmt);
					}
					else
					{
						strBuilder.Append("null");
						dDebitAmt=0;
					}

					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["Description"]) ==true && drDND["Description"].ToString() != "")
					{
						strDebitDesc=(String) drDND["Description"];
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(strDebitDesc) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
		
					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["charge_code"]) ==true && drDND["charge_code"].ToString() != "")
					{						
						strBuilder.Append("'"+Utility.ReplaceSingleQuote(drDND["charge_code"].ToString()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	

					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["reason_code"]) ==true && drDND["reason_code"].ToString().Trim() != "")
					{						
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drDND["reason_code"].ToString().Trim()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
					strBuilder.Append(")");	
																								
					//NOW INSERT Debit_Note_Detail Information
					strSQLQuery = strBuilder.ToString();			
					dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note_Detail table");
					//dTotaDebitAmnt=0;															
				}
				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0014","App db Transaction committed.");
			}
					
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0006","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0009","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsAffected;
		}

		public static int AddDebitNoteDS(String strAppID, String strEnterpriseID,DataSet dsToInsert, ref String strDebitNote_No)
		{
			int iRowsAffected = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)				
					conApp.Close();
				
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}		
			
			String strInvoiceNo=null;
			String strConsignmentNo=null;
			Decimal dDebitAmt=0;
			String strDebitDesc=null;

			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{
				dbCommandApp = dbConApp.CreateCommand("",CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				//generate a new DebitNote Number and insert into  table		
				String strPrefix = "DND"+DateTime.Now.Year.ToString().Substring(2,2);
				int runnigNo = (int)Counter.GetNext(strAppID,strEnterpriseID,"DebitNote_Number",dbConApp,dbCommandApp);
				strDebitNote_No = strPrefix+runnigNo.ToString("000000");
				//strDebitNote_No = (int)Counter.GetNext(strAppID,strEnterpriseID,"DebitNote_Number");
			
				StringBuilder strBuilder = new StringBuilder();
				//Insert a Record into Debit_Note
				strBuilder.Append("insert into Debit_Note (ApplicationID,EnterpriseID,Invoice_No,Debit_No,Debit_Date,Remark,Deleted,status,Create_by,Create_date,Update_By,Update_Date) values (");
				strBuilder.Append("'"+strAppID+"','"+strEnterpriseID);

				int iRowCnt = dsToInsert.Tables[0].Rows.Count;
				if (iRowCnt<=0) 
					return iRowsAffected;

				DataRow drEach = dsToInsert.Tables[0].Rows[0];			
			
				strBuilder.Append("',");
				if(Utility.IsNotDBNull(drEach["Invoice_No"]) ==true && drEach["Invoice_No"].ToString() != "")
				{
					strInvoiceNo=Utility.ReplaceSingleQuote(drEach["Invoice_No"].ToString());
					strBuilder.Append("'"+strInvoiceNo+"'");							
				}
				else			
					strBuilder.Append("null");
		
				strBuilder.Append(",");
				if(strDebitNote_No != "" && strDebitNote_No != null)
					strBuilder.Append("'"+strDebitNote_No+"'");				
				else			
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Debit_Date"]) ==true && drEach["Debit_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Debit_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Remark"]) ==true && drEach["Remark"].ToString() != "")
					strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drEach["Remark"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",'N'");		//Deleted is 'N' default when inserting a new record

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["status"]) ==true && drEach["status"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["status"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Create_By"]) ==true && drEach["Create_By"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["Create_By"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Create_Date"]) ==true && drEach["Create_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Create_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Update_By"]) ==true && drEach["Update_By"].ToString() != "")
					strBuilder.Append("'"+Utility.ReplaceSingleQuote(drEach["Update_By"].ToString())+"'");				
				else
					strBuilder.Append("null");

				strBuilder.Append(",");
				if(Utility.IsNotDBNull(drEach["Update_Date"]) ==true && drEach["Update_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Update_Date"]);					
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));				
				}
				else
					strBuilder.Append("null");

				strBuilder.Append(")");
				String strSQLQuery = strBuilder.ToString();			
				//dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				//dbCommandApp.Connection = conApp;
				//dbCommandApp.Transaction = transactionApp;
				dbCommandApp.CommandText = strSQLQuery;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note table");
															
				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0014","App db Transaction committed.");
			}
					
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0006","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0009","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitNoteDS","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsAffected;
		}
		
		public static int ModifyDebitNoteDS(String strAppID, String strEnterpriseID,DataSet dsToModify, DataSet dsCNDetail,int index)
		{
			int iRowsAffected = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)				
					conApp.Close();
				
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}		
			
			String strInvoiceNo=null;
			String strConsignmentNo=null;
			String strDebitNo=null;
			Decimal dDebitAmt=0;
			Decimal dOLDDebitAmt=0;
			Decimal dActDebitAmt=0;
			Decimal dTotaDebitAmnt=0;
			String strDebitDesc=null;
			String strOldStat="";
			String strStat="";	
			String strRemark=null;
			int bookingNo=0;
			String statusUpdateBy = null;
			String strTmpCons = "";
			DataRow drEach = null;
			string oldstatusforcc = "";
			//Begin App Transaction
			
			try
			{
				transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
				if(transactionApp == null)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0005","transactionApp is null");
					throw new ApplicationException("Failed Begining a App Transaction..",null);
				}
				int iRowCnt = dsToModify.Tables[0].Rows.Count;
				if (iRowCnt<=0) 
					return iRowsAffected;

				StringBuilder strBuilder = new StringBuilder();
				StringBuilder strSumBuilder = new StringBuilder();

				drEach = dsToModify.Tables[0].Rows[index];	
			
				
				
				if(Utility.IsNotDBNull(drEach["Debit_No"]) ==true && drEach["Debit_No"].ToString() != "")
					strDebitNo=(String) drEach["Debit_No"];
				if(Utility.IsNotDBNull(drEach["Invoice_No"]) ==true && drEach["Invoice_No"].ToString() != "")
					strInvoiceNo=Utility.ReplaceSingleQuote(drEach["Invoice_No"].ToString());

				//Update Debit_Note Information
				strBuilder.Append("Update Debit_Note set Debit_Date =");				
				if(Utility.IsNotDBNull(drEach["Debit_Date"]) ==true && drEach["Debit_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Debit_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", remark = ");
				if(Utility.IsNotDBNull(drEach["remark"]) ==true && drEach["remark"].ToString() != "")
				{				
					strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drEach["remark"].ToString())+"'");
					strRemark = Utility.ReplaceSingleQuote(drEach["remark"].ToString());
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status = ");
				if(Utility.IsNotDBNull(drEach["status"]) ==true && drEach["status"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["status"].ToString())+"'");
					strStat = Utility.ReplaceSingleQuote(drEach["status"].ToString());
					strOldStat = Utility.ReplaceSingleQuote(drEach["status",System.Data.DataRowVersion.Original].ToString());
					oldstatusforcc = strOldStat;
				}
				else
				{
					strStat="";
					strOldStat="";
					strBuilder.Append("null");
				}

				strBuilder.Append(", update_by = ");
				if(Utility.IsNotDBNull(drEach["Update_By"]) ==true && drEach["Update_By"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["Update_By"].ToString())+"'");				
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", update_date = ");
				if(Utility.IsNotDBNull(drEach["Update_Date"]) ==true && drEach["Update_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Update_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status_update_by = ");
				if(Utility.IsNotDBNull(drEach["Status_Update_By"]) ==true && drEach["Status_Update_By"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["Status_Update_By"].ToString())+"'");	
					statusUpdateBy = Utility.ReplaceSingleQuote(drEach["Status_Update_By"].ToString());
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status_update_date = ");
				if(Utility.IsNotDBNull(drEach["Status_Update_Date"]) ==true && drEach["Status_Update_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Status_Update_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(" where ApplicationID = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID);
				strBuilder.Append("' and Invoice_No = '"+strInvoiceNo);
				strBuilder.Append("' and Debit_No='"+drEach["Debit_No"].ToString()+"'");

				String strSQLQuery = strBuilder.ToString();			
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note table");

				//delete debitnote detail
				String strSQL=" Delete From Debit_Note_Detail where ApplicationID='"+strAppID+"'";
				strSQL+=" and EnterpriseID='"+strEnterpriseID+"'";
				strSQL+=" and Debit_No='"+ strDebitNo +"' ";					
				dbCommandApp = dbConApp.CreateCommand(strSQL,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows Deleted from Debit_Note_Detail table");
				transactionApp.Commit();

				for (int i=0;i<dsCNDetail.Tables[0].Rows.Count;i++)
				{
					transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
					if(transactionApp == null)
					{
						Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0005","transactionApp is null");
						throw new ApplicationException("Failed Begining a App Transaction..",null);
					}
					DataRow drDND=dsCNDetail.Tables[0].Rows[i];	

					strBuilder.Remove(0,strBuilder.Length);	
				
					//strBuilder = new StringBuilder();			
					strBuilder.Append(" Insert into Debit_Note_Detail(ApplicationID, EnterpriseID, Invoice_No, ");
					strBuilder.Append(" Debit_No, Consignment_No, Debit_Amt, Debit_Description,charge_code,reason_code,remark) Values(");
					strBuilder.Append("'"+ strAppID+"','"+strEnterpriseID+"','"+strInvoiceNo+"','"+strDebitNo+"',");

					if(Utility.IsNotDBNull(drDND["Consignment_No"]) ==true && drDND["Consignment_No"].ToString() != "")
					{
						strConsignmentNo=Utility.ReplaceSingleQuote(drDND["Consignment_No"].ToString());						
						strBuilder.Append("'"+strConsignmentNo+"'");						
					}
					
					strBuilder.Append(",");
					if(Utility.IsNotDBNull(drDND["Amt"]) ==true && drDND["Amt"].ToString() != "")
					{
						dDebitAmt=(Decimal) drDND["Amt"];						
						if (drDND.HasVersion(DataRowVersion.Original))
						{					
							if (drDND["Amt", DataRowVersion.Original].ToString() !="")
								dOLDDebitAmt=(Decimal) drDND["Amt", DataRowVersion.Original];
						}	
						strBuilder.Append(dDebitAmt);
					}
					else
					{
						strBuilder.Append("null");
						dDebitAmt=0;
					}

					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["Description"]) ==true && drDND["Description"].ToString() != "")
					{
						strDebitDesc=(String) drDND["Description"];
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(strDebitDesc) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
		
					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["charge_code"]) ==true && drDND["charge_code"].ToString() != "")
					{						
						strBuilder.Append("'"+Utility.ReplaceSingleQuote(drDND["charge_code"].ToString()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	

					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["reason_code"]) ==true && drDND["reason_code"].ToString().Trim() != "")
					{						
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drDND["reason_code"].ToString().Trim()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
					strBuilder.Append(",");	

					if(Utility.IsNotDBNull(drDND["remark"]) ==true && drDND["remark"].ToString().Trim() != "")
					{						
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drDND["remark"].ToString().Trim()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
					strBuilder.Append(")");	
																								
					//NOW INSERT Debit_Note_Detail Information
					strSQLQuery = strBuilder.ToString();			
					dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note_Detail table");
					dTotaDebitAmnt=0;
					if(strStat.ToUpper() == "A")
					{
						//calculate total debit amnt
						strSumBuilder.Remove(0,strSumBuilder.Length);
						strSumBuilder.Append(" select isnull(sum(isnull(Debit_Amt,0)),0) debit_amt from debit_note_detail dt with(nolock) ");
						strSumBuilder.Append(" inner join debit_note dn with(nolock) on dt.debit_no = dn.debit_no and dt.applicationid = dn.applicationid and dt.enterpriseid = dn.enterpriseid ");
						strSumBuilder.Append(" where dt.Consignment_No='"+strConsignmentNo+"' and dt.applicationid = '"+strAppID+"' and dt.enterpriseid = '"+strEnterpriseID+"' and dn.status='A' ");
						dbCommandApp = dbConApp.CreateCommand(strSumBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsTotal = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(Utility.IsNotDBNull(dsTotal.Tables[0].Rows[0])&&dsTotal.Tables[0].Rows.Count>0)
						{
							try
							{
								dTotaDebitAmnt = (Decimal)dsTotal.Tables[0].Rows[0]["debit_amt"];
							}
							catch
							{
								dTotaDebitAmnt=0;
							}
						}
						// Update Shipment Table
						if(strOldStat.ToUpper()=="A")
						{
							strBuilder.Remove(0,strBuilder.Length);
							//dActDebitAmt=(dDebitAmt-dOLDDebitAmt);
							strBuilder.Append("Update shipment set Debit_Amt= "+dTotaDebitAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						else if(strOldStat.ToUpper()=="N")
						{
							strBuilder.Remove(0,strBuilder.Length);
							//dActDebitAmt=(dDebitAmt);
							strBuilder.Append("Update shipment set Debit_Amt= "+dTotaDebitAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						strBuilder.Remove(0,strBuilder.Length);
						strBuilder.Append("select isnull(inv.invoice_amt,0)+isnull(s.debit_amt,0)-isnull(s.credit_amt,0) total_cons_revenue, s.booking_no from invoice_detail inv with(nolock) inner join shipment s with(nolock) on inv.invoice_no=s.invoice_no and inv.Consignment_No=s.Consignment_No ");
						strBuilder.Append("where s.Consignment_No='"+strConsignmentNo+"' and s.invoice_no = '"+strInvoiceNo+"' ");
						strBuilder.Append(" and s.applicationid = '"+strAppID+"' and s.enterpriseid = '"+strEnterpriseID+"'");
						dbCommandApp = dbConApp.CreateCommand(strBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsResult = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(dsResult.Tables[0].Rows.Count>0)
							if(Utility.IsNotDBNull(dsResult.Tables[0].Rows[0]))
							{
								Decimal dRevenue = (Decimal)dsResult.Tables[0].Rows[0]["total_cons_revenue"];
								bookingNo = (int)dsResult.Tables[0].Rows[0]["booking_no"];
								strBuilder.Remove(0,strBuilder.Length);
								strBuilder.Append("Update shipment set total_cons_revenue = "+dRevenue+" where Consignment_No='"+strConsignmentNo+"' ");
								strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
								strSQLQuery = strBuilder.ToString();			
								dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
								dbCommandApp.Connection = conApp;
								dbCommandApp.Transaction = transactionApp;
								iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
								Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

							}
					}
					else if (strStat.ToUpper() == "C")
					{
						if(strOldStat.ToUpper()=="A")
						{
							//calculate total debit amnt
							strSumBuilder.Remove(0,strSumBuilder.Length);
							strSumBuilder.Append(" select isnull(sum(isnull(Debit_Amt,0)),0) debit_amt from debit_note_detail dt with(nolock) ");
							strSumBuilder.Append(" inner join debit_note dn with(nolock) on dt.debit_no = dn.debit_no and dt.applicationid = dn.applicationid and dt.enterpriseid = dn.enterpriseid ");
							strSumBuilder.Append(" where dt.Consignment_No='"+strConsignmentNo+"' and dt.applicationid = '"+strAppID+"' and dt.enterpriseid = '"+strEnterpriseID+"' and dn.status='A' ");
							dbCommandApp = dbConApp.CreateCommand(strSumBuilder.ToString(),CommandType.Text);
							dbCommandApp.Connection = conApp;
							DataSet dsTotal = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
							if(Utility.IsNotDBNull(dsTotal.Tables[0])&&dsTotal.Tables[0].Rows.Count>0)
							{
								try
								{
									dTotaDebitAmnt = (Decimal)dsTotal.Tables[0].Rows[0]["debit_amt"];
								}
								catch
								{
									dTotaDebitAmnt=0;
								}
							}

							// Update Shipment Table
							strBuilder.Remove(0,strBuilder.Length);
							//dActDebitAmt=(dDebitAmt-dOLDDebitAmt);
							strBuilder.Append("Update shipment set Debit_Amt="+dTotaDebitAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						strBuilder.Remove(0,strBuilder.Length);
						strBuilder.Append("select isnull(inv.invoice_amt,0)+isnull(s.debit_Amt,0)-isnull(s.credit_amt,0) total_cons_revenue, s.booking_no from invoice_detail inv with(nolock) inner join shipment s with(nolock) on inv.invoice_no=s.invoice_no and inv.Consignment_No=s.Consignment_No ");
						strBuilder.Append("where s.Consignment_No='"+strConsignmentNo+"' and s.invoice_no = '"+strInvoiceNo+"' ");
						strBuilder.Append(" and s.applicationid = '"+strAppID+"' and s.enterpriseid = '"+strEnterpriseID+"'");
						dbCommandApp = dbConApp.CreateCommand(strBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsResult = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(dsResult.Tables[0].Rows.Count>0)
							if(Utility.IsNotDBNull(dsResult.Tables[0].Rows[0]))
							{
								Decimal dRevenue = (Decimal)dsResult.Tables[0].Rows[0]["total_cons_revenue"];
								bookingNo = (int)dsResult.Tables[0].Rows[0]["booking_no"];
								strBuilder.Remove(0,strBuilder.Length);
								strBuilder.Append("Update shipment set total_cons_revenue = "+dRevenue+" where Consignment_No='"+strConsignmentNo+"' ");
								strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
								strSQLQuery = strBuilder.ToString();			
								dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
								dbCommandApp.Connection = conApp;
								dbCommandApp.Transaction = transactionApp;
								iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
								Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

							}
					}
					transactionApp.Commit();
					if(strTmpCons!=strConsignmentNo)
					{
						strTmpCons = strConsignmentNo;
						if(strStat == "A")
						{
							strRemark = " DN #: "+strDebitNo+" amount THB: "+Convert.ToDecimal(dsCNDetail.Tables[0].Compute("sum(Amt)","Consignment_No='"+strConsignmentNo+"'")).ToString("####.00");
							ShipmentTrackingMgrDAL.UpdateShipmentStatus(strAppID,strEnterpriseID,bookingNo,DateTime.Now,strConsignmentNo,"DNA",
								strRemark,statusUpdateBy);
						}
						else if(strStat == "C")
						{
							strRemark = " DN #: "+strDebitNo+" amount THB: "+Convert.ToDecimal(dsCNDetail.Tables[0].Compute("sum(Amt)","Consignment_No='"+strConsignmentNo+"'")).ToString("####.00");
							if(strOldStat.ToUpper()=="A")
							{
								ShipmentTrackingMgrDAL.UpdateShipmentStatus(strAppID,strEnterpriseID,bookingNo,DateTime.Now,strConsignmentNo,"DNC",
									strRemark,statusUpdateBy);
							}
						}
					}
										
				}
				
				//commit the transaction
				
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0014","App db Transaction committed.");
			}					
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0006","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0009","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}			
			return iRowsAffected;
		}

		public static int ModifyDebitNoteDS(String strAppID, String strEnterpriseID,DataSet dsToModify, DataSet dsCNDetail)
		{
			int iRowsAffected = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)				
					conApp.Close();
				
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}		
			
			String strInvoiceNo=null;
			String strConsignmentNo=null;
			String strDebitNo=null;
			Decimal dDebitAmt=0;
			Decimal dOLDDebitAmt=0;
			//Decimal dActDebitAmt=0;
			Decimal dTotaDebitAmnt=0;
			String strDebitDesc=null;
			String strOldStat="";
			String strStat="";			
			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			
			try
			{
				int iRowCnt = dsToModify.Tables[0].Rows.Count;
				if (iRowCnt<=0) 
					return iRowsAffected;

				StringBuilder strBuilder = new StringBuilder();
				StringBuilder strSumBuilder = new StringBuilder();

				DataRow drEach = dsToModify.Tables[0].Rows[0];				
				if(Utility.IsNotDBNull(drEach["Debit_No"]) ==true && drEach["Debit_No"].ToString() != "")
					strDebitNo=(String) drEach["Debit_No"];
				if(Utility.IsNotDBNull(drEach["Invoice_No"]) ==true && drEach["Invoice_No"].ToString() != "")
					strInvoiceNo=Utility.ReplaceSingleQuote(drEach["Invoice_No"].ToString());

				//Update Debit_Note Information
				strBuilder.Append("Update Debit_Note set Debit_Date =");				
				if(Utility.IsNotDBNull(drEach["Debit_Date"]) ==true && drEach["Debit_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Debit_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", remark = ");
				if(Utility.IsNotDBNull(drEach["remark"]) ==true && drEach["remark"].ToString() != "")
				{				
					strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drEach["remark"].ToString())+"'");				
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status = ");
				if(Utility.IsNotDBNull(drEach["status"]) ==true && drEach["status"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["status"].ToString())+"'");
					strStat = Utility.ReplaceSingleQuote(drEach["status"].ToString());
					strOldStat = Utility.ReplaceSingleQuote(drEach["status",System.Data.DataRowVersion.Original].ToString());
				}
				else
				{
					strStat="";
					strOldStat="";
					strBuilder.Append("null");
				}

				strBuilder.Append(", update_by = ");
				if(Utility.IsNotDBNull(drEach["Update_By"]) ==true && drEach["Update_By"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["Update_By"].ToString())+"'");				
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", update_date = ");
				if(Utility.IsNotDBNull(drEach["Update_Date"]) ==true && drEach["Update_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Update_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status_update_by = ");
				if(Utility.IsNotDBNull(drEach["Status_Update_By"]) ==true && drEach["Status_Update_By"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["Status_Update_By"].ToString())+"'");				
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status_update_date = ");
				if(Utility.IsNotDBNull(drEach["Status_Update_Date"]) ==true && drEach["Status_Update_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Status_Update_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(" where ApplicationID = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID);
				strBuilder.Append("' and Invoice_No = '"+strInvoiceNo);
				strBuilder.Append("' and Debit_No='"+drEach["Debit_No"].ToString()+"'");

				String strSQLQuery = strBuilder.ToString();			
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note table");

				//delete debitnote detail
				String strSQL=" Delete From Debit_Note_Detail where ApplicationID='"+strAppID+"'";
				strSQL+=" and EnterpriseID='"+strEnterpriseID+"'";
				strSQL+=" and Debit_No='"+ strDebitNo +"' ";					
				dbCommandApp = dbConApp.CreateCommand(strSQL,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows Deleted from Debit_Note_Detail table");

				for (int i=0;i<dsCNDetail.Tables[0].Rows.Count;i++)
				{
					DataRow drDND=dsCNDetail.Tables[0].Rows[i];	

					strBuilder.Remove(0,strBuilder.Length);	
				
					strBuilder = new StringBuilder();			
					strBuilder.Append(" Insert into Debit_Note_Detail(ApplicationID, EnterpriseID, Invoice_No, ");
					strBuilder.Append(" Debit_No, Consignment_No, Debit_Amt, Debit_Description,charge_code,reason_code,remark) Values(");
					strBuilder.Append("'"+ strAppID+"','"+strEnterpriseID+"','"+strInvoiceNo+"','"+strDebitNo+"',");

					if(Utility.IsNotDBNull(drDND["Consignment_No"]) ==true && drDND["Consignment_No"].ToString() != "")
					{
						strConsignmentNo=Utility.ReplaceSingleQuote(drDND["Consignment_No"].ToString());						
						strBuilder.Append("'"+strConsignmentNo+"'");						
					}
					
					strBuilder.Append(",");
					if(Utility.IsNotDBNull(drDND["Amt"]) ==true && drDND["Amt"].ToString() != "")
					{
						dDebitAmt=(Decimal) drDND["Amt"];						
						if (drDND.HasVersion(DataRowVersion.Original))
						{					
							if (drDND["Amt", DataRowVersion.Original].ToString() !="")
								dOLDDebitAmt=(Decimal) drDND["Amt", DataRowVersion.Original];
						}	
						strBuilder.Append(dDebitAmt);
					}
					else
					{
						strBuilder.Append("null");
						dDebitAmt=0;
					}

					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["Description"]) ==true && drDND["Description"].ToString() != "")
					{
						strDebitDesc=(String) drDND["Description"];
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(strDebitDesc) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
		
					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["charge_code"]) ==true && drDND["charge_code"].ToString() != "")
					{						
						strBuilder.Append("'"+Utility.ReplaceSingleQuote(drDND["charge_code"].ToString()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	

					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["reason_code"]) ==true && drDND["reason_code"].ToString().Trim() != "")
					{						
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drDND["reason_code"].ToString().Trim()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
										strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["remark"]) ==true && drDND["remark"].ToString().Trim() != "")
					{						
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drDND["remark"].ToString().Trim()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
					strBuilder.Append(")");	
																								
					//NOW INSERT Debit_Note_Detail Information
					strSQLQuery = strBuilder.ToString();			
					dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
					dbCommandApp.Connection = conApp;
					dbCommandApp.Transaction = transactionApp;
					iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note_Detail table");
					dTotaDebitAmnt=0;
					if(strStat.ToUpper() == "A")
					{
						//calculate total debit amnt
						strSumBuilder.Remove(0,strSumBuilder.Length);
						strSumBuilder.Append(" select isnull(sum(isnull(Debit_Amt,0)),0) debit_amt from debit_note_detail dt with(nolock) ");
						strSumBuilder.Append(" inner join debit_note dn with(nolock) on dt.debit_no = dn.debit_no and dt.applicationid = dn.applicationid and dt.enterpriseid = dn.enterpriseid ");
						strSumBuilder.Append(" where dt.Consignment_No='"+strConsignmentNo+"' and dt.applicationid = '"+strAppID+"' and dt.enterpriseid = '"+strEnterpriseID+"' and dn.status='A' ");
						dbCommandApp = dbConApp.CreateCommand(strSumBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsTotal = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(Utility.IsNotDBNull(dsTotal.Tables[0].Rows[0])&&dsTotal.Tables[0].Rows.Count>0)
						{
							try
							{
								dTotaDebitAmnt = (Decimal)dsTotal.Tables[0].Rows[0]["debit_amt"];
							}
							catch
							{
								dTotaDebitAmnt=0;
							}
						}
						// Update Shipment Table
						if(strOldStat.ToUpper()=="A")
						{
							strBuilder.Remove(0,strBuilder.Length);
							//dActDebitAmt=(dDebitAmt-dOLDDebitAmt);
							strBuilder.Append("Update shipment set Debit_Amt= "+dTotaDebitAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						else if(strOldStat.ToUpper()=="N")
						{
							strBuilder.Remove(0,strBuilder.Length);
							//dActDebitAmt=(dDebitAmt);
							strBuilder.Append("Update shipment set Debit_Amt= "+dTotaDebitAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						strBuilder.Remove(0,strBuilder.Length);
						strBuilder.Append("select isnull(inv.invoice_amt,0)+isnull(s.debit_Amt,0)-isnull(s.credit_amt,0) total_cons_revenue from invoice_detail inv with(nolock) inner join shipment s with(nolock) on inv.invoice_no=s.invoice_no and inv.Consignment_No=s.Consignment_No ");
						strBuilder.Append("where s.Consignment_No='"+strConsignmentNo+"' and s.invoice_no = '"+strInvoiceNo+"' ");
						strBuilder.Append(" and s.applicationid = '"+strAppID+"' and s.enterpriseid = '"+strEnterpriseID+"'");
						dbCommandApp = dbConApp.CreateCommand(strBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsResult = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(dsResult.Tables[0].Rows.Count>0)
							if(Utility.IsNotDBNull(dsResult.Tables[0].Rows[0]))
							{
								Decimal dRevenue = (Decimal)dsResult.Tables[0].Rows[0]["total_cons_revenue"];
								strBuilder.Remove(0,strBuilder.Length);
								strBuilder.Append("Update shipment set total_cons_revenue = "+dRevenue+" where Consignment_No='"+strConsignmentNo+"' ");
								strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
								strSQLQuery = strBuilder.ToString();			
								dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
								dbCommandApp.Connection = conApp;
								dbCommandApp.Transaction = transactionApp;
								iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
								Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

							}
					}
					else if (strStat.ToUpper() == "C")
					{
						if(strOldStat.ToUpper()=="A")
						{
							//calculate total debit amnt
							strSumBuilder.Remove(0,strSumBuilder.Length);
							strSumBuilder.Append(" select isnull(sum(isnull(Debit_Amt,0)),0) debit_amt from debit_note_detail dt with(nolock) ");
							strSumBuilder.Append(" inner join debit_note dn with(nolock) on dt.debit_no = dn.debit_no and dt.applicationid = dn.applicationid and dt.enterpriseid = dn.enterpriseid ");
							strSumBuilder.Append(" where dt.Consignment_No='"+strConsignmentNo+"' and dt.applicationid = '"+strAppID+"' and dt.enterpriseid = '"+strEnterpriseID+"' and dn.status='A' ");
							dbCommandApp = dbConApp.CreateCommand(strSumBuilder.ToString(),CommandType.Text);
							dbCommandApp.Connection = conApp;
							DataSet dsTotal = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
							if(Utility.IsNotDBNull(dsTotal.Tables[0].Rows[0])&&dsTotal.Tables[0].Rows.Count>0)
							{
								try
								{
									dTotaDebitAmnt = (Decimal)dsTotal.Tables[0].Rows[0]["debit_amt"];
								}
								catch
								{
									dTotaDebitAmnt=0;
								}
							}

							// Update Shipment Table
							strBuilder.Remove(0,strBuilder.Length);
							//dActDebitAmt=(dDebitAmt-dOLDDebitAmt);
							strBuilder.Append("Update shipment set Debit_Amt="+dTotaDebitAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						strBuilder.Remove(0,strBuilder.Length);
						strBuilder.Append("select isnull(inv.invoice_amt,0)+isnull(s.debit_amt,0)-isnull(s.credit_amt,0) total_cons_revenue from invoice_detail inv with(nolock) inner join shipment s with(nolock) on inv.invoice_no=s.invoice_no and inv.Consignment_No=s.Consignment_No ");
						strBuilder.Append("where s.Consignment_No='"+strConsignmentNo+"' and s.invoice_no = '"+strInvoiceNo+"' ");
						strBuilder.Append(" and s.applicationid = '"+strAppID+"' and s.enterpriseid = '"+strEnterpriseID+"'");
						dbCommandApp = dbConApp.CreateCommand(strBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsResult = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(dsResult.Tables[0].Rows.Count>0)
							if(Utility.IsNotDBNull(dsResult.Tables[0].Rows[0]))
							{
								Decimal dRevenue = (Decimal)dsResult.Tables[0].Rows[0]["total_cons_revenue"];
								strBuilder.Remove(0,strBuilder.Length);
								strBuilder.Append("Update shipment set total_cons_revenue = "+dRevenue+" where Consignment_No='"+strConsignmentNo+"' ");
								strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
								strSQLQuery = strBuilder.ToString();			
								dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
								dbCommandApp.Connection = conApp;
								dbCommandApp.Transaction = transactionApp;
								iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
								Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

							}
					}
										
				}
				
				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0014","App db Transaction committed.");
			}					
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0006","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0009","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}			
			return iRowsAffected;
		}


		public static int DeleteDebitNoteDS(String strAppID, String strEnterpriseID,String strInvoiceNo, String strDebit_No)
		{
			int iRowsAffected = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitNoteDS","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitNoteDS","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitNoteDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)				
					conApp.Close();
				
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitNoteDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}		

			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitNoteDS","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				//Logical DELETE in Debit_Note Table
				StringBuilder strBuilder = new StringBuilder();			
				strBuilder.Append("Update Debit_Note set Deleted='Y' where Invoice_No='"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' and Debit_No='");
				strBuilder.Append(strDebit_No +"' and ApplicationId='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"'");
			
				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitNoteDS","ERR0012",iRowsAffected + " rows Logically Deleted from Debit_Note table");				

				//DELETE from Debit_Note_Detail table
				strBuilder.Remove(0,strBuilder.Length);
				strBuilder.Append(" Delete from Debit_Note_Detail where applicationid = '"+strAppID+"' ");
				strBuilder.Append(" and enterpriseid = '"+strEnterpriseID+"' and Invoice_No = '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' ");
				strBuilder.Append(" and Debit_No = '"+strDebit_No+"'");

				strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitNoteDS","ERR0012",iRowsAffected + " rows Deleted from Debit_Note_Detail table");				

				// UPDATE  Debit_Amt=0 for ALL CONSIGNMNETS of the INVOICE found in SHIPMENT 
				strBuilder.Remove(0,strBuilder.Length);
				strBuilder.Append(" UPDATE Shipment SH SET Debit_Amt=0 where SH.Consignment_No in ( ");
				strBuilder.Append(" Select IND.Consignment_No from Invoice_Detail IND Where SH.ApplicationID=IND.ApplicationID ");
				strBuilder.Append(" and SH.EnterpriseID=IND.EnterpriseID and IND.Invoice_No='"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' ");
				strBuilder.Append(" and SH.Consignment_No =IND.Consignment_No ) and SH.Applicationid = '"+strAppID+"' and SH.Enterpriseid = '"+strEnterpriseID+"'");		

				strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitNoteDS","ERR0012",iRowsAffected + " rows Deleted from Debit_Note_Detail table");				

				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitDetailDS","ERR0014","App db Transaction committed.");
			}
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitDetailDS","ERR0006","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitDetailDS","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitDetailDS","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitDetailDS","ERR0009","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitDetailDS","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","AddDebitDetailDS","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}			
			return iRowsAffected;
		}

		/*public static DataSet GetDebitDetailDS(String strAppID, String strEnterpriseID, String strInvoiceNo, String strDebitNote)
		{
			DataSet dsDebitDetail = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DebitDebitMgrDAL","DebitDebitMgrDAL","GetDebitDetailDS","EDB101","DbConnection object is null!!");
				return dsDebitDetail;
			}
			StringBuilder strBuilder = new StringBuilder();		

			strBuilder.Append(" Select DND.Invoice_No,DND.Debit_No,DND.Consignment_No,DND.charge_code, DND.reason_code,SH.Invoice_Amt, DND.Debit_Amt as Amt,DND.Debit_Description as Description");
			strBuilder.Append(" From Debit_Note_Detail DND, Shipment SH Where DND.ApplicationID=SH.ApplicationID and ");
			strBuilder.Append(" DND.Enterpriseid =SH.Enterpriseid  and DND.Consignment_No=SH.Consignment_No and ");
			strBuilder.Append(" DND.Applicationid = '"+strAppID+"' and DND.Enterpriseid = '"+strEnterpriseID+"' and ");
			strBuilder.Append(" DND.Invoice_No= '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' and DND.Debit_No='"+strDebitNote+"'"); 
			
			dsDebitDetail = (DataSet) dbCon.ExecuteQuery(strBuilder.ToString(),ReturnType.DataSetType);
			return  dsDebitDetail ;

		}*/

		/*public static SessionDS GetDebitDetailDS(String strAppID, String strEnterpriseID, String strInvoiceNo, String strDebitNote, int iCurrent, int iDSRecSize)
		{
			SessionDS dsDebitDetail = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DebitDebitMgrDAL","DebitDebitMgrDAL","GetDebitDetailDS","EDB101","DbConnection object is null!!");
				return dsDebitDetail;
			}
			StringBuilder strBuilder = new StringBuilder();		

			strBuilder.Append(" Select DND.Invoice_No,DND.Debit_No,DND.Consignment_No,DND.charge_code, DND.reason_code,SH.Invoice_Amt, DND.Debit_Amt as Amt,DND.Debit_Description as Description");
			strBuilder.Append(" From Debit_Note_Detail DND, Shipment SH Where DND.ApplicationID=SH.ApplicationID and ");
			strBuilder.Append(" DND.Enterpriseid =SH.Enterpriseid  and DND.Consignment_No=SH.Consignment_No and ");
			strBuilder.Append(" DND.Applicationid = '"+strAppID+"' and DND.Enterpriseid = '"+strEnterpriseID+"' and ");
			strBuilder.Append(" DND.Invoice_No= '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' and DND.Debit_No='"+strDebitNote+"'"); 
			
			dsDebitDetail = (SessionDS) dbCon.ExecuteQuery(strBuilder.ToString(),iCurrent,iDSRecSize,"Debit_Note_Detail");
			return  dsDebitDetail ;

		}*/
		#region Comment by Sompote 2010-05-11 (Fix Database Lock)
		/*
		public static DataSet GetDebitDetailDS(String strAppID, String strEnterpriseID, String strInvoiceNo, String strDebitNote)
		{
			DataSet dsDebitDetail = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DebitDebitMgrDAL","DebitDebitMgrDAL","GetDebitDetailDS","EDB101","DbConnection object is null!!");
				return dsDebitDetail;
			}
			StringBuilder strBuilder = new StringBuilder();		
	
			strBuilder.Append(" Select Invoice_No,Debit_No,Consignment_No,charge_code,reason_code, 0.00 as Invoice_Amt,Debit_Amt as Amt,Debit_Description as Description, 1 as flag");
			strBuilder.Append(" From Debit_Note_Detail ");
			strBuilder.Append(" Where Applicationid = '"+strAppID+"' and Enterpriseid = '"+strEnterpriseID+"' and ");
			strBuilder.Append(" Invoice_No= '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' and Debit_No='"+strDebitNote+"'");
			
			dsDebitDetail = (DataSet) dbCon.ExecuteQuery(strBuilder.ToString(),ReturnType.DataSetType);
			Decimal amnt=0;
			for(int i=0;i<dsDebitDetail.Tables[0].Rows.Count;i++)
			{
			amnt = GetCalcuratedInvoiceAmt(strAppID,strEnterpriseID,dsDebitDetail.Tables[0].Rows[i]["Consignment_No"].ToString(),strInvoiceNo,dsDebitDetail.Tables[0].Rows[i]["charge_code"].ToString());
//				if(amnt==null)
//				{
//					amnt="0";
//				}
//				else
//				{
//					if(amnt.Trim()=="")
//					{
//						amnt="0";
//					}
//					dsDebitDetail.Tables[0].Rows[i]["Invoice_Amt"]= Convert.ToDecimal(amnt);
//				}
				dsDebitDetail.Tables[0].Rows[i]["Invoice_Amt"]=amnt; //add new
			}
			return  dsDebitDetail ;

		}

		*/
		#endregion

		public static DataSet GetDebitDetailDS(String strAppID, String strEnterpriseID, String strInvoiceNo, String strDebitNote)
		{
			DataSet dsDebitDetail = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DebitDebitMgrDAL","DebitDebitMgrDAL","GetDebitDetailDS","EDB101","DbConnection object is null!!");
				return dsDebitDetail;
			}			
			return  GetDebitDetailDS( strAppID,  strEnterpriseID,  strInvoiceNo,  strDebitNote, dbCon) ;
		}

		public static DataSet GetDebitDetailDS(String strAppID, String strEnterpriseID, String strInvoiceNo, String strDebitNote, DbConnection dbCon)
		{			
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","GetDebitDetailDS","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","GetDebitDetailDS","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","GetDebitDetailDS","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	


			return  GetDebitDetailDS( strAppID,  strEnterpriseID,  strInvoiceNo,  strDebitNote,dbCon,dbCmd) ;
		}

		public static DataSet GetDebitDetailDS(String strAppID, String strEnterpriseID, String strInvoiceNo, String strDebitNote, DbConnection dbCon,IDbCommand dbCmd)
		{
			DataSet dsDebitDetail = null;
		
			StringBuilder strBuilder = new StringBuilder();		
	
			strBuilder.Append(" Select Invoice_No,Debit_No,Consignment_No,charge_code,reason_code, 0.00 as Invoice_Amt,Debit_Amt as Amt,Debit_Description as Description, 1 as flag,remark");
			strBuilder.Append(" From Debit_Note_Detail ");
			strBuilder.Append(" Where Applicationid = '"+strAppID+"' and Enterpriseid = '"+strEnterpriseID+"' and ");
			strBuilder.Append(" Invoice_No= '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' and Debit_No='"+strDebitNote+"'");
			
			dbCmd.CommandText = strBuilder.ToString();

			//dsDebitDetail = (DataSet) dbCon.ExecuteQuery(strBuilder.ToString(),ReturnType.DataSetType);
			dsDebitDetail = (DataSet) dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			
			for(int i=0;i<dsDebitDetail.Tables[0].Rows.Count;i++)
			{
				Decimal amnt = GetCalcuratedInvoiceAmt(strAppID,strEnterpriseID,dsDebitDetail.Tables[0].Rows[i]["Consignment_No"].ToString(),strInvoiceNo,dsDebitDetail.Tables[0].Rows[i]["charge_code"].ToString(),dbCon,dbCmd);
				
				dsDebitDetail.Tables[0].Rows[i]["Invoice_Amt"]=amnt; //add new
			}
			return  dsDebitDetail ;
		}



		public static SessionDS GetDebitDetailDS(String strAppID, String strEnterpriseID, String strInvoiceNo, String strDebitNote, int iCurrent, int iDSRecSize)
		{
			SessionDS dsDebitDetail = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DebitDebitMgrDAL","DebitDebitMgrDAL","GetDebitDetailDS","EDB101","DbConnection object is null!!");
				return dsDebitDetail;
			}
			StringBuilder strBuilder = new StringBuilder();		
			strBuilder.Append(" Select Invoice_No,Debit_No,Consignment_No,charge_code,reason_code, 0 as Invoice_Amt,Debit_Amt,Debit_Description as Description, 1 as flag,remark");
			strBuilder.Append(" From Debit_Note_Detail ");
			strBuilder.Append(" Where Applicationid = '"+strAppID+"' and Enterpriseid = '"+strEnterpriseID+"' and ");
			strBuilder.Append(" Invoice_No= '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' and Debit_No='"+strDebitNote+"'");
			/*strBuilder.Append(" Select DND.Invoice_No,DND.Debit_No,DND.Consignment_No,DND.charge_code, DND.reason_code,SH.Invoice_Amt, DND.Debit_Amt as Amt,DND.Debit_Description as Description");
			strBuilder.Append(" From Debit_Note_Detail DND, Shipment SH Where DND.ApplicationID=SH.ApplicationID and ");
			strBuilder.Append(" DND.Enterpriseid =SH.Enterpriseid  and DND.Consignment_No=SH.Consignment_No and ");
			strBuilder.Append(" DND.Applicationid = '"+strAppID+"' and DND.Enterpriseid = '"+strEnterpriseID+"' and ");
			strBuilder.Append(" DND.Invoice_No= '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' and DND.Debit_No='"+strDebitNote+"'"); */
			
			dsDebitDetail = (SessionDS) dbCon.ExecuteQuery(strBuilder.ToString(),iCurrent,iDSRecSize,"Debit_Note_Detail");
			return  dsDebitDetail ;

		}

		public static int DeleteDebitDetailDS(String strAppID, String strEnterpriseID,String strInvoiceNo, String strDebitNo,String strConsignmentNo, Decimal dDebitAmount)
		{
			int iRowsAffected = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitDetailDS","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitDetailDS","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitDetailDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)				
					conApp.Close();
				
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitDetailDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}		
			//Begin App Transaction
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitDetailDS","ERR0005","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			try
			{

				StringBuilder strBuilder = new StringBuilder();			
				strBuilder.Append("Delete from Debit_Note_Detail where applicationid = '"+strAppID+"' ");
				strBuilder.Append(" and enterpriseid = '"+strEnterpriseID+"' and Invoice_No = '"+Utility.ReplaceSingleQuote(strInvoiceNo)+"' ");
				strBuilder.Append(" and Debit_No = '"+strDebitNo+"' and Consignment_No='"+Utility.ReplaceSingleQuote(strConsignmentNo)+"'");
				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;		
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitDetailDS","SDM001",iRowsAffected+" rows deleted from  Exception_Code table");

				strBuilder.Remove(0,strBuilder.Length);
				strBuilder.Append("Update shipment set Debit_Amt=Debit_Amt - "+dDebitAmount+"  where Consignment_No='"+Utility.ReplaceSingleQuote(strConsignmentNo)+"' ");
				strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
				strSQLQuery = strBuilder.ToString();			
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitDetailDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitDetailDS","ERR0014","App db Transaction committed.");
			}
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitDetailDS","ERR0006","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitDetailDS","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitDetailDS","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitDetailDS","ERR0009","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitDetailDS","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","DeleteDebitDetailDS","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;		
		}

		// Credit Debit Tracking methods

		public static SessionDS GetEmptyCDDetailDS(int iNumRows)
		{
			DataTable dtDebitDetail = new DataTable();
 
			dtDebitDetail.Columns.Add(new DataColumn("credit_no", typeof(string)));			
			dtDebitDetail.Columns.Add(new DataColumn("invoice_no", typeof(string)));
			dtDebitDetail.Columns.Add(new DataColumn("Consignment_no", typeof(string)));
			dtDebitDetail.Columns.Add(new DataColumn("charge_code", typeof(string)));
			dtDebitDetail.Columns.Add(new DataColumn("Invoice_Amt", typeof(decimal)));						
			dtDebitDetail.Columns.Add(new DataColumn("reason_code", typeof(string)));
			dtDebitDetail.Columns.Add(new DataColumn("Amt", typeof(decimal)));						
			dtDebitDetail.Columns.Add(new DataColumn("Description", typeof(string)));			
			dtDebitDetail.Columns.Add(new DataColumn("remark", typeof(string)));
			
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtDebitDetail.NewRow();
				dtDebitDetail.Rows.Add(drEach);
			}

			DataSet dsDebitDtl = new DataSet();
			dsDebitDtl.Tables.Add(dtDebitDetail);

			dsDebitDtl.Tables[0].Columns["Consignment_no"].Unique = true; //Checks duplicate records..
			//			dsDebitDtl.Tables[0].Columns["Consignment_no"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsDebitDtl;
			sessionDS.DataSetRecSize = dsDebitDtl.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
		}

		public static SessionDS GetCreditDebitList(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize, DataSet dsCD)
		{
			SessionDS sessionDS = null;			

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","GetCreditDebitList","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			DataRow drCurr=dsCD.Tables[0].Rows[0];

			String sSearchType=""; 
			String sCreditDebitNo="";
			String sInvoiceNo="";
			String sConsignmentNo="";

			String sDateType="";			
			String sdtFrom="";			
			String sdtTo="";

			String sPayerType="";
			String sPayerID="";
			String sPayerName="";

			if (Utility.IsNotDBNull(drCurr["Search_Type"])==true && drCurr["Search_Type"].ToString() !="")
				sSearchType=drCurr["Search_Type"].ToString();
			if (Utility.IsNotDBNull(drCurr["Date_Type"])==true && drCurr["Date_Type"].ToString() !="")
				sDateType=drCurr["Date_Type"].ToString();
			if (Utility.IsNotDBNull(drCurr["Payer_Type"])==true && drCurr["Payer_Type"].ToString() !="")
				sPayerType=drCurr["Payer_Type"].ToString();
			if (Utility.IsNotDBNull(drCurr["Credit_Debit_No"])==true && drCurr["Credit_Debit_No"].ToString() !="")
				sCreditDebitNo=drCurr["Credit_Debit_No"].ToString();
			if (Utility.IsNotDBNull(drCurr["Invoice_No"])==true && drCurr["Invoice_No"].ToString() !="")
				sInvoiceNo=Utility.ReplaceSingleQuote(drCurr["Invoice_No"].ToString());
			if (Utility.IsNotDBNull(drCurr["From_Date"])==true && drCurr["From_Date"].ToString() !="")
			{
				DateTime dtFrom=System.Convert.ToDateTime(drCurr["From_Date"]);
				sdtFrom=Utility.DateFormat(strAppID,strEnterpriseID,dtFrom, DTFormat.Date);
			}
			if (Utility.IsNotDBNull(drCurr["To_Date"])==true && drCurr["To_Date"].ToString() !="")
			{
				DateTime dtTo=System.Convert.ToDateTime(drCurr["To_Date"]);
				sdtTo=Utility.DateFormat(strAppID,strEnterpriseID, dtTo ,DTFormat.Date);
			}
			if (Utility.IsNotDBNull(drCurr["Consignment_No"])==true && drCurr["Consignment_No"].ToString() !="")
				sConsignmentNo=Utility.ReplaceSingleQuote(drCurr["Consignment_No"].ToString());
			if (Utility.IsNotDBNull(drCurr["Payer_ID"])==true && drCurr["Payer_ID"].ToString() !="")
				sPayerID=Utility.ReplaceSingleQuote(drCurr["Payer_ID"].ToString());
			if (Utility.IsNotDBNull(drCurr["Payer_Name"])==true && drCurr["Payer_Name"].ToString() !="")
				sPayerName=Utility.ReplaceSingleQuote(drCurr["Payer_Name"].ToString());

			StringBuilder strBuilder = new StringBuilder();	

			strBuilder.Append(" Select distinct I.Invoice_Date, InD.Invoice_Amt, ");
			if (sSearchType=="C")
			{
				strBuilder.Append(" CN.Invoice_No, CND.Consignment_No, CN.Credit_No as Note_No From Invoice I, ");
				strBuilder.Append(" Invoice_Detail InD, Credit_Note CN, Credit_Note_Detail CND where ");
				
				strBuilder.Append(" I.EnterpriseID=InD.EnterpriseID and I.ApplicationID=InD.ApplicationID and  ");
				strBuilder.Append(" I.Invoice_No=InD.Invoice_No and ");

				strBuilder.Append(" CN.EnterpriseID=I.EnterpriseID and CN.ApplicationID=I.ApplicationID and ");
				strBuilder.Append(" CN.Invoice_No=I.Invoice_No and ");

				strBuilder.Append(" CN.EnterpriseID=CND.EnterpriseID and CN.ApplicationID=CND.ApplicationID and ");
				strBuilder.Append(" CN.Invoice_No=CND.Invoice_No and CND.Consignment_No=InD.Consignment_No ");

				strBuilder.Append(" and CN.Credit_No like '%"+sCreditDebitNo+"%' ");
				strBuilder.Append(" and CND.Consignment_No like '%"+sConsignmentNo+"%' ");
				strBuilder.Append(" and CN.Invoice_No like '%"+sInvoiceNo+"%' ");
				strBuilder.Append(" and CN.Applicationid = '"+strAppID+"' and CN.Enterpriseid = '"+strEnterpriseID+"' ");

				if (sDateType=="C" && (sdtFrom !="" && sdtTo !=""))
					strBuilder.Append(" and CN.Credit_Date between "+sdtFrom+ " and "+sdtTo+" ");
			}

			else if (sSearchType=="D")
			{
				strBuilder.Append(" DN.Invoice_No, DND.Consignment_No, DN.Debit_No as Note_No From Invoice I, ");
				strBuilder.Append(" Invoice_Detail InD, Debit_Note DN, Debit_Note_Detail DND where ");
				
				strBuilder.Append(" I.EnterpriseID=InD.EnterpriseID and I.ApplicationID=InD.ApplicationID and  ");
				strBuilder.Append(" I.Invoice_No=InD.Invoice_No and ");

				strBuilder.Append(" DN.EnterpriseID=I.EnterpriseID and DN.ApplicationID=I.ApplicationID and ");
				strBuilder.Append(" DN.Invoice_No=I.Invoice_No and ");

				strBuilder.Append(" DN.EnterpriseID=DND.EnterpriseID and DN.ApplicationID=DND.ApplicationID and ");
				strBuilder.Append(" DN.Invoice_No=DND.Invoice_No and DND.Consignment_No=InD.Consignment_No ");

				strBuilder.Append(" and DN.Debit_No like '%"+sCreditDebitNo+"%' ");
				strBuilder.Append(" and DND.Consignment_No like '%"+sConsignmentNo+"%' ");
				strBuilder.Append(" and DN.Invoice_No like '%"+sInvoiceNo+"%' ");
				strBuilder.Append(" and DN.Applicationid = '"+strAppID+"' and DN.Enterpriseid = '"+strEnterpriseID+"' ");

				if (sDateType=="C" && (sdtFrom !="" && sdtTo !=""))
					strBuilder.Append(" and DN.Debit_Date between "+sdtFrom+ " and "+sdtTo+" ");
			}

			if (sDateType=="I" && (sdtFrom !="" && sdtTo !=""))
				strBuilder.Append(" and I.Invoice_Date between "+sdtFrom+ " and "+sdtTo+" ");
			
			strBuilder.Append(" and I.PayerID like '%"+Utility.ReplaceSingleQuote(sPayerID)+"%' ");
			strBuilder.Append(" and I.Payer_Name like N'%"+Utility.ReplaceSingleQuote(sPayerName)+"%' ");			

			if (sSearchType=="C")
				strBuilder.Append(" Order By CN.Credit_No, CN.Invoice_No, CND.Consignment_No ");
			else if (sSearchType=="D")
				strBuilder.Append(" Order By DN.Debit_No, DN.Invoice_No, DND.Consignment_No ");

			strBuilder.Append(", I.Invoice_Date, InD.Invoice_Amt ");

			sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(),iCurrent,iDSRecSize,"CreditDebitList");
			return  sessionDS;

		}

		public static int getCNCount(String strAppID, String strEnterpriseID, String sInvoiceNo,String sStatus)
		{
			int total=0;
			DataSet dsCreditNote= null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","GetCreditNoteDS","EDB101","DbConnection object is null!!");
				return total;
			}
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" Select count(*) total ");
			strBuilder.Append(" from Credit_Note where ");
			strBuilder.Append(" ApplicationID = '"+strAppID+ "'");
			strBuilder.Append(" and Enterpriseid = '"+strEnterpriseID+"' ");

			if (sStatus !="")
				strBuilder.Append(" and status = '"+sStatus+"' ");
			if(sInvoiceNo != "")
				strBuilder.Append(" and Invoice_no = '"+Utility.ReplaceSingleQuote(sInvoiceNo)+"' ");

			String strSQLQuery = strBuilder.ToString();			
			dsCreditNote = (DataSet) dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			if(Utility.IsNotDBNull(dsCreditNote.Tables[0].Rows[0]))
			{
				total = (int)dsCreditNote.Tables[0].Rows[0]["total"];
			}

			return  total;	
		}
		#region Comment by Sompote 2010-05-11 (Fix Database Lock)
		/*
		public static DataSet GetCreditNoteDS(String strAppID, String strEnterpriseID, String sCreditNo, String sInvoiceNo)
		{
			DataSet dsCreditNote = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("CreditCreditMgrDAL","GetCreditNoteDS","EDB101","DbConnection object is null!!");
				return dsCreditNote;
			}
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" Select Credit_No,Credit_Date,I.Invoice_No, Invoice_Date, PayerId, Payer_Name, DN.Remark,");
			strBuilder.Append(" DN.Deleted from Credit_Note DN, Invoice I where DN.Invoice_No=I.Invoice_No and ");
			strBuilder.Append(" DN.ApplicationID=I.ApplicationID and DN.EnterpriseID=I.EnterpriseID and ");
			strBuilder.Append(" DN.ApplicationID = '"+strAppID+ "' and DN.Enterpriseid = '"+strEnterpriseID+"' ");
			
			if (sCreditNo !="")
				strBuilder.Append(" and Credit_No like '%"+sCreditNo+"%' ");
			if(sInvoiceNo != "")
				strBuilder.Append(" and I.Invoice_no like '%"+Utility.ReplaceSingleQuote(sInvoiceNo)+"%' ");

			String strSQLQuery = strBuilder.ToString();			
			dsCreditNote = (DataSet) dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			return  dsCreditNote;	
		}

		*/
		#endregion
	
		public static DataSet GetCreditNoteDS(String strAppID, String strEnterpriseID, String sCreditNo, String sInvoiceNo)
		{		
			DataSet dsCreditNote = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("CreditCreditMgrDAL","GetCreditNoteDS","EDB101","DbConnection object is null!!");
				return dsCreditNote;
			}
			return  GetCreditNoteDS( strAppID,  strEnterpriseID,  sCreditNo,  sInvoiceNo, dbCon);	
		}

		public static DataSet GetCreditNoteDS(String strAppID, String strEnterpriseID, String sCreditNo, String sInvoiceNo,DbConnection dbCon)
		{	
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("CreditCreditMgrDAL","GetCreditNoteDS","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditCreditMgrDAL","GetCreditNoteDS","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditCreditMgrDAL","GetCreditNoteDS","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	
		
			return  GetCreditNoteDS( strAppID,  strEnterpriseID,  sCreditNo,  sInvoiceNo, dbCon, dbCmd);	
		}

		public static DataSet GetCreditNoteDS(String strAppID, String strEnterpriseID, String sCreditNo, String sInvoiceNo,DbConnection dbCon,IDbCommand dbCmd)
		{
			DataSet dsCreditNote = null;
			
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" Select Credit_No,Credit_Date,I.Invoice_No, Invoice_Date, PayerId, Payer_Name, DN.Remark,DN.status,DN.Update_by,DN.Update_Date,DN.Status_Update_By,DN.Status_Update_Date,  ");
			strBuilder.Append(" DN.Deleted from Credit_Note DN, Invoice I where DN.Invoice_No=I.Invoice_No and ");
			strBuilder.Append(" DN.ApplicationID=I.ApplicationID and DN.EnterpriseID=I.EnterpriseID and ");
			strBuilder.Append(" DN.ApplicationID = '"+strAppID+ "' and DN.Enterpriseid = '"+strEnterpriseID+"' ");
			
			if (sCreditNo !="")
				strBuilder.Append(" and Credit_No like '%"+sCreditNo+"%' ");
			if(sInvoiceNo != "")
				strBuilder.Append(" and I.Invoice_no like '%"+Utility.ReplaceSingleQuote(sInvoiceNo)+"%' ");

			String strSQLQuery = strBuilder.ToString();		
			dbCmd.CommandText = strSQLQuery;
			dsCreditNote = (DataSet) dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);

			return  dsCreditNote;	
		}



		#region Comment by Sompote 2010-05-11 (Fix Database Lock)
		/*
		public static void GetCreditNoteDS(ref DataSet ds,String strAppID, String strEnterpriseID, String sCreditNo, String sInvoiceNo)
		{			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("CreditCreditMgrDAL","GetCreditNoteDS","EDB101","DbConnection object is null!!");
				return;
			}
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" Select Credit_No,Credit_Date,I.Invoice_No, Invoice_Date, PayerId, Payer_Name, DN.Remark,");
			strBuilder.Append(" DN.Deleted , DN.Status , DN.Update_By ,DN.update_date ,DN.status_update_by ,DN.status_update_date ");
			strBuilder.Append(" FROM Credit_Note DN, Invoice I WHERE DN.Invoice_No=I.Invoice_No and ");
			strBuilder.Append(" DN.ApplicationID=I.ApplicationID and DN.EnterpriseID=I.EnterpriseID and ");
			strBuilder.Append(" DN.ApplicationID = '"+strAppID+ "' and DN.Enterpriseid = '"+strEnterpriseID+"' ");
			strBuilder.Append(" AND DN.Status <> 'C'  ");
			
			if (sCreditNo !="")
				strBuilder.Append(" and Credit_No like '%"+sCreditNo+"%' ");
			if(sInvoiceNo != "")
				strBuilder.Append(" and I.Invoice_no like '%"+Utility.ReplaceSingleQuote(sInvoiceNo)+"%' ");

			String strSQLQuery = strBuilder.ToString();			
			ds = (DataSet) dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			
		}
		
		*/
		#endregion
		
		public static void GetCreditNoteDS(ref DataSet ds,String strAppID, String strEnterpriseID, String sCreditNo, String sInvoiceNo)
		{			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("CreditCreditMgrDAL","GetCreditNoteDS","EDB101","DbConnection object is null!!");
				return;
			}
			GetCreditNoteDS(ref ds, strAppID,  strEnterpriseID,  sCreditNo,  sInvoiceNo, dbCon);
			
		}		
		public static void GetCreditNoteDS(ref DataSet ds,String strAppID, String strEnterpriseID, String sCreditNo, String sInvoiceNo,DbConnection dbCon)
		{		
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("CreditCreditMgrDAL","GetCreditNoteDS","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditCreditMgrDAL","GetCreditNoteDS","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditCreditMgrDAL","GetCreditNoteDS","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	


			GetCreditNoteDS(ref  ds, strAppID,  strEnterpriseID,  sCreditNo,  sInvoiceNo, dbCon, dbCmd);
			
		}
		
		public static void GetCreditNoteDS(ref DataSet ds,String strAppID, String strEnterpriseID, String sCreditNo, String sInvoiceNo,DbConnection dbCon,IDbCommand dbCmd)
		{						
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" Select Credit_No,Credit_Date,I.Invoice_No, Invoice_Date, PayerId, Payer_Name, DN.Remark,");
			strBuilder.Append(" DN.Deleted , DN.Status , DN.Update_By ,DN.update_date ,DN.status_update_by ,DN.status_update_date ");
			strBuilder.Append(" FROM Credit_Note DN, Invoice I WHERE DN.Invoice_No=I.Invoice_No and ");
			strBuilder.Append(" DN.ApplicationID=I.ApplicationID and DN.EnterpriseID=I.EnterpriseID and ");
			strBuilder.Append(" DN.ApplicationID = '"+strAppID+ "' and DN.Enterpriseid = '"+strEnterpriseID+"' ");
			strBuilder.Append(" AND DN.Status <> 'C'  ");
			
			if (sCreditNo !="")
				strBuilder.Append(" and Credit_No like '%"+sCreditNo+"%' ");
			if(sInvoiceNo != "")
				strBuilder.Append(" and I.Invoice_no like '%"+Utility.ReplaceSingleQuote(sInvoiceNo)+"%' ");

			String strSQLQuery = strBuilder.ToString();	
			dbCmd.CommandText =  strSQLQuery;
			ds = (DataSet) dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			
		}
		

		public static DataSet GetDebitNoteDS(String strAppID, String strEnterpriseID, String sDebitNo, String sInvoiceNo)
		{
			DataSet dsDebitNote = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","GetDebitNoteDS","EDB101","DbConnection object is null!!");
				return dsDebitNote;
			}
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" Select Debit_No,Debit_Date,I.Invoice_No, Invoice_Date, PayerId, Payer_Name, DN.Remark,DN.status,DN.Update_by,DN.Update_Date,DN.Status_Update_By,DN.Status_Update_Date,  ");
			strBuilder.Append(" DN.Deleted from Debit_Note DN, Invoice I where DN.Invoice_No=I.Invoice_No and ");
			strBuilder.Append(" DN.ApplicationID=I.ApplicationID and DN.EnterpriseID=I.EnterpriseID and ");
			strBuilder.Append(" DN.ApplicationID = '"+strAppID+ "' and DN.Enterpriseid = '"+strEnterpriseID+"' ");
			
			if (sDebitNo !="")
				strBuilder.Append(" and Debit_No like '%"+sDebitNo+"%' ");
			if(sInvoiceNo != "")
				strBuilder.Append(" and I.Invoice_no like '%"+Utility.ReplaceSingleQuote(sInvoiceNo)+"%' ");

			String strSQLQuery = strBuilder.ToString();			
			dsDebitNote = (DataSet) dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			return  dsDebitNote;	
		}
		#region Comment by Sompote 2010-05-11 (Fix Database Lock)
		/*
		public static void GetDebitNoteDS(ref DataSet ds, String strAppID, String strEnterpriseID, String sDebitNo, String sInvoiceNo)
		{		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","GetDebitNoteDS","EDB101","DbConnection object is null!!");
				
			}
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" Select Debit_No,Debit_Date,I.Invoice_No, Invoice_Date, PayerId, Payer_Name, DN.Remark,");
			strBuilder.Append(" DN.Deleted ");
			strBuilder.Append(" , DN.Status , DN.Update_By ,DN.update_date "); //Add Select Collumn
			strBuilder.Append(" , DN.status_update_by ,DN.status_update_date "); //Add Select Collumn
			strBuilder.Append(" FROM  Debit_Note DN, Invoice I ");
			strBuilder.Append(" WHERE DN.Invoice_No=I.Invoice_No and ");
			strBuilder.Append(" DN.ApplicationID=I.ApplicationID and DN.EnterpriseID=I.EnterpriseID and ");
			strBuilder.Append(" DN.ApplicationID = '"+strAppID+ "' and DN.Enterpriseid = '"+strEnterpriseID+"' ");
			strBuilder.Append(" AND DN.Status <> 'C'  ");
			
			if (sDebitNo !="")
				strBuilder.Append(" and Debit_No like '%"+sDebitNo+"%' ");
			if(sInvoiceNo != "")
				strBuilder.Append(" and I.Invoice_no like '%"+Utility.ReplaceSingleQuote(sInvoiceNo)+"%' ");

			String strSQLQuery = strBuilder.ToString();			
			ds = (DataSet) dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			
		}

		*/
		#endregion

		public static void GetDebitNoteDS(ref DataSet ds, String strAppID, String strEnterpriseID, String sDebitNo, String sInvoiceNo)
		{		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","GetDebitNoteDS","EDB101","DbConnection object is null!!");
				
			}
			GetDebitNoteDS(ref ds, strAppID, strEnterpriseID, sDebitNo, sInvoiceNo,dbCon);		
		}

		public static void GetDebitNoteDS(ref DataSet ds, String strAppID, String strEnterpriseID, String sDebitNo, String sInvoiceNo,DbConnection dbCon)
		{		
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","GetDebitNoteDS","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","GetDebitNoteDS","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","GetDebitNoteDS","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	


			GetDebitNoteDS(ref  ds,  strAppID,  strEnterpriseID,  sDebitNo,  sInvoiceNo, dbCon, dbCmd);	
		}

		public static void GetDebitNoteDS(ref DataSet ds, String strAppID, String strEnterpriseID, String sDebitNo, String sInvoiceNo,DbConnection dbCon,IDbCommand dbCmd)
		{					
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" Select Debit_No,Debit_Date,I.Invoice_No, Invoice_Date, PayerId, Payer_Name, DN.Remark,");
			strBuilder.Append(" DN.Deleted ");
			strBuilder.Append(" , DN.Status , DN.Update_By ,DN.update_date "); //Add Select Collumn
			strBuilder.Append(" , DN.status_update_by ,DN.status_update_date "); //Add Select Collumn
			strBuilder.Append(" FROM  Debit_Note DN, Invoice I ");
			strBuilder.Append(" WHERE DN.Invoice_No=I.Invoice_No and ");
			strBuilder.Append(" DN.ApplicationID=I.ApplicationID and DN.EnterpriseID=I.EnterpriseID and ");
			strBuilder.Append(" DN.ApplicationID = '"+strAppID+ "' and DN.Enterpriseid = '"+strEnterpriseID+"' ");
			strBuilder.Append(" AND DN.Status <> 'C'  ");
			
			if (sDebitNo !="")
				strBuilder.Append(" and Debit_No like '%"+sDebitNo+"%' ");
			if(sInvoiceNo != "")
				strBuilder.Append(" and I.Invoice_no like '%"+Utility.ReplaceSingleQuote(sInvoiceNo)+"%' ");

			String strSQLQuery = strBuilder.ToString();		
			dbCmd.CommandText = strSQLQuery;
			ds = (DataSet) dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			
		}



		public static DataSet GetReasonCodeFromChargeCode(String strAppID,String strEnterpriseID,int sequence,String culture)
		{
			String sTempWhere=null;
			culture = "en-US";
			if(sequence==-1)
				sTempWhere= " and codeid = 'cn_reasoncode' and culture = '"+culture+"' ";
			else
				sTempWhere= " and codeid = 'cn_reasoncode' and sequence ='"+sequence+"' and culture = '"+culture+"' ";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);	
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT code_str_value AS DbComboText, code_text AS DbComboValue, sequence  FROM Core_System_Code ";
					strQry+= " WHERE applicationID='"+strAppID+"' "+sTempWhere+" ORDER BY codeid";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT code_str_value AS DbComboText, code_text AS DbComboValue, sequence  FROM Core_System_Code where ";
					strQry+= " and applicationID='"+strAppID+"' "+sTempWhere+" ORDER BY codeid";
					break;
				
				case DataProviderType.Oledb:
					break;
			}
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet getChargeCodeDetail(String strAppID,String strEnterpriseID,String strChargeCode,String culture)
		{
			String sTempWhere=null;
			culture = "en-US";
			sTempWhere= " and codeid ='cn_chargecode' and culture = '"+culture+"' ";
			if (strChargeCode!="")
			{
				sTempWhere += " and code_str_value = '"+strChargeCode+"' ";
			}
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);	
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT code_str_value AS DbComboText, code_num_value AS DbComboValue, code_text FROM Core_System_Code ";
					strQry+= " WHERE applicationID='"+strAppID+"' "+sTempWhere+" ORDER BY codeid";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT code_str_value AS DbComboText, code_num_value AS DbComboValue, code_text FROM Core_System_Code where ";
					strQry+= " applicationID='"+strAppID+"' "+sTempWhere+" ORDER BY codeid";
					break;
				
				case DataProviderType.Oledb:
					break;
			}
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet GetReasonCodeDetail(String strAppID,String strEnterpriseID,String strReasonCode,String culture)
		{
			String sTempWhere=null;
			culture = "en-US";
			sTempWhere= " and codeid = 'cn_reasoncode' and code_str_value ='"+strReasonCode+"' and culture = '"+culture+"' ";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);	
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT code_str_value AS DbComboText, code_text AS DbComboValue, code_num_value FROM Core_System_Code ";
					strQry+= " WHERE applicationID='"+strAppID+"' "+sTempWhere+" ORDER BY codeid";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT code_str_value AS DbComboText, code_text AS DbComboValue FROM Core_System_Code where ";
					strQry+= " and applicationID='"+strAppID+"' "+sTempWhere+" ORDER BY codeid";
					break;
				
				case DataProviderType.Oledb:
					break;
			}
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}

		public static DataSet InvoiceDateQuery(String strAppID,String strEnterpriseID,String custID,String invNo)
		{
			String sTempWhere=null;
			sTempWhere= " and Invoice_No = '"+invNo+"' and payerid = '"+custID+"' and invoice_status in('A','L') ";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			//			IDbDataParameter dataParam = dbCon.MakeParam("@Query",DbType.String,100,ParameterDirection.Input,"%"+args.Query+"%");
			//			queryParams.Add(dataParam);			
			
			String strQry=null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " SELECT Invoice_No,invoice_date  FROM Invoice ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY Invoice_No";
					break;

				case DataProviderType.Oracle:
					strQry = " SELECT Invoice_No,invoice_date  FROM Invoice where ";
					strQry+= " applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' "+sTempWhere+" ORDER BY Invoice_No";
					break;
				
				case DataProviderType.Oledb:
					break;
			}
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			return dsQryData;
		}
		
		
		public static decimal GetAvaliableTotalCDNAmount(String strAppID,String strEnterpriseID,String strInvNo, String strConNo, String strChargeCode)
		{
			decimal decReturnValue = 0;
			decimal decMain = 0;
			decimal decDebit = 0;
			decimal decCredit = 0;
			decimal decCreditDetail = 0;

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);		
			
			String strQry=null;
			String strCQry = null;
			String strDQry = null;
			String strCreditDetailQry = null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " select consignment_no, isnull(tot_freight_charge,0) 'FRG' ";
					strQry += " ,isnull(insurance_amt,0) 'INS' ";
					strQry += " ,isnull(tot_vas_surcharge,0) 'VAS' ";
					strQry += " ,isnull(esa_surcharge,0) 'ESA' ";
					strQry += " ,isnull(other_surcharge,0) 'OTH' ";
					strQry += " ,isnull(total_exception,0) 'PDX' ";
					strQry += " ,isnull(invoice_adj_amount,0) 'ADJ' ";
					strQry += " ,isnull(invoice_amt,0) 'TOT' ";
					strQry += " from invoice_detail ";
					strQry+= " WHERE applicationID='"+strAppID+"'and EnterpriseID='"+strEnterpriseID+"' and invoice_no = '"+strInvNo+"' and consignment_no = '"+strConNo+"'ORDER BY Invoice_No";

					strCQry = " select isnull(sum(credit_amt),0) as credit_amt from credit_note_detail a inner join credit_note b ";
					strCQry += " on a.invoice_no = b.invoice_no and a.applicationid = b.applicationid and a.enterpriseid = b.enterpriseid and a.credit_no=b.credit_no ";
					strCQry += " where a.applicationid = '"+strAppID+"' ";
					strCQry += " and a.enterpriseid = '"+strEnterpriseID+"' ";
					strCQry += " and a.invoice_no = '"+strInvNo+"' ";
					strCQry += " and a.consignment_no = '"+strConNo+"' ";
					strCQry += " and a.charge_code = '"+strChargeCode+"' ";
					strCQry += " and b.status = 'A' ";

					/*strCreditDetailQry = " select isnull(sum(credit_amt),0) as credit_amt from credit_note_detail a inner join credit_note b ";
					strCreditDetailQry += " on a.invoice_no = b.invoice_no and a.applicationid = b.applicationid and a.enterpriseid = b.enterpriseid and a.credit_no=b.credit_no ";
					strCreditDetailQry += " where a.applicationid = '"+strAppID+"' ";
					strCreditDetailQry += " and a.enterpriseid = '"+strEnterpriseID+"' ";
					strCreditDetailQry += " and b.credit_no  = '"+strCreditNo+"' ";
					strCreditDetailQry += " and a.invoice_no = '"+strInvNo+"' ";
					strCreditDetailQry += " and a.consignment_no = '"+strConNo+"' ";
					strCreditDetailQry += " and a.charge_code = '"+strChargeCode+"' ";
					strCreditDetailQry += " and b.status = 'A' ";*/

					strDQry = " select isnull(sum(debit_amt),0) as debit_amt from debit_note_detail a inner join debit_note b ";
					strDQry += " on a.invoice_no = b.invoice_no and a.applicationid = b.applicationid and a.enterpriseid = b.enterpriseid and a.debit_no=b.debit_no ";
					strDQry += " where a.applicationid = '"+strAppID+"' ";
					strDQry += " and a.enterpriseid = '"+strEnterpriseID+"' ";
					strDQry += " and a.invoice_no = '"+strInvNo+"' ";
					strDQry += " and a.consignment_no = '"+strConNo+"' ";
					strDQry += " and a.charge_code = '"+strChargeCode+"' ";
					strDQry += " and b.status = 'A' ";

					break;

				case DataProviderType.Oracle:
					strQry = " select consignment_no, isnull(tot_freight_charge,0) 'FRG' ";
					strQry += " ,isnull(insurance_amt,0) 'INS' ";
					strQry += " ,isnull(tot_vas_surcharge,0) 'VAS' ";
					strQry += " ,isnull(esa_surcharge,0) 'ESA' ";
					strQry += " ,isnull(other_surcharge,0) 'OTH' ";
					strQry += " ,isnull(total_exception,0) 'PDX' ";
					strQry += " ,isnull(invoice_adj_amount,0) 'ADJ' ";
					strQry += " ,isnull(invoice_amt,0) 'TOT' ";
					strQry += " from invoice_detail ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and invoice_no = '"+strInvNo+"' and consignment_no = '"+strConNo+"'ORDER BY Invoice_No";

					strCQry = " select isnull(sum(credit_amt),0) as credit_amt from credit_note_detail a inner join credit_note b ";
					strCQry += " on a.invoice_no = b.invoice_no and a.applicationid = b.applicationid and a.enterpriseid = b.enterpriseid and a.credit_no=b.credit_no ";
					strCQry += " where a.applicationid = '"+strAppID+"' ";
					strCQry += " and a.enterpriseid = '"+strEnterpriseID+"' ";
					strCQry += " and a.invoice_no = '"+strInvNo+"' ";
					strCQry += " and a.consignment_no = '"+strConNo+"' ";
					strCQry += " and a.charge_code = '"+strChargeCode+"' ";
					strCQry += " and b.status = 'A' ";

					/*strCreditDetailQry = " select isnull(sum(credit_amt),0) as credit_amt from credit_note_detail a inner join credit_note b ";
					strCreditDetailQry += " on a.invoice_no = b.invoice_no and a.applicationid = b.applicationid and a.enterpriseid = b.enterpriseid and a.credit_no=b.credit_no ";
					strCreditDetailQry += " where a.applicationid = '"+strAppID+"' ";
					strCreditDetailQry += " and a.enterpriseid = '"+strEnterpriseID+"' ";
					strCreditDetailQry += " and b.credit_no  = '"+strCreditNo+"' ";
					strCreditDetailQry += " and a.invoice_no = '"+strInvNo+"' ";
					strCreditDetailQry += " and a.consignment_no = '"+strConNo+"' ";
					strCreditDetailQry += " and a.charge_code = '"+strChargeCode+"' ";
					strCreditDetailQry += " and b.status = 'A' ";*/

					strDQry = " select isnull(sum(debit_amt),0) as debit_amt from debit_note_detail a inner join debit_note b ";
					strDQry += " on a.invoice_no = b.invoice_no and a.applicationid = b.applicationid and a.enterpriseid = b.enterpriseid and a.debit_no=b.debit_no ";
					strDQry += " where a.applicationid = '"+strAppID+"' ";
					strDQry += " and a.enterpriseid = '"+strEnterpriseID+"' ";
					strDQry += " and a.invoice_no = '"+strInvNo+"' ";
					strDQry += " and a.consignment_no = '"+strConNo+"' ";
					strDQry += " and a.charge_code = '"+strChargeCode+"' ";
					strDQry += " and b.status = 'A' ";

					break;
				
				case DataProviderType.Oledb:
					break;
			}
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			DataSet dsDQry = (DataSet)dbCon.ExecuteQuery(strDQry,queryParams,ReturnType.DataSetType);
			DataSet dsCQry = (DataSet)dbCon.ExecuteQuery(strCQry,queryParams,ReturnType.DataSetType);
			//DataSet dsCreditDetailQry = (DataSet)dbCon.ExecuteQuery(strCreditDetailQry,queryParams,ReturnType.DataSetType);

			if(dsQryData != null && dsDQry != null && dsCQry != null)
			{
				foreach(DataRow drMain in dsQryData.Tables[0].Rows)
				{
					decMain = Convert.ToDecimal(drMain[strChargeCode.Trim()].ToString());
				}

				foreach(DataRow drD in dsDQry.Tables[0].Rows)
				{
					decDebit = Convert.ToDecimal(drD["debit_amt"].ToString());
				}

				foreach(DataRow drC in dsCQry.Tables[0].Rows)
				{
					decCredit = Convert.ToDecimal(drC["credit_amt"].ToString());
				}
				/*foreach(DataRow drC in dsCreditDetailQry.Tables[0].Rows)
				{
					decCreditDetail = Convert.ToDecimal(drC["credit_amt"].ToString());
				}*/

				decReturnValue = decMain + (decDebit -decCredit);
				/*if(decMain == 0)
				{
					decReturnValue = decMain;
				}
				else
				{
					decReturnValue = decMain + (decDebit -(decCredit-decCreditDetail));
				}*/
			}

			return decReturnValue;
		}
		
		public static decimal GetAvaliableTotalCNAmount(String strAppID,String strEnterpriseID,String strCreditNo,String strInvNo, String strConNo, String strChargeCode)
		{
			decimal decReturnValue = 0;
			decimal decMain = 0;
			decimal decDebit = 0;
			decimal decCredit = 0;
			decimal decCreditDetail = 0;

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);		
			
			String strQry=null;
			String strCQry = null;
			String strDQry = null;
			String strCreditDetailQry = null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " select consignment_no, isnull(tot_freight_charge,0) 'FRG' ";
					strQry += " ,isnull(insurance_amt,0) 'INS' ";
					strQry += " ,isnull(tot_vas_surcharge,0) 'VAS' ";
					strQry += " ,isnull(esa_surcharge,0) 'ESA' ";
					strQry += " ,isnull(other_surcharge,0) 'OTH' ";
					strQry += " ,isnull(total_exception,0) 'PDX' ";
					strQry += " ,isnull(invoice_adj_amount,0) 'ADJ' ";
					strQry += " ,isnull(invoice_amt,0) 'TOT' ";
					strQry += " from invoice_detail ";
					strQry+= " WHERE applicationID='"+strAppID+"'and EnterpriseID='"+strEnterpriseID+"' and invoice_no = '"+strInvNo+"' and consignment_no = '"+strConNo+"'ORDER BY Invoice_No";

					strCQry = " select isnull(sum(credit_amt),0) as credit_amt from credit_note_detail a inner join credit_note b ";
					strCQry += " on a.invoice_no = b.invoice_no and a.applicationid = b.applicationid and a.enterpriseid = b.enterpriseid and a.credit_no=b.credit_no ";
					strCQry += " where a.applicationid = '"+strAppID+"' ";
					strCQry += " and a.enterpriseid = '"+strEnterpriseID+"' ";
					strCQry += " and a.invoice_no = '"+strInvNo+"' ";
					strCQry += " and a.consignment_no = '"+strConNo+"' ";
					strCQry += " and a.charge_code = '"+strChargeCode+"' ";
					strCQry += " and b.status <> 'C' ";

					strCreditDetailQry = " select isnull(sum(credit_amt),0) as credit_amt from credit_note_detail a inner join credit_note b ";
					strCreditDetailQry += " on a.invoice_no = b.invoice_no and a.applicationid = b.applicationid and a.enterpriseid = b.enterpriseid and a.credit_no=b.credit_no ";
					strCreditDetailQry += " where a.applicationid = '"+strAppID+"' ";
					strCreditDetailQry += " and a.enterpriseid = '"+strEnterpriseID+"' ";
					strCreditDetailQry += " and b.credit_no  = '"+strCreditNo+"' ";
					strCreditDetailQry += " and a.invoice_no = '"+strInvNo+"' ";
					strCreditDetailQry += " and a.consignment_no = '"+strConNo+"' ";
					strCreditDetailQry += " and a.charge_code = '"+strChargeCode+"' ";
					strCreditDetailQry += " and b.status <> 'C' ";

					strDQry = " select isnull(sum(debit_amt),0) as debit_amt from debit_note_detail a inner join debit_note b ";
					strDQry += " on a.invoice_no = b.invoice_no and a.applicationid = b.applicationid and a.enterpriseid = b.enterpriseid and a.debit_no=b.debit_no ";
					strDQry += " where a.applicationid = '"+strAppID+"' ";
					strDQry += " and a.enterpriseid = '"+strEnterpriseID+"' ";
					strDQry += " and a.invoice_no = '"+strInvNo+"' ";
					strDQry += " and a.consignment_no = '"+strConNo+"' ";
					strDQry += " and a.charge_code = '"+strChargeCode+"' ";
					strDQry += " and b.status <> 'C' ";

					break;

				case DataProviderType.Oracle:
					strQry = " select consignment_no, isnull(tot_freight_charge,0) 'FRG' ";
					strQry += " ,isnull(insurance_amt,0) 'INS' ";
					strQry += " ,isnull(tot_vas_surcharge,0) 'VAS' ";
					strQry += " ,isnull(esa_surcharge,0) 'ESA' ";
					strQry += " ,isnull(other_surcharge,0) 'OTH' ";
					strQry += " ,isnull(total_exception,0) 'PDX' ";
					strQry += " ,isnull(invoice_adj_amount,0) 'ADJ' ";
					strQry += " ,isnull(invoice_amt,0) 'TOT' ";
					strQry += " from invoice_detail ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and invoice_no = '"+strInvNo+"' and consignment_no = '"+strConNo+"'ORDER BY Invoice_No";

					strCQry = " select isnull(sum(credit_amt),0) as credit_amt from credit_note_detail a inner join credit_note b ";
					strCQry += " on a.invoice_no = b.invoice_no and a.applicationid = b.applicationid and a.enterpriseid = b.enterpriseid and a.credit_no=b.credit_no ";
					strCQry += " where a.applicationid = '"+strAppID+"' ";
					strCQry += " and a.enterpriseid = '"+strEnterpriseID+"' ";
					strCQry += " and a.invoice_no = '"+strInvNo+"' ";
					strCQry += " and a.consignment_no = '"+strConNo+"' ";
					strCQry += " and a.charge_code = '"+strChargeCode+"' ";
					strCQry += " and b.status <> 'C' ";

					strCreditDetailQry = " select isnull(sum(credit_amt),0) as credit_amt from credit_note_detail a inner join credit_note b ";
					strCreditDetailQry += " on a.invoice_no = b.invoice_no and a.applicationid = b.applicationid and a.enterpriseid = b.enterpriseid and a.credit_no=b.credit_no ";
					strCreditDetailQry += " where a.applicationid = '"+strAppID+"' ";
					strCreditDetailQry += " and a.enterpriseid = '"+strEnterpriseID+"' ";
					strCreditDetailQry += " and b.credit_no  = '"+strCreditNo+"' ";
					strCreditDetailQry += " and a.invoice_no = '"+strInvNo+"' ";
					strCreditDetailQry += " and a.consignment_no = '"+strConNo+"' ";
					strCreditDetailQry += " and a.charge_code = '"+strChargeCode+"' ";
					strCreditDetailQry += " and b.status <> 'C' ";

					strDQry = " select isnull(sum(debit_amt),0) as debit_amt from debit_note_detail a inner join debit_note b ";
					strDQry += " on a.invoice_no = b.invoice_no and a.applicationid = b.applicationid and a.enterpriseid = b.enterpriseid and a.debit_no=b.debit_no ";
					strDQry += " where a.applicationid = '"+strAppID+"' ";
					strDQry += " and a.enterpriseid = '"+strEnterpriseID+"' ";
					strDQry += " and a.invoice_no = '"+strInvNo+"' ";
					strDQry += " and a.consignment_no = '"+strConNo+"' ";
					strDQry += " and a.charge_code = '"+strChargeCode+"' ";
					strDQry += " and b.status <> 'C' ";

					break;
				
				case DataProviderType.Oledb:
					break;
			}
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			DataSet dsDQry = (DataSet)dbCon.ExecuteQuery(strDQry,queryParams,ReturnType.DataSetType);
			DataSet dsCQry = (DataSet)dbCon.ExecuteQuery(strCQry,queryParams,ReturnType.DataSetType);
			DataSet dsCreditDetailQry = (DataSet)dbCon.ExecuteQuery(strCreditDetailQry,queryParams,ReturnType.DataSetType);

			if(dsQryData != null && dsDQry != null && dsCQry != null && dsCreditDetailQry!=null)
			{
				foreach(DataRow drMain in dsQryData.Tables[0].Rows)
				{
					decMain = Convert.ToDecimal(drMain[strChargeCode.Trim()].ToString());
				}

				foreach(DataRow drD in dsDQry.Tables[0].Rows)
				{
					decDebit = Convert.ToDecimal(drD["debit_amt"].ToString());
				}

				foreach(DataRow drC in dsCQry.Tables[0].Rows)
				{
					decCredit = Convert.ToDecimal(drC["credit_amt"].ToString());
				}
				foreach(DataRow drC in dsCreditDetailQry.Tables[0].Rows)
				{
					decCreditDetail = Convert.ToDecimal(drC["credit_amt"].ToString());
				}

				if(decMain == 0)
				{
					decReturnValue = decMain;
				}
				else
				{
					decReturnValue = decMain + (decDebit -(decCredit-decCreditDetail));
				}
			}

			return decReturnValue;
		}

		public static decimal GetAvaliableTotalCNAmount(String strAppID,String strEnterpriseID,String strInvNo, String strConNo, String strChargeCode)
		{
			decimal decReturnValue = 0;
			decimal decMain = 0;
			decimal decDebit = 0;
			decimal decCredit = 0;

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			ArrayList queryParams = new ArrayList();
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);		
			
			String strQry=null;
			String strCQry = null;
			String strDQry = null;

			switch (dbProvider)
			{
				case DataProviderType.Sql:	
					strQry = " select consignment_no, isnull(tot_freight_charge,0) 'FRG' ";
					strQry += " ,isnull(insurance_amt,0) 'INS' ";
					strQry += " ,isnull(tot_vas_surcharge,0) 'VAS' ";
					strQry += " ,isnull(esa_surcharge,0) 'ESA' ";
					strQry += " ,isnull(other_surcharge,0) 'OTH' ";
					strQry += " ,isnull(total_exception,0) 'PDX' ";
					strQry += " ,isnull(invoice_adj_amount,0) 'ADJ' ";
					strQry += " ,isnull(invoice_amt,0) 'TOT' ";
					strQry += " from invoice_detail ";
					strQry+= " WHERE applicationID='"+strAppID+"'and EnterpriseID='"+strEnterpriseID+"' and invoice_no = '"+strInvNo+"' and consignment_no = '"+strConNo+"'ORDER BY Invoice_No";

					strCQry = " select isnull(sum(credit_amt),0) as credit_amt from credit_note_detail a inner join credit_note b ";
					strCQry += " on a.invoice_no = b.invoice_no and a.applicationid = b.applicationid and a.enterpriseid = b.enterpriseid and a.credit_no=b.credit_no ";
					strCQry += " where a.applicationid = '"+strAppID+"' ";
					strCQry += " and a.enterpriseid = '"+strEnterpriseID+"' ";
					strCQry += " and a.invoice_no = '"+strInvNo+"' ";
					strCQry += " and a.consignment_no = '"+strConNo+"' ";
					strCQry += " and a.charge_code = '"+strChargeCode+"' ";
					strCQry += " and b.status <> 'C' ";

					strDQry = " select isnull(sum(debit_amt),0) as debit_amt from debit_note_detail a inner join debit_note b ";
					strDQry += " on a.invoice_no = b.invoice_no and a.applicationid = b.applicationid and a.enterpriseid = b.enterpriseid and a.debit_no=b.debit_no ";
					strDQry += " where a.applicationid = '"+strAppID+"' ";
					strDQry += " and a.enterpriseid = '"+strEnterpriseID+"' ";
					strDQry += " and a.invoice_no = '"+strInvNo+"' ";
					strDQry += " and a.consignment_no = '"+strConNo+"' ";
					strDQry += " and a.charge_code = '"+strChargeCode+"' ";
					strDQry += " and b.status <> 'C' ";

					break;

				case DataProviderType.Oracle:
					strQry = " select consignment_no, isnull(tot_freight_charge,0) 'FRG' ";
					strQry += " ,isnull(insurance_amt,0) 'INS' ";
					strQry += " ,isnull(tot_vas_surcharge,0) 'VAS' ";
					strQry += " ,isnull(esa_surcharge,0) 'ESA' ";
					strQry += " ,isnull(other_surcharge,0) 'OTH' ";
					strQry += " ,isnull(total_exception,0) 'PDX' ";
					strQry += " ,isnull(invoice_adj_amount,0) 'ADJ' ";
					strQry += " ,isnull(invoice_amt,0) 'TOT' ";
					strQry += " from invoice_detail ";
					strQry+= " WHERE applicationID='"+strAppID+"' and EnterpriseID='"+strEnterpriseID+"' and invoice_no = '"+strInvNo+"' and consignment_no = '"+strConNo+"'ORDER BY Invoice_No";

					strCQry = " select isnull(sum(credit_amt),0) as credit_amt from credit_note_detail a inner join credit_note b ";
					strCQry += " on a.invoice_no = b.invoice_no and a.applicationid = b.applicationid and a.enterpriseid = b.enterpriseid and a.credit_no=b.credit_no ";
					strCQry += " where a.applicationid = '"+strAppID+"' ";
					strCQry += " and a.enterpriseid = '"+strEnterpriseID+"' ";
					strCQry += " and a.invoice_no = '"+strInvNo+"' ";
					strCQry += " and a.consignment_no = '"+strConNo+"' ";
					strCQry += " and a.charge_code = '"+strChargeCode+"' ";
					strCQry += " and b.status <> 'C' ";

					strDQry = " select isnull(sum(debit_amt),0) as debit_amt from debit_note_detail a inner join debit_note b ";
					strDQry += " on a.invoice_no = b.invoice_no and a.applicationid = b.applicationid and a.enterpriseid = b.enterpriseid and a.debit_no=b.debit_no ";
					strDQry += " where a.applicationid = '"+strAppID+"' ";
					strDQry += " and a.enterpriseid = '"+strEnterpriseID+"' ";
					strDQry += " and a.invoice_no = '"+strInvNo+"' ";
					strDQry += " and a.consignment_no = '"+strConNo+"' ";
					strDQry += " and a.charge_code = '"+strChargeCode+"' ";
					strDQry += " and b.status <> 'C' ";

					break;
				
				case DataProviderType.Oledb:
					break;
			}
			DataSet dsQryData = (DataSet)dbCon.ExecuteQuery(strQry,queryParams,ReturnType.DataSetType);
			DataSet dsDQry = (DataSet)dbCon.ExecuteQuery(strDQry,queryParams,ReturnType.DataSetType);
			DataSet dsCQry = (DataSet)dbCon.ExecuteQuery(strCQry,queryParams,ReturnType.DataSetType);

			if(dsQryData != null && dsDQry != null && dsCQry != null)
			{
				foreach(DataRow drMain in dsQryData.Tables[0].Rows)
				{
					decMain = Convert.ToDecimal(drMain[strChargeCode.Trim()].ToString());
				}

				foreach(DataRow drD in dsDQry.Tables[0].Rows)
				{
					decDebit = Convert.ToDecimal(drD["debit_amt"].ToString());
				}

				foreach(DataRow drC in dsCQry.Tables[0].Rows)
				{
					decCredit = Convert.ToDecimal(drC["credit_amt"].ToString());
				}

				if(decMain == 0)
				{
					decReturnValue = decMain;
				}
				else
				{
					decReturnValue = decMain + (decDebit - decCredit);
				}
			}

			return decReturnValue;
		}

//		private static DbConnection GetConnection(String strAppID,String strEnterpriseID)
//		{			
//			//Get App Database Connection
//			DbConnection dbConApp = null;
//			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
//			//dbConApp = DbConnectionManager.GetInstance().GetDbConnection("TIES","KDTH");
//			if(dbConApp == null)
//			{
//				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0001","dbConApp is null");
//				throw new ApplicationException("Connection to Database failed",null);
//			}			
//			return dbConApp;
//		}
		private static Boolean GetData(String strAppID,String strEnterpriseID,ref DataSet ds ,String strQuery)
		{
			DbConnection dbConApp= DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID); 	
			ds = (DataSet)dbConApp.ExecuteQuery(strQuery,ReturnType.DataSetType);
		
			if(ds != null)
			{
				if(ds.Tables.Count > 0)
				{
					return true;
				}
			}
			return false;
		}
		private static Object GetData_Scalar(String strAppID,String strEnterpriseID,String strQuery)
		{
			DbConnection dbConApp= DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID); 	
			return dbConApp.ExecuteScalar(strQuery);			
		}


		public static bool ValidateCurrentMonth(String strAppID,String strEnterpriseID,DateTime dtUpdateDate)
		{	
			//Get current month of TODAY from database server
			DateTime DBDate;
			Object obj = GetData_Scalar(strAppID,strEnterpriseID,"select getdate() ");
			
			if(obj == null)	
				return false;
			
			try
			{
				DBDate = (DateTime)obj;
			}
			catch
			{
				return false;
			}			
	           
			// Compare current month with month of updatedate
			// Return true if current(month) = updatedate(month)
			// Return false if current(month) > updatedate(month)
			if(DBDate.Month == dtUpdateDate.Month && DBDate.Year == dtUpdateDate.Year)
			{
				return true;
			}
			else
				return false;			
		}

		public static string ValidateDNCNRelateInvoice(String strAppID
			, String strEnterpriseID ,string InvoiceNo)
		{
			//Get Data Credit
			DataSet dsCD = new DataSet();
			CreditDebitMgrDAL.GetCreditNoteDS(ref dsCD, strAppID , strEnterpriseID , "", InvoiceNo);	
			if(dsCD.Tables[0].Rows.Count > 0)	
				return InvoiceNo;


			//Get Data Debit
			DataSet dsDB = new DataSet();
			CreditDebitMgrDAL.GetDebitNoteDS(ref dsDB, strAppID , strEnterpriseID , "", InvoiceNo);
			if(dsDB.Tables[0].Rows.Count > 0)	
				return InvoiceNo;


			return "";
		}

		public static int UpdateCreditNoteDS(     String strAppID
			, String strEnterpriseID
			, DataSet dsToModify
			, DataSet dsCNDetail
			, IDbTransaction transactionApp
			, IDbConnection conApp
			, DbConnection dbConApp)
		{			
			int iRowsAffected = 0;
			//DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			//IDbTransaction transactionApp = null;
			//IDbConnection conApp = null;
			
			//Get App Database Connection
			//dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			//			if(dbConApp == null)
			//			{
			//				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0001","dbConApp is null");
			//				throw new ApplicationException("Connection to Database failed",null);
			//			}			
			
			//conApp = dbConApp.GetConnection();			
			//			if(conApp == null)
			//			{
			//				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0002","conApp is null");
			//				throw new ApplicationException("Connection to Database failed",null);
			//			}
			//			try
			//			{
			//				dbConApp.OpenConnection(ref conApp);
			//			}
			//			catch(ApplicationException appException)
			//			{
			//				if(conApp != null)
			//				{
			//					conApp.Close();
			//				}
			//				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
			//				throw new ApplicationException("Error opening database connection ",appException);
			//			}
			//			catch(Exception exception)
			//			{
			//				if(conApp != null)				
			//					conApp.Close();
			//				
			//				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
			//				throw new ApplicationException("Error opening database connection ",exception);
			//			}		
			
			String strInvoiceNo=null;
			String strConsignmentNo=null;
			String strCreditNo=null;
			Decimal dCreditAmt=0;
			Decimal dOLDCreditAmt=0;
			Decimal dActCreditAmt=0;
			Decimal dTotaCreditAmnt=0;
			//String strCreditDesc=null;
			String strOldStat="";
			String strStat="";			
			//Begin App Transaction
			//			try
			//			{
			//DataRow drEach = dsToModify.Tables[0].Rows[0];	
			foreach(DataRow drEach in dsToModify.Tables[0].Rows)
			{
				//					transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
				//					if(transactionApp == null)
				//					{
				//						Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0005","transactionApp is null");
				//						throw new ApplicationException("Failed Begining a App Transaction..",null);
				//					}
			

				int iRowCnt = dsToModify.Tables[0].Rows.Count;
				if (iRowCnt<=0) 
					return  iRowsAffected;

				StringBuilder strBuilder = new StringBuilder();
				StringBuilder strSumBuilder = new StringBuilder();


				if(Utility.IsNotDBNull(drEach["Credit_No"]) ==true && drEach["Credit_No"].ToString() != "")
					strCreditNo=(String) drEach["Credit_No"];
				if(Utility.IsNotDBNull(drEach["Invoice_No"]) ==true && drEach["Invoice_No"].ToString() != "")
					strInvoiceNo=Utility.ReplaceSingleQuote(drEach["Invoice_No"].ToString());

				//Update Debit_Note Information
				strBuilder.Append("Update Credit_Note set Credit_Date =");				
				if(Utility.IsNotDBNull(drEach["Credit_Date"]) ==true && drEach["Credit_Date"].ToString() != "")
				{	
					DateTime dtCredit_Date=System.Convert.ToDateTime(drEach["Credit_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCredit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", remark = ");
				if(Utility.IsNotDBNull(drEach["remark"]) ==true && drEach["remark"].ToString() != "")
				{				
					strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drEach["remark"].ToString())+"'");				
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status = ");
				if(Utility.IsNotDBNull(drEach["status"]) ==true && drEach["status"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["status"].ToString())+"'");
					strStat = Utility.ReplaceSingleQuote(drEach["status"].ToString());
					strOldStat = Utility.ReplaceSingleQuote(drEach["status",System.Data.DataRowVersion.Original].ToString());
				}
				else
				{
					strStat="";
					strOldStat="";
					strBuilder.Append("null");
				}

				strBuilder.Append(", update_by = ");
				if(Utility.IsNotDBNull(drEach["Update_By"]) ==true && drEach["Update_By"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["Update_By"].ToString())+"'");				
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", update_date = ");
				if(Utility.IsNotDBNull(drEach["Update_Date"]) ==true && drEach["Update_Date"].ToString() != "")
				{	
					DateTime dtCredit_Date=System.Convert.ToDateTime(drEach["Update_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCredit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status_update_by = ");
				if(Utility.IsNotDBNull(drEach["Status_Update_By"]) ==true && drEach["Status_Update_By"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["Status_Update_By"].ToString())+"'");				
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status_update_date = ");
				if(Utility.IsNotDBNull(drEach["Status_Update_Date"]) ==true && drEach["Status_Update_Date"].ToString() != "")
				{	
					DateTime dtCredit_Date=System.Convert.ToDateTime(drEach["Status_Update_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtCredit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(" where ApplicationID = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID);
				strBuilder.Append("' and Invoice_No = '"+strInvoiceNo);
				strBuilder.Append("' and Credit_No='"+drEach["Credit_No"].ToString()+"'");

				String strSQLQuery = strBuilder.ToString();			
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note table");

				//Remove deleting code

				for (int i=0;i<dsCNDetail.Tables[0].Rows.Count;i++)
				{
					DataRow drDND=dsCNDetail.Tables[0].Rows[i];	

					strBuilder.Remove(0,strBuilder.Length);	

					//Remove inserting code

					dTotaCreditAmnt=0;
					if(strStat.ToUpper() == "A")
					{
						//calculate total debit amnt
						strSumBuilder.Remove(0,strSumBuilder.Length);
						strSumBuilder.Append(" select  Credit_Amt from credit_note_detail dt with(nolock) ");
						strSumBuilder.Append(" inner join credit_note dn with(nolock) on dt.Credit_No = dn.Credit_No and dt.applicationid = dn.applicationid and dt.enterpriseid = dn.enterpriseid ");
						strSumBuilder.Append(" where dt.Consignment_No='"+strConsignmentNo+"' and dt.applicationid = '"+strAppID+"' and dt.enterpriseid = '"+strEnterpriseID+"' and dn.status='A' ");
						dbCommandApp = dbConApp.CreateCommand(strSumBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsTotal = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(Utility.IsNotDBNull(dsTotal.Tables[0].Rows[0])&&dsTotal.Tables[0].Rows.Count>0)
						{
							try
							{
								dTotaCreditAmnt = (Decimal)dsTotal.Tables[0].Rows[0]["Credit_Amt"];
							}
							catch
							{
								dTotaCreditAmnt=0;
							}
						}
						// Update Shipment Table
						if(strOldStat.ToUpper()=="A")
						{
							strBuilder.Remove(0,strBuilder.Length);
							dActCreditAmt=(dCreditAmt-dOLDCreditAmt);
							strBuilder.Append("Update shipment set Credit_Amt= "+dTotaCreditAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						else if(strOldStat.ToUpper()=="N")
						{
							strBuilder.Remove(0,strBuilder.Length);
							dActCreditAmt=(dCreditAmt);
							strBuilder.Append("Update shipment set Credit_Amt="+dActCreditAmt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						strBuilder.Remove(0,strBuilder.Length);
						strBuilder.Append("select isnull(inv.invoice_amt,0)+isnull(s.debit_Amt,0)-isnull(s.credit_amt,0) total_cons_revenue from invoice_detail inv with(nolock) inner join shipment s with(nolock) on inv.invoice_no=s.invoice_no and inv.Consignment_No=s.Consignment_No ");
						strBuilder.Append("where s.Consignment_No='"+strConsignmentNo+"' and s.invoice_no = '"+strInvoiceNo+"' ");
						strBuilder.Append(" and s.applicationid = '"+strAppID+"' and s.enterpriseid = '"+strEnterpriseID+"'");
						dbCommandApp = dbConApp.CreateCommand(strBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsResult = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(dsResult.Tables[0].Rows.Count>0)
							if(Utility.IsNotDBNull(dsResult.Tables[0].Rows[0]))
							{
								Decimal dRevenue = (Decimal)dsResult.Tables[0].Rows[0]["total_cons_revenue"];
								strBuilder.Remove(0,strBuilder.Length);
								strBuilder.Append("Update shipment set total_cons_revenue = "+dRevenue+" where Consignment_No='"+strConsignmentNo+"' ");
								strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
								strSQLQuery = strBuilder.ToString();			
								dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
								dbCommandApp.Connection = conApp;
								dbCommandApp.Transaction = transactionApp;
								iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
								Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

							}
					}
					else if (strStat.ToUpper() == "C")
					{
						if(strOldStat.ToUpper()=="A")
						{
							//calculate total debit amnt
							strSumBuilder.Remove(0,strSumBuilder.Length);
							strSumBuilder.Append(" select  Credit_Amt from debit_note_detail dt with(nolock) ");
							strSumBuilder.Append(" inner join debit_note dn with(nolock) on dt.Credit_No = dn.Credit_No and dt.applicationid = dn.applicationid and dt.enterpriseid = dn.enterpriseid ");
							strSumBuilder.Append(" where dt.Consignment_No='"+strConsignmentNo+"' and dt.applicationid = '"+strAppID+"' and dt.enterpriseid = '"+strEnterpriseID+"' and dn.status='A' ");
							dbCommandApp = dbConApp.CreateCommand(strSumBuilder.ToString(),CommandType.Text);
							dbCommandApp.Connection = conApp;
							DataSet dsTotal = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
							if(Utility.IsNotDBNull(dsTotal.Tables[0].Rows[0])&&dsTotal.Tables[0].Rows.Count>0)
							{
								try
								{
									dTotaCreditAmnt = (Decimal)dsTotal.Tables[0].Rows[0]["Credit_Amt"];
								}
								catch
								{
									dTotaCreditAmnt=0;
								}
							}

							// Update Shipment Table
							strBuilder.Remove(0,strBuilder.Length);
							//dActCreditAmt=(dCreditAmt-dOLDCreditAmt);
							strBuilder.Append("Update shipment set Credit_Amt="+dTotaCreditAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						strBuilder.Remove(0,strBuilder.Length);
						strBuilder.Append("select isnull(inv.invoice_amt,0)+isnull(s.debit_Amt,0)-isnull(s.credit_amt,0) total_cons_revenue from invoice_detail inv with(nolock) inner join shipment s with(nolock) on inv.invoice_no=s.invoice_no and inv.Consignment_No=s.Consignment_No ");
						strBuilder.Append("where s.Consignment_No='"+strConsignmentNo+"' and s.invoice_no = '"+strInvoiceNo+"' ");
						strBuilder.Append(" and s.applicationid = '"+strAppID+"' and s.enterpriseid = '"+strEnterpriseID+"'");
						dbCommandApp = dbConApp.CreateCommand(strBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsResult = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(dsResult.Tables[0].Rows.Count>0)
							if(Utility.IsNotDBNull(dsResult.Tables[0].Rows[0]))
							{
								Decimal dRevenue = (Decimal)dsResult.Tables[0].Rows[0]["total_cons_revenue"];
								strBuilder.Remove(0,strBuilder.Length);
								strBuilder.Append("Update shipment set total_cons_revenue = "+dRevenue+" where Consignment_No='"+strConsignmentNo+"' ");
								strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
								strSQLQuery = strBuilder.ToString();			
								dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
								dbCommandApp.Connection = conApp;
								dbCommandApp.Transaction = transactionApp;
								iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
								Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

							}
					}
										
				}
				
				//commit the transaction
				//transactionApp.Commit();
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0014","App db Transaction committed.");
		
			
			}
			//			}					
			//			catch(ApplicationException appException)
			//			{				
			//			}
			//			catch(Exception exception)
			//			{
			//			}
			//			finally
			//			{
			//
			//			}			
			return iRowsAffected;
		}

		public static int UpdateDebitNoteDS(	  String strAppID
			, String strEnterpriseID
			, DataSet dsToModify
			, DataSet dsCNDetail
			, IDbTransaction transactionApp
			, IDbConnection conApp
			, DbConnection dbConApp)
		{
			int iRowsAffected = 0;
			//DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			//IDbTransaction transactionApp = null;
			//IDbConnection conApp = null;
					
			//			//Get App Database Connection
			//			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			//			if(dbConApp == null)
			//			{
			//				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0001","dbConApp is null");
			//				throw new ApplicationException("Connection to Database failed",null);
			//			}			
			//					
			//			conApp = dbConApp.GetConnection();			
			//			if(conApp == null)
			//			{
			//				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0002","conApp is null");
			//				throw new ApplicationException("Connection to Database failed",null);
			//			}
			//			try
			//			{
			//				dbConApp.OpenConnection(ref conApp);
			//			}
			//			catch(ApplicationException appException)
			//			{
			//				if(conApp != null)
			//				{
			//					conApp.Close();
			//				}
			//				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0003","Error opening connection : "+ appException.Message.ToString());
			//				throw new ApplicationException("Error opening database connection ",appException);
			//			}
			//			catch(Exception exception)
			//			{
			//				if(conApp != null)				
			//					conApp.Close();
			//						
			//				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0004","Error opening connection : "+ exception.Message.ToString());
			//				throw new ApplicationException("Error opening database connection ",exception);
			//			}		
					
			String strInvoiceNo=null;
			String strConsignmentNo=null;
			String strDebitNo=null;
			Decimal dDebitAmt=0;
			Decimal dOLDDebitAmt=0;
			Decimal dActDebitAmt=0;
			Decimal dTotaDebitAmnt=0;
			String strDebitDesc=null;
			String strOldStat="";
			String strStat="";		
			

			//			try
			//			{
			//Begin App Transaction
			//DataRow drEach = dsToModify.Tables[0].Rows[0];		
			foreach(DataRow drEach in dsToModify.Tables[0].Rows)
			{
				//					transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
				//					if(transactionApp == null)
				//					{
				//						Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0005","transactionApp is null");
				//						throw new ApplicationException("Failed Begining a App Transaction..",null);
				//					}
					

				int iRowCnt = dsToModify.Tables[0].Rows.Count;
				if (iRowCnt<=0) 
					return iRowsAffected;

				StringBuilder strBuilder = new StringBuilder();
				StringBuilder strSumBuilder = new StringBuilder();



				if(Utility.IsNotDBNull(drEach["Debit_No"]) ==true && drEach["Debit_No"].ToString() != "")
					strDebitNo=(String) drEach["Debit_No"];
				if(Utility.IsNotDBNull(drEach["Invoice_No"]) ==true && drEach["Invoice_No"].ToString() != "")
					strInvoiceNo=Utility.ReplaceSingleQuote(drEach["Invoice_No"].ToString());

				//Update Debit_Note Information
				strBuilder.Append("Update Debit_Note set Debit_Date =");				
				if(Utility.IsNotDBNull(drEach["Debit_Date"]) ==true && drEach["Debit_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Debit_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", remark = ");
				if(Utility.IsNotDBNull(drEach["remark"]) ==true && drEach["remark"].ToString() != "")
				{				
					strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drEach["remark"].ToString())+"'");				
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status = ");
				if(Utility.IsNotDBNull(drEach["status"]) ==true && drEach["status"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["status"].ToString())+"'");
					strStat = Utility.ReplaceSingleQuote(drEach["status"].ToString());
					strOldStat = Utility.ReplaceSingleQuote(drEach["status",System.Data.DataRowVersion.Original].ToString());
				}
				else
				{
					strStat="";
					strOldStat="";
					strBuilder.Append("null");
				}

				strBuilder.Append(", update_by = ");
				if(Utility.IsNotDBNull(drEach["Update_By"]) ==true && drEach["Update_By"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["Update_By"].ToString())+"'");				
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", update_date = ");
				if(Utility.IsNotDBNull(drEach["Update_Date"]) ==true && drEach["Update_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Update_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status_update_by = ");
				if(Utility.IsNotDBNull(drEach["Status_Update_By"]) ==true && drEach["Status_Update_By"].ToString() != "")
				{				
					strBuilder.Append(" '"+Utility.ReplaceSingleQuote(drEach["Status_Update_By"].ToString())+"'");				
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(", status_update_date = ");
				if(Utility.IsNotDBNull(drEach["Status_Update_Date"]) ==true && drEach["Status_Update_Date"].ToString() != "")
				{	
					DateTime dtDebit_Date=System.Convert.ToDateTime(drEach["Status_Update_Date"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtDebit_Date,DTFormat.DateTime));			
				}
				else
				{
					strBuilder.Append("null");
				}

				strBuilder.Append(" where ApplicationID = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID);
				strBuilder.Append("' and Invoice_No = '"+strInvoiceNo);
				strBuilder.Append("' and Debit_No='"+drEach["Debit_No"].ToString()+"'");

				String strSQLQuery = strBuilder.ToString();			
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("CreditDebitMgrDAL","UpdateDebitNoteDS","UpdateDebitNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note table");

				//delete debitnote detail
				//				String strSQL=" Delete From Debit_Note_Detail where ApplicationID='"+strAppID+"'";
				//				strSQL+=" and EnterpriseID='"+strEnterpriseID+"'";
				//				strSQL+=" and Debit_No='"+ strDebitNo +"' ";					
				//				dbCommandApp = dbConApp.CreateCommand(strSQL,CommandType.Text);
				//				dbCommandApp.Connection = conApp;
				//				dbCommandApp.Transaction = transactionApp;
				//				iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				//				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows Deleted from Debit_Note_Detail table");

				for (int i=0;i<dsCNDetail.Tables[0].Rows.Count;i++)
				{
					DataRow drDND=dsCNDetail.Tables[0].Rows[i];	

					strBuilder.Remove(0,strBuilder.Length);	
						
					strBuilder = new StringBuilder();			
					//					strBuilder.Append(" Insert into Debit_Note_Detail(ApplicationID, EnterpriseID, Invoice_No, ");
					//					strBuilder.Append(" Debit_No, Consignment_No, Debit_Amt, Debit_Description,charge_code,reason_code) Values(");
					//					strBuilder.Append("'"+ strAppID+"','"+strEnterpriseID+"','"+strInvoiceNo+"','"+strDebitNo+"',");

					if(Utility.IsNotDBNull(drDND["Consignment_No"]) ==true && drDND["Consignment_No"].ToString() != "")
					{
						strConsignmentNo=Utility.ReplaceSingleQuote(drDND["Consignment_No"].ToString());						
						strBuilder.Append("'"+strConsignmentNo+"'");						
					}
							
					strBuilder.Append(",");
					if(Utility.IsNotDBNull(drDND["Amt"]) ==true && drDND["Amt"].ToString() != "")
					{
						dDebitAmt=(Decimal) drDND["Amt"];						
						if (drDND.HasVersion(DataRowVersion.Original))
						{					
							if (drDND["Amt", DataRowVersion.Original].ToString() !="")
								dOLDDebitAmt=(Decimal) drDND["Amt", DataRowVersion.Original];
						}	
						strBuilder.Append(dDebitAmt);
					}
					else
					{
						strBuilder.Append("null");
						dDebitAmt=0;
					}

					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["Description"]) ==true && drDND["Description"].ToString() != "")
					{
						strDebitDesc=(String) drDND["Description"];
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(strDebitDesc) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
				
					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["charge_code"]) ==true && drDND["charge_code"].ToString() != "")
					{						
						strBuilder.Append("'"+Utility.ReplaceSingleQuote(drDND["charge_code"].ToString()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	

					strBuilder.Append(",");	
					if(Utility.IsNotDBNull(drDND["reason_code"]) ==true && drDND["reason_code"].ToString().Trim() != "")
					{						
						strBuilder.Append("N'"+Utility.ReplaceSingleQuote(drDND["reason_code"].ToString().Trim()) +"'");
					}
					else
					{
						strBuilder.Append("null");						
					}	
					strBuilder.Append(")");	
																										
					//NOW INSERT Debit_Note_Detail Information
					//					strSQLQuery = strBuilder.ToString();			
					//					dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
					//					dbCommandApp.Connection = conApp;
					//					dbCommandApp.Transaction = transactionApp;
					//iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
					//Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows inserted into Debit_Note_Detail table");
					dTotaDebitAmnt=0;
					if(strStat.ToUpper() == "A")
					{
						//calculate total debit amnt
						strSumBuilder.Remove(0,strSumBuilder.Length);
						strSumBuilder.Append(" select isnull(sum(isnull(Debit_Amt,0)),0) debit_amt from debit_note_detail dt with(nolock) ");
						strSumBuilder.Append(" inner join debit_note dn with(nolock) on dt.debit_no = dn.debit_no and dt.applicationid = dn.applicationid and dt.enterpriseid = dn.enterpriseid ");
						strSumBuilder.Append(" where dt.Consignment_No='"+strConsignmentNo+"' and dt.applicationid = '"+strAppID+"' and dt.enterpriseid = '"+strEnterpriseID+"' and dn.status='A' ");
						dbCommandApp = dbConApp.CreateCommand(strSumBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsTotal = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(Utility.IsNotDBNull(dsTotal.Tables[0].Rows[0])&&dsTotal.Tables[0].Rows.Count>0)
						{
							try
							{
								dTotaDebitAmnt = (Decimal)dsTotal.Tables[0].Rows[0]["debit_amt"];
							}
							catch
							{
								dTotaDebitAmnt=0;
							}
						}
						// Update Shipment Table
						if(strOldStat.ToUpper()=="A")
						{
							strBuilder.Remove(0,strBuilder.Length);
							dActDebitAmt=(dDebitAmt-dOLDDebitAmt);
							strBuilder.Append("Update shipment set Debit_Amt= "+dTotaDebitAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","UpdateDebitNoteDS","UpdateDebitNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						else if(strOldStat.ToUpper()=="N")
						{
							strBuilder.Remove(0,strBuilder.Length);
							dActDebitAmt=(dDebitAmt);
							strBuilder.Append("Update shipment set Debit_Amt=Debit_Amt + "+dActDebitAmt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","UpdateDebitNoteDS","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						strBuilder.Remove(0,strBuilder.Length);
						strBuilder.Append("select isnull(inv.invoice_amt,0)+isnull(s.debit_Amt,0)-isnull(s.credit_amt,0) total_cons_revenue from invoice_detail inv with(nolock) inner join shipment s with(nolock) on inv.invoice_no=s.invoice_no and inv.Consignment_No=s.Consignment_No ");
						strBuilder.Append("where s.Consignment_No='"+strConsignmentNo+"' and s.invoice_no = '"+strInvoiceNo+"' ");
						strBuilder.Append(" and s.applicationid = '"+strAppID+"' and s.enterpriseid = '"+strEnterpriseID+"'");
						dbCommandApp = dbConApp.CreateCommand(strBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsResult = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(dsResult.Tables[0].Rows.Count>0)
							if(Utility.IsNotDBNull(dsResult.Tables[0].Rows[0]))
							{
								Decimal dRevenue = (Decimal)dsResult.Tables[0].Rows[0]["total_cons_revenue"];
								strBuilder.Remove(0,strBuilder.Length);
								strBuilder.Append("Update shipment set total_cons_revenue = "+dRevenue+" where Consignment_No='"+strConsignmentNo+"' ");
								strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
								strSQLQuery = strBuilder.ToString();			
								dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
								dbCommandApp.Connection = conApp;
								dbCommandApp.Transaction = transactionApp;
								iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
								Logger.LogTraceInfo("CreditDebitMgrDAL","UpdateDebitNoteDS","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

							}
					}
					else if (strStat.ToUpper() == "C")
					{
						if(strOldStat.ToUpper()=="A")
						{
							//calculate total debit amnt
							strSumBuilder.Remove(0,strSumBuilder.Length);
							strSumBuilder.Append(" select isnull(sum(isnull(Debit_Amt,0)),0) debit_amt from debit_note_detail dt with(nolock) ");
							strSumBuilder.Append(" inner join debit_note dn with(nolock) on dt.debit_no = dn.debit_no and dt.applicationid = dn.applicationid and dt.enterpriseid = dn.enterpriseid ");
							strSumBuilder.Append(" where dt.Consignment_No='"+strConsignmentNo+"' and dt.applicationid = '"+strAppID+"' and dt.enterpriseid = '"+strEnterpriseID+"' and dn.status='A' ");
							dbCommandApp = dbConApp.CreateCommand(strSumBuilder.ToString(),CommandType.Text);
							dbCommandApp.Connection = conApp;
							DataSet dsTotal = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
							if(Utility.IsNotDBNull(dsTotal.Tables[0].Rows[0])&&dsTotal.Tables[0].Rows.Count>0)
							{
								try
								{
									dTotaDebitAmnt = (Decimal)dsTotal.Tables[0].Rows[0]["debit_amt"];
								}
								catch
								{
									dTotaDebitAmnt=0;
								}
							}

							// Update Shipment Table
							strBuilder.Remove(0,strBuilder.Length);
							//dActDebitAmt=(dDebitAmt-dOLDDebitAmt);
							strBuilder.Append("Update shipment set Debit_Amt="+dTotaDebitAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","UpdateDebitNoteDS","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						strBuilder.Remove(0,strBuilder.Length);
						strBuilder.Append("select isnull(inv.invoice_amt,0)+isnull(s.debit_Amt,0)-isnull(s.credit_amt,0) total_cons_revenue from invoice_detail inv with(nolock) inner join shipment s with(nolock) on inv.invoice_no=s.invoice_no and inv.Consignment_No=s.Consignment_No ");
						strBuilder.Append("where s.Consignment_No='"+strConsignmentNo+"' and s.invoice_no = '"+strInvoiceNo+"' ");
						strBuilder.Append(" and s.applicationid = '"+strAppID+"' and s.enterpriseid = '"+strEnterpriseID+"'");
						dbCommandApp = dbConApp.CreateCommand(strBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsResult = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(dsResult.Tables[0].Rows.Count>0)
							if(Utility.IsNotDBNull(dsResult.Tables[0].Rows[0]))
							{
								Decimal dRevenue = (Decimal)dsResult.Tables[0].Rows[0]["total_cons_revenue"];
								strBuilder.Remove(0,strBuilder.Length);
								strBuilder.Append("Update shipment set total_cons_revenue = "+dRevenue+" where Consignment_No='"+strConsignmentNo+"' ");
								strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
								strSQLQuery = strBuilder.ToString();			
								dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
								dbCommandApp.Connection = conApp;
								dbCommandApp.Transaction = transactionApp;
								iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
								Logger.LogTraceInfo("CreditDebitMgrDAL","UpdateDebitNoteDS","ModifyDebitNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

							}
					}
												
				}
						
				//commit the transaction
				//transactionApp.Commit();
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateDebitNoteDS","ERR0014","App db Transaction committed.");
					

			}
						
			//			}					
			//			catch(ApplicationException appException)
			//			{				
			//				
			//			}
			//			catch(Exception exception)
			//			{
			//				
			//			}
			//			finally
			//			{
			//				
			//			}			
			return iRowsAffected;
		}

		public static int GetTotalCNDN(String strAppID,String strEnterpriseID,String sInvoiceNo) 
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","GetDebitNoteDS","EDB101","DbConnection object is null!!");
				
			}
			int iCNTotal ;
			int iDNTotal ;
			DataSet ds ;
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" SELECT count(*) total_cn  ");
			strBuilder.Append(" FROM credit_note cn ");
			strBuilder.Append(" INNER JOIN invoice inv on cn.invoice_no = inv.invoice_no "); 
			strBuilder.Append(" WHERE cn.status <> 'C' "); 
			strBuilder.Append(" AND inv.invoice_no='"+ Utility.ReplaceSingleQuote(sInvoiceNo) +"' ");
			strBuilder.Append(" AND cn.ApplicationID = '"+strAppID+ "' and cn.Enterpriseid = '"+strEnterpriseID+"' ");

			strBuilder.Append(" SELECT count(*) total_dn  ");
			strBuilder.Append(" FROM debit_note dn ");
			strBuilder.Append(" INNER JOIN invoice inv on dn.invoice_no = inv.invoice_no  "); 
			strBuilder.Append(" WHERE dn.status <> 'C' "); 
			strBuilder.Append(" AND inv.invoice_no='"+ Utility.ReplaceSingleQuote(sInvoiceNo) +"' ");
			strBuilder.Append(" AND dn.ApplicationID = '"+strAppID+ "' and dn.Enterpriseid = '"+strEnterpriseID+"' ");

			
			String strSQLQuery = strBuilder.ToString();			
			ds = (DataSet) dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			iCNTotal = int.Parse(ds.Tables[0].Rows[0][0].ToString());
			iDNTotal = int.Parse(ds.Tables[1].Rows[0][0].ToString());
				
			return iCNTotal+iDNTotal;
		}
		
		public static int UpdateCreditShipment(String strAppID, String strEnterpriseID,DataSet dsToModify, DataSet dsCNDetail)
		{

			int iRowsAffected = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;
			
			//Get App Database Connection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditShipment","ERR0001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}			
			
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditShipment","ERR0002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditShipment","ERR0003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)				
					conApp.Close();
				
				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditShipment","ERR0004","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}		

			try
			{
				int iRowCnt = dsToModify.Tables[0].Rows.Count;
				if (iRowCnt<=0) 
					return iRowsAffected;

				StringBuilder strBuilder = new StringBuilder();
				StringBuilder strSumBuilder = new StringBuilder();

				String strInvoiceNo=null;
				String strConsignmentNo=null;
				//String strCreditNo=null;
				Decimal dCreditAmt=0;
				Decimal dOLDCreditAmt=0;
				Decimal dActCreditAmt=0;
				Decimal dTotaCreditAmnt=0;
				//String strCreditDesc=null;
				String strOldStat="";
				String strStat="";

				String strSQLQuery="";

				for (int i=0;i<dsCNDetail.Tables[0].Rows.Count;i++)
				{
					DataRow drDND=dsCNDetail.Tables[0].Rows[i];	

					strBuilder.Remove(0,strBuilder.Length);	

					//Remove inserting code

					dTotaCreditAmnt=0;
					if(strStat.ToUpper() == "A")
					{
						//calculate total debit amnt
						strSumBuilder.Remove(0,strSumBuilder.Length);
						strSumBuilder.Append(" select  Credit_Amt from credit_note_detail dt with(nolock) ");
						strSumBuilder.Append(" inner join credit_note dn with(nolock) on dt.Credit_No = dn.Credit_No and dt.applicationid = dn.applicationid and dt.enterpriseid = dn.enterpriseid ");
						strSumBuilder.Append(" where dt.Consignment_No='"+strConsignmentNo+"' and dt.applicationid = '"+strAppID+"' and dt.enterpriseid = '"+strEnterpriseID+"' and dn.status='A' ");
						dbCommandApp = dbConApp.CreateCommand(strSumBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsTotal = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(Utility.IsNotDBNull(dsTotal.Tables[0].Rows[0])&&dsTotal.Tables[0].Rows.Count>0)
						{
							try
							{
								dTotaCreditAmnt = (Decimal)dsTotal.Tables[0].Rows[0]["Credit_Amt"];
							}
							catch
							{
								dTotaCreditAmnt=0;
							}
						}
						// Update Shipment Table
						if(strOldStat.ToUpper()=="A")
						{
							strBuilder.Remove(0,strBuilder.Length);
							dActCreditAmt=(dCreditAmt-dOLDCreditAmt);
							strBuilder.Append("Update shipment set Credit_Amt= "+dTotaCreditAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						else if(strOldStat.ToUpper()=="N")
						{
							strBuilder.Remove(0,strBuilder.Length);
							dActCreditAmt=(dCreditAmt);
							strBuilder.Append("Update shipment set Credit_Amt="+dActCreditAmt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						strBuilder.Remove(0,strBuilder.Length);
						strBuilder.Append("select isnull(inv.invoice_amt,0)+isnull(s.debit_Amt,0)-isnull(s.credit_amt,0) total_cons_revenue from invoice_detail inv with(nolock) inner join shipment s with(nolock) on inv.invoice_no=s.invoice_no and inv.Consignment_No=s.Consignment_No ");
						strBuilder.Append("where s.Consignment_No='"+strConsignmentNo+"' and s.invoice_no = '"+strInvoiceNo+"' ");
						strBuilder.Append(" and s.applicationid = '"+strAppID+"' and s.enterpriseid = '"+strEnterpriseID+"'");
						dbCommandApp = dbConApp.CreateCommand(strBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsResult = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(dsResult.Tables[0].Rows.Count>0)
							if(Utility.IsNotDBNull(dsResult.Tables[0].Rows[0]))
							{
								Decimal dRevenue = (Decimal)dsResult.Tables[0].Rows[0]["total_cons_revenue"];
								strBuilder.Remove(0,strBuilder.Length);
								strBuilder.Append("Update shipment set total_cons_revenue = "+dRevenue+" where Consignment_No='"+strConsignmentNo+"' ");
								strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
								strSQLQuery = strBuilder.ToString();			
								dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
								dbCommandApp.Connection = conApp;
								dbCommandApp.Transaction = transactionApp;
								iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
								Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

							}
					}
					else if (strStat.ToUpper() == "C")
					{
						if(strOldStat.ToUpper()=="A")
						{
							//calculate total debit amnt
							strSumBuilder.Remove(0,strSumBuilder.Length);
							strSumBuilder.Append(" select  Credit_Amt from debit_note_detail dt with(nolock) ");
							strSumBuilder.Append(" inner join debit_note dn with(nolock) on dt.Credit_No = dn.Credit_No and dt.applicationid = dn.applicationid and dt.enterpriseid = dn.enterpriseid ");
							strSumBuilder.Append(" where dt.Consignment_No='"+strConsignmentNo+"' and dt.applicationid = '"+strAppID+"' and dt.enterpriseid = '"+strEnterpriseID+"' and dn.status='A' ");
							dbCommandApp = dbConApp.CreateCommand(strSumBuilder.ToString(),CommandType.Text);
							dbCommandApp.Connection = conApp;
							DataSet dsTotal = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
							if(Utility.IsNotDBNull(dsTotal.Tables[0].Rows[0])&&dsTotal.Tables[0].Rows.Count>0)
							{
								try
								{
									dTotaCreditAmnt = (Decimal)dsTotal.Tables[0].Rows[0]["Credit_Amt"];
								}
								catch
								{
									dTotaCreditAmnt=0;
								}
							}

							// Update Shipment Table
							strBuilder.Remove(0,strBuilder.Length);
							//dActCreditAmt=(dCreditAmt-dOLDCreditAmt);
							strBuilder.Append("Update shipment set Credit_Amt="+dTotaCreditAmnt+" where Consignment_No='"+strConsignmentNo+"' ");
							strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
							strSQLQuery = strBuilder.ToString();			
							dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
							dbCommandApp.Connection = conApp;
							dbCommandApp.Transaction = transactionApp;
							iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
							Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");
						}
						strBuilder.Remove(0,strBuilder.Length);
						strBuilder.Append("select isnull(inv.invoice_amt,0)+isnull(s.debit_Amt,0)-isnull(s.credit_amt,0) total_cons_revenue from invoice_detail inv with(nolock) inner join shipment s with(nolock) on inv.invoice_no=s.invoice_no and inv.Consignment_No=s.Consignment_No ");
						strBuilder.Append("where s.Consignment_No='"+strConsignmentNo+"' and s.invoice_no = '"+strInvoiceNo+"' ");
						strBuilder.Append(" and s.applicationid = '"+strAppID+"' and s.enterpriseid = '"+strEnterpriseID+"'");
						dbCommandApp = dbConApp.CreateCommand(strBuilder.ToString(),CommandType.Text);
						dbCommandApp.Connection = conApp;
						DataSet dsResult = (DataSet)dbConApp.ExecuteQuery(dbCommandApp,ReturnType.DataSetType);
						if(dsResult.Tables[0].Rows.Count>0)
							if(Utility.IsNotDBNull(dsResult.Tables[0].Rows[0]))
							{
								Decimal dRevenue = (Decimal)dsResult.Tables[0].Rows[0]["total_cons_revenue"];
								strBuilder.Remove(0,strBuilder.Length);
								strBuilder.Append("Update shipment set total_cons_revenue = "+dRevenue+" where Consignment_No='"+strConsignmentNo+"' ");
								strBuilder.Append(" and applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"'");				
								strSQLQuery = strBuilder.ToString();			
								dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text);
								dbCommandApp.Connection = conApp;
								dbCommandApp.Transaction = transactionApp;
								iRowsAffected = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
								Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0012",iRowsAffected + " rows updated in Shipment table");

							}
					}				
				}

				//commit the transaction
				transactionApp.Commit();
				Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0014","App db Transaction committed.");

			}					
			catch(ApplicationException appException)
			{				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0006","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0008","Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error  "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0009","App db  transaction rolled back.");
				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","UpdateCreditNoteDS","ERR0011","Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}			
			return iRowsAffected;
		}

		public static int InsertShipmentTracking()
		{
			return 1;
		}
		public static Decimal getTotalDebitNote(String application_id,String enterprise_id,String invoice_no)
		{
			DataSet dsResult=null;
			Decimal dTotalDN=0;
			try
			{
				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(application_id,enterprise_id);
				if(dbCon == null)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","getTotalDebitNote","EDB101","DbConnection object is null!!");
					return 0;
				}
				ArrayList arrParam = new ArrayList();
				arrParam.Add(new SqlParameter("@application_id",application_id));
				arrParam.Add(new SqlParameter("@enterprise_id",enterprise_id));
				arrParam.Add(new SqlParameter("@invoice_no",invoice_no));
				dsResult = (DataSet)dbCon.ExecuteProcedure("GetTotalDebitNote",arrParam,ReturnType.DataSetType);
				if(Utility.IsNotDBNull(dsResult)&&dsResult!=null)
				{
					dTotalDN = Convert.ToDecimal(dsResult.Tables[0].Rows[0][0]);
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
			return dTotalDN;
		}
		public static Decimal getTotalCreditNote(String application_id,String enterprise_id,String invoice_no)
		{
			DataSet dsResult=null;
			Decimal dTotalDN=0;
			try
			{
				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(application_id,enterprise_id);
				if(dbCon == null)
				{
					Logger.LogTraceError("CreditDebitMgrDAL","CreditDebitMgrDAL","getTotalDebitNote","EDB101","DbConnection object is null!!");
					return 0;
				}
				ArrayList arrParam = new ArrayList();
				arrParam.Add(new SqlParameter("@application_id",application_id));
				arrParam.Add(new SqlParameter("@enterprise_id",enterprise_id));
				arrParam.Add(new SqlParameter("@invoice_no",invoice_no));
				dsResult = (DataSet)dbCon.ExecuteProcedure("GetTotalCreditNote",arrParam,ReturnType.DataSetType);
				if(Utility.IsNotDBNull(dsResult)&&dsResult!=null)
				{
					dTotalDN = Convert.ToDecimal(dsResult.Tables[0].Rows[0][0]);
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
			return dTotalDN;
		}
	}
}
