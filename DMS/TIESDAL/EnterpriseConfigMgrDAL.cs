using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using com.ties.classes;

namespace TIESDAL
{

	public class EnterpriseConfigMgrDAL
	{
		public EnterpriseConfigMgrDAL()
		{

		}

		public static DomesticShipmentConfigurations GetManualRatingOverrideConfigurations(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","DomesticShipment","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('"+enterpriseID+"','ManualRatingOverride')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","","Error ");
				throw appException;
			}
			return EnterpriseConfigurations.DomesticShipment(dsConfig);
		}

		public static DomesticShipmentConfigurations GetDomesticShipmentConfigurations(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","DomesticShipment","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('"+enterpriseID+"','DomesticShipment')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","","Error ");
				throw appException;
			}
			return EnterpriseConfigurations.DomesticShipment(dsConfig);
		}

		public static InvoiceManagementListingConfigurations GetInvoiceManagementListingConfigurations(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","DomesticShipment","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('"+enterpriseID+"','InvoiceManagementListing')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","","Error ");
				throw appException;
			}

			return EnterpriseConfigurations.InvoiceManagementListing(dsConfig);
		}

		public static InvoiceGenerationPreviewConfigurations GetInvoiceGenerationPreviewConfigurations(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","DomesticShipment","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('"+enterpriseID+"','InvoiceGenerationPreview')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","","Error ");
				throw appException;
			}

			return EnterpriseConfigurations.InvoiceGenerationPreview(dsConfig);
		}

		public static CustomerProfileConfigurations GetCustomerProfileConfigurations(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","DomesticShipment","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('"+enterpriseID+"','CustomerProfile')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","","Error ");
				throw appException;
			}
			return EnterpriseConfigurations.CustomerProfile(dsConfig);
		}


		public static ManifestFormsConfigurations GetManifestFormsConfigurations(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","DomesticShipment","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('"+enterpriseID+"','ManifestForms')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","","Error ");
				throw appException;
			}
			return EnterpriseConfigurations.ManifestForms(dsConfig);
		}


		public static PackageWtDimConfigurations GetPackageWtDimConfigurations(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","PackageWtDim","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('"+enterpriseID+"','SWB_PkgDims')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","GetPackageWtDimConfigurations","","Error ");
				throw appException;
			}
			return EnterpriseConfigurations.PackageWtDimForms(dsConfig);
		}


		public static QueryShipmentTrackingConfigurations GetQueryShipmentTrackingConfigurations(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","QueryShipmentTracking","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('"+enterpriseID+"','QueryShipmentTracking')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("QueryShipmentTrackingManager","GetConfig","","Error ");
				throw appException;
			}
			return EnterpriseConfigurations.QueryShipmentTracking(dsConfig);
		}


		public static CreateUpdateConsignmentConfigurations GetCreateUpdateConsignmentConfigurations(string appID,string enterpriseID,string payerid)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","CreateUpdateConsignmentConfigurations","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry="WITH EnterpriseConfigs AS";
			strQry+="(";
			strQry+="    SELECT [key], [value]";
			strQry+="    FROM dbo.EnterpriseConfigurations('"+enterpriseID+"', 'CreateUpdateConsignment')    ";
			strQry+=") ";
			strQry+="SELECT [key], [value] ";
			strQry+="FROM EnterpriseConfigs ";
			strQry+="UNION ALL ";
			strQry+="SELECT a.[key] ,[value]=CASE WHEN b.[Value] = 'N' THEN '0' ELSE a.[Value] END ";
			strQry+="FROM ";
			strQry+="(";
			strQry+="    SELECT [key]='MaxDeclaredValue'";
			strQry+="        ,[value]=CAST(ISNULL(b.insurance_maximum_amt-b.free_insurance_amt";
			strQry+="        ,a.max_insurance_amt-a.free_insurance_amt) AS VARCHAR(10))";
			strQry+="        ,[key2]='InsSupportedbyEnterprise'";
			strQry+="    FROM dbo.Enterprise a";
			strQry+="    LEFT JOIN dbo.Customer b";
			strQry+="        ON a.applicationid = b.applicationid AND a.enterpriseid = b.enterpriseid AND";
			strQry+="            custid = '"+payerid+"'";
			strQry+="    WHERE a.applicationid = '"+appID+"' AND a.enterpriseid = '"+enterpriseID+"'";
			strQry+="    UNION ALL ";
			strQry+="    SELECT 'MaxCODAmount', '999999', 'CODSupportedbyEnterprise'";
			strQry+=") a ";
			strQry+="JOIN EnterpriseConfigs b ON [key2] = b.[key];";
			
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("CreateUpdateConsignmentConfigurations","GetConfig","","Error ");
				throw appException;
			}
			return EnterpriseConfigurations.CreateUpdateConsignmentPage(dsConfig);
		}


		public static ConsignmentStatusPrintingConfigurations GetConsignmentStatusPrintingConfigurations(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","GetConsignmentStatusPrintingConfigurations","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('"+enterpriseID+"','ConsignmentStatusPrinting')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("GetConsignmentStatusPrintingConfigurations","GetConfig","","Error ");
				throw appException;
			}
			return EnterpriseConfigurations.CreateConsignmentStatusPrintingPage(dsConfig);
		}


		public static PackageDetailsLimitsConfigurations GetPackageDetailsLimitsConfigurations(string appID,string enterpriseID,string payerid)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","GetPackageDetailsLimitsConfigurations","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select [key], [value] from dbo.EnterpriseConfigurations('"+enterpriseID+"','PackageDetailsLimits') ";
			strQry+= "UNION ALL ";
			strQry+= "SELECT 'DensityFactor', ISNULL(b.density_factor, a.density_factor)  ";
			strQry+= "FROM dbo.Enterprise a  ";
			strQry+= "JOIN dbo.Customer b  ON a.applicationid = b.applicationid AND a.enterpriseid = b.enterpriseid  ";
			strQry+= "WHERE a.applicationid = '"+appID+"' AND a.enterpriseid = '"+enterpriseID+"' AND  b.custid = '"+payerid+"' ";
			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("GetPackageDetailsLimitsConfigurations","GetConfig","","Error ");
				throw appException;
			}
			return EnterpriseConfigurations.CreatePackageDetailsLimitsConfigurations(dsConfig);
		}


		public static bool ShowLabelDangerousGoods(string appID,string enterpriseID)
		{
			bool result =false;
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","ShowLabelDangerousGoods","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "SELECT code_text FROM dbo.Enterprise_Configurations where applicationid = '"+appID+"' and enterpriseid = '"+enterpriseID+"' and codeid = 'DangerousGoods'";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if(dsConfig != null && dsConfig.Tables.Count>0 && dsConfig.Tables[0].Rows.Count>0
					&& dsConfig.Tables[0].Rows[0]["code_text"].ToString().Trim().ToUpper() == "Y")
				{
					result = true;
				}
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("ShowLabelDangerousGoods","GetConfig","","Error ");
				throw appException;
			}

			return result;
		}


		public static MaintainPhonesIMEIsConfigurations GetMaintainPhonesIMEIsConfigurations(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","EnterpriseConfigurations","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('"+enterpriseID+"','MaintainPhonesIMEIs')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","GetMaintainPhonesIMEIsConfigurations","","Error ");
				throw appException;
			}
			return EnterpriseConfigurations.MaintainPhonesIMEIs(dsConfig);
		}


		public static int CustomsJobsSearchGridRows(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			int gridRows = 10;
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","CustomsJobsSearchGridRows","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('"+enterpriseID+"','CustomsJobsSearch')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","CustomsJobsSearchGridRows","","Error ");
				throw appException;
			}
			if(dsConfig.Tables[0].Rows.Count>0 && dsConfig.Tables[0].Rows[0]["key"] != DBNull.Value
				&& dsConfig.Tables[0].Rows[0]["key"].ToString().ToLower() =="gridrows")
			{
				try
				{
					gridRows=Convert.ToInt32(dsConfig.Tables[0].Rows[0]["value"].ToString());
				}
				catch
				{
					gridRows=10;
					Logger.LogTraceError("EnterpriseConfigMgrDAL","CustomsJobsSearchGridRows","","Error ");
				}

			}
			return gridRows;
		}


		public static int ConsStatusPrintingGridRows(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			int gridRows = 10;
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","ConsStatusPrintingGridRows","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from dbo.EnterpriseConfigurations('"+enterpriseID+"','ConsStatusPrinting')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","ConsStatusPrintingGridRows","","Error ");
				throw appException;
			}
			if(dsConfig.Tables[0].Rows.Count>0 && dsConfig.Tables[0].Rows[0]["key"] != DBNull.Value
				&& dsConfig.Tables[0].Rows[0]["key"].ToString().ToLower() =="gridrows")
			{
				try
				{
					gridRows=Convert.ToInt32(dsConfig.Tables[0].Rows[0]["value"].ToString());
				}
				catch
				{
					gridRows=10;
					Logger.LogTraceError("EnterpriseConfigMgrDAL","ConsStatusPrintingGridRows","","Error ");
				}

			}
			return gridRows;
		}

		public static DataSet ExportDelConsGridRows(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			int gridRows = 10;
			
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","ConsStatusPrintingGridRows","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			strQry = "select [key],[value] from dbo.EnterpriseConfigurations('"+enterpriseID+"','ExportDelCons')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","ConsStatusPrintingGridRows","","Error ");
				throw appException;
			}
//			if(dsConfig.Tables[0].Rows.Count>0 && dsConfig.Tables[0].Rows[0]["key"] != DBNull.Value
//				&& dsConfig.Tables[0].Rows[0]["key"].ToString().ToLower() =="gridrows")
//			{
//				try
//				{
//					gridRows=Convert.ToInt32(dsConfig.Tables[0].Rows[0]["value"].ToString());
//					tpFilename = dsConfig.Tables[0].Rows[1]["value"].ToString();
//				}
//				catch
//				{
//					gridRows=10;
//					Logger.LogTraceError("EnterpriseConfigMgrDAL","ConsStatusPrintingGridRows","","Error ");
//				}
//
//			}
			return dsConfig;
		}

		public static string TargerSQLServerFolder(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			string gridRows = "";
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","TargerSQLServerFolder","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select [key],[value] from dbo.EnterpriseConfigurations('"+enterpriseID+"','UpdateImportStatus')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","TargerSQLServerFolder","","Error ");
				throw appException;
			}
			if(dsConfig.Tables[0].Rows.Count>0 && dsConfig.Tables[0].Rows[0]["key"] != DBNull.Value
				&& dsConfig.Tables[0].Rows[0]["key"].ToString().ToLower() =="targetsqlserverfolder")
			{
				try
				{
					gridRows=dsConfig.Tables[0].Rows[0]["value"].ToString();
				}
				catch
				{
					gridRows="";
					Logger.LogTraceError("EnterpriseConfigMgrDAL","TargerSQLServerFolder","","Error ");
				}

			}
			return gridRows;
		}


		public static string LicensePlateTemplate(string appID,string enterpriseID)
		{
			string strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsConfig = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			string reportTemplate = "";
			if(dbCon == null)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","LicensePlateTemplate","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select [key],[value] from dbo.EnterpriseConfigurations('"+enterpriseID+"','CreateSIP')";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsConfig = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("EnterpriseConfigMgrDAL","LicensePlateTemplate","","Error ");
				throw appException;
			}
			if(dsConfig.Tables[0].Rows.Count>0 && dsConfig.Tables[0].Rows[0]["key"] != DBNull.Value
				&& dsConfig.Tables[0].Rows[0]["key"].ToString().ToLower() =="licenseplatetemplate")
			{
				try
				{
					reportTemplate=dsConfig.Tables[0].Rows[0]["value"].ToString();
				}
				catch
				{
					reportTemplate="";
					Logger.LogTraceError("EnterpriseConfigMgrDAL","LicensePlateTemplate","","Error ");
				}

			}
			return reportTemplate;
		}

	}
}
