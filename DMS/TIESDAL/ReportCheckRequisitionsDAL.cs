using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using TIESClasses;

namespace TIESDAL
{
	public class ReportCheckRequisitionsDAL
	{
		private string SQLText = string.Empty;
		private DbConnection dbCon;
		private IDbCommand dbCmd = null;

		public ReportCheckRequisitionsDAL(string _appID, string _enterpriseID)
		{
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(_appID, _enterpriseID);
		}

		public DataSet ExecCustomsSearchChequeReqs(ReportCheckRequisitions objInfo)
		{
			DataSet dsReturn = new DataSet();
			try
			{
				if(objInfo.Enterpriseid.Trim().Equals(""))
					throw new ApplicationException("Please specify EnterpriseId");

				SQLText = @"EXEC dbo.Customs_SearchChequeReqs @enterpriseid = '{0}'";
				SQLText = string.Format(SQLText, objInfo.Enterpriseid.Trim());
				
				if(!objInfo.StartDate.Trim().Equals(""))
					SQLText += string.Format(", @StartDate = '{0}'", objInfo.StartDate.Trim());
				if(!objInfo.EndDate.Trim().Equals(""))
					SQLText += string.Format(", @EndDate = '{0}'", objInfo.EndDate.Trim());
				if(!objInfo.DateType .Trim().Equals(""))
					SQLText += string.Format(", @DateType = {0}", objInfo.DateType .Trim());
				if(!objInfo.ChequeNo .Trim().Equals(""))
					SQLText += string.Format(", @Cheque_No = {0}", objInfo.ChequeNo .Trim());
				if(!objInfo.ChequeNoTo .Trim().Equals(""))
					SQLText += string.Format(", @Cheque_No_To = {0}", objInfo.ChequeNoTo .Trim());
				if(!objInfo.RequisitionNo.Trim().Equals(""))
					SQLText += string.Format(", @Requisition_No = {0}", objInfo.RequisitionNo .Trim());
				if(!objInfo.RequisitionNoTo.Trim().Equals(""))
					SQLText += string.Format(", @Requisition_No_To = {0}", objInfo.RequisitionNoTo .Trim());
				if(!objInfo.ChequeReqType.Trim().Equals(""))
					SQLText += string.Format(", @ChequeReq_Type = '{0}'", objInfo.ChequeReqType .Trim());
				if(!objInfo.PayeeName.Trim().Equals(""))
					SQLText += string.Format(", @PayeeName = '{0}'", objInfo.PayeeName .Trim());
				if(!objInfo.ReceiptNumber.Trim().Equals(""))
					SQLText += string.Format(", @ReceiptNumber = '{0}'", objInfo.ReceiptNumber .Trim());

				dbCmd = dbCon.CreateCommand(SQLText, CommandType.Text);
				dsReturn = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method ExecCustomsSearchInvoicesCheques : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "ReportCheckRequisitionsDAL", "ExecCustomsSearchChequeReqs", msgException);
				throw new ApplicationException(msgException);
			}
		}
	}
}
