using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for DistributionCenter.
	/// </summary>
	public class DistributionCenterDAL
	{
		public DistributionCenterDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}


	
		/*Comment by Sompote 2010-04-05
		public static String GetHubStationOfAutoManifest(String strAppID, String strEnterpriseID)
		{
			String origin_code = "";
			SessionDS sessionDS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("DistributionCenter","GetHubStationOfAutoManifest","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DistributionCenter::GetHubStationOfAutoManifest()] DbConnection is null","ERROR");
				return origin_code;
			}

			StringBuilder str = new StringBuilder();
			str.Append("select origin_code ");
			str.Append(" from Distribution_center  ");
			str.Append(" where applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("' and hub = 'Y'");


			sessionDS = dbCon.ExecuteQuery(str.ToString(),0, 0,"HubStationOfAutoManifest");

			if (sessionDS.ds.Tables[0].Rows.Count > 0)
				origin_code = (String)sessionDS.ds.Tables[0].Rows[0]["origin_code"];

			return  origin_code;
		}
		*/
		/// <summary>
		/// //Automatically Assing to Manifests (DMS Phase 1)
		/// 15.1. Locate the hub distribution center:
		/// </summary>
		public static String GetHubStationOfAutoManifest(String strAppID, String strEnterpriseID)
		{
			String origin_code = "";
			SessionDS sessionDS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("DistributionCenter","GetHubStationOfAutoManifest","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DistributionCenter::GetHubStationOfAutoManifest()] DbConnection is null","ERROR");
				return origin_code;
			}
			return GetHubStationOfAutoManifest( strAppID,  strEnterpriseID, dbCon);
			
		}
		
		/// <summary>
		/// Add by Sompote 2010-04-05
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="dbCon"></param>
		/// <returns></returns>
		public static String GetHubStationOfAutoManifest(String strAppID, String strEnterpriseID,DbConnection dbCon)
		{
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DistributionCenter","GetHubStationOfAutoManifest","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DistributionCenter","GetHubStationOfAutoManifest","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DistributionCenter","GetHubStationOfAutoManifest","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	
			
			//return GetHubStationOfAutoManifest( strAppID,  strEnterpriseID, dbCon, dbCmd);
			
			string result  ;	
			try
			{
				result = GetHubStationOfAutoManifest( strAppID,  strEnterpriseID, dbCon, dbCmd);
			}
			catch(ApplicationException appExpection)
			{
				throw appExpection;
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return result;
		}

		
		/// <summary>
		/// Add by Sompote 2010-04-05
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="dbCon"></param>
		/// <param name="dbCmd"></param>
		/// <returns></returns>
		public static String GetHubStationOfAutoManifest(String strAppID, String strEnterpriseID,DbConnection dbCon,IDbCommand dbCmd)
		{
			String origin_code = "";
			SessionDS sessionDS = new SessionDS();			

			StringBuilder str = new StringBuilder();
			str.Append("select origin_code ");
			str.Append(" from Distribution_center  ");
			str.Append(" where applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("' and hub = 'Y'");

			dbCmd.CommandText = str.ToString();

			//sessionDS.ds = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,0, 0,"HubStationOfAutoManifest");
			sessionDS.ds = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);

			if (sessionDS.ds.Tables[0].Rows.Count > 0)
				origin_code = (String)sessionDS.ds.Tables[0].Rows[0]["origin_code"];

			return  origin_code;
		}

		
		/* Comment by Sompote 2010-04-05
		public static bool IsDChasSWB(String strAppID, String strEnterpriseID,String strDC)
		{
			SessionDS sessionDS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);


			StringBuilder str = new StringBuilder();
			str.Append("select * ");
			str.Append(" from Distribution_center  ");
			str.Append(" where applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("' and origin_code = '");
			str.Append(strDC);
			str.Append("' and manifest = 'Y'");


			sessionDS = dbCon.ExecuteQuery(str.ToString(),0, 0,"HubStationOfAutoManifest");

			if (sessionDS.ds.Tables[0].Rows.Count > 0)
				return true;

				return false;
		}
	
		*/

		/// <summary>
		/// Add by Sompote 2010-04-05
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="strDC"></param>
		/// <returns></returns>
		public static bool IsDChasSWB(String strAppID, String strEnterpriseID,String strDC)
		{		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("DistributionCenter","IsDChasSWB","EDB101","DbConnection object is null!!");
				System.Diagnostics.Trace.WriteLineIf(Logger.AppSwitch.TraceError,"[DistributionCenter::IsDChasSWB()] DbConnection is null","ERROR");
			}		
			return DistributionCenterDAL.IsDChasSWB( strAppID,  strEnterpriseID, strDC, dbCon);
		}
		/// <summary>
		/// Add by Sompote 2010-04-05
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="strDCDbConnection"></param>
		/// <returns></returns>
		public static bool IsDChasSWB(String strAppID, String strEnterpriseID,String strDC,DbConnection dbCon)
		{			
			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("DistributionCenter","IsDChasSWB","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DistributionCenter","IsDChasSWB","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("DistributionCenter","IsDChasSWB","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			IDbCommand dbCmd = null;		
			dbCmd = dbCon.CreateCommand("",CommandType.Text);
			dbCmd.Connection = conApp;	
			//return DistributionCenterDAL.IsDChasSWB( strAppID,  strEnterpriseID, strDC, dbCon, dbCmd);
			bool result = false;	
			try
			{
				result = DistributionCenterDAL.IsDChasSWB( strAppID,  strEnterpriseID, strDC, dbCon, dbCmd);
			}
			catch(ApplicationException appExpection)
			{
				throw appExpection;
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return result;
		}
		/// <summary>
		//Add by Sompote 2010-04-05
		/// 
		/// </summary>
		/// <param name="strAppID"></param>
		/// <param name="strEnterpriseID"></param>
		/// <param name="strDC"></param>
		/// <param name="dbCon"></param>
		/// <param name="dbCmd"></param>
		/// <returns></returns>
		public static bool IsDChasSWB(String strAppID, String strEnterpriseID,String strDC,DbConnection dbCon,IDbCommand dbCmd)
		{
			SessionDS sessionDS = null;

			StringBuilder str = new StringBuilder();
			str.Append("select * ");
			str.Append(" from Distribution_center  ");
			str.Append(" where applicationid = '");
			str.Append(strAppID);
			str.Append("' and enterpriseid = '");
			str.Append(strEnterpriseID);
			str.Append("' and origin_code = '");
			str.Append(strDC);
			str.Append("' and manifest = 'Y'");

			dbCmd.CommandText = str.ToString();

			//sessionDS = dbCon.ExecuteQuery(dbCmd,0, 0,"HubStationOfAutoManifest");
			sessionDS = new SessionDS();
			sessionDS.ds = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd,ReturnType.DataSetType);
			
			if (sessionDS.ds.Tables[0].Rows.Count > 0)
				return true;

			return false;
		}
	
	}
}
