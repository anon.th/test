using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for ReportDAL.
	/// </summary>
	public class ReportDAL
	{
		public ReportDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		public static DataSet GetSopScaning(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsSop = null;

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("ReportDAL.cs","GetSopScaning","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//@consignment_no 		varchar(30) = null,
			//	@user_id				varchar(30) = null,
			//	@service_type			varchar(5) = null,
			//	@delivery_route_code	varchar(12) = null,
			//	@destination_zipcode	varchar(5) = null,
			//	@scanning_datetime		varchar(100) = null

			DataRow dr = dsQuery.Tables[0].Rows[0];

			ArrayList sopscan_params = new ArrayList();
			//*************************** Consignment_no : Part ***************************

			string consignment_no = "";
			string user_id = "";
			string service_type = "";
			string delivery_route_code = "";
			string destination_zipcode = "";
			string scanning_datetime = "";
			string line_haul = "";
			if (Utility.IsNotDBNull(dr["consignment_no"]) )
				consignment_no = dr["consignment_no"].ToString();
			if (Utility.IsNotDBNull(dr["user_id"]) )
				user_id = dr["user_id"].ToString();
			if (Utility.IsNotDBNull(dr["service_type"]) )
				service_type = dr["service_type"].ToString();
			if (Utility.IsNotDBNull(dr["delivery_route_code"]) )
				delivery_route_code = dr["delivery_route_code"].ToString();
			if (Utility.IsNotDBNull(dr["destination_zipcode"]) )
				destination_zipcode = dr["destination_zipcode"].ToString();
			if (Utility.IsNotDBNull(dr["scanning_datetime"]) )
				scanning_datetime = dr["scanning_datetime"].ToString();
			if (Utility.IsNotDBNull(dr["line_haul"]) )
				line_haul = dr["line_haul"].ToString();

			sopscan_params.Add(dbCon.MakeParam("@consignment_no", System.Data.DbType.String , 30, ParameterDirection.Input, consignment_no));
			sopscan_params.Add(dbCon.MakeParam("@user_id", System.Data.DbType.String, 30, ParameterDirection.Input, user_id));
			sopscan_params.Add(dbCon.MakeParam("@service_type", System.Data.DbType.String, 10, ParameterDirection.Input, service_type));
			sopscan_params.Add(dbCon.MakeParam("@delivery_route_code", System.Data.DbType.String, 12, ParameterDirection.Input, delivery_route_code));
			sopscan_params.Add(dbCon.MakeParam("@destination_zipcode", System.Data.DbType.String, 5, ParameterDirection.Input, destination_zipcode));
			sopscan_params.Add(dbCon.MakeParam("@scanning_datetime", System.Data.DbType.String, 100, ParameterDirection.Input, scanning_datetime));
			sopscan_params.Add(dbCon.MakeParam("@line_haul", System.Data.DbType.String, 12, ParameterDirection.Input, line_haul));

			string strStoredName = "";
			if (Utility.IsNotDBNull(dr["missconsignment"]) )
			{
				strStoredName = "sp_SOP_Shipment_Report_SOPM";
			}
			else
			{
				strStoredName = "sp_SOP_Shipment_Report_SOP";
			}

			dbcmd = dbCon.CreateCommand(strStoredName, sopscan_params, CommandType.StoredProcedure);
			
			try
			{
				dsSop = (DataSet)dbCon.ExecuteQuery(dbcmd, com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ReportDAL.cs","GetSopScaning","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsSop;
		}

		public static DataSet GetTopNPayer(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsTopN = null;
			StringBuilder strBuilder = null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("ReportDAL.cs","GetTopNPayer","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			string strSqlQuery = null;
			string strSqlWhere = null;
			string strSqlSort = null;

			DataRow dr = dsQuery.Tables[0].Rows[0];
			strBuilder = new StringBuilder();

			//*************************** Booking Date : Part ***************************
			if (Utility.IsNotDBNull(dr["start_date"]) && Utility.IsNotDBNull(dr["end_date"]))
			{
				DateTime dateStartDate = (DateTime) dr["start_date"];
				DateTime dateEndDate = (DateTime) dr["end_date"];
				string strStartDate = Utility.DateFormat(strAppID, strEnterpriseID, dateStartDate, DTFormat.DateTime);
				string strEndDate = Utility.DateFormat(strAppID, strEnterpriseID, dateEndDate, DTFormat.DateTime);

				strBuilder.Append(" and s.booking_datetime >= ");
				strBuilder.Append(strStartDate);
				strBuilder.Append(" and s.booking_datetime <= ");
				strBuilder.Append(strEndDate);
				strBuilder.Append(" ");
			}

			//*************************** Payer Type : Part ***************************
			string strCustPayerType = dr["payer_type"].ToString() + "";
			if (strCustPayerType != "")
			{
				strBuilder.Append(" and c.payer_type in (");
				strBuilder.Append(strCustPayerType);
				strBuilder.Append(") ");
			}

			//*************************** Route / DC Selection : Part ***************************
			string strorigin_dc = dr["origin_dc"].ToString() + "";
			if (strorigin_dc != "")
			{
				strBuilder.Append(" and s.origin_station = '");
				strBuilder.Append(strorigin_dc);
				strBuilder.Append("' ");
			}
			string strdestination_dc = dr["destination_dc"].ToString() + "";
			if (strdestination_dc != "")
			{
				strBuilder.Append(" and s.destination_station = '");
				strBuilder.Append(strdestination_dc);
				strBuilder.Append("' ");
			}
			string strroute_code = dr["route_code"].ToString() + "";
			if (strroute_code != "")
			{
				string strroute_type = dr["route_type"].ToString() + "";
				if (strroute_type == "L" || strroute_type == "A")
				{
					string strdelPath_origin_dc = dr["delPath_origin_dc"].ToString() + "";
					strBuilder.Append(" and s.origin_station = '");
					strBuilder.Append(strdelPath_origin_dc);
					strBuilder.Append("' ");

					string strdelPath_destination_dc = dr["delPath_destination_dc"].ToString() + "";
					strBuilder.Append(" and s.destination_station = '");
					strBuilder.Append(strdelPath_destination_dc);
					strBuilder.Append("' ");
				}
				else
				{
					strBuilder.Append(" and s.route_code = '");
					strBuilder.Append(strroute_code);
					strBuilder.Append("' ");
				}
			}

			//*************************** Destination ***************************
			string strzip_code = dr["zip_code"].ToString() + "";
			if (strzip_code != "")
			{
				strBuilder.Append(" and s.recipient_zipcode = '");
				strBuilder.Append(strzip_code);
				strBuilder.Append("' ");
			}
			string strstate_code = dr["state_code"].ToString() + "";
			if (strstate_code != "")
			{
				strBuilder.Append(" and s.destination_state_code = '");
				strBuilder.Append(strstate_code);
				strBuilder.Append("' ");
			}

			//*************************** Retrieval Basis on : Part ***************************
			string strBasis = dr["basis"].ToString();
			if (strBasis == "I")
				strBuilder.Append(" and s.invoice_no is not null ");
			else if (strBasis == "D")
				strBuilder.Append(" and (s.delivery_manifested = 'Y' or s.delivery_manifested = 'y' ) ");
			else if (strBasis == "B")
				strBuilder.Append(" and s.invoice_no is not null ");

			strSqlWhere = strBuilder.ToString();

            int intTop = 0;
            string topN = dr["top_n"].ToString();
            if(topN != "" && topN != null)
            {
                intTop = (int)dr["top_n"];
            }
            else
            {
                intTop = -1;
            }            

			strSqlSort += " ORDER BY ";
			string strSort = dr["sort_by"].ToString();
			if (strSort == "R")
				strSqlSort += "revenue desc ";
			else if (strSort == "W")
				strSqlSort += "chargeable_wt desc ";
			else if (strSort == "V")
				strSqlSort += "tot_volume desc ";
			else
				strSqlSort += "no_of_consignment desc ";

			DateTime dateStartDate2 = (DateTime) dr["start_date"];
			DateTime dateEndDate2 = (DateTime) dr["end_date"];
			string strStartDate2 = Utility.DateFormat(strAppID, strEnterpriseID, dateStartDate2, DTFormat.DateTime);
			string strEndDate2 = Utility.DateFormat(strAppID, strEnterpriseID, dateEndDate2, DTFormat.DateTime);
			
			string strSqlDocsFlag = "";
			string intl_docs_flag = dr["intl_docs_flag"].ToString();
			
			if(intl_docs_flag == "DC")
				strSqlDocsFlag = " AND a.ConsignmentType = 'D' ";
				//strSqlDocsFlag = " AND s.consignment_no IN (SELECT S.consignment_no FROM dbo.Shipment S INNER JOIN dbo.Enterprise E ON S.sender_country = E.country AND S.applicationid = E.applicationid AND S.enterpriseid = E.enterpriseid WHERE E.applicationid = '"+strAppID+"' AND E.enterpriseid = '"+strEnterpriseID+"' AND  S.invoice_no IS NOT NULL AND ISNULL(S.export_flag, 'N') != 'Y' AND ISNULL(S.intl_docs_flag, 'N') IN ('Y', 'N') AND booking_datetime >= " + strStartDate2 + " and booking_datetime <= " + strEndDate2 + ") ";
			else if(intl_docs_flag == "DF")
				strSqlDocsFlag = " AND a.ConsignmentType = 'D' ";
				//strSqlDocsFlag = " AND s.consignment_no IN (SELECT S.consignment_no FROM dbo.Shipment S INNER JOIN dbo.Enterprise E ON S.sender_country = E.country AND S.applicationid = E.applicationid AND S.enterpriseid = E.enterpriseid WHERE E.applicationid = '"+strAppID+"' AND E.enterpriseid = '"+strEnterpriseID+"' AND  S.invoice_no IS NOT NULL AND ISNULL(S.export_flag, 'N') != 'Y' AND ISNULL(S.intl_docs_flag, 'N') IN ('N') AND booking_datetime >= " + strStartDate2 + " and booking_datetime <= " + strEndDate2 + ") ";
			else if(intl_docs_flag == "DD")
				strSqlDocsFlag = " AND a.ConsignmentType = 'D' ";
				//strSqlDocsFlag = " AND s.consignment_no IN (SELECT S.consignment_no FROM dbo.Shipment S INNER JOIN dbo.Enterprise E ON S.sender_country = E.country AND S.applicationid = E.applicationid AND S.enterpriseid = E.enterpriseid WHERE E.applicationid = '"+strAppID+"' AND E.enterpriseid = '"+strEnterpriseID+"' AND  S.invoice_no IS NOT NULL AND ISNULL(S.export_flag, 'N') != 'Y' AND  ISNULL(S.intl_docs_flag, 'N') IN ('Y') AND booking_datetime >= " + strStartDate2 + " and booking_datetime <= " + strEndDate2 + ") ";
			else if(intl_docs_flag == "EC")
				strSqlDocsFlag = " AND a.ConsignmentType = 'E' ";
				//strSqlDocsFlag = " AND s.consignment_no IN (SELECT S.consignment_no FROM dbo.Shipment S INNER JOIN dbo.Enterprise E ON S.sender_country = E.country AND S.applicationid = E.applicationid AND S.enterpriseid = E.enterpriseid WHERE E.applicationid = '"+strAppID+"' AND E.enterpriseid = '"+strEnterpriseID+"' AND  S.invoice_no IS NOT NULL AND ISNULL(S.export_flag, 'N') = 'Y' AND ISNULL(S.intl_docs_flag, 'N') IN ('Y', 'N') AND booking_datetime >= " + strStartDate2 + " and booking_datetime <= " + strEndDate2 + ") ";
			else if(intl_docs_flag == "EF")
				strSqlDocsFlag = " AND a.ConsignmentType = 'E' ";
				//strSqlDocsFlag = " AND s.consignment_no IN (SELECT S.consignment_no FROM dbo.Shipment S INNER JOIN dbo.Enterprise E ON S.sender_country = E.country AND S.applicationid = E.applicationid AND S.enterpriseid = E.enterpriseid WHERE E.applicationid = '"+strAppID+"' AND E.enterpriseid = '"+strEnterpriseID+"' AND  S.invoice_no IS NOT NULL AND ISNULL(S.export_flag, 'N') = 'Y' AND ISNULL(S.intl_docs_flag, 'N') IN ('N') AND booking_datetime >= " + strStartDate2 + " and booking_datetime <= " + strEndDate2 + ") ";
			else if(intl_docs_flag == "ED")
				strSqlDocsFlag = " AND a.ConsignmentType = 'E' ";
				//strSqlDocsFlag = " AND s.consignment_no IN (SELECT S.consignment_no FROM dbo.Shipment S INNER JOIN dbo.Enterprise E ON S.sender_country = E.country AND S.applicationid = E.applicationid AND S.enterpriseid = E.enterpriseid WHERE E.applicationid = '"+strAppID+"' AND E.enterpriseid = '"+strEnterpriseID+"' AND  S.invoice_no IS NOT NULL AND ISNULL(S.export_flag, 'N') = 'Y' AND ISNULL(S.intl_docs_flag, 'N') IN ('Y') AND booking_datetime >= " + strStartDate2 + " and booking_datetime <= " + strEndDate2 + ") ";
			else
				strSqlDocsFlag = " AND a.ConsignmentType = 'I' ";
				//strSqlDocsFlag = " AND s.consignment_no IN (SELECT S.consignment_no FROM dbo.Shipment S INNER JOIN dbo.Enterprise E ON S.recipient_country = E.country AND S.applicationid = E.applicationid AND S.enterpriseid = E.enterpriseid WHERE E.applicationid = '"+strAppID+"' AND E.enterpriseid = '"+strEnterpriseID+"' AND booking_datetime >= " + strStartDate2 + " and booking_datetime <= " + strEndDate2 + ") AND S.consignment_no in (SELECT S.consignment_no FROM dbo.Shipment S WHERE S.sender_name IN (SELECT [code1]  FROM Customs_Configurations WHERE codeid in ('AW_BondedWarehouses' , 'BondedWarehouses') AND code3 = 'Y' AND applicationid='TIES' AND enterpriseid='PNGAF')) ";

			strBuilder = new StringBuilder();

			switch (dbProvider)
			{
				case DataProviderType.Sql :
                    if(intTop >= 0)
                    {
                        strBuilder.Append("SELECT TOP ");
                        strBuilder.Append(intTop);
                    }
                    else
                    {
                        strBuilder.Append("SELECT ");                     
                    }
					strBuilder.Append(" enterprise_name, currency, payerid, payer_name, payer_type, count(consignment_no) as no_of_consignment, ");
					strBuilder.Append("sum(chargeable_wt) as chargeable_wt, sum(tot_volume) AS tot_volume, sum(case mbg ");
					strBuilder.Append("WHEN 'Y' THEN 0 ELSE (CASE WHEN ConsignmentType='D' THEN ISNULL(total_rated_amount, 0) + ISNULL(tax_on_rated_amount, 0) ");
					strBuilder.Append("WHEN ConsignmentType='E' THEN ISNULL(total_rated_amount, 0) + ISNULL(tax_on_rated_amount, 0) + ISNULL(export_freight_charge, 0) ");
					strBuilder.Append("WHEN ConsignmentType='I' THEN ISNULL(invoice_amt,0) ");
					strBuilder.Append("END) end) as revenue ");
					strBuilder.Append("FROM (SELECT s.*, a.ConsignmentType, e.enterprise_name, e.currency, "); 
					strBuilder.Append("(SELECT sum(pkg_length * pkg_breadth * pkg_height) FROM dbo.shipment_pkg "); 
					strBuilder.Append("WHERE consignment_no = s.consignment_no AND applicationid = s.applicationid AND enterpriseid = s.enterpriseid AND booking_no = s.booking_no) AS tot_volume ");
					strBuilder.Append("FROM Shipment s CROSS APPLY dbo.GetConsignmentType (s.enterpriseid, s.export_flag, s.sender_name, s.sender_zipcode, s.recipient_zipcode) a ");
					strBuilder.Append("INNER JOIN Enterprise e ON e.applicationid = s.applicationid AND e.enterpriseid = s.enterpriseid "); 
					strBuilder.Append("INNER JOIN Customer c ON s.applicationid = c.applicationid AND s.enterpriseid = c.enterpriseid AND s.payerid = c.custid ");
					strBuilder.Append("WHERE s.applicationid = '");
					strBuilder.Append(strAppID);
					strBuilder.Append("' and s.enterpriseid = '");
					strBuilder.Append(strEnterpriseID);
					strBuilder.Append("' ");
					strBuilder.Append(strSqlWhere);
					strBuilder.Append(strSqlDocsFlag);
					strBuilder.Append(") b ");
					strBuilder.Append(" GROUP BY applicationid, enterpriseid, enterprise_name, currency, payerid, payer_name, payer_type ");
					strBuilder.Append(strSqlSort);						
					break;
				
				case DataProviderType.Oracle:
					strBuilder.Append("select ");
					strBuilder.Append(" e.enterprise_name, e.currency, s.payerid, s.payer_name,s.payer_type, count(*) as no_of_consignment, ");
					strBuilder.Append("sum(s.chargeable_wt) as chargeable_wt, sum(s.tot_dim_wt*6000) as tot_volume,");
					strBuilder.Append("Decode(s.mbg,'Y',0,'N',s.tot_freight_charge + s.tot_vas_surcharge + s.esa_surcharge + s.insurance_surcharge + st.surcharge)");
					strBuilder.Append("as revenue from shipment s,(select applicationid, enterpriseid, consignment_no, sum(surcharge) surcharge from shipment_tracking  ");
					strBuilder.Append("where applicationid = '");
					strBuilder.Append(strAppID);
					strBuilder.Append("' and enterpriseid = '");
					strBuilder.Append(strEnterpriseID);
					strBuilder.Append("'  group by applicationid, enterpriseid, consignment_no) st, enterprise e ");
					strBuilder.Append("where st.applicationid = s.applicationid and st.enterpriseid = s.enterpriseid and ");
					strBuilder.Append("st.consignment_no = s.consignment_no and e.applicationid = s.applicationid and e.enterpriseid = s.enterpriseid and ");
					strBuilder.Append("s.applicationid = '");
					strBuilder.Append(strAppID);
					strBuilder.Append("' and s.enterpriseid = '");
					strBuilder.Append(strEnterpriseID);
					strBuilder.Append("' ");
					strBuilder.Append(strSqlWhere);
					strBuilder.Append(" AND s.consignment_no = sp.consignment_no AND s.applicationid = sp.applicationid AND s.enterpriseid = sp.enterpriseid ");
					strBuilder.Append(" and rownum < 3");
					strBuilder.Append("  group by s.applicationid, s.enterpriseid, e.enterprise_name, e.currency, s.payerid, s.payer_name ,s.payer_type, ");
					strBuilder.Append("s.mbg,s.tot_freight_charge , s.tot_vas_surcharge , s.esa_surcharge , s.insurance_surcharge , st.surcharge");
					strBuilder.Append(strSqlSort);						

					break;
				
				case DataProviderType.Oledb:
					break;
			}			
		
			strSqlQuery = strBuilder.ToString();

			dbcmd = dbCon.CreateCommand(strSqlQuery,CommandType.Text);
 
			try
			{
				dsTopN = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ReportDAL.cs","GetTopNPayer","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsTopN;
		}

		public static String GetSQICommandStr(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{	
			#region member
			String strstart_date=null;
			String strend_date=null;
			String strtran_date=null;
			String strpayer_type=null;
			String strpayer_code = null;
			String strorigin_dc = null;
			String strdestination_dc = null;
			String strroute_code = null;
			String strroute_type = null;
			String strdelPath_origin_dc = null;
			String strdelPath_destination_dc = null;
			String strzipcode = null;
			String strstate_code = null;
			String strservice_type = null;
			#endregion
			
			String strSqlQuery = null;
			StringBuilder strBuilder = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			strBuilder = new StringBuilder();

			//*************************** Booking Date : Part ***************************
			if (Utility.IsNotDBNull(dr["tran_date"]))
			{
				switch(dr["tran_date"].ToString().ToUpper())
				{
					case "E":
						strtran_date = " est_delivery_datetime";
						break;
					case "S":
						strtran_date = " act_pickup_datetime";
						break;					
					case "B":
						strtran_date = " booking_datetime";
						break;
				}
			}
			if (Utility.IsNotDBNull(dr["start_date"]) && Utility.IsNotDBNull(dr["end_date"]))
			{
				DateTime dateStartDate = (DateTime) dr["start_date"];
				DateTime dateEndDate = (DateTime) dr["end_date"];
				strstart_date = Utility.DateFormat(strAppID, strEnterpriseID, dateStartDate, DTFormat.DateTime);
				strend_date	= Utility.DateFormat(strAppID, strEnterpriseID, dateEndDate, DTFormat.DateTime);
			}			
			//*************************** Payer Type : Part ***************************
			String strpayer_type_tmp;
			strpayer_type_tmp = dr["payer_type"].ToString() + "";
			if (strpayer_type_tmp != "")
			{
				strpayer_type = " and payerid in ( select custid from customer where ";
				for (int i=0; i <= strpayer_type_tmp.Length - 1; i ++)
				{
					if (i==0)
					{
						strpayer_type += "payer_type = '" +strpayer_type_tmp.Substring(i,1).ToUpper()+ "'";
					}
					else 
					{
						strpayer_type += "or payer_type = '" +strpayer_type_tmp.Substring(i,1).ToUpper()+ "'";
					}
				}
				strpayer_type += " ) ";
			}

			strpayer_code = dr["payer_code"].ToString() + "";
			if (strpayer_code != "")
			{
				strpayer_code = "and payerid = '" +strpayer_code+ "'";
			}
			//*************************** Route / DC Selection : Part ***************************
			strroute_type = dr["route_type"].ToString() + "";
			strroute_code = dr["route_code"].ToString() + "";
			if (strroute_code != "")
			{
				
				if (strroute_type == "L" || strroute_type == "A")
				{
					strdelPath_origin_dc = "and origin_station = '" + dr["delPath_origin_dc"].ToString() + "'" ;
					strdelPath_destination_dc = "and destination_station = '" + dr["delPath_destination_dc"].ToString() + "'" ; 					
					strroute_code = "";
				}
				else
				{
					strroute_code = " and route_code = '" + strroute_code + "'";
				}
			}
			
			strorigin_dc = dr["origin_dc"].ToString() + "";
			if (strorigin_dc != "")
			{
				strorigin_dc = "and origin_station = '" +strorigin_dc+ "'";
			}

			strdestination_dc = dr["destination_dc"].ToString() + "";	
			if(strdestination_dc != "")
			{
				strdestination_dc = "and destination_station = '" +strdestination_dc+ "'";
			}
            
			//*************************** Destination ***************************
			strzipcode = dr["zip_code"].ToString() + "";
			if(strzipcode != "")
			{
				strzipcode = "and recipient_zipcode = '" +strzipcode+ "'";
			}
			strstate_code = dr["state_code"].ToString() + "";
			if(strstate_code != "")
			{
				strstate_code = "and destination_state_code = '" +strstate_code+ "'";
			}

			//*************************** Service Type ***************************
			strservice_type = dr["service_type"].ToString() + "";
			if(strservice_type != "")
			{
				strservice_type = "and service_code = '" +strservice_type+ "'";
			}
			string strMasterAcct = dr["master_account"].ToString();  //Jeab 18 Jul 2011

			/*
			//*********************************** Building Command String *****************************
			strBuilder.Append("SELECT SHP.consignment_no, ");
			strBuilder.Append("case isnull(SHP_DEL.consignment_no, '') ");
			strBuilder.Append("when '' then 'NOT DEL' ");
			strBuilder.Append("else 'DEL' end as DEL_STATUS, ");
			strBuilder.Append("case ");
			strBuilder.Append("when SHP_DEL.tracking_datetime is null then 'NOT DEL' ");
			strBuilder.Append("else (case ");
			strBuilder.Append("when SHP_DEL.tracking_datetime > SHP.est_delivery_datetime then ");
			strBuilder.Append("(case ");
			strBuilder.Append("when SHP_AUDIT.consignment_no is not null then 'LATE AUDIT' ");
			strBuilder.Append("else 'LATE NOT AUDIT' end) ");
			strBuilder.Append("else 'NOT LATE' end) ");
			strBuilder.Append("end as AUDIT_STATUS, ");
			strBuilder.Append(" ISNULL(SHP_AUDIT.exception_code, 'NOT EXCEPT') as exception_code, PODEX_CODE.exception_type, PODEX_CODE.exception_description ");
			strBuilder.Append("from ");
			//Jeab 18 Jul 2011
			if (strMasterAcct != "")
			{
				strBuilder.Append("(SELECT s.applicationid, s.enterpriseid, s.consignment_no, s.booking_no, s.est_delivery_datetime ");
				strBuilder.Append("FROM Shipment   s  Left Outer Join Customer c ON   ");
				strBuilder.Append(" s.applicationid = c.applicationid And  s.enterpriseid = c.enterpriseid And s.payerid = c.custid  ");
				strBuilder.Append("WHERE s.applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append("AND s.enterpriseid = '");
				strBuilder.Append(strEnterpriseID);  
				strBuilder.Append("'");
				strBuilder.Append("AND c.master_account = '");
				strBuilder.Append(strMasterAcct);  
				strBuilder.Append("'");
				strBuilder.Append("AND s." + strtran_date + " between " + strstart_date + " and " + strend_date );			
			}
			else
			{
				strBuilder.Append("(SELECT applicationid, enterpriseid, consignment_no, booking_no, est_delivery_datetime ");
				strBuilder.Append("FROM Shipment   ");				
				strBuilder.Append("WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append("AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);  
				strBuilder.Append("'");				
				strBuilder.Append("AND " + strtran_date + " between " + strstart_date + " and " + strend_date );
			}

			//Critiria
			strBuilder.Append(strdelPath_origin_dc +" ");
			strBuilder.Append(strdelPath_destination_dc +" ");
			strBuilder.Append(strroute_code +" ");
			strBuilder.Append(strorigin_dc +" ");
			strBuilder.Append(strdestination_dc +" ");
			strBuilder.Append(strzipcode +" ");
			strBuilder.Append(strstate_code +" ");
			strBuilder.Append(strpayer_code +" ");
			strBuilder.Append(strpayer_type +" ");
			strBuilder.Append(strservice_type +" ");


			strBuilder.Append(")SHP ");

			//Jeab 18 Jul 2011  =========> End

			strBuilder.Append("LEFT OUTER JOIN ");
			strBuilder.Append("(SELECT 	SUB_SHP_DEL2.applicationid, SUB_SHP_DEL2.enterpriseid, SUB_SHP_DEL2.consignment_no, SUB_SHP_DEL2.booking_no, SUB_SHP_DEL2.tracking_datetime ");
			strBuilder.Append("FROM ");
			//Jeab 18 Jul 2011 
			if (strMasterAcct !="")
			{
				strBuilder.Append("(select s.applicationid, s.enterpriseid, s.consignment_no, s.booking_no  ");
				strBuilder.Append("FROM Shipment  s  Left  Outer  Join  Customer c ON ");
				strBuilder.Append("s.applicationid = c.applicationid And  s.enterpriseid = c.enterpriseid And s.payerid = c.custid  ");
				strBuilder.Append("WHERE s.applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append("AND s.enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append("AND c.master_account = '");
				strBuilder.Append(strMasterAcct);  
				strBuilder.Append("'");
				strBuilder.Append("AND s." + strtran_date + " between " + strstart_date + " and " + strend_date );
			}
			else
			{
				strBuilder.Append("(select applicationid, enterpriseid, consignment_no, booking_no  ");
				strBuilder.Append("FROM Shipment  ");
				strBuilder.Append("WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append("AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append("AND " + strtran_date + " between " + strstart_date + " and " + strend_date );
			}
			//Critiria
			strBuilder.Append(strdelPath_origin_dc +" ");
			strBuilder.Append(strdelPath_destination_dc +" ");
			strBuilder.Append(strroute_code +" ");
			strBuilder.Append(strorigin_dc +" ");
			strBuilder.Append(strdestination_dc +" ");
			strBuilder.Append(strzipcode +" ");
			strBuilder.Append(strstate_code +" ");
			strBuilder.Append(strpayer_code +" ");
			strBuilder.Append(strpayer_type +" ");
			strBuilder.Append(strservice_type +" ");
			strBuilder.Append(")SUB_SHP_DEL1 ");

			//Jeab 18 Jul 2011  =========> End

			strBuilder.Append("	INNER JOIN");
			strBuilder.Append("	(SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append("	FROM shipment_tracking ");
			strBuilder.Append("WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append("AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");			
			strBuilder.Append("	AND (status_code = 'POD' AND isnull(deleted, 'N') <> 'Y') ");	
			strBuilder.Append("	GROUP BY applicationid, enterpriseid, consignment_no, booking_no");
			strBuilder.Append("	)SUB_SHP_DEL2 ");
			strBuilder.Append("	ON 	(SUB_SHP_DEL1.applicationid = SUB_SHP_DEL2.applicationid ");
			strBuilder.Append("	AND SUB_SHP_DEL1.enterpriseid = SUB_SHP_DEL2.enterpriseid ");
			strBuilder.Append("	AND 	SUB_SHP_DEL1.consignment_no = SUB_SHP_DEL2.consignment_no ");
			strBuilder.Append("	AND 	SUB_SHP_DEL1.booking_no = SUB_SHP_DEL2.booking_no)");
			strBuilder.Append(" ) SHP_DEL ");			
			strBuilder.Append("ON 	(SHP.applicationid = SHP_DEL.applicationid ");
			strBuilder.Append("AND 	SHP.enterpriseid = SHP_DEL.enterpriseid "); 
			strBuilder.Append("AND 	SHP.consignment_no = SHP_DEL.consignment_no "); 
			strBuilder.Append("AND 	SHP.booking_no = SHP_DEL.booking_no) "); 
			strBuilder.Append("LEFT OUTER JOIN ");
			strBuilder.Append("(SELECT  SUB_SHP_AUDIT2.*");
			strBuilder.Append(" FROM ");
			strBuilder.Append("(SELECT 	SUB_SHP_AUDIT1_2.applicationid, SUB_SHP_AUDIT1_2.enterpriseid, SUB_SHP_AUDIT1_2.consignment_no, SUB_SHP_AUDIT1_2.booking_no, SUB_SHP_AUDIT1_2.tracking_datetime");
			strBuilder.Append(" FROM ");
			//Jeab 18 Jul 2011  
			if (strMasterAcct != "")
			{
				strBuilder.Append("(SELECT s.applicationid, s.enterpriseid, s.consignment_no, s.booking_no ");
				strBuilder.Append(" FROM Shipment  s  Left  Outer  Join  Customer  c  ON ");
				strBuilder.Append(" s.applicationid = c.applicationid  And s.enterpriseid  =  c.enterpriseid  And  s.payerid = c.custid ");
				strBuilder.Append("WHERE s.applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append("AND s.enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append("AND  c.master_account = '");
				strBuilder.Append(strMasterAcct);
				strBuilder.Append("'");
				strBuilder.Append("AND s." + strtran_date + " between " + strstart_date + " and " + strend_date );
			}
			else
			{
				strBuilder.Append("(SELECT applicationid, enterpriseid, consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment  ");
				strBuilder.Append("WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append("AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append("AND " + strtran_date + " between " + strstart_date + " and " + strend_date );				
			}

			//Critiria
			strBuilder.Append(strdelPath_origin_dc +" ");
			strBuilder.Append(strdelPath_destination_dc +" ");
			strBuilder.Append(strroute_code +" ");
			strBuilder.Append(strorigin_dc +" ");
			strBuilder.Append(strdestination_dc +" ");
			strBuilder.Append(strzipcode +" ");
			strBuilder.Append(strstate_code +" ");
			strBuilder.Append(strpayer_code +" ");
			strBuilder.Append(strpayer_type +" ");
			strBuilder.Append(strservice_type +" ");
			strBuilder.Append(") SUB_SHP_AUDIT1_1");
			//Jeab 18 Jul 2011  =========> End

			strBuilder.Append(" INNER JOIN ");
			strBuilder.Append("(SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append(" FROM shipment_tracking ");
			strBuilder.Append("WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append("AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");			
			strBuilder.Append("	AND (status_code = 'PODEX' AND isnull(deleted, 'N') <> 'Y')");	
			strBuilder.Append("	GROUP BY applicationid, enterpriseid, consignment_no, booking_no");
			strBuilder.Append("	)SUB_SHP_AUDIT1_2 ");
			strBuilder.Append("ON 	(SUB_SHP_AUDIT1_1.applicationid = SUB_SHP_AUDIT1_2.applicationid ");
			strBuilder.Append("AND 	SUB_SHP_AUDIT1_1.enterpriseid = SUB_SHP_AUDIT1_2.enterpriseid "); 
			strBuilder.Append("AND 	SUB_SHP_AUDIT1_1.consignment_no = SUB_SHP_AUDIT1_2.consignment_no "); 
			strBuilder.Append("AND 	SUB_SHP_AUDIT1_1.booking_no = SUB_SHP_AUDIT1_2.booking_no) ");
			strBuilder.Append(") SUB_SHP_AUDIT1 ");	
			strBuilder.Append("LEFT OUTER JOIN ");
			strBuilder.Append("(SELECT * ");
			strBuilder.Append("FROM	shipment_tracking ");
			strBuilder.Append("WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append("AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");						
			strBuilder.Append(")SUB_SHP_AUDIT2 ");
			strBuilder.Append("ON (SUB_SHP_AUDIT1.applicationid = SUB_SHP_AUDIT2.applicationid ");
			strBuilder.Append("AND SUB_SHP_AUDIT1.enterpriseid = SUB_SHP_AUDIT2.enterpriseid "); 
			strBuilder.Append("AND SUB_SHP_AUDIT1.consignment_no = SUB_SHP_AUDIT2.consignment_no "); 
			strBuilder.Append("AND SUB_SHP_AUDIT1.booking_no = SUB_SHP_AUDIT2.booking_no ");
			strBuilder.Append("AND	SUB_SHP_AUDIT1.tracking_datetime = SUB_SHP_AUDIT2.tracking_datetime) ");
			strBuilder.Append(") SHP_AUDIT ");
			strBuilder.Append("ON	(SHP.applicationid = SHP_AUDIT.applicationid "); 
			strBuilder.Append("AND 	SHP.enterpriseid = SHP_AUDIT.enterpriseid ");
			strBuilder.Append("AND 	SHP.consignment_no = SHP_AUDIT.consignment_no "); 
			strBuilder.Append("AND 	SHP.booking_no = SHP_AUDIT.booking_no) ");
			strBuilder.Append("LEFT OUTER JOIN ");
			strBuilder.Append("(SELECT * "); 
			strBuilder.Append("FROM 	EXCEPTION_CODE "); 
			strBuilder.Append("WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append("AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");					
			strBuilder.Append("and 	status_code = 'PODEX' ");
			strBuilder.Append(") PODEX_CODE  ");
			strBuilder.Append("ON 	(SHP_AUDIT.applicationid = PODEX_CODE.applicationid ");
			strBuilder.Append("AND 	SHP_AUDIT.enterpriseid = PODEX_CODE.enterpriseid "); 
			strBuilder.Append("AND 	SHP_AUDIT.exception_code = PODEX_CODE.exception_code) "); 
			*/
			
			strBuilder.Append(" SELECT COUNT(consignment_no) AS consignment_no ");
			strBuilder.Append(",DEL_STATUS, AUDIT_STATUS, exception_code, exception_type, exception_description ");
			strBuilder.Append("FROM ( ");
			strBuilder.Append("SELECT s.consignment_no ");
			strBuilder.Append(",CASE WHEN pod.consignment_no IS NULL THEN 'NOT DEL' ELSE 'DEL' END as DEL_STATUS ");
			strBuilder.Append(",CASE WHEN pod.tracking_datetime IS NULL THEN 'NOT DEL' ");
			strBuilder.Append("ELSE ( ");
			strBuilder.Append("CASE WHEN pod.tracking_datetime > s.est_delivery_datetime ");
			strBuilder.Append("THEN ( ");
			strBuilder.Append("CASE WHEN px.consignment_no IS NOT NULL THEN 'LATE AUDIT' ELSE 'LATE NOT AUDIT' END) ");
			strBuilder.Append("ELSE 'NOT LATE' END) END as AUDIT_STATUS ");
			strBuilder.Append(", ISNULL(px.exception_code, 'NOT EXCEPT') as exception_code ");
			strBuilder.Append(",ec.exception_type, ec.exception_description, s.payerid ");
			strBuilder.Append("FROM ( ");
			strBuilder.Append("SELECT applicationid, enterpriseid, consignment_no, booking_no, est_delivery_datetime,payerid ");
			strBuilder.Append("FROM dbo.Shipment WITH(NOLOCK) ");
			strBuilder.Append("WHERE applicationid = '"+strAppID+"' AND enterpriseid = '"+strEnterpriseID+"' ");
			//strBuilder.Append("AND act_delivery_date between " + strstart_date + " and " + strend_date +" ");
			
			strBuilder.Append("AND" + strtran_date + " between " + strstart_date + " and " + strend_date);

			// Criteria
			strBuilder.Append(strdelPath_origin_dc +" ");
			strBuilder.Append(strdelPath_destination_dc +" ");
			strBuilder.Append(strroute_code +" ");
			strBuilder.Append(strorigin_dc +" ");
			strBuilder.Append(strdestination_dc +" ");
			strBuilder.Append(strzipcode +" ");
			strBuilder.Append(strstate_code +" ");
			strBuilder.Append(strpayer_code +" ");
			strBuilder.Append(strpayer_type +" ");
			strBuilder.Append(strservice_type +" ");
			
			strBuilder.Append(") s ");
			
			if (strMasterAcct != "")
			{
				// When selecting by customer ID or master account ID
				strBuilder.Append(" INNER JOIN Customer c ");
				strBuilder.Append("ON s.applicationid = c.applicationid And  s.enterpriseid = c.enterpriseid And c.custid = s.payerid "); 
				// When user selected master account
				strBuilder.Append("AND c.master_account = '" + strMasterAcct +"' ");
			}
			/*
			else
			{
				// When user selected customer account
				strBuilder.Append("WHERE c.custid = '" + strCustAcct + "' ");
			}
			*/               

			strBuilder.Append("LEFT OUTER JOIN ( ");
			strBuilder.Append("SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append("FROM dbo.shipment_tracking WITH(NOLOCK) ");
			strBuilder.Append("WHERE applicationid = '"+strAppID+"' AND enterpriseid = '"+strEnterpriseID+"' AND (status_code = 'POD' AND isnull(deleted, 'N') <> 'Y') ");
			strBuilder.Append(" GROUP BY applicationid, enterpriseid, consignment_no, booking_no) pod ");
			strBuilder.Append("ON s.applicationid = pod.applicationid and s.enterpriseid = pod.enterpriseid and ");
			strBuilder.Append("s.booking_no = pod.booking_no and s.consignment_no = pod.consignment_no ");
			
			strBuilder.Append("LEFT OUTER JOIN ( ");
			strBuilder.Append("SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime, MAX(exception_code) AS exception_code "); 
			strBuilder.Append("FROM dbo.shipment_tracking WITH(NOLOCK) ");
			strBuilder.Append("WHERE applicationid = '"+strAppID+"' AND enterpriseid = '"+strEnterpriseID+"' AND (status_code = 'PODEX' AND isnull(deleted, 'N') <> 'Y') ");
			strBuilder.Append(" GROUP BY applicationid, enterpriseid, consignment_no, booking_no) px ");
			strBuilder.Append("ON s.applicationid = px.applicationid and s.enterpriseid = px.enterpriseid and s.booking_no = px.booking_no and s.consignment_no = px.consignment_no ");
			
			strBuilder.Append("LEFT OUTER JOIN dbo.Exception_code ec WITH(NOLOCK) ");
			strBuilder.Append("ON s.applicationid = ec.applicationid and s.enterpriseid = px.enterpriseid and ec.status_code = 'PODEX' and px.exception_code = ec.exception_code) x ");
			strBuilder.Append("GROUP BY DEL_STATUS, AUDIT_STATUS, EXCEPTION_CODE, exception_type, exception_description ");

			strSqlQuery = strBuilder.ToString();
		
			return strSqlQuery;
		}


		public static String GetSQICmdStrByPODDate(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{	
			#region member
			String strstart_date=null;
			String strend_date=null;
			String strtran_date=null;
			String strpayer_type=null;
			String strpayer_code = null;
			String strorigin_dc = null;
			String strdestination_dc = null;
			String strroute_code = null;
			String strroute_type = null;
			String strdelPath_origin_dc = null;
			String strdelPath_destination_dc = null;
			String strzipcode = null;
			String strstate_code = null;
			String strservice_type = null;
			#endregion
			
			String strSqlQuery = null;
			StringBuilder strBuilder = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			strBuilder = new StringBuilder();

			//*************************** Booking Date : Part ***************************
			if (Utility.IsNotDBNull(dr["tran_date"]))
			{
				strtran_date = " tracking_datetime ";
			}
			if (Utility.IsNotDBNull(dr["start_date"]) && Utility.IsNotDBNull(dr["end_date"]))
			{
				DateTime dateStartDate = (DateTime) dr["start_date"];
				DateTime dateEndDate = (DateTime) dr["end_date"];
				strstart_date = Utility.DateFormat(strAppID, strEnterpriseID, dateStartDate, DTFormat.DateTime);
				strend_date	= Utility.DateFormat(strAppID, strEnterpriseID, dateEndDate, DTFormat.DateTime);
			}			
			//*************************** Payer Type : Part ***************************
			String strpayer_type_tmp;
			strpayer_type_tmp = dr["payer_type"].ToString() + "";
			if (strpayer_type_tmp != "")
			{
				strpayer_type = " and payerid in ( select custid from customer where ";
				for (int i=0; i <= strpayer_type_tmp.Length - 1; i ++)
				{
					if (i==0)
					{
						strpayer_type += "payer_type = '" +strpayer_type_tmp.Substring(i,1).ToUpper()+ "'";
					}
					else 
					{
						strpayer_type += "or payer_type = '" +strpayer_type_tmp.Substring(i,1).ToUpper()+ "'";
					}
				}
				strpayer_type += " ) ";
			}

			strpayer_code = dr["payer_code"].ToString() + "";
			if (strpayer_code != "")
			{
				strpayer_code = "and payerid = '" +strpayer_code+ "'";
			}
			//*************************** Route / DC Selection : Part ***************************
			strroute_type = dr["route_type"].ToString() + "";
			strroute_code = dr["route_code"].ToString() + "";
			if (strroute_code != "")
			{
				
				if (strroute_type == "L" || strroute_type == "A")
				{
					strdelPath_origin_dc = "and origin_station = '" + dr["delPath_origin_dc"].ToString() + "'" ;
					strdelPath_destination_dc = "and destination_station = '" + dr["delPath_destination_dc"].ToString() + "'" ; 					
				}
				else
				{
					strroute_code = " and route_code = '" + strroute_code + "'";
				}
			}
			
			strorigin_dc = dr["origin_dc"].ToString() + "";
			if (strorigin_dc != "")
			{
				strorigin_dc = "and origin_station = '" +strorigin_dc+ "'";
			}

			strdestination_dc = dr["destination_dc"].ToString() + "";	
			if(strdestination_dc != "")
			{
				strdestination_dc = "and destination_station = '" +strdestination_dc+ "'";
			}
            
			//*************************** Destination ***************************
			strzipcode = dr["zip_code"].ToString() + "";
			if(strzipcode != "")
			{
				strzipcode = "and recipient_zipcode = '" +strzipcode+ "'";
			}
			strstate_code = dr["state_code"].ToString() + "";
			if(strstate_code != "")
			{
				strstate_code = "and destination_state_code = '" +strstate_code+ "'";
			}

			//*************************** Service Type ***************************
			strservice_type = dr["service_type"].ToString() + "";
			if(strservice_type != "")
			{
				strservice_type = "and service_code= '" +strservice_type+ "'";
			}
			string strMasterAcct = dr["master_account"].ToString();  //Jeab 18 Jul 2011
			
			/*
			//*********************************** Building Command String *****************************
			strBuilder.Append(" SELECT SHP.consignment_no, ");
			strBuilder.Append(" case isnull(SHP_DEL.consignment_no, '') ");
			strBuilder.Append(" when '' then 'NOT DEL' ");
			strBuilder.Append(" else 'DEL' end as DEL_STATUS, ");
			strBuilder.Append(" case ");
			strBuilder.Append(" when SHP_DEL.tracking_datetime is null then 'NOT DEL' ");
			strBuilder.Append(" else (case ");
			strBuilder.Append(" when SHP_DEL.tracking_datetime > SHP.est_delivery_datetime then ");
			strBuilder.Append(" (case ");
			strBuilder.Append(" when SHP_AUDIT.consignment_no is not null then 'LATE AUDIT' ");
			strBuilder.Append(" else 'LATE NOT AUDIT' end) ");
			strBuilder.Append(" else 'NOT LATE' end) ");
			strBuilder.Append(" end as AUDIT_STATUS, ISNULL(SHP_AUDIT.exception_code, 'NOT EXCEPT') as exception_code,"); 
			strBuilder.Append(" PODEX_CODE.exception_type, PODEX_CODE.exception_description ");
			strBuilder.Append(" from ");

			strBuilder.Append("(SELECT	SUB_SHP2.applicationid, ");
			strBuilder.Append(" SUB_SHP2.enterpriseid, SUB_SHP2.consignment_no, ");
			strBuilder.Append(" SUB_SHP2.booking_no, SUB_SHP2.est_delivery_datetime ");
			strBuilder.Append(" FROM(SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append(" FROM    shipment_tracking 	WHERE  applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			strBuilder.Append("AND (status_code = 'POD' AND isnull(deleted, 'N') <> 'Y') ");
			strBuilder.Append("AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
			strBuilder.Append("GROUP BY applicationid, enterpriseid, consignment_no, booking_no	");
			strBuilder.Append(") SUB_SHP1 ");
			strBuilder.Append(" INNER JOIN ");
			//Jeab 18 Jul 2011
			if (strMasterAcct != "")
			{
				strBuilder.Append("(SELECT s.applicationid, s.enterpriseid, s.consignment_no, s.booking_no, s.est_delivery_datetime ");
				strBuilder.Append(" FROM Shipment  s  Left  Outer  Join  Customer c  ON ");
				strBuilder.Append(" s.applicationid = c.applicationid  And  s.enterpriseid  =  c.enterpriseid  And  s.payerid = c.custid  ");
				strBuilder.Append(" WHERE s.applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append("AND s.enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append("AND c.master_account = '");
				strBuilder.Append(strMasterAcct);
				strBuilder.Append("'");				
			}
			else
			{
				strBuilder.Append("(SELECT applicationid, enterpriseid, consignment_no, booking_no, est_delivery_datetime ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append("AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");				
			}

			//Critiria
			strBuilder.Append(strdelPath_origin_dc +" ");
			strBuilder.Append(strdelPath_destination_dc +" ");
			strBuilder.Append(strroute_code +" ");
			strBuilder.Append(strorigin_dc +" ");
			strBuilder.Append(strdestination_dc +" ");
			strBuilder.Append(strzipcode +" ");
			strBuilder.Append(strstate_code +" ");
			strBuilder.Append(strpayer_code +" ");
			strBuilder.Append(strpayer_type +" ");
			strBuilder.Append(strservice_type +" ");
			strBuilder.Append(") SUB_SHP2 ");
			strBuilder.Append(" ON	(SUB_SHP1.applicationid = SUB_SHP2.applicationid ");
			strBuilder.Append(" AND SUB_SHP1.enterpriseid = SUB_SHP2.enterpriseid ");
			strBuilder.Append(" AND SUB_SHP1.consignment_no = SUB_SHP2.consignment_no ");
			strBuilder.Append("	AND SUB_SHP1.booking_no = SUB_SHP2.booking_no)");
			strBuilder.Append(")SHP ");

			//Jeab 18 Jul 2011  =========> End
			strBuilder.Append("LEFT OUTER JOIN ");
			strBuilder.Append("(SELECT	applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append(" FROM	shipment_tracking ");
			strBuilder.Append(" WHERE 	applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			strBuilder.Append(" AND (status_code = 'POD' AND isnull(deleted, 'N') <> 'Y') ");
			strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
			strBuilder.Append(" GROUP BY applicationid, enterpriseid, consignment_no, booking_no	");
			strBuilder.Append(") SHP_DEL");
			strBuilder.Append(" ON	(SHP.applicationid = SHP_DEL.applicationid ");
			strBuilder.Append(" AND SHP.enterpriseid = SHP_DEL.enterpriseid ");
			strBuilder.Append("	AND SHP.consignment_no = SHP_DEL.consignment_no ");
			strBuilder.Append(" AND SHP.booking_no = SHP_DEL.booking_no)");
			strBuilder.Append(" LEFT OUTER JOIN");
			strBuilder.Append(" (SELECT  SUB_SHP_AUDIT2.*");
			strBuilder.Append(" FROM (SELECT 	SUB_SHP_AUDIT1_2.applicationid, SUB_SHP_AUDIT1_2.enterpriseid, ");
			strBuilder.Append(" SUB_SHP_AUDIT1_2.consignment_no, SUB_SHP_AUDIT1_2.booking_no, SUB_SHP_AUDIT1_2.tracking_datetime");
			strBuilder.Append(" FROM (SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append(" FROM    	shipment_tracking");
			strBuilder.Append(" WHERE 	applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			strBuilder.Append(" AND (status_code = 'POD' AND isnull(deleted, 'N') <> 'Y') ");
			strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
			strBuilder.Append(" GROUP BY applicationid, enterpriseid, consignment_no, booking_no	");
			strBuilder.Append(" ) SUB_SHP_AUDIT1_1");
			strBuilder.Append(" INNER JOIN ");
			strBuilder.Append(" (SELECT 		applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append(" FROM 		shipment_tracking ");
			strBuilder.Append(" WHERE 	applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			strBuilder.Append(" AND (status_code = 'PODEX' AND isnull(deleted, 'N') <> 'Y') ");	
			strBuilder.Append(" GROUP BY applicationid, enterpriseid, consignment_no, booking_no	");
			strBuilder.Append(" ) SUB_SHP_AUDIT1_2 ");
			strBuilder.Append(" ON 	(SUB_SHP_AUDIT1_1.applicationid = SUB_SHP_AUDIT1_2.applicationid ");
			strBuilder.Append(" AND 	SUB_SHP_AUDIT1_1.enterpriseid = SUB_SHP_AUDIT1_2.enterpriseid ");
			strBuilder.Append(" AND 	SUB_SHP_AUDIT1_1.consignment_no = SUB_SHP_AUDIT1_2.consignment_no ");
			strBuilder.Append(" AND 	SUB_SHP_AUDIT1_1.booking_no = SUB_SHP_AUDIT1_2.booking_no) ");
			strBuilder.Append(" ) SUB_SHP_AUDIT1 ");
			strBuilder.Append(" LEFT OUTER JOIN ");
			strBuilder.Append(" (SELECT * ");
			strBuilder.Append(" FROM	shipment_tracking ");
			strBuilder.Append(" WHERE 	applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			strBuilder.Append(" )SUB_SHP_AUDIT2 ");
			strBuilder.Append(" ON 	(SUB_SHP_AUDIT1.applicationid = SUB_SHP_AUDIT2.applicationid ");
			strBuilder.Append(" AND 	SUB_SHP_AUDIT1.enterpriseid = SUB_SHP_AUDIT2.enterpriseid "); 
			strBuilder.Append(" AND 	SUB_SHP_AUDIT1.consignment_no = SUB_SHP_AUDIT2.consignment_no "); 
			strBuilder.Append(" AND 	SUB_SHP_AUDIT1.booking_no = SUB_SHP_AUDIT2.booking_no ");
			strBuilder.Append(" AND	SUB_SHP_AUDIT1.tracking_datetime = SUB_SHP_AUDIT2.tracking_datetime) ");
			strBuilder.Append(" ) SHP_AUDIT	");
			strBuilder.Append(" ON	(SHP.applicationid = SHP_AUDIT.applicationid ");  
			strBuilder.Append(" AND 	SHP.enterpriseid = SHP_AUDIT.enterpriseid ");  
			strBuilder.Append(" AND 	SHP.consignment_no = SHP_AUDIT.consignment_no ");  
			strBuilder.Append(" AND 	SHP.booking_no = SHP_AUDIT.booking_no) "); 
			strBuilder.Append(" LEFT OUTER JOIN ");	
			strBuilder.Append(" (SELECT * 	FROM 	EXCEPTION_CODE ");
			strBuilder.Append(" WHERE 	applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			strBuilder.Append(" ) PODEX_CODE ");
			strBuilder.Append(" ON	(SHP_AUDIT.applicationid = PODEX_CODE.applicationid ");	 
			strBuilder.Append(" AND 	SHP_AUDIT.enterpriseid = PODEX_CODE.enterpriseid ");  
			strBuilder.Append(" AND 	SHP_AUDIT.exception_code = PODEX_CODE.exception_code) "); 
			*/

			strBuilder.Append(" SELECT COUNT(consignment_no) AS consignment_no ");
			strBuilder.Append(",DEL_STATUS, AUDIT_STATUS, exception_code, exception_type, exception_description ");
			strBuilder.Append("FROM ( ");
			strBuilder.Append("SELECT s.consignment_no ");
			strBuilder.Append(",CASE WHEN pod.consignment_no IS NULL THEN 'NOT DEL' ELSE 'DEL' END as DEL_STATUS ");
			strBuilder.Append(",CASE WHEN pod.tracking_datetime IS NULL THEN 'NOT DEL' ");
			strBuilder.Append("ELSE ( ");
			strBuilder.Append("CASE WHEN pod.tracking_datetime > s.est_delivery_datetime ");
			strBuilder.Append("THEN ( ");
			strBuilder.Append("CASE WHEN px.consignment_no IS NOT NULL THEN 'LATE AUDIT' ELSE 'LATE NOT AUDIT' END) ");
			strBuilder.Append("ELSE 'NOT LATE' END) END as AUDIT_STATUS ");
			strBuilder.Append(", ISNULL(px.exception_code, 'NOT EXCEPT') as exception_code ");
			strBuilder.Append(",ec.exception_type, ec.exception_description, s.payerid ");
			strBuilder.Append("FROM ( ");
			strBuilder.Append("SELECT applicationid, enterpriseid, consignment_no, booking_no, est_delivery_datetime, payerid ");
			strBuilder.Append("FROM dbo.Shipment WITH(NOLOCK) ");
			strBuilder.Append("WHERE applicationid = '"+strAppID+"' AND enterpriseid = '"+strEnterpriseID+"' ");
			//strBuilder.Append("AND act_delivery_date between " + strstart_date + " and " + strend_date +" ");
			strBuilder.Append("AND act_delivery_date between " + strstart_date + " and " + strend_date +" ");
			
			// Criteria
			strBuilder.Append(strdelPath_origin_dc +" ");
			strBuilder.Append(strdelPath_destination_dc +" ");
			strBuilder.Append(strroute_code +" ");
			strBuilder.Append(strorigin_dc +" ");
			strBuilder.Append(strdestination_dc +" ");
			strBuilder.Append(strzipcode +" ");
			strBuilder.Append(strstate_code +" ");
			strBuilder.Append(strpayer_code +" ");
			strBuilder.Append(strpayer_type +" ");
			strBuilder.Append(strservice_type +" ");
			
			strBuilder.Append(") s ");
			
			if (strMasterAcct != "")
			{
				// When selecting by customer ID or master account ID
				strBuilder.Append(" INNER JOIN Customer c ");
				strBuilder.Append("ON s.applicationid = c.applicationid And  s.enterpriseid = c.enterpriseid And c.custid = s.payerid "); 
				// When user selected master account
				strBuilder.Append("AND c.master_account = '" + strMasterAcct +"' ");
			}
			/*
			else
			{
				// When user selected customer account
				strBuilder.Append("WHERE c.custid = '" + strCustAcct + "' ");
			}
			*/               

			//strBuilder.Append("LEFT OUTER JOIN ( ");
			strBuilder.Append("INNER JOIN ( ");
			strBuilder.Append("SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append("FROM dbo.shipment_tracking WITH(NOLOCK) ");
			strBuilder.Append("WHERE applicationid = '"+strAppID+"' AND enterpriseid = '"+strEnterpriseID+"' AND (status_code = 'POD' AND isnull(deleted, 'N') <> 'Y') ");
			// Actual POD Date criteria is selected
			strBuilder.Append("AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
			strBuilder.Append(" GROUP BY applicationid, enterpriseid, consignment_no, booking_no) pod ");
			strBuilder.Append("ON s.applicationid = pod.applicationid and s.enterpriseid = pod.enterpriseid and ");
			strBuilder.Append("s.booking_no = pod.booking_no and s.consignment_no = pod.consignment_no ");
			
			strBuilder.Append("LEFT OUTER JOIN ( ");
			strBuilder.Append("SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime, MAX(exception_code) AS exception_code "); 
            strBuilder.Append("FROM dbo.shipment_tracking WITH(NOLOCK) ");
			strBuilder.Append("WHERE applicationid = '"+strAppID+"' AND enterpriseid = '"+strEnterpriseID+"' AND (status_code = 'PODEX' AND isnull(deleted, 'N') <> 'Y') ");
			// Actual POD Date criteria is selected
			//strBuilder.Append("AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
			strBuilder.Append(" GROUP BY applicationid, enterpriseid, consignment_no, booking_no) px ");
			strBuilder.Append("ON s.applicationid = px.applicationid and s.enterpriseid = px.enterpriseid and s.booking_no = px.booking_no and s.consignment_no = px.consignment_no ");
			
			strBuilder.Append("LEFT OUTER JOIN dbo.Exception_code ec WITH(NOLOCK) ");
			strBuilder.Append("ON s.applicationid = ec.applicationid and s.enterpriseid = px.enterpriseid and ec.status_code = 'PODEX' and px.exception_code = ec.exception_code) x ");
			strBuilder.Append("GROUP BY DEL_STATUS, AUDIT_STATUS, EXCEPTION_CODE, exception_type, exception_description ");

			strSqlQuery = strBuilder.ToString();
		
			return strSqlQuery;
		}


		//Modifid By Tom
		public static String GetSQIVersusByMissingCode(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{	
			#region member
			String strstart_date=null;
			String strend_date=null;
			String strtran_date=null;
			String strorigin_dc = null;
			String strdestination_dc = null;
			String conditions = null;
			#endregion
			
			String strSqlQuery = null;
			StringBuilder strBuilder = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			strBuilder = new StringBuilder();

			//*************************** Booking Date : Part ***************************
			if (Utility.IsNotDBNull(dr["tran_date"]))
			{
				conditions = " LEFT OUTER JOIN ";
				if(dr["tran_date"].ToString().ToUpper() == "U")
					strtran_date = " last_status_datetime ";
				else if(dr["tran_date"].ToString().ToUpper() == "M")
				{
					strtran_date = " shpt_manifest_datetime ";
				}
			}
			if (Utility.IsNotDBNull(dr["start_date"]) && Utility.IsNotDBNull(dr["end_date"]))
			{
				DateTime dateStartDate = (DateTime) dr["start_date"];
				DateTime dateEndDate = (DateTime) dr["end_date"];
				strstart_date = Utility.DateFormat(strAppID, strEnterpriseID, dateStartDate, DTFormat.DateTime);
				strend_date	= Utility.DateFormat(strAppID, strEnterpriseID, dateEndDate, DTFormat.DateTime);
			}

			//*************************** Route / DC Selection : Part ***************************
			strorigin_dc = dr["origin_dc"].ToString() + "";
			if (strorigin_dc != "")
			{
				strorigin_dc = "and origin_station = '" +strorigin_dc+ "'";
			}

			strdestination_dc = dr["destination_dc"].ToString() + "";	
			if(strdestination_dc != "")
			{
				strdestination_dc = "and destination_station = '" +strdestination_dc+ "'";
			}
			
			//*************************** Status Code ***************************
			String strStatusCode1 = "'"+dr["state_code1"].ToString()+"'";
			String strStatusCode2 = "'"+dr["state_code2"].ToString()+"'";
			String strStatusCode3 = "'"+dr["state_code3"].ToString()+"'";
			//*********************************** Building Command String *****************************
			if (dr["versus_type"].ToString() == "M1")
			{
				strBuilder.Append(" SELECT	 SHP.consignment_no, SHP.ref_no, SHP.booking_no, SHP.payerid, SHP.route_code, ");
				strBuilder.Append("SHP.act_pickup_datetime, SHP.shpt_manifest_datetime, SHP.origin_station, SHP.destination_station, ");
				strBuilder.Append("STATUS_CODE1.status_code as sCode1, STATUS_CODE1.last_userid, STATUS_CODE1.tracking_datetime as sCode1DT, ");
				strBuilder.Append(strStatusCode2 + "  as sCode2, null as sCode2DT,'-'  as sCode3, null as sCode3DT , 0 as TotalTime, SHP.last_status_datetime as last_updated_date");
			
				strBuilder.Append(" FROM (SELECT	* FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
				}
			
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(") SHP ");

				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no, status_code, last_userid, ");
				strBuilder.Append(" MAX(tracking_datetime) AS tracking_datetime ");
				strBuilder.Append(" FROM shipment_tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = "+strStatusCode1+" AND isnull(deleted, 'N') <> 'Y') "); //Temp_Status_code1
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND last_updated between " + strstart_date + " and " + strend_date);
				}
				strBuilder.Append(" GROUP BY	applicationid, enterpriseid, consignment_no, booking_no, status_code, last_userid) STATUS_CODE1 ");
				strBuilder.Append(" ON (SHP.applicationid =	STATUS_CODE1.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid = STATUS_CODE1.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = STATUS_CODE1.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = STATUS_CODE1.booking_no) ");
			
				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(" AND	consignment_no not in (SELECT DISTINCT consignment_no ");
				strBuilder.Append(" FROM Shipment_Tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = " + strStatusCode2 + "AND isnull(deleted, 'N') <> 'Y'))) MISSING_SAT2 ");
				strBuilder.Append(" ON (SHP.applicationid	=	MISSING_SAT2.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid =	MISSING_SAT2.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = MISSING_SAT2.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = MISSING_SAT2.booking_no) ");
				strBuilder.Append(" ORDER BY SHP.last_updated_date, SHP.consignment_no ");
			} else if (dr["versus_type"].ToString() == "M2")
			{
				strBuilder.Append(" SELECT	 SHP.consignment_no, SHP.ref_no, SHP.booking_no, SHP.payerid, SHP.route_code, ");
				strBuilder.Append("SHP.act_pickup_datetime, SHP.shpt_manifest_datetime, SHP.origin_station, SHP.destination_station, ");
				strBuilder.Append(strStatusCode2 + " as sCode2, null as sCode2DT, ");
				strBuilder.Append(" '-'  as sCode1, '-' as last_userid, null as sCode1DT,'-'  as sCode3, null as sCode3DT , 0 as TotalTime,SHP.last_status_datetime as last_updated_date");
			
				strBuilder.Append(" FROM (SELECT	* FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
				}
			
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(") SHP ");

				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(" AND	consignment_no not in (SELECT DISTINCT consignment_no ");
				strBuilder.Append(" FROM Shipment_Tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = " + strStatusCode2 + "AND isnull(deleted, 'N') <> 'Y'))) MISSING_SAT2 ");
				strBuilder.Append(" ON (SHP.applicationid	=	MISSING_SAT2.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid =	MISSING_SAT2.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = MISSING_SAT2.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = MISSING_SAT2.booking_no) ");
//				strBuilder.Append(" ORDER BY SHP.last_updated_date, SHP.consignment_no ");
			} else if (dr["versus_type"].ToString() == "M3")
			{
				strBuilder.Append("SELECT AAA.* FROM (");
				strBuilder.Append(" SELECT	 SHP.consignment_no, SHP.ref_no, SHP.booking_no, SHP.payerid, SHP.route_code, ");
				strBuilder.Append("SHP.act_pickup_datetime, SHP.shpt_manifest_datetime, SHP.origin_station, SHP.destination_station, ");
				strBuilder.Append("STATUS_CODE1.status_code as sCode1, STATUS_CODE1.last_userid, STATUS_CODE1.tracking_datetime as sCode1DT, ");
				strBuilder.Append(strStatusCode2 + "  as sCode2, null as sCode2DT,'-'  as sCode3, null as sCode3DT , 0 as TotalTime,SHP.last_status_datetime as last_updated_date");
			
				strBuilder.Append(" FROM (SELECT	* FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
				}
			
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(") SHP ");

				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no, status_code, last_userid, ");
				strBuilder.Append(" MAX(tracking_datetime) AS tracking_datetime ");
				strBuilder.Append(" FROM shipment_tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = "+strStatusCode1+" AND isnull(deleted, 'N') <> 'Y') "); //Temp_Status_code1
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND last_updated between " + strstart_date + " and " + strend_date);
				}
				strBuilder.Append(" GROUP BY	applicationid, enterpriseid, consignment_no, booking_no, status_code, last_userid) STATUS_CODE1 ");
				strBuilder.Append(" ON (SHP.applicationid =	STATUS_CODE1.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid = STATUS_CODE1.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = STATUS_CODE1.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = STATUS_CODE1.booking_no) ");
			
				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(" AND	consignment_no not in (SELECT DISTINCT consignment_no ");
				strBuilder.Append(" FROM Shipment_Tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = " + strStatusCode2 + "AND isnull(deleted, 'N') <> 'Y'))) MISSING_SAT2 ");
				strBuilder.Append(" ON (SHP.applicationid	=	MISSING_SAT2.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid =	MISSING_SAT2.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = MISSING_SAT2.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = MISSING_SAT2.booking_no) ");
//				strBuilder.Append(" ORDER BY SHP.last_updated_date, SHP.consignment_no ");

				strBuilder.Append("UNION");

				strBuilder.Append(" SELECT	 SHP.consignment_no, SHP.ref_no, SHP.booking_no, SHP.payerid, SHP.route_code, ");
				strBuilder.Append("SHP.act_pickup_datetime, SHP.shpt_manifest_datetime, SHP.origin_station, SHP.destination_station, ");
				strBuilder.Append("STATUS_CODE1.status_code as sCode1, STATUS_CODE1.last_userid, STATUS_CODE1.tracking_datetime as sCode1DT, ");
				strBuilder.Append("'-' as sCode2, null as sCode2DT," + strStatusCode3 + " as sCode3, null as sCode3DT , 0 as TotalTime,SHP.last_status_datetime as last_updated_date");
			
				strBuilder.Append(" FROM (SELECT	* FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
				}
			
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(") SHP ");

				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no, status_code, last_userid, ");
				strBuilder.Append(" MAX(tracking_datetime) AS tracking_datetime ");
				strBuilder.Append(" FROM shipment_tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = "+strStatusCode1+" AND isnull(deleted, 'N') <> 'Y') "); //Temp_Status_code1
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND last_updated between " + strstart_date + " and " + strend_date);
				}
				strBuilder.Append(" GROUP BY	applicationid, enterpriseid, consignment_no, booking_no, status_code, last_userid) STATUS_CODE1 ");
				strBuilder.Append(" ON (SHP.applicationid =	STATUS_CODE1.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid = STATUS_CODE1.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = STATUS_CODE1.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = STATUS_CODE1.booking_no) ");
			
				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(" AND	consignment_no not in (SELECT DISTINCT consignment_no ");
				strBuilder.Append(" FROM Shipment_Tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = " + strStatusCode3 + "AND isnull(deleted, 'N') <> 'Y'))) MISSING_SAT2 ");
				strBuilder.Append(" ON (SHP.applicationid	=	MISSING_SAT2.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid =	MISSING_SAT2.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = MISSING_SAT2.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = MISSING_SAT2.booking_no)) AAA where AAA.consignment_no not in ");

				
				
				strBuilder.Append("(( SELECT	 SHP.consignment_no FROM (SELECT	* FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
				}
			
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(") SHP ");

				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no, status_code, ");
				strBuilder.Append(" MAX(tracking_datetime) AS tracking_datetime ");
				strBuilder.Append(" FROM shipment_tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = "+strStatusCode1+" AND isnull(deleted, 'N') <> 'Y') "); //Temp_Status_code1
				strBuilder.Append(" GROUP BY	applicationid, enterpriseid, consignment_no, booking_no, status_code) STATUS_CODE1 ");
				strBuilder.Append(" ON (SHP.applicationid =	STATUS_CODE1.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid = STATUS_CODE1.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = STATUS_CODE1.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = STATUS_CODE1.booking_no) ");
			
				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(" AND	consignment_no not in (SELECT DISTINCT consignment_no ");
				strBuilder.Append(" FROM Shipment_Tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = " + strStatusCode2 + "AND isnull(deleted, 'N') <> 'Y'))) MISSING_SAT1 ");
				strBuilder.Append(" ON (SHP.applicationid	=	MISSING_SAT1.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid =	MISSING_SAT1.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = MISSING_SAT1.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = MISSING_SAT1.booking_no) ");
			
				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(" AND	consignment_no not in (SELECT DISTINCT consignment_no ");
				strBuilder.Append(" FROM Shipment_Tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = " + strStatusCode3 + "AND isnull(deleted, 'N') <> 'Y'))) MISSING_SAT2 ");
				strBuilder.Append(" ON (SHP.applicationid	=	MISSING_SAT2.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid =	MISSING_SAT2.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = MISSING_SAT2.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = MISSING_SAT2.booking_no))) ");
//				strBuilder.Append(" ORDER BY SHP.last_updated_date, SHP.consignment_no ");

				strBuilder.Append(" UNION ");
			
				
				strBuilder.Append("( SELECT	 SHP.consignment_no, SHP.ref_no, SHP.booking_no, SHP.payerid, SHP.route_code, ");
				strBuilder.Append("SHP.act_pickup_datetime, SHP.shpt_manifest_datetime, SHP.origin_station, SHP.destination_station, ");
				strBuilder.Append("STATUS_CODE1.status_code as sCode1, STATUS_CODE1.last_userid, STATUS_CODE1.tracking_datetime as sCode1DT, ");
				strBuilder.Append(strStatusCode2 + "  as sCode2, null as sCode2DT," + strStatusCode3 + "  as sCode3, null as sCode3DT , 0 as TotalTime, SHP.last_status_datetime as last_updated_date");
			
				strBuilder.Append(" FROM (SELECT	* FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
				}
			
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(") SHP ");

				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no, status_code, last_userid, ");
				strBuilder.Append(" MAX(tracking_datetime) AS tracking_datetime ");
				strBuilder.Append(" FROM shipment_tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = "+strStatusCode1+" AND isnull(deleted, 'N') <> 'Y') "); //Temp_Status_code1
				if(dr["tran_date"].ToString().ToUpper() != "P")
				{
					strBuilder.Append(" AND last_updated between " + strstart_date + " and " + strend_date);
				}
				strBuilder.Append(" GROUP BY	applicationid, enterpriseid, consignment_no, booking_no, status_code, last_userid) STATUS_CODE1 ");
				strBuilder.Append(" ON (SHP.applicationid =	STATUS_CODE1.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid = STATUS_CODE1.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = STATUS_CODE1.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = STATUS_CODE1.booking_no) ");
				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(" AND	consignment_no not in (SELECT DISTINCT consignment_no ");
				strBuilder.Append(" FROM Shipment_Tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = " + strStatusCode2 + "AND isnull(deleted, 'N') <> 'Y'))) MISSING_SAT1 ");
				strBuilder.Append(" ON (SHP.applicationid	=	MISSING_SAT1.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid =	MISSING_SAT1.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = MISSING_SAT1.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = MISSING_SAT1.booking_no) ");
				strBuilder.Append(" INNER JOIN ");
				strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
				strBuilder.Append(" consignment_no, booking_no ");
				strBuilder.Append(" FROM Shipment ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(strorigin_dc +" ");
				strBuilder.Append(strdestination_dc +" ");
				strBuilder.Append(" AND	consignment_no not in (SELECT DISTINCT consignment_no ");
				strBuilder.Append(" FROM Shipment_Tracking ");
				strBuilder.Append(" WHERE applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("'");
				strBuilder.Append(" AND enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("'");
				strBuilder.Append(" AND (status_code = " + strStatusCode3 + "AND isnull(deleted, 'N') <> 'Y'))) MISSING_SAT2 ");
				strBuilder.Append(" ON (SHP.applicationid	=	MISSING_SAT2.applicationid ");
				strBuilder.Append(" AND	SHP.enterpriseid =	MISSING_SAT2.enterpriseid ");
				strBuilder.Append(" AND	SHP.consignment_no = MISSING_SAT2.consignment_no ");
				strBuilder.Append(" AND	SHP.booking_no = MISSING_SAT2.booking_no)) ");
				strBuilder.Append(" ORDER BY last_updated_date, consignment_no ");
			
			}
			strSqlQuery = strBuilder.ToString();
		
			return strSqlQuery;
		}

		//End Modified By Tom

		public static String GetSQIVersusByOrder(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{	
			#region member
			String strstart_date=null;
			String strend_date=null;
			String strtran_date=null;
			String strpayer_type=null;
			String strpayer_code = null;
			String strorigin_dc = null;
			String strdestination_dc = null;
			String strroute_code = null;
			String strroute_type = null;
			String strdelPath_origin_dc = null;
			String strdelPath_destination_dc = null;
			String strzipcode = null;
			String strstate_code = null;
			String conditions = null;
			#endregion
			
			String strSqlQuery = null;
			StringBuilder strBuilder = null;
			DataRow dr = dsQuery.Tables[0].Rows[0];
			strBuilder = new StringBuilder();

			//*************************** Booking Date : Part ***************************
			if (Utility.IsNotDBNull(dr["tran_date"]))
			{
				conditions = " LEFT OUTER JOIN ";
				if(dr["tran_date"].ToString().ToUpper() == "B")
					strtran_date = " booking_datetime ";
				else if(dr["tran_date"].ToString().ToUpper() == "U")
					strtran_date = " last_status_datetime ";
				else if(dr["tran_date"].ToString().ToUpper() == "P")
				{
					strtran_date = " act_delivery_date ";
					conditions = " INNER JOIN ";
				}
				else if(dr["tran_date"].ToString().ToUpper() == "C")
					strtran_date = " act_hc_return_datetime ";
			}
			if (Utility.IsNotDBNull(dr["start_date"]) && Utility.IsNotDBNull(dr["end_date"]))
			{
				DateTime dateStartDate = (DateTime) dr["start_date"];
				DateTime dateEndDate = (DateTime) dr["end_date"];
				strstart_date = Utility.DateFormat(strAppID, strEnterpriseID, dateStartDate, DTFormat.DateTime);
				strend_date	= Utility.DateFormat(strAppID, strEnterpriseID, dateEndDate, DTFormat.DateTime);
			}			
			//*************************** Payer Type : Part ***************************
			String strpayer_type_tmp;
			strpayer_type_tmp = dr["payer_type"].ToString() + "";
			if (strpayer_type_tmp != "")
			{
				strpayer_type = " and payerid in ( select custid from customer where ";
				for (int i=0; i <= strpayer_type_tmp.Length - 1; i ++)
				{
					if (i==0)
					{
						strpayer_type += "payer_type = '" +strpayer_type_tmp.Substring(i,1).ToUpper()+ "'";
					}
					else 
					{
						strpayer_type += "or payer_type = '" +strpayer_type_tmp.Substring(i,1).ToUpper()+ "'";
					}
				}
				strpayer_type += " ) ";
			}

			strpayer_code = dr["payer_code"].ToString() + "";
			if (strpayer_code != "")
			{
				strpayer_code = "and payerid = '" +strpayer_code+ "'";
			}
			//*************************** Route / DC Selection : Part ***************************
			strroute_type = dr["route_type"].ToString() + "";
			strroute_code = dr["route_code"].ToString() + "";
			if (strroute_code != "")
			{
				
				if (strroute_type == "L" || strroute_type == "A")
				{
					strdelPath_origin_dc = "and origin_station = '" + dr["delPath_origin_dc"].ToString() + "'" ;
					strdelPath_destination_dc = "and destination_station = '" + dr["delPath_destination_dc"].ToString() + "'" ; 					
				}
				else
				{
					strroute_code = " and route_code = '" + strroute_code + "'";
				}
			}
			
			strorigin_dc = dr["origin_dc"].ToString() + "";
			if (strorigin_dc != "")
			{
				strorigin_dc = "and origin_station = '" +strorigin_dc+ "'";
			}

			strdestination_dc = dr["destination_dc"].ToString() + "";	
			if(strdestination_dc != "")
			{
				strdestination_dc = "and destination_station = '" +strdestination_dc+ "'";
			}
            
			//*************************** Destination ***************************
			strzipcode = dr["zip_code"].ToString() + "";
			if(strzipcode != "")
			{
				strzipcode = "and recipient_zipcode = '" +strzipcode+ "'";
			}
			strstate_code = dr["state_code"].ToString() + "";
			if(strstate_code != "")
			{
				strstate_code = "and destination_state_code = '" +strstate_code+ "'";
			}

			//*************************** Status Code ***************************
			String strStatusCode1 = "'"+dr["state_code1"].ToString()+"'";
			String strStatusCode2 = "'"+dr["state_code2"].ToString()+"'";
			//*********************************** Building Command String *****************************
			strBuilder.Append(" SELECT	SHP.applicationid, SHP.enterpriseid, ");
			strBuilder.Append(" SHP.booking_no, SHP.consignment_no, SHP.ref_no, ");
			strBuilder.Append(" SHP.recipient_telephone, SHP.recipient_name, ");
			strBuilder.Append(" SHP.recipient_address1, SHP.recipient_address2, ");
			strBuilder.Append(" (select state_name from state ");
			strBuilder.Append(" where applicationid = SHP.applicationid ");
			strBuilder.Append(" and enterpriseid = SHP.enterpriseid ");
			strBuilder.Append(" and state_code = SHP.destination_state_code) as recipient_state_name, SHP.recipient_zipcode, ");
			strBuilder.Append(" SHP.sender_name, SHP.sender_address1, SHP.sender_address2, ");
			strBuilder.Append(" (select state_name from state ");
			strBuilder.Append(" where applicationid = SHP.applicationid ");
			strBuilder.Append(" and enterpriseid = SHP.enterpriseid ");
			strBuilder.Append(" and state_code = SHP.origin_state_code) as sender_state_name, SHP.sender_zipcode, ");
			strBuilder.Append(" SHP.origin_state_code, SHP.destination_state_code, ");
			strBuilder.Append(" SHP.route_code as delivery_route, SHP.booking_datetime, SHP.act_pickup_datetime, ");
			strBuilder.Append(" SHP_DEL.tracking_datetime as act_delivery_date, SHP.act_hc_return_datetime, ");
			strBuilder.Append(" STATUS_CODE1.status_code as sCode1, STATUS_CODE1.tracking_datetime as sCode1DT, ");
			strBuilder.Append(" STATUS_CODE2.status_code as sCode2, STATUS_CODE2.tracking_datetime as sCode2DT, ");
			strBuilder.Append(" isnull(datediff(minute, STATUS_CODE1.tracking_datetime, STATUS_CODE2.tracking_datetime),0) as TotalTime, SHP.last_status_datetime as last_updated_date ");
			//strBuilder.Append(" datediff(hh, STATUS_CODE1.tracking_datetime,STATUS_CODE2.tracking_datetime) as varchar),2)+':' + right('0' + cast(datediff(mi, STATUS_CODE1.tracking_datetime,STATUS_CODE2.tracking_datetime) % 60 as varchar),2) as TotalTime ");			
			strBuilder.Append(" FROM (SELECT	* FROM Shipment ");
			strBuilder.Append(" WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			//strBuilder.Append(" AND booking_datetime between '02/01/2008 00:00' and '02/29/2008 23:59' ");//temp_Date
			if(dr["tran_date"].ToString().ToUpper() != "P")
			{
				strBuilder.Append(" AND" + strtran_date + " between " + strstart_date + " and " + strend_date);
			}
			
			strBuilder.Append(strroute_code +" ");
			strBuilder.Append(strdelPath_origin_dc +" ");
			strBuilder.Append(strdelPath_destination_dc +" ");
			strBuilder.Append(strorigin_dc +" ");
			strBuilder.Append(strdestination_dc +" ");
			strBuilder.Append(strzipcode +" ");
			strBuilder.Append(strstate_code +" ");
			strBuilder.Append(strpayer_code +" ");
			strBuilder.Append(strpayer_type +" ");
			strBuilder.Append(") SHP ");

			strBuilder.Append(" INNER JOIN ");
			strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
			strBuilder.Append(" consignment_no, booking_no, status_code, ");
			strBuilder.Append(" MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append(" FROM shipment_tracking ");
			strBuilder.Append(" WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			strBuilder.Append(" AND (status_code = "+strStatusCode1+" AND isnull(deleted, 'N') <> 'Y') "); //Temp_Status_code1
			strBuilder.Append(" GROUP BY	applicationid, enterpriseid, consignment_no, booking_no, status_code) STATUS_CODE1 ");
			strBuilder.Append(" ON (SHP.applicationid =	STATUS_CODE1.applicationid ");
			strBuilder.Append(" AND	SHP.enterpriseid = STATUS_CODE1.enterpriseid ");
			strBuilder.Append(" AND	SHP.consignment_no = STATUS_CODE1.consignment_no ");
			strBuilder.Append(" AND	SHP.booking_no = STATUS_CODE1.booking_no) ");
			
			strBuilder.Append(" INNER JOIN ");
			strBuilder.Append(" (SELECT		applicationid, enterpriseid, ");
			strBuilder.Append(" consignment_no, booking_no, status_code, ");
			strBuilder.Append(" MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append(" FROM shipment_tracking ");
			strBuilder.Append(" WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'");
			strBuilder.Append(" AND	(status_code = "+strStatusCode2+" AND isnull(deleted, 'N') <> 'Y') ");//Temp_Status_code2
			strBuilder.Append(" GROUP BY	applicationid, enterpriseid, consignment_no, booking_no, status_code) STATUS_CODE2 ");
			strBuilder.Append(" ON (SHP.applicationid =	STATUS_CODE2.applicationid ");
			strBuilder.Append(" AND	SHP.enterpriseid = STATUS_CODE2.enterpriseid ");
			strBuilder.Append(" AND	SHP.consignment_no = STATUS_CODE2.consignment_no ");
			strBuilder.Append(" AND	SHP.booking_no = STATUS_CODE2.booking_no) ");
			
			strBuilder.Append(conditions);
			strBuilder.Append(" (SELECT	applicationid, enterpriseid, ");
			strBuilder.Append(" consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
			strBuilder.Append(" FROM shipment_tracking ");
			strBuilder.Append(" WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("'");
			strBuilder.Append(" AND enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("'"); 
			strBuilder.Append(" AND	(status_code = 'POD' AND isnull(deleted, 'N') <> 'Y') ");
			if(dr["tran_date"].ToString().ToUpper() == "P")
			{
				strBuilder.Append(" AND	tracking_datetime  between "+strstart_date+" and "+strend_date);
			}
			//strBuilder.Append(" AND	tracking_datetime  between "+strstart_date+" and "+strend_date);
			strBuilder.Append(" GROUP BY applicationid, enterpriseid, consignment_no, booking_no) SHP_DEL ");
			strBuilder.Append(" ON (SHP.applicationid = SHP_DEL.applicationid ");
			strBuilder.Append(" AND	SHP.enterpriseid = SHP_DEL.enterpriseid ");
			strBuilder.Append(" AND	SHP.consignment_no = SHP_DEL.consignment_no ");
			strBuilder.Append(" AND	SHP.booking_no = SHP_DEL.booking_no) ");

			strBuilder.Append(" WHERE	STATUS_CODE1.tracking_datetime < STATUS_CODE2.tracking_datetime ");
			
			strSqlQuery = strBuilder.ToString();
		
			return strSqlQuery;
		}


		public static DataSet GetSQIData(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsTopN = null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("ReportDAL.cs","GetTopNPayer","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			String strSqlQuery = null;
//			String strSqlWhere = null;
//			String strSqlSort = null;
//
//			DataRow dr = dsQuery.Tables[0].Rows[0];
//			strBuilder = new StringBuilder();
//
//			strBuilder.Append("SELECT SHP.consignment_no, ");
//			strBuilder.Append("case isnull(SHP_DEL.consignment_no, '') ");
//			strBuilder.Append("when '' then 'NOT DEL' ");
//			strBuilder.Append("else 'DEL' end as DEL_STATUS, ");
//			strBuilder.Append("case ");
//			strBuilder.Append("when SHP_DEL.tracking_datetime is null then 'NOT DEL' ");
//			strBuilder.Append("else (case ");
//			strBuilder.Append("when SHP_DEL.tracking_datetime > SHP.est_delivery_datetime then ");
//			strBuilder.Append("(case ");
//			strBuilder.Append("when SHP_AUDIT.consignment_no is not null then 'LATE AUDIT' ");
//			strBuilder.Append("else 'LATE NOT AUDIT' end) ");
//			strBuilder.Append("else 'NOT LATE' end) ");
//			strBuilder.Append("end as AUDIT_STATUS, SHP_AUDIT.exception_code, PODEX_CODE.exception_type ");
//			strBuilder.Append("from ");
//			strBuilder.Append("(SELECT applicationid, enterpriseid, consignment_no, booking_no, est_delivery_datetime ");
//			strBuilder.Append("FROM Shipment ");
//			strBuilder.Append("WHERE applicationid = 'TIES' ");
//			strBuilder.Append("AND enterpriseid = 'KDTH' ");
//
//			strBuilder.Append("AND est_delivery_datetime between '2007-10-01 00:00:00' and '2007-10-31 23:59:59') SHP ");
//			strBuilder.Append("LEFT OUTER JOIN ");
//			strBuilder.Append("(SELECT shipment_tracking.* ");
//			strBuilder.Append("FROM shipment_tracking INNER JOIN ");
//			strBuilder.Append("(SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
//			strBuilder.Append("FROM shipment_tracking ");
//			strBuilder.Append("WHERE (status_code = 'POD' AND isnull(deleted, 'N') <> 'Y') ");
//			strBuilder.Append("AND consignment_no in (select distinct consignment_no ");
//
//			strBuilder.Append("from Shipment ");
//			strBuilder.Append("where applicationid = 'TIES' ");
//			strBuilder.Append("and enterpriseid = 'KDTH' ");
//			strBuilder.Append("and est_delivery_datetime between '2007-10-01 00:00:00' and '2007-10-31 23:59:59') ");
//
//			strBuilder.Append("GROUP BY applicationid, enterpriseid, consignment_no, booking_no) SUB_TRACK1 ON "); 
//			strBuilder.Append("(dbo.shipment_tracking.applicationid = SUB_TRACK1.applicationid AND ");
//			strBuilder.Append("dbo.shipment_tracking.enterpriseid = SUB_TRACK1.enterpriseid AND ");
//			strBuilder.Append("dbo.shipment_tracking.consignment_no = SUB_TRACK1.consignment_no AND ");
//			strBuilder.Append("dbo.shipment_tracking.booking_no = SUB_TRACK1.booking_no AND ");
//			strBuilder.Append("dbo.shipment_tracking.tracking_datetime = SUB_TRACK1.tracking_datetime)) SHP_DEL ");
//			strBuilder.Append("ON (SHP.applicationid = SHP_DEL.applicationid ");
//			strBuilder.Append("AND SHP.enterpriseid = SHP_DEL.enterpriseid ");
//			strBuilder.Append("AND SHP.consignment_no = SHP_DEL.consignment_no ");
//			strBuilder.Append("AND SHP.booking_no = SHP_DEL.booking_no) ");
//			strBuilder.Append("LEFT OUTER JOIN ");
//			strBuilder.Append("(SELECT shipment_tracking.* ");
//			strBuilder.Append("FROM shipment_tracking INNER JOIN ");
//			strBuilder.Append("(SELECT applicationid, enterpriseid, consignment_no, booking_no, MAX(tracking_datetime) AS tracking_datetime ");
//			strBuilder.Append("FROM shipment_tracking ");
//			strBuilder.Append("WHERE (status_code = 'PODEX' AND isnull(deleted, 'N') <> 'Y') ");
//			strBuilder.Append("AND consignment_no in (	select distinct consignment_no ");
//
//			strBuilder.Append("from Shipment ");
//			strBuilder.Append("where applicationid = 'TIES' ");
//			strBuilder.Append("and enterpriseid = 'KDTH' ");
//			strBuilder.Append("and est_delivery_datetime between '2007-10-01 00:00:00' and '2007-10-31 23:59:59') ");
//
//			strBuilder.Append("GROUP BY applicationid, enterpriseid, consignment_no, booking_no) SUB_TRACK2 ON ");
//			strBuilder.Append("(dbo.shipment_tracking.applicationid = SUB_TRACK2.applicationid AND ");
//			strBuilder.Append("dbo.shipment_tracking.enterpriseid = SUB_TRACK2.enterpriseid AND ");
//			strBuilder.Append("dbo.shipment_tracking.consignment_no = SUB_TRACK2.consignment_no AND ");
//			strBuilder.Append("dbo.shipment_tracking.booking_no = SUB_TRACK2.booking_no AND ");
//			strBuilder.Append("dbo.shipment_tracking.tracking_datetime = SUB_TRACK2.tracking_datetime)) SHP_AUDIT ");
//			strBuilder.Append("ON(SHP.applicationid = SHP_AUDIT.applicationid ");
//			strBuilder.Append("AND SHP.enterpriseid = SHP_AUDIT.enterpriseid ");
//			strBuilder.Append("AND SHP.consignment_no = SHP_AUDIT.consignment_no ");
//			strBuilder.Append("AND SHP.booking_no = SHP_AUDIT.booking_no) ");
//			strBuilder.Append("LEFT OUTER JOIN ");
//			strBuilder.Append("(SELECT * ");
//			strBuilder.Append("FROM EXCEPTION_CODE ");
//			strBuilder.Append("where applicationid = 'TIES' ");
//			strBuilder.Append("and enterpriseid = 'KDTH' ");
//			strBuilder.Append("and status_code = 'PODEX') PODEX_CODE ");
//			strBuilder.Append("ON (SHP_AUDIT.applicationid = PODEX_CODE.applicationid ");
//			strBuilder.Append("AND SHP_AUDIT.enterpriseid = PODEX_CODE.enterpriseid ");
//			strBuilder.Append("AND SHP_AUDIT.exception_code = PODEX_CODE.exception_code) ");
//
//		
			DataRow dr = dsQuery.Tables[0].Rows[0];
			if (Utility.IsNotDBNull(dr["tran_date"]))
			{
				if (dr["tran_date"].ToString().ToUpper() == "P")
				{	strSqlQuery = GetSQICmdStrByPODDate(strAppID,strEnterpriseID,dsQuery); }
				else
				{	strSqlQuery = GetSQICommandStr(strAppID,strEnterpriseID,dsQuery); }

			}
		
			dbcmd = dbCon.CreateCommand(strSqlQuery,CommandType.Text);
			dbcmd.CommandTimeout = 600;
 
			try
			{
				dsTopN = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ReportDAL.cs","GetTopNPayer","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsTopN;
		}

		public static DataSet GetAccPacExport(String strAppID, String strEnterpriseID, string Param)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsReturn = null;

			string strSQL = "";

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			strSQL = @"select sm.consignment_no as 'FreightNo', 
							  sm.shpt_manifest_datetime as 'Date',
							  sm.sender_zipcode as 'Origin',
							  sm.recipient_zipcode as 'Destination',
							  sm.payerid as 'CustomerID',
							  sm.payer_name as 'Customer',
							  sm.intl_docs_flag  AS 'Docs',
							  sm.tot_act_wt as 'Weight',
							  ivd.service_code as 'Service',
							  case ivd.service_code when 'DG' then 'Y' when 'PACK' then 'Y' else 'N' end as 'VAS',
							  ISNULL(sm.total_rated_amount, 0) + ISNULL(sm.export_freight_charge, 0) as 'Revenue',
							  isnull(sm.tax_on_rated_amount, 0) as 'Tax',
							  ivh.exported_by,
							  LEFT(convert(nvarchar(50), exported_date, 103) + ' ' + convert(nvarchar(50), exported_date, 108), 16) AS 'jobid'
						from dbo.Invoice ivh
						left join Invoice_Detail ivd on ivh.invoice_no = ivd.invoice_no
						right join dbo.Shipment sm on ivd.consignment_no = sm.consignment_no AND sm.invoice_no = ivh.invoice_no
						where ivh.invoice_no in ({0})
						GROUP BY sm.consignment_no,
									sm.shpt_manifest_datetime,
									sm.sender_zipcode,
									sm.recipient_zipcode,
									sm.payerid,
									sm.payer_name,
									ivd.service_code,
									sm.intl_docs_flag,
									sm.tot_act_wt,
									ivd.service_code,
									sm.tot_vas_surcharge,
									sm.total_rated_amount,
									sm.invoice_amt,
									ivh.exported_by,
									exported_date,
									sm.tax_on_rated_amount,
									sm.export_freight_charge
						order by sm.consignment_no ";
	
			strSQL = string.Format(strSQL, Param);
			dbcmd = dbCon.CreateCommand(strSQL, CommandType.Text);
			dbcmd.CommandTimeout = 1500;

			try
			{
				dsReturn = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ReportDAL.cs","GetTopNPayer","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			return dsReturn;
		}

		public static DataSet GetUpliftStatistics(string strAppID, string strEnterpriseID, DataSet dsQuery)
		{
			string reportType = string.Empty;
			string dateType = string.Empty;
			string dateFrom = string.Empty;
			string dateTo = string.Empty;
			string payerType = string.Empty;
			string payerId = string.Empty;
			string zipcodeFROM = string.Empty;
			string dataType = string.Empty;

			DataSet dsResult = new DataSet();

			DataRow dr = dsQuery.Tables[0].Rows[0];

			if (Utility.IsNotDBNull(dr["reportType"])) 
				reportType = dr["reportType"].ToString();
			if (Utility.IsNotDBNull(dr["dateType"])) 
				dateType = dr["dateType"].ToString();
			if (Utility.IsNotDBNull(dr["dateFrom"])) 
				dateFrom = dr["dateFrom"].ToString();
			if (Utility.IsNotDBNull(dr["dateTo"])) 
				dateTo = dr["dateTo"].ToString();
			if (Utility.IsNotDBNull(dr["payer_type"]))
				payerType = dr["payer_type"].ToString().Replace(@"""", "").Replace("'", "");
			if (Utility.IsNotDBNull(dr["payerId"]))
				payerId = dr["payerId"].ToString();
			if (Utility.IsNotDBNull(dr["zipcodeFROM"]))
				zipcodeFROM = dr["zipcodeFROM"].ToString();
			if (Utility.IsNotDBNull(dr["dataType"]))
				dataType = dr["dataType"].ToString();

			try
			{ 
				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid", strEnterpriseID));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid", strAppID));

				if(reportType != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@reportType", reportType));
				if(dateType != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@dateType", dateType));
				if(dateFrom != "")
				{
					string[] tempDateFrom = dateFrom.Split(' ');
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@dateFrom", tempDateFrom[0]));
				}
				if(dateTo != "")
				{
					string[] tempDateTo = dateTo.Split(' ');
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@dateTo", tempDateTo[0]));
				}
				if(payerType != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerType", payerType));
				if(payerId != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerId", payerId));
				if(zipcodeFROM != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@zipcodeFROM", zipcodeFROM));
				if(dataType != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@dataType", dataType));

				dsResult = (DataSet)dbCon.ExecuteProcedure("UpliftStatistics", storedParams, ReturnType.DataSetType);
				return dsResult;
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "UpliftStatisticsDAL", "GetUpliftStatistics", ex.Message);
				throw new ApplicationException("Call Stored Procedure failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 
		}

		public static DataSet GetFreightSummary(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsReturn = null;

			string strSQL = "";
			string start_date = "";
			string end_date = "";
			string payer_type = "";
			string payer_code = "";
			string date_mode = "";
            string shipment_type = "";

            DataRow dr = dsQuery.Tables[0].Rows[0];

			if (Utility.IsNotDBNull(dr["start_date"])) 
				start_date = dr["start_date"].ToString();
			if (Utility.IsNotDBNull(dr["end_date"])) 
				end_date = dr["end_date"].ToString();
			if (Utility.IsNotDBNull(dr["payer_type"])) 
				payer_type = dr["payer_type"].ToString();
			if (Utility.IsNotDBNull(dr["payer_code"])) 
				payer_code = dr["payer_code"].ToString();
			if (Utility.IsNotDBNull(dr["date_mode"]))
				date_mode = dr["date_mode"].ToString();
            if (Utility.IsNotDBNull(dr["shipment_type"]))
                shipment_type = dr["shipment_type"].ToString();

            DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			string[] dateFrom = start_date.Split(' ');
			string[] tempDateFrom = dateFrom[0].Split('/');
			string df = (tempDateFrom[1].Length == 1 ? "0" + tempDateFrom[1] : tempDateFrom[1]) + "/" + 
						(tempDateFrom[0].Length == 1 ? "0" + tempDateFrom[0] : tempDateFrom[0]) + "/" + tempDateFrom[2];
			string[] dateTo = end_date.Split(' ');
			string[] tempDateTo = dateTo[0].Split('/');
			string dt = (tempDateTo[1].Length == 1 ? "0" + tempDateTo[1] : tempDateTo[1]) + "/" + 
						(tempDateTo[0].Length == 1 ? "0" + tempDateTo[0] : tempDateTo[0]) + "/" + tempDateTo[2];

            strSQL = @"SELECT s.shpt_manifest_datetime,
							s.consignment_no, 
							s.sender_zipcode as 'origin_station',
							s.recipient_zipcode as 'destination_station',
							tot_act_wt=s.chargeable_wt,
							(
								SELECT TOP 1 CAST(ROUND(increment_price * (CASE WHEN c.payer_type='F' THEN 0.85 ELSE 1 END), 2) AS decimal(18,2))
									FROM dbo.base_zone_rates 
									WHERE applicationid = '" + strAppID + @"'
									AND enterpriseid = '" + strEnterpriseID + @"' 
									AND origin_zone_code = s.origin_station 
									AND destination_zone_code = s.destination_station
									AND service_code = s.service_code
									AND s.tot_act_wt >= start_wt AND s.tot_act_wt < end_wt
									AND effective_date = (SELECT MAX(effective_date) 
																		FROM dbo.base_zone_rates
																		WHERE applicationid = '" + strAppID + @"'
																		and enterpriseid = '" + strEnterpriseID + @"' 
																		AND origin_zone_code = s.origin_station 
																		AND destination_zone_code = s.destination_station
																		AND service_code = s.service_code
																		AND s.tot_act_wt >= start_wt AND s.tot_act_wt < end_wt
																		AND effective_date <= s.shpt_manifest_datetime
																		)
								) AS 'rate',
							basic_charge=CASE WHEN (b.ConsignmentType='D' OR f.TotalInvoice=0) THEN s.basic_charge WHEN (b.ConsignmentType='I' and s.payerid2 IS NULL) THEN 0 WHEN (b.ConsignmentType='I' and s.payerid2 IS NOT NULL) THEN s.basic_charge END, 
							tot_freight_charge=CASE WHEN (b.ConsignmentType='D' OR f.TotalInvoice=0) THEN s.tot_freight_charge WHEN (b.ConsignmentType='I' and s.payerid2 IS NULL) THEN 0 WHEN (b.ConsignmentType='I' and s.payerid2 IS NOT NULL) THEN s.tot_freight_charge END,
                            other_surch_amount=CASE WHEN (b.ConsignmentType='D' OR f.TotalInvoice=0) THEN s.other_surch_amount WHEN (b.ConsignmentType='I' and s.payerid2 IS NULL) THEN 0 WHEN (b.ConsignmentType='I' and s.payerid2 IS NOT NULL) THEN s.other_surch_amount END,
                            tot_vas_surcharge=CASE WHEN (b.ConsignmentType='D' OR f.TotalInvoice=0) THEN s.tot_vas_surcharge WHEN (b.ConsignmentType='I' and s.payerid2 IS NULL) THEN 0 WHEN (b.ConsignmentType='I' and s.payerid2 IS NOT NULL) THEN s.tot_vas_surcharge END,
                            tax=CASE WHEN (b.ConsignmentType='D' OR f.TotalInvoice=0) THEN s.tax_on_rated_amount WHEN (b.ConsignmentType='I' and s.payerid2 IS NULL) THEN 0 WHEN (b.ConsignmentType='I' and s.payerid2 IS NOT NULL) THEN s.tax_on_rated_amount END,
							s.MasterAWBNumber,
							ISNULL(s.payerid2, s.payerid) as 'payerid',
							c.cust_name,
							total_rated_amount=CASE WHEN (b.ConsignmentType='D' OR f.TotalInvoice=0) THEN s.total_rated_amount WHEN (b.ConsignmentType='I' and s.payerid2 IS NULL) THEN 0 WHEN (b.ConsignmentType='I' and s.payerid2 IS NOT NULL) THEN s.total_rated_amount END,
							invoice_amt=CASE WHEN (b.ConsignmentType='D' OR f.TotalInvoice=0) THEN ISNULL(s.total_rated_amount, 0) + ISNULL(s.tax_on_rated_amount, 0)
                                             WHEN b.ConsignmentType='E' THEN ISNULL(s.total_rated_amount, 0) + ISNULL(s.tax_on_rated_amount, 0) + ISNULL(s.export_freight_charge, 0)
                                             WHEN b.ConsignmentType='I' and s.payerid2 IS NULL THEN f.TotalInvoice
											 WHEN b.ConsignmentType='I' and s.payerid2 IS NOT NULL THEN ISNULL(s.total_rated_amount, 0) + ISNULL(s.tax_on_rated_amount, 0) + ISNULL(s.payerid2_agency_charges,0)
                                        END,
							AgencyFees=CASE WHEN b.ConsignmentType='I' and s.payerid2 IS NULL THEN ROUND(ISNULL(f.AgencyFees,0),2) WHEN b.ConsignmentType='I' and s.payerid2 IS NOT NULL THEN ISNULL(s.payerid2_agency_charges,0) END, 
                            Disbursements=CASE WHEN b.ConsignmentType='I' and s.payerid2 IS NULL THEN ROUND(ISNULL(f.Disbursements,0),2) WHEN b.ConsignmentType='I' and s.payerid2 IS NOT NULL THEN 0 END, 
                            TotalInvoice=CASE WHEN b.ConsignmentType='I' and s.payerid2 IS NULL THEN ROUND(ISNULL(f.TotalInvoice,0),2) WHEN b.ConsignmentType='I' and s.payerid2 IS NOT NULL THEN 0 END,
                            s.MasterAWBNumber,
							ISNULL(s.payerid2, s.payerid) as 'payerid',
							c.cust_name,					                                       
							'" + date_mode + @"' as mode,
							'" + df + @"' as dateFrom,
							'" + dt + @"' as dateTo,
							cs.code_text, 
                            cost_centre=CASE WHEN ISNULL(r.cost_centre,'')='' THEN '' ELSE r.cost_centre END, s.service_code, b.ConsignmentType,                     
                            shipment_type_selected='" + shipment_type.ToString() + "',pngaf_invoice=a.JobEntryNo,tot_pkg=s.tot_pkg ";
            strSQL += "FROM dbo.Shipment s CROSS APPLY dbo.GetConsignmentType(s.enterpriseid, s.export_flag, s.sender_name, s.sender_zipcode, s.recipient_zipcode) b ";
            strSQL += "LEFT JOIN dbo.Customer c  ON ISNULL(s.payerid2, s.payerid) = custid AND s.enterpriseid = c.enterpriseid AND s.applicationid = c.applicationid ";
            strSQL += "LEFT JOIN v_References r ON s.applicationid = r.applicationid AND s.enterpriseid = r.enterpriseid AND s.recipient_telephone = r.telephone  ";
			strSQL += "INNER JOIN dbo.Core_System_Code cs ON cs.applicationid = '" + strAppID + "' and cs.culture = 'en-US' AND codeid = 'customer_type' AND cs.code_str_value = c.payer_type ";
            strSQL += "LEFT JOIN dbo.Customs_Jobs a ON s.applicationid = a.applicationid AND s.enterpriseid = a.enterpriseid AND s.consignment_no = a.consignment_no ";
            strSQL += "OUTER APPLY ( ";
            strSQL += "SELECT AgencyFees = SUM(CASE FeeDisb WHEN 'FEE' THEN COALESCE(g.OverrideAmount, CalculatedAmount, 0) END) ";
            strSQL += ", Disbursements = SUM(CASE FeeDisb WHEN 'DISB' THEN COALESCE(g.OverrideAmount, CalculatedAmount, 0) END) ";
            strSQL += ", TotalInvoice = SUM(COALESCE(g.OverrideAmount, CalculatedAmount, 0)) ";
            strSQL += "FROM dbo.CalcCustomsInvoice(a.enterpriseid, a.consignment_no, ISNULL(s.payerid2, s.payerid), a.JobEntryNo) g ";
            strSQL += ") f ";
            //strSQL += "WHERE s.sender_zipcode <> s.recipient_zipcode AND s.applicationid = '" + strAppID + "' ";
            strSQL += "WHERE s.applicationid = '" + strAppID + "' ";
            strSQL += "AND s.enterpriseid = '" + strEnterpriseID + "' ";

			if(date_mode == "M")
			{
				if(start_date!="")
					strSQL += "AND s.shpt_manifest_datetime >= '" + start_date + "' ";
				if(end_date!="")
					strSQL += "AND s.shpt_manifest_datetime <= '" + end_date + "' ";
			}
			else if(date_mode == "I")
			{
				if(start_date!="")
					strSQL += "AND s.invoice_date >= '" + start_date + "' ";
				if(end_date!="")
					strSQL += "AND s.invoice_date <= '" + end_date + "' ";
			}
			if(date_mode == "N")
			{
				if(start_date!="")
					strSQL += "AND s.shpt_manifest_datetime >= '" + start_date + "' ";
				if(end_date!="")
					strSQL += "AND s.shpt_manifest_datetime <= '" + end_date + "' ";
				strSQL += "AND s.invoice_no is null ";
			}


			if(payer_type != "")
				strSQL += "AND c.payer_type = " + payer_type + " ";
			if(payer_code != "")
				strSQL += "AND c.custid = '" + payer_code + "' ";

            if (shipment_type == "D" || shipment_type == "I")
                strSQL += "AND b.ConsignmentType = '" + shipment_type + "' ";
            else if (shipment_type == "B")
                strSQL += "AND b.ConsignmentType IN ('D', 'I') ";

            strSQL += "Order By s.shpt_manifest_datetime, s.consignment_no ";

			dbcmd = dbCon.CreateCommand(strSQL, CommandType.Text);
			dbcmd.CommandTimeout = 1500;

			try
			{
				dsReturn = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ReportDAL.cs","GetTopNPayer","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			return dsReturn;
		}

        public static DataSet GetFreightSummaryFedEx_TNT_Invoices(String strAppID, String strEnterpriseID, DataSet dsQuery)
        {
            DbConnection dbCon = null;
            IDbCommand dbcmd = null;
            DataSet dsReturn = null;

            string strSQL = "";
            string start_date = "";
            string end_date = "";
            string payer_type = "";
            string payer_code = "";
            string date_mode = "";
            string shipment_type = "";

            DataRow dr = dsQuery.Tables[0].Rows[0];

            if (Utility.IsNotDBNull(dr["start_date"]))
                start_date = dr["start_date"].ToString();
            if (Utility.IsNotDBNull(dr["end_date"]))
                end_date = dr["end_date"].ToString();
            if (Utility.IsNotDBNull(dr["payer_type"]))
                payer_type = dr["payer_type"].ToString();
            if (Utility.IsNotDBNull(dr["payer_code"]))
                payer_code = dr["payer_code"].ToString();
            if (Utility.IsNotDBNull(dr["date_mode"]))
                date_mode = dr["date_mode"].ToString();
            if (Utility.IsNotDBNull(dr["shipment_type"]))
                shipment_type = dr["shipment_type"].ToString();

            DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
            dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

            string[] dateFrom = start_date.Split(' ');
            string[] tempDateFrom = dateFrom[0].Split('/');
            string df = (tempDateFrom[1].Length == 1 ? "0" + tempDateFrom[1] : tempDateFrom[1]) + "/" +
                        (tempDateFrom[0].Length == 1 ? "0" + tempDateFrom[0] : tempDateFrom[0]) + "/" + tempDateFrom[2];
            string[] dateTo = end_date.Split(' ');
            string[] tempDateTo = dateTo[0].Split('/');
            string dt = (tempDateTo[1].Length == 1 ? "0" + tempDateTo[1] : tempDateTo[1]) + "/" +
                        (tempDateTo[0].Length == 1 ? "0" + tempDateTo[0] : tempDateTo[0]) + "/" + tempDateTo[2];

            strSQL = @"SELECT s.shpt_manifest_datetime,
							s.consignment_no, 
							s.sender_zipcode as 'origin_station',
							s.recipient_zipcode as 'destination_station',
							tot_act_wt=s.chargeable_wt,
							(
								SELECT TOP 1 CAST(ROUND(increment_price * (CASE WHEN c.payer_type='F' THEN 0.85 ELSE 1 END), 2) AS decimal(18,2))
									FROM dbo.base_zone_rates 
									WHERE applicationid = '" + strAppID + @"'
									AND enterpriseid = '" + strEnterpriseID + @"' 
									AND origin_zone_code = s.origin_station 
									AND destination_zone_code = s.destination_station
									AND service_code = s.service_code
									AND s.tot_act_wt >= start_wt AND s.tot_act_wt < end_wt
									AND effective_date = (SELECT MAX(effective_date) 
																		FROM dbo.base_zone_rates
																		WHERE applicationid = '" + strAppID + @"'
																		and enterpriseid = '" + strEnterpriseID + @"' 
																		AND origin_zone_code = s.origin_station 
																		AND destination_zone_code = s.destination_station
																		AND service_code = s.service_code
																		AND s.tot_act_wt >= start_wt AND s.tot_act_wt < end_wt
																		AND effective_date <= s.shpt_manifest_datetime
																		)
								) AS 'rate',
							basic_charge=CASE WHEN (b.ConsignmentType='D' OR f.TotalInvoice=0) THEN s.basic_charge WHEN (b.ConsignmentType='I' and s.payerid2 IS NULL) THEN 0 WHEN (b.ConsignmentType='I' and s.payerid2 IS NOT NULL) THEN s.basic_charge END, 
							tot_freight_charge=CASE WHEN (b.ConsignmentType='D' OR f.TotalInvoice=0) THEN s.tot_freight_charge WHEN (b.ConsignmentType='I' and s.payerid2 IS NULL) THEN 0 WHEN (b.ConsignmentType='I' and s.payerid2 IS NOT NULL) THEN s.tot_freight_charge END,
                            other_surch_amount=CASE WHEN (b.ConsignmentType='D' OR f.TotalInvoice=0) THEN s.other_surch_amount WHEN (b.ConsignmentType='I' and s.payerid2 IS NULL) THEN 0 WHEN (b.ConsignmentType='I' and s.payerid2 IS NOT NULL) THEN s.other_surch_amount END,
                            tot_vas_surcharge=CASE WHEN (b.ConsignmentType='D' OR f.TotalInvoice=0) THEN s.tot_vas_surcharge WHEN (b.ConsignmentType='I' and s.payerid2 IS NULL) THEN 0 WHEN (b.ConsignmentType='I' and s.payerid2 IS NOT NULL) THEN s.tot_vas_surcharge END,
                            tax=CASE WHEN (b.ConsignmentType='D' OR f.TotalInvoice=0) THEN s.tax_on_rated_amount WHEN (b.ConsignmentType='I' and s.payerid2 IS NULL) THEN 0 WHEN (b.ConsignmentType='I' and s.payerid2 IS NOT NULL) THEN s.tax_on_rated_amount END,
							s.MasterAWBNumber,
							ISNULL(s.payerid2, s.payerid) as 'payerid',
							c.cust_name,
							total_rated_amount=CASE WHEN (b.ConsignmentType='D' OR f.TotalInvoice=0) THEN s.total_rated_amount WHEN (b.ConsignmentType='I' and s.payerid2 IS NULL) THEN 0 WHEN (b.ConsignmentType='I' and s.payerid2 IS NOT NULL) THEN s.total_rated_amount END,
							invoice_amt=CASE WHEN (b.ConsignmentType='D' OR f.TotalInvoice=0) THEN ISNULL(s.total_rated_amount, 0) + ISNULL(s.tax_on_rated_amount, 0)
                                             WHEN b.ConsignmentType='E' THEN ISNULL(s.total_rated_amount, 0) + ISNULL(s.tax_on_rated_amount, 0) + ISNULL(s.export_freight_charge, 0)
                                             WHEN b.ConsignmentType='I' and s.payerid2 IS NULL THEN f.TotalInvoice
											 WHEN b.ConsignmentType='I' and s.payerid2 IS NOT NULL THEN ISNULL(s.total_rated_amount, 0) + ISNULL(s.tax_on_rated_amount, 0) + ISNULL(s.payerid2_agency_charges,0)
                                        END,
							AgencyFees=ROUND(ISNULL(f.AgencyFees,0),2), 
                            Disbursements=ROUND(ISNULL(f.Disbursements,0),2), 
                            TotalInvoice=ROUND(ISNULL(f.TotalInvoice,0),2),
                            s.MasterAWBNumber,
							ISNULL(s.payerid2, s.payerid) as 'payerid',
							c.cust_name,					                                       
							'" + date_mode + @"' as mode,
							'" + df + @"' as dateFrom,
							'" + dt + @"' as dateTo,
							cs.code_text, 
                            cost_centre=CASE WHEN ISNULL(r.cost_centre,'')='' THEN '' ELSE r.cost_centre END, s.service_code, b.ConsignmentType,                     
                            shipment_type_selected='" + shipment_type.ToString() + "',pngaf_invoice=a.JobEntryNo, tot_pkg=s.tot_pkg ";
            strSQL += "FROM dbo.Shipment s CROSS APPLY dbo.GetConsignmentType(s.enterpriseid, s.export_flag, s.sender_name, s.sender_zipcode, s.recipient_zipcode) b ";
            strSQL += "LEFT JOIN dbo.Customer c  ON ISNULL(s.payerid2, s.payerid) = custid AND s.enterpriseid = c.enterpriseid AND s.applicationid = c.applicationid ";
            strSQL += "LEFT JOIN v_References r ON s.applicationid = r.applicationid AND s.enterpriseid = r.enterpriseid AND s.recipient_telephone = r.telephone  ";
            strSQL += "INNER JOIN dbo.Core_System_Code cs ON cs.applicationid = '" + strAppID + "' and cs.culture = 'en-US' AND codeid = 'customer_type' AND cs.code_str_value = c.payer_type ";
            strSQL += "LEFT JOIN dbo.Customs_Jobs a ON s.applicationid = a.applicationid AND s.enterpriseid = a.enterpriseid AND s.consignment_no = a.consignment_no ";
            strSQL += "OUTER APPLY ( ";
            strSQL += "SELECT AgencyFees = SUM(CASE FeeDisb WHEN 'FEE' THEN COALESCE(g.OverrideAmount, CalculatedAmount, 0) END) ";
            strSQL += ", Disbursements = SUM(CASE FeeDisb WHEN 'DISB' THEN COALESCE(g.OverrideAmount, CalculatedAmount, 0) END) ";
            strSQL += ", TotalInvoice = SUM(COALESCE(g.OverrideAmount, CalculatedAmount, 0)) ";
            strSQL += "FROM dbo.CalcCustomsInvoice(a.enterpriseid, a.consignment_no, s.payerid, a.JobEntryNo) g ";
            strSQL += "WHERE g.Code IN ('DF', 'FC') ";
            strSQL += ") f ";
            //strSQL += "WHERE s.sender_zipcode <> s.recipient_zipcode AND s.applicationid = '" + strAppID + "' ";
            strSQL += "WHERE s.applicationid = '" + strAppID + "' ";
            strSQL += "AND s.enterpriseid = '" + strEnterpriseID + "' ";
            strSQL += "AND f.TotalInvoice = 0 ";

            if (date_mode == "M")
            {
                if (start_date != "")
                    strSQL += "AND s.shpt_manifest_datetime >= '" + start_date + "' ";
                if (end_date != "")
                    strSQL += "AND s.shpt_manifest_datetime <= '" + end_date + "' ";
            }
            else if (date_mode == "I")
            {
                if (start_date != "")
                    strSQL += "AND s.invoice_date >= '" + start_date + "' ";
                if (end_date != "")
                    strSQL += "AND s.invoice_date <= '" + end_date + "' ";
            }
            if (date_mode == "N")
            {
                if (start_date != "")
                    strSQL += "AND s.shpt_manifest_datetime >= '" + start_date + "' ";
                if (end_date != "")
                    strSQL += "AND s.shpt_manifest_datetime <= '" + end_date + "' ";
                strSQL += "AND s.invoice_no is null ";
            }


            if (payer_type != "")
                strSQL += "AND c.payer_type = 'F'";
            if (payer_code != "")
                strSQL += "AND c.custid = '" + payer_code + "' ";

            if (shipment_type == "D" || shipment_type == "I")
                strSQL += "AND b.ConsignmentType = '" + shipment_type + "' ";
            else if (shipment_type == "B")
                strSQL += "AND b.ConsignmentType IN ('D', 'I') ";

            strSQL += "Order By s.shpt_manifest_datetime, s.consignment_no ";

            dbcmd = dbCon.CreateCommand(strSQL, CommandType.Text);
            dbcmd.CommandTimeout = 1500;

            try
            {
                dsReturn = (DataSet)dbCon.ExecuteQuery(dbcmd, com.common.DAL.ReturnType.DataSetType);
            }
            catch (ApplicationException appException)
            {
                Logger.LogTraceError("ReportDAL.cs", "GetTopNPayer", "ERR002", "Error in Query String " + appException.Message);
                throw appException;
            }

            return dsReturn;
        }


        public static DataSet GetFreightSummaryByCustomerID(String strAppID, String strEnterpriseID, string payer_code, string ReportDay)
        {
            DbConnection dbCon = null;
            IDbCommand dbcmd = null;
            DataSet dsReturn = null;

            string strSQL = "";

            DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
            dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

            strSQL = @"SELECT s.shpt_manifest_datetime,
							s.consignment_no, 
							s.sender_zipcode as 'origin_station',
							s.recipient_zipcode as 'destination_station',
							s.tot_act_wt,
							(
								SELECT CAST(ROUND(increment_price * (CASE WHEN c.payer_type='F' THEN 0.85 ELSE 1 END), 2) AS decimal(18,2))
									FROM dbo.base_zone_rates 
									WHERE applicationid = '" + strAppID + @"'
									AND enterpriseid = '" + strEnterpriseID + @"' 
									AND origin_zone_code = s.origin_station 
									AND destination_zone_code = s.destination_station
									AND service_code = s.service_code
									AND s.tot_act_wt >= start_wt AND s.tot_act_wt < end_wt
									AND effective_date = (SELECT MAX(effective_date) 
																		FROM dbo.base_zone_rates
																		WHERE applicationid = '" + strAppID + @"'
																		and enterpriseid = '" + strEnterpriseID + @"' 
																		AND origin_zone_code = s.origin_station 
																		AND destination_zone_code = s.destination_station
																		AND service_code = s.service_code
																		AND s.tot_act_wt >= start_wt AND s.tot_act_wt < end_wt
																		AND effective_date <= s.shpt_manifest_datetime
																		)
								) AS 'rate',
							s.basic_charge,
							s.tot_freight_charge,
							s.other_surch_amount,
							s.tot_vas_surcharge,
							s.tax_on_rated_amount as 'tax',
							s.MasterAWBNumber,
							ISNULL(s.payerid2, s.payerid) as 'payerid',
							c.cust_name,
							s.total_rated_amount,
							invoice_amt=CASE WHEN b.ConsignmentType='D' THEN ISNULL(s.total_rated_amount, 0) + ISNULL(s.tax_on_rated_amount, 0)
                                             WHEN b.ConsignmentType='E' THEN ISNULL(s.total_rated_amount, 0) + ISNULL(s.tax_on_rated_amount, 0) + ISNULL(s.export_freight_charge, 0)
                                             WHEN b.ConsignmentType='I' AND ISNULL(s.invoice_amt,0) > 0 THEN ISNULL(s.invoice_amt,0) END,
                            'M' mode,convert(varchar(10),DATEADD(D,-" + ReportDay + @",GETDATE()), 103) dateFrom, convert(varchar(10),DATEADD(D,-1,GETDATE()), 103) dateTo,
                            cs.code_text, cost_centre=CASE WHEN ISNULL(r.cost_centre,'')='' THEN '-' ELSE r.cost_centre END, s.service_code 
						FROM dbo.Shipment s CROSS APPLY dbo.GetConsignmentType(s.enterpriseid, s.export_flag, s.sender_name, s.sender_zipcode, s.recipient_zipcode) b
						LEFT JOIN dbo.Customer c  ON ISNULL(s.payerid2, s.payerid) = custid AND s.enterpriseid = c.enterpriseid AND s.applicationid = c.applicationid 
						LEFT JOIN v_References r ON s.applicationid = r.applicationid AND s.enterpriseid = r.enterpriseid AND s.recipient_telephone = r.telephone  ";
            strSQL += "INNER JOIN dbo.Core_System_Code cs ON cs.applicationid = '" + strAppID + "' and cs.culture = 'en-US' AND codeid = 'customer_type' AND cs.code_str_value = c.payer_type ";
            strSQL += "WHERE s.sender_zipcode <> s.recipient_zipcode AND s.applicationid = '" + strAppID + "' ";
            strSQL += "AND s.enterpriseid = '" + strEnterpriseID + "' ";
            strSQL += "AND c.custid = '" + payer_code + "' ";
            strSQL += "AND convert(date,s.shpt_manifest_datetime, 103) >= convert(date,DATEADD(D,-" + ReportDay + ",GETDATE()), 103)";
            strSQL += "AND convert(date,s.shpt_manifest_datetime, 103) < convert(date,GETDATE(), 103)";
            strSQL += "Order By s.shpt_manifest_datetime, s.consignment_no ";

            dbcmd = dbCon.CreateCommand(strSQL, CommandType.Text);
            dbcmd.CommandTimeout = 1500;

            try
            {
                dsReturn = (DataSet)dbCon.ExecuteQuery(dbcmd, com.common.DAL.ReturnType.DataSetType);
            }
            catch (ApplicationException appException)
            {
                Logger.LogTraceError("ReportDAL.cs", "GetFreightSummaryByCustomerID", "ERR002", "Error in Query String " + appException.Message);
                throw appException;
            }

            return dsReturn;
        }

        public static DataSet GetSQIDataVersus(String strAppID, String strEnterpriseID, DataSet dsQuery)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsTopN = null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("ReportDAL.cs","GetTopNPayer","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			String strSqlQuery = null;

			DataRow dr = dsQuery.Tables[0].Rows[0];
			if (Utility.IsNotDBNull(dr["tran_date"]))
			{
				if(dr["versus_type"] == "O")
					strSqlQuery = GetSQIVersusByOrder(strAppID,strEnterpriseID,dsQuery);
				else
					strSqlQuery = GetSQIVersusByMissingCode(strAppID,strEnterpriseID,dsQuery);
			}
		
			dbcmd = dbCon.CreateCommand(strSqlQuery,CommandType.Text);
			dbcmd.CommandTimeout = 1500;
 
			try
			{
				dsTopN = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ReportDAL.cs","GetTopNPayer","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsTopN;
		}


		public static DataSet GetHeaderCompName(String strAppID, String strEnterpriseID)
		{
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsHeader = null;
			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID,strEnterpriseID);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("ReportDAL.cs","GetHeaderCompName","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select enterprise_name as companyName, ");
			strBuilder.Append("(Enterprise.address1+' '+Enterprise.address2) as companyAddress, ");
			strBuilder.Append("country+' '+zipcode as countryName ");
			strBuilder.Append("from Enterprise ");
			strBuilder.Append("where ");
			strBuilder.Append("applicationid=");
			strBuilder.Append("'"+strAppID+"'");
			strBuilder.Append("and enterpriseid=");
			strBuilder.Append("'"+strEnterpriseID+"'");

			dbcmd = dbCon.CreateCommand(strBuilder.ToString(),CommandType.Text);
			try
			{
				dsHeader = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType); 
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ReportDAL.cs","GetHeaderCompName","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			return dsHeader;
		}
		//SOMPOTE
		public static DataSet GetCreditAging(DataSet dsQuery)
		{	
			DbConnection dbCon= null;
			IDbCommand dbcmd=null;
			DataSet dsCreditAging = null;
			//StringBuilder strBuilder = null;

			DataRow dr = dsQuery.Tables[0].Rows[0];
			string applicationid = null; 
			string enterpriseid = null; 
			string custid = null; 
			string salesmanid = null; 
			string status_active = null; 
			string master_account = null; 
			int ref_date_no = 0;
			bool isSummary = true; 
			bool isReport = true ;	

			if (Utility.IsNotDBNull(dr["applicationid"])) 
				applicationid = dr["applicationid"].ToString();
			if (Utility.IsNotDBNull(dr["enterpriseid"])) 
				enterpriseid = dr["enterpriseid"].ToString();
			if (Utility.IsNotDBNull(dr["custid"])) 
				custid = dr["custid"].ToString();
			if (Utility.IsNotDBNull(dr["salesmanid"])) 
				salesmanid = dr["salesmanid"].ToString();
			if (Utility.IsNotDBNull(dr["status_active"])) 
				status_active = dr["status_active"].ToString();
			if (Utility.IsNotDBNull(dr["master_account"])) 
				master_account = dr["master_account"].ToString();
			if (Utility.IsNotDBNull(dr["ref_date_no"])) 
				ref_date_no = Convert.ToInt16(dr["ref_date_no"].ToString());
			if (Utility.IsNotDBNull(dr["isSummary"])) 
				isSummary = Convert.ToBoolean(dr["isSummary"].ToString()); 
			if (Utility.IsNotDBNull(dr["isReport"])) 
				isReport = Convert.ToBoolean(dr["isReport"].ToString());

			DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(applicationid,enterpriseid);
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(applicationid,enterpriseid);

			if (dbCon==null)
			{
				Logger.LogTraceError("ReportDAL.cs","GetCreditAging","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//String strSqlQuery = null;
			//String strSqlWhere = null;
			//String strSqlSort = null;

			
			//strBuilder = new StringBuilder();


			/*switch (dbProvider)
			{
				case DataProviderType.Sql :
										
					break;
				
				case DataProviderType.Oracle:
									

					break;
				
				case DataProviderType.Oledb:
					break;
			}*/
		

			//strSqlQuery = "GetCreditAging";
			//dbcmd.CommandType = CommandType.StoredProcedure;
			dbcmd = dbCon.CreateCommand("GetCreditAging",CommandType.StoredProcedure);

			IDbDataParameter paramApplicationid = dbcmd.CreateParameter();
			paramApplicationid.DbType = DbType.String;
			paramApplicationid.ParameterName = "@applicationid";
			paramApplicationid.Value = applicationid;			
			dbcmd.Parameters.Add(paramApplicationid);

			IDbDataParameter paramEnterpriseid = dbcmd.CreateParameter();
			paramEnterpriseid.DbType = DbType.String;
			paramEnterpriseid.ParameterName = "@enterpriseid";
			paramEnterpriseid.Value = enterpriseid;			
			dbcmd.Parameters.Add(paramEnterpriseid);

			IDbDataParameter paramCustid = dbcmd.CreateParameter();
			paramCustid.DbType = DbType.String;
			paramCustid.ParameterName = "@custid";
			paramCustid.Value = custid;			
			dbcmd.Parameters.Add(paramCustid);

			IDbDataParameter paramSalesmanid = dbcmd.CreateParameter();
			paramSalesmanid.DbType = DbType.String;
			paramSalesmanid.ParameterName = "@salesmanid";
			paramSalesmanid.Value = salesmanid;			
			dbcmd.Parameters.Add(paramSalesmanid);

			IDbDataParameter paramStatus_active = dbcmd.CreateParameter();
			paramStatus_active.DbType = DbType.String;
			paramStatus_active.ParameterName = "@status_active";
			paramStatus_active.Value = status_active;			
			dbcmd.Parameters.Add(paramStatus_active);

			IDbDataParameter paramMaster_account = dbcmd.CreateParameter();
			paramMaster_account.DbType = DbType.String;
			paramMaster_account.ParameterName = "@master_account";
			paramMaster_account.Value = master_account;			
			dbcmd.Parameters.Add(paramMaster_account);

			IDbDataParameter paramRef_date_no = dbcmd.CreateParameter();
			paramRef_date_no.DbType = DbType.Int16;
			paramRef_date_no.ParameterName = "@ref_date_no";
			paramRef_date_no.Value = ref_date_no;			
			dbcmd.Parameters.Add(paramRef_date_no);

			IDbDataParameter paramIsSummary = dbcmd.CreateParameter();
			paramIsSummary.DbType = DbType.Boolean;
			paramIsSummary.ParameterName = "@isSummary";
			paramIsSummary.Value = isSummary;			
			dbcmd.Parameters.Add(paramIsSummary);
			
			IDbDataParameter paramIsReport = dbcmd.CreateParameter();
			paramIsReport.DbType = DbType.Boolean;
			paramIsReport.ParameterName = "@isReport";
			paramIsReport.Value = isReport;			
			dbcmd.Parameters.Add(paramIsReport);

			try
			{
				dsCreditAging = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("ReportDAL.cs","GetTopNPayer","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}
			return dsCreditAging;
		}

		public static DataSet GetAutomatedReport(string strAppID, string strEnterpriseID, DataSet dsQuery)
		{
			string reportType = string.Empty;
			string dateType = string.Empty;
			string dateFrom = string.Empty;
			string dateTo = string.Empty;
			string payerType = string.Empty;
			string payerId = string.Empty;
			string zipcodeFROM = string.Empty;

			DataSet dsResult = new DataSet();

			DataRow dr = dsQuery.Tables[0].Rows[0];

			if (Utility.IsNotDBNull(dr["reportType"])) 
				reportType = dr["reportType"].ToString();
			if (Utility.IsNotDBNull(dr["dateType"])) 
				dateType = dr["dateType"].ToString();
			if (Utility.IsNotDBNull(dr["dateFrom"])) 
				dateFrom = dr["dateFrom"].ToString();
			if (Utility.IsNotDBNull(dr["dateTo"])) 
				dateTo = dr["dateTo"].ToString();
			if (Utility.IsNotDBNull(dr["payer_type"]))
				payerType = dr["payer_type"].ToString().Replace(@"""", "").Replace("'", "");
			if (Utility.IsNotDBNull(dr["payerId"]))
				payerId = dr["payerId"].ToString();
			if (Utility.IsNotDBNull(dr["zipcodeFROM"]))
				zipcodeFROM = dr["zipcodeFROM"].ToString();

			try
			{ 
				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid", strEnterpriseID));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid", strAppID));

				if(reportType != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@reportType", reportType));
				if(dateType != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@dateType", dateType));
				if(dateFrom != "")
				{
					string[] tempDateFrom = dateFrom.Split(' ');
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@dateFrom", tempDateFrom[0]));
				}
				if(dateTo != "")
				{
					string[] tempDateTo = dateTo.Split(' ');
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@dateTo", tempDateTo[0]));
				}
				if(payerType != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerType", payerType));
				if(payerId != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerId", payerId));
				if(zipcodeFROM != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@zipcodeFROM", zipcodeFROM));

				dsResult = (DataSet)dbCon.ExecuteProcedure("AutomatedReport", storedParams, ReturnType.DataSetType);
				return dsResult;
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "AutomatedReportDAL", "GetAutomatedReport", ex.Message);
				throw new ApplicationException("Call Stored Procedure failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 
		}

        public static DataSet GetCustomsFreightCollectMatchingReport(string strAppID, string strEnterpriseID, string strUserLoggedin , string strShipper, string strYear, string strWeek)
        {           
            string userLoggedIn = string.Empty;
            string Shipper = string.Empty;
            string Year = string.Empty;
            string Week = string.Empty;
            
            DataSet dsResult = new DataSet();
            userLoggedIn = strUserLoggedin;
            Shipper = strShipper;
            Year = strYear;
            Week = strWeek;

            try
            {
                DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
                System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
                storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid", strEnterpriseID));

                if (userLoggedIn != "")
                    storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin", userLoggedIn));
                if (Shipper != "")
                    storedParams.Add(new System.Data.SqlClient.SqlParameter("@Shipper", Shipper));               
                if (Year != "")
                    storedParams.Add(new System.Data.SqlClient.SqlParameter("@Year", Year));
                if (Week != "")
                    storedParams.Add(new System.Data.SqlClient.SqlParameter("@Week", Week));                

                dsResult = (DataSet)dbCon.ExecuteProcedure("Customs_InvoiceMatching", storedParams, ReturnType.DataSetType);
                return dsResult;
            }
            catch (Exception ex)
            {
                Logger.LogTraceError("TIESDAL", "Customs_InvoiceMatchingDAL", "GetCustomsFreightCollectMatchingReport", ex.Message);
                throw new ApplicationException("Call Stored Procedure failed");
            }
            finally
            {
                dsResult.Dispose();
            }
        }

        public static DataSet GetAutomatedReportBySpend(string strAppID, string strEnterpriseID, DataSet dsQuery)
		{
			string reportType = string.Empty;
			string dateType = string.Empty;
			string dateFrom = string.Empty;
			string dateTo = string.Empty;
			string payerType = string.Empty;
			string payerId = string.Empty;
			string zipcodeFROM = string.Empty;

			DataSet dsResult = new DataSet();

			DataRow dr = dsQuery.Tables[0].Rows[0];

			if (Utility.IsNotDBNull(dr["reportType"])) 
				reportType = dr["reportType"].ToString();
			if (Utility.IsNotDBNull(dr["dateType"])) 
				dateType = dr["dateType"].ToString();
			if (Utility.IsNotDBNull(dr["dateFrom"])) 
				dateFrom = dr["dateFrom"].ToString();
			if (Utility.IsNotDBNull(dr["dateTo"])) 
				dateTo = dr["dateTo"].ToString();
			if (Utility.IsNotDBNull(dr["payer_type"]))
				payerType = dr["payer_type"].ToString().Replace(@"""", "").Replace("'", "");
			if (Utility.IsNotDBNull(dr["payerId"]))
				payerId = dr["payerId"].ToString();
			if (Utility.IsNotDBNull(dr["zipcodeFROM"]))
				zipcodeFROM = dr["zipcodeFROM"].ToString();

			try
			{ 
				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid", strEnterpriseID));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid", strAppID));

				if(reportType != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@reportType", reportType));
				if(dateType != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@dateType", dateType));
				if(dateFrom != "")
				{
					string[] tempDateFrom = dateFrom.Split(' ');
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@dateFrom", tempDateFrom[0]));
				}
				if(dateTo != "")
				{
					string[] tempDateTo = dateTo.Split(' ');
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@dateTo", tempDateTo[0]));
				}
				if(payerType != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerType", payerType));
				if(payerId != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerId", payerId));
				if(zipcodeFROM != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@zipcodeFROM", zipcodeFROM));

				dsResult = (DataSet)dbCon.ExecuteProcedure("AutomatedReportBySpend", storedParams, ReturnType.DataSetType);
				return dsResult;
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "AutomatedReportDAL", "GetAutomatedReportBySpend", ex.Message);
				throw new ApplicationException("Call Stored Procedure failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 
		}

        public static int SaveFreightSummaryLog(String strAppID, String strEnterpriseID, string custid, string ReportDay)
        {
            DbConnection dbCon = null;
            IDbCommand dbCmd = null;
            StringBuilder strBuild = new StringBuilder();
            int iRowsAffected = 0;

            dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
            if (dbCon == null)
            {
                Logger.LogTraceError("TIESDAL", "ReportDAL", "SaveFreightSummaryLog", "ERR001", "DbConnection object is null!!");
                throw new ApplicationException("DbConnection object is null", null);
            }

            try
            {
                strBuild = new StringBuilder();

                strBuild.Append("INSERT INTO [FreightSummary_Log] ");
                strBuild.Append("(applicationid, enterpriseid, startdate, enddate, custid, status, createdate, lastupdate) ");
                strBuild.Append("VALUES ");
                strBuild.Append("('" + strAppID + "', '" + strEnterpriseID + "', convert(date,DATEADD(D,-" + ReportDay + ",GETDATE()), 103), convert(date,GETDATE(), 103) , '" + custid + "', 'E' , GETDATE(), GETDATE())");

                dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
                iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Save Default Sender", ex);
            }
            return iRowsAffected;
        }

        public static int UpdateFreightSummaryLog(String strAppID, String strEnterpriseID, string ID)
        {
            DbConnection dbCon = null;
            IDbCommand dbCmd = null;
            StringBuilder strBuild = new StringBuilder();
            int iRowsAffected = 0;

            dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
            if (dbCon == null)
            {
                Logger.LogTraceError("TIESDAL", "ReportDAL", "UpdateFreightSummaryLog", "ERR001", "DbConnection object is null!!");
                throw new ApplicationException("DbConnection object is null", null);
            }

            try
            {
                strBuild = new StringBuilder();

                strBuild.Append("UPDATE [FreightSummary_Log] SET status = 'S', lastupdate = GETDATE()");
                strBuild.Append("WHERE applicationid = '" + strAppID + "'");
                strBuild.Append("AND enterpriseid = '" + strEnterpriseID + "'");
                strBuild.Append("AND logid = '" + ID + "'");

                dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
                iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error Save Default Sender", ex);
            }
            return iRowsAffected;
        }

        public static DataSet GetLogEmailError(String strAppID, String strEnterpriseID, string custid)
        {
            DbConnection dbCon = null;
            IDbCommand dbcmd = null;
            DataSet dsReturn = null;

            string strSQL = "";

            DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(strAppID, strEnterpriseID);
            dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);

            strSQL = @"select * from [FreightSummary_Log] where status = 'E' 
                       and applicationid = '" + strAppID + "' and enterpriseid = '" + strEnterpriseID + "' and custid = '" + custid + "'";

            dbcmd = dbCon.CreateCommand(strSQL, CommandType.Text);
            dbcmd.CommandTimeout = 1500;

            try
            {
                dsReturn = (DataSet)dbCon.ExecuteQuery(dbcmd, com.common.DAL.ReturnType.DataSetType);
            }
            catch (ApplicationException appException)
            {
                Logger.LogTraceError("ReportDAL.cs", "GetLogEmailError", "ERR002", "Error in Query String " + appException.Message);
                throw appException;
            }

            return dsReturn;
        }

        public static DataSet GetFreightSummaryByParam(DataRow dr)
        {
            DbConnection dbCon = null;
            IDbCommand dbcmd = null;
            DataSet dsReturn = null;

            string strSQL = "";

            DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType(dr["applicationid"].ToString(), dr["enterpriseid"].ToString());
            dbCon = DbConnectionManager.GetInstance().GetDbConnection(dr["applicationid"].ToString(), dr["enterpriseid"].ToString());

            strSQL = @"SELECT s.shpt_manifest_datetime,
							s.consignment_no, 
							s.sender_zipcode as 'origin_station',
							s.recipient_zipcode as 'destination_station',
							s.tot_act_wt,
							(
								SELECT CAST(ROUND(increment_price * (CASE WHEN c.payer_type='F' THEN 0.85 ELSE 1 END), 2) AS decimal(18,2))
									FROM dbo.base_zone_rates 
									WHERE applicationid = '" + dr["applicationid"].ToString() + @"'
									AND enterpriseid = '" + dr["enterpriseid"].ToString() + @"' 
									AND origin_zone_code = s.origin_station 
									AND destination_zone_code = s.destination_station
									AND service_code = s.service_code
									AND s.tot_act_wt >= start_wt AND s.tot_act_wt < end_wt
									AND effective_date = (SELECT MAX(effective_date) 
																		FROM dbo.base_zone_rates
																		WHERE applicationid = '" + dr["applicationid"].ToString() + @"'
																		and enterpriseid = '" + dr["enterpriseid"].ToString() + @"' 
																		AND origin_zone_code = s.origin_station 
																		AND destination_zone_code = s.destination_station
																		AND service_code = s.service_code
																		AND s.tot_act_wt >= start_wt AND s.tot_act_wt < end_wt
																		AND effective_date <= s.shpt_manifest_datetime
																		)
								) AS 'rate',
							s.basic_charge,
							s.tot_freight_charge,
							s.other_surch_amount,
							s.tot_vas_surcharge,
							s.tax_on_rated_amount as 'tax',
							s.MasterAWBNumber,
							ISNULL(s.payerid2, s.payerid) as 'payerid',
							c.cust_name,
							s.total_rated_amount,
							invoice_amt=CASE WHEN b.ConsignmentType='D' THEN ISNULL(s.total_rated_amount, 0) + ISNULL(s.tax_on_rated_amount, 0)
                                             WHEN b.ConsignmentType='E' THEN ISNULL(s.total_rated_amount, 0) + ISNULL(s.tax_on_rated_amount, 0) + ISNULL(s.export_freight_charge, 0)
                                             WHEN b.ConsignmentType='I' AND ISNULL(s.invoice_amt,0) > 0 THEN ISNULL(s.invoice_amt,0) END,
                            'M' mode,'" + Convert.ToDateTime(dr["startdate"]).ToString("yyyy-MM-dd") + @"' dateFrom, DATEADD(D,-1,'" + Convert.ToDateTime(dr["enddate"]).ToString("yyyy-MM-dd") + @"') dateTo, 
                            cs.code_text, cost_centre=CASE WHEN ISNULL(r.cost_centre,'')='' THEN '-' ELSE r.cost_centre END, s.service_code 
						FROM dbo.Shipment s CROSS APPLY dbo.GetConsignmentType(s.enterpriseid, s.export_flag, s.sender_name, s.sender_zipcode, s.recipient_zipcode) b
						LEFT JOIN dbo.Customer c  ON ISNULL(s.payerid2, s.payerid) = custid AND s.enterpriseid = c.enterpriseid AND s.applicationid = c.applicationid 
						LEFT JOIN v_References r ON s.applicationid = r.applicationid AND s.enterpriseid = r.enterpriseid AND s.recipient_telephone = r.telephone  ";
            strSQL += "INNER JOIN dbo.Core_System_Code cs ON cs.applicationid = '" + dr["applicationid"].ToString() + "' and cs.culture = 'en-US' AND codeid = 'customer_type' AND cs.code_str_value = c.payer_type ";
            strSQL += "WHERE s.sender_zipcode <> s.recipient_zipcode AND s.applicationid = '" + dr["applicationid"].ToString() + "' ";
            strSQL += "AND s.enterpriseid = '" + dr["enterpriseid"].ToString() + "' ";
            strSQL += "AND c.custid = '" + dr["custid"].ToString() + "' ";
            strSQL += "AND convert(date,s.shpt_manifest_datetime, 103) >= '" + Convert.ToDateTime(dr["startdate"]).ToString("yyyy-MM-dd") + "' ";
            strSQL += "AND convert(date,s.shpt_manifest_datetime, 103) < '" + Convert.ToDateTime(dr["enddate"]).ToString("yyyy-MM-dd") + "' ";
            strSQL += "Order By s.shpt_manifest_datetime, s.consignment_no ";

            dbcmd = dbCon.CreateCommand(strSQL, CommandType.Text);
            dbcmd.CommandTimeout = 1500;

            try
            {
                dsReturn = (DataSet)dbCon.ExecuteQuery(dbcmd, com.common.DAL.ReturnType.DataSetType);
            }
            catch (ApplicationException appException)
            {
                Logger.LogTraceError("ReportDAL.cs", "GetTopNPayer", "ERR002", "Error in Query String " + appException.Message);
                throw appException;
            }

            return dsReturn;
        }

        public static DataSet GetShipmentTrackingData(DataRow dr)
        {
            DbConnection dbCon = null;
            IDbCommand dbcmd = null;
            DataSet dsReturn = null;

            string strSQL = "";

            DataProviderType dbProvider = DbConnectionManager.GetInstance().GetDbProviderType("TIES", "PNGAF");
            dbCon = DbConnectionManager.GetInstance().GetDbConnection("TIES", "PNGAF");

            strSQL = @"SELECT * FROM v_ShipmentTrackingReport WHERE 1=1 ";

            // Booking Date / Actual Pickup / Status Date
            if (dr["tran_date"].ToString() == "B")
                strSQL += "AND (booking_datetime >= '" + Convert.ToDateTime(dr["start_date"]).ToString("yyyy-MM-dd") + " 00:00:00' AND booking_datetime < '" + Convert.ToDateTime(dr["end_date"]).ToString("yyyy-MM-dd") + " 23:59:01') ";
            if (dr["tran_date"].ToString() == "P")
                strSQL += "AND (act_pickup_datetime >= '" + Convert.ToDateTime(dr["start_date"]).ToString("yyyy-MM-dd") + " 00:00:00' AND act_pickup_datetime < '" + Convert.ToDateTime(dr["end_date"]).ToString("yyyy-MM-dd") + " 23:59:01') ";
            if (dr["tran_date"].ToString() == "S")
                strSQL += "AND (last_status_datetime >= '" + Convert.ToDateTime(dr["start_date"]).ToString("yyyy-MM-dd") + " 00:00:00' AND last_status_datetime < '" + Convert.ToDateTime(dr["end_date"]).ToString("yyyy-MM-dd") + " 23:59:01') ";

            // Payer Code
            if (Utility.IsNotDBNull(dr["payer_code"]) && dr["payer_code"].ToString() != "")
                strSQL += "AND payerid = '" + dr["payer_code"].ToString() + "' ";

            // Consignment No
            if (Utility.IsNotDBNull(dr["consignment_no"]) && dr["consignment_no"].ToString() != "")
                strSQL += "AND consignment_no = '" + dr["consignment_no"].ToString() + "' ";

            dbcmd = dbCon.CreateCommand(strSQL, CommandType.Text);
            dbcmd.CommandTimeout = 1500;

            try
            {
                dsReturn = (DataSet)dbCon.ExecuteQuery(dbcmd, com.common.DAL.ReturnType.DataSetType);
            }
            catch (ApplicationException appException)
            {
                Logger.LogTraceError("ReportDAL.cs", "GetShipmentTrackingData", "ERR002", "Error in Query String " + appException.Message);
                throw appException;
            }

            return dsReturn;
        }
    }
}
