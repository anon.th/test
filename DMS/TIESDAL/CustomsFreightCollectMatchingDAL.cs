using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;
using System.Collections;
using System.Text.RegularExpressions;
using TIESClasses;

namespace TIESDAL
{
	/// <summary>
	/// Summary description for CustomsFreightCollectMatchingDAL.
	/// </summary>
	public class CustomsFreightCollectMatchingDAL
	{
		public CustomsFreightCollectMatchingDAL()
		{

		}

		public static System.Data.DataSet Customs_Query_FCInvoices (string strAppID, string strEnterpriseID, string strUserloggedin, string shipper, string year, string weekNo, string debug)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsFreightCollectMatchingDAL","Customs_Query_FCInvoices","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{ 
				StringBuilder sbQry = new StringBuilder();
				sbQry.Append("DECLARE @RowsReturned INT ");
				sbQry.Append("EXEC dbo.Customs_Query_FCInvoices ");
				sbQry.Append("@enterpriseid = '{0}' ");
				sbQry.Append(",@userloggedin = '{1}' ");
				
				if(shipper != null && shipper != string.Empty)
				{
					sbQry.Append(",@Shipper = '{2}' ");
				}
				else
				{
					sbQry.Append(",@Shipper = '{2}' ");
				}

				if(year != null && year != string.Empty)
				{
					sbQry.Append(",@Year = '{3}' ");
				}
				else
				{
					sbQry.Append(",@Year = NULL ");
				}
				
				if(weekNo != null && weekNo != string.Empty)
				{
					sbQry.Append(",@WeekNo = '{4}' ");
				}
				else
				{
					sbQry.Append(",@WeekNo = NULL ");
				}

				sbQry.Append(",@ReturnedRows = @RowsReturned OUTPUT ");
				sbQry.Append(",@Debug = {5} ");
				sbQry.Append("SELECT @RowsReturned ");

				string strQry = string.Format(sbQry.ToString(), strEnterpriseID, strUserloggedin, shipper, year, weekNo, debug);

				dbCmd = dbCon.CreateCommand(strQry, CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsFreightCollectMatchingDAL", "Customs_Query_FCInvoices", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			}
		}

		public static System.Data.DataSet Customs_Import_FCInvoice3(string strAppID, string strEnterpriseID, string strUserloggedin, string shipper, string year, string weekNo, string fileName,int fileMode,string textString)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsFreightCollectMatchingDAL","Customs_Import_FCInvoice3","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{ 
				StringBuilder sbQry = new StringBuilder();
				sbQry.Append("EXEC dbo.Customs_Import_FCInvoice3 ");
				sbQry.Append("@enterpriseid = '{0}' ");
				sbQry.Append(",@userloggedin = '{1}' ");
				sbQry.Append(",@Shipper = '{2}'");

				if(year != null && year != string.Empty)
				{
					sbQry.Append(",@Year = '{3}' ");
				}
				else
				{
					sbQry.Append(",@Year = NULL ");
				}
				
				if(weekNo != null && weekNo != string.Empty)
				{
					sbQry.Append(",@WeekNo = '{4}' ");
				}
				else
				{
					sbQry.Append(",@WeekNo = NULL ");
				}

				if(fileName != null && fileName != string.Empty)
				{
					sbQry.Append(",@FileName = '{5}'");
				}
				else
				{
					sbQry.Append(",@FileName = NULL");
				}
				
				sbQry.Append(",@FileMode = {6}");

				if(fileMode==1)
				{
					sbQry.Append(",@TextString = N'{7}'");
				}
				else
				{
					sbQry.Append(",@TextString = NULL ");
				}

				string strQry = string.Format(sbQry.ToString(), strEnterpriseID, strUserloggedin, shipper, year, weekNo, fileName,fileMode,textString);

				dbCmd = dbCon.CreateCommand(strQry, CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsFreightCollectMatchingDAL", "Customs_Import_FCInvoice3", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			}
		}



		public static System.Data.DataSet Customs_InvoiceMatching (string strAppID, string strEnterpriseID, string strUserloggedin, string shipper, string year, string weekNo)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsFreightCollectMatchingDAL","Customs_Import_FCInvoice3","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{ 
				StringBuilder sbQry = new StringBuilder();
				sbQry.Append("EXEC dbo.Customs_InvoiceMatching ");
				sbQry.Append("@enterpriseid = '{0}' ");
				sbQry.Append(",@userloggedin = '{1}' ");
				sbQry.Append(",@Shipper = '{2}'");

				if(year != null && year != string.Empty)
				{
					sbQry.Append(",@Year = '{3}' ");
				}
				else
				{
					sbQry.Append(",@Year = NULL ");
				}
				
				if(weekNo != null && weekNo != string.Empty)
				{
					sbQry.Append(",@WeekNo = '{4}' ");
				}
				else
				{
					sbQry.Append(",@WeekNo = NULL ");
				}

				string strQry = string.Format(sbQry.ToString(), strEnterpriseID, strUserloggedin, shipper, year, weekNo);

				dbCmd = dbCon.CreateCommand(strQry, CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsFreightCollectMatchingDAL", "Customs_Import_FCInvoice3", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			}
		}
	}
}
