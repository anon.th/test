using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;

namespace TIESDAL
{
	/// <summary>
	/// Summary description for UpliftStatisticsDAL.
	/// </summary>
	public class UpliftStatisticsDAL
	{
		public UpliftStatisticsDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static DataSet GetUpliftStatistics(string strAppID, 
												  string strEnterpriseID, 
												  string reportType,
												  string dateType,
												  string dateFrom,
												  string dateTo,
												  string payerType,
												  string payerId,
												  string zipcodeFROM)
		{
			DataSet dsResult = new DataSet();
			try
			{ 
				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid", strEnterpriseID));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid", strAppID));

				if(reportType != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@reportType", reportType));
				if(dateType != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@dateType", dateType));
				if(dateFrom != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@dateFrom", dateFrom));
				if(dateTo != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@dateTo", dateTo));
				if(dateTo != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerType", payerType));
				if(payerId != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerId", payerId));
				if(zipcodeFROM != "")
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@zipcodeFROM", zipcodeFROM));

				dsResult = (DataSet)dbCon.ExecuteProcedure("UpliftStatistics", storedParams, ReturnType.DataSetType);
				return dsResult;
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "UpliftStatisticsDAL", "GetUpliftStatistics", ex.Message);
				throw new ApplicationException("Call Stored Procedure failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 
		}
	}
}
