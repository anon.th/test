﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Data;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using System.Threading;
using TIESClasses;

namespace TIESDAL
{
    public class ReportPODSummaryDAL
    {
        private string SQLText = string.Empty;
        private DbConnection dbCon;
        private IDbCommand dbCmd = null;
        public ReportPODSummaryDAL(string _appID, string _enterpriseID)
        {
            dbCon = DbConnectionManager.GetInstance().GetDbConnection(_appID, _enterpriseID);
        }



        public DataSet GetPODSummary(string enterpriseID, string fromYear, string toYears, string payerCode, string custType)
        {
            DataSet dsReturn = new DataSet();
            StringBuilder SQL = new StringBuilder();
            if (int.Parse(fromYear) > int.Parse(toYears))
            {
                ChangeValues(ref fromYear, ref toYears);
            }
            SQL.AppendLine("SELECT");
            SQL.AppendLine(" v_shipment_pod.applicationid,v_shipment_pod.enterpriseid, LEFT(");
            SQL.AppendLine("    CONVERT(");
            SQL.AppendLine("      VARCHAR");
            SQL.AppendLine("     ,v_shipment_pod.act_delivery_date");
            SQL.AppendLine("     ,112)");
            SQL.AppendLine("   ,6)");
            SQL.AppendLine("    AS yyyyMM");
            SQL.AppendLine(" ,year(v_shipment_pod.act_delivery_date) AS yy");
            SQL.AppendLine(" ,month(v_shipment_pod.act_delivery_date) AS mm");
            SQL.AppendLine(" ,(sum(v_shipment_pod.IsInTime)+sum(v_shipment_pod.IsDelayed)) AS total");
            SQL.AppendLine(" ,sum(v_shipment_pod.IsInTime) AS sumInTime");
            SQL.AppendLine(" ,sum(v_shipment_pod.IsDelayed) AS sumDelayed");

            SQL.AppendLine(" ,sum(v_shipment_pod.tot_act_wt) AS totalWT, sum(CASE WHEN IsInTime = 1 THEN v_shipment_pod.tot_act_wt ELSE 0 END) AS IntimeWT");
            SQL.AppendLine(" ,sum(CASE WHEN IsDelayed = 1 THEN v_shipment_pod.tot_act_wt ELSE 0 END) AS delayedWT ");
            SQL.AppendLine(" FROM ");
            SQL.AppendLine(" v_shipment_pod v_shipment_pod");
            SQL.AppendLine("              INNER JOIN  Customer Customer");
            SQL.AppendLine("                ON (Customer.custid = v_shipment_pod.payerid)");

            SQL.AppendFormat("  Where LEFT(CONVERT(varchar, v_shipment_pod.act_delivery_date, 112),6) < LEFT(CONVERT(varchar, GETDATE(), 112),6) AND ");
            SQL.AppendFormat("Year(v_shipment_pod.act_delivery_date) between '{0}' and '{1}'", fromYear, toYears);

            if (!string.IsNullOrEmpty(payerCode) || !string.IsNullOrEmpty(custType))
            {
                SQL.Append("  and   v_shipment_pod.consignment_no <> '' ");

            }
            if (!string.IsNullOrEmpty(payerCode))
            {
                SQL.AppendFormat(" and v_shipment_pod.payerid = '{0}'", payerCode);
            }

            if (!string.IsNullOrEmpty(custType))
            {
                SQL.AppendFormat(" and Customer.payer_type = '{0}'", custType);
            }
            SQL.AppendLine(" GROUP BY v_shipment_pod.applicationid,v_shipment_pod.enterpriseid,");
            SQL.AppendLine("  LEFT(");
            SQL.AppendLine("    CONVERT(");
            SQL.AppendLine("      VARCHAR");
            SQL.AppendLine("     ,v_shipment_pod.act_delivery_date");
            SQL.AppendLine("     ,112)");
            SQL.AppendLine("   ,6)");
            SQL.AppendLine(" ,year(v_shipment_pod.act_delivery_date)");
            SQL.AppendLine(" ,month(v_shipment_pod.act_delivery_date)");
            SQL.AppendLine(" ORDER BY");
            SQL.AppendLine("  year(v_shipment_pod.act_delivery_date)");
            SQL.AppendLine(" ,month(v_shipment_pod.act_delivery_date)");

            try
            {
                dsReturn = (DataSet)dbCon.ExecuteQuery(SQL.ToString(), ReturnType.DataSetType);
                return dsReturn;
            }
            catch (Exception ex)
            {
                string msgException = string.Format("Method GetPODSummary : {0}|{1}", ex.Message, ex.InnerException);
                Logger.LogTraceError("TIESDAL", "ReportPODSummaryDAL", "GetPODSummary", msgException);
                throw new ApplicationException(msgException);
            }
            finally
            {
                dsReturn.Dispose();
            }
        }
        public static void ChangeValues(ref string str1, ref string str2)
        {
            var temp = str1;
            str1 = str2;
            str2 = temp;
        }
    }



}
