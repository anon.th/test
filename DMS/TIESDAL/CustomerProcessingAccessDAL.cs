using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using TIESClasses;

namespace TIESDAL
{
	public class CustomerProcessingAccessDAL
	{
		private string SQLText = string.Empty;
		private DbConnection dbCon;
		private IDbCommand dbCmd = null;

		public CustomerProcessingAccessDAL(string _appID, string _enterpriseID) 
		{ 
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(_appID, _enterpriseID);
		}

		public DataSet ExecCSS_CustomerProcessingAccess(CustomerProcessingAccess objInfo)
		{
			DataSet dsReturn = new DataSet();
			try
			{
				if(objInfo.Action.Trim().Equals(""))
					throw new ApplicationException("Please specify Action");
				if(objInfo.EnterpriseId.Trim().Equals(""))
					throw new ApplicationException("Please specify EnterpriseId");

				SQLText = @"DECLARE @ReturnedRows INT;
							EXEC dbo.CSS_CustomerProcessingAccess @action = {0},
														          @enterpriseid = '{1}'";
				SQLText = string.Format(SQLText, 
					objInfo.Action.Trim(),
					objInfo.EnterpriseId.Trim());
				
				if(!objInfo.UserId.Trim().Equals(""))
					SQLText += string.Format(", @userid = '{0}'", objInfo.UserId.Trim());
				if(!objInfo.CustId.Trim().Equals(""))
					SQLText += string.Format(", @custid = '{0}'", objInfo.CustId.Trim());
				
				SQLText += @", @ReturnedRows = @ReturnedRows OUTPUT; 
							 SELECT @ReturnedRows;";

				dbCmd = dbCon.CreateCommand(SQLText, CommandType.Text);
				dsReturn = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method ExecCustomsExchangeRates : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CustomerProcessingAccessDAL", "ExecCSS_CustomerProcessingAccess", msgException);
				throw new ApplicationException(msgException);
			}
		}
	}
}
