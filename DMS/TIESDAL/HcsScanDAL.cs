using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for HcsScanDAL.
	/// </summary>
	public class HcsScanDAL
	{
		public HcsScanDAL()
		{
		}

		
		public static SessionDS GetEmptyHcs()
		{
			DataSet  dsHCS = new DataSet();
			DataTable dtHCS =  new DataTable();
			
			dtHCS.Columns.Add(new DataColumn("Hcs_No",typeof(string)));
			dtHCS.Columns.Add(new DataColumn("Consignment_No",typeof(string)));
			dtHCS.Columns.Add(new DataColumn("ScanDatetime",typeof(DateTime))); 
			dtHCS.Columns.Add(new DataColumn("ScanUserId",typeof(string)));
			dtHCS.Columns.Add(new DataColumn("IsInvr",typeof(string))); 
			dtHCS.Columns.Add(new DataColumn("InvrRemark",typeof(string)));
			
			dsHCS.Tables.Add(dtHCS); 

			dsHCS.Tables[0].Columns["Hcs_No"].AllowDBNull = false;
			dsHCS.Tables[0].Columns["Consignment_No"].AllowDBNull = false;
			dsHCS.Tables[0].Columns["ScanDatetime"].AllowDBNull = false;
			dsHCS.Tables[0].Columns["ScanUserId"].AllowDBNull = false;

			SessionDS sessionds = new SessionDS();
			sessionds.ds = dsHCS;
				
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;
			return  sessionds;

		}

		
		public static SessionDS GetEmptyHcsError()
		{
			DataSet  dsHcsError = new DataSet();
			DataTable dtHcsError =  new DataTable();
			
			dtHcsError.Columns.Add(new DataColumn("Hcs_No",typeof(string)));
			dtHcsError.Columns.Add(new DataColumn("Consignment_No",typeof(string)));
			dtHcsError.Columns.Add(new DataColumn("ScanDatetime",typeof(DateTime))); 
			dtHcsError.Columns.Add(new DataColumn("ScanUserId",typeof(string)));
			dtHcsError.Columns.Add(new DataColumn("IsDup",typeof(string))); 
			dtHcsError.Columns.Add(new DataColumn("IsNoPod",typeof(string)));
			dtHcsError.Columns.Add(new DataColumn("IsHcr",typeof(string)));
			
			dsHcsError.Tables.Add(dtHcsError); 

			dsHcsError.Tables[0].Columns["Hcs_No"].AllowDBNull = false;
			dsHcsError.Tables[0].Columns["Consignment_No"].AllowDBNull = false;
			dsHcsError.Tables[0].Columns["ScanDatetime"].AllowDBNull = false;
			dsHcsError.Tables[0].Columns["ScanUserId"].AllowDBNull = false;

			SessionDS sessionds = new SessionDS();
			sessionds.ds = dsHcsError;
				
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;
			return  sessionds;
		}


		public static SessionDS GetHcsDS(string strAppId, string strEntId, int iCurRow, int iDSRowSize, string strHcsNo)
		{
			SessionDS sessionDS = null;

			string strSQLQuery = "";

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId,strEntId);

			if(dbCon == null)
			{
				Logger.LogTraceError("HcsScanDAL","GetHcsDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			
			strSQLQuery = "SELECT hcs_no, consignment_no, scandatetime, scanuserid, isinvr, invrremark ";
			strSQLQuery += " FROM HCS_DT ";
			strSQLQuery += " WHERE ApplicationId = '" + strAppId + "'  AND enterpriseid = '" + strEntId + "'"; 

			if (strHcsNo != null)
				strSQLQuery += " AND hcs_no = '" + strHcsNo + "' ";

			strSQLQuery += " Order by scandatetime desc ";
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery, iCurRow, iDSRowSize, "HCS_DT");

			return  sessionDS;
		}


		public static SessionDS GetHcsErrorDS(string strAppId, string strEntId, int iCurRow, int iDSRowSize, string strHcsNo)
		{
			SessionDS sessionDS = null;

			string strSQLQuery = "";
					
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId,strEntId);

			if(dbCon == null)
			{
				Logger.LogTraceError("HcsScanDAL","GetHcsErrorDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			
			strSQLQuery = "SELECT hcs_no, consignment_no, scandatetime, scanuserid, isdup, isnopod, ishcr ";
			strSQLQuery += " FROM HCS_ERROR ";
			strSQLQuery += " WHERE ApplicationId = '" + strAppId + "'  AND enterpriseid = '" + strEntId + "'"; 

			if (strHcsNo != null)
				strSQLQuery += " AND hcs_no = '" + strHcsNo + "' ";

			strSQLQuery += " ORDER BY scandatetime desc ";
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery, iCurRow, iDSRowSize, "HCS_ERROR");

			return  sessionDS;
		}

	
		public static int GetOpenHcsNumber(string strAppId, string strEntId, string strLocation, string strSubDc)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet dsResult = null;
			
			string strQry = null;
			string strDay = System.DateTime.Now.Day.ToString();
			string strMonth = System.DateTime.Now.Month.ToString();
			string strYear = System.DateTime.Now.Year.ToString();
			int iRecCnt = 0;

				
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId,strEntId);  

			if(dbCon == null)
			{
				Logger.LogTraceError("HcsScanDAL","GetHcsNumber","D001","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
					 
			strQry = "SELECT COUNT(*) FROM HCS_HD ";
			strQry += " WHERE applicationid = '" + strAppId + "' AND enterpriseid = '" + strEntId + "' ";
			strQry += " AND DAY(hcs_date) = " + strDay + " ";
			strQry += " AND MONTH(hcs_date) = " + strMonth + " ";
			strQry += " AND YEAR(hcs_date) = " + strYear + " ";
			strQry += " AND (isclosed != 'Y' OR isclosed IS NULL) ";
			strQry += " AND location = '" + strLocation + "' ";
			strQry += " AND subdc_code = '" + strSubDc + "' ";

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);									  
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("HcsScanDAL","GetHcsNumber","D002","Error in the query String");
				throw appExpection;
			}

			DataRow drEach = dsResult.Tables[0].Rows[0] ;
			iRecCnt = (int)drEach[0]; 

			return iRecCnt; 
		}
		

		public static int HcsHdInsert(string strAppId, string strEntId, string strHcsNumber, string strUserId, string strLocation, string strSubDc)
		{
			int iRowsInserted = 0;
			IDbCommand dbCmd = null;
			DataSet dsResult = null;
			string strQry = "";
			string strHcsPostfix = "";
			string strHcsNo = "";
			string strHcsPostfix_ins = "";
		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId, strEntId);

			if(dbCon == null)
			{
				Logger.LogTraceError("HcsScanDAL","HcsHdInsert","D001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				if(strHcsNumber != null && strHcsNumber != "")
				{
					///---- Find Max HcsPostfix
					strQry = "SELECT MAX(hcspostfix) FROM HCS_HD ";
					strQry += " WHERE applicationid = '" + strAppId + "' AND enterpriseid = '" + strEntId + "' ";
					strQry += " AND hcsnumber = '" + strHcsNumber + "' ";

					dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
					try
					{
						dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);									  
					}
					catch(ApplicationException appExpection)
					{
						Logger.LogTraceError("HcsScanDAL","HcsHdInsert","D002","Error in the query String");
						throw appExpection;
					}

					DataRow drEach = dsResult.Tables[0].Rows[0] ;

					strHcsPostfix = (drEach[0]==System.DBNull.Value)? "" : (string)drEach[0];


					if (strHcsPostfix != null && strHcsPostfix != "")
					{
						strHcsPostfix_ins = Convert.ToString((Convert.ToInt32(strHcsPostfix) + 1));
						strHcsNo = strHcsNumber + "-" + strHcsPostfix_ins;
					}
					else
					{
						strHcsPostfix_ins = "0";
						strHcsNo = strHcsNumber;
					}					

					////--- Insert data to HCS_HD
					strQry = "INSERT INTO HCS_HD ";
					strQry += " (APPLICATIONID, ENTERPRISEID, HCS_NO, HCS_DATE, ";
					strQry += "  HCSNUMBER, HCSPOSTFIX, LOCATION, SUBDC_CODE, ";
					strQry += "  CRT_USERID, CRT_DATETIME) ";
					strQry += " VALUES ('" + strAppId + "','" + strEntId + "','" + strHcsNo + "',Getdate(),";
					strQry += " '" + strHcsNumber + "','" + strHcsPostfix_ins + "','" + strLocation + "','" + strSubDc + "',";
					strQry += " '" + strUserId + "',Getdate())";
				
					dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);

					iRowsInserted = dbCon.ExecuteNonQuery(dbCmd);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("HcsScanDAL","HcsHdInsert","D003",appException.Message.ToString());
				throw new ApplicationException("Insert operation failed"+appException.Message,appException);
			}
			return iRowsInserted;
		}


		public static int HcsHdDelete(string strAppId, string strEntId, string strHcsNo)
		{
			int iRowsDeleted = 0;
			IDbCommand dbCom = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId, strEntId);

			if(dbCon == null)
			{
				Logger.LogTraceError("HcsScanDAL", "Hcs_Hd_Delete", "DPC001", "dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				if(strHcsNo != null && strHcsNo != "")
				{
					string strSQLQuery;

					strSQLQuery = "DELETE FROM HCS_ERROR ";
					strSQLQuery += " WHERE applicationid = " + "'" + strAppId + "' AND enterpriseid = '" + strEntId + "' ";
					strSQLQuery += " AND hcs_no = '" + strHcsNo + "'";
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);

					
					if (iRowsDeleted == 0 || iRowsDeleted > 0)
					{
						iRowsDeleted = 0;

						strSQLQuery = "DELETE FROM HCS_HD ";
						strSQLQuery += " WHERE applicationid = " + "'" + strAppId + "' AND enterpriseid = '" + strEntId + "' ";
						strSQLQuery += " AND hcs_no = '" + strHcsNo + "'";
	
						dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
						iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);
					}
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("HcsScanDAL", "Hcs_Hd_Delete", "DPC002", appException.Message.ToString());
			
				throw new ApplicationException("Delete operation failed" + appException.Message,appException);
			}
			return iRowsDeleted;
		}		
	

		public static int HcsDtInsert(string strAppId, string strEntId, string strHcsNo, string strBarCode, string strScanUserId)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId, strEntId);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();

			storedParams.Add(new System.Data.SqlClient.SqlParameter("@AppID", strAppId));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@EntID", strEntId));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@HcsNo", strHcsNo));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@BarCode", strBarCode));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@ScanUserID", strScanUserId));

			int result = (int)dbCon.ExecuteProcedure("sp_HCS_Dt_Insert",storedParams);
			return result;
		}

	
		public static int HcsDtUpdate(string strAppId, string strEntId, DataSet dsHCS)
		{
			int iRowsUpdated = 0;
			string strSQLQuery = null;
			IDbCommand dbCom = null;
			DataRow dr = dsHCS.Tables[0].Rows[0];

			string strHcsNo = (string)dr["Hcs_No"];
			string strConsNo = (string)dr["Consignment_No"];
			string strScanUserId = (string)dr["ScanUserId"];
			string strInvrRemark = (string)dr["InvrRemark"];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId, strEntId);

			if(dbCon == null)
			{
				Logger.LogTraceError("HcsScanDAL", "Hcs_Dt_Update", "DP004", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null);
			}

			try
			{
				if(strHcsNo != null && strHcsNo != "" && strConsNo != null && strConsNo != "")
				{
					strSQLQuery = "UPDATE HCS_DT ";
					strSQLQuery += " SET InvrRemark = N'" + Utility.ReplaceSingleQuote(strInvrRemark) + "'";				
					strSQLQuery += " WHERE applicationid = '" + strAppId + "' AND enterpriseid = '" + strEntId + "'";
					strSQLQuery += " AND hcs_no = '" + strHcsNo + "'"; 
					strSQLQuery += " AND consignment_no = '" + strConsNo + "'"; 
					strSQLQuery += " AND barcode = '" + strConsNo + "'"; 
	
					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsUpdated = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("HcsScanDAL", "Hcs_Dt_Update", "DP005", appException.Message.ToString());
				throw new ApplicationException("Update operation failed", appException);				
			}
			return iRowsUpdated;
		}
		

		public static int HcsDtDelete(string strAppId, string strEntId, DataSet dsHcs, int rowIndex)
		{
			int iRowsDeleted = 0;
			IDbCommand dbCom = null;
			DataRow dr = dsHcs.Tables[0].Rows[rowIndex];

			string strHcsNo = (string)dr["Hcs_No"];	
			string strConsNo = (string)dr["Consignment_No"];

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId,strEntId);

			if(dbCon == null)
			{
				Logger.LogTraceError("HcsScanDAL", "Hcs_Dt_Delete", "DPC001", "dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			try
			{
				if(strHcsNo != null && strHcsNo != "" && strConsNo != null && strConsNo != "")
				{

					string strSQLQuery = "DELETE FROM HCS_DT ";
					strSQLQuery += " WHERE applicationid = " + "'" + strAppId + "' AND enterpriseid = '" + strEntId + "' ";
					strSQLQuery += " AND hcs_no = '" + strHcsNo + "'";
					strSQLQuery += " AND consignment_no = '" + strConsNo + "'";
					strSQLQuery += " AND barcode = '" + strConsNo + "'"; 

					dbCom = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
					iRowsDeleted = dbCon.ExecuteNonQuery(dbCom);
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("HcsScanDAL", "Hcs_Dt_Delete", "DPC002", appException.Message.ToString());
			
				throw new ApplicationException("Delete operation failed" + appException.Message,appException);
			}
			return iRowsDeleted;
		}		
	

		public static string HcsClosed(string strAppId, string strEntId, string strHcsNo, string strUserId, string strLocation)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId, strEntId);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();

			storedParams.Add(new System.Data.SqlClient.SqlParameter("@AppID", strAppId));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@EntID", strEntId));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@HcsNo", strHcsNo));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@UserID", strUserId));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Location", strLocation));

			System.Data.SqlClient.SqlParameter paraOutRet = new System.Data.SqlClient.SqlParameter("@OutRet", SqlDbType.NVarChar); 
			paraOutRet.Size = 100;
			paraOutRet.Direction = ParameterDirection.Output; 
			storedParams.Add(paraOutRet);

			int result = (int)dbCon.ExecuteProcedure("sp_HCS_Closed",storedParams);
			return Convert.ToString(paraOutRet.Value);

//			return result;
		}


		public static string HcsCancelClosed(string strAppId, string strEntId, string strHcsNo, string strUserId, string strLocation)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppId, strEntId);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();

			storedParams.Add(new System.Data.SqlClient.SqlParameter("@AppID", strAppId));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@EntID", strEntId));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@HcsNo", strHcsNo));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@UserID", strUserId));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Location", strLocation));

			System.Data.SqlClient.SqlParameter paraOutRet = new System.Data.SqlClient.SqlParameter("@OutReturn", SqlDbType.NVarChar); 
			paraOutRet.Size = 100;
			paraOutRet.Direction = ParameterDirection.Output; 
			storedParams.Add(paraOutRet);

			int result = (int)dbCon.ExecuteProcedure("sp_HCS_Cancel_Closed",storedParams);
			return Convert.ToString(paraOutRet.Value);

//			return result;
		}


		public static void AddNewRowHcsDS(ref SessionDS dsHcsCode)
		{
			DataRow drNew = dsHcsCode.ds.Tables[0].NewRow();

			drNew["Hcs_No"] = "";
			drNew["Consignment_No"] = "";
			drNew["ScanDatetime"] = DateTime.Now;
			drNew["ScanUserId"] = "";
			drNew["IsInvr"] = "";
			drNew["InvrRemark"] = "";

			try
			{
				dsHcsCode.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("HcsScanDAL","AddNewRowHcsDS","R005",ex.Message.ToString());
			}
		}


		public static void AddNewRowHcsErrorDS(ref SessionDS dsErrorCode)
		{
			DataRow drNew = dsErrorCode.ds.Tables[0].NewRow();

			drNew["Hcs_No"] = "";
			drNew["Consignment_No"] = "";
			drNew["ScanDatetime"] = DateTime.Now;
			drNew["ScanUserId"] = "";
			drNew["IsDup"] = "";
			drNew["IsNoPod"] = "";
			drNew["IsHcr"] = "";

			try
			{
				dsErrorCode.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("HcsScanDAL","AddNewRowHcsErrorDS","R005",ex.Message.ToString());
			}
		}


		public static DataSet GetHcsReportDS(string strAppId, string strEntId, string strHcsNo)
		{
			DbConnection dbConnect = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbConnect = DbConnectionManager.GetInstance().GetDbConnection(strAppId, strEntId);
  
			if (dbConnect == null)
			{
				Logger.LogTraceError("TIESDAL", "HcsScanDAL", "GetHcsReportDS", "dbConnect is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			string strSQLQuery = "";

			strSQLQuery = "SELECT ROW_NUMBER() OVER(ORDER BY scandatetime, consignment_no) AS Row, ";
			strSQLQuery += " hcs_no, consignment_no, cust_ref, payerid, last_status_datetime, ";
			strSQLQuery += " scanuserid, org_dc, del_route, last_status_code, isinvr, invrremark ";
			strSQLQuery += " FROM HCS_DT ";
			strSQLQuery += " WHERE ApplicationId = '" + strAppId + "'  AND enterpriseid = '" + strEntId + "'"; 
			strSQLQuery += " AND hcs_no = '" + strHcsNo + "' ";
			strSQLQuery += " ORDER BY scandatetime asc ";
 
			dbCmd = dbConnect.CreateCommand(strSQLQuery.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbConnect.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;     
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "HcsScanDAL", "GetHcsReportDS", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}	
		
		
		public static DataSet GetAllOpenHcsHdDS(string strAppId, string strEntId, string strLoc)
		{
			DbConnection dbConnect = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbConnect = DbConnectionManager.GetInstance().GetDbConnection(strAppId, strEntId);
  
			if (dbConnect == null)
			{
				Logger.LogTraceError("TIESDAL", "HcsScanDAL", "GetAllOpenHcsHdDS", "dbConnect is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			string strSQLQuery = "";

			strSQLQuery = "SELECT hcs_no, hcs_date, location, isclosed, total_cons, crt_userid, crt_datetime ";
			strSQLQuery += " FROM HCS_HD ";
			strSQLQuery += " WHERE ApplicationId = '" + strAppId + "'  AND enterpriseid = '" + strEntId + "'"; 
			strSQLQuery += " AND (isclosed IS NULL OR isclosed != 'Y')"; 

			if (strLoc != "")
				strSQLQuery += " AND location = '" + strLoc + "' ";

			strSQLQuery += " ORDER BY hcs_date desc ";

 
			dbCmd = dbConnect.CreateCommand(strSQLQuery.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbConnect.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;     
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "HcsScanDAL", "GetAllOpenHcsHdDS", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}	
		
		
		public static DataSet GetByNoHcsHdDS(string strAppId, string strEntId, string strHcsNo, string strIsClosed)
		{
			DbConnection dbConnect = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbConnect = DbConnectionManager.GetInstance().GetDbConnection(strAppId, strEntId);
  
			if (dbConnect == null)
			{
				Logger.LogTraceError("TIESDAL", "HcsScanDAL", "GetByNoHcsHdDS", "dbConnect is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			string strSQLQuery = "";

			strSQLQuery = "SELECT TOP 1 hcs_no, hcs_date, location, isclosed, total_cons, crt_userid, crt_datetime ";
			strSQLQuery += " FROM HCS_HD ";
			strSQLQuery += " WHERE ApplicationId = '" + strAppId + "' AND enterpriseid = '" + strEntId + "'"; 
			strSQLQuery += " AND hcs_no = '"+ strHcsNo + "' ";

			if (strIsClosed == "Y")
				strSQLQuery += " AND isclosed = 'Y'"; 
			else
                strSQLQuery += " AND (isclosed IS NULL OR isclosed != 'Y')"; 			

 
			dbCmd = dbConnect.CreateCommand(strSQLQuery.ToString(), CommandType.Text);
			try
			{
				DS = (DataSet)dbConnect.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;     
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "HcsScanDAL", "GetByNoHcsHdDS", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}	
		
		
		public static DataSet GetByDateHcsHdDS(string strAppId, string strEntId, string strDate, string strLoc)
		{
			DbConnection dbConnect = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbConnect = DbConnectionManager.GetInstance().GetDbConnection(strAppId, strEntId);
  
			if (dbConnect == null)
			{
				Logger.LogTraceError("TIESDAL", "HcsScanDAL", "GetByDateHcsHdDS", "dbConnect is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			string strSQLQuery = "";

			strSQLQuery = "SELECT hcs_no, hcs_date, location, isclosed, total_cons, crt_userid, crt_datetime ";
			strSQLQuery += " FROM HCS_HD ";
			strSQLQuery += " WHERE ApplicationId = '" + strAppId + "'  AND enterpriseid = '" + strEntId + "'"; 
			strSQLQuery += " AND CONVERT(datetime, CONVERT(varchar(10),hcs_date,103), 103) = ";
			strSQLQuery += "     CONVERT(datetime, '" + strDate + "', 103) ";
			strSQLQuery += " AND isclosed = 'Y' ";

			if (strLoc != "")
				strSQLQuery += " AND location = '" + strLoc + "' ";

			strSQLQuery += " ORDER BY hcs_date DESC ";
 
			dbCmd = dbConnect.CreateCommand(strSQLQuery.ToString(), CommandType.Text);

			try
			{
				DS = (DataSet)dbConnect.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;   
  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "HcsScanDAL", "GetByDateHcsHdDS", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}	
		
		
		public static DataSet GetByConsHcsHdDS(string strAppId, string strEntId, string strConsNo)
		{
			DbConnection dbConnect = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbConnect = DbConnectionManager.GetInstance().GetDbConnection(strAppId, strEntId);
  
			if (dbConnect == null)
			{
				Logger.LogTraceError("TIESDAL", "HcsScanDAL", "GetByConsHcsHdDS", "dbConnect is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			string strSQLQuery = "";

			strSQLQuery = "SELECT HCS_HD.hcs_no, HCS_HD.hcs_date, HCS_HD.isclosed, HCS_HD.crt_userid, HCS_HD.crt_datetime ";
			strSQLQuery += " FROM HCS_HD, HCS_DT ";
			strSQLQuery += " WHERE HCS_HD.applicationid = HCS_DT.applicationid ";
			strSQLQuery += " AND HCS_HD.enterpriseid = HCS_DT.enterpriseid "; 
			strSQLQuery += " AND HCS_HD.hcs_no = HCS_DT.hcs_no ";
			strSQLQuery += " AND HCS_HD.applicationid = '" + strAppId + "'  AND HCS_HD.enterpriseid = '" + strEntId + "'"; 
			strSQLQuery += " AND HCS_DT.consignment_no = '" + strConsNo + "' ";
 
			dbCmd = dbConnect.CreateCommand(strSQLQuery.ToString(), CommandType.Text);

			try
			{
				DS = (DataSet)dbConnect.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;   
  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "HcsScanDAL", "GetByConsHcsHdDS", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}	
		
		
		public static DataSet GetSubDcByLoc(string strAppId, string strEntId, string strLocation)
		{
			DbConnection dbConnect = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbConnect = DbConnectionManager.GetInstance().GetDbConnection(strAppId, strEntId);
  
			if (dbConnect == null)
			{
				Logger.LogTraceError("TIESDAL", "HcsScanDAL", "GetSubDcByLoc", "dbConnect is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			string strSQL = "";

			strSQL = "SELECT subdc_code ";
			strSQL += " FROM sub_distribution_center ";
			strSQL += " WHERE applicationid = '" + strAppId + "'  AND enterpriseid = '" + strEntId + "'"; 
			strSQL += " AND origin_code = '" + strLocation + "' ";
			strSQL += " AND isenabled = 'Y' ";
			strSQL += " AND (isdeleted IS NULL OR isdeleted <> 'Y') ";
 
			dbCmd = dbConnect.CreateCommand(strSQL.ToString(), CommandType.Text);

			try
			{
				DS = (DataSet)dbConnect.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;   
  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "HcsScanDAL", "GetSubDcByLoc", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}	
		
		

		

	
	}
}
