using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using System.Data.SqlClient;

namespace TIESDAL
{
	/// <summary>
	/// Summary description for CustomJobCompiling.
	/// </summary>
	public class CustomJobCompilingDAL
	{
		public CustomJobCompilingDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		public static System.Data.DataSet SearchCustomJobCompiling(string strAppID, string strEnterpriseID, string userloggedin, string consignment_no, string JobEntryNo)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","SearchCustomJobCompiling","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" EXEC dbo.Customs_JobEntryCompiling  @action= 0  ");
				strQry.Append(" ,@enterpriseid  = '"+ strEnterpriseID +"' "); 
				if( userloggedin != String.Empty)
				{
					strQry.Append(" ,@userloggedin  = '"+ userloggedin +"' "); 
				}
				if( consignment_no != String.Empty)
				{
					strQry.Append(" ,@consignment_no  = '"+ consignment_no +"' "); 
				}

				if( JobEntryNo != String.Empty)
				{
					strQry.Append(" ,@JobEntryNo  = '"+ JobEntryNo +"' "); 
				}
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "SearchCustomJobCompiling", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 
		} 


		public static System.Data.DataSet GetDropDownListDisplayValue(string strAppID,string strEnterpriseID, string ConType)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","SearchCustomJobCompiling","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT DropDownListDisplayValue, DefaultValue, [Description], CodedValue  ");
				strQry.Append(" FROM dbo.GetCustomsConfiguration( '"+strEnterpriseID+"', '"+ConType+"', 1) " ); 
				 
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "SearchCustomJobCompiling", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}
 

		public static System.Data.DataSet GetDropDownListDefaultDisplayValue(string strAppID,string strEnterpriseID, string ConType)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","SearchCustomJobCompiling","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT DropDownListDisplayValue, DefaultValue, [Description], CodedValue  ");
				strQry.Append(" FROM dbo.GetCustomsConfiguration( '"+strEnterpriseID+"', '"+ConType+"', 0) " ); 
				 
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "SearchCustomJobCompiling", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}
 

		public static System.Data.DataSet GetUserAssignedRoles(string strAppID,string strEnterpriseID, string User)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","GetUserAssignedRoles","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT [user_name], RoleName, IsAssigned  ");
				strQry.Append(" FROM dbo.UserAssignedRoles ( '"+strEnterpriseID+"', '"+User+"', 'CUSTOMSSU') " ); 
				 
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetUserAssignedRoles", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}
 

		public static System.Data.DataSet GetCountry(string strAppID, string strEnterpriseID)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","GetCountry","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT CountryCode=' ', CountryName=' ', DefaultValue=1, CurrencyCode=' '");
				strQry.Append(" UNION ALL " );  
				strQry.Append(" SELECT CountryCode, CountryName, 0, CurrencyCode");
				strQry.Append(" FROM dbo.GetCountries('"+strEnterpriseID+"' )  " );  
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetCountry", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}


		public static System.Data.DataSet GetCountryDefaultValue(string strAppID, string strEnterpriseID)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","GetCountry","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT CountryCode, CountryName, DefaultValue, CurrencyCode");
				strQry.Append(" FROM dbo.GetCountries('"+strEnterpriseID+"' )  " );  
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetCountry", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}


		public static System.Data.DataSet GetCountry(string strAppID, string strEnterpriseID,string CountryCode)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","GetCountry","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				if(CountryCode != null && CountryCode.Trim() != "")
				{
					strQry.Append(" SELECT CountryCode, CountryName, 0, CurrencyCode");
					strQry.Append(" FROM dbo.GetCountries('"+strEnterpriseID+"' )" );  
					strQry.Append(" WHERE CountryCode = '"+CountryCode+"'" ); 
				}
				else
				{
					strQry.Append(" SELECT CountryCode=' ', CountryName=' ', DefaultValue=1, CurrencyCode=' '");
					strQry.Append(" UNION ALL " );  
					strQry.Append(" SELECT CountryCode, CountryName, 0, CurrencyCode");
					strQry.Append(" FROM dbo.GetCountries('"+strEnterpriseID+"' )" );  
				}
												
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetCountry", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}


		public static System.Data.DataSet GetCountryCurrency(string strAppID, string strEnterpriseID, string  CountryCode)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","GetCountryCurrency","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{ 
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" select  Currencycode  ");
				strQry.Append(" from CountriesCurrencies where CountryCode = '"+CountryCode+"'" );  
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetCountryCurrency", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}


		public static System.Data.DataSet GetConfigMarksNos(string strAppID, string strEnterpriseID)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","GetConfigMarksNos","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{ 
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT [key], value  ");
				strQry.Append(" FROM dbo.EnterpriseConfigurations('"+strEnterpriseID+"', 'CustomsJobCompiling' )" );  
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetConfigMarksNos", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}

		public static System.Data.DataSet GetCountryDefaultNull(string strAppID, string strEnterpriseID)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","GetCountry","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT CountryCode='  ', CountryName=' ', DefaultValue=1  ");
				strQry.Append(" UNION ALL " );  
				strQry.Append(" SELECT CountryCode, CountryName, DefaultValue  ");
				strQry.Append(" FROM dbo.GetCountries('"+strEnterpriseID+"' )" );  
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetCountry", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}		

		public static string GetCountryName(string strAppID, string strEnterpriseID,string CountryCode)
		{
			string CountryName="";
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","GetCountry","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT CountryCode, CountryName, DefaultValue  ");
				strQry.Append(" FROM dbo.GetCountries('"+strEnterpriseID+"' )" );  
				strQry.Append(" WHERE CountryCode = '"+CountryCode+"'" );  				
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				if(dsResult.Tables[0].Rows.Count>0)
				{
					CountryName=dsResult.Tables[0].Rows[0]["CountryName"].ToString().ToString();
				}
				return CountryName;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetCountry", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}
 

		public static System.Data.DataSet GetCurrencies( string strAppID, string strEnterpriseID )
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","GetCountry","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT CurrencyCode, DefaultValue ");
				strQry.Append(" FROM dbo.GetCurrencies( '"+strEnterpriseID+"' )" );  
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetCountry", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}
 


		public static System.Data.DataSet GetExistsCustoms_JobDetails( string strAppID, string strEnterpriseID, string strConsimenNo, string strPayerID, string strJobNo )
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","GetExistsCustoms_JobDetails","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{ 
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT applicationid, enterpriseid, consignment_no, payerid, JobEntryNo ,ChargesExist, EntryLinesExist");
				strQry.Append(" FROM dbo.ExistsCustoms_JobDetails('"+strEnterpriseID+"', '"+strConsimenNo+"', '"+strPayerID+"', '"+strJobNo+"' )" );  
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetExistsCustoms_JobDetails", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}
 


		public static System.Data.DataSet GetDropDownListDisplayValue(string strAppID,string strEnterpriseID, string ConType,string strValue)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","SearchCustomJobCompiling","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT DropDownListDisplayValue, DefaultValue, [Description], CodedValue  ");
				strQry.Append(" FROM dbo.GetCustomsConfiguration( '"+strEnterpriseID+"', '"+ConType+"', 1) " ); 
				strQry.Append(" WHERE CodedValue = '"+strValue+"' " ); 
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "SearchCustomJobCompiling", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 
		}


		public static DataSet SaveCustomJobCompiling2(DataTable dtParams )
		{			

			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null;  
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(dtParams.Rows[0]["strAppID"].ToString().ToString(),dtParams.Rows[0]["enterpriseid"].ToString());
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","SaveCustomJobCompiling","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
					
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" EXEC dbo.Customs_JobEntryCompiling  @action= 1  ");
				strQry.Append(" ,@enterpriseid  = '"+ dtParams.Rows[0]["enterpriseid"].ToString() +"' "); 
				strQry.Append(" ,@userloggedin  = '"+ dtParams.Rows[0]["userloggedin"].ToString()+"' "); 
				strQry.Append(" ,@consignment_no  = '"+ dtParams.Rows[0]["consignment_no"].ToString() +"' "); 
				strQry.Append(" ,@JobEntryNo  = '"+ dtParams.Rows[0]["JobEntryNo"].ToString() +"' "); 
				strQry.Append(" ,@TransportMode  =  '"+ dtParams.Rows[0]["TransportMode"].ToString()+"' ");  
				strQry.Append(" ,@ExporterName  = '"+ dtParams.Rows[0]["ExporterName"].ToString() +"' "); 
				strQry.Append(" ,@ExporterAddress1  = '"+ dtParams.Rows[0]["ExporterAddress1"].ToString() +"' "); 
				strQry.Append(" ,@ExporterAddress2  = '"+ dtParams.Rows[0]["ExporterAddress2"].ToString() +"' "); 
				strQry.Append(" ,@ExporterCountry   = '"+ dtParams.Rows[0]["ExporterCountry"].ToString() +"' "); 
				strQry.Append(" ,@LoadingPortCity  = '"+ dtParams.Rows[0]["LoadingPortCity"].ToString() +"' "); 
				strQry.Append(" ,@LoadingPortCountry  = '"+ dtParams.Rows[0]["LoadingPortCountry"].ToString() +"' "); 
				strQry.Append(" ,@DischargePort   = '"+ dtParams.Rows[0]["DischargePort"].ToString() +"' "); 
				strQry.Append(" ,@CurrencyCode  = '"+ dtParams.Rows[0]["CurrencyCode"].ToString() +"' "); 
				strQry.Append(" ,@InfoReceivedDate  = '"+ dtParams.Rows[0]["InfoReceivedDate"].ToString() +"' "); 
				strQry.Append(" ,@MasterAWBNumber  = '"+ dtParams.Rows[0]["MasterAWBNumber"].ToString() +"' "); 
				strQry.Append(" ,@ShipFlightNo  = '"+ dtParams.Rows[0]["ShipFlightNo"].ToString() +"' "); 
				strQry.Append(" ,@ExpectedArrivalDate  = '"+ dtParams.Rows[0]["ExpectedArrivalDate"].ToString() +"' "); 
				strQry.Append(" ,@ActualArrivalDate  = '"+ dtParams.Rows[0]["ActualArrivalDate"].ToString() +"' "); 
				strQry.Append(" ,@EntryType   = '"+ dtParams.Rows[0]["EntryType"].ToString() +"' "); 
				strQry.Append(" ,@DeliveryTerms  = '"+ dtParams.Rows[0]["DeliveryTerms"].ToString() +"' "); 
				strQry.Append(" ,@FolioNumber  = '"+ dtParams.Rows[0]["FolioNumber"].ToString() +"' "); 
				strQry.Append(" ,@BondedWarehouse  = '"+ dtParams.Rows[0]["BondedWarehouse"].ToString() +"' "); 
				strQry.Append(" ,@CurrencyRateDate   = '"+ dtParams.Rows[0]["CurrencyRateDate"].ToString() +"' "); 
				strQry.Append(" ,@CustomsAgent  = '"+ dtParams.Rows[0]["CustomsAgent"].ToString() +"' "); 
				strQry.Append(" ,@ModelOfDeclaration  = '"+ dtParams.Rows[0]["ModelOfDeclaration"].ToString() +"' "); 
				strQry.Append(" ,@GeneralProcedureCode  = '"+ dtParams.Rows[0]["GeneralProcedureCode"].ToString() +"' "); 
				strQry.Append(" ,@CustomsProcedureCode  = '"+ dtParams.Rows[0]["CustomsProcedureCode"].ToString() +"' "); 
				strQry.Append(" ,@CustomsAddlCode   = '"+ dtParams.Rows[0]["CustomsAddlCode"].ToString() +"' "); 
				strQry.Append(" ,@CustomsDeclarationNo   = '"+ dtParams.Rows[0]["CustomsDeclarationNo"].ToString() +"' "); 
				strQry.Append(" ,@DateOfAssessment  = '"+ dtParams.Rows[0]["DateOfAssessment"].ToString() +"' "); 
				strQry.Append(" ,@AssignedProfile   = '"+ dtParams.Rows[0]["AssignedProfile"].ToString() +"' "); 
				strQry.Append(" ,@DeclarationFormPages  = '"+ dtParams.Rows[0]["DeclarationFormPages"].ToString() +"' "); 
				strQry.Append(" ,@Total_pkgs  = '"+ dtParams.Rows[0]["Total_pkgs"].ToString() +"' "); 
				strQry.Append(" ,@Total_weight  = '"+ dtParams.Rows[0]["Total_weight"].ToString() +"' "); 
				strQry.Append(" ,@GoodsDescription  = '"+ dtParams.Rows[0]["GoodsDescription"].ToString() +"' ;");  

								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
					
				return dsResult;
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "ConfigureCustomerSelfServiceMgrDAL", "UpdateConfigCusSelfService", "ERR009", "Update Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error updateing UpdateConfigCusSelfService", ex); 
			}
			finally
			{
				dsResult.Dispose();
			}
		
		}


		public static DataSet SaveCustomJobCompiling(DataTable dtParams )
		{			

			DataSet dsResult = new DataSet(); 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(dtParams.Rows[0]["strAppID"].ToString().ToString(),dtParams.Rows[0]["enterpriseid"].ToString());
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","SaveCustomJobCompiling","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{
					
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList(); 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@action", 1));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid", dtParams.Rows[0]["enterpriseid"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin", dtParams.Rows[0]["userloggedin"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no", dtParams.Rows[0]["consignment_no"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@JobEntryNo", dtParams.Rows[0]["JobEntryNo"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@TransportMode",dtParams.Rows[0]["TransportMode"].ToString())); 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ExporterName", dtParams.Rows[0]["ExporterName"].ToString()));
				//storedParams.Add(new System.Data.SqlClient.SqlParameter("@ExporterAddress1", dtParams.Rows[0]["ExporterAddress1"].ToString()));
				if(dtParams.Rows[0]["ExporterAddress1"] != DBNull.Value  && dtParams.Rows[0]["ExporterAddress1"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ExporterAddress1",dtParams.Rows[0]["ExporterAddress1"].ToString()));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ExporterAddress1", DBNull.Value));
				}
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ExporterAddress2", dtParams.Rows[0]["ExporterAddress2"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ExporterCountry ", dtParams.Rows[0]["ExporterCountry"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@LoadingPortCity", dtParams.Rows[0]["LoadingPortCity"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@LoadingPortCountry", dtParams.Rows[0]["LoadingPortCountry"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@DischargePort ", dtParams.Rows[0]["DischargePort"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@CurrencyCode", dtParams.Rows[0]["CurrencyCode"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@InfoReceivedDate", dtParams.Rows[0]["InfoReceivedDate"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@MasterAWBNumber", dtParams.Rows[0]["MasterAWBNumber"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ShipFlightNo", dtParams.Rows[0]["ShipFlightNo"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ExpectedArrivalDate", dtParams.Rows[0]["ExpectedArrivalDate"].ToString()));							 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@EntryType ", dtParams.Rows[0]["EntryType"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@DeliveryTerms", dtParams.Rows[0]["DeliveryTerms"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@BondedWarehouse", dtParams.Rows[0]["BondedWarehouse"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@CustomsAgent", dtParams.Rows[0]["CustomsAgent"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ModelOfDeclaration", dtParams.Rows[0]["ModelOfDeclaration"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@GeneralProcedureCode", dtParams.Rows[0]["GeneralProcedureCode"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@CustomsProcedureCode", dtParams.Rows[0]["CustomsProcedureCode"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@AssignedProfile ", dtParams.Rows[0]["AssignedProfile"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Total_pkgs", dtParams.Rows[0]["Total_pkgs"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Total_weight", dtParams.Rows[0]["Total_weight"].ToString()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@GoodsDescription", dtParams.Rows[0]["GoodsDescription"].ToString()));
				if(dtParams.Rows[0]["ActualArrivalDate"] != DBNull.Value  && dtParams.Rows[0]["ActualArrivalDate"].ToString() !="")
				{ storedParams.Add(new System.Data.SqlClient.SqlParameter("@ActualArrivalDate",dtParams.Rows[0]["ActualArrivalDate"].ToString())); }
				else
				{ storedParams.Add(new System.Data.SqlClient.SqlParameter("@ActualArrivalDate", DBNull.Value));  }	

				if(dtParams.Rows[0]["FolioNumber"] != DBNull.Value  && dtParams.Rows[0]["FolioNumber"].ToString() !="")
				{ storedParams.Add(new System.Data.SqlClient.SqlParameter("@FolioNumber",dtParams.Rows[0]["FolioNumber"].ToString())); }
				else
				{ storedParams.Add(new System.Data.SqlClient.SqlParameter("@FolioNumber", DBNull.Value)); }
				//.Add(new System.Data.SqlClient.SqlParameter("@FolioNumber", dtParams.Rows[0]["FolioNumber"].ToString()));
								 
				if(dtParams.Rows[0]["CurrencyRateDate"] != DBNull.Value  && dtParams.Rows[0]["CurrencyRateDate"].ToString() !="")
				{ storedParams.Add(new System.Data.SqlClient.SqlParameter("@CurrencyRateDate",dtParams.Rows[0]["CurrencyRateDate"].ToString())); }
				else
				{ storedParams.Add(new System.Data.SqlClient.SqlParameter("@CurrencyRateDate", DBNull.Value)); }
				
				if(dtParams.Rows[0]["CustomsAddlCode"] != DBNull.Value  && dtParams.Rows[0]["CustomsAddlCode"].ToString() !="")
				{ storedParams.Add(new System.Data.SqlClient.SqlParameter("@CustomsAddlCode",dtParams.Rows[0]["CustomsAddlCode"].ToString())); }
				else
				{ storedParams.Add(new System.Data.SqlClient.SqlParameter("@CustomsAddlCode", DBNull.Value)); }

				if(dtParams.Rows[0]["CustomsDeclarationNo"] != DBNull.Value  && dtParams.Rows[0]["CustomsDeclarationNo"].ToString() !="")
				{ storedParams.Add(new System.Data.SqlClient.SqlParameter("@CustomsDeclarationNo",dtParams.Rows[0]["CustomsDeclarationNo"].ToString()));}
				else
				{storedParams.Add(new System.Data.SqlClient.SqlParameter("@CustomsDeclarationNo", DBNull.Value));}

				if(dtParams.Rows[0]["DateOfAssessment"] != DBNull.Value  && dtParams.Rows[0]["DateOfAssessment"].ToString() !="")
				{storedParams.Add(new System.Data.SqlClient.SqlParameter("@DateOfAssessment",dtParams.Rows[0]["DateOfAssessment"].ToString()));}
				else
				{storedParams.Add(new System.Data.SqlClient.SqlParameter("@DateOfAssessment", DBNull.Value));}
				 
				if(dtParams.Rows[0]["DeclarationFormPages"] != DBNull.Value  && dtParams.Rows[0]["DeclarationFormPages"].ToString() !="")
				{storedParams.Add(new System.Data.SqlClient.SqlParameter("@DeclarationFormPages",dtParams.Rows[0]["DeclarationFormPages"].ToString()));}
				else
				{storedParams.Add(new System.Data.SqlClient.SqlParameter("@DeclarationFormPages", DBNull.Value));}
				
				if(dtParams.Rows[0]["AssessmentAmount"] != DBNull.Value  && dtParams.Rows[0]["AssessmentAmount"].ToString() !="")
				{storedParams.Add(new System.Data.SqlClient.SqlParameter("@AssessmentAmount",dtParams.Rows[0]["AssessmentAmount"].ToString()));}
				else
				{storedParams.Add(new System.Data.SqlClient.SqlParameter("@AssessmentAmount", DBNull.Value));}
				
				if(dtParams.Rows[0]["ExpressCity"] != DBNull.Value  && dtParams.Rows[0]["ExpressCity"].ToString() !="")
				{storedParams.Add(new System.Data.SqlClient.SqlParameter("@ExpressCity",dtParams.Rows[0]["ExpressCity"].ToString()));}
				else
				{storedParams.Add(new System.Data.SqlClient.SqlParameter("@ExpressCity", DBNull.Value));}

				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ExpressDoorToDoor",dtParams.Rows[0]["ExpressDoorToDoor"].ToString()));

				if(dtParams.Rows[0]["ChargeableWeight"] != DBNull.Value  && dtParams.Rows[0]["ChargeableWeight"].ToString() !="")
				{storedParams.Add(new System.Data.SqlClient.SqlParameter("@ChargeableWeight",dtParams.Rows[0]["ChargeableWeight"].ToString()));}
				else
				{storedParams.Add(new System.Data.SqlClient.SqlParameter("@ChargeableWeight", DBNull.Value));}

				if(dtParams.Rows[0]["Remarks"] != DBNull.Value  && dtParams.Rows[0]["Remarks"].ToString() !="")
				{storedParams.Add(new System.Data.SqlClient.SqlParameter("@Remarks",dtParams.Rows[0]["Remarks"].ToString()));}
				else
				{storedParams.Add(new System.Data.SqlClient.SqlParameter("@Remarks", DBNull.Value));}

				storedParams.Add(new System.Data.SqlClient.SqlParameter("@IgnoreMAWBValidation", dtParams.Rows[0]["IgnoreMAWBValidation"].ToString()));
				dsResult = (DataSet)dbCon.ExecuteProcedure("Customs_JobEntryCompiling",storedParams,ReturnType.DataSetType);
				return dsResult;
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "ConfigureCustomerSelfServiceMgrDAL", "UpdateConfigCusSelfService", "ERR009", "Update Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error updateing UpdateConfigCusSelfService", ex); 
			}
			finally
			{
				dsResult.Dispose();
			}
		
		}

 
		public static System.Data.DataTable GetCustomsTariffDescription(string strAppID, string strEnterpriseID,string tariff_code)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","GetCustomsTariffDescription","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT applicationid, enterpriseid, tariff_code, tariff_description ");
				strQry.Append(" ,amount_required, tariff_qty_required, suppl_qty_required " );  
				strQry.Append(" FROM dbo.GetCustomsTariff('"+strEnterpriseID+"','"+tariff_code+"' )" );  
												
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return dsResult.Tables[0];
				
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetCountry", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}


		public static DataSet SaveCustomJobMraksNos(String strAppID,
			String enterpriseid,
			String userloggedin , 
			String action ,
			String consignment_no ,
			String payerid,
			String JobEntryNo,
			String SeqNo,
			String MarksNos_Code,
			String Description)
		{			

			DataSet dsResult = null;
			IDbCommand dbCmd = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,enterpriseid);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","SaveCustomJobMraksNos","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{ 
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				if (action == "0")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@action", "0"));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@action", "1"));
				}				
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid", enterpriseid));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin", userloggedin));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",consignment_no)); 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid", payerid)); 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@JobEntryNo", JobEntryNo));  
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@SeqNo", SeqNo)); 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@MarksNos_Code", MarksNos_Code)); 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Description", Description));  
				 
				dsResult = (DataSet)dbCon.ExecuteProcedure("Customs_JobMarksNos",storedParams,ReturnType.DataSetType);
				return dsResult; 
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "SaveCustomJobMraksNos", "ERR009", "Update Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error updateing UpdateConfigCusSelfService", ex); 
			}
			finally
			{
				dsResult.Dispose();
			}
		
		}	


		public static DataSet SaveCustomJobCharge(String strAppID,
			String enterpriseid,
			String userloggedin ,
			String consignment_no,
			String payerid,
			String JobEntryNo,
			String SeqNo,
			String Charges_Code,
			String Amount,
			String CurrencyCode)
		{
			DataSet dsResult = null;
			IDbCommand dbCmd = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,enterpriseid);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","SaveCustoms_JobCharges","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{ 
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList(); 
				//storedParams.Add(new System.Data.SqlClient.SqlParameter("@action", "0"));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid", enterpriseid));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin", userloggedin));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",consignment_no)); 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid", payerid)); 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@JobEntryNo", JobEntryNo));  
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@SeqNo", SeqNo)); 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Charges_Code", Charges_Code)); 
				if (Amount == null )
				{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Amount", DBNull.Value));
				}
				else
				{
storedParams.Add(new System.Data.SqlClient.SqlParameter("@Amount", Amount));
				}
				
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@CurrencyCode", CurrencyCode));
				 
				dsResult = (DataSet)dbCon.ExecuteProcedure("Customs_JobCharges",storedParams,ReturnType.DataSetType);
				return dsResult; 
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "ConfigureCustomerSelfServiceMgrDAL", "SaveCustoms_JobCharges", "ERR009", "Update Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error updateing UpdateConfigCusSelfService", ex); 
			}
			finally
			{
				dsResult.Dispose();
			}
		
		}	



		
		public static DataSet SaveCustoms_JobEntryLines(String strAppID,
			String enterpriseid,
			String userloggedin ,
			String action, 
			String consignment_no,
			String payerid,
			String JobEntryNo,
			String SeqNo,
			String ModelOfDeclaration,
			String CustomsProcedureCode,
			String Description1,
			String Description2,
			String OriginCountry,
			String CurrencyCode,
			String tariff_code,
			String Tariff_Qty,
			String Supplemental_Qty,
			String UnitsOfMeasure,
			String Amount) 
		{
			DataSet dsResult = null;
			IDbCommand dbCmd = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,enterpriseid);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","SaveCustoms_JobEntryLines","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{ 

				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();  
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@action", action));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid", enterpriseid));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin", userloggedin));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no", consignment_no));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid", payerid));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@JobEntryNo", JobEntryNo));
				if( SeqNo.ToString() !="")
				{ storedParams.Add(new System.Data.SqlClient.SqlParameter("@SeqNo",SeqNo)); }
				else
				{ storedParams.Add(new System.Data.SqlClient.SqlParameter("@SeqNo",1)); }
				 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ModelOfDeclaration", ModelOfDeclaration));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@CustomsProcedureCode", CustomsProcedureCode));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Description1", Description1));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Description2", Description2));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@OriginCountry", OriginCountry));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@CurrencyCode", CurrencyCode));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@tariff_code", tariff_code));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Tariff_Qty", Tariff_Qty.Replace(",","")));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Supplemental_Qty", Supplemental_Qty.Replace(",","")));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@UnitsOfMeasure", UnitsOfMeasure));
				
				if (Amount == "" )
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Amount", DBNull.Value));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Amount", Amount.Replace(",",""))); 
				}
				dsResult = (DataSet)dbCon.ExecuteProcedure("Customs_JobEntryLines",storedParams,ReturnType.DataSetType);
				return dsResult; 
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "ConfigureCustomerSelfServiceMgrDAL", "SaveCustoms_JobEntryLines", "ERR009", "Update Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error updateing UpdateConfigCusSelfService", ex); 
			}
			finally
			{
				dsResult.Dispose();
			}
		
		}	


		public static DataSet DeleteCustoms_JobEntryLines(String strAppID,
			String enterpriseid,
			String userloggedin ,
			String action, 
			String consignment_no,
			String payerid,
			String JobEntryNo,
			String SeqNo) 
		{
			DataSet dsResult = null;
			IDbCommand dbCmd = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,enterpriseid);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","Customs_JobEntryLines","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{ 
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" EXEC dbo.Customs_JobEntryLines  @action= 2  ");
				strQry.Append(" ,@enterpriseid  = '"+ enterpriseid +"' "); 
				if( userloggedin != String.Empty)
				{
					strQry.Append(" ,@userloggedin  = '"+ userloggedin +"' "); 
				}
				if( consignment_no != String.Empty)
				{
					strQry.Append(" ,@consignment_no  = '"+ consignment_no +"' "); 
				}
				if( payerid != String.Empty)
				{
					strQry.Append(" ,@payerid  = '"+ payerid +"' "); 
				}
				if( JobEntryNo != String.Empty)
				{
					strQry.Append(" ,@JobEntryNo  = '"+ JobEntryNo +"' "); 
				}
				if( SeqNo != String.Empty)
				{
					strQry.Append(" ,@SeqNo  = '"+ SeqNo +"' "); 
				}
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult; 
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "Customs_JobEntryLines", "ERR009", "Update Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error updateing UpdateConfigCusSelfService", ex); 
			}
			finally
			{
				dsResult.Dispose();
			}
		
		}	



		public static System.Data.DataTable CustomJobCompilingParameter()
		{ 
			DataTable dt = new DataTable();
			dt.Columns.Add("action",typeof(string));
			dt.Columns.Add("strAppID",typeof(string));
			dt.Columns.Add("enterpriseid",typeof(string));
			dt.Columns.Add("userloggedin",typeof(string));
			dt.Columns.Add("consignment_no",typeof(string));
			dt.Columns.Add("JobEntryNo",typeof(string));
			dt.Columns.Add("TransportMode",typeof(string));   
			dt.Columns.Add("ExporterName",typeof(string));
			dt.Columns.Add("ExporterAddress1",typeof(string));
			dt.Columns.Add("ExporterAddress2",typeof(string));
			dt.Columns.Add("ExporterCountry",typeof(string));
			dt.Columns.Add("LoadingPortCity",typeof(string));
			dt.Columns.Add("LoadingPortCountry",typeof(string));
			dt.Columns.Add("DischargePort",typeof(string)); 
			dt.Columns.Add("CurrencyCode",typeof(string));
			dt.Columns.Add("InfoReceivedDate",typeof(string));
			dt.Columns.Add("MasterAWBNumber",typeof(string));
			dt.Columns.Add("ShipFlightNo",typeof(string));
			dt.Columns.Add("ExpectedArrivalDate",typeof(string));
			dt.Columns.Add("ActualArrivalDate",typeof(string));
			dt.Columns.Add("EntryType",typeof(string));
			dt.Columns.Add("DeliveryTerms",typeof(string));
			dt.Columns.Add("FolioNumber",typeof(string));
			dt.Columns.Add("BondedWarehouse",typeof(string));
			dt.Columns.Add("CurrencyRateDate",typeof(string)); 
			dt.Columns.Add("CustomsAgent",typeof(string));
			dt.Columns.Add("ModelOfDeclaration",typeof(string));
			dt.Columns.Add("GeneralProcedureCode",typeof(string));
			dt.Columns.Add("CustomsProcedureCode",typeof(string));
			dt.Columns.Add("CustomsAddlCode",typeof(string));
			dt.Columns.Add("CustomsDeclarationNo",typeof(string));
			dt.Columns.Add("DateOfAssessment",typeof(string));
			dt.Columns.Add("AssignedProfile",typeof(string));
			dt.Columns.Add("DeclarationFormPages",typeof(string));
			dt.Columns.Add("Total_pkgs",typeof(string));
			dt.Columns.Add("Total_weight",typeof(Double));
			dt.Columns.Add("GoodsDescription",typeof(string));
			dt.Columns.Add("ExpressDoorToDoor",typeof(int));
			dt.Columns.Add("AssessmentAmount",typeof(Double));
			dt.Columns.Add("ExpressCity",typeof(string));
			dt.Columns.Add("ChargeableWeight",typeof(Double));
			dt.Columns.Add("Remarks",typeof(string));
			dt.Columns.Add("IgnoreMAWBValidation",typeof(int));
			return dt;
		}

 
		public static System.Data.DataSet GetExchangeRate(string strAppID, string strEnterpriseID,string CurrenyCode, string RateDate)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","GetExchangeRate","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT enterpriseid, rate_date, CurrencyCode, rate_type, exchange_rate  ");
				strQry.Append(" FROM dbo.GetExchangeRate('"+strEnterpriseID+"', '"+CurrenyCode+"', 'D', '"+RateDate+"' )" );  

				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetExchangeRate", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}

	
		public static System.Data.DataSet GetUOM(string strAppID, string strEnterpriseID )
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","GetUOM","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT DropDownListDisplayValue, DefaultValue, Description, CodedValue  ");
				strQry.Append(" FROM dbo.GetCustomsConfiguration('"+strEnterpriseID+"', 'KindOfPkgASYCUDA',  1)" );  

				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetUOM", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}


		public static System.Data.DataSet GetFormatTariff(string strAppID, string strEnterpriseID )
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","GetFormatTariff","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{ 
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT [key], [value] ");
				strQry.Append(" FROM dbo.EnterpriseConfigurations('"+strEnterpriseID+"', 'CustomsTariffs')" );  

				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetFormatTariff", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}

	
		public static System.Data.DataSet SearchCustoms_JobEntryLines(string strAppID, string strEnterpriseID, string userloggedin, string consignment_no, string JobEntryNo, string payerid)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","SearchCustoms_JobEntryLines","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" EXEC dbo.Customs_JobEntryLines  @action= 0  ");
				strQry.Append(" ,@enterpriseid  = '"+ strEnterpriseID +"' "); 
				if( userloggedin != String.Empty)
				{
					strQry.Append(" ,@userloggedin  = '"+ userloggedin +"' "); 
				}
				if( consignment_no != String.Empty)
				{
					strQry.Append(" ,@consignment_no  = '"+ consignment_no +"' "); 
				}
				if( payerid != String.Empty)
				{
					strQry.Append(" ,@payerid  = '"+ payerid +"' "); 
				}

				if( JobEntryNo != String.Empty)
				{
					strQry.Append(" ,@JobEntryNo  = '"+ JobEntryNo +"' "); 
				}
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "SearchCustoms_JobEntryLines", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 
		} 


		public static DataSet Customs_JobDeleteDetails(String strAppID,
			String enterpriseid,
			String userloggedin ,  
			String consignment_no ,
			String payerid,
			String JobEntryNo )
		{			

			DataSet dsResult = null;
			IDbCommand dbCmd = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,enterpriseid);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","Customs_JobDeleteDetails","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			try
			{ 
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList(); 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid", enterpriseid));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin", userloggedin));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",consignment_no)); 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid", payerid)); 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@JobEntryNo", JobEntryNo));  
				 
				dsResult = (DataSet)dbCon.ExecuteProcedure("Customs_JobDeleteDetails",storedParams,ReturnType.DataSetType);
				return dsResult; 
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "Customs_JobDeleteDetails", "ERR009", "Update Failed : " + ex.Message.ToString()); 
				throw new ApplicationException("2 Error updateing UpdateConfigCusSelfService", ex); 
			}
			finally
			{
				dsResult.Dispose();
			}
		
		}

		public static System.Data.DataSet GetViewCustomsDeclarationForReport(string strAppID, string strEnterpriseID, string JobEntryNo)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomJobCompilingDAL","GetViewCustomsDeclarationForReport","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append("SELECT enterpriseid, JobEntryNo, ModelOfDeclaration, ExporterName, ExporterAddress1, ExporterAddress2");
				strQry.Append(", ExporterCountryName,Pages, PkgQty, DeclarantRefNo, ConsigneeName, ConsigneeAddress1, ConsigneeAddress2, ConsigneeZipcode, ConsigneeState");
				strQry.Append(", ConsigneeCountry, ConsigneeTCNo, CustomsAgent, CustomsAgentCode, ShipFlightNo, ExporterCountry, DeliveryTerms, Exch1, TransportMode");
				strQry.Append(", LoadingPortCity, DischargePort,BondedWarehouse");
				strQry.Append(", MarksNos1, MarksNos2, MarksNos3, MarksNos4, MarksNos5");
				strQry.Append(", DS11, DS21, DS12, DS22, DS13, DS23, ItmNo1, ItmNo2, ItmNo3");
				strQry.Append(", TC1, TC2, TC3, OC1, OC2, OC3, CPC1, CPC2, CPC3, TQ1, TQ2, TQ3, SQ1, SQ2, SQ3, AWBNo");
				
				// Append column by A 24/09/2014
				strQry.Append(", AMT1, AMT2, AMT3");
				strQry.Append(", CIF1, CIF2, CIF3");

				// Append column by A 17/09/2014
				strQry.Append(", Duty_Rate1, Duty_Amount1, Excise_Rate1, Excise_Amount1, Tax_Rate1, Tax_Amount1, Total_DutyExciseTax1 ");
				strQry.Append(", Duty_Rate2, Duty_Amount2, Excise_Rate2, Excise_Amount2, Tax_Rate2, Tax_Amount2, Total_DutyExciseTax2 ");
				strQry.Append(", Duty_Rate3, Duty_Amount3, Excise_Rate3, Excise_Amount3, Tax_Rate3, Tax_Amount3, Total_DutyExciseTax3 ");
				strQry.Append(", Total_Import ,Total_Tax ,Total_Declaration ,Total_Invoice_Price ,Invoice_Currency ");

				strQry.Append(", FRT_Amount, FRT_Currency, INS_Amount, INS_Currency, OTF_Amount, OTF_Currency");
				strQry.Append(", OTD_Amount, OTD_Currency, AWD_Amount, AWD_Currency, Location, PrintedDT ");
				strQry.Append(", MarksNos1_2, MarksNos2_2, MarksNos3_2, MarksNos4_2, MarksNos5_2 ");
				strQry.Append(", MarksNos1_3, MarksNos2_3, MarksNos3_3, MarksNos4_3, MarksNos5_3 ");
				strQry.Append(", AWBNo_2, AWBNo_3, Total_Fees ");
				strQry.Append("FROM dbo.CustomsDeclarationForm('" + strEnterpriseID + "', " + JobEntryNo + ") ");
				strQry.Append("ORDER BY JobEntryNo, DeclarationFormPage ");
				
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetViewCustomsDeclarationForReport", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 
		} 

	}
}


