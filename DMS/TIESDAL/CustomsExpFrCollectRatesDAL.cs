using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using TIESClasses;

namespace TIESDAL
{
	public class CustomsExpFrCollectRatesDAL
	{
		private string SQLText = string.Empty;
		private DbConnection dbCon;
		private IDbCommand dbCmd = null;

		public CustomsExpFrCollectRatesDAL(string _appID, string _enterpriseID) 
		{ 
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(_appID, _enterpriseID);
		}

		public DataSet GetCustomsSpecialConfiguration(string _enterpriseID)
		{			
			DataSet dsReturn = new DataSet();

			try
			{
				if(_enterpriseID.Equals(""))
					throw new ApplicationException("Please specify EnterpriseId");

				SQLText = @"SELECT DropDownListDisplayValue, DefaultValue, [Description], CodedValue
							FROM dbo.GetCustomsSpecialConfiguration('{0}', 'RateZones', 1);";
				SQLText = string.Format(SQLText, _enterpriseID);

				dbCmd = dbCon.CreateCommand(SQLText, CommandType.Text);
				dsReturn = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method InsupdCurrencyExchangeRates : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CustomsExpFrCollectRatesDAL", "GetCustomsSpecialConfiguration", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
		}

		public DataSet ExecCustomsExpFrCollectRates(CustomsExpFrCollectRates objInfo)
		{
			DataSet dsReturn = new DataSet();
			ArrayList arlParams = new ArrayList();

			try
			{
				if(objInfo.EnterpriseId.Trim().Equals(""))
					throw new ApplicationException("Please specify EnterpriseId");

				if(objInfo.UserLoggedin.Trim().Equals(""))
					throw new ApplicationException("Please specify UserLoggedin");

				#region Add parameter for execute procedure
				arlParams.Add(new SqlParameter("@action", objInfo.Action));
				arlParams.Add(new SqlParameter("@enterpriseid", objInfo.EnterpriseId));
				arlParams.Add(new SqlParameter("@userloggedin", objInfo.UserLoggedin));

				if(!objInfo.WeightFrom.Trim().Equals(""))
					arlParams.Add(new SqlParameter("@WeightFrom", objInfo.WeightFrom));
				if(!objInfo.WeightTo.Trim().Equals(""))
					arlParams.Add(new SqlParameter("@WeightTo", objInfo.WeightTo));
				if(!objInfo.NewEffectiveDate.Trim().Equals(""))
					arlParams.Add(new SqlParameter("@NewEffectiveDate", objInfo.NewEffectiveDate));
				if(!objInfo.Zone.Trim().Equals(""))
					arlParams.Add(new SqlParameter("@Zone", objInfo.Zone));
				if(!objInfo.EffectiveDate.Trim().Equals(""))
					arlParams.Add(new SqlParameter("@EffectiveDate", objInfo.EffectiveDate));
				if(!objInfo.MaxWeight.Trim().Equals(""))
					arlParams.Add(new SqlParameter("@MaxWeight", objInfo.MaxWeight));
				if(!objInfo.RateToHub.Trim().Equals(""))
					arlParams.Add(new SqlParameter("@RateToHub", objInfo.RateToHub));
				if(!objInfo.RateToOthers.Trim().Equals(""))
					arlParams.Add(new SqlParameter("@RateToOthers", objInfo.RateToOthers));
				if(!objInfo.Debug .Trim().Equals(""))
					arlParams.Add(new SqlParameter("@Debug ", objInfo.Debug ));
				#endregion

				dsReturn = (DataSet)dbCon.ExecuteProcedure("Customs_ExpFrCollect_Rates",
															arlParams,
															ReturnType.DataSetType);

				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method InsupdCurrencyExchangeRates : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CustomsExpFrCollectRatesDAL", "ExecCustomsExpFrCollectRates", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
		}
		public string GetEnterpriseConfigurations(string _enterpriseId)
		{
			string result = string.Empty;

			DataSet dsReturn = new DataSet();

			SQLText = string.Format(@"SELECT [key], [value] 
								      FROM dbo.EnterpriseConfigurations('{0}', 'CustomsExprFreightCollect')", _enterpriseId);
			try
			{
				dsReturn = (DataSet)dbCon.ExecuteQuery(SQLText, ReturnType.DataSetType);
				if(dsReturn.Tables[0].Rows.Count > 0)
					result = dsReturn.Tables[0].Rows[0][1].ToString();
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method GetCurrencies : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CurrencyExchangeRatesDAL", "GetEnterpriseConfigurations", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
			return result;
		}
	}
}
