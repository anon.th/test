using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using TIESClasses;

namespace TIESDAL
{
	public class CurrencyExchangeRatesDAL
	{
		private string SQLText = string.Empty;
		private DbConnection dbCon;
		private IDbCommand dbCmd = null;

		public CurrencyExchangeRatesDAL(string _appID, 
										string _enterpriseID) 
		{ 
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(_appID, _enterpriseID);
		}

		public DataSet GetCurrencyExchangeRates(CurrencyExchangeRates objInfo)
		{			
			DataSet dsReturn = new DataSet();
			ArrayList arlParams = new ArrayList();

			try
			{
				if(objInfo.EnterpriseId.Trim().Equals(""))
					throw new ApplicationException("Please specify EnterpriseId");

				if(objInfo.UserLoggedin.Trim().Equals(""))
					throw new ApplicationException("Please specify UserLoggedin");

				#region Add parameter for execute procedure

				arlParams.Add(new SqlParameter("@action", objInfo.Action));
				arlParams.Add(new SqlParameter("@enterpriseid", objInfo.EnterpriseId));
				arlParams.Add(new SqlParameter("@userloggedin", objInfo.UserLoggedin));

				if(!objInfo.ExchangeRateType.Trim().Equals(""))
					arlParams.Add(new SqlParameter("@ExchangeRateType", objInfo.ExchangeRateType));

				#endregion

				dsReturn = (DataSet)dbCon.ExecuteProcedure("Customs_Exchange_Rates",
															arlParams,
															ReturnType.DataSetType);

				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method GetInitCurrencyExchangeRates : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CurrencyExchangeRatesDAL", "GetInitCurrencyExchangeRates", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
		}

		public DataSet FitterCurrencyExchangeRates(CurrencyExchangeRates objInfo)
		{
			DataSet dsReturn = new DataSet();
			ArrayList arlParams = new ArrayList();

			try
			{
				objInfo.Action = 2;

				if(objInfo.EnterpriseId.Trim().Equals(""))
					throw new ApplicationException("Please specify EnterpriseId");

				if(objInfo.UserLoggedin.Trim().Equals(""))
					throw new ApplicationException("Please specify UserLoggedin");

				SQLText = @"EXEC dbo.Customs_Exchange_Rates @action = 1,
															@enterpriseid = '{0}',
															@userloggedin = '{1}'";

				SQLText = string.Format(SQLText, objInfo.EnterpriseId, objInfo.UserLoggedin);

				if(!objInfo.ExchangeRateType.Equals(""))
					SQLText += string.Format(", @ExchangeRateType = '{0}'", objInfo.ExchangeRateType);
				if(!objInfo.CurrencyCode.Equals(""))
					SQLText += string.Format(", @CurrencyCode = '{0}'", objInfo.CurrencyCode);
				if(!objInfo.RateDate.Equals(""))
					SQLText += string.Format(", @RateDate = '{0}'", objInfo.RateDate);

				SQLText += ";";

				dbCmd = dbCon.CreateCommand(SQLText, CommandType.Text);
				dsReturn = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method InsupdCurrencyExchangeRates : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CurrencyExchangeRatesDAL", "FitterCurrencyExchangeRates", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
		}

		public DataSet GetCurrencies(string _enterpriseId)
		{
			DataSet dsReturn = new DataSet();

			SQLText = string.Format(@"SELECT '' AS CurrencyCode, -1 AS DefaultValue 
									  UNION ALL
									  SELECT CurrencyCode, DefaultValue 
								      FROM dbo.GetCurrencies('{0}')", _enterpriseId);
			try
			{
				dsReturn = (DataSet)dbCon.ExecuteQuery(SQLText, ReturnType.DataSetType);
				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method GetCurrencies : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CurrencyExchangeRatesDAL", "GetCurrencies", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
		}

		public DataSet InsupdCurrencyExchangeRates(CurrencyExchangeRates objInfo)
		{
			DataSet dsReturn = new DataSet();
			ArrayList arlParams = new ArrayList();

			try
			{
				objInfo.Action = 2;

				if(objInfo.EnterpriseId.Trim().Equals(""))
					throw new ApplicationException("Please specify EnterpriseId");

				if(objInfo.UserLoggedin.Trim().Equals(""))
					throw new ApplicationException("Please specify UserLoggedin");


				objInfo.ExchangeRateType = (objInfo.ExchangeRateType.Equals("") ? "NULL" : "'" + objInfo.ExchangeRateType + "'");
				objInfo.CurrencyCode = (objInfo.CurrencyCode.Equals("") ? "NULL" : "'" + objInfo.CurrencyCode + "'");
				objInfo.RateDate = (objInfo.RateDate.Equals("") ? "NULL": "'" + objInfo.RateDate + "'");
				objInfo.Rate = (objInfo.Rate.Equals("") ? "NULL": objInfo.Rate);

				SQLText = @"EXEC dbo.Customs_Exchange_Rates @action = 2,
															@enterpriseid = '{0}',
															@userloggedin = '{1}',
															@ExchangeRateType = {2},
															@CurrencyCode = {3},
															@RateDate = {4},
															@Rate = {5};";

				SQLText = string.Format(SQLText, objInfo.EnterpriseId,
												 objInfo.UserLoggedin,
												 objInfo.ExchangeRateType,
												 objInfo.CurrencyCode,
												 objInfo.RateDate,
												 objInfo.Rate);

				dbCmd = dbCon.CreateCommand(SQLText, CommandType.Text);
				dsReturn = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method InsupdCurrencyExchangeRates : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CurrencyExchangeRatesDAL", "InsupdCurrencyExchangeRates", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
		}

		public string GetEnterpriseConfigurations(string _enterpriseId)
		{
			string result = string.Empty;

			DataSet dsReturn = new DataSet();

			SQLText = string.Format(@"SELECT [key], [value] 
								      FROM dbo.EnterpriseConfigurations('{0}', 'CustomsExchangeRates')", _enterpriseId);
			try
			{
				dsReturn = (DataSet)dbCon.ExecuteQuery(SQLText, ReturnType.DataSetType);
				if(dsReturn.Tables[0].Rows.Count > 0)
					result = dsReturn.Tables[0].Rows[0][1].ToString();
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method GetCurrencies : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CurrencyExchangeRatesDAL", "GetEnterpriseConfigurations", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
			return result;
		}
	}
}
