using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;
using System.Collections;
using System.Text.RegularExpressions;
using TIESClasses;
namespace TIESDAL
{

	public class CustomsDAL
	{
		public CustomsDAL()
		{

		}


		public static System.Data.DataTable MasterAWBEntryParameter()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("action");
			dt.Columns.Add("enterpriseid");
			dt.Columns.Add("userloggedin");
			dt.Columns.Add("MasterAWB_no");
			dt.Columns.Add("ShipFlightNo");
			dt.Columns.Add("CustomsOffice");
			dt.Columns.Add("DepartureDate");

			dt.Columns.Add("DepartureCity");
			dt.Columns.Add("DepartureCountry");
			dt.Columns.Add("ArrivalDate");
			dt.Columns.Add("ArrivalTime");
			dt.Columns.Add("ArrivalCity");
			dt.Columns.Add("ArrivalCountry");
			dt.Columns.Add("Carrier");
			dt.Columns.Add("ModeOfTransport");
			dt.Columns.Add("FolioNumber");
			dt.Columns.Add("TotalWeight");

			dt.Columns.Add("TotalAWBs");
	
			dt.Columns.Add("ConsolidatorName");
			dt.Columns.Add("ConsolidatorAddress1");
			dt.Columns.Add("ConsolidatorAddress2");
			dt.Columns.Add("ConsolidatorCountry");

			dt.Columns.Add("IgnoreMAWBValidation");
			return dt;
		}


		public static System.Data.DataTable AWBDetailDataTable()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("MasterAWB_no");
			dt.Columns.Add("MAWBNumber");
			dt.Columns.Add("consignment_no");
			dt.Columns.Add("ShipFlightNo");
			dt.Columns.Add("MAWBHasBeenSaved");
			dt.Columns.Add("ExporterName");
			dt.Columns.Add("ConsigneeName");
			return dt;
		}


		public static System.Data.DataTable MasterAWBDetailParameter()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("action");
			dt.Columns.Add("enterpriseid");
			dt.Columns.Add("userloggedin");
			dt.Columns.Add("MasterAWB_no");
			dt.Columns.Add("ShipFlightNo");
			dt.Columns.Add("consignment_no");

			dt.Columns.Add("CustomerID");
			dt.Columns.Add("ExporterName");
			dt.Columns.Add("ExporterAddress1");
			dt.Columns.Add("ExporterAddress2");
			dt.Columns.Add("ExporterCountry");
			dt.Columns.Add("NotifyName");
			dt.Columns.Add("NotifyAddress1");
			dt.Columns.Add("NotifyAddress2");
			dt.Columns.Add("NotifyCountry");
			dt.Columns.Add("CustomsShed_Location");
			dt.Columns.Add("GoodsDescription");
			dt.Columns.Add("Kind_UOM");
			dt.Columns.Add("QuantityOfKind");			
			dt.Columns.Add("WeightKG");	
			dt.Columns.Add("Nature");	

			dt.Columns.Add("OverrideConsigneeDetails");
			dt.Columns.Add("Consignee_Name");
			dt.Columns.Add("Consignee_address1");
			dt.Columns.Add("Consignee_address2");
			dt.Columns.Add("Gross_Mass");
			dt.Columns.Add("Shipping_Marks");
			return dt;
		}


		public static System.Data.DataTable InvoiceParameter()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("action");
			dt.Columns.Add("enterpriseid");
			dt.Columns.Add("userloggedin");
			dt.Columns.Add("Invoice_No");


			dt.Columns.Add("InvoiceDate");
			dt.Columns.Add("Desc_OthCharges1");
			dt.Columns.Add("Desc_OthCharges2");
			dt.Columns.Add("Desc_OthCharges3");
			dt.Columns.Add("Desc_OthCharges4");

			dt.Columns.Add("Delivery_Name");
			dt.Columns.Add("Delivery_address1");
			dt.Columns.Add("Delivery_address2");
			dt.Columns.Add("FolioNumber");
			dt.Columns.Add("DeclarationFormPages");

			dt.Columns.Add("TotalWeight");
			dt.Columns.Add("Delivery_zipcode");

			dt.Columns.Add("ChargeableWeight");

			return dt;
		}


		public static System.Data.DataTable InvoiceDetailParameter()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("enterpriseid");
			dt.Columns.Add("userloggedin");
			dt.Columns.Add("consignment_no");
			dt.Columns.Add("payerid");
			dt.Columns.Add("Invoice_No");
			dt.Columns.Add("ChargeCode");
			dt.Columns.Add("OverrideAmount");		
			return dt;
		}


		public static System.Data.DataTable InvoiceDetailDataTable()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("Code");
			dt.Columns.Add("consignment_no");
			dt.Columns.Add("payerid");
			dt.Columns.Add("Invoice_No");
			dt.Columns.Add("FeeDisb");
			dt.Columns.Add("Description");
			dt.Columns.Add("GLCode");
			dt.Columns.Add("CalculatedAmount");
			dt.Columns.Add("OverrideAmount");
			dt.Columns.Add("InvoiceAmount");
			return dt;
		}


		public static System.Data.DataTable QueryJobsStatusParameter()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("enterpriseid");
			dt.Columns.Add("StartDate");
			dt.Columns.Add("EndDate");
			dt.Columns.Add("DateType");
			dt.Columns.Add("JobEntryNo");
			dt.Columns.Add("HouseAWBNo");
			dt.Columns.Add("MasterAWBNo");
			dt.Columns.Add("SearchMAWBinJobEntry");
			dt.Columns.Add("CustomerID");	
	
			dt.Columns.Add("CustomerGroup");
			dt.Columns.Add("BondedWarehouse");
			dt.Columns.Add("DischargePort");
			dt.Columns.Add("JobStatus");
			dt.Columns.Add("SearchStatusNotAchieved");
			dt.Columns.Add("EntryType");
			dt.Columns.Add("ShipFlightNo");
			dt.Columns.Add("ExporterName");
			dt.Columns.Add("FolioNumber");
			dt.Columns.Add("ChequeReqNo");

			dt.Columns.Add("AssignedProfile");
			dt.Columns.Add("ReceiptNumber");
			dt.Columns.Add("CustomsDeclarationNo");

			//Add by beer 6-9-2014
			dt.Columns.Add("DutyTaxNotOnChequeReq");
			dt.Columns.Add("userloggedin");
			
			dt.Columns.Add("Compiler");
			dt.Columns.Add("Registrar");
			dt.Columns.Add("DestinationDC");
			dt.Columns.Add("NotDestinationDC");
			dt.Columns.Add("NoSOPorPOD");

			return dt;
		}


		public static System.Data.DataTable QueryJobsStatusDataTable()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("applicationid");
			dt.Columns.Add("enterpriseid");
			dt.Columns.Add("JobEntryNo");
			dt.Columns.Add("CustomerID");
			dt.Columns.Add("HouseAWBNumber");
			dt.Columns.Add("CreatedDate");
			dt.Columns.Add("MasterAWBNumber");
			dt.Columns.Add("EntryCompilingDate");
			dt.Columns.Add("FreightCollectDate");
			dt.Columns.Add("InvoicedDate");	
	
			dt.Columns.Add("InvoiceReceivedDate");
			dt.Columns.Add("ManifestDataEntryDate");
			dt.Columns.Add("ChequeRequisitionDate");
			dt.Columns.Add("DutyPaidDate");
			dt.Columns.Add("DeliveryDate");

			return dt;
		}


		public static System.Data.DataSet Customs_MAWB(string strAppID, string strEnterpriseID, string userloggedin, System.Data.DataTable dtParams)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomsDAL","Customs_MAWB","Customs_MAWB","DbConnection object is null!!");
				return null;
			}
			string action = dtParams.Rows[0]["action"].ToString();
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			if(action == "0")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@action",action));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));				
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
				if(dtParams.Rows[0]["MasterAWB_no"] != DBNull.Value)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@MasterAWB_no",dtParams.Rows[0]["MasterAWB_no"].ToString()));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@MasterAWB_no",DBNull.Value));
				}
				
				
			}
			else if(action == "1")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@action",action));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));				
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
				if(dtParams.Rows[0]["MasterAWB_no"] != DBNull.Value)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@MasterAWB_no",dtParams.Rows[0]["MasterAWB_no"].ToString()));
				}				
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@MasterAWB_no",DBNull.Value));
				}
				if(dtParams.Rows[0]["IgnoreMAWBValidation"] != DBNull.Value)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@IgnoreMAWBValidation",dtParams.Rows[0]["IgnoreMAWBValidation"].ToString()));
				}				
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@IgnoreMAWBValidation",DBNull.Value));
				}
			}
			else if(action == "3")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@action",action));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));				
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
				if(dtParams.Rows[0]["MasterAWB_no"] != DBNull.Value)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@MasterAWB_no",dtParams.Rows[0]["MasterAWB_no"].ToString()));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@MasterAWB_no",DBNull.Value));
				}
				
				
			}
			else
			{
				foreach(System.Data.DataColumn col in dtParams.Columns)
				{
					string colName = col.ColumnName;
					if(colName=="JobEntryNo")
					{
						if(dtParams.Rows[0][colName]==DBNull.Value)
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,DBNull.Value));										
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
					}
					else if(colName.ToLower()=="ExporterAddress1")
					{
						if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,""));	
						}
					}
					else if(colName.ToLower()=="ExporterAddress2")
					{
						if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,""));	
						}
					}
					else if(colName.ToLower()=="ActualArrivalDate")
					{
						if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,""));	
						}
					}
					else
					{
						if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
					}
				}	
			}
			
			result = (DataSet)dbCon.ExecuteProcedure("dbo.Customs_MAWB",storedParams,ReturnType.DataSetType);
			return result;
		}


		public static System.Data.DataSet Customs_MAWB_Detail(string strAppID, string strEnterpriseID, string userloggedin, System.Data.DataTable dtParams)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomsDAL","Customs_MAWB_Detail","Customs_MAWB_Detail","DbConnection object is null!!");
				return null;
			}
			string action = dtParams.Rows[0]["action"].ToString();
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			if(action == "1")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@action",action));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));				
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@MasterAWB_no",dtParams.Rows[0]["MasterAWB_no"].ToString()));		
				if(dtParams.Rows[0]["ShipFlightNo"] != DBNull.Value && dtParams.Rows[0]["ShipFlightNo"] != null)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ShipFlightNo",dtParams.Rows[0]["ShipFlightNo"].ToString()));	
				}			
				if(dtParams.Rows[0]["consignment_no"] != DBNull.Value)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",dtParams.Rows[0]["consignment_no"].ToString()));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",DBNull.Value));
				}

				if(dtParams.Rows[0]["CustomerID"] != DBNull.Value)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@CustomerID",dtParams.Rows[0]["CustomerID"].ToString()));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@CustomerID",DBNull.Value));
				}
			}
			else
			{
				foreach(System.Data.DataColumn col in dtParams.Columns)
				{
					string colName = col.ColumnName;
					if(colName=="JobEntryNo")
					{
						if(dtParams.Rows[0][colName]==DBNull.Value)
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,DBNull.Value));										
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
					}
					else if(colName.ToLower()=="ExporterAddress1")
					{
						if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,""));	
						}
					}
					else if(colName.ToLower()=="ExporterAddress2")
					{
						if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,""));	
						}
					}
					else if(colName.ToLower()=="ActualArrivalDate")
					{
						if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,""));	
						}
					}
					else if(colName.ToLower()== "goodsdescription")
					{
						if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,DBNull.Value));	
						}
					}
					else
					{
						if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
					}
				}	
			}
			
			result = (DataSet)dbCon.ExecuteProcedure("dbo.Customs_MAWB_Detail",storedParams,ReturnType.DataSetType);
			return result;
		}


		public static System.Data.DataSet Customs_Invoice(string strAppID, string strEnterpriseID, string userloggedin, System.Data.DataTable dtParams)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomsDAL","Customs_Invoice","Customs_Invoice","DbConnection object is null!!");
				return null;
			}
			string action = dtParams.Rows[0]["action"].ToString();
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			if(action == "0")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@action",action));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));				
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
				if(dtParams.Rows[0]["Invoice_No"] != DBNull.Value)
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Invoice_No",System.Int64.Parse(dtParams.Rows[0]["Invoice_No"].ToString())));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Invoice_No",DBNull.Value));
				}
								
			}
			else
			{
				foreach(System.Data.DataColumn col in dtParams.Columns)
				{
					string colName = col.ColumnName;
					if(colName=="Invoice_No")
					{
						if(dtParams.Rows[0]["Invoice_No"] != DBNull.Value)
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@Invoice_No",System.Int64.Parse(dtParams.Rows[0]["Invoice_No"].ToString())));
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@Invoice_No",DBNull.Value));
						}
					}
					else if(colName=="Delivery_zipcode")
					{
						if(dtParams.Rows[0]["Delivery_zipcode"] != DBNull.Value)
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@RecipientZipcode",dtParams.Rows[0]["Delivery_zipcode"].ToString()));
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@RecipientZipcode",DBNull.Value));
						}
					}
					else
					{
						if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));								
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,DBNull.Value));									
						}
					}

				}	
			}
			result = (DataSet)dbCon.ExecuteProcedure("dbo.Customs_Invoice",storedParams,ReturnType.DataSetType);
			return result;
		}


		public static System.Data.DataSet Customs_InvoiceDetails(string strAppID, string strEnterpriseID, string userloggedin, System.Data.DataTable dtParams)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomsDAL","Customs_InvoiceDetails","Customs_InvoiceDetails","DbConnection object is null!!");
				return null;
			}
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			foreach(System.Data.DataColumn col in dtParams.Columns)
			{
				string colName = col.ColumnName;
				if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));								
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,DBNull.Value));									
				}

			}	
			
			result = (DataSet)dbCon.ExecuteProcedure("dbo.Customs_InvoiceDetails",storedParams,ReturnType.DataSetType);
			return result;
		}


		public static System.Data.DataSet Customs_Invoice_Report(string strAppID, string strEnterpriseID, string strUserloggedin, string strInvoiceNo)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsCheckRequisitionDAL","GetChequeRequisitionTypes","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{ 
				StringBuilder strQry = new StringBuilder();
				strQry.Append("EXEC dbo.Customs_PrintInvoices ");
				strQry.Append("@enterpriseid = '" + strEnterpriseID + "' ");
				strQry.Append(",@userloggedin = '" + strUserloggedin + "' ");
				strQry.Append(",@Invoice_Nos_List = '" + strInvoiceNo + "';");
			
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsDAL", "Customs_Invoice_Report", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 
		}

        public static System.Data.DataSet Customs_Invoice_Report_JobPOD(string strAppID, string strEnterpriseID, string strUserloggedin, string strInvoiceNo)
        {
            DataSet dsResult = new DataSet();
            IDbCommand dbCmd = null;
            DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
            if (dbCon == null)
            {
                Logger.LogTraceError("TIESDAL", "Customs_Invoice_Report_JobPOD", "GetChequeRequisitionTypes", "EDB101", "DbConnection object is null!!");
                throw new ApplicationException("Connection to Database failed", null);
            }
            try
            {
                StringBuilder strQry = new StringBuilder();
                strQry.Append("EXEC dbo.Customs_PrintInvoices_JobPOD ");
                strQry.Append("@enterpriseid = '" + strEnterpriseID + "' ");
                strQry.Append(",@userloggedin = '" + strUserloggedin + "' ");
                strQry.Append(",@Invoice_Nos_List = '" + strInvoiceNo + "';");

                dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
                dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

                return dsResult;
            }
            catch (Exception ex)
            {
                Logger.LogTraceError("TIESDAL", "CustomsDAL", "Customs_Invoice_Report_JobPOD", ex.Message);
                throw new ApplicationException("Exec command failed");
            }
            finally
            {
                dsResult.Dispose();
            }
        }


        public static System.Data.DataSet Customs_QueryJobsStatus(string strAppID, string strEnterpriseID, string userloggedin, System.Data.DataTable dtParams)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomsDAL","Customs_InvoiceDetails","Customs_InvoiceDetails","DbConnection object is null!!");
				return null;
			}
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			foreach(System.Data.DataColumn col in dtParams.Columns)
			{
				string colName = col.ColumnName;
				if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));								
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,DBNull.Value));									
				}
			}
			System.Data.SqlClient.SqlParameter outputRowParam = new System.Data.SqlClient.SqlParameter("@ReturnedRows", SqlDbType.Int);
			outputRowParam.Direction = ParameterDirection.Output;
			storedParams.Add(outputRowParam);
			result = (DataSet)dbCon.ExecuteProcedure("dbo.Customs_QueryJobsStatus",storedParams,ReturnType.DataSetType);

			DataTable dtRowReturn = new DataTable("RowReturn");
			dtRowReturn.Columns.Add("value");
			DataRow dr = dtRowReturn.NewRow();
			dr["value"] = outputRowParam.Value.ToString();
			dtRowReturn.Rows.Add(dr);

			result.Tables.Add(dtRowReturn);

			return result;
		}


		public static System.Data.DataSet Customs_AWB_XML_Verification(string strAppID, string strEnterpriseID, string strUserloggedin, string action, string masterAwb_no)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsDAL","Customs_AWB_XML_Verification","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{ 
				StringBuilder strQry = new StringBuilder();
				
				if(action.Equals("1"))
				{	
					strQry.Append("declare @verifiedBy as varchar(40); ");
					strQry.Append("declare @verifiedDT as DateTime; ");
					//strQry.Append("set @verifiedBy = '" + strUserloggedin + "'; ");
					//strQry.Append("set @verifiedDT = GETDATE(); ");
				}

				strQry.Append("EXEC dbo.Customs_MAWB_Verify ");
				strQry.Append("@action = " + action + " ");
				strQry.Append(",@enterpriseid = '" + strEnterpriseID + "' ");
				strQry.Append(",@userloggedin = '" + strUserloggedin + "' ");
				strQry.Append(",@MasterAWB_no = '" + masterAwb_no + "' ");

				if(action.Equals("1"))
				{
					strQry.Append(",@VerifiedBy = @verifiedBy out");
					strQry.Append(",@VerifiedDT = @VerifiedDT out;");
					strQry.Append("select @VerifiedBy as VerifyBy, @VerifiedDT as VerrifyDate;");
				}
			
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsDAL", "Customs_AWB_XML_Verification", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 
		}


		public static System.Data.DataSet Customs_MAWB_ExportXML(string appID, string enterpriseId, string userLoggedin, string MAWBNumber)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseId);

			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsDAL","GetChequeRequisitionTypes","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			try
			{				
				StringBuilder strQry = new StringBuilder();
				strQry.Append(@"Declare @ExportedBy VARCHAR(40),
										@ExportedDT DATETIME ");
				strQry.Append("EXEC dbo.Customs_MAWB_ExportXML ");
				strQry.Append("@enterpriseid = '" + enterpriseId + "' ");
				strQry.Append(",@userloggedin = '" + userLoggedin + "' ");
				strQry.Append(",@MAWBNumber = '" + MAWBNumber + "'");
				strQry.Append(",@ExportedBy = @ExportedBy OUT");
				strQry.Append(",@ExportedDT = @ExportedDT OUT;");

				strQry.Append("Select @ExportedBy AS 'ExportedBy', @ExportedDT AS 'ExportedDT'");
			
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;

			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsDAL", "Customs_Invoice_Report", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
		}


		public static System.Data.DataSet Customs_MAWB_ExportXMLV2(string appID, string enterpriseId, string userLoggedin, string MAWBNumber)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseId);

			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsDAL","GetChequeRequisitionTypes","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			try
			{				
				StringBuilder strQry = new StringBuilder();
				strQry.Append(@"Declare @ExportedBy VARCHAR(40),
										@ExportedDT DATETIME ");
				strQry.Append("EXEC dbo.Customs_MAWB_ExportXML_V2 ");
				strQry.Append("@enterpriseid = '" + enterpriseId + "' ");
				strQry.Append(",@userloggedin = '" + userLoggedin + "' ");
				strQry.Append(",@MAWBNumber = '" + MAWBNumber + "'");
				strQry.Append(",@ExportedBy = @ExportedBy OUT");
				strQry.Append(",@ExportedDT = @ExportedDT OUT;");

				strQry.Append("Select @ExportedBy AS 'ExportedBy', @ExportedDT AS 'ExportedDT'");
			
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;

			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsDAL", "Customs_Invoice_Report", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
		}

		public static string GetPathCustomsMAWB(string appID, string enterpriseId)
		{
				DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseId);

			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsDAL","GetChequeRequisitionTypes","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			try
			{				
				string strQry = @"SELECT [key], [value]
								  FROM dbo.EnterpriseConfigurations('{0}', 'CustomsMAWB');";
				strQry = string.Format(strQry, enterpriseId);
			
				dbCmd = dbCon.CreateCommand(strQry, CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult.Tables[0].Rows[0][1].ToString();;

			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsDAL", "Customs_Invoice_Report", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
		}


		public static System.Data.DataSet Customs_UnfinalizeInvoice(string strAppID, string strEnterpriseID, string userloggedin, string Invoice_No)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomsDAL","Customs_UnfinalizeInvoice","Customs_UnfinalizeInvoice","DbConnection object is null!!");
				return null;
			}
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));				
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Invoice_No",Invoice_No));	
			
			result = (DataSet)dbCon.ExecuteProcedure("dbo.Customs_UnfinalizeInvoice",storedParams,ReturnType.DataSetType);
			return result;
		}


		public static System.Data.DataSet Customs_MAWB_ImportXML(string strAppID, string strEnterpriseID, string userloggedin, string format,string filename,string consolidationShipper,int fileMode,string textString)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomsDAL","Customs_MAWB_ImportXML","Customs_UnfinalizeInvoice","DbConnection object is null!!");
				return null;
			}
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));				
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@XMLFormat",format));	
			if(filename != null && filename.Trim() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Filename",filename));	
			}
			else
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Filename",DBNull.Value));	
			}
			
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@ConsolidationShipper",consolidationShipper));	
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@FileMode",fileMode));	
			if(fileMode == 1)
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@TextString",textString));	
			}
			

			result = (DataSet)dbCon.ExecuteProcedure("dbo.Customs_MAWB_ImportXML",storedParams,ReturnType.DataSetType);
			return result;
		}


		public static System.Data.DataSet Customs_MAWB_OverallStatus(string strAppID, string strEnterpriseID, DateTime ArrivalDate)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomsDAL","Customs_MAWB_OverallStatus","Customs_MAWB_OverallStatus","DbConnection object is null!!");
				return null;
			}

			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@ArrivalDate",ArrivalDate));
						
			result = (DataSet)dbCon.ExecuteProcedure("dbo.Customs_MAWB_OverallStatus",storedParams,ReturnType.DataSetType);
			return result;
		}


		public static System.Data.DataSet Customs_MAWB_ConsRecdByBond(string strAppID, string strEnterpriseID, string userloggedin, int ReceivedByBond,string MAWBNumber,string ShipFlightNo,string consignment_no)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomsDAL","Customs_MAWB_ConsRecdByBond","Customs_MAWB_ConsRecdByBond","DbConnection object is null!!");
				return null;
			}

			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@ReceivedByBond",ReceivedByBond));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@MAWBNumber",MAWBNumber));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@ShipFlightNo",ShipFlightNo));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",consignment_no));
						
			result = (DataSet)dbCon.ExecuteProcedure("dbo.Customs_MAWB_ConsRecdByBond",storedParams,ReturnType.DataSetType);
			return result;
		}

		public static DataSet GetCustomsMAWBConfigurations(string appID, string enterpriseId)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseId);

			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsDAL","GetCustomsMAWBConfigurations","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			try
			{				
				string strQry = @"SELECT [key], [value]
								  FROM dbo.EnterpriseConfigurations('{0}', 'CustomsMAWB');";
				strQry = string.Format(strQry, enterpriseId);
			
				dbCmd = dbCon.CreateCommand(strQry, CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;

			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsDAL", "GetCustomsMAWBConfigurations", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
		}


		public static bool IsAutoPrint(string strAppID, string strEnterprintID, string codeid , string strLocation)
		{
			bool booAutoPrint = false;
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterprintID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "IsAutoPrint", "IsAutoPrint", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append(" SELECT CanDirectPrint=ISNULL(( ");
			strQry = strQry.Append(" SELECT CASE WHEN Item IS NULL THEN 0 ELSE 1 END ");
			strQry = strQry.Append(" FROM dbo.Enterprise_Configurations A  CROSS APPLY dbo.DelimitedSplit8K(code_text, ',') B ");
			strQry = strQry.Append(" WHERE applicationid = '" + strAppID + "' AND enterpriseid  = '" + strEnterprintID + "' AND codeid = '"+codeid+"' AND item = '" + strLocation + "'), 0) ");

		
			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);

			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

				foreach (DataRow rw in DS.Tables[0].Rows)
				{
					if (rw[0].ToString() == "1") 
					{
						booAutoPrint = true; 
					}
				}
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "PopupPrintLicensePlate", "GetPrintReportLicensePlate", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}

			return booAutoPrint;

		}

		
		public static DataSet GetMiniLicensePlates(string strAppID, string strEnterprintID, string userloggedin,string MAWBNumber,string ShipFlightNo,string consignment_no)
		{
			DbConnection dbCon = null;
			IDbCommand dbCmd = null;
			DataSet DS = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterprintID);
			if (dbCon == null)
			{
				Logger.LogTraceError("TIESDAL", "CustomsDAL", "GetMiniLicensePlates", "dbCon is null");
				throw new ApplicationException("Connection to Database failed", null); 
			}

			StringBuilder strQry = new StringBuilder();
			//strQry = strQry.Append("SELECT BarCode, consignment_no, PkgNo, TotalPkgs, UserID, PrintedDT ");
			strQry = strQry.Append("SELECT * ");
			strQry = strQry.Append("FROM dbo.GetMiniLicensePlates('"+strEnterprintID+"', '"+userloggedin+"', '"+MAWBNumber+"', '"+ShipFlightNo+"', '"+consignment_no+"')");

			dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);

			try
			{
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
				return DS;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("TIESDAL", "CustomsDAL", "GetMiniLicensePlates", "Error in Query String " + appException.Message);
				throw appException;
			}
			finally
			{
				DS.Dispose();
			}
		}

        public static string GetEmailCustomsbyID(string appID, string enterpriseId, string CusID)
        {
            DataSet dsResult = new DataSet();
            IDbCommand dbCmd = null;
            DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID, enterpriseId);

            if (dbCon == null)
            {
                Logger.LogTraceError("TIESDAL", "CustomsDAL", "GetEmailCustomsbyID", "EDB101", "DbConnection object is null!!");
                throw new ApplicationException("Connection to Database failed", null);
            }

            try
            {
                string strQry = @"select email from Customer where applicationid = '{0}' and enterpriseid = '{1}' and custid = '{2}';";
                strQry = string.Format(strQry, appID, enterpriseId, CusID);

                dbCmd = dbCon.CreateCommand(strQry, CommandType.Text);
                dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

                return dsResult.Tables[0].Rows[0][0].ToString();

            }
            catch (Exception ex)
            {
                Logger.LogTraceError("TIESDAL", "CustomsDAL", "GetEmailCustomsbyID", ex.Message);
                throw new ApplicationException("Select command failed");
            }
        }
    }
}
