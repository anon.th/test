using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using System.Data.SqlClient;
namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for InvoicePaymentUpdateMgrDAL.
	/// </summary>
	public class InvoicePaymentUpdateMgrDAL
	{
		public InvoicePaymentUpdateMgrDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static DataSet getAssociatedCNDN(String application_id,String enterprise_id,String invoice_no,String culture)
		{
			DataSet dsResult=null;
			try
			{
				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(application_id,enterprise_id);
				if(dbCon == null)
				{
					Logger.LogTraceError("InvoicePaymentUpdateMgrDAL","InvoicePaymentUpdateMgrDAL","getAssociatedCNDN","EDB101","DbConnection object is null!!");
					return dsResult;
				}
				ArrayList arrParam = new ArrayList();
				arrParam.Add(new SqlParameter("@application_id",application_id));
				arrParam.Add(new SqlParameter("@enterprise_id",enterprise_id));
				arrParam.Add(new SqlParameter("@invoice_no",invoice_no));
				arrParam.Add(new SqlParameter("@culture",culture));
				dsResult = (DataSet)dbCon.ExecuteProcedure("GetAssociatedCNDN",arrParam,ReturnType.DataSetType);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
			return dsResult;
		}
		public static Decimal getTotalCurrentAmntPaid(String application_id,String enterprise_id,String invoice_no)
		{
			DataSet dsResult=null;
			Decimal dTotalAmnt=0;
			try
			{
				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(application_id,enterprise_id);
				if(dbCon == null)
				{
					Logger.LogTraceError("InvoicePaymentUpdateMgrDAL","InvoicePaymentUpdateMgrDAL","getTotalCurrentAmntPaid","EDB101","DbConnection object is null!!");
					return 0;
				}
				ArrayList arrParam = new ArrayList();
				arrParam.Add(new SqlParameter("@invoice_no",invoice_no));
				dsResult = (DataSet)dbCon.ExecuteProcedure("GetTotalInvoicePayment",arrParam,ReturnType.DataSetType);
				if(dsResult.Tables.Count>0)
				{
					dTotalAmnt=Convert.ToDecimal(dsResult.Tables[0].Rows[0][0].ToString());
				}
				else
				{
					dTotalAmnt=0;
				}
			}
			catch
			{
				//Console.WriteLine(e.Message);
				dTotalAmnt=0;
			}
			return dTotalAmnt;
		}
		public static Decimal getCurrentAmntPaid(String application_id,String enterprise_id,String invoice_no)
		{
			DataSet dsResult=null;
			Decimal dTotalAmnt=0;
			try
			{
				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(application_id,enterprise_id);
				if(dbCon == null)
				{
					Logger.LogTraceError("InvoicePaymentUpdateMgrDAL","InvoicePaymentUpdateMgrDAL","getCurrentAmntPaid","EDB101","DbConnection object is null!!");
					return 0;
				}
				ArrayList arrParam = new ArrayList();
				arrParam.Add(new SqlParameter("@invoice_no",invoice_no));
				dsResult = (DataSet)dbCon.ExecuteProcedure("GetCurrentInvoicePayment",arrParam,ReturnType.DataSetType);
				if(dsResult.Tables.Count>0)
				{
					dTotalAmnt=Convert.ToDecimal(dsResult.Tables[0].Rows[0]["amt_paid"].ToString());
				}
				else
				{
					dTotalAmnt=0;
				}
			}
			catch
			{
				//Console.WriteLine(e.Message);
				dTotalAmnt=0;
			}
			return dTotalAmnt;
		}

		public static int updateInvoice(String application_id,String enterprise_id,DataTable dtParam)
		{
			DataSet dsResult=null;
			int dTotalAmnt=0;
			try
			{
				DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(application_id,enterprise_id);
				if(dbCon == null)
				{
					Logger.LogTraceError("InvoicePaymentUpdateMgrDAL","InvoicePaymentUpdateMgrDAL","updateInvoice","EDB101","DbConnection object is null!!");
					return 0;
				}
				ArrayList arrParam = new ArrayList();
				arrParam.Add(new SqlParameter("@application_id",application_id));
				arrParam.Add(new SqlParameter("@enterprise_id",enterprise_id));
				arrParam.Add(new SqlParameter("@invoice_no",dtParam.Rows[0]["invoice_no"]));
				arrParam.Add(new SqlParameter("@invoice_status",dtParam.Rows[0]["invoice_status"]));
				arrParam.Add(new SqlParameter("@amount_paid",dtParam.Rows[0]["amount_paid"]));
				arrParam.Add(new SqlParameter("@batch_no",dtParam.Rows[0]["batch_no"]));
				arrParam.Add(new SqlParameter("@user",dtParam.Rows[0]["user"]));
				arrParam.Add(new SqlParameter("@promised_payment_date",dtParam.Rows[0]["promised_payment_date"]));
				arrParam.Add(new SqlParameter("@paid_date",dtParam.Rows[0]["paid_date"]));
				arrParam.Add(new SqlParameter("@internal_due_date",dtParam.Rows[0]["internal_due_date"]));
				arrParam.Add(new SqlParameter("@actual_bill_placement_date",dtParam.Rows[0]["actual_bill_placement_date"]));
				
				dsResult = (DataSet)dbCon.ExecuteProcedure("InvoicePaymentUpdate_Update",arrParam,ReturnType.DataSetType);
				if(dsResult.Tables.Count>0)
				{
					dTotalAmnt=Convert.ToInt16(dsResult.Tables[dsResult.Tables.Count-1].Rows[0]["ReturnValue"].ToString());
				}
				else
				{
					dTotalAmnt=1;
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				dTotalAmnt=1;
			}
			return dTotalAmnt;
		}
	}
}
