using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;
using System.Collections;
using System.Text.RegularExpressions;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for ImportConsignmentsDAL.
	/// </summary>
	public class ImportConsignmentsDAL
	{

		public static DataSet GetEmptyConsignments()
		{
			DataTable dtConsignment =  new DataTable();
			
			dtConsignment.Columns.Add(new DataColumn("recordid",typeof(string)));
			dtConsignment.Columns.Add(new DataColumn("booking_no",typeof(string)));
			dtConsignment.Columns.Add(new DataColumn("consignment_no",typeof(string)));
			dtConsignment.Columns.Add(new DataColumn("ref_no",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("payerid",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("new_account",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("payer_name",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("payer_address1",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("payer_address2",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("payer_zipcode",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("payer_telephone",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("payer_fax",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("payment_mode",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("sender_name",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("sender_address1",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("sender_address2",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("sender_zipcode",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("sender_telephone",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("sender_fax",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("sender_contact_person",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("recipient_name",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("recipient_address1",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("recipient_address2",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("recipient_zipcode",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("recipient_telephone",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("recipient_fax",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("recipient_contact_person",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("service_code",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("declare_value",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("payment_type",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("commodity_code",typeof(string)));
			dtConsignment.Columns.Add(new DataColumn("remark",typeof(string))); 
			dtConsignment.Columns.Add(new DataColumn("cod_amount",typeof(string)));

			//HC Return Task
			dtConsignment.Columns.Add(new DataColumn("return_pod_hc",typeof(string)));
			dtConsignment.Columns.Add(new DataColumn("return_invoice_hc",typeof(string)));
			//HC Return Task

			dtConsignment.Columns.Add(new DataColumn("booking_datetime",typeof(DateTime)));  //Jeab 24 Dec 10

			DataRow drEach = dtConsignment.NewRow();

			dtConsignment.Rows.Add(drEach);
			DataSet dsConsignment = new DataSet();
			dsConsignment.Tables.Add(dtConsignment);
			return  dsConsignment;	

		}


		public static DataSet GetEmptyPackages()
		{
			DataTable dtPackage =  new DataTable();
			
			dtPackage.Columns.Add(new DataColumn("recordid",typeof(string)));
			dtPackage.Columns.Add(new DataColumn("consignment_no",typeof(string)));
			dtPackage.Columns.Add(new DataColumn("mps_code",typeof(string))); 
			dtPackage.Columns.Add(new DataColumn("pkg_length",typeof(string))); 
			dtPackage.Columns.Add(new DataColumn("pkg_breadth",typeof(string))); 
			dtPackage.Columns.Add(new DataColumn("pkg_height",typeof(string))); 
			dtPackage.Columns.Add(new DataColumn("pkg_wt",typeof(string))); 
			dtPackage.Columns.Add(new DataColumn("pkg_qty",typeof(string))); 

			DataRow drEach = dtPackage.NewRow();

			dtPackage.Rows.Add(drEach);
			DataSet dsPackage = new DataSet();
			dsPackage.Tables.Add(dtPackage);
			return  dsPackage;	

		}


		public static DataSet GetEmptyAudits()
		{
			DataTable dtPackage =  new DataTable();
			
			dtPackage.Columns.Add(new DataColumn("error_code",typeof(string)));
			dtPackage.Columns.Add(new DataColumn("associated_text",typeof(string)));
			dtPackage.Columns.Add(new DataColumn("error_message",typeof(string)));

			DataRow drEach = dtPackage.NewRow();
			
			drEach["error_code"] = "";
			drEach["associated_text"] = "";
			drEach["error_message"] = "";

			dtPackage.Rows.Add(drEach);
			DataSet dsPackage = new DataSet();
			dsPackage.Tables.Add(dtPackage);
			return  dsPackage;	

		}


		public static void ImportToTempTables(String strAppID, String strEnterpriseID,
			DataSet dsConsignment, DataSet dsPackage, String user_culture, String userId)
		{
			int iRowsAffected = 0;
			IDbCommand dbCmdCons = null;
			IDbCommand dbCmdPKG = null;
			
			if (dsConsignment == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","DataSet is null!!");
				throw new ApplicationException("The Consignment DataSet is null",null);
			}

			if (dsPackage == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","DataSet is null!!");
				throw new ApplicationException("The Package DataSet is null",null);
			}

			DbConnection dbConCons = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConCons == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conAppCons = dbConCons.GetConnection();
			if(conAppCons == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConCons.OpenConnection(ref conAppCons);
			}
			catch(ApplicationException appException)
			{
				if(conAppCons != null)
				{
					conAppCons.Close();
				}
				Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conAppCons != null)
				{
					conAppCons.Close();
				}
				Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			DbConnection dbConPKG = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConPKG == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conAppPKG = dbConPKG.GetConnection();
			if(conAppPKG == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConPKG.OpenConnection(ref conAppPKG);
			}
			catch(ApplicationException appException)
			{
				if(conAppPKG != null)
				{
					conAppPKG.Close();
				}
				Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conAppPKG != null)
				{
					conAppPKG.Close();
				}
				Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}

			IDbTransaction transactionAppCons = dbConCons.BeginTransaction(conAppCons,IsolationLevel.ReadCommitted);
			if(transactionAppCons == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			IDbTransaction transactionAppPKG = dbConPKG.BeginTransaction(conAppPKG,IsolationLevel.ReadCommitted);
			if(transactionAppPKG == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}



			int i = 0;

			StringBuilder strBuilder = new StringBuilder();

			foreach(DataRow drEach in dsConsignment.Tables[0].Rows)
			{
				try
				{
					if (i == 0)
					{
						strBuilder = new StringBuilder();
						strBuilder.Append("select count(*) from Core_Enterprise");
						dbCmdCons = dbConCons.CreateCommand(strBuilder.ToString() , CommandType.Text);
						dbCmdCons.Connection = conAppCons;
						dbCmdCons.Transaction = transactionAppCons;
						iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
					}
					i++;
					
					bool canBypassCheckingCustomerSender = false;

					#region Check Consignment_no - Not Found
					//Error Checking : Code 1003
					if((drEach["consignment_no"] == null) || (drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) || drEach["consignment_no"].ToString().Trim() == "")
					{
						strBuilder = new StringBuilder();
						strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
						strBuilder.Append("recordtype, applicationid, user_culture,");
						strBuilder.Append("error_code)");
						strBuilder.Append("VALUES(");
						strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
						strBuilder.Append("'" + userId + "',");
						strBuilder.Append("'Consignment',");
						strBuilder.Append("'" + strAppID + "',");
						strBuilder.Append("'" + user_culture + "',");
						strBuilder.Append("1003)");

						dbCmdCons.CommandText = strBuilder.ToString();
						iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);

					}
						#endregion   Check Consignment_no - Not Found

						#region Check Consignment_no - Found
					else
					{
						//Error Checking : Code 1001
						strBuilder = new StringBuilder();
						strBuilder.Append("SELECT * ");	
						strBuilder.Append("FROM Shipment ");
						strBuilder.Append("where applicationid = '");
						strBuilder.Append(strAppID+"'");
						strBuilder.Append(" and enterpriseid = '");
						strBuilder.Append(strEnterpriseID+"'");
						strBuilder.Append(" and consignment_no = '");
						strBuilder.Append(drEach["consignment_no"].ToString().Trim()+"' ");
						//For Preshipment Data
						strBuilder.Append(" and booking_datetime is not null ");

						dbCmdCons.CommandText = strBuilder.ToString();
						DataSet dsTmpShipmentData = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

						//						//Error Checking : Purged Shipment
						//						strBuilder = new StringBuilder();
						//						strBuilder.Append("SELECT * ");	
						//						strBuilder.Append("FROM Purged_Shipment ");
						//						strBuilder.Append("where applicationid = '");
						//						strBuilder.Append(strAppID+"'");
						//						strBuilder.Append(" and enterpriseid = '");
						//						strBuilder.Append(strEnterpriseID+"'");
						//						strBuilder.Append(" and consignment_no = '");
						//						strBuilder.Append(drEach["consignment_no"].ToString().Trim()+"' ");
						//
						//						dbCmdCons.CommandText = strBuilder.ToString();
						//						DataSet dsPurged = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

						if(dsTmpShipmentData.Tables[0].Rows.Count > 0 )//|| dsPurged.Tables[0].Rows.Count > 0 )
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Consignment',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1001)");

							dbCmdCons.CommandText = strBuilder.ToString();
							iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
						}
						dsTmpShipmentData.Dispose();
						dsTmpShipmentData = null;

						//						dsPurged.Dispose();
						//						dsPurged = null;

						//Error Checking : Code 1002
						if(dsConsignment.Tables[0].Select("consignment_no = '" + drEach["consignment_no"].ToString().Trim() + "'").Length > 1)
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Consignment',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1002)");

							dbCmdCons.CommandText = strBuilder.ToString();
							iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
						}

						//Error Checking : Code 1004
						if(dsPackage.Tables[0].Select("consignment_no = '" + drEach["consignment_no"].ToString().Trim() + "'").Length == 0)
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Consignment',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1004)");

							dbCmdCons.CommandText = strBuilder.ToString();
							iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
						}
					}
					#endregion Check Consignment_no - Found

					#region Check Booking_no
					if((drEach["booking_no"] != null) && (!drEach["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["booking_no"].ToString() != ""))
					{
						strBuilder = new StringBuilder();
						strBuilder.Append("SELECT * ");	
						strBuilder.Append("FROM Pickup_Request ");
						strBuilder.Append("where applicationid = '");
						strBuilder.Append(strAppID+"'");
						strBuilder.Append(" and enterpriseid = '");
						strBuilder.Append(strEnterpriseID+"'");
						strBuilder.Append(" and booking_no = '");
						strBuilder.Append(drEach["booking_no"].ToString().Trim()+"'");

						dbCmdCons.CommandText = strBuilder.ToString();
						DataSet dsTmpBookingData = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

						if(dsTmpBookingData.Tables[0].Rows.Count == 0)
						{
							//Error Checking : Code 1011
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Consignment',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1011)");

							dbCmdCons.CommandText = strBuilder.ToString();
							iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);

							canBypassCheckingCustomerSender = false;
						}
						else
						{
							DataRow drTmpBookingData = dsTmpBookingData.Tables[0].Rows[0];

							//Replace drConsignment "payer data" with data from Pickup_Request
							//Jeab 23 Dec 10
							if((drTmpBookingData["booking_datetime"] != null) && 
								(!drTmpBookingData["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drTmpBookingData["booking_datetime"].ToString() != ""))
								drEach["booking_datetime"] = drTmpBookingData["booking_datetime"];
							else
								drEach["booking_datetime"] = "";
							//Jeab 23 Dec 10 ============> End

							if((drTmpBookingData["payerid"] != null) && 
								(!drTmpBookingData["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drTmpBookingData["payerid"].ToString() != ""))
								drEach["payerid"] = drTmpBookingData["payerid"];
							else
								drEach["payerid"] = "";
								
							if((drTmpBookingData["payer_name"] != null) && 
								(!drTmpBookingData["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drTmpBookingData["payer_name"].ToString() != ""))
								drEach["payer_name"] = drTmpBookingData["payer_name"];
							else
								drEach["payer_name"] = "";
					
							if((drTmpBookingData["payer_address1"] != null) && 
								(!drTmpBookingData["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drTmpBookingData["payer_address1"].ToString() != ""))
								drEach["payer_address1"] = drTmpBookingData["payer_address1"];
							else
								drEach["payer_address1"] = "";
					
							if((drTmpBookingData["payer_address2"] != null) && 
								(!drTmpBookingData["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drTmpBookingData["payer_address2"].ToString() != ""))
								drEach["payer_address2"] = drTmpBookingData["payer_address2"];
							else
								drEach["payer_address2"] = "";

							if((drTmpBookingData["payer_zipcode"] != null) && 
								(!drTmpBookingData["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drTmpBookingData["payer_zipcode"].ToString() != ""))
								drEach["payer_zipcode"] = drTmpBookingData["payer_zipcode"];
							else
								drEach["payer_zipcode"] = "";

							if((drTmpBookingData["payer_telephone"] != null) && 
								(!drTmpBookingData["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drTmpBookingData["payer_telephone"].ToString() != ""))
								drEach["payer_telephone"] = drTmpBookingData["payer_telephone"];
							else
								drEach["payer_telephone"] = "";

							if((drTmpBookingData["payer_fax"] != null) && 
								(!drTmpBookingData["payer_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drTmpBookingData["payer_fax"].ToString() != ""))
								drEach["payer_fax"] = drTmpBookingData["payer_fax"];
							else
								drEach["payer_fax"] = "";
							

							if((drTmpBookingData["payment_mode"] != null) && 
								(!drTmpBookingData["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drTmpBookingData["payment_mode"].ToString() != ""))
								drEach["payment_mode"] = drTmpBookingData["payment_mode"];
							else
								drEach["payment_mode"] = "";
							

							//Replace drConsignment "sender data" with data from Pickup_Request
							if((drTmpBookingData["sender_name"] != null) && 
								(!drTmpBookingData["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drTmpBookingData["sender_name"].ToString() != ""))
								drEach["sender_name"] = drTmpBookingData["sender_name"];
							else
								drEach["sender_name"] = "";
						
							if((drTmpBookingData["sender_address1"] != null) && 
								(!drTmpBookingData["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drTmpBookingData["sender_address1"].ToString() != ""))
								drEach["sender_address1"] = drTmpBookingData["sender_address1"];
							else
								drEach["sender_address1"] = "";
					
							if((drTmpBookingData["sender_address2"] != null) && 
								(!drTmpBookingData["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drTmpBookingData["sender_address2"].ToString() != ""))
								drEach["sender_address2"] = drTmpBookingData["sender_address2"];
							else
								drEach["sender_address2"] = "";
							
							if((drTmpBookingData["sender_zipcode"] != null) && 
								(!drTmpBookingData["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drTmpBookingData["sender_zipcode"].ToString() != ""))
								drEach["sender_zipcode"] = drTmpBookingData["sender_zipcode"];
							else
								drEach["sender_zipcode"] = "";

							if((drTmpBookingData["sender_telephone"] != null) && 
								(!drTmpBookingData["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drTmpBookingData["sender_telephone"].ToString() != ""))
								drEach["sender_telephone"] = drTmpBookingData["sender_telephone"];
							else
								drEach["sender_telephone"] = "";
							
							if((drTmpBookingData["sender_fax"] != null) && 
								(!drTmpBookingData["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
								(drTmpBookingData["sender_fax"].ToString() != ""))
								drEach["sender_fax"] = drTmpBookingData["sender_fax"];
							else
								drEach["sender_fax"] = "";
							
							if((drTmpBookingData["sender_contact_person"] != null) && 
								(!drTmpBookingData["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
								(drTmpBookingData["sender_contact_person"].ToString() != ""))
								drEach["sender_contact_person"] = drTmpBookingData["sender_contact_person"];
							else
								drEach["sender_contact_person"] = "";
							
							canBypassCheckingCustomerSender = true;
						}

						dsTmpBookingData.Dispose();
						dsTmpBookingData = null;
					}
					#endregion Check Booking_no

					#region Check 1021
					//Error Checking : Code 1021
					strBuilder = new StringBuilder();
					strBuilder.Append("SELECT * ");	
					strBuilder.Append("FROM service ");
					strBuilder.Append("where applicationid = '");
					strBuilder.Append(strAppID+"'");
					strBuilder.Append(" and enterpriseid = '");
					strBuilder.Append(strEnterpriseID+"'");
					strBuilder.Append(" and service_code = '");
					strBuilder.Append(drEach["service_code"].ToString().Trim()+"'");

					dbCmdCons.CommandText = strBuilder.ToString();
					DataSet dsTmpserviceData = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

					if(dsTmpserviceData	.Tables[0].Rows.Count == 0)
					{
						strBuilder = new StringBuilder();
						strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
						strBuilder.Append("recordtype, applicationid, user_culture,");
						strBuilder.Append("error_code)");
						strBuilder.Append("VALUES(");
						strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
						strBuilder.Append("'" + userId + "',");
						strBuilder.Append("'Consignment',");
						strBuilder.Append("'" + strAppID + "',");
						strBuilder.Append("'" + user_culture + "',");
						strBuilder.Append("1021)");

						dbCmdCons.CommandText = strBuilder.ToString();
						iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
					}

					dsTmpserviceData.Dispose();
					dsTmpserviceData = null;

					#endregion Check 1021

					#region Start Payer & Sender Checking
					//Start Payer & Sender Checking
					if(!canBypassCheckingCustomerSender)
					{
						//Error Checking : Payer N/A
						if((drEach["payerid"] == null) || (drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))) || (drEach["payerid"].ToString() == ""))
						{
							drEach["payerid"] = "NEW";

							//Error Checking : Code 1032
							if(((drEach["payer_address1"] == null) || 
								(drEach["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
								(drEach["payer_address1"].ToString() == "")) || 
								((drEach["payer_address2"] == null) || 
								(drEach["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) ||
								(drEach["payer_address2"].ToString() == "")))
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1032)");

								dbCmdCons.CommandText = strBuilder.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
							}

							//Error Checking : Code 1033
							if((drEach["payer_telephone"] == null) || 
								(drEach["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
								(drEach["payer_telephone"].ToString() == ""))
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1033)");

								dbCmdCons.CommandText = strBuilder.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
							}

							//Error Checking : Code 1034
							if((drEach["payer_zipcode"] != null) && 
								(!drEach["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
								(drEach["payer_zipcode"].ToString() != ""))
							{
						
								strBuilder = new StringBuilder();
								strBuilder.Append("SELECT * ");	
								strBuilder.Append("FROM Zipcode ");
								strBuilder.Append("where applicationid = '");
								strBuilder.Append(strAppID+"'");
								strBuilder.Append(" and enterpriseid = '");
								strBuilder.Append(strEnterpriseID+"'");
								strBuilder.Append(" and zipcode = '");
								strBuilder.Append(drEach["payer_zipcode"].ToString().Trim()+"'");

								dbCmdCons.CommandText = strBuilder.ToString();
								DataSet dsTmpZipcodeData = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

								if(dsTmpZipcodeData.Tables[0].Rows.Count == 0)
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Consignment',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1034)");

									dbCmdCons.CommandText = strBuilder.ToString();
									iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
								}

								dsTmpZipcodeData.Dispose();
								dsTmpZipcodeData = null;
							}
							else
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1034)");

								dbCmdCons.CommandText = strBuilder.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
							}

							drEach["new_account"] = "Y";
						}
						else
						{
							if (drEach["payerid"].ToString().Trim() == "NEW")
							{
								drEach["payerid"] = "NEW";

								//Error Checking : Code 1032
								if(((drEach["payer_address1"] == null) || 
									(drEach["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
									(drEach["payer_address1"].ToString() == "")) || 
									((drEach["payer_address2"] == null) || 
									(drEach["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) ||
									(drEach["payer_address2"].ToString() == "")))
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Consignment',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1032)");

									dbCmdCons.CommandText = strBuilder.ToString();
									iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
								}

								//Error Checking : Code 1033
								if((drEach["payer_telephone"] == null) || 
									(drEach["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
									(drEach["payer_telephone"].ToString() == ""))
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Consignment',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1033)");

									dbCmdCons.CommandText = strBuilder.ToString();
									iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
								}

								//Error Checking : Code 1034
								if((drEach["payer_zipcode"] != null) && 
									(!drEach["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
									(drEach["payer_zipcode"].ToString() != ""))
								{
						
									strBuilder = new StringBuilder();
									strBuilder.Append("SELECT * ");	
									strBuilder.Append("FROM Zipcode ");
									strBuilder.Append("where applicationid = '");
									strBuilder.Append(strAppID+"'");
									strBuilder.Append(" and enterpriseid = '");
									strBuilder.Append(strEnterpriseID+"'");
									strBuilder.Append(" and zipcode = '");
									strBuilder.Append(drEach["payer_zipcode"].ToString().Trim()+"'");

									dbCmdCons.CommandText = strBuilder.ToString();
									DataSet dsTmpZipcodeData = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

									if(dsTmpZipcodeData.Tables[0].Rows.Count == 0)
									{
										strBuilder = new StringBuilder();
										strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
										strBuilder.Append("recordtype, applicationid, user_culture,");
										strBuilder.Append("error_code)");
										strBuilder.Append("VALUES(");
										strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
										strBuilder.Append("'" + userId + "',");
										strBuilder.Append("'Consignment',");
										strBuilder.Append("'" + strAppID + "',");
										strBuilder.Append("'" + user_culture + "',");
										strBuilder.Append("1034)");

										dbCmdCons.CommandText = strBuilder.ToString();
										iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
									}

									dsTmpZipcodeData.Dispose();
									dsTmpZipcodeData = null;
								}
								else
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Consignment',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1034)");

									dbCmdCons.CommandText = strBuilder.ToString();
									iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
								}

								drEach["new_account"] = "Y";
							}
							else
							{
								//Error Checking : Code 1035
								Customer customer = new Customer();
								customer.Populate(strAppID, strEnterpriseID,drEach["payerid"].ToString().Trim());
								if(customer.IsCustomer_Box == "Y")
								{
									String strSendZipCode="";
									String strRecipZipCode = "";
									if(!((drEach["sender_zipcode"] == null) || (drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) || (drEach["sender_zipcode"].ToString() == "")))
										strSendZipCode = drEach["sender_zipcode"].ToString().Trim();	
									if(!(((drEach["recipient_zipcode"] == null) || (drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) || (drEach["recipient_zipcode"].ToString() == ""))))
										strRecipZipCode = drEach["recipient_zipcode"].ToString().Trim();
									//									Zipcode zipcode = new Zipcode();
									//									zipcode.Populate(strAppID, strEnterpriseID,strSendZipCode);
									//									String strOrgZone = zipcode.ZoneCode;
									//									zipcode.Populate(strAppID, strEnterpriseID ,strRecipZipCode);
									//									String strDestnZone = zipcode.ZoneCode;

									DataSet tmpCustZone = null;
									string sendCustZone = null;
									string recipCustZone = null;
									tmpCustZone = SysDataMgrDAL.GetCustZoneData(strAppID, strEnterpriseID,
										0, 0, drEach["payerid"].ToString().Trim(), strSendZipCode).ds;
									if(tmpCustZone.Tables[0].Rows.Count > 0)
									{
										foreach(DataRow dr in tmpCustZone.Tables[0].Rows)
										{
											sendCustZone = dr["zone_code"].ToString().Trim();
										}
									}
									tmpCustZone = SysDataMgrDAL.GetCustZoneData(strAppID, strEnterpriseID,
										0, 0, drEach["payerid"].ToString().Trim(), strRecipZipCode).ds;
									if(tmpCustZone.Tables[0].Rows.Count > 0)
									{
										foreach(DataRow dr in tmpCustZone.Tables[0].Rows)
										{
											recipCustZone = dr["zone_code"].ToString().Trim();
										}
									}
									if(!customer.IsBoxRateAvailable(strAppID, strEnterpriseID,sendCustZone,recipCustZone,drEach["service_code"].ToString().Trim()))
									{
										strBuilder = new StringBuilder();
										strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
										strBuilder.Append("recordtype, applicationid, user_culture,");
										strBuilder.Append("error_code)");
										strBuilder.Append("VALUES(");
										strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
										strBuilder.Append("'" + userId + "',");
										strBuilder.Append("'Consignment',");
										strBuilder.Append("'" + strAppID + "',");
										strBuilder.Append("'" + user_culture + "',");
										strBuilder.Append("1035)");

										dbCmdCons.CommandText = strBuilder.ToString();
										iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
									}
								}
								//Error Checking : Code 1031
								strBuilder = new StringBuilder();
								strBuilder.Append("SELECT * ");	
								strBuilder.Append("FROM Customer ");
								strBuilder.Append("where applicationid = '");
								strBuilder.Append(strAppID+"'");
								strBuilder.Append(" and enterpriseid = '");
								strBuilder.Append(strEnterpriseID+"'");
								strBuilder.Append(" and custid = '");
								strBuilder.Append(drEach["payerid"].ToString().Trim()+"'");

								dbCmdCons.CommandText = strBuilder.ToString();
								DataSet dsTmpCustomerData = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

								if (dsTmpCustomerData.Tables[0].Rows.Count == 0)
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Consignment',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1031)");

									dbCmdCons.CommandText = strBuilder.ToString();
									iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);

									//Error Checking : Code 1032
									if(((drEach["payer_address1"] == null) || 
										(drEach["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
										(drEach["payer_address1"].ToString() == "")) || 
										((drEach["payer_address2"] == null) || 
										(drEach["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) ||
										(drEach["payer_address2"].ToString() == "")))
									{
										strBuilder = new StringBuilder();
										strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
										strBuilder.Append("recordtype, applicationid, user_culture,");
										strBuilder.Append("error_code)");
										strBuilder.Append("VALUES(");
										strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
										strBuilder.Append("'" + userId + "',");
										strBuilder.Append("'Consignment',");
										strBuilder.Append("'" + strAppID + "',");
										strBuilder.Append("'" + user_culture + "',");
										strBuilder.Append("1032)");

										dbCmdCons.CommandText = strBuilder.ToString();
										iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
									}

									//Error Checking : Code 1033
									if((drEach["payer_telephone"] == null) || 
										(drEach["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
										(drEach["payer_telephone"].ToString() == ""))
									{
										strBuilder = new StringBuilder();
										strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
										strBuilder.Append("recordtype, applicationid, user_culture,");
										strBuilder.Append("error_code)");
										strBuilder.Append("VALUES(");
										strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
										strBuilder.Append("'" + userId + "',");
										strBuilder.Append("'Consignment',");
										strBuilder.Append("'" + strAppID + "',");
										strBuilder.Append("'" + user_culture + "',");
										strBuilder.Append("1033)");

										dbCmdCons.CommandText = strBuilder.ToString();
										iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
									}

									//Error Checking : Code 1034
									if((drEach["payer_zipcode"] != null) && 
										(!drEach["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
										(drEach["payer_zipcode"].ToString() != ""))
									{
						
										strBuilder = new StringBuilder();
										strBuilder.Append("SELECT * ");	
										strBuilder.Append("FROM Zipcode ");
										strBuilder.Append("where applicationid = '");
										strBuilder.Append(strAppID+"'");
										strBuilder.Append(" and enterpriseid = '");
										strBuilder.Append(strEnterpriseID+"'");
										strBuilder.Append(" and zipcode = '");
										strBuilder.Append(drEach["payer_zipcode"].ToString().Trim()+"'");

										dbCmdCons.CommandText = strBuilder.ToString();
										DataSet dsTmpZipcodeData = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

										if(dsTmpZipcodeData.Tables[0].Rows.Count == 0)
										{
											strBuilder = new StringBuilder();
											strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
											strBuilder.Append("recordtype, applicationid, user_culture,");
											strBuilder.Append("error_code)");
											strBuilder.Append("VALUES(");
											strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
											strBuilder.Append("'" + userId + "',");
											strBuilder.Append("'Consignment',");
											strBuilder.Append("'" + strAppID + "',");
											strBuilder.Append("'" + user_culture + "',");
											strBuilder.Append("1034)");

											dbCmdCons.CommandText = strBuilder.ToString();
											iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
										}

										dsTmpZipcodeData.Dispose();
										dsTmpZipcodeData = null;
									}
									else
									{
										strBuilder = new StringBuilder();
										strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
										strBuilder.Append("recordtype, applicationid, user_culture,");
										strBuilder.Append("error_code)");
										strBuilder.Append("VALUES(");
										strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
										strBuilder.Append("'" + userId + "',");
										strBuilder.Append("'Consignment',");
										strBuilder.Append("'" + strAppID + "',");
										strBuilder.Append("'" + user_culture + "',");
										strBuilder.Append("1034)");

										dbCmdCons.CommandText = strBuilder.ToString();
										iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
									}
								}
								else
								{
									DataRow drTmpCustomerData = dsTmpCustomerData.Tables[0].Rows[0];

									///BOON - Check Error: Code 1036 for ImportToTempTables -- start
									if(drTmpCustomerData["status_active"].ToString() == "N")
									{
										strBuilder = new StringBuilder();
										strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
										strBuilder.Append("recordtype, applicationid, user_culture,");
										strBuilder.Append("error_code)");
										strBuilder.Append("VALUES(");
										strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
										strBuilder.Append("'" + userId + "',");
										strBuilder.Append("'Consignment',");
										strBuilder.Append("'" + strAppID + "',");
										strBuilder.Append("'" + user_culture + "',");
										strBuilder.Append("1036)");

										dbCmdCons.CommandText = strBuilder.ToString();
										iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
									}
									///BOON - Check Error: Code 1036 for ImportToTempTables -- end

									//Removed by CRTS 954 Turn off Locking from over Credit Limit
									//Error Checking : Code 1037 Credit Available
									//									if((drTmpCustomerData["creditstatus"] == null) || 
									//										(drTmpCustomerData["creditstatus"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
									//										(drTmpCustomerData["creditstatus"].ToString() == ""))
									//									{
									//										strBuilder = new StringBuilder();
									//										strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									//										strBuilder.Append("recordtype, applicationid, user_culture,");
									//										strBuilder.Append("error_code)");
									//										strBuilder.Append("VALUES(");
									//										strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
									//										strBuilder.Append("'" + userId + "',");
									//										strBuilder.Append("'Consignment',");
									//										strBuilder.Append("'" + strAppID + "',");
									//										strBuilder.Append("'" + user_culture + "',");
									//										strBuilder.Append("1037)");
									//
									//										dbCmdCons.CommandText = strBuilder.ToString();
									//										iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
									//									}
									//									else
									//									{
									//										if(drTmpCustomerData["creditstatus"].ToString().Equals("N"))
									//										{
									//											strBuilder = new StringBuilder();
									//											strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									//											strBuilder.Append("recordtype, applicationid, user_culture,");
									//											strBuilder.Append("error_code)");
									//											strBuilder.Append("VALUES(");
									//											strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
									//											strBuilder.Append("'" + userId + "',");
									//											strBuilder.Append("'Consignment',");
									//											strBuilder.Append("'" + strAppID + "',");
									//											strBuilder.Append("'" + user_culture + "',");
									//											strBuilder.Append("1037)");
									//
									//											dbCmdCons.CommandText = strBuilder.ToString();
									//											iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
									//										}
									//									}//END Error Checking : Code 1037 Credit Available

									//Replace drConsignment "payer data" with data from Customer
									if((drTmpCustomerData["custid"] != null) && 
										(!drTmpCustomerData["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(drTmpCustomerData["custid"].ToString() != ""))
										drEach["payerid"] = drTmpCustomerData["custid"];
									else
										drEach["payerid"] = "";
						
									if((drTmpCustomerData["cust_name"] != null) && 
										(!drTmpCustomerData["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(drTmpCustomerData["cust_name"].ToString() != ""))
										drEach["payer_name"] = drTmpCustomerData["cust_name"];
									else
										drEach["payer_name"] = "";
								
									if((drTmpCustomerData["address1"] != null) && 
										(!drTmpCustomerData["address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(drTmpCustomerData["address1"].ToString() != ""))
										drEach["payer_address1"] = drTmpCustomerData["address1"];
									else
										drEach["payer_address1"] = "";

									if((drTmpCustomerData["address2"] != null) && 
										(!drTmpCustomerData["address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(drTmpCustomerData["address2"].ToString() != ""))
										drEach["payer_address2"] = drTmpCustomerData["address2"];
									else
										drEach["payer_address2"] = "";

									if((drTmpCustomerData["zipcode"] != null) && 
										(!drTmpCustomerData["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(drTmpCustomerData["zipcode"].ToString() != ""))
										drEach["payer_zipcode"] = drTmpCustomerData["zipcode"];
									else
										drEach["payer_zipcode"] = "";

									if((drTmpCustomerData["telephone"] != null) && 
										(!drTmpCustomerData["telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(drTmpCustomerData["telephone"].ToString() != ""))
										drEach["payer_telephone"] = drTmpCustomerData["telephone"];
									else
										drEach["payer_telephone"] = "";
								
									if((drTmpCustomerData["fax"] != null) && 
										(!drTmpCustomerData["fax"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(drTmpCustomerData["zipcode"].ToString() != ""))
										drEach["payer_fax"] = drTmpCustomerData["fax"];
									else
										drEach["payer_fax"] = "";
								
									if((drTmpCustomerData["payment_mode"] != null) && 
										(!drTmpCustomerData["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(drTmpCustomerData["payment_mode"].ToString() != ""))
										drEach["payment_mode"] = drTmpCustomerData["payment_mode"];
									else
										drEach["payment_mode"] = "";
								}

								dsTmpCustomerData.Dispose();
								dsTmpCustomerData = null;

								drEach["new_account"] = "N";
							}
						}

						//Error Checking : Sender N/A
						strBuilder = new StringBuilder();
						strBuilder.Append("SELECT * ");	
						strBuilder.Append("FROM Customer_Snd_Rec ");
						strBuilder.Append("where applicationid = '");
						strBuilder.Append(strAppID+"'");
						strBuilder.Append(" and enterpriseid = '");
						strBuilder.Append(strEnterpriseID+"'");
						strBuilder.Append(" and snd_rec_name = '");
						strBuilder.Append(drEach["sender_name"].ToString().Trim()+"'");
						//boon 2010/12/20
						strBuilder.Append(" and custid = '" + drEach["payerid"].ToString().Trim() + "'");


						dbCmdCons.CommandText = strBuilder.ToString();
						DataSet dsTmpSenderData = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

						if (dsTmpSenderData.Tables[0].Rows.Count > 0)
						{
							DataRow drSenderDataTmp = dsTmpSenderData.Tables[0].Rows[0];

							//Replace drConsignment "sender data" with data from Cust_Snd_Rec
							if((drSenderDataTmp["snd_rec_name"] != null) && 
								(!drSenderDataTmp["snd_rec_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drSenderDataTmp["snd_rec_name"].ToString() != ""))
								drEach["sender_name"] = drSenderDataTmp["snd_rec_name"];
							else
								drEach["sender_name"] = "";
							
							if((drSenderDataTmp["address1"] != null) && 
								(!drSenderDataTmp["address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drSenderDataTmp["address1"].ToString() != ""))
								drEach["sender_address1"] = drSenderDataTmp["address1"];
							else
								drEach["sender_address1"] = "";
							
							if((drSenderDataTmp["address2"] != null) && 
								(!drSenderDataTmp["address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drSenderDataTmp["address2"].ToString() != ""))
								drEach["sender_address2"] = drSenderDataTmp["address2"];
							else
								drEach["sender_address2"] = "";
					
							if((drSenderDataTmp["zipcode"] != null) && 
								(!drSenderDataTmp["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drSenderDataTmp["zipcode"].ToString() != ""))
								drEach["sender_zipcode"] = drSenderDataTmp["zipcode"];
							else
								drEach["sender_zipcode"] = "";
							
							if((drSenderDataTmp["telephone"] != null) && 
								(!drSenderDataTmp["telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drSenderDataTmp["telephone"].ToString() != ""))
								drEach["sender_telephone"] = drSenderDataTmp["telephone"];
							else
								drEach["sender_telephone"] = "";
							
							if((drSenderDataTmp["fax"] != null) && 
								(!drSenderDataTmp["fax"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drSenderDataTmp["fax"].ToString() != ""))
								drEach["sender_fax"] = drSenderDataTmp["fax"];
							else
								drEach["sender_fax"] = "";

							if((drSenderDataTmp["contact_person"] != null) && 
								(!drSenderDataTmp["contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drSenderDataTmp["contact_person"].ToString() != ""))
								drEach["sender_contact_person"] = drSenderDataTmp["contact_person"];
							else
								drEach["sender_contact_person"] = "";
						}
						else
						{
							if((drEach["sender_zipcode"] != null) && (!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["sender_zipcode"].ToString() != ""))
							{
								//Error Checking : Code 1051
								strBuilder = new StringBuilder();
								strBuilder.Append("SELECT * ");	
								strBuilder.Append("FROM Zipcode ");
								strBuilder.Append("where applicationid = '");
								strBuilder.Append(strAppID+"'");
								strBuilder.Append(" and enterpriseid = '");
								strBuilder.Append(strEnterpriseID+"'");
								strBuilder.Append(" and zipcode = '");
								strBuilder.Append(drEach["sender_zipcode"].ToString().Trim()+"'");

								dbCmdCons.CommandText = strBuilder.ToString();
								DataSet dsTmpSenderZipData = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

								if(dsTmpSenderZipData.Tables[0].Rows.Count == 0)
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Consignment',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1051)");

									dbCmdCons.CommandText = strBuilder.ToString();
									iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
								}

								dsTmpSenderZipData.Dispose();
								dsTmpSenderZipData = null;
							}
							else
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1051)");

								dbCmdCons.CommandText = strBuilder.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
							}

							//Error Checking : Code 1052
							if(((drEach["sender_address1"] == null) || 
								(drEach["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
								(drEach["sender_address1"].ToString() == "")) || 
								((drEach["sender_address2"] == null) || 
								(drEach["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) ||
								(drEach["sender_address2"].ToString() == "")))
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1052)");

								dbCmdCons.CommandText = strBuilder.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
							}

							//Error Checking : Code 1053
							if((drEach["sender_telephone"] == null) || 
								(drEach["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
								(drEach["sender_telephone"].ToString() == ""))
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1053)");

								dbCmdCons.CommandText = strBuilder.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
							}

							//Error Checking : Code 1054
							if((drEach["sender_contact_person"] == null) || 
								(drEach["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
								(drEach["sender_contact_person"].ToString() == ""))
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1054)");

								dbCmdCons.CommandText = strBuilder.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
							}

							//Error Checking : Code 1055
							if((drEach["sender_name"] == null) || 
								(drEach["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
								(drEach["sender_name"].ToString() == ""))
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1055)");

								dbCmdCons.CommandText = strBuilder.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
							}
						}

						dsTmpSenderData.Dispose();
						dsTmpSenderData = null;
					}
					//End  Payer & Sender Checking
					#endregion Start Payer & Sender Checking

					#region Check code 1041
					//Error Checking : Code 1041
					if((drEach["payment_mode"].ToString().Trim() != "R") && (drEach["payment_mode"].ToString().Trim() != "C"))
					{
						strBuilder = new StringBuilder();
						strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
						strBuilder.Append("recordtype, applicationid, user_culture,");
						strBuilder.Append("error_code)");
						strBuilder.Append("VALUES(");
						strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
						strBuilder.Append("'" + userId + "',");
						strBuilder.Append("'Consignment',");
						strBuilder.Append("'" + strAppID + "',");
						strBuilder.Append("'" + user_culture + "',");
						strBuilder.Append("1041)");

						dbCmdCons.CommandText = strBuilder.ToString();
						iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);

						drEach["payment_mode"] = System.DBNull.Value;
					}
					#endregion Check code 1041

					#region Check 1==2
					if(1==2)//(drEach["recipient_telephone"] != null) && (!drEach["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["recipient_telephone"].ToString() != "")) 
					{
						//Error Checking : Recipient N/A
						strBuilder = new StringBuilder();
						strBuilder.Append("SELECT * ");	
						strBuilder.Append("FROM v_References ");
						strBuilder.Append("where applicationid = '");
						strBuilder.Append(strAppID+"'");
						strBuilder.Append(" and enterpriseid = '");
						strBuilder.Append(strEnterpriseID+"'");
						strBuilder.Append(" and telephone = '");
						strBuilder.Append(drEach["recipient_telephone"].ToString().Trim()+"'");

						dbCmdCons.CommandText = strBuilder.ToString();
						DataSet dsTmpReferenceData = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

						if (1==2)
						{
							DataRow drReferenceDataTmp = dsTmpReferenceData.Tables[0].Rows[0];

							//Replace drConsignment "recipient data" with data from References
							if((drReferenceDataTmp["reference_name"] != null) && 
								(!drReferenceDataTmp["reference_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drReferenceDataTmp["reference_name"].ToString() != ""))
								drEach["recipient_name"] = drReferenceDataTmp["reference_name"];
							else
								drEach["recipient_name"]="";

							if((drReferenceDataTmp["address1"] != null) && 
								(!drReferenceDataTmp["address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drReferenceDataTmp["address1"].ToString() != ""))
								drEach["recipient_address1"] = drReferenceDataTmp["address1"];
							else
								drEach["recipient_address1"] = "";
								
							if((drReferenceDataTmp["address2"] != null) && 
								(!drReferenceDataTmp["address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drReferenceDataTmp["address2"].ToString() != ""))
								drEach["recipient_address2"] = drReferenceDataTmp["address2"];
							else
								drEach["recipient_address2"] = "";
								
							if((drReferenceDataTmp["zipcode"] != null) && 
								(!drReferenceDataTmp["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drReferenceDataTmp["zipcode"].ToString() != ""))
								drEach["recipient_zipcode"] = drReferenceDataTmp["zipcode"];
							else
								drEach["recipient_zipcode"] = "";
								
							if((drReferenceDataTmp["fax"] != null) && 
								(!drReferenceDataTmp["fax"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drReferenceDataTmp["fax"].ToString() != "") && (drReferenceDataTmp["fax"].ToString().ToUpper() != "NULL".ToUpper()))
								drEach["recipient_fax"] = drReferenceDataTmp["fax"];
							else
								drEach["recipient_fax"] = "";
						
							if((drReferenceDataTmp["contactperson"] != null) && 
								(!drReferenceDataTmp["contactperson"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drReferenceDataTmp["contactperson"].ToString() != ""))
								drEach["recipient_contact_person"] = drReferenceDataTmp["contactperson"];
							else
								drEach["recipient_contact_person"] = "";
						}

						dsTmpReferenceData.Dispose();
						dsTmpReferenceData = null;
						//}
					}
						#endregion Check 1==2
						
						#region Check 1==2 >> else
					else
					{
						//Error Checking : Code 1061
						if((drEach["recipient_zipcode"] == null) || 
							(drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
							(drEach["recipient_zipcode"].ToString() == ""))
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Consignment',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1061)");

							dbCmdCons.CommandText = strBuilder.ToString();
							iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
						}
						else
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("SELECT * ");	
							strBuilder.Append("FROM Zipcode ");
							strBuilder.Append("where applicationid = '");
							strBuilder.Append(strAppID+"'");
							strBuilder.Append(" and enterpriseid = '");
							strBuilder.Append(strEnterpriseID+"'");
							strBuilder.Append(" and zipcode = '");
							strBuilder.Append(drEach["recipient_zipcode"].ToString().Trim()+"'");

							dbCmdCons.CommandText = strBuilder.ToString();
							DataSet dsTmpZipcodeData = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

							if(dsTmpZipcodeData.Tables[0].Rows.Count == 0)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1061)");

								dbCmdCons.CommandText = strBuilder.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
							}

							dsTmpZipcodeData.Dispose();
							dsTmpZipcodeData = null;
						}

						//Error Checking : Code 1062
						if(((drEach["recipient_address1"] == null) || 
							(drEach["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
							(drEach["recipient_address1"].ToString().Trim() == "")) || 
							((drEach["recipient_address2"] == null) || 
							(drEach["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) ||
							(drEach["recipient_address2"].ToString().Trim() == "")))
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Consignment',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1062)");

							dbCmdCons.CommandText = strBuilder.ToString();
							iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
						}
						else
						{
							//Error Checking : Code 1068
							if (drEach["recipient_address1"].ToString().Length > 100)
							{
								drEach["recipient_address1"] = drEach["recipient_address1"].ToString().Substring(0, 100); 

								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1068)");

								dbCmdCons.CommandText = strBuilder.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
							}

							//Error Checking : Code 1069
							if (drEach["recipient_address2"].ToString().Length > 100)
							{
								drEach["recipient_address2"] = drEach["recipient_address2"].ToString().Substring(0, 100); 

								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1069)");

								dbCmdCons.CommandText = strBuilder.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
							}
						}

						//Error Checking : Code 1063
						if((drEach["recipient_telephone"] == null) || 
							(drEach["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
							(drEach["recipient_telephone"].ToString() == ""))
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Consignment',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1063)");

							dbCmdCons.CommandText = strBuilder.ToString();
							iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
						}
						else
						{
							//Error Checking : Code 1066
							if (drEach["recipient_telephone"].ToString().Length > 20)
							{
								drEach["recipient_telephone"] = drEach["recipient_telephone"].ToString().Substring(0, 20); 

								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1066)");

								dbCmdCons.CommandText = strBuilder.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
							}
						}

						//Error Checking : Code 1067
						if((drEach["recipient_fax"] != null) || 
							(!drEach["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
							(drEach["recipient_fax"].ToString() != ""))
						{
							if (drEach["recipient_fax"].ToString().Length > 20)
							{
								drEach["recipient_fax"] = drEach["recipient_fax"].ToString().Substring(0, 20); 

								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1067)");

								dbCmdCons.CommandText = strBuilder.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
							}
						}

						//Error Checking : Code 1064
						//						if((drEach["recipient_contact_person"] == null) || 
						//							(drEach["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
						//							(drEach["recipient_contact_person"].ToString() == ""))
						//						{
						//							strBuilder = new StringBuilder();
						//							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
						//							strBuilder.Append("recordtype, applicationid, user_culture,");
						//							strBuilder.Append("error_code)");
						//							strBuilder.Append("VALUES(");
						//							strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
						//							strBuilder.Append("'" + userId + "',");
						//							strBuilder.Append("'Consignment',");
						//							strBuilder.Append("'" + strAppID + "',");
						//							strBuilder.Append("'" + user_culture + "',");
						//							strBuilder.Append("1064)");
						//
						//							dbCmdCons.CommandText = strBuilder.ToString();
						//							iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
						//						}

						//Error Checking : Code 1065
						if((drEach["recipient_name"] == null) || 
							(drEach["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
							(drEach["recipient_name"].ToString() == ""))
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Consignment',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1065)");

							dbCmdCons.CommandText = strBuilder.ToString();
							iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
						}
					}
					#endregion Check 1==2 >> else

					#region Check recipient_zipcode
					if((drEach["recipient_zipcode"] != null) && 
						(!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
						(drEach["recipient_zipcode"].ToString() != ""))
					{
						//Error Checking : Code 1022
						String strSendZipCode="";
						String strRecipZipCode = "";
						String strServiceCode = "";

						if(!((drEach["sender_zipcode"] == null) || (drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) || (drEach["sender_zipcode"].ToString() == "")))
							strSendZipCode = drEach["sender_zipcode"].ToString().Trim();
	
						if(!(((drEach["recipient_zipcode"] == null) || (drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) || (drEach["recipient_zipcode"].ToString() == ""))))
							strRecipZipCode = drEach["recipient_zipcode"].ToString().Trim();

						if(!((drEach["service_code"] == null) || (drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) || (drEach["service_code"].ToString() == "")))
							strServiceCode = drEach["service_code"].ToString().Trim();


						if(!Service.IsAvailable(strAppID,strEnterpriseID,strServiceCode,strSendZipCode,strRecipZipCode))//(dsTmpZipcode_Service_ExcludedData.Tables[0].Rows.Count <= 0)
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Consignment',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1022)");

							dbCmdCons.CommandText = strBuilder.ToString();
							iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
						}

					}
					#endregion Check recipient_zipcode

					#region Check Declare_Value
					//Error Checking : Declare Value N/A
					if((drEach["declare_value"] != null) && (!drEach["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["declare_value"].ToString() != ""))
					{
						if(!IsNumeric(drEach["declare_value"].ToString())) 
						{
							drEach["declare_value"] = 0;
						}
						//						else		//commment by boon -- confirm by Express Team (Kai), 2010/12/20
						//						{
						//							//Error Checking : Code 1071
						//							strBuilder = new StringBuilder();
						//							strBuilder.Append("SELECT max_insurance_amt ");	
						//							strBuilder.Append("FROM Enterprise ");
						//							strBuilder.Append("where applicationid = '");
						//							strBuilder.Append(strAppID+"'");
						//							strBuilder.Append(" and enterpriseid = '");
						//							strBuilder.Append(strEnterpriseID+"'");
						//
						//							dbCmdCons.CommandText = strBuilder.ToString();
						//							DataSet dsEnterprise = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  
						//
						//							if(dsEnterprise.Tables[0].Rows.Count > 0)
						//							{
						//								if(Convert.ToDecimal(dsEnterprise.Tables[0].Rows[0]["max_insurance_amt"]) < Convert.ToDecimal(drEach["declare_value"]))
						//								{
						//									strBuilder = new StringBuilder();
						//									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
						//									strBuilder.Append("recordtype, applicationid, user_culture,");
						//									strBuilder.Append("error_code)");
						//									strBuilder.Append("VALUES(");
						//									strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
						//									strBuilder.Append("'" + userId + "',");
						//									strBuilder.Append("'Consignment',");
						//									strBuilder.Append("'" + strAppID + "',");
						//									strBuilder.Append("'" + user_culture + "',");
						//									strBuilder.Append("1071)");
						//
						//									dbCmdCons.CommandText = strBuilder.ToString();
						//									iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
						//								}
						//							}
						//
						//							dsEnterprise.Dispose();
						//							dsEnterprise = null;
						//						}
					}
					else
					{
						drEach["declare_value"] = 0;
					}
					#endregion Check Declare_Value

					#region Check COD Amount
					//Error Checking : COD Amount N/A
					if((drEach["cod_amount"] != null) && (!drEach["cod_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["cod_amount"].ToString() != ""))
					{
						if(!IsNumeric(drEach["cod_amount"].ToString())) 
						{
							drEach["cod_amount"] = 0;
						}
						else
						{
							///Boon - Error Checking : Code 1072 for ImportToTempTables -- start
							Regex regCOD1 = new Regex(@"^[+-]?(?:\d+|\d{1,3}(?:,\d{3})+|(?:\d*|\d{1,3}(?:,\d{3})+)\.(\d{1}|\d{2}))$");

							if ((regCOD1.IsMatch(drEach["cod_amount"].ToString()) == false) || (Convert.ToDouble(drEach["cod_amount"]) > 99999999.99))
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1072)");

								dbCmdCons.CommandText = strBuilder.ToString();
								iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
							}
							///Boon - Check Error: Code 1072 for ImportToTempTables -- end
						}
					}
					else
					{
						drEach["cod_amount"] = 0;
					}
					#endregion Check COD Amount

					#region Check Code 1081
					//Error Checking : Code 1081
					if((drEach["payment_type"].ToString().Trim() != "FP") && (drEach["payment_type"].ToString().Trim() != "FC"))
					{
						strBuilder = new StringBuilder();
						strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
						strBuilder.Append("recordtype, applicationid, user_culture,");
						strBuilder.Append("error_code)");
						strBuilder.Append("VALUES(");
						strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
						strBuilder.Append("'" + userId + "',");
						strBuilder.Append("'Consignment',");
						strBuilder.Append("'" + strAppID + "',");
						strBuilder.Append("'" + user_culture + "',");
						strBuilder.Append("1081)");

						dbCmdCons.CommandText = strBuilder.ToString();
						iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);

						drEach["payment_type"] = System.DBNull.Value;
					}
					#endregion Check Code 1081

					#region Check Code 1091
					//Error Checking : Code 1091
					if((drEach["commodity_code"] != null) && (!drEach["commodity_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["commodity_code"].ToString() != ""))
					{
						strBuilder = new StringBuilder();
						strBuilder.Append("SELECT * ");	
						strBuilder.Append("FROM Commodity ");
						strBuilder.Append("where applicationid = '");
						strBuilder.Append(strAppID+"'");
						strBuilder.Append(" and enterpriseid = '");
						strBuilder.Append(strEnterpriseID+"'");
						strBuilder.Append(" and commodity_code = '");
						strBuilder.Append(drEach["commodity_code"].ToString().Trim()+"'");

						dbCmdCons.CommandText = strBuilder.ToString();
						DataSet dsTmpCommodityData = (DataSet)dbConCons.ExecuteQueryInTransaction(dbCmdCons, ReturnType.DataSetType);  

						if(dsTmpCommodityData.Tables[0].Rows.Count == 0)
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Consignment',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1091)");

							dbCmdCons.CommandText = strBuilder.ToString();
							iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
						}

						dsTmpCommodityData.Dispose();
						dsTmpCommodityData = null;
					}
					//					else
					//					{
					//						strBuilder = new StringBuilder();
					//						strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
					//						strBuilder.Append("recordtype, applicationid, user_culture,");
					//						strBuilder.Append("error_code)");
					//						strBuilder.Append("VALUES(");
					//						strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
					//						strBuilder.Append("'" + userId + "',");
					//						strBuilder.Append("'Consignment',");
					//						strBuilder.Append("'" + strAppID + "',");
					//						strBuilder.Append("'" + user_culture + "',");
					//						strBuilder.Append("1091)");
					//
					//						dbCmdCons.CommandText = strBuilder.ToString();
					//						iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
					//					}
					#endregion Check Code 1091

					#region Check Code 1201
					//HC Return Task
					//Error Checking : Code 1201
					if((drEach["return_pod_hc"] != null) && 
						(!drEach["return_pod_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
						(drEach["return_pod_hc"].ToString() != ""))
					{
						if((drEach["return_pod_hc"].ToString().Trim() != "D") && 
							(drEach["return_pod_hc"].ToString().Trim() != "Y") &&
							(drEach["return_pod_hc"].ToString().Trim() != "N"))
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Consignment',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1201)");

							dbCmdCons.CommandText = strBuilder.ToString();
							iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
						}
					}
					#endregion Check Code 1201

					#region Check Code 1202
					//Error Checking : Code 1202
					if((drEach["return_invoice_hc"] != null) && 
						(!drEach["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
						(drEach["return_invoice_hc"].ToString() != ""))
					{
						if((drEach["return_invoice_hc"].ToString().Trim() != "D") && 
							(drEach["return_invoice_hc"].ToString().Trim() != "Y") &&
							(drEach["return_invoice_hc"].ToString().Trim() != "N"))
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Consignment',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1202)");

							dbCmdCons.CommandText = strBuilder.ToString();
							iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);
						}
					}
					//HC Return Task
					#endregion Check Code 1202

					#region Insert Consignment
					dsConsignment.AcceptChanges();
					//Finished Consignment Audition Processes

					strBuilder = new StringBuilder();
					strBuilder.Append("INSERT INTO ImportedConsignments(recordid, userid, booking_no, consignment_no, ref_no,");
					strBuilder.Append("payerid, payer_name, payer_address1, payer_address2,");
					strBuilder.Append("payer_zipcode, payer_telephone, payer_fax, payment_mode,");
					strBuilder.Append("sender_name, sender_address1, sender_address2,");
					strBuilder.Append("sender_zipcode, sender_telephone, sender_fax, sender_contact_person,");
					strBuilder.Append("recipient_name, recipient_address1, recipient_address2,");
					strBuilder.Append("recipient_zipcode, recipient_telephone, recipient_fax, recipient_contact_person,");
					strBuilder.Append("service_code, declare_value, payment_type,");
					strBuilder.Append("commodity_code, remark, cod_amount, new_account,");//HC Return Task
					strBuilder.Append("return_pod_hc, return_invoice_hc ,booking_datetime");
					strBuilder.Append(")");//HC Return Task
					strBuilder.Append("VALUES(");

					//recordid
					if((drEach["recordid"]!= null) && (!drEach["recordid"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["recordid"].ToString().Trim() != ""))
					{
						String recordid = Utility.ReplaceSingleQuote(drEach["recordid"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(recordid);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//userid
					if((userId != null) && (userId.Trim() != ""))
					{
						strBuilder.Append("'");
						strBuilder.Append(userId);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//booking_no
					if((drEach["booking_no"]!= null) && (!drEach["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["booking_no"].ToString().Trim() != ""))
					{
						int booking_no = Convert.ToInt32(drEach["booking_no"]);
						strBuilder.Append(booking_no);
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//consignment_no
					if((drEach["consignment_no"]!= null) && (!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["consignment_no"].ToString().Trim() != ""))
					{
						String consignment_no = Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(consignment_no);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//ref_no
					if((drEach["ref_no"]!= null) && (!drEach["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["ref_no"].ToString().Trim() != ""))
					{
						String ref_no = Utility.ReplaceSingleQuote(drEach["ref_no"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(ref_no);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//payerid
					if((drEach["payerid"]!= null) && (!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["payerid"].ToString().Trim() != ""))
					{
						String payerid = Utility.ReplaceSingleQuote(drEach["payerid"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(payerid);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//payer_name
					if((drEach["payer_name"]!= null) && (!drEach["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["payer_name"].ToString().Trim() != ""))
					{
						String payer_name = Utility.ReplaceSingleQuote(drEach["payer_name"].ToString());
						strBuilder.Append("N'");
						strBuilder.Append(payer_name);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//payer_address1
					if((drEach["payer_address1"]!= null) && (!drEach["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["payer_address1"].ToString().Trim() != ""))
					{
						String payer_address1 = Utility.ReplaceSingleQuote(drEach["payer_address1"].ToString());
						strBuilder.Append("N'");
						strBuilder.Append(payer_address1);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//payer_address2
					if((drEach["payer_address2"]!= null) && (!drEach["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["payer_address2"].ToString().Trim() != ""))
					{
						String payer_address2 = Utility.ReplaceSingleQuote(drEach["payer_address2"].ToString());
						strBuilder.Append("N'");
						strBuilder.Append(payer_address2);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//payer_zipcode
					if((drEach["payer_zipcode"]!= null) && (!drEach["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["payer_zipcode"].ToString().Trim() != ""))
					{
						String payer_zipcode = Utility.ReplaceSingleQuote(drEach["payer_zipcode"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(payer_zipcode);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//payer_telephone
					if((drEach["payer_telephone"]!= null) && (!drEach["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["payer_telephone"].ToString().Trim() != ""))
					{
						String payer_telephone = Utility.ReplaceSingleQuote(drEach["payer_telephone"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(payer_telephone);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//payer_fax
					if((drEach["payer_fax"]!= null) && (!drEach["payer_fax"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["payer_fax"].ToString().Trim() != ""))
					{
						String payer_fax = Utility.ReplaceSingleQuote(drEach["payer_fax"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(payer_fax);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//payment_mode
					if((drEach["payment_mode"]!= null) && (!drEach["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["payment_mode"].ToString().Trim() != ""))
					{
						String payment_mode = Utility.ReplaceSingleQuote(drEach["payment_mode"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(payment_mode);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//sender_name
					if((drEach["sender_name"]!= null) && (!drEach["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["sender_name"].ToString().Trim() != ""))
					{
						String sender_name = Utility.ReplaceSingleQuote(drEach["sender_name"].ToString());
						strBuilder.Append("N'");
						strBuilder.Append(sender_name);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//sender_address1
					if((drEach["sender_address1"]!= null) && (!drEach["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["sender_address1"].ToString().Trim() != ""))
					{
						String sender_address1 = Utility.ReplaceSingleQuote(drEach["sender_address1"].ToString());
						strBuilder.Append("N'");
						strBuilder.Append(sender_address1);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//sender_address2
					if((drEach["sender_address2"]!= null) && (!drEach["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["sender_address2"].ToString().Trim() != ""))
					{
						String sender_address2 = Utility.ReplaceSingleQuote(drEach["sender_address2"].ToString());
						strBuilder.Append("N'");
						strBuilder.Append(sender_address2);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//sender_zipcode
					if((drEach["sender_zipcode"]!= null) && (!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["sender_zipcode"].ToString().Trim() != ""))
					{
						String sender_zipcode = Utility.ReplaceSingleQuote(drEach["sender_zipcode"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(sender_zipcode);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//sender_telephone
					if((drEach["sender_telephone"]!= null) && (!drEach["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["sender_telephone"].ToString().Trim() != ""))
					{
						String sender_telephone = Utility.ReplaceSingleQuote(drEach["sender_telephone"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(sender_telephone);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//sender_fax
					if((drEach["sender_fax"]!= null) && (!drEach["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["sender_fax"].ToString().Trim() != ""))
					{
						String sender_fax = Utility.ReplaceSingleQuote(drEach["sender_fax"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(sender_fax);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//sender_contact_person
					if((drEach["sender_contact_person"]!= null) && (!drEach["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["sender_contact_person"].ToString().Trim() != ""))
					{
						String sender_contact_person = Utility.ReplaceSingleQuote(drEach["sender_contact_person"].ToString());
						strBuilder.Append("N'");
						strBuilder.Append(sender_contact_person);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//recipient_name
					if((drEach["recipient_name"]!= null) && (!drEach["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["recipient_name"].ToString().Trim() != ""))
					{
						String recipient_name = Utility.ReplaceSingleQuote(drEach["recipient_name"].ToString());
						strBuilder.Append("N'");
						strBuilder.Append(recipient_name);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//recipient_address1
					if((drEach["recipient_address1"]!= null) && (!drEach["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["recipient_address1"].ToString().Trim() != ""))
					{
						String recipient_address1 = Utility.ReplaceSingleQuote(drEach["recipient_address1"].ToString());
						strBuilder.Append("N'");
						strBuilder.Append(recipient_address1);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//recipient_address2
					if((drEach["recipient_address2"]!= null) && (!drEach["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["recipient_address2"].ToString().Trim() != ""))
					{
						String recipient_address2 = Utility.ReplaceSingleQuote(drEach["recipient_address2"].ToString());
						strBuilder.Append("N'");
						strBuilder.Append(recipient_address2);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//recipient_zipcode
					if((drEach["recipient_zipcode"]!= null) && (!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["recipient_zipcode"].ToString().Trim() != ""))
					{
						String recipient_zipcode = Utility.ReplaceSingleQuote(drEach["recipient_zipcode"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(recipient_zipcode);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//recipient_telephone
					if((drEach["recipient_telephone"]!= null) && (!drEach["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["recipient_telephone"].ToString().Trim() != ""))
					{
						String recipient_telephone = Utility.ReplaceSingleQuote(drEach["recipient_telephone"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(recipient_telephone);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//recipient_fax
					if((drEach["recipient_fax"]!= null) && (!drEach["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["recipient_fax"].ToString().Trim() != ""))
					{
						String recipient_fax = Utility.ReplaceSingleQuote(drEach["recipient_fax"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(recipient_fax);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//recipient_contact_person
					if((drEach["recipient_contact_person"]!= null) && (!drEach["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["recipient_contact_person"].ToString().Trim() != ""))
					{
						String recipient_contact_person = Utility.ReplaceSingleQuote(drEach["recipient_contact_person"].ToString());
						strBuilder.Append("N'");
						strBuilder.Append(recipient_contact_person);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//service_code
					if((drEach["service_code"]!= null) && (!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["service_code"].ToString().Trim() != ""))
					{
						String service_code = Utility.ReplaceSingleQuote(drEach["service_code"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(service_code);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//declare_value
					if((drEach["declare_value"]!= null) && (!drEach["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["declare_value"].ToString().Trim() != ""))
					{
						decimal declare_value = Convert.ToDecimal(drEach["declare_value"]);
						strBuilder.Append(declare_value);
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//payment_type
					if((drEach["payment_type"]!= null) && (!drEach["payment_type"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["payment_type"].ToString().Trim() != ""))
					{
						String payment_type = Utility.ReplaceSingleQuote(drEach["payment_type"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(payment_type);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//commodity_code
					if((drEach["commodity_code"]!= null) && (!drEach["commodity_code"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["commodity_code"].ToString().Trim() != ""))
					{
						String commodity_code = Utility.ReplaceSingleQuote(drEach["commodity_code"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(commodity_code);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//remark
					if((drEach["remark"]!= null) && (!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["remark"].ToString().Trim() != ""))
					{
						String remark = Utility.ReplaceSingleQuote(drEach["remark"].ToString());
						strBuilder.Append("N'");
						strBuilder.Append(remark);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//cod_amount
					if((drEach["cod_amount"]!= null) && (!drEach["cod_amount"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["cod_amount"].ToString().Trim() != ""))
					{
						decimal cod_amount = Convert.ToDecimal(drEach["cod_amount"]);
						strBuilder.Append(cod_amount);	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//new_account
					if((drEach["new_account"]!= null) && (!drEach["new_account"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["new_account"].ToString().Trim() != ""))
					{
						String new_account = Utility.ReplaceSingleQuote(drEach["new_account"].ToString());
						strBuilder.Append("N'");
						strBuilder.Append(new_account);
						strBuilder.Append("'");
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//HC Return Task
					//return_pod_hc
					if((drEach["return_pod_hc"]!= null) && (!drEach["return_pod_hc"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["return_pod_hc"].ToString().Trim() != ""))
					{
						String return_pod_hc = Utility.ReplaceSingleQuote(drEach["return_pod_hc"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(return_pod_hc);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					strBuilder.Append(",");

					//return_invoice_hc
					if((drEach["return_invoice_hc"]!= null) && (!drEach["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["return_invoice_hc"].ToString().Trim() != ""))
					{
						String return_invoice_hc = Utility.ReplaceSingleQuote(drEach["return_invoice_hc"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(return_invoice_hc);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					//HC Return Task

					//Jeab 24 Dec 10 =========> Start
					//booking_datetime
					strBuilder.Append(",");
					if((drEach["booking_datetime"]!= null) && (!drEach["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach["booking_datetime"].ToString().Trim() != ""))
					{
						String booking_dt = Utility.ReplaceSingleQuote(drEach["booking_datetime"].ToString());
						strBuilder.Append("'");
						strBuilder.Append(booking_dt);
						strBuilder.Append("'");	
					}
					else
					{
						strBuilder.Append("null");
					}
					//Jeab 24 Dec 10 =========> End

					strBuilder.Append(")");

					dbCmdCons.CommandText = strBuilder.ToString();
					iRowsAffected = dbConCons.ExecuteNonQueryInTransaction(dbCmdCons);

					//					transactionAppCons.Commit();		//boon: got error if throw exception

					#endregion Insert Consignment
				
				}
				catch(ApplicationException appException)
				{
					try
					{
						transactionAppCons.Rollback();
						Logger.LogDebugInfo("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","App db insert transaction rolled back.");

					}
					catch(Exception rollbackException)
					{
						Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","Error during app transanction roll back "+ rollbackException.Message.ToString());
					}

					Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","Insert Transaction Failed : "+ appException.Message.ToString());

					throw new ApplicationException("Appliction Exception Error adding Temp Table",appException); //boon
				}
				catch(Exception exception)
				{
					try
					{
						transactionAppCons.Rollback();
						Logger.LogDebugInfo("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","App db insert transaction rolled back.");

					}
					catch(Exception rollbackException)
					{
						Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables", "Error during app transanction roll back "+ rollbackException.Message.ToString());
					}

					Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","Insert Transaction Failed : "+ exception.Message.ToString());
					
					throw new ApplicationException("Error adding Temp Table ",exception);
				}
			}
			transactionAppCons.Commit();	//Commit after end For Loop, by boon

			if(conAppCons != null)
			{
				conAppCons.Close();
			}


			#region Package Process
			//Package
			i = 0;
			foreach(DataRow drEach in dsPackage.Tables[0].Rows)
			{
				try
				{
					if (i == 0)
					{
						strBuilder = new StringBuilder();
						strBuilder.Append("select count(*) from Core_Enterprise");
						dbCmdPKG = dbConPKG.CreateCommand(strBuilder.ToString() , CommandType.Text);
						dbCmdPKG.Connection = conAppPKG;
						dbCmdPKG.Transaction = transactionAppPKG;
						iRowsAffected = dbConPKG.ExecuteNonQueryInTransaction(dbCmdPKG);
					}
					i++;

					//Error Checking : N/A Consignment No for Package
					if(dsConsignment.Tables[0].Select("consignment_no = '" + drEach["consignment_no"] + "'").Length > 0)
					{
						//Error Checking : Code 1101
						if(dsPackage.Tables[0].Select("consignment_no = '" + drEach["consignment_no"] + "' and mps_code = '" + drEach["consignment_no"] + "'").Length > 1)
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Package',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1101)");

							dbCmdPKG.CommandText = strBuilder.ToString();
							iRowsAffected = dbConPKG.ExecuteNonQueryInTransaction(dbCmdPKG);
						}

						//Error Checking : N/A Length
						if((drEach["pkg_length"] != null) && (!drEach["pkg_length"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["pkg_length"].ToString() != ""))
						{
							if(!IsNumeric(drEach["pkg_length"].ToString())) 
							{
								drEach["pkg_length"] = 0;
							}
						}
						else
						{
							drEach["pkg_length"] = 0;
						}

						if((drEach["pkg_length"] != null) && (!drEach["pkg_length"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["pkg_length"].ToString() != ""))
						{
							decimal tmpLength = Convert.ToDecimal(drEach["pkg_length"]);

							//Error Checking : Code 1111
							if(tmpLength <= 0)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Package',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1111)");

								dbCmdPKG.CommandText = strBuilder.ToString();
								iRowsAffected = dbConPKG.ExecuteNonQueryInTransaction(dbCmdPKG);
							}

							//Error Checking : Code 1112
							if(tmpLength > 10000)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Package',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1112)");

								dbCmdPKG.CommandText = strBuilder.ToString();
								iRowsAffected = dbConPKG.ExecuteNonQueryInTransaction(dbCmdPKG);
							}
						}

						//Error Checking : N/A Breadth
						if((drEach["pkg_breadth"] != null) && (!drEach["pkg_breadth"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["pkg_breadth"].ToString() != ""))
						{
							if(!IsNumeric(drEach["pkg_breadth"].ToString())) 
							{
								drEach["pkg_breadth"] = 0;
							}
						}
						else
						{
							drEach["pkg_breadth"] = 0;
						}

							
						if((drEach["pkg_breadth"] != null) && (!drEach["pkg_breadth"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["pkg_breadth"].ToString() != ""))
						{
							decimal tmpBreadth = Convert.ToDecimal(drEach["pkg_breadth"]);

							//Error Checking : Code 1113
							if(tmpBreadth <= 0)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Package',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1113)");

								dbCmdPKG.CommandText = strBuilder.ToString();
								iRowsAffected = dbConPKG.ExecuteNonQueryInTransaction(dbCmdPKG);
							}

							//Error Checking : Code 1114
							if(tmpBreadth > 10000)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Package',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1114)");

								dbCmdPKG.CommandText = strBuilder.ToString();
								iRowsAffected = dbConPKG.ExecuteNonQueryInTransaction(dbCmdPKG);
							}
						}

						//Error Checking : N/A Height
						if((drEach["pkg_height"] != null) && (!drEach["pkg_height"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["pkg_height"].ToString() != ""))
						{
							if(!IsNumeric(drEach["pkg_height"].ToString())) 
							{
								drEach["pkg_height"] = 0;
							}
						}
						else
						{
							drEach["pkg_height"] = 0;
						}

							
						if((drEach["pkg_height"] != null) && (!drEach["pkg_height"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["pkg_height"].ToString() != ""))
						{
							decimal tmpHeight = Convert.ToDecimal(drEach["pkg_height"]);

							//Error Checking : Code 1115
							if(tmpHeight <= 0)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Package',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1115)");

								dbCmdPKG.CommandText = strBuilder.ToString();
								iRowsAffected = dbConPKG.ExecuteNonQueryInTransaction(dbCmdPKG);
							}

							//Error Checking : Code 1116
							if(tmpHeight > 10000)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Package',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1116)");

								dbCmdPKG.CommandText = strBuilder.ToString();
								iRowsAffected = dbConPKG.ExecuteNonQueryInTransaction(dbCmdPKG);
							}
						}

						//Error Checking : N/A Weight
						if((drEach["pkg_wt"] != null) && (!drEach["pkg_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["pkg_wt"].ToString() != ""))
						{
							if(!IsNumeric(drEach["pkg_wt"].ToString())) 
							{
								drEach["pkg_wt"] = 0;
							}
						}
						else
						{
							drEach["pkg_wt"] = 0;
						}

							
						if((drEach["pkg_wt"] != null) && (!drEach["pkg_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["pkg_wt"].ToString() != ""))
						{
							decimal tmpWT = Convert.ToDecimal(drEach["pkg_wt"]);

							//Error Checking : Code 1117
							if(tmpWT <= 0)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Package',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1117)");

								dbCmdPKG.CommandText = strBuilder.ToString();
								iRowsAffected = dbConPKG.ExecuteNonQueryInTransaction(dbCmdPKG);
							}

							//Error Checking : Code 1118
							if(tmpWT > 10000)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Package',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1118)");

								dbCmdPKG.CommandText = strBuilder.ToString();
								iRowsAffected = dbConPKG.ExecuteNonQueryInTransaction(dbCmdPKG);
							}
						}

						//Error Checking : N/A Quantity
						if((drEach["pkg_qty"] != null) && (!drEach["pkg_qty"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["pkg_qty"].ToString() != ""))
						{
							if(!IsNumeric(drEach["pkg_qty"].ToString())) 
							{
								drEach["pkg_qty"] = 0;
							}
						}
						else
						{
							drEach["pkg_qty"] = 0;
						}

							
						if((drEach["pkg_qty"] != null) && (!drEach["pkg_qty"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach["pkg_qty"].ToString() != ""))
						{
							decimal tmpQTY = Convert.ToDecimal(drEach["pkg_qty"]);

							//Error Checking : Code 1119
							if(tmpQTY <= 0)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Package',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1119)");

								dbCmdPKG.CommandText = strBuilder.ToString();
								iRowsAffected = dbConPKG.ExecuteNonQueryInTransaction(dbCmdPKG);
							}

							//Error Checking : Code 1120
							if(tmpQTY > 9999)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Package',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1120)");

								dbCmdPKG.CommandText = strBuilder.ToString();
								iRowsAffected = dbConPKG.ExecuteNonQueryInTransaction(dbCmdPKG);
							}
						}

						strBuilder = new StringBuilder();
						strBuilder.Append("INSERT INTO ImportedConsignmentsPkgs(recordid, userid, consignment_no,");
						strBuilder.Append("mps_code,");
						strBuilder.Append("pkg_length, pkg_breadth, pkg_height,");
						strBuilder.Append("pkg_wt, pkg_qty)");
						strBuilder.Append("VALUES(");
						
						//recordid
						if((drEach["recordid"]!= null) && (!drEach["recordid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String recordid = Utility.ReplaceSingleQuote(drEach["recordid"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(recordid);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						//userid
						if((userId!= null) && (userId.Trim() != ""))
						{
							strBuilder.Append("'");
							strBuilder.Append(userId);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						//consignment_no
						if((drEach["consignment_no"]!= null) && (!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String consignment_no = Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(consignment_no);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						//mps_code
						if((drEach["mps_code"]!= null) && (!drEach["mps_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String mps_code = Utility.ReplaceSingleQuote(drEach["mps_code"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(mps_code);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						//pkg_length
						if((drEach["pkg_length"]!= null) && (!drEach["pkg_length"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String pkg_length = Utility.ReplaceSingleQuote(drEach["pkg_length"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(pkg_length);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						//pkg_breadth
						if((drEach["pkg_breadth"]!= null) && (!drEach["pkg_breadth"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String pkg_breadth = Utility.ReplaceSingleQuote(drEach["pkg_breadth"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(pkg_breadth);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						//pkg_height
						if((drEach["pkg_height"]!= null) && (!drEach["pkg_height"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String pkg_height = Utility.ReplaceSingleQuote(drEach["pkg_height"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(pkg_height);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						//pkg_wt
						if((drEach["pkg_wt"]!= null) && (!drEach["pkg_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String pkg_wt = Utility.ReplaceSingleQuote(drEach["pkg_wt"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(pkg_wt);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(",");

						//pkg_qty
						if((drEach["pkg_qty"]!= null) && (!drEach["pkg_qty"].GetType().Equals(System.Type.GetType("System.DBNull"))))
						{
							String pkg_qty = Utility.ReplaceSingleQuote(drEach["pkg_qty"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(pkg_qty);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						
						strBuilder.Append(")");

						dbCmdPKG.CommandText = strBuilder.ToString();
						iRowsAffected = dbConPKG.ExecuteNonQueryInTransaction(dbCmdPKG);		

						//						transactionAppPKG.Commit();		//boon: got error, throw exception 
					}
				}
				catch(ApplicationException appException)
				{
					try
					{
						transactionAppPKG.Rollback();
						Logger.LogDebugInfo("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","App db insert transaction rolled back.");

					}
					catch(Exception rollbackException)
					{
						Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","Error during app transanction roll back "+ rollbackException.Message.ToString());
					}


					Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","Insert Transaction Failed : "+ appException.Message.ToString());
					throw new ApplicationException("Error adding Temp Table in package process ",appException);
				}
				catch(Exception exception)
				{
					try
					{
						transactionAppPKG.Rollback();
						Logger.LogDebugInfo("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","App db insert transaction rolled back.");

					}
					catch(Exception rollbackException)
					{
						Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables", "Error during app transanction roll back "+ rollbackException.Message.ToString());
					}

					Logger.LogTraceError("ImportConsignmentsDAL","ImportToTempTables","ImportToTempTables","Insert Transaction Failed : "+ exception.Message.ToString());
					throw new ApplicationException("Error adding Temp Table in package process ",exception);
				}
			}

			transactionAppPKG.Commit();	//Commit transaction when at end For Loop

			#endregion Package Process

			if(conAppPKG != null)
			{
				conAppPKG.Close();
			}
		}


		public static DataSet GetConsignmentData(String strAppID, String strEnterpriseID, int iCurRow, int iDSRowSize, String userId)
		{
			SessionDS sessionDS = null;			
			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("SELECT ic.recordid, ic.booking_no, ic.consignment_no, ic.ref_no,");
			strBuilder.Append("ic.payerid, ic.payer_name, ic.payer_address1, ic.payer_address2, ic.payer_zipcode,");
			strBuilder.Append("ic.payer_telephone, ic.payer_fax, ic.payment_mode,");
			strBuilder.Append("ic.sender_name, ic.sender_address1, ic.sender_address2, ic.sender_zipcode,");
			strBuilder.Append("ic.sender_telephone, ic.sender_fax, ic.sender_contact_person,");
			strBuilder.Append("ic.recipient_name, ic.recipient_address1, ic.recipient_address2, ic.recipient_zipcode,");
			strBuilder.Append("ic.recipient_telephone, ic.recipient_fax, ic.recipient_contact_person,");
			strBuilder.Append("ic.service_code, ic.declare_value, ic.payment_type, ic.commodity_code,");//HC Return Task
			strBuilder.Append("ic.remark, ic.cod_amount, ic.new_account, ic.return_pod_hc, ic.return_invoice_hc,ic.booking_datetime "); 		
			strBuilder.Append("FROM ImportedConsignments ic ");//HC Return Task
			//			strBuilder.Append("LEFT JOIN Shipment s");
			//			strBuilder.Append(" ON ic.consignment_no = s.consignment_no AND ic.applicationid = s.applicationid AND ic.enterpriseid = s.enterpriseid");
			strBuilder.Append(" WHERE ic.userid = '" + userId + "' " + "  AND ic.enterpriseid='" + strEnterpriseID + "' AND ic.applicationid ='" + strAppID + "'"); //AND ic.consignment_no IS NULL
	
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","GetConsignmentData","GetConsignmentData","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			
			sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "ConsignmentTable");
			return  sessionDS.ds;
		}


		public static DataSet GetPackageData(String strAppID, String strEnterpriseID, 
			int iCurRow, int iDSRowSize, String consignment_no, String userId)
		{
			SessionDS sessionDS = null;			
			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("SELECT recordid, consignment_no, mps_code, ");
			strBuilder.Append("pkg_length, pkg_breadth, pkg_height, pkg_wt, pkg_qty ");	
			strBuilder.Append("FROM ImportedConsignmentsPkgs ");
			strBuilder.Append("WHERE consignment_no = '" + consignment_no + "'");
			strBuilder.Append("AND userid = '" + userId + "' ");
	
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","GetPackageData","GetPackageData","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			
			sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), iCurRow, iDSRowSize, "PackageTable");
			return  sessionDS.ds;
		}


		public static DataSet GetAuditData(String strAppID, String strEnterpriseID,
			String recordid, String userid, String recordtype)
		{
			SessionDS sessionDS = null;			
			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("SELECT  cse.error_code, cse.associated_text, cse.error_message ");	
			strBuilder.Append("FROM Core_System_Errors cse inner join ImportedConsignmentsAuditFailures ad ON");
			strBuilder.Append("(cse.applicationid = ad.applicationid ");
			strBuilder.Append("and cse.user_culture = ad.user_culture ");
			strBuilder.Append("and cse.error_code = ad.error_code) ");
			strBuilder.Append(" WHERE recordid = '");
			strBuilder.Append(recordid+"'");
			strBuilder.Append(" and userid = '");
			strBuilder.Append(userid+"'");
			strBuilder.Append(" and recordtype = '");
			strBuilder.Append(recordtype+"'");
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","GetAuditData","GetAuditData","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			
			sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), 0, 0, "AuditTable");
			return  sessionDS.ds;
		}


		public static String GetUserCulture(String strAppID, String strEnterpriseID, String userId)
		{
			SessionDS sessionDS = null;			
			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("SELECT * ");
			strBuilder.Append("From User_Master ");
			strBuilder.Append("where applicationid = '");
			strBuilder.Append(strAppID+"'");
			strBuilder.Append(" and enterpriseid = '");
			strBuilder.Append(strEnterpriseID+"'");
			strBuilder.Append(" and userid = '");
			strBuilder.Append(userId+"'");

	
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","audit1001","audit1001","DbConnection object is null!!");
			}
			
			sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), 0, 0, "audit1001");
			if(sessionDS.ds.Tables[0].Rows.Count > 0)
				return sessionDS.ds.Tables[0].Rows[0]["user_culture"].ToString();
			else
				return "";
		}
		

		public static int DeleteConsignment(String strAppID, String strEnterpriseID,
			String consignmentNo, String recordId, String userId)
		{
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			IDbCommand dbCmd = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ImportConsignmentsDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ImportConsignmentsDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				DataSet dsConsignment = new DataSet();

				strBuild = new StringBuilder();
				strBuild.Append("select count(*) from Core_Enterprise");
				dbCmd = dbCon.CreateCommand(strBuild.ToString() , CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				if(consignmentNo.Trim() != "")
				{
					//get exist consignment record from the consignment table
					strBuild = new StringBuilder();
					strBuild.Append("SELECT recordid " );
					strBuild.Append("FROM ImportedConsignments ");
					strBuild.Append("WHERE consignment_no = '" + consignmentNo + "' ");
					strBuild.Append("AND userid = '" + userId + "' ");
					dbCmd.CommandText = strBuild.ToString();
					dsConsignment = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);
				}
			
				//delete consignment record from the audit table
				strBuild = new StringBuilder();
				strBuild.Append("DELETE " );
				strBuild.Append("FROM ImportedConsignmentsAuditFailures ");
				strBuild.Append("WHERE recordid = '" + recordId + "' ");
				strBuild.Append("AND userid = '" + userId + "' ");
				strBuild.Append("AND recordtype = 'Consignment' ");
				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				//delete consignment record from the consignment table
				strBuild = new StringBuilder();
				strBuild.Append("DELETE " );
				strBuild.Append("FROM ImportedConsignments ");
				strBuild.Append("WHERE recordid = '" + recordId + "' ");
				strBuild.Append("AND userid = '" + userId + "' ");
				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				if(consignmentNo.Trim() != "")
				{
					if(dsConsignment.Tables[0].Rows.Count == 1)
					{
						//get exist package records from the package table
						strBuild = new StringBuilder();
						strBuild.Append("SELECT recordid " );
						strBuild.Append("FROM ImportedConsignmentsPkgs ");
						strBuild.Append("WHERE consignment_no = '" + consignmentNo + "' ");
						strBuild.Append("AND userid = '" + userId + "' ");
						dbCmd.CommandText = strBuild.ToString();
						DataSet dsPackage = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);

						foreach(DataRow dr in dsPackage.Tables[0].Rows)
						{
							//delete audit of package record from the audit table
							strBuild = new StringBuilder();
							strBuild.Append("DELETE " );
							strBuild.Append("FROM ImportedConsignmentsAuditFailures ");
							strBuild.Append("WHERE recordid = '" + dr["recordid"].ToString() + "' ");
							strBuild.Append("AND userid = '" + userId + "' ");
							strBuild.Append("AND recordtype = 'Package' ");
							dbCmd.CommandText = strBuild.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

							//delete package record from the package table
							strBuild = new StringBuilder();
							strBuild.Append("DELETE " );
							strBuild.Append("FROM ImportedConsignmentsPkgs ");
							strBuild.Append("WHERE recordid = '" +  dr["recordid"].ToString() + "' ");
							strBuild.Append("AND userid = '" + userId + "' ");
							dbCmd.CommandText = strBuild.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						}
					}
				}
				
				transactionApp.Commit();
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("ImportConsignmentsDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ImportConsignmentsDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("ImportConsignmentsDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting audit data ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("ImportConsignmentsDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ImportConsignmentsDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("ImportConsignmentsDAL","DeleteConsignmentByConsignmentNo","DeleteConsignmentByConsignmentNo","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting audit data ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;

		}

		

		public static int DeletePacakgeByRecordId(String strAppID, String strEnterpriseID,
			String ConsignNo, String recordid, String strRecordIDOfConsign, String userId, String user_culture)
		{
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","DeletePacakgeByRecordId","DeletePacakgeByRecordId","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","DeletePacakgeByRecordId","DeletePacakgeByRecordId","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ImportConsignmentsDAL","DeletePacakgeByRecordId","DeletePacakgeByRecordId","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ImportConsignmentsDAL","DeletePacakgeByRecordId","DeletePacakgeByRecordId","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","DeletePacakgeByRecordId","DeletePacakgeByRecordId","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{

				//delete from the audit table
				strBuild = new StringBuilder();
				strBuild.Append("DELETE " );
				strBuild.Append("FROM ImportedConsignmentsAuditFailures ");
				strBuild.Append("WHERE recordid = '" + recordid + "' ");
				strBuild.Append("AND userid = '" + userId + "' ");
				strBuild.Append("AND recordtype = 'Package' ");

				dbCmd = dbCon.CreateCommand(strBuild.ToString(),CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				//delete from the Package table
				strBuild = new StringBuilder();
				strBuild.Append("DELETE " );
				strBuild.Append("FROM ImportedConsignmentsPkgs ");
				strBuild.Append("WHERE recordid = '" + recordid + "' ");
				strBuild.Append("AND userid = '" + userId + "' ");

				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				if(strRecordIDOfConsign.Trim() != "None")
				{
					//Error Checking : Code 1004
					strBuild = new StringBuilder();
					strBuild.Append("SELECT * ");	
					strBuild.Append("FROM ImportedConsignmentsPkgs ");
					strBuild.Append("where consignment_no = '"+ConsignNo.Trim()+"' ");
					strBuild.Append("and userid = '"+userId+"' ");

					dbCmd.CommandText = strBuild.ToString();
					DataSet dsTmpPackageData = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  

					if (dsTmpPackageData.Tables[0].Rows.Count == 0)
					{
						strBuild = new StringBuilder();
						strBuild.Append("SELECT * ");	
						strBuild.Append("FROM ImportedConsignmentsAuditFailures ");
						strBuild.Append("where recordid = '"+strRecordIDOfConsign.Trim()+"' ");
						strBuild.Append("and userid = '"+userId+"' ");
						strBuild.Append("and recordtype = 'Consignment' ");
						strBuild.Append("and error_code = '1004'");

						dbCmd.CommandText = strBuild.ToString();
						DataSet dsTmpAuditData = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  
						if(dsTmpAuditData.Tables[0].Rows.Count == 0)
						{
							strBuild = new StringBuilder();
							strBuild.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
							strBuild.Append("recordtype, applicationid, user_culture,");
							strBuild.Append("error_code)");
							strBuild.Append("VALUES(");
							strBuild.Append("'" + strRecordIDOfConsign.Trim() + "',");
							strBuild.Append("'" + userId + "',");
							strBuild.Append("'Consignment',");
							strBuild.Append("'" + strAppID + "',");
							strBuild.Append("'" + user_culture + "',");
							strBuild.Append("1004)");

							dbCmd.CommandText = strBuild.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						}
						dsTmpAuditData.Dispose();
						dsTmpAuditData = null;
					}

					dsTmpPackageData.Dispose();
					dsTmpPackageData = null;
				}

				transactionApp.Commit();
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("ImportConsignmentsDAL","DeletePacakgeByRecordId","DeletePacakgeByRecordId","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ImportConsignmentsDAL","DeletePacakgeByRecordId","DeletePacakgeByRecordId","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				throw new ApplicationException("Error deleting audit data ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("ImportConsignmentsDAL","DeletePacakgeByRecordId","DeletePacakgeByRecordId","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ImportConsignmentsDAL","DeletePacakgeByRecordId","DeletePacakgeByRecordId","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				throw new ApplicationException("Error deleting audit data ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;

		}


		public static int ClearTempDataByUserId(String strAppID, String strEnterpriseID,
			String userId)
		{
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","DeleteOfFirstRun","DeleteOfFirstRun","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","DeleteOfFirstRun","DeleteOfFirstRun","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ImportConsignmentsDAL","DeleteOfFirstRun","DeleteOfFirstRun","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ImportConsignmentsDAL","DeleteOfFirstRun","DeleteOfFirstRun","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","DeleteOfFirstRun","DeleteOfFirstRun","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{

				//delete from the Consignment table
				strBuild = new StringBuilder();
				strBuild.Append("DELETE " );
				strBuild.Append("FROM ImportedConsignments ");
				strBuild.Append("WHERE userid = '" + userId + "' ");

				dbCmd = dbCon.CreateCommand(strBuild.ToString(), CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				//delete from the Package table
				strBuild = new StringBuilder();
				strBuild.Append("DELETE " );
				strBuild.Append("FROM ImportedConsignmentsPkgs ");
				strBuild.Append("WHERE userid = '" + userId + "' ");

				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				//delete from the Audit table
				strBuild = new StringBuilder();
				strBuild.Append("DELETE " );
				strBuild.Append("FROM ImportedConsignmentsAuditFailures ");
				strBuild.Append("WHERE userid = '" + userId + "' ");

				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				transactionApp.Commit();
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("ImportConsignmentsDAL","DeleteOfFirstRun","DeleteOfFirstRun","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ImportConsignmentsDAL","DeleteOfFirstRun","DeleteOfFirstRun","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				throw new ApplicationException("Error deleting audit data ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("ImportConsignmentsDAL","DeleteOfFirstRun","DeleteOfFirstRun","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ImportConsignmentsDAL","DeleteOfFirstRun","DeleteOfFirstRun","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				throw new ApplicationException("Error deleting audit data ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;

		}


		public static int UpdateConsignment(String strAppID,String strEnterpriseID,
			DataSet dsConsignment, ArrayList recordid, String userId, 
			String user_culture)
		{
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","UpdateConsignment","UpdateConsignment","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","UpdateConsignment","UpdateConsignment","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ImportConsignmentsDAL","UpdateConsignment","UpdateConsignment","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ImportConsignmentsDAL","UpdateConsignment","UpdateConsignment","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","UpdateConsignment","UpdateConsignment","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append("select count(*) from Core_Enterprise");
				dbCmd = dbCon.CreateCommand(strBuilder.ToString() , CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				for(int i = 0; i <= recordid.Count - 1; i++)
				{
					DataRow[] drEach = dsConsignment.Tables[0].Select("recordid = '" + recordid[i] + "'");

					if (drEach.Length > 0)
					{
						//delete audit of consignment record from the audit table
						strBuilder = new StringBuilder();
						strBuilder.Append("DELETE " );
						strBuilder.Append("FROM ImportedConsignmentsAuditFailures ");
						strBuilder.Append("WHERE recordid = '" + recordid[i] + "' ");
						strBuilder.Append("AND userid = '" + userId + "' ");
						strBuilder.Append("AND recordtype = 'Consignment' ");

						dbCmd.CommandText = strBuilder.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

						bool canBypassCheckingCustomerSender = false;

						#region Check Consignment_no - Not Found
						//Error Checking : Code 1003
						if((drEach[0]["consignment_no"] == null) || (drEach[0]["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) || drEach[0]["consignment_no"].ToString().Trim() == "")
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + recordid[i] + "',");
							strBuilder.Append("'" + strEnterpriseID + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Consignment',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1003)");

							dbCmd.CommandText = strBuilder.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

						}
							#endregion Check Consignment_no - Not Found

							#region Check Consignment_no - Found
						else
						{
							//Error Checking : Code 1001
							strBuilder = new StringBuilder();
							strBuilder.Append("SELECT * ");	
							strBuilder.Append("FROM Shipment ");
							strBuilder.Append("where applicationid = '");
							strBuilder.Append(strAppID+"'");
							strBuilder.Append(" and enterpriseid = '");
							strBuilder.Append(strEnterpriseID+"'");
							strBuilder.Append(" and consignment_no = '");
							strBuilder.Append(drEach[0]["consignment_no"].ToString().Trim()+"'");

							dbCmd.CommandText = strBuilder.ToString();
							DataSet dsTmp = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  

							//							//Error Checking : Purged Shipment
							//							strBuilder = new StringBuilder();
							//							strBuilder.Append("SELECT * ");	
							//							strBuilder.Append("FROM Purged_Shipment ");
							//							strBuilder.Append("where applicationid = '");
							//							strBuilder.Append(strAppID+"'");
							//							strBuilder.Append(" and enterpriseid = '");
							//							strBuilder.Append(strEnterpriseID+"'");
							//							strBuilder.Append(" and consignment_no = '");
							//							strBuilder.Append(drEach[0]["consignment_no"].ToString().Trim()+"'");
							//
							//							dbCmd.CommandText = strBuilder.ToString();
							//							DataSet dsPurged = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  

							if(dsTmp.Tables[0].Rows.Count > 0 || dsTmp.Tables[0].Rows.Count > 0)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + recordid[i] + "',");
								strBuilder.Append("'" + strEnterpriseID + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1001)");

								dbCmd.CommandText = strBuilder.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							}
							dsTmp.Dispose();
							dsTmp = null;

							//dsPurged.Dispose();
							//dsPurged = null;

							//Error Checking : Code 1002
							if(dsConsignment.Tables[0].Select("consignment_no = '" + drEach[0]["consignment_no"].ToString().Trim() + "'").Length > 1)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + recordid[i] + "',");
								strBuilder.Append("'" + strEnterpriseID + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1002)");

								dbCmd.CommandText = strBuilder.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							}

							//Error Checking : Code 1004
							strBuilder = new StringBuilder();
							strBuilder.Append("SELECT * ");	
							strBuilder.Append("FROM ImportedConsignmentsPkgs ");
							strBuilder.Append("where consignment_no = '"+drEach[0]["consignment_no"].ToString().Trim()+"' ");
							strBuilder.Append("and userid = '"+userId+"' ");

							dbCmd.CommandText = strBuilder.ToString();
							DataSet dsTmpPackageData = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  
							if(dsTmpPackageData.Tables[0].Rows.Count == 0)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach[0]["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + strEnterpriseID + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1004)");

								dbCmd.CommandText = strBuilder.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							}

							dsTmpPackageData.Dispose();
							dsTmpPackageData = null;
						}
						#endregion Check Consignment_no - Found

						#region Check Booking_no
						//Error Checking : Code 1011
						if((drEach[0]["booking_no"] != null) && (!drEach[0]["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["booking_no"].ToString() != ""))
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("SELECT * ");	
							strBuilder.Append("FROM Pickup_Request ");
							strBuilder.Append("where applicationid = '");
							strBuilder.Append(strAppID+"'");
							strBuilder.Append(" and enterpriseid = '");
							strBuilder.Append(strEnterpriseID+"'");
							strBuilder.Append(" and booking_no = '");
							strBuilder.Append(drEach[0]["booking_no"].ToString().Trim()+"'");

							dbCmd.CommandText = strBuilder.ToString();
							DataSet dsTmpBookingData = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  

							if(dsTmpBookingData.Tables[0].Rows.Count == 0)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + recordid[i] + "',");
								strBuilder.Append("'" + strEnterpriseID + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1011)");

								dbCmd.CommandText = strBuilder.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

								canBypassCheckingCustomerSender = false;
							}
							else
							{
								DataRow drTmpBookingData = dsTmpBookingData.Tables[0].Rows[0];

								//Replace drConsignment "payer data" with data from Pickup_Request
								//Boon 29 Dec 10 ----- Start
								if((drTmpBookingData["booking_datetime"] != null) && 
									(!drTmpBookingData["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drTmpBookingData["booking_datetime"].ToString() != ""))
									drEach[0]["booking_datetime"] = drTmpBookingData["booking_datetime"];
								else
									drEach[0]["booking_datetime"] = "";
								//Boon 29 Dec 10 ----- End

								if((drTmpBookingData["payerid"] != null) && 
									(!drTmpBookingData["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drTmpBookingData["payerid"].ToString() != ""))
									drEach[0]["payerid"] = drTmpBookingData["payerid"];
								else
									drEach[0]["payerid"] = "";
								
								if((drTmpBookingData["payer_name"] != null) && 
									(!drTmpBookingData["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drTmpBookingData["payer_name"].ToString() != ""))
									drEach[0]["payer_name"] = drTmpBookingData["payer_name"];
								else
									drEach[0]["payer_name"] = "";
								
								if((drTmpBookingData["payer_address1"] != null) && 
									(!drTmpBookingData["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drTmpBookingData["payer_address1"].ToString() != ""))
									drEach[0]["payer_address1"] = drTmpBookingData["payer_address1"];
								else
									drEach[0]["payer_address1"] = "";
								
								if((drTmpBookingData["payer_address2"] != null) && 
									(!drTmpBookingData["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drTmpBookingData["payer_address2"].ToString() != ""))
									drEach[0]["payer_address2"] = drTmpBookingData["payer_address2"];
								else
									drEach[0]["payer_address2"] = "";

								if((drTmpBookingData["payer_zipcode"] != null) && 
									(!drTmpBookingData["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drTmpBookingData["payer_zipcode"].ToString() != ""))
									drEach[0]["payer_zipcode"] = drTmpBookingData["payer_zipcode"];
								else
									drEach[0]["payer_zipcode"] = "";
								
								if((drTmpBookingData["payer_telephone"] != null) && 
									(!drTmpBookingData["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drTmpBookingData["payer_telephone"].ToString() != ""))
									drEach[0]["payer_telephone"] = drTmpBookingData["payer_telephone"];
								else
									drEach[0]["payer_telephone"] = "";
									
								if((drTmpBookingData["payer_fax"] != null) && 
									(!drTmpBookingData["payer_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drTmpBookingData["payer_fax"].ToString() != ""))
									drEach[0]["payer_fax"] = drTmpBookingData["payer_fax"];
								else
									drEach[0]["payer_fax"] = "";
								
								if((drTmpBookingData["payment_mode"] != null) && 
									(!drTmpBookingData["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drTmpBookingData["payment_mode"].ToString() != ""))
									drEach[0]["payment_mode"] = drTmpBookingData["payment_mode"];
								else
									drEach[0]["payment_mode"] = "";
								
								//Replace drConsignment "sender data" with data from Pickup_Request
								if((drTmpBookingData["sender_name"] != null) && 
									(!drTmpBookingData["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drTmpBookingData["sender_name"].ToString() != ""))
									drEach[0]["sender_name"] = drTmpBookingData["sender_name"];
								else
									drEach[0]["sender_name"] = "";
									
								if((drTmpBookingData["sender_address1"] != null) && 
									(!drTmpBookingData["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drTmpBookingData["sender_address1"].ToString() != ""))
									drEach[0]["sender_address1"] = drTmpBookingData["sender_address1"];
								else
									drEach[0]["sender_address1"] = "";
								
								if((drTmpBookingData["sender_address2"] != null) && 
									(!drTmpBookingData["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drTmpBookingData["sender_address2"].ToString() != ""))
									drEach[0]["sender_address2"] = drTmpBookingData["sender_address2"];
								else
									drEach[0]["sender_address2"] = "";
								
								if((drTmpBookingData["sender_zipcode"] != null) && 
									(!drTmpBookingData["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drTmpBookingData["sender_zipcode"].ToString() != ""))
									drEach[0]["sender_zipcode"] = drTmpBookingData["sender_zipcode"];
								else
									drEach[0]["sender_zipcode"] = "";
								
								if((drTmpBookingData["sender_telephone"] != null) && 
									(!drTmpBookingData["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drTmpBookingData["sender_telephone"].ToString() != ""))
									drEach[0]["sender_telephone"] = drTmpBookingData["sender_telephone"];
								else
									drEach[0]["sender_telephone"] = "";
								
								if((drTmpBookingData["sender_fax"] != null) && 
									(!drTmpBookingData["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drTmpBookingData["sender_fax"].ToString() != ""))
									drEach[0]["sender_fax"] = drTmpBookingData["sender_fax"];
								else
									drEach[0]["sender_fax"] = "";
								
								if((drTmpBookingData["sender_contact_person"] != null) && 
									(!drTmpBookingData["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drTmpBookingData["sender_contact_person"].ToString() != ""))
									drEach[0]["sender_contact_person"] = drTmpBookingData["sender_contact_person"];
								else
									drEach[0]["sender_contact_person"] = "";
								
								canBypassCheckingCustomerSender = true;
							}

							dsTmpBookingData.Dispose();
							dsTmpBookingData = null;
						}
						#endregion Check Booking_no

						#region Check 1021
						//Error Checking : Code 1021
						strBuilder = new StringBuilder();
						strBuilder.Append("SELECT * ");	
						strBuilder.Append("FROM service ");
						strBuilder.Append("where applicationid = '");
						strBuilder.Append(strAppID+"'");
						strBuilder.Append(" and enterpriseid = '");
						strBuilder.Append(strEnterpriseID+"'");
						strBuilder.Append(" and service_code = '");
						strBuilder.Append(drEach[0]["service_code"].ToString().Trim()+"'");

						dbCmd.CommandText = strBuilder.ToString();
						DataSet dsserviceTmp = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  

						if(dsserviceTmp.Tables[0].Rows.Count == 0)
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + recordid[i] + "',");
							strBuilder.Append("'" + strEnterpriseID + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Consignment',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1021)");

							dbCmd.CommandText = strBuilder.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						}

						dsserviceTmp.Dispose();
						dsserviceTmp = null;
						#endregion Check 1021

						#region Start Payer & Sender Checking
						//Start Payer & Sender Checking
						if(!canBypassCheckingCustomerSender)
						{
							//Error Checking : Payer N/A
							if((drEach[0]["payerid"] == null) || 
								(drEach[0]["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
								(drEach[0]["payerid"].ToString() == ""))
							{
								drEach[0]["payerid"] = "NEW";

								//Error Checking : Code 1032
								if(((drEach[0]["payer_address1"] == null) || 
									(drEach[0]["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
									(drEach[0]["payer_address1"].ToString() == "")) || 
									((drEach[0]["payer_address2"] == null) || 
									(drEach[0]["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) ||
									(drEach[0]["payer_address2"].ToString() == "")))
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + recordid[i] + "',");
									strBuilder.Append("'" + strEnterpriseID + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Consignment',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1032)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}

								//Error Checking : Code 1033
								if((drEach[0]["payer_telephone"] == null) || 
									(drEach[0]["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
									(drEach[0]["payer_telephone"].ToString() == ""))
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + recordid[i] + "',");
									strBuilder.Append("'" + strEnterpriseID + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Consignment',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1033)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}

								//Error Checking : Code 1034
								if((drEach[0]["payer_zipcode"] != null) && 
									(!drEach[0]["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
									(drEach[0]["payer_zipcode"].ToString() != ""))
								{
							
									strBuilder = new StringBuilder();
									strBuilder.Append("SELECT * ");	
									strBuilder.Append("FROM Zipcode ");
									strBuilder.Append("where applicationid = '");
									strBuilder.Append(strAppID+"'");
									strBuilder.Append(" and enterpriseid = '");
									strBuilder.Append(strEnterpriseID+"'");
									strBuilder.Append(" and zipcode = '");
									strBuilder.Append(drEach[0]["payer_zipcode"].ToString().Trim()+"'");

									dbCmd.CommandText = strBuilder.ToString();
									DataSet dsZipcodeTmp = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  

									if(dsZipcodeTmp.Tables[0].Rows.Count == 0)
									{
										strBuilder = new StringBuilder();
										strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
										strBuilder.Append("recordtype, applicationid, user_culture,");
										strBuilder.Append("error_code)");
										strBuilder.Append("VALUES(");
										strBuilder.Append("'" + recordid[i] + "',");
										strBuilder.Append("'" + strEnterpriseID + "',");
										strBuilder.Append("'" + userId + "',");
										strBuilder.Append("'Consignment',");
										strBuilder.Append("'" + strAppID + "',");
										strBuilder.Append("'" + user_culture + "',");
										strBuilder.Append("1034)");

										dbCmd.CommandText = strBuilder.ToString();
										iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
									}

									dsZipcodeTmp.Dispose();
									dsZipcodeTmp = null;
								}
								else
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + recordid[i] + "',");
									strBuilder.Append("'" + strEnterpriseID + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Consignment',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1034)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}

								drEach[0]["new_account"] = "Y";
							}
							else
							{
								if (drEach[0]["payerid"].ToString().Trim() == "NEW")
								{
									drEach[0]["payerid"] = "NEW";

									//Error Checking : Code 1032
									if(((drEach[0]["payer_address1"] == null) || 
										(drEach[0]["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
										(drEach[0]["payer_address1"].ToString() == "")) || 
										((drEach[0]["payer_address2"] == null) || 
										(drEach[0]["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) ||
										(drEach[0]["payer_address2"].ToString() == "")))
									{
										strBuilder = new StringBuilder();
										strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
										strBuilder.Append("recordtype, applicationid, user_culture,");
										strBuilder.Append("error_code)");
										strBuilder.Append("VALUES(");
										strBuilder.Append("'" + recordid[i] + "',");
										strBuilder.Append("'" + strEnterpriseID + "',");
										strBuilder.Append("'" + userId + "',");
										strBuilder.Append("'Consignment',");
										strBuilder.Append("'" + strAppID + "',");
										strBuilder.Append("'" + user_culture + "',");
										strBuilder.Append("1032)");

										dbCmd.CommandText = strBuilder.ToString();
										iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
									}

									//Error Checking : Code 1033
									if((drEach[0]["payer_telephone"] == null) || 
										(drEach[0]["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
										(drEach[0]["payer_telephone"].ToString() == ""))
									{
										strBuilder = new StringBuilder();
										strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
										strBuilder.Append("recordtype, applicationid, user_culture,");
										strBuilder.Append("error_code)");
										strBuilder.Append("VALUES(");
										strBuilder.Append("'" + recordid[i] + "',");
										strBuilder.Append("'" + strEnterpriseID + "',");
										strBuilder.Append("'" + userId + "',");
										strBuilder.Append("'Consignment',");
										strBuilder.Append("'" + strAppID + "',");
										strBuilder.Append("'" + user_culture + "',");
										strBuilder.Append("1033)");

										dbCmd.CommandText = strBuilder.ToString();
										iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
									}

									//Error Checking : Code 1034
									if((drEach[0]["payer_zipcode"] != null) && 
										(!drEach[0]["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
										(drEach[0]["payer_zipcode"].ToString() != ""))
									{
							
										strBuilder = new StringBuilder();
										strBuilder.Append("SELECT * ");	
										strBuilder.Append("FROM Zipcode ");
										strBuilder.Append("where applicationid = '");
										strBuilder.Append(strAppID+"'");
										strBuilder.Append(" and enterpriseid = '");
										strBuilder.Append(strEnterpriseID+"'");
										strBuilder.Append(" and zipcode = '");
										strBuilder.Append(drEach[0]["payer_zipcode"].ToString().Trim()+"'");

										dbCmd.CommandText = strBuilder.ToString();
										DataSet dsZipcodeTmp = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  

										if(dsZipcodeTmp.Tables[0].Rows.Count == 0)
										{
											strBuilder = new StringBuilder();
											strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
											strBuilder.Append("recordtype, applicationid, user_culture,");
											strBuilder.Append("error_code)");
											strBuilder.Append("VALUES(");
											strBuilder.Append("'" + recordid[i] + "',");
											strBuilder.Append("'" + strEnterpriseID + "',");
											strBuilder.Append("'" + userId + "',");
											strBuilder.Append("'Consignment',");
											strBuilder.Append("'" + strAppID + "',");
											strBuilder.Append("'" + user_culture + "',");
											strBuilder.Append("1034)");

											dbCmd.CommandText = strBuilder.ToString();
											iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
										}

										dsZipcodeTmp.Dispose();
										dsZipcodeTmp = null;
									}
									else
									{
										strBuilder = new StringBuilder();
										strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
										strBuilder.Append("recordtype, applicationid, user_culture,");
										strBuilder.Append("error_code)");
										strBuilder.Append("VALUES(");
										strBuilder.Append("'" + recordid[i] + "',");
										strBuilder.Append("'" + strEnterpriseID + "',");
										strBuilder.Append("'" + userId + "',");
										strBuilder.Append("'Consignment',");
										strBuilder.Append("'" + strAppID + "',");
										strBuilder.Append("'" + user_culture + "',");
										strBuilder.Append("1034)");

										dbCmd.CommandText = strBuilder.ToString();
										iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
									}

									drEach[0]["new_account"] = "Y";
								}
								else
								{
									//Error Checking : Code 1031
									strBuilder = new StringBuilder();
									strBuilder.Append("SELECT * ");	
									strBuilder.Append("FROM Customer ");
									strBuilder.Append("where applicationid = '");
									strBuilder.Append(strAppID+"'");
									strBuilder.Append(" and enterpriseid = '");
									strBuilder.Append(strEnterpriseID+"'");
									strBuilder.Append(" and custid = '");
									strBuilder.Append(drEach[0]["payerid"].ToString().Trim()+"'");

									dbCmd.CommandText = strBuilder.ToString();
									DataSet dsCustomerTmp = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  

									if (dsCustomerTmp.Tables[0].Rows.Count == 0)
									{
										strBuilder = new StringBuilder();
										strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
										strBuilder.Append("recordtype, applicationid, user_culture,");
										strBuilder.Append("error_code)");
										strBuilder.Append("VALUES(");
										strBuilder.Append("'" + recordid[i] + "',");
										strBuilder.Append("'" + strEnterpriseID + "',");
										strBuilder.Append("'" + userId + "',");
										strBuilder.Append("'Consignment',");
										strBuilder.Append("'" + strAppID + "',");
										strBuilder.Append("'" + user_culture + "',");
										strBuilder.Append("1031)");

										dbCmd.CommandText = strBuilder.ToString();
										iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

										//Error Checking : Code 1032
										if(((drEach[0]["payer_address1"] == null) || 
											(drEach[0]["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
											(drEach[0]["payer_address1"].ToString() == "")) || 
											((drEach[0]["payer_address2"] == null) || 
											(drEach[0]["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) ||
											(drEach[0]["payer_address2"].ToString() == "")))
										{
											strBuilder = new StringBuilder();
											strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
											strBuilder.Append("recordtype, applicationid, user_culture,");
											strBuilder.Append("error_code)");
											strBuilder.Append("VALUES(");
											strBuilder.Append("'" + recordid[i] + "',");
											strBuilder.Append("'" + strEnterpriseID + "',");
											strBuilder.Append("'" + userId + "',");
											strBuilder.Append("'Consignment',");
											strBuilder.Append("'" + strAppID + "',");
											strBuilder.Append("'" + user_culture + "',");
											strBuilder.Append("1032)");

											dbCmd.CommandText = strBuilder.ToString();
											iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
										}

										//Error Checking : Code 1033
										if((drEach[0]["payer_telephone"] == null) || 
											(drEach[0]["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
											(drEach[0]["payer_telephone"].ToString() == ""))
										{
											strBuilder = new StringBuilder();
											strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
											strBuilder.Append("recordtype, applicationid, user_culture,");
											strBuilder.Append("error_code)");
											strBuilder.Append("VALUES(");
											strBuilder.Append("'" + recordid[i] + "',");
											strBuilder.Append("'" + strEnterpriseID + "',");
											strBuilder.Append("'" + userId + "',");
											strBuilder.Append("'Consignment',");
											strBuilder.Append("'" + strAppID + "',");
											strBuilder.Append("'" + user_culture + "',");
											strBuilder.Append("1033)");

											dbCmd.CommandText = strBuilder.ToString();
											iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
										}

										//Error Checking : Code 1034
										if((drEach[0]["payer_zipcode"] != null) && 
											(!drEach[0]["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
											(drEach[0]["payer_zipcode"].ToString() != ""))
										{
							
											strBuilder = new StringBuilder();
											strBuilder.Append("SELECT * ");	
											strBuilder.Append("FROM Zipcode ");
											strBuilder.Append("where applicationid = '");
											strBuilder.Append(strAppID+"'");
											strBuilder.Append(" and enterpriseid = '");
											strBuilder.Append(strEnterpriseID+"'");
											strBuilder.Append(" and zipcode = '");
											strBuilder.Append(drEach[0]["payer_zipcode"].ToString().Trim()+"'");

											dbCmd.CommandText = strBuilder.ToString();
											DataSet dsZipcodeTmp = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  

											if(dsZipcodeTmp.Tables[0].Rows.Count == 0)
											{
												strBuilder = new StringBuilder();
												strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
												strBuilder.Append("recordtype, applicationid, user_culture,");
												strBuilder.Append("error_code)");
												strBuilder.Append("VALUES(");
												strBuilder.Append("'" + recordid[i] + "',");
												strBuilder.Append("'" + strEnterpriseID + "',");
												strBuilder.Append("'" + userId + "',");
												strBuilder.Append("'Consignment',");
												strBuilder.Append("'" + strAppID + "',");
												strBuilder.Append("'" + user_culture + "',");
												strBuilder.Append("1034)");

												dbCmd.CommandText = strBuilder.ToString();
												iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
											}

											dsZipcodeTmp.Dispose();
											dsZipcodeTmp = null;
										}
										else
										{
											strBuilder = new StringBuilder();
											strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
											strBuilder.Append("recordtype, applicationid, user_culture,");
											strBuilder.Append("error_code)");
											strBuilder.Append("VALUES(");
											strBuilder.Append("'" + recordid[i] + "',");
											strBuilder.Append("'" + strEnterpriseID + "',");
											strBuilder.Append("'" + userId + "',");
											strBuilder.Append("'Consignment',");
											strBuilder.Append("'" + strAppID + "',");
											strBuilder.Append("'" + user_culture + "',");
											strBuilder.Append("1034)");

											dbCmd.CommandText = strBuilder.ToString();
											iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
										}
									}
									else
									{
										DataRow drTmpCustomerData = dsCustomerTmp.Tables[0].Rows[0];

										////BOON - Check Error: Code 1036 for Update -- start
										if(drTmpCustomerData["status_active"].ToString() == "N")
										{
											strBuilder = new StringBuilder();
											strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
											strBuilder.Append("recordtype, applicationid, user_culture,");
											strBuilder.Append("error_code)");
											strBuilder.Append("VALUES(");
											strBuilder.Append("'" + recordid[i] + "',");
											strBuilder.Append("'" + strEnterpriseID + "',");
											strBuilder.Append("'" + userId + "',");
											strBuilder.Append("'Consignment',");
											strBuilder.Append("'" + strAppID + "',");
											strBuilder.Append("'" + user_culture + "',");
											strBuilder.Append("1036)");

											dbCmd.CommandText = strBuilder.ToString();
											iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
										}
										////BOON - Check Error: Code 1036 for Update -- end

										//Removed by CRTS 954 Turn off Locking from over Credit Limit
										//Error Checking : Code 1037 Credit Available
										//										if((dsCustomerTmp.Tables[0].Rows[0]["creditstatus"] == null) || 
										//											(dsCustomerTmp.Tables[0].Rows[0]["creditstatus"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
										//											(dsCustomerTmp.Tables[0].Rows[0]["creditstatus"].ToString() == ""))
										//										{
										//											strBuilder = new StringBuilder();
										//											strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
										//											strBuilder.Append("recordtype, applicationid, user_culture,");
										//											strBuilder.Append("error_code)");
										//											strBuilder.Append("VALUES(");
										//											strBuilder.Append("'" + recordid[i] + "',");
										//											strBuilder.Append("'" + userId + "',");
										//											strBuilder.Append("'Consignment',");
										//											strBuilder.Append("'" + strAppID + "',");
										//											strBuilder.Append("'" + user_culture + "',");
										//											strBuilder.Append("1037)");
										//
										//											dbCmd.CommandText = strBuilder.ToString();
										//											iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
										//										}
										//										else
										//										{
										//											if(dsCustomerTmp.Tables[0].Rows[0]["creditstatus"].ToString().Equals("N"))
										//											{
										//												strBuilder = new StringBuilder();
										//												strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
										//												strBuilder.Append("recordtype, applicationid, user_culture,");
										//												strBuilder.Append("error_code)");
										//												strBuilder.Append("VALUES(");
										//												strBuilder.Append("'" + recordid[i] + "',");
										//												strBuilder.Append("'" + userId + "',");
										//												strBuilder.Append("'Consignment',");
										//												strBuilder.Append("'" + strAppID + "',");
										//												strBuilder.Append("'" + user_culture + "',");
										//												strBuilder.Append("1037)");
										//
										//												dbCmd.CommandText = strBuilder.ToString();
										//												iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
										//											}
										//										}//END Error Checking : Code 1037 Credit Available
										//										DataRow drTmpCustomerData = dsCustomerTmp.Tables[0].Rows[0];

										//Replace drConsignment "payer data" with data from Customer
										if((drTmpCustomerData["custid"] != null) && 
											(!drTmpCustomerData["custid"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
											(drTmpCustomerData["custid"].ToString() != ""))
											drEach[0]["payerid"] = drTmpCustomerData["custid"];
										else
											drEach[0]["payerid"] = "";
									
										if((drTmpCustomerData["cust_name"] != null) && 
											(!drTmpCustomerData["cust_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
											(drTmpCustomerData["cust_name"].ToString() != ""))
											drEach[0]["payer_name"] = drTmpCustomerData["cust_name"];
										else
											drEach[0]["payer_name"] = "";
								
										if((drTmpCustomerData["address1"] != null) && 
											(!drTmpCustomerData["address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
											(drTmpCustomerData["address1"].ToString() != ""))
											drEach[0]["payer_address1"] = drTmpCustomerData["address1"];
										else
											drEach[0]["payer_address1"] = "";
									
										if((drTmpCustomerData["address2"] != null) && 
											(!drTmpCustomerData["address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
											(drTmpCustomerData["address2"].ToString() != ""))
											drEach[0]["payer_address2"] = drTmpCustomerData["address2"];
										else
											drEach[0]["payer_address2"] = "";
									
										if((drTmpCustomerData["zipcode"] != null) && 
											(!drTmpCustomerData["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
											(drTmpCustomerData["zipcode"].ToString() != ""))
											drEach[0]["payer_zipcode"] = drTmpCustomerData["zipcode"];
										else
											drEach[0]["payer_zipcode"] = "";
									
										if((drTmpCustomerData["telephone"] != null) && 
											(!drTmpCustomerData["telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
											(drTmpCustomerData["telephone"].ToString() != ""))
											drEach[0]["payer_telephone"] = drTmpCustomerData["telephone"];
										else
											drEach[0]["payer_telephone"] = "";
									
										if((drTmpCustomerData["fax"] != null) && 
											(!drTmpCustomerData["fax"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
											(drTmpCustomerData["fax"].ToString() != ""))
											drEach[0]["payer_fax"] = drTmpCustomerData["fax"];
										else
											drEach[0]["payer_fax"] = "";
									
										if((drTmpCustomerData["payment_mode"] != null) && 
											(!drTmpCustomerData["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
											(drTmpCustomerData["payment_mode"].ToString() != ""))
											drEach[0]["payment_mode"] = drTmpCustomerData["payment_mode"];
										else
											drEach[0]["payment_mode"] = "";
									}

									dsCustomerTmp.Dispose();
									dsCustomerTmp = null;

									drEach[0]["new_account"] = "N";
								}
							}

							//Error Checking : Sender N/A
							strBuilder = new StringBuilder();
							strBuilder.Append("SELECT * ");	
							strBuilder.Append("FROM Customer_Snd_Rec ");
							strBuilder.Append("where applicationid = '");
							strBuilder.Append(strAppID+"'");
							strBuilder.Append(" and enterpriseid = '");
							strBuilder.Append(strEnterpriseID+"'");
							strBuilder.Append(" and snd_rec_name = '");
							strBuilder.Append(drEach[0]["sender_name"].ToString().Trim()+"'");
							//boon 2010/12/20
							strBuilder.Append(" and custid = '" + drEach[0]["payerid"].ToString().Trim() + "'");

							dbCmd.CommandText = strBuilder.ToString();
							DataSet dsSenderDataTmp = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  

							if (dsSenderDataTmp.Tables[0].Rows.Count > 0)
							{
								DataRow drSenderDataTmp = dsSenderDataTmp.Tables[0].Rows[0];

								//Replace drConsignment "sender data" with data from Cust_Snd_Rec
								if((drSenderDataTmp["snd_rec_name"] != null) && 
									(!drSenderDataTmp["snd_rec_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drSenderDataTmp["snd_rec_name"].ToString() != ""))
									drEach[0]["sender_name"] = drSenderDataTmp["snd_rec_name"];
								else
									drEach[0]["sender_name"] = "";
								
								if((drSenderDataTmp["address1"] != null) && 
									(!drSenderDataTmp["address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drSenderDataTmp["address1"].ToString() != ""))
									drEach[0]["sender_address1"] = drSenderDataTmp["address1"];
								else
									drEach[0]["sender_address1"] = "";
								
								if((drSenderDataTmp["address2"] != null) && 
									(!drSenderDataTmp["address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drSenderDataTmp["address2"].ToString() != ""))
									drEach[0]["sender_address2"] = drSenderDataTmp["address2"];
								else
									drEach[0]["sender_address2"] = "";
								
								if((drSenderDataTmp["zipcode"] != null) && 
									(!drSenderDataTmp["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drSenderDataTmp["zipcode"].ToString() != ""))
									drEach[0]["sender_zipcode"] = drSenderDataTmp["zipcode"];
								else
									drEach[0]["sender_zipcode"] = "";
								
								if((drSenderDataTmp["telephone"] != null) && 
									(!drSenderDataTmp["telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drSenderDataTmp["telephone"].ToString() != ""))
									drEach[0]["sender_telephone"] = drSenderDataTmp["telephone"];
								else
									drEach[0]["sender_telephone"] = "";
								
								if((drSenderDataTmp["fax"] != null) && 
									(!drSenderDataTmp["fax"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drSenderDataTmp["fax"].ToString() != ""))
									drEach[0]["sender_fax"] = drSenderDataTmp["fax"];
								else
									drEach[0]["sender_fax"] = "";
								
								if((drSenderDataTmp["contact_person"] != null) && 
									(!drSenderDataTmp["contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
									(drSenderDataTmp["contact_person"].ToString() != ""))
									drEach[0]["sender_contact_person"] = drSenderDataTmp["contact_person"];
								else
									drEach[0]["sender_contact_person"] = "";
							}
							else
							{
								//Error Checking : Code 1051
								if((drEach[0]["sender_zipcode"] != null) && (!drEach[0]["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["sender_zipcode"].ToString() != ""))
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("SELECT * ");	
									strBuilder.Append("FROM Zipcode ");
									strBuilder.Append("where applicationid = '");
									strBuilder.Append(strAppID+"'");
									strBuilder.Append(" and enterpriseid = '");
									strBuilder.Append(strEnterpriseID+"'");
									strBuilder.Append(" and zipcode = '");
									strBuilder.Append(drEach[0]["sender_zipcode"].ToString().Trim()+"'");

									dbCmd.CommandText = strBuilder.ToString();
									DataSet dsSenderZipDataTmp = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  

									if(dsSenderZipDataTmp.Tables[0].Rows.Count == 0)
									{
										strBuilder = new StringBuilder();
										strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
										strBuilder.Append("recordtype, applicationid, user_culture,");
										strBuilder.Append("error_code)");
										strBuilder.Append("VALUES(");
										strBuilder.Append("'" + recordid[i] + "',");
										strBuilder.Append("'" + strEnterpriseID + "',");
										strBuilder.Append("'" + userId + "',");
										strBuilder.Append("'Consignment',");
										strBuilder.Append("'" + strAppID + "',");
										strBuilder.Append("'" + user_culture + "',");
										strBuilder.Append("1051)");

										dbCmd.CommandText = strBuilder.ToString();
										iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
									}

									dsSenderZipDataTmp.Dispose();
									dsSenderZipDataTmp = null;
								}
								else
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + recordid[i] + "',");
									strBuilder.Append("'" + strEnterpriseID + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Consignment',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1051)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}

								//Error Checking : Code 1052
								if(((drEach[0]["sender_address1"] == null) || 
									(drEach[0]["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
									(drEach[0]["sender_address1"].ToString() == "")) || 
									((drEach[0]["sender_address2"] == null) || 
									(drEach[0]["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) ||
									(drEach[0]["sender_address2"].ToString() == "")))
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + recordid[i] + "',");
									strBuilder.Append("'" + strEnterpriseID + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Consignment',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1052)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}

								//Error Checking : Code 1053
								if((drEach[0]["sender_telephone"] == null) || 
									(drEach[0]["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
									(drEach[0]["sender_telephone"].ToString() == ""))
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + recordid[i] + "',");
									strBuilder.Append("'" + strEnterpriseID + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Consignment',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1053)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}

								//Error Checking : Code 1054
								if((drEach[0]["sender_contact_person"] == null) || 
									(drEach[0]["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
									(drEach[0]["sender_contact_person"].ToString() == ""))
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + recordid[i] + "',");
									strBuilder.Append("'" + strEnterpriseID + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Consignment',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1054)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}

								//Error Checking : Code 1055
								if((drEach[0]["sender_name"] == null) || 
									(drEach[0]["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
									(drEach[0]["sender_name"].ToString() == ""))
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + recordid[i] + "',");
									strBuilder.Append("'" + strEnterpriseID + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Consignment',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1055)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}
							}

							dsSenderDataTmp.Dispose();
							dsSenderDataTmp = null;
						}
						//End  Payer & Sender Checking
						#endregion Start Payer & Sender Checking

						#region Check Code 1041
						//Error Checking : Code 1041
						if((drEach[0]["payment_mode"].ToString().Trim() != "R") && (drEach[0]["payment_mode"].ToString().Trim() != "C"))
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + recordid[i] + "',");
							strBuilder.Append("'" + strEnterpriseID + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Consignment',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1041)");

							dbCmd.CommandText = strBuilder.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

							drEach[0]["payment_mode"] = System.DBNull.Value;
						}
						#endregion Check Code 1041

						#region Check Recipient
						//Error Checking : Recipient N/A
						strBuilder = new StringBuilder();
						strBuilder.Append("SELECT * ");	
						strBuilder.Append("FROM Customer_Snd_Rec ");
						strBuilder.Append("where applicationid = '");
						strBuilder.Append(strAppID+"'");
						strBuilder.Append(" and enterpriseid = '");
						strBuilder.Append(strEnterpriseID+"'");
						strBuilder.Append(" and snd_rec_name = '");
						strBuilder.Append(Utility.ReplaceSingleQuote(drEach[0]["recipient_name"].ToString().Trim())+"'");

						dbCmd.CommandText = strBuilder.ToString();
						DataSet dsRecipientDataTmp = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  

						if (1==2)
						{
							DataRow drRecipientDataTmp = dsRecipientDataTmp.Tables[0].Rows[0];

							//Replace drConsignment "recipient data" with data from Cust_Snd_Rec
							if((drRecipientDataTmp["snd_rec_name"] != null) && 
								(!drRecipientDataTmp["snd_rec_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drRecipientDataTmp["snd_rec_name"].ToString() != ""))
								drEach[0]["recipient_name"] = drRecipientDataTmp["snd_rec_name"];
							else
								drEach[0]["recipient_name"] = "";
							
							if((drRecipientDataTmp["address1"] != null) && 
								(!drRecipientDataTmp["address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drRecipientDataTmp["address1"].ToString() != ""))
								drEach[0]["recipient_address1"] = drRecipientDataTmp["address1"];
							else
								drEach[0]["recipient_address1"] = "";
							
							if((drRecipientDataTmp["address2"] != null) && 
								(!drRecipientDataTmp["address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drRecipientDataTmp["address2"].ToString() != ""))
								drEach[0]["recipient_address2"] = drRecipientDataTmp["address2"];
							else
								drEach[0]["recipient_address2"] = "";
							
							if((drRecipientDataTmp["zipcode"] != null) && 
								(!drRecipientDataTmp["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drRecipientDataTmp["zipcode"].ToString() != ""))
								drEach[0]["recipient_zipcode"] = drRecipientDataTmp["zipcode"];
							else
								drEach[0]["recipient_zipcode"] = "";
							
							if((drRecipientDataTmp["fax"] != null) && 
								(!drRecipientDataTmp["fax"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drRecipientDataTmp["fax"].ToString() != ""))
								drEach[0]["recipient_fax"] = drRecipientDataTmp["fax"];
							else
								drEach[0]["recipient_fax"] = "";
							
							if((drRecipientDataTmp["contact_person"] != null) && 
								(!drRecipientDataTmp["contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drRecipientDataTmp["contact_person"].ToString() != ""))
								drEach[0]["recipient_contact_person"] = drRecipientDataTmp["contact_person"];
							else
								drEach[0]["recipient_contact_person"] = "";
							
							if((drRecipientDataTmp["telephone"] != null) && 
								(!drRecipientDataTmp["telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
								(drRecipientDataTmp["telephone"].ToString() != ""))
								drEach[0]["recipient_telephone"] = drRecipientDataTmp["telephone"];
							else
								drEach[0]["recipient_telephone"] = "";
							
							if((drEach[0]["recipient_telephone"] != null) 
								&& (!drEach[0]["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) 
								&& (drEach[0]["recipient_telephone"].ToString() != "")) 
							{

								//Error Checking : Recipient N/A
								strBuilder = new StringBuilder();
								strBuilder.Append("SELECT * ");	
								strBuilder.Append("FROM v_References ");
								strBuilder.Append("where applicationid = '");
								strBuilder.Append(strAppID+"'");
								strBuilder.Append(" and enterpriseid = '");
								strBuilder.Append(strEnterpriseID+"'");
								strBuilder.Append(" and telephone = '");
								strBuilder.Append(drEach[0]["recipient_telephone"].ToString().Trim()+"'");

								dbCmd.CommandText = strBuilder.ToString();
								DataSet dsReferenceDataTmp = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  

								if (1==2)
								{
									DataRow drReferenceDataTmp = dsReferenceDataTmp.Tables[0].Rows[0];

									//Replace drConsignment "recipient data" with data from References
									if((drReferenceDataTmp["address1"] != null) && 
										(!drReferenceDataTmp["address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(drReferenceDataTmp["address1"].ToString() != ""))
										drEach[0]["recipient_address1"] = drReferenceDataTmp["address1"];
									else
										drEach[0]["recipient_address1"] = "";
									
									if((drReferenceDataTmp["address2"] != null) && 
										(!drReferenceDataTmp["address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(drReferenceDataTmp["address2"].ToString() != ""))
										drEach[0]["recipient_address2"] = drReferenceDataTmp["address2"];
									else
										drEach[0]["recipient_address2"] = "";
									
									if((drReferenceDataTmp["zipcode"] != null) && 
										(!drReferenceDataTmp["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(drReferenceDataTmp["zipcode"].ToString() != ""))
										drEach[0]["recipient_zipcode"] = drReferenceDataTmp["zipcode"];
									else
										drEach[0]["recipient_zipcode"] = "";
									
									if((drReferenceDataTmp["fax"] != null) && 
										(!drReferenceDataTmp["fax"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(drReferenceDataTmp["fax"].ToString() != ""))
										drEach[0]["recipient_fax"] = drReferenceDataTmp["fax"];
									else
										drEach[0]["recipient_fax"] = "";
									
									if((drReferenceDataTmp["contactperson"] != null) && 
										(!drReferenceDataTmp["contactperson"].GetType().Equals(System.Type.GetType("System.DBNull"))) &&
										(drReferenceDataTmp["contactperson"].ToString() != ""))
										drEach[0]["recipient_contact_person"] = drReferenceDataTmp["contactperson"];
									else
										drEach[0]["recipient_contact_person"] = "";
								}

								dsReferenceDataTmp.Dispose();
								dsReferenceDataTmp = null;
							}
						}
						else
						{
							//Error Checking : Code 1061
							if((drEach[0]["recipient_zipcode"] == null) || 
								(drEach[0]["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
								(drEach[0]["recipient_zipcode"].ToString().Trim() == ""))
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + recordid[i] + "',");
								strBuilder.Append("'" + strEnterpriseID + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1061)");

								dbCmd.CommandText = strBuilder.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							}
							else
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("SELECT * ");	
								strBuilder.Append("FROM Zipcode ");
								strBuilder.Append("where applicationid = '");
								strBuilder.Append(strAppID+"'");
								strBuilder.Append(" and enterpriseid = '");
								strBuilder.Append(strEnterpriseID+"'");
								strBuilder.Append(" and zipcode = '");
								strBuilder.Append(drEach[0]["recipient_zipcode"].ToString().Trim()+"'");

								dbCmd.CommandText = strBuilder.ToString();
								DataSet dsZipcodeDataTmp = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  

								if(dsZipcodeDataTmp.Tables[0].Rows.Count == 0)
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + recordid[i] + "',");
									strBuilder.Append("'" + strEnterpriseID + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Consignment',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1061)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}

								dsZipcodeDataTmp.Dispose();
								dsZipcodeDataTmp = null;
							}

							//Error Checking : Code 1062
							if(((drEach[0]["recipient_address1"] == null) || 
								(drEach[0]["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
								(drEach[0]["recipient_address1"].ToString().Trim() == "")) || 
								((drEach[0]["recipient_address2"] == null) || 
								(drEach[0]["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) ||
								(drEach[0]["recipient_address2"].ToString().Trim() == "")))
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + recordid[i] + "',");
								strBuilder.Append("'" + strEnterpriseID + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1062)");

								dbCmd.CommandText = strBuilder.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							}

							//Error Checking : Code 1063
							if((drEach[0]["recipient_telephone"] == null) || 
								(drEach[0]["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
								(drEach[0]["recipient_telephone"].ToString() == ""))
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + recordid[i] + "',");
								strBuilder.Append("'" + strEnterpriseID + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1063)");

								dbCmd.CommandText = strBuilder.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							}

							//Error Checking : Code 1064
							//							if((drEach[0]["recipient_contact_person"] == null) || 
							//								(drEach[0]["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
							//								(drEach[0]["recipient_contact_person"].ToString() == ""))
							//							{
							//								strBuilder = new StringBuilder();
							//								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
							//								strBuilder.Append("recordtype, applicationid, user_culture,");
							//								strBuilder.Append("error_code)");
							//								strBuilder.Append("VALUES(");
							//								strBuilder.Append("'" + recordid[i] + "',");
							//								strBuilder.Append("'" + userId + "',");
							//								strBuilder.Append("'Consignment',");
							//								strBuilder.Append("'" + strAppID + "',");
							//								strBuilder.Append("'" + user_culture + "',");
							//								strBuilder.Append("1064)");
							//
							//								dbCmd.CommandText = strBuilder.ToString();
							//								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							//							}

							//Error Checking : Code 1065
							if((drEach[0]["recipient_name"] == null) || 
								(drEach[0]["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) || 
								(drEach[0]["recipient_name"].ToString() == ""))
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + recordid[i] + "',");
								strBuilder.Append("'" + strEnterpriseID + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1065)");

								dbCmd.CommandText = strBuilder.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							}
						}
						dsRecipientDataTmp.Dispose();
						dsRecipientDataTmp = null;

						if((drEach[0]["recipient_zipcode"] != null) && 
							(!drEach[0]["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
							(drEach[0]["recipient_zipcode"].ToString() != ""))
						{
							//Error Checking : Code 1022
							String strSendZipCode="";
							String strRecipZipCode = "";
							String strServiceCode = "";

							if(!((drEach[0]["sender_zipcode"] == null) || (drEach[0]["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) || (drEach[0]["sender_zipcode"].ToString() == "")))
								strSendZipCode = drEach[0]["sender_zipcode"].ToString().Trim();
	
							if(!(((drEach[0]["recipient_zipcode"] == null) || (drEach[0]["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) || (drEach[0]["recipient_zipcode"].ToString() == ""))))
								strRecipZipCode = drEach[0]["recipient_zipcode"].ToString().Trim();

							if(!((drEach[0]["service_code"] == null) || (drEach[0]["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) || (drEach[0]["service_code"].ToString() == "")))
								strServiceCode = drEach[0]["service_code"].ToString().Trim();


							if(!Service.IsAvailable(strAppID,strEnterpriseID,strServiceCode,strSendZipCode,strRecipZipCode))//(dsTmpZipcode_Service_ExcludedData.Tables[0].Rows.Count <= 0)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + recordid[i] + "',");
								strBuilder.Append("'" + strEnterpriseID + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1022)");

								dbCmd.CommandText = strBuilder.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							}

						}
						#endregion Check Recipient

						#region Check Declare Value
						//Error Checking : Declare Value N/A
						if((drEach[0]["declare_value"] != null) && (!drEach[0]["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["declare_value"].ToString() != ""))
						{
							if(!IsNumeric(drEach[0]["declare_value"].ToString())) 
							{
								drEach[0]["declare_value"] = 0;
							}
							//							else  //comment by boon -- confirm by Express Team (Kai) 2010/12/20
							//							{
							//								//Error Checking : Code 1071
							//								strBuilder = new StringBuilder();
							//								strBuilder.Append("SELECT max_insurance_amt ");	
							//								strBuilder.Append("FROM Enterprise ");
							//								strBuilder.Append("where applicationid = '");
							//								strBuilder.Append(strAppID+"'");
							//								strBuilder.Append(" and enterpriseid = '");
							//								strBuilder.Append(strEnterpriseID+"'");
							//
							//								dbCmd.CommandText = strBuilder.ToString();
							//								DataSet dsEnterprise = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  
							//
							//								if(dsEnterprise.Tables[0].Rows.Count > 0)
							//								{
							//									if(Convert.ToDecimal(dsEnterprise.Tables[0].Rows[0]["max_insurance_amt"]) < Convert.ToDecimal(drEach[0]["declare_value"]))
							//									{
							//										strBuilder = new StringBuilder();
							//										strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
							//										strBuilder.Append("recordtype, applicationid, user_culture,");
							//										strBuilder.Append("error_code)");
							//										strBuilder.Append("VALUES(");
							//										strBuilder.Append("'" + recordid[i] + "',");
							//										strBuilder.Append("'" + userId + "',");
							//										strBuilder.Append("'Consignment',");
							//										strBuilder.Append("'" + strAppID + "',");
							//										strBuilder.Append("'" + user_culture + "',");
							//										strBuilder.Append("1071)");
							//
							//										dbCmd.CommandText = strBuilder.ToString();
							//										iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							//									}
							//								}
							//
							//								dsEnterprise.Dispose();
							//								dsEnterprise = null;
							//							}
						}
						else
						{
							drEach[0]["declare_value"] = 0;
						}
						#endregion Check Declare Value

						#region Check COD Amount
						//Error Checking : COD Amount N/A
						if((drEach[0]["cod_amount"] != null) && (!drEach[0]["cod_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["cod_amount"].ToString() != ""))
						{
							if(!IsNumeric(drEach[0]["cod_amount"].ToString())) 
							{
								drEach[0]["cod_amount"] = 0;
							}
							else
							{
								///Boon - Error Checking : Code 1072 for Update -- start
								Regex regCOD1 = new Regex(@"^[+-]?(?:\d+|\d{1,3}(?:,\d{3})+|(?:\d*|\d{1,3}(?:,\d{3})+)\.(\d{1}|\d{2}|\d{3}|\d{4}))$");

								if ((regCOD1.IsMatch(drEach[0]["cod_amount"].ToString()) == false) || (Convert.ToDouble(drEach[0]["cod_amount"]) > 99999999.9999))
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + recordid[i] + "',");
									strBuilder.Append("'" + strEnterpriseID + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Consignment',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1073)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}
								///Boon - Check Error: Code 1072 for Update -- end
							}
						}
						else
						{
							drEach[0]["cod_amount"] = 0;
						}
						#endregion Check COD Amount

						#region Check Code 1081
						//Error Checking : Code 1081
						if((drEach[0]["payment_type"].ToString().Trim() != "FP") && (drEach[0]["payment_type"].ToString().Trim() != "FC"))
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
							strBuilder.Append("recordtype, applicationid, user_culture,");
							strBuilder.Append("error_code)");
							strBuilder.Append("VALUES(");
							strBuilder.Append("'" + recordid[i] + "',");
							strBuilder.Append("'" + strEnterpriseID + "',");
							strBuilder.Append("'" + userId + "',");
							strBuilder.Append("'Consignment',");
							strBuilder.Append("'" + strAppID + "',");
							strBuilder.Append("'" + user_culture + "',");
							strBuilder.Append("1081)");

							dbCmd.CommandText = strBuilder.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

							drEach[0]["payment_type"] = System.DBNull.Value;
						}
						#endregion Check Code 1081

						#region Check Code 1091
						//Error Checking : Code 1091
						if((drEach[0]["commodity_code"] != null) && (!drEach[0]["commodity_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["commodity_code"].ToString() != ""))
						{
							strBuilder = new StringBuilder();
							strBuilder.Append("SELECT * ");	
							strBuilder.Append("FROM Commodity ");
							strBuilder.Append("where applicationid = '");
							strBuilder.Append(strAppID+"'");
							strBuilder.Append(" and enterpriseid = '");
							strBuilder.Append(strEnterpriseID+"'");
							strBuilder.Append(" and commodity_code = '");
							strBuilder.Append(drEach[0]["commodity_code"].ToString().Trim()+"'");

							dbCmd.CommandText = strBuilder.ToString();
							DataSet dsCommodity = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);  

							if(dsCommodity.Tables[0].Rows.Count == 0)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + recordid[i] + "',");
								strBuilder.Append("'" + strEnterpriseID + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1091)");

								dbCmd.CommandText = strBuilder.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							}

							dsCommodity.Dispose();
							dsCommodity = null;
						}
						//						else
						//						{
						//							strBuilder = new StringBuilder();
						//							strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
						//							strBuilder.Append("recordtype, applicationid, user_culture,");
						//							strBuilder.Append("error_code)");
						//							strBuilder.Append("VALUES(");
						//							strBuilder.Append("'" + recordid[i] + "',");
						//							strBuilder.Append("'" + userId + "',");
						//							strBuilder.Append("'Consignment',");
						//							strBuilder.Append("'" + strAppID + "',");
						//							strBuilder.Append("'" + user_culture + "',");
						//							strBuilder.Append("1091)");
						//
						//							dbCmd.CommandText = strBuilder.ToString();
						//							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						//						}
						#endregion Check Code 1091

						#region Check Code 1201
						//HC Return Task
						//Error Checking : Code 1201
						if((drEach[0]["return_pod_hc"] != null) && 
							(!drEach[0]["return_pod_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
							(drEach[0]["return_pod_hc"].ToString() != ""))
						{
							if((drEach[0]["return_pod_hc"].ToString().Trim() != "D") && 
								(drEach[0]["return_pod_hc"].ToString().Trim() != "Y") &&
								(drEach[0]["return_pod_hc"].ToString().Trim() != "N"))
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach[0]["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + strEnterpriseID + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1201)");

								dbCmd.CommandText = strBuilder.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							}
						}
						#endregion Check Code 1201

						#region Check Code 1202
						//Error Checking : Code 1202
						if((drEach[0]["return_invoice_hc"] != null) && 
							(!drEach[0]["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))) && 
							(drEach[0]["return_invoice_hc"].ToString() != ""))
						{
							if((drEach[0]["return_invoice_hc"].ToString().Trim() != "D") && 
								(drEach[0]["return_invoice_hc"].ToString().Trim() != "Y") &&
								(drEach[0]["return_invoice_hc"].ToString().Trim() != "N"))
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, enterpriseid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach[0]["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + strEnterpriseID + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Consignment',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1202)");

								dbCmd.CommandText = strBuilder.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							}
						}
						//HC Return Task
						#endregion Check Code 1201

						#region Update Consignment
						//Update Consignment Data
						strBuilder = new StringBuilder();
						strBuilder.Append("UPDATE ImportedConsignments ");
						strBuilder.Append("SET ");

						//booking_no
						strBuilder.Append("booking_no = ");
						if((drEach[0]["booking_no"]!= null) && (!drEach[0]["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["booking_no"].ToString().Trim() != ""))
						{
							int booking_no = Convert.ToInt32(drEach[0]["booking_no"]);
							strBuilder.Append(booking_no);
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//consignment_no
						strBuilder.Append("consignment_no = ");
						if((drEach[0]["consignment_no"]!= null) && (!drEach[0]["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["consignment_no"].ToString().Trim() != ""))
						{
							String consignment_no = Utility.ReplaceSingleQuote(drEach[0]["consignment_no"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(consignment_no);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//ref_no
						strBuilder.Append("ref_no = ");
						if((drEach[0]["ref_no"]!= null) && (!drEach[0]["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["ref_no"].ToString().Trim() != ""))
						{
							String ref_no = Utility.ReplaceSingleQuote(drEach[0]["ref_no"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(ref_no);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//payerid
						strBuilder.Append("payerid = ");
						if((drEach[0]["payerid"]!= null) && (!drEach[0]["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["payerid"].ToString().Trim() != ""))
						{
							String payerid = Utility.ReplaceSingleQuote(drEach[0]["payerid"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(payerid);
							strBuilder.Append("'");
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//payer_name
						strBuilder.Append("payer_name = ");
						if((drEach[0]["payer_name"]!= null) && (!drEach[0]["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["payer_name"].ToString().Trim() != ""))
						{
							String payer_name = Utility.ReplaceSingleQuote(drEach[0]["payer_name"].ToString());
							strBuilder.Append("N'");
							strBuilder.Append(payer_name);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//payer_address1
						strBuilder.Append("payer_address1 = ");
						if((drEach[0]["payer_address1"]!= null) && (!drEach[0]["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["payer_address1"].ToString().Trim() != ""))
						{
							String payer_address1 = Utility.ReplaceSingleQuote(drEach[0]["payer_address1"].ToString());
							strBuilder.Append("N'");
							strBuilder.Append(payer_address1);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//payer_address2
						strBuilder.Append("payer_address2 = ");
						if((drEach[0]["payer_address2"]!= null) && (!drEach[0]["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["payer_address2"].ToString().Trim() != ""))
						{
							String payer_address2 = Utility.ReplaceSingleQuote(drEach[0]["payer_address2"].ToString());
							strBuilder.Append("N'");
							strBuilder.Append(payer_address2);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//payer_zipcode
						strBuilder.Append("payer_zipcode = ");
						if((drEach[0]["payer_zipcode"]!= null) && (!drEach[0]["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["payer_zipcode"].ToString().Trim() != ""))
						{
							String payer_zipcode = Utility.ReplaceSingleQuote(drEach[0]["payer_zipcode"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(payer_zipcode);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//payer_telephone
						strBuilder.Append("payer_telephone = ");
						if((drEach[0]["payer_telephone"]!= null) && (!drEach[0]["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["payer_telephone"].ToString().Trim() != ""))
						{
							String payer_telephone = Utility.ReplaceSingleQuote(drEach[0]["payer_telephone"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(payer_telephone);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//payer_fax
						strBuilder.Append("payer_fax = ");
						if((drEach[0]["payer_fax"]!= null) && (!drEach[0]["payer_fax"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach[0]["payer_fax"].ToString().Trim() != ""))
						{
							String payer_fax = Utility.ReplaceSingleQuote(drEach[0]["payer_fax"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(payer_fax);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//payment_mode
						strBuilder.Append("payment_mode = ");
						if((drEach[0]["payment_mode"]!= null) && (!drEach[0]["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach[0]["payment_mode"].ToString().Trim() != ""))
						{
							String payment_mode = Utility.ReplaceSingleQuote(drEach[0]["payment_mode"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(payment_mode);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//sender_name
						strBuilder.Append("sender_name = ");
						if((drEach[0]["sender_name"]!= null) && (!drEach[0]["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["sender_name"].ToString().Trim() != ""))
						{
							String sender_name = Utility.ReplaceSingleQuote(drEach[0]["sender_name"].ToString());
							strBuilder.Append("N'");
							strBuilder.Append(sender_name);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//sender_address1
						strBuilder.Append("sender_address1 = ");
						if((drEach[0]["sender_address1"]!= null) && (!drEach[0]["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["sender_address1"].ToString().Trim() != ""))
						{
							String sender_address1 = Utility.ReplaceSingleQuote(drEach[0]["sender_address1"].ToString());
							strBuilder.Append("N'");
							strBuilder.Append(sender_address1);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//sender_address2
						strBuilder.Append("sender_address2 = ");
						if((drEach[0]["sender_address2"]!= null) && (!drEach[0]["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["sender_address2"].ToString().Trim() != ""))
						{
							String sender_address2 = Utility.ReplaceSingleQuote(drEach[0]["sender_address2"].ToString());
							strBuilder.Append("N'");
							strBuilder.Append(sender_address2);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//sender_zipcode
						strBuilder.Append("sender_zipcode = ");
						if((drEach[0]["sender_zipcode"]!= null) && (!drEach[0]["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["sender_zipcode"].ToString().Trim() != ""))
						{
							String sender_zipcode = Utility.ReplaceSingleQuote(drEach[0]["sender_zipcode"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(sender_zipcode);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//sender_telephone
						strBuilder.Append("sender_telephone = ");
						if((drEach[0]["sender_telephone"]!= null) && (!drEach[0]["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["sender_telephone"].ToString().Trim() != ""))
						{
							String sender_telephone = Utility.ReplaceSingleQuote(drEach[0]["sender_telephone"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(sender_telephone);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//sender_fax
						strBuilder.Append("sender_fax = ");
						if((drEach[0]["sender_fax"]!= null) && (!drEach[0]["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["sender_fax"].ToString().Trim() != ""))
						{
							String sender_fax = Utility.ReplaceSingleQuote(drEach[0]["sender_fax"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(sender_fax);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//sender_contact_person
						strBuilder.Append("sender_contact_person = ");
						if((drEach[0]["sender_contact_person"]!= null) && (!drEach[0]["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["sender_contact_person"].ToString().Trim() != ""))
						{
							String sender_contact_person = Utility.ReplaceSingleQuote(drEach[0]["sender_contact_person"].ToString());
							strBuilder.Append("N'");
							strBuilder.Append(sender_contact_person);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//recipient_name
						strBuilder.Append("recipient_name = ");
						if((drEach[0]["recipient_name"]!= null) && (!drEach[0]["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["recipient_name"].ToString().Trim() != ""))
						{
							String recipient_name = Utility.ReplaceSingleQuote(drEach[0]["recipient_name"].ToString());
							strBuilder.Append("N'");
							strBuilder.Append(recipient_name);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//recipient_address1
						strBuilder.Append("recipient_address1 = ");
						if((drEach[0]["recipient_address1"]!= null) && (!drEach[0]["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["recipient_address1"].ToString().Trim() != ""))
						{
							String recipient_address1 = Utility.ReplaceSingleQuote(drEach[0]["recipient_address1"].ToString());
							strBuilder.Append("N'");
							strBuilder.Append(recipient_address1);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//recipient_address2
						strBuilder.Append("recipient_address2 = ");
						if((drEach[0]["recipient_address2"]!= null) && (!drEach[0]["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["recipient_address2"].ToString().Trim() != ""))
						{
							String recipient_address2 = Utility.ReplaceSingleQuote(drEach[0]["recipient_address2"].ToString());
							strBuilder.Append("N'");
							strBuilder.Append(recipient_address2);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//recipient_zipcode
						strBuilder.Append("recipient_zipcode = ");
						if((drEach[0]["recipient_zipcode"]!= null) && (!drEach[0]["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["recipient_zipcode"].ToString().Trim() != ""))
						{
							String recipient_zipcode = Utility.ReplaceSingleQuote(drEach[0]["recipient_zipcode"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(recipient_zipcode);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//recipient_telephone
						strBuilder.Append("recipient_telephone = ");
						if((drEach[0]["recipient_telephone"]!= null) && (!drEach[0]["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["recipient_telephone"].ToString().Trim() != ""))
						{
							String recipient_telephone = Utility.ReplaceSingleQuote(drEach[0]["recipient_telephone"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(recipient_telephone);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//recipient_fax
						strBuilder.Append("recipient_fax = ");
						if((drEach[0]["recipient_fax"]!= null) && (!drEach[0]["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["recipient_fax"].ToString().Trim() != ""))
						{
							String recipient_fax = Utility.ReplaceSingleQuote(drEach[0]["recipient_fax"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(recipient_fax);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//recipient_contact_person
						strBuilder.Append("recipient_contact_person = ");
						if((drEach[0]["recipient_contact_person"]!= null) && (!drEach[0]["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["recipient_contact_person"].ToString().Trim() != ""))
						{
							String recipient_contact_person = Utility.ReplaceSingleQuote(drEach[0]["recipient_contact_person"].ToString());
							strBuilder.Append("N'");
							strBuilder.Append(recipient_contact_person);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//service_code
						strBuilder.Append("service_code = ");
						if((drEach[0]["service_code"]!= null) && (!drEach[0]["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["service_code"].ToString().Trim() != ""))
						{
							String service_code = Utility.ReplaceSingleQuote(drEach[0]["service_code"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(service_code);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//declare_value
						strBuilder.Append("declare_value = ");
						if((drEach[0]["declare_value"]!= null) && (!drEach[0]["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["declare_value"].ToString().Trim() != ""))
						{
							decimal declare_value = Convert.ToDecimal(drEach[0]["declare_value"]);
							strBuilder.Append(declare_value);
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//payment_type
						strBuilder.Append("payment_type = ");
						if((drEach[0]["payment_type"]!= null) && (!drEach[0]["payment_type"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["payment_type"].ToString().Trim() != ""))
						{
							String payment_type = Utility.ReplaceSingleQuote(drEach[0]["payment_type"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(payment_type);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//commodity_code
						strBuilder.Append("commodity_code = ");
						if((drEach[0]["commodity_code"]!= null) && (!drEach[0]["commodity_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["commodity_code"].ToString().Trim() != ""))
						{
							String commodity_code = Utility.ReplaceSingleQuote(drEach[0]["commodity_code"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(commodity_code);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//remark
						strBuilder.Append("remark = ");
						if((drEach[0]["remark"]!= null) && (!drEach[0]["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["remark"].ToString().Trim() != ""))
						{
							String remark = Utility.ReplaceSingleQuote(drEach[0]["remark"].ToString());
							strBuilder.Append("N'");
							strBuilder.Append(remark);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//cod_amount
						strBuilder.Append("cod_amount = ");
						if((drEach[0]["cod_amount"]!= null) && (!drEach[0]["cod_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["cod_amount"].ToString().Trim() != ""))
						{
							decimal cod_amount = Convert.ToDecimal(drEach[0]["cod_amount"]);
							strBuilder.Append(cod_amount);	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//new_account
						strBuilder.Append("new_account = ");
						if((drEach[0]["new_account"]!= null) && (!drEach[0]["new_account"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["new_account"].ToString().Trim() != ""))
						{
							String new_account = Utility.ReplaceSingleQuote(drEach[0]["new_account"].ToString());
							strBuilder.Append("N'");
							strBuilder.Append(new_account);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//HC Return Task
						//return_pod_hc
						strBuilder.Append("return_pod_hc = ");
						if((drEach[0]["return_pod_hc"]!= null) && (!drEach[0]["return_pod_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["return_pod_hc"].ToString().Trim() != ""))
						{
							String return_pod_hc = Utility.ReplaceSingleQuote(drEach[0]["return_pod_hc"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(return_pod_hc);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						strBuilder.Append(", ");

						//return_invoice_hc
						strBuilder.Append("return_invoice_hc = ");
						if((drEach[0]["return_invoice_hc"]!= null) && (!drEach[0]["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["return_invoice_hc"].ToString().Trim() != ""))
						{
							String return_invoice_hc = Utility.ReplaceSingleQuote(drEach[0]["return_invoice_hc"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(return_invoice_hc);
							strBuilder.Append("'");	
						}
						else
						{
							strBuilder.Append("null");
						}
						//HC Return Task
						
						//Boon 29 Dec 10 =========> Start
						//Booking_datetime
						if((drEach[0]["booking_datetime"]!= null) && (!drEach[0]["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))&& (drEach[0]["booking_datetime"].ToString().Trim() != ""))
						{
							strBuilder.Append(", ");
							strBuilder.Append("booking_datetime = ");
							String booking_dt = Utility.ReplaceSingleQuote(drEach[0]["booking_datetime"].ToString());
							strBuilder.Append("'");
							strBuilder.Append(booking_dt);
							strBuilder.Append("'");	
						}
						//Boon 29 Dec 10 =========> End
						
						strBuilder.Append(" ");
						strBuilder.Append("WHERE recordid = '" + recordid[i] + "' ");
						strBuilder.Append("AND userid = '" + userId + "' ");
					
						dbCmd.CommandText = strBuilder.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						#endregion Update Consignment
					}
				}

				transactionApp.Commit();
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("ImportConsignmentsDAL","UpdateConsignment","UpdateConsignment","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ImportConsignmentsDAL","UpdateConsignment","UpdateConsignment","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("ImportConsignmentsDAL","UpdateConsignment","UpdateConsignment","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error update audit ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("ImportConsignmentsDAL","UpdateConsignment","UpdateConsignment","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ImportConsignmentsDAL","UpdateConsignment","UpdateConsignment","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("ImportConsignmentsDAL","UpdateConsignment","UpdateConsignment","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error update audit ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;
		}

		
		public static int UpdatePackage(String strAppID,String strEnterpriseID,
			DataSet dsConsignment, DataSet dsPackage, ArrayList recordid, String userId, String user_culture)
		{
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","UpdatePackage","UpdatePackage","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}

			IDbConnection conApp = dbCon.GetConnection();
			if(conApp == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","UpdatePackage","UpdatePackage","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ImportConsignmentsDAL","UpdatePackage","UpdatePackage","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("ImportConsignmentsDAL","UpdatePackage","UpdatePackage","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","UpdatePackage","UpdatePackage","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}
			try
			{
				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append("select count(*) from Core_Enterprise");
				dbCmd = dbCon.CreateCommand(strBuilder.ToString() , CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				for(int i = 0; i <= recordid.Count - 1; i++)
				{
					DataRow[] drEach = dsPackage.Tables[0].Select("recordid = '" + recordid[i] + "'");

					if (drEach.Length > 0)
					{
						//delete audit of consignment record from the audit table
						strBuilder = new StringBuilder();
						strBuilder.Append("DELETE " );
						strBuilder.Append("FROM ImportedConsignmentsAuditFailures ");
						strBuilder.Append("WHERE recordid = '" + recordid[i] + "' ");
						strBuilder.Append("AND userid = '" + userId + "' ");
						strBuilder.Append("AND recordtype = 'Package' ");

						dbCmd.CommandText = strBuilder.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

						//Error Checking : N/A Consignment No for Package
						if(dsConsignment.Tables[0].Select("consignment_no = '" + drEach[0]["consignment_no"] + "'").Length > 0)
						{
							//Error Checking : Code 1101
							if(dsPackage.Tables[0].Select("consignment_no = '" + drEach[0]["consignment_no"] + "' and mps_code = '" + drEach[0]["mps_code"] + "'").Length > 1)
							{
								strBuilder = new StringBuilder();
								strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
								strBuilder.Append("recordtype, applicationid, user_culture,");
								strBuilder.Append("error_code)");
								strBuilder.Append("VALUES(");
								strBuilder.Append("'" + drEach[0]["recordid"].ToString().Trim() + "',");
								strBuilder.Append("'" + userId + "',");
								strBuilder.Append("'Package',");
								strBuilder.Append("'" + strAppID + "',");
								strBuilder.Append("'" + user_culture + "',");
								strBuilder.Append("1101)");

								dbCmd.CommandText = strBuilder.ToString();
								iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							}

							//Error Checking : N/A Length
							if((drEach[0]["pkg_length"] != null) && (!drEach[0]["pkg_length"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["pkg_length"].ToString() != ""))
							{
								if(!IsNumeric(drEach[0]["pkg_length"].ToString())) 
								{
									drEach[0]["pkg_length"] = 0;
								}
							}
							else
							{
								drEach[0]["pkg_length"] = 0;
							}

							if((drEach[0]["pkg_length"] != null) && (!drEach[0]["pkg_length"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["pkg_length"].ToString() != ""))
							{
								decimal tmpLength = Convert.ToDecimal(drEach[0]["pkg_length"]);

								//Error Checking : Code 1111
								if(tmpLength <= 0)
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + drEach[0]["recordid"].ToString().Trim() + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Package',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1111)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}

								//Error Checking : Code 1112
								if(tmpLength > 10000)
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + drEach[0]["recordid"].ToString().Trim() + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Package',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1112)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}
							}

							//Error Checking : N/A Breadth
							if((drEach[0]["pkg_breadth"] != null) && (!drEach[0]["pkg_breadth"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["pkg_breadth"].ToString() != ""))
							{
								if(!IsNumeric(drEach[0]["pkg_breadth"].ToString())) 
								{
									drEach[0]["pkg_breadth"] = 0;
								}
							}
							else
							{
								drEach[0]["pkg_breadth"] = 0;
							}

						
							if((drEach[0]["pkg_breadth"] != null) && (!drEach[0]["pkg_breadth"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["pkg_breadth"].ToString() != ""))
							{
								decimal tmpBreadth = Convert.ToDecimal(drEach[0]["pkg_breadth"]);

								//Error Checking : Code 1113
								if(tmpBreadth <= 0)
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + drEach[0]["recordid"].ToString().Trim() + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Package',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1113)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}

								//Error Checking : Code 1114
								if(tmpBreadth > 10000)
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + drEach[0]["recordid"].ToString().Trim() + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Package',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1114)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}
							}

							//Error Checking : N/A Height
							if((drEach[0]["pkg_height"] != null) && (!drEach[0]["pkg_height"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["pkg_height"].ToString() != ""))
							{
								if(!IsNumeric(drEach[0]["pkg_height"].ToString())) 
								{
									drEach[0]["pkg_height"] = 0;
								}
							}
							else
							{
								drEach[0]["pkg_height"] = 0;
							}

						
							if((drEach[0]["pkg_height"] != null) && (!drEach[0]["pkg_height"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["pkg_height"].ToString() != ""))
							{
								decimal tmpHeight = Convert.ToDecimal(drEach[0]["pkg_height"]);

								//Error Checking : Code 1115
								if(tmpHeight <= 0)
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + drEach[0]["recordid"].ToString().Trim() + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Package',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1115)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}

								//Error Checking : Code 1116
								if(tmpHeight > 10000)
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + drEach[0]["recordid"].ToString().Trim() + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Package',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1116)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}
							}

							//Error Checking : N/A Weight
							if((drEach[0]["pkg_wt"] != null) && (!drEach[0]["pkg_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["pkg_wt"].ToString() != ""))
							{
								if(!IsNumeric(drEach[0]["pkg_wt"].ToString())) 
								{
									drEach[0]["pkg_wt"] = 0;
								}
							}
							else
							{
								drEach[0]["pkg_wt"] = 0;
							}

						
							if((drEach[0]["pkg_wt"] != null) && (!drEach[0]["pkg_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["pkg_wt"].ToString() != ""))
							{
								decimal tmpWT = Convert.ToDecimal(drEach[0]["pkg_wt"]);

								//Error Checking : Code 1117
								if(tmpWT <= 0)
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + drEach[0]["recordid"].ToString().Trim() + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Package',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1117)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}

								//Error Checking : Code 1118
								if(tmpWT > 10000)
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + drEach[0]["recordid"].ToString().Trim() + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Package',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1118)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}
							}

							//Error Checking : N/A Quantity
							if((drEach[0]["pkg_qty"] != null) && (!drEach[0]["pkg_qty"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["pkg_qty"].ToString() != ""))
							{
								if(!IsNumeric(drEach[0]["pkg_qty"].ToString())) 
								{
									drEach[0]["pkg_qty"] = 0;
								}
							}
							else
							{
								drEach[0]["pkg_qty"] = 0;
							}

						
							if((drEach[0]["pkg_qty"] != null) && (!drEach[0]["pkg_qty"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["pkg_qty"].ToString() != ""))
							{
								decimal tmpQTY = Convert.ToDecimal(drEach[0]["pkg_qty"]);

								//Error Checking : Code 1119
								if(tmpQTY <= 0)
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + drEach[0]["recordid"].ToString().Trim() + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Package',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1119)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}

								//Error Checking : Code 1120
								if(tmpQTY > 9999)
								{
									strBuilder = new StringBuilder();
									strBuilder.Append("INSERT INTO ImportedConsignmentsAuditFailures(recordid, userid, ");
									strBuilder.Append("recordtype, applicationid, user_culture,");
									strBuilder.Append("error_code)");
									strBuilder.Append("VALUES(");
									strBuilder.Append("'" + drEach[0]["recordid"].ToString().Trim() + "',");
									strBuilder.Append("'" + userId + "',");
									strBuilder.Append("'Package',");
									strBuilder.Append("'" + strAppID + "',");
									strBuilder.Append("'" + user_culture + "',");
									strBuilder.Append("1120)");

									dbCmd.CommandText = strBuilder.ToString();
									iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
								}
							}

							strBuilder = new StringBuilder();
							strBuilder.Append("UPDATE ImportedConsignmentsPkgs ");
							strBuilder.Append("SET ");

							//consignment_no
							strBuilder.Append("consignment_no = ");
							if((drEach[0]["consignment_no"]!= null) && (!drEach[0]["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["consignment_no"].ToString().Trim() != ""))
							{
								String consignment_no = Utility.ReplaceSingleQuote(drEach[0]["consignment_no"].ToString());
								strBuilder.Append("'");
								strBuilder.Append(consignment_no);
								strBuilder.Append("'");
							}
							else
							{
								strBuilder.Append("null");
							}
							strBuilder.Append(", ");

							//mps_code
							strBuilder.Append("mps_code = ");
							if((drEach[0]["mps_code"]!= null) && (!drEach[0]["mps_code"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["mps_code"].ToString().Trim() != ""))
							{
								String mps_code = Utility.ReplaceSingleQuote(drEach[0]["mps_code"].ToString());
								strBuilder.Append("'");
								strBuilder.Append(mps_code);
								strBuilder.Append("'");
							}
							else
							{
								strBuilder.Append("null");
							}
							strBuilder.Append(", ");

							//pkg_length
							strBuilder.Append("pkg_length = ");
							if((drEach[0]["pkg_length"]!= null) && (!drEach[0]["pkg_length"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["pkg_length"].ToString().Trim() != ""))
							{
								String pkg_length = Utility.ReplaceSingleQuote(drEach[0]["pkg_length"].ToString());
								strBuilder.Append("'");
								strBuilder.Append(pkg_length);
								strBuilder.Append("'");
							}
							else
							{
								strBuilder.Append("null");
							}
							strBuilder.Append(", ");

							//pkg_breadth
							strBuilder.Append("pkg_breadth = ");
							if((drEach[0]["pkg_breadth"]!= null) && (!drEach[0]["pkg_breadth"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["pkg_breadth"].ToString().Trim() != ""))
							{
								String pkg_breadth = Utility.ReplaceSingleQuote(drEach[0]["pkg_breadth"].ToString());
								strBuilder.Append("'");
								strBuilder.Append(pkg_breadth);
								strBuilder.Append("'");	
							}
							else
							{
								strBuilder.Append("null");
							}
							strBuilder.Append(", ");

							//pkg_height
							strBuilder.Append("pkg_height = ");
							if((drEach[0]["pkg_height"]!= null) && (!drEach[0]["pkg_height"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["pkg_height"].ToString().Trim() != ""))
							{
								String pkg_height = Utility.ReplaceSingleQuote(drEach[0]["pkg_height"].ToString());
								strBuilder.Append("'");
								strBuilder.Append(pkg_height);
								strBuilder.Append("'");	
							}
							else
							{
								strBuilder.Append("null");
							}
							strBuilder.Append(", ");

							//pkg_wt
							strBuilder.Append("pkg_wt = ");
							if((drEach[0]["pkg_wt"]!= null) && (!drEach[0]["pkg_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["pkg_wt"].ToString().Trim() != ""))
							{
								String pkg_wt = Utility.ReplaceSingleQuote(drEach[0]["pkg_wt"].ToString());
								strBuilder.Append("'");
								strBuilder.Append(pkg_wt);
								strBuilder.Append("'");	
							}
							else
							{
								strBuilder.Append("null");
							}
							strBuilder.Append(", ");

							//pkg_qty
							strBuilder.Append("pkg_qty = ");
							if((drEach[0]["pkg_qty"]!= null) && (!drEach[0]["pkg_qty"].GetType().Equals(System.Type.GetType("System.DBNull"))) && (drEach[0]["pkg_qty"].ToString().Trim() != ""))
							{
								String pkg_qty = Utility.ReplaceSingleQuote(drEach[0]["pkg_qty"].ToString());
								strBuilder.Append("'");
								strBuilder.Append(pkg_qty);
								strBuilder.Append("'");	
							}
							else
							{
								strBuilder.Append("null");
							}

							strBuilder.Append(" ");
							strBuilder.Append("WHERE recordid = '" + recordid[i] + "' ");
							strBuilder.Append("AND userid = '" + userId + "' ");

							dbCmd.CommandText = strBuilder.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						}
						else
						{
							//delete audit of consignment record from the audit table
							strBuilder = new StringBuilder();
							strBuilder.Append("DELETE " );
							strBuilder.Append("FROM ImportedConsignmentsPkgs ");
							strBuilder.Append("WHERE recordid = '" + recordid[i] + "' ");
							strBuilder.Append("AND userid = '" + userId + "' ");

							dbCmd.CommandText = strBuilder.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						}
					}
				}
				transactionApp.Commit();
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("ImportConsignmentsDAL","UpdatePackage","UpdatePackage","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ImportConsignmentsDAL","UpdatePackage","UpdatePackage","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("ImportConsignmentsDAL","UpdatePackage","UpdatePackage","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error update audit ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("ImportConsignmentsDAL","UpdatePackage","UpdatePackage","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("ImportConsignmentsDAL","UpdatePackage","UpdatePackage","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("ImportConsignmentsDAL","UpdatePackage","UpdatePackage","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error update audit ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iRowsAffected;
		}

		
		private static bool IsNumeric(string str) 
		{
			try 
			{
				Convert.ToDecimal(str);
				return true;
			}
			catch(FormatException) 
			{
				return false;
			}
		}


		public static DataSet GetCoreSysetmCode(String strAppID, String strEnterpriseID,
			String strCodeID, int iCurRow, int iDSRowSize, String user_culture)
		{
			SessionDS sessionDS = null;			
			String strSQL = "";

			strSQL = "SELECT code_text, code_str_value "; 	
			strSQL += "FROM core_system_code ";
			strSQL += "WHERE applicationid = '" + strAppID + "' ";
			strSQL += "AND codeid = '" + strCodeID + "' ";
			strSQL += "AND culture = '" + user_culture + "' ";
			strSQL += "ORDER BY sequence asc ";
	
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","GetCoreSysetmCode","GetCoreSysetmCode","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			
			sessionDS = dbCon.ExecuteQuery(strSQL, iCurRow, iDSRowSize, "core_system_code");
			return  sessionDS.ds;
		}

		//HC Return Task
		public static DataSet GetCoreSysetmCodeNumVal(String strAppID, String strEnterpriseID,
			String strCodeID, int iCurRow, int iDSRowSize)
		{
			SessionDS sessionDS = null;			
			String strSQL = "";

			strSQL = "SELECT code_num_value "; 	
			strSQL += "FROM core_system_code ";
			strSQL += "WHERE applicationid = '" + strAppID + "' ";
			strSQL += "AND codeid = '" + strCodeID + "' ";
			strSQL += "ORDER BY sequence asc ";
	
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","GetCoreSysetmCodeNumVal","GetCoreSysetmCodeNumVal","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			
			sessionDS = dbCon.ExecuteQuery(strSQL, iCurRow, iDSRowSize, "core_system_code_num_val");
			return  sessionDS.ds;
		}
		//HC Return Task

		public static ArrayList GetArrayOfRecordIDCons(String strAppID, String strEnterpriseID,
			int iCurRow, int iDSRowSize, ArrayList arrOfRecordid, String strNewConsignment, 
			string strOldConsignment, String userId)
		{
			SessionDS sessionDS = null;	
			String strSQL = "";
	
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","GetCoreSysetmCode","GetCoreSysetmCode","DbConnection object is null!!");
				return arrOfRecordid;
			}

			if(strOldConsignment.Trim() != "")
			{
				strSQL = "SELECT recordid ";
				strSQL += "FROM ImportedConsignments ";
				strSQL += "WHERE userid = '" + userId + "' ";
				strSQL += "AND consignment_no = '" + strOldConsignment + "'";
				sessionDS = dbCon.ExecuteQuery(strSQL, iCurRow, iDSRowSize, "Consignment");
			
				if(sessionDS.ds.Tables[0].Rows.Count > 0)
				{
					foreach(DataRow dr in sessionDS.ds.Tables[0].Rows)
					{
						if((dr["recordid"] != null) || 
							(!dr.GetType().Equals(System.Type.GetType("System.DBNull"))) || 
							(dr["recordid"].ToString() != ""))
						{
							for(int i = 0; i <= arrOfRecordid.Count - 1; i++)
							{
								if(arrOfRecordid[i].ToString() != dr["recordid"].ToString())
									arrOfRecordid.Add(dr["recordid"].ToString());
							}
						}
					}
				}
			}
			

			if(strNewConsignment.Trim() != "")
			{
				strSQL = "SELECT recordid ";
				strSQL += "FROM ImportedConsignments ";
				strSQL += "WHERE userid = '" + userId + "' ";
				strSQL += "AND consignment_no = '" + strNewConsignment + "'";
				sessionDS = dbCon.ExecuteQuery(strSQL, iCurRow, iDSRowSize, "Consignment");
			
				if(sessionDS.ds.Tables[0].Rows.Count > 0)
				{
					foreach(DataRow dr in sessionDS.ds.Tables[0].Rows)
					{
						if((dr["recordid"] != null) || 
							(!dr.GetType().Equals(System.Type.GetType("System.DBNull"))) || 
							(dr["recordid"].ToString() != ""))
						{
							for(int i = 0; i <= arrOfRecordid.Count - 1; i++)
							{
								if(arrOfRecordid[i].ToString() != dr["recordid"].ToString())
									arrOfRecordid.Add(dr["recordid"].ToString());
							}
						}
					}
				}
			}
			
			sessionDS.ds.Dispose();
			sessionDS.ds = null;
			sessionDS = null;

			return arrOfRecordid;
		}


		public static DataSet GetAuditedConsignment(String strAppID, String strEnterpriseID,
			String user_culture, String userId)
		{
			SessionDS sessionDS = null;			
			String strSQL = "";

			strSQL = "SELECT * ";
			strSQL += "FROM ImportedConsignments ";
			strSQL += "WHERE userid = '" + userId + "' ";
			strSQL += "AND recordid not in ";
			strSQL += "(select recordid ";
			strSQL += "from ImportedConsignmentsAuditFailures ";
			strSQL += "where userid = '" + userId + "' ";
			strSQL += "and recordtype = 'Consignment' ";
			strSQL += "group by recordid) ";
			strSQL += "and consignment_no in ( ";
			strSQL += "select consignment_no ";
			strSQL += "from ImportedConsignmentsPkgs ";
			strSQL += "where userid = '" + userId + "' ";
			strSQL += "and recordid not in ( ";
			strSQL += "select recordid ";
			strSQL += "from ImportedConsignmentsAuditFailures ";
			strSQL += "where userid = '" + userId + "' ";
			strSQL += "and recordtype = 'Package' ";
			strSQL += "group by recordid)) ";
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","GetCoreSysetmCode","GetCoreSysetmCode","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			sessionDS = dbCon.ExecuteQuery(strSQL, 0, 0, "AuditedCon");
			return  sessionDS.ds;
		}


		public static DataSet GetAuditedPackage(String strAppID, String strEnterpriseID,
			String user_culture, String userId)
		{
			SessionDS sessionDS = null;			
			String strSQL = "";

			strSQL = "SELECT * FROM ImportedConsignmentsPkgs ";
			strSQL += "WHERE userid = '" + userId + "' ";
			strSQL += "AND recordid not in ";
			strSQL += "(select recordid ";
			strSQL += "from ImportedConsignmentsAuditFailures ";
			strSQL += "where userid = '" + userId + "' ";
			strSQL += "and recordtype = 'Package' ";
			strSQL += "group by recordid) ";
			strSQL += "and consignment_no in ( ";
			strSQL += "select consignment_no ";
			strSQL += "from ImportedConsignments ";
			strSQL += "where userid = '" + userId + "' ";
			strSQL += "and recordid not in ( ";
			strSQL += "select recordid ";
			strSQL += "from ImportedConsignmentsAuditFailures ";
			strSQL += "where userid = '" + userId + "' ";
			strSQL += "and recordtype = 'Consignment' ";
			strSQL += "group by recordid)) ";
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","GetCoreSysetmCode","GetCoreSysetmCode","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			sessionDS = dbCon.ExecuteQuery(strSQL, 0, 0, "AuditedPKG");
			return  sessionDS.ds;
		}


		public static DataSet GetBookingDetailsByBookingNo(String strAppID, String strEnterpriseID,
			String booking_no)
		{
			SessionDS sessionDS = null;			
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT * ");	
			strBuilder.Append("FROM Pickup_Request ");
			strBuilder.Append("where applicationid = '");
			strBuilder.Append(strAppID+"'");
			strBuilder.Append(" and enterpriseid = '");
			strBuilder.Append(strEnterpriseID+"'");
			strBuilder.Append(" and booking_no = '");
			strBuilder.Append(booking_no+"'");

			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","GetCoreSysetmCode","GetCoreSysetmCode","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), 0, 0, "BookingData");
			return  sessionDS.ds;
		}


		public static long AddDomesticShipment(String strAppID, String strEnterpriseID, DataSet dsDomesticShipment,DataSet dsVAS, DataSet dsPkg,String userID,int iSerialNo,String RecipZipCode)
		{
			IDbCommand dbCmd = null;
			int iRowsAffected = 0;
			int cnt = 0;

			cnt = dsVAS.Tables[0].Rows.Count;
			int i = 0;
			long iBooking_no = 0;
			bool iBooking_State = false; //Phase2 - J02
			String strConsignmentNo = null;
			String strSenderZipCode = null;
			String strRecipientZipCode = null;
			String strServiceCode = null;
			DataTable dtCreditUsed = new DataTable();
			dtCreditUsed.Columns.Add("CustID");
			dtCreditUsed.Columns.Add("ConsignmentNo");
			dtCreditUsed.Columns.Add("CreditAmount");
			dtCreditUsed.AcceptChanges();

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("module1","DomesticShipmentManager","AddDomesticShipment","ERR001","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			if (dsDomesticShipment == null)
			{
				Logger.LogTraceError("module1","DomesticShipmentManager","AddDomesticShipment","ERR002","DataSet is null!!");
				throw new ApplicationException("The Shipment DataSet is null",null);
			}
			
			if(dsPkg == null)
			{
				Logger.LogTraceError("module1","DomesticShipmentManager","AddDomesticShipment","ERR003","DataSet is null!!");
				throw new ApplicationException("The Pakage DataSet is null",null);
			}

			if (dsDomesticShipment.Tables[0].Rows.Count > 0)
			{
				DataRow drEachTmp = dsDomesticShipment.Tables[0].Rows[0];

				if((drEachTmp["consignment_no"]!= null) && (!drEachTmp["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					if(DomesticShipmentMgrDAL.IsAssignedConsignment(strAppID, strEnterpriseID, (String)drEachTmp["consignment_no"]))
					{
						Logger.LogTraceError("module1","DomesticShipmentManager","AddDomesticShipment","ERR003","Duplicate Consignment");
						throw new ApplicationException("Duplicate Consignment",null);
					}
				}	
			}

			IDbConnection conApp = dbCon.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment","ERR004","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment","ERR005","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment","ERR006","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment","ERR007","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}



			try
			{
				dbCmd = dbCon.CreateCommand("",CommandType.Text);
				dbCmd.Connection = conApp;
				dbCmd.Transaction = transactionApp;

				String strPayerID = null;
				DataRow drEach = dsDomesticShipment.Tables[0].Rows[0];
				StringBuilder strBuilder = new StringBuilder();
				strBuilder.Append ("insert into shipment(applicationid,enterpriseid,booking_no,");
				strBuilder.Append("consignment_no,ref_no,booking_datetime,payerid,payer_type,");
				strBuilder.Append("new_account,payer_name,payer_address1,payer_address2,payer_zipcode,");
				strBuilder.Append("payer_country,payer_telephone,payer_fax,");
				strBuilder.Append("payment_mode,sender_name,sender_address1,sender_address2,sender_zipcode,");
				strBuilder.Append("sender_country,sender_telephone,sender_fax,");
				strBuilder.Append("sender_contact_person,recipient_name,recipient_address1,recipient_address2,");
				strBuilder.Append("recipient_zipcode,recipient_country,tot_wt,tot_act_wt,recipient_telephone,tot_dim_wt,");//TU 08/07/08
				strBuilder.Append("recipient_fax,tot_pkg,service_code,recipient_contact_person,declare_value,");
				strBuilder.Append("chargeable_wt,insurance_surcharge,max_insurance_cover,");
				strBuilder.Append("act_pickup_datetime,");
				strBuilder.Append("commodity_code,");
				strBuilder.Append("est_delivery_datetime,percent_dv_additional,");
				strBuilder.Append("act_delivery_date,");
				strBuilder.Append("payment_type,");
				strBuilder.Append("return_pod_slip,");
				strBuilder.Append("shipment_type,");
				strBuilder.Append("origin_state_code,");
				strBuilder.Append("invoice_no,destination_state_code,invoice_amt,tot_freight_charge,");
				strBuilder.Append("tot_vas_surcharge,debit_amt,credit_amt,cash_collected,last_status_code,");
				strBuilder.Append("last_exception_code,last_status_datetime,shpt_manifest_datetime,");
				strBuilder.Append("delivery_manifested,");
				strBuilder.Append("route_code,quotation_no,quotation_version,esa_surcharge,other_surch_amount,");
				//		strBuilder.Append("agent_pup_cost_consignment,agent_pup_cost_wt,agent_del_cost_consignment,agent_del_cost_wt,agent");
				strBuilder.Append("agent_pup_cost_consignment,agent_pup_cost_wt,agent_del_cost_consignment,agent_del_cost_wt,remark, mbg, cod_amount,");
				//HC Return Task
				strBuilder.Append("origin_station, destination_station, return_invoice_hc, est_hc_return_datetime, act_hc_return_datetime");
				strBuilder.Append(", total_rated_amount");//added by ching 21/04/2010
				//HC Return Task
				strBuilder.Append(")values('"+strAppID+"',"+"'"+strEnterpriseID+"',");
				if((drEach["booking_no"]!= null) && (!drEach["booking_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					iBooking_no = Convert.ToInt64(drEach["booking_no"]);
					if(iBooking_no == 0)
						//generate the booking no for adhoc 						
						iBooking_no = (long)Counter.GetNext(strAppID,strEnterpriseID,"booking_number", dbCon, dbCmd);
					iBooking_State = true;//Phase2 - J02
				}
				else
				{
					//generate the booking no for adhoc 
					iBooking_no = (long)Counter.GetNext(strAppID,strEnterpriseID,"booking_number", dbCon, dbCmd);
				}
				strBuilder.Append(iBooking_no+",");
				
				if((drEach["consignment_no"]!= null) && (!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strConsignmentNo = Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString());
					strBuilder.Append("'");
					strBuilder.Append(strConsignmentNo);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");


				if((drEach["ref_no"]!= null) && (!drEach["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strRefNo = (String) drEach["ref_no"];
					strBuilder.Append("'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strRefNo));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");


				if((drEach["booking_datetime"]!= null) && (!drEach["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtBookingDtTime = (DateTime) drEach["booking_datetime"];
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtBookingDtTime,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");


				if((drEach["payerid"]!= null) && (!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strPayerID = (String) drEach["payerid"];
					strBuilder.Append("'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strPayerID));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["payer_type"]!= null) && (!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPayerType = (String) drEach["payer_type"];
					strBuilder.Append("'");
					strBuilder.Append(strPayerType);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["new_account"]!= null) && (!drEach["new_account"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strNewAccount = (String) drEach["new_account"];
					strBuilder.Append("'");
					strBuilder.Append(strNewAccount);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["payer_name"]!= null) && (!drEach["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPayerName = (String) drEach["payer_name"];
					strBuilder.Append("N'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strPayerName));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["payer_address1"]!= null) && (!drEach["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPayerAddress1 = (String) drEach["payer_address1"];
					strBuilder.Append("N'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strPayerAddress1));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["payer_address2"]!= null) && (!drEach["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPayerAddress2 = (String) drEach["payer_address2"];
					strBuilder.Append("N'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strPayerAddress2));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["payer_zipcode"]!= null) && (!drEach["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPayerZipCode = (String) drEach["payer_zipcode"];
					strBuilder.Append("'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strPayerZipCode));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				

				if((drEach["payer_country"]!= null) && (!drEach["payer_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPayerCountry = (String) drEach["payer_country"];
					strBuilder.Append("'");
					strBuilder.Append(strPayerCountry);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["payer_telephone"]!= null) && (!drEach["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPayerTelephone = (String) drEach["payer_telephone"];
					strBuilder.Append("'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strPayerTelephone));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["payer_fax"]!= null) && (!drEach["payer_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPayerFax = (String) drEach["payer_fax"];
					strBuilder.Append("'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strPayerFax));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["payment_mode"]!= null) && (!drEach["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPaymentMode = (String) drEach["payment_mode"];
					strBuilder.Append("'");
					strBuilder.Append(strPaymentMode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["sender_name"]!= null) && (!drEach["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strSenderName = (String) drEach["sender_name"];
					strBuilder.Append("N'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strSenderName));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["sender_address1"]!= null) && (!drEach["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strSenderAddress1 = (String) drEach["sender_address1"];
					strBuilder.Append("N'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strSenderAddress1));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["sender_address2"]!= null) && (!drEach["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strSenderAddress2 = (String) drEach["sender_address2"];
					strBuilder.Append("N'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strSenderAddress2));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["sender_zipcode"]!= null) && (!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strSenderZipCode = (String) drEach["sender_zipcode"];
					strBuilder.Append("'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strSenderZipCode));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["sender_country"]!= null) && (!drEach["sender_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strSenderCountry = (String) drEach["sender_country"];
					strBuilder.Append("'");
					strBuilder.Append(strSenderCountry);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["sender_telephone"]!= null) && (!drEach["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strSenderTelephone = (String) drEach["sender_telephone"];
					strBuilder.Append("'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strSenderTelephone));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["sender_fax"]!= null) && (!drEach["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strSenderFax = (String) drEach["sender_fax"];
					strBuilder.Append("'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strSenderFax));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["sender_contact_person"]!= null) && (!drEach["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strSenderContactPerson = (String) drEach["sender_contact_person"];
					strBuilder.Append("N'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strSenderContactPerson));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["recipient_name"]!= null) && (!drEach["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strRecipientName = (String) drEach["recipient_name"];
					strBuilder.Append("N'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientName));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["recipient_address1"]!= null) && (!drEach["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strRecipientAddress1 = (String) drEach["recipient_address1"];
					strBuilder.Append("N'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientAddress1));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["recipient_address2"]!= null) && (!drEach["recipient_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strRecipientAddress2 = (String) drEach["recipient_address2"];
					strBuilder.Append("N'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientAddress2));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");


				if((drEach["recipient_zipcode"]!= null) && (!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strRecipientZipCode = (String) drEach["recipient_zipcode"];
					strBuilder.Append("'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientZipCode));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["recipient_country"]!= null) && (!drEach["recipient_country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strRecipientCountry = (String) drEach["recipient_country"];
					strBuilder.Append("'");
					strBuilder.Append(strRecipientCountry);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["tot_wt"]!= null) && (!drEach["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decTotWt = (decimal) drEach["tot_wt"];
					strBuilder.Append(decTotWt);
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//TU 08/07/08
				if((drEach["tot_act_wt"]!= null) && (!drEach["tot_act_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decTotactWt = (decimal) drEach["tot_act_wt"];
					strBuilder.Append(decTotactWt);
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				//
				if((drEach["recipient_telephone"]!= null) && (!drEach["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strRecipientTelephone = (String) drEach["recipient_telephone"];
					strBuilder.Append("'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientTelephone));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");


				if((drEach["tot_dim_wt"]!= null) && (!drEach["tot_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decTotDimWt = (decimal) drEach["tot_dim_wt"];
					strBuilder.Append(decTotDimWt);
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["recipient_fax"]!= null) && (!drEach["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strRecipientFax = (String) drEach["recipient_fax"];
					strBuilder.Append("'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientFax));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				
				if((drEach["tot_pkg"]!= null) && (!drEach["tot_pkg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					int iTotPkg = Convert.ToInt32(drEach["tot_pkg"]);
					strBuilder.Append(iTotPkg);
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");



				if((drEach["service_code"]!= null) && (!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strServiceCode = (String) drEach["service_code"];
					strBuilder.Append("'");
					strBuilder.Append(strServiceCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");


				if((drEach["recipient_contact_person"]!= null) && (!drEach["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strRecipientContactPerson = (String) drEach["recipient_contact_person"];
					strBuilder.Append("N'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strRecipientContactPerson));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["declare_value"]!= null) && (!drEach["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decDeclareValue = (decimal) drEach["declare_value"];
					strBuilder.Append(decDeclareValue);
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["chargeable_wt"]!= null) && (!drEach["chargeable_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decChargeableWt = (decimal) drEach["chargeable_wt"];
					strBuilder.Append(decChargeableWt);
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				decimal decInsuranceSurcharge=0;//moved ching 21/04/2010
				if((drEach["insurance_surcharge"]!= null) && (!drEach["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decInsuranceSurcharge = (decimal) drEach["insurance_surcharge"];
					strBuilder.Append(decInsuranceSurcharge);
				}
				else
				{
					decInsuranceSurcharge=0;
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["max_insurance_cover"]!= null) && (!drEach["max_insurance_cover"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decMaxInsuranceCover = (decimal) drEach["max_insurance_cover"];
					strBuilder.Append(decMaxInsuranceCover);
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["act_pickup_datetime"]!= null) && (!drEach["act_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtActPickup = Convert.ToDateTime(drEach["act_pickup_datetime"]);
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtActPickup,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["commodity_code"]!= null) && (!drEach["commodity_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strCommodityCode = (String) drEach["commodity_code"];
					strBuilder.Append("'");
					strBuilder.Append(strCommodityCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");


				if((drEach["est_delivery_datetime"]!= null) && (!drEach["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtEstDeliveryDatetime = (DateTime) drEach["est_delivery_datetime"];
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtEstDeliveryDatetime,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["percent_dv_additional"]!= null) && (!drEach["percent_dv_additional"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decPercentDvAdditional = (decimal) drEach["percent_dv_additional"];
					strBuilder.Append(decPercentDvAdditional);
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["act_delivery_date"]!= null) && (!drEach["act_delivery_date"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtActDeliveryDate = (DateTime) drEach["act_delivery_date"];
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtActDeliveryDate,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["payment_type"]!= null) && (!drEach["payment_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strPaymentType = (String) drEach["payment_type"];
					strBuilder.Append("'");
					strBuilder.Append(strPaymentType);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				

				if((drEach["return_pod_slip"]!= null) && (!drEach["return_pod_slip"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strReturnPODSlip = (String) drEach["return_pod_slip"];
					strBuilder.Append("'");
					strBuilder.Append(strReturnPODSlip);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["shipment_type"]!= null) && (!drEach["shipment_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strShipmentType = (String) drEach["shipment_type"];
					strBuilder.Append("'");
					strBuilder.Append(strShipmentType);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");


				if((drEach["origin_state_code"]!= null) && (!drEach["origin_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strOriginStateCode = (String) drEach["origin_state_code"];
					strBuilder.Append("'");
					strBuilder.Append(strOriginStateCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["invoice_no"]!= null) && (!drEach["invoice_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strInvoiceNo = (String) drEach["invoice_no"];
					strBuilder.Append("'");
					strBuilder.Append(strInvoiceNo);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["destination_state_code"]!= null) && (!drEach["destination_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strDestinationStateCode = (String) drEach["destination_state_code"];
					strBuilder.Append("'");
					strBuilder.Append(strDestinationStateCode);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["invoice_amt"]!= null) && (!drEach["invoice_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decInvoiceAmt = (decimal) drEach["invoice_amt"];
					
					strBuilder.Append(decInvoiceAmt);
					
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				decimal decTotFreight=0;// moved ching 21/04/2010
				if((drEach["tot_freight_charge"]!= null) && (!drEach["tot_freight_charge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decTotFreight = (decimal) drEach["tot_freight_charge"];
					
					strBuilder.Append(decTotFreight);
					
				}
				else
				{
					decTotFreight=0;
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				decimal decTotVASSurcharge=0;// moved ching 21/04/2010
				if((drEach["tot_vas_surcharge"]!= null) && (!drEach["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decTotVASSurcharge= (decimal) drEach["tot_vas_surcharge"];
					
					strBuilder.Append(decTotVASSurcharge);
					
				}
				else
				{
					decTotVASSurcharge=0;
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["debit_amt"]!= null) && (!drEach["debit_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decDebitAmt = (decimal) drEach["debit_amt"];
					
					strBuilder.Append(decDebitAmt);
					
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
			
				if((drEach["credit_amt"]!= null) && (!drEach["credit_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decCreditAmt = (decimal) drEach["credit_amt"];
					
					strBuilder.Append(decCreditAmt);
					
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["cash_collected"]!= null) && (!drEach["cash_collected"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal decCashCollected = (decimal) drEach["cash_collected"];
					strBuilder.Append(decCashCollected);	
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				
				if((drEach["last_status_code"]!= null) && (!drEach["last_status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strLastStatusCode = (String) drEach["last_status_code"];
					strBuilder.Append("'");
					strBuilder.Append(strLastStatusCode);	
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["last_exception_code"]!= null) && (!drEach["last_exception_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strLastExceptionCode = (String) drEach["last_exception_code"];
					strBuilder.Append("'");
					strBuilder.Append(strLastExceptionCode);	
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				
				if((drEach["last_status_datetime"]!= null) && (!drEach["last_status_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtLastStatusDateTime = (DateTime) drEach["last_status_datetime"];
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtLastStatusDateTime,DTFormat.DateTime));


				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["shpt_manifest_datetime"]!= null) && (!drEach["shpt_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtShptManifestDateTime = (DateTime) drEach["shpt_manifest_datetime"];
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtShptManifestDateTime,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["delivery_manifested"]!= null) && (!drEach["delivery_manifested"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strDeliveryManifested = (String) drEach["delivery_manifested"];
					strBuilder.Append("'");
					strBuilder.Append(strDeliveryManifested);	
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["route_code"]!= null) && (!drEach["route_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strRouteCode = (String) drEach["route_code"];
					strBuilder.Append("'");
					strBuilder.Append(strRouteCode);	
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["quotation_no"]!= null) && (!drEach["quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strQuotationNo = (String) drEach["quotation_no"];
					strBuilder.Append("'");
					strBuilder.Append(strQuotationNo);	
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");			

				if((drEach["quotation_version"]!= null) && (!drEach["quotation_version"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					int iQuotationVersion = Convert.ToInt32(drEach["quotation_version"]);
					strBuilder.Append(iQuotationVersion);
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");
				decimal decESASurcharge =0;//moved ching 21/04/2010
				if((drEach["esa_surcharge"]!= null) && (!drEach["esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decESASurcharge = (decimal) drEach["esa_surcharge"];
					strBuilder.Append(decESASurcharge);
				}
				else
				{
					decESASurcharge=0;
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				decimal decOTHSurcharge =0;//moved ching 21/04/2010
				if(drEach["other_surch_amount"]!= null && drEach["other_surch_amount"]!=DBNull.Value)
				{
					decOTHSurcharge = (decimal)drEach["other_surch_amount"];
					strBuilder.Append(decOTHSurcharge);
				}
				else
				{
					decOTHSurcharge=0;
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//Calculate the Agent Cost.
				if((drEach["payer_type"]!= null) && (!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))) && drEach["payer_type"].ToString().Equals("A"))
				{
					
					decimal decTotWt = 0;
					AgentConsgCost agentConsgSnd;
					agentConsgSnd.decCost = 0;
					agentConsgSnd.strAgentID = null;

					AgentWtCost agentWtSnd;
					agentWtSnd.decCost = 0;
					agentWtSnd.strAgentID = null;

					AgentConsgCost agentConsgRcp;
					agentConsgRcp.decCost = 0;
					agentConsgRcp.strAgentID = null;

					AgentWtCost agentWtRcp;
					agentWtRcp.decCost = 0;
					agentWtRcp.strAgentID = null;

					if((drEach["tot_wt"]!= null) && (!drEach["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						decTotWt = (decimal) drEach["tot_wt"];
					}
				
					if((drEach["sender_zipcode"]!= null) && (!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strSenderZipCode = (String) drEach["sender_zipcode"];
						//Check whether the Sender Zipcode exists in Agent_Zipcode table.
						//				agentConsgSnd =  TIESUtility.GetAgentConsignmentCost(strAppID,strEnterpriseID,strSenderZipCode);
						
						//Agent pickup Cost Weight
						//				agentWtSnd = TIESUtility.GetAgentWtCost(strAppID,strEnterpriseID,strSenderZipCode,decTotWt);
					}

					if((drEach["recipient_zipcode"]!= null) && (!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
					{
						strRecipientZipCode = (String) drEach["recipient_zipcode"];
						//decimal decDlvryCost = 0;
						//Check whether the Recipient Zipcode exists in Agent_Zipcode table.
						//				agentConsgRcp =  TIESUtility.GetAgentConsignmentCost(strAppID,strEnterpriseID,strRecipientZipCode);
						
						//						if(decDlvryCost > 0)
						//							strBuilder.Append(decDlvryCost);
						//						else
						//							strBuilder.Append("null");
						//						strBuilder.Append(",");
						
						//Agent Delivery Cost Weight
						//				//decimal decDlvryWt = 0;
						//				agentWtRcp = TIESUtility.GetAgentWtCost(strAppID,strEnterpriseID,strRecipientZipCode,decTotWt);

						//						if(decDlvryWt > 0)
						//							strBuilder.Append(decDlvryWt);
						//						else
						//							strBuilder.Append("null");
					}

					if((agentConsgSnd.strAgentID != null) && (agentConsgRcp.strAgentID != null) && (agentConsgSnd.strAgentID == agentConsgRcp.strAgentID))
					{
						//No charges
						strBuilder.Append("null");
						strBuilder.Append(",");
						strBuilder.Append("null");
						strBuilder.Append(",");
						strBuilder.Append("null");
						strBuilder.Append(",");
						strBuilder.Append("null");	
						strBuilder.Append(",");
					}
					else
					{

						if(agentConsgSnd.decCost > 0)
							strBuilder.Append(agentConsgSnd.decCost);
						else
							strBuilder.Append("null");
						strBuilder.Append(",");

						if(agentWtSnd.decCost > 0)
							strBuilder.Append(agentWtSnd.decCost);
						else
							strBuilder.Append("null");
						strBuilder.Append(",");



						if(agentConsgRcp.decCost > 0)
							strBuilder.Append(agentConsgRcp.decCost);
						else
							strBuilder.Append("null");
						strBuilder.Append(",");

						if(agentWtRcp.decCost > 0)
							strBuilder.Append(agentWtRcp.decCost);
						else
							strBuilder.Append("null");
						strBuilder.Append(",");

					}

				}
				else if((drEach["payer_type"]!= null) && (!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull"))) 
					&& (!drEach["payer_type"].ToString().Equals("A")))
				{
					strBuilder.Append("null");
					strBuilder.Append(",");
					strBuilder.Append("null");
					strBuilder.Append(",");
					strBuilder.Append("null");
					strBuilder.Append(",");
					strBuilder.Append("null");
					strBuilder.Append(",");
				}
				//
				if((drEach["remark"]!= null) && (!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					strBuilder.Append("N'");
					String strRemark = (String) drEach["remark"];			
					strBuilder.Append(Utility.ReplaceSingleQuote(strRemark));
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["mbg"]!= null) && (!drEach["mbg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strmbg = (String) drEach["mbg"];
					strBuilder.Append("'");
					strBuilder.Append(strmbg);	
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["cod_amount"]!= null) && (!drEach["cod_amount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					decimal deccod_amount = (decimal) drEach["cod_amount"];
					
					strBuilder.Append(deccod_amount);
					
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["origin_station"]!= null) && (!drEach["origin_station"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strorigin_station = (String) drEach["origin_station"];
					strBuilder.Append("'");
					strBuilder.Append(strorigin_station);	
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["destination_station"]!= null) && (!drEach["destination_station"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strdest_station = (String) drEach["destination_station"];
					strBuilder.Append("'");
					strBuilder.Append(strdest_station);	
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				//HC Return Task
				if((drEach["return_invoice_hc"]!= null) && (!drEach["return_invoice_hc"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strReturnPODHC = (String) drEach["return_invoice_hc"];
					strBuilder.Append("'");
					strBuilder.Append(strReturnPODHC);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["est_hc_return_datetime"]!= null) && (!drEach["est_hc_return_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtEstHCReturnDatetime = (DateTime) drEach["est_hc_return_datetime"];
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtEstHCReturnDatetime,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if((drEach["act_hc_return_datetime"]!= null) && (!drEach["act_hc_return_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					DateTime dtActHCReturnDatetime = (DateTime) drEach["act_hc_return_datetime"];
					strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtActHCReturnDatetime,DTFormat.DateTime));
				}
				else
				{
					strBuilder.Append("null");
				}			
				//HC Return Task
				// calcurate total_rate_amount by ching 21/04/2010
				strBuilder.Append(",");
				decimal decTOTRatedAmnt = 0;
				decTOTRatedAmnt = decESASurcharge+decInsuranceSurcharge+decOTHSurcharge+decTotFreight+decTotVASSurcharge;
				strBuilder.Append(decTOTRatedAmnt);
				//
				DataRow drCreditUsed = dtCreditUsed.NewRow();
				drCreditUsed["CustID"]=strPayerID;
				drCreditUsed["ConsignmentNo"]=strConsignmentNo;
				drCreditUsed["CreditAmount"]=decTOTRatedAmnt;
				dtCreditUsed.Rows.Add(drCreditUsed);
				dtCreditUsed.AcceptChanges();

				strBuilder.Append(")");
				String strQuery = strBuilder.ToString();
				//dbCmd = dbCon.CreateCommand(strQuery,CommandType.Text);
				//dbCmd.Connection = conApp;
				//dbCmd.Transaction = transactionApp;
				dbCmd.CommandText = strQuery;

				Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF001","QUERY IS : "+strQuery);
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF002",iRowsAffected + " rows inserted into Shipment table ....");
			
				

				//Inserting the data in the Shipment_VAS table
				cnt = dsVAS.Tables[0].Rows.Count;
				i = 0;

				for(i=0;i<cnt;i++)
				{
					DataRow drEachVAS = dsVAS.Tables[0].Rows[i];
			
					String strVASCode = "";
					if(drEachVAS["vas_code"].ToString() != "")
					{
						strVASCode = (String)drEachVAS["vas_code"];
					}
					decimal decSurcharge = (decimal)drEachVAS["surcharge"];
			
					String strRemarks = "";
					if(drEachVAS["remarks"].ToString() != "")
					{
						strRemarks = (String)drEachVAS["remarks"];
					}

					strBuilder = new StringBuilder();			
					strBuilder.Append("insert into shipment_vas (applicationid,enterpriseid,booking_no,consignment_no,vas_code,vas_surcharge,remark)values('");
					strBuilder.Append(strAppID+"','");
					strBuilder.Append(strEnterpriseID+"',");
					strBuilder.Append(iBooking_no);
					strBuilder.Append(",'");
					strBuilder.Append(strConsignmentNo);
					strBuilder.Append("','");
					strBuilder.Append(strVASCode+"',");
					strBuilder.Append(decSurcharge+",N'");
					strBuilder.Append(Utility.ReplaceSingleQuote(strRemarks)+"')");

					dbCmd.CommandText = strBuilder.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF003",iRowsAffected + " rows inserted into shipment_vas table");
					
				}
				//Inserting the data in the Shipment_PKG table
				Customer customer = new Customer();
				customer.Populate(strAppID,strEnterpriseID, strPayerID);
				Zipcode zipcode = new Zipcode();

				zipcode.Populate(strAppID,strEnterpriseID,strSenderZipCode);
				String strOrgZone = zipcode.ZoneCode;
				zipcode.Populate(strAppID,strEnterpriseID,strRecipientZipCode);
				String strDestnZone = zipcode.ZoneCode;

				cnt = dsPkg.Tables[0].Rows.Count;
				i = 0;
				int sumQty = 0;
				float weightToFreight = 0;
				decimal freightCharge = 0;

				for(i=0;i<cnt;i++)
				{
					DataRow drEachPKG = dsPkg.Tables[0].Rows[i];
					String strMpsNo = "";
					if(drEachPKG["mps_no"].ToString() != "")
					{
						strMpsNo = (String)drEachPKG["mps_no"];
					}

					decimal decPkgLgth = Convert.ToDecimal(drEachPKG["pkg_length"]);
					decimal decPkgBrdth = Convert.ToDecimal(drEachPKG["pkg_breadth"]);
					decimal decPkgHgth = Convert.ToDecimal(drEachPKG["pkg_height"]);
					decimal decPkgWt = Convert.ToDecimal(drEachPKG["pkg_wt"]);
					int iPkgQty = Convert.ToInt32(drEachPKG["pkg_qty"]);
					decimal decTotalWt = Convert.ToDecimal(drEachPKG["tot_wt"]);
					decimal decTotalactWt = Convert.ToDecimal(drEachPKG["tot_act_wt"]);//TU 08/07/08
					decimal decTotalDimWt = Convert.ToDecimal(drEachPKG["tot_dim_wt"]);
					decimal decChrgbleWt = Convert.ToDecimal(drEachPKG["chargeable_wt"]);


					if(float.Parse(drEachPKG["tot_wt"].ToString())>float.Parse(drEachPKG["tot_dim_wt"].ToString()))
					{
						if(customer.IsCustomer_Box== "Y")
							weightToFreight = float.Parse(drEachPKG["tot_wt"].ToString())/int.Parse(drEachPKG["pkg_qty"].ToString());
						else
							weightToFreight = float.Parse(drEachPKG["tot_wt"].ToString());
					}
					else
					{
						if(customer.IsCustomer_Box== "Y")
							weightToFreight = float.Parse(drEachPKG["tot_dim_wt"].ToString())/int.Parse(drEachPKG["pkg_qty"].ToString());
						else
							weightToFreight = float.Parse(drEachPKG["tot_dim_wt"].ToString());
					}
					sumQty = sumQty+int.Parse(drEachPKG["pkg_qty"].ToString());

					freightCharge = (decimal)TIESUtility.ComputeFreightCharge(strAppID, strEnterpriseID,
						strPayerID, "C", 
						strOrgZone, strDestnZone, weightToFreight,
						strServiceCode, strSenderZipCode, strRecipientZipCode);

					if(customer.IsCustomer_Box== "Y")
						freightCharge = (freightCharge * int.Parse(drEachPKG["pkg_qty"].ToString()));

					strBuilder = new StringBuilder();		
					//TU 08/07/08
					strBuilder.Append("insert into shipment_pkg (applicationid,enterpriseid,booking_no,consignment_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_wt,tot_act_wt,tot_dim_wt,chargeable_wt)values('");
					strBuilder.Append(strAppID+"','");
					strBuilder.Append(strEnterpriseID+"',");
					strBuilder.Append(iBooking_no);
					strBuilder.Append(",'");
					strBuilder.Append(strConsignmentNo);
					strBuilder.Append("','");
					strBuilder.Append(strMpsNo+"',");
					strBuilder.Append(decPkgLgth+",");
					strBuilder.Append(decPkgBrdth+",");
					strBuilder.Append(decPkgHgth+",");
					strBuilder.Append(decPkgWt+",");
					strBuilder.Append(iPkgQty+",");
					strBuilder.Append(decTotalWt+",");
					//TU 08/07/08
					strBuilder.Append(decTotalactWt+",");
					strBuilder.Append(decTotalDimWt+",");
					strBuilder.Append(decChrgbleWt+")");
			
					dbCmd.CommandText = strBuilder.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF004",iRowsAffected + " rows inserted into shipment_pkg table");
						
					//Chai	18/04/2012
					/*
					strBuilder = new StringBuilder();
					strBuilder.Append("insert into shipment_pkg_box (applicationid,enterpriseid,booking_no,consignment_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_wt,tot_act_wt,tot_dim_wt,chargeable_wt,Freight_Charge)values('");
					strBuilder.Append(strAppID+"','");
					strBuilder.Append(strEnterpriseID+"',");
					strBuilder.Append(iBooking_no); //New booking no
					strBuilder.Append(",'");
					strBuilder.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
					strBuilder.Append("','");
					strBuilder.Append(strMpsNo+"',");
					strBuilder.Append(decPkgLgth+",");
					strBuilder.Append(decPkgBrdth+",");
					strBuilder.Append(decPkgHgth+",");
					strBuilder.Append(decPkgWt+",");
					strBuilder.Append(iPkgQty+",");
					strBuilder.Append(decTotalWt+",");
					strBuilder.Append(decTotalactWt+",");//TU on 17June08
					strBuilder.Append(decTotalDimWt+",");
					strBuilder.Append(decChrgbleWt+",");
					strBuilder.Append(freightCharge+")");
				
					dbCmd.CommandText = strBuilder.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF004",iRowsAffected + " rows inserted into shipment_pkg table");
					*/
				}


			

				if(sumQty < int.Parse(customer.Minimum_Box))
				{
					int packageAdd = int.Parse(customer.Minimum_Box)-sumQty;
					int mpsNo = sumQty+1;
					freightCharge = (decimal)TIESUtility.ComputeFreightCharge(strAppID, strEnterpriseID,
						strPayerID, "C", 
						strOrgZone, strDestnZone, 0,
						strServiceCode, strSenderZipCode, strRecipientZipCode);
					for(int k=0;k<packageAdd;k++)
					{
						//Chai 18/04/2012
						/*
						strBuilder = new StringBuilder();
						strBuilder.Append("insert into shipment_pkg_box (applicationid,enterpriseid,booking_no,consignment_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_wt,tot_act_wt,tot_dim_wt,chargeable_wt,Freight_Charge)values('");
						strBuilder.Append(strAppID+"','");
						strBuilder.Append(strEnterpriseID+"',");
						strBuilder.Append(iBooking_no); //New booking no
						strBuilder.Append(",'");
						strBuilder.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
						strBuilder.Append("','");
						strBuilder.Append(mpsNo.ToString()+"',");
						strBuilder.Append(0+",");
						strBuilder.Append(0+",");
						strBuilder.Append(0+",");
						strBuilder.Append(0+",");
						strBuilder.Append(0+",");
						strBuilder.Append(0+",");
						strBuilder.Append(0+",");
						strBuilder.Append(0+",");
						strBuilder.Append(0+",");
						strBuilder.Append(freightCharge+")");

						dbCmd.CommandText = strBuilder.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						*/
						mpsNo = mpsNo+1;
						Logger.LogDebugInfo("DomesticShipmentMgrDAL","AddDomesticShipment","INF003",iRowsAffected + " rows inserted into shipment_pkg_box table");
					}
				}

				//Update in the pickup_shipment table
				strBuilder = new StringBuilder();	
				strBuilder.Append("update pickup_shipment set consignment_no = '");
				strBuilder.Append(strConsignmentNo);
				strBuilder.Append("'");
				strBuilder.Append( " where applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and booking_no = ");
				strBuilder.Append(iBooking_no);
				strBuilder.Append(" and serial_no = ");
				strBuilder.Append(iSerialNo);
				strBuilder.Append(" and recipient_zipcode = '");
				strBuilder.Append(Utility.ReplaceSingleQuote(RecipZipCode));
				strBuilder.Append("'");
				dbCmd.CommandText = strBuilder.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF005",iRowsAffected + " rows updated in pickup_shipment table");

				//Phase2 - J02
				if(iBooking_State)	
				{
					DataSet dsDispatch = DispatchTrackingMgrDAL.QueryDispatchForShipmentTracking(strAppID,strEnterpriseID,
						iBooking_no, 0, 0).ds;

					if(dsDispatch.Tables[0].Rows.Count > 0)
					{
						foreach(DataRow tmpDr in dsDispatch.Tables[0].Rows)
						{
							//Inserting data into shipment_tracking form Dispatch
							strBuilder = new StringBuilder();
							strBuilder.Append ("insert into shipment_tracking (applicationid,enterpriseid,consignment_no,tracking_datetime,booking_no,");
							strBuilder.Append("status_code,exception_code,person_incharge, remark, deleted, delete_remark, last_userid, last_updated, invoiceable, location) ");
							strBuilder.Append(" values('"+strAppID+"',"+"'"+strEnterpriseID+"',");
							if((drEach["consignment_no"]!= null) && (!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								String strConsgNo = (String) drEach["consignment_no"];
								strBuilder.Append("'");
								strBuilder.Append(strConsgNo);
								strBuilder.Append("'");
							}
							else
							{
								strBuilder.Append("null");
							}
							strBuilder.Append(",");


							if((tmpDr["tracking_datetime"]!= null) && (!tmpDr["tracking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								DateTime dttrackingDatetime= (DateTime)tmpDr["tracking_datetime"];
								strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dttrackingDatetime,DTFormat.DateTime));
							}
							else
							{
								strBuilder.Append("null");
							}
							strBuilder.Append(",");
							strBuilder.Append(iBooking_no);
							strBuilder.Append(",");

							if((tmpDr["status_code"]!= null) && (!tmpDr["status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								String strStatusCode = (String) tmpDr["status_code"];
								strBuilder.Append("'");
								strBuilder.Append(strStatusCode);	
								strBuilder.Append("'");
							}
							else
							{
								strBuilder.Append("null");
							}
							strBuilder.Append(",");

							if((tmpDr["exception_code"]!= null) && (!tmpDr["exception_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								String strExceptionCode = (String) tmpDr["exception_code"];
								strBuilder.Append("'");
								strBuilder.Append(strExceptionCode);	
								strBuilder.Append("'");
							}
							else
							{
								strBuilder.Append("null");
							}
							strBuilder.Append(",");
				
							if((tmpDr["person_incharge"]!= null) && (!tmpDr["person_incharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								String strPersonIncharge = (String) tmpDr["person_incharge"];
								strBuilder.Append("'");
								strBuilder.Append(strPersonIncharge);	
								strBuilder.Append("'");
							}
							else
							{
								strBuilder.Append("null");
							}
							strBuilder.Append(",");

							if((tmpDr["remark"]!= null) && (!tmpDr["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								String strPersonIncharge = (String) tmpDr["remark"];
								strBuilder.Append("'");
								strBuilder.Append(strPersonIncharge);	
								strBuilder.Append("'");
							}
							else
							{
								strBuilder.Append("null");
							}
							strBuilder.Append(",");

							if((tmpDr["deleted"]!= null) && (!tmpDr["deleted"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								String strDeleted = (String) tmpDr["deleted"];
								strBuilder.Append("'");
								strBuilder.Append(strDeleted);	
								strBuilder.Append("'");
							}
							else
							{
								strBuilder.Append("'N'");
							}
							strBuilder.Append(",");

							if((tmpDr["delete_remark"]!= null) && (!tmpDr["delete_remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								String strDeletedRem = (String) tmpDr["delete_remark"];
								strBuilder.Append("'");
								strBuilder.Append(strDeletedRem);	
								strBuilder.Append("'");
							}
							else
							{
								strBuilder.Append("null");
							}
							strBuilder.Append(",");

							if((tmpDr["last_userid"]!= null) && (!tmpDr["last_userid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								String strLastUserID = (String) tmpDr["last_userid"];
								strBuilder.Append("'");
								strBuilder.Append(strLastUserID);	
								strBuilder.Append("'");
							}
							else
							{
								strBuilder.Append("null");
							}
							strBuilder.Append(",");

							if((tmpDr["last_updated"]!= null) && (!tmpDr["last_updated"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								DateTime dtLastUpdatedDateTime = (DateTime) tmpDr["last_updated"];
								strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,dtLastUpdatedDateTime,DTFormat.DateTime));
							}
							else
							{
								strBuilder.Append("null");
							}
							
							strBuilder.Append(",'N',");

							if((tmpDr["location"]!= null) && (!tmpDr["location"].GetType().Equals(System.Type.GetType("System.DBNull"))))
							{
								String strlocation = (String) tmpDr["location"];
								strBuilder.Append("'");
								strBuilder.Append(strlocation);
								strBuilder.Append("'");
							}
							else
							{
								strBuilder.Append("null");
							}
							strBuilder.Append(")");

							dbCmd.CommandText = strBuilder.ToString();
							iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
							Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF008",iRowsAffected + " rows inserted into shipment_tracking table");
						}
					}
				}		
				//Phase2 - J02
				int booking_no;
				String consgnNo;
				String remark;
				DateTime Shpment_dt;
				String last_user;
				//Inserting data into shipment_tracking

				strBuilder = new StringBuilder();
				strBuilder.Append ("insert into shipment_tracking (applicationid,enterpriseid,consignment_no,tracking_datetime,booking_no,");
				strBuilder.Append("status_code,last_userid,last_updated, invoiceable, surcharge, location) ");
				strBuilder.Append(" values('"+strAppID+"',"+"'"+strEnterpriseID+"',");
				if((drEach["consignment_no"]!= null) && (!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strConsgNo = (String) drEach["consignment_no"];
					strBuilder.Append("'");
					strBuilder.Append(strConsgNo);
					strBuilder.Append("'");
					consgnNo = (String) drEach["consignment_no"];
				}
				else
				{
					strBuilder.Append("null");
					consgnNo = null;
				}
				strBuilder.Append(",");
				strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,System.DateTime.Now,DTFormat.DateTime));
				Shpment_dt = System.DateTime.Now;
				strBuilder.Append(",");

				strBuilder.Append(iBooking_no);
				strBuilder.Append(",");
				booking_no = (int)iBooking_no;
				
				if((drEach["last_status_code"]!= null) && (!drEach["last_status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strLastStatusCode = (String) drEach["last_status_code"];
					strBuilder.Append("'");
					strBuilder.Append(strLastStatusCode);	
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(",");

				if(userID != null)
				{
					strBuilder.Append("'");
					strBuilder.Append(userID);	
					strBuilder.Append("'");
					last_user = userID;
				}
				else
				{
					strBuilder.Append("null");
					last_user = null;
				}
				strBuilder.Append(",");
				strBuilder.Append(Utility.DateFormat(strAppID,strEnterpriseID,System.DateTime.Now,DTFormat.DateTime));
				strBuilder.Append(",'N',0");

				strBuilder.Append(",");
				if((drEach["origin_state_code"]!= null) && (!drEach["origin_state_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
				{
					String strlocation = (String) drEach["origin_state_code"];
					strBuilder.Append("'");
					strBuilder.Append(strlocation);
					strBuilder.Append("'");
				}
				else
				{
					strBuilder.Append("null");
				}
				strBuilder.Append(")");

				//				dbCmd.CommandText = strBuilder.ToString();
				//				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Utility.CreateAutoConsignmentHistory(strAppID, strEnterpriseID,2,null,booking_no,consgnNo,Shpment_dt,last_user,ref dbCmd,ref dbCon);

				Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF008",iRowsAffected + " rows inserted into shipment_tracking table");

				//get exist consignment record from the consignment table
				strBuilder = new StringBuilder();
				strBuilder.Append("SELECT recordid " );
				strBuilder.Append("FROM ImportedConsignments ");
				strBuilder.Append("WHERE consignment_no = '" + drEach["consignment_no"].ToString() + "' ");
				strBuilder.Append("AND userid = '" + userID + "' ");

				dbCmd.CommandText = strBuilder.ToString();
				DataSet dsConsignment = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);

				//delete audit of consignment record from the audit table
				foreach(DataRow dr in dsConsignment.Tables[0].Rows)
				{
					strBuilder = new StringBuilder();
					strBuilder.Append("DELETE " );
					strBuilder.Append("FROM ImportedConsignmentsAuditFailures ");
					strBuilder.Append("WHERE recordid = '" + dr["recordid"].ToString() + "' ");
					strBuilder.Append("AND userid = '" + userID + "' ");
					strBuilder.Append("AND recordtype = 'Consignment' ");

					dbCmd.CommandText = strBuilder.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				}

				//delete consignment record from the consignment table
				strBuilder = new StringBuilder();
				strBuilder.Append("DELETE " );
				strBuilder.Append("FROM ImportedConsignments ");
				strBuilder.Append("WHERE consignment_no = '" + drEach["consignment_no"].ToString() + "' ");
				strBuilder.Append("AND userid = '" + userID + "' ");

				dbCmd.CommandText = strBuilder.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);

				//get exist package records from the package table
				strBuilder = new StringBuilder();
				strBuilder.Append("SELECT recordid " );
				strBuilder.Append("FROM ImportedConsignmentsPkgs ");
				strBuilder.Append("WHERE consignment_no = '" + drEach["consignment_no"].ToString() + "' ");
				strBuilder.Append("AND userid = '" + userID + "' ");

				dbCmd.CommandText = strBuilder.ToString();
				DataSet dsPackage = (DataSet)dbCon.ExecuteQueryInTransaction(dbCmd, ReturnType.DataSetType);

				//delete audit of package record from the audit table
				foreach(DataRow dr in dsPackage.Tables[0].Rows)
				{
					strBuilder = new StringBuilder();
					strBuilder.Append("DELETE " );
					strBuilder.Append("FROM ImportedConsignmentsAuditFailures ");
					strBuilder.Append("WHERE recordid = '" + dr["recordid"].ToString() + "' ");
					strBuilder.Append("AND userid = '" + userID + "' ");
					strBuilder.Append("AND recordtype = 'Package' ");

					dbCmd.CommandText = strBuilder.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				}

				//delete package record from the package table
				strBuilder = new StringBuilder();
				strBuilder.Append("DELETE " );
				strBuilder.Append("FROM ImportedConsignmentsPkgs ");
				strBuilder.Append("WHERE consignment_no = '" + drEach["consignment_no"].ToString() + "' ");
				strBuilder.Append("AND userid = '" + userID + "' ");

				dbCmd.CommandText = strBuilder.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				

				//Add by X Mar 25 10 - Remove wrong Booking No of this Con
				strBuilder = new StringBuilder();			
				strBuilder.Append("delete from shipment_tracking ");
				strBuilder.Append("where applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and consignment_no = '");
				strBuilder.Append(Utility.ReplaceSingleQuote(consgnNo));
				strBuilder.Append("' and booking_no <> " + booking_no);

				dbCmd.CommandText = strBuilder.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				//End Add by X Mar 25 10



				transactionApp.Commit();

				CustomerCreditused ocustused = new CustomerCreditused();
				ocustused.applicationid = strAppID;
				ocustused.enterpriseid = strEnterpriseID;

				foreach(DataRow drCU in dtCreditUsed.Rows)
				{
					ocustused.consignment_no = drCU["ConsignmentNo"].ToString();
					ocustused.cusid = drCU["CustID"].ToString();
					ocustused.transcode = "IMP";
					ocustused.amount = decimal.Parse(drCU["CreditAmount"].ToString());
					ocustused.updateCreditused();
				}

				Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF009","App db insert transaction committed.");
			}
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF010","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment","ERR008","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment","ERR009","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error adding Shipment ",appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("module1","DomesticShipmentMgrDAL","AddDomesticShipment","INF011","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment","ERR010","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("module1","DomesticShipmentMgrDAL","AddDomesticShipment","ERR011","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error adding Shipment ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			return iBooking_no;
		}
		
		

		public static DataSet GetSurchargeFromVAS(String strAppID, String strEnterpriseID)
		{
			SessionDS sessionDS = null;			
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT * ");
			strBuilder.Append("FROM vas ");
			strBuilder.Append("WHERE enterpriseid = '" + strEnterpriseID + "' ");
			strBuilder.Append("AND applicationid = '" + strAppID + "' ");
			strBuilder.Append("AND VAS_CODE = 'COD'");
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","GetCoreSysetmCode","GetCoreSysetmCode","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), 0, 0, "IVAS");
			return  sessionDS.ds;
		}

		public static DataSet GetSurchargeAllEntVAS(String strAppID, String strEnterpriseID)
		{
			SessionDS sessionDS = null;			
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT * ");
			strBuilder.Append("FROM vas ");
			strBuilder.Append("WHERE enterpriseid = '" + strEnterpriseID + "' ");
			strBuilder.Append("AND applicationid = '" + strAppID + "' ");
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","GetCoreSysetmCode","GetCoreSysetmCode","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), 0, 0, "IVAS");
			return  sessionDS.ds;
		}



		public static DataSet GetSurchargeFromEnterpriseSurcharge(String strAppID, String strEnterpriseID)
		{
			SessionDS sessionDS = null;			
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT A.vas_code, B.vas_description, B.surcharge ");
			strBuilder.Append("FROM (select * ");
			strBuilder.Append("from Enterprise_Default_Surcharge ");
			strBuilder.Append("where applicationid = '" + strAppID + "' ");
			strBuilder.Append("and enterpriseid = '" + strEnterpriseID + "' ");
			strBuilder.Append("and getDate() > effective_from ");
			strBuilder.Append("and status_active = 'Y') A inner join ");
			strBuilder.Append("(select * ");
			strBuilder.Append("from VAS	");
			strBuilder.Append("where applicationid = '" + strAppID + "' ");
			strBuilder.Append("and enterpriseid = '" + strEnterpriseID + "') B ");
			strBuilder.Append("on (A.applicationid = B.applicationid ");
			strBuilder.Append("and A.enterpriseid = B.enterpriseid ");
			strBuilder.Append("and A.vas_code = B.vas_code) ");
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","GetCoreSysetmCode","GetCoreSysetmCode","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), 0, 0, "IENT");
			return  sessionDS.ds;
		}

		public static SessionDS getDCToOrigStation(String strAppID, String strEnterpriseID,
			int iCurrent, int iDSRecSize, String strSenderZipcode)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","getDCToOrigStation","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strQry = new StringBuilder();
			strQry.Append("SELECT distinct b.origin_station ");
			strQry.Append("FROM zipcode a ");
			strQry.Append("INNER JOIN Delivery_Path b ");
			strQry.Append("ON (a.applicationid = b.applicationid ");
			strQry.Append("AND a.enterpriseid = b.enterpriseid ");
			strQry.Append("AND a.pickup_route = b.path_code) ");
			strQry.Append("WHERE a.applicationid = '" + strAppID + "' ");
			strQry.Append("AND a.enterpriseid = '" + strEnterpriseID + "' ");
			strQry.Append("AND a.zipcode = '" + strSenderZipcode + "'");
					
			sessionDS = dbCon.ExecuteQuery(strQry.ToString(),iCurrent, iDSRecSize,"DCOriginStation");
			return  sessionDS;	
		}


		public static SessionDS getDCToDestStation(String strAppID, String strEnterpriseID,
			int iCurrent, int iDSRecSize, String strRouteCode)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL","getDCToDestStation","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strQry = new StringBuilder();
			strQry.Append("SELECT distinct path_code, origin_station ");
			strQry.Append("FROM Delivery_Path ");
			strQry.Append("WHERE applicationid = '" + strAppID + "' ");
			strQry.Append("AND enterpriseid = '" + strEnterpriseID + "' ");
			strQry.Append("AND path_code = '" + strRouteCode + "'");
					
			sessionDS = dbCon.ExecuteQuery(strQry.ToString(),iCurrent, iDSRecSize,"DCDestStation");
			return  sessionDS;	
		}


		public static DataSet GetPreshipRec(String strAppID, String strEnterpriseID, String ConsignmentNo)
		{
			SessionDS sessionDS = null;			
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("	SELECT * ");
			strBuilder.Append(" FROM shipment  ");
			strBuilder.Append(" WHERE applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and consignment_no = '");
			strBuilder.Append(ConsignmentNo + "'");
			
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("ImportConsignmentsDAL","GetCoreSysetmCode","GetCoreSysetmCode","DbConnection object is null!!");
				return sessionDS.ds;
			}
			
			sessionDS = dbCon.ExecuteQuery(strBuilder.ToString(), 0, 0, "IVAS");
			return  sessionDS.ds;
		}

		public static DataSet GetShipment_PKG(String strAppID, String strEnterpriseID, String ConsignmentNo)
		{
			StringBuilder strQry =  new StringBuilder(); 
			strQry.Append("select * from shipment_pkg where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID+"'");
			//			strQry.Append(" and booking_no = ");
			//			strQry.Append(bookingNo);
			strQry.Append(" and consignment_no = '");
			strQry.Append(ConsignmentNo+"'");

			IDbCommand dbcmd = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
 
			try
			{
				DataSet dsPkgDtls = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
				return  dsPkgDtls;
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentMgrDAL.cs","GetPakageDtlsSWB","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

		}


		public ImportConsignmentsDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static int ImportConsignment_SavingPkgDetail(string strAppID, string strEnterpriseID, string userId)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userid",userId));

			int result = (int)dbCon.ExecuteProcedure("ImportConsignment",storedParams);
			
			return result;

		}
		
		public static int ImportConsignments_Clear(string strAppID, string strEnterpriseID, string strUserID)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userid",strUserID));

			DataSet ds = (DataSet)dbCon.ExecuteProcedure("ImportConsignmentsClear",storedParams, ReturnType.DataSetType);

			int result = 0;
			if(ds != null)
				result = (int)ds.Tables[0].Rows[0][0];
			return result;
		}
		public static int ImportConsignments_Import(string strAppID, string strEnterpriseID, string strUserID, string fn)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userid",strUserID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@fn",fn));

			DataSet ds = (DataSet)dbCon.ExecuteProcedure("ImportConsignments",storedParams, ReturnType.DataSetType);

			int result = 0;
			if(ds != null)
				result = (int)ds.Tables[0].Rows[0][0];
			return result;
		}
		public static DataSet ImportConsignments_Save(string strAppID, string strEnterpriseID, string strUserID, int importInsertUpdate, string strVas)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userid",strUserID));
			//if(importInsertUpdate != 0)
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@ImportInsertUpdate",importInsertUpdate));

			if(strVas.Trim().Length>0)
			{
				string vas = "@VAS"+strVas;
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@VAS",vas));
			}


			DataSet ds = (DataSet)dbCon.ExecuteProcedure("ImportConsignmentsSave",storedParams, ReturnType.DataSetType);

			return ds;
		}
	}
}
