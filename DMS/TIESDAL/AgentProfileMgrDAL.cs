using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;



namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for AgentProfileMgrDAL.
	/// </summary>
	public class AgentProfileMgrDAL
	{
		public AgentProfileMgrDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		//Methods for Agent Profile Base Module
		public static DataSet GetStatusCodeDS(String strAppID, String strEnterpriseID)
		{
			DataSet dsStatusCode = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return dsStatusCode;
			}

			String strSQLQuery = "select status_code, status_description, status_close, invoiceable from Status_Code where ";
			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and ";
 
			

			
			
			dsStatusCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);


			return  dsStatusCode;
	
		}

		public static SessionDS GetStatusCodeDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{

			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select status_code, status_description, status_close, invoiceable, system_code from Status_Code where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' ");

			DataRow drEach = dsQueryParam.Tables[0].Rows[0];
				
			String strStatusCode = (String) drEach["status_code"];
			if((strStatusCode != null) && (strStatusCode != ""))
			{
				strBuilder.Append(" and status_code like '%");
				strBuilder.Append(strStatusCode);
				strBuilder.Append("%' ");

			}

			String strStatusDescription = (String) drEach["status_description"];
			if((strStatusDescription != null) && (strStatusDescription != ""))
			{
				strBuilder.Append(" and status_description like N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strStatusDescription));
				strBuilder.Append("%' ");

			}

			String strInvoiceable = (String) drEach["invoiceable"];
			if((strInvoiceable != null) && (strInvoiceable != ""))
			{
				strBuilder.Append(" and invoiceable = '");
				strBuilder.Append(strInvoiceable);
				strBuilder.Append("' ");
			}

			String strCloseStatus = (String) drEach["status_close"];
			if((strCloseStatus != null) && (strCloseStatus != ""))
			{
				strBuilder.Append(" and status_close = '");
				strBuilder.Append(strCloseStatus);
				strBuilder.Append("' ");
			}

			String strSQLQuery = strBuilder.ToString();
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"StatusCodeTable");

			return  sessionDS;
	
		}

		public static int AddStatusCodeDS(String strAppID, String strEnterpriseID,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Status_Code (applicationid,enterpriseid,status_code, status_description, status_close, invoiceable,system_code) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			String strStatusCode = (String) drEach["status_code"];
			//			if((strStatusCode != null) && (strStatusCode != ""))
			//			{
			strBuilder.Append(strStatusCode);
			strBuilder.Append("',N'");

			//			}

			String strStatusDescription = (String) drEach["status_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strStatusDescription));
			strBuilder.Append("','");


			String strCloseStatus = (String) drEach["status_close"];
			strBuilder.Append(strCloseStatus);
			strBuilder.Append("','");

			String strInvoiceable = (String) drEach["invoiceable"];
			strBuilder.Append(strInvoiceable);
			strBuilder.Append("','N')");


			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","AddStatusCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddStatusCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}



			return iRowsAffected;

		}

		public static int ModifyStatusCodeDS(String strAppID, String strEnterpriseID,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Status_Code set ");//, status_close, invoiceable,system_code) values ('");
			strBuilder.Append("status_description = '");
			String strStatusDescription = (String) drEach["status_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strStatusDescription));
			strBuilder.Append("', status_close = '");
			String strCloseStatus = (String) drEach["status_close"];
			strBuilder.Append(strCloseStatus);
			strBuilder.Append("', invoiceable = '");
			String strInvoiceable = (String) drEach["invoiceable"];
			strBuilder.Append(strInvoiceable);
			strBuilder.Append("' where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and status_code = '");
			String strStatusCode = (String) drEach["status_code"];
			strBuilder.Append(strStatusCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","AddStatusCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddStatusCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}

			return iRowsAffected;

		}

		public static SessionDS GetEmptyStatusCodeDS(int iNumRows)
		{
			DataTable dtStatusCode = new DataTable();
 
			dtStatusCode.Columns.Add(new DataColumn("status_code", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("status_description", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("status_close", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("invoiceable", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("system_code", typeof(string)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtStatusCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";
				drEach[3] = "";
				drEach[4] = "";

				dtStatusCode.Rows.Add(drEach);
			}

			DataSet dsStatusCode = new DataSet();
			dsStatusCode.Tables.Add(dtStatusCode);

			dsStatusCode.Tables[0].Columns["status_code"].Unique = true; //Checks duplicate records..
			dsStatusCode.Tables[0].Columns["status_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsStatusCode;
			sessionDS.DataSetRecSize = dsStatusCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}

		public static void AddNewRowInStatusCodeDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			drNew[3] = "";
			drNew[4] = "";

			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","AddNewRowInStatusCodeDS","R005",ex.Message.ToString());
			}
			sessionDS.QueryResultMaxSize++;
		}

		public static int DeleteStatusCodeDS(String strAppID, String strEnterpriseID,String strStatusCode)
		{
			int iRowsDeleted = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

	
			//Get App dbConnection

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteStatusCodeDS","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Begin Core and App Transaction

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteStatusCodeDS","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteStatusCodeDS","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("AgentProfileMgrDAL","DeleteStatusCodeDS","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteStatusCodeDS","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Beginning a App Transaction",null);
			}


			try
			{

				//Update record into core_enterprise Table

				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Exception_Code where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and status_code = '");
				strBuilder.Append(strStatusCode);
				strBuilder.Append("'");


				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteStatusCodeDS","INF001",iRowsDeleted + " rows deleted from Exception_Code table");


				strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Status_Code where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and status_code = '");
				strBuilder.Append(strStatusCode);
				strBuilder.Append("'");

				strSQLQuery = strBuilder.ToString();

				dbCommandApp.CommandText = strSQLQuery;


				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteStatusCodeDS","INF001",iRowsDeleted + " rows deleted from Status_Code table");
			
				//Commit application and core transaction.

				transactionApp.Commit();
				Logger.LogTraceInfo("AgentProfileMgrDAL","DeleteStatusCodeDS","SDM003","App db delete transaction committed.");

			}
			catch(ApplicationException appException)
			{
				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("AgentProfileMgrDAL","DeleteStatusCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("AgentProfileMgrDAL","DeleteStatusCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("AgentProfileMgrDAL","DeleteStatusCodeDS","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("AgentProfileMgrDAL","DeleteStatusCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("AgentProfileMgrDAL","DeleteStatusCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("AgentProfileMgrDAL","DeleteStatusCodeDS","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				
				dbCommandApp.Dispose();
				transactionApp.Dispose();
				conApp.Dispose();
			}
			
			return iRowsDeleted;
		}


		/* Exception Code Dataset*/

		public static DataSet GetExceptionCodeDS(String strAppID, String strEnterpriseID,String strStatusCode)
		{
			DataSet dsExceptionCode = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return dsExceptionCode;
			}

			String strSQLQuery = "select exception_code, exception_description, mbg, status_close, invoiceable from Exception_Code where ";
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and status_code = '"+strStatusCode+"'"; 
			
			dsExceptionCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);

			int cnt = dsExceptionCode.Tables[0].Rows.Count;


			return  dsExceptionCode;
	
		}

		public static SessionDS GetExceptionCodeDS(String strAppID, String strEnterpriseID,String strStatusCode,int iCurrent, int iDSRecSize)
		{
			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			String strSQLQuery = "select exception_code, exception_description, mbg, status_close, invoiceable, system_code from Exception_Code where ";
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and status_code = '"+strStatusCode+"'"; 
			
			//dsExceptionCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ExceptionCodeTable");

			return  sessionDS;
	
		}

		public static int AddExceptionCodeDS(String strAppID, String strEnterpriseID,String strStatusCode,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","AddExceptionCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Exception_Code (applicationid,enterpriseid,status_code, exception_code, exception_description, mbg, status_close, invoiceable,system_code) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(strStatusCode);
			strBuilder.Append("','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			String strExceptionCode = (String) drEach["exception_code"];
			//			if((strStatusCode != null) && (strStatusCode != ""))
			//			{
			strBuilder.Append(strExceptionCode);
			strBuilder.Append("',N'");

			//			}

			String strExceptionDescription = (String) drEach["exception_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strExceptionDescription));
			strBuilder.Append("','");

			String strMBG = (String) drEach["mbg"];
			strBuilder.Append(strMBG);
			strBuilder.Append("','");

			String strCloseStatus = (String) drEach["status_close"];
			strBuilder.Append(strCloseStatus);
			strBuilder.Append("','");

			String strInvoiceable = (String) drEach["invoiceable"];
			strBuilder.Append(strInvoiceable);
			strBuilder.Append("','N')");


			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","AddExceptionCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddExceptionCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}



			return iRowsAffected;

		}

		public static int ModifyExceptionCodeDS(String strAppID, String strEnterpriseID,String strStatusCode,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","ModifyExceptionCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Exception_Code set ");//, status_close, invoiceable,system_code) values ('");
			strBuilder.Append("exception_description = N'");
			String strExceptionDescription = (String) drEach["exception_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strExceptionDescription));
			strBuilder.Append("', mbg = '");
			String strMBG = (String) drEach["mbg"];
			strBuilder.Append(strMBG);
			strBuilder.Append("', status_close = '");
			String strCloseStatus = (String) drEach["status_close"];
			strBuilder.Append(strCloseStatus);
			strBuilder.Append("', invoiceable = '");
			String strInvoiceable = (String) drEach["invoiceable"];
			strBuilder.Append(strInvoiceable);
			strBuilder.Append("' where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and status_code = '");
			strBuilder.Append(strStatusCode);
			strBuilder.Append("' and exception_code = '");
			String strExceptionCode = (String) drEach["exception_code"];
			strBuilder.Append(strExceptionCode);
			strBuilder.Append("'");


			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","ModifyExceptionCodeDS","SDM001",iRowsAffected+" rows inserted in to Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyExceptionCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}

			return iRowsAffected;
		}

		public static SessionDS GetEmptyExceptionCodeDS(int iNumRows)
		{
			DataTable dtExceptionCode = new DataTable();
 
			dtExceptionCode.Columns.Add(new DataColumn("exception_code", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("exception_description", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("mbg", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("status_close", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("invoiceable", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("system_code", typeof(string)));


			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtExceptionCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";
				drEach[3] = "";
				drEach[4] = "";
				drEach[5] = "";

				dtExceptionCode.Rows.Add(drEach);
			}

			DataSet dsExceptionCode = new DataSet();
			dsExceptionCode.Tables.Add(dtExceptionCode);

			dsExceptionCode.Tables[0].Columns["exception_code"].Unique = true; //Checks duplicate records..
			dsExceptionCode.Tables[0].Columns["exception_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsExceptionCode;
			sessionDS.DataSetRecSize = dsExceptionCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;	
		}

		public static void AddNewRowInExceptionCodeDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			drNew[3] = "";
			drNew[4] = "";
			drNew[5] = "";
			
			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","AddNewRowInExceptionCodeDS","R005",ex.Message.ToString());
			}
			sessionDS.QueryResultMaxSize++;
		}

		public static int DeleteExceptionCodeDS(String strAppID, String strEnterpriseID,String strStatusCode,String strExceptionCode)
		{
			int iRowsDeleted = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteExceptionCodeRecord","EDB101","DbConnection object is null!!");
				return iRowsDeleted;
			}


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("Delete from Exception_Code where ");
			strBuilder.Append(" applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and status_code = '");
			strBuilder.Append(strStatusCode);
			strBuilder.Append("' and exception_code = '");
			strBuilder.Append(strExceptionCode);
			strBuilder.Append("'");


			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsDeleted = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","DeleteExceptionCodeRecord","SDM001",iRowsDeleted+" rows deleted from  Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteExceptionCodeRecord","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code "+appException.Message,appException);
			}

			return iRowsDeleted;		
		}

/* Service Code Dataset*/

		public static SessionDS GetServiceCodeDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{

			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","GetServiceCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select service_code, service_description, percent_discount, commit_time, transit_day,transit_hour from Service where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' ");

			DataRow drEach = dsQueryParam.Tables[0].Rows[0];
				
			String strServiceCode = (String) drEach["service_code"];
			if((strServiceCode != null) && (strServiceCode != ""))
			{
				strBuilder.Append(" and service_code like '%");
				strBuilder.Append(strServiceCode);
				strBuilder.Append("%' ");

			}

			String strServiceDescription = (String) drEach["service_description"];
			if((strServiceDescription != null) && (strServiceDescription != ""))
			{
				strBuilder.Append(" and service_description like N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strServiceDescription));
				strBuilder.Append("%' ");

			}

			if((drEach["percent_discount"]!= null) && (!drEach["percent_discount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decPercentDiscount = (decimal) drEach["percent_discount"];

				strBuilder.Append(" and percent_discount = ");
				strBuilder.Append(decPercentDiscount);
				strBuilder.Append("  ");
			}

			if((drEach["commit_time"]!= null) && (!drEach["commit_time"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtCommitTime = (DateTime) drEach["commit_time"];
				strBuilder.Append(" and commit_time = ");
				String strTime = Utility.DateFormat(strAppID,strEnterpriseID,dtCommitTime,DTFormat.Time);
				strBuilder.Append(strTime);
				strBuilder.Append(" ");
			}

			if((drEach["transit_day"]!= null) && (!drEach["transit_day"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				int iTransitDay = (int) drEach["transit_day"];
				strBuilder.Append(" and transit_day = ");
				strBuilder.Append(iTransitDay);
				strBuilder.Append("  ");
			}

			if((drEach["transit_hour"]!= null) && (!drEach["transit_hour"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				int iTransitHour = (int) drEach["transit_hour"];
				strBuilder.Append(" and transit_hour = ");
				strBuilder.Append(iTransitHour);
				strBuilder.Append("  ");
			}


			String strSQLQuery = strBuilder.ToString();
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ServiceCodeTable");

			return  sessionDS;
	
		}

		public static int AddServiceCodeDS(String strAppID, String strEnterpriseID,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","GetServiceCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Service (applicationid,enterpriseid,service_code, service_description, percent_discount, commit_time,transit_day,transit_hour) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			String strServiceCode = (String) drEach["service_code"];
			strBuilder.Append(strServiceCode);
			strBuilder.Append("',N'");

			String strServiceDescription = (String) drEach["service_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strServiceDescription));
			strBuilder.Append("',");

			if((drEach["percent_discount"]!= null) && (!drEach["percent_discount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decPercentDiscount = (decimal) drEach["percent_discount"];
				strBuilder.Append(decPercentDiscount);
				strBuilder.Append(",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");
			}

			if((drEach["commit_time"]!= null) && (!drEach["commit_time"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtCommitTime = (DateTime) drEach["commit_time"];
				String strTime = Utility.DateFormat(strAppID,strEnterpriseID,dtCommitTime,DTFormat.Time);
				strBuilder.Append(strTime);
				strBuilder.Append(",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");
			}

			if((drEach["transit_day"]!= null) && (!drEach["transit_day"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				int iTransitDay = (int) drEach["transit_day"];
				strBuilder.Append(iTransitDay);
				strBuilder.Append(",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");
			}

			if((drEach["transit_hour"]!= null) && (!drEach["transit_hour"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				int iTransitHour = (int) drEach["transit_hour"];
				strBuilder.Append(iTransitHour);
				strBuilder.Append(")");

			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(")");
			}


			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","AddServiceCodeDS","SDM001",iRowsAffected+" rows inserted in to Service table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddServiceCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Service Code ",appException);
			}



			return iRowsAffected;

		}

		public static int ModifyServiceCodeDS(String strAppID, String strEnterpriseID,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","GetServiceCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Service set ");
			strBuilder.Append("service_description = N'");
			String strServiceDescription = (String) drEach["service_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strServiceDescription));
			strBuilder.Append("', percent_discount = ");

			if((drEach["percent_discount"]!= null) && (!drEach["percent_discount"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decPercentDiscount = (decimal) drEach["percent_discount"];
				strBuilder.Append(decPercentDiscount);
			}
			else
			{
				strBuilder.Append("null");
			}


			strBuilder.Append(", commit_time = ");

			if((drEach["commit_time"]!= null) && (!drEach["commit_time"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				DateTime dtCommitTime = (DateTime) drEach["commit_time"];
				String strTime = Utility.DateFormat(strAppID,strEnterpriseID,dtCommitTime,DTFormat.Time);
				strBuilder.Append(strTime);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", transit_day = ");

			if((drEach["transit_day"]!= null) && (!drEach["transit_day"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				int iTransitDay = (int) drEach["transit_day"];
				strBuilder.Append(iTransitDay);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", transit_hour = ");

			if((drEach["transit_hour"]!= null) && (!drEach["transit_hour"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				int iTransitHour = (int) drEach["transit_hour"];
				strBuilder.Append(iTransitHour);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and service_code = '");
			String strServiceCode = (String) drEach["service_code"];
			strBuilder.Append(strServiceCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","AddServiceCodeDS","SDM001",iRowsAffected+" rows inserted in to Service_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddServiceCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Service Code ",appException);
			}



			return iRowsAffected;

		}

		public static SessionDS GetEmptyServiceCodeDS(int iNumRows)
		{			
			DataTable dtServiceCode = new DataTable();
 
			dtServiceCode.Columns.Add(new DataColumn("service_code", typeof(string)));
			dtServiceCode.Columns.Add(new DataColumn("service_description", typeof(string)));
			dtServiceCode.Columns.Add(new DataColumn("percent_discount", typeof(decimal)));
			dtServiceCode.Columns.Add(new DataColumn("commit_time", typeof(DateTime)));
			dtServiceCode.Columns.Add(new DataColumn("transit_day", typeof(int)));
			dtServiceCode.Columns.Add(new DataColumn("transit_hour", typeof(int)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtServiceCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";

				dtServiceCode.Rows.Add(drEach);
			}

			DataSet dsServiceCode = new DataSet();
			dsServiceCode.Tables.Add(dtServiceCode);

			dsServiceCode.Tables[0].Columns["service_code"].Unique = true; //Checks duplicate records..
			dsServiceCode.Tables[0].Columns["service_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsServiceCode;
			sessionDS.DataSetRecSize = dsServiceCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}

		public static void AddNewRowInServiceCodeDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";

			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","AddNewRowInServiceCodeDS","R005",ex.Message.ToString());
			}
			sessionDS.QueryResultMaxSize++;

		}

		public static int DeleteServiceCodeDS(String strAppID, String strEnterpriseID,String strServiceCode)
		{
			int iRowsDeleted = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

	
			//Get App dbConnection

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteServiceCodeDS","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Begin Core and App Transaction

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteServiceCodeDS","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteServiceCodeDS","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("AgentProfileMgrDAL","DeleteServiceCodeDS","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteServiceCodeDS","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Beginning a App Transaction",null);
			}


			try
			{

				//Update record into core_enterprise Table

				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Zipcode_Service_Excluded where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and service_code = '");
				strBuilder.Append(strServiceCode);
				strBuilder.Append("'");


				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteServiceCodeDS","INF001",iRowsDeleted + " rows deleted from Exception_Code table");


				strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Service where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and service_code = '");
				strBuilder.Append(strServiceCode);
				strBuilder.Append("'");

				strSQLQuery = strBuilder.ToString();

				dbCommandApp.CommandText = strSQLQuery;


				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteServiceCodeDS","INF001",iRowsDeleted + " rows deleted from Service table");
			
				//Commit application and core transaction.

				transactionApp.Commit();
				Logger.LogTraceInfo("AgentProfileMgrDAL","DeleteServiceCodeDS","SDM003","App db delete transaction committed.");

			}
			catch(ApplicationException appException)
			{
				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("AgentProfileMgrDAL","DeleteServiceCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("AgentProfileMgrDAL","DeleteServiceCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("AgentProfileMgrDAL","DeleteServiceCodeDS","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Service Code "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("AgentProfileMgrDAL","DeleteServiceCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("AgentProfileMgrDAL","DeleteServiceCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("AgentProfileMgrDAL","DeleteServiceCodeDS","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Service Code "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				dbCommandApp.Dispose();
				transactionApp.Dispose();
				conApp.Dispose();
			}
			
			return iRowsDeleted;
		}

/*ZIP Code Service Excluded Dataset*/

		public static SessionDS GetZipcodeServiceExcludedDS(String strAppID, String strEnterpriseID,String strServiceCode,int iCurrent, int iDSRecSize)
		{
			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			String strSQLQuery = "select zse.zipcode as zipcode, s.state_name as state_name, z.country as country from Zipcode_Service_Excluded zse, Zipcode z, State s where ";
			strSQLQuery += " zse.applicationid = z.applicationid and zse.enterpriseid = z.enterpriseid and zse.zipcode = z.zipcode and ";
			strSQLQuery += " z.applicationid = s.applicationid and z.enterpriseid = s.enterpriseid and z.country = s.country and z.state_code = s.state_code and ";
			strSQLQuery += " zse.applicationid = '"+strAppID+"' and zse.enterpriseid = '"+strEnterpriseID+"' and zse.service_code = '"+strServiceCode+"'"; 
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ZipcodeServiceExcludedTable");

			return  sessionDS;
	
		}

		public static int AddZipcodeServiceExcludedDS(String strAppID, String strEnterpriseID,String strServiceCode,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","AddZipcodeServiceExcludedDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Zipcode_Service_Excluded (applicationid,enterpriseid,service_code, zipcode ) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(strServiceCode);
			strBuilder.Append("','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			String strZipcode = (String) drEach["zipcode"];
			strBuilder.Append(strZipcode);
			strBuilder.Append("')");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","AddZipcodeServiceExcludedDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddZipcodeServiceExcludedDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}



			return iRowsAffected;

		}

		public static SessionDS GetEmptyZipcodeServiceExcludedDS(int iNumRows)
		{			

			DataTable dtZipcodeServiceExcluded = new DataTable();
 
			dtZipcodeServiceExcluded.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtZipcodeServiceExcluded.Columns.Add(new DataColumn("state_name", typeof(string)));
			dtZipcodeServiceExcluded.Columns.Add(new DataColumn("country", typeof(string)));



			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtZipcodeServiceExcluded.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";

				dtZipcodeServiceExcluded.Rows.Add(drEach);
			}

			DataSet dsZipcodeServiceExcluded = new DataSet();
			dsZipcodeServiceExcluded.Tables.Add(dtZipcodeServiceExcluded);

			dsZipcodeServiceExcluded.Tables[0].Columns["zipcode"].Unique = true; //Checks duplicate records..
			dsZipcodeServiceExcluded.Tables[0].Columns["zipcode"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsZipcodeServiceExcluded;
			sessionDS.DataSetRecSize = dsZipcodeServiceExcluded.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}

		public static void AddNewRowInZipcodeServiceExcludedDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";

			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","AddNewRowInZipcodeServiceExcludedDS","R005",ex.Message.ToString());
			}
			sessionDS.QueryResultMaxSize++;
		}

		public static int DeleteZipcodeServiceExcludedDS(String strAppID, String strEnterpriseID,String strServiceCode,String strZipcode)
		{
			int iRowsDeleted = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteExceptionCodeRecord","EDB101","DbConnection object is null!!");
				return iRowsDeleted;
			}


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("Delete from Zipcode_Service_Excluded where ");
			strBuilder.Append(" applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and service_code = '");
			strBuilder.Append(strServiceCode);
			strBuilder.Append("' and zipcode = '");
			strBuilder.Append(strZipcode);
			strBuilder.Append("'");


			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsDeleted = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","DeleteExceptionCodeRecord","SDM001",iRowsDeleted+" rows deleted from  Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteExceptionCodeRecord","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code "+appException.Message,appException);
			}

			return iRowsDeleted;		
		}

		public static SessionDS GetAgentDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","GetAgentProfileDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			//strBuilder.Append("select custid,ref_code,cust_name,contact_person,address1,address2,country,zipcode,telephone,mbg,fax,credit_term,industrial_sector_code,credit_limit,active_quotation_no,status_active,credit_outstanding,remark,salesmanid,prom_tot_wt,payment_mode,prom_tot_package,prom_period,free_insurance_amt,insurance_percent_surcharge,email,created_by,apply_dim_wt,pod_slip_required,apply_esa_surcharge from Agent where applicationid = '");
			strBuilder.Append ("Select agentid,agent_name ");
			strBuilder.Append (" FROM Agent where applicationid ='"+strAppID+"' and enterpriseid ='"+strEnterpriseID+"'");

			DataRow drEach = dsQueryParam.Tables[0].Rows[0];

			//agentid
			if((drEach["agentid"]!= null) && (!drEach["agentid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAgentAccNo = (String) drEach["agentid"];
				strBuilder.Append(" and agentid like ");
				strBuilder.Append("'%");
				strBuilder.Append(strAgentAccNo);
				strBuilder.Append("%' ");
			}	
			if((drEach["agent_name"]!= null) && (!drEach["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAgentName = (String) drEach["agent_name"];
				strBuilder.Append(" and agent_name like ");
				strBuilder.Append("N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAgentName));
				strBuilder.Append("%' ");
			}


			// Mohan, 20/11/2002 
			strBuilder.Append (" Order by Agent_name");
			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"AgentProfileTable");

			return  sessionDS;
	
		}

		public static SessionDS GetAgentProfileDS(String strAppID, String strEnterpriseID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","GetAgentProfileDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			//strBuilder.Append("select custid,ref_code,cust_name,contact_person,address1,address2,country,zipcode,telephone,mbg,fax,credit_term,industrial_sector_code,credit_limit,active_quotation_no,status_active,credit_outstanding,remark,salesmanid,prom_tot_wt,payment_mode,prom_tot_package,prom_period,free_insurance_amt,insurance_percent_surcharge,email,created_by,apply_dim_wt,pod_slip_required,apply_esa_surcharge from Agent where applicationid = '");
			strBuilder.Append ("Select agentid,ref_code,agent_name,contact_person,email,agent_address1,agent_address2, ");
			strBuilder.Append ("country,zipcode,telephone,fax,active_quotation_no,status_active,free_insurance_amt, ");
			strBuilder.Append ("insurance_percent_surcharge,remark,apply_dim_wt,pod_slip_required,apply_esa_surcharge, "); 
			strBuilder.Append ("Created_By,credit_term,credit_limit,credit_outstanding,salesmanid,payment_mode,mbg ");
			strBuilder.Append (" FROM Agent where applicationid ='"+strAppID+"' and enterpriseid ='"+strEnterpriseID+"'");

			DataRow drEach = dsQueryParam.Tables[0].Rows[0];

			//agentid
			if((drEach["agentid"]!= null) && (!drEach["agentid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAgentAccNo = (String) drEach["agentid"];
				strBuilder.Append(" and agentid like ");
				strBuilder.Append("'%");
				strBuilder.Append(strAgentAccNo);
				strBuilder.Append("%' ");
			}

			if((drEach["ref_code"]!= null) && (!drEach["ref_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRefCode = (String) drEach["ref_code"];
				strBuilder.Append(" and ref_code like ");
				strBuilder.Append("'%");
				strBuilder.Append(strRefCode);
				strBuilder.Append("%' ");
			}
	
			if((drEach["agent_name"]!= null) && (!drEach["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAgentName = (String) drEach["agent_name"];
				strBuilder.Append(" and agent_name like ");
				strBuilder.Append("N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAgentName));
				strBuilder.Append("%' ");
			}

			if((drEach["contact_person"]!= null) && (!drEach["contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strContactPerson = (String) drEach["contact_person"];
				strBuilder.Append(" and contact_person like ");
				strBuilder.Append("N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strContactPerson));
				strBuilder.Append("%' ");
			}

			if((drEach["email"]!= null) && (!drEach["email"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strEmail = (String) drEach["email"];
				strBuilder.Append(" and email like ");
				strBuilder.Append("'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strEmail));
				strBuilder.Append("%' ");
			}

			if((drEach["agent_address1"]!= null) && (!drEach["agent_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAddress1 = (String) drEach["agent_address1"];
				strBuilder.Append(" and agent_address1 like ");
				strBuilder.Append("N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress1));
				strBuilder.Append("%' ");
			}


			if((drEach["agent_address2"]!= null) && (!drEach["agent_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAddress2 = (String) drEach["agent_address2"];
				strBuilder.Append(" and agent_address2 like ");
				strBuilder.Append("N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress2));
				strBuilder.Append("%' ");
			}

			if((drEach["country"]!= null) && (!drEach["country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strCountry = (String) drEach["country"];
				strBuilder.Append(" and country like ");
				strBuilder.Append("'%");
				strBuilder.Append(strCountry);
				strBuilder.Append("%' ");
			}

			if((drEach["zipcode"]!= null) && (!drEach["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strZipcode = (String) drEach["zipcode"];
				strBuilder.Append(" and zipcode like ");
				strBuilder.Append("'%");
				strBuilder.Append(strZipcode);
				strBuilder.Append("%' ");
			}

			if((drEach["telephone"]!= null) && (!drEach["telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strTelephone = (String) drEach["telephone"];
				strBuilder.Append(" and telephone like ");
				strBuilder.Append("'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strTelephone));
				strBuilder.Append("%' ");
			}

			if((drEach["fax"]!= null) && (!drEach["fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strFax = (String) drEach["fax"];
				strBuilder.Append(" and fax like ");
				strBuilder.Append("'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strFax));
				strBuilder.Append("%' ");
			}

			if((drEach["active_quotation_no"]!= null) && (!drEach["active_quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strActiveQuotationNo = (String) drEach["active_quotation_no"];
				strBuilder.Append(" and active_quotation_no like ");
				strBuilder.Append("'%");
				strBuilder.Append(strActiveQuotationNo);
				strBuilder.Append("%' ");
			}

			if((drEach["status_active"]!= null) && (!drEach["status_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strStatusActive = (String) drEach["status_active"];
				strBuilder.Append(" and status_active like ");
				strBuilder.Append("'%");
				strBuilder.Append(strStatusActive);
				strBuilder.Append("%' ");
			}

			if((drEach["remark"]!= null) && (!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRemark = (String) drEach["remark"];
				strBuilder.Append(" and remark like ");
				strBuilder.Append("N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strRemark));
				strBuilder.Append("%' ");
			}

			if((drEach["free_insurance_amt"]!= null) && (!drEach["free_insurance_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decFreeInsuranceAmt = (decimal) drEach["free_insurance_amt"];
				strBuilder.Append(" and free_insurance_amt = ");
				strBuilder.Append(decFreeInsuranceAmt);
				strBuilder.Append(" ");
			}

			if((drEach["insurance_percent_surcharge"]!= null) && (!drEach["insurance_percent_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decInsurancePercentSurcharge = (decimal) drEach["insurance_percent_surcharge"];
				strBuilder.Append(" and insurance_percent_surcharge = ");
				strBuilder.Append(decInsurancePercentSurcharge);
				strBuilder.Append(" ");
			}

			if((drEach["apply_dim_wt"]!= null) && (!drEach["apply_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strApplyDimWt = (String) drEach["apply_dim_wt"];
				strBuilder.Append(" and apply_dim_wt like ");
				strBuilder.Append("'%");
				strBuilder.Append(strApplyDimWt);
				strBuilder.Append("%' ");
			}

			if((drEach["pod_slip_required"]!= null) && (!drEach["pod_slip_required"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPodSlipRequired = (String) drEach["pod_slip_required"];
				strBuilder.Append(" and pod_slip_required like ");
				strBuilder.Append("'%");
				strBuilder.Append(strPodSlipRequired);
				strBuilder.Append("%' ");
			}

			if((drEach["apply_esa_surcharge"]!= null) && (!drEach["apply_esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strApplyESASurcharge = (String) drEach["apply_esa_surcharge"];
				strBuilder.Append(" and apply_esa_surcharge like ");
				strBuilder.Append("'%");
				strBuilder.Append(strApplyESASurcharge);
				strBuilder.Append("%' ");
			}
			// Mohan, 21/11/02
			if((drEach["credit_term"]!= null) && (!drEach["credit_term"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				int strCreditTerm = (int) drEach["credit_term"];
				strBuilder.Append(" and credit_term= ");				
				strBuilder.Append(strCreditTerm);				
			}

			if((drEach["credit_limit"]!= null) && (!drEach["credit_limit"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal strCreditLimit = (decimal) drEach["credit_limit"];
				strBuilder.Append(" and credit_limit= ");				
				strBuilder.Append(strCreditLimit);				
			}
			
			if((drEach["credit_outstanding"]!= null) && (!drEach["credit_outstanding"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal strCreditOutstanding = (decimal) drEach["credit_outstanding"];
				strBuilder.Append(" and credit_outstanding = ");				
				strBuilder.Append(strCreditOutstanding);				
			}
			if((drEach["salesmanid"]!= null) && (!drEach["salesmanid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strSalesmanID = (String) drEach["salesmanid"];
				strBuilder.Append(" and salesmanid like ");
				strBuilder.Append("'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strSalesmanID));
				strBuilder.Append("%' ");
			}
			
			if((drEach["payment_mode"]!= null) && (!drEach["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPaymentMode = (String) drEach["payment_mode"];
				strBuilder.Append(" and payment_mode like ");
				strBuilder.Append("'%");
				strBuilder.Append(strPaymentMode);
				strBuilder.Append("%' ");
			}

			if((drEach["mbg"]!= null) && (!drEach["mbg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strMbg = (String) drEach["mbg"];
				strBuilder.Append(" and mbg like ");
				strBuilder.Append("'%");
				strBuilder.Append(strMbg);
				strBuilder.Append("%' ");
			}

			// Mohan, 20/11/2002 
			strBuilder.Append (" Order by Agent_name");
			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"AgentProfileTable");

			return  sessionDS;
	
		}

		//* INSERT INTO Agent (applicationid, agentid, enterpriseid, ref_code, agent_name, agent_address1, agent_address2, country, zipcode, telephone, fax, active_quotation_no, 
        //status_active, free_insurance_amt, insurance_percent_surcharge, remark, apply_dim_wt, pod_slip_required, apply_esa_surcharge) VALUES     (
		
		public static int AddAgentProfileDS(String strAppID, String strEnterpriseID,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","AddAgentProfileDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into agent (applicationid,enterpriseid,agentid,ref_code,agent_name, ");
			strBuilder.Append("contact_person,email,agent_address1,agent_address2,country,zipcode,telephone,fax, ");
			strBuilder.Append("active_quotation_no,status_active,free_insurance_amt,insurance_percent_surcharge,remark, ");
			strBuilder.Append("apply_dim_wt,pod_slip_required,apply_esa_surcharge,Created_By,credit_term,credit_limit, ");
			strBuilder.Append("credit_outstanding,salesmanid,payment_mode,mbg)  values ('");			
			strBuilder.Append(strAppID+"','"+strEnterpriseID+"','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;
			DataRow drEach = dsToInsert.Tables[0].Rows[0];					
			String strAgentID = (String) drEach["agentid"];
			strBuilder.Append(strAgentID);
			
		strBuilder.Append("',");
			if((drEach["ref_code"]!= null) && (!drEach["ref_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRefCode = (String) drEach["ref_code"];
				strBuilder.Append("'");
				strBuilder.Append(strRefCode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

		strBuilder.Append(",");
			if((drEach["agent_name"]!= null) && (!drEach["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAgentName = (String) drEach["agent_name"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAgentName));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

		strBuilder.Append(",");
			if((drEach["contact_person"]!= null) && (!drEach["contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strContactPerson = (String) drEach["contact_person"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strContactPerson));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

		strBuilder.Append(",");
			if((drEach["email"]!= null) && (!drEach["email"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strEmail = (String) drEach["email"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strEmail));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

		strBuilder.Append(",");
			if((drEach["agent_address1"]!= null) && (!drEach["agent_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAddress1 = (String) drEach["agent_address1"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress1));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

		strBuilder.Append(",");
			if((drEach["agent_address2"]!= null) && (!drEach["agent_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAddress2 = (String) drEach["agent_address2"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress2));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

		strBuilder.Append(",");
			if((drEach["country"]!= null) && (!drEach["country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strCountry = (String) drEach["country"];
				strBuilder.Append("'");
				strBuilder.Append(strCountry);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
		
		strBuilder.Append(",");
			if((drEach["zipcode"]!= null) && (!drEach["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strZipcode = (String) drEach["zipcode"];
				strBuilder.Append("'");
				strBuilder.Append(strZipcode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

		strBuilder.Append(",");
			if((drEach["telephone"]!= null) && (!drEach["telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strTelephone = (String) drEach["telephone"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strTelephone));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

		strBuilder.Append(",");
			if((drEach["fax"]!= null) && (!drEach["fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strFax = (String) drEach["fax"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strFax));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

		strBuilder.Append(",");
			if((drEach["active_quotation_no"]!= null) && (!drEach["active_quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strActiveQuotationNo = (String) drEach["active_quotation_no"];
				strBuilder.Append("'");
				strBuilder.Append(strActiveQuotationNo);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

		strBuilder.Append(",");
			if((drEach["status_active"]!= null) && (!drEach["status_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strStatusActive = (String) drEach["status_active"];
				strBuilder.Append("'");
				strBuilder.Append(strStatusActive);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

		strBuilder.Append(",");
			if((drEach["free_insurance_amt"]!= null) && (!drEach["free_insurance_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decFreeInsuranceAmt = (decimal) drEach["free_insurance_amt"];
				strBuilder.Append(decFreeInsuranceAmt);
			}
			else
			{
				strBuilder.Append("null");
			}

		strBuilder.Append(",");
			if((drEach["insurance_percent_surcharge"]!= null) && (!drEach["insurance_percent_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decInsurancePercentSurcharge = (decimal) drEach["insurance_percent_surcharge"];
				strBuilder.Append(decInsurancePercentSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

		strBuilder.Append(",");
			if((drEach["remark"]!= null) && (!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRemark = (String) drEach["remark"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strRemark));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

		strBuilder.Append(",");
			if((drEach["apply_dim_wt"]!= null) && (!drEach["apply_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strApplyDimWt = (String) drEach["apply_dim_wt"];
				strBuilder.Append("'");
				strBuilder.Append(strApplyDimWt);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

		strBuilder.Append(",");
			if((drEach["pod_slip_required"]!= null) && (!drEach["pod_slip_required"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPodSlipRequired = (String) drEach["pod_slip_required"];
				strBuilder.Append("'");
				strBuilder.Append(strPodSlipRequired);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

		strBuilder.Append(",");
			if((drEach["apply_esa_surcharge"]!= null) && (!drEach["apply_esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strApplyESASurcharge = (String) drEach["apply_esa_surcharge"];
				strBuilder.Append("'");
				strBuilder.Append(strApplyESASurcharge);
				strBuilder.Append("'");

			}
			else
			{
				strBuilder.Append("null");
			}
			
			//Create_by field , 11/11/2002 Mohan
		strBuilder.Append(",");
			if((drEach["Created_by"]!= null) && (!drEach["Created_by"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strCreatedby = (String) drEach["Created_by"];
				strBuilder.Append("'");
				strBuilder.Append(strCreatedby);
				strBuilder.Append("'");

			}
			else
			{
				strBuilder.Append("null");
			}
		// Mohan, 21/11/02
			strBuilder.Append(",");
			if((drEach["credit_term"]!= null) && (!drEach["credit_term"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				int strCreditTerm = (int) drEach["credit_term"];				
				strBuilder.Append(strCreditTerm);			
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if((drEach["credit_limit"]!= null) && (!drEach["credit_limit"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal strCreditLimit = (decimal) drEach["credit_limit"];				
				strBuilder.Append(strCreditLimit);
			}
			else
			{
				strBuilder.Append("null");
			}
			
			strBuilder.Append(",");
			if((drEach["credit_outstanding"]!= null) && (!drEach["credit_outstanding"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal strCreditOutstanding = (decimal) drEach["credit_outstanding"];				
				strBuilder.Append(strCreditOutstanding);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if((drEach["salesmanid"]!= null) && (!drEach["salesmanid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strSalesmanID = (String) drEach["salesmanid"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strSalesmanID));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			if((drEach["payment_mode"]!= null) && (!drEach["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPaymentMode = (String) drEach["payment_mode"];
				strBuilder.Append("'");
				strBuilder.Append(strPaymentMode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}	

			strBuilder.Append(",");
			if((drEach["mbg"]!= null) && (!drEach["mbg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strMbg = (String) drEach["mbg"];
				strBuilder.Append("'");
				strBuilder.Append(strMbg);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}	

			strBuilder.Append(")");
			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentDataMgrDAL","AddStatusCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddAgentProfileDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting agent profile : "+appException.Message,appException);
			}
			return iRowsAffected;

		}

		public static int ModifyAgentProfileDS(String strAppID, String strEnterpriseID,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","ModifyAgentProfileDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Agent set ref_code = ");
			if((drEach["ref_code"]!= null) && (!drEach["ref_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRefCode = (String) drEach["ref_code"];
				strBuilder.Append("'");
				strBuilder.Append(strRefCode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", agent_name = ");
			if((drEach["agent_name"]!= null) && (!drEach["agent_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAgentName = (String) drEach["agent_name"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAgentName));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", contact_person = ");
			if((drEach["contact_person"]!= null) && (!drEach["contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strContactPerson = (String) drEach["contact_person"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strContactPerson));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", email = ");
			if((drEach["email"]!= null) && (!drEach["email"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strEmail = (String) drEach["email"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strEmail));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", agent_address1 = ");
			if((drEach["agent_address1"]!= null) && (!drEach["agent_address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAddress1 = (String) drEach["agent_address1"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress1));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", agent_address2 = ");
			if((drEach["agent_address2"]!= null) && (!drEach["agent_address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAddress2 = (String) drEach["agent_address2"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress2));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", country = ");
			if((drEach["country"]!= null) && (!drEach["country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strCountry = (String) drEach["country"];
				strBuilder.Append("'");
				strBuilder.Append(strCountry);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", zipcode = ");
			if((drEach["zipcode"]!= null) && (!drEach["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strZipcode = (String) drEach["zipcode"];
				strBuilder.Append("'");
				strBuilder.Append(strZipcode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", telephone = ");
			if((drEach["telephone"]!= null) && (!drEach["telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strTelephone = (String) drEach["telephone"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strTelephone));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", fax = ");
			if((drEach["fax"]!= null) && (!drEach["fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strFax = (String) drEach["fax"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strFax));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", active_quotation_no = ");
			if((drEach["active_quotation_no"]!= null) && (!drEach["active_quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strActiveQuotationNo = (String) drEach["active_quotation_no"];
				strBuilder.Append("'");
				strBuilder.Append(strActiveQuotationNo);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", status_active = ");
			if((drEach["status_active"]!= null) && (!drEach["status_active"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strStatusActive = (String) drEach["status_active"];
				strBuilder.Append("'");
				strBuilder.Append(strStatusActive);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", remark = ");
			if((drEach["remark"]!= null) && (!drEach["remark"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strRemark = (String) drEach["remark"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strRemark));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", free_insurance_amt = ");
			if((drEach["free_insurance_amt"]!= null) && (!drEach["free_insurance_amt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decFreeInsuranceAmt = (decimal) drEach["free_insurance_amt"];
				strBuilder.Append(decFreeInsuranceAmt);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", insurance_percent_surcharge = ");
			if((drEach["insurance_percent_surcharge"]!= null) && (!drEach["insurance_percent_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decInsurancePercentSurcharge = (decimal) drEach["insurance_percent_surcharge"];
				strBuilder.Append(decInsurancePercentSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", apply_dim_wt = ");
			if((drEach["apply_dim_wt"]!= null) && (!drEach["apply_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strApplyDimWt = (String) drEach["apply_dim_wt"];
				strBuilder.Append("'");
				strBuilder.Append(strApplyDimWt);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", pod_slip_required = ");
			if((drEach["pod_slip_required"]!= null) && (!drEach["pod_slip_required"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPodSlipRequired = (String) drEach["pod_slip_required"];
				strBuilder.Append("'");
				strBuilder.Append(strPodSlipRequired);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", apply_esa_surcharge = ");
			if((drEach["apply_esa_surcharge"]!= null) && (!drEach["apply_esa_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strApplyESASurcharge = (String) drEach["apply_esa_surcharge"];
				strBuilder.Append("'");
				strBuilder.Append(strApplyESASurcharge);
				strBuilder.Append("'");

			}
			else
			{
				strBuilder.Append("null");
			}
			// Created_By field , 11/11/2002 Mohan
//			strBuilder.Append(", Created_By = ");
//			if((drEach["Created_By"]!= null) && (!drEach["Created_By"].GetType().Equals(System.Type.GetType("System.DBNull"))))
//			{
//				String strCreatedBy = (String) drEach["Created_By"];
//				strBuilder.Append("'");
//				strBuilder.Append(strCreatedBy);
//				strBuilder.Append("'");
//
//			}
//			else
//			{
//				strBuilder.Append("null");
//			}

			//Mohan, 21/11/02
			strBuilder.Append(", credit_term = ");
			if((drEach["credit_term"]!= null) && (!drEach["credit_term"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				int strCreditTerm = Convert.ToInt16(drEach["credit_term"]);				
				strBuilder.Append(strCreditTerm);
			}
			else
			{
				strBuilder.Append("null");
			}
			
			strBuilder.Append(", credit_limit = ");
			if((drEach["credit_limit"]!= null) && (!drEach["credit_limit"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal strCreditLimit = (decimal) drEach["credit_limit"];				
				strBuilder.Append(strCreditLimit);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", credit_outstanding = ");
			if((drEach["credit_outstanding"]!= null) && (!drEach["credit_outstanding"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal strCreditOutstanding = (decimal) drEach["credit_outstanding"];				
				strBuilder.Append(strCreditOutstanding);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", salesmanid = ");
			if((drEach["salesmanid"]!= null) && (!drEach["salesmanid"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strSalesmanID = (String) drEach["salesmanid"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strSalesmanID));
				strBuilder.Append("'");

			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", payment_mode = ");
			if((drEach["payment_mode"]!= null) && (!drEach["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strPaymentMode = (String) drEach["payment_mode"];
				strBuilder.Append("'");
				strBuilder.Append(strPaymentMode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", mbg = ");
			if((drEach["mbg"]!= null) && (!drEach["mbg"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strMbg = (String) drEach["mbg"];
				strBuilder.Append("'");
				strBuilder.Append(strMbg);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and agentid = '");
			String strAgentAccNo = (String) drEach["agentid"];
			strBuilder.Append(strAgentAccNo);
			strBuilder.Append("' ");

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);			
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentDataMgrDAL","ModifyAgentProfileDS","SDM001",iRowsAffected+" rows inserted in to Agent table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyAgentProfileDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error modifying Agent Profile "+appException.Message,appException);
			}
			return iRowsAffected;
		}

		public static SessionDS GetEmptyAgentProfileDS(int iNumRows)
		{
			DataTable dtAgentProfile = new DataTable(); 
			dtAgentProfile.Columns.Add(new DataColumn("agentid", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("ref_code", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("agent_name", typeof(string)));
			//Mohan 20/11/02
			dtAgentProfile.Columns.Add(new DataColumn("contact_person", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("email", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("agent_address1", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("agent_address2", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("country", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("telephone", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("fax", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("active_quotation_no", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("status_active", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("remark", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("free_insurance_amt", typeof(decimal)));
			dtAgentProfile.Columns.Add(new DataColumn("insurance_percent_surcharge", typeof(decimal)));
			dtAgentProfile.Columns.Add(new DataColumn("apply_dim_wt", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("pod_slip_required", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("apply_esa_surcharge", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("Created_By", typeof(string)));
			// Mohan, 21/11/02			
			dtAgentProfile.Columns.Add(new DataColumn("payment_mode", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("credit_term", typeof(int)));
			dtAgentProfile.Columns.Add(new DataColumn("credit_limit", typeof(decimal)));			
			dtAgentProfile.Columns.Add(new DataColumn("credit_outstanding", typeof(decimal)));
			dtAgentProfile.Columns.Add(new DataColumn("salesmanid", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("Mbg", typeof(string)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtAgentProfile.NewRow();
				dtAgentProfile.Rows.Add(drEach);
			}

			DataSet dsAgentProfile = new DataSet();
			dsAgentProfile.Tables.Add(dtAgentProfile);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsAgentProfile;
			sessionDS.DataSetRecSize = dsAgentProfile.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}
		public static SessionDS GetEmptyAgentDS(int iNumRows)
		{
			DataTable dtAgentProfile = new DataTable(); 
			dtAgentProfile.Columns.Add(new DataColumn("agentid", typeof(string)));
			dtAgentProfile.Columns.Add(new DataColumn("agent_name", typeof(string)));
			
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtAgentProfile.NewRow();
				dtAgentProfile.Rows.Add(drEach);
			}

			DataSet dsAgentProfile = new DataSet();
			dsAgentProfile.Tables.Add(dtAgentProfile);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsAgentProfile;
			sessionDS.DataSetRecSize = dsAgentProfile.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}
	
		//Methods for CP - Reference Base Module

		public static SessionDS GetReferenceDS(String strAppID, String strEnterpriseID, String strAgentID, int iCurrent, int iDSRecSize)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","GetReferenceDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select snd_rec_name,snd_rec_type,contact_person,email,address1,address2,country,zipcode,telephone,fax from Agent_Snd_Rec where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and agentid = '");
			strBuilder.Append(strAgentID);
			strBuilder.Append("'");

			strBuilder.Append (" Order by snd_rec_name"); //Mohan, 20/11/2002
			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ReferenceTable");
			return  sessionDS;
	
		}

		public static int AddReferenceDS(String strAppID, String strEnterpriseID, String strAgentID, DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","AddReferenceDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Agent_Snd_Rec (applicationid,enterpriseid,agentid,snd_rec_name,snd_rec_type,contact_person,email,address1,address2,country,zipcode,telephone,fax) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(strAgentID);
			strBuilder.Append("',");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;
			DataRow drEach = dsToInsert.Tables[0].Rows[0];

			if((drEach["snd_rec_name"]!= null) && (!drEach["snd_rec_name"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strSndRecName = (String) drEach["snd_rec_name"];
				strBuilder.Append("N'");
				strBuilder.Append(strSndRecName);
				strBuilder.Append("'");
			}

			strBuilder.Append(",");
			
			if((drEach["snd_rec_type"]!= null) && (!drEach["snd_rec_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strSndRecType = (String) drEach["snd_rec_type"];
				strBuilder.Append("'");
				strBuilder.Append(strSndRecType);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}


			strBuilder.Append(",");

			if((drEach["contact_person"]!= null) && (!drEach["contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strContactPerson = (String) drEach["contact_person"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strContactPerson));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			
			if((drEach["email"]!= null) && (!drEach["email"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strEmail = (String) drEach["email"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strEmail));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(",");
			
			if((drEach["address1"]!= null) && (!drEach["address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAddress1 = (String) drEach["address1"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress1));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if((drEach["address2"]!= null) && (!drEach["address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAddress2 = (String) drEach["address2"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress2));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if((drEach["country"]!= null) && (!drEach["country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strCountry = (String) drEach["country"];
				strBuilder.Append("'");
				strBuilder.Append(strCountry);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if((drEach["zipcode"]!= null) && (!drEach["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strZipcode = (String) drEach["zipcode"];
				strBuilder.Append("'");
				strBuilder.Append(strZipcode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if((drEach["telephone"]!= null) && (!drEach["telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strTelephone = (String) drEach["telephone"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strTelephone));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(",");
			if((drEach["fax"]!= null) && (!drEach["fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strFax = (String) drEach["fax"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strFax));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(")");
			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);		
			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentDataMgrDAL","AddStatusCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddReferenceDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Agent profile "+appException.Message,appException);
			}
			return iRowsAffected;

		}

		public static int ModifyReferenceDS(String strAppID, String strEnterpriseID, String strAgentID, DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","ModifyReferenceDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];
			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Agent_Snd_Rec set snd_rec_type = ");//, status_close, invoiceable,system_code) values ('");
			if((drEach["snd_rec_type"]!= null) && (!drEach["snd_rec_type"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strSndRecType = (String) drEach["snd_rec_type"];
				strBuilder.Append("'");
				strBuilder.Append(strSndRecType);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}			
			strBuilder.Append(", contact_person = ");
			if((drEach["contact_person"]!= null) && (!drEach["contact_person"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strContactPerson = (String) drEach["contact_person"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strContactPerson));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", email = ");
			if((drEach["email"]!= null) && (!drEach["email"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strEmail = (String) drEach["email"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strEmail));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", address1 = ");
			if((drEach["address1"]!= null) && (!drEach["address1"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAddress1 = (String) drEach["address1"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress1));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", address2 = ");
			if((drEach["address2"]!= null) && (!drEach["address2"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strAddress2 = (String) drEach["address2"];
				strBuilder.Append("N'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strAddress2));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", country = ");
			if((drEach["country"]!= null) && (!drEach["country"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strCountry = (String) drEach["country"];
				strBuilder.Append("'");
				strBuilder.Append(strCountry);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", zipcode = ");
			if((drEach["zipcode"]!= null) && (!drEach["zipcode"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strZipcode = (String) drEach["zipcode"];
				strBuilder.Append("'");
				strBuilder.Append(strZipcode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", telephone = ");
			if((drEach["telephone"]!= null) && (!drEach["telephone"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strTelephone = (String) drEach["telephone"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strTelephone));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", fax = ");
			if((drEach["fax"]!= null) && (!drEach["fax"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strFax = (String) drEach["fax"];
				strBuilder.Append("'");
				strBuilder.Append(Utility.ReplaceSingleQuote(strFax));
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and agentid = '");
			strBuilder.Append(strAgentID);
			//strBuilder.Append("' and snd_rec_name = N'"); fix by Suthat
			strBuilder.Append("' and snd_rec_name like N'%");
			String strSndRecName = (String) drEach["snd_rec_name"];
			strBuilder.Append(strSndRecName);
			strBuilder.Append("%'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentDataMgrDAL","ModifyReferenceDS","SDM001",iRowsAffected+" rows inserted in to Agent table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyReferenceDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error modifying Agent Profile "+appException.Message,appException);
			}
			return iRowsAffected;
		}

		public static SessionDS GetEmptyReferenceDS(int iNumRows)
		{	
			DataTable dtReference = new DataTable(); 
			dtReference.Columns.Add(new DataColumn("snd_rec_name", typeof(string)));
			dtReference.Columns.Add(new DataColumn("snd_rec_type", typeof(string)));
			dtReference.Columns.Add(new DataColumn("contact_person", typeof(string)));
			dtReference.Columns.Add(new DataColumn("email", typeof(string)));
			dtReference.Columns.Add(new DataColumn("address1", typeof(string)));
			dtReference.Columns.Add(new DataColumn("address2", typeof(string)));
			dtReference.Columns.Add(new DataColumn("country", typeof(string)));
			dtReference.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtReference.Columns.Add(new DataColumn("telephone", typeof(string)));
			dtReference.Columns.Add(new DataColumn("fax", typeof(string)));
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtReference.NewRow();
				dtReference.Rows.Add(drEach);
			}
			DataSet dsReference = new DataSet();
			dsReference.Tables.Add(dtReference);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsReference;
			sessionDS.DataSetRecSize = dsReference.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return  sessionDS;
	
		}

		//Mehtods for CP - Status Code module

		public static SessionDS GetEmptyCPStatusCodeDS(int iNumRows)
		{
			DataTable dtStatusCode = new DataTable(); 
			dtStatusCode.Columns.Add(new DataColumn("status_code", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("status_description", typeof(string)));
			dtStatusCode.Columns.Add(new DataColumn("surcharge", typeof(decimal)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtStatusCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";

				dtStatusCode.Rows.Add(drEach);
			}

			DataSet dsStatusCode = new DataSet();
			dsStatusCode.Tables.Add(dtStatusCode);

			dsStatusCode.Tables[0].Columns["status_code"].Unique = true; //Checks duplicate records..
			dsStatusCode.Tables[0].Columns["status_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsStatusCode;
			sessionDS.DataSetRecSize = dsStatusCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return  sessionDS;
	
		}

		public static void AddNewRowInCPStatusCodeDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			
			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","AddNewRowInCPStatusCodeDS","R005",ex.Message.ToString());
			}
			sessionDS.QueryResultMaxSize++;
		}

		public static SessionDS GetCPStatusCodeDS(String strAppID, String strEnterpriseID,  String strAgentID , int iCurrent, int iDSRecSize)
		{

			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("select csc.status_code as status_code, sc.status_description as status_description, csc.surcharge as surcharge from Agent_Status_Charge csc, Status_Code sc ");
			strBuilder.Append(" where csc.applicationid = sc.applicationid and csc.enterpriseid = sc.enterpriseid and csc.status_code = sc.status_code and csc.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and csc.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and csc.agentid = '");
			strBuilder.Append(strAgentID);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"StatusCodeTable");
			return  sessionDS;
	
		}

		public static int AddCPStatusCodeDS(String strAppID, String strEnterpriseID, String strAgentID,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","AddCPStatusCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Agent_Status_Charge (applicationid,enterpriseid,agentid,status_code,surcharge) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(strAgentID);
			strBuilder.Append("',");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];

			if((drEach["status_code"]!= null) && (!drEach["status_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strStatusCode = (String) drEach["status_code"];
				strBuilder.Append("'");
				strBuilder.Append(strStatusCode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
				
			strBuilder.Append(",");

			if((drEach["surcharge"]!= null) && (!drEach["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decSurcharge = (decimal) drEach["surcharge"];
			
				strBuilder.Append(decSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(")");			
			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);		

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentDataMgrDAL","AddCPStatusCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddCPStatusCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}



			return iRowsAffected;

		}

		public static int ModifyCPStatusCodeDS(String strAppID, String strEnterpriseID, String strAgentID,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","ModifyCPStatusCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Agent_Status_Charge set surcharge = ");//, status_close, invoiceable,system_code) values ('");
			if((drEach["surcharge"]!= null) && (!drEach["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decSurcharge = (decimal) drEach["surcharge"];
				strBuilder.Append(decSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and agentid = '");
			strBuilder.Append(strAgentID);
			strBuilder.Append("' and status_code = '");
			String strStatusCode = (String) drEach["status_code"];
			strBuilder.Append(strStatusCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentDataMgrDAL","AddStatusCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddStatusCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}



			return iRowsAffected;

		}

		public static int DeleteCPStatusCodeDS(String strAppID, String strEnterpriseID, String strAgentID,String strStatusCode)
		{
			int iRowsDeleted = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

	
			//Get App dbConnection

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","DeleteCPStatusCodeDS","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Begin Core and App Transaction

			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","DeleteCPStatusCodeDS","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("AgentDataMgrDAL","DeleteCPStatusCodeDS","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("AgentDataMgrDAL","DeleteCPStatusCodeDS","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("AgentDataMgrDAL","DeleteCPStatusCodeDS","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction",null);
			}
			try
			{

				//Update record into core_enterprise Table

				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Agent_Exception_Charge where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and agentid = '");
				strBuilder.Append(strAgentID);
				strBuilder.Append("' and status_code = '");
				strBuilder.Append(strStatusCode);
				strBuilder.Append("'");


				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteStatusCodeDS","INF001",iRowsDeleted + " rows deleted from Exception_Code table");


				strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Agent_Status_Charge where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and agentid = '");
				strBuilder.Append(strAgentID);
				strBuilder.Append("' and status_code = '");
				strBuilder.Append(strStatusCode);
				strBuilder.Append("'");

				strSQLQuery = strBuilder.ToString();

				dbCommandApp.CommandText = strSQLQuery;


				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteCPStatusCodeDS","INF001",iRowsDeleted + " rows deleted from Status_Code table");
			
				//Commit application and core transaction.

				transactionApp.Commit();
				Logger.LogTraceInfo("AgentDataMgrDAL","DeleteCPStatusCodeDS","SDM003","App db delete transaction committed.");

			}
			catch(ApplicationException appException)
			{
				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("AgentDataMgrDAL","DeleteCPStatusCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("AgentDataMgrDAL","DeleteCPStatusCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("AgentDataMgrDAL","DeleteCPStatusCodeDS","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("AgentDataMgrDAL","DeleteCPStatusCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("AgentDataMgrDAL","DeleteCPStatusCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("AgentDataMgrDAL","DeleteCPStatusCodeDS","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				dbCommandApp.Dispose();
				transactionApp.Dispose();
				conApp.Dispose();
				
			}
			
			return iRowsDeleted;
		}

		/*CP Exception Code Dataset*/

		public static SessionDS GetEmptyCPExceptionCodeDS(int iNumRows)
		{
			DataTable dtExceptionCode = new DataTable(); 
			dtExceptionCode.Columns.Add(new DataColumn("exception_code", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("exception_description", typeof(string)));
			dtExceptionCode.Columns.Add(new DataColumn("surcharge", typeof(decimal)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtExceptionCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";

				dtExceptionCode.Rows.Add(drEach);
			}
			DataSet dsExceptionCode = new DataSet();
			dsExceptionCode.Tables.Add(dtExceptionCode);

			dsExceptionCode.Tables[0].Columns["exception_code"].Unique = true; //Checks duplicate records..
			dsExceptionCode.Tables[0].Columns["exception_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsExceptionCode;
			sessionDS.DataSetRecSize = dsExceptionCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return  sessionDS;
		}

		public static void AddNewRowInCPExceptionCodeDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();
			drNew[0] = "";
			drNew[1] = "";

			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","AddNewRowInCPExceptionCodeDS","R005",ex.Message.ToString());
			}
			
			sessionDS.QueryResultMaxSize++;
		}
		
		public static SessionDS GetCPExceptionCodeDS(String strAppID, String strEnterpriseID,String strAgentID,String strStatusCode,int iCurrent, int iDSRecSize)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			//			String strSQLQuery = "select exception_code, exception_description, surcharge from Customer_Status_Charge where ";
			//			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and status_code = '"+strStatusCode+"'"; 

			StringBuilder strBuilder = new StringBuilder();

			strBuilder.Append("select cec.exception_code as exception_code, ec.exception_description as exception_description, cec.surcharge as surcharge from Agent_Exception_Charge cec, Exception_Code ec ");
			strBuilder.Append(" where cec.applicationid = ec.applicationid and cec.enterpriseid = ec.enterpriseid and cec.exception_code = ec.exception_code and cec.applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and cec.enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and cec.Agentid = '");
			strBuilder.Append(strAgentID);
			strBuilder.Append("' and cec.status_code = '");
			strBuilder.Append(strStatusCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();			
			//dsExceptionCode = (DataSet)dbCon.ExecuteQuery(strSQLQuery,ReturnType.DataSetType);
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ExceptionCodeTable");
			return  sessionDS;
	
		}

		public static int AddCPExceptionCodeDS(String strAppID, String strEnterpriseID,String strAgentID,String strStatusCode,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","AddCPExceptionCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Agent_Exception_Charge (applicationid,enterpriseid,Agentid,status_code,exception_code,surcharge) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(strAgentID);
			strBuilder.Append("','");
			strBuilder.Append(strStatusCode);
			strBuilder.Append("',");


			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];

			if((drEach["exception_code"]!= null) && (!drEach["exception_code"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				String strExceptionCode = (String) drEach["exception_code"];
				strBuilder.Append("'");
				strBuilder.Append(strExceptionCode);
				strBuilder.Append("'");
			}
			else
			{
				strBuilder.Append("null");
			}
				
			strBuilder.Append(",");

			if((drEach["surcharge"]!= null) && (!drEach["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decSurcharge = (decimal) drEach["surcharge"];
			
				strBuilder.Append(decSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(")");			
			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","AddCPExceptionCodeDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddCPExceptionCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}

			return iRowsAffected;

		}

		public static int ModifyCPExceptionCodeDS(String strAppID, String strEnterpriseID,String strAgentID,String strStatusCode,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","ModifyCPExceptionCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();			
			strBuilder.Append("update Agent_Exception_Charge set surcharge = ");
			if((drEach["surcharge"]!= null) && (!drEach["surcharge"].GetType().Equals(System.Type.GetType("System.DBNull"))))
			{
				decimal decSurcharge = (decimal) drEach["surcharge"];
				strBuilder.Append(decSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and Agentid = '");
			strBuilder.Append(strAgentID);
			strBuilder.Append("' and status_code = '");
			strBuilder.Append(strStatusCode);
			strBuilder.Append("' and exception_code = '");
			String strExceptionCode = (String) drEach["exception_code"];
			strBuilder.Append(strExceptionCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);		

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","ModifyCPExceptionCodeDS","SDM001",iRowsAffected+" rows inserted in to Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyCPExceptionCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}

			return iRowsAffected;
		}

		public static int DeleteCPExceptionCodeDS(String strAppID, String strEnterpriseID,String strAgentID,String strStatusCode,String strExceptionCode)
		{
			int iRowsDeleted = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteCPExceptionCodeDS","EDB101","DbConnection object is null!!");
				return iRowsDeleted;
			}


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("Delete from Agent_Exception_Charge where ");
			strBuilder.Append(" applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and Agentid = '");
			strBuilder.Append(strAgentID);
			strBuilder.Append("' and status_code = '");
			strBuilder.Append(strStatusCode);
			strBuilder.Append("' and exception_code = '");
			strBuilder.Append(strExceptionCode);
			strBuilder.Append("'");

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);			
			try
			{
				iRowsDeleted = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","DeleteCPExceptionCodeDS","SDM001",iRowsDeleted+" rows deleted from  Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteCPExceptionCodeDS","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code : "+appException.Message,appException);
			}

			return iRowsDeleted;		
		}


		// 12/11/2002 Quotation Tab Page Methods

		public static SessionDS QueryQuotation(String strAppID, String strEnterpriseID, String strAgentID, int recStart, int recSize)
		{
			
			SessionDS sdsBaseTables = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);		
			String strSQLQuery = "select Quotation_No, Quotation_Version, Quotation_Date,Quotation_Status from Agent_Quotation where ";			
			strSQLQuery += "applicationid = '"+strAppID+"' and enterpriseid = '"+strEnterpriseID+"' and Agentid = '"+strAgentID+"'"; 						
			sdsBaseTables = (SessionDS)dbCon.ExecuteQuery(strSQLQuery,recStart, recSize,"Quotation");
			return  sdsBaseTables;
		}


		public static SessionDS GetEmptyQuotation(int iNumRows)
		{
			DataTable dtQuotation = new DataTable();
 
			dtQuotation.Columns.Add(new DataColumn("Quotation_No", typeof(string)));
			dtQuotation.Columns.Add(new DataColumn("Quotation_Version", typeof(string)));
			dtQuotation.Columns.Add(new DataColumn("Quotation_Date", typeof(string)));
			dtQuotation.Columns.Add(new DataColumn("Quotation_Status", typeof(string)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtQuotation.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";
				drEach[3] = "";

				dtQuotation.Rows.Add(drEach);
			}
			DataSet dsQuotation = new DataSet();
			SessionDS sessionds = new SessionDS();
			dsQuotation.Tables.Add(dtQuotation);

			dsQuotation.Tables[0].Columns["Quotation_No"].Unique = true; //Checks duplicate records..
			dsQuotation.Tables[0].Columns["Quotation_No"].AllowDBNull = false;

			sessionds.ds = dsQuotation;

			sessionds.DataSetRecSize = dsQuotation.Tables[0].Rows.Count;
			sessionds.QueryResultMaxSize = sessionds.DataSetRecSize;
			return  sessionds;
	
		}

		/// Agent Zone and zip methods , nova
		public static SessionDS GetEmptyAgentZoneDS(int iNumRows)
		{
			DataTable dtAgentZipCode = new DataTable(); 
			dtAgentZipCode.Columns.Add(new DataColumn("zone_code", typeof(string)));
			dtAgentZipCode.Columns.Add(new DataColumn("zone_description", typeof(string)));
			
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtAgentZipCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				dtAgentZipCode.Rows.Add(drEach);
			}

			DataSet dsAgentZipCode = new DataSet();
			dsAgentZipCode.Tables.Add(dtAgentZipCode);			

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsAgentZipCode;
			sessionDS.DataSetRecSize = dsAgentZipCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return  sessionDS;	
		}

		public static SessionDS GetEmptyAgentZoneZipDS(int iNumRows)
		{
			DataTable dtAgentCost = new DataTable(); 
			dtAgentCost.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtAgentCost.Columns.Add(new DataColumn("state_name", typeof(string)));
			dtAgentCost.Columns.Add(new DataColumn("country", typeof(string)));
			dtAgentCost.Columns.Add(new DataColumn("esa_surcharge", typeof(decimal)));
			
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtAgentCost.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";
				drEach[3] = 0;
				dtAgentCost.Rows.Add(drEach);
			}

			DataSet dsAgentCost = new DataSet();
			dsAgentCost.Tables.Add(dtAgentCost);
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsAgentCost;
			sessionDS.DataSetRecSize = dsAgentCost.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return  sessionDS;	
		}

		public static void AddNewRowInAgentZoneDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();
			drNew[0] = "";
			drNew[1] = "";			
			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}
			sessionDS.QueryResultMaxSize++;
		}

		public static void AddNewRowInAgentZoneZipDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();
			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";	
			drNew[3] = 0;
			
			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}
			sessionDS.QueryResultMaxSize++;
		}
		
		public static int DeleteAgentZoneDS(String strAppID, String strEnterpriseID,String strAgentID,String strZoneCode)
		{
			int iRowsDeleted = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;
			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;	
			//Get App dbConnection
			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbConApp == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteAgentZipCodeDS","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			//Begin Core and App Transaction
			conApp = dbConApp.GetConnection();			
			if(conApp == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteAgentZipCodeDS","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteAgentZipCodeDS","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("AgentProfileMgrDAL","DeleteAgentZipCodeDS","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}
			
			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);
			if(transactionApp == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteAgentZipCodeDS","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction",null);
			}
			try
			{
				//Update record into core_enterprise Table
				StringBuilder strBuilder = new StringBuilder();			
				strBuilder.Append("Delete from Agent_zone_zipcode where applicationid = '"+strAppID);
				strBuilder.Append("' and enterpriseid = '"+strEnterpriseID+"' and zone_code ='");
				strBuilder.Append(strZoneCode+"' and AgentID='"+strAgentID+"'");				
				String strSQLQuery = strBuilder.ToString();
				
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time				
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;
				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteAgentZipCodeDS","INF001",iRowsDeleted + " rows deleted from Agent_cost table");

				strBuilder = new StringBuilder();			
				strBuilder.Append("Delete from Agent_Zone where applicationid ='"+strAppID);
				strBuilder.Append("' and enterpriseid ='"+strEnterpriseID+"' and AgentID ='"+strAgentID);
				strBuilder.Append("' and zone_code='"+strZoneCode+"'");
				strSQLQuery = strBuilder.ToString();
				dbCommandApp.CommandText = strSQLQuery;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteAgentZipCodeDS","INF001",iRowsDeleted + " rows deleted from Agent_Zipcode table");
			
				//Commit application and core transaction.
				transactionApp.Commit();
				Logger.LogTraceInfo("AgentProfileMgrDAL","DeleteAgentZipCodeDS","SDM003","App db delete transaction committed.");

			}
			catch(ApplicationException appException)
			{
				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("AgentProfileMgrDAL","DeleteAgentZipCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("AgentProfileMgrDAL","DeleteAgentZipCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("AgentProfileMgrDAL","DeleteAgentZipCodeDS","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("AgentProfileMgrDAL","DeleteAgentZipCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("AgentProfileMgrDAL","DeleteAgentZipCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("AgentProfileMgrDAL","DeleteAgentZipCodeDS","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsDeleted;
		}

		public static int DeleteAgentZoneZipDS(String strAppID, String strEnterpriseID,String strAgentID, String strZone,String strZipCode,String strCountry)
		{
			int iRowsDeleted = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteAgentCostDS","EDB101","DbConnection object is null!!");
				return iRowsDeleted;
			}
			
			StringBuilder strBuilder = new StringBuilder();			
			strBuilder.Append("delete from agent_zone_zipcode where applicationid ='"+strAppID+"'");
			strBuilder.Append(" and enterpriseid ='"+strEnterpriseID+"' and "); 
			strBuilder.Append(" zone_code ='"+strZone+"' and country='"+strCountry+"' and zipcode='"+strZipCode+"' and AgentId='"+ strAgentID+"'");
			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsDeleted = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","DeleteAgentCostDS","SDM001",iRowsDeleted+" rows deleted from  Agent_Cost table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","DeleteAgentCostDS","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Status Code "+appException.Message,appException);
			}

			return iRowsDeleted;		
		}

        public static int ModifyAgentZoneDS(String strAppID, String strEnterpriseID, String strAgentID, DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","ModifyAgentZipCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];
			String strZonecode = (String) drEach["zone_code"];
			String strZonedescription = (String) drEach["zone_description"];;

			StringBuilder strBuilder = new StringBuilder();			
			strBuilder.Append(" update Agent_Zone set zone_description = N'"+strZonedescription);
			strBuilder.Append("' ");			
			strBuilder.Append(" where applicationid = '"+strAppID);
			strBuilder.Append("' and enterpriseid = '"+strEnterpriseID);
			strBuilder.Append("' and zone_code = '"+strZonecode);
			strBuilder.Append("' and AgentID='"+strAgentID+"'");

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);	

			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","ModifyAgentZipCodeDS","SDM001",iRowsAffected+" rows inserted in to Agent_Zipcode table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","ModifyAgentZipCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Agent Zip Code ",appException);
			}

			return iRowsAffected;
		}

		public static int ModifyAgentCostDS(String strAppID, String strEnterpriseID, String strAgentID, String strZipCode, DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","ModifyAgentCostDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];
			Decimal dStartPrice = (Decimal) drEach["start_price"];
			Decimal dIncrementPrice = (Decimal) drEach["Increment_Price"];			
			Decimal dStartWt = (Decimal) drEach["start_wt"];
			Decimal dEndWt = (Decimal) drEach["end_wt"];			

			StringBuilder strBuilder = new StringBuilder();			
			strBuilder.Append("update Agent_Cost set start_price = "+dStartPrice+", increment_price =");			
			strBuilder.Append(dIncrementPrice+" where applicationid = '"+strAppID);
			strBuilder.Append("' and enterpriseid = '"+strEnterpriseID);
			strBuilder.Append("' and ZipCode = '"+strZipCode);
			strBuilder.Append("' and Start_Wt = "+dStartWt+" and End_Wt="+dEndWt+" and AgentId='"+strAgentID+"'");
			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","ModifyAgentCostDS","SDM001",iRowsAffected+" rows updated in to Agent_Cost table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","ModifyAgentCostDS","SDM001","Error During update "+ appException.Message.ToString());
				throw new ApplicationException("Error updating Zip Code ",appException);
			}

			return iRowsAffected;
		}
		public static int AddAgentZoneDS(String strAppID, String strEnterpriseID, String strAgentID, DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","AddAgentZipCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;
			DataRow drEach = dsToInsert.Tables[0].Rows[0];
	
			String strZoneCode = (String) drEach["zone_code"];
			String strZoneDescription = (String) drEach["zone_description"];		
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Agent_Zone (applicationid,enterpriseid,AgentID,zone_code,zone_description");
			strBuilder.Append(" ) values ('"+strAppID+"','"+strEnterpriseID+"'");
			strBuilder.Append(",'"+strAgentID+"','"+strZoneCode+"',N'"+strZoneDescription+"')");			
			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","AddAgentZipCodeDS","SDM001",iRowsAffected+" rows inserted in to Agent_ZipCode table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","AddAgentZipCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Agent_ZipCode ",appException);
			}
			return iRowsAffected;
		}

		public static int AddAgentCostDS(String strAppID, String strEnterpriseID,String strAgentID,String strZipCode, DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","AddAgentCostDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;
			DataRow drEach = dsToInsert.Tables[0].Rows[0];
//			String strAgentID = (String) drEach["AgentID"];			
//			String strZipCode = (String) drEach["ZipCode"];
			Decimal dStartWt = (Decimal) drEach["Start_Wt"];			
			Decimal dEndWt = (Decimal) drEach["End_Wt"];
			Decimal dStartPrice = (Decimal) drEach["Start_Price"];			
			Decimal dIncrementPrice = (Decimal) drEach["Increment_Price"];

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Agent_Cost (applicationid,enterpriseid,AgentID, ZipCode,Start_Wt,End_Wt");
			strBuilder.Append(",Start_Price,Increment_Price) values ('"+strAppID+"','"+strEnterpriseID+"'");
			strBuilder.Append(",'"+strAgentID+"','"+strZipCode+"',"+dStartWt+","+dEndWt+","+dStartPrice+","+dIncrementPrice+")");			
			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);	
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","AddAgentCostDS","SDM001",iRowsAffected+" rows inserted in to Agent_Cost table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","AddAgentCostDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Agent_Cost ",appException);
			}
			return iRowsAffected;

		}

		public static SessionDS GetAgentZoneDS(String strAppID, String strEnterpriseID, String strAgentID, int iCurrent,int iDSRecSize)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","GetAgentZipCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select zone_code,zone_description from Agent_Zone ");
			strBuilder.Append(" where applicationid = '"+strAppID+"' and enterpriseid ='"+strEnterpriseID+"'");
			strBuilder.Append(" and AgentID='"+ strAgentID +"'");
			String strSQLQuery = strBuilder.ToString();						
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent , iDSRecSize,"GetAgentZipCodeDS");
			return  sessionDS;
	
		}

		public static SessionDS GetAgentZoneZipDS(String strAppID, String strEnterpriseID, String strAgentID, String strZoneCode, int iCurrent ,int iDSRecSize)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","GetAgentCostDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" Select S.State_Name ,AZZ.Zipcode, AZZ.Country, AZZ.ESA_surcharge from State S inner join  agent_zone_zipcode AZZ on S.Applicationid=AZZ.Applicationid ");
			strBuilder.Append(" and  S.EnterpriseID=AZZ.EnterpriseID and  S.state_Code=AZZ.State_Code and S.Country=AZZ.Country where AZZ.Zone_Code='"+strZoneCode+"' ");
			strBuilder.Append(" and AZZ.applicationid = '"+strAppID+"' and AZZ.enterpriseid ='"+strEnterpriseID+"'");		
			String strSQLQuery = strBuilder.ToString();				
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"GetAgentCostDS");
			return  sessionDS;
	
		}		
		public static int ModifyAgentZipDS(String strAppID, String strEnterpriseID, String strAgentID, String strZoneCode, DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","ModifyAgentZipCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];
			String strZipcode	= (String) drEach["zipcode"];
			String strStateName = (String) drEach["state_name"];
			String strCountry	= (String) drEach["country"];
			decimal strESA		 = (decimal) drEach["esa_surcharge"];

			StringBuilder strBuilder = new StringBuilder();			
			strBuilder.Append(" update Agent_Zone_Zipcode set esa_surcharge = "+strESA);
			strBuilder.Append(" ");			
			strBuilder.Append(" where applicationid = '"+strAppID);
			strBuilder.Append("' and enterpriseid = '"+strEnterpriseID);
			strBuilder.Append("' and zone_code = '"+strZoneCode);
			strBuilder.Append("' and AgentID='"+strAgentID);
			strBuilder.Append("' and zipcode='"+strZipcode+"'");

			String strSQLQuery = strBuilder.ToString();
			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);	

			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("AgentProfileMgrDAL","ModifyAgentZoneZipDS","SDM001",iRowsAffected+" rows inserted in to Agent_Zipcode table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("AgentProfileMgrDAL","ModifyAgentZipCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Agent Zip Code ",appException);
			}
			return iRowsAffected;
		}
		/// VAS for agent
		public static SessionDS GetAgentVASCodeDS(String strAppID, String strEnterpriseID,String strAgentID, int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{
			SessionDS sessionDS = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetVASCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select vas_code, vas_description, surcharge  from Agent_VAS where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and agentid= '");
			strBuilder.Append(strAgentID+"'");
//			DataRow drEach = dsQueryParam.Tables[0].Rows[0];				
//			String strVASCode = (String) drEach["vas_code"];
//			if((strVASCode != null) && (strVASCode != ""))
//			{
//				strBuilder.Append(" and vas_code like '%");
//				strBuilder.Append(strVASCode);
//				strBuilder.Append("%' ");
//			}
//			String strVASDescription = (String) drEach["vas_description"];
//			if((strVASDescription != null) && (strVASDescription != ""))
//			{
//				strBuilder.Append(" and vas_description like '%");
//				strBuilder.Append(Utility.ReplaceSingleQuote(strVASDescription));
//				strBuilder.Append("%' ");
//
//			}
//
//			if(Utility.IsNotDBNull(drEach["surcharge"]) && drEach["surcharge"].ToString()!="")
//			{
//				decimal decSurcharge = (decimal) drEach["surcharge"];
//
//				strBuilder.Append(" and surcharge = ");
//				strBuilder.Append(decSurcharge);
//				strBuilder.Append("  ");
//			}



			String strSQLQuery = strBuilder.ToString();
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"VASCodeTable");

			return  sessionDS;
	
		}

		public static int AddAgentVASCodeDS(String strAppID, String strEnterpriseID,String strAgentID,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetVASCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Agent_VAS (applicationid,enterpriseid,agentid,vas_code,vas_description, surcharge) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");

			strBuilder.Append(strAgentID);
			strBuilder.Append("','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			String strVASCode = (String) drEach["vas_code"];
			strBuilder.Append(strVASCode);
			strBuilder.Append("',N'");

			String strVASDescription = (String) drEach["vas_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strVASDescription));
			strBuilder.Append("',");
			
			if(Utility.IsNotDBNull(drEach["surcharge"]) &&  drEach["surcharge"].ToString() !="")
			{
				decimal decSurcharge = (decimal) drEach["surcharge"];			
				strBuilder.Append(decSurcharge);
				strBuilder.Append(")");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(")");

			}

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddVASCodeDS","SDM001",iRowsAffected+" rows inserted in to VAS table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddVASCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting VAS Code ",appException);
			}



			return iRowsAffected;

		}

		public static int ModifyAgentVASCodeDS(String strAppID, String strEnterpriseID,String strAgentID,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetVASCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];

			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Agent_VAS set ");
			strBuilder.Append("vas_description = N'");
			String strVASDescription = (String) drEach["vas_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strVASDescription));
			strBuilder.Append("', surcharge = ");

			if(Utility.IsNotDBNull(drEach["surcharge"]) && drEach["surcharge"].ToString()!="")
			{
				decimal decSurcharge = (decimal) drEach["surcharge"];
				strBuilder.Append(decSurcharge);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and vas_code = '");
			String strVASCode = (String) drEach["vas_code"];
			strBuilder.Append(strVASCode);
			strBuilder.Append("' and agentid = '");
			strBuilder.Append(strAgentID);
			strBuilder.Append("'");
			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddVASCodeDS","SDM001",iRowsAffected+" rows inserted in to VAS_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddVASCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting VAS Code ",appException);
			}
			return iRowsAffected;

		}

		public static SessionDS GetEmptyVASCodeDS(int iNumRows)
		{
			
			DataTable dtVASCode = new DataTable(); 
			dtVASCode.Columns.Add(new DataColumn("vas_code", typeof(string)));
			dtVASCode.Columns.Add(new DataColumn("vas_description", typeof(string)));
			dtVASCode.Columns.Add(new DataColumn("surcharge", typeof(decimal)));


			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtVASCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";

				dtVASCode.Rows.Add(drEach);
			}

			DataSet dsVASCode = new DataSet();
			dsVASCode.Tables.Add(dtVASCode);

			dsVASCode.Tables[0].Columns["vas_code"].Unique = true; //Checks duplicate records..
			dsVASCode.Tables[0].Columns["vas_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsVASCode;
			sessionDS.DataSetRecSize = dsVASCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}

		public static void AddNewRowInAgentVASCodeDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";

			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}
			
			sessionDS.QueryResultMaxSize++;
		}

		public static int DeleteAgentVASCodeDS(String strAppID, String strEnterpriseID,String strAgentID,String strVASCode)
		{
			int iRowsDeleted = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

	
			//Get App dbConnection

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Begin Core and App Transaction

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}


			try
			{

				//Update record into core_enterprise Table

				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Agent_VAS_Excluded where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and vas_code = '");
				strBuilder.Append(strVASCode);
				strBuilder.Append("' and agentid = '");
				strBuilder.Append(strAgentID);
				strBuilder.Append("'");


				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteVASCodeDS","INF001",iRowsDeleted + " rows deleted from Exception_Code table");


				strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Agent_VAS where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and vas_code = '");
				strBuilder.Append(strVASCode);
				strBuilder.Append("'");

				strSQLQuery = strBuilder.ToString();

				dbCommandApp.CommandText = strSQLQuery;


				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteVASCodeDS","INF001",iRowsDeleted + " rows deleted from VAS table");
			
				//Commit application and core transaction.

				transactionApp.Commit();
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteVASCodeDS","SDM003","App db delete transaction committed.");

			}
			catch(ApplicationException appException)
			{
				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteVASCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting VAS Code "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteVASCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteVASCodeDS","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting VAS Code "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsDeleted;
		}

		public static SessionDS GetAgentZipcodeVASExcludedDS(String strAppID, String strEnterpriseID,String strAgentId,String strVASCode,int iCurrent, int iDSRecSize)
		{
			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			String strSQLQuery = "select zvase.zipcode as zipcode, s.state_name as state_name, z.country as country from Agent_VAS_Excluded zvase, Zipcode z, State s where ";
			strSQLQuery += " zvase.applicationid = z.applicationid and zvase.enterpriseid = z.enterpriseid and zvase.zipcode = z.zipcode and ";
			strSQLQuery += " z.applicationid = s.applicationid and z.enterpriseid = s.enterpriseid and z.country = s.country and z.state_code = s.state_code and ";
			strSQLQuery += " zvase.applicationid = '"+strAppID+"' and zvase.enterpriseid = '"+strEnterpriseID+"' and zvase.vas_code = '"+strVASCode+"' and zvase.agentid ='"+strAgentId+"' and zvase.country = s.country and zvase.state_code = s.state_code"; 
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ZipcodeVASExcludedTable");

			return  sessionDS;
	
		}
		public static int DeleteAgentZipcodeVASExcludedDS(String strAppID, String strEnterpriseID,String strAgentID,String strVASCode,String strZipcode)
		{
			int iRowsDeleted = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteExceptionCodeRecord","EDB101","DbConnection object is null!!");
				return iRowsDeleted;
			}


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("Delete from Agent_VAS_Excluded where ");
			strBuilder.Append(" applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and vas_code = '");
			strBuilder.Append(strVASCode);
			strBuilder.Append("' and agentid = '");
			strBuilder.Append(strAgentID);
			strBuilder.Append("' and zipcode = '");
			strBuilder.Append(strZipcode);
			strBuilder.Append("'");
			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsDeleted = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteExceptionCodeRecord","SDM001",iRowsDeleted+" rows deleted from  Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteExceptionCodeRecord","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code : "+appException.Message,appException);
			}

			return iRowsDeleted;		
		}
		#region service & service-zipcode excluded for agent
		//Methods for Service Code
		public static SessionDS GetAgentServiceCodeDS(String strAppID, String strEnterpriseID,String strAgentID,int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{

			SessionDS sessionDS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetServiceCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select service_code, service_description, service_charge_percent, service_charge_amt,commit_time, transit_day,transit_hour from Agent_Service where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and agentid = '");
			strBuilder.Append(strAgentID);
			strBuilder.Append("' ");

			DataRow drEach = dsQueryParam.Tables[0].Rows[0];
				
			String strServiceCode = (String) drEach["service_code"];
			if((strServiceCode != null) && (strServiceCode != ""))
			{
				strBuilder.Append(" and service_code like '%");
				strBuilder.Append(strServiceCode);
				strBuilder.Append("%' ");

			}

			String strServiceDescription = (String) drEach["service_description"];
			if((strServiceDescription != null) && (strServiceDescription != ""))
			{
				strBuilder.Append(" and service_description like N'%");
				strBuilder.Append(Utility.ReplaceSingleQuote(strServiceDescription));
				strBuilder.Append("%' ");
			}

//			if(Utility.IsNotDBNull(drEach["service_charge_percent"]) && drEach["service_charge_percent"].ToString() !="")
//			{
//				decimal decPercentDiscount = (decimal) drEach["service_charge_percent"];
//				strBuilder.Append(" and service_charge_percent = ");
//				strBuilder.Append(decPercentDiscount);
//				strBuilder.Append("  ");
//			}
//
//			if(Utility.IsNotDBNull(drEach["service_charge_amt"]) && drEach["service_charge_amt"].ToString() !="")
//			{
//				decimal decPercentDiscount = (decimal) drEach["service_charge_amt"];
//				strBuilder.Append(" and service_charge_amt = ");
//				strBuilder.Append(decPercentDiscount);
//				strBuilder.Append("  ");
//			}

			if(Utility.IsNotDBNull(drEach["commit_time"]) && drEach["commit_time"].ToString()!="")
			{
				DateTime dtCommitTime = (DateTime) drEach["commit_time"];
				strBuilder.Append(" and commit_time = ");
				String strTime = Utility.DateFormat(strAppID,strEnterpriseID,dtCommitTime,DTFormat.Time);
				strBuilder.Append(strTime);
				strBuilder.Append(" ");
			}

			if(Utility.IsNotDBNull(drEach["transit_day"]) && drEach["transit_day"].ToString()!="")
			{
				int iTransitDay = System.Convert.ToInt32(drEach["transit_day"]);
				strBuilder.Append(" and transit_day = ");
				strBuilder.Append(iTransitDay);
				strBuilder.Append("  ");
			}

			if(Utility.IsNotDBNull(drEach["transit_hour"]) && drEach["transit_hour"].ToString()!="")
			{
				int iTransitHour = System.Convert.ToInt32(drEach["transit_hour"]);
				strBuilder.Append(" and transit_hour = ");
				strBuilder.Append(iTransitHour);
				strBuilder.Append("  ");
			}

			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ServiceCodeTable");

			return  sessionDS;
	
		}

		public static int AddAgentServiceCodeDS(String strAppID, String strEnterpriseID,String strAgentID,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetServiceCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Agent_Service (applicationid,enterpriseid,agentid,service_code, service_description, service_charge_percent,service_charge_amt, commit_time,transit_day,transit_hour) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(strAgentID);
			strBuilder.Append("','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			String strServiceCode = (String) drEach["service_code"];
			strBuilder.Append(strServiceCode);
			strBuilder.Append("',N'");

			String strServiceDescription = (String) drEach["service_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strServiceDescription));
			strBuilder.Append("',");

			if(Utility.IsNotDBNull(drEach["service_charge_percent"]) && drEach["service_charge_percent"].ToString() !="")
			{
				decimal decPercentDiscount = (decimal) drEach["service_charge_percent"];
				if(decPercentDiscount == 0)
					strBuilder.Append("null");
				else
					strBuilder.Append(decPercentDiscount);
				strBuilder.Append(",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");
			}

			if(Utility.IsNotDBNull(drEach["service_charge_amt"]) && drEach["service_charge_amt"].ToString() !="")
			{
				decimal decDollerDiscount = (decimal) drEach["service_charge_amt"];
				if(decDollerDiscount == 0)
					strBuilder.Append("null");
				else
					strBuilder.Append(decDollerDiscount);
				strBuilder.Append(",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");
			}

			if(Utility.IsNotDBNull(drEach["commit_time"]) && drEach["commit_time"].ToString()!="")
			{
				DateTime dtCommitTime = (DateTime) drEach["commit_time"];
				String strTime = Utility.DateFormat(strAppID,strEnterpriseID,dtCommitTime,DTFormat.Time);
				strBuilder.Append(strTime);
				strBuilder.Append(",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");
			}

			if(Utility.IsNotDBNull(drEach["transit_day"]) && drEach["transit_day"].ToString()!="")
			{
				int iTransitDay = System.Convert.ToInt32(drEach["transit_day"]);
				strBuilder.Append(iTransitDay);
				strBuilder.Append(",");
			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(",");
			}

			if(Utility.IsNotDBNull(drEach["transit_hour"]) && drEach["transit_hour"].ToString()!="")
			{
				int iTransitHour = System.Convert.ToInt32( drEach["transit_hour"]);
				strBuilder.Append(iTransitHour);
				strBuilder.Append(")");

			}
			else
			{
				strBuilder.Append("null");
				strBuilder.Append(")");
			}

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddServiceCodeDS","SDM001",iRowsAffected+" rows inserted in to Service table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddServiceCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Service Code ",appException);
			}



			return iRowsAffected;

		}

		public static int ModifyAgentServiceCodeDS(String strAppID, String strEnterpriseID,String strAgentID,DataSet dsToModify)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetServiceCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update Agent_Service set ");
			strBuilder.Append("service_description = N'");
			String strServiceDescription = (String) drEach["service_description"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strServiceDescription));
			strBuilder.Append("', service_charge_percent = ");

			if(Utility.IsNotDBNull(drEach["service_charge_percent"]) && drEach["service_charge_percent"].ToString()!="")
			{
				decimal decPercentDiscount = (decimal) drEach["service_charge_percent"];
				if(decPercentDiscount == 0 )
					strBuilder.Append("null");
				else
					strBuilder.Append(decPercentDiscount);
			}
			else
			{
				strBuilder.Append("null");
			}
			strBuilder.Append(", service_charge_amt = ");
			if(Utility.IsNotDBNull(drEach["service_charge_amt"]) && drEach["service_charge_amt"].ToString()!="")
			{
				decimal decDollerDiscount = (decimal) drEach["service_charge_amt"];
				if(decDollerDiscount == 0)
					strBuilder.Append("null");
				else
					strBuilder.Append(decDollerDiscount);
			}
			else
			{
				strBuilder.Append("null");
			}


			strBuilder.Append(", commit_time = ");

			if(Utility.IsNotDBNull(drEach["commit_time"]) && drEach["commit_time"].ToString() !="")
			{
				DateTime dtCommitTime = (DateTime) drEach["commit_time"];
				String strTime = Utility.DateFormat(strAppID,strEnterpriseID,dtCommitTime,DTFormat.Time);
				strBuilder.Append(strTime);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", transit_day = ");

			if(Utility.IsNotDBNull(drEach["transit_day"]) && drEach["transit_day"].ToString() !="")
			{
				int iTransitDay = System.Convert.ToInt32(drEach["transit_day"]);
				strBuilder.Append(iTransitDay);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(", transit_hour = ");

			if(Utility.IsNotDBNull(drEach["transit_hour"]) && drEach["transit_hour"].ToString() !="")
			{
				int iTransitHour = System.Convert.ToInt32(drEach["transit_hour"]);
				strBuilder.Append(iTransitHour);
			}
			else
			{
				strBuilder.Append("null");
			}

			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and service_code = '");
			String strServiceCode = (String) drEach["service_code"];
			strBuilder.Append(strServiceCode);
			strBuilder.Append("' and agentid='"+strAgentID+"'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddServiceCodeDS","SDM001",iRowsAffected+" rows inserted in to Service_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddServiceCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Service Code ",appException);
			}



			return iRowsAffected;

		}
		public static SessionDS GetAgentEmptyServiceCodeDS(int iNumRows)
		{
			

			DataTable dtServiceCode = new DataTable();
 
			dtServiceCode.Columns.Add(new DataColumn("service_code", typeof(string)));
			dtServiceCode.Columns.Add(new DataColumn("service_description", typeof(string)));
			dtServiceCode.Columns.Add(new DataColumn("service_charge_percent", typeof(decimal)));
			dtServiceCode.Columns.Add(new DataColumn("service_charge_amt", typeof(decimal)));
			dtServiceCode.Columns.Add(new DataColumn("commit_time", typeof(DateTime)));
			dtServiceCode.Columns.Add(new DataColumn("transit_day", typeof(int)));
			dtServiceCode.Columns.Add(new DataColumn("transit_hour", typeof(int)));


			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtServiceCode.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = 0;
				drEach[3] = 0;
				dtServiceCode.Rows.Add(drEach);
			}

			DataSet dsServiceCode = new DataSet();
			dsServiceCode.Tables.Add(dtServiceCode);

			dsServiceCode.Tables[0].Columns["service_code"].Unique = true; //Checks duplicate records..
			dsServiceCode.Tables[0].Columns["service_code"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsServiceCode;
			sessionDS.DataSetRecSize = dsServiceCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}

		public static void AddAgentNewRowInServiceCodeDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = 0;
			drNew[3] = 0;


			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}			
			sessionDS.QueryResultMaxSize++;

		}

		public static int DeleteAgentServiceCodeDS(String strAppID, String strEnterpriseID,String strAgentID,String strServiceCode)
		{
			int iRowsDeleted = 0;

			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

	
			//Get App dbConnection

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Begin Core and App Transaction

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}


			try
			{

				//Update record into core_enterprise Table

				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Agent_Service_Excluded where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and service_code = '");
				strBuilder.Append(strServiceCode);
				strBuilder.Append("' and agentid = '");
				strBuilder.Append(strAgentID);
				strBuilder.Append("'");


				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteServiceCodeDS","INF001",iRowsDeleted + " rows deleted from Exception_Code table");


				strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Agent_Service where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and service_code = '");
				strBuilder.Append(strServiceCode);
				strBuilder.Append("'");

				strSQLQuery = strBuilder.ToString();

				dbCommandApp.CommandText = strSQLQuery;


				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteServiceCodeDS","INF001",iRowsDeleted + " rows deleted from Service table");
			
				//Commit application and core transaction.

				transactionApp.Commit();
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteServiceCodeDS","SDM003","App db delete transaction committed.");

			}
			catch(ApplicationException appException)
			{
				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Service Code "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Service Code "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsDeleted;
		}

		///Zipcode-service code excluded methods..
		public static SessionDS GetAgentZipcodeServiceExcludedDS(String strAppID, String strEnterpriseID,String strAgentID,String strServiceCode,int iCurrent, int iDSRecSize)
		{
			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetStatusCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			String strSQLQuery = "select zse.zipcode as zipcode, s.state_name as state_name, z.country as country from Agent_Service_Excluded zse, Zipcode z, State s where ";
			strSQLQuery += " zse.applicationid = z.applicationid and zse.enterpriseid = z.enterpriseid and zse.zipcode = z.zipcode and ";
			strSQLQuery += " z.applicationid = s.applicationid and z.enterpriseid = s.enterpriseid and z.country = s.country and z.state_code = s.state_code and ";
			strSQLQuery += " zse.applicationid = '"+strAppID+"' and zse.enterpriseid = '"+strEnterpriseID+"' and zse.service_code = '"+strServiceCode+"' and zse.agentid='"+strAgentID+"' and zse.country = s.country and zse.state_code = s.state_code"; 
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ZipcodeServiceExcludedTable");

			return  sessionDS;
	
		}

		public static int AddAentZipcodeServiceExcludedDS(String strAppID, String strEnterpriseID,String strServiceCode,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","AddZipcodeServiceExcludedDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Zipcode_Service_Excluded (applicationid,enterpriseid,service_code, zipcode ) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(strServiceCode);
			strBuilder.Append("','");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			String strZipcode = (String) drEach["zipcode"];
			strBuilder.Append(strZipcode);
			strBuilder.Append("')");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{

				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddZipcodeServiceExcludedDS","SDM001",iRowsAffected+" rows inserted in to Status_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddZipcodeServiceExcludedDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}



			return iRowsAffected;

		}

		public static SessionDS GetAgentEmptyZipcodeServiceExcludedDS(int iNumRows)
		{
			DataTable dtZipcodeServiceExcluded = new DataTable();
 
			dtZipcodeServiceExcluded.Columns.Add(new DataColumn("zipcode", typeof(string)));
			dtZipcodeServiceExcluded.Columns.Add(new DataColumn("state_name", typeof(string)));
			dtZipcodeServiceExcluded.Columns.Add(new DataColumn("country", typeof(string)));

			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtZipcodeServiceExcluded.NewRow();
				drEach[0] = "";
				drEach[1] = "";
				drEach[2] = "";

				dtZipcodeServiceExcluded.Rows.Add(drEach);
			}

			DataSet dsZipcodeServiceExcluded = new DataSet();
			dsZipcodeServiceExcluded.Tables.Add(dtZipcodeServiceExcluded);

			dsZipcodeServiceExcluded.Tables[0].Columns["zipcode"].Unique = true; //Checks duplicate records..
			dsZipcodeServiceExcluded.Tables[0].Columns["zipcode"].AllowDBNull = false;

			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dsZipcodeServiceExcluded;
			sessionDS.DataSetRecSize = dsZipcodeServiceExcluded.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;


			return  sessionDS;
	
		}

		public static void AddAgentNewRowInZipcodeServiceExcludedDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = "";
			drNew[1] = "";
			drNew[2] = "";
			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}
			sessionDS.QueryResultMaxSize++;
		}

		public static int DeleteAgentZipcodeServiceExcludedDS(String strAppID, String strEnterpriseID,String strAgentID,String strServiceCode,String strZipcode)
		{
			int iRowsDeleted = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteExceptionCodeRecord","EDB101","DbConnection object is null!!");
				return iRowsDeleted;
			}

			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("Delete from Agent_Service_Excluded where ");
			strBuilder.Append(" applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and service_code = '");
			strBuilder.Append(strServiceCode);
			strBuilder.Append("' and zipcode = '");
			strBuilder.Append(strZipcode);
			strBuilder.Append("' and agentid='"+strAgentID+"'");

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsDeleted = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteExceptionCodeRecord","SDM001",iRowsDeleted+" rows deleted from  Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteExceptionCodeRecord","SDM001","Error During Delete "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code : "+appException.Message,appException);
			}

			return iRowsDeleted;		
		}
		public static int DeleteAgentCustomizedCodeDS(String strAppID, String strEnterpriseID,String strAgentID,String strServiceCode)
		{
			int iRowsDeleted = 0;
			DbConnection dbConApp = null;
			IDbCommand dbCommandApp = null;

			IDbTransaction transactionApp = null;
			IDbConnection conApp = null;

	
			//Get App dbConnection

			dbConApp = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbConApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","dbConApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			//Begin Core and App Transaction

			conApp = dbConApp.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbConApp.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM003","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM003","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			transactionApp = dbConApp.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}


			try
			{

				//Update record into core_enterprise Table

				StringBuilder strBuilder = new StringBuilder();
			
				strBuilder.Append("Delete from Agent_assembly where ");
				strBuilder.Append(" applicationid = '");
				strBuilder.Append(strAppID);
				strBuilder.Append("' and enterpriseid = '");
				strBuilder.Append(strEnterpriseID);
				strBuilder.Append("' and assemblyid = '");
				strBuilder.Append(strServiceCode);
				strBuilder.Append("' and agentid = '");
				strBuilder.Append(strAgentID);
				strBuilder.Append("'");


				String strSQLQuery = strBuilder.ToString();
				dbCommandApp = dbConApp.CreateCommand(strSQLQuery,CommandType.Text); //Creating the command first time
				dbCommandApp.Connection = conApp;
				dbCommandApp.Transaction = transactionApp;

				iRowsDeleted = dbConApp.ExecuteNonQueryInTransaction(dbCommandApp);
				Logger.LogTraceInfo("RBACManager","DeleteCustomizedCodeDS","INF001",iRowsDeleted + " rows deleted from agent_assembly/customer_assembly table");

			
				//Commit application and core transaction.

				transactionApp.Commit();
				Logger.LogTraceInfo("SysDataMgrDAL","DeleteServiceCodeDS","SDM003","App db delete transaction committed.");

			}
			catch(ApplicationException appException)
			{
				
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","Delete Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error deleting Service Code "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogTraceInfo("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","App db delete transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM002","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("SysDataMgrDAL","DeleteServiceCodeDS","SDM004","delete Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error deleting Service Code "+exception.Message,exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			
			return iRowsDeleted;
		}

		#endregion
		#region Customized code
		/*public static SessionDS GetAgentEmptyCustomizedCodeDS(int iNumRows)
		{
			DataTable dtCustomizedCode = new DataTable(); 
			
			dtCustomizedCode.Columns.Add(new DataColumn("code_text", typeof(string)));
			dtCustomizedCode.Columns.Add(new DataColumn("customized_assembly_code", typeof(decimal)));
			dtCustomizedCode.Columns.Add(new DataColumn("assemblyid", typeof(string)));
			dtCustomizedCode.Columns.Add(new DataColumn("Assembly_description", typeof(string)));
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtCustomizedCode.NewRow();
				drEach[0] = "";
				drEach[1] = 0;
				drEach[2] = "";
				drEach[3] = "";
				dtCustomizedCode.Rows.Add(drEach);
			}
			DataSet dgCustomizedCode = new DataSet();
			dgCustomizedCode.Tables.Add(dtCustomizedCode);

			dgCustomizedCode.Tables[0].Columns["assemblyid"].Unique = true; //Checks duplicate records..
			dgCustomizedCode.Tables[0].Columns["assemblyid"].AllowDBNull = false;
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dgCustomizedCode;
			sessionDS.DataSetRecSize = dgCustomizedCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;

			return  sessionDS;
	
		}*/
		
		public static SessionDS GetAgentEmptyCustomizedCodeDS(int iNumRows)
		{
			DataTable dtCustomizedCode = new DataTable(); 
			dtCustomizedCode.Columns.Add(new DataColumn("customized_assembly_code", typeof(decimal)));
			dtCustomizedCode.Columns.Add(new DataColumn("assemblyid", typeof(string)));
			dtCustomizedCode.Columns.Add(new DataColumn("assembly_description", typeof(string)));
			dtCustomizedCode.Columns.Add(new DataColumn("code_text", typeof(string)));
			dtCustomizedCode.Columns.Add(new DataColumn("code_num_value", typeof(string)));
			dtCustomizedCode.Columns.Add(new DataColumn("codeid", typeof(string)));
			for(int i=0; i < iNumRows; i++)
			{
				DataRow drEach = dtCustomizedCode.NewRow();
				drEach[0] = 0;
				drEach[1] = "";
				drEach[2] = "";
				drEach[3] = "";
				drEach[4] = "";
				drEach[5] = "";
				dtCustomizedCode.Rows.Add(drEach);
			}
			DataSet dgCustomizedCode = new DataSet();
			dgCustomizedCode.Tables.Add(dtCustomizedCode);
			dgCustomizedCode.Tables[0].Columns["assemblyid"].Unique = true; //Checks duplicate records..
			dgCustomizedCode.Tables[0].Columns["assemblyid"].AllowDBNull = false;
			SessionDS sessionDS = new SessionDS();
			sessionDS.ds = dgCustomizedCode;
			sessionDS.DataSetRecSize = dgCustomizedCode.Tables[0].Rows.Count;
			sessionDS.QueryResultMaxSize = sessionDS.DataSetRecSize;
			return  sessionDS;
		}

		public static SessionDS GetAgentCustomizedCodeDS(String strAppID, String strEnterpriseID,String strAgentID,int iCurrent, int iDSRecSize,DataSet dsQueryParam)
		{

			SessionDS sessionDS = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetServiceCodeDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append(" select distinct ag.customized_assembly_code,a.assemblyid,a.assembly_description,s.code_text,s.code_num_value, s.codeid from agent_assembly ag ,core_system_code s,assembly a ");
			strBuilder.Append(" where  a.assemblyid = ag.assemblyid and ag.customized_assembly_code = s.code_num_value and ag.agentid='"+strAgentID+"' and codeid='customized_assembly'  and ag.applicationid='"+strAppID +"' and ag.enterpriseid='"+strEnterpriseID+"'");			

			String strSQLQuery = strBuilder.ToString();			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ServiceCodeTable");

			return  sessionDS;
	
		}
		public static void AddAgentNewRowInCustomizedCodeDS(SessionDS sessionDS)
		{
			DataRow drNew = sessionDS.ds.Tables[0].NewRow();

			drNew[0] = 0;
			drNew[1] = "";
			drNew[2] = "";
			try
			{
				sessionDS.ds.Tables[0].Rows.Add(drNew);
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("SysDataMgrDAL","DeleteRouteCodeDS","R005",ex.Message.ToString());
			}			
			sessionDS.QueryResultMaxSize++;
		}
		public static int AddAgentCustomizedCodeDS(String strAppID, String strEnterpriseID,String strAgentID,DataSet dsToInsert)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetServiceCodeDS","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("insert into Agent_assembly (applicationid,enterpriseid,agentid,customized_assembly_code,assemblyid) values ('");
			strBuilder.Append(strAppID);
			strBuilder.Append("','");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(strAgentID);
			strBuilder.Append("',");

			int iRowCnt = dsToInsert.Tables[0].Rows.Count;

			DataRow drEach = dsToInsert.Tables[0].Rows[0];
				
			Decimal strServiceCode = Convert.ToDecimal(drEach["customized_assembly_code"].ToString());
			strBuilder.Append(strServiceCode);
			strBuilder.Append(",'");

			String strServiceDescription = (String) drEach["assemblyid"];
			strBuilder.Append(Utility.ReplaceSingleQuote(strServiceDescription));
			strBuilder.Append("')");	

			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			

			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("SysDataMgrDAL","AddServiceCodeDS","SDM001",iRowsAffected+" rows inserted in to Service table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","AddServiceCodeDS","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Service Code ",appException);
			}
			return iRowsAffected;

		}



		#endregion
	}
}
