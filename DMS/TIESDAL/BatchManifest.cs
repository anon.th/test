using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using System.Data.SqlClient;


namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for BatchManifest.
	/// </summary>
	public class BatchManifest
	{
		public BatchManifest()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static DataSet GetBatchList(string m_strAppID,string m_strEnterpriseID,string batch_no,
			string truck_id,string batch_id,string batchdate,string batch_type,string location,string chkloc,
			string chkOpen,string driver_id)
		{
			DataSet dsCons = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",m_strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",m_strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@batch_no",batch_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@truck_id",truck_id));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@batch_id",batch_id));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@batchdate",batchdate));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@batch_type",batch_type));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@location",location));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@chkloc",chkloc));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@chkOpen",chkOpen));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@driver_id",driver_id));

			dsCons =(DataSet)dbCon.ExecuteProcedure ("MF_Batch_Manifest_Read",storedParams,common.DAL.ReturnType.DataSetType);

			return dsCons ;
		}

		public static DataSet GetBatchDetail_TruckLoad(string m_strAppID,string m_strEnterpriseID,string truckid)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","getTruckDetail","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select truckid,isnull(tare_weight,0) tare_weight , isnull(gross_weight,0) gross_weight from MF_Vehicles  ");
			strQry.Append("where  truckid = '");
			strQry.Append(truckid);
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}
		public static DataSet GetBatchDetail_ShippingOriginLoad(string m_strAppID,string m_strEnterpriseID,string batch_no,string curloc)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","getTruckDetail","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * from dbo.MF_Batch_Manifest_DepArr WITH (NOLOCK) ");
			strQry.Append("where  batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("' and dep_dc = '");
			strQry.Append(curloc);
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}

		public static DataSet GetBatchDetail_ShippingTerminationLoad(string m_strAppID,string m_strEnterpriseID,string batch_no,string curloc)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","getTruckDetail","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * from dbo.MF_Batch_Manifest_DepArr WITH (NOLOCK) ");
			strQry.Append("where  batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("' and arr_dc = '");
			strQry.Append(curloc);
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}
		public static DataSet GetBatchDetail_Shipping(string m_strAppID,string m_strEnterpriseID,string batch_no)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","getShippingDetail","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select distinct * from dbo.MF_Batch_Manifest_DepArr WITH (NOLOCK) ");
			strQry.Append("where  batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("' order by updated_DT DESC");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}
		public static DataSet GetBatchDetail_Shipping_BMCP(string m_strAppID,string m_strEnterpriseID,string batch_no)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","getShippingDetail","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * from dbo.MF_Batch_Manifest_DepArr WITH (NOLOCK) ");
			strQry.Append("where  batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("'order by (CASE WHEN Dep_DT IS NULL THEN 1 ELSE 0 END) DESC,");
			strQry.Append(" Dep_DT DESC,");
			strQry.Append(" Updated_DT DESC");



			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}


		public static DataSet GetBatchDetail_Shipping_OnArrival(string m_strAppID,string m_strEnterpriseID,string batch_no)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","getShippingDetail","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select distinct * from dbo.MF_Batch_Manifest_DepArr WITH (NOLOCK) ");
			strQry.Append("where  batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("' order by dep_DT DESC");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}

		public static DataSet GetBatchDetailForBatchType_L_OnDeparture(string m_strAppID,string m_strEnterpriseID,string batch_no,string location)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","getShippingDetail","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select distinct * from dbo.MF_Batch_Manifest_DepArr WITH (NOLOCK) ");
			strQry.Append("where  batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("' and isnull(dep_dc,'') ='"+ location +"' and dep_dt is not null order by dep_DT DESC");
			

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}
		public static DataSet GetBatchDetailForBatchType_L_OnArrival(string m_strAppID,string m_strEnterpriseID,string batch_no)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","getShippingDetail","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select distinct * from dbo.MF_Batch_Manifest_DepArr WITH (NOLOCK) ");
			strQry.Append("where  batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("' and isnull(dep_dc,'') !='' and dep_dt is not null order by dep_DT DESC");
			

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}

				public static int ModifyUpdateDT(string m_strAppID,string m_strEnterpriseID,string batch_no)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("BatchManifest","ModifyUpdateDT","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}
			//			int iRowCnt = dsToModify.Tables[0].Rows.Count;
			//			DataRow drEach = dsToModify.Tables[0].Rows[0];


			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update dbo.MF_Batch_Manifest_DepArr WITH (ROWLOCK) set ");
			strBuilder.Append("Updated_DT = getdate()");
			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(m_strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(m_strEnterpriseID);
			strBuilder.Append("' and batch_no = '");
			strBuilder.Append(batch_no);
			strBuilder.Append("'");


			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("BatchManifest","ModifyUpdateDT","SDM001",iRowsAffected+" rows inserted in to Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyUpdateDT","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}

			return iRowsAffected;
		}

		public static int ModifyDep_DTTONull(string m_strAppID,string m_strEnterpriseID,string batch_no,string Dep_DC)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("BatchManifest","ModifyUpdateDTTonull","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update dbo.MF_Batch_Manifest_DepArr WITH (ROWLOCK) set ");
			strBuilder.Append("Dep_DT = null,");
			strBuilder.Append(" Dep_Truck_ID = null");
//			strBuilder.Append(" Dep_Driver_ID = null");   // Thosapol.y	 Comment >> Issue 163 (28/06/2013)
//			strBuilder.Append(" Dep_Est_Arr_DT = null,");
//			strBuilder.Append(" Dep_Flight_No = null,");
//			strBuilder.Append(" Dep_Flight_DT = null,");
//			strBuilder.Append(" Dep_AWB_No = null");
			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(m_strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(m_strEnterpriseID);
			strBuilder.Append("' and batch_no = '");
			strBuilder.Append(batch_no);
			strBuilder.Append("' and Dep_DC = '");
			strBuilder.Append(Dep_DC);
			strBuilder.Append("'");


			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("BatchManifest","ModifyUpdateDTTonull","SDM001",iRowsAffected+" rows inserted in to Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyUpdateDTTonull","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}

			return iRowsAffected;
		}


		public static int checkduplicate(string m_strAppID,string m_strEnterpriseID,string cons,string mps_no)
		{
			int Result = 0;
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","getTruckDetail","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select consignment_no from Package_Details  ");
			strQry.Append("where  consignment_no = '");
			strQry.Append(cons);
			strQry.Append("' and mps_no = '"); 
			strQry.Append(mps_no);
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				if ( dsCons.Tables[0].Rows.Count > 0  )
				{
					Result = 1;
				}
				else
				{
					Result = 0;
				}
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return Result;
		}

		public static DataSet GetDataArrivalDeparture(string m_strAppID,string m_strEnterpriseID,string m_strculture,string batch_no)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","GetDataArrivalDeparture","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * from MF_Batch_Manifest a "+
									" left outer join "+
									" (select code_str_value,code_text from Core_System_Code where codeid = 'delivery_type' and culture='"+m_strculture+"') "+ 
									" b on a.batch_type = b.code_str_value ");
			strQry.Append("where  batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}
		public static DataSet getManifestStatus(string m_strAppID,string m_strEnterpriseID,string batch_typecode,string strculture)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Manifest.cs","getManifestStatus","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * from Core_System_Code ");
			strQry.Append("where  codeid = '");
			strQry.Append(batch_typecode);
			strQry.Append("'");
			strQry.Append("and  culture = '");
			strQry.Append(strculture);
			strQry.Append("'");


			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Manifest.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}
		public static DataSet GetManifestDetail(string m_strAppID,string m_strEnterpriseID,string batch_no,string statusList,string VarianceType,string DestDC,string strCulture)
		{
			DataSet dsCons = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@AppID",m_strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",m_strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Batch_no",batch_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@StatusList",statusList));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@VarianceType",VarianceType));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@DestDC",DestDC));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@culture",strCulture));

			dsCons =(DataSet)dbCon.ExecuteProcedure ("MF_Batch_Manifest_View_Variance_Read",storedParams,common.DAL.ReturnType.DataSetType);

			return dsCons ;
		}


		public static int UpdateArrivalDeparture(string m_strAppID,string m_strEnterpriseID,string batch_no,
			string type,string dep_dc,string dep_dt,string dep_driver_id,string Dep_Est_Arr_DT,
			string arr_dc,string arr_dt,
			string Dep_Flight_No,string Dep_AWB_No,string Dep_Flight_DT,
			string Arr_Truck_ID,string Arr_Driver_ID,
			string lastuser,string location)
		{
			//DataSet dsCons = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",m_strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",m_strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@batch_no",batch_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@type",type));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@dep_dc",dep_dc));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@dep_dt",dep_dt));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@dep_driver_id",dep_driver_id));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Dep_Est_Arr_DT",Dep_Est_Arr_DT));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@arr_dc",arr_dc));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@arr_dt",arr_dt));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Dep_Flight_No",Dep_Flight_No));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Dep_AWB_No",Dep_AWB_No));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Dep_Flight_DT",Dep_Flight_DT));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Arr_Truck_ID",Arr_Truck_ID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Arr_Driver_ID",Arr_Driver_ID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@lastuserid",lastuser));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@location",location));

			//dsCons =(DataSet)dbCon.ExecuteProcedure ("MF_Batch_Manifest_Update_Arrival_Departure",storedParams,common.DAL.ReturnType.DataSetType);
			int result = (int)dbCon.ExecuteProcedure("MF_Batch_Manifest_Update_Arrival_Departure",storedParams);

			return result ;
		}

		public static int InsertReceivingComplete(string m_strAppID,string m_strEnterpriseID,string batch_no,
			string arr_dc,string arr_dt,string truck_id,string batch_id,string driver_id,
			string lastuserid,string location,string batch_type)
		{
			//DataSet dsCons = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",m_strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",m_strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@batch_no",batch_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@arr_dc",arr_dc));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@arr_dt",arr_dt));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@truck_id",truck_id));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@batch_id",batch_id));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@driver_id",driver_id));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@lastuserid",lastuserid));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@location",location));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@batch_type",batch_type));

			//dsCons =(DataSet)dbCon.ExecuteProcedure ("MF_Batch_Manifest_Update_Arrival_Departure",storedParams,common.DAL.ReturnType.DataSetType);
			int result = (int)dbCon.ExecuteProcedure("MF_Batch_Manifest_Insert_Receiving_Complete",storedParams);

			return result ;
		}

		public static int UpdateScannedPkgs(string m_strAppID,string m_strEnterpriseID,
			string userid,string location,string batch_no)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",m_strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",m_strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userid",userid));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@location",location));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@batch_no",batch_no));

			int result = (int)dbCon.ExecuteProcedure("MF_Batch_Manifest_ScannedPkgs",storedParams);

			return result ;
		}

		public static DataSet GetDayToRead(string m_strAppID,string m_strEnterpriseID)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","GetDayToRead","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();

			//--------------Edit new sql by PAK -----------10 June 2013

			strQry = strQry.Append("select *,getdate()-convert(int,code_str_value) DayToRead from Core_System_Code  ");
			strQry.Append("where culture = '");
			strQry.Append(m_strEnterpriseID);
			strQry.Append("' and codeid = 'ManifestLifeDays");
			//strQry.Append("' and code_text = 'Life_of_Batch_Manifest");
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}

		public static DataSet GetDCEmailAddress(Utility util, string strBatch_No)
		{	
			string AppID = util.GetAppID(), EnterpriseID=util.GetEnterpriseID();

			IDbCommand dbCmd = null;
			DbConnection dbCon= DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
			if (dbCon==null)
			{
				throw new ApplicationException("Connection to Database failed",null);
			}
			DataSet DS = null; 
			try
			{
				String strSQL ="  select distinct DC.origin_code,DC.EmailAddress, ";
				strSQL += " DC.origin_code +'  '+ isnull(DC.EmailAddress,'') AS DCDispaly from MF_Batch_Manifest_Detail MD  ";
				strSQL += " inner join Distribution_center DC on MD.Dest_DC=DC.Origin_code ";
				strSQL += " AND MD.enterpriseID=DC.enterpriseID ";
				strSQL += " AND MD.applicationID=DC.applicationID ";
				strSQL+=" WHERE (DC.applicationid = '"+AppID+"') and (DC.enterpriseid = '"+EnterpriseID+"') and (MD.Batch_No = '"+strBatch_No+"') ";
				strSQL+=" and  origin_code not in ( ";
				strSQL+=" select distinct Dest_DC from MF_Batch_Manifest_Detail ";
				strSQL+=" where applicationid = '"+AppID+"' and enterpriseid = '"+EnterpriseID+"' and Batch_No = '"+strBatch_No+"' ";
				strSQL+=" and Recv_Pkg_Qty>=Shipped_Pkg_Qty group by dest_dc having count(consignment_no)=0)";
				strSQL+="   order by origin_code ASC";
				
				dbCmd = dbCon.CreateCommand(strSQL, CommandType.Text);
				dbCmd.CommandTimeout = 300;  //Add Command TimeOut for case of serveral records: 300 seconds (5 minutes)
				DS = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				
			}
			catch(ApplicationException ex)
			{
				throw ex;
			}

			return DS;
		}

		public static int ForwardManifest(Utility util,User us,string strBatch_no,string strEmailContent,string strEmailAddress)
		{		
			string AppID = util.GetAppID(), EnterpriseID=util.GetEnterpriseID();

			IDbCommand dbCmd = null;
			DbConnection dbCon= DbConnectionManager.GetInstance().GetDbConnection(AppID,EnterpriseID);
			if (dbCon==null)
			{
				throw new ApplicationException("Connection to Database failed",null);
			}
			int iRowsEffected = 0;
			try
			{
				String strSQL =" Update MF_Batch_Manifest_Process SET Updated_DT=getdate(),userID='"+ us.UserID +"', ";
				strSQL += " Location='"+ us.UserLocation +"',Email_To='" +us.UserEmail +"',Email_CC='"+ strEmailAddress +"',  ";
				strSQL += " Email_Content=N'"+ strEmailContent +"' ";
				strSQL+=" WHERE (applicationid = '"+AppID+"') and (enterpriseid = '"+EnterpriseID+"') and (Batch_No = '"+strBatch_no+"') ";
				dbCmd = dbCon.CreateCommand(strSQL, CommandType.Text);
				dbCmd.CommandTimeout = 300;  //Add Command TimeOut for case of serveral records: 300 seconds (5 minutes)
				iRowsEffected = dbCon.ExecuteNonQuery(dbCmd);
				
			}
			catch(ApplicationException ex)
			{
				throw ex;
			}
			return iRowsEffected;
		}
		public static int UpdateReComplete(string m_strAppID,string m_strEnterpriseID,string batch_no)
		{
			int iRowsAffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("BatchManifest","ModifyUpdateDT","EDB101","DbConnection object is null!!");
				return iRowsAffected;
			}

			StringBuilder strBuilder = new StringBuilder();
			
			strBuilder.Append("update dbo.MF_Batch_Manifest_DepArr WITH (ROWLOCK) set ");
			strBuilder.Append("Recv_Is_Complete = 'Y'");
			strBuilder.Append(" where applicationid = '");
			strBuilder.Append(m_strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(m_strEnterpriseID);
			strBuilder.Append("' and batch_no = '");
			strBuilder.Append(batch_no);
			strBuilder.Append("'");


			String strSQLQuery = strBuilder.ToString();

			IDbCommand dbCmd = dbCon.CreateCommand(strSQLQuery,CommandType.Text);
			
			try
			{
				iRowsAffected = dbCon.ExecuteNonQuery(dbCmd);
				Logger.LogTraceInfo("BatchManifest","ModifyUpdateDT","SDM001",iRowsAffected+" rows inserted in to Exception_Code table");
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RBACManager","ModifyUpdateDT","SDM001","Error During Insert "+ appException.Message.ToString());
				throw new ApplicationException("Error inserting Status Code ",appException);
			}

			return iRowsAffected;
		}

		public static DataSet GetRowScan(string m_strAppID,string m_strEnterpriseID,string batch_no,string location)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","GetDataArrivalDeparture","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select count(batch_no) cbatch_no from MF_Shipment_Tracking_staging ");
			strQry.Append(" where applicationid = '");
			strQry.Append(m_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(m_strEnterpriseID);
			strQry.Append("' and batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("' and location = '");
			strQry.Append(location);
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}

		public static DataSet GetEnableArrivalForBatchTypeL(string m_strAppID,string m_strEnterpriseID,string batch_no)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","GetDataArrivalDeparture","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select count(batch_no) cbatch_no from dbo.MF_Batch_Manifest_DepArr WITH (NOLOCK) ");
			strQry.Append(" where applicationid = '");
			strQry.Append(m_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(m_strEnterpriseID);
			strQry.Append("' and batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("' and isnull(dep_dc,'') !='' and dep_dt is not null ");
			

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}
		public static DataSet GetEnableRecForBatchTypeL(string m_strAppID,string m_strEnterpriseID,string batch_no,string arr_dc)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","GetDataArrivalDeparture","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select count(batch_no) cbatch_no from dbo.MF_Batch_Manifest_DepArr WITH (NOLOCK) ");
			strQry.Append(" where applicationid = '");
			strQry.Append(m_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(m_strEnterpriseID);
			strQry.Append("' and batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("' and arr_dc = '");
			strQry.Append(arr_dc);
			strQry.Append("' and arr_dt is not null and  isnull(Recv_Is_Complete,'') !='Y' ");
			

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}
		public static DataSet GetCHKDDLDriverID(string m_strAppID,string m_strEnterpriseID,string emp_id,string location)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","GetDataArrivalDeparture","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select count(emp_id) cemp_id FROM mf_staff ");
			strQry.Append(" where applicationid = '");
			strQry.Append(m_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(m_strEnterpriseID);
			strQry.Append("' and Emp_Type = 'D");
			strQry.Append("' and emp_id = '");
			strQry.Append(emp_id);
			strQry.Append("' and base_location = '");
			strQry.Append(location);
			strQry.Append("' and (Initial_In_Service_Date <= Getdate()) AND (Separation_Date > Getdate()) AND (((Out_Of_Service_Date Is Null) AND (In_Service_Date Is Null)) OR ((In_Service_Date Is Null) AND (Getdate()<Out_Of_Service_Date)) OR ((Getdate()<Out_Of_Service_Date) OR (Getdate()>=In_Service_Date))) ");
			//strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}

		public static DataSet GetCHKDDLDriverIDforbatchtypeL_A(string m_strAppID,string m_strEnterpriseID,string emp_id,string base_location,string batch_id)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","GetDataArrivalDeparture","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select count(emp_id) cemp_id FROM mf_staff ");
			strQry.Append(" where applicationid = '");
			strQry.Append(m_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(m_strEnterpriseID);
			strQry.Append("' and Emp_Type = 'D");
			strQry.Append("' and emp_id = '");
			strQry.Append(emp_id);
			strQry.Append("' and (base_location ='"+base_location+"' or base_location in (select origin_station from Delivery_Path where path_code ='"+batch_id+"') or base_location in (select destination_station from Delivery_Path where path_code ='"+batch_id+"')) ");
			strQry.Append(" and (Initial_In_Service_Date <= Getdate()) AND (Separation_Date > Getdate()) AND (((Out_Of_Service_Date Is Null) AND (In_Service_Date Is Null)) OR ((In_Service_Date Is Null) AND (Getdate()<Out_Of_Service_Date)) OR ((Getdate()<Out_Of_Service_Date) OR (Getdate()>=In_Service_Date))) ");
			//strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}
		public static DataSet GetDestinationStation(string m_strAppID,string m_strEnterpriseID,string batch_id,string batch_type)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","GetDataArrivalDeparture","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * FROM Delivery_Path ");
			strQry.Append(" where applicationid = '");
			strQry.Append(m_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(m_strEnterpriseID);
			strQry.Append("' and path_code = '");
			strQry.Append(batch_id);
			strQry.Append("' and delivery_type = '");
			strQry.Append(batch_type);
			strQry.Append("' and delivery_type IN ('A', 'L') "); 
			//strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}

		public static DataSet GetDestinationAllType(string m_strAppID,string m_strEnterpriseID,string batch_id)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","GetDataArrivalDeparture","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT path_code, applicationid, enterpriseid, path_description, delivery_type, line_haul_cost, isnull(origin_station,' ') as origin_station, isnull(destination_station,' ') as destination_station, flight_vehicle_no, awb_driver_name, departure_time FROM Delivery_Path ");
			strQry.Append(" where applicationid = '");
			strQry.Append(m_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(m_strEnterpriseID);
			strQry.Append("' and path_code = '");
			strQry.Append(batch_id);
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}
		public static DataSet GetEnableScanPkg(string m_strAppID,string m_strEnterpriseID,string batch_no)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","GetDataArrivalDeparture","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select count(batch_no) cbatch_no from MF_Batch_Manifest_Detail ");
			strQry.Append(" where applicationid = '");
			strQry.Append(m_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(m_strEnterpriseID);
			strQry.Append("' and batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}

		public static DataSet GetTracking_Datetime(string m_strAppID,string m_strEnterpriseID,string batch_no,string location)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","getShippingDetail","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * from MF_Shipment_Tracking_Staging  ");
			strQry.Append("where  batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("' and location = '"+ location +"' and tracking_datetime = (select max(tracking_datetime) from MF_Shipment_Tracking_Staging where batch_no ='"+ batch_no+"' and status_code in ('SOP') and location ='"+ location +"')");
			

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}

		public static DataSet GetArrival_Datetime(string m_strAppID,string m_strEnterpriseID,string batch_no,string location)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","getShippingDetail","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select * from MF_Shipment_Tracking_Staging  ");
			strQry.Append("where  batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("' and location = '"+ location +"' and tracking_datetime = (select min(tracking_datetime) from MF_Shipment_Tracking_Staging where batch_no ='"+ batch_no+"' and status_code in ('SIP') and location ='"+ location +"')");
			

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}

		public static DataSet GetArrival_DatetimeDepArr(string m_strAppID,string m_strEnterpriseID,string batch_no,string location)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","getShippingDetail","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append(" select top 1 arr_dt,arr_dc from dbo.MF_Batch_Manifest_DepArr WITH (NOLOCK) where batch_no ='"+ batch_no +"' and arr_dc ='"+ location +"' order by arr_dt desc ");			

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}

		public static DataSet GetArrDTforTypeL(string m_strAppID,string m_strEnterpriseID,string batch_no,string location)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","getShippingDetail","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append(" select arr_dt from dbo.MF_Batch_Manifest_DepArr WITH (NOLOCK) where arr_dc ='"+location+"' and batch_no ='"+batch_no+"' ");			

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}
		public static int GetDupinstatging(string m_strAppID,string m_strEnterpriseID,string batch_no,string location,string cons,string pkgs,string status_code)
		{
			//DataSet dsCons = null;
			//int result;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@m_strAppID",m_strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@m_strEnterpriseID",m_strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@batch_no ",batch_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@location",location));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@cons",cons));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@pkgs",pkgs));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@status_code",status_code));
			//storedParams.Add(new System.Data.SqlClient.SqlParameter("@call","ship"));
			//dsCons =(DataSet)dbCon.ExecuteProcedure ("GetDupinstatging",storedParams,common.DAL.ReturnType.DataSetType);
			int result = (int)dbCon.ExecuteProcedureScalar("GetDupinstatging",storedParams);

			return result ;
			//return dsCons ;

		}
		public static DataSet GetCountConsignment(string m_strAppID,string m_strEnterpriseID,string status_code,string batch_no,string location)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","GetDataArrivalDeparture","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select count(distinct consignment_no) c_count from MF_Shipment_Tracking_Staging ");
			strQry.Append(" where batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("' and status_code = '");
			strQry.Append(status_code);
			strQry.Append("' and location = '");
			strQry.Append(location);
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}


		// Thosapol Yennam (04/09/2013) ** New Code ** Check Config EnterpriseConfigurations is DeparturesArrivals
		public static string IsConfigDeparturesArrivals(string m_strAppID,string m_strEnterpriseID)
		{
			string sData = string.Empty;
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("BatchMannifest.cs","IsConfigDeparturesArrivals","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("SELECT * FROM dbo.EnterpriseConfigurations('"+m_strEnterpriseID+"', 'DeparturesArrivals')");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
				foreach(DataRow dr in dsCons.Tables[0].Rows)
				{
					if(dr["key"].ToString() == "DisableFlightTimeAudits")
					{
						sData = dr["value"].ToString();
						break;
					}					
				}
				dsCons.Clear();
				dsCons.Dispose();
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("BatchMannifest.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			
			return sData;
		}



		public static int InsertToStategingNoexist(string m_strAppID,string m_strEnterpriseID,string strconsignment_no,
			string strstatus,string struser)
		{
			//DataSet dsCons = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",m_strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",m_strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",strconsignment_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@status",strstatus));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@user",struser));
		
			int result = (int)dbCon.ExecuteProcedure("MF_Shipment_Tracking_Staging_Insert_NoExist",storedParams);

			return result ;
		}

		public static DataSet GetDepDTISNULL(string m_strAppID,string m_strEnterpriseID,string batch_no)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","GetDataArrivalDeparture","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select count(distinct batch_no) cbatch_no from dbo.MF_Batch_Manifest_DepArr WITH (NOLOCK) ");
			strQry.Append(" where dep_dt is null and applicationid = '");
			strQry.Append(m_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(m_strEnterpriseID);
			strQry.Append("' and batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}

		public static DataSet GetDepdcforArrdc(string m_strAppID,string m_strEnterpriseID,string batch_no,string arr_dc)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","GetDataArrivalDeparture","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("select distinct dep_dc FROM dbo.MF_Batch_Manifest_DepArr WITH (NOLOCK) ");
			strQry.Append(" where applicationid = '");
			strQry.Append(m_strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(m_strEnterpriseID);
			strQry.Append("' and batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("' and dep_dc = '");
			strQry.Append(arr_dc);
			strQry.Append("' and dep_dt is not null "); 
			//strQry.Append("'");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}

		public static DataSet GetDestDCForSIPByBatch(string m_strAppID,string m_strEnterpriseID,string batch_no)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("Create_SIPDAL.cs","GetDestDCForSIPByBatch","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry.Append(" select distinct dest_dc,origin_name from mf_batch_manifest_detail bmd ");
			strQry.Append(" inner join distribution_center dc ");
			strQry.Append(" On dc.applicationid = bmd.applicationid and dc.enterpriseid = bmd.enterpriseid and ");
			strQry.Append(" origin_code = dest_DC ");
			strQry.Append(" where bmd.applicationid = '" +m_strAppID+ "' and bmd.enterpriseid = '"+m_strEnterpriseID+"' and ");
			strQry.Append(" batch_no = '");
			strQry.Append(batch_no);
			strQry.Append("' and (shipped_pkg_qty - recv_pkg_qty) > 0");
			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("Service.cs","GetDestDCForSIPByBatch","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}
		public static int InsertToStategingSIPbyBatch(string m_strAppID,string m_strEnterpriseID,string struserID,string strLocation,
			string strBatchNo,string strDesc,string strPersonInchange)
		{
			//DataSet dsCons = null;

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",m_strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",m_strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userid",struserID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@location",strLocation));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@batch_no",strBatchNo));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Dest_DCs",strDesc));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@person_incharge",strPersonInchange));
		
			int result = (int)dbCon.ExecuteProcedureScalar("MF_Batch_SIP_Insert",storedParams);
			return result ;
		}



        //

        public static System.Data.DataSet GetManifestDataToReport(string strAppID, string strEnterpriseID, string sBatch_No)//, string sTruck_ID, string sBatch_ID, string sBatch_Date, string sBatch_Type, string sCurrLoc, string sDriver_ID
        {
            
            DataSet dsResult = new DataSet();
            IDbCommand dbCmd = null;
            DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
            if (dbCon == null)
            {
                //Logger.LogTraceError("TIESDAL", "CustomsCheckRequisitionDAL", "GetChequeRequisitionTypes", "EDB101", "DbConnection object is null!!");
                //throw new ApplicationException("Connection to Database failed", null);
            }
            try
            {
                StringBuilder SQL = new StringBuilder();
                SQL.AppendLine(" SELECT ");
                SQL.AppendLine("   v_DeliveryManifestByRoute.AWB_Driver_Name ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.Consignment_No ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.origin_state_code ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.destination_state_code ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.Tot_Pkg ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.service_code ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.sender_name ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.SenderAddress ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.sender_zipcode ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.sender_telephone ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.sender_fax ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.recipient_name ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.RecipientAddress ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.recipient_zipcode ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.recipient_telephone ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.enterprise_name ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.recipient_fax ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.applicationid ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.enterpriseid ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.delivery_type ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.Enterprise_Address ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.est_delivery_datetime ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.cod_amount ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.cod_amount_type ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.ref_no ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.tot_dim_wt ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.OriginStateName ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.DestinationStateName ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.OriginStateCode ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.DestinationStateCode ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.tot_wt ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.recipient_contact_person ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.remark ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.return_invoice_hc ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.return_pod_slip ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.Batch_No ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.Batch_ID ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.Truck_ID ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.Shipped_Pkgs ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.last_status_code ");
                SQL.AppendLine("  ,MF_Batch_Manifest_Detail.Batch_No ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.Batch_Date ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.MobileNo ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.Shipped_Pkg_Qty ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.Recv_Pkg_Qty ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.StatusReport ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.MobileNo ");
                SQL.AppendLine(" FROM ");
                SQL.AppendLine("   v_DeliveryManifestByRoute_MF v_DeliveryManifestByRoute ");
                SQL.AppendLine("   INNER JOIN MF_Batch_Manifest_Detail MF_Batch_Manifest_Detail ");
                SQL.AppendLine("     ON (((v_DeliveryManifestByRoute.applicationid = ");
                SQL.AppendLine("             MF_Batch_Manifest_Detail.applicationID) AND ");
                SQL.AppendLine("          (v_DeliveryManifestByRoute.enterpriseid = ");
                SQL.AppendLine("             MF_Batch_Manifest_Detail.enterpriseID)) AND ");
                SQL.AppendLine("         (v_DeliveryManifestByRoute.Consignment_No = ");
                SQL.AppendLine("            MF_Batch_Manifest_Detail.Consignment_No)) AND ");
                SQL.AppendLine("        (v_DeliveryManifestByRoute.Batch_No = MF_Batch_Manifest_Detail.Batch_No) ");
                SQL.AppendLine(" WHERE ");
                SQL.AppendLine("   v_DeliveryManifestByRoute.Shipped_Pkg_Qty > ");
                SQL.AppendLine("     v_DeliveryManifestByRoute.Recv_Pkg_Qty ");
                if(!string.IsNullOrEmpty(sBatch_No))
                    SQL.AppendLine("and MF_Batch_Manifest_Detail.Batch_No = '"+sBatch_No+"'");
                SQL.AppendLine(" ORDER BY ");
                SQL.AppendLine("   v_DeliveryManifestByRoute.Batch_ID ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.Truck_ID ");
                SQL.AppendLine("  ,v_DeliveryManifestByRoute.Consignment_No ");


                dbCmd = dbCon.CreateCommand(SQL.ToString(), CommandType.Text);
                dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

                return dsResult;
            }
            catch (Exception ex)
            {
                Logger.LogTraceError("TIESDAL", "CustomsCheckRequisitionDAL", "GetChequeReqDataToReport", ex.Message);
                throw new ApplicationException("Select command failed");
            }
            finally
            {
                dsResult.Dispose();
            }

        }

        //
    }
}
