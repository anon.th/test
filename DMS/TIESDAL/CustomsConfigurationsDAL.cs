using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using TIESClasses;

namespace TIESDAL
{
	public class CustomsConfigurationsDAL
	{
		private string SQLText = string.Empty;
		private DbConnection dbCon;
		private IDbCommand dbCmd = null;

		public CustomsConfigurationsDAL(string _appID, string _enterpriseID) 
		{ 
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(_appID, _enterpriseID);
		}

		public DataSet DropdownList(CustomsConfigurations objInfo)
		{
			DataSet dsReturn = new DataSet();
			ArrayList arlParams = new ArrayList();

			try
			{
				SQLText = @"EXEC dbo.Customs_EditConfigurations @action = 0,
															    @enterpriseid = '{0}';";
				SQLText = string.Format(SQLText, objInfo.EnterpriseId);

				dbCmd = dbCon.CreateCommand(SQLText, CommandType.Text);
				dsReturn = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method InsupdCurrencyExchangeRates : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CustomsConfigurationsDAL", "DropdownList", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
		}
		public DataSet GridView(CustomsConfigurations objInfo)
		{
			DataSet dsReturn = new DataSet();
			ArrayList arlParams = new ArrayList();

			try
			{
				SQLText = @"EXEC dbo.Customs_EditConfigurations @action = 1,
															    @enterpriseid = '{0}',
																@CodeID = '{1}';";
				SQLText = string.Format(SQLText, objInfo.EnterpriseId, objInfo.CodeID);

				dbCmd = dbCon.CreateCommand(SQLText, CommandType.Text);
				dsReturn = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);

				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method InsupdCurrencyExchangeRates : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CustomsConfigurationsDAL", "GridView", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
		}

		public DataSet ExecCustomsEditConfigurations(CustomsConfigurations objInfo)
		{
			DataSet dsReturn = new DataSet();
			try
			{
				if(objInfo.Action.Trim().Equals(""))
					throw new ApplicationException("Please specify Action");
				if(objInfo.EnterpriseId.Trim().Equals(""))
					throw new ApplicationException("Please specify EnterpriseId");
			
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@action", objInfo.Action.Trim()));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid", objInfo.EnterpriseId.Trim()));

				if(!objInfo.CodeID.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@CodeID", objInfo.CodeID.Trim()));
				if(!objInfo.PK.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@PK", objInfo.PK.Trim()));
				if(!objInfo.C1.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@C1", objInfo.C1.Trim()));
				if(!objInfo.C2.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@C2", objInfo.C2.Trim()));
				if(!objInfo.C3.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@C3", objInfo.C3.Trim()));
				if(!objInfo.C4.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@C4", objInfo.C4.Trim()));
				if(!objInfo.C5.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@C5", objInfo.C5.Trim()));
				if(!objInfo.C6.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@C6", objInfo.C6.Trim()));
				if(!objInfo.C7.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@C7", objInfo.C7.Trim()));
				if(!objInfo.C8.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@C8", objInfo.C8.Trim()));
				if(!objInfo.C9.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@C9", objInfo.C9.Trim()));
				if(!objInfo.C10.Trim().Equals(""))
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@C10", objInfo.C10.Trim()));

				dbCmd = dbCon.CreateCommand("Customs_EditConfigurations", storedParams, CommandType.StoredProcedure);
				dsReturn = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);  

				return dsReturn;
			}
			catch(Exception ex)
			{
				string msgException = string.Format("Method InsupdCurrencyExchangeRates : {0}|{1}", ex.Message, ex.InnerException);
				Logger.LogTraceError("TIESDAL", "CustomsConfigurationsDAL", "ExecCustomsEditConfigurations", msgException);
				throw new ApplicationException(msgException);
			}
			finally
			{
				dsReturn.Dispose();
			}
		}
	}
}
