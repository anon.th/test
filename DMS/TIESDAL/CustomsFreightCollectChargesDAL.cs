using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;
using System.Collections;
using System.Text.RegularExpressions;
using TIESClasses;

namespace TIESDAL
{
	/// <summary>
	/// Summary description for CustomsFreightCollectChargesDAL.
	/// </summary>
	public class CustomsFreightCollectChargesDAL
	{
		public CustomsFreightCollectChargesDAL()
		{
			
		}

		public static System.Data.DataSet Customs_Freight_Collect_Charges(string strAppID, string strEnterpriseID, string strUserloggedin, 
			string action, string consignment_no, string jobentryno, string payerid, string Amount, string FreightDate, string CurrencyCode)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsFreightCollectChargesDAL","Customs_Freight_Collect_Charges","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{ 
				StringBuilder strQry = new StringBuilder();
				strQry.Append("EXEC dbo.Customs_FreightCollect ");
				strQry.Append("@action = " + action );
				strQry.Append(",@enterpriseid = '" + strEnterpriseID + "' ");
				strQry.Append(",@userloggedin = '" + strUserloggedin + "' ");

				// consignment_no
				if(consignment_no.ToString().Trim() != String.Empty)
				{
					strQry.Append(",@consignment_no = '" + consignment_no + "' ");
				}
				else
				{
					strQry.Append(",@consignment_no = NULL ");
				}

				// JobEntryNo
				if(jobentryno.ToString().Trim() != String.Empty)
				{
					strQry.Append(",@JobEntryNo = " + jobentryno);
				}
				else
				{
					strQry.Append(",@JobEntryNo = NULL ");
				}
				
				// payerid
				if(payerid.ToString().Trim() != String.Empty)
				{
					strQry.Append(",@payerid = '" + payerid + "'");
				}
				else
				{
					strQry.Append(",@payerid = NULL ");
				}

				// Amount is Empty
				if(Amount.ToString().Trim() != String.Empty)
				{
					strQry.Append(",@Amount = " + Amount.Replace(",",""));
				}
				else
				{
					strQry.Append(",@Amount = NULL ");
				}

				// FreightDate
				if(FreightDate.ToString().Trim() != String.Empty)
				{
					strQry.Append(",@FreightDate = '" + FreightDate + "'");
				}
				else
				{
					strQry.Append(",@FreightDate = NULL ");
				}

				// CurrencyCode
				if(CurrencyCode.ToString().Trim() != String.Empty)
				{
					strQry.Append(",@CurrencyCode = '" + CurrencyCode + "'");
				}
				else
				{
					strQry.Append(",@CurrencyCode = NULL ");
				}

				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsFreightCollectChargesDAL", "Customs_Invoice_Report", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 
		}

		public static System.Data.DataSet GetCurrencies( string strAppID, string strEnterpriseID )
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsFreightCollectChargesDAL","GetCountry","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT CurrencyCode, DefaultValue ");
				strQry.Append(" FROM dbo.GetCurrencies( '"+strEnterpriseID+"' )" );  
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsFreightCollectChargesDAL", "GetCurrencies", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			}
		}

		public static System.Data.DataSet GetExchangeRate(string strAppID, string strEnterpriseID,string CurrenyCode, string RateDate)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsFreightCollectChargesDAL","GetExchangeRate","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT enterpriseid, rate_date, CurrencyCode, rate_type, exchange_rate  ");
				strQry.Append(" FROM dbo.GetExchangeRate('"+strEnterpriseID+"', '"+CurrenyCode+"', 'F', '"+RateDate+"' )" );  

				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsFreightCollectChargesDAL", "GetExchangeRate", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}
	}
}
