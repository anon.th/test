using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for ShipmentManager.
	/// </summary>
	public class DomesticShipmentManager
	{
		public DomesticShipmentManager()
		{
		}
	
		public static DataSet GetEmptyDomesticShipDS()
		{
			DataTable dtDomesticShipment = new DataTable();
			dtDomesticShipment.Columns.Add(new DataColumn("booking_no", typeof(int)));
			dtDomesticShipment.Columns.Add(new DataColumn("consignment_no", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("ref_no", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("booking_datetime", typeof(DateTime)));
			dtDomesticShipment.Columns.Add(new DataColumn("payerid", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payer_type", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("new_account", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payer_name", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payer_address1", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payer_address2", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payer_zipcode", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payer_country", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payer_telephone", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payer_fax", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("payment_mode", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("sender_name", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("sender_address1", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("sender_address2", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("sender_zipcode", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("sender_country", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("sender_telephone", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("sender_fax", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("sender_contact_person", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("recipient_name", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("recipient_address1", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("recipient_address2", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("recipient_zipcode", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("recipient_country", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("tot_wt", typeof(decimal)));
			dtDomesticShipment.Columns.Add(new DataColumn("recipient_telephone", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("tot_dim_wt", typeof(decimal)));
			dtDomesticShipment.Columns.Add(new DataColumn("recipient_fax", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("tot_pkg", typeof(int)));
			dtDomesticShipment.Columns.Add(new DataColumn("service_code", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("recipient_contact_person", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("declare_value", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("chargeable_wt", typeof(decimal)));
			dtDomesticShipment.Columns.Add(new DataColumn("insurance_surcharge", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("max_insurance_cover", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("act_pickup_datetime", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("commodity_code", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("est_delivery_datetime", typeof(DateTime)));
			dtDomesticShipment.Columns.Add(new DataColumn("percent_dv_additional", typeof(decimal)));
			dtDomesticShipment.Columns.Add(new DataColumn("act_delivery_date", typeof(DateTime)));
			dtDomesticShipment.Columns.Add(new DataColumn("payment_type", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("return_pod_slip", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("shipment_type", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("origin_state_code", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("invoice_no", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("destination_state_code", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("invoice_amt", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("tot_vas_surcharge", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("debit_amt", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("credit_amt", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("cash_collected", typeof(decimal)));//money
			dtDomesticShipment.Columns.Add(new DataColumn("last_status_code", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("last_exception_code", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("last_status_datetime", typeof(DateTime)));
			dtDomesticShipment.Columns.Add(new DataColumn("shpt_manifest_datetime", typeof(DateTime)));
			dtDomesticShipment.Columns.Add(new DataColumn("delivery_manifested", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("route_code", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("quotation_no", typeof(string)));
			dtDomesticShipment.Columns.Add(new DataColumn("quotation_version", typeof(int)));
//Add By Aoo
			dtDomesticShipment.Columns.Add(new DataColumn("mbg_amount",typeof(decimal)));
			dtDomesticShipment.Columns.Add(new DataColumn("total_excp_charges",typeof(decimal)));
			dtDomesticShipment.Columns.Add(new DataColumn("invoice_adjustment",typeof(decimal)));
			dtDomesticShipment.Columns.Add(new DataColumn("total_cons_revenue",typeof(decimal)));
			dtDomesticShipment.Columns.Add(new DataColumn("invoice_date",typeof(DateTime)));
			DataRow drEach = dtDomesticShipment.NewRow();
			//drEach[0] = "";
			
			drEach["booking_no"] = 0;
			drEach["consignment_no"] = "";
			drEach["ref_no"] = "";
			drEach["booking_datetime"] = DateTime.Now;
			drEach["payerid"] = "";
			drEach["payer_type"] = "";
			drEach["new_account"] = "";
			drEach["payer_name"] = "";
			drEach["payer_address1"] = "";
			drEach["payer_address2"] = "";
			drEach["payer_zipcode"] = "";
			drEach["payer_country"] = "";
			drEach["payer_telephone"] = "";
			drEach["payer_fax"] = "";
			drEach["payment_mode"] = "";
			drEach["payment_mode"] = "";
			drEach["sender_name"] = "";
			drEach["sender_address1"] = "";
			drEach["sender_address2"] = "";
			drEach["sender_zipcode"] = "";
			drEach["sender_country"] = "";
			drEach["sender_telephone"] = "";
			drEach["sender_fax"] = "";
			drEach["sender_contact_person"] = "";
			drEach["recipient_name"] = "";
			drEach["recipient_address1"] = "";
			drEach["recipient_address2"] = "";
			drEach["recipient_zipcode"] = "";
			drEach["recipient_country"] = "";
			drEach["tot_wt"] = 0;
			drEach["recipient_telephone"] = "";
			drEach["tot_dim_wt"] = 0;
			drEach["recipient_fax"] = "";
			drEach["tot_pkg"] = 0;
			drEach["service_code"] = "";
			drEach["recipient_contact_person"] = "";
			drEach["declare_value"] = 0;
			drEach["chargeable_wt"] = 0;
			drEach["insurance_surcharge"] = 0;
			drEach["max_insurance_cover"] = 0;
			drEach["act_pickup_datetime"] = DateTime.Now;
			drEach["commodity_code"] = "";
			drEach["est_delivery_datetime"] = DateTime.Now;
			drEach["percent_dv_additional"] = 0;
			drEach["act_delivery_date"] = DateTime.Now;
			drEach["payment_type"] = "";
			drEach["return_pod_slip"] = "";
			drEach["shipment_type"]= "";
			drEach["origin_state_code"] = "";
			drEach["invoice_no"] = "";
			drEach["destination_state_code"] = "";
			drEach["invoice_amt"] = 0;
			drEach["tot_vas_surcharge"] = 0;
			drEach["debit_amt"] = 0;
			drEach["credit_amt"] = 0;
			drEach["cash_collected"] = 0;
			drEach["last_status_code"] = "";
			drEach["last_exception_code"] = "";
			drEach["last_status_datetime"] = DateTime.Now;
			drEach["shpt_manifest_datetime"] = DateTime.Now;
			drEach["delivery_manifested"] = "";
			drEach["route_code"] = "";
			drEach["quotation_no"] = "";
			drEach["quotation_version"] = 0;

			//By Aoo
			drEach["mbg_amount"] = 0;
			drEach["total_excp_charges"] = 0;
			drEach["invoice_adjustment"] = 0;
			drEach["total_cons_revenue"] = 0;
			drEach["invoice_date"] = DateTime.Now;


			dtDomesticShipment.Rows.Add(drEach);
			DataSet dsDomesticShipment = new DataSet();
			dsDomesticShipment.Tables.Add(dtDomesticShipment);
			return  dsDomesticShipment;	
		}

		public static int AddDomesticShipment(String appID, String enterpriseID, DataSet dsDomesticShipment)
		{
			String strQry = null;
			IDbCommand dbCmd = null;
			int rowsEffected = 0;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","AddDomesticShipment","EDSM101","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			if (dsDomesticShipment == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","AddDomesticShipment","EDSM102","DataSet is null!!");
				throw new ApplicationException("The DataSet is null",null);
			}
			
			DataRow drEach = dsDomesticShipment.Tables[0].Rows[0];
			
			strQry = "insert into shipment(applicationid,enterpriseid,booking_no,serial_no,";
			strQry = strQry + "consignment_no,ref_no,booking_datetime,payerid,payer_type,";
			strQry = strQry + "new_account,payer_name,payer_address1,payer_address2,payer_zipcode,";
			strQry = strQry + "payer_country,payer_telephone,payer_fax,";
			strQry = strQry + "payment_mode,sender_name,sender_address1,sender_address2,sender_zipcode,";
			strQry = strQry + "sender_country,sender_telephone,sender_fax,";
			strQry = strQry + "sender_contact_person,recipient_name,recipient_address1,recipient_address2,";
			strQry = strQry + "recipient_zipcode,recipient_country,tot_wt,recipient_telephone,tot_dim_wt,";
			strQry = strQry + "recipient_fax,tot_pkg,service_code,recipient_contact_person,declare_value,";
			strQry = strQry + "chargeable_wt,insurance_surcharge,max_insurance_cover,";
			strQry = strQry + "act_pickup_datetime,";
			//strQry = strQry + "commodity_code,"; Foreign Key
			strQry = strQry + "est_delivery_datetime,percent_dv_additional,";
			strQry = strQry + "act_delivery_date,";
			//strQry = strQry + "payment_type,";
			//strQry = strQry + "return_pod_slip,";
			//strQry = strQry + "shipment_type,";
			strQry = strQry + "origin_state_code,";
			strQry = strQry + "invoice_no,destination_state_code,invoice_amt,tot_freight_charge,invoice_date,";
			strQry = strQry + "tot_vas_surcharge,debit_amt,credit_amt,cash_collected,last_status_code,";
			strQry = strQry + "last_exception_code,last_status_datetime,shpt_manifest_datetime,";
			//strQry = strQry + "delivery_manifested,";
			strQry = strQry + "route_code,quotation_no,quotation_version";
			strQry = strQry + ")values('"+appID+"',"+"'"+enterpriseID+"',";
			//generate the booking no for adhoc 
			//long iBooking_no = (long)Counter.GetNext(appID,enterpriseID,"booking_number");
			long iBooking_no = 208;
			strQry = strQry +iBooking_no+",";
			//for all adhoc shipments the serial no is 1
			strQry = strQry +1+",";
			String strConsgNo = "";
			if(!drEach["consignment_no"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strConsgNo = (String)drEach["consignment_no"];
			}
			strQry = strQry + "'"+strConsgNo+"',";
			
			String strRefNo = "";
			if(!drEach["ref_no"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strRefNo = (String)drEach["ref_no"];
			}
			strQry = strQry + "'"+strRefNo+"',";

			DateTime dtBookingDtTime = DateTime.Now;
			if(!drEach["booking_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dtBookingDtTime = Convert.ToDateTime(drEach["booking_datetime"]);
			}
			strQry = strQry + "'"+dtBookingDtTime+"',";

			string strPayerID = "";
			if(!drEach["payerid"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strPayerID = (String)drEach["payerid"];
			}
			strQry = strQry + "'"+strPayerID+"',";

			String strPayerType = "";
			if(!drEach["payer_type"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strPayerType = (String)drEach["payer_type"];
			}
			strQry = strQry + "'"+strPayerType+"',";

			String strNewAccount = "";
			if(!drEach["new_account"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strNewAccount = (String)drEach["new_account"];
			}
			strQry = strQry + "'"+strNewAccount+"',";

			String strPayerName = "";
			if(!drEach["payer_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strPayerName = (String)drEach["payer_name"];
			}
			strQry = strQry + "N'"+strPayerName+"',";

			String strPayerAddress1 = "";
			if(!drEach["payer_address1"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strPayerAddress1 = (String)drEach["payer_address1"];
			}

			strQry = strQry + "N'"+strPayerAddress1+"',";

			String strPayerAddress2 = "";
			if(!drEach["payer_address2"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strPayerAddress2 = (String)drEach["payer_address2"];
			}
			strQry = strQry + "N'"+strPayerAddress2+"',";

			String strPayerZipCode = "";
			if(!drEach["payer_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strPayerZipCode = (String)drEach["payer_zipcode"];
			}
			strQry = strQry + "'"+strPayerZipCode+"',";
			
			String strPayerCountry = "";
			if(!drEach["payer_country"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strPayerCountry = (String)drEach["payer_country"];
			}
			strQry = strQry + "'"+strPayerCountry+"',";

			String strPayerTelephone = "";
			if(!drEach["payer_telephone"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strPayerTelephone = (String)drEach["payer_telephone"];
			}
			strQry = strQry + "'"+strPayerTelephone+"',";

			String strPayerFax = "";
			if(!drEach["payer_fax"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strPayerFax = (String)drEach["payer_fax"];
			}
			strQry = strQry + "'"+strPayerFax+"',";

			String strPaymentMode = "";
			if(!drEach["payment_mode"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strPaymentMode = (String)drEach["payment_mode"];
			}
			strQry = strQry + "'"+strPaymentMode+"',";

			String strSenderName = "";
			if(!drEach["sender_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strSenderName = (String)drEach["sender_name"];
			}
			strQry = strQry + "N'"+strSenderName+"',";

			String strSenderAddress1 = "";
			if(!drEach["sender_address1"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strSenderAddress1 = (String)drEach["sender_address1"]; 
			}
			strQry = strQry + "N'"+strSenderAddress1+"',";

			String strSenderAddress2 = "";
			if(!drEach["sender_address2"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strSenderAddress2 = (String)drEach["sender_address2"];
			}
			strQry = strQry + "N'"+strSenderAddress2+"',";

			String strSenderZipCode = "";
			if(!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strSenderZipCode = (String)drEach["sender_zipcode"];
			}
			strQry = strQry + "'"+strSenderZipCode+"',";

			String strSenderCountry = "";
			if(!drEach["sender_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strSenderCountry = (String)drEach["sender_country"];
			}
			strQry = strQry + "'"+strSenderCountry+"',";

			String strSenderTelephone = "";
			if(!drEach["sender_telephone"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strSenderTelephone = (String)drEach["sender_telephone"];
			}
			strQry = strQry + "'"+strSenderTelephone+"',";

			String strSenderFax = "";
			if(!drEach["sender_fax"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strSenderFax = (String)drEach["sender_fax"];
			}
			strQry = strQry + "'"+strSenderFax+"',";

			String strSenderContactPerson = "";
			if(!drEach["sender_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strSenderContactPerson = (String)drEach["sender_contact_person"];
			}
			strQry = strQry + "N'"+strSenderContactPerson+"',";

			String strRecipientName = "";
			if(!drEach["recipient_name"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strRecipientName = (String)drEach["recipient_name"];
			}
			strQry = strQry + "N'"+strRecipientName+"',";

			String strRecipientAddress1 = "";
			if(!drEach["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strRecipientAddress1 = (String)drEach["recipient_address1"];
			}
			strQry = strQry + "N'"+strRecipientAddress1+"',";

			String strRecipientAddress2 = "";
			if(!drEach["recipient_address1"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strRecipientAddress2 = (String)drEach["recipient_address2"];
			}
			strQry = strQry + "N'"+strRecipientAddress2+"',";

			String strRecipientZipCode = "";
			if(!drEach["recipient_zipcode"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strRecipientZipCode = (String)drEach["recipient_zipcode"];
			}
			strQry = strQry + "'"+strRecipientZipCode+"',";

			String strRecipientCountry = "";
			if(!drEach["recipient_country"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strRecipientCountry = (String)drEach["recipient_country"];
			}
			strQry = strQry + "'"+strRecipientCountry+"',";

			decimal decTotWt = 0;
			if(!drEach["tot_wt"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				decTotWt = (decimal)drEach["tot_wt"];
			}
			strQry = strQry +decTotWt;

			String strRecipientTelephone = "";
			if(!drEach["recipient_telephone"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strRecipientTelephone = (String)drEach["recipient_telephone"];

			}
			strQry = strQry + ",'"+strRecipientTelephone+"',";

			decimal decTotDimWt = 0;
			if(!drEach["tot_dim_wt"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				decTotDimWt = (decimal)drEach["tot_dim_wt"]; 
			}
			strQry = strQry +decTotDimWt;
			
			String strRecipientFax = "";
			if(!drEach["recipient_fax"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strRecipientFax = (String)drEach["recipient_fax"];
			}

			strQry = strQry + ",'"+strRecipientFax+"',";

			int iTotPkg = 0;
			if(!drEach["tot_pkg"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				iTotPkg = (int)drEach["tot_pkg"];
			}
			strQry = strQry +iTotPkg;

			String strServiceCode = "";
			if(!drEach["service_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strServiceCode = (String)drEach["service_code"];
			}
			strQry = strQry + ",'"+strServiceCode+"',";

			String strRecipientContactPerson = "";
			if(!drEach["recipient_contact_person"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strRecipientContactPerson = (String)drEach["recipient_contact_person"];
			}
			strQry = strQry + "N'"+strRecipientContactPerson+"',";

			decimal decDeclareValue = 0;
			if(!drEach["declare_value"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				decDeclareValue = (decimal)drEach["declare_value"];
			}
			strQry = strQry +decDeclareValue+",";

			decimal decChargeableWt = 0;
			if(!drEach["chargeable_wt"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				decChargeableWt = (decimal)drEach["chargeable_wt"];
			}
			strQry = strQry +decChargeableWt+",";

			decimal decInsuranceSurcharge = 0;
			if(!drEach["insurance_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				decInsuranceSurcharge = (decimal)drEach["insurance_surcharge"];
			}
			strQry = strQry +decInsuranceSurcharge+",";

			decimal decMaxInsuranceCover = 0;
			if(!drEach["max_insurance_cover"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				decMaxInsuranceCover = (decimal)drEach["max_insurance_cover"];
			}
			strQry = strQry +decMaxInsuranceCover+",";

			DateTime dtActPickup = DateTime.Now;
			if(!drEach["act_pickup_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{	
				dtActPickup = Convert.ToDateTime(drEach["act_pickup_datetime"]);
			}
			strQry = strQry + "'"+dtActPickup+"',";

			/*String strCommodityCode = "";
			if(!drEach["commodity_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strCommodityCode = (String)drEach["commodity_code"];
			}
			strQry = strQry + "'"+strCommodityCode+"',";*/

			DateTime dtEstDeliveryDatetime = DateTime.Now;
			if(!drEach["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dtEstDeliveryDatetime = (DateTime)drEach["est_delivery_datetime"];
			}
			strQry = strQry + "'"+dtEstDeliveryDatetime+"',";

			decimal decPercentDvAdditional = 0;
			if(!drEach["est_delivery_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				decPercentDvAdditional = Convert.ToDecimal(drEach["percent_dv_additional"]);
			}
			strQry = strQry +decPercentDvAdditional;

			DateTime dtActDeliveryDate = DateTime.Now;
			if(!drEach["act_delivery_date"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dtActDeliveryDate = Convert.ToDateTime(drEach["act_delivery_date"]);
			}
			strQry = strQry + ",'"+dtActDeliveryDate+"',";

			/*String strPaymentType = "";
			if(!drEach["payment_type"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strPaymentType = (String)drEach["payment_type"];
			}
			strQry = strQry + "'"+strPaymentType+"',";

			String strReturnPODSlip = "";
			if(!drEach["return_pod_slip"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strReturnPODSlip = (String)drEach["return_pod_slip"];
			}
			strQry = strQry + "'"+strReturnPODSlip+"',";

			String strShipmentType = "";
			if(!drEach["shipment_type"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{	
				strShipmentType = (String)drEach["shipment_type"];
			}
			strQry = strQry + "'"+strShipmentType+"',";*/

			String strOriginStateCode = "";
			if(!drEach["origin_state_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strOriginStateCode = (String)drEach["origin_state_code"];
			}
			strQry = strQry + "'"+strOriginStateCode+"',";

			String strInvoiceNo = "";
			if(!drEach["invoice_no"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strInvoiceNo = (String)drEach["invoice_no"];
			}
			strQry = strQry + "'"+strInvoiceNo+"',";

			String strDestinationStateCode = "";
			if(!drEach["destination_state_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strDestinationStateCode = (String)drEach["destination_state_code"];
			}
			strQry = strQry + "'"+strDestinationStateCode+"',";

			decimal decInvoiceAmt = 0;
			if(!drEach["invoice_amt"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				decInvoiceAmt = Convert.ToDecimal(drEach["invoice_amt"]);
			}
			strQry = strQry +decInvoiceAmt+",";

			int iTotFreightCharge = 0;
			DateTime dtInvDt = DateTime.Now;
			strQry = strQry +iTotFreightCharge+",'"+dtInvDt+"',";
			//tot_freight_charge
			//invoice_date
			decimal decTotVASSurcharge = 0;
			if(!drEach["tot_vas_surcharge"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				decTotVASSurcharge = Convert.ToDecimal(drEach["tot_vas_surcharge"]);
			}
			strQry = strQry +decTotVASSurcharge+",";
			
			decimal decDebitAmt = 0;
			if(!drEach["debit_amt"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				decDebitAmt = Convert.ToDecimal(drEach["debit_amt"]);
			}
			strQry = strQry +decDebitAmt+",";

			decimal decCreditAmt = 0;
			if(!drEach["credit_amt"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				decCreditAmt = Convert.ToDecimal(drEach["credit_amt"]);
			}
			strQry = strQry +decCreditAmt+",";

			decimal decCashCollected = 0;
			if(!drEach["cash_collected"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				decCashCollected = Convert.ToDecimal(drEach["cash_collected"]);
			}
			strQry = strQry +decCashCollected+",";

			String strLastStatusCode = "";
			if(!drEach["last_status_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strLastStatusCode = (String)drEach["last_status_code"];
			}
			strQry = strQry + "'"+strLastStatusCode+"',";

			String strLastExceptionCode = "";
			if(!drEach["last_exception_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strLastExceptionCode = (String)drEach["last_exception_code"];
			}
			strQry = strQry + "'"+strLastExceptionCode+"',";

			DateTime dtLastStatusDateTime = DateTime.Now;
			if(!drEach["last_status_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dtLastStatusDateTime = Convert.ToDateTime(drEach["last_status_datetime"]);
			}
			strQry = strQry + "'"+dtLastStatusDateTime+"',";

			DateTime dtShptManifestDateTime = DateTime.Now;
			if(!drEach["shpt_manifest_datetime"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				dtShptManifestDateTime = Convert.ToDateTime(drEach["shpt_manifest_datetime"]);
			}
			strQry = strQry + "'"+dtShptManifestDateTime+"',";

			/*String strDeliveryManifested = "";
			if(!drEach["delivery_manifested"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strDeliveryManifested = (String)drEach["delivery_manifested"];
			}
			strQry = strQry + "'"+strDeliveryManifested+"',";*/

			String strRouteCode = "";
			if(!drEach["route_code"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strRouteCode = (String)drEach["route_code"];
			}
			strQry = strQry + "'"+strRouteCode+"',";

			String strQuotationNo = "";
			if(!drEach["quotation_no"].GetType().Equals(System.Type.GetType("System.DBNull")))
			{
				strQuotationNo = (String)drEach["quotation_no"];
			}
			strQry = strQry + "'"+strQuotationNo+"',";

			int iQuotationVersion = (int)drEach["quotation_version"];
			
			strQry = strQry +iQuotationVersion+")";



			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				rowsEffected = dbCon.ExecuteNonQuery(dbCmd);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","AddDomesticShipment","","Error ");
				throw appException;
			}
			return rowsEffected;
		}
		
		public static DataSet GetFromPickUp(String appID,String enterpriseID,int iBookingNo)
		{
			String strQry = null;
			IDbCommand dbCmd = null;
			DataSet dsDisplayShipmnt = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			strQry = "select * from pickup_request where applicationid = ";
			strQry = strQry + "'"+appID+"'";
			strQry = strQry + " and enterpriseID = "+"'"+enterpriseID+"'";
			strQry = strQry + " and booking_no = "+iBookingNo;

			dbCmd = dbCon.CreateCommand(strQry,CommandType.Text);
			try
			{
				dsDisplayShipmnt = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","","Error ");
				throw appException;
			}
			return dsDisplayShipmnt;
		}

		public static DataSet GetSenderReceipt(string appID,string enterpriseID,string strSndRcpName,string strCustID,string strZipcode,string strType)
		{
			IDbCommand dbCmd = null;
			DataSet dsDisplayShipmnt = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(appID,enterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","EDSM102","DbConnection object is null!!");
				throw new ApplicationException("DbConnection object is null",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry.Append("SELECT TOP 1 b.state_name, pickup_route=b.route_code, a.* ");
			strQry.Append("FROM dbo.Customer_Snd_Rec a CROSS APPLY dbo.GetZipcodeInfo(1, a.enterpriseid, a.zipcode) b ");
			strQry.Append("WHERE snd_rec_type IN ('B', '" + strType + "')");
			if(strSndRcpName != null && strSndRcpName.Trim() != "")
			{
				strQry.Append(" and snd_rec_name   = N'");
				strQry.Append(Utility.ReplaceSingleQuote(strSndRcpName));
				strQry.Append("' ");
			}
			if(strCustID != null && strCustID.Trim () != "")
			{
				strQry.Append(" and custid like '%");
				strQry.Append(Utility.ReplaceSingleQuote(strCustID));
				strQry.Append("%' ");
			}
			if(strZipcode != null && strZipcode.Trim() != "")
			{
				strQry.Append(" and a.zipcode like '%");
				strQry.Append(Utility.ReplaceSingleQuote(strZipcode));
				strQry.Append("%' ");
			}

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
			try
			{
				dsDisplayShipmnt = (DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(System.ApplicationException appException)
			{
				Logger.LogTraceError("DomesticShipmentManager","GetFromPickUp","","Error ");
				throw appException;
			}
			return dsDisplayShipmnt;
		}
	}
}
