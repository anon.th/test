using System;
using System.Data;
using System.Collections;
using System.Text;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using com.ties.classes;
using System.Data.SqlClient;


namespace TIESDAL
{
	/// <summary>
	/// Summary description for CustomsCheckRequisitionDAL.
	/// </summary>
	public class CustomsCheckRequisitionDAL
	{
		public CustomsCheckRequisitionDAL()
		{
			//
			// TODO: Add constructor logic here
			// 
		}


		public static System.Data.DataSet GetChequeRequisitionTypes(string strAppID,string strEnterpriseID )
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsCheckRequisitionDAL","GetChequeRequisitionTypes","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{ 
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT DropDownListDisplayValue, DefaultValue, [Description], CodedValue ");
				strQry.Append(" FROM dbo.GetCustomsSpecialConfiguration( '"+strEnterpriseID+"', 'ChequeRequisitionTypes' , 0) " ); 
				 
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsCheckRequisitionDAL", "GetChequeRequisitionTypes", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}


		public static System.Data.DataSet GetChequeRequisitionTypes(string strAppID,string strEnterpriseID,int blankFirstRow)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsCheckRequisitionDAL","GetChequeRequisitionTypes","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{ 
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT DropDownListDisplayValue, DefaultValue, [Description], CodedValue ");
				strQry.Append(" FROM dbo.GetCustomsSpecialConfiguration( '"+strEnterpriseID+"', 'ChequeRequisitionTypes' , "+blankFirstRow.ToString()+") " ); 
				 
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsCheckRequisitionDAL", "GetChequeRequisitionTypes", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}


		public static System.Data.DataSet GetChequeReqDataToReport(string strAppID,string strEnterpriseID, string ChequeRequisitionNo)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsCheckRequisitionDAL","GetChequeRequisitionTypes","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{ 
				StringBuilder strQry = new StringBuilder();
				strQry.Append("SELECT applicationid, enterpriseid, EnterpriseName, EnterpriseAddress1, EnterpriseAddress2, ChequeRequisitionNo");
				strQry.Append(", ChequeRequisitionStatus, Payee, ChequeNo, ChequeIssueDate, ReceiptNo, ReceiptDate");
				strQry.Append(", PreparedBy, PreparedDT, ApprovedBy, ApprovedDT, CompletedBy, CompletedDT");
				strQry.Append(", InvoiceNo, Client, DutyTax, EPF, Total, TotalChequeReq, PrintedDT FROM dbo.v_Customs_ChequeReq_Normal");
				strQry.Append(" WHERE enterpriseid = '" + strEnterpriseID + "' AND ChequeRequisitionNo = " + ChequeRequisitionNo);
				strQry.Append(" ORDER BY InvoiceNo;");
			
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsCheckRequisitionDAL", "GetChequeReqDataToReport", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}

		public static System.Data.DataSet GetChequeReqFormalDataToReport(string strAppID,string strEnterpriseID, string ChequeRequisitionNo)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsCheckRequisitionDAL","GetChequeRequisitionTypes","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append("SELECT applicationid, enterpriseid, EnterpriseName, EnterpriseAddress1, EnterpriseAddress2");
				strQry.Append(",ChequeRequisitionNo, ChequeRequisitionStatus, Payee, ChequeNo, ChequeIssueDate");
				strQry.Append(",ReceiptNo, ReceiptDate");
				strQry.Append(",PreparedBy, PreparedDT, ApprovedBy, ApprovedDT, CompletedBy, CompletedDT");
				strQry.Append(",InvoiceNo, Client, Assessment, AssessmentDate, DutyTax, EPF, Total, TotalChequeReq, PrintedDT");
				strQry.Append(" FROM dbo.v_Customs_ChequeReq_Formal");
				strQry.Append(" WHERE enterpriseid = '" + strEnterpriseID + "' AND ChequeRequisitionNo = " + ChequeRequisitionNo);
			
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsCheckRequisitionDAL", "GetChequeReqDataToReport", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 
		}

		public static System.Data.DataSet GetChequeReqITFDataToReport(string strAppID,string strEnterpriseID, string ChequeRequisitionNo)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsCheckRequisitionDAL","GetChequeRequisitionTypes","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append("SELECT applicationid, enterpriseid, EnterpriseName, EnterpriseAddress1, EnterpriseAddress2");
				strQry.Append(" ,ChequeRequisitionNo, ChequeRequisitionStatus, Payee, ChequeNo, ChequeIssueDate");
				strQry.Append(" ,ReceiptNo, ReceiptDate, PreparedBy, PreparedDT, ApprovedBy, ApprovedDT, CompletedBy, CompletedDT");
				strQry.Append(" ,MasterAWBNumber, ITFAmount, FreightCollectAmount, Total, TotalChequeReq, PrintedDT, FolioNumber");
				strQry.Append(" FROM dbo.v_Customs_ChequeReq_ITF");
				strQry.Append(" WHERE enterpriseid = '" + strEnterpriseID + "' AND ChequeRequisitionNo = " + ChequeRequisitionNo);

				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsCheckRequisitionDAL", "GetChequeReqDataToReport", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 
		}

		public static System.Data.DataSet GetChequeReqAdhocDataToReport(string strAppID,string strEnterpriseID, string ChequeRequisitionNo)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsCheckRequisitionDAL","GetChequeRequisitionTypes","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{ 
				StringBuilder strQry = new StringBuilder();
				strQry.Append("SELECT applicationid, enterpriseid, EnterpriseName, EnterpriseAddress1, EnterpriseAddress2, ChequeRequisitionNo");
				strQry.Append(", ChequeRequisitionStatus, Payee, ChequeNo, ChequeIssueDate, ReceiptNo, ReceiptDate");
				strQry.Append(", PreparedBy, PreparedDT, ApprovedBy, ApprovedDT, CompletedBy, CompletedDT");
				strQry.Append(", InvoiceNo, Client, DisbursementType, Amount, TotalChequeReq, PrintedDT FROM dbo.v_Customs_ChequeReq_Adhoc");
				strQry.Append(" WHERE enterpriseid = '" + strEnterpriseID + "' AND ChequeRequisitionNo = " + ChequeRequisitionNo);
				strQry.Append(" ORDER BY InvoiceNo;");
			
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomsCheckRequisitionDAL", "GetChequeReqDataToReport", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}
 

		public static System.Data.DataSet Customs_ChequeRequisition(string strAppID, string strEnterpriseID, string userloggedin, System.Data.DataTable dtParams)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomsCheckRequisitionDAL","CustomsCheckRequisitionDAL","Customs_ChequeRequisition","DbConnection object is null!!");
				return null;
			}
			string action = dtParams.Rows[0]["action"].ToString();
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			if(action == "0")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@action",action));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));				
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
				if(dtParams.Rows[0]["requisition_no"] != DBNull.Value  && dtParams.Rows[0]["requisition_no"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@requisition_no",dtParams.Rows[0]["requisition_no"].ToString()));
				} 
				if(dtParams.Rows[0]["Cheque_No"] != DBNull.Value  && dtParams.Rows[0]["Cheque_No"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Cheque_No",dtParams.Rows[0]["Cheque_No"].ToString()));
				}
				if(dtParams.Rows[0]["Receipt_No"] != DBNull.Value  && dtParams.Rows[0]["Receipt_No"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Receipt_No",dtParams.Rows[0]["Receipt_No"].ToString()));
				}
			}
			else if(action == "1" || action == "2")
			{			 
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@action",action));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));				
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
				if(dtParams.Rows[0]["requisition_no"] != DBNull.Value  && dtParams.Rows[0]["requisition_no"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@requisition_no",dtParams.Rows[0]["requisition_no"].ToString()));
				} 
				if(dtParams.Rows[0]["Cheque_No"] != DBNull.Value  && dtParams.Rows[0]["Cheque_No"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Cheque_No",dtParams.Rows[0]["Cheque_No"].ToString()));
				} 
				if(dtParams.Rows[0]["Receipt_No"] != DBNull.Value  && dtParams.Rows[0]["Receipt_No"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Receipt_No",dtParams.Rows[0]["Receipt_No"].ToString()));
				}
				if(dtParams.Rows[0]["payee"] != DBNull.Value  && dtParams.Rows[0]["payee"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payee",dtParams.Rows[0]["payee"].ToString()));
				}
				if(dtParams.Rows[0]["ChequeReq_Type"] != DBNull.Value  && dtParams.Rows[0]["ChequeReq_Type"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ChequeReq_Type",dtParams.Rows[0]["ChequeReq_Type"].ToString()));
				}
				if(dtParams.Rows[0]["Date_Cheque_Issued"] != DBNull.Value  && dtParams.Rows[0]["Date_Cheque_Issued"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Date_Cheque_Issued",dtParams.Rows[0]["Date_Cheque_Issued"].ToString()));
				}
				if(dtParams.Rows[0]["Receipt_Date"] != DBNull.Value  && dtParams.Rows[0]["Receipt_Date"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@Receipt_Date",dtParams.Rows[0]["Receipt_Date"].ToString()));
				}
			}
			 
			
			result = (DataSet)dbCon.ExecuteProcedure("Customs_ChequeRequisition",storedParams,ReturnType.DataSetType);
			return result;
		}


		public static System.Data.DataSet Customs_ChequeRequisitionDetails(string strAppID, string strEnterpriseID, string userloggedin, System.Data.DataTable dtParams)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomsCheckRequisitionDAL","CustomsCheckRequisitionDAL","Customs_ChequeRequisitionDetails","DbConnection object is null!!");
				return null;
			}
			string action = dtParams.Rows[0]["action"].ToString();
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			//			if(action == "0")
			//			{
			//				EXEC dbo.Customs_ChequeRequisitionDetails 
			//@action = 0 -- 0=insert, 1=update, 2=delete
			//,@enterpriseid = 'PNGAF' 
			//,@userloggedin = 'DCAMPS'
			//,@requisition_no = 10005 
			//,@consignment_no = 'DC004'
			//,@payerid = '10260' 
			//,@Invoice_No = 10000007
			//--,@OtherDisbursement dbo.Dollar = NULL 
			//--,@DisbursementType dbo.Base_Code = NULL; 

			storedParams.Add(new System.Data.SqlClient.SqlParameter("@action",action));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));				
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
			if(dtParams.Rows[0]["requisition_no"] != DBNull.Value  && dtParams.Rows[0]["requisition_no"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@requisition_no",dtParams.Rows[0]["requisition_no"].ToString()));
			} 
			if(dtParams.Rows[0]["consignment_no"] != DBNull.Value  && dtParams.Rows[0]["consignment_no"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",dtParams.Rows[0]["consignment_no"].ToString()));
			}
			if(action != "0")
			{
				if(dtParams.Rows[0]["payerid"] != DBNull.Value  && dtParams.Rows[0]["payerid"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid",dtParams.Rows[0]["payerid"].ToString()));
				}
			}
			else
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid",DBNull.Value));
			}
			
//			if(dtParams.Rows[0]["payee"] != DBNull.Value  && dtParams.Rows[0]["payee"].ToString() !="")
//			{
//				storedParams.Add(new System.Data.SqlClient.SqlParameter("@payee",dtParams.Rows[0]["payee"].ToString()));
//			}
			if(dtParams.Rows[0]["Invoice_No"] != DBNull.Value  && dtParams.Rows[0]["Invoice_No"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Invoice_No",dtParams.Rows[0]["Invoice_No"].ToString()));
			}
			if(dtParams.Rows[0]["OtherDisbursement"] != DBNull.Value  && dtParams.Rows[0]["OtherDisbursement"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@OtherDisbursement",dtParams.Rows[0]["OtherDisbursement"].ToString()));
			}
			if(dtParams.Rows[0]["DisbursementType"] != DBNull.Value  && dtParams.Rows[0]["DisbursementType"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@DisbursementType",dtParams.Rows[0]["DisbursementType"].ToString()));
			}
			if(dtParams.Rows[0]["AssessmentNumber"] != DBNull.Value  && dtParams.Rows[0]["AssessmentNumber"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@AssessmentNumber",dtParams.Rows[0]["AssessmentNumber"].ToString()));
			}
			if(dtParams.Rows[0]["AssessmentAmount"] != DBNull.Value  && dtParams.Rows[0]["AssessmentAmount"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@AssessmentAmount",dtParams.Rows[0]["AssessmentAmount"].ToString()));
			}
			if(dtParams.Rows[0]["AssessmentDate"] != DBNull.Value  && dtParams.Rows[0]["AssessmentDate"].ToString() !="")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@AssessmentDate",dtParams.Rows[0]["AssessmentDate"].ToString()));
			}
			 
			
			result = (DataSet)dbCon.ExecuteProcedure("dbo.Customs_ChequeRequisitionDetails",storedParams,ReturnType.DataSetType);
			return result;
		}


		public static System.Data.DataSet Customs_QueryInvoiceCharges(string strAppID, string strEnterpriseID, string userloggedin, System.Data.DataTable dtParams)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomsCheckRequisitionDAL","CustomsCheckRequisitionDAL","Customs_QueryInvoiceCharges","DbConnection object is null!!");
				return null;
			}
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			foreach(System.Data.DataColumn col in dtParams.Columns)
			{
				string colName = col.ColumnName;
				if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));								
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,DBNull.Value));									
				}
			}
			//storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));		
			
			result = (DataSet)dbCon.ExecuteProcedure("dbo.Customs_QueryInvoiceCharges",storedParams,ReturnType.DataSetType);
			return result;
		}


		public static System.Data.DataSet Customs_RetrieveInvoicesToChequeReq(string strAppID, string strEnterpriseID, string userloggedin, Int64 requisition_no,string Invoice_Nos,int Charge_Type)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomsCheckRequisitionDAL","CustomsCheckRequisitionDAL","Customs_RetrieveInvoicesToChequeReq","DbConnection object is null!!");
				return null;
			}
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));				
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@requisition_no",requisition_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Invoice_Nos",Invoice_Nos));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@Charge_Type",Charge_Type));
			result = (DataSet)dbCon.ExecuteProcedure("dbo.Customs_RetrieveInvoicesToChequeReq",storedParams,ReturnType.DataSetType);
			return result;
		}

		public static System.Data.DataTable CustomsChequeRequisition()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("action");
			dt.Columns.Add("enterpriseid");
			dt.Columns.Add("userloggedin");
			dt.Columns.Add("requisition_no");
			dt.Columns.Add("Cheque_No");
			dt.Columns.Add("Receipt_No");
			dt.Columns.Add("payee");
			dt.Columns.Add("payerid");
			dt.Columns.Add("ChequeReq_Type");
			dt.Columns.Add("Date_Cheque_Issued");
			dt.Columns.Add("Receipt_Date"); 
		
			// For Detail
			dt.Columns.Add("consignment_no"); 
			dt.Columns.Add("Invoice_No");
			dt.Columns.Add("OtherDisbursement");
			dt.Columns.Add("DisbursementType");  

			dt.Columns.Add("AssessmentNumber"); 
			dt.Columns.Add("AssessmentAmount");
			dt.Columns.Add("AssessmentDate");
			dt.Columns.Add("EntryProcessingFee");  
			dt.Columns.Add("DisbursementTotal");  

			return dt;
		}



		public static System.Data.DataTable QueryInvoiceChargesDataTable()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("Check");
			dt.Columns.Add("Invoice_No");
			dt.Columns.Add("CustomerName");
			dt.Columns.Add("DutyTax");
			dt.Columns.Add("EPF");
			dt.Columns.Add("WharfageHandling");
			dt.Columns.Add("FreightCollect");
			dt.Columns.Add("DeliveryOrderFee");
			dt.Columns.Add("Storage");
			dt.Columns.Add("Other");

			return dt;
		}

		public static System.Data.DataTable CustomsChequeRequisitionITF()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("requisition_no");
			dt.Columns.Add("consignment_no");
			dt.Columns.Add("CustomerName");
			dt.Columns.Add("ITFAmount");
			dt.Columns.Add("FreightCollectAmount");
			dt.Columns.Add("DisbursementTotal");

			return dt;
		}

		public static System.Data.DataTable CustomsChequeRequisitionDetailsRequest()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("action");
			dt.Columns.Add("enterpriseid");
			dt.Columns.Add("userloggedin");
			dt.Columns.Add("requisition_no");
			dt.Columns.Add("consignment_no");
			dt.Columns.Add("payerid");
			dt.Columns.Add("Invoice_No");
			dt.Columns.Add("ITFAmount");
			dt.Columns.Add("FreightCollectAmount");
			dt.Columns.Add("IgnoreMAWBValidation");

			return dt;
		}

		public static System.Data.DataSet ITFCustomsChequeRequisitionDetails(string strAppID, string strEnterpriseID, string userloggedin, System.Data.DataTable dtParams)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomsCheckRequisitionDAL","CustomsCheckRequisitionDAL","Customs_ChequeRequisitionDetails","DbConnection object is null!!");
				return null;
			}
			string action = dtParams.Rows[0]["action"].ToString();
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList(); 

			storedParams.Add(new System.Data.SqlClient.SqlParameter("@action",action));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));				
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
			if(dtParams.Rows[0]["requisition_no"] != DBNull.Value  && dtParams.Rows[0]["requisition_no"].ToString() !="")
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@requisition_no",dtParams.Rows[0]["requisition_no"].ToString()));

			if(dtParams.Rows[0]["consignment_no"] != DBNull.Value  && dtParams.Rows[0]["consignment_no"].ToString() !="")
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",dtParams.Rows[0]["consignment_no"].ToString()));

			if(dtParams.Rows[0]["payerid"] != DBNull.Value  && dtParams.Rows[0]["payerid"].ToString() !="")
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid",dtParams.Rows[0]["payerid"].ToString()));
			else
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid", DBNull.Value));

			if(dtParams.Rows[0]["Invoice_No"] != DBNull.Value  && dtParams.Rows[0]["Invoice_No"].ToString() !="")
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Invoice_No",dtParams.Rows[0]["Invoice_No"].ToString()));
			else
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Invoice_No", DBNull.Value));

			if(dtParams.Rows[0]["ITFAmount"] != DBNull.Value  && dtParams.Rows[0]["ITFAmount"].ToString() !="")
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ITFAmount",dtParams.Rows[0]["ITFAmount"].ToString()));

			if(dtParams.Rows[0]["FreightCollectAmount"] != DBNull.Value  && dtParams.Rows[0]["FreightCollectAmount"].ToString() !="")
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@FreightCollectAmount",dtParams.Rows[0]["FreightCollectAmount"].ToString()));

			if(dtParams.Rows[0]["IgnoreMAWBValidation"] != DBNull.Value  && dtParams.Rows[0]["IgnoreMAWBValidation"].ToString() !="")
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@IgnoreMAWBValidation",dtParams.Rows[0]["IgnoreMAWBValidation"].ToString()));

			result = (DataSet)dbCon.ExecuteProcedure("dbo.Customs_ChequeRequisitionDetails",storedParams,ReturnType.DataSetType);
			return result;
		}
	}
}
