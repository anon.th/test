using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;
namespace TIESDAL
{
	/// <summary>
	/// Summary description for RetSWBPcksBatchDAL.
	/// </summary>
	public class RetSWBPcksBatchDAL
	{
		public RetSWBPcksBatchDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public static DataSet getPackageDetail(String strAppID, String strEnterpriseID)
		{
		
		
			DbConnection dbCon= null;
			DataSet dsPkgDetail =null;
			IDbCommand dbcmd=null;
 
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("RetSWBPcksBatchDAL.cs","GetPakageDtlsSWB","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry.Append("select distinct * ,shipment.chargeable_wt char_wt "); 
			strQry.Append(" FROM Shipment INNER JOIN ");
			strQry.Append(" Shipment_PKG ON Shipment.consignment_no = Shipment_PKG.consignment_no ");
			strQry.Append(" where Shipment.applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and Shipment.enterpriseid = '");
			strQry.Append(strEnterpriseID+"'");
			strQry.Append(" and  Shipment.Booking_Datetime >=  ");
			strQry.Append("(select getdate()-convert(int,code_str_value) DayToRead from Core_System_Code  ");
			strQry.Append("where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and codeid = 'RetSWBPkgQuery");
			strQry.Append("' and code_text = 'DayToGetData");
			strQry.Append("')");


			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
		 
			try
			{
				dsPkgDetail = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RetSWBPckBatchDAL.cs","GetPakageDtlsSWB","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			return dsPkgDetail;
		}
		public static DataSet getPackageDetailByConsignment_No(String strAppID, String strEnterpriseID, string consignment_no)
		{
		
		
			DbConnection dbCon= null;
			DataSet dsPkgDetail =null;
			IDbCommand dbcmd=null;
 
			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("RetSWBPcksBatchDAL.cs","GetPakageDtlsSWB","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry.Append("select distinct * ,shipment.chargeable_wt char_wt "); 
			strQry.Append(" FROM Shipment INNER JOIN ");
			strQry.Append(" Shipment_PKG ON Shipment.consignment_no = Shipment_PKG.consignment_no ");
			strQry.Append(" where Shipment.applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and Shipment.enterpriseid = '");
			strQry.Append(strEnterpriseID+"' and Shipment.consignment_no='" + consignment_no + "' ");
			strQry.Append(" and  Shipment.Booking_Datetime >=  ");
			strQry.Append("(select getdate()-convert(int,code_str_value) DayToRead from Core_System_Code  ");
			strQry.Append("where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and codeid = 'RetSWBPkgQuery");
			strQry.Append("' and code_text = 'DayToGetData");
			strQry.Append("')");


			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
		 
			try
			{
				dsPkgDetail = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RetSWBPckBatchDAL.cs","GetPakageDtlsSWB","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			return dsPkgDetail;
		}
		public static DataSet getShipmentData(String strAppID, String strEnterpriseID,String consignmentNo)
		{
				DbConnection dbCon= null;
				DataSet dsShipment =null;
				IDbCommand dbcmd=null;
				//DataSet dsPkgDtlsData =null;
				//string Inv_No="";

				dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

				if (dbCon==null)
			{
				Logger.LogTraceError("RetSWBPcksBatchDAL.cs","GetPakageDtlsSWB","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry.Append("select * from shipment where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID+"'");
			strQry.Append("and consignment_no = '");
			strQry.Append(consignmentNo+"'");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
		 
			try
			{
				dsShipment = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
				catch(ApplicationException appException)
			{
				Logger.LogTraceError("RetSWBPckBatchDAL.cs","GetPakageDtlsSWB","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

		return dsShipment;
		}

		/// <summary>
		/// Get SWB packages data from table 'Package_Details'
		/// </summary>
		/// <param name="strAppID">TIES</param>
		/// <param name="strEnterpriseID">KDTH</param>
		/// <param name="consignmentNo">Shipment.consignment_no</param>
		/// <returns></returns>
		public static DataSet getPackageData(String strAppID, String strEnterpriseID,String consignmentNo)
		{
			DbConnection dbCon= null;
			DataSet dsShipment =null;
			IDbCommand dbcmd=null;
			//DataSet dsPkgDtlsData =null;
			//string Inv_No="";

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("RetSWBPcksBatchDAL.cs","GetPakageDtlsSWB","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			StringBuilder strQry =  new StringBuilder(); 
			strQry.Append("select * "); 
			strQry.Append(" FROM Shipment INNER JOIN ");
            strQry.Append(" Package_Details ON Shipment.consignment_no = Package_Details.consignment_no ");
			strQry.Append(" where applicationid = '");
			strQry.Append(strAppID);
			strQry.Append("' and enterpriseid = '");
			strQry.Append(strEnterpriseID+"'");
			strQry.Append(" and  Package_Details.consignment_no = '");
			strQry.Append(consignmentNo+"'");

			dbcmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);
		 
			try
			{
				dsShipment = (DataSet)dbCon.ExecuteQuery(dbcmd,com.common.DAL.ReturnType.DataSetType);  
			}
			catch(ApplicationException appException)
			{
				Logger.LogTraceError("RetSWBPckBatchDAL.cs","GetPakageDtlsSWB","ERR002","Error in Query String "+appException.Message);
				throw appException;
			}

			return dsShipment;
		}
		public static DataSet GetDayToRead(string m_strAppID,string m_strEnterpriseID)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("RetSWBPcksBatchDAL.cs","GetDayToRead","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("(select *,getdate()-convert(int,code_str_value) DayToRead from Core_System_Code  ");
			strQry.Append("where applicationid = '");
			strQry.Append(m_strAppID);
			strQry.Append("' and codeid = 'RetSWBPkgQuery");
			strQry.Append("' and code_text = 'DayToGetData");
			strQry.Append("')");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dsCons =(DataSet)dbCon.ExecuteQuery(dbCmd,com.common.DAL.ReturnType.DataSetType);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("RetSWBPckBatchDAL.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return dsCons;
		}
		public static int GetIdenticalPackage(string m_strAppID,string m_strEnterpriseID,string Booking_no,string Consignment_no)
		{
			DbConnection dbCon = null;
			//DataSet dsCons = null;
			IDbCommand dbCmd = null;
			int nInden = 0;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("RetSWBPcksBatchDAL.cs","GetIdenticalPackage","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("if((SELECT count(*)  FROM Package_Details where consignment_no = '" + Consignment_no.Trim() + "') = (SELECT count(*) ");
			strQry.Append(" FROM Package_Details INNER JOIN ");
			strQry.Append(" Shipment_PKG ON Package_Details.consignment_no = Shipment_PKG.consignment_no AND Package_Details.mps_no = Shipment_PKG.mps_code AND ");
			strQry.Append(" Package_Details.quantity = Shipment_PKG.pkg_qty AND Package_Details.weight = Shipment_PKG.pkg_wt AND ");
			strQry.Append(" Package_Details.length = Shipment_PKG.pkg_length AND Package_Details.breadth = Shipment_PKG.pkg_breadth AND ");
			strQry.Append(" Package_Details.height = Shipment_PKG.pkg_height");
			strQry.Append(" where Shipment_PKG.Booking_No = '");
			strQry.Append(Booking_no);
			strQry.Append("' and Shipment_PKG.Consignment_no = '");
			strQry.Append(Consignment_no);
			strQry.Append("'");
			strQry.Append(" and (SELECT count(*) from Shipment_PKG pkg where pkg.Booking_No = '" + Booking_no + "' and pkg.Consignment_no = '" + Consignment_no + "') = (SELECT count(*) from Package_Details pd where pd.Consignment_no = '" + Consignment_no + "'))) select 1 else select 0");

			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				nInden =(int)dbCon.ExecuteScalar(strQry.ToString());
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("RetSWBPckBatchDAL.cs","Populate","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
			return nInden;
		}


		public static int MoveDataToOriginal_Insert(string m_strAppID,string m_strEnterpriseID,string booking_no,string consignment_no)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",m_strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@EnterpriseId",m_strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@booking_no",booking_no));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",consignment_no));
	
			int result = (int)dbCon.ExecuteProcedure("RetSWBPcks_MoveData_Insert",storedParams);
			
			return result;

		}
		public static void UpdateFreightCharge(string m_strAppID,string m_strEnterpriseID,string booking_no,string consignment_no,decimal freightCharge)
		{
				DbConnection dbCon = null;
				DataSet dsCons = null;
				IDbCommand dbCmd = null;

				dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
				if(dbCon == null)
				{
					Logger.LogTraceError("RetSWBPcksBatchDAL.cs","UpdateFreightCharge","ERR001","dbCon is null");
					throw new ApplicationException("Connection to Database failed",null);
				}
			
				StringBuilder strQry = new StringBuilder();
				strQry = strQry.Append("UPDATE Shipment_PKG_BOX ");
				strQry.Append(" SET Freight_Charge = " + freightCharge);
				strQry.Append(" where Shipment_PKG_BOX.Booking_No = '");
				strQry.Append(booking_no);
				strQry.Append("' and Shipment_PKG_BOX.Consignment_no = '");
				strQry.Append(consignment_no);
				strQry.Append("'");

				dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

				try
				{
					dbCon.ExecuteNonQuery(dbCmd);
				}
				catch(ApplicationException appExpection)
				{
					Logger.LogTraceError("RetSWBPckBatchDAL.cs","UpdateFreightCharge","ERR002","Error in  Execute Query ");
					throw appExpection;
				}
			}
		public static void UpdateShipmentSurcharge(string m_strAppID,string m_strEnterpriseID,string booking_no,string consignment_no,decimal freightCharge,decimal decInsSurcharge,decimal totVASSurcharge ,decimal ESASurcharge,decimal OtherSurcharge,decimal tot_amt)
		{
			DbConnection dbCon = null;
			DataSet dsCons = null;
			IDbCommand dbCmd = null;

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("RetSWBPcksBatchDAL.cs","UpdateShipmentSurcharge","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			
			StringBuilder strQry = new StringBuilder();
			strQry = strQry.Append("UPDATE Shipment ");
			strQry.Append(" SET tot_freight_charge = " + freightCharge);
//			strQry.Append(" , insurance_surcharge = " + decInsSurcharge);
//			strQry.Append(" , tot_vas_surcharge = " + totVASSurcharge);
//			strQry.Append(" , esa_surcharge = " + ESASurcharge);
//			strQry.Append(" , other_surch_amount = " + OtherSurcharge);
			strQry.Append(" , total_rated_amount = " + tot_amt);

			strQry.Append(" where Shipment.Booking_No = '");
			strQry.Append(booking_no);
			strQry.Append("' and Shipment.Consignment_no = '");
			strQry.Append(consignment_no);
			strQry.Append("' and Shipment.applicationid = '");
			strQry.Append(m_strAppID);
			strQry.Append("' and Shipment.enterpriseid = '");
			strQry.Append(m_strEnterpriseID);
			strQry.Append("'");
 
			dbCmd = dbCon.CreateCommand(strQry.ToString(),CommandType.Text);

			try
			{
				dbCon.ExecuteNonQuery(dbCmd);
			}
			catch(ApplicationException appExpection)
			{
				Logger.LogTraceError("RetSWBPckBatchDAL.cs","UpdateShipmentSurcharge","ERR002","Error in  Execute Query ");
				throw appExpection;
			}
		}
		public  static void UpdateShipment_PKG_BOX(string strAppID,string strEnterpriseID,int iBooking_no ,String Consignment_no,String strPayerID ,String strSenderZipCode,String strRecipientZipCode,String strServiceCode,DataSet dsPkg  )
		{
			DbConnection dbCon= null;
 			IDbCommand dbCmd=null;
			int iRowsAffected = 0;
			StringBuilder strBuild = null;
			int cnt = 0;
			int i = 0;
			

			dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if (dbCon==null)
			{
				Logger.LogTraceError("RetSWBPckBatchDAL.cs","UpdateShipment_PKG_BOX","ERR001","dbCon is null");
				throw new ApplicationException("Connection to Database failed",null);
			}
			if (dsPkg == null)
			{
				Logger.LogTraceError("RetSWBPckBatchDAL","UpdateShipment_PKG_BOX","ERR002","DataSet is null!!");
				throw new ApplicationException("The Shipment DataSet is null",null);
			}
			
			IDbConnection conApp = dbCon.GetConnection();
			
			if(conApp == null)
			{
				Logger.LogTraceError("RetSWBPckBatchDAL","UpdateShipment_PKG_BOX","ERR003","conApp is null");
				throw new ApplicationException("Connection to Database failed",null);
			}

			try
			{
				dbCon.OpenConnection(ref conApp);
			}
			catch(ApplicationException appException)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("RetSWBPckBatchDAL","UpdateShipment_PKG_BOX","ERR004","Error opening connection : "+ appException.Message.ToString());
				throw new ApplicationException("Error opening database connection ",appException);
			}
			catch(Exception exception)
			{
				if(conApp != null)
				{
					conApp.Close();
				}
				Logger.LogTraceError("RetSWBPckBatchDAL","UpdateShipment_PKG_BOX","ERR005","Error opening connection : "+ exception.Message.ToString());
				throw new ApplicationException("Error opening database connection ",exception);
			}


			IDbTransaction transactionApp = dbCon.BeginTransaction(conApp,IsolationLevel.ReadCommitted);

			if(transactionApp == null)
			{
				Logger.LogTraceError("RetSWBPckBatchDAL","RetSWBPckBatchDAL","ERR006","transactionApp is null");
				throw new ApplicationException("Failed Begining a App Transaction..",null);
			}

			dbCmd = dbCon.CreateCommand("", CommandType.Text);
			dbCmd.Connection = conApp;
			dbCmd.Transaction = transactionApp;
			try
			{
			if(dsPkg != null)
			{
			/*	//delete from the shipment_pkg table
				strBuild = new StringBuilder();
				strBuild.Append("delete from shipment_pkg where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and booking_no <> ");
				strBuild.Append(iBooking_no); // Old booking No
				strBuild.Append(" and consignment_no = '");
				strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
				strBuild.Append("'");
				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
			*/

				strBuild = new StringBuilder();
				strBuild.Append("delete from shipment_pkg_box where applicationid = '");
				strBuild.Append(strAppID+"' and enterpriseid = '");
				strBuild.Append(strEnterpriseID);
				strBuild.Append("' and booking_no = ");
				strBuild.Append(iBooking_no);
				strBuild.Append(" and consignment_no = '");
				strBuild.Append(Utility.ReplaceSingleQuote(Consignment_no.ToString()));
				strBuild.Append("'");
				dbCmd.CommandText = strBuild.ToString();
				iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
				Logger.LogDebugInfo("RetSWBPckBatchDAL","UpdateShipment_PKG_BOX","INF002",iRowsAffected + " rows deleted from Shipment_pkg table");

				//insert into the shipment_pkg table
				cnt = dsPkg.Tables[0].Rows.Count;
				i = 0;
				Customer customer = new Customer();
				customer.Populate(strAppID,strEnterpriseID, strPayerID);
				Zipcode zipcode = new Zipcode();

				zipcode.Populate(strAppID,strEnterpriseID,strSenderZipCode);
				String strOrgZone = zipcode.ZoneCode;

				zipcode.Populate(strAppID,strEnterpriseID,strRecipientZipCode);
				String strDestnZone = zipcode.ZoneCode;
				int sumQty = 0;
				float weightToFreight = 0;
				decimal freightCharge = 0;
				for(i=0;i<cnt;i++)
				{
					DataRow drEachPKG = dsPkg.Tables[0].Rows[i];
					String strMpsNo = "";
					if(drEachPKG["mps_no"].ToString() != "")
					{
						strMpsNo = (String)drEachPKG["mps_no"];
					}
							
					decimal decPkgLgth = Convert.ToDecimal(drEachPKG["pkg_length"]);
					decimal decPkgBrdth = Convert.ToDecimal(drEachPKG["pkg_breadth"]);
					decimal decPkgHgth = Convert.ToDecimal(drEachPKG["pkg_height"]);
					decimal decPkgWt = Convert.ToDecimal(drEachPKG["pkg_wt"]);
					int iPkgQty = Convert.ToInt32(drEachPKG["pkg_qty"]);
					decimal decTotalWt = Convert.ToDecimal(drEachPKG["tot_wt"]);
					decimal decTotal_act_Wt = Convert.ToDecimal(drEachPKG["tot_act_wt"]);//TU on 17June08
					decimal decTotalDimWt = Convert.ToDecimal(drEachPKG["tot_dim_wt"]);
					decimal decChrgbleWt = Convert.ToDecimal(drEachPKG["chargeable_wt"]);

					if(float.Parse(drEachPKG["tot_wt"].ToString())>float.Parse(drEachPKG["tot_dim_wt"].ToString()))
					{
							if(customer.IsCustomer_Box== "Y")
							weightToFreight = float.Parse(drEachPKG["tot_wt"].ToString())/int.Parse(drEachPKG["pkg_qty"].ToString());
						else
							weightToFreight = float.Parse(drEachPKG["tot_wt"].ToString());
					}
					else
					{
						if(customer.IsCustomer_Box== "Y")
							weightToFreight = float.Parse(drEachPKG["tot_dim_wt"].ToString())/int.Parse(drEachPKG["pkg_qty"].ToString());
						else
							weightToFreight = float.Parse(drEachPKG["tot_dim_wt"].ToString());
					}
					sumQty = sumQty+int.Parse(drEachPKG["pkg_qty"].ToString());
					freightCharge = (decimal)TIESUtility.ComputeFreightCharge(strAppID, strEnterpriseID,
						strPayerID, "C", 
						strOrgZone, strDestnZone, weightToFreight,
						strServiceCode, strSenderZipCode, strRecipientZipCode);

					if(customer.IsCustomer_Box == "Y")
						freightCharge = (freightCharge * int.Parse(drEachPKG["pkg_qty"].ToString()));

					
					strBuild = new StringBuilder();		
					/*
					//TU on 17June08
					strBuild.Append("insert into shipment_pkg (applicationid,enterpriseid,booking_no,consignment_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_wt,tot_act_wt,tot_dim_wt,chargeable_wt)values('");
					strBuild.Append(strAppID+"','");
					strBuild.Append(strEnterpriseID+"',");
					strBuild.Append(iBooking_no); //New booking no
					strBuild.Append(",'");
					strBuild.Append(Utility.ReplaceSingleQuote(drEach["consignment_no"].ToString()));
					strBuild.Append("','");
					strBuild.Append(strMpsNo+"',");
					strBuild.Append(decPkgLgth+",");
					strBuild.Append(decPkgBrdth+",");
					strBuild.Append(decPkgHgth+",");
					strBuild.Append(decPkgWt+",");
					strBuild.Append(iPkgQty+",");
					strBuild.Append(decTotalWt+",");
					strBuild.Append(decTotal_act_Wt+",");//TU on 17June08
					strBuild.Append(decTotalDimWt+",");
					strBuild.Append(decChrgbleWt+")");
				
					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					*/

					//Chai	18/04/2012
					/*
					strBuild = new StringBuilder();
					strBuild.Append("insert into shipment_pkg_box (applicationid,enterpriseid,booking_no,consignment_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_wt,tot_act_wt,tot_dim_wt,chargeable_wt,Freight_Charge)values('");
					strBuild.Append(strAppID+"','");
					strBuild.Append(strEnterpriseID+"',");
					strBuild.Append(iBooking_no); //New booking no
					strBuild.Append(",'");
					strBuild.Append(Utility.ReplaceSingleQuote(Consignment_no.ToString()));
					strBuild.Append("','");
					strBuild.Append(strMpsNo+"',");
					strBuild.Append(decPkgLgth+",");
					strBuild.Append(decPkgBrdth+",");
					strBuild.Append(decPkgHgth+",");
					strBuild.Append(decPkgWt+",");
					strBuild.Append(iPkgQty+",");
					strBuild.Append(decTotalWt+",");
					strBuild.Append(decTotal_act_Wt+",");//TU on 17June08
					strBuild.Append(decTotalDimWt+",");
					strBuild.Append(decChrgbleWt+",");
					strBuild.Append(freightCharge+")");
				
					dbCmd.CommandText = strBuild.ToString();
					iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
					*/
					Logger.LogDebugInfo("RetSWBPckBatchDAL","UpdateShipment_PKG_BOX","INF003",iRowsAffected + " rows inserted into shipment_pkg table");
				}					
				if(sumQty < int.Parse(customer.Minimum_Box))
				{
					int packageAdd = int.Parse(customer.Minimum_Box)-sumQty;
					int mpsNo = sumQty+1;
					freightCharge = (decimal)TIESUtility.ComputeFreightCharge(strAppID, strEnterpriseID,
						strPayerID, "C", 
						strOrgZone, strDestnZone, 0,
						strServiceCode, strSenderZipCode, strRecipientZipCode);
					for(int k=0;k<packageAdd;k++)
					{
						//Chai	18/04/2012
						/*
						strBuild = new StringBuilder();
						strBuild.Append("insert into shipment_pkg_box (applicationid,enterpriseid,booking_no,consignment_no,mps_code,pkg_length,pkg_breadth,pkg_height,pkg_wt,pkg_qty,tot_wt,tot_act_wt,tot_dim_wt,chargeable_wt,Freight_Charge)values('");
						strBuild.Append(strAppID+"','");
						strBuild.Append(strEnterpriseID+"',");
						strBuild.Append(iBooking_no); //New booking no
						strBuild.Append(",'");
						strBuild.Append(Utility.ReplaceSingleQuote(Consignment_no.ToString()));
						strBuild.Append("','");
						strBuild.Append(mpsNo.ToString()+"',");
						strBuild.Append(0+",");
						strBuild.Append(0+",");
						strBuild.Append(0+",");
						strBuild.Append(0+",");
						strBuild.Append(0+",");
						strBuild.Append(0+",");
						strBuild.Append(0+",");//TU on 17June08
						strBuild.Append(0+",");
						strBuild.Append(0+",");
						strBuild.Append(freightCharge+")");

						dbCmd.CommandText = strBuild.ToString();
						iRowsAffected = dbCon.ExecuteNonQueryInTransaction(dbCmd);
						*/
						mpsNo = mpsNo+1;
						Logger.LogDebugInfo("RetSWBPckBatchDAL","UpdateShipment_PKG_BOX","INF003",iRowsAffected + " rows inserted into shipment_pkg_box table");
					}
				}
			}
							transactionApp.Commit();
				Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF008","App db insert transaction committed.");

			}
				
			catch(ApplicationException appException)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF009","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR007","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}


				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR008","Insert Transaction Failed : "+ appException.Message.ToString());
				throw new ApplicationException("Error adding Shipment : "+appException.Message,appException);
			}
			catch(Exception exception)
			{
				try
				{
					transactionApp.Rollback();
					Logger.LogDebugInfo("DomesticShipmentMgrDAL","UpdateDomesticShp","INF010","App db insert transaction rolled back.");

				}
				catch(Exception rollbackException)
				{
					Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR009","Error during app transanction roll back "+ rollbackException.Message.ToString());
				}

				Logger.LogTraceError("DomesticShipmentMgrDAL","UpdateDomesticShp","ERR010","Insert Transaction Failed : "+ exception.Message.ToString());
				throw new ApplicationException("Error adding Shipment ",exception);
			}
			finally
			{
				if(conApp != null)
				{
					conApp.Close();
				}
			}
			//return iRowsAffected;
		}

		public static int RetrieveSWBPkg_Details_to_DMS(string m_strAppID, string m_strEnterpriseID, string m_userID)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",m_strEnterpriseID));
			//storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",m_strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@booking_date", null));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@updated_by", m_userID));
			int result = (int)dbCon.ExecuteProcedure("RetrieveSWBPkg_Details_to_DMS",storedParams);
			
			return result;

		}

		public static DataSet RetrieveSWBPkg_CheckJob(string m_strAppID, string m_strEnterpriseID)
		{
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(m_strAppID,m_strEnterpriseID);
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",m_strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",m_strAppID));		
	
			DataSet ds = (DataSet)dbCon.ExecuteProcedure("RetrieveSWBPkg_CheckJob",storedParams,ReturnType.DataSetType);

			return ds;
		}

		}
	}

