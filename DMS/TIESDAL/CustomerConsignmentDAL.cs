using System;
using System.Data;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using com.ties.classes;
using System.Collections;
using System.Text.RegularExpressions;
using TIESClasses;

namespace TIESDAL
{
	/// <summary>
	/// Summary description for CustomerConsignmentDAL.
	/// </summary>
	public class CustomerConsignmentDAL
	{
		public CustomerConsignmentDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		public static CustomerAccount GetCustomerAccount(string strAppID, string strEnterpriseID, string userloggedin)
		{
			CustomerAccount cust = null;
			SessionDS sessionDS = null;			
			String strSQL = "";

			strSQL = "SELECT * FROM dbo.CustomerAccount2('" + strEnterpriseID + "','"+userloggedin+"') ";
				
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","GetCustomerAccount","GetCustomerAccount","DbConnection object is null!!");
				return null;
			}
			
			
			sessionDS = dbCon.ExecuteQuery(strSQL, 0, 0, "CustomerAccount");
			if(sessionDS.ds != null && sessionDS.ds.Tables["CustomerAccount"] != null && sessionDS.ds.Tables["CustomerAccount"].Rows.Count>0)
			{
				cust=new CustomerAccount();
				System.Data.DataRow dr = sessionDS.ds.Tables["CustomerAccount"].Rows[0];
				if(dr["IsEnterpriseUser"] != null && Convert.ToInt32(dr["IsEnterpriseUser"])==1)
				{
					cust.IsEnterpriseUser=true;
				}

				if(dr["IsCustomerUser"] != null && Convert.ToInt32(dr["IsCustomerUser"])==1)
				{
					cust.IsCustomerUser=true;
				}

				if(dr["IsCustomsUser"] != null && Convert.ToInt32(dr["IsCustomsUser"])==1)
				{
					cust.IsCustomsUser=true;
				}

				if(dr["IsMasterCustomerUser"] != null && Convert.ToInt32(dr["IsMasterCustomerUser"])==1)
				{
					cust.IsMasterCustomerUser=true;
				}

				if(dr["payerid"] != null)
				{
					cust.Payerid=dr["payerid"].ToString();
				}

				if(dr["DefaultSender"] != null)
				{
					cust.DefaultSender=dr["DefaultSender"].ToString();
				}

				if(dr["cust_name"] != null)
				{
					cust.Cust_name=dr["cust_name"].ToString();
				}

				if(dr["currency"] != null)
				{
					cust.Currency=dr["currency"].ToString();
				}

				if(dr["currency_decimal"] != null)
				{
					try
					{
						cust.Currency_decimal=Decimal.Parse(dr["currency_decimal"].ToString());
					}
					catch(Exception exception) 
					{
						Logger.LogDebugInfo("CustomerConsignmentDAL","GetCustomerAccount","GetCustomerAccount","App db Currency_decimal Decimal.Parse : '"+exception.Message+"'.");
					}
				}
			}
			return  cust;
		}


		public static System.Data.DataSet CSS_Consignment(string strAppID, string strEnterpriseID, string userloggedin, int action)
		{
			System.Data.DataSet result = new DataSet();		
			String strSQL = "";

			strSQL = "SELECT * FROM dbo.CustomerAccount('" + strEnterpriseID + "','"+userloggedin+"') ";
				
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","GetCustomerAccount","GetCustomerAccount","DbConnection object is null!!");
				return null;
			}
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@action",action));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));

			result = (DataSet)dbCon.ExecuteProcedure("CSS_Consignment",storedParams,ReturnType.DataSetType);
			return result;
		}


		public static System.Data.DataSet CSS_Consignment(string strAppID, string strEnterpriseID, string userloggedin, System.Data.DataTable dtParams)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","GetCustomerAccount","GetCustomerAccount","DbConnection object is null!!");
				return null;
			}
			string action = dtParams.Rows[0]["action"].ToString();
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			if(action == "0")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@action",action));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",dtParams.Rows[0]["consignment_no"].ToString()));
				if(dtParams.Rows[0]["payerid"] != DBNull.Value  && dtParams.Rows[0]["payerid"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid",dtParams.Rows[0]["payerid"].ToString()));
				}
			}
			else if(action == "1")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@action",action));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
				if(dtParams.Rows[0]["OverrideSenderName"] != DBNull.Value  && dtParams.Rows[0]["OverrideSenderName"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@OverrideSenderName",dtParams.Rows[0]["OverrideSenderName"].ToString()));
				}
				if(dtParams.Rows[0]["consignment_no"] != DBNull.Value  && dtParams.Rows[0]["consignment_no"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",dtParams.Rows[0]["consignment_no"].ToString()));
				}				
				if(dtParams.Rows[0]["payerid"] != DBNull.Value  && dtParams.Rows[0]["payerid"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid",dtParams.Rows[0]["payerid"].ToString()));
				}
			}
			else if(action == "3")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@action",action));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
				if(dtParams.Rows[0]["consignment_no"] != DBNull.Value  && dtParams.Rows[0]["consignment_no"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",dtParams.Rows[0]["consignment_no"].ToString()));
				}				
				if(dtParams.Rows[0]["payerid"] != DBNull.Value  && dtParams.Rows[0]["payerid"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid",dtParams.Rows[0]["payerid"].ToString()));
				}
				if(dtParams.Rows[0]["ForceDelete"] != DBNull.Value  && dtParams.Rows[0]["ForceDelete"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ForceDelete",dtParams.Rows[0]["ForceDelete"].ToString()));
				}
				else
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ForceDelete",1));
				}
			}
			else
			{
				foreach(System.Data.DataColumn col in dtParams.Columns)
				{
					string colName = col.ColumnName;
					if(colName =="OverrideSenderName")
					{
						continue;
					}

					if(colName=="ContractAccepted")
					{
						if(dtParams.Rows[0][colName]==DBNull.Value)
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,DBNull.Value));										
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
					}
					else if(colName.ToLower()=="recipient_fax")
					{
						if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,""));	
						}
					}
					else
					{
						if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
					}

				}	
			}
			
	

			result = (DataSet)dbCon.ExecuteProcedure("CSS_Consignment",storedParams,ReturnType.DataSetType);
			return result;
		}


		public static System.Data.DataSet UpdateReferences(string strAppID, string strEnterpriseID, string userloggedin, System.Data.DataTable dtParams, string recipient_CostCentre)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","UpdateReferences","UpdateReferences","DbConnection object is null!!");
				return null;
			}
			
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@applicationid",strAppID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@telephone",dtParams.Rows[0]["recipient_telephone"].ToString()));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@reference_name",dtParams.Rows[0]["recipient_name"].ToString()));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@address1",dtParams.Rows[0]["recipient_address1"].ToString()));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@address2",dtParams.Rows[0]["recipient_address2"].ToString()));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@zipcode",dtParams.Rows[0]["recipient_zipcode"].ToString()));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@fax",dtParams.Rows[0]["recipient_fax"].ToString()));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@contactperson",dtParams.Rows[0]["recipient_contact_person"].ToString()));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@cost_centre", recipient_CostCentre));

//			string z1 = dtParams.Rows[0]["recipient_telephone"].ToString();
//			string z2 = dtParams.Rows[0]["recipient_name"].ToString();
//			string z3 = dtParams.Rows[0]["recipient_address1"].ToString();
//			string z4 = dtParams.Rows[0]["recipient_address2"].ToString();
//			string z5 = dtParams.Rows[0]["recipient_zipcode"].ToString();
//			string z6 = dtParams.Rows[0]["recipient_fax"].ToString();
//			string z7 = dtParams.Rows[0]["recipient_contact_person"].ToString();
//			string z8 = recipient_CostCentre;
//			string z9 = strAppID;
//			string z10 = strEnterpriseID;
//			string z11 = userloggedin;

			result = (DataSet)dbCon.ExecuteProcedure("updateReferences",storedParams,ReturnType.DataSetType);
			DataSet ds = result;
			DataTable dt = ds.Tables[0];
			if(dt != null && dt.Rows.Count>0)
			{
				string msgStatus = dt.Rows[0]["msgStatus"].ToString();
				string msgMessage = dt.Rows[0]["msgMessage"].ToString();
//				string zz= "";
			}

			return result;
		}

		public static int CustomerConsProcessStep(string strAppID, string strEnterpriseID, string userloggedin,string payerid,string consignmentno)
		{
			int result = 0;
			SessionDS sessionDS = null;			
			String strSQL = "";

			strSQL = "SELECT ProcessStep FROM dbo.CustomerConsProcessStep('" + strEnterpriseID + "'";
			if(userloggedin != null && userloggedin.Trim() != "")
			{
				strSQL +=",'"+userloggedin+"'";
			}
			else
			{
				strSQL +=",null";
			}
			if(payerid != null && payerid.Trim() != "")
			{
				strSQL +=",'"+payerid+"'";
			}
			else
			{
				strSQL +=",null";
			}

			if(consignmentno != null && consignmentno.Trim() != "")
			{
				strSQL +=",'"+consignmentno+"'";
			}
			else
			{
				strSQL +=",null";
			}
			
			strSQL += ") ";
				
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","GetCustomerAccount","GetCustomerAccount","DbConnection object is null!!");
				return -1;
			}
			sessionDS = dbCon.ExecuteQuery(strSQL, 0, 0, "CustomerConsProcessStep");
			if(sessionDS.ds != null && sessionDS.ds.Tables["CustomerConsProcessStep"] != null && sessionDS.ds.Tables["CustomerConsProcessStep"].Rows.Count>0 && sessionDS.ds.Tables["CustomerConsProcessStep"].Rows[0]["ProcessStep"] != DBNull.Value)
			{
				result = int.Parse(sessionDS.ds.Tables["CustomerConsProcessStep"].Rows[0]["ProcessStep"].ToString());
			}
			return result;
		}


		public static System.Data.DataTable ConsignmentParameter()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("action");
			dt.Columns.Add("enterpriseid");
			dt.Columns.Add("userloggedin");
			dt.Columns.Add("consignment_no");
			dt.Columns.Add("service_code");
			dt.Columns.Add("ref_no");
			dt.Columns.Add("sender_name");
			dt.Columns.Add("sender_address1");
			dt.Columns.Add("sender_address2");
			dt.Columns.Add("sender_zipcode");
			dt.Columns.Add("sender_telephone");
			dt.Columns.Add("sender_fax");
			dt.Columns.Add("sender_contact_person");
			dt.Columns.Add("sender_email");
			dt.Columns.Add("recipient_telephone");
			dt.Columns.Add("recipient_name");
			dt.Columns.Add("recipient_address1");
			dt.Columns.Add("recipient_address2");
			dt.Columns.Add("recipient_zipcode");
			dt.Columns.Add("recipient_fax");
			dt.Columns.Add("recipient_contact_person");
			dt.Columns.Add("declare_value");
			dt.Columns.Add("cod_amount");
			dt.Columns.Add("remark");
			dt.Columns.Add("return_pod_slip");
			dt.Columns.Add("return_invoice_hc");
			dt.Columns.Add("DangerousGoods");
			dt.Columns.Add("ContractAccepted");
			dt.Columns.Add("PackageDetails");
			dt.Columns.Add("GoodsDescription");
			dt.Columns.Add("payerid");
			dt.Columns.Add("OverrideSenderName");
			dt.Columns.Add("ForceDelete");
			return dt;
		}


		public static System.Data.DataSet EnterpriseContract(string strAppID, string strEnterpriseID, string userloggedin, string payerid)
		{
			System.Data.DataSet result = new DataSet();		
			string strSQL = "";

			strSQL = "SELECT AcceptedCurrentContract,ContractText FROM dbo.EnterpriseContract('" + strEnterpriseID + "','"+payerid+"','"+userloggedin+"') ";
				
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","EnterpriseContract","EnterpriseContract","DbConnection object is null!!");
				return null;
			}
			
			result= (DataSet)dbCon.ExecuteQuery(strSQL,null,ReturnType.DataSetType);			
			//result = sessionDS.ds;
			return result;
		}


		public static SessionDS GetReferenceDS(string strAppID, string strEnterpriseID, string strCustID,string PayerId)
		{

			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetReferenceDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("SELECT snd_rec_name FROM dbo.GetSenderNames('");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("','");
			strBuilder.Append(strCustID);
			strBuilder.Append("','");
			strBuilder.Append(PayerId);
			strBuilder.Append("') ");
			
			strBuilder.Append (" Order by snd_rec_name"); //Mohan, 20/11/2002
			String strSQLQuery = strBuilder.ToString();
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,0,0,"ReferenceTable");

			return  sessionDS;
	
		}


		public static SessionDS GetReferenceDefaultDS(string strAppID, string strEnterpriseID, string strCustID, string strDefaultName, int iCurrent, int iDSRecSize)
		{

			SessionDS sessionDS = null;


			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);

			if(dbCon == null)
			{
				Logger.LogTraceError("SysDataMgrDAL","GetReferenceDS","EDB101","DbConnection object is null!!");
				return sessionDS;
			}
			if(strDefaultName ==null || strDefaultName.Trim() =="")
			{
				strDefaultName="-1";
			}

			StringBuilder strBuilder = new StringBuilder();
			strBuilder.Append("select snd_rec_name,snd_rec_type,contact_person,email,address1,address2,country,zipcode,telephone,fax from Customer_Snd_Rec where applicationid = '");
			strBuilder.Append(strAppID);
			strBuilder.Append("' and enterpriseid = '");
			strBuilder.Append(strEnterpriseID);
			strBuilder.Append("' and custid = '");
			strBuilder.Append(strCustID);
			strBuilder.Append("'");
			strBuilder.Append(" and snd_rec_name = '");
			strBuilder.Append(strDefaultName);
			strBuilder.Append("' AND snd_rec_type IN ('S', 'B') ");
			
			strBuilder.Append (" Order by snd_rec_name"); //Mohan, 20/11/2002
			String strSQLQuery = strBuilder.ToString();
			
			sessionDS = dbCon.ExecuteQuery(strSQLQuery,iCurrent,iDSRecSize,"ReferenceTable");

			return  sessionDS;
	
		}


		public static System.Data.DataSet GetZipcode(string strAppID, string strEnterpriseID, string zipcode)
		{
			System.Data.DataSet result = new DataSet();		
			string strSQL = "";

			strSQL = "SELECT a.zipcode, s.state_name,b.Country FROM dbo.Zipcode a JOIN dbo.Enterprise b ";
			strSQL += "ON a.applicationid = b.applicationid AND a.enterpriseid = b.enterpriseid ";
			strSQL += "AND a.Country = b.Country ";
			strSQL += "Join dbo.State  s ON a.state_code = s.state_code ";
			strSQL += "WHERE a.applicationid = '"+strAppID+"' AND a.enterpriseid =  '"+strEnterpriseID+"' ";
			strSQL += "AND a.zipcode = '"+zipcode+"' ";
				
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","GetZipcode","GetZipcode","DbConnection object is null!!");
				return null;
			}
			
			SessionDS sessionDS = dbCon.ExecuteQuery(strSQL, 0, 0, "Zipcode");
			result = sessionDS.ds;
			return result;
		}


		public static System.Data.DataSet GetCustomerStatus(string strAppID, string strEnterpriseID)
		{
			System.Data.DataSet result = new DataSet();		
			string strSQL = "";

			strSQL = "SELECT DisplayStatus=code_num_value, StatusID=sequence ";
			strSQL += "FROM dbo.Enterprise_Configurations ";
			strSQL += "WHERE applicationid = '"+strAppID+"' AND enterpriseid =  '"+strEnterpriseID+"' ";
			strSQL += "AND codeid = 'CustomerStatus' ORDER BY CASE sequence WHEN 0 THEN 999 ELSE sequence END";
				
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","GetZipcode","GetZipcode","DbConnection object is null!!");
				return null;
			}
			
			SessionDS sessionDS = dbCon.ExecuteQuery(strSQL, 0, 0, "CustomerStatus");
			result = sessionDS.ds;
			return result;
		}

		public static System.Data.DataSet GetCustomsStatus(string strAppID, string strEnterpriseID)
		{
			System.Data.DataSet result = new DataSet();		
			string strSQL = "";

			strSQL = "SELECT DisplayStatus=code_num_value, StatusID=sequence ";
			strSQL += "FROM dbo.Enterprise_Configurations ";
			strSQL += "WHERE applicationid = '"+strAppID+"' AND enterpriseid =  '"+strEnterpriseID+"' ";
			strSQL += "AND codeid IN ('CustomerStatus', 'CustomsStatus') AND sequence NOT IN (192, 193) ";
			strSQL += "ORDER BY CASE sequence WHEN 0 THEN 999 ELSE sequence END";
				
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","GetZipcode","GetZipcode","DbConnection object is null!!");
				return null;
			}
			
			SessionDS sessionDS = dbCon.ExecuteQuery(strSQL, 0, 0, "CustomerStatus");
			result = sessionDS.ds;
			return result;
		}


		public static System.Data.DataSet CSS_ConsignmentStatus(string strAppID, string strEnterpriseID, string userloggedin, string SenderName
			, string consignment_no, int status_id, string ShippingList_No, string recipient_telephone
			, string ref_no, string recipient_zipcode, string service_code, DateTime datefrom, DateTime dateto,string payerid,string MasterAWBNumber,string JobEntryNo)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","CSS_ConsignmentStatus","CSS_ConsignmentStatus","DbConnection object is null!!");
				return null;
			}
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
			if(SenderName != null && SenderName.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@SenderName",SenderName));
			}
			
			if(consignment_no != null && consignment_no.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",consignment_no));
			}
			
			if(status_id != -1)
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@status_id",status_id));
			}
			
			if(ShippingList_No != null && ShippingList_No.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ShippingList_No",ShippingList_No));
			}
			
			if(recipient_telephone != null && recipient_telephone.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@recipient_telephone",recipient_telephone));
			}
			
			if(ref_no != null && ref_no.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ref_no",ref_no));
			}
			
			if(recipient_zipcode != null && recipient_zipcode.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@recipient_zipcode",recipient_zipcode));
			}
			
			if(service_code != null && service_code.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@service_code",service_code));
			}
			
			if(datefrom != DateTime.MinValue)
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@datefrom",datefrom));
			}
			
			if(dateto != DateTime.MinValue)
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@dateto",dateto));
			}

			if(payerid != null && payerid.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid",payerid));
			}

			if(MasterAWBNumber != null && MasterAWBNumber.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@MasterAWBNumber",MasterAWBNumber));
			}

			if(JobEntryNo != null && JobEntryNo.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@JobEntryNo",JobEntryNo));
			}
			
			result = (DataSet)dbCon.ExecuteProcedure("CSS_ConsignmentStatus",storedParams,ReturnType.DataSetType);
			return result;
		}


		public static System.Data.DataSet CSS_PrintShippingList(string strAppID, string strEnterpriseID, string userloggedin, string ConsignmentsList)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","CSS_PrintShippingList","CSS_PrintShippingList","DbConnection object is null!!");
				return null;
			}
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
			if(ConsignmentsList != null && ConsignmentsList.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ConsignmentsList",ConsignmentsList));
			}						
			
			result = (DataSet)dbCon.ExecuteProcedure("CSS_PrintShippingList",storedParams,ReturnType.DataSetType);
			return result;
		}
		
		public static DataSet CSS_PrintConsNotes( String strAppID ,String strEnterpriseID, String strUserLogin, String arrConsignmentsList )
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomerConsignmentDAL","CSS_PrintConsNotes","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",strUserLogin));
				if(arrConsignmentsList != null && arrConsignmentsList.Trim() != "")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@ConsignmentsList",arrConsignmentsList));
				}						
			
				dsResult = (DataSet)dbCon.ExecuteProcedure("CSS_PrintConsNotes",storedParams,ReturnType.DataSetType);
			
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomerConsignmentDAL", "CSS_PrintConsNotes", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			}
			
		}

		
		public static System.Data.DataSet CSS_ReprintConsNote(string strAppID, string strEnterpriseID, string userloggedin, string ConsignmentsList, int DataSource ,int BookingNo)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","CSS_ReprintConsNote","CSS_ReprintConsNote","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			
			try
			{
					
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" EXEC dbo.CSS_ReprintConsNote  ");
				strQry.Append(" @enterpriseid  = '"+ strEnterpriseID +"' "); 
				strQry.Append(" ,@userloggedin  = '"+ userloggedin +"' "); 
				strQry.Append(" ,@ConsignmentsList  = '"+ ConsignmentsList +"' "); 
				strQry.Append(" ,@DataSource  = "+ DataSource +" "); 
				if(BookingNo >0)
				{
					strQry.Append(" ,@BookingNo  = '"+ BookingNo +"' "); 
				}
			
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
					
				return dsResult;
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "ConfigureCustomerSelfServiceMgrDAL", "CSS_ReprintConsNote", ex.Message);
				throw new ApplicationException("Execute failed"); 
			}
			finally
			{
				dsResult.Dispose();
			}

		}
	
		public static System.Data.DataSet ReprintInvoicedConsNote(string strAppID, string strEnterpriseID, string userloggedin, string ConsignmentsList, int DataSource ,int BookingNo)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null;
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","CSS_ReprintConsNote","CSS_ReprintConsNote","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			
			try
			{
					
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" EXEC dbo.ReprintInvoicedConsNote  ");
				strQry.Append(" @enterpriseid  = '"+ strEnterpriseID +"' "); 
				strQry.Append(" ,@userloggedin  = '"+ userloggedin +"' "); 
				strQry.Append(" ,@ConsignmentsList  = '"+ ConsignmentsList +"' "); 
				strQry.Append(" ,@DataSource  = "+ DataSource +" "); 
				if(BookingNo >0)
				{
					strQry.Append(" ,@BookingNo  = '"+ BookingNo +"' "); 
				}
			
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
					
				return dsResult;
			}
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "ConfigureCustomerSelfServiceMgrDAL", "CSS_ReprintConsNote", ex.Message);
				throw new ApplicationException("Execute failed"); 
			}
			finally
			{
				dsResult.Dispose();
			}

		}

		public static System.Data.DataSet CSS_RetrieveCustomerCons(string strAppID, string strEnterpriseID, string ConsignmentsList,int BookingNo,string Invoice_No,string consignment_no)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","CSS_RetrieveCustomerCons","CSS_RetrieveCustomerCons","DbConnection object is null!!");
				return null;
			}
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			if(ConsignmentsList != null && ConsignmentsList.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ShippingList_No",ConsignmentsList));
			}
			else
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ShippingList_No",DBNull.Value));				
			}
			if(BookingNo != -1)
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@booking_no",BookingNo));
			}

			if(Invoice_No != null && Invoice_No.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Invoice_No",Invoice_No));
			}
			else
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Invoice_No",DBNull.Value));				
			}

			if(consignment_no != null && consignment_no.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",consignment_no));
			}
			else
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",DBNull.Value));				
			}
			
			result = (DataSet)dbCon.ExecuteProcedure("dbo.CSS_RetrieveCustomerCons",storedParams,ReturnType.DataSetType);
			return result;
		}

		public static System.Data.DataSet CSS_RetrieveConsignmentsToDMS(string strAppID, string strEnterpriseID, string userloggedin, string ConsignmentsList,int Booking_No,DateTime PUP_DT)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","CSS_RetrieveConsignmentsToDMS","CSS_RetrieveConsignmentsToDMS","DbConnection object is null!!");
				return null;
			}
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
			if(ConsignmentsList != null && ConsignmentsList.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ConsignmentsList",ConsignmentsList));
			}		
			if(Booking_No != -1)
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Booking_No",Booking_No));
			}		
			if(PUP_DT != DateTime.MinValue)
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@PUP_DT",PUP_DT));
			}	
			
			result = (DataSet)dbCon.ExecuteProcedure("CSS_RetrieveConsignmentsToDMS",storedParams,ReturnType.DataSetType);
			return result;
		}


		public static System.Data.DataSet Customs_InvoiceReceivedByOps(string strAppID, string strEnterpriseID, string userloggedin, string Invoice_No)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","Customs_InvoiceReceivedByOps","Customs_InvoiceReceivedByOps","DbConnection object is null!!");
				return null;
			}
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
			if(Invoice_No != null && Invoice_No.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Invoice_No",Invoice_No));
			}					
			
			result = (DataSet)dbCon.ExecuteProcedure("dbo.Customs_InvoiceReceivedByOps",storedParams,ReturnType.DataSetType);
			return result;
		}


		public static System.Data.DataTable JobRegistrationParameter()
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("action");
			dt.Columns.Add("enterpriseid");
			dt.Columns.Add("userloggedin");
			dt.Columns.Add("payerid");
			dt.Columns.Add("consignment_no");
			dt.Columns.Add("JobEntryNo");
			dt.Columns.Add("TransportMode");

			dt.Columns.Add("ExporterName");
			dt.Columns.Add("ExporterAddress1");
			dt.Columns.Add("ExporterAddress2");
			dt.Columns.Add("ExporterCountry");
			dt.Columns.Add("LoadingPortCity");
			dt.Columns.Add("LoadingPortCountry");
			dt.Columns.Add("DischargePort");
			dt.Columns.Add("CurrencyCode");
			dt.Columns.Add("InfoReceivedDate");
			dt.Columns.Add("MasterAWBNumber");

			dt.Columns.Add("ShipFlightNo");
			dt.Columns.Add("ExpectedArrivalDate");
			dt.Columns.Add("ActualArrivalDate");
			dt.Columns.Add("EntryType");
			dt.Columns.Add("DeliveryTerms");
			dt.Columns.Add("ExpressDoorToDoor");	
			dt.Columns.Add("ExpressCity");	

			dt.Columns.Add("Total_pkgs");	
			dt.Columns.Add("Total_weight");	

			dt.Columns.Add("ChargeableWeight");	
			dt.Columns.Add("IgnoreMAWBValidation");	
			return dt;
		}


		public static System.Data.DataSet Customs_JobRegistration(string strAppID, string strEnterpriseID, string userloggedin, System.Data.DataTable dtParams)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","CustomerConsignmentDAL","Customs_JobRegistration","DbConnection object is null!!");
				return null;
			}
			string action = dtParams.Rows[0]["action"].ToString();
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
			if(action == "0")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@action",action));
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));				
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
				if(dtParams.Rows[0]["payerid"] != DBNull.Value  && dtParams.Rows[0]["payerid"].ToString() !="")
				{
					storedParams.Add(new System.Data.SqlClient.SqlParameter("@payerid",dtParams.Rows[0]["payerid"].ToString()));
				}
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@consignment_no",dtParams.Rows[0]["consignment_no"].ToString()));
			}
			else
			{
				foreach(System.Data.DataColumn col in dtParams.Columns)
				{
					string colName = col.ColumnName;
					if(colName=="JobEntryNo")
					{
						if(dtParams.Rows[0][colName]==DBNull.Value)
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,DBNull.Value));										
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
					}
					else if(colName.ToLower()=="ExporterAddress1")
					{
						if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,""));	
						}
					}
					else if(colName.ToLower()=="ExporterAddress2")
					{
						if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,""));	
						}
					}
					else if(colName.ToLower()=="ActualArrivalDate")
					{
						if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
						else
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,""));	
						}
					}
					else
					{
						if(dtParams.Rows[0][colName] != null && dtParams.Rows[0][colName].ToString().Trim() != "")
						{
							storedParams.Add(new System.Data.SqlClient.SqlParameter("@"+colName,dtParams.Rows[0][colName].ToString()));										
						}
					}
				}	
			}
			
			result = (DataSet)dbCon.ExecuteProcedure("Customs_JobRegistration",storedParams,ReturnType.DataSetType);
			return result;
		}


		public static string GetEnterpriseUserAccounts(string strAppID, string strEnterpriseID, string userloggedin)
		{			
			string strSQL = "";
			string cusid="";

			strSQL = "select [custid] from dbo.CSS_EnterpriseUserAccounts ";
			strSQL += "where [enterpriseid] = '"+strEnterpriseID+"' and [userid] = '"+userloggedin+"'";
				
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","GetEnterpriseUserAccounts","GetEnterpriseUserAccounts","DbConnection object is null!!");
				return null;
			}
			
			
			DataSet result= (DataSet)dbCon.ExecuteQuery(strSQL,null,ReturnType.DataSetType);	
			if(result.Tables[0].Rows.Count>0 && result.Tables[0].Rows[0]["custid"] != DBNull.Value)
			{
				cusid=result.Tables[0].Rows[0]["custid"].ToString();
			}
			return  cusid;
		}

		public static DataSet GetEnterpriseUserAccountsDataSet(string strAppID, string strEnterpriseID, string userloggedin)
		{			
			string strSQL = "";
			strSQL = "select [custid] from dbo.CSS_EnterpriseUserAccounts ";
			strSQL += "where [enterpriseid] = '"+strEnterpriseID+"' and [userid] = '"+userloggedin+"'";
				
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","GetEnterpriseUserAccounts","GetEnterpriseUserAccounts","DbConnection object is null!!");
				return null;
			}
			
			
			DataSet result= (DataSet)dbCon.ExecuteQuery(strSQL,null,ReturnType.DataSetType);	
			return  result;
		}

		public string GetCustoms_Jobs_Invoices(string strAppID, string strEnterpriseID,string cons_no)
		{
			string Invoice_No="";
			string strSQL = "";
			strSQL = "SELECT Invoice_No FROM dbo.Customs_Jobs_Invoices ";
			strSQL += "WHERE [enterpriseid] = '"+strEnterpriseID+"' and [consignment_no] = '"+cons_no+"'";
				
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","GetEnterpriseUserAccounts","GetEnterpriseUserAccounts","DbConnection object is null!!");
				return null;
			}
						
			DataSet result= (DataSet)dbCon.ExecuteQuery(strSQL,null,ReturnType.DataSetType);	
			if(result.Tables[0].Rows.Count>0)
			{
				Invoice_No=result.Tables[0].Rows[0]["Invoice_No"].ToString();
			}
			return Invoice_No;
		}

		public static System.Data.DataSet QueryInvoicedConsignment(string strAppID, string strEnterpriseID, string userloggedin, string PayerID
			, string consignment_no, DateTime ManifestedDataFrom, DateTime ManifestedDatato, DateTime InvoiceDataFrom, DateTime InvoiceDatato
            , DateTime PODDataFrom, DateTime PODDatato, int PngafInvoiceNo)
		{
			System.Data.DataSet result = new DataSet();		
			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("CustomerConsignmentDAL","QueryInvoicedConsignment","QueryInvoicedConsignment","DbConnection object is null!!");
				return null;
			}
			System.Collections.ArrayList storedParams = new System.Collections.ArrayList();
//			storedParams.Add(new System.Data.SqlClient.SqlParameter("@enterpriseid",strEnterpriseID));
//			storedParams.Add(new System.Data.SqlClient.SqlParameter("@userloggedin",userloggedin));
			if(PayerID != null && PayerID.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@Payerid",PayerID));
			}
			
			if(consignment_no != null && consignment_no.Trim() != "")
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ConsignmentNo",consignment_no));
			}
			
			if(ManifestedDataFrom != DateTime.MinValue)
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ManifestDateFROM",ManifestedDataFrom));
			}
			
			if(ManifestedDatato != DateTime.MinValue)
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@ManifestDateTO",ManifestedDatato));
			}
			
			if(InvoiceDataFrom != DateTime.MinValue)
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@InvoiceDateFROM",InvoiceDataFrom));
			}
			
			if(InvoiceDatato != DateTime.MinValue)
			{
				storedParams.Add(new System.Data.SqlClient.SqlParameter("@InvoiceDateTO",InvoiceDatato));
			}

            if (PODDataFrom != DateTime.MinValue)
            {
                storedParams.Add(new System.Data.SqlClient.SqlParameter("@PODDateFROM", PODDataFrom));
            }

            if (PODDatato != DateTime.MinValue)
            {
                storedParams.Add(new System.Data.SqlClient.SqlParameter("@PODDateTO", PODDatato));
            }

            if (PngafInvoiceNo != 0)
            {
                storedParams.Add(new System.Data.SqlClient.SqlParameter("@PNGAFInvoiceNo", PngafInvoiceNo));
            }

            result = (DataSet)dbCon.ExecuteProcedure("QueryInvoicedConsignment",storedParams,ReturnType.DataSetType);
			return result;
		}
	}
}