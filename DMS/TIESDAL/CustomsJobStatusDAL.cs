using System;
using System.Data;
using System.Data.Common;
using com.common.DAL;
using com.common.util;
using com.common.classes;
using System.Data.SqlClient;
using System.Collections;
using System.Text;

namespace TIESDAL
{
	/// <summary>
	/// Summary description for CustomsJobStatusDAL.
	/// </summary>
	public class CustomsJobStatusDAL
	{
		public CustomsJobStatusDAL()
		{
		}

		public System.Data.DataSet GetDropDownListDisplayValue(String strAppID, String strEnterpriseID, String codeID, String sqlFunction)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			com.common.DAL.DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsJobStatusDAL","SearchCustomJobCompiling","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT DropDownListDisplayValue, DefaultValue, [Description], CodedValue  ");
				strQry.Append(" FROM dbo." + sqlFunction + "( '"+strEnterpriseID+"', '"+codeID+"', 1) " );
				strQry.Append(" ORDER BY DropDownListDisplayValue");
				 
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "SearchCustomJobCompiling", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}


		public System.Data.DataSet GetJobStatus(String strAppID, String strEnterpriseID)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			com.common.DAL.DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsJobStatusDAL","GetJobStatus","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT DropDownListDisplayValue, DefaultValue, [Description], CodedValue  ");
				strQry.Append(" from dbo.GetCustomsSpecialConfiguration('"+strEnterpriseID+"','CustomsStatus',1) " ); 
				 								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetJobStatus", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}


		public System.Data.DataSet GetCustomerGroupings(String strAppID, String strEnterpriseID)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			com.common.DAL.DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsJobStatusDAL","GetJobStatus","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append(" SELECT DropDownListDisplayValue, DefaultValue, [Description], CodedValue  ");
				strQry.Append(" from dbo.GetCustomsSpecialConfiguration('"+strEnterpriseID+"','CustomerGroupings',1) " ); 
				 								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetJobStatus", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}

		public System.Data.DataSet GetOriginDC(String strAppID, String strEnterpriseID)
		{
			DataSet dsResult = new DataSet();
			IDbCommand dbCmd = null; 
			com.common.DAL.DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID,strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("TIESDAL","CustomsJobStatusDAL","GetJobStatus","EDB101","DbConnection object is null!!");
				throw new ApplicationException("Connection to Database failed", null); 
			}
			try
			{
				StringBuilder strQry = new StringBuilder();
				strQry.Append("SELECT origin_code='' UNION ALL SELECT origin_code FROM dbo.Distribution_Center WHERE applicationid='"+strAppID+"' AND enterpriseid='"+strEnterpriseID+"'");
								
				dbCmd = dbCon.CreateCommand(strQry.ToString(), CommandType.Text);
				dsResult = (DataSet)dbCon.ExecuteQuery(dbCmd, com.common.DAL.ReturnType.DataSetType);
 
				return dsResult;
			}	
			catch(Exception ex)
			{
				Logger.LogTraceError("TIESDAL", "CustomJobCompilingDAL", "GetJobStatus", ex.Message);
				throw new ApplicationException("Select command failed"); 
			}
			finally
			{
				dsResult.Dispose();
			} 

		}

	}
}
