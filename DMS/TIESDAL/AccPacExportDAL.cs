using System;
using System.Data;
using System.Collections;
using com.common.classes;
using com.common.DAL;
using com.common.util;
using System.Text;
using System.Data.SqlClient;

namespace com.ties.DAL
{
	/// <summary>
	/// Summary description for AccPacExportDAL.
	/// </summary>
	public class AccPacExportDAL
	{
		public AccPacExportDAL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static DataSet GetInvoicesMatching(String strAppID, String strEnterpriseID, String PeriodFrom, String PeriodTo, String ExportedBy)
		{
			IDbCommand dbCmd = null;	
			DataSet ds = new DataSet();
			string strSQL = @"SELECT    t1.exported_date,
										t1.exported_by,
										COUNT(t1.exported_date) 'NoOfCons',
											SUM(t1.DomesticDocRev) 'DomesticDocRev',
											SUM(t1.DomesticFreightRev) 'DomesticFreightRev',
											SUM(t1.ExportDocRev) 'ExportDocRev',
											SUM(t1.ExportFreightRev) 'ExportFreightRev',
											t2.invoice_list 'invoice_no'
									FROM
									(SELECT LEFT(convert(nvarchar(50), exported_date, 103) + ' ' + convert(nvarchar(50), exported_date, 108), 16) as exported_date, exported_by, iv.jobid,
										(SELECT ISNULL(SUM(S.invoice_amt), 0)
										FROM dbo.Shipment S
										INNER JOIN dbo.Enterprise E ON S.sender_country = E.country
										WHERE E.applicationid = 'TIES'
											AND E.enterpriseid = 'PNGAF'
											AND S.intl_docs_flag = 'Y'
											AND iv.invoice_status = 'A'
											AND  S.export_flag != 'Y'
											AND S.invoice_no = iv.invoice_no) AS 'DomesticDocRev',
										(SELECT ISNULL(SUM(S.invoice_amt), 0)
										FROM dbo.Shipment S
										INNER JOIN dbo.Enterprise E ON S.sender_country = E.country
										WHERE E.applicationid = 'TIES'
											AND E.enterpriseid = 'PNGAF'
											AND S.invoice_no = iv.invoice_no
											AND iv.invoice_status = 'A'
										    AND  S.export_flag != 'Y'
											AND S.intl_docs_flag = 'N') AS 'DomesticFreightRev',
										(SELECT ISNULL(SUM(S.invoice_amt), 0)
										FROM dbo.Shipment S
										WHERE S.export_flag = 'Y'
											AND S.invoice_no = iv.invoice_no
											AND iv.invoice_status = 'A'
											AND S.intl_docs_flag = 'Y') AS 'ExportDocRev',
										(SELECT ISNULL(SUM(S.invoice_amt), 0)
										FROM dbo.Shipment S
										WHERE S.export_flag = 'Y'
											AND S.invoice_no = iv.invoice_no
											AND iv.invoice_status = 'A'
											AND S.intl_docs_flag = 'N') AS 'ExportFreightRev',
																		iv.invoice_no
									FROM dbo.Invoice iv
									WHERE NOT iv.exported_date IS NULL ";
//			string strSQL = @"SELECT    exported_date, 
//										exported_by,
//										(select COUNT(*) from dbo.Invoice_Detail id where id.invoice_no = iv.invoice_no) AS 'NoOfCons',
//										(select ISNULL(SUM(S.invoice_amt), 0)
//											from dbo.Shipment S
//											inner join dbo.Enterprise E on S.sender_country = E.country
//											where E.applicationid = 'TIES'
//												and E.enterpriseid = 'PNGAF'
//												and S.invoice_no IS NULL
//												and S.invoice_no = iv.invoice_no) AS 'DomesticDocRev',
//										(select ISNULL(SUM(S.invoice_amt), 0)
//											from dbo.Shipment S
//											inner join dbo.Enterprise E on S.sender_country = E.country
//											where E.applicationid = 'TIES'
//												and E.enterpriseid = 'PNGAF'
//												and S.invoice_no IS NULL
//												and S.service_code IN ('OWS', 'WTS', 'PAK')
//												and S.invoice_no = iv.invoice_no) AS 'DomesticFreightRev',
//										(select ISNULL(SUM(S.invoice_amt), 0)
//											from dbo.Shipment S
//											where S.export_flag = 'Y'
//											and S.invoice_no = iv.invoice_no) AS 'ExportDocRev',
//										(0) AS 'ExportFreightRev',
//										iv.invoice_no
//									FROM dbo.Invoice iv 
//								    WHERE NOT iv.exported_date IS NULL ";

			if(PeriodFrom != "")
				strSQL += "AND exported_date >= '" + DateTime.ParseExact(PeriodFrom+" 01:00:00","dd/MM/yyyy HH:mm:ss",null).ToString() + "'";
			if(PeriodTo != "")
				strSQL += "AND exported_date <= '" + DateTime.ParseExact(PeriodTo+" 23:59:59","dd/MM/yyyy HH:mm:ss",null).ToString() + "'";
			if(ExportedBy != "")
				strSQL += "AND exported_by = '" + ExportedBy + "'";

			strSQL += ") t1 cross apply GetInvoiceListForAccPac(t1.exported_date) t2 GROUP BY t1.exported_date, t1.exported_by, t2.invoice_list Order by exported_date desc ";

			DbConnection dbCon = DbConnectionManager.GetInstance().GetDbConnection(strAppID, strEnterpriseID);
			if(dbCon == null)
			{
				Logger.LogTraceError("AccPacExportDAL","GetInvoicesMatching","GetInvoicesMatching","DbConnection object is null!!");
				return ds;
			}
			
			dbCmd = dbCon.CreateCommand(strSQL, CommandType.Text);
			ds = (DataSet)dbCon.ExecuteQuery(dbCmd,ReturnType.DataSetType);
			return  ds;
		}
	}
}
